## [1.51.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.51.1...v1.51.2) (2022-05-16)


### Bug Fixes

* **scholarship:** fix checking user attendances ([bb6c64a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bb6c64aebbbc895838b6d59398d07901f28c085a))

## [1.51.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.51.0...v1.51.1) (2022-05-06)


### Performance Improvements

* **applications:** remove relateds from target notifications actions ([ad67e33](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ad67e338f3c7673ed52f568f96daca8fe7bc83a0))

# [1.51.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.50.0...v1.51.0) (2022-05-04)


### Features

* **scholarship:** change field optional monthly report ([98045fd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/98045fd90c8e0dffed10bbf1eb9b1b23f5d71e3a))

# [1.50.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.49.0...v1.50.0) (2022-05-03)


### Features

* **scholarship:** add application to response ([6ef3027](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6ef30273a94a84b067ec80ad804eb437a9c0c54f))
* **scholarship:** fix cache problem ([fd5c569](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fd5c569d928fada2cc0223470fa735c755a84172))
* **scholarship:** missing changes in report ([a436f8f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a436f8f3a0e10b017aa88fab0aa771244c6083da))

# [1.49.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.48.4...v1.49.0) (2022-04-28)


### Features

* **scholarship:** check for open attendances ([dd6b9dd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/dd6b9dd9cf853d2678c2da84b1257fd169b5f10f))
* **scholarship:** dispacth value ([6b8cccb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6b8cccb7a26172541406c392b9c12a35c19a5e61))
* **scholarship:** endpoint for changing closing date ([5f509fb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5f509fba29c68cce199cd2a53f74d3b6a892756d))

## [1.48.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.48.3...v1.48.4) (2022-04-13)


### Bug Fixes

* **payment-grid:** fix current_account name on null ([092508c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/092508c815956488204a8e5f218faa7da7839f2e))

## [1.48.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.48.2...v1.48.3) (2022-04-12)


### Bug Fixes

* **payments-grid:** remove logging ([b688d85](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b688d8561baf5da7db46e91018ef2ccaadff60d6))

## [1.48.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.48.1...v1.48.2) (2022-04-12)


### Bug Fixes

* **payment-grid:** fix getCurrentAccount method ([9fa7fcf](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9fa7fcf04245dfe527001dbc55453566aa6fe86f))

## [1.48.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.48.0...v1.48.1) (2022-04-11)


### Bug Fixes

* **payment-grid:** make  user_account_id optional field ([c1766e5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c1766e5f8e9cd668bf8cf0f576ed3bf0a2c69c5c))

# [1.48.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.47.0...v1.48.0) (2022-04-06)


### Features

* **social:** express interes in approved, selection and colaboration ([095c754](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/095c754d97e8c766f6e872ecbe45e799f92f563b))

# [1.47.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.6...v1.47.0) (2022-04-01)


### Bug Fixes

* **social scholarship:** attendances validation by device ([126b5a4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/126b5a4346a7b9c8a7052c19b1eaf445b907faf1))


### Features

* **social scholarship:** attendances validation ([f34287c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f34287caa1f38ecc6a309eceb4053de580af663a))
* **social scholarship:** check for all attendances in same period ([b8149e7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b8149e773d19e643a2287e263921b59af2ad6ec9))

## [1.46.6](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.5...v1.46.6) (2022-03-11)


### Bug Fixes

* **attendances:** n_hours_reject must be string ([b5eac99](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b5eac9988934a7fdc161983785a913b29de5702b))

## [1.46.5](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.4...v1.46.5) (2022-03-11)


### Bug Fixes

* **attendaces:** remove n_hours_reject from status validation ([fbdb3d5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fbdb3d52133348ffc1c984a1b6db65ef6c6d9098))

## [1.46.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.3...v1.46.4) (2022-03-11)


### Bug Fixes

* **attendances:** rename status params name ([f04728e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f04728e2463be0ded7f505d73184322f2d8fd031))
* **payments:** current acccout can be null ([64668db](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/64668db1bb604b7def516c3f57e87fc68b84ea48))

## [1.46.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.2...v1.46.3) (2022-02-18)


### Bug Fixes

* **applications:** remove dges fields ([19ad5aa](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/19ad5aa106f8f226cb873c0e88d69715a6f58ef6))

## [1.46.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.1...v1.46.2) (2022-02-17)


### Performance Improvements

* **applications:** remove awaits from send notifications ([e068b0b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e068b0b815dc7af3b3b1f884d491a5dfa48d3c3c))

## [1.46.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.46.0...v1.46.1) (2022-02-16)


### Bug Fixes

* **experiences:** dont validate perc_student_iban on webpage ([5f33c40](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5f33c40172a8fc300eff9674f7354fc59fa49097))

# [1.46.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.45.0...v1.46.0) (2022-01-25)


### Bug Fixes

* **scholarship:** seed solving issue ([de59a62](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/de59a6274bb76f6e3b8298b4566c271614bc343a))
* **scholarship:** without_active_collaborations missing option ([a848a6e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a848a6e4a80405ea8ef15d01f2249c70de6907d1))


### Features

* **scholarship:** configurations improvment and lint issues ([4bd164b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4bd164b993fc8c4a4e8066111adb745dcef833fc))

# [1.45.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.44.0...v1.45.0) (2022-01-18)


### Features

* students without active collaborations ([78db878](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/78db878e28389ae6b0ef0cf0a51b8f610938507b))

# [1.44.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.43.0...v1.44.0) (2022-01-10)


### Features

* fix errors, automatic application accepted ([3c3dc08](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3c3dc087418a1400ade05191be98e60e9077b5ad))

# [1.43.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.42.1...v1.43.0) (2022-01-05)


### Features

* new experience, user experience, application ([e109fa9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e109fa90acc30aace56c699d57775b25cedd66b6))

## [1.42.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.42.0...v1.42.1) (2022-01-05)


### Bug Fixes

* **experiences:** change dashboard scopes check ([f569ef7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f569ef7db1f70a33e5b3488ee25bca98973eab74))

# [1.42.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.41.0...v1.42.0) (2021-12-06)


### Features

* payment monthly report add field ([be79cf7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/be79cf7b61f1c323340563e2e99aedbe3e6bc11f))

# [1.41.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.40.1...v1.41.0) (2021-12-03)


### Features

* address publications, new filters ([0227457](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/022745728cb64a24e01e1ed02395b71245c10ed4))

## [1.40.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.40.0...v1.40.1) (2021-12-02)


### Bug Fixes

* **configurations:** remove  limit from list ([7578823](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7578823cc8dfa79fab2bd33e1f01c50993278985))

# [1.40.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.39.0...v1.40.0) (2021-12-02)


### Features

* decision status correction ([a27ffe1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a27ffe1fd10884dea723ad0675aa05fcf056f8df))
* decision status migration ([0f00ba5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0f00ba51942fde5ad4348eb37375118c2284a4b7))
* has current account configuration ([779ed7e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/779ed7e3b5c98f1df3f0885c996f26683277e0cf))

# [1.39.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.38.0...v1.39.0) (2021-12-02)


### Features

* payment grid report ([6251a60](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6251a608da845ce58ffdc64a126f2daceba69df4))

# [1.38.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.37.0...v1.38.0) (2021-11-29)


### Features

* status management, attendance rejection ([4d22132](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4d22132be69cb847cde2883bd0cbeed8f758b8fc))

# [1.37.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.36.0...v1.37.0) (2021-11-24)


### Features

* change key to uppercase ([d2503fa](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d2503fa313e5b66cd5163530a41a8299e75f509d))
* edit candid,anexos,horas,campos,notif,entrev ([eb7c779](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/eb7c779d1ee0309c169470d2aaf122eb199b88da))

# [1.36.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.35.0...v1.36.0) (2021-11-09)


### Features

* automatic status, profile in report ([5087c1a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5087c1a26d51727f3c3bfa08b34cbc292c592b6e))

# [1.35.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.34.1...v1.35.0) (2021-10-28)


### Features

* generate experience offer report ([fe01c34](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fe01c34c76794b00ced7250b78153dbafbab7f1e))

## [1.34.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.34.0...v1.34.1) (2021-10-26)


### Bug Fixes

* **applications:** save applications files ([a8224af](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a8224af196ca580ac6b2a938ffa35307dc821def))

# [1.34.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.33.0...v1.34.0) (2021-10-14)


### Bug Fixes

* **application_machine:** add new transactions ([0b8bc2c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0b8bc2c9536477363021c44d102096445c5a7c8d))
* **applications:** small bug ([c873391](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c873391eb656f509092fc6487af49608bff356f4))
* **attendances:** small fix ([58ef8f3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/58ef8f3bdf3c1807f78f98911626da8d80d546fb))
* **experienceUserInterest:** small fix ([4c30b61](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4c30b6178e57fed3f061002e3fb16ee4fe5bb4bd))
* **user_interest_machine:** update machine user_interest ([96235d8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/96235d8045d4957d52096b0a2b09fc38f0a7784f))


### Features

* **applications:** new status transations ([e65bbf2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e65bbf21bcd194a223645d99731d6a38b31f0ccc))
* **applications:** remove fields ([aca292e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aca292e7e9dafe497f3340f28a06423246979afb))
* **constants:** add dispach interest ([3cadaea](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3cadaea1346230efdfb7ee2f213f54b905b7f058))
* **constants:** add dispatch constant ([6c9bf9d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6c9bf9d555c30f918a4ac50d8a6b444a86937402))
* **constants:** add dispatch experience ([bbf7534](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bbf75345073c7d5856303c4bebee44e2d774a837))
* **experience:**  add dispach, decision, reject_reason ([7db8e86](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7db8e86838c37ff9f18ce91284118e213fd3d299))
* **experience_machine:** update machine ([20bb80f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/20bb80fe321a4e192451f5de5c5e8c148ff2f789))
* **experience:** add decision and reject_reason ([ddc14dc](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ddc14dc1ced682aee9b762ba08fcde92363869a7))
* **migrations:** add new status in experience ([f709ab3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f709ab39f4662193a476bab36ad6d4b7ef06011b))
* **migrations:** add status dispatch ([7dcf54c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7dcf54c726d62e7743967ac071845c18480d6179))
* **migrations:** add status dispatch ([bacb912](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bacb912f28aef4824102310e3f069b4b63c27d4c))

# [1.33.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.32.4...v1.33.0) (2021-10-11)


### Bug Fixes

* **attendances:** update calculate times ([b1aacc7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b1aacc7cfd45c8f7d29aef7420f1568dcc2bd081))
* **payment-grid:** update calculate hours and values ([4345747](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/434574748ea8f92690aa9bdc21d3c5c2eb0415f6))
* **payment-grid:** update current_account ([695a2e2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/695a2e2e362d24ef8de26bebbb6ae5bfd6d68c7c))


### Features

* **application_files:** add service ([bec886f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bec886f8b8173b37f2385097d2df8fa5e8a9d751))
* **applications:** add new fields ([530309b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/530309b30e4312ef2d73d3b826042577fcc829b1))
* **applications:** add relation files ([ffe07bd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ffe07bd1864bea0e9edc2cda0c8325ff2c395df4))
* **configurations:** add endpoint get/set configuration 'responsable_profile' ([0c1ad0f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0c1ad0f539a877bdb8c1fc91affbb3c9d564eff7))
* **migration:** add new fields in applications ([998ba14](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/998ba14cf6616ebf2fbb06bfd3e6211a6c83f638))
* **migrations:** add application_file table ([33974d6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/33974d6371076000f2f8714a8e523b69f67af01c))
* **migrations:** add migration update attendandes ([bc8aec3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bc8aec3fc7802859d9540a46797f57747d9ba7d6))
* **seeds:** add 'responsable-profile' in configurations ([5eddc81](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5eddc81c3d5c778e6941603362679395487167ff))

## [1.32.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.32.3...v1.32.4) (2021-10-07)


### Bug Fixes

* **experiences:** update info for template ([dfcd7b3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/dfcd7b322f917467564745541bb12be178e1a4fc))

## [1.32.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.32.2...v1.32.3) (2021-10-06)


### Bug Fixes

* **applications:** remove interview file required ([393b22f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/393b22fbe67063b6687cb2bc5ecad47574d0f9d1))
* **experienceUserInterests:** remove file interview required ([e6c5dd3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e6c5dd3036e4f9b7facd388e900b79e8c8197260))

## [1.32.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.32.1...v1.32.2) (2021-09-15)


### Bug Fixes

* **endpoints-stats:** add new validation ([00ef353](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/00ef35362cc6147da86466e59193d26ef6f94b1c))

## [1.32.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.32.0...v1.32.1) (2021-09-09)


### Bug Fixes

* **experiences:** update hours error ([3279c54](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3279c5480352c735aa9064393bc414bb1f58c77b))

# [1.32.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.31.0...v1.32.0) (2021-09-09)


### Bug Fixes

* **experienceUserInterest:** add clear cache ([cbd086d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cbd086dd849be9f537b7fceda0a44d00ad7b597a))
* **experienceUserInterest:** update endpoimt stats ([4580e75](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4580e7554c3ff8f1399023da39920962625082b3))


### Features

* **applications:** update endpoimt stats ([f81241b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f81241be2e5226d6234b3b35e7b6357c026f9fcd))
* **experiences:** update endpoimt stats ([448e9fe](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/448e9fec0587c3c7244450267247c236139b330a))

# [1.31.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.30.0...v1.31.0) (2021-09-08)


### Bug Fixes

* **experiences:** dashboard upadte endpoint ([d64ec7a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d64ec7aa17a75b45567931c717f3d097d3a799f8))
* **experiences:** update message, update notification error ([52e2e19](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/52e2e19e62313973b10c927228f44d63f85d693a))


### Features

* **experiences:** add notifications title experience ([d33e9cb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d33e9cb985b1bcd02234c5b7b8623d4d7ecb3a2c))

# [1.30.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.29.0...v1.30.0) (2021-08-30)


### Bug Fixes

* **seeds:** update seeds configurations ([253c033](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/253c033df35e966542bb5c9342e95b347a4f0266))


### Features

* **migrations:** update table configurations ([aef739a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aef739a72fef7e1fa4f7d63b91e8f64e946237e0))

# [1.29.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.28.2...v1.29.0) (2021-08-04)


### Features

* **seed:** add seed configurations current-account ([94d966f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/94d966f144137b781c609cd88d3fd3ca63ee70e1))

## [1.28.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.28.1...v1.28.2) (2021-07-30)


### Bug Fixes

* **experienceUserInterests:** refactor date error ([43dbfd7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/43dbfd748b864a49469ba70b37b8b4c56e7f7e05))

## [1.28.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.28.0...v1.28.1) (2021-07-27)


### Bug Fixes

* **applications:** update template info ([324b4a9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/324b4a9ad862e3f6ec41e7918a638b73aac86db0))

# [1.28.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.27.1...v1.28.0) (2021-07-27)


### Bug Fixes

* **experiences:** remove current_acccount_id ([40d0298](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/40d02986a218403dbe7a51540d4a32cd190f5c9b))


### Features

* **configurations:** set and gte configuration current account ([5459a0d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5459a0d88fb1c4a38b091919d5e7906ccd54722d))
* **migrations:** remove field current_account_id from table experience ([b8fec29](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b8fec2949e51641cfffcbdfb2b09068f69436f84))
* **payment_grid:** get current_account from configurations ([70f271e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/70f271ebbd2c7d7c25d77397b221185d8953914a))

## [1.27.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.27.0...v1.27.1) (2021-07-13)


### Bug Fixes

* **experiences:** add validation number_weekly_hours > total_hours_estimation ([0f4e301](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0f4e301d27b06a978e327016858e222dbf7ed424))
* **experiences:** add validation on update number_weekly_hours > total_hours_estimation ([ea8f59b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ea8f59b66716af4e8260656266e6521f46befb80))
* **experiences:** key error rename ([34ec8e9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/34ec8e9ff99a7cae368c2fc0116dbeccd71c1a4a))
* **experiences:** update key ([0a3c324](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0a3c324eb15448ced1a91a1df4da92c70590a48e))

# [1.27.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.26.0...v1.27.0) (2021-07-12)


### Bug Fixes

* **absence_reason:** accept field, reject_reason, update endpint accept/reject ([3940ec0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3940ec05e9149d09c41ae87e0eee710f4d82872d))
* **absence_reason:** add and remove validations, set start and end date to the attedance ([0cbe5ad](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0cbe5addea068f66c24e4975d92ce6cf75f71427))
* **applications:** rename key error ([7985d52](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7985d52c1db0ae06107b3a095c2844ad8a1f4c66))


### Features

* **migrations:** update table absence_reason ([2be298a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2be298a1ad9791c038a3ed1f5b4a8f5c2b7637de))

# [1.26.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.5...v1.26.0) (2021-07-09)


### Bug Fixes

* **experiences:** add number_interests in experiences list ([9633219](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/96332191c9057c946116300ea1a155f11b44e128))


### Features

* **experienceUserInterest:** add count interests by experience_id ([6bcf638](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6bcf638faa57fb5ef8f26149c29c44aac0ebd7b5))

## [1.25.5](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.4...v1.25.5) (2021-07-09)


### Bug Fixes

* **applications:** format dates applications ([095d8b8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/095d8b821516d63d0cc08f409cb11aed8ac2862c))

## [1.25.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.3...v1.25.4) (2021-07-07)


### Bug Fixes

* **applications:** validation in get ([5749a72](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5749a72a7a1a9fc93a980ed1e70a11a899d51b50))
* **experienceUserInterest:** return get ([b92df8a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b92df8a09c3d91a14b173313a58dee918ec124ed))

## [1.25.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.2...v1.25.3) (2021-07-06)


### Bug Fixes

* **experiences:** remove status filter ([da2bf09](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/da2bf09b176248a60fca5a97de173c6b480e986d))

## [1.25.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.1...v1.25.2) (2021-07-06)


### Bug Fixes

* **experiences:** add validation start date and end date ([4b48fed](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4b48fed6c45b4565d7aa0c34c3aceaaf2634a4df))
* **experiences:** format date ([a54a159](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a54a15954b13d38e4ad1cb449f524872ae5b08db))
* **experienceUserInterest:** fix name scopes ([4a5ca11](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4a5ca11bd251ab8ee51250eae63f0fbe163a7475))

## [1.25.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.25.0...v1.25.1) (2021-07-05)


### Bug Fixes

* **applicationHistory:** omit info user ([93c1661](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/93c166137b801b5739e479adb6ae6ab5359a2c51))
* **applications:** omit info user ([a2feb6c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a2feb6cbe78d3f849bbe6a912bacc2b42e351310))
* **attendances:** add validation access data and validation access ([454a7b1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/454a7b11d9716f97ccfaf5f53a98bdf498ba8a60))
* **attendances:** resp get ([405801b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/405801b0b8ece7b3c5b1c3f3710b4aa35f7694ff))
* **attendances:** type error access ([b0674ad](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b0674ad0e4ed77c8bac412f58944b0f887803b23))
* **complain_applications:** omit info user ([39d43d0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/39d43d0ff6004efbee90f54258dcafac16da1525))
* **complain_experience:** omit info user ([3734701](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3734701f886470a855df242d9492b65a0ea9e2c2))
* **complain_user_interest:** omit info user ([3044895](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/30448953304f658d2cb0674b1d2f38e4356d67c6))
* **experience_history:** omit info user ([5d0f1c9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d0f1c90da319f17811f209c5b57c48ac62cb9af))
* **experiences:** omit info user and add validation access data ([41aa58e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/41aa58e59b173d53f7802eaff9ff262f286ff400))
* **experiences:** type error access ([61da611](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/61da611f414190c4c2ac5b2a003d53a36e62a5a2))
* **experiences:** update query isAvaliable ([a204df1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a204df1b0242c654e92bd459f8ac24b1f8f58997))
* **experiencesUserInterest:** add validation acess data, endpoint for responsable advisor ([fed3f6a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fed3f6a782029aaa75b8eac295016bf9380c8542))
* **experiencesUserInterest:** update if and remove logger ([7a5ac5c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7a5ac5c43ad051ec502e77b922e79af2b9bf0b6e))
* **experienceUserInterest:** omit info responsable ([0794c40](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0794c406dc1aa26a245e290a5085c47e6580d207))
* **experienceUserInterest:** type error access ([662fc81](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/662fc81538240544ca078b0f73cb2b8e4711a0ab))
* **experienceUserInterestHistory:** omit info user ([c369757](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c36975710f4d39f74ace126ee7e520716fe4ed9d))
* **external_entities:** omit info user and remove logger ([9ae8cab](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9ae8cabce6faaae23982a9f516baac5b687b80e7))
* **general_complain:** omit info user ([96ebb87](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/96ebb8759a46de2177f36bd497d8471775df5441))
* **monthly-reports:** add validation acess ([2515f94](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2515f948a6ba3b1e9bec6af370ffdfc0e07e631b))
* **monthly-reports:** return in get ([d544d99](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d544d993e82369d2ac1820e4068d2086db949124))
* **monthly-reports:** type error access ([6b2322a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6b2322aafdc6722b1f3c5b5fce485d7173f41473))
* **package:** update core ([56b4a39](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/56b4a390d3cb4c3c0236219b93fa5536f514309b))

# [1.25.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.24.0...v1.25.0) (2021-07-01)


### Bug Fixes

* **applications:** rename scope ([a2d7e92](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a2d7e92d3241123f03b944c007d904c2293f4aff))
* **experiences:** rename scope ([c25824a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c25824a67f85ea354452cff5af56f747f8bfb79f))
* **experienceUserInterest:** rename scope ([d099f3e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d099f3edab118b0dddfa1e75994b8e90ac79b2dd))
* **experienceUserInterest:** rename scopes ([27b8e7e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/27b8e7e07255a8663e4269b83a4f1cbd9aeeae29))


### Features

* **applications:** add cancel ([e2423e0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e2423e07ddb6cc7c2b7b05aeb3e02400dac51911))

# [1.24.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.23.0...v1.24.0) (2021-06-30)


### Bug Fixes

* **experiences:** add new flow advisor selection ([b8833d5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b8833d50f7096e0e6cb07df792d582cef130d73a))
* **migrations:** update table experience ([7c0617f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7c0617f1cff0c67f7713e31c8519d6631e3da482))


### Features

* **migrations:** update enum last_status user_interest ([9014054](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/90140545a6b4a9f20b91d24028866365cfc0dfdb))

# [1.23.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.22.0...v1.23.0) (2021-06-29)


### Bug Fixes

* **applications:** remove logger ([4bccb7a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4bccb7a5b36ce71b67efcde4bd60b3545f3c966f))
* **experienceUserInterest:** update scope ([5de84ac](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5de84ac85abfcb2be0b1e8da3522a5b8f67d4520))


### Features

* **applications:** add logic for cancel application ([8a68109](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8a6810944341e0743cab4406062d7975d35319ff))
* **colaborations:** set colaboration status by condition ([538da7d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/538da7d284e62045a044474ed07eb0319d0d84d5))

# [1.22.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.7...v1.22.0) (2021-06-28)


### Bug Fixes

* **constantes:** update constants ([ce15486](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ce15486663925297667bf51e34433bb1691d1bb7))
* **experienceUserInterest:** add validation ([11ef11c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/11ef11cf65f179554d4d1b3d15fccb8117588723))
* **user_interest_machine:** update machine ([5b73af5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5b73af5564ca0430b72f5bedea5571055c98fc18))


### Features

* **migrations:** add new status user_interest ([3e6fc35](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3e6fc353398364bb854da7693acdaee6755c2a6f))

## [1.21.7](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.6...v1.21.7) (2021-06-28)


### Bug Fixes

* **activities:** update created_at validated_at ([826f705](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/826f7050a29b917b56fba4d192bb3f9be42ccacc))

## [1.21.6](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.5...v1.21.6) (2021-06-25)


### Bug Fixes

* **experienceuserInterest:** optimize withrelated ([2ed253a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2ed253aaf98e7a0a6cabee554bed9908430d24b9))

## [1.21.5](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.4...v1.21.5) (2021-06-25)


### Bug Fixes

* **applications:** optimize withrelateds ([40c9954](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/40c99545df2bcdb66eab4ffbd26ed4cf8be4d150))

## [1.21.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.3...v1.21.4) (2021-06-22)


### Bug Fixes

* **applications:** add patter academic year ([5959ca3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5959ca3da702704d3eee33c1a6809dab357da377))
* **experiences:** add patter academic year ([f55b12d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f55b12d4146da7ae45009a6ef36c37cbe0c7be2b))

## [1.21.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.2...v1.21.3) (2021-06-21)


### Bug Fixes

* **applications:** rename scope ([da3a403](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/da3a40317f0bfadf761a5d4984ea48e0428a86af))

## [1.21.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.1...v1.21.2) (2021-06-17)


### Bug Fixes

* **external_entities:** set active true ([aaa6c22](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aaa6c22da523c5eca9f07c6c842f159ac6835865))

## [1.21.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.21.0...v1.21.1) (2021-06-15)


### Bug Fixes

* **external_entities:** add update endpoint ([96ab644](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/96ab64484a070a4611bbcbd7fb9e75d17316f1f9))

# [1.21.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.20.1...v1.21.0) (2021-06-09)


### Bug Fixes

* **experiences:** add notification return status ([01fd488](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/01fd4887cdf8d658527d0f2992c5fb3cba9f8ef1))
* **external_entities:** add new fields and status ([0f80052](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0f8005297ace2782ecdd1b138c07605007241b6f))
* **external_entity:** add validations for active/inactive ([d5d5d8c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d5d5d8c50ab7b83c83f5908d6905af1083a8ffdd))


### Features

* **migrations:** add new fields table external_entity ([f1493ff](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f1493ff6dc663d934f24e8fed6c61fa9f6d48859))

## [1.20.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.20.0...v1.20.1) (2021-06-07)


### Bug Fixes

* **payment_grid:** fix scopes ([206378f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/206378f19fa1d44069b9c958a6b8f2cc41ac24d8))

# [1.20.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.19.1...v1.20.0) (2021-05-31)


### Bug Fixes

* **attendances:** lint problems fix ([fa6173f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fa6173fe77521d163de794cca330efc03c65afe0))
* **general_complain:** lint problems fix ([28bb4e9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/28bb4e99eec32c6161293f73d64c64e281732a4a))
* **payment_grid:** add new status ([0798a6c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0798a6c56c000257bf9724d58e63a0fa73f418ca))
* **payment_grid:** lint problems fix ([ff68cd7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ff68cd74ed8392424f8c38ab0ce37fa54c66948b))


### Features

* **external_entities:** add SearchRelation ([2919de6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2919de6a3113fb76c58c31ce9a9ee7bdf66bbf1c))
* **migrations:** add payment_grid_model ([bb0f3e2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bb0f3e2ddb25b60e26c75767c8de39b4efe759a3))
* **migrations:** add payment_grid_status ([a885410](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a885410f42924480176306c62e0713ad9e0364b2))
* **utils:** add payment_grid_mode, payment_grid_status ([94ce862](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/94ce862514fc8ab7ba9ca9c1c012073a04ed6844))

## [1.19.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.19.0...v1.19.1) (2021-05-26)


### Bug Fixes

* **monthy_reports:** update scope ([f3d3290](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f3d32907c71781f7ec82e13c08c850b78ba9d603))

# [1.19.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.18.0...v1.19.0) (2021-05-25)


### Bug Fixes

* **attendances:** calculate hours attendances ([c5d8e8d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c5d8e8d968a3884eaa826d36da2466bb33d876cb))
* **experiencesUserInterest:** update list colaborations ([a2f88f9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a2f88f9a8fb086eda200c27d17cb49bbc8f9fe5f))
* **experienceUserInterest:** update certificate_generated_id ([070e454](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/070e454aa72fac3a24694c4f4908f4178456f239))
* **experienceUserInterestInterview:** add relation file ([368aec7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/368aec755ea33e74933cfbd64219c40fea26710f))
* **general-reports:** calculate hours ([4c8f3d4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4c8f3d42ed4bd755ddc39c801e61028e3a11b24b))
* **migration:** update certificate_id ([f57c3d1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f57c3d1c683b0c58d93f55269fabac2696b1a980))
* **monthly-reports:** calculate hours, and add endpoint alerts ([e8b01b9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e8b01b946814debc94ec55e5aa98e5a24c88a6b8))
* **payment-grid:** caculated hours ([df739d7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/df739d7ab9843d741bc78afb3995db10982f1456))


### Features

* **experienceUserInterest:** add certificate generate ([bb4c467](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bb4c467072af27e5a9af62aea194c2e934b56496))
* **external_entities:** add active entitie ([29f6861](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/29f6861f8fd172c836b769437417c97b6f3349ee))
* **migration:** add certificate fields generated ([b3c481c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b3c481c181fb84fe8d116282fa9a3c6e824374b7))

# [1.18.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.17.0...v1.18.0) (2021-05-17)


### Bug Fixes

* **experiencesUser:** unique endpoint user interests ([a40f522](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a40f5227f39598cb56c2896efba3f75397037ffa))
* **payment-grid:** update generate template ([5435156](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/54351568857eb1c45573eb9549ea02ecdd462499))


### Features

* **experiences:** add call generate report and send iportaldoc ([dcf8e2d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/dcf8e2d96d8295a5b2f0ff597527cdfc1d3009dd))

# [1.17.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.16.0...v1.17.0) (2021-05-10)


### Features

* **payment_grid:** add filter in list and iban ([aa73312](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aa73312f8646656bd63e6e7ec04345100a5af1ec))

# [1.16.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.15.1...v1.16.0) (2021-05-06)


### Bug Fixes

* **applications:** add limit 3 wp ([4a57120](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4a57120d86bf54505f11f7578ffcc79f949666c6))


### Features

* **experiences:** add limit 3 ([c4797b8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c4797b875a54e391ae99bd406b18c6a352cf3f78))

## [1.15.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.15.0...v1.15.1) (2021-05-05)


### Bug Fixes

* **applications:** dashboard limit status ([c7887e5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c7887e5de02ebec8d02e234f97e846e8365be0e6))
* **experiences:** dashboard status limit ([373288e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/373288e5929600754cca7665d48aba5624ffb579))

# [1.15.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.14.0...v1.15.0) (2021-05-05)


### Bug Fixes

* **applications:** remove status from endpoint ([54a91cd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/54a91cdf3d078c49c02977aa038df7afd15ad4ef))
* **applications:** update field and endpoint dashboard ([8841291](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8841291b9433ffda4bf8cc91ed2cee8c2bbce6e4))
* **experiences:** add return reason, dashbord endpoint ([40504c0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/40504c09a7f895d1a34cdad7de5935770efd4003))
* **experiencesuserinterest:** omit info ([e9e6f0d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e9e6f0ddf13578747ae11b6c9b1f84de05ed063a))


### Features

* **migrations:** add experience return_reason ([dcbd155](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/dcbd1554ffca901fcdabdb95ef1b2fa4e2bdd8bc))

# [1.14.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.13.0...v1.14.0) (2021-05-04)


### Bug Fixes

* **experiences:** fix validations and remove comments ([a37773a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a37773a8f7285cd9ca5dc6a04aa3d16af205df99))


### Features

* **applications:** add cron job close applications ([5a7173c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5a7173c896d5708a594f70820a31949981fea7c1))
* **configurations:** set and get tipe of expire applications without attendances ([53fc3a5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/53fc3a52278ceec100fb676f0973d86b74c17102))
* **experiences:** add published and update wp_dashboard ([a93173a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a93173a29b7ca684aadb032d361bb77fdc5c1ce3))
* **migrations:** published flag ([b2903fd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b2903fdf7c7a9bcc4c35c1b65e3e17ade54ef266))
* **seeds:** add seed time ([8d139c6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8d139c61ada089295b9de6abc8303f6ff92df2dd))

# [1.13.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.12.0...v1.13.0) (2021-05-03)


### Bug Fixes

* **applications:** alter integers to string ([69432e4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/69432e4e412cf9b1cf2dfdbdcad2feb083256478))
* **constants:** constant name ([7e1808d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7e1808d23034984965b567af5397f0147186503a))
* **experienceuserinterests:** update view info permition ([10909f5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/10909f55eca8e6d4f4cb49d16a69f48fd07b810f))
* **machine-experiences:** function name ([a80f928](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a80f928eda45b9fee8529f83a466f7f168d7dc2b))
* **monthly-reports:** endpoints and reports ([25a5e91](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/25a5e910a4eebdcaaf2a6ed0eca1f0a109068f01))
* **payment_grid:** return and report name ([7ef7bf7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7ef7bf7bcf521f79a38468b4c27e74e52eb8246b))


### Features

* **external_entities:** create and inactive external entities ([ded3216](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ded321637d0073d2b9584d419ad2cea4ba8d44e7))
* **general_reports:** add hours validated ([e98398d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e98398d2b3d7a053e3dffa6ae2de333dca2c2894))
* **migrations:** application table update ([1c2855c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/1c2855c75b29396d6e32899335a4f4279b29f4c4))

# [1.12.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.11.0...v1.12.0) (2021-04-28)


### Bug Fixes

* **experienceuserinterest:** add visible ([2420baa](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2420baa6ee70b77a23874c85014682c63561ee38))


### Features

* **appication:** add visible ([01ae973](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/01ae97309b7b4dcf08f56ed58d16c168804a363d))

# [1.11.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.10.0...v1.11.0) (2021-04-28)


### Bug Fixes

* **applications:** update change status and schedule interview ([e1de2b2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e1de2b22cca1ec016ffcf4f76c0e934b0f8ccdd9))
* **experience_machine:** fix name ([083b65e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/083b65e8c5531945db227a9b2067e75764c94151))
* **experienceuserinterest:** optimaze endpoint ([df11e83](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/df11e833762f9ae4cef8e7115662b952e3c6c04b))


### Features

* **application:** endpoint for add report interview ([4c20251](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4c2025129bef7baca04daeef9866e437b53c2846))
* **application:** optimaze endpoint ([b4d8cb0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b4d8cb02f1bc039c01d34ae3b1b73bce89d9851f))
* **experienceuserinterest:** endpoint for add report of interview ([d32cf5d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d32cf5d666240018f9bb0097f8fcb021cfde5371))

# [1.10.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.9.0...v1.10.0) (2021-04-27)


### Bug Fixes

* **complain_application:** fix problem with user ([e1b6f01](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e1b6f01021f6d1307605eecf25052f9fb885527f))
* **complain_user_interest:** fix problem with user ([f34f3e5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f34f3e5dbbee71c375737d32c81aaada49ff00db))


### Features

* **complain_experience:** add service ([8457d2f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8457d2f27be5dd2f6c84ccff4eac70eb7a6e9c8b))
* **migration:** fix tables ([1176413](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/11764134a0994267a438ab133a73957f7e8ae2fb))

# [1.9.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.8.2...v1.9.0) (2021-04-26)


### Bug Fixes

* **machine_experience:** fix bug ([f4dde72](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f4dde7258c7d7870523b3c1ace9d50f5d33f9294))


### Features

* **applications:** add last status ([7367705](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/73677052cf478f3b2e02c67291bb8712b54d5767))
* **experience:** add last status change ([84a3970](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/84a3970c48df155b7389c7641125bd52c4a3c054))
* **experiences:** add statistics experiences ([cc99928](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cc99928b78d4d28901454a56eafd6ce310cc41a9))
* **experienceuserinterest:** add last status ([d5e3f74](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d5e3f74bc3686db0a663532e7dc8ae22b80485e1))
* **machine_application:** add last status ([b1f4929](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b1f492942a5df375fcc7828e286f81bf89c5fc17))
* **machine_experiences:** add last status ([97c7372](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/97c737243c271a23a4fb0901545a05aca0bbff86))
* **machine_user_interest:** add last status ([b9745a3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b9745a388aa8e44bba7d3d76e6845ee2291b432e))
* **migration:** add last status ([a87fa1e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a87fa1eb9993e2418ae3338c5eeacf8f8e8c223d))
* **user_interest:** add statistics experienceuserinterest ([5832cb2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5832cb268ea7458459b73d7b3b82ec5d2bce3170))

## [1.8.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.8.1...v1.8.2) (2021-04-22)


### Bug Fixes

* **expericenuserinterets:** remove filter status ([077cd7b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/077cd7bcc90052ac0d113a9765018ddcea4b29cd))

## [1.8.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.8.0...v1.8.1) (2021-04-22)


### Bug Fixes

* **expericeUserInterest:** null object error ([3f3ac4d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3f3ac4d77cefc7bb493dd2b05c105157d2ea91e1))

# [1.8.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.7.0...v1.8.0) (2021-04-21)


### Bug Fixes

* **application-interview:** add observations and scope ([95abebf](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/95abebfdb5d5a5384155a88cba085e00aeeb55fb))
* **applications:** add new info of interview ([62034d2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/62034d2585c4c6271544119d58b5c185076d751d))
* **applications:** remove info for accepted status ([370c9f9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/370c9f902884528f4533851f51b26fe0b7ad8ed6))
* **attendances:** add internal validator ([cb07b71](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cb07b715b1e3879a89b314ff7b6de04fd7a038e2))
* **attendances:** fix scope ([8c486ec](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8c486ec2e840cbce26058ebb1bfa885bfd02d924))
* **attendances:** update filter ([4f26ef4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4f26ef4b1703df2e0fa35d8e695f8f1f4a61b606))
* **complain_application:** add response and scope ([9c626ed](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9c626ed7d94886885a47bed88bf2758778d8f242))
* **complain_user_interest:** add response and scope ([ee3a083](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ee3a083a2896cfc5a852c7585f0c723da2732f7f))
* **complain_user_interests:** add scopes ([838bb0c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/838bb0ceebab16a6b4ceb2e522f9e6f8e17a753d))
* **configurations:** add financial email ([e13c8c8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e13c8c8cfbaac706e54039ae28a63018451e68b4))
* **constants:** update constantes payment_grid ([54f2086](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/54f2086ba8a8c62a561035f25dd89c4253aaeb21))
* **experence_interview:** add info observations and scope ([d38535f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d38535f53a2e4a5ce2e36a86fc920fa416c770d4))
* **experience_machine:** remove status published ([57ea423](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/57ea423b1f48f534cfb14300bc83cc3d0bda6122))
* **experiences:** add visibility ([4fc4e9a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4fc4e9add79f7aefbf502ee1722af77c3c8e2ee6))
* **experiences:** update templates, remove publish status, method for update status auto ([91eeec2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/91eeec2f0dcd440121a3cdda0bdcca05e7cd8700))
* **experienceuserinterest:** remove required contract file ([06f2686](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/06f26867cac2eb364f7a7e279c50a159a79f8bf4))
* **experienceUserInterestsInterview:** fix error ([fc4e64d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fc4e64dde4448b4ab5fface6838f32e5f644b0b4))
* **general_complain:** add scopes ([f692c28](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f692c280b5d0cb66dca71be53559000684b3c1b1))
* **gerenal_complain:** add response and scope ([153d24e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/153d24ecc847e96e7e11a776e298dc764297aee7))
* **migration:** duplicate altertable ([31b5bdd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/31b5bdd931512aace3841fc9002f824c74953755))
* **monthly-reports:** add scopes ([57fcc5d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/57fcc5d808fc499d389fbabc5e464bcf5e6d8e2b))
* **monthly-reports:** format dates ([e892599](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e892599c28c66e1af9e8a88b329a7e41967a84c5))
* **monthly-reports:** update attendances info ([9bb4344](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9bb43446b793cd2ec516675ac0724f05d2d6f2df))
* **payment-grid:** message error ([6383c6d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6383c6d31a895b90af13e2fc8c89061b16dc04d9))
* **user_interest:** add info interview observations and scope ([0075edb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0075edb824dc516c0155b259aaa3742555b9eea6))


### Features

* **applicationHistory:** add user relation ([bcb13cd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bcb13cdf2a8d81c022b60c780d814c7495de89ff))
* **attendances:** add status for accept or reject attendances ([1a6e97e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/1a6e97e0971625e852f94e80498f99506c957814))
* **complain_applications:** add scope ([076bb07](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/076bb076d2522d9867493b020d7f5c29d6da1e3c))
* **experiences:** add user in colaboration ([f0e2c38](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f0e2c38f9e3c86dd55d025a1b1b7cf3410a7bd0c))
* **experienceuserinteresthistory:** add user relation ([fbed44a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fbed44a71defed6ed408bff1fda909a6ec40ea64))
* **experincesUsersInterests:** remove info because RGPD ([d7c1ee9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d7c1ee977ee7a0322078270c6c1546e17592d6c6))
* **external_entities:** add relation ([3a4be59](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3a4be599bdb2bd944fca8545419bc49962d4e848))
* **general-reports:** update attendances by status ([e82d10a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e82d10a83462a3928098b733a33aa2ce46b07ab9))
* **migrations:** add altertables and create tables ([ca855b6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ca855b60df1f30eb8116192627af2857a50e597e))
* **monthly-reports:** update attendances status ([fceaf8e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/fceaf8e039d9414240ae6733fa713b2d8c43e7b1))
* **payment_grid:** add flow payment ([d48b6f6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d48b6f62811946db1b24c2758453a8ddcc6ba445))
* **payment_hours_transitions:** add table and service ([51c08eb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/51c08ebf582fbebf262b1399c24c280f7b3489c0))

# [1.7.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.6.0...v1.7.0) (2021-04-13)


### Features

* **experienceUserInterest:** add relation applications ([73a4652](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/73a46525860b4b71bc85529c5c4f5befd0f952b3))

# [1.6.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.5.0...v1.6.0) (2021-04-09)


### Features

* **absence_reason:** add hours validations ([7aed3cd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7aed3cddfbba07dbfe3f0e5db114ab07b09e0509))
* **attendances:** calculate hours validation ([3a1ce25](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3a1ce25747e2ea463b56e9160afdd72be4ec5471))

# [1.5.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.6...v1.5.0) (2021-04-07)


### Bug Fixes

* **applications:** update relations ([c31c22f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c31c22f786d557385832a07c33e3a08a75b1b462))
* **attendances:** small fix ([68d2252](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/68d2252494d978598bb6c6bfc0dd6f9f541acd45))
* **experienceUserInterest:** fix withdrawal endpoint ([6bfaa1e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6bfaa1e8f6ebe79e5d3d899fd51226be6c249bd2))
* **external_entities:** access Bo is false ([be5c0a4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/be5c0a4112c455566e35e72ac5e791d15ee4ccd3))
* **external_entities:** fix create endpoint ([41f8995](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/41f8995642f87d983b832eb3ec742f2a99e1007d))
* **external_entities:** update external entity ([94713b2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/94713b22874f6eef8e3e9240b02b3544f51e70d6))
* **monthly_reports:** add reports hours month ([736a81d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/736a81d55a45b19f375df8e77c6d4ede2ae4f645))


### Features

* **applications:** add relations historic application/colaborations ([c8e84df](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c8e84dfbd2871bc156c00ab3a8ed9238ab625aa4))
* **experiences:** add relation external_entity ([c90a6e9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c90a6e931bd59b774e15e9a6693df21b990e584a))
* **experienceUserInterest:** add endpoint for withdrawal manifest ([e107666](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e107666f004de4a4d364732e007d03f9c7e55632))
* **experienceUserInterest:** add relation historic applications/colaborations ([bcf2d4c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bcf2d4c23b7cb315247794596487fbd1e88ae012))
* **migration:** add migration external_entity ([52b821f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/52b821f8663d9651c56d6279998a4d4e2a510fab))
* **payment_grid:** report with payment_grid ([cfe4460](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cfe4460de4ed63cf132852fdb9f8774414663b93))

## [1.4.6](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.5...v1.4.6) (2021-03-30)


### Bug Fixes

* **monthly-reports:** add default withrelated ([cde27e4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cde27e4c5b4ab97fd5e990d4c7d35f0e34bdc269))
* **monthly-reports:** hasone not defined ([be81212](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/be812125535c12e39d7d17c9d6ee33e2e670a14f))

## [1.4.5](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.4...v1.4.5) (2021-03-29)


### Bug Fixes

* **absence_reason:** entity validator and withrelated ([9594a4b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9594a4bb030b87a0b135519bb3f1801eefcf5859))

## [1.4.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.3...v1.4.4) (2021-03-26)


### Bug Fixes

* **applications:** withrelated notifications ([67a2a6a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/67a2a6aba16b4da9870847835575dbbc7f052542))
* **experiences:** bug ([aa043a2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aa043a26d1f5b54fe657af77034499b338f524b7))
* **experienceUserInterest:** update endpoint ([5d7b0da](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d7b0daf346b824d392b6144fd4ea15c6c772140))

## [1.4.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.2...v1.4.3) (2021-03-26)


### Bug Fixes

* **core:** update ms core ([e7e48a4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e7e48a49a6257ed8900ca48fa7b8aa78caba3afd))

## [1.4.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.1...v1.4.2) (2021-03-26)


### Bug Fixes

* **experienceUserInterest:** fix endpoint ([3c2711d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3c2711d065b97615ae1d944c3e463f470f6885f2))
* **migration:** migration application ([a8da073](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a8da07366dd470272c3e281bd7d88e96ce3d1733))

## [1.4.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.4.0...v1.4.1) (2021-03-25)


### Bug Fixes

* **application:** cache problem ([0a72995](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0a729955d602d1d303c2b9804812076293704c6f))
* **experiences:** problem cache ([691b904](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/691b904a7cd6b364c68b727edcee269566f4bfe3))
* **experienceUserInterest:** problem cache ([c7a5ea5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c7a5ea58f8d0ef6a7d784e086f26ae30f8ae170e))

# [1.4.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.3.0...v1.4.0) (2021-03-25)


### Bug Fixes

* **application:** add visibility ([d918757](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d918757139a78079c1970b90aa1f8323c3a921ee))


### Features

* **general-reports:** add scopes ([7fb9d22](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7fb9d22d3dc2dba9b1c5e4652d5f271a155f8ff2))

# [1.3.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.2.1...v1.3.0) (2021-03-25)


### Bug Fixes

* **absence_reason:** add scope ([f3029a3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f3029a37dc74936a2b96ab4a698908ab71724989))
* **application:** add info of user ([61a8367](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/61a8367d11c505b4e04901a633659c9a505c4078))
* **application:** add scope and fix withrelated ([6423c98](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6423c98cd02e05626fa27a295872e3c8da6b829c))
* **application_organic_units:** add scopes ([720156a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/720156a1599d63544fd873a65d12c1d015be4917))
* **application_schedule:** add scope ([c2f8430](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c2f843003de09782981d3818bf59c018902b6ecc))
* **applications:** update endpoint applications actives ([a13a5e1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a13a5e1237097495b83e860eb8cd9a045707f678))
* **configurations:** add scope ([27be6f4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/27be6f40503c39c33586000d3a87a877fce72ce5))
* **constants:** name of return ([f23ccad](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/f23ccad81c3bc9b6d217452c0463b1a5fa2272c8))
* **experience_machine:** update return name ([78274a6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/78274a69a2de36b3dc22b9b172005de3c71c1615))
* **experienceUserInterest:** add scope ([4c7da60](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/4c7da6031bcbe03d5c9ec58d7d13ccb75539e191))
* **experienceUserInterest:** add withrelated and fix bug ([2c99a99](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2c99a993e9502d17d82c89b7bf4d324390d9bb85))
* **general-reports:** update endpoints ([50c6cfb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/50c6cfb5aa83ffa9199846a21b0e81a992265457))
* **monthly-reports:** add scope ([3e7d1eb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3e7d1ebb822226a626abdd70fde0cd7769746fc6))
* **payment_grid:** add scope ([926d83a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/926d83aae3cdd401cc44820d15c3674c3f652738))


### Features

* **experiences:** add reports ([71f21a3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/71f21a3c2b591066a391ed7ac8a39bc1404d3429))

## [1.2.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.2.0...v1.2.1) (2021-03-22)


### Bug Fixes

* **migration:** update file ([cd0d9ef](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cd0d9eff250d8c64db650d6fdcbda289fe7e4714))

# [1.2.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.1.1...v1.2.0) (2021-03-22)


### Bug Fixes

* **absence_reason:** update id ([ecc964b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ecc964babae214bcf2c869147dde6e247dcc0577))
* **application:** add field max_hours_colaboration ([24e1e66](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/24e1e66f0a0d3af2c5daeeb84e36ba8165fa32e9))
* **application:** update state machine ([0d71dff](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0d71dff2f6ec2e8add6b6afa8858dd30e69812f9))
* **applications:** filter by user ([e51a911](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e51a9113d49c131d74733d1deccccb82285b9c6e))
* **applications:** update applications ([36d6754](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/36d67547979bb6deb7a0d46542d8d137dab0a9d7))
* **applications:** update withrelateds ([941df6c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/941df6c484f2f464942a891855fb03b3680defc8))
* **attendances:** update service, rename table ([7c8f0bb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7c8f0bb5709f9101eda7a101d8b37e147d927099))
* **attendances:** update withrelateds ([188e3e7](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/188e3e743ec7e5d6022f7aa330b2f97969de2800))
* **experciences:** add important endpoints ([1c7beaa](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/1c7beaa2d852055970467a794904348ed28b2208))
* **experience:** update withrelateds ([c5acfbc](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c5acfbca838b77a26a032afc2983b5857fa0005f))
* **experiences:** update state machine ([35d6648](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/35d66484583cedd01bfd2c36afb70b813fd83ead))
* **experiences:** update withrelateds ([bdb212a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bdb212a0e948eb6e383e8aac927af059b687afb4))
* **experiences:** update withrelateds ([b975f91](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b975f911d84d32348170c55f9a3b9eba91c6b79f))
* **experienceUserInterest:** fix flow user manifest interest ([aa407f9](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aa407f9e412988b0b714b741c6d7c502288307bd))
* **migrations:** update migrations ([a1ca0c6](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a1ca0c6724f5096d72d0cab0299c8d7ca8c37d4d))
* **migrations:** update tables ([8ebb614](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8ebb614f680839e9ea4777e6c32a38aefedfafb3))
* **monthly-reports:** remove generate file ([cd2cdf4](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/cd2cdf4c49414d44f7b3f0e279d7756f5fc3a5c1))
* **monthly-reports:** update fields and services ([7c27e8c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7c27e8c0d0c7d537f819aa27fe9dac62addd8faa))
* **payment_grid:** update payment-grid ([a09145d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a09145d0828a895c95fff692d1dcd1334f44305c))
* **payment-grid:** remove spaces ([8f985f1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8f985f19cbd9b376786fd606e6b4c4fbd6083682))
* **user_interest:** update and add enpoints ([2e4751f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2e4751f39faaaf76eb5c2db970dd7688a8a9a12a))
* **user-consent:** remove user consent ([093156c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/093156c9f4e21f1a5e58e0091b88137468120dda))


### Features

* **complain-user-interest:** form for complain user-interest ([a540eec](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a540eec7253c3d35121f611d2b4c7fe37b9a8372))
* **configurations:** add max-hours-work ([ca458cb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ca458cb4647f0ca4a745707a1c7f12043bbff2bb))
* **migrations:** add migrations files ([818434f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/818434f9a3c069d6de35f0e1a8e00dbb4d6dc4e7))

## [1.1.1](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.1.0...v1.1.1) (2021-03-10)


### Bug Fixes

* **applications:** add missing withrelated ([60197ab](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/60197ab9d7ec8b1d4c9bb82dfe50b26ad86bbe89))

# [1.1.0](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0...v1.1.0) (2021-03-09)


### Bug Fixes

* **applications:** fix wp widget endpoint  for logged users ([45ca2e0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/45ca2e026ec1b334155a49139bd9c85179f2a7af))
* **experiences:** fix endpoint for wp widget information for not logged users ([a01f45c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a01f45c0dbfb40eb099b26b99fa1e2643e8be173))


### Features

* **applications:** add webpage widget endpoint ([5111b4c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5111b4c020b2c9f4e7c6da504fcaecd3760c26e7))
* **experiences:** add webpage widget endpoint ([2b146d3](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2b146d3524d6403788b2d89ce4d2dee90d429e31))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **applicaiton:** minor fix ([3656f27](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3656f272015dfd5f0195922b733d8eadbe165214))
* **application:** fix application experience withRelated ([6fcb3cc](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6fcb3cc6f92def1b6ae32f256039b9c34c44121c))
* **applications:** applicant_accept path ([d0b5bdb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d0b5bdbacc4eccb213b1580797c5cd5c63c7b768))
* **applications:** fix notification user ([5d5ce45](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d5ce4515d710a621f127d41d5c4369e5bb63f85))
* **applications:** fix organic-units creation on with-related ([331d0c5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/331d0c548710eed07156fec48dc67226c9cb04d4))
* **applications:** set user default withrelated ([156fabe](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/156fabeb0d986f6bfe19707953ed532ab7f1c3da))
* **attendances:** remove initial and final time required ([28c7823](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/28c7823db1793c0ee0705b298487ac5dc7e864ec))
* **attendances:** remove required on executed_service ([5d3fbdb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d3fbdb94b1fc2c419bf9e915ad0184dc7b4bf81))
* **attendances:** resove cache problems ([b10edb0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b10edb00b4a9c6a58978b51b2b67bde40fb908fd))
* **attendence:** remove cache ([26f50da](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/26f50daf963c7bbe4584d0a403524062476faee1))
* **attendences:** fix absence_reason with related ([a8c8f35](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a8c8f35dd79a71baef217d52a77ea9db7c247b7c))
* **attendences:** fix n_hours calculation ([7f9fb86](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7f9fb8611ca1cadf4e0d4e2c4483df10b367ee07))
* **attendences:** missing absence_reason related ([e6fc2d2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e6fc2d26ef5e385103808af380f6f4b16f8f9233))
* **build:** fix package not found error ([8a83421](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8a834210fa2509512200341985ea9e3e5d60a6ac))
* **experience:** add default status on create ([8d4c296](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8d4c29696ab52942e8eba95f6ba92f94c61ae2dd))
* **experience:** remove required current_account_id ([c6de9a8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c6de9a82fa2564fd78f5e9b61cfaa598db2d6e5a))
* **experiences:** add stringify on patch ([5cd63c0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5cd63c0959e9ac2cdf0bae0921bfeda8d4beb03e))
* **experiences:** experience history withrelated ([9f62dcf](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9f62dcf92b1445de3baaa1d8e770b368a40be5de))
* **experiences:** fix application withrelated ([0d9e6e8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0d9e6e872f13e717a15d8839d91b881084a16e7e))
* **experiences:** remove cache on language withRelated ([19be483](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/19be4832d7c411b588f5364fe7a26891faf6f2af))
* **general:** minor fixs ([8699a10](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8699a1034c0c92260486fa8e2b948fb369601d75))
* **general-reports:** fixings errors ([2d42e56](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2d42e563917e5f5159d47c6c60c6e2de33e19530))
* **migration:** enums ([d1c72c8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d1c72c8cdd9e1b9783dc8aafcf182e3960f55656))
* **monthly-reports:** add dates to generated report ([2ec7f83](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2ec7f838e1b60304a5526914c392c20bfab226fa))
* **monthly-reports:** add result file name to reports request ([abfa08c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/abfa08c4ff16f89650e617df77e6f373cd6d3ab6))
* **seeds:** remove data for dropped fields ([2df1c4c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2df1c4ca461691023aa62b2d677c26314dd7e3fb))
* application status enum ([aafcd91](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aafcd9173ce00750a6e99b53591ae76ac27bb891))
* fix the configurations scope ([5cf087a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5cf087af028650c32ed3577b82341eb38a22a008))
* minor fix ([9e7d34c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9e7d34cc99ba8cfcdfc8bd8f3ce5f01f4f0bcb06))
* **validation:** enums ([bfa4fec](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bfa4fec6fd93bcc617ed3b6c02ba7d3a1ad533c0))


### Features

* **applications:** add application state machine ([7fa366f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7fa366f66ac29936c01030fc98a8add3489ae909))
* **applications:** add get certificate request ([b824b20](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b824b206fb1e2b5bf91b204e0fae884ad6c2b578))
* **applications:** add withRelateds ([1b46f64](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/1b46f6491f50a677e1c567cb5fcd1fb54d915a6d))
* **applications:** save application preferred activities and schedule ([b987b8e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b987b8e68573a7aa3c37ec22eee3e99d9a81bce2))
* **attendances:** create action for attendaces between dates ([436826d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/436826dd9404ac0002c3627e35c9cc4d032c6ad8))
* **attendences:** add initial hours,final hoursand executed service fields ([be97092](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/be9709239fc047b1bd7f8ccb9546a7885b36acf8))
* **experiences:** add endpoint to wp widget ([54ed704](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/54ed704f41e7de3670a7aa24bca4ef59c083aeef))
* **experiences:** add requirement number to experiences ([622f263](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/622f263e83e45eafdcd0ba3ecd82d88d5a927774))
* **experiences:** add state-machine ([8bd5b9b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8bd5b9b71936409e65fedeb357fc525d85e2a448))
* **general-reports:** add general report service ([90929fd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/90929fd104ce01038a1c2571d210204386dcd288))
* create external user from scholarship BO ([c886e09](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c886e09ca78c02fad642224472e14fbb2c712837))
* **package:** add moment in dependencies ([ad98321](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ad9832125323a90c4b9208f940faff091a98a3ae))
* add translations to experiences ([00acfe1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/00acfe12e13dfaff3c63831eb9480c808152eb35))

# [1.0.0-rc.31](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-03-02)


### Bug Fixes

* **applications:** fix notification user ([5d5ce45](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d5ce4515d710a621f127d41d5c4369e5bb63f85))


### Features

* **experiences:** add endpoint to wp widget ([54ed704](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/54ed704f41e7de3670a7aa24bca4ef59c083aeef))

# [1.0.0-rc.30](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-02-22)


### Bug Fixes

* **monthly-reports:** add dates to generated report ([2ec7f83](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2ec7f838e1b60304a5526914c392c20bfab226fa))
* **monthly-reports:** add result file name to reports request ([abfa08c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/abfa08c4ff16f89650e617df77e6f373cd6d3ab6))

# [1.0.0-rc.29](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-02-17)


### Bug Fixes

* **experiences:** remove cache on language withRelated ([19be483](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/19be4832d7c411b588f5364fe7a26891faf6f2af))

# [1.0.0-rc.28](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-02-11)


### Features

* **applications:** add get certificate request ([b824b20](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b824b206fb1e2b5bf91b204e0fae884ad6c2b578))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-02-10)


### Bug Fixes

* **attendances:** remove required on executed_service ([5d3fbdb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5d3fbdb94b1fc2c419bf9e915ad0184dc7b4bf81))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-02-09)


### Bug Fixes

* **attendances:** remove initial and final time required ([28c7823](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/28c7823db1793c0ee0705b298487ac5dc7e864ec))
* **experience:** remove required current_account_id ([c6de9a8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c6de9a82fa2564fd78f5e9b61cfaa598db2d6e5a))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-02-09)


### Bug Fixes

* **general-reports:** fixings errors ([2d42e56](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2d42e563917e5f5159d47c6c60c6e2de33e19530))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-02-08)


### Bug Fixes

* **applications:** fix organic-units creation on with-related ([331d0c5](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/331d0c548710eed07156fec48dc67226c9cb04d4))


### Features

* **general-reports:** add general report service ([90929fd](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/90929fd104ce01038a1c2571d210204386dcd288))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-02-04)


### Bug Fixes

* **attendences:** fix n_hours calculation ([7f9fb86](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7f9fb8611ca1cadf4e0d4e2c4483df10b367ee07))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-02-04)


### Features

* **attendences:** add initial hours,final hoursand executed service fields ([be97092](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/be9709239fc047b1bd7f8ccb9546a7885b36acf8))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-02-03)


### Bug Fixes

* **experiences:** experience history withrelated ([9f62dcf](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9f62dcf92b1445de3baaa1d8e770b368a40be5de))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-02-03)


### Features

* **experiences:** add requirement number to experiences ([622f263](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/622f263e83e45eafdcd0ba3ecd82d88d5a927774))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-02-03)


### Features

* **applications:** save application preferred activities and schedule ([b987b8e](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b987b8e68573a7aa3c37ec22eee3e99d9a81bce2))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-02-02)


### Features

* **experiences:** add state-machine ([8bd5b9b](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8bd5b9b71936409e65fedeb357fc525d85e2a448))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-02-02)


### Bug Fixes

* **applicaiton:** minor fix ([3656f27](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/3656f272015dfd5f0195922b733d8eadbe165214))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-02-02)


### Bug Fixes

* **build:** fix package not found error ([8a83421](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8a834210fa2509512200341985ea9e3e5d60a6ac))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-02-02)


### Features

* **applications:** add application state machine ([7fa366f](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/7fa366f66ac29936c01030fc98a8add3489ae909))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-02-02)


### Bug Fixes

* **attendances:** resove cache problems ([b10edb0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/b10edb00b4a9c6a58978b51b2b67bde40fb908fd))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-02)


### Bug Fixes

* **attendence:** remove cache ([26f50da](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/26f50daf963c7bbe4584d0a403524062476faee1))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-01)


### Bug Fixes

* **attendences:** fix absence_reason with related ([a8c8f35](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/a8c8f35dd79a71baef217d52a77ea9db7c247b7c))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-02-01)


### Bug Fixes

* **general:** minor fixs ([8699a10](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8699a1034c0c92260486fa8e2b948fb369601d75))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-02-01)


### Bug Fixes

* **attendences:** missing absence_reason related ([e6fc2d2](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/e6fc2d26ef5e385103808af380f6f4b16f8f9233))
* **experience:** add default status on create ([8d4c296](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/8d4c29696ab52942e8eba95f6ba92f94c61ae2dd))
* **experiences:** add stringify on patch ([5cd63c0](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5cd63c0959e9ac2cdf0bae0921bfeda8d4beb03e))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-29)


### Features

* create external user from scholarship BO ([c886e09](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/c886e09ca78c02fad642224472e14fbb2c712837))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-29)


### Bug Fixes

* **application:** fix application experience withRelated ([6fcb3cc](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/6fcb3cc6f92def1b6ae32f256039b9c34c44121c))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-28)


### Bug Fixes

* **applications:** applicant_accept path ([d0b5bdb](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d0b5bdbacc4eccb213b1580797c5cd5c63c7b768))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-28)


### Bug Fixes

* **applications:** set user default withrelated ([156fabe](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/156fabeb0d986f6bfe19707953ed532ab7f1c3da))
* **experiences:** fix application withrelated ([0d9e6e8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/0d9e6e872f13e717a15d8839d91b881084a16e7e))


### Features

* **applications:** add withRelateds ([1b46f64](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/1b46f6491f50a677e1c567cb5fcd1fb54d915a6d))
* **attendances:** create action for attendaces between dates ([436826d](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/436826dd9404ac0002c3627e35c9cc4d032c6ad8))
* **package:** add moment in dependencies ([ad98321](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/ad9832125323a90c4b9208f940faff091a98a3ae))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-28)


### Bug Fixes

* **seeds:** remove data for dropped fields ([2df1c4c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/2df1c4ca461691023aa62b2d677c26314dd7e3fb))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-01-28)


### Bug Fixes

* application status enum ([aafcd91](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/aafcd9173ce00750a6e99b53591ae76ac27bb891))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-01-19)


### Bug Fixes

* minor fix ([9e7d34c](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/9e7d34cc99ba8cfcdfc8bd8f3ce5f01f4f0bcb06))


### Features

* add translations to experiences ([00acfe1](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/00acfe12e13dfaff3c63831eb9480c808152eb35))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-social-scholarship/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-18)


### Bug Fixes

* fix the configurations scope ([5cf087a](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/5cf087af028650c32ed3577b82341eb38a22a008))

# 1.0.0-rc.1 (2021-01-18)


### Bug Fixes

* **migration:** enums ([d1c72c8](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/d1c72c8cdd9e1b9783dc8aafcf182e3960f55656))
* **validation:** enums ([bfa4fec](https://gitlab.com/fi-sas/fisas-social-scholarship/commit/bfa4fec6fd93bcc617ed3b6c02ba7d3a1ad533c0))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/social-support-scholarship/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-18)


### Bug Fixes

* **action:** rename namespace services ([86efc94](https://gitlab.com/fi-sas/social-support-scholarship/commit/86efc943c379bdc8694ffea9a57dd53de3d09b48))

# 1.0.0-rc.1 (2020-12-10)


### Bug Fixes

* **applications:** remove max in course year ([7e4b861](https://gitlab.com/fi-sas/social-support-scholarship/commit/7e4b8616202904b9d6107418e8e648b7842f47f9))
* **experience:** change hours to decimal number ([5732677](https://gitlab.com/fi-sas/social-support-scholarship/commit/5732677a845703f40e5f63b991cf52a463498f5c))
* **general-reports:** filter total hours per month of a experience ([0619e04](https://gitlab.com/fi-sas/social-support-scholarship/commit/0619e0486725f399d4c23b7905bcaf7cfa324048))
* adding missing application_id @ monthly-statistics endpoint ([79f597c](https://gitlab.com/fi-sas/social-support-scholarship/commit/79f597c528f998445adf67a89508ae9c786e673f))
* fixing wrong validations @ application schema ([affa7aa](https://gitlab.com/fi-sas/social-support-scholarship/commit/affa7aad72839489c65cfdc407a3329551ac16d1))
* loadRelated default limit to -1 ([4571502](https://gitlab.com/fi-sas/social-support-scholarship/commit/45715025797bb8ea55cb1e24f2f46d155970bd54))
* **application:** set condition for iban use only at bc ([fc42482](https://gitlab.com/fi-sas/social-support-scholarship/commit/fc42482909b3773f4a49331b4e7ee6f4e36478bc))
* **application-stats:** fix stats values ([bc94acc](https://gitlab.com/fi-sas/social-support-scholarship/commit/bc94acc6d4475a95b320d5b5dac330097539947b))
* **expericence:** change validations for volunteering ([b4a47d9](https://gitlab.com/fi-sas/social-support-scholarship/commit/b4a47d984d3d944d3e414ca866593d0c39bd1c26))
* **experience:** add changed status in state machine ([be46cff](https://gitlab.com/fi-sas/social-support-scholarship/commit/be46cff8da35d216646833f39cccc7d7b595b409))
* **experience-user-interest:** remove comment ([d302f22](https://gitlab.com/fi-sas/social-support-scholarship/commit/d302f220b90b97d4425bb9c72688c96dcbeea878))
* **experience-user-unterest:** add user data in response ([3d54910](https://gitlab.com/fi-sas/social-support-scholarship/commit/3d54910bd731de9acfdd9fd3c00086a24fd9bf0f))
* **experiences-status:** fix joi default values validation ([63e91f0](https://gitlab.com/fi-sas/social-support-scholarship/commit/63e91f0a52b2a2b78f600533b5f4b1940599068d))
* added request parameters searchFields ([34c9afd](https://gitlab.com/fi-sas/social-support-scholarship/commit/34c9afd9cda4afdfe1187c55c1d0802f921ce442))
* added support to 'preferred schedule' field ([df0938d](https://gitlab.com/fi-sas/social-support-scholarship/commit/df0938dc9482849ebf45f0c0808fe6dfd892f2f9))
* adding course and school @ application ([6790689](https://gitlab.com/fi-sas/social-support-scholarship/commit/679068916e4955aa92584d50b8e7bb3b7d4f3853))
* adding custom filter 'published'  @ experiences ([3ce32d0](https://gitlab.com/fi-sas/social-support-scholarship/commit/3ce32d08d3a931f6cbb4ea684f5f5da8afa9fe82))
* adding missing fields @ experience and application ([1c2182c](https://gitlab.com/fi-sas/social-support-scholarship/commit/1c2182c107e1d79241d376ca1d5e1243e2511024))
* adding organic unit external relation @ experience ([ed763bb](https://gitlab.com/fi-sas/social-support-scholarship/commit/ed763bb900e61a9c7dadfca22194056c575280ab))
* adding organic_units @ applications ([7bbc0f8](https://gitlab.com/fi-sas/social-support-scholarship/commit/7bbc0f8ad99e611c72bec1f3084559e5616c3ba5))
* adding user_id behaviour for GET and POST @ applications ([a1bc669](https://gitlab.com/fi-sas/social-support-scholarship/commit/a1bc669608181fddd31538f23854286f758ee361))
* addint 'users_interest' withRelation @ experience ([7d9d17c](https://gitlab.com/fi-sas/social-support-scholarship/commit/7d9d17cbb3565eb523727d69ca2fec7fac1149e2))
* application state-machine ([368cb7c](https://gitlab.com/fi-sas/social-support-scholarship/commit/368cb7cad998d9abc172a81eafcc9f2a35878d4c))
* created absence-reason endpoints ([dce4854](https://gitlab.com/fi-sas/social-support-scholarship/commit/dce4854ea94e821cdda9a26d2583b5926eec24dd))
* destroyCascade @ ApplicationAttendance ([dff371f](https://gitlab.com/fi-sas/social-support-scholarship/commit/dff371fb1c45cf974c0262d8f82cea2031884c5d))
* fetch (GET) error with default relations w/ experiences ([982bc08](https://gitlab.com/fi-sas/social-support-scholarship/commit/982bc08c59d82e704efbe87c76b6a4b71f317d0c))
* minor code fix ([621bcf0](https://gitlab.com/fi-sas/social-support-scholarship/commit/621bcf096e0389333dab5096b93974b1fc277b7b))
* missing  external withRelations @ experience ([9172431](https://gitlab.com/fi-sas/social-support-scholarship/commit/9172431f272b657c197e660b8ca3b80106585707))
* refactoring attributes for experience and applications ([19cb13b](https://gitlab.com/fi-sas/social-support-scholarship/commit/19cb13b00be41311a7f1021e4cf613fa9090e5c5))
* validations @ experience ([a568e62](https://gitlab.com/fi-sas/social-support-scholarship/commit/a568e62222e59db9732d1783be0f7ff47f52e7ec))
* **application:** add observations and cv file ([93c1464](https://gitlab.com/fi-sas/social-support-scholarship/commit/93c14648979ddcff2cf356b1d7fc586334edef2c))
* **application:** allow volunteer application ([0de9c70](https://gitlab.com/fi-sas/social-support-scholarship/commit/0de9c7054e7ca488372f67a3aa9188c7b5f8ec60))
* **applications:** fix fields in application ([7ee54f5](https://gitlab.com/fi-sas/social-support-scholarship/commit/7ee54f598a61c63d0b9cf6c08b6c50c56e4b2b2c))
* **experience:** add experience responsible,experience advisor and attachment file relateds ([4f07c32](https://gitlab.com/fi-sas/social-support-scholarship/commit/4f07c32f08fc4fbaca2721c33c62f015341ea9db))
* **experience:** add organic unit id ([59852ac](https://gitlab.com/fi-sas/social-support-scholarship/commit/59852aceb06fa11d04fd30f7edc5474275ad94ce))
* **experiences:** set status to opcional ([fec464f](https://gitlab.com/fi-sas/social-support-scholarship/commit/fec464fef4727c0b93d765c7ebc72c9718cf5412))
* **migrations:** add migration for organic unit id ([48d02f5](https://gitlab.com/fi-sas/social-support-scholarship/commit/48d02f5b602f22da0f36f043708211b869dc167c))
* **models:** fix withrelateds and sorts ([ddf4465](https://gitlab.com/fi-sas/social-support-scholarship/commit/ddf4465b723d6d29c886ca7c6b8b0e240f09036f))


### Features

* creating cronjob for publishing experiences ([c5e7843](https://gitlab.com/fi-sas/social-support-scholarship/commit/c5e784353b10f5b443d62b2e731ba44203982bf2))
* creating general-reports/user/{id}/monthly-statistics endpoint ([276b072](https://gitlab.com/fi-sas/social-support-scholarship/commit/276b072d99182ae8876c279df4f9927cd09f0c1a))
* **applications-stats:** total application by stats ([28422ce](https://gitlab.com/fi-sas/social-support-scholarship/commit/28422ce13cba8e9fc72ad023924c9e0ab6c55486))
* added experience user interest @ experience ([85e77d5](https://gitlab.com/fi-sas/social-support-scholarship/commit/85e77d570d21c3a5895ec68481b6737455157dd1))
* added general report for applicant user ([2fc460f](https://gitlab.com/fi-sas/social-support-scholarship/commit/2fc460fc4479783f338903ac137627fe39ff7ddb))
* Added generalReport for single Application ([6a1d319](https://gitlab.com/fi-sas/social-support-scholarship/commit/6a1d319982b2b037e9ca4aaae895c273507a4059))
* adding new state (applicant acknowledged) @ application ([d424d2c](https://gitlab.com/fi-sas/social-support-scholarship/commit/d424d2cecd2a6cc9e92bd56102ccdfe5ae0af9b0))
* adding on fetch behaviour @ application ([88a07c9](https://gitlab.com/fi-sas/social-support-scholarship/commit/88a07c9b637ba12958d7b2f0bf625f714650f8a1))
* adding organic_unit as default relation @ experience ([93ca598](https://gitlab.com/fi-sas/social-support-scholarship/commit/93ca59847b9665db81b26149a6a700ab1aea9dec))
* removed finance email field @ experience ([62de2a6](https://gitlab.com/fi-sas/social-support-scholarship/commit/62de2a643df51db779fff93656e8d68085ef6945))


### Performance Improvements

* **applications:** requests on external data more faster ([3a89042](https://gitlab.com/fi-sas/social-support-scholarship/commit/3a89042819827a633a797ec4a809358c63e1ef06))
* **experience:** requests on external data more faster ([97591e2](https://gitlab.com/fi-sas/social-support-scholarship/commit/97591e27345754c5692b0180a8b23ddf2630265a))
