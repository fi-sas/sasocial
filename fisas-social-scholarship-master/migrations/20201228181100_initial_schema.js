module.exports.up = async db =>
	db.schema.createTable("activity", (table) => {
		table.increments();
		table.string("name", 255).notNullable();
		table.boolean("active").notNullable();
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})

		// experience
		.createTable("experience", (table) => {
			table.increments();
			table.string("academic_year", 255).notNullable();
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //table.string('subject', 2).notNullable(); //table.enu('subject', ['BC', 'V']).notNullable();
			table.string("title", 255).notNullable();
			table.string("proponent_service", 255).notNullable();
			table.integer("organic_unit_id ").notNullable();
			table.string("address", 255).notNullable();
			table.string("applicant_profile", 255).notNullable();
			table.string("job", 255).notNullable();
			table.float("number_weekly_hours").notNullable();
			table.float("total_hours_estimation").notNullable();
			table.json("schedule").notNullable();

			//table.string('payment_model', 2).nullable(); //table.enu('payment_model', ['BA', 'VH']).notNullable();
			table.enu("payment_model", ["BC",
				"BA",
				"VH"
			], { useNative: true, existingType: false, enumName: "experience_payment_model" });

			table.float("payment_value").nullable();
			table.float("perc_student_iban").nullable();
			table.float("perc_student_ca").nullable();
			table.integer("current_account_id").nullable();
			table.integer("experience_responsible_id").notNullable();
			table.integer("experience_advisor_id").notNullable();
			table.string("description", 255).notNullable();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.datetime("publish_date").notNullable();
			table.datetime("application_deadline_date").notNullable();
			table.string("selection_criteria", 255).notNullable();
			table.integer("attachment_file_id");
			table.integer("contract_file_id");
			table.integer("certificate_file_id");

			//table.string('status', 255).notNullable();
			table.enu("status", ["SUBMITTED",
				"CONFIRMED",
				"CHANGED",
				"APPROVED",
				"PUBLISHED",
				"REJECTED",
				"CANCELLED",
				"CLOSED"
			], { useNative: true, existingType: false, enumName: "experience_status" }).notNullable();

			table.integer("number_candidates").nullable();
			table.integer("number_simultaneous_candidates").nullable();
			table.boolean("holydays_availability").notNullable().defaultTo(false);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		// application
		.createTable("application", (table) => {
			table.increments();
			table.string("academic_year", 255).notNullable();
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //table.string('subject', 2).notNullable(); //table.enu('subject', ['BC', 'V', 'M']).notNullable();
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //table.string('mentor_or_mentee', 6).notNullable(); //table.enu('mentor_or_mentee', ['MENTOR', 'MENTEE']).notNullable();
			table.string("course_year", 255).notNullable();
			table.integer("course_id ").nullable();
			table.integer("course_degree_id ").nullable();
			table.integer("school_id ").nullable();

			table.string("iban", 255);
			table.boolean("has_scholarship").notNullable();
			table.boolean("has_applied_scholarship").notNullable();
			table.boolean("is_waiting_scholarship_response").notNullable();
			table.float("scholarship_monthly_value").nullable();
			table.boolean("has_emergency_fund").notNullable();
			table.boolean("has_profissional_experience").notNullable();
			table.string("type_of_company", 255).nullable();
			table.string("job_at_company", 255).nullable();
			table.string("job_description", 255).nullable();
			table.boolean("has_collaborated_last_year").notNullable();
			table.string("previous_activity_description", 255).nullable();
			table.boolean("has_holydays_availability").notNullable();
			table.boolean("foreign_languages").notNullable();
			table.json("language_skills").nullable();
			table.integer("user_id").unsigned();
			table.text("observations ");
			table.integer("cv_file_id").unsigned();

			//table.string('status', 255).notNullable();
			table.enu("status", ["SUBMITTED",
				"WAITING",
				"ANALYSED",
				"INTERVIEWED",
				"ACCEPTED",
				"ACKNOWLEDGED",
				"PUBLISHED",
				"WITHDRAWAL",
				"DECLINED",
				"CANCELLED",
				"CLOSED"], { useNative: true, existingType: false, enumName: "application_status" }).notNullable();

			table.string("notification_guid", 36);
			table.unique("notification_guid");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			// Relationships
			table.integer("experience_id").unsigned().references("id").inTable("experience");
		})

		// application status history
		.createTable("experience_history", (table) => {
			table.increments();
			table.integer("experience_id").unsigned().references("id").inTable("experience");

			//table.string('status').notNullable();
			table.enu("status", ["SUBMITTED",
				"CONFIRMED",
				"CHANGED",
				"APPROVED",
				"PUBLISHED",
				"REJECTED",
				"CANCELLED",
				"CLOSED"], { useNative: true, existingType: true, enumName: "experience_status" }).notNullable();

			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// application status history
		.createTable("application_history", (table) => {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("application");

			//table.string('status').notNullable();
			table.enu("status", ["SUBMITTED",
				"WAITING",
				"ANALYSED",
				"INTERVIEWED",
				"ACCEPTED",
				"ACKNOWLEDGED",
				"PUBLISHED",
				"WITHDRAWAL",
				"DECLINED",
				"CANCELLED",
				"CLOSED"], { useNative: true, existingType: true, enumName: "application_status" }).notNullable();


			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// application_attendance
		.createTable("application_attendance", (table) => {
			table.increments();
			table.integer("application_id").notNullable().unsigned().references("id")
				.inTable("application");
			table.datetime("date").notNullable();
			table.boolean("was_present").notNullable();
			table.text("notes");
			table.float("n_hours").notNullable().defaultTo(0);
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// application_organic_unit
		.createTable("application_organic_unit", (table) => {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("application");
			table.integer("organic_unit_id").unsigned().notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
			table.unique(["application_id", "organic_unit_id"]);
		})

		// application_activity
		.createTable("application_activity", (table) => {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("application");
			table.integer("activity_id").unsigned().references("id").inTable("activity");
			table.unique(["application_id", "activity_id"]);
		})

		// application_monthly_report
		.createTable("application_monthly_report", (table) => {
			table.increments();
			table.integer("application_id").notNullable().unsigned().references("id")
				.inTable("application");
			table.integer("month").notNullable();
			table.integer("year").notNullable();
			table.text("report").notNullable();
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// payment_grid
		.createTable("payment_grid", (table) => {
			table.increments();
			table.integer("application_id").notNullable().unsigned().references("id")
				.inTable("application");
			table.integer("experience_id").notNullable().unsigned().references("id")
				.inTable("experience");
			table.integer("payment_month").notNullable();
			table.integer("payment_year").notNullable();
			table.enu("payment_model", ["BA", "VH"], { useNative: true, existingType: false, enumName: "payment_grid_model" }).notNullable(); //table.enu('payment_model', ['BA', 'VH']).notNullable(); 
			table.float("payment_value").notNullable();
			table.float("number_hours").notNullable();
			table.float("payment_value_iban").notNullable();
			table.float("payment_value_ca").notNullable();

			//table.string('status', 8).notNullable(); //table.enu('status', ['Pending', 'Approved', 'Rejected']).notNullable();
			table.enu("status", ["PENDING",
				"APPROVED",
				"REJECTED"], { useNative: true, existingType: false, enumName: "payment_grid_status" }).notNullable();

			table.text("notes");
			table.datetime("created_at").notNullable();
			table.datetime("updated_at").notNullable();
		})

		// configuration
		.createTable("configuration", (table) => {
			table.increments();
			table.string("key", 120).notNullable();
			table.string("value", 250).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("key");
		})

		// schedule
		.createTable("schedule", (table) => {
			table.increments();
			table.enu("day_week", ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"], { useNative: true, existingType: false, enumName: "schedule_day_week" }).notNullable(); //table.enu('day_week', ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']).notNullable();
			table.enu("day_period", ["MORNING", "AFTERNOON", "NIGHT"], { useNative: true, existingType: false, enumName: "schedule_day_period" }).notNullable(); //table.enu('day_period', ['MORNING', 'AFTERNOON', 'NIGHT']).notNullable();
			table.unique(["day_week", "day_period"]);
		})

		// application_schedule
		.createTable("application_schedule", (table) => {
			table.increments();
			table.integer("application_id").unsigned().references("id").inTable("application");
			table.integer("schedule_id").unsigned().references("id").inTable("schedule");
			table.unique(["application_id", "schedule_id"]);
		})

		// absence_reason
		.createTable("absence_reason", (table) => {
			table.increments();
			table.integer("application_attendance_id").unsigned().references("id").inTable("application_attendance");
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.string("reason");
			table.integer("attachment_file_id");
			table.boolean("accept").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("application_attendance_id");
		})

		// experience_user_interest
		.createTable("experience_user_interest", (table) => {
			table.increments();
			table.integer("experience_id").unsigned().references("id").inTable("experience");
			table.integer("user_id").unsigned();
			table.boolean("is_interest").notNullable().defaultTo(true);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});




module.exports.down = async db => db.schema
	.dropTable("application_history")
	.dropTable("experience_history")
	.dropTable("application")
	.dropTable("experience")
	.dropTable("activity")
	.dropTable("application_monthly_report")
	.dropTable("application_attendance")
	.dropTable("payment_grid")
	.dropTable("configuration");

module.exports.configuration = { transaction: true };
