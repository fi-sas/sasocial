module.exports.up = async db =>
	db.schema.createTable("translation", (table) => {
		table.increments();
		table.string("title", 255).notNullable();
		table.string("proponent_service", 255).notNullable();
		table.string("applicant_profile", 255).notNullable();
		table.string("job", 255).notNullable();
		table.string("description", 255).notNullable();
		table.string("selection_criteria", 255).notNullable();
		table.integer("language_id").unsigned().notNullable();
		table.integer("experience_id").unsigned().notNullable().references("id").inTable("experience");
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})
		.table("experience", (table) => {
			table.dropColumn("title");
			table.dropColumn("proponent_service");
			table.dropColumn("applicant_profile");
			table.dropColumn("job");
			table.dropColumn("description");
			table.dropColumn("selection_criteria");
		})
	;




module.exports.down = async db => db.schema
	.dropTable("translation")
	.table("experience", (table) => {
		table.string("title", 255).notNullable();
		table.string("proponent_service", 255).notNullable();
		table.string("applicant_profile", 255).notNullable();
		table.string("job", 255).notNullable();
		table.string("description", 255).notNullable();
		table.string("selection_criteria", 255).notNullable();
	})
	;

module.exports.configuration = { transaction: true };
