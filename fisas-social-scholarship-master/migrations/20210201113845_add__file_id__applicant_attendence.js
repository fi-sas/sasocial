module.exports.up = async db =>

await db.schema.alterTable('application_attendance', (table) => {
    table.integer('file_id').after('n_hours').default();
  });

module.exports.down = async db => 

db.schema.alterTable('application_attendance', (table) => {
	table.dropColumn('file_id');
  });  

module.exports.configuration = { transaction: true };
