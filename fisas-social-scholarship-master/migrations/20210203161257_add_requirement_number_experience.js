module.exports.up = async db =>

	await db.schema.alterTable("experience", (table) => {
		table.integer("requirement_number");
	});

module.exports.down = async db =>

	db.schema.alterTable("experience", (table) => {
		table.dropColumn("requirement_number");
	});

module.exports.configuration = { transaction: true };
