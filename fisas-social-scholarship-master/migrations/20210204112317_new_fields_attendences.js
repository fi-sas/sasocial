module.exports.up = async db =>

	await db.schema.alterTable("application_attendance", (table) => {
		table.datetime("initial_time");
		table.datetime("final_time");
		table.string("executed_service");
	});

module.exports.down = async db =>

	db.schema.alterTable("application_attendance", (table) => {
		table.dropColumn("initial_time");
		table.dropColumn("final_time");
		table.dropColumn("executed_service");
	});

module.exports.configuration = { transaction: true };
