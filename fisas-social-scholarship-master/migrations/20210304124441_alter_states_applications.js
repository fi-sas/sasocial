exports.up = function (knex) {
    return knex.raw("ALTER TYPE application_status ADD VALUE 'EXPIRED' BEFORE 'CLOSED';");
};
exports.down = function (knex) {
};
