module.exports.up = async db =>
    db.schema.createTable("application_interview", (table) => {
        table.increments();
        table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
        table.string("local", 255).notNullable();
        table.datetime("date").notNullable();
        table.integer("responsable_id").unsigned().notNullable();
        table.string("notes").nullable();
        table.integer("file_id").unsigned().nullable();
        table.timestamps(true, true);
    });

module.exports.down = async db => db.schema
    .dropTable("application_interview");

module.exports.configuration = { transaction: true };


