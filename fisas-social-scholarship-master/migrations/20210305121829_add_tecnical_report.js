
module.exports.up = async db =>
await db.schema.alterTable("application", (table) => {
    table.integer("report_file_id").unsigned().nullable();
    table.string("report_notes").nullable();
});

module.exports.down = async db =>

db.schema.alterTable("application", (table) => {
    table.dropColumn("report_file_id");
    table.dropColumn("report_notes");
});

module.exports.configuration = { transaction: true };