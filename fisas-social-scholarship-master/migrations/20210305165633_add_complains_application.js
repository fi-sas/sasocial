
module.exports.up = async db =>
db.schema.createTable("complain_application", (table) => {
    table.increments();
    table.integer("application_id").unsigned().references("id").inTable("application").notNullable();
    table.string("complain").notNullable();
    table.integer("file_id").unsigned().nullable();
    table.timestamps(true, true);
});

module.exports.down = async db => db.schema
.dropTable("complain_application");

module.exports.configuration = { transaction: true };

