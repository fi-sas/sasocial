exports.up = function (knex) {
    return knex.raw(
        "ALTER TYPE experience_status ADD VALUE 'RETURNED' BEFORE 'CLOSED'; ALTER TYPE experience_status ADD VALUE 'ANALYSED' BEFORE 'CLOSED'; ALTER TYPE experience_status ADD VALUE 'SELECTION' BEFORE 'CLOSED'; ALTER TYPE experience_status ADD VALUE 'SEND_SEEM' BEFORE 'CLOSED'; ALTER TYPE experience_status ADD VALUE 'IN_COLABORATION' BEFORE 'CLOSED'; ALTER TYPE experience_status ADD VALUE 'EXTERNAL_SYSTEM' BEFORE 'CLOSED';"
    )
    
};
exports.down = function (knex) {
};
