
module.exports.up = async db =>

await db.schema.alterTable("experience", (table) => {
    table.date("publish_date").nullable().alter();
    table.string("uuid").nullable();
});

module.exports.down = async db =>

db.schema.alterTable("experience", (table) => {
    table.date("publish_date").notNullable().alter();
    table.dropColumn("uuid");
});

module.exports.configuration = { transaction: true };