
module.exports.up = async db =>

await db.schema.alterTable("experience_user_interest", (table) => {
    table.dropColumn("is_interest");
    table.string("uuid");
    table.enum("status", ["SUBMITTED",
    "ANALYSED",
    "INTERVIEWED",
    "APPROVED",
    "WAITING",
    "NOT_SELECTED",
    "ACCEPTED",
    "COLABORATION",
    "WITHDRAWAL",
    "DECLINED",
    "CANCELLED",
    "CLOSED"], { useNative: true, existingType: false, enumName: "user_interest_status" }).notNullable();
    table.integer("contract_file_id").unsigned().nullable();
    table.integer("certificate_file_id").unsigned().nullable();
})
.createTable("user_interest_history", (table) => {
    table.increments();
    table.integer("user_interest_id").unsigned().references("id").inTable("experience_user_interest");
    table.enum("status", ["SUBMITTED",
    "ANALYSED",
    "INTERVIEWED",
    "APPROVED",
    "WAITING",
    "NOT_SELECTED",
    "ACCEPTED",
    "COLABORATION",
    "WITHDRAWAL",
    "DECLINED",
    "CANCELLED",
    "CLOSED"], { useNative: true, existingType: true, enumName: "user_interest_status" }).notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.string("notes").nullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
})
.createTable("user_interest_interview", (table) => {
    table.increments();
    table.integer("user_interest_id").unsigned().references("id").inTable("experience_user_interest");
    table.string("local", 255).notNullable();
    table.datetime("date").notNullable();
    table.integer("responsable_id").unsigned().notNullable();
    table.string("notes").nullable();
    table.integer("file_id").unsigned().nullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
});

module.exports.down = async db =>
db.schema.alterTable("experience_user_interest", (table) => {
    table.boolean("is_interest").notNullable().defaultTo(true);
    table.dropColumn("status");
    table.dropColumn("uuid");
    table.dropColumn("contract_file_id");
    table.dropColumn("certificate_file_id");
})
.dropTable("user_interest_history")
.dropTable("user_interest_interview");

module.exports.configuration = { transaction: true };