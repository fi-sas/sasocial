
module.exports.up = async db =>

await db.schema
.renameTable('application_monthly_report', 'experience_monthly_report')
.alterTable("experience_monthly_report", (table) => {
    table.dropColumn("application_id");
    table.integer("file_id").unsigned().notNullable();
    table.integer("user_interest_id").notNullable().unsigned().references("id")
    .inTable("experience_user_interest");
});

module.exports.down = async db =>
db.schema.alterTable("application_monthly_report", (table) => {
    table.dropColumn("user_interest_id");
    table.dropColumn("file_id");
    table.integer("application_id").notNullable().unsigned().references("id")
				.inTable("application");
})
.renameTable("experience_monthly_report", "application_monthly_report");

module.exports.configuration = { transaction: true };