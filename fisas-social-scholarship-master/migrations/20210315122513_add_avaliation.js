
module.exports.up = async db =>

await db.schema.alterTable("experience_user_interest", (table) => {
    table.string("student_avaliation").nullable();
});

module.exports.down = async db =>

db.schema.alterTable("experience_user_interest", (table) => {
    table.dropColumn("student_avaliation");
});

module.exports.configuration = { transaction: true };
