
module.exports.up = async db =>
await db.schema.
alterTable("application_attendance", (table)=> {
    table.dropColumn("application_id");
    table.integer("user_interest_id").notNullable().unsigned().references("id")
    .inTable("experience_user_interest");
})
.renameTable('application_attendance', 'experience_attendance')
.createTable('complain_user_interest', (table) =>{
    table.increments();
    table.integer("user_interest_id").unsigned().notNullable().references("id").inTable("experience_user_interest");
    table.string("complain").notNullable();
    table.integer("file_id").unsigned().nullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
});

module.exports.down = async db =>

db.schema
.renameTable('experience_attendance', 'application_attendance')
.alterTable("application_attendance", (table)=> {
    table.dropColumn("user_interest_id");
    table.integer("application_id").notNullable().unsigned().references("id")
				.inTable("application");
})
.dropTable("complain_user_interest");

module.exports.configuration = { transaction: true };
