
module.exports.up = async db =>

    await db.schema.alterTable("payment_grid", (table) => {
        table.dropColumn("application_id");
        table.dropColumn("experience_id");
        table.integer("user_account_id").unsigned().nullable();
        table.integer("user_interest_id").notNullable().unsigned().references("id")
            .inTable("experience_user_interest");
    });

module.exports.down = async db =>

    db.schema.alterTable("payment_grid", (table) => {
        table.integer("application_id").notNullable().unsigned().references("id")
            .inTable("application");
        table.integer("experience_id").notNullable().unsigned().references("id")
            .inTable("experience");
        table.dropColumn("user_interest_id");
        table.dropColumn("user_account_id");
    });

module.exports.configuration = { transaction: true };

