module.exports.up = async db =>
    await db.schema
    .alterTable("absence_reason", (table) => {
        table.dropColumn("application_attendance_id");
        table.integer("experience_attendance_id").unsigned().references("id").inTable("experience_attendance");
    })
    .alterTable("experience_user_interest", (table) => {
        table.integer("student_file_avaliation").unsigned().nullable();
        table.integer("report_avaliation").unsigned().nullable()
    });

module.exports.down = async db =>
    db.schema
    .alterTable("absence_reason", (table) => {
        table.dropColumn("experience_attendance_id");
        table.integer("application_attendance_id").unsigned().references("id").inTable("application_attendance");
    })
    .alterTable("experience_user_interest", (table) => {
        table.dropColumn("student_file_avaliation")
        table.dropColumn("report_avaliation")
    });

module.exports.configuration = { transaction: true };
