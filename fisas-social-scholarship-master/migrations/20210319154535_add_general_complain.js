module.exports.up = async db =>
    await db.schema
    .createTable("general-complain", (table) => {
        table.increments();
        table.integer("user_id").unsigned().notNullable();
        table.string("complain").notNullable();
        table.integer("file_id").unsigned().nullable();
        table.timestamps(true, true);
    });

module.exports.down = async db =>
    db.schema
    .dropTable("general-complain");
module.exports.configuration = { transaction: true };
