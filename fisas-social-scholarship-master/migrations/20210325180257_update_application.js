module.exports.up = db =>
    db.schema
    .alterTable("application", (table) => {
        table.string("student_name").notNullable();
        table.string("student_address").notNullable();
        table.string("student_code_postal").notNullable();
        table.string("student_location").notNullable();
        table.date("student_birthdate").notNullable();
        table.string("student_nationality").notNullable();
        table.integer("student_tin").notNullable();
        table.integer("student_identification").notNullable();
        table.integer("student_number").notNullable();
        table.string("student_email").notNullable();
        table.integer("student_mobile_phone").nullable();
        table.integer("student_phone").nullable();
    });
module.exports.down = db =>
    db.schema
    .alterTable("application", (table) => {
        table.dropColumn("student_name");
        table.dropColumn("student_address");
        table.dropColumn("student_code_postal");
        table.dropColumn("student_location");
        table.dropColumn("student_birthdate");
        table.dropColumn("student_nationality");
        table.dropColumn("student_tin");
        table.dropColumn("student_identification");
        table.dropColumn("student_number");
        table.dropColumn("student_email");
        table.dropColumn("student_mobile_phone");
        table.dropColumn("student_phone");
    });
module.exports.configuration = { transaction: true };

