module.exports.up = db =>
    db.schema
    .alterTable("experience_attendance", (table) => {
        table.enum("status", ["PENDING", "ACCEPTED", "REJECTED", "LACK"]).notNullable();
        table.float("n_hours_reject").nullable();
        table.string("reject_reason").nullable();
    })
    .alterTable("payment_grid", (table)=> {
        table.float("hours_pay").notNullable();
        table.float("hours_transit").notNullable();
    })
    .createTable("user_interest_transit_hours", (table) => {
        table.increments();
        table.float("hours").notNullable();
        table.integer("user_interest_id").unsigned().notNullable().references("id").inTable("experience_user_interest");
        table.datetime("created_at").notNullable();
        table.datetime("updated_at").notNullable();
    })
    .alterTable("application_interview", (table) => {
        table.string("observations").nullable();
        table.string("scope").nullable();
    })
    .alterTable("user_interest_interview", (table) => {
        table.string("observations").nullable();
        table.string("scope").nullable();
    })
    .alterTable("complain_application", (table) =>{
        table.string("response").nullable();
        table.enum("status",  ["SUBMITTED", "ANALYSED", "REPLYED"])
    })
    .alterTable("complain_user_interest", (table) => {
        table.string("response").nullable();
        table.enum("status",  ["SUBMITTED", "ANALYSED", "REPLYED"])
    })
    .alterTable("general-complain", (table) => {
        table.string("response").nullable();
        table.enum("status",  ["SUBMITTED", "ANALYSED", "REPLYED"])
    });
    
module.exports.down = db =>
    db.schema
    .alterTable("experience_attendance", (table) => {
        table.dropColumn("status");
        table.dropColumn("n_hours_reject");
        table.dropColumn("reject_reason");
    })
    .alterTable("payment_grid", (table)=> {
        table.dropColumn("hours_pay");
        table.dropColumn("hours_transit");
    })
    .alterTable("application_interview", (table) => {
        table.dropColumn("observations");
        table.dropColumn("scope");
    })
    .alterTable("user_interest_interview", (table) => {
        table.dropColumn("observations");
        table.dropColumn("scope");
    })
    .alterTable("complain_application", (table) => {
        table.dropColumn("response");
        table.dropColumn("status");
    })
    .alterTable("complain_user_interest", (table) => {
        table.dropColumn("response");
        table.dropColumn("status");
    })
    .alterTable("general-complain", (table) => {
        table.dropColumn("response");
        table.dropColumn("status");
    })
    .dropTable("user_interest_transit_hours");
module.exports.configuration = { transaction: true };