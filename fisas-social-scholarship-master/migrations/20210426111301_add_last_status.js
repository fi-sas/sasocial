module.exports.up = db =>
    db.schema
    .alterTable("application", (table) => {
        table.enum("last_status", ["SUBMITTED","ANALYSED","INTERVIEWED","ACCEPTED", "DECLINED", "EXPIRED", "CANCELLED"]).nullable();
    })
    .alterTable("experience", (table) => {
        table.enum("last_status", ["SUBMITTED", "RETURNED", "ANALYSED", "APPROVED", "PUBLISHED", "REJECTED", "CANCELLED", "SEND_SEEM", "EXTERNAL_SYSTEM", "SELECTION", "IN_COLABORATION", "CLOSED", "CANCELED"]).nullable();
    })
    .alterTable("experience_user_interest", (table) => {
        table.enum("last_status", ["SUBMITTED", "ANALYSED", "INTERVIEWED", "APPROVED", "WAITING", "NOT_SELECTED", "ACCEPTED", "COLABORATION", "WITHDRAWAL", "DECLINED", "CANCELLED", "CLOSED",]).nullable();
    });
module.exports.down = db =>
    db.schema
    .alterTable("application", (table) => {
        table.dropColumn("last_status");
    })
    .alterTable("experience", (table)=> {
        table.dropColumn("last_status");
    })
    .alterTable("experience_user_interest", (table) => {
        table.dropColumn("last_status");
    });
module.exports.configuration = { transaction: true };