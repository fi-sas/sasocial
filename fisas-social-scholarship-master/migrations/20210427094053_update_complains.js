module.exports.up = db =>
    db.schema
    .alterTable("complain_application", (table) => {
        table.integer("user_id").unsigned().nullable();
    })
    .alterTable("complain_user_interest", (table) => {
        table.integer("user_id").unsigned().nullable();
    })
    .createTable("complain_experience", (table) => {
        table.increments();
        table.integer("experience_id").unsigned().references("id").inTable("experience");
        table.integer("user_id").unsigned().nullable();
        table.integer("file_id").unsigned().nullable();
        table.string("complain").notNullable();
        table.string("response").nullable();
        table.enum("status", ["SUBMITTED", "ANALYSED", "REPLYED"]).notNullable();
        table.datetime("updated_at").notNullable();
        table.datetime("created_at").notNullable();
    });

module.exports.down = db =>
    db.schema
    .alterTable("complain_application", (table) => {
        table.dropColumn("user_id");
    })
    .alterTable("complain_user_interest", (table)=> {
        table.dropColumn("user_id");
    })
    .dropTable("complain_experience");
module.exports.configuration = { transaction: true };