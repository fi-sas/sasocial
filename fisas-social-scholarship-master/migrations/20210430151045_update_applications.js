module.exports.up = db =>
    db.schema
    .alterTable("experience_monthly_report", (table) => {
        table.integer("file_id").unsigned().nullable().alter();
    })
    .alterTable("application", (table) => {
        table.string("student_tin").notNullable().alter();
        table.string("student_identification").notNullable().alter();
        table.string("student_number").notNullable().alter();
        table.string("student_mobile_phone").nullable().alter();
        table.string("student_phone").nullable().alter();
    })
module.exports.down = db =>
    db.schema
    .alterTable("experience_monthly_report", (table) => {
        table.integer("file_id").unsigned().notNullable().alter();
    })
    .alterTable("application", (table)=> {
        table.integer("student_identification").notNullable();
        table.integer("student_number").notNullable();
        table.integer("student_tin").notNullable();
        table.integer("student_mobile_phone").nullable();
        table.integer("student_phone").nullable();
    });
module.exports.configuration = { transaction: true };