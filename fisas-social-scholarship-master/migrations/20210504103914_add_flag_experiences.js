
module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.boolean("published").default(true);
})
module.exports.down = db =>
db.schema
.alterTable("experience", (table)=> {
    table.dropColumn("published");
});
module.exports.configuration = { transaction: true };
