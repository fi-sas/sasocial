

module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.string("return_reason").nullable();
})
module.exports.down = db =>
db.schema
.alterTable("experience", (table)=> {
    table.dropColumn("return_reason");
});
module.exports.configuration = { transaction: true };

