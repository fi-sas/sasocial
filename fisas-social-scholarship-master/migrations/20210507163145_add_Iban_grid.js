
module.exports.up = db =>
db.schema
.alterTable("payment_grid", (table) => {
    table.string("iban").nullable();
})
module.exports.down = db =>
db.schema
.alterTable("payment_grid", (table)=> {
    table.dropColumn("iban");
});
module.exports.configuration = { transaction: true };
