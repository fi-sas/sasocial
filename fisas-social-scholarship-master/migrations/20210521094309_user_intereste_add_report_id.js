
module.exports.up = db =>
db.schema
.alterTable("experience_user_interest", (table) => {
    table.integer("certificate_generated_id").nullable();
})
module.exports.down = db =>
db.schema
.alterTable("experience_user_interest", (table)=> {
    table.dropColumn("certificate_generated_id");
});
module.exports.configuration = { transaction: true };