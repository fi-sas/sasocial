
module.exports.up = db =>
db.schema
.alterTable("external_entity", (table) => {
    table.string("name").nullable();
    table.string("email").nullable();
    table.enum("gender", ["M", "F", "U"]);
    table.string("address").nullable();
    table.string("postal_code").nullable();
    table.string("city").nullable();
    table.string("country").nullable();
    table.string("phone").nullable();
    table.string("tin").nullable();
    table.integer("user_id").nullable().alter();
    table.enum("status", ["CREATED", "VALIDATED"]).nullable();
    table.boolean("active").defaultTo(true);
})

module.exports.down = db =>
db.schema
.alterTable("external_entity", (table)=> {
    table.dropColumn("name");
    table.dropColumn("email");
    table.dropColumn("gender");
    table.dropColumn("address");
    table.dropColumn("portal_code");
    table.dropColumn("city");
    table.dropColumn("country");
    table.dropColumn("phone");
    table.dropColumn("tin");
    table.dropColumn("status");
    table.dropColumn("active");
    table.integer("user_id").notNullable().alter();
});
module.exports.configuration = { transaction: true };