
module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.integer("experience_advisor_id").nullable().alter();
    table.string("email_search_advisor").nullable();
})

module.exports.down = db =>
db.schema
.alterTable("experience", (table)=> {
    table.integer("experience_advisor_id").notNullable().alter();
    table.dropColumn("email_search_advisor");
});
module.exports.configuration = { transaction: true };