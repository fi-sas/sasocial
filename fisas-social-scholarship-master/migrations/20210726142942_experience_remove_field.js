

module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.dropColumn("current_account_id");
})

module.exports.down = db =>
db.schema
.alterTable("experience", (table)=> {
    table.integer("current_account_id").nullable();
});
module.exports.configuration = { transaction: true };