
module.exports.up = db =>
db.schema
.alterTable("configuration", (table) => {
    table.string("value", 250).nullable().alter();
})

module.exports.down = db =>
db.schema
.alterTable("configuration", (table)=> {
    table.string("value", 250).notNullable().alter();
});
module.exports.configuration = { transaction: true };