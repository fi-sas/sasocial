
module.exports.up = db =>
db.schema
.alterTable("experience_attendance", (table) => {
    table.string("n_hours", 5).nullable().alter();
    table.string("n_hours_reject", 5).nullable().alter();
})

module.exports.down = db =>
db.schema
.alterTable("experience_attendance", (table)=> {
    table.float("n_hours").nullable().defaultTo(0);
    table.float("n_hours_reject").nullable();
});
module.exports.configuration = { transaction: true };