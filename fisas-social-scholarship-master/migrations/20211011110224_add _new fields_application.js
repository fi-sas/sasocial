
module.exports.up = db =>
db.schema
.alterTable("application", (table) => {
    table.enum("genre", ["M", "F", "U"]).nullable()
    table.string("nationality", 255).nullable();
    table.boolean("housed_residence").nullable().defaultTo(false);
    table.boolean("scholarship_dges").nullable().defaultTo(false);
    table.boolean("other_scholarship").nullable().defaultTo(false);
    table.float("scholarship_values").nullable();
})

module.exports.down = db =>
db.schema
.alterTable("application", (table)=> {
    table.dropColumn("genre");
    table.dropColumn("nationality");
    table.dropColumn("housed_residence");
    table.dropColumn("scholarship_dges");
    table.dropColumn("other_scholarship");
    table.dropColumn("scholarship_values");
});
module.exports.configuration = { transaction: true };