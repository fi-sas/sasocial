
module.exports.up = db =>
db.schema
.alterTable("application", (table) => {
    table.enum("decision", ["ACCEPTED", "REJECTED"]).nullable();
    table.string("reject_reason" ).nullable();
})

module.exports.down = db =>
db.schema
.alterTable("applications", (table) => {
    table.dropColumn("decision");
    table.dropColumn("reject_reason");
})

module.exports.configuration = { transaction: true };
