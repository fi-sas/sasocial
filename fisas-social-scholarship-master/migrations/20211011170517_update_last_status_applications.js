
exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "application"
	  DROP CONSTRAINT "application_last_status_check",
	  ADD CONSTRAINT "application_last_status_check" 
	  CHECK (last_status IN ('SUBMITTED','ANALYSED','INTERVIEWED','ACCEPTED', 'DECLINED', 'EXPIRED', 'CANCELLED', 'WITHDRAWAL_ACCEPTED', 'DISPATCH', NULL))
	`);
  };

  exports.down = function (knex) {
};
