module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.enum("decision", ["ACCEPTED", "REJECTED", "RETURNED"]).nullable();
    table.string("reject_reason" ).nullable();
})

module.exports.down = db =>
db.schema
.alterTable("experience", (table) => {
    table.dropColumn("decision");
    table.dropColumn("reject_reason");
})

module.exports.configuration = { transaction: true };
