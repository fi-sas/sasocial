
module.exports.up = db =>
db.schema
.alterTable("application", (table) => {
    table.dropColumn("nationality");
})

module.exports.down = db =>
db.schema
.alterTable("applications", (table) => {
    table.string("nationality").nullable();
})

module.exports.configuration = { transaction: true };
