module.exports.up = (db) =>
	db.schema
		.alterTable("experience_attendance", (table) => {
			table.text("executed_service").alter();
		})
		.alterTable("absence_reason", (table) => {
			table.text("reject_reason").alter();
			table.text("reason").alter();
		});

module.exports.down = (db) =>
	db.schema
		.alterTable("experience_attendance", (table) => {
			table.string("executed_service").alter();
		})
		.alterTable("absence_reason", (table) => {
			table.string("reject_reason").nullable().alter();
			table.string("reason").alter();
		});

module.exports.configuration = { transaction: true };
