module.exports.up = (db) =>
	db.schema.alterTable("experience_attendance", (table) => {
		table.datetime("real_initial_time");
		table.datetime("real_final_time");
	});

module.exports.down = (db) =>
	db.schema.alterTable("experience_attendance", (table) => {
		table.dropColumn("real_initial_time");
		table.dropColumn("real_final_time");
	});

module.exports.configuration = { transaction: true };
