exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "experience_user_interest"
	  DROP CONSTRAINT "experience_user_interest_decision_check",
	  ADD CONSTRAINT "experience_user_interest_decision_check" 
	  CHECK (decision IN ('ACCEPTED', 'REJECTED', 'NOT_SELECTED', 'WAITING', NULL))
	`);
  };

  exports.down = function (knex) {
};
