module.exports.up = (db) =>
	db.schema
		.raw("ALTER TABLE application RENAME COLUMN scholarship_values TO other_scholarship_values;")
		.alterTable("application", (table) => {
			table.dropColumn("scholarship_dges");
		});

module.exports.down = (db) =>
	db.schema
		.raw("ALTER TABLE application RENAME COLUMN other_scholarship_values TO scholarship_values;")
		.alterTable("application", (table) => {
			table.boolean("scholarship_dges").nullable().defaultTo(false);
		});
module.exports.configuration = { transaction: true };
