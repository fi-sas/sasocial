module.exports.up = (db) =>
	db.schema.alterTable("experience_monthly_report", (table) => {
		table.text("report").nullable().alter();
	});
module.exports.down = (db) =>
	db.schema.alterTable("experience_monthly_report", (table) => {
		table.text("report").notNullable().alter();
	});

module.exports.configuration = { transaction: true };
