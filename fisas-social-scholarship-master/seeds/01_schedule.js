module.exports.seed = async (knex) => {
  const data = [{
      id: 1,
      day_week: 'SUNDAY',
      day_period: 'MORNING',
    },
    {
      id: 2,
      day_week: 'SUNDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 3,
      day_week: 'SUNDAY',
      day_period: 'NIGHT',
    },
    {
      id: 4,
      day_week: 'MONDAY',
      day_period: 'MORNING',
    },
    {
      id: 5,
      day_week: 'MONDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 6,
      day_week: 'MONDAY',
      day_period: 'NIGHT',
    },
    {
      id: 7,
      day_week: 'TUESDAY',
      day_period: 'MORNING',
    },
    {
      id: 8,
      day_week: 'TUESDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 9,
      day_week: 'TUESDAY',
      day_period: 'NIGHT',
    },
    {
      id: 10,
      day_week: 'WEDNESDAY',
      day_period: 'MORNING',
    },
    {
      id: 11,
      day_week: 'WEDNESDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 12,
      day_week: 'WEDNESDAY',
      day_period: 'NIGHT',
    },
    {
      id: 13,
      day_week: 'THURSDAY',
      day_period: 'MORNING',
    },
    {
      id: 14,
      day_week: 'THURSDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 15,
      day_week: 'THURSDAY',
      day_period: 'NIGHT',
    },
    {
      id: 16,
      day_week: 'FRIDAY',
      day_period: 'MORNING',
    },
    {
      id: 17,
      day_week: 'FRIDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 18,
      day_week: 'FRIDAY',
      day_period: 'NIGHT',
    },
    {
      id: 19,
      day_week: 'SATURDAY',
      day_period: 'MORNING',
    },
    {
      id: 20,
      day_week: 'SATURDAY',
      day_period: 'AFTERNOON',
    },
    {
      id: 21,
      day_week: 'SATURDAY',
      day_period: 'NIGHT',
    }
  ];

  return Promise.all(data.map(async (d) => {
    // Check if item exist
    const rows = await knex('schedule').select().where('id', d.id);
    if (rows.length === 0) {
      await knex('schedule').insert(d);
    }
    return true;
  }));
};
