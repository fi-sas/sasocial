exports.seed = async (knex) => {
	const data = [
		{
			key: "ADMIN_NOTIFICATION_EMAIL",
			value: "admin@sasocial.pt",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "EXTERNAL_ENTITY_PROFILE_ID",
			value: "4",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "TIME_CLOSE_APPLICATIONS",
			value: "6",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "CURRENT_ACCOUNT",
			value: null,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "MAX_HOURS_WORK",
			value: null,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "FINANCIAL_NOTIFICATION_EMAIL",
			value: "admin@sasocial.pt",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "RESPONSABLE_PROFILE",
			value: null,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "ASSIGNED_STATUS_AUTO_CHANGE",
			value: "false",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "STATUS_AUTO_CHANGE_DAYS",
			value: "5",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "EVENT_FOR_AUTO_CHANGE",
			value: "COLABORATION",
			updated_at: new Date(),
			created_at: new Date(),
		},
		{
			key: "SERVICES_RESPONSABLES_NOTIFICATION_EMAIL",
			value: "admin@sasocial.pt",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "HAS_CURRENT_ACCOUNT",
			value: "true",
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "APPLICATION_EMAILS",
			value: [],
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "EXPERIENCE_EMAILS",
			value: [],
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "USER_INTEREST_EMAILS",
			value: [],
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			key: "ACCEPTED_APPLICATION_STATUS_AUTO_CHANGE",
			value: "false",
			updated_at: new Date(),
			created_at: new Date(),
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("configuration").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("configuration").insert(d);
			}
			return true;
		}),
	);
};
