"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.absence_reason",
	table: "absence_reason",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "absence_reason")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"experience_attendance_id",
			"start_date",
			"end_date",
			"reason",
			"attachment_file_id",
			"accept",
			"reject_reason",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: ["attachment_file"],
		withRelateds: {
			"experience_attendance"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.attendances", "experience_attendance", "experience_attendance_id");
			},
			"attachment_file"(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "media.files", "attachment_file", "attachment_file_id");
			}
		},
		entityValidator: {
			experience_attendance_id: { type: "number", integer: true, convert: true },
			start_date: { type: "date", convert: true, optional: true },
			end_date: { type: "date", convert: true, optional: true },
			reason: { type: "string", optional: true },
			attachment_file_id: { type: "number", integer: true, convert: true, optional: true },
			accept: { type: "boolean", convert: true, optional: true },
			reject_reason: { type: "string", optional: true, nullable: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: [
				"validateNoDuplicateAbsences",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.accept = null;
					const attendance = await ctx.call("social_scholarship.attendances.get", { id: ctx.params.experience_attendance_id, withRelated: false });
					if(attendance[0].status !== "LACK") {
						throw new Errors.ValidationError(
							"This attendance is not a lack",
							"SOCIAL_SCHOLARSHIP_ABSENCE_REASON_CREATE_ERROR",
							{},
						);
					}
					ctx.params.start_date =  attendance[0].initial_time;
					ctx.params.end_date = attendance[0].final_time;
				}
			],
			update: [
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					const attendance = await ctx.call("social_scholarship.attendances.get", { id: ctx.params.experience_attendance_id, withRelated: false });
					ctx.params.start_date =  attendance[0].initial_time;
					ctx.params.end_date = attendance[0].final_time;
				}
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			"accept-absence-reason-by-id": ["validateAbsenceReason"]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query"
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping"],
			}
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		"accept-absence-reason-by-id": {
			rest: "POST /:id/accept",
			visibility: "published",
			scope: "social_scholarship:absence_reason:list",
			params: {
				id: { type: "string", convert: true },
				accept: { type: "boolean", convert: true }
			},
			async handler(ctx) {
				if (ctx.params.accept) {
					return await ctx.call("social_scholarship.absence_reason.patch", {
						id: ctx.params.id,
						accept: ctx.params.accept,
						reject_reason: null
					});
				} else {
					const validation = new Validator();
					const schema = {
						reject_reason: { type: "string" },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(new Errors.ValidationError("Entity validation error!", null, res),);
					} else {
						return await ctx.call("social_scholarship.absence_reason.patch", {
							id: ctx.params.id,
							accept: ctx.params.accept,
							reject_reason: ctx.params.reject_reason
						});
					}
				}
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async validateNoDuplicateAbsences(ctx){
			if(ctx.params.experience_attendance_id){
				const absence = await this._find(ctx, { query: {
					experience_attendance_id: ctx.params.experience_attendance_id
				} });
				if(absence.length > 0 ) {
					throw new Errors.ValidationError(
						"Already exist absence reason for attendance",
						"SOCIAL_SCHOLARSHIP_ABSENCE_REASON_CREATE_ERROR",
						{},
					);

				}
			}

		},
		async validateAbsenceReason(ctx) {
			await ctx.call("social_scholarship.absence_reason.get", {
				id: ctx.params.id
			});

		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
