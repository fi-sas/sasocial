"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.application_activities",
	table: "application_activity",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "application_activities")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"activity_id",
		],
		defaultWithRelateds: ["activity"],
		withRelateds: {
			activity(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.activities", "activity", "activity_id");
			},
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.applications", "application", "application_id");
			},
		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			activity_id: { type: "number", positive: true, integer: true, convert: true },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
