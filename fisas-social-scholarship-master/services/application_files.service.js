"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.application_files",
	table: "application_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "application_files")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "file_id", "application_id", "updated_at", "created_at"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			name: "string|max:255|optional",
			file_id: "number|integer|convert",
			application_id: "number|integer|convert",
			updated_at: "date|convert|optional",
			created_at: "date|convert|optional",
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_application_files: {
			params: {
				application_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				file_ids: {
					type: "array",
					min: 0,
					items: {
						type: "object",
						props: {
							file_id: {
								type: "number",
								positive: true,
								integer: true,
								convert: true,
								optional: false,
								nullable: false,
							},
							name: "string|max:255",
						},
					},
				},
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id,
				});
				this.clearCache();
				const entities = ctx.params.file_ids.map((file_id) => ({
					application_id: ctx.params.application_id,
					file_id: file_id.file_id,
					name: file_id.name,
					created_at: new Date(),
					updated_at: new Date(),
				}));

				return this._insert(ctx, { entities });
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
