"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.application_schedules",
	table: "application_schedule",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "application_schedules")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"schedule_id",
		],
		defaultWithRelateds: ["schedule"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.applications", "application", "application_id");
			},
			schedule(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.schedules", "schedule", "schedule_id");
			}
		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			schedule_id: { type: "number", positive: true, integer: true, convert: true },
		}
	},
	hooks: {
		before: {
			create: [],
			update: []
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		schedules_of_application: {
			rest: {
				method: "GET",
				path: "/:application_id/schedules"
			},
			scope: "social_scholarship:applications:read",
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {

				return this._find(ctx, {
					query: {
						application_id: ctx.params.application_id,
					},
				}).then((res) => {
					return ctx.call("social_scholarship.applications.find", {
						query: {
							id: res.map((g) => g.application_id),
						},
					});
				});
			}
		},
		create_relation_application_schedules: {
			cache: {
				keys: ["application_id", "schedule_id"],
				ttl: 60
			},
			rest: {
				method: "POST",
				path: "/application_schedules"
			},
			scope: "social_scholarship:application_schedules:create",
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				},
				schedule_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {
				await this.removeRelationsBetweenApplicationSchedule(ctx);
				this.clearCache();

				const entities = [{ application_id: ctx.params.application_id, schedule_id: ctx.params.schedule_id }];

				return this._insert(ctx, { entities });
			}
		},
		save_schedules: {
			params: {
				application_id: { type: "number", positive: true, integer: true, convert: true },
				schedules_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id
				});
				this.clearCache();

				const entities = ctx.params.schedules_ids.map(schedule_id => ({
					application_id: ctx.params.application_id,
					schedule_id,
				}));
				this.logger.info("save_schedules/insert - entities:");
				this.logger.info(entities);
				return this._insert(ctx, { entities });
			}
		},
		remove_relation_application_schedule: {
			cache: {
				keys: ["application_id", "schedule_id"],
				ttl: 60
			},
			rest: {
				method: "DELETE",
				path: "/:application_id/:schedule_id"
			},
			scope: "social_scholarship:application_schedules:create",
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				},
				schedule_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {
				return this.removeRelationsBetweenApplicationSchedule(ctx);
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

		removeRelationsBetweenApplicationSchedule(ctx) {
			return this.adapter.find({
				query: (q) => {
					q.where({
						application_id: ctx.params.application_id,
						schedule_id: ctx.params.schedule_id
					})
						.del();
					return q;
				}
			})
				.then(docs => this.transformDocuments(ctx, ctx.params, docs));
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
