"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError, ForbiddenError } = require("@fisas/ms_core").Helpers.Errors;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const Validator = require("fastest-validator");
const {
	APPLICATION_STATUS,
	APPLICATION_EVENTS,
	CONFIGURATION_KEYS,
} = require("./utils/constants.js");
const UUIDV4 = require("uuid").v4;
const ApplicationStateMachine = require("./state-machines/application.machine");
const moment = require("moment");
moment.locale("pt");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.applications",
	table: "application",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "applications")],

	crons: [
		{
			name: "closeApplications",
			cronTime: "0 0 * * *",
			onTick: function () {
				this.logger.info("cron expire applications!");
				this.getLocalService("social_scholarship.applications")
					.actions.closeByJob()
					.then(() => {});
			},
			runOnInit: function () {
				//this.logger.info("cron saveDaylyMetrics created!");
			},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"academic_year",
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring)//"subject",
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring)//"mentor_or_mentee",
			"course_year",
			"student_name",
			"student_address",
			"student_code_postal",
			"student_location",
			"student_birthdate",
			"student_nationality",
			"student_tin",
			"student_identification",
			"student_number",
			"student_email",
			"student_mobile_phone",
			"student_phone",
			"course_id",
			"course_degree_id",
			"school_id",
			"iban",
			"has_scholarship",
			"has_applied_scholarship",
			"is_waiting_scholarship_response",
			"scholarship_monthly_value",
			"has_emergency_fund",
			"has_profissional_experience",
			"type_of_company",
			"job_at_company",
			"job_description",
			"has_collaborated_last_year",
			"previous_activity_description",
			"has_holydays_availability",
			"foreign_languages",
			"language_skills",
			"user_id",
			"observations",
			"cv_file_id",
			"status",
			"notification_guid",
			"report_file_id",
			"report_notes",
			"updated_at",
			"created_at",
			"experience_id",
			"last_status",
			"genre",
			"housed_residence",
			"other_scholarship",
			"other_scholarship_values",
			"decision",
		],
		defaultWithRelateds: [
			"history",
			"preferred_activities",
			"preferred_schedule",
			"user",
			"course_degree",
			"school",
			"course",
			"organic_units",
			"interviews",
			"files",
		], //['experience', 'history', 'preferred_activity', 'preferred_schedule', 'user', 'course_degree', 'school', 'course', 'organic_units'],
		withRelateds: {
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			school(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "school", "school_id");
			},
			course_degree(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"configuration.course-degrees",
					"course_degree",
					"course_degree_id",
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.applicationHistory",
					"history",
					"id",
					"application_id",
				);
			},
			preferred_schedule(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.application_schedules",
					"preferred_schedule",
					"id",
					"application_id",
				);
			},
			preferred_activities(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.application_activities",
					"preferred_activities",
					"id",
					"application_id",
				);
			},
			cv_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "cv_file", "cv_file_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file_9_16(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file_9_16", "file_id_9_16");
			},
			organic_units(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.application_organic_units",
					"organic_units",
					"id",
					"application_id",
				);
			},
			interviews(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.application-interviews",
					"interviews",
					"id",
					"application_id",
				);
			},
			historic_applications(ids, applications, rule, ctx) {
				const user_ids = applications.map((app) => app.user_id);
				return this.getUserApplications(ctx, user_ids).then((historycs) => {
					applications.forEach((application) => {
						application.historic_applications = historycs.filter(
							(historyc) => historyc.user_id === application.user_id,
						);
					});
				});
			},
			historic_colaborations(ids, applications, rule, ctx) {
				const user_ids = applications.map((app) => app.user_id);
				return this.getUserExperiences(ctx, user_ids).then((historycs) => {
					applications.forEach((application) => {
						application.historic_colaborations = historycs.filter(
							(historyc) => historyc.user_id === application.user_id,
						);
					});
				});
			},
			notifications(ids, docs, rule, ctx) {
				let idss = docs.map((d) => d.notification_guid);
				idss = idss.filter((id) => id !== null);
				const query = {};
				query.external_uuid = idss;
				return ctx
					.call("notifications.alerts.find", {
						query,
					})
					.then((data) => {
						return docs.map(
							(d) =>
								(d.notifications = data.filter((dt) => dt.external_uuid === d.notification_guid)),
						);
					});
			},
			files(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.application_files",
					"files",
					"id",
					"application_id",
				);
			},
		},
		entityValidator: {
			student_name: "string|max:255",
			student_address: "string|max:255",
			student_code_postal: "string|max:9",
			student_location: "string|max:255",
			student_birthdate: "date|convert",
			student_nationality: "string|max:255",
			student_tin: "string",
			student_identification: "string",
			student_number: "string",
			student_email: "email",
			student_mobile_phone: "string|optional|nullable",
			student_phone: "string|optional|nullable",
			academic_year: { type: "string", max: 9, pattern: "^\\d{4}-\\d{4}" },
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //subject: /*"string|max:2",*/ { type: "enum", values: Object.keys(APPLICATION_SUBJECT) },
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //mentor_or_mentee: /*"string|max:6",*/ { type: "enum", values: Object.keys(APPLICATION_MENTOR_OR_MENTEE) },
			course_year: "string|max:255|convert",
			course_id: "number|integer|convert|optional",
			course_degree_id: "number|integer|convert|optional",
			school_id: "number|integer|convert|optional",
			iban: "string|max:255|optional",
			has_scholarship: "boolean|convert",
			has_applied_scholarship: "boolean|convert",
			is_waiting_scholarship_response: "boolean|convert",
			scholarship_monthly_value: "number|convert|optional",
			has_emergency_fund: "boolean|convert",
			has_profissional_experience: "boolean|convert",
			type_of_company: "string|max:255|optional",
			job_at_company: "string|max:255|optional",
			job_description: "string|max:255|optional",
			has_collaborated_last_year: "boolean|convert",
			previous_activity_description: "string|max:255|optional",
			has_holydays_availability: "boolean|convert",
			foreign_languages: "boolean|convert",
			language_skills: "string|optional", //{ type: "array", items: "string", optional: true }, //{ type: "object", strict: false },//"string|optional", // -> TODO set as Array of strings
			user_id: "number|integer|convert|optional",
			observations: "string|optional",
			cv_file_id: "number|integer|convert|optional",
			status: /*"string|max:255"*/ { type: "enum", values: Object.keys(APPLICATION_STATUS) },
			notification_guid: "string|max:36|optional",
			experience_id: "number|integer|convert|optional",
			updated_at: "date|convert|optional",
			created_at: "date|convert|optional",
			report_notes: { type: "string", optional: true, nullable: true },
			report_file_id: { type: "number", integer: true, optional: true, nullable: true },
			preferred_activities_ids: { type: "array", items: "number", optional: true },
			preferred_schedule_ids: { type: "array", items: "number", optional: true },
			organic_units_ids: { type: "array", items: "number", optional: true },
			last_status: {
				type: "enum",
				values: Object.keys(APPLICATION_STATUS),
				optional: true,
				nullable: true,
			},
			genre: { type: "enum", values: ["M", "F", "U"] },
			housed_residence: { type: "boolean", convert: true, default: false },
			other_scholarship: { type: "boolean", convert: true, default: false },
			other_scholarship_values: {
				type: "number",
				integer: false,
				convert: true,
				optional: true,
				nullable: true,
			},
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "CANCELLED"],
				optional: true,
				nullable: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
			file_ids: {
				type: "array",
				min: 0,
				items: {
					type: "object",
					props: {
						file_id: {
							type: "number",
							positive: true,
							integer: true,
							convert: true,
							optional: false,
							nullable: false,
						},
						name: "string|max:255",
					},
				},
			},
		},
	},
	hooks: {
		before: {
			create: [
				"validateStatus",
				"parseJsonFields",
				"validateActiveApplication",
				async function sanatizeParams(ctx) {
					// Validar se ja tem candidatura à experiencia
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";

					if (ctx.params.experience_id)
						await ctx.call("social_scholarship.experiences.get", { id: ctx.params.experience_id });

					await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
					await ctx.call("configuration.course-degrees.get", { id: ctx.params.course_degree_id });
					await ctx.call("infrastructure.organic-units.get", { id: ctx.params.school_id });
				},
			],
			update: [
				"validateStatus",
				"parseJsonFields",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();

					await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
					await ctx.call("configuration.course-degrees.get", { id: ctx.params.course_degree_id });
					await ctx.call("infrastructure.organic-units.get", { id: ctx.params.school_id });
				},
			],
			patch: [
				"parseJsonFields",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query["user_id"] = ctx.meta.user.id;
					}
				},
			],
			remove: ["deleteRelatedSchedule"],
		},
		after: {
			create: [
				"saveSchedule",
				"savePreferredActivities",
				"saveCreateHistory",
				"saveOrganicUnits",
				"saveApplicationFiles",
				async function sendNotifications(ctx, response) {
					this.sendNotification(
						ctx,
						"SUBMITTED",
						ctx.meta.user.id,
						ctx.meta.user.name,
						response[0].notification_guid,
					);

					return response;
				},
				"acceptedApplicationStatusAutoChange",
			],
			update: ["updateWithRelateds", "saveApplicationFiles"],
			patch: ["updateWithRelateds", "saveApplicationFiles"],
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						if (resp[0].user_id !== ctx.meta.user.id) {
							throw new ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_APPLICATION_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				const applications = await this._find(ctx, {
					query: {
						user_id: ctx.meta.user.id,
						status: ["ACCEPTED", "SUBMITTED", "ANALYSED", "INTERVIEWED", "DISPATCH"],
					},
					withRelated: ["user", "course_degree", "school", "course"],
					limit: 3,
				});
				if (applications.length > 0) {
					applications[0].hours = await ctx.call(
						"social_scholarship.general-reports.general-report-by-user-id",
						{ id: ctx.meta.user.id },
					);
					return applications;
				} else {
					throw new ValidationError(
						"User don't have any contracted application",
						"SOCIAL_SCHOLARSHIP_USER_DONT_HAVE_PUBLISHED_APPLICATION",
						{},
					);
				}
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#user.id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		closeByJob: {
			async handler(ctx) {
				const time_close = await ctx.call(
					"social_scholarship.configurations.getTimeCloseApplications",
				);
				if (time_close.length > 0) {
					// start_date calculate
					const time_month = time_close[0].value;
					const date_start_close = moment().subtract(time_month, "months").format("YYYY-MM-DD");
					const end_start_close = moment().format("YYYY-MM-DD");
					const academic_year = await ctx.call(
						"configuration.academic_years.get_current_academic_year",
					);

					// all applications accepted for current academic_year
					const applications = await this._find(ctx, {
						query: {
							academic_year: academic_year[0].academic_year,
							status: ["ACCEPTED"],
						},
						withRelated: false,
					});

					// all experiences in colaboration and closed for current academic_year
					const experiences = await ctx.call("social_scholarship.experiences.find", {
						query: {
							academic_year: academic_year[0].academic_year,
							status: ["IN_COLABORATION", "CLOSED"],
						},
						withRelated: false,
					});

					// all user_intereste of current academic year
					let user_interest_list = [];
					for (let experience of experiences) {
						const user_interest = await ctx.call(
							"social_scholarship.experience-user-interest.find",
							{
								query: {
									experience_id: experience.id,
									status: ["COLABORATION", "CLOSED", "WITHDRAWAL", "CANCELLED"],
								},
								withRelated: false,
							},
						);
						for (const int of user_interest) {
							user_interest_list.push(int);
						}
					}

					// all users with attendances in startDate and endDate
					let users_with_attendances = [];
					for (const interest of user_interest_list) {
						const attendances = await this.getAllAttendances(
							ctx,
							interest.id,
							date_start_close,
							end_start_close,
						);
						if (attendances.length > 0) {
							users_with_attendances.push(interest.user_id);
						}
					}

					// remove from list applications without attendances
					for (const user of users_with_attendances) {
						const index = applications.findIndex((item) => item.user_id === user);
						applications.splice(index, 1);
					}
					await this.closeApplications(ctx, applications);
				}
			},
		},

		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true },
			},
			scope: "social_scholarship:applications:list",
			async handler(ctx) {
				let academic_year;
				if (!ctx.params.academic_year) {
					const current_year = await ctx.call(
						"configuration.academic_years.get_current_academic_year",
					);
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}
				let query = `select ap.status, Count(*) from application as ap where ap.academic_year = ? group by ap.status `;
				return this.adapter.raw(query, academic_year).then((response) => {
					let allStatus = {
						SUBMITTED: 0,
						ANALYSED: 0,
						INTERVIEWED: 0,
						ACCEPTED: 0,
						DECLINED: 0,
						CANCELLED: 0,
						EXPIRED: 0,
						DISPATCH: 0,
					};
					response.rows.map((r) => (allStatus[r.status] = r.count));
					return allStatus;
				});
			},
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "social_scholarship:applications:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: APPLICATION_EVENTS,
					optional: true,
				},
				notes: { type: "string", optional: true },
				application: { type: "object", optional: true },
				interviews: {
					type: "array",
					min: 0,
					optional: true,
					items: {
						type: "object",
						props: {
							local: { type: "string" },
							date: { type: "date", convert: true },
							responsable_id: { type: "number", integer: true, convert: true },
							scope: { type: "string", optional: true, nullable: true },
							observations: { type: "string", optional: true, nullable: true },
						},
					},
				},
				interview_reports: {
					type: "array",
					min: 0,
					optional: true,
					items: {
						type: "object",
						props: {
							interview_id: { type: "number", integer: true, convert: true },
							file_id: {
								type: "number",
								integer: true,
								convert: true,
								optional: true,
								nullable: true,
							},
							notes: { type: "string" },
						},
					},
				},
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				// If application is already in INTERVIEWED status and the event is interview there is no need to change application status
				let stateMachine = null;

				if (
					application[0].status !== "INTERVIEWED" ||
					ctx.params.event.toLowerCase() !== "interview"
				) {
					stateMachine = await ApplicationStateMachine.createStateMachine(
						application[0].status,
						ctx,
					);

					// Validate Status
					if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
						throw new ValidationError(
							"Unauthorized application status change",
							"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR",
							{},
						);
					}
				}

				if (application[0].status === "DISPATCH") {
					const validation = new Validator();

					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};

					const check = validation.compile(schema);
					const res = check(ctx.params);

					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;

							if (application[0].decision === "REJECTED") {
								status = "decline";
							}

							if (application[0].decision === "ACCEPTED") {
								status = "accept";
							}

							if (application[0].decision === "CANCELLED") {
								status = "cancel";
							}

							const result = await stateMachine[status]();
							this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].notification_guid,
							);
							this.clearCache();

							return result;
						} else {
							const result = await stateMachine["analyse"]();
							this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].notification_guid,
							);
							this.clearCache();

							return result;
						}
					}
				}

				// Validate interview has info
				if (application[0].status === "INTERVIEWED") {
					if (
						ctx.params.event.toLowerCase() === "cancel" ||
						ctx.params.event.toLowerCase() === "decline"
					) {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].notification_guid,
						);
						this.clearCache();

						return result;
					} else {
						if (ctx.params.event.toLowerCase() !== "interview") {
							const interviews = await ctx.call("social_scholarship.application-interviews.find", {
								query: {
									application_id: application[0].id,
								},
							});

							const interview_info = interviews.filter((inte) => inte.notes === null);

							if (interview_info.length !== 0) {
								if (!ctx.params.interview_reports) {
									throw new ValidationError(
										"Unauthorized application status change, it is necessary to add notes in interview.",
										"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_INFO_ERROR",
										{},
									);
								} else {
									// Make validations in interviews
									for (let item_interview_info of interview_info) {
										let findInterviewInInput = false;

										for (let item_interview_report of ctx.params.interview_reports) {
											if (item_interview_info.id === item_interview_report.interview_id) {
												findInterviewInInput = true;
												break;
											}
										}

										if (findInterviewInInput === false) {
											throw new ValidationError(
												"Unauthorized application status change, it is necessary to add notes in interview.",
												"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_INFO_ERROR",
												{},
											);
										}
									}

									// Make updates in interviews
									for (let item_interview_info of interview_info) {
										for (let item_interview_report of ctx.params.interview_reports) {
											if (item_interview_info.id === item_interview_report.interview_id) {
												item_interview_info.file_id = item_interview_report.file_id;
												item_interview_info.notes = item_interview_report.notes;
												await ctx.call(
													"social_scholarship.application-interviews.update",
													item_interview_info,
												);
												break;
											}
										}
									}
								}
							}
						}
					}
				}

				// Validate is interview
				if (ctx.params.event.toLowerCase() === "interview") {
					if (ctx.params.interviews) {
						let user_id = null;
						let user_name = null;
						let uuid = null;

						// If application is already in INTERVIEWED status there is no need to change application status
						if (application[0].status !== "INTERVIEWED") {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();

							user_id = result[0].user.id;
							user_name = result[0].user.name;
							uuid = result[0].notification_guid;
						} else {
							user_id = ctx.meta.user.id;
							user_name = ctx.meta.user.name;
							uuid = application[0].notification_guid;
						}

						// Create interviews and send interviews notifications
						for (let item_interview of ctx.params.interviews) {
							item_interview.application_id = ctx.params.id;

							await ctx.call("social_scholarship.application-interviews.create", item_interview);
							const responsable = await ctx.call("authorization.users.get", {
								id: item_interview.responsable_id,
							});
							await this.sendNotificationInterview(
								ctx,
								user_name,
								user_id,
								item_interview.local,
								item_interview.date,
								responsable[0].name,
								uuid,
							);
						}

						return this._get(ctx, { id: ctx.params.id });
					} else {
						throw new ValidationError(
							"Unauthorized application status change, it is necessary to schedule an interview.",
							"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_ERROR",
							{},
						);
					}
				}

				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();

					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "CANCELLED"] },
					};

					const check = validation.compile(schema);
					const res = check(ctx.params);

					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].notification_guid,
						);
						this.clearCache();

						return result;
					}
				}

				if (application[0].status === "ACCEPTED") {
					if (ctx.params.event.toLowerCase() === "cancel") {
						const colaborations = await ctx.call(
							"social_scholarship.experience-user-interest.find",
							{
								query: {
									user_id: ctx.meta.user.id,
									status: ["COLABORATION", "WITHDRAWAL"],
								},
								withRelated: false,
							},
						);

						if (colaborations.length > 0) {
							throw new ValidationError(
								"Has active colaborations",
								"SOCIAL_SCHOLARSHIP_HAS_COLABORATIONS",
								{},
							);
						}

						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].notification_guid,
						);
						await this.cancelUserInterests(ctx, application[0].user_id);
						this.clearCache();

						return result;
					}
				}

				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].notification_guid,
				);
				this.clearCache();

				return result;
			},
		},

		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			scope: "social_scholarship:applications:list",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				return stateMachine.transitions();
			},
		},

		"applicant-cancel": {
			rest: "POST /:id/status/application-cancel",
			visibility: "published",
			scope: "social_scholarship:applications:cancel",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				if (application[0].user_id != ctx.meta.user.id) {
					throw new ValidationError(
						"Unauthorized application status change",
						"SOCIAL_SCHOLARSHIP_APPLICATION_UNAUTHORIZED",
						{},
					);
				}

				const colaborations = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: {
						user_id: ctx.meta.user.id,
						status: ["COLABORATION", "WITHDRAWAL"],
					},
					withRelated: false,
				});

				if (colaborations.length > 0) {
					throw new ValidationError(
						"Has active colaborations",
						"SOCIAL_SCHOLARSHIP_HAS_COLABORATIONS",
						{},
					);
				}

				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				// Validate Status
				if (stateMachine.cannot("CANCEL")) {
					throw new ValidationError(
						"Unauthorized application status change",
						"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR",
						{},
					);
				}
				const result = await stateMachine["cancel"]();
				await this.sendNotification(
					ctx,
					result[0].status,
					ctx.meta.user.id,
					ctx.meta.user.name,
					result[0].notification_guid,
				);
				await this.cancelUserInterests(ctx, ctx.meta.user.id);
				return result;
			},
		},

		"schedule-interview": {
			rest: "POST /:id/status/applicant-interview",
			visibility: "published",
			scope: "social_scholarship:applications:list",
			params: {
				id: { type: "number", integer: true, convert: true },
				local: { type: "string" },
				date: { type: "date", convert: true },
				responsable_id: { type: "number", integer: true, convert: true },
				observations: { type: "string", nullable: true, optional: true },
				scope: { type: "string" },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				// If application is already in INTERVIEWED status there is no need to change application status
				let user_id = null;
				let user_name = null;
				let uuid = null;

				if (application[0].status !== "INTERVIEWED") {
					let stateMachine = await ApplicationStateMachine.createStateMachine(
						application[0].status,
						ctx,
					);

					if (stateMachine.cannot("INTERVIEW")) {
						throw new ValidationError(
							"Unauthorized application status change",
							"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR",
							{},
						);
					}

					const result = await stateMachine["interview"]();

					user_id = result[0].user.id;
					user_name = result[0].user.name;
					uuid = result[0].notification_guid;
				} else {
					user_id = ctx.meta.user.id;
					user_name = ctx.meta.user.name;
					uuid = application[0].notification_guid;
				}

				const interview = {
					local: ctx.params.local,
					date: ctx.params.date,
					responsable_id: ctx.params.responsable_id,
					application_id: ctx.params.id,
					observations: ctx.params.observations,
					scope: ctx.params.scope,
				};

				await ctx.call("social_scholarship.application-interviews.create", interview);
				const responsable = await ctx.call("authorization.users.get", {
					id: ctx.params.responsable_id,
				});
				await this.sendNotificationInterview(
					ctx,
					user_name,
					user_id,
					ctx.params.local,
					ctx.params.date,
					responsable[0].name,
					uuid,
				);
				this.clearCache();

				return application;
			},
		},

		"add-interview-report": {
			rest: "POST /:interview_id/add-interview-report",
			params: {
				interview_id: { type: "number", integer: true, convert: true },
				file_id: { type: "number", interger: true, convert: true, optional: true, nullable: true },
				notes: { type: "string" },
			},
			scope: "social_scholarship:applications:list",
			visibility: "published",
			async handler(ctx) {
				await ctx.call("social_scholarship.application-interviews.get", {
					id: ctx.params.interview_id,
				});
				return ctx.call("social_scholarship.application-interviews.patch", {
					id: ctx.params.interview_id,
					file_id: ctx.params.file_id,
					notes: ctx.params.notes,
				});
			},
		},

		"close-applications-academic-year": {
			rest: "POST /close-applications",
			scope: "social_scholarship:applications:list",
			params: {
				academic_year: { type: "string" },
			},
			async handler(ctx) {
				let resp = [];
				const applications = await this._find(ctx, {
					query: {
						academic_year: ctx.params.academic_year,
					},
				});
				for (const application of applications) {
					const expired = await this.expireApplication(ctx, application);
					resp.push(expired);
				}
				return resp;
			},
		},

		"generate-report-application": {
			rest: "GET /:id/report",
			visibility: "published",
			scope: "social_scholarship:applications:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				application[0].student_birthdate = moment(application[0].student_birthdate).format(
					"YYYY-MM-DD",
				);
				const data = {
					date: moment().format("YYYY-MM-DD"),
					application: application[0],
				};
				return ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_APPLICATION",
					data: data,
					name: "Informação de uma candidatura" + "(" + data.date + ")",
				});
			},
		},

		"verify-active-application": {
			rest: "GET /user-application",
			visibility: "published",
			scope: "social_scholarship:applications:create",
			async handler(ctx) {
				const application = await ctx.call("social_scholarship.applications.find", {
					query: {
						user_id: ctx.meta.user.id,
						status: ["SUBMITTED", "ANALYSED", "INTERVIEWED", "ACCEPTED", "DISPATCH"],
					},
				});
				if (application.length !== 0) {
					return { id: application[0].id, active: true };
				} else {
					return { id: null, active: false };
				}
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "social_scholarship:applications:update",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				if (application[0].last_status !== null) {
					const result = await ctx.call("social_scholarship.applications.patch", {
						id: ctx.params.id,
						status: application[0].last_status,
						last_status: application[0].status,
					});
					await ctx.call("social_scholarship.applicationHistory.create", {
						application_id: ctx.params.id,
						status: application[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].notification_guid,
					);
					return result;
				} else {
					const result = await ctx.call("social_scholarship.applications.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: application[0].status,
					});
					await ctx.call("social_scholarship.applicationHistory.create", {
						application_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].notification_guid,
					);
					return result;
				}
			},
		},

		get_users_with_application_approved: {
			//rest: "GET /get-users-with-application-approved",
			//visibility: "published",
			visibility: "public",
			params: {},
			async handler(ctx) {
				return this.adapter.find({
					query: (q) => {
						q.distinct("user_id as id");
						q.where("status", "=", "ACCEPTED");

						return q;
					},
					withRelated: false,
				});
			},
		},

		get_application_users_for_interest: {
			//rest: "GET /get-application-users-for-interest",
			//visibility: "published",
			visibility: "public",
			params: {
				school_id: { type: "number", integer: true, convert: true, optional: true },
				course_id: { type: "number", integer: true, convert: true, optional: true },
				course_year: { type: "number", integer: true, convert: true, optional: true },
				without_active_collaborations: { type: "boolean", convert: true, optional: true },
				academic_year: { type: "string" },
			},
			async handler(ctx) {
				const applications = await this._find(ctx, {
					withRelated: ["user"],
					query: (qb) => {
						qb.distinct("user_id");
						qb.where("status", "=", "ACCEPTED");
						qb.where("academic_year", "=", ctx.params.academic_year);

						if (ctx.params.school_id) {
							qb.where("school_id", "=", ctx.params.school_id);
						}
						if (ctx.params.course_id) {
							qb.where("course_id", "=", ctx.params.course_id);
						}
						if (ctx.params.course_year) {
							qb.where("course_year", "=", ctx.params.course_year);
						}

						if (
							ctx.params.without_active_collaborations &&
							ctx.params.without_active_collaborations === true
						) {
							qb.whereNotIn("user_id", (qb) =>
								qb
									.from("experience_user_interest")
									.distinct("user_id")
									.whereIn("status", ["COLABORATION", "WITHDRAWAL"]),
							);
						}
					},
				});

				let resp = [];
				for (const application of applications) {
					if (application.user !== null) {
						resp.push(application.user);
					}
				}

				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async closeApplications(ctx, applications) {
			for (const application of applications) {
				await ctx.call("social_scholarship.applications.patch", {
					id: application.id,
					status: "EXPIRED",
				});
			}
		},

		async expireApplication(ctx, application) {
			return ctx.call("social_scholarship.applications.patch", {
				id: application.id,
				status: "EXPIRED",
			});
		},

		async validateStatus(ctx) {
			if (!ctx.params.id) {
				ctx.params.notification_guid = UUIDV4();
				//ctx.params.status = "DRAFTED";
				if (!ctx.meta.isBackoffice && !ctx.params.student_draft) {
					ctx.params.status = "SUBMITTED";
				}
			}
		},
		async parseJsonFields(ctx, res) {
			if (ctx.params.language_skills && Array.isArray(ctx.params.language_skills)) {
				ctx.params.language_skills = JSON.stringify(ctx.params.language_skills);
			}
			return res;
		},
		async saveSchedule(ctx, res) {
			if (ctx.params.preferred_schedule_ids) {
				res[0].preferred_schedule = await this.createWithRelatedSchedules(
					ctx,
					"social_scholarship.application_schedules.create",
					res[0].id,
					ctx.params.preferred_schedule_ids,
				);
			}
			return res;
		},
		async savePreferredActivities(ctx, res) {
			if (ctx.params.preferred_activities_ids) {
				res[0].preferred_activities = await this.createWithRelatedActivities(
					ctx,
					"social_scholarship.application_activities.create",
					res[0].id,
					ctx.params.preferred_activities_ids,
				);
			}
			return res;
		},
		async saveOrganicUnits(ctx, res) {
			if (ctx.params.organic_units_ids) {
				res[0].organic_units = await this.createWithRelatedOrganicUnits(
					ctx,
					"social_scholarship.application_organic_units.create",
					res[0].id,
					ctx.params.organic_units_ids,
				);
			}
			return res;
		},
		async saveApplicationFiles(ctx, response) {
			if (Array.isArray(ctx.params.file_ids)) {
				const file_ids = [...new Set(ctx.params.file_ids)];
				await ctx.call("social_scholarship.application_files.save_application_files", {
					application_id: response[0].id,
					file_ids,
				});
				response[0].files = await ctx.call("social_scholarship.application_files.find", {
					query: {
						application_id: response[0].id,
					},
				});
			}
			return response;
		},
		async sendNotification(ctx, status, user_id, name, notification_gui) {
			// Send notification to user
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_APPLICATION_STATUS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name },
				variables: {},
				external_uuid: notification_gui,
				medias: [],
			});

			// Services responsibles are notified whenever there are changes caused by students who require action from the services
			if (
				status.toUpperCase() === "SUBMITTED" ||
				status.toUpperCase() === "ACCEPTED" ||
				status.toUpperCase() === "CANCELLED"
			) {
				const services_responsables_email = await ctx.call(
					"social_scholarship.configurations.find",
					{
						query: {
							key: CONFIGURATION_KEYS.SERVICES_RESPONSABLES_NOTIFICATION_EMAIL,
						},
					},
				);

				await ctx.call("notifications.alerts.create_alert", {
					alert_type_key: "SOCIAL_SUPPORT_APPLICATION_STATUS_" + status.toUpperCase(),
					user_id: user_id,
					user_data: { email: services_responsables_email[0].value },
					data: { name: name },
					variables: {},
					external_uuid: notification_gui,
					medias: [],
				});
			}

			// Emails configured in APPLICATION_EMAILS are notified whenever that an application is submitted
			if (status.toUpperCase() === "SUBMITTED") {
				ctx
					.call("social_scholarship.configurations.find", {
						query: {
							key: CONFIGURATION_KEYS.APPLICATION_EMAILS,
						},
					})
					.then((response) => {
						if (response.length) {
							response.map((user_id) => {
								ctx
									.call("authorization.users.get", {
										id: user_id,
									})
									.then((user) => {
										if (user.length) {
											ctx.call("notifications.alerts.create_alert", {
												alert_type_key: "SOCIAL_SUPPORT_NEW_APPLICATION_SUBMITTED",
												user_id: user_id,
												user_data: { email: user[0].email },
												data: { name: name },
												variables: {},
												external_uuid: notification_gui,
												medias: [],
											});
										}
									});
							});
						}
					});
			}
		},

		/**
		 * Function for get
		 * @param {*} ctx
		 * @param {*} application_id
		 * @returns
		 */
		async getUserApplications(ctx, user_id) {
			const applications = await this._find(ctx, {
				query: {
					user_id: user_id,
				},
				withRelated: false,
			});
			return applications.map((app) => {
				const container = {};
				container.academic_year = app.academic_year;
				container.id = app.id;
				container.user_id = app.user_id;
				return container;
			});
		},

		async getUserExperiences(ctx, user_ids) {
			const user_manifest = await ctx.call("social_scholarship.experience-user-interest.find", {
				query: {
					user_id: user_ids,
				},
				withRelated: ["experience"],
			});
			return user_manifest.map((user_interest) => {
				const container = {};
				container.academic_year = user_interest.experience.academic_year;
				container.user_manifest_id = user_interest.id;
				container.user_id = user_interest.user_id;
				container.experience_translations = user_interest.experience.translations;
				return container;
			});
		},

		/**
		 * Validate existe application active
		 * @param {*} ctx
		 */
		async validateActiveApplication(ctx) {
			const application = await this._find(ctx, {
				query: {
					academic_year: ctx.params.academic_year,
					user_id: ctx.meta.user.id,
					status: ["ACCEPTED", "SUBMITTED", "INTERVIEWED"],
				},
			});
			if (application.length !== 0) {
				throw new ValidationError(
					"You have a active application",
					"SOCIAL_SCHOLARSHIP_APPLICATION_HAS_ACTIVE_APLICATION",
					{},
				);
			}
		},

		/**
		 * Send notification to schedule interview
		 * @param {*} ctx
		 * @param {*} name
		 * @param {*} user_id
		 * @param {*} local
		 * @param {*} date
		 * @param {*} responsable
		 * @param {*} notification_gui
		 */
		async sendNotificationInterview(
			ctx,
			name,
			user_id,
			local,
			date,
			responsable,
			notification_gui,
		) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_APPLICATION_SCHEDULE_INTERVIEW",
				user_id: user_id,
				user_data: {},
				data: { name: name, local: local, date: date, responsable: responsable },
				variables: {},
				external_uuid: notification_gui,
				medias: [],
			});
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedActivities(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const applicationActivity = {
					application_id: application_id,
					activity_id: id,
				};
				const created = await ctx.call(service, applicationActivity);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		async saveCreateHistory(ctx, response) {
			response[0].history = [
				...(await ctx.call("social_scholarship.applicationHistory.create", {
					application_id: response[0].id,
					status: response[0].status,
					user_id: ctx.meta.user.id,
					notes: null,
				})),
			];
			return response;
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedSchedules(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const applicationActivity = {
					application_id: application_id,
					schedule_id: id,
				};
				const created = await ctx.call(service, applicationActivity);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedOrganicUnits(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const organic_unit = {
					application_id: application_id,
					organic_unit_id: id,
				};
				const created = await ctx.call(service, organic_unit);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) {
				return;
			}
			for (const item of list) {
				await ctx.call(service, item);
			}
		},

		async updateWithRelateds(ctx, response) {
			if (ctx.params.preferred_schedule_ids) {
				await this.deleteWithRelated(
					ctx,
					"social_scholarship.application_schedules.remove",
					await ctx.call("social_scholarship.application_schedules.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].preferred_schedule = await this.createWithRelatedSchedules(
					ctx,
					"social_scholarship.application_schedules.create",
					response[0].id,
					ctx.params.preferred_schedule_ids,
				);
			}
			if (ctx.params.preferred_activities_ids) {
				await this.deleteWithRelated(
					ctx,
					"social_scholarship.application_activities.remove",
					await ctx.call("social_scholarship.application_activities.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].preferred_activities = await this.createWithRelatedActivities(
					ctx,
					"social_scholarship.application_activities.create",
					response[0].id,
					ctx.params.preferred_activities_ids,
				);
			}
			if (ctx.params.organic_units_ids) {
				await this.deleteWithRelated(
					ctx,
					"social_scholarship.application_organic_units.remove",
					await ctx.call("social_scholarship.application_organic_units.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].organic_units = await this.createWithRelatedOrganicUnits(
					ctx,
					"social_scholarship.application_organic_units.create",
					response[0].id,
					ctx.params.organic_units_ids,
				);
			}
			return response;
		},

		async getAllAttendances(ctx, user_interest_id, start_date, end_date) {
			return ctx.call("social_scholarship.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: start_date,
				status: ["ACCEPTED", "REJECTED", "PENDING"],
				end_date: moment(end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},

		async cancelUserInterests(ctx, user_id) {
			const users_interests = await ctx.call("social_scholarship.experience-user-interest.find", {
				query: {
					user_id: user_id,
					status: [
						"SUBMITTED",
						"ANALYSED",
						"INTERVIEWED",
						"APPROVED",
						"WAITING",
						"ACCEPTED",
						"DECLINED",
					],
				},
				withRelated: false,
			});
			for (const user_interest of users_interests) {
				await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: user_interest.id,
					status: "CANCELLED",
				});
				await ctx.call("social_scholarship.user-interest-historic.create", {
					user_interest_id: user_interest.id,
					status: "CANCELLED",
					user_id: user_interest.user_id,
				});
			}
		},

		async acceptedApplicationStatusAutoChange(ctx, response) {
			let configurations = await ctx.call("social_scholarship.configurations.find", {
				query: {
					key: "ACCEPTED_APPLICATION_STATUS_AUTO_CHANGE",
				},
				withRelated: false,
			});

			if (configurations.length > 0 && configurations[0].value === "true") {
				await ctx.call("social_scholarship.applications.status", {
					id: response[0].id,
					event: "ACCEPT",
					decision_dispatch: "ACCEPT",
					notes: "Candidatura aceite automaticamente",
				});
			}

			return response;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
