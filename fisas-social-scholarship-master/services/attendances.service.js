"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const Validator = require("fastest-validator");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.attendances",
	table: "experience_attendance",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "attendances")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"date",
			"was_present",
			"notes",
			"n_hours",
			"file_id",
			"created_at",
			"updated_at",
			"initial_time",
			"final_time",
			"status",
			"n_hours_reject",
			"reject_reason",
			"executed_service",
			"real_initial_time",
			"real_final_time",
		],
		defaultWithRelateds: ["file", "absence_reason"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			user_interest(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"social_scholarship.experience-user-interest",
					"user_interest",
					"user_interest_id",
				);
			},
			absence_reason(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.absence_reason",
					"absence_reason",
					"id",
					"experience_attendance_id",
				);
			},
		},
		entityValidator: {
			user_interest_id: "number|integer|convert", //<-- REVIEW to withRelated
			date: "date|convert",
			was_present: "boolean",
			notes: "string|optional",
			n_hours: "string|optional",
			created_at: "date|convert",
			updated_at: "date|convert",
			initial_time: "date|convert|optional",
			final_time: "date|convert|optional",
			executed_service: "string|optional",
			status: { type: "enum", values: ["PENDING", "ACCEPTED", "REJECTED", "LACK"] },
			n_hours_reject: { type: "string", nullable: true, optional: true },
			reject_reason: { type: "string", optional: true, nullable: true },
			real_initial_time: "date|convert|optional",
			real_final_time: "date|convert|optional",
		},
	},
	hooks: {
		before: {
			create: [
				"validateExperience",
				"validateUserExperiences",
				function sanatizeParams(ctx) {
					ctx.params.date = new Date();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.n_hours = "0";
					if (ctx.params.was_present) {
						ctx.params.status = "PENDING";
						ctx.params.n_hours = this.calculateNHours(
							ctx.params.initial_time,
							ctx.params.final_time,
						);
					} else {
						ctx.params.status = "LACK";
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.n_hours = 0;
					if (ctx.params.was_present) {
						ctx.params.n_hours = this.calculateNHours(
							ctx.params.initial_time,
							ctx.params.final_time,
						);
					}
				},
			],
			patch: [
				//"validateUserExperiences",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					if (ctx.params.final_time || ctx.params.initial_time)
						ctx.params.n_hours = this.calculateNHours(
							ctx.params.initial_time,
							ctx.params.final_time,
						);
				},
			],
			list: [
				async function validateAccess(ctx) {
					if (!ctx.meta.isBackoffice) {
						if (ctx.params.query) {
							if (ctx.params.query.user_interest_id) {
								const user_interest = await ctx.call(
									"social_scholarship.experience-user-interest.get",
									{ id: ctx.params.query.user_interest_id, withRelated: "experience" },
								);
								if (
									user_interest[0].user_id !== ctx.meta.user.id &&
									user_interest[0].experience.experience_advisor_id !== ctx.meta.user.id &&
									user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id
								) {
									throw new Errors.ForbiddenError(
										"Unauthorized access to data",
										"SOCIAL_SCHOLARSHIP_ATTENDANCE_FORBIDDEN",
										{},
									);
								}
							}
							if (!ctx.params.query.user_interest_id) {
								throw new Errors.ForbiddenError(
									"Unauthorized access to data",
									"SOCIAL_SCHOLARSHIP_ATTENDANCE_FORBIDDEN",
									{},
								);
							}
						}
						if (!ctx.params.query) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_ATTENDANCE_FORBIDDEN",
								{},
							);
						}
					}
				},
			],
		},
		after: {
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						const user_interest = await ctx.call(
							"social_scholarship.experience-user-interest.get",
							{ id: resp[0].user_interest_id, withRelated: "experience" },
						);
						if (
							user_interest[0].user_id !== ctx.meta.user.id &&
							user_interest[0].experience.experience_advisor_id !== ctx.meta.user.id &&
							user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id
						) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_ATTENDANCE_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "#isBackoffice", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		attendancesBetweenDates: {
			params: {
				user_interest_id: "number|integer|convert",
				was_present: "boolean",
				status: {
					type: "array",
					items: {
						type: "string",
					},
				},
				start_date: "date|convert",
				end_date: "date|convert",
			},
			async handler(ctx) {
				return this.findAttendancesBetweenDates(ctx);
			},
		},

		status: {
			rest: "POST /:id/change-status",
			visibility: "published",
			scope: "social_scholarship:attendances:status",
			params: {
				id: { type: "number", integer: true, convert: true },
				status: { type: "enum", values: ["ACCEPTED", "REJECTED"] },
				initial_time: { type: "date", convert: true, optional: true },
				final_time: { type: "date", convert: true, optional: true },
				reject_reason: { type: "string", convert: true, optional: true, nullable: true },
			},
			async handler(ctx) {
				const attendance = await this._get(ctx, { id: ctx.params.id });

				const colaboration = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: attendance[0].user_interest_id,
					withRelated: "experience",
				});

				if (!ctx.meta.isBackoffice) {
					if (
						colaboration[0].experience.experience_responsible_id !== ctx.meta.user.id &&
						colaboration[0].experience.experience_advisor_id !== ctx.meta.user.id
					) {
						throw new Errors.ForbiddenError(
							"This collaboration does not correspond to your experiences",
							"SOCIAL_SCHOLARSHIP_ATTENDANCE_CHANGE_STATUS_FORBIDDEN",
							{},
						);
					}
				}

				if (ctx.params.status === "REJECTED") {
					const validation = new Validator();

					const schema = {
						initial_time: { type: "date" },
						final_time: { type: "date" },
						reject_reason: { type: "string" },
					};

					const check = validation.compile(schema);
					const res = check(ctx.params);

					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const n_hours = this.calculateNHours(ctx.params.initial_time, ctx.params.final_time);

						return ctx.call("social_scholarship.attendances.patch", {
							id: ctx.params.id,
							real_initial_time: ctx.params.initial_time,
							real_final_time: ctx.params.final_time,
							n_hours,
							n_hours_reject: Math.abs(attendance[0].n_hours - n_hours).toString(),
							reject_reason: ctx.params.reject_reason,
							status: "REJECTED",
						});
					}
				} else {
					return ctx.call("social_scholarship.attendances.patch", {
						id: ctx.params.id,
						status: "ACCEPTED",
					});
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"social_scholarship.absence_reason.*"(ctx) {
			this.logger.info("CAPTURED EVENT => social_scholarship.absence_reason.*");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		findAttendancesBetweenDates(ctx) {
			return this.adapter.find({
				query: (qb) => {
					qb.where("initial_time", ">=", ctx.params.start_date);
					qb.where("final_time", "<=", ctx.params.end_date);
					qb.where("was_present", "=", ctx.params.was_present);
					qb.where("user_interest_id", "=", ctx.params.user_interest_id);
					qb.where("status", "IN", ctx.params.status);
					return qb;
				},
			});
		},

		async validateUserExperiences(ctx) {
			let experience_user_interest = null;
			if (ctx.params.user_interest_id) {
				experience_user_interest = ctx.params.user_interest_id;
			} else if (ctx.params.id && !ctx.params.user_interest_id) {
				experience_user_interest = await this._get(ctx, { id: ctx.params.id }).then((attendance) =>
					attendance.length ? attendance[0].user_interest_id : null,
				);
			}
			return ctx
				.call("social_scholarship.experience-user-interest.get", {
					id: experience_user_interest,
				})
				.then((experience_user_interest) => {
					if (experience_user_interest.length) {
						return ctx.call("social_scholarship.experience-user-interest.find", {
							query: {
								user_id: ctx.meta.isBackoffice
									? experience_user_interest[0].user_id
									: ctx.meta.user.id,
							},
							withRelated: false,
						});
					}
				})
				.then((all_experiences) => {
					return this._find(ctx, {
						query: {
							user_interest_id: all_experiences.map((e) => e.id),
						},
						withRelated: false,
					});
				})
				.then((all_attendances) => {
					if (all_attendances.length) {
						let checkAttendances = [];
						all_attendances.forEach((attendance) => {
							if (attendance.status !== "REJECTED") {
								if (
									moment(ctx.params.initial_time).isBetween(
										moment(attendance.initial_time),
										moment(attendance.final_time),
										null,
										"[]",
									) ||
									moment(ctx.params.final_time).isBetween(
										moment(attendance.initial_time),
										moment(attendance.final_time),
										null,
										"[]",
									)
								) {
									checkAttendances.push(attendance);
								} else if (
									moment(attendance.initial_time).isBetween(
										moment(ctx.params.initial_time),
										moment(ctx.params.final_time),
										null,
										"[]",
									) ||
									moment(attendance.final_time).isBetween(
										moment(ctx.params.initial_time),
										moment(ctx.params.final_time),
										null,
										"[]",
									)
								) {
									checkAttendances.push(attendance);
								}
							}
						});
						if (ctx.params.id) {
							checkAttendances = checkAttendances.filter(
								(checked) => checked.id !== parseInt(ctx.params.id),
							);
						}
						if (checkAttendances.length) {
							throw new Errors.ValidationError(
								"You have an open attendances for the same period",
								"SOCIAL_SCHOLARSHIP_EXPERIENCES_SAME_PERIOD",
								{},
							);
						}
					}
				});
		},

		async validateExperience(ctx) {
			const manifest_interest = await ctx.call("social_scholarship.experience-user-interest.get", {
				id: ctx.params.user_interest_id,
			});
			if (!ctx.meta.isBackoffice) {
				if (manifest_interest[0].user_id !== ctx.meta.user.id) {
					throw new Errors.ForbiddenError(
						"This collaboration does not correspond to you",
						"SOCIAL_SCHOLARSHIP_ATTENDANCE_CREATE_FORBIDDEN",
						{},
					);
				}
			}
			const experience = await ctx.call("social_scholarship.experiences.get", {
				id: manifest_interest[0].experience_id,
			});
			if (
				new Date(experience[0].start_date).getTime() > new Date(ctx.params.date).getTime() ||
				new Date(experience[0].end_date).getTime() < new Date(ctx.params.date).getTime()
			) {
				throw new Errors.ValidationError(
					"Experience already finish or not started yet",
					"SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE",
					{},
				);
			}
		},

		calculateNHours(initial_time, final_time) {
			if (!final_time || !initial_time) {
				return 0;
			}

			if (new Date(final_time).getTime() <= new Date(initial_time).getTime()) {
				throw new Errors.ValidationError(
					"Initial time cannot be bigger that final time",
					"SOCIAL_SCHOLARSHIP_ATTENDANCE_INVALID_HOURS",
					{},
				);
			}

			if (final_time && initial_time) {
				let end = moment(final_time);
				let startTime = moment(initial_time);
				let duration = moment.duration(end.diff(startTime));
				let minutes = duration.minutes();

				if (minutes.toString().length <= 1) {
					minutes = "0" + duration.minutes();
				}

				return Math.round(duration.hours()) + "." + minutes;
			}

			return 0;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
