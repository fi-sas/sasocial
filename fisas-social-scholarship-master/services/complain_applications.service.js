"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.complain-applications",
	table: "complain_application",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "complain-applications")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"complain",
			"user_id",
			"file_id",
			"response",
			"status",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.applications", "application", "application_id");
			},
			user(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			application_id: { type: "number", integer: true, convert: true },
			complain: { type: "string" },
			user_id: { type: "number", integer: true, convert: true, nullable: true },
			response: { type: "string", optional: true, nullable: true },
			status: { type: "enum", values : ["SUBMITTED", "ANALYSED", "REPLYED"] },
			file_id: { type: "number", integer: true, convert: true, optional: true, nullable: true }
		}
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "SUBMITTED";
					await ctx.call("social_scholarship.applications.get", { id: ctx.params.application_id });
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},

		analyse : {
			rest: "POST /:id/analyse",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			scope: "social_scholarship:complain-applications:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0].status !== "REPLYED"){
					complain[0].status = "ANALYSED";
					return await this._update(ctx, complain[0]);
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"SOCIAL_SHOLARSHIP_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		},

		response : {
			rest: "POST /:id/response",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				response: { type: "string" }
			},
			scope: "social_scholarship:complain-applications:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0].status !== "REPLYED"){
					complain[0].status = "REPLYED";
					complain[0].response = ctx.params.response;
					const updated = await this._update(ctx, complain[0]);

					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "SOCIAL_SUPPORT_COMPLAIN",
						user_id: complain[0].user.id,
						user_data: {},
						data: { name: complain[0].user.name, response : ctx.params.response },
						variables: {},
						external_uuid: null,
						medias: [],
					});

					return updated;
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"SOCIAL_SHOLARSHIP_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
