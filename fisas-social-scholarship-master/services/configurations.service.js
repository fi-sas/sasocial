"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { CONFIGURATION_KEYS } = require("./utils/constants.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			list: [
				function sanatizeParams(ctx) {
					// SET THE LIMIT 100 FOR FRONTEND RECEIVE ALL CONFIGS
					ctx.params.limit = 100;
				},
			],
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});
				return result;
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			params: {
				ADMIN_NOTIFICATION_EMAIL: { type: "string", optional: true },
				FINANCIAL_NOTIFICATION_EMAIL: { type: "string", optional: true },
				EXTERNAL_ENTITY_PROFILE_ID: { type: "number", convert: true, optional: true },
				MAX_HOURS_WORK: { type: "number", convert: true, optional: true },
				TIME_CLOSE_APPLICATIONS: { type: "number", convert: true, optional: true },
				CURRENT_ACCOUNT: { type: "number", convert: true, optional: true },
				ASSIGNED_STATUS_AUTO_CHANGE: { type: "boolean", optional: true },
				STATUS_AUTO_CHANGE_DAYS: { type: "number", convert: true, optional: true },
				ACCEPTED_APPLICATION_STATUS_AUTO_CHANGE: { type: "boolean", optional: true },
				HAS_CURRENT_ACCOUNT: { type: "boolean", optional: true },
				RESPONSABLE_PROFILE: {
					type: "array",
					optional: true,
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
				EXPERIENCE_EMAILS: {
					type: "array",
					optional: true,
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
				APPLICATION_EMAILS: {
					type: "array",
					optional: true,
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
				USER_INTEREST_EMAILS: {
					type: "array",
					optional: true,
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true,
					},
				},
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key: key } }).then((config) => {
						if (config.length > 0 && ctx.params[config[0].key] != null) {
							return this._update(
								ctx,
								{ id: config[0].id, value: JSON.stringify(ctx.params[config[0].key]) },
								true,
							);
						}
					});
				});
				return Promise.all(promisses).then(() =>
					ctx.call("social_scholarship.configurations.list", {}),
				);
			},
		},
		get: {
			// REST: GET /:id
			visibility: "protected",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "protected",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "protected",
		},
		getResponsableProfile: {
			visibility: "published",
			rest: "GET /responsable-profile",
			scope: "social_scholarship:configurations:read",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: CONFIGURATION_KEYS.RESPONSABLE_PROFILE,
					},
				});
			},
		},
		/*
		setResponsabelProfile: {
			visibility: "published",
			rest: "POST /responsable-profile",
			scope: "social_scholarship:configurations:update",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "responsable_profile",
					},
				}).then(async (config) => {
					if (config.length) {
						// UPDATE
						return await ctx.call(`${this.name}.update`, {
							id: config[0].id,
							key: "responsable_profile",
							value: ctx.params.value.toString(),
						});
					} else {
						//INSERT
						return await ctx.call(`${this.name}.create`, {
							key: "responsable_profile",
							value: ctx.params.value.toString(),
						});
					}
				});
			},
		},

		setExperienceEmails: {
			visibility: "published",
			rest: "POST /experience_emails",
			scope: "social_scholarship:configurations:update",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "EXPERIENCE_EMAILS",
					},
				}).then(async (config) => {
					if (config.length) {
						// UPDATE
						return await ctx.call(`${this.name}.update`, {
							id: config[0].id,
							key: "EXPERIENCE_EMAILS",
							value: ctx.params.value.toString(),
						});
					} else {
						//INSERT
						return await ctx.call(`${this.name}.create`, {
							key: "EXPERIENCE_EMAILS",
							value: ctx.params.value.toString(),
						});
					}
				});
			},
		},

		getExperienceEmails: {
			visibility: "published",
			rest: "GET /experience_emails",
			scope: "social_scholarship:configurations:read",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "EXPERIENCE_EMAILS",
					},
				});
			},
		},

		setApplicationEmails: {
			visibility: "published",
			rest: "POST /application_emails",
			scope: "social_scholarship:configurations:update",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "APPLICATION_EMAILS",
					},
				}).then(async (config) => {
					if (config.length) {
						// UPDATE
						return await ctx.call(`${this.name}.update`, {
							id: config[0].id,
							key: "APPLICATION_EMAILS",
							value: ctx.params.value.toString(),
						});
					} else {
						//INSERT
						return await ctx.call(`${this.name}.create`, {
							key: "APPLICATION_EMAILS",
							value: ctx.params.value.toString(),
						});
					}
				});
			},
		},

		getApplicationEmails: {
			visibility: "published",
			rest: "GET /application_emails",
			scope: "social_scholarship:configurations:read",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "APPLICATION_EMAILS",
					},
				});
			},
		},

		setUserInterestEmails: {
			visibility: "published",
			rest: "POST /user_interest_emails",
			scope: "social_scholarship:configurations:update",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "USER_INTEREST_EMAILS",
					},
				}).then(async (config) => {
					if (config.length) {
						// UPDATE
						return await ctx.call(`${this.name}.update`, {
							id: config[0].id,
							key: "USER_INTEREST_EMAILS",
							value: ctx.params.value.toString(),
						});
					} else {
						//INSERT
						return await ctx.call(`${this.name}.create`, {
							key: "USER_INTEREST_EMAILS",
							value: ctx.params.value.toString(),
						});
					}
				});
			},
		},

		getUserInterestEmails: {
			visibility: "published",
			rest: "GET /user_interest_emails",
			scope: "social_scholarship:configurations:read",
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "USER_INTEREST_EMAILS",
					},
				});
			},
		},*/
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
