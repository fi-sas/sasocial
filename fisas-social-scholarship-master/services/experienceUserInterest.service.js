"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError, ForbiddenError } = require("@fisas/ms_core").Helpers.Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const {
	USER_INTEREST_STATUS,
	USER_INTEREST_EVENTS,
	CONFIGURATION_KEYS,
} = require("./utils/constants.js");
const UserInterestStateMachine = require("./state-machines/user_interest.machine");
const UUIDV4 = require("uuid").v4;
const moment = require("moment");
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.experience-user-interest",
	table: "experience_user_interest",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "experience-user-interest")],

	/*
	 * Crons
	 */
	crons: [
		{
			name: "automaticChangeAssignedStatusAfterXDays",
			cronTime: "0 * * * *",
			onTick: function () {
				this.logger.info("cron expire user interests!");
				this.getLocalService("social_scholarship.experience-user-interest")
					.actions.automatic_change_status()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"experience_id",
			"user_id",
			"status",
			"contract_file_id",
			"report_avaliation",
			"certificate_file_id",
			"student_avaliation",
			"student_file_avaliation",
			"certificate_generated_id",
			"uuid",
			"last_status",
			"updated_at",
			"created_at",
			"decision",
			"reject_reason",
		],
		defaultWithRelateds: [
			"user",
			"history",
			"contract_file",
			"certificate_file",
			"certificate_generated",
		],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.user-interest-historic",
					"history",
					"id",
					"user_interest_id",
				);
			},
			contract_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "contract_file", "contract_file_id");
			},
			certificate_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "certificate_file", "certificate_file_id");
			},
			report_avaliation_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "report_avaliation_file", "report_avaliation");
			},
			experience(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "social_scholarship.experiences", "experience", "experience_id");
			},
			monthly_reports(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.monthly-reports",
					"monthly_reports",
					"id",
					"user_interest_id",
				);
			},
			notifications(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "notifications.alerts", "notifications", "uuid", "external_uuid");
			},
			attendances(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.attendances",
					"attendances",
					"id",
					"user_interest_id",
				);
			},
			payment_grid(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.payment-grid",
					"payment_grid",
					"id",
					"user_interest_id",
				);
			},
			interview(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.user-interest-interviews",
					"interview",
					"id",
					"user_interest_id",
				);
			},
			historic_applications(ids, experiences, rule, ctx) {
				const user_ids = experiences.map((app) => app.user_id);
				return this.getUserApplications(ctx, user_ids).then((historycs) => {
					experiences.forEach((experience) => {
						experience.historic_applications = historycs.filter(
							(historyc) => historyc.user_id === experience.user_id,
						);
					});
				});
			},
			historic_colaborations(ids, experiences, rule, ctx) {
				const user_ids = experiences.map((app) => app.user_id);
				return this.getUserExperiences(ctx, user_ids).then((historycs) => {
					experiences.forEach((experience) => {
						experience.historic_colaborations = historycs.filter(
							(historyc) => historyc.user_id === experience.user_id,
						);
					});
				});
			},
			application(ids, experiences, rule, ctx) {
				return Promise.all(
					experiences.map((experience) => {
						return this.getCurrentApplication(
							ctx,
							experience.experience_id,
							experience.user_id,
						).then((application) => {
							experience.application = application;
						});
					}),
				);
			},
			certificate_generated(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"reports.reports",
					"certificate_generated",
					"certificate_generated_id",
				);
			},
		},
		entityValidator: {
			experience_id: "number|integer|convert|optional",
			user_id: "number|integer|convert|optional",
			status: { type: "enum", values: Object.keys(USER_INTEREST_STATUS) },
			contract_file_id: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			certificate_file_id: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			uuid: { type: "string" },
			student_avaliation: { type: "string", optional: true, nullable: true },
			student_file_avaliation: { type: "number", convert: true, optional: true, nullable: true },
			report_avaliation: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			last_status: {
				type: "enum",
				values: Object.keys(USER_INTEREST_STATUS),
				optional: true,
				nullable: true,
			},
			certificate_generated_id: { type: "number", integer: true, optional: true, nullable: true },
			updated_at: "date|convert",
			created_at: "date|convert",
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "NOT_SELECTED", "WAITING"],
				optional: true,
				nullable: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateUserInterestExperience",
				function sanatizeParams(ctx) {
					ctx.params.uuid = UUIDV4();
					ctx.params.status = "SUBMITTED";
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice && !ctx.meta.no_validate_user) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query["user_id"] = ctx.meta.user.id;
					}
				},
			],
		},
		after: {
			create: [
				"saveCreateHistory",
				async function sanatizeParams(ctx, response) {
					await this.sendNotification(
						ctx,
						"SUBMITTED",
						ctx.meta.user.id,
						ctx.meta.user.name,
						response[0].uuid,
					);
					this.clearCache();

					return response;
				},
			],
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						const experience = await ctx.call("social_scholarship.experiences.get", {
							id: resp[0].experience_id,
							withRelated: false,
						});
						if (
							resp[0].user_id !== ctx.meta.user.id &&
							experience[0].experience_responsible_id !== ctx.meta.user.id &&
							experience[0].experience_advisor_id !== ctx.meta.user.id
						) {
							throw new ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_USER_INTEREST_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#user.id",
					"#no_validate_user",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#isBackoffice", "#user.id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		list_interest_experience: {
			rest: "GET /experience/:experience_id",
			visibility: "published",
			params: {
				experience_id: { type: "number", integer: true, convert: true },
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			scope: "social_scholarship:experience-user-interest:list",
			async handler(ctx) {
				const experience = await ctx.call("social_scholarship.experiences.get", {
					id: ctx.params.experience_id,
				});
				if (
					experience[0].experience_responsible_id !== ctx.meta.user.id &&
					experience[0].experience_advisor_id !== ctx.meta.user.id
				) {
					throw new ForbiddenError(
						"Unauthorized access to experience, not advisor or responsible",
						"SOCIAL_SCHOLARSHIP_USER_INTEREST_FORBIDDEN",
						{},
					);
				} else {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["experience_id"] = ctx.params.experience_id;
					delete ctx.params.experience_id;
					ctx.meta.no_validate_user = true;
					return ctx.call("social_scholarship.experience-user-interest.list", ctx.params);
				}
			},
		},

		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true },
			},
			scope: "social_scholarship:experience-user-interest:list",
			async handler(ctx) {
				let academic_year;
				const current_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
				);
				if (!ctx.params.academic_year) {
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}
				const experience = await ctx.call("social_scholarship.experiences.find", {
					query: {
						academic_year: academic_year,
					},
				});
				let query = `select ex.status, Count(*) from experience_user_interest as ex where ex.experience_id = ANY(?) group by ex.status`;
				return this.adapter.raw(query, [experience.map((ex) => ex.id)]).then((response) => {
					let allStatus = {
						SUBMITTED: 0,
						ANALYSED: 0,
						INTERVIEWED: 0,
						APPROVED: 0,
						WAITING: 0,
						NOT_SELECTED: 0,
						ACCEPTED: 0,
						COLABORATION: 0,
						WITHDRAWAL: 0,
						DECLINED: 0,
						CANCELLED: 0,
						CLOSED: 0,
						DISPATCH: 0,
					};
					response.rows.map((r) => (allStatus[r.status] = r.count));
					return allStatus;
				});
			},
		},
		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "social_scholarship:experience-user-interest:list",
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					user_interest[0].status,
					ctx,
				);
				return await stateMachine.transitions();
			},
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:status",
			params: {
				id: { type: "number", integer: true, convert: true },
				event: { type: "enum", values: USER_INTEREST_EVENTS, optional: true, nullable: true },
				notes: { type: "string", optional: true },
				manifest_interest: { type: "object", optional: true },
				interviews: {
					type: "array",
					min: 0,
					optional: true,
					items: {
						type: "object",
						props: {
							local: { type: "string" },
							date: { type: "date", convert: true },
							responsable_id: { type: "number", integer: true, convert: true },
							scope: { type: "string", optional: true, nullable: true },
							observations: { type: "string", optional: true, nullable: true },
						},
					},
				},
				interview_reports: {
					type: "array",
					min: 0,
					optional: true,
					items: {
						type: "object",
						props: {
							interview_id: { type: "number", integer: true, convert: true },
							file_id: {
								type: "number",
								integer: true,
								convert: true,
								optional: true,
								nullable: true,
							},
							notes: { type: "string" },
						},
					},
				},
			},
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });

				// If user interest is already in INTERVIEWED status and the event is interview there is no need to change user interest status
				let stateMachine = null;

				if (
					user_interest[0].status !== "INTERVIEWED" ||
					ctx.params.event.toLowerCase() !== "interview"
				) {
					stateMachine = await UserInterestStateMachine.createStateMachine(
						user_interest[0].status,
						ctx,
					);

					// Validate Status
					if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
						throw new ValidationError(
							"Unauthorized user interest status change",
							"SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR",
							{},
						);
					}
				}

				if (user_interest[0].status === "DISPATCH") {
					const validation = new Validator();

					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};

					const check = validation.compile(schema);
					const res = check(ctx.params);

					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;

							if (user_interest[0].decision === "REJECTED") {
								status = "decline";
							}

							if (user_interest[0].decision === "ACCEPTED") {
								status = "approve";
							}

							if (user_interest[0].decision === "NOT_SELECTED") {
								status = "notselect";
							}

							if (user_interest[0].decision === "WAITING") {
								status = "waiting";
							}

							const result = await stateMachine[status]();

							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);

							this.clearCache();

							return result;
						} else {
							const result = await stateMachine["analyse"]();

							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);

							this.clearCache();

							return result;
						}
					}
				}

				if (user_interest[0].status === "INTERVIEWED") {
					if (ctx.params.event.toLowerCase() === "cancel") {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						await this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].uuid,
						);

						return result;
					} else {
						if (ctx.params.event.toLowerCase() !== "interview") {
							const interviews = await ctx.call(
								"social_scholarship.user-interest-interviews.find",
								{
									query: {
										user_interest_id: user_interest[0].id,
									},
								},
							);

							const interview_info = interviews.filter((inte) => inte.notes === null);

							if (interview_info.length !== 0) {
								if (!ctx.params.interview_reports) {
									throw new ValidationError(
										"Unauthorized manifest interest status change, it is necessary to add notes or report in interview.",
										"SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_INTERVIEW_INFO_ERROR",
										{},
									);
								} else {
									// Make validations in interviews
									for (let item_interview_info of interview_info) {
										let findInterviewInInput = false;

										for (let item_interview_report of ctx.params.interview_reports) {
											if (item_interview_info.id === item_interview_report.interview_id) {
												findInterviewInInput = true;
												break;
											}
										}

										if (findInterviewInInput === false) {
											throw new ValidationError(
												"Unauthorized manifest interest status change, it is necessary to add notes or report in interview.",
												"SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_INTERVIEW_INFO_ERROR",
												{},
											);
										}
									}

									// Make updates in interviews
									for (let item_interview_info of interview_info) {
										for (let item_interview_report of ctx.params.interview_reports) {
											if (item_interview_info.id === item_interview_report.interview_id) {
												(item_interview_info.file_id = item_interview_report.file_id),
													(item_interview_info.notes = item_interview_report.notes),
													await ctx.call(
														"social_scholarship.user-interest-interviews.update",
														item_interview_info,
													);
												break;
											}
										}
									}
								}
							}
						}
					}
				}

				// Validate create a interview
				if (ctx.params.event.toLowerCase() === "interview") {
					if (ctx.params.interviews) {
						let user_id = null;
						let user_name = null;
						let uuid = null;

						// If user interest is already in INTERVIEWED status there is no need to change user interest status
						if (user_interest[0].status !== "INTERVIEWED") {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();

							user_id = result[0].user.id;
							user_name = result[0].user.name;
							uuid = result[0].uuid;
						} else {
							user_id = ctx.meta.user.id;
							user_name = ctx.meta.user.name;
							uuid = user_interest[0].uuid;
						}

						// Create interviews and send interviews notifications
						for (let item_interview of ctx.params.interviews) {
							item_interview.user_interest_id = ctx.params.id;

							await ctx.call("social_scholarship.user-interest-interviews.create", item_interview);
							const responsable = await ctx.call("authorization.users.get", {
								id: item_interview.responsable_id,
							});
							await this.sendNotificationInterview(
								ctx,
								user_name,
								user_id,
								item_interview.local,
								item_interview.date,
								responsable[0].name,
								uuid,
							);
						}

						return await this._get(ctx, { id: ctx.params.id });
					} else {
						throw new ValidationError(
							"Unauthorized application status change, it is necessary to schedule an interview.",
							"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_INTERVIEW_ERROR",
							{},
						);
					}
				}

				//Validate if exist or send certificate
				if (user_interest[0].status === "COLABORATION") {
					if (ctx.params.event.toLowerCase() === "close") {
						if (
							(user_interest[0].certificate_file_id !== null ||
								user_interest[0].certificate_generated_id !== null) &&
							user_interest[0].report_avaliation !== null
						) {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);
							return result;
						} else {
							if (
								(user_interest[0].certificate_file_id !== null ||
									user_interest[0].certificate_generated_id !== null) &&
								user_interest[0].report_avaliation === null
							) {
								if (ctx.params.manifest_interest) {
									if (
										ctx.params.manifest_interest.report_avaliation &&
										ctx.params.manifest_interest.report_avaliation !== null
									) {
										const result = await stateMachine[ctx.params.event.toLowerCase()]();
										await this.sendNotification(
											ctx,
											result[0].status,
											result[0].user.id,
											result[0].user.name,
											result[0].uuid,
										);
										return result;
									} else {
										throw new ValidationError(
											"Need add report avaliation file",
											"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_REPORT_ERROR",
											{},
										);
									}
								} else {
									throw new ValidationError(
										"Need add report avaliation file",
										"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_REPORT_ERROR",
										{},
									);
								}
							}

							if (
								user_interest[0].certificate_file_id === null &&
								user_interest[0].certificate_generated_id === null &&
								user_interest[0].report_avaliation === null
							) {
								if (ctx.params.manifest_interest) {
									if (
										ctx.params.manifest_interest.certificate_file_id &&
										ctx.params.manifest_interest.certificate_file_id !== null &&
										ctx.params.manifest_interest.report_avaliation &&
										ctx.params.manifest_interest.report_avaliation !== null
									) {
										const result = await stateMachine[ctx.params.event.toLowerCase()]();
										await this.sendNotification(
											ctx,
											result[0].status,
											result[0].user.id,
											result[0].user.name,
											result[0].uuid,
										);

										return result;
									} else {
										throw new ValidationError(
											"Need add certificate and report avaliation file",
											"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_REPORT_ERROR",
											{},
										);
									}
								} else {
									throw new ValidationError(
										"Need add certificate and report avaliation file",
										"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_REPORT_ERROR",
										{},
									);
								}
							}

							if (
								user_interest[0].certificate_file_id === null &&
								user_interest[0].certificate_generated_id === null &&
								user_interest[0].report_avaliation !== null
							) {
								if (ctx.params.manifest_interest) {
									if (
										ctx.params.manifest_interest.certificate_file_id &&
										ctx.params.manifest_interest.certificate_file_id !== null
									) {
										const result = await stateMachine[ctx.params.event.toLowerCase()]();
										await this.sendNotification(
											ctx,
											result[0].status,
											result[0].user.id,
											result[0].user.name,
											result[0].uuid,
										);

										return result;
									} else {
										throw new ValidationError(
											"Need add certificate file",
											"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_ERROR",
											{},
										);
									}
								} else {
									throw new ValidationError(
										"Need add certificate file",
										"SOCIAL_SCHOLARSHIP_USER_INTEREST_NO_CERTIFICATE_ERROR",
										{},
									);
								}
							}
						}
					}
				}

				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();

					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "NOT_SELECTED", "WAITING"] },
					};

					const check = validation.compile(schema);
					const res = check(ctx.params);

					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						await this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].uuid,
						);

						return result;
					}
				}

				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].uuid,
				);

				if (ctx.params.event.toLowerCase() === "analyse") {
					await ctx.call("social_scholarship.experiences.change-status-by-manifest-interest", {
						id: result[0].experience_id,
						status: "SELECTION",
					});
				}

				if (ctx.params.event.toLowerCase() === "colaboration") {
					await ctx.call("social_scholarship.experiences.change-status-by-manifest-interest", {
						id: result[0].experience_id,
						status: "IN_COLABORATION",
					});
				}

				return result;
			},
		},

		"cancel-interest": {
			rest: "POST /:id/status/interest-cancel",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:cancel",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const interest = await this._get(ctx, { id: ctx.params.id });

				// Validate user_id
				if (interest[0].user_id !== ctx.meta.user.id) {
					throw new ValidationError(
						"Unauthorized user interest status change",
						"SOCIAL_SCHOLARSHIP_USER_INTEREST_UNAUTHORIZED",
						{},
					);
				}
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					interest[0].status,
					ctx,
				);

				// Validate status
				if (stateMachine.cannot("CANCEL")) {
					throw new ValidationError(
						"Unauthorized user interest status change",
						"SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR",
						{},
					);
				}
				const result = await stateMachine["cancel"]();
				await this.sendNotification(
					ctx,
					result[0].status,
					ctx.meta.user.id,
					ctx.meta.user.name,
					result[0].notification_guid,
				);
				return result;
			},
		},

		"add-contract": {
			rest: "POST /:id/add-contract",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:list",
			params: {
				id: { type: "number", integer: true, convert: true },
				contract_file_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				return await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: ctx.params.id,
					contract_file_id: ctx.params.contract_file_id,
				});
			},
		},

		"add-certificate": {
			rest: "POST /:id/add-certificate",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:certificate",
			params: {
				id: { type: "number", integer: true, convert: true },
				certificate_file_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				return await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: ctx.params.id,
					certificate_file_id: ctx.params.certificate_file_id,
				});
			},
		},

		"add-avalidation-report": {
			rest: "POST /:id/add-report-avaliation",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:final_report",
			params: {
				avaliation_file: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				return await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: ctx.params.id,
					report_avaliation: ctx.params.avaliation_file,
				});
			},
		},

		"schedule-interview": {
			rest: "POST /:id/status/user-manifest-interview",
			scope: "social_scholarship:experience-user-interest:list",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				local: { type: "string", optional: true },
				date: { type: "date", convert: true },
				responsable_id: { type: "number", integer: true, convert: true },
				observations: { type: "string", optional: true, nullable: true },
				scope: { type: "string", optional: true, nullable: true },
			},
			async handler(ctx) {
				const manifest_interest = await this._get(ctx, { id: ctx.params.id });

				// If user interest is already in INTERVIEWED status there is no need to change user interest status
				let user_id = null;
				let user_name = null;
				let uuid = null;

				if (manifest_interest[0].status !== "INTERVIEWED") {
					let stateMachine = await UserInterestStateMachine.createStateMachine(
						manifest_interest[0].status,
						ctx,
					);

					if (stateMachine.cannot("INTERVIEW")) {
						throw new ValidationError(
							"Unauthorized application status change",
							"SOCIAL_SCHOLARSHIP_APPLICATION_STATUS_ERROR",
							{},
						);
					}
					const result = await stateMachine["interview"]();

					user_id = result[0].user.id;
					user_name = result[0].user.name;
					uuid = result[0].uuid;
				} else {
					user_id = ctx.meta.user.id;
					user_name = ctx.meta.user.name;
					uuid = manifest_interest[0].uuid;
				}

				const interview = {
					local: ctx.params.local,
					date: ctx.params.date,
					responsable_id: ctx.params.responsable_id,
					user_interest_id: ctx.params.id,
					observations: ctx.params.observations,
					scope: ctx.params.scope,
				};

				await ctx.call("social_scholarship.user-interest-interviews.create", interview);
				const responsable = await ctx.call("authorization.users.get", {
					id: ctx.params.responsable_id,
				});
				await this.sendNotificationInterview(
					ctx,
					user_name,
					user_id,
					ctx.params.local,
					ctx.params.date,
					responsable[0].name,
					uuid,
				);

				return manifest_interest;
			},
		},

		"add-student-avaliation": {
			rest: "POST /:id/add-student-avaliation",
			scope: "social_scholarship:experience-user-interest:list",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				avaliation: { type: "string" },
				file: { type: "number", integer: true, optional: true, nullable: true },
			},
			async handler(ctx) {
				let file = null;
				if (ctx.params.file) {
					file = ctx.params.file;
				}
				return await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: ctx.params.id,
					student_avaliation: ctx.params.avaliation,
					student_file_avaliation: file,
				});
			},
		},

		"generate-certificate": {
			rest: "POST /:id/generate-certificate",
			scope: "social_scholarship:experience-user-interest:certificate",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });
				const experience = await ctx.call("social_scholarship.experiences.get", {
					id: user_interest[0].experience_id,
				});
				const application = await ctx.call("social_scholarship.applications.find", {
					query: {
						experience_id: user_interest[0].experience_id,
					},
				});
				let course = null;
				if (application.length) {
					course = await ctx.call("configuration.courses.get", { id: application[0].course_id });
				}
				const experience_attendance = await ctx.call("social_scholarship.attendances.find", {
					query: {
						user_interest_id: user_interest[0].id,
					},
				});

				const data = {
					experience_name: experience[0].translations[0].title,
					student_name: user_interest[0].user.name,
					student_number: application.length
						? application[0].student_number
						: user_interest[0].user.student_number
						? user_interest[0].user.student_number
						: "---",
					school_name: experience[0].organic_unit.name,
					academic_year: experience[0].academic_year,
					responsable: experience[0].experience_responsible.name,
					advisor: experience[0].experience_advisor.name,
					date: moment().format("DD/MM/YYYY"),
					course_name: course ? course[0].name : "---",
					hours: experience_attendance.length ? experience_attendance[0].n_hours : 0,
				};
				const certificate = await ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_CERTIFICATE",
					data: data,
					name: "Certificado de experiência" + "(" + data.date + ")",
				});
				await ctx.call("social_scholarship.experience-user-interest.patch", {
					id: ctx.params.id,
					certificate_generated_id: certificate[0].id,
				});
				return certificate;
			},
		},

		check_interest: {
			rest: "GET /check-interest/:id",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:create",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await ctx.call("social_scholarship.experiences.get", {
					id: ctx.params.id,
					withRelated: false,
				});
				const user_interest = await this._count(ctx, {
					query: {
						user_id: ctx.meta.user.id,
						experience_id: ctx.params.id,
					},
				});
				const application = await ctx.call("social_scholarship.applications.count", {
					query: {
						user_id: ctx.meta.user.id,
						academic_year: experience[0].academic_year,
						status: "ACCEPTED",
					},
				});

				const applcations_actives = await ctx.call("social_scholarship.applications.count", {
					query: {
						user_id: ctx.meta.user.id,
						academic_year: experience[0].academic_year,
						status: ["SUBMITTED", "ANALYSED", "INTERVIEWED", "ACCEPTED", "DISPATCH"],
					},
				});

				let application_active = false;
				let interest = true;
				let applications = false;
				if (user_interest > 0) {
					interest = false;
				}
				if (application > 0) {
					applications = true;
				}
				if (applcations_actives > 0) {
					application_active = true;
				}

				return {
					can_express_interest:
						["APPROVED", "SELECTION", "IN_COLABORATION"].includes(experience[0].status) && interest,
					experience_available: ["APPROVED", "SELECTION", "IN_COLABORATION"].includes(
						experience[0].status,
					),
					has_application: application_active,
					has_interest: !interest,
					has_application_accepted: applications,
				};
			},
		},

		"withdrawal-interest": {
			rest: "POST /:id/status/interest-withdrawal",
			scope: "social_scholarship:experience-user-interest:interest-withdrawal",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const interest = await this._get(ctx, { id: ctx.params.id });

				// Validate user_id
				if (interest[0].user_id !== ctx.meta.user.id) {
					throw new ValidationError(
						"Unauthorized user interest status change",
						"SOCIAL_SCHOLARSHIP_USER_INTEREST_UNAUTHORIZED",
						{},
					);
				}
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					interest[0].status,
					ctx,
				);

				// Validate status
				if (stateMachine.cannot("WITHDRAWAL")) {
					throw new ValidationError(
						"Unauthorized user interest status change",
						"SOCIAL_SCHOLARSHIP_USER_INTEREST_STATUS_ERROR",
						{},
					);
				}
				const result = await stateMachine["withdrawal"]();
				await this.sendNotification(
					ctx,
					result[0].status,
					ctx.meta.user.id,
					ctx.meta.user.name,
					result[0].notification_guid,
				);
				return result;
			},
		},

		"add-interview-report": {
			rest: "POST /:interview_id/add-interview-report",
			params: {
				interview_id: { type: "number", integer: true, convert: true },
				file_id: { type: "number", interger: true, convert: true, optional: true, nullable: true },
				notes: { type: "string" },
			},
			scope: "social_scholarship:experience-user-interest:list",
			visibility: "published",
			async handler(ctx) {
				await ctx.call("social_scholarship.user-interest-interviews.get", {
					id: ctx.params.interview_id,
				});
				const interview = await ctx.call("social_scholarship.user-interest-interviews.patch", {
					id: ctx.params.interview_id,
					file_id: ctx.params.file_id,
					notes: ctx.params.notes,
				});
				this.clearCache();
				return interview;
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "social_scholarship:experience-user-interest:update",
			visibility: "published",
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });
				if (user_interest[0].last_status !== null) {
					const result = await ctx.call("social_scholarship.experience-user-interest.patch", {
						id: ctx.params.id,
						status: user_interest[0].last_status,
						last_status: user_interest[0].status,
					});
					await ctx.call("social_scholarship.user-interest-historic.create", {
						user_interest_id: ctx.params.id,
						status: user_interest[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].uuid,
					);
					return result;
				} else {
					const result = await ctx.call("social_scholarship.experience-user-interest.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: user_interest[0].status,
					});
					await ctx.call("social_scholarship.user-interest-historic.create", {
						user_interest_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].uuid,
					);
					return result;
				}
			},
		},

		"get-all-users": {
			rest: "GET /users-colaboration",
			visibility: "published",
			scope: "social_scholarship:experience-user-interest:list",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			async handler(ctx) {
				const q = await this.adapter.raw(
					"select DISTINCT user_id from experience_user_interest where status = 'COLABORATION'",
				);
				ctx.params.query = ctx.params.query || {};
				ctx.params.query.user_id = q.rows.map((u) => u.user_id);
				ctx.params.query.status = "ACCEPTED";
				return ctx.call("social_scholarship.applications.list", ctx.params);
			},
		},

		count_interests: {
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				return await this._count(ctx, { query: { experience_id: ctx.params.id } });
			},
		},

		automatic_change_status: {
			visibility: "public",
			async handler(ctx) {
				let configurations = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.ASSIGNED_STATUS_AUTO_CHANGE,
					},
					withRelated: false,
				});

				let assigned_status_auto_change = null;
				if (configurations.length > 0) {
					assigned_status_auto_change = configurations[0].value;
				}

				//
				configurations = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.STATUS_AUTO_CHANGE_DAYS,
					},
					withRelated: false,
				});

				let status_auto_change_days = null;
				if (configurations.length > 0) {
					status_auto_change_days = configurations[0].value;
				}

				//
				configurations = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.EVENT_FOR_AUTO_CHANGE,
					},
					withRelated: false,
				});

				let event_for_auto_change = null;
				if (configurations.length > 0) {
					event_for_auto_change = configurations[0].value;
				}

				if (
					assigned_status_auto_change === "true" &&
					status_auto_change_days !== null &&
					event_for_auto_change !== null
				) {
					// Treat user interests with more than x days after approval
					const user_interests = await this.adapter.db.raw(
						`select eui.id, eui.status, eui.user_id FROM user_interest_history uih
							JOIN experience_user_interest eui ON eui.id = uih.user_interest_id
							WHERE eui.status = 'APPROVED' AND uih.status ='APPROVED'
							and DATE_PART('day', AGE(current_timestamp,uih.created_at)) >=  ?
							and uih.id = (select max(uih1.id) FROM user_interest_history uih1 where uih1.user_interest_id = eui.id);`,
						[status_auto_change_days],
					);

					for (const user_int of user_interests.rows) {
						ctx.params = {};
						ctx.params.id = user_int.id;

						try {
							const stateMachine = UserInterestStateMachine.createStateMachine(
								user_int.status,
								ctx,
							);

							ctx.params.notes =
								"[Automatic] User Interest in " +
								event_for_auto_change.toLowerCase() +
								" performed automatically after " +
								status_auto_change_days +
								" days";

							await stateMachine[event_for_auto_change.toLowerCase()]();
						} catch (err) {
							this.logger.info(err);
						}
					}
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateUserInterestExperience(ctx) {
			const experience = await ctx.call("social_scholarship.experiences.get", {
				id: ctx.params.experience_id,
			});
			const application = await ctx.call("social_scholarship.applications.find", {
				query: {
					academic_year: experience[0].academic_year,
					user_id: ctx.meta.user.id,
					status: "ACCEPTED",
				},
			});
			if (application.length === 0) {
				throw new ValidationError(
					"This user not have application",
					"SOCIAL_SCHOLARSHIP_USER_NOT_APPLICATION",
					{},
				);
			}
			const validateInterest = await this._find(ctx, {
				query: {
					user_id: ctx.meta.user.id,
					experience_id: ctx.params.experience_id,
				},
			});
			if (validateInterest.length !== 0) {
				throw new ValidationError(
					"This user have a interest manifest active in this experience",
					"SOCIAL_SCHOLARSHIP_USER_INTEREST_ALREADY_EXIST",
					{},
				);
			}
		},

		async saveCreateHistory(ctx, response) {
			response[0].history = [
				...(await ctx.call("social_scholarship.user-interest-historic.create", {
					user_interest_id: response[0].id,
					status: response[0].status,
					user_id: ctx.meta.user.id,
					notes: null,
				})),
			];
			await this.sendNotification(
				ctx,
				response[0].status,
				response[0].user.id,
				response[0].user.name,
				response[0].uuid,
			);
			return response;
		},

		async sendNotification(ctx, status, user_id, name, notification_gui) {
			// Send notification to user
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name },
				variables: {},
				external_uuid: notification_gui,
				medias: [],
			});

			// Services responsibles are notified whenever there are changes caused by students who require action from the services
			if (
				status.toUpperCase() === "SUBMITTED" ||
				status.toUpperCase() === "ACCEPTED" ||
				status.toUpperCase() === "CANCELLED" ||
				status.toUpperCase() === "WITHDRAWAL"
			) {
				const services_responsables_email = await ctx.call(
					"social_scholarship.configurations.find",
					{
						query: {
							key: CONFIGURATION_KEYS.SERVICES_RESPONSABLES_NOTIFICATION_EMAIL,
						},
					},
				);

				await ctx.call("notifications.alerts.create_alert", {
					alert_type_key: "SOCIAL_SUPPORT_MANIFEST_INTEREST_USERS_" + status.toUpperCase(),
					user_id: user_id,
					user_data: { email: services_responsables_email[0].value },
					data: { name: name },
					variables: {},
					external_uuid: notification_gui,
					medias: [],
				});
			}

			// Emails configured in USER_INTEREST_EMAILS are notified whenever that an user interest is submitted
			if (status.toUpperCase() === "SUBMITTED") {
				ctx
					.call("social_scholarship.configurations.find", {
						query: {
							key: CONFIGURATION_KEYS.APPLICATION_EMAILS,
						},
					})
					.then((response) => {
						if (response.length) {
							response.map((user_id) => {
								ctx
									.call("authorization.users.get", {
										id: user_id,
									})
									.then((user) => {
										if (user.length) {
											ctx.call("notifications.alerts.create_alert", {
												alert_type_key: "SOCIAL_SUPPORT_NEW_MANIFEST_INTEREST_USERS_SUBMITTED",
												user_id: user_id,
												user_data: { email: user[0].email },
												data: { name: name },
												variables: {},
												external_uuid: notification_gui,
												medias: [],
											});
										}
									});
							});
						}
					});

				/*
				const user_interest_emails = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.USER_INTEREST_EMAILS,
					},
				});

				if (
					user_interest_emails.length > 0 &&
					user_interest_emails[0].value !== null &&
					user_interest_emails[0].value !== ""
				) {
					const emailArray = user_interest_emails[0].value.split(";");

					for (let i = 0; i < emailArray.length; i++) {
						await ctx.call("notifications.alerts.create_alert", {
							alert_type_key: "SOCIAL_SUPPORT_NEW_MANIFEST_INTEREST_USERS_SUBMITTED",
							user_id: user_id,
							user_data: { email: emailArray[i] },
							data: { name: name },
							variables: {},
							external_uuid: notification_gui,
							medias: [],
						});
					}
				}*/
			}
		},

		async sendNotificationInterview(ctx, name, user_id, local, date, responsable, uuid) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_MANIFEST_INTEREST_SCHEDULE_INTERVIEW",
				user_id: user_id,
				user_data: {},
				data: { name: name, local: local, date: date, responsable: responsable },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});
		},

		async getUserApplications(ctx, user_id) {
			if (!ctx.meta.isBackoffice) {
				return [];
			} else {
				const applications = await ctx.call("social_scholarship.applications.find", {
					query: {
						user_id: user_id,
					},
					withRelated: false,
				});
				return applications.map((app) => {
					const container = {};
					container.academic_year = app.academic_year;
					container.id = app.id;
					container.user_id = app.user_id;
					return container;
				});
			}
		},

		async getUserExperiences(ctx, user_id) {
			const user_manifest = await ctx.call("social_scholarship.experience-user-interest.find", {
				query: {
					user_id: user_id,
				},
				withRelated: ["experience"],
			});
			return user_manifest.map((user_interest) => {
				let container = {};
				if (user_interest.experience !== null) {
					if (!ctx.meta.isBackoffice) {
						if (
							user_interest.experience.experience_responsible_id === ctx.meta.user.id ||
							user_interest.experience.experience_advisor_id
						) {
							container.academic_year = user_interest.experience.academic_year;
							container.user_manifest_id = user_interest.id;
							container.user_id = user_interest.user_id;
							container.experience_translations = user_interest.experience.translations;
						}
					} else {
						container.academic_year = user_interest.experience.academic_year;
						container.user_manifest_id = user_interest.id;
						container.user_id = user_interest.user_id;
						container.experience_translations = user_interest.experience.translations;
					}
				}
				return container;
			});
		},

		async getCurrentApplication(ctx, experience_id, user_id) {
			const experience = await ctx.call("social_scholarship.experiences.get", {
				id: experience_id,
				withRelated: false,
			});
			const application = await ctx.call("social_scholarship.applications.find", {
				query: {
					academic_year: experience[0].academic_year,
					user_id: user_id,
				},
				withRelated: [
					"preferred_activities",
					"preferred_schedule",
					"course_degree",
					"school",
					"course",
					"organic_units",
				],
			});
			if (!ctx.meta.isBackoffice) {
				if (application.length > 0) {
					delete application[0].has_scholarship;
					delete application[0].has_applied_scholarship;
					delete application[0].is_waiting_scholarship_response;
					delete application[0].scholarship_monthly_value;
					delete application[0].has_emergency_fund;
					delete application[0].student_address;
					delete application[0].student_code_postal;
					delete application[0].student_location;
					delete application[0].student_nationality;
					delete application[0].student_tin;
					delete application[0].student_birthdate;
					delete application[0].student_identification;
					delete application[0].iban;
					return application[0];
				} else {
					return null;
				}
			} else {
				return application[0];
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}

					return value;
				`
			};
		});*/
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}

					return value;
				`
			};
		});*/
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
