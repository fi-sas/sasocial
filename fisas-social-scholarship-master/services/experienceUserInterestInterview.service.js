"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.user-interest-interviews",
	table: "user_interest_interview",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "user-interest-interviews")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"local",
			"date",
			"responsable_id",
			"file_id",
			"notes",
			"observations",
			"scope",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["responsable", "file"],
		withRelateds: {
			responsable(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "authorization.users", "responsable", "responsable_id", "id", {}, "name,email,phone", false);
			},
			file(ids, docs, rule, ctx){
				return hasOne(
					docs,
					ctx,
					"media.files",
					"file",
					"file_id"
				);
			}
		},
		entityValidator: {
			user_interest_id: { type: "number", interger: true, convert: true },
			local: { type: "string" },
			date: { type: "date", convert: true },
			responsable_id: { type: "number", integer: true, convert: true },
			file_id: { type: "number", integer: true, convert: true, optional: true },
			notes: { type: "string", optional: true, nullable: true },
			observations: { type: "string", optional: true, nullable: true },
			scope: { type: "string", optional: true, nullable: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
