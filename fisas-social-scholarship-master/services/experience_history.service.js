"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EXPERIENCE_STATUS } = require("./utils/constants.js");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.experience_history",
	table: "experience_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "experience_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"experience_id",
			"status",
			"user_id",
			"notes",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			"user"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			experience_id: "number|integer|convert|optional",
			status: /*"string|max:255"*/{ type: "enum", values: Object.keys(EXPERIENCE_STATUS) },
			user_id: "number|integer|convert|optional",
			notes: "string|max:255|optional",
			created_at: "date|convert",
			updated_at: "date|convert",
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
