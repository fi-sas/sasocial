"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError, ForbiddenError } = require("@fisas/ms_core").Helpers.Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const {
	EXPERIENCE_STATUS,
	EXPERIENCE_PAYMENT_MODEL,
	EXPERIENCE_EVENTS,
	CONFIGURATION_KEYS,
} = require("./utils/constants.js");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const ExperiencesStateMachine = require("./state-machines/experiences.machine");
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const Validator = require("fastest-validator");
const UUIDV4 = require("uuid").v4;
const moment = require("moment");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.experiences",
	table: "experience",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "experiences")],
	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"academic_year",
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring) //"subject",
			"organic_unit_id",
			"address",
			"number_weekly_hours",
			"total_hours_estimation",
			"schedule",
			"payment_model",
			"payment_value",
			"perc_student_iban",
			"perc_student_ca",
			"experience_responsible_id",
			"experience_advisor_id",
			"start_date",
			"end_date",
			"publish_date",
			"application_deadline_date",
			"attachment_file_id",
			"status",
			"number_candidates",
			"number_simultaneous_candidates",
			"holydays_availability",
			"updated_at",
			"created_at",
			"requirement_number",
			"uuid",
			"published",
			"last_status",
			"return_reason",
			"email_search_advisor",
			"decision",
			"reject_reason",
		],
		defaultWithRelateds: [
			"translations",
			"history",
			"organic_unit",
			"experience_responsible",
			"experience_advisor",
		],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("social_scholarship.translations.find", {
									query: { experience_id: doc.id, language_id: ctx.meta.language_id },
								})
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("social_scholarship.translations.find", { query: { experience_id: doc.id } })
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				}
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.experience_history",
					"history",
					"id",
					"experience_id",
				);
			},
			organic_unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organic_unit", "organic_unit_id");
			},
			experience_responsible(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"experience_responsible",
					"experience_responsible_id",
					"id",
					{},
					"name,email,phone",
					"profile",
				);
			},
			experience_advisor(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"experience_advisor",
					"experience_advisor_id",
					"id",
					{},
					"name,email,phone",
					false,
				);
			},
			attachment_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "attachment_file", "attachment_file_id");
			},
			users_colaboration(ids, experiences, rule, ctx) {
				return Promise.all(
					experiences.map((experience) => {
						return ctx
							.call("social_scholarship.experience-user-interest.find", {
								query: {
									experience_id: experience.id,
									status: "COLABORATION",
								},
							})
							.then(async (user_colaboration) => {
								for (const colaboration of user_colaboration) {
									colaboration.application = await ctx
										.call("social_scholarship.applications.find", {
											query: {
												academic_year: experience.academic_year,
												user_id: colaboration.user_id,
											},
											withRelated: false,
										})
										.then((response) => (response.length ? response[0] : null));
								}
								experience.users_colaboration = user_colaboration;
							});
					}),
				);
			},
			users_interest(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("social_scholarship.experience-user-interest.find", {
								query: {
									experience_id: doc.id,
								},
								withRelated: [
									"user",
									"history",
									"contract_file",
									"certificate_file",
									"certificate_generated",
									"interview",
								],
							})
							.then(async (user_experience) => {
								for (const experience of user_experience) {
									experience.application = await ctx
										.call("social_scholarship.applications.find", {
											query: {
												academic_year: doc.academic_year,
												user_id: experience.user_id,
											},
											withRelated: false,
										})
										.then((response) => (response.length ? response[0] : null));
								}
								doc.users_interest = user_experience;
							});
					}),
				);

				/*return hasMany(
					docs,
					ctx,
					"social_scholarship.experience-user-interest",
					"users_interest",
					"id",
					"experience_id",
					{},
					null,
					"user,history,contract_file,certificate_file,certificate_generated,interview"
				);*/
			},
			external_entity(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"social_scholarship.external_entities",
					"external_entity",
					"experience_responsible_id",
					"user_id",
				);
			},
		},
		entityValidator: {
			academic_year: { type: "string", max: 9, pattern: "^\\d{4}-\\d{4}" },
			//[DEPRECATED] splited into 3 MS's (Social_Scholarship, Volunteering and Mentoring)
			organic_unit_id: "number|integer|convert",
			address: "string|max:255",
			number_weekly_hours: "number|convert",
			total_hours_estimation: "number|convert",
			schedule: "string",
			/*
			NESTE MS/BOLSA_COLABORADORES estes c	ampos são necessários, no MS/VOLUNTARIADO não existirão:
				Boilerplate 1.0:
				is: Joi.string().valid(['BC']).required(),
				then: Joi.string().valid(['BA', 'VH']).required(),
				otherwise: Joi.string().valid(['BA', 'VH']).allow(null).default(null).optional(),*/
			payment_model: /*"string|max:2|optional"*/ {
				type: "enum",
				values: Object.keys(EXPERIENCE_PAYMENT_MODEL),
				default: "VH",
			},
			payment_value: "number|convert|min:0|default:0",
			perc_student_iban: "number|convert|min:0|max:1|default:1",
			perc_student_ca: "number|convert|min:0|max:1|default:0",
			/* /MS/BOLSA_COLABORADORES */
			experience_responsible_id: "number|integer|convert",
			experience_advisor_id: "number|integer|convert|optional|nullable",
			start_date: /*"date|convert"*/ { type: "date", convert: true /*, min: () => new Date()*/ },
			end_date: /*"date|convert"*/ {
				type: "date",
				convert: true /*, min: (val) => val >= start_date*/,
			},
			publish_date: /*"date|convert"*/ {
				type: "date",
				convert: true,
				nullable: true,
				optional: true /*, min: () => new Date()*/,
			},
			application_deadline_date: /*"date|convert"*/ {
				type: "date",
				convert: true /*, min: () => new Date()*/,
			},
			attachment_file_id: "number|integer|convert|optional",
			status: /*"string|max:255"*/ {
				type: "enum",
				optional: true,
				values: Object.keys(EXPERIENCE_STATUS),
			},
			number_candidates: "number|integer|convert|optional",
			number_simultaneous_candidates: "number|integer|convert|optional",
			holydays_availability: "number|integer|convert",
			updated_at: "date|convert|optional",
			created_at: "date|convert|optional",
			translations: {
				type: "array",
				min: 1,
				items: {
					type: "object",
					props: {
						title: "string|max: 255",
						proponent_service: "string|max: 255",
						applicant_profile: "string|max: 255",
						job: "string|max: 255",
						description: "string|max: 255",
						selection_criteria: "string|max: 255",
						language_id: "number|integer|convert",
					},
				},
			},
			uuid: "string|optional",
			requirement_number: "number|integer|convert|optional",
			last_status: {
				type: "enum",
				values: Object.keys(EXPERIENCE_STATUS),
				optional: true,
				nullable: true,
			},
			published: "boolean|optional",
			return_reason: { type: "string", optional: true, nullable: true },
			email_search_advisor: { type: "email", optional: true, nullable: true },
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "RETURNED"],
				nullable: true,
				optional: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateHasCurrentAccount",
				async function sanatizeParams(ctx) {
					ctx.params.uuid = UUIDV4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";
					ctx.params.schedule = JSON.stringify(ctx.params.schedule);
					ctx.params.published = true;
					if (ctx.params.number_weekly_hours > ctx.params.total_hours_estimation) {
						throw new ValidationError(
							"The 'number weekly hours' need smaller 'total_hours_estimation'",
							"SOCIAL_SCHOLARSHIP_EXPERIENCE_NUM_WEEKLY_SMALLER",
							{},
						);
					}
					if (!ctx.meta.isBackoffice) {
						ctx.params.experience_responsible_id = ctx.meta.user.id;
						const validation = new Validator();
						const schema = {
							email_search_advisor: { type: "email" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							const user = await ctx.call("authorization.users.find", {
								query: {
									email: ctx.params.email_search_advisor,
								},
							});
							if (user.length > 0) {
								ctx.params.experience_advisor_id = user[0].id;
							} else {
								ctx.params.experience_advisor_id = null;
							}
						}
					}

					// /* Boilerplate 1.0
					// is: Joi.string().valid(['BC']).required(),
					// then: Joi.string().valid(['BA', 'VH']).required(),
					// otherwise: Joi.string().valid(['BA', 'VH']).allow(null).default(null).optional(),*/

					if (ctx.params.start_date < new Date()) {
						this.logger.error("#####-----> Data tem que ser superior a xpto");
					}

					const validate_dates = moment(moment(ctx.params.end_date).format("YYYY-MM-DD")).isAfter(
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
					);
					if (validate_dates === false) {
						throw new ValidationError(
							"The end_date need is after start_date",
							"SOCIAL_SCHOLARSHIP_EXPERIENCE_CREATE_ERROR",
							{},
						);
					}

					// Validate translations
					if (!Array.isArray(ctx.params.translations) || ctx.params.translations == 0) {
						throw new ValidationError(
							"The 'translations' field is required and must be an array",
							"SOCIAL_SCHOLARSHIP_TRANSLATIONS_NOT_FOUND",
							{},
						);
					}

					for (const translation of ctx.params.translations) {
						await ctx.call("configuration.languages.get", { id: translation.language_id });
						if (await this.isUniqueLanguage(ctx.params.translations, translation.language_id)) {
							throw new ValidationError(
								"This service already have a translation",
								"SOCIAL_SCHOLARSHIP_TRANSLATION_ALREADY_EXIST",
								{},
							);
						}
					}
				},
			],
			update: [
				"validateHasCurrentAccount",
				async function sanatizeParams(ctx) {
					const experience = await this._get(ctx, { id: ctx.params.id });
					if (!ctx.meta.isBackoffice) {
						if (
							experience[0].experience_responsible_id !== ctx.meta.user.id &&
							experience[0].experience_advisor_id !== ctx.meta.user.id
						) {
							throw new ForbiddenError(
								"You are not an advisor or responsible for the experience",
								"SOCIAL_SCHOLARSHIP_EXPERIENCE_UPDATE_FORBIDDEN",
								{},
							);
						}
						ctx.params.experience_responsible_id = experience[0].experience_responsible_id;
						ctx.params.experience_advisor_id = experience[0].experience_advisor_id;
						if (ctx.params.experience_advisor_id !== null) {
							const advisor = await ctx.call("authorization.users.get", {
								id: ctx.params.experience_advisor_id,
							});
							ctx.params.email_search_advisor = advisor[0].email;
						}
						if (ctx.params.experience_advisor_id === null) {
							const user = await ctx.call("authorization.users.find", {
								query: {
									email: ctx.params.email_search_advisor,
								},
							});
							if (user.length > 0) {
								ctx.params.experience_advisor_id = user[0].id;
							} else {
								ctx.params.experience_advisor_id = null;
							}
						}
						if (experience[0].experience_responsible_id === ctx.meta.user.id) {
							ctx.params.status = "SUBMITTED";
						}
					}
					if (ctx.meta.isBackoffice) {
						if (ctx.params.experience_advisor_id !== null) {
							const advisor = await ctx.call("authorization.users.get", {
								id: ctx.params.experience_advisor_id,
							});
							ctx.params.email_search_advisor = advisor[0].email;
						}
						if (experience[0].experience_responsible_id === ctx.meta.user.id) {
							ctx.params.status = "SUBMITTED";
						}
						if (experience[0].experience_responsible_id !== ctx.meta.user.id) {
							ctx.params.status = experience[0].status;
						}
					}
					if (ctx.params.number_weekly_hours > ctx.params.total_hours_estimation) {
						throw new ValidationError(
							"The 'number weekly hours' need smaller 'total_hours_estimation'",
							"SOCIAL_SCHOLARSHIP_EXPERIENCE_NUM_WEEKLY_SMALLER",
							{},
						);
					}
					ctx.params.updated_at = new Date();
					ctx.params.schedule = JSON.stringify(ctx.params.schedule);
				},
			],
			patch: [
				async function sanatizeParams(ctx) {
					if (ctx.params.schedule) {
						ctx.params.schedule = JSON.stringify(ctx.params.schedule);
					}
				},
			],
			remove: [
				async function sanatizeParams(ctx) {
					await this.deleteWithRelated(
						ctx,
						"social_scholarship.translations.remove",
						await ctx.call("social_scholarship.translations.find", {
							query: { experience_id: ctx.params.id },
						}),
					);
				},
			],
			list: [
				function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.isAvaliable) {
						ctx.params.query["publish_date"] = {};
						ctx.params.query["publish_date"]["lte"] = moment(new Date()).format("YYYY-MM-DD");
						ctx.params.query["application_deadline_date"] = {};
						ctx.params.query["application_deadline_date"]["gte"] = moment(new Date()).format(
							"YYYY-MM-DD",
						);
						ctx.params.query["published"] = true;
						delete ctx.params.query.isAvaliable;
					}
				},
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["title", "description"],
						"social_scholarship.translations",
						"experience_id",
					);
				},
			],
		},
		after: {
			create: [
				"saveCreateHistory",
				"saveTranslations",
				async function sanatizeParams(ctx, response) {
					await this.sendNotification(
						ctx,
						"SUBMITTED",
						ctx.meta.user.id,
						ctx.meta.user.name,
						response[0].uuid,
						response[0].translations[0].title,
					);
					this.clearCache();

					return response;
				},
			],
			update: [
				"saveUpdateHistory",
				async function sanatizeParams(ctx, response) {
					await this.deleteWithRelated(
						ctx,
						"social_scholarship.translations.remove",
						await ctx.call("social_scholarship.translations.find", {
							query: { experience_id: response[0].id },
						}),
					);
					response[0].translations = await this.createWithRelated(
						ctx,
						"social_scholarship.translations.create",
						response[0].id,
						ctx.params.translations,
					);
					return response;
				},
			],
			patch: [
				async function sanatizeParams(ctx, response) {
					if (ctx.params.translations) {
						await this.deleteWithRelated(
							ctx,
							"social_scholarship.translations.remove",
							await ctx.call("social_scholarship.translations.find", {
								query: { experience_id: response[0].id },
							}),
						);
						response[0].translations = await this.createWithRelated(
							ctx,
							"social_scholarship.translations.create",
							response[0].id,
							ctx.params.translations,
						);
					}
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				let experiences_list = [];
				if (ctx.meta.isGuest) {
					experiences_list = await this._find(ctx, {
						query: (qb) => {
							qb.where(this.adapter.raw("publish_date::date"), "<=", moment().format("YYYY-MM-DD"));
							qb.whereIn("status", ["APPROVED", "SELECTION", "IN_COLABORATION"]);
							qb.where("published", "=", true);
							qb.where(
								this.adapter.raw("application_deadline_date::date"),
								">=",
								moment().format("YYYY-MM-DD"),
							);
							return qb;
						},
						limit: 3,
					});
				}
				if (!ctx.meta.isGuest) {
					const scope_advisor = await ctx.call("authorization.scopes.userHasScope", {
						scope: "social_scholarship:experiences:advisor",
					});
					const scope_responsable = await ctx.call("authorization.scopes.userHasScope", {
						scope: "social_scholarship:experiences:responsable",
					});

					if (scope_advisor && !scope_responsable) {
						experiences_list = await this._find(ctx, {
							query: (qb) => {
								qb.where("experience_advisor_id", "=", ctx.meta.user.id);
								qb.whereIn("status", [
									"APPROVED",
									"SUBMITTED",
									"ANALYSED",
									"SELECTION",
									"IN_COLABORATION",
									"RETURNED",
								]);
								return qb;
							},
							limit: 3,
						});
					}
					if (scope_responsable && !scope_advisor) {
						experiences_list = await this._find(ctx, {
							query: (qb) => {
								qb.where("experience_responsible_id", "=", ctx.meta.user.id);
								qb.whereIn("status", [
									"APPROVED",
									"SUBMITTED",
									"ANALYSED",
									"SELECTION",
									"IN_COLABORATION",
									"RETURNED",
								]);
								return qb;
							},
							limit: 3,
						});
					}
					if (scope_advisor && scope_responsable) {
						experiences_list = await this._find(ctx, {
							query: (qb) => {
								qb.whereIn("status", [
									"APPROVED",
									"SUBMITTED",
									"ANALYSED",
									"SELECTION",
									"IN_COLABORATION",
									"RETURNED",
								]);
								qb.where("experience_responsible_id", "=", ctx.meta.user.id);
								qb.orWhere("experience_advisor_id", "=", ctx.meta.user.id);
								qb.whereIn("status", [
									"APPROVED",
									"SUBMITTED",
									"ANALYSED",
									"SELECTION",
									"IN_COLABORATION",
									"RETURNED",
								]);
								return qb;
							},
							limit: 3,
						});
					}
				}
				if (experiences_list.length > 0) {
					return experiences_list;
				} else {
					throw new ValidationError(
						"Dont exist any published experience",
						"SOCIAL_SCHOLARSHIP_NO_PUBLISHED_EXPERIENCES",
						{},
					);
				}
			},
		},

		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true },
			},
			scope: "social_scholarship:experiences:list",
			async handler(ctx) {
				let academic_year;
				const current_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
				);
				if (!ctx.params.academic_year) {
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}

				let query = `select ex.status, Count(*) from experience as ex where ex.academic_year = ? group by ex.status `;
				return this.adapter.raw(query, academic_year).then((response) => {
					let allStatus = {
						SUBMITTED: 0,
						RETURNED: 0,
						ANALYSED: 0,
						APPROVED: 0,
						PUBLISHED: 0,
						REJECTED: 0,
						CANCELLED: 0,
						SEND_SEEM: 0,
						EXTERNAL_SYSTEM: 0,
						SELECTION: 0,
						IN_COLABORATION: 0,
						CLOSED: 0,
						DISPATCH: 0,
					};
					response.rows.map((r) => (allStatus[r.status] = r.count));
					return allStatus;
				});
			},
		},

		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			scope: "social_scholarship:experiences:list",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await ExperiencesStateMachine.createStateMachine(
					experience[0].status,
					ctx,
				);
				return await stateMachine.transitions();
			},
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "social_scholarship:experiences:status",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: EXPERIENCE_EVENTS,
					optional: true,
					nullable: true,
				},
				notes: { type: "string", optional: true },
				experience: { type: "object", optional: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await ExperiencesStateMachine.createStateMachine(
					experience[0].status,
					ctx,
				);
				// Validate Status
				if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
					throw new ValidationError(
						"Unauthorized experience status change",
						"SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR",
						{},
					);
				}
				if (experience[0].status === "DISPATCH") {
					const validation = new Validator();
					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;
							if (experience[0].decision === "REJECTED") {
								status = "reject";
							}
							if (experience[0].decision === "RETURNED") {
								status = "return";
							}
							if (experience[0].decision === "ACCEPTED") {
								status = "approve";
							}
							const result = await stateMachine[status]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].experience_responsible.id,
								result[0].experience_responsible.name,
								result[0].uuid,
								result[0].translations[0].title,
							);
							this.clearCache();
							return result;
						} else {
							const result = await stateMachine["analyse"]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].experience_responsible.id,
								result[0].experience_responsible.name,
								result[0].uuid,
								result[0].translations[0].title,
							);
							this.clearCache();
							return result;
						}
					}
				}
				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();
					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "RETURNED"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						await this.sendNotification(
							ctx,
							result[0].status,
							result[0].experience_responsible.id,
							result[0].experience_responsible.name,
							result[0].uuid,
							result[0].translations[0].title,
						);
						return result;
					}
				}
				if (ctx.params.event.toLowerCase() === "approve") {
					if (experience[0].experience_advisor_id === null) {
						if (ctx.params.experience) {
							if (ctx.params.experience.experience_advisor_id) {
								const advisor = await ctx.call("authorization.users.get", {
									id: ctx.params.experience.experience_advisor_id,
								});
								ctx.params.experience.email_search_advisor = advisor[0].email;
								const result = await stateMachine[ctx.params.event.toLowerCase()]();
								await this.sendNotification(
									ctx,
									result[0].status,
									result[0].experience_responsible.id,
									result[0].experience_responsible.name,
									result[0].uuid,
									result[0].translations[0].title,
								);
								return result;
							} else {
								throw new ValidationError(
									"Need send experience_advisor_id",
									"SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_NO_ADVISOR_ID",
									{},
								);
							}
						} else {
							throw new ValidationError(
								"Need send experience_advisor_id",
								"SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_NO_ADVISOR_ID",
								{},
							);
						}
					}
				}
				if (ctx.params.event.toLowerCase() === "colaboration") {
					const colaborations = await ctx.call("social_scholarship.experience-user-interest.find", {
						query: {
							experience_id: ctx.params.id,
							status: "COLABORATION",
						},
						withRelated: false,
					});
					if (colaborations.length === 0) {
						throw new ValidationError(
							"Unauthorized experience status change dont have users in colaborations",
							"SOCIAL_SHOLARSHIP_EXPERIENCE_NO_COLABORATIONS_ERROR",
							{},
						);
					}
				}
				if (ctx.params.event.toLowerCase() === "return") {
					if (ctx.params.experience) {
						if (ctx.params.experience.return_reason) {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();
							await this.sendNotificationReturn(
								ctx,
								result[0].experience_responsible_id,
								result[0].experience_responsible.name,
								result[0].return_reason,
								result[0].uuid,
								result[0].translations[0].title,
							);
							return result;
						} else {
							throw new ValidationError(
								"Unauthorized application status change need send return reason",
								"SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR",
								{},
							);
						}
					} else {
						throw new ValidationError(
							"Unauthorized application status change need send return reason",
							"SOCIAL_SHOLARSHIP_EXPERIENCE_STATUS_ERROR",
							{},
						);
					}
				}
				if (ctx.params.event.toLowerCase() === "sendseem") {
					const end_date = moment(experience[0].end_date);
					experience[0].number_weeks = end_date.diff(experience[0].start_date, "week");
					experience[0].value_week =
						experience[0].number_weekly_hours *
						experience[0].number_simultaneous_candidates *
						experience[0].payment_value;
					experience[0].total_value =
						experience[0].total_hours_estimation * experience[0].payment_value;
					experience[0].start_date = moment(experience[0].start_date).format("YYYY-MM-DD");
					experience[0].end_date = moment(experience[0].end_date).format("YYYY-MM-DD");
					experience[0].publish_date = moment(experience[0].publish_date).format("YYYY-MM-DD");
					experience[0].application_deadline_date = moment(
						experience[0].application_deadline_date,
					).format("YYYY-MM-DD");
					let data = {
						experience: experience[0],
					};

					await ctx
						.call("reports.templates.print", {
							key: "SOCIAL_SCHOLARSHIP_EXPERIENCE",
							data: data,
							confirmation_path: "colaboration.service.send_doc",
							extra_info: {
								experience_id: ctx.params.id,
							},
							name:
								"Resumo da experiencia - Bolsa de colaboradores" +
								"(" +
								moment().format("DD/MM/YYYY") +
								")",
						})
						.catch((error) => this.logger.info("ERROR TEMPLATE", error));
				}
				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].experience_responsible.id,
					result[0].experience_responsible.name,
					result[0].uuid,
					result[0].translations[0].title,
				);
				return result;
			},
		},

		"change-status-by-manifest-interest": {
			params: {
				id: { type: "number", integer: true, convert: true },
				status: { type: "string" },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				if (ctx.params.status === "IN_COLABORATION") {
					if (experience[0].status !== "IN_COLABORATION") {
						const experience_updated = await ctx.call("social_scholarship.experiences.patch", {
							id: ctx.params.id,
							status: ctx.params.status,
						});
						await ctx.call("social_scholarship.experience_history.create", {
							experience_id: ctx.params.id,
							status: ctx.params.status,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
						});
						await this.sendNotification(
							ctx,
							experience_updated[0].status,
							experience_updated[0].experience_responsible.id,
							experience_updated[0].experience_responsible.name,
							experience_updated[0].uuid,
							experience_updated[0].translations[0].title,
						);
					}
				}
				if (ctx.params.status === "SELECTION") {
					if (experience[0].status !== "SELECTION") {
						const experience_updated = await ctx.call("social_scholarship.experiences.patch", {
							id: ctx.params.id,
							status: ctx.params.status,
						});
						await ctx.call("social_scholarship.experience_history.create", {
							experience_id: ctx.params.id,
							status: ctx.params.status,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
						});
						await this.sendNotification(
							ctx,
							experience_updated[0].status,
							experience_updated[0].experience_responsible.id,
							experience_updated[0].experience_responsible.name,
							experience_updated[0].uuid,
							experience_updated[0].translations[0].title,
						);
					}
				}
			},
		},

		"get-users-for-interest": {
			rest: "GET /:id/target-users",
			scope: "social_scholarship:experiences:list",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				school_id: { type: "number", integer: true, convert: true, optional: true },
				course_id: { type: "number", integer: true, convert: true, optional: true },
				course_year: { type: "number", integer: true, convert: true, optional: true },
				without_active_collaborations: { type: "boolean", convert: true, optional: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });

				const userIds = await ctx.call(
					"social_scholarship.applications.get_application_users_for_interest",
					{
						school_id: ctx.params.school_id,
						course_id: ctx.params.course_id,
						course_year: ctx.params.course_year,
						without_active_collaborations: ctx.params.without_active_collaborations,
						academic_year: experience[0].academic_year,
					},
				);

				return userIds;
			},
		},

		"send-notification-users-target": {
			rest: "POST /:id/send-notifications",
			scope: "social_scholarship:experiences:list",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				user_ids: { type: "array", items: "number" },
			},
			async handler(ctx) {
				let resp = [];
				const experience = await this._get(ctx, { id: ctx.params.id });
				for (const id of ctx.params.user_ids) {
					const user_info = await ctx.call("authorization.users.get", { id: id });
					this.sendNotificationUsers(
						ctx,
						user_info[0].id,
						user_info[0].name,
						experience[0].translations[0].title,
						experience[0].uuid,
					);
					resp.push(user_info);
				}
				return resp;
			},
		},

		"get-experiences-responsable": {
			rest: "GET /responsable",
			scope: "social_scholarship:experiences:responsable",
			visibility: "published",
			async handler(ctx) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["experience_responsible_id"] = ctx.meta.user.id;
				const experiences = await ctx.call("social_scholarship.experiences.find", ctx.params);
				for (const experience of experiences) {
					const count_interests = await ctx.call(
						"social_scholarship.experience-user-interest.count_interests",
						{ id: experience.id },
					);
					experience.number_interests = count_interests;
				}
				return experiences;
			},
		},

		"get-experiences-advisor": {
			rest: "GET /advisor",
			visibility: "published",
			scope: "social_scholarship:experiences:advisor",
			async handler(ctx) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["experience_advisor_id"] = ctx.meta.user.id;
				const experiences = await ctx.call("social_scholarship.experiences.find", ctx.params);
				for (const experience of experiences) {
					const count_interests = await ctx.call(
						"social_scholarship.experience-user-interest.count_interests",
						{ id: experience.id },
					);
					experience.number_interests = count_interests;
				}
				return experiences;
			},
		},

		"generate-report-user-experience": {
			rest: "GET /:id/users-manifest",
			visibility: "published",
			scope: "social_scholarship:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
				status: { type: "string", optional: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				const user_interests = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: {
						experience_id: ctx.params.id,
					},
					withRelated: ["application"],
				});
				for (let user of user_interests) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}
				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					experience_name: experience[0].translations[0].title,
					experience_responsable: experience[0].experience_responsible.name,
					experience_advisor: experience[0].experience_advisor.name,
					organic_unit: experience[0].organic_unit.name,
					user_interests: user_interests,
				};
				return ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_USER_INTEREST",
					data: data,
					name: "Experiência - Manifestações de interesse",
				});
			},
		},

		"generate-report-user-interest-selection": {
			rest: "GET /:id/user-manifest-selection",
			visibility: "published",
			scope: "social_scholarship:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				const user_manifest_interst = await ctx.call(
					"social_scholarship.experience-user-interest.find",
					{
						query: {
							experience_id: ctx.params.id,
							status: [
								"SUBMITTED",
								"ANALYSED",
								"INTERVIEWED",
								"APPROVED",
								"WAITING",
								"NOT_SELECTED",
							],
						},
						withRelated: ["application"],
					},
				);
				for (let user of user_manifest_interst) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}
				const user_manifest_selection = await ctx.call(
					"social_scholarship.experience-user-interest.find",
					{
						query: {
							experience_id: ctx.params.id,
							status: "ACCEPTED",
						},
						withRelated: ["application"],
					},
				);

				for (let user of user_manifest_selection) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}

				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					experience_name: experience[0].translations[0].title,
					experience_responsable: experience[0].experience_responsible.name,
					experience_advisor: experience[0].experience_advisor.name,
					organic_unit: experience[0].organic_unit.name,
					user_interests: user_manifest_interst,
					user_interests_selection: user_manifest_selection,
				};

				return ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_USER_INTEREST_AND_SELECTION",
					data: data,
					name: "Lista de manifestações de interesse-seleção" + "(" + data.date + ")",
				});
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "social_scholarship:experiences:update",
			visibility: "published",
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				if (experience[0].last_status !== null) {
					const result = await ctx.call("social_scholarship.experiences.patch", {
						id: ctx.params.id,
						status: experience[0].last_status,
						last_status: experience[0].status,
					});
					await ctx.call("social_scholarship.experience_history.create", {
						experience_id: ctx.params.id,
						status: experience[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].experience_responsible.id,
						result[0].experience_responsible.name,
						result[0].uuid,
						result[0].translations[0].title,
					);
					return result;
				} else {
					const result = await ctx.call("social_scholarship.experiences.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: experience[0].status,
					});
					await ctx.call("social_scholarship.experience_history.create", {
						experience_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].experience_responsible.id,
						result[0].experience_responsible.name,
						result[0].uuid,
						result[0].translations[0].title,
					);
					return result;
				}
			},
		},

		"update-publish": {
			rest: "PUT /:id/change-publish",
			params: {
				id: { type: "number", integer: true, convert: true },
				published: { type: "boolean" },
			},
			scope: "social_scholarship:experiences:update",
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("social_scholarship.experiences.patch", {
					id: ctx.params.id,
					published: ctx.params.published,
				});
			},
		},

		update_closing_date: {
			rest: "POST /:id/change-closing-date",
			params: {
				id: { type: "number", integer: true, convert: true },
				new_end_date: { type: "date", convert: true },
			},
			scope: "social_scholarship:experiences:update",
			visibility: "published",
			async handler(ctx) {
				return await ctx
					.call("social_scholarship.experiences.get", { id: ctx.params.id })
					.then(async (experience) => {
						if (experience.length) {
							if (
								experience[0].status !== "CANCELLED" &&
								experience[0].status !== "CLOSED" &&
								experience[0].status !== "REJECTED"
							) {
								if (
									moment(ctx.params.new_end_date, "YYYY-MM-DD").isSameOrAfter(
										moment(new Date()).format("YYYY-MM-DD"),
									)
								) {
									return await ctx.call("social_scholarship.experiences.patch", {
										id: ctx.params.id,
										application_deadline_date: ctx.params.new_end_date,
									});
								} else {
									throw new ValidationError(
										"Experience closing date must be equal or after than today.",
										"SOCIAL_SCHOLARSHIP_EXPERIENCE_DATE_TODAY",
										{},
									);
								}
							} else {
								throw new ValidationError(
									"Experience not available to change the closing date.",
									"SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_AVAILABLE_CLOSING_DATE",
									{},
								);
							}
						} else {
							throw new ValidationError(
								"Experience not found.",
								"SOCIAL_SCHOLARSHIP_EXPERIENCE_NOT_FOUND",
								{},
							);
						}
					});
			},
		},

		// Generate experience offer report
		"generate-report-experience-offer": {
			rest: "GET /:id/generate-report-experience-offer",
			visibility: "published",
			scope: "social_scholarship:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });

				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					submission_date: moment(experience[0].created_at).format("YYYY-MM-DD"),
					collaboration_start_date: moment(experience[0].start_date).format("YYYY-MM-DD"),
					collaboration_end_date: moment(experience[0].end_date).format("YYYY-MM-DD"),
					publish_date: moment(experience[0].publish_date).format("YYYY-MM-DD"),
					experience_name:
						experience[0].translations.length > 0 ? experience[0].translations[0].title : null,
					experience_responsable:
						experience[0].experience_responsible === null
							? null
							: experience[0].experience_responsible.name,
					organic_unit:
						experience[0].organic_unit === null ? null : experience[0].organic_unit.name,
					active_collaborators: experience[0].requirement_number,
					necessary_collaborators: experience[0].number_simultaneous_candidates,
					status: experience[0].status,
				};

				return ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_EXPERIENCE_OFFER",
					options: {
						convertTo: "pdf",
					},
					data: data,
					name: "Experiência - Oferta - Bolsa de colaboradores (" + data.date + ")",
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"configuration.languages.*"(ctx) {
			this.logger.info("CAPTURED EVENT => configuration.languages.*");
			this.clearCache();
		},
		"social_scholarship.experience-user-interest.*"() {
			this.logger.info("CAPTURED EVENT => social_scholarship.experience-user-interest.updated");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Verify if exists repeated languages on translations list
		 * @param {*} list Translations list
		 * @param {*} id Language_id to compare if exists
		 */
		async isUniqueLanguage(list, id) {
			const results = await list.filter((x) => x.language_id == id);
			return results.length > 1;
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} experience_id
		 * @param {*} list
		 */
		async createWithRelated(ctx, service, experience_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const item of list) {
				item.experience_id = experience_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) {
				return;
			}
			for (const item of list) {
				await ctx.call(service, item);
			}
		},

		async saveCreateHistory(ctx, response) {
			response[0].history = [
				...(await ctx.call("social_scholarship.experience_history.create", {
					experience_id: response[0].id,
					status: response[0].status,
					user_id: ctx.meta.user.id,
					notes: null,
				})),
			];
			return response;
		},

		async saveUpdateHistory(ctx, response) {
			response[0].history = [
				...(await ctx.call("social_scholarship.experience_history.create", {
					experience_id: response[0].id,
					status: response[0].status,
					user_id: ctx.meta.user.id,
					notes: null,
				})),
			];
			return response;
		},
		async saveTranslations(ctx, response) {
			if (ctx.params.translations) {
				response[0].translations = await this.createWithRelated(
					ctx,
					"social_scholarship.translations.create",
					response[0].id,
					ctx.params.translations,
				);
			}
			return response;
		},

		async sendNotification(ctx, status, user_id, name, uuid, experience_name) {
			// Send notification to user
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_EXPERIENCE_STATUS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name, experience_name: experience_name },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});

			// Emails configured in EXPERIENCE_EMAILS are notified whenever that an offer is submitted
			if (status.toUpperCase() === "SUBMITTED") {
				ctx
					.call("social_scholarship.configurations.find", {
						query: {
							key: CONFIGURATION_KEYS.EXPERIENCE_EMAILS,
						},
					})
					.then((response) => {
						if (response.length) {
							response.map((user_id) => {
								ctx
									.call("authorization.users.get", {
										id: user_id,
									})
									.then((user) => {
										if (user.length) {
											ctx.call("notifications.alerts.create_alert", {
												alert_type_key: "SOCIAL_SUPPORT_NEW_EXPERIENCE_SUBMITTED",
												user_id: user_id,
												user_data: { email: user[0].email },
												data: { name: name },
												variables: {},
												external_uuid: uuid,
												medias: [],
											});
										}
									});
							});
						}
					});

				/*const experience_emails = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.EXPERIENCE_EMAILS,
					},
				});

				if (experience_emails.length) {
					const emailArray = [];
					experience_emails.map(user_id => {
						ctx.call("authorization.users.get", {
							id: user_id,
						}).then(user => emailArray.push(user[0].email))
					});
					//const emailArray = experience_emails[0].value.split(";");

					for (let i = 0; i < emailArray.length; i++) {
						await ctx.call("notifications.alerts.create_alert", {
							alert_type_key: "SOCIAL_SUPPORT_NEW_EXPERIENCE_SUBMITTED",
							user_id: user_id,
							user_data: { email: emailArray[i] },
							data: { name: name },
							variables: {},
							external_uuid: uuid,
							medias: [],
						});
					}
				}*/
			}
		},

		async sendNotificationUsers(ctx, user_id, name, experience_title, uuid) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_EXPERIENCE_SEND_TARGET_USERS",
				user_id: user_id,
				user_data: {},
				data: { name: name, title: experience_title },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});
		},

		async sendNotificationReturn(ctx, user_id, name, reason_return, uuid, experience_name) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_EXPERIENCE_STATUS_RETURNED",
				user_id: user_id,
				user_data: {},
				data: { name: name, reason_return: reason_return, experience_name: experience_name },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});
		},

		/**
		 * Validate configurations
		 * @param {*} ctx
		 */
		async validateHasCurrentAccount(ctx) {
			const has_current_account = await ctx.call("social_scholarship.configurations.find", {
				query: {
					key: CONFIGURATION_KEYS.HAS_CURRENT_ACCOUNT,
				},
			});

			if (ctx.meta.isBackoffice && has_current_account[0].value === "false") {
				ctx.params.perc_student_iban = 1;
				ctx.params.perc_student_ca = 0;
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}
					return value;
				`
			};
		});*/
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}
					return value;
				`
			};
		});*/
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
