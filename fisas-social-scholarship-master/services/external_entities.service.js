"use strict";
const { Errors } = require("@fisas/ms_core").Helpers;
const {  hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "social_scholarship.external_entities",
	table: "external_entity",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "external_entities")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"email",
			"gender",
			"address",
			"postal_code",
			"city",
			"country",
			"phone",
			"tin",
			"status",
			"active",
			"user_id",
			"file_id",
			"function_description",
			"description"
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			}
		},
		entityValidator: {
			name: { type: "string" },
			email: { type: "string" },
			gender: { type: "enum", values: ["M", "F", "U" ] },
			address: { type: "string" },
			postal_code: { type: "string" },
			city: { type: "string" },
			country: { type: "string" },
			phone: { type: "string" },
			tin: { type: "string" },
			status: { type: "enum", values: ["CREATED", "VALIDATED"] },
			active: { type: "boolean", default: true },
			user_id: { type: "number", integer: true, convert: true, optional: true, nullable: true },
			file_id : { type: "number", integer: true, convert: true, optional: true },
			function_description: { type: "string" },
			description: { type: "string" }
		}
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create: {
			visibility: "published",
			params: {
				name: { type: "string", max: 250, optional: false },
				email: { type: "email", optional: false },
				birth_date: { type: "date", convert: true, optional: true },
				gender: { type: "enum", values: ["M", "F", "U"] },
				address: { type: "string", max: 255, optional: false },
				postal_code: { type: "string", max: 10, optional: false },
				city: { type: "string", max: 250, optional: false },
				country: { type: "string", max: 250, optional: false },
				phone: { type: "string", max: 250, optional: false },
				tin: { type: "string", optional: false },
				file_id : { type: "number", integer: true, convert: true, optional: true },
				function_description : { type: "string" },
				description: { type: "string" }
			},
			scope: "social_scholarship:external_entities:create",
			async handler(ctx) {
				await this.validateEmail(ctx);
				const external_entity = {};
				external_entity.name = ctx.params.name;
				external_entity.email = ctx.params.email;
				external_entity.birth_date = ctx.params.birth_date ? ctx.params.birth_date : new Date();
				external_entity.gender = ctx.params.gender;
				external_entity.address = ctx.params.address;
				external_entity.postal_code = ctx.params.postal_code;
				external_entity.city = ctx.params.city;
				external_entity.country = ctx.params.country;
				external_entity.phone = ctx.params.phone;
				external_entity.tin = ctx.params.tin;
				external_entity.active = false;
				external_entity.status = "CREATED";
				external_entity.file_id = ctx.params.file_id;
				external_entity.function_description = ctx.params.function_description;
				external_entity.description = ctx.params.description;
				const external_entity_created =  await this._insert(ctx,  { entity : external_entity } );
				return external_entity_created;
			}
		},

		inactive: {
			rest: "POST /:id/inactive",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			visibility: "published",
			scope: "social_scholarship:external_entities:update",
			async handler(ctx){
				const external_entity = await this._get(ctx, { id: ctx.params.id });
				if(external_entity[0].status === "CREATED") {
					throw new Errors.ValidationError(
						"External entity has not validated",
						"SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_HAS_NOT_VALIDATED",
						{},
					);
				}
				await ctx.call("social_scholarship.external_entities.patch", { id: ctx.params.id, active: false });
				return ctx.call("authorization.users.change_status", { user_id: external_entity[0].user_id, active: false }).then(() => {
					this.clearCache();
					return this._get(ctx, { id: ctx.params.id });
				});
			}
		},

		active: {
			rest: "POST /:id/active",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			visibility: "published",
			scope: "social_scholarship:external_entities:update",
			async handler(ctx){
				const external_entity = await this._get(ctx, { id: ctx.params.id });
				if(external_entity[0].status === "CREATED") {
					throw new Errors.ValidationError(
						"External entity has not validated",
						"SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_HAS_NOT_VALIDATED",
						{},
					);
				}
				await ctx.call("social_scholarship.external_entities.patch", { id: ctx.params.id, active: true });
				return ctx.call("authorization.users.change_status", { user_id: external_entity[0].user_id, active: true }).then(() => {
					this.clearCache();
					return this._get(ctx, { id: ctx.params.id });
				});
			}
		},


		validate: {
			rest: "POST /:id/validate",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			visibility: "published",
			scope: "social_scholarship:external_entities:update",
			async handler(ctx) {
				const external_entetie = await this._get(ctx, { id: ctx.params.id });
				if(external_entetie[0].status === "VALIDATED"){
					throw new Errors.ValidationError(
						"External entity already is validated",
						"SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_ALREADY_IS_VALIDATED",
						{},
					);
				}
				let user = {};
				user.user_name = external_entetie[0].email;
				user.name = external_entetie[0].name;
				user.email = external_entetie[0].email;
				user.birth_date = user.birth_date ? user.birth_date : new Date();
				user.gender = external_entetie[0].gender;
				user.address = external_entetie[0].address;
				user.postal_code = external_entetie[0].postal_code;
				user.city = external_entetie[0].city;
				user.country = external_entetie[0].country;
				user.phone = external_entetie[0].phone;
				user.tin = external_entetie[0].tin;
				user.can_access_BO = false;
				user.external = true;
				user.profile_id = await this.getProfileId(ctx);
				user.active = external_entetie[0].active;
				return await ctx.call("authorization.users.create", user).then((user_created) => {
					this.clearCache();
					return ctx.call("social_scholarship.external_entities.patch", { id: ctx.params.id, user_id: user_created[0].id, status: "VALIDATED", active: true });
				}).catch(() => {
					throw new Errors.ValidationError(
						"Error create user",
						"SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_CREATE_USER_ERROR",
						{},
					);

				});

			}
		},
		update: {
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true, positive: true },
				name: { type: "string", max: 250, optional: false },
				email: { type: "email", optional: false },
				gender: { type: "enum", values: ["M", "F", "U"] },
				address: { type: "string", max: 255, optional: false },
				postal_code: { type: "string", max: 10, optional: false },
				city: { type: "string", max: 250, optional: false },
				country: { type: "string", max: 250, optional: false },
				phone: { type: "string", max: 250, optional: false },
				tin: { type: "string", optional: false },
				file_id: { type: "number", integer: true, convert: true, optional: true },
				function_description: { type: "string" },
				description: { type: "string" }
			},
			async handler(ctx) {
				const external_entity = await this._get(ctx, { id: ctx.params.id });
				if (external_entity[0].status === "VALIDATED") {
					if (ctx.params.email) {
						delete ctx.params.email;
					}
					external_entity[0].name = ctx.params.name;
					external_entity[0].gender = ctx.params.gender;
					external_entity[0].address = ctx.params.address;
					external_entity[0].postal_code = ctx.params.postal_code;
					external_entity[0].city = ctx.params.city;
					external_entity[0].country = ctx.params.country;
					external_entity[0].phone = ctx.params.phone;
					external_entity[0].tin = ctx.params.tin;
					external_entity[0].file_id = ctx.params.file_id ? ctx.params.file_id : external_entity[0].file_id;
					external_entity[0].function_description = ctx.params.function_description;
					external_entity[0].description = ctx.params.description;
					await this._update(ctx, external_entity[0]);
					return await ctx.call("authorization.users.patch", {
						id: external_entity[0].user_id,
						name: ctx.params.name,
						gender: ctx.params.gender,
						address: ctx.params.address,
						postal_code: ctx.params.postal_code,
						city: ctx.params.city,
						country: ctx.params.country,
						phone: ctx.params.phone,
						tin: ctx.params.tin,
					}).then(() => {
						this.clearCache();
						return this._get(ctx, { id: ctx.params.id });
					});
				} else {
					external_entity[0].name = ctx.params.name;
					external_entity[0].email = ctx.params.email;
					external_entity[0].gender = ctx.params.gender;
					external_entity[0].address = ctx.params.address;
					external_entity[0].postal_code = ctx.params.postal_code;
					external_entity[0].city = ctx.params.city;
					external_entity[0].country = ctx.params.country;
					external_entity[0].phone = ctx.params.phone;
					external_entity[0].tin = ctx.params.tin;
					external_entity[0].file_id = ctx.params.file_id ? ctx.params.file_id : external_entity[0].file_id;
					external_entity[0].function_description = ctx.params.function_description;
					external_entity[0].description = ctx.params.description;
					return await this._update(ctx, external_entity[0]);
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

		async validateEmail(ctx){
			const find_email = await this._find(ctx, { query: {
				email: ctx.params.email
			} });
			if(find_email.length > 0 ){
				throw new Errors.ValidationError(
					"The email already exist.",
					"SOCIAL_SCHOLARSHIP_EXTERNAL_ENTITY_EMAIL_ALREADY_EXIST",
					{},
				);
			}
		},

		makeid(length) {
			let result = "";
			const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			const charactersLength = characters.length;
			for (let i = 0; i < length; i++) {
				result += characters.charAt(Math.floor(Math.random() * charactersLength));
			}
			return result;
		},
		async getProfileId(ctx) {
			const profile = await ctx.call("social_scholarship.configurations.getExternalEntityProfileId");
			if (profile.length > 0) {
				return +profile[0].value;
			}
			throw new Errors.ValidationError(
				"The configuration for external profile doesn't found.",
				"SOCIAL_SCHOLARSHIP_CONFIGURATION_PROFILEID_NOT_FOUND",
				{},
			);
		},
		async sendNotification(ctx, user_id, name, email, password) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "SOCIAL_SUPPORT_EXTERNAL_ENTITY_CREATED",
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name, email: email, password: password },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
