"use strict";

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "social_scholarship.general-reports",
	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		"general-report-by-user-id": {
			rest: "GET /user/:id",
			visibility: "published",
			scope: "social_scholarship:general-reports:report_by_user_id",
			params: {
				id: { type: "string", convert: true },
			},
			async handler(ctx) {
				let totalHoursExperience = 0;
				const appIds = [];

				const closedApps = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: { status: "CLOSED", user_id: ctx.params.id },
					withRelated: ["experience"],
				});
				const activeApps = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: { status: "COLABORATION", user_id: ctx.params.id },
					withRelated: ["experience"],
				});

				for (const app of closedApps) {
					totalHoursExperience +=
						app.experience != null ? app.experience.total_hours_estimation : 0;
					appIds.push(app.id);
				}
				for (const app of activeApps) {
					totalHoursExperience +=
						app.experience != null ? app.experience.total_hours_estimation : 0;
					appIds.push(app.id);
				}

				let totalHoursAttendance = 0;
				const attIds = [];
				const appAttendances = await ctx.call("social_scholarship.attendances.find", {
					query: { user_interest_id: appIds, status: ["PENDING", "ACCEPTED", "REJECTED", "LACK"] },
				});
				for (const attendence of appAttendances) {
					attIds.push(attendence.id);
					if (attendence.was_present) {
						totalHoursAttendance = this.calculate_sum_hours_to_minutes(
							totalHoursAttendance,
							attendence.n_hours,
						);
					}
				}

				let totalAbsencesWithReason = 0;
				let totalAbsencesWithoutReason = 0;

				const appAbsences = await ctx.call("social_scholarship.absence_reason.find", {
					query: { experience_attendance_id: attIds },
				});

				for (const absence of appAbsences) {
					if (absence.accept) {
						totalAbsencesWithReason += 1;
					} else {
						totalAbsencesWithoutReason += 1;
					}
				}

				return {
					total_hours_experience: totalHoursExperience,
					total_hours_attendance: this.format_minutes_to_hours(totalHoursAttendance),
					total_absences_with_accepted_reason: totalAbsencesWithReason,
					total_absences_without_accepted_reason: totalAbsencesWithoutReason,
				};
			},
		},

		"general-report-by-month-and-experience": {
			rest: "GET /:user_id/monthly-statistics/:experience_id",
			visibility: "published",
			scope: "social_scholarship:general-reports:report_by_month_and_experience",
			params: {
				user_id: { type: "string", convert: true },
				experience_id: { type: "string", convert: true },
			},
			async handler(ctx) {
				const dl = require("datalib");
				let attendancesPresent = [];
				const attendancesPresentIds = [];
				let attendancesNotPresent = [];
				const attendancesNotPresentIds = [];
				let attendancesWithAbsenceReasonAccepted = [];
				let attendancesWithAbsenceReasonNotAccepted = [];
				let attendancesWithoutAbsenceReason = [];
				const appIds = [];
				const closedApps = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: {
						status: "CLOSED",
						user_id: ctx.params.user_id,
						experience_id: ctx.params.experience_id,
					},
				});
				const activeApps = await ctx.call("social_scholarship.experience-user-interest.find", {
					query: {
						status: "COLABORATION",
						user_id: ctx.params.user_id,
						experience_id: ctx.params.experience_id,
					},
				});
				for (const app of closedApps) {
					appIds.push(app.id);
				}
				for (const app of activeApps) {
					appIds.push(app.id);
				}
				const appAttendances = await ctx.call("social_scholarship.attendances.find", {
					query: { user_interest_id: appIds, status: ["ACCEPTED", "REJECTED", "LACK"] },
				});

				for (const attendence of appAttendances) {
					if (attendence.was_present) {
						attendancesPresentIds.push(attendence.id);
						attendence.date = new Date(attendence.date);
						attendancesPresent.push(attendence);
					} else {
						attendancesNotPresentIds.push(attendence.id);
						attendence.date = new Date(attendence.date);
						attendancesNotPresent.push(attendence);
					}
				}
				const appAbsences = await ctx.call("social_scholarship.absence_reason.find", {
					query: { experience_attendance_id: attendancesNotPresentIds },
				});
				for (const absence of appAbsences) {
					if (absence.accept) {
						attendancesWithAbsenceReasonAccepted.push(absence);
					} else {
						attendancesWithAbsenceReasonNotAccepted.push(absence);
					}
				}

				// Accessors for dataset
				let attMonthDate = dl.$month("date");
				let attYearDate = dl.$year("date");
				// Summarize dataset for the provided data - attendancesPresent
				const attendancesPresentAggData = dl
					.groupby("application_id", attYearDate, attMonthDate)
					.summarize([
						{
							name: "id",
							ops: ["count"],
							as: ["count_attendances"],
						},
						{
							name: "n_hours",
							ops: ["sum"],
							as: ["total_hours"],
						},
					])
					.execute(dl.read(attendancesPresent));
				// Summarize dataset for the provided data - attendancesWithAbsenceReasonAccepted
				const attendancesWithAbsenceReasonAcceptedAggData = dl
					.groupby("application_id", attYearDate, attMonthDate)
					.summarize({
						attendances: ["count"],
					})
					.execute(dl.read(attendancesWithAbsenceReasonAccepted));
				// Summarize dataset for the provided data - attendancesWithoutAbsenceReasonOrNotAcceptedAggData

				const attendancesWithoutAbsenceReasonOrNotAcceptedAggData = dl
					.groupby("application_id", attYearDate, attMonthDate)
					.summarize({
						attendances: ["count"],
					})
					.execute(
						dl.read(
							attendancesWithAbsenceReasonNotAccepted.concat(attendancesWithoutAbsenceReason),
						),
					);

				for (const iterator of attendancesPresentAggData) {
					iterator.month_date = iterator.month_date + 1;
				}
				const attendancesPresentAux = attendancesPresentAggData;

				for (const iterator of attendancesWithAbsenceReasonAcceptedAggData) {
					iterator.month_date = iterator.month_date + 1;
				}
				const attendancesWithAbsenceReasonAcceptedAux = attendancesWithAbsenceReasonAcceptedAggData;
				for (const iterator of attendancesWithoutAbsenceReasonOrNotAcceptedAggData) {
					iterator.month_date = iterator.month_date + 1;
				}
				const attendancesWithoutAbsenceReasonOrNotAcceptedAux = attendancesWithoutAbsenceReasonOrNotAcceptedAggData;

				let found;

				attendancesPresentAux.forEach((attendance) => {
					found = attendancesWithAbsenceReasonAcceptedAux.find(
						(absenceReasonAccepted) =>
							attendance.month_date === absenceReasonAccepted.month_date &&
							attendance.year_date === absenceReasonAccepted.year_date,
					);
					attendance.count_absence_reason_accepted =
						found !== undefined ? found.count_attendances : 0;

					found = attendancesWithoutAbsenceReasonOrNotAcceptedAux.find(
						(absenceReasonNotAccepted) =>
							attendance.month_date === absenceReasonNotAccepted.month_date &&
							attendance.year_date === absenceReasonNotAccepted.year_date,
					);
					attendance.count_absence_reason_not_accepted =
						found !== undefined ? found.count_attendances : 0;
				});

				return attendancesPresentAux;
			},
		},
		"general-report-by-user_interest": {
			rest: "GET /user-interest/:id",
			visibility: "published",
			scope: "social_scholarship:general-reports:report_by_user_interest",
			params: {
				id: { type: "string", convert: true },
			},
			async handler(ctx) {
				const app = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: ctx.params.id,
					withRelated: ["experience"],
				});
				const totalHoursExperience =
					app[0].experience != null ? app[0].experience.total_hours_estimation : 0;
				let totalHoursAttendance = 0;
				const attendances = await ctx.call("social_scholarship.attendances.find", {
					query: {
						user_interest_id: app[0].id,
						status: ["PENDING", "ACCEPTED", "REJECTED", "LACK"],
					},
				});
				let attIds = [];
				for (const att of attendances) {
					attIds.push(att.id);
					if (att.was_present) {
						totalHoursAttendance = this.calculate_sum_hours_to_minutes(
							totalHoursAttendance,
							att.n_hours,
						);
					}
				}
				let totalHoursValidated = 0;
				let totalHoursReject = 0;
				for (const attvalidated of attendances) {
					if (attvalidated.status === "ACCEPTED" || attvalidated.status === "REJECTED") {
						totalHoursValidated = this.calculate_sum_hours_to_minutes(
							totalHoursValidated,
							attvalidated.n_hours,
						);

						if (attvalidated.status === "REJECTED") {
							totalHoursReject = this.calculate_sum_hours_to_minutes(
								totalHoursReject,
								attvalidated.n_hours_reject,
							);
						}
					}
				}
				let totalAbsencesWithReason = 0;
				let totalAbsencesWithoutReason = 0;

				const absences = await ctx.call("social_scholarship.absence_reason.find", {
					query: { experience_attendance_id: attIds },
				});
				for (const absence of absences) {
					if (absence.accept) {
						totalAbsencesWithReason += 1;
					} else {
						totalAbsencesWithoutReason += 1;
					}
				}
				return {
					total_hours_experience: totalHoursExperience,
					total_hours_attendance: this.format_minutes_to_hours(totalHoursAttendance),
					total_hours_validated: this.format_minutes_to_hours(
						totalHoursValidated - totalHoursReject,
					),
					total_absences_with_accepted_reason: totalAbsencesWithReason,
					total_absences_without_accepted_reason: totalAbsencesWithoutReason,
				};
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Calculate sum hours to minutes
		 * @param {*} valor_inicial
		 * @param {*} valor
		 * @returns
		 */
		calculate_sum_hours_to_minutes(valor_inicial, valor) {
			const valor_string = valor.toString();
			const valor_split = valor_string.split(".");
			if (valor_split.length >= 2) {
				const hours = parseInt(valor_split[0]);
				let minutes = 0;
				if (valor_split[1].length === 1) {
					minutes = parseInt(valor_split[1]) * 10;
				} else {
					minutes = parseInt(valor_split[1]);
				}
				valor_inicial += hours * 60;
				valor_inicial += minutes;
			} else {
				valor_inicial += parseInt(valor_string) * 60;
			}

			return valor_inicial;
		},
		/**
		 * Format minutes to hours (hh.mm)
		 * @param {*} total_minutes
		 * @returns
		 */
		format_minutes_to_hours(total_minutes) {
			const hours = Math.floor(total_minutes / 60);
			const minutes = total_minutes % 60;
			const a = (hours < 10 ? "0" + hours : hours) + "." + (minutes < 10 ? "0" + minutes : minutes);
			return parseFloat(parseFloat(a).toFixed(2));
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
