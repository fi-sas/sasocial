"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.general-complains",
	table: "general-complain",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "general-complains")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"user_id",
			"complain",
			"file_id",
			"response",
			"status",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["file", "user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", integer: true, convert: true },
			complain: { type: "string" },
			file_id : { type: "number", integer: true, optional: true, nullable: true },
			response: { type: "string", optional: true, nullable: true },
			status: { type: "enum", values : ["SUBMITTED", "ANALYSED", "REPLYED"] },
			created_at: { type: "date", optional: true, convert: true },
			updated_at: { type: "date", optional: true, convert: true }
		}
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.status = "SUBMITTED";
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},

		analyse : {
			rest: "POST /:id/analyse",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			scope: "social_scholarship:general-complains:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0] !== "REPLYED"){
					complain[0].status = "ANALYSED";
					return await this._update(ctx, complain[0]);
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"SOCIAL_SHOLARSHIP_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		},

		response : {
			rest: "POST /:id/response",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				response: { type: "string" }
			},
			scope: "social_scholarship:general-complains:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0].status !== "REPLYED"){
					complain[0].status = "REPLYED";
					complain[0].response = ctx.params.response;
					const updated = await this._update(ctx, complain[0]);
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "SOCIAL_SUPPORT_COMPLAIN",
						user_id: complain[0].user_id,
						user_data: {},
						data: { name: complain[0].user.name, response : ctx.params.response },
						variables: {},
						external_uuid: null,
						medias: [],
					});
					return updated;
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"SOCIAL_SHOLARSHIP_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {


	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
