"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const _ = require("lodash");
const { CONFIGURATION_KEYS } = require("./utils/constants.js");
const moment = require("moment");
moment.locale("pt");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.monthly-reports",
	table: "experience_monthly_report",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "monthly-reports")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"month",
			"year",
			"report",
			"file_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			user_interest_id: "number|integer|convert",
			month: "number|integer|convert",
			year: "number|integer|convert",
			report: "string|optional",
			file_id: "number|integer|convert|optional|nullable",
			created_at: "date|convert",
			updated_at: "date|convert",
		},
	},
	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					await ctx.call("social_scholarship.experience-user-interest.get", {
						id: ctx.params.user_interest_id,
					});
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					const monthly_report = await this._find(ctx, {
						query: {
							user_interest_id: ctx.params.user_interest_id,
							month: ctx.params.month,
							year: ctx.params.year,
						},
					});
					if (monthly_report.length > 0) {
						throw new Errors.ValidationError(
							"Already exist monthly report for send month, year and user_interest",
							"SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_ERROR",
							{},
						);
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function validateAccess(ctx) {
					if (!ctx.meta.isBackoffice) {
						if (ctx.params.query) {
							if (ctx.params.query.user_interest_id) {
								const user_interest = await ctx.call(
									"social_scholarship.experience-user-interest.get",
									{ id: ctx.params.query.user_interest_id, withRelated: "experience" },
								);
								if (
									user_interest[0].user_id !== ctx.meta.user.id &&
									user_interest[0].experience.experience_advisor_id !== ctx.meta.user.id &&
									user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id
								) {
									throw new Errors.ForbiddenError(
										"Unauthorized access to data",
										"SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_FORBIDDEN",
										{},
									);
								}
							}
							if (!ctx.params.query.user_interest_id) {
								throw new Errors.ForbiddenError(
									"Unauthorized access to data",
									"SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_FORBIDDEN",
									{},
								);
							}
						}
						if (!ctx.params.query) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_FORBIDDEN",
								{},
							);
						}
					}
				},
			],
		},
		after: {
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						const user_interest = await ctx.call(
							"social_scholarship.experience-user-interest.get",
							{ id: resp[0].user_interest_id, withRelated: "experience" },
						);
						if (
							user_interest[0].user_id !== ctx.meta.user.id &&
							user_interest[0].experience.experience_advisor_id !== ctx.meta.user.id &&
							user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id
						) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"SOCIAL_SCHOLARSHIP_MONTHLY_REPORT_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "#isBackoffice", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		generateReportHours: {
			visibility: "published",
			rest: "GET /generate_hours/:experience_id/:user_interest_id/:month/:year",
			scope: "social_scholarship:monthly-reports:list",
			params: {
				experience_id: "number|integer|convert",
				user_interest_id: "number|integer|convert",
				month: "number|integer|convert",
				year: "number|integer|convert",
			},
			async handler(ctx) {
				const month_year = ctx.params.year + "-" + ctx.params.month;

				ctx.params.start_date = moment(month_year).startOf("month").format("YYYY-MM-DD");
				ctx.params.end_date = moment(month_year).endOf("month").format("YYYY-MM-DD");

				let experience = await this.getExperience(ctx, ctx.params.experience_id);

				let experience_user = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: ctx.params.user_interest_id,
				});

				const application_user = await ctx.call("social_scholarship.applications.find", {
					query: {
						academic_year: experience.academic_year,
						status: "ACCEPTED",
						user_id: experience_user[0].user_id,
					},
				});

				let application = await this.getApplication(ctx, application_user[0].id);

				let attendances = await this.getAttendances(ctx, experience_user[0].id);

				let all_attendances = await this.getAllAttendances(ctx, experience_user[0].id);

				await this.setMonthAndDayAttendancesHours(attendances);
				let totalHours_validated = this.calculateAttendancesHours(attendances);

				let totalHours = this.calculateAttendancesHours(all_attendances);

				const data = {
					experience: experience,
					application: application,
					attendances: attendances,
					all_attendances: all_attendances,
					totalHoursValidated: this.format_minutes_to_hours(totalHours_validated),
					totalHours: this.format_minutes_to_hours(totalHours),
					date: moment().format("DD/MM/YYYY"),
				};
				return data;
			},
		},

		generateFileReportHours: {
			visibility: "published",
			rest: "POST /generate_hours_file/:experience_id/:user_interest_id/:month/:year",
			params: {
				experience_id: "number|integer|convert",
				user_interest_id: "number|integer|convert",
				month: "number|integer|convert",
				year: "number|integer|convert",
			},
			scope: "social_scholarship:monthly-reports:list",
			async handler(ctx) {
				const month_year = ctx.params.year + "-" + ctx.params.month;

				ctx.params.start_date = moment(month_year).startOf("month").format("YYYY-MM-DD");
				ctx.params.end_date = moment(month_year).endOf("month").format("YYYY-MM-DD");

				let experience = await this.getExperience(ctx, ctx.params.experience_id);

				let experience_user = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: ctx.params.user_interest_id,
				});

				const application_user = await ctx.call("social_scholarship.applications.find", {
					query: {
						academic_year: experience.academic_year,
						status: "ACCEPTED",
						user_id: experience_user[0].user_id,
					},
				});

				let application = await this.getApplication(ctx, application_user[0].id);

				let attendances = await this.getAttendances(ctx, experience_user[0].id);

				await this.setMonthAndDayAttendancesHours(attendances);
				let totalHours = this.calculateAttendancesHours(attendances);

				let formated_attendances = [];
				for (let attendance of attendances) {
					attendance.month = moment(attendance.date).format("M");
					attendance.day = moment(attendance.date).format("D");
					attendance.initial_time = moment(attendance.initial_time).format("DD/MM/YYYY hh:mm:ss");
					attendance.final_time = moment(attendance.final_time).format("DD/MM/YYYY hh:mm:ss");
					formated_attendances.push(attendance);
				}
				const data = {
					month_year: month_year,
					experience: experience,
					application: application,
					attendances: formated_attendances,
					totalHours: this.format_minutes_to_hours(totalHours),
					date: moment().format("DD/MM/YYYY"),
				};
				return this.generateReport(ctx, data);
			},
		},

		"generate-template-month": {
			rest: "POST /:id/generate-report",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const monthly_report = await this._get(ctx, { id: ctx.params.id });
				const month_year = monthly_report[0].year + "-" + monthly_report[0].month;
				ctx.params.start_date = moment(month_year).startOf("month").format("YYYY-MM-DD");
				ctx.params.end_date = moment(month_year).endOf("month").format("YYYY-MM-DD");
				const user_interest = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: monthly_report[0].user_interest_id,
					withRelated: false,
				});
				let experience = await this.getExperience(ctx, user_interest[0].experience_id);

				let experience_user = await ctx.call("social_scholarship.experience-user-interest.get", {
					id: monthly_report[0].user_interest_id,
				});

				const application_user = await ctx.call("social_scholarship.applications.find", {
					query: {
						academic_year: experience.academic_year,
						status: "ACCEPTED",
						user_id: experience_user[0].user_id,
					},
				});

				let application = await this.getApplication(ctx, application_user[0].id);

				let attendances = await this.getAttendances(ctx, experience_user[0].id);

				await this.setMonthAndDayAttendancesHours(attendances);
				let totalHours = this.calculateAttendancesHours(attendances);

				let formated_attendances = [];
				for (let attendance of attendances) {
					attendance.month = moment(attendance.date).format("M");
					attendance.day = moment(attendance.date).format("D");
					attendance.initial_time = moment(attendance.initial_time).format("DD/MM/YYYY hh:mm:ss");
					attendance.final_time = moment(attendance.final_time).format("DD/MM/YYYY hh:mm:ss");
					formated_attendances.push(attendance);
				}
				const data = {
					month_year: month_year,
					experience: experience,
					application: application,
					attendances: formated_attendances,
					totalHours: this.format_minutes_to_hours(totalHours),
					report: monthly_report[0].report,
					date: moment().format("DD/MM/YYYY"),
				};

				return ctx.call("reports.templates.print", {
					key: "SOCIAL_SCHOLARSHIP_MONTHLY_REPORT",
					data: data,
					name:
						"Report Mensal - Bolsa de colaboradores" + "(" + moment().format("DD/MM/YYYY") + ")",
				});
			},
		},

		get_alert_month: {
			rest: "GET /:user_interest_id/alerts",
			visibility: "published",
			scope: "social_scholarship:monthly-reports:list",
			params: {
				user_interest_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				let thisMoment = moment();
				let endOfMonth = moment(thisMoment).endOf("month").format("YYYY-MM-DD");
				let startOfMonth = moment(thisMoment).startOf("month").format("YYYY-MM-DD");
				const current_attendances = await this.attendancesForAlert(
					ctx,
					ctx.params.user_interest_id,
					startOfMonth,
					endOfMonth,
				);
				let total_current_month = 0;
				for (const attendance of current_attendances) {
					total_current_month = this.calculate_sum_hours_to_minutes(
						total_current_month,
						attendance.n_hours,
					);
				}
				const last_month = moment().subtract(1, "month");
				let endOfLastMonth = moment(last_month).endOf("month").format("YYYY-MM-DD");
				let startOfLastMonth = moment(last_month).startOf("month").format("YYYY-MM-DD");
				const last_month_attendances = await this.attendancesForAlert(
					ctx,
					ctx.params.user_interest_id,
					startOfLastMonth,
					endOfLastMonth,
				);
				let total_last_month = 0;
				for (const attendance of last_month_attendances) {
					total_last_month = this.calculate_sum_hours_to_minutes(
						total_last_month,
						attendance.n_hours,
					);
				}
				const max_hours = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.MAX_HOURS_WORK,
					},
				});
				let value_alert = 0;
				if (max_hours.length > 0) {
					value_alert = max_hours[0].value;
				}
				const response = {
					last_month: {
						total_hours: this.format_minutes_to_hours(total_last_month),
						alert:
							value_alert > 0
								? this.format_minutes_to_hours(total_last_month) > value_alert
								: false,
					},
					current_month: {
						total_hours: this.format_minutes_to_hours(total_current_month),
						alert:
							value_alert > 0
								? this.format_minutes_to_hours(total_current_month) > value_alert
								: false,
					},
				};
				return response;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Get the application by id
		 * @param {object} ctx
		 */
		async getApplication(ctx, id) {
			let application = await ctx.call("social_scholarship.applications.get", {
				id: id,
				withRelated: ["course", "user"],
			});
			return application[0];
		},
		/**
		 * Get the experience from the application
		 * @param {object} ctx
		 * @param {integer} experience_id
		 */
		async getExperience(ctx, experience_id) {
			if (experience_id !== null) {
				let experience = await ctx.call("social_scholarship.experiences.get", {
					id: experience_id,
					withRelated: [
						"experience_advisor",
						"organic_unit",
						"experience_responsible",
						"translations",
					],
				});
				return this.filterExperienceData(_.first(experience));
			} else {
				throw new Errors.ValidationError(
					"The application don't have assigned experience!",
					"APPLICATION_WITHOUT_EXPERIENCE",
					{
						application_id: ctx.params.application_id,
					},
				);
			}
		},
		/**
		 * Get the attendances from the application id
		 * @param {object} ctx
		 */
		async getAttendances(ctx, user_interest_id) {
			return ctx.call("social_scholarship.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: ctx.params.start_date,
				status: ["ACCEPTED", "REJECTED"],
				end_date: moment(ctx.params.end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},

		async getAllAttendances(ctx, user_interest_id) {
			return ctx.call("social_scholarship.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: ctx.params.start_date,
				status: ["ACCEPTED", "REJECTED", "PENDING"],
				end_date: moment(ctx.params.end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},
		/**
		 * Filter the application fields needed to generate the report
		 * @param {object} application
		 */
		filterApplicationData(application) {
			let applicationData = {};
			let course = {};
			let courseDegree = {};
			let user = {};

			applicationData = Object.assign(
				applicationData,
				this.getObjectFields(application, [
					"id",
					"experience_id",
					"academic_year",
					"course_year",
					"iban",
				]),
			);

			user = Object.assign(
				user,
				this.getObjectFields(application.user, [
					"id",
					"name",
					"email",
					"phone",
					"student_number",
					"tin",
					"identification",
					"gender",
				]),
			);

			course = Object.assign(
				course,
				this.getObjectFields(application.course, ["id", "name", "acronym"]),
			);
			courseDegree = Object.assign(
				courseDegree,
				this.getObjectFields(application.course.courseDegree, ["id", "name"]),
			);

			course.courseDegree = courseDegree;
			applicationData.course = course;
			applicationData.user = user;

			return applicationData;
		},
		/**
		 * Filter the experience fields needed to generate the report
		 * @param {object} experience
		 */
		filterExperienceData(experience) {
			let experienceData = {};
			let organicUnit = {};
			let experienceResponsible = {};

			experienceData = Object.assign(
				experienceData,
				this.getObjectFields(experience, [
					"id",
					"translations",
					"academic_year",
					"address",
					"total_hours_estimation",
					"number_weekly_hours",
					"start_date",
					"end_date",
				]),
			);

			organicUnit = Object.assign(
				organicUnit,
				this.getObjectFields(experience.organic_unit, ["id", "name", "code"]),
			);

			experienceResponsible = Object.assign(
				experienceResponsible,
				this.getObjectFields(experience.experience_responsible, [
					"id",
					"name",
					"email",
					"phone",
					"gender",
				]),
			);

			experienceData.organic_unit = organicUnit;
			experienceData.experience_responsible = experienceResponsible;
			experienceData.start_date = moment(experience.start_date).format("DD/MM/YYYY");
			experienceData.end_date = moment(experience.end_date).format("DD/MM/YYYY");
			experienceData.experience_advisor = experience.experience_advisor;
			experienceData.payment_value = experience.payment_value;

			return experienceData;
		},
		/**
		 * Set month in text and day from the date attendance
		 * @param {array} attendances
		 */
		async setMonthAndDayAttendancesHours(attendances) {
			return attendances.forEach((attendance) => {
				attendance.month = moment(attendance.date).format("MMMM").toUpperCase();
				attendance.day = moment(attendance.date).format("DD");
			});
		},
		/**
		 * Calculate the total hours of attendances
		 * @param {array} attendances
		 */
		calculateAttendancesHours(attendances) {
			let valor_inicial = 0;
			for (const attendance of attendances) {
				valor_inicial = this.calculate_sum_hours_to_minutes(valor_inicial, attendance.n_hours);
			}
			return valor_inicial;
		},
		/**
		 * Formate minutes to hours (hh.mm)
		 * @param {*} total_minutes
		 * @returns
		 */
		format_minutes_to_hours(total_minutes) {
			const a = Math.floor(total_minutes / 60) + "." + (total_minutes % 60);
			return parseFloat(parseFloat(a).toFixed(2));
		},
		/**
		 * Calculate sum hours to minutes
		 * @param {*} valor_inicial
		 * @param {*} valor
		 * @returns
		 */
		calculate_sum_hours_to_minutes(valor_inicial, valor) {
			const valor_string = valor.toString();
			const valor_split = valor_string.split(".");
			if (valor_split.length >= 2) {
				const hours = parseInt(valor_split[0]);
				let minutes = 0;
				if (valor_split[1].length === 1) {
					minutes = parseInt(valor_split[1]) * 10;
				} else {
					minutes = parseInt(valor_split[1]);
				}
				valor_inicial += hours * 60;
				valor_inicial += minutes;
			} else {
				valor_inicial += parseInt(valor_string) * 60;
			}

			return valor_inicial;
		},
		/**
		 * Generate the file report
		 * @param {object} ctx
		 * @param {object} data
		 */
		async generateReport(ctx, data) {
			/**
			 * Template to crete in reports automatically/*TODO
			 * {
				"key": "SOCIAL_SCHOLARSHIP_HOURS_REPORT",
				"name": "Horas Efectuadas - Bolsa de colaboradores",
				"description": "Horas efectuadas pelo aluno na bolsa de colaboradores",
				"file_id": 0
			   }
			 */
			return ctx.call("reports.templates.print", {
				key: "SOCIAL_SCHOLARSHIP_HOURS_REPORT",
				data: data,
				name:
					"Horas Efectuadas - Bolsa de colaboradores" + "(" + moment().format("DD/MM/YYYY") + ")",
			});
		},
		/**
		 * Gets the object properties selected in the fields
		 * @param {object} object
		 * @param {array} fields
		 */
		getObjectFields(object, fields) {
			let newObject = {};
			fields.forEach((key) => (newObject[key] = object[key]));
			return newObject;
		},

		async attendancesForAlert(ctx, user_interest_id, start_date, end_date) {
			return ctx.call("social_scholarship.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: start_date,
				status: ["ACCEPTED", "REJECTED", "PENDING"],
				end_date: moment(end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
