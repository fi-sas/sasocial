"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {
	PAYMENT_GRID_MODEL,
	PAYMENT_GRID_STATUS,
	CONFIGURATION_KEYS,
} = require("./utils/constants.js");
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
moment.locale("pt");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.payment-grid",
	table: "payment_grid",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "payment-grid")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"payment_month",
			"payment_year",
			"payment_model",
			"payment_value",
			"number_hours",
			"payment_value_iban",
			"payment_value_ca",
			"user_account_id",
			"status",
			"notes",
			"hours_pay",
			"hours_transit",
			"iban",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			user_interest(ids, applications, rule, ctx) {
				return Promise.all(
					applications.map((application) => {
						return ctx
							.call("social_scholarship.experience-user-interest.get", {
								id: application.user_interest_id,
								withRelated: ["experience", "application"],
							})
							.then((user_interest) => (application.user_interest = user_interest[0]));
					}),
				);
			},
			user_account(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "current_account.accounts", "user_account", "user_account_id");
			},
		},
		entityValidator: {
			user_interest_id: "number|integer|convert",
			payment_month: "number|integer|convert",
			payment_year: "number|integer|convert",
			payment_model: /*"string|max:2"*/ { type: "enum", values: Object.keys(PAYMENT_GRID_MODEL) },
			payment_value: "number|convert",
			number_hours: "number|convert",
			payment_value_iban: "number|convert",
			payment_value_ca: "number|convert",
			status: /*"string|max:8"*/ { type: "enum", values: Object.keys(PAYMENT_GRID_STATUS) },
			user_account_id: { type: "number", optional: true, convert: true },
			hours_pay: "number|convert",
			hours_transit: "number|convert",
			notes: "string|optional",
			iban: "string|optional|nullable",
			created_at: "date|convert|optional",
			updated_at: "date|convert|optional",
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "PENDING";
				},
			],
			list: [
				async function sanatizeParams(ctx) {
					if (ctx.params.query) {
						if (ctx.params.query.isAdvisor) {
							const experiences = await ctx.call("social_scholarship.experiences.find", {
								query: {
									experience_advisor_id: ctx.meta.user.id,
									status: "IN_COLABORATION",
								},
							});
							const a = experiences.map((exp) => exp.id);
							const user_interests = await ctx.call(
								"social_scholarship.experience-user-interest.find",
								{
									query: {
										experience_id: a,
										status: "COLABORATION",
									},
								},
							);
							ctx.params.query["user_interest_id"] = user_interests.map((z) => z.id);
							delete ctx.params.query.isAdvisor;
						}
						if (ctx.params.query.experience_id) {
							const user_interests = await ctx.call(
								"social_scholarship.experience-user-interest.find",
								{
									query: {
										experience_id: ctx.params.query.experience_id,
										status: "COLABORATION",
									},
								},
							);
							ctx.params.query["user_interest_id"] = user_interests.map((z) => z.id);
							delete ctx.params.query.experience_id;
						}
						if (ctx.params.query.experience_id && ctx.params.query.user_interest_id) {
							delete ctx.params.query.experience_id;
						}
					}
				},
			],
			update: [
				async function sanatizeParams(ctx) {
					const payment_grid = await this._get(ctx, { id: ctx.params.id });
					if (payment_grid[0].status === "PAYED") {
						throw new Errors.ValidationError(
							"This payment grid in status 'PAYED'.",
							"SOCIAL_SCHOLARSHIP_PAYMENT_GRID_UPDATE_ERROR",
							{},
						);
					}
					ctx.params.updated_at = new Date();
					if (ctx.params.hours_transit) {
						const hours_transit = await ctx.call(
							"social_scholarship.payment_hours_transitions.find",
							{
								query: {
									user_interest_id: ctx.params.user_interest_id,
								},
							},
						);
						if (hours_transit.length > 0) {
							await ctx.call("social_scholarship.payment_hours_transitions.patch", {
								id: hours_transit[0].id,
								hours: ctx.params.hours_transit,
							});
						} else {
							const entity = {
								user_interest_id: ctx.params.user_interest_id,
								hours: ctx.params.hours_transit,
							};
							await ctx.call("social_scholarship.payment_hours_transitions.create", entity);
						}
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		generateReport: {
			rest: "POST /:id/generate/report",
			visibility: "published",
			scope: "social_scholarship:payment-grid:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const data = await this.getInfoReport(ctx, ctx.params.id);
				return this.generateReport(ctx, data);
			},
		},

		approve_payment_grid: {
			rest: "POST /:id/approve",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "social_scholarship:payment-grid:approve",
			visibility: "published",
			async handler(ctx) {
				await ctx.call("social_scholarship.payment-grid.patch", {
					id: ctx.params.id,
					status: "APPROVED",
				});
				const financial_email = await ctx.call("social_scholarship.configurations.find", {
					query: {
						key: CONFIGURATION_KEYS.FINANCIAL_NOTIFICATION_EMAIL,
					},
				});
				const payment_grid = await this._get(ctx, {
					id: ctx.params.id,
					withRelated: ["user_interest"],
				});
				await ctx.call("notifications.alerts.create_alert", {
					alert_type_key: "SOCIAL_SUPPORT_PAYMENT_GRID_APPROVED",
					user_id: ctx.meta.user.id,
					user_data: { email: financial_email[0].value },
					data: {
						month: payment_grid[0].month,
						year: payment_grid[0].year,
						title: payment_grid[0].user_interest.experience.translations[0].title,
					},
					variables: {},
					external_uuid: null,
					medias: [],
				});
				return payment_grid;
			},
		},

		pay_payment_grid: {
			rest: "POST /:id/pay",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "social_scholarship:payment-grid:pay",
			async handler(ctx) {
				await ctx.call("social_scholarship.payment-grid.patch", {
					id: ctx.params.id,
					status: "PAYED",
				});
			},
		},

		create_payment_grid: {
			rest: "POST /:month/:year/:user_interest_id",
			visibility: "published",
			scope: "social_scholarship:payment-grid:create",
			params: {
				month: { type: "number", integer: true, convert: true },
				year: { type: "number", integer: true, convert: true },
				user_interest_id: { type: "number", integer: true, convert: true },
				notes: { type: "string", optional: true, nullable: true },
			},
			async handler(ctx) {
				const payment_grid = await this._find(ctx, {
					query: {
						payment_month: ctx.params.month,
						payment_year: ctx.params.year,
						user_interest_id: ctx.params.user_interest_id,
					},
					withRelated: ["user_interest"],
				});
				if (payment_grid.length > 0) {
					return payment_grid;
				} else {
					const user_interest = await ctx.call("social_scholarship.experience-user-interest.get", {
						id: ctx.params.user_interest_id,
						withRelated: ["experience", "application"],
					});
					if (user_interest[0].experience.status === "CLOSED") {
						throw new Errors.ValidationError(
							"Cannot create a payment grid because the experience is closed",
							"SOCIAL_SCHOLARSHIP_PAYMENT_GRID_EXPERIENCE_CLOSED",
							{},
						);
					}
					const experience = user_interest[0].experience;
					const value_hour = experience.payment_value;
					const perc_iban = experience.perc_student_iban;
					const perc_cc = experience.perc_student_ca;
					const formateDate = ctx.params.year + "-" + ctx.params.month;
					const starDateMonth = moment(formateDate).startOf("month").format("YYYY-MM-DD");
					const endDateMonth = moment(formateDate).endOf("month").format("YYYY-MM-DD");
					const totalHoursMonth = await this.getHoursMonth(
						ctx,
						ctx.params.user_interest_id,
						starDateMonth,
						endDateMonth,
					);
					const hours_transit = await ctx.call(
						"social_scholarship.payment_hours_transitions.find",
						{
							query: {
								user_interest_id: ctx.params.user_interest_id,
							},
						},
					);
					let transit_hours = 0;
					if (hours_transit.length > 0) {
						transit_hours = hours_transit[0].hours;
						await ctx.call("social_scholarship.payment_hours_transitions.patch", {
							id: hours_transit[0].id,
							hours: 0,
						});
					}
					const formated_hours = this.format_minutes_to_hours(totalHoursMonth);
					let hours = this.sum_hours_transit_to_minutes(totalHoursMonth, transit_hours);
					const total_pay = (hours * value_hour) / 60;
					const value_cc = total_pay * perc_cc;
					const value_iban = total_pay * perc_iban;
					const current_account = await this.getCurrentAccount(ctx);

					const entity =
						{
							user_interest_id: ctx.params.user_interest_id,
							payment_month: ctx.params.month,
							payment_year: ctx.params.year,
							payment_model: experience.payment_model,
							payment_value: total_pay,
							hours_pay: this.format_minutes_to_hours(hours),
							number_hours: formated_hours,
							payment_value_iban: value_iban,
							payment_value_ca: value_cc,
							hours_transit: 0,
							status: "PENDING",
							user_account_id: current_account && current_account.value ? current_account.value : null,
							notes: ctx.params.notes,
							iban: user_interest[0].application.iban,
							created_at: new Date(),
							updated_at: new Date(),
						};

					const payment_created = await this._create(ctx, entity);
					payment_created[0].user_interest = await ctx.call(
						"social_scholarship.experience-user-interest.get",
						{ id: payment_created[0].user_interest_id, withRelated: ["experience"] },
					);
					return payment_created;
				}
			},
		},
		reopen: {
			rest: "POST /:id/reopen",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "social_scholarship:payment-grid:reopen",
			async handler(ctx) {
				const payment_grid = await this._get(ctx, { id: ctx.params.id });
				(payment_grid[0].status = "REOPEN"), (payment_grid[0].updated_at = new Date());
				return await this._update(ctx, payment_grid[0]);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async generateReport(ctx, data) {
			return ctx.call("reports.templates.print", {
				key: "SOCIAL_SCHOLARSHIP_PAYMENT_MONTHLY_REPORT",
				data: data,
				name: "Grelha de Pagamento - Bolsa de colaboradores" + "(" + data.date + ")",
			});
		},

		async getInfoReport(ctx, id) {
			const payment_grid = await this._get(ctx, { id: id, withRelated: ["user_account"] });
			const experience_user = await ctx.call("social_scholarship.experience-user-interest.get", {
				id: payment_grid[0].user_interest_id,
			});
			const experience = await ctx.call("social_scholarship.experiences.get", {
				id: experience_user[0].experience_id,
			});
			const application_user = await ctx.call("social_scholarship.applications.find", {
				query: {
					academic_year: experience[0].academic_year,
					//status: "ACCEPTED",
					user_id: experience_user[0].user_id,
				},
			});
			/*if (application_user.length === 0) {
				throw new Errors.ValidationError(
					"User don't have a active application",
					"NO_ACTIVE_APPLICATION",
					{},
				);
			}*/

			const formateDate = payment_grid[0].payment_year + "-" + payment_grid[0].payment_month;
			const starDateMonth = moment(formateDate).startOf("month").format("YYYY-MM-DD");
			const endDateMonth = moment(formateDate).endOf("month").format("YYYY-MM-DD");
			const attendances = await this.getAttendances(
				ctx,
				payment_grid[0].user_interest_id,
				starDateMonth,
				endDateMonth,
			);

			let formated_attendances = [];
			for (let attendance of attendances) {
				attendance.initial_time = moment(attendance.initial_time).format("DD/MM/YYYY HH:mm:ss");
				attendance.final_time = moment(attendance.final_time).format("DD/MM/YYYY HH:mm:ss");
				formated_attendances.push(attendance);
			}

			const data = {
				experience_id: experience_user[0].experience_id,
				date: payment_grid[0].payment_month + "/" + payment_grid[0].payment_year,
				full_name_student: experience_user[0].user.name,
				course_name: application_user.length === 0 ? "" : application_user[0].course.name,
				full_name_advisor: experience[0].experience_advisor.name,
				total_hours: payment_grid[0].number_hours,
				total_value: payment_grid[0].payment_value,
				value_transfer: payment_grid[0].payment_value_iban,
				iban_student: payment_grid[0].iban,
				cc_transfer: payment_grid[0].payment_value_ca,
				cc: payment_grid[0].user_account ? payment_grid[0].user_account.name : "N/D",
				attendances: formated_attendances,
			};

			return data;
		},

		async getHoursMonth(ctx, user_interest_id, startDate, endDate) {
			const attendances = await this.getAttendances(ctx, user_interest_id, startDate, endDate);
			return this.calculateAttendancesHours(attendances);
		},

		async getAttendances(ctx, user_interest_id, startDate, endDate) {
			return ctx.call("social_scholarship.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: startDate,
				status: ["ACCEPTED", "REJECTED"],
				end_date: moment(endDate)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},

		calculateAttendancesHours(attendances) {
			let valor_inicial = 0;
			for (const attendance of attendances) {
				valor_inicial = this.calculate_sum_hours_to_minutes(valor_inicial, attendance.n_hours);
			}
			return valor_inicial;
		},

		format_minutes_to_hours(total_minutes) {
			const a = Math.floor(total_minutes / 60) + "." + (total_minutes % 60);
			return parseFloat(parseFloat(a).toFixed(2));
		},

		calculate_sum_hours_to_minutes(valor_inicial, valor) {
			const valor_string = valor.toString();
			const valor_split = valor_string.split(".");
			if (valor_split.length >= 2) {
				const hours = parseInt(valor_split[0]);
				let minutes = 0;
				if (valor_split[1].length === 1) {
					minutes = parseInt(valor_split[1]) * 10;
				} else {
					minutes = parseInt(valor_split[1]);
				}
				valor_inicial += hours * 60;
				valor_inicial += minutes;
			} else {
				valor_inicial += parseInt(valor_string) * 60;
			}

			return valor_inicial;
		},

		sum_hours_transit_to_minutes(valor_inicial, valor) {
			const valor_string = valor.toString();
			const valor_split = valor_string.split(".");
			if (valor_split.length >= 2) {
				const hours = parseInt(valor_split[0]);
				let minutes = parseInt(valor_split[1]);
				if (valor_split[1].length < 2) {
					minutes = parseInt(valor_split[1] + "0");
				}
				valor_inicial += hours * 60;
				valor_inicial += minutes;
			} else {
				valor_inicial += parseInt(valor_string) * 60;
			}

			return valor_inicial;
		},

		async getCurrentAccount(ctx) {

			const has_current_account = await ctx.call("social_scholarship.configurations.find", {
				query: {
					key: [CONFIGURATION_KEYS.HAS_CURRENT_ACCOUNT],
				},
				withRelated: false,
			});
			if (has_current_account.length > 0) {
				if(has_current_account[0].value == "true") {
					const current_account = await ctx.call("social_scholarship.configurations.find", {
						query: {
							key: [CONFIGURATION_KEYS.CURRENT_ACCOUNT],
						},
						withRelated: false,
					});
					if (current_account.length > 0) {
						return current_account[0];
					}
				}
			}
			return false;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
