"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { SCHEDULE_WEEK, SCHEDULE_DAY_PERIOD } = require("./utils/constants");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.schedules",
	table: "schedule",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "schedules")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"day_week",
			"day_period"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			day_week: { type: "enum", values: SCHEDULE_WEEK },
			day_period: { type: "enum", values: SCHEDULE_DAY_PERIOD },
		}
	},
	hooks: {

	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {


	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
