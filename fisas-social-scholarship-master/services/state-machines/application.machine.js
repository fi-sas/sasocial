let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			// update status to "em analise"
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DECLINED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DISPATCH", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CANCELLED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "EXPIRED", to: "ANALYSED" },

			// update status to "rejeitada"
			{ name: "DECLINE", from: "DISPATCH", to: "DECLINED" },

			// update status to "cancelada"
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSED", to: "CANCELLED" },
			//{ name: "CANCEL", from: "INTERVIEWED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ACCEPTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" }, // New

			// update status to "entrevista"
			{ name: "INTERVIEW", from: "ANALYSED", to: "INTERVIEWED" },

			// update status to "aceite"
			{ name: "ACCEPT", from: "DISPATCH", to: "ACCEPTED" },
			{ name: "ACCEPT", from: "SUBMITTED", to: "ACCEPTED" },

			// update status to "expirada"
			{ name: "EXPIRE", from: "ACCEPTED", to: "EXPIRED" },

			// update status to "despacho"
			{ name: "DISPATCH", from: "ANALYSED", to: "DISPATCH" },
			{ name: "DISPATCH", from: "INTERVIEWED", to: "DISPATCH" },
		],
		methods: {
			onAfterAnalyse: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterCancel: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterInterview: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterAccept: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterDecline: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterExpire: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			// onAfterWait: async function (lifecycle) {
			// 	return await this.saveApplication(lifecycle);
			// },
			// onAfterAcknowledge: async function (lifecycle) {
			// 	return await this.saveApplication(lifecycle);
			// },
			// onAfterClose: async function (lifecycle) {
			// 	return await this.saveApplication(lifecycle);
			// },
			onAfterWithdraw: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},

			onAfterDispatch: async function(lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			// onAfterRetry: async function (lifecycle) {
			// 	return await this.saveApplication(lifecycle);
			// },
			// onAfterPublish: async function (lifecycle) {
			// 	return await this.saveApplication(lifecycle);
			// },
			saveApplication: async function(lifecycle) {
				await ctx.call("social_scholarship.applications.patch", {
					...ctx.params.application,
					id: ctx.params.id,
					decision: ctx.params.decision,
					reject_reason: ctx.params.reject_reason,
					status: lifecycle.to,
					last_status: lifecycle.from,
				});
				await ctx.call("social_scholarship.applicationHistory.create", {
					application_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});

				return await ctx.call("social_scholarship.applications.get", { id: ctx.params.id });
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
