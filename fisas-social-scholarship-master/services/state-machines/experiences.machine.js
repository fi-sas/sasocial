let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			// update status to "em analise"
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "REJECTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DISPATCH", to: "ANALYSED" },

			// update status to "devolvida",
			{ name: "RETURN", from: "DISPATCH", to: "RETURNED" },
			{ name: "RETURN", from: "SEND_SEEM", to: "RETURNED" },

			// update status to "submetida"
			{ name: "SUBMIT", from: "RETURNED", to: "SUBMITTED" },

			// update status to "rejeitada"
			{ name: "REJECT", from: "DISPATCH", to: "REJECTED" },
			//{ name: "REJECT", from: "SEND_SEEM", to: "REJECTED" },

			// update status to "aprovada"
			{ name: "APPROVE", from: "DISPATCH", to: "APPROVED" },
			//{ name: "APPROVE", from: "SEND_SEEM", to: "APPROVED" },

			// update status to "emitir parecer",
			{ name: "SENDSEEM", from: "ANALYSED", to: "SEND_SEEM" },
			{ name: "SENDSEEM", from: "EXTERNAL_SYSTEM", to: "SEND_SEEM" },

			//update status to "sistema externo",
			{ name: "SENDEXTERNAL", from: "SEND_SEEM", to: "EXTERNAL_SYSTEM" },

			//update status to "selecao",
			{ name: "SELECTION", from: "APPROVED", to: "SELECTION" },

			// updated status to "em colaboração"
			{ name: "COLABORATION", from: "SELECTION", to: "IN_COLABORATION" },

			// update status to "fechada"
			//{ name: "CLOSE", from: "SELECTION", to: "CLOSED" },
			{ name: "CLOSE", from: "IN_COLABORATION", to: "CLOSED" },

			// upated status to "cancelada"
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSED", to: "CANCELLED" },
			{ name: "CANCEL", from: "APPROVED", to: "CANCELLED" },
			{ name: "CANCEL", from: "SELECTION", to: "CANCELLED" },

			// update status to "dispatch"
			{ name: "DISPATCH", from: "ANALYSED", to: "DISPATCH" },
			{ name: "DISPATCH", from: "SEND_SEEM", to: "DISPATCH" }, // New
		],
		methods: {
			onAfterAnalyse: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterApprove: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterReturn: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterSubmit: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterReject: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterSendseem: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterSendexternal: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterSelection: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterColaboration: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterClose: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterCancel: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterDispatch: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},

			// Save new application state and operation in applications history
			saveExperience: async function(lifecycle) {
				await ctx.call("social_scholarship.experiences.patch", {
					...ctx.params.experience,
					id: ctx.params.id,
					status: lifecycle.to,
					decision: ctx.params.decision,
					reject_reason: ctx.params.reject_reason,
					last_status: lifecycle.from,
				});
				await ctx.call("social_scholarship.experience_history.create", {
					experience_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});
				return await ctx.call("social_scholarship.experiences.get", { id: ctx.params.id });
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
