let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			// update status to "em analise"
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CLOSED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CANCELLED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DECLINED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DISPATCH", to: "ANALYSED" },
			{ name: "ANALYSE", from: "NOT_SELECTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "WAITING", to: "ANALYSED" }, // New

			// update status to "em entrevista"
			{ name: "INTERVIEW", from: "ANALYSED", to: "INTERVIEWED" },
			//{ name: "INTERVIEW", from: "WAITING", to: "INTERVIEWED" },

			// update status to "aprovada"
			//{ name: "APPROVE", from: "WAITING", to: "APPROVED" },
			//{ name: "APPROVE", from: "INTERVIEWED", to: "APPROVED" },
			{ name: "APPROVE", from: "DISPATCH", to: "APPROVED" },

			// update status to "lista de espera"
			//{ name: "WAITING", from: "ANALYSED", to: "WAITING" },
			//{ name: "WAITING", from: "INTERVIEWED", to: "WAITING" },
			{ name: "WAITING", from: "DISPATCH", to: "WAITING" }, // New

			// update status to "nao selecionado"
			//{ name: "NOTSELECT", from: "WAITING", to: "NOT_SELECTED" },
			//{ name: "NOTSELECT", from: "INTERVIEWED", to: "NOT_SELECTED" },
			//{ name: "NOTSELECT", from: "ANALYSED", to: "NOT_SELECTED" },
			{ name: "NOTSELECT", from: "DISPATCH", to: "NOT_SELECTED" },

			// update status to "aceite"
			{ name: "ACCEPT", from: "APPROVED", to: "ACCEPTED" },

			// update status to "em colaboraçao"
			{ name: "COLABORATION", from: "ACCEPTED", to: "COLABORATION" },
			{ name: "COLABORATION", from: "WITHDRAWAL", to: "COLABORATION" },
			{ name: "COLABORATION", from: "CLOSED", to: "COLABORATION" },

			// update status to "fechada"
			{ name: "CLOSE", from: "COLABORATION", to: "CLOSED" },
			{ name: "CLOSE", from: "WITHDRAWAL", to: "CLOSED" }, // New
			{ name: "CLOSE", from: "NOT_SELECTED", to: "CLOSED" }, // New
			{ name: "CLOSE", from: "WAITING", to: "CLOSED" }, // New

			// update status to "em desistencia"
			{ name: "WITHDRAWAL", from: "COLABORATION", to: "WITHDRAWAL" },

			// update status to "desistencia aceite"
			{ name: "WITHDRAWALACCEPTED", from: "WITHDRAWAL", to: "WITHDRAWAL_ACCEPTED" },

			// update status to "rejeitada"
			{ name: "DECLINE", from: "DISPATCH", to: "DECLINED" },
			//{ name: "DECLINE", from: "INTERVIEWED", to: "DECLINED" },

			// update status to "cancelada"
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSED", to: "CANCELLED" },
			{ name: "CANCEL", from: "INTERVIEWED", to: "CANCELLED" },
			//{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" },
			{ name: "CANCEL", from: "WAITING", to: "CANCELLED" },
			{ name: "CANCEL", from: "APPROVED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ACCEPTED", to: "CANCELLED" },
			//{ name: "CANCEL", from: "COLABORATION", to: "CANCELLED" },

			// update status to "dispatch"
			{ name: "DISPATCH", from: "ANALYSED", to: "DISPATCH" },
			{ name: "DISPATCH", from: "INTERVIEWED", to: "DISPATCH" },

			// automatic acceptance
			{ name: "COLABORATION", from: "APPROVED", to: "COLABORATION" },
		],
		methods: {
			onAfterAnalyse: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterInterview: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterApprove: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterWaiting: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterNotselect: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterAccept: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterColaboration: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterClose: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterWithdrawal: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterDecline: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterCancel: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterWithdrawalaccepted: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},
			onAfterDispatch: async function(lifecycle) {
				return await this.saveExperience(lifecycle);
			},

			// Save new manifest interest state and operation in manifest interest history
			saveExperience: async function(lifecycle) {
				await ctx.call("social_scholarship.experience-user-interest.patch", {
					...ctx.params.manifest_interest,
					id: ctx.params.id,
					status: lifecycle.to,
					decision: ctx.params.decision,
					reject_reason: ctx.params.reject_reason,
					last_status: lifecycle.from,
				});
				await ctx.call("social_scholarship.user-interest-historic.create", {
					user_interest_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});
				return await ctx.call("social_scholarship.experience-user-interest.get", {
					id: ctx.params.id,
				});
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
