"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "social_scholarship.translations",
	table: "translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("social_scholarship", "translations")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"title",
			"proponent_service",
			"applicant_profile",
			"job",
			"description",
			"selection_criteria",
			"language_id",
			"experience_id",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["language"],
		withRelateds: {
			language(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.languages", "language", "language_id");
			},
		},
		entityValidator: {
			title: "string|max:255",
			proponent_service: "string|max:255",
			applicant_profile: "string|max:255",
			job: "string|max:255",
			description: "string|max:255",
			selection_criteria: "string|max:255",
			language_id: "number|integer|convert",
			experience_id: "number|integer|convert|optional",
			updated_at: "date|convert|optional",
			created_at: "date|convert|optional",
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query"
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"configuration.languages.*"(ctx) {
			this.logger.info("CAPTURED EVENT => configuration.languages.*");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
