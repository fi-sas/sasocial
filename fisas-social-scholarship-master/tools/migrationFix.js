const fs = require("fs");
const files = require("./migrationFiles.json");
const knexOptions = require("../knexfile");
const { exit } = require("process");
const knex = require("knex")(knexOptions);

// eslint-disable-next-line no-console
const Debug = function (migration, message) {
	console.log(`[${new Date().toISOString()}] [${migration}] - ${message}`);
};

Debug("INIT", "Skipping time - to POSTGRES Database initialize");

Debug("INIT", "looping files");
// FOR EACH FILE
Promise.all(
	Object.keys(files).map((file) => {
		// CHECK IF FILE EXIST
		Debug(file, "Check if file exist, ".concat("./migrations/".concat(files[file])));

		if (fs.existsSync("./migrations/".concat(files[file]))) {
			// SEARCH FOR MIGRATION ROW
			Debug(file, "File exist");
			return knex("migrations")
				.where("name", file)
				.then(([result]) => {
					if (result) {
						Debug(file, "Migration found");
						// UPDATE MIGRATION NAME
						return knex("migrations")
							.update("name", files[file])
							.where("id", result.id)
							.then(() => {
								Debug(file, "Migration updated");
							})
							.catch(() => {
								Debug(file, "Migration failed to update");
							});
					}

					Debug(file, "Migration not found");
				});
		} else {
			Debug(file, "File not found");
			return Promise.resolve();
		}
	}),
)
	.then(() => {
		exit();
	})
	.catch((err) => {
		Debug("Error", err);
		exit();
	});

