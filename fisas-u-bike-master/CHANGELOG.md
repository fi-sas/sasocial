## [1.26.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.26.1...v1.26.2) (2022-06-28)


### Bug Fixes

* **ubike:** fix disponible query ([fed3be1](https://gitlab.com/fi-sas/fisas-u-bike/commit/fed3be1ebea9d54c031726be07e0a5074c7bdcb6))

## [1.26.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.26.0...v1.26.1) (2022-06-24)


### Bug Fixes

* **ubike:** fix state machine status ([c1318e1](https://gitlab.com/fi-sas/fisas-u-bike/commit/c1318e1138657d81a8a72eb670d9f63a275aa547))

# [1.26.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.25.0...v1.26.0) (2022-06-06)


### Features

* **ubike:** phone and tin added to request ([282dbea](https://gitlab.com/fi-sas/fisas-u-bike/commit/282dbea58d7ef57435c82373c204d1a1426917bb))

# [1.25.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.24.0...v1.25.0) (2022-06-01)


### Bug Fixes

* **applications:** fix report data ([adaad22](https://gitlab.com/fi-sas/fisas-u-bike/commit/adaad22323368a5f6a97f7c6072b1a744c2dc525))
* **ubike:** removing import mistake ([5456f79](https://gitlab.com/fi-sas/fisas-u-bike/commit/5456f792e1df06d3711c0853c18e13010d7d52a5))


### Features

* **ubike:** action for msu reports ([655f76e](https://gitlab.com/fi-sas/fisas-u-bike/commit/655f76e0105313ebfdaa45c267bbb74bf21e8202))
* **ubike:** new reports and actions) ([2193cba](https://gitlab.com/fi-sas/fisas-u-bike/commit/2193cba899dad182b48048de3876770250529e66))

# [1.24.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.23.3...v1.24.0) (2022-06-01)


### Features

* **ubike:** new report ([70af760](https://gitlab.com/fi-sas/fisas-u-bike/commit/70af760cd5cef8745208cd4dc4f42f79c88265ed))
* **ubike:** removing space between column name ([81ce9ab](https://gitlab.com/fi-sas/fisas-u-bike/commit/81ce9ab0df4b52d5909f3f50b9a0f6b3ebf1c802))

## [1.23.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.23.2...v1.23.3) (2022-05-25)


### Bug Fixes

* **configs:** sanatize dates before save ([7e88092](https://gitlab.com/fi-sas/fisas-u-bike/commit/7e8809276402313a1780dc77fea6fd6d1059f4ce))

## [1.23.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.23.1...v1.23.2) (2022-05-25)


### Bug Fixes

* **configs:** add toISOString on configs dates ([01d19f9](https://gitlab.com/fi-sas/fisas-u-bike/commit/01d19f95ff3410206b6ad02f5cea83a269c2c116))

## [1.23.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.23.0...v1.23.1) (2022-05-20)


### Bug Fixes

* **applications:** add validations to refreshNotificationHour ([61e5733](https://gitlab.com/fi-sas/fisas-u-bike/commit/61e5733e3656197ad11fb54f7ec5171e38d8fa6b))

# [1.23.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.22.0...v1.23.0) (2022-05-18)


### Bug Fixes

* **ubike:** improve code ([4bf06c7](https://gitlab.com/fi-sas/fisas-u-bike/commit/4bf06c7a0f7c368c488aa6bd04f88dd3779c858f))


### Features

* **ubike:** kms traveled report ([4f6096d](https://gitlab.com/fi-sas/fisas-u-bike/commit/4f6096d0ca22ea4f70bfc099ac1914dc20cc8531))

# [1.22.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.21.0...v1.22.0) (2022-05-18)


### Bug Fixes

* **ubike:** fix status when creating an application by bo ([80011dd](https://gitlab.com/fi-sas/fisas-u-bike/commit/80011ddd22a4c6cff2389253ab4556a40ed0ebf7))


### Features

* **ubike:** add preferred location to applications ([85288af](https://gitlab.com/fi-sas/fisas-u-bike/commit/85288af6c69ba4d0a00c23f23136c4a4229bb0aa))

# [1.21.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.20.1...v1.21.0) (2022-05-17)


### Features

* **ubike:** applications report ([5bcb967](https://gitlab.com/fi-sas/fisas-u-bike/commit/5bcb96749ef1d13d86ada6ae274a6478976f8cf8))

## [1.20.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.20.0...v1.20.1) (2022-05-06)


### Performance Improvements

* **applications:** remove relateds from target notificaitons actions ([07343bd](https://gitlab.com/fi-sas/fisas-u-bike/commit/07343bde6df0db92d2b8347626fe9ea141ffaaac))

# [1.20.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.19.0...v1.20.0) (2022-04-26)


### Bug Fixes

* **ubike:** allow null values to configs ([1f04096](https://gitlab.com/fi-sas/fisas-u-bike/commit/1f040963d2f4cf76d21c7a0c2bcbeb924c14fbda))


### Features

* **ubike:** improve trips service ([c3cf626](https://gitlab.com/fi-sas/fisas-u-bike/commit/c3cf6261acaa0d1760b6870dbbb832cb1edcbd6f))

# [1.19.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.7...v1.19.0) (2022-04-11)


### Bug Fixes

* **ubike:** change configs for one request ([d01c4b7](https://gitlab.com/fi-sas/fisas-u-bike/commit/d01c4b705110fe0a7ff01db8fee2cd903cf4092f))
* **ubike:** dates from configs ([f04bd3d](https://gitlab.com/fi-sas/fisas-u-bike/commit/f04bd3d54876201d0695f8a11aea8fdc9d065877))


### Features

* **ubike:** add terms endpoint ([b78f637](https://gitlab.com/fi-sas/fisas-u-bike/commit/b78f637d048bb8d5df000eeaed4ae6b002bb3d24))
* **ubike:** change configurations service ([d5a9ff1](https://gitlab.com/fi-sas/fisas-u-bike/commit/d5a9ff19271b6bc7804ad6bd960aeb5f1167210d))

## [1.18.7](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.6...v1.18.7) (2022-03-25)


### Bug Fixes

* **ubike:** fix searching by user applications ([7eefdf5](https://gitlab.com/fi-sas/fisas-u-bike/commit/7eefdf52fcb22a76c580976607568c498720299d))

## [1.18.6](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.5...v1.18.6) (2022-03-25)


### Bug Fixes

* **ubike:** fix wp permissions and set contract_file not required ([933dc7d](https://gitlab.com/fi-sas/fisas-u-bike/commit/933dc7df909c67c43f5d76b90a38e82def8ae4c8))
* **ubike:** removing commented code ([11ad3d6](https://gitlab.com/fi-sas/fisas-u-bike/commit/11ad3d663039d7efe79276d6b2defe918a4841b9))

## [1.18.5](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.4...v1.18.5) (2022-03-18)


### Bug Fixes

* **ubike:** removing checking backoffice user ([895b5a4](https://gitlab.com/fi-sas/fisas-u-bike/commit/895b5a4e1caa44ca088ca9ba623f8304b64da5f5))

## [1.18.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.3...v1.18.4) (2022-03-15)


### Bug Fixes

* **ubike:** fix user cache ([92ec51f](https://gitlab.com/fi-sas/fisas-u-bike/commit/92ec51fd7b76f1948bae41a7763d59bb4c712337))

## [1.18.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.2...v1.18.3) (2022-03-15)


### Bug Fixes

* **ubike:** cache problems ([89a8284](https://gitlab.com/fi-sas/fisas-u-bike/commit/89a828474a9c04694ea63006549bbf8c84560c7f))

## [1.18.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.1...v1.18.2) (2022-03-14)


### Bug Fixes

* **ubike:** remove user from query ([47234e2](https://gitlab.com/fi-sas/fisas-u-bike/commit/47234e2281f9dd5dcf133be963d11582e4f68d85))
* **ubike:** remove user_id from query ([94181b7](https://gitlab.com/fi-sas/fisas-u-bike/commit/94181b73cb45db8fadb38cc86cf725d171763176))

## [1.18.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.18.0...v1.18.1) (2022-02-02)


### Bug Fixes

* **application:** update application update. add field ready_to_submit ([7afa50a](https://gitlab.com/fi-sas/fisas-u-bike/commit/7afa50abae8fff572f5cdd43177badd298d94072))

# [1.18.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.17.0...v1.18.0) (2021-12-16)


### Features

* application validate access ([d582fd1](https://gitlab.com/fi-sas/fisas-u-bike/commit/d582fd15f5a36a034181bd9c5f25bc6cb2fb368f))

# [1.17.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.16.0...v1.17.0) (2021-12-15)


### Features

* verify application in elaboration ([e31d17e](https://gitlab.com/fi-sas/fisas-u-bike/commit/e31d17e27b6e13ed26d466ad99b06fc324dec03f))
* verify application in elaboration ([bc5f56c](https://gitlab.com/fi-sas/fisas-u-bike/commit/bc5f56ccce87e3efdfc041e572ff99c06b650bf5))

# [1.16.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.15.0...v1.16.0) (2021-12-14)


### Features

* save application step-by-step ([2f6af79](https://gitlab.com/fi-sas/fisas-u-bike/commit/2f6af79897d4902f4c39d64db4612073b38b1f68))

# [1.15.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.14.4...v1.15.0) (2021-12-03)


### Features

* address publication, new filters ([bee85e9](https://gitlab.com/fi-sas/fisas-u-bike/commit/bee85e96fd9e0b2880cd4ec0fe33fec8d5c3a27d))

## [1.14.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.14.3...v1.14.4) (2021-11-29)


### Bug Fixes

* update core package ([e78a31f](https://gitlab.com/fi-sas/fisas-u-bike/commit/e78a31fdb8b8d2248dd3daafc9a723bddd0c669e))

## [1.14.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.14.2...v1.14.3) (2021-11-29)


### Bug Fixes

* update core package ([e8109ec](https://gitlab.com/fi-sas/fisas-u-bike/commit/e8109ec4c3aac121ebef87bccafe389bf9c706d5))

## [1.14.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.14.1...v1.14.2) (2021-11-29)


### Bug Fixes

* update package ([fef3014](https://gitlab.com/fi-sas/fisas-u-bike/commit/fef30144d2a42f6cf712c29289adc8ad7b9b55d7))

## [1.14.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.14.0...v1.14.1) (2021-11-26)


### Bug Fixes

* **trips:** add sort be bike identification ([feea95c](https://gitlab.com/fi-sas/fisas-u-bike/commit/feea95cafe3d15b5f9584e0dfd0cb769c8c6bde5))

# [1.14.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.13.0...v1.14.0) (2021-11-26)


### Features

* print monitorizarion / reports ([b441c37](https://gitlab.com/fi-sas/fisas-u-bike/commit/b441c37a6034e209514027d17ed467e0fa52f8ca))

# [1.13.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.12.0...v1.13.0) (2021-11-25)


### Features

* monitorization / reports ([f23c2cc](https://gitlab.com/fi-sas/fisas-u-bike/commit/f23c2cca11eb06f0f859a162e82dfc56885851da))

# [1.12.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.11.2...v1.12.0) (2021-11-11)


### Features

* complaint / application status ([0be8bd3](https://gitlab.com/fi-sas/fisas-u-bike/commit/0be8bd38c350d03c0cc3ae802142448e40370ca5))

## [1.11.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.11.1...v1.11.2) (2021-09-01)


### Bug Fixes

* **applications:** update endpoint dasboard ([7eb3caa](https://gitlab.com/fi-sas/fisas-u-bike/commit/7eb3caacca106580cbac7b6dc26d17a230385465))
* **stats:** update endpoint saving c02 ([030472b](https://gitlab.com/fi-sas/fisas-u-bike/commit/030472b426bdcacfa0249b1343e44a58d43e83bb))
* **trips:** update filter ([71829ef](https://gitlab.com/fi-sas/fisas-u-bike/commit/71829ef1e37aa80ad88d45ab54ad76388c25f9e5))

## [1.11.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.11.0...v1.11.1) (2021-08-27)


### Bug Fixes

* **bikes:** remove submitted and analysed from bike undisponible ([9128c99](https://gitlab.com/fi-sas/fisas-u-bike/commit/9128c99c814f31ee9fe84737b33ab76c49be4e26))

# [1.11.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.10.3...v1.11.0) (2021-08-26)


### Features

* **applications:** permit create applications by backoffice ([bf8f99f](https://gitlab.com/fi-sas/fisas-u-bike/commit/bf8f99f68b4cc15f4adb6a7ee70839b29285ab1f))

## [1.10.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.10.2...v1.10.3) (2021-08-12)


### Bug Fixes

* **bikes:** add two states in list bikes disponible ([2689fae](https://gitlab.com/fi-sas/fisas-u-bike/commit/2689fae982ad97527087243848d21b70fa010107))

## [1.10.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.10.1...v1.10.2) (2021-08-11)


### Bug Fixes

* **trip:** error in get typology ([4ecdc9a](https://gitlab.com/fi-sas/fisas-u-bike/commit/4ecdc9af094954e3c92b36bb07434c77cbfeeaca))

## [1.10.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.10.0...v1.10.1) (2021-08-11)


### Bug Fixes

* **trips:** update endpoint for calculate stats of users and bikes ([2b6a07b](https://gitlab.com/fi-sas/fisas-u-bike/commit/2b6a07bdf7bdcbbf7b95c1e57e7eeda3f1c3f0cf))

# [1.10.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.9.1...v1.10.0) (2021-08-10)


### Features

* **application_files:** add service, validation and relations ([514ce4f](https://gitlab.com/fi-sas/fisas-u-bike/commit/514ce4f493d106cc967b27b6fb24e0130b16e21a))
* **applications:** add relation and clear cache ([1153ce3](https://gitlab.com/fi-sas/fisas-u-bike/commit/1153ce390f7ccd76748fabfce9e58112567e719d))
* **migration:** add create application_file table ([a0c7a9a](https://gitlab.com/fi-sas/fisas-u-bike/commit/a0c7a9ab50c8b7eb9f9bba7f89aef6e51ddd2f50))

## [1.9.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.9.0...v1.9.1) (2021-08-09)


### Bug Fixes

* **applications:** remove async and await ([51fb322](https://gitlab.com/fi-sas/fisas-u-bike/commit/51fb322ce739ba779c1548c30ed8bea284fbacdc))
* **applications:** update for optimize stats ([4a342b1](https://gitlab.com/fi-sas/fisas-u-bike/commit/4a342b14b84d923471d11f3570b155b079849a75))
* **stats:** update endpoint not get all user ([71df140](https://gitlab.com/fi-sas/fisas-u-bike/commit/71df1409581df2598dfeb2a424f8982d789b4c8e))

# [1.9.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.8.4...v1.9.0) (2021-08-09)


### Features

* **applications:** add valdiations and set info in change status ([5eb323d](https://gitlab.com/fi-sas/fisas-u-bike/commit/5eb323d361365c5a4b662a020d6f5410404b560f))

## [1.8.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.8.3...v1.8.4) (2021-07-29)


### Bug Fixes

* **stats:** add validation null ([744236b](https://gitlab.com/fi-sas/fisas-u-bike/commit/744236b7d71cb827bdd6a5f8d808347171d61663))
* **stats:** remove error ([8d64581](https://gitlab.com/fi-sas/fisas-u-bike/commit/8d6458153748d997a19811a71084fbd65636fe98))

## [1.8.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.8.2...v1.8.3) (2021-07-28)


### Bug Fixes

* **applications:** erro ao mudar estado das candidaturas ([18b3134](https://gitlab.com/fi-sas/fisas-u-bike/commit/18b31342e56a308caafbb36404bdcc118fb17195))

## [1.8.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.8.1...v1.8.2) (2021-07-15)


### Bug Fixes

* **application-forms:** add missing awaits ([afe7b90](https://gitlab.com/fi-sas/fisas-u-bike/commit/afe7b902c59b2d64a88264bf1005e7429c906498))
* **lint:** fix lint errors and warns ([c5cf5f5](https://gitlab.com/fi-sas/fisas-u-bike/commit/c5cf5f5dd539ef5566bd2f5a70e466168d17aebe))

## [1.8.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.8.0...v1.8.1) (2021-07-13)


### Bug Fixes

* **bikes:** update relation bike model ([a17ec13](https://gitlab.com/fi-sas/fisas-u-bike/commit/a17ec136c0ee522e83ae9718447bf906b5482261))
* **trips:** create trip error ([dc576b5](https://gitlab.com/fi-sas/fisas-u-bike/commit/dc576b52ca69b5a2f0549da66883810ec106f7cc))

# [1.8.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.6...v1.8.0) (2021-07-09)


### Features

* **applications:** add search by bike identification ([033c682](https://gitlab.com/fi-sas/fisas-u-bike/commit/033c682b5e091b52c2cebc33e5bf1bb7fe754d67))

## [1.7.6](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.5...v1.7.6) (2021-07-08)


### Bug Fixes

* **seed:** remove homepage seeds ([cab527a](https://gitlab.com/fi-sas/fisas-u-bike/commit/cab527ac5a917fd31d9e30f6a05fba1445114087))
* **various:** fix trips ans application withrelateds ([e035c92](https://gitlab.com/fi-sas/fisas-u-bike/commit/e035c92adab3533d51c5b4c6fe84c32ab178f98d))

## [1.7.5](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.4...v1.7.5) (2021-06-24)


### Bug Fixes

* **application_form:** revert commit ([702d869](https://gitlab.com/fi-sas/fisas-u-bike/commit/702d8697d3cab1127f3627cebbe3f54c7b602e75))
* **application_forms:** fix problems answer and status ([9fb1a32](https://gitlab.com/fi-sas/fisas-u-bike/commit/9fb1a32487d492b4dbcaecf95f0a5235c251207d))

## [1.7.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.3...v1.7.4) (2021-06-24)


### Bug Fixes

* **applications:** problem lint ([dda524f](https://gitlab.com/fi-sas/fisas-u-bike/commit/dda524f63d64b1ff6f5c15a5069b09de44ed0fba))

## [1.7.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.2...v1.7.3) (2021-06-24)


### Bug Fixes

* **applications:** fix endpoint print applications ([e3e43da](https://gitlab.com/fi-sas/fisas-u-bike/commit/e3e43dafa418f2612af55edfaad69824d315fa8f))

## [1.7.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.1...v1.7.2) (2021-06-23)


### Bug Fixes

* **package:** update ms core ([7dc5558](https://gitlab.com/fi-sas/fisas-u-bike/commit/7dc555888fa9adcc46f96b17effa608060f16a28))
* **package-lock:** update core ([bc3aa01](https://gitlab.com/fi-sas/fisas-u-bike/commit/bc3aa01e383ce0eb2043bd8fc6d6ca2e129f4a6d))

## [1.7.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.7.0...v1.7.1) (2021-06-22)


### Bug Fixes

* **application_forms:** change error message ([6a27d09](https://gitlab.com/fi-sas/fisas-u-bike/commit/6a27d09871a7f840453b1a3da9fe4b8c2fc71f76))
* **applications_form:** add breakdown status ([2e6dd96](https://gitlab.com/fi-sas/fisas-u-bike/commit/2e6dd960a93dc0502f8873a153f39ce9411083cb))
* **stats:** scope ([015a2d6](https://gitlab.com/fi-sas/fisas-u-bike/commit/015a2d6d4d8b55137220196f9d6265424a532737))
* **trips:** reverse things ([5114771](https://gitlab.com/fi-sas/fisas-u-bike/commit/5114771eb98ffc8de8f4427414de41b4cfa04ab0))

# [1.7.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.6.0...v1.7.0) (2021-06-21)


### Bug Fixes

* **application_forms:** update query ([f8527a5](https://gitlab.com/fi-sas/fisas-u-bike/commit/f8527a5056d1e04fd5f98bf13e20bad972686a05))
* **applications:** add doc_type ([eccb8b0](https://gitlab.com/fi-sas/fisas-u-bike/commit/eccb8b0754a998075c6093ac307e936bff98cef2))
* **applications:** add filter in endpoint dashboard ([72e7a50](https://gitlab.com/fi-sas/fisas-u-bike/commit/72e7a5068d0823394c464268a5081c0d65877814))
* **enum:** remome ',' ([20c3a0b](https://gitlab.com/fi-sas/fisas-u-bike/commit/20c3a0b83c12bab6c2e192907e0365f507484915))
* **stats:** create new endpoint saving_co2 ([98b301e](https://gitlab.com/fi-sas/fisas-u-bike/commit/98b301e5db207664f8f00bba4af639a049dfd468))
* **trips:** lint problem ([7d25572](https://gitlab.com/fi-sas/fisas-u-bike/commit/7d255724e073da453bde0835ba159b5d79c4f324))
* **trips:** patch and create action ([84c30c2](https://gitlab.com/fi-sas/fisas-u-bike/commit/84c30c2b7c28e75c589c24e74ffd06668dca5c2c))


### Features

* **trips:** add  new endpoint get info sums trips ([fca9f0c](https://gitlab.com/fi-sas/fisas-u-bike/commit/fca9f0cad4c78a369c543edb988652669c6f3df2))

# [1.6.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.5.1...v1.6.0) (2021-06-15)


### Features

* **bikes:** add field asset_id, validation and relation ([a6344fa](https://gitlab.com/fi-sas/fisas-u-bike/commit/a6344fab9d2318862d1c3615e4c90457537a352a))

## [1.5.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.5.0...v1.5.1) (2021-05-06)


### Bug Fixes

* **bikes:** add decision to bikes disponible ([501daaf](https://gitlab.com/fi-sas/fisas-u-bike/commit/501daaf44e86633638ef062fb498d5bf79ebfa6d))

# [1.5.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.4.0...v1.5.0) (2021-04-30)


### Features

* **application:** add oposition_justification to application ([0d1bd04](https://gitlab.com/fi-sas/fisas-u-bike/commit/0d1bd043b5ce1d5b4ab68b0bdc8fd8a727128820))

# [1.4.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.3.4...v1.4.0) (2021-04-29)


### Features

* **application:** print quest ([3c5be83](https://gitlab.com/fi-sas/fisas-u-bike/commit/3c5be83a2706211dec21c461db9127f5ea252533))
* **applications:** Create applications list report ([04c6001](https://gitlab.com/fi-sas/fisas-u-bike/commit/04c6001cc5a4490af8cd2bdc93fdcbd41c84d392))
* **stats:** add request to print bike stats ([56082e1](https://gitlab.com/fi-sas/fisas-u-bike/commit/56082e10c2d8f953cefc48a82c647c66aed2f7fc))

## [1.3.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.3.3...v1.3.4) (2021-04-21)

### Bug Fixes

- **application:** remove required bike on dispatch ([40b1b9f](https://gitlab.com/fi-sas/fisas-u-bike/commit/40b1b9fac4b91ec2315f97c77faf6e4f668a01fb))
- **trips:** add convert to number fields ([0c4f78e](https://gitlab.com/fi-sas/fisas-u-bike/commit/0c4f78e39ecdcaa0a7fcc4ac5949309aa0af042f))

## [1.3.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.3.2...v1.3.3) (2021-04-19)

### Bug Fixes

- **application_forms:** fix withrelateds ([e744482](https://gitlab.com/fi-sas/fisas-u-bike/commit/e744482a5f3c7519258d0204078521da59b5dfca))

## [1.3.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.3.1...v1.3.2) (2021-04-19)

### Bug Fixes

- **application:** calculate daily co2 emissions ([96d5309](https://gitlab.com/fi-sas/fisas-u-bike/commit/96d530988b2e2363d10523fb66fbe485ed61b005))

## [1.3.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.3.0...v1.3.1) (2021-04-19)

### Bug Fixes

- **application:** add user data to application ([1ebbcec](https://gitlab.com/fi-sas/fisas-u-bike/commit/1ebbcec3d046b19109d7461e1666db442d46f828))
- **application:** get user data form ctx.meta ([98965ba](https://gitlab.com/fi-sas/fisas-u-bike/commit/98965ba4844cae37e2eb95ebad97b262ffc53841))

# [1.3.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.2.1...v1.3.0) (2021-04-15)

### Bug Fixes

- **applications:** fix application dashboard by day error ([94d6e5c](https://gitlab.com/fi-sas/fisas-u-bike/commit/94d6e5ccfeed5a67c4bb6b11ff4198f5909cd873))
- **applications:** fix some validations ([120448a](https://gitlab.com/fi-sas/fisas-u-bike/commit/120448aa4540acdbc326ede72db2efede3fdc457))
- **applications:** save rounded numbers ([88816ec](https://gitlab.com/fi-sas/fisas-u-bike/commit/88816ec349e39ef988712ec4a883b689ae5a47e5))

### Features

- **bikes:** get all disponible bikes ([ec44713](https://gitlab.com/fi-sas/fisas-u-bike/commit/ec4471398a60b9c35863624de570b325e18eef9b))

## [1.2.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.2.0...v1.2.1) (2021-03-30)

### Bug Fixes

- **stats:** add scopes to stats service ([8ab2dff](https://gitlab.com/fi-sas/fisas-u-bike/commit/8ab2dff2161ebe60157ec103b6b8e688a8764e24))

# [1.2.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.1.4...v1.2.0) (2021-03-29)

### Features

- **application_forms:** add optional to not required fields ([ce82bc9](https://gitlab.com/fi-sas/fisas-u-bike/commit/ce82bc9427af4e39c7d7d37d811ad444d2c95f97))

## [1.1.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.1.3...v1.1.4) (2021-03-19)

### Bug Fixes

- **stats:** change list to find and fix cache ([fb15b35](https://gitlab.com/fi-sas/fisas-u-bike/commit/fb15b35cfba4ee0218df3ddaa01aab727ca2f741))
- **stats:** fix errors on stats requests ([8c025d9](https://gitlab.com/fi-sas/fisas-u-bike/commit/8c025d9695c3f59ff89f5685015d882b1eb78074))
- **trips:** add missing optionals ([2e96d86](https://gitlab.com/fi-sas/fisas-u-bike/commit/2e96d86c4393885682b9b07a8b01a05d70c9fc29))

## [1.1.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.1.2...v1.1.3) (2021-03-17)

### Bug Fixes

- **applications:** add missing field on dashboard ([65283d6](https://gitlab.com/fi-sas/fisas-u-bike/commit/65283d65b32decf5c041edc8814ae0a9b5564f20))
- **applications:** fix dashboard date filter ([a3bae6d](https://gitlab.com/fi-sas/fisas-u-bike/commit/a3bae6d2e86eacbf4a55813e1f717223f630315e))

## [1.1.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.1.1...v1.1.2) (2021-03-17)

### Bug Fixes

- **application_forms:** add missing optionals ([b922ce9](https://gitlab.com/fi-sas/fisas-u-bike/commit/b922ce9c8dd5985ed98166c8751229b141ff6f6d))
- **application-forms:** return logged user applications-forms by device ([c4a96ba](https://gitlab.com/fi-sas/fisas-u-bike/commit/c4a96baa8bc6feca426f69356359b3e70d026a95))
- **applications:** add can apply endpoint ([b04a426](https://gitlab.com/fi-sas/fisas-u-bike/commit/b04a4265da6cc2c0a69bcb5b177094f5d447896d))
- **applications:** limit dashboard numbers decimal cases and dashboad request fixs ([5620031](https://gitlab.com/fi-sas/fisas-u-bike/commit/5620031a6c4b4c5cd6592913c47f8e4647cfbf13))
- **trips:** change trips list to default query filters ([6286ef3](https://gitlab.com/fi-sas/fisas-u-bike/commit/6286ef323ef7db3026b46023dcc5c06d51977ea9))

## [1.1.1](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.1.0...v1.1.1) (2021-03-10)

### Bug Fixes

- **applications:** change period on wp widget information ([2d7a47d](https://gitlab.com/fi-sas/fisas-u-bike/commit/2d7a47dd44fa9ef313f51bbf8967fe80db83e747))

# [1.1.0](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0...v1.1.0) (2021-03-09)

### Bug Fixes

- **trips:** add user_id filter ([51cd51b](https://gitlab.com/fi-sas/fisas-u-bike/commit/51cd51be74d37365e72c6c0bf0577038cc6a6706))

### Features

- **applications:** add endpoint to WP widget information ([79a4146](https://gitlab.com/fi-sas/fisas-u-bike/commit/79a4146b5a24208474191c197f49af17d9dccebe))

# 1.0.0 (2021-03-09)

### Bug Fixes

- **applcation-forms:** change the enum of subject field ([091161c](https://gitlab.com/fi-sas/fisas-u-bike/commit/091161ce77e41b764c17ecc8277009a6607a24d1))
- **applicaions:** fix methd of co2 calculation ([ed0249b](https://gitlab.com/fi-sas/fisas-u-bike/commit/ed0249bf24f48b902c8670a55df7767c6f97a266))
- **application:** consent_data is not allowed error ([2906281](https://gitlab.com/fi-sas/fisas-u-bike/commit/2906281cba4ef1c03c4e342680e0879c77e5d4db))
- **application:** consent_data is not allowed error ([d9f52c1](https://gitlab.com/fi-sas/fisas-u-bike/commit/d9f52c1b471a391e579029f976cc0f5ae0c7e1ee))
- **application:** consent_date is a automatic field ([235d8db](https://gitlab.com/fi-sas/fisas-u-bike/commit/235d8dbad5df2b8919af8f46018d3bd521008f29))
- **application:** remove consent_date ([3c0e6c1](https://gitlab.com/fi-sas/fisas-u-bike/commit/3c0e6c18c9e78f2955d7ec96858a34e55c92a16d))
- **application:** remove consent_date ([dc4fa8c](https://gitlab.com/fi-sas/fisas-u-bike/commit/dc4fa8c034944265f26577037cd2f7324da6ddb6))
- **application:** sudent number is not required anymore ([bde6184](https://gitlab.com/fi-sas/fisas-u-bike/commit/bde6184c2051ab1a76d45af40847deea3d6a77ce))
- **application:** verify if application dates are bewteen configured dates ([c244421](https://gitlab.com/fi-sas/fisas-u-bike/commit/c244421a0f5220ef3f4aac9ed87f4c4b3de5fcbe))
- **applicationForm:** fix applicationForm state machines ([d1f75cc](https://gitlab.com/fi-sas/fisas-u-bike/commit/d1f75ccebfc2f6ecf9b67b9966d40e52983f61d9))
- **applications:** add burned_calories to dashboard ([a9552cf](https://gitlab.com/fi-sas/fisas-u-bike/commit/a9552cff008cc4cf1937ce5a02ab49b74afae2ea))
- **applications:** add closed status ([614375b](https://gitlab.com/fi-sas/fisas-u-bike/commit/614375b125047f4f30089c91f1ab9b31f388d2f4))
- **applications:** add closed status ([e5b2cc6](https://gitlab.com/fi-sas/fisas-u-bike/commit/e5b2cc68ba2ba087f77d2d95b3ced342d168a6b2))
- **applications:** add perios of time to dashboard ([47b1dfc](https://gitlab.com/fi-sas/fisas-u-bike/commit/47b1dfca601ece7d2b74c54c9eb866e94e362b76))
- **applications:** change dashboard status ([77f969a](https://gitlab.com/fi-sas/fisas-u-bike/commit/77f969a32c486f331868f527f115dce1e675a427))
- **applications:** change field contract_id to contract_file_id ([02e6a00](https://gitlab.com/fi-sas/fisas-u-bike/commit/02e6a00a0f75022106e1c1e04a5519d709c4d583))
- **applications:** change phone_number valitation ([16449c6](https://gitlab.com/fi-sas/fisas-u-bike/commit/16449c689d2696326554328c78eb6e58008f5018))
- **applications:** dashboard send 0 in case of 0 trips ([947e923](https://gitlab.com/fi-sas/fisas-u-bike/commit/947e92383e313389e85362ae45235655981a0b5e))
- **applications:** destroy trips on delete ([5f1f3eb](https://gitlab.com/fi-sas/fisas-u-bike/commit/5f1f3eb6f3e05c08194ed08b7ee465503d0e6220))
- **applications:** fix applications dashboard filter ([b9fdcf3](https://gitlab.com/fi-sas/fisas-u-bike/commit/b9fdcf399f011f03c6ca5d6d74a66f5427fa52a5))
- **applications:** fix co2 calculation function ([fc9784c](https://gitlab.com/fi-sas/fisas-u-bike/commit/fc9784c5838f8aa0533d64915fd0d2162cf0abd9))
- **applications:** fix co2 method calculation ([482e27e](https://gitlab.com/fi-sas/fisas-u-bike/commit/482e27e90ce13107bc7d500f6777779ec05ee519))
- **applications:** fix condition on co2 calculation method ([24dc567](https://gitlab.com/fi-sas/fisas-u-bike/commit/24dc567dca22f837016b9092c617796e28b37dc2))
- **applications:** fix default value of status column ([da882ad](https://gitlab.com/fi-sas/fisas-u-bike/commit/da882addccb652636084a3b4f954762def72fb71))
- **applications:** reanalyse cant be change directly be status endpoint ([9e4e9ef](https://gitlab.com/fi-sas/fisas-u-bike/commit/9e4e9efdfa29d2d5b942c01818d07e3346f87611))
- **applications:** remove decision and bike_id on reanalyse ([a543e83](https://gitlab.com/fi-sas/fisas-u-bike/commit/a543e83bde6f52c33eae0510309e3368a12a7a22))
- **applications:** remove fuel_type field ([9c927fb](https://gitlab.com/fi-sas/fisas-u-bike/commit/9c927fbc722274634a795432309ab4d40dbafb0b))
- **applications:** remove student_number not null ([09aa246](https://gitlab.com/fi-sas/fisas-u-bike/commit/09aa2462def61a6a27ccb76c7d687b147d81f03c))
- **applicationsForm:** block application to only one complain ([de441b7](https://gitlab.com/fi-sas/fisas-u-bike/commit/de441b7da006a73b8958ccf74fbd904bf1f7678e))
- **bike:** bike_model query order after validate ([22e64b4](https://gitlab.com/fi-sas/fisas-u-bike/commit/22e64b4a8155af166b01d569644ce1ad2b296e22))
- **bikes:** fix bike relaions ([29dae04](https://gitlab.com/fi-sas/fisas-u-bike/commit/29dae04c7b4f2036b61f72a519ae1f5c4004ae18))
- **bikes:** get free bikes query ([dfc605b](https://gitlab.com/fi-sas/fisas-u-bike/commit/dfc605b2372c9ccf54eba3dbf9ac04b36a938ae9))
- **bikes:** get free bikes query ([b17b645](https://gitlab.com/fi-sas/fisas-u-bike/commit/b17b6455a2b06a2ac846fc526eb622780b898ed3))
- **helpers:** calorie formula fix ([2227083](https://gitlab.com/fi-sas/fisas-u-bike/commit/2227083df3bdfe5429670cf8b09d158404811e72))
- **helpers:** fix energy formula ([e3d68d4](https://gitlab.com/fi-sas/fisas-u-bike/commit/e3d68d4d62505621ac9d78f6f133e2942acb5fcb))
- **migrations:** fix migration ([314b305](https://gitlab.com/fi-sas/fisas-u-bike/commit/314b30531330b4a3a3e4d55f8cb16062712ffa65))
- **trip:** add related application ([ef6caad](https://gitlab.com/fi-sas/fisas-u-bike/commit/ef6caad0e36b3b06fc90ba460e0e14816039a449))
- **trips:** bike_id from application ([55d323a](https://gitlab.com/fi-sas/fisas-u-bike/commit/55d323a552220017b8e93657c120e0dc37232e92))
- **trips:** bike_id from application ([463cbae](https://gitlab.com/fi-sas/fisas-u-bike/commit/463cbaefd85639ff1b3e2a2e1ef0543838c920f6))
- **trips:** add bike model ([5af090f](https://gitlab.com/fi-sas/fisas-u-bike/commit/5af090f1c2a497b3bc962ad0a11908d948e03ff6))
- **trips:** add date filters on list ([73b4bef](https://gitlab.com/fi-sas/fisas-u-bike/commit/73b4bef9a7140ab7296cb4c37d87813f86c5ba4f))
- accepting null notes @ application form status schema ([5be0eb9](https://gitlab.com/fi-sas/fisas-u-bike/commit/5be0eb9c6aa05749ea955ab2aa0275b719bcc590))
- add admin validation to admin request on application forms ([d11240b](https://gitlab.com/fi-sas/fisas-u-bike/commit/d11240bb37369a519dad8be0170dba6234272392))
- added default external relation asset @ bike model ([c8b5203](https://gitlab.com/fi-sas/fisas-u-bike/commit/c8b52032d137dd579191ed0e1afcc6e686ab9d77))
- added default external relation user @ trip model ([f4876c4](https://gitlab.com/fi-sas/fisas-u-bike/commit/f4876c466031a44f72a93167408193caf7f193ea))
- added externalWithRelation asset @ bike ([b0cabe4](https://gitlab.com/fi-sas/fisas-u-bike/commit/b0cabe4b19912e8e70d601c2b750683211bebefe))
- added typology for bikes relation @ bike model ([f6bf39f](https://gitlab.com/fi-sas/fisas-u-bike/commit/f6bf39f5fca7544570df69fd8c4b1de25ce6cddc))
- adding external with relation @ bikes route ([b8c78b7](https://gitlab.com/fi-sas/fisas-u-bike/commit/b8c78b7f27819f0adb6f78c1265839ad5f259553))
- adding file external relation @ GET application form ([c66a14e](https://gitlab.com/fi-sas/fisas-u-bike/commit/c66a14effa7a8dadbe93bc5b3c9f25d2c9866ad3))
- bike_id already in use @ application ([3f58618](https://gitlab.com/fi-sas/fisas-u-bike/commit/3f586189fb1eb2bf8753e04783637305bc02d8d3))
- bug on filterOrderAndFetch default case should be eq not like ([62ff88c](https://gitlab.com/fi-sas/fisas-u-bike/commit/62ff88c22bc023f654c2a6bac1b4d4cd0e1f3f4e))
- change applications_form to application-forms ([abc9faa](https://gitlab.com/fi-sas/fisas-u-bike/commit/abc9faaf4d85a14287ad9bd842ea333d3a765d35))
- change typology_first_preference to typology_first_preference_id ([bc14b24](https://gitlab.com/fi-sas/fisas-u-bike/commit/bc14b247e8a92cfa73b53c97d502674c7a40b78e))
- client/id/status valid actions ([eb4fd9d](https://gitlab.com/fi-sas/fisas-u-bike/commit/eb4fd9da3cc6ac056aba2309202962b90fd4cc47))
- error missing db on service init ([5a16dc2](https://gitlab.com/fi-sas/fisas-u-bike/commit/5a16dc219674d95bf77c7aa56b09e6ca4f0507d5))
- fix typologies seed ([e3c5806](https://gitlab.com/fi-sas/fisas-u-bike/commit/e3c58064442d562042df356b2ac8849781ea742f))
- issue [#94](https://gitlab.com/fi-sas/fisas-u-bike/issues/94) and error on validation ([1da0a31](https://gitlab.com/fi-sas/fisas-u-bike/commit/1da0a3198c91f88d72e8e1d288865b21219951f7))
- minor fix ([7706c9e](https://gitlab.com/fi-sas/fisas-u-bike/commit/7706c9e76f77dbc594323567d4f0ce4a223a4b04))
- remove required application on client/:id/status ([fb4bc31](https://gitlab.com/fi-sas/fisas-u-bike/commit/fb4bc313fbf3058b8095f14b130ee411ad329ff4))
- remove required field on application ([88d5f7f](https://gitlab.com/fi-sas/fisas-u-bike/commit/88d5f7f088b49bafc6414a2dee4b70ae1debfd6d))
- remove required fields and fix error on trips ([6752bea](https://gitlab.com/fi-sas/fisas-u-bike/commit/6752bea24b8a78a19f7ef2dd59909ad83c61f334))
- **application:** add new fields ([9ff8309](https://gitlab.com/fi-sas/fisas-u-bike/commit/9ff8309650178d25aa789109abd102f870cad91d))
- **application:** notifications data ([9b8c2d0](https://gitlab.com/fi-sas/fisas-u-bike/commit/9b8c2d0d3075be53470835df5378a9535c0fbab6))
- **application s:** change the name of the events name ([044d9e9](https://gitlab.com/fi-sas/fisas-u-bike/commit/044d9e980e1af8ec5bd943f2a9b5204ca32dc06b))
- **applications:** dashboard return empty on no active application ([bff6def](https://gitlab.com/fi-sas/fisas-u-bike/commit/bff6defdf8f57b0bb7873d069045b3c93a15c9c2))
- **applications:** fix typo ([aa44991](https://gitlab.com/fi-sas/fisas-u-bike/commit/aa44991ec34185a971cfc598bd4b7bcff9cd10da))
- **bike stats:** add daily commute co2 measures ([cae3ace](https://gitlab.com/fi-sas/fisas-u-bike/commit/cae3ace5f6acf1c224731537e4d93dd7120d6682))
- **bike stats:** add new measures ([8420210](https://gitlab.com/fi-sas/fisas-u-bike/commit/8420210e6a9b07969beab72b9dc6c51f94003dfc))
- **bike stats:** fix response data ([fd178ea](https://gitlab.com/fi-sas/fisas-u-bike/commit/fd178eae9a8bc277cb1e10bc1ebe603746d7c104))
- **configs:** error on get null ([003cdbc](https://gitlab.com/fi-sas/fisas-u-bike/commit/003cdbce62873f79978a5884416716f652062749))
- **docker-compose:** add docker-compose for prod and dev ([3669579](https://gitlab.com/fi-sas/fisas-u-bike/commit/3669579b12bad22671dfd322c09dbe6a0505c54f))
- **docker-compose:** create docker-compose to prod and dev ([d6c0275](https://gitlab.com/fi-sas/fisas-u-bike/commit/d6c02756e5f0599e1412e98c77d790b669645a61))
- **docker-compose:** delete docker-compose.yml ([18d38c9](https://gitlab.com/fi-sas/fisas-u-bike/commit/18d38c97068d9cc4025b6a91f7fa16578ab83405))
- **docker-compose.prod:** add restart ([0dcd0cb](https://gitlab.com/fi-sas/fisas-u-bike/commit/0dcd0cbac7585bd3579ce2fb0c22e22b60d384c5))
- **package:** add new lib to copy files ([d55ae7f](https://gitlab.com/fi-sas/fisas-u-bike/commit/d55ae7f9f4fcb624e7a1a6150b0a7e69c2c9f015))
- **package:** fix comands ([30256a0](https://gitlab.com/fi-sas/fisas-u-bike/commit/30256a07d8d3140c3d8ece6434fa5236e3cfbc48))
- **readme:** fix readme ([09a1aa0](https://gitlab.com/fi-sas/fisas-u-bike/commit/09a1aa065b8f30833c1149969f5c8c652f75e0db))
- **trips:** registration of kilometers by the backoffice ([e76ed4e](https://gitlab.com/fi-sas/fisas-u-bike/commit/e76ed4e2f0ba184317bcf95e85d1e157560f8548))
- **u-bike:** create bike without asset ([f62b905](https://gitlab.com/fi-sas/fisas-u-bike/commit/f62b905098f4c549910aa66ee81771dc39ef2efd))
- fixing andWhere is not a function @ application model ([2a5899c](https://gitlab.com/fi-sas/fisas-u-bike/commit/2a5899cb27ba4375516b7d1a992261c586e496c5))
- fixing typology toJSON behaviour @ bike model ([c4f0ca6](https://gitlab.com/fi-sas/fisas-u-bike/commit/c4f0ca6c7f866649959c7a3ee02aea1330ffaa21))
- user_id check ([c8ba08a](https://gitlab.com/fi-sas/fisas-u-bike/commit/c8ba08a81a4ea5c0445d2d1dc09f75cdefb9e66f))
- wrong value external relation @ bike model ([735a4fe](https://gitlab.com/fi-sas/fisas-u-bike/commit/735a4fe6ae60ff808ba869bf7cd2dec4d2a133b8))
- **migration:** remove dot ([cb2ef71](https://gitlab.com/fi-sas/fisas-u-bike/commit/cb2ef7168957335f2dcacc5291ff430b982cf8cf))
- **trip and application:** add withRelated to bike ([8e875f5](https://gitlab.com/fi-sas/fisas-u-bike/commit/8e875f5a88a24c42fee359f89c02f04a235cc84a))
- **trip and applications:** fix withRelated bike ([6f16af9](https://gitlab.com/fi-sas/fisas-u-bike/commit/6f16af9b34f9f2d86ce3446340a57a642c684886))
- **trips:** add new formula to calories ([6195699](https://gitlab.com/fi-sas/fisas-u-bike/commit/6195699971adc8a22c2b6ae8e284ae2ba0f584db))
- **trips:** check if the application hava a bike_id before new ([bafbd06](https://gitlab.com/fi-sas/fisas-u-bike/commit/bafbd0656410f540b5be179724f1b7392d66fd3c))
- **trips:** error on bike not associated ([98f3979](https://gitlab.com/fi-sas/fisas-u-bike/commit/98f3979c162926a694ed419303f929d5d609288b))
- **trips:** fix application fetch method ([187b318](https://gitlab.com/fi-sas/fisas-u-bike/commit/187b3187935149bb2f20dfff813d56d874c43a93))
- **trips:** getting bike id from entity ([d2f509d](https://gitlab.com/fi-sas/fisas-u-bike/commit/d2f509dccc0155011fb038e283be78b4c67141dd))
- **trips:** remove elastic search from ubike ([0d31b21](https://gitlab.com/fi-sas/fisas-u-bike/commit/0d31b21af574bbfebc73e59096c6664ff2068638))
- **trips:** round calories value ([779eb35](https://gitlab.com/fi-sas/fisas-u-bike/commit/779eb35d4869c686ca4cb593d95a0cf44732ee93))
- **trips:** search active application of the user ([9b0a432](https://gitlab.com/fi-sas/fisas-u-bike/commit/9b0a432d85543b670e8681a94fe211e36d4bbb7c))
- **trips:** trip creation validations ([552c604](https://gitlab.com/fi-sas/fisas-u-bike/commit/552c60477f5ed4349b5730c636a4478f804e0afa))
- **trips:** trip creation validations ([9128a30](https://gitlab.com/fi-sas/fisas-u-bike/commit/9128a30f241ed64095a2229a00c875b0ae3c5e9d))
- **trips:** values of trips in kilometrers ([652bde5](https://gitlab.com/fi-sas/fisas-u-bike/commit/652bde552b9d9252db438a3f32bcc365fd670038))
- **u-bike:** add asset fields in create bike ([160d8fb](https://gitlab.com/fi-sas/fisas-u-bike/commit/160d8fb00f53eb66b4b68ff59266b2519b34331c))

### Features

- **application:** add contract file to application ([6f1dabd](https://gitlab.com/fi-sas/fisas-u-bike/commit/6f1dabd53e1e9e890e300071bbf951766f266d33))
- **applications:** get dashboard information request ([3d13775](https://gitlab.com/fi-sas/fisas-u-bike/commit/3d13775dea0907303bbc01096ce81151ee5ed2fa))
- **moleculer:** add new version ([9329b7d](https://gitlab.com/fi-sas/fisas-u-bike/commit/9329b7d54beb24588c7593b859b70139f7f23253))
- **trips:** add trip_end and time calculation. Change is_weekend and is_holiday to boolean ([3838406](https://gitlab.com/fi-sas/fisas-u-bike/commit/3838406299109b43d0c21595e5fcda7c4ba0d2a6))
- add application cancel option by admin ([79f9b36](https://gitlab.com/fi-sas/fisas-u-bike/commit/79f9b368f0d645b3806f2d20ff8f6884bbca2567))
- added endpoint for statistics related application forms ([5434ed4](https://gitlab.com/fi-sas/fisas-u-bike/commit/5434ed44d949bf38e2841f4a4beb6b4da6f46f2e))
- added file into cover and conditions @ POST application-homepage ([e7facc5](https://gitlab.com/fi-sas/fisas-u-bike/commit/e7facc58dadf2737c39b096d531b5ad8d02e66b3))
- adding custom filters for dates @ application ([f37f1c2](https://gitlab.com/fi-sas/fisas-u-bike/commit/f37f1c28e57b79bb65d9ccb3d39b368bc9bf8bbd))
- adding operator like behaviour @ filterOrderAndFetch ([53f83f7](https://gitlab.com/fi-sas/fisas-u-bike/commit/53f83f77e175b359c418de13d54237c6479149ac))
- adding user external relation @ application ([676c896](https://gitlab.com/fi-sas/fisas-u-bike/commit/676c8966ada98c2ec3706aab9bbdcbb361ff3085))
- candidaturas: Notificações e Lembretes (Issue: [#146](https://gitlab.com/fi-sas/fisas-u-bike/issues/146)) ([649356d](https://gitlab.com/fi-sas/fisas-u-bike/commit/649356dec53e00bc3ab4638d8c14e081720e40fe))
- create boilerplate structure ([4508afe](https://gitlab.com/fi-sas/fisas-u-bike/commit/4508afe7fadc4edbd71c4f76bfebde909453f994))
- migration MS to new boilerplate ([ce8c6d2](https://gitlab.com/fi-sas/fisas-u-bike/commit/ce8c6d2e58a4f853849151091e276552e6517bbb))
- removed endpoint related to number of bikes config ([0187e2c](https://gitlab.com/fi-sas/fisas-u-bike/commit/0187e2ce0ac2eeeb7b97082aba21014056db6198))
- **applicationForm:** add applicationForm new status and funcionalities ([c9b600c](https://gitlab.com/fi-sas/fisas-u-bike/commit/c9b600cc231620426435b0fad909c76eb07aec2a))
- **applicationForm:** inital complain state machine ([aa4f765](https://gitlab.com/fi-sas/fisas-u-bike/commit/aa4f765d33b5be73ec2d9894606fc01f95146bad))
- **applications:** add applications status notifications ([ee9c655](https://gitlab.com/fi-sas/fisas-u-bike/commit/ee9c655520764bcaad66bd34cc9bd9ac07a30a7f))
- **applications:** add dashboard endpoint ([0e9ffd7](https://gitlab.com/fi-sas/fisas-u-bike/commit/0e9ffd75ac981254ed2bac8518483d969d872847))
- **applications:** add index of co2 to application creation ([533e7ac](https://gitlab.com/fi-sas/fisas-u-bike/commit/533e7ac78398da0ff4f2a11a746069227a47f683))
- **applications:** add new indices to dashboard ([70a7f79](https://gitlab.com/fi-sas/fisas-u-bike/commit/70a7f79bbfa57d061bb3eef63ab77f6c18645d84))
- **applications:** add other_typology_preference field ([f0bb990](https://gitlab.com/fi-sas/fisas-u-bike/commit/f0bb99033ddd163194d7a5b9808aa949413794ac))
- **applications:** added totl burned calories to dashboard ([1b329d7](https://gitlab.com/fi-sas/fisas-u-bike/commit/1b329d7bd1b9078a016250359a2f956d3d0abe7d))
- **applications:** initial version of state machine ([cdbe11c](https://gitlab.com/fi-sas/fisas-u-bike/commit/cdbe11c038e1465eb251080f6a8d162c448d4265))
- **bike-maintenance:** add date field to bike maintenance ([867c21f](https://gitlab.com/fi-sas/fisas-u-bike/commit/867c21fb99ab3fc53259b82afa81118ea10127f9))
- **bike-models:** add bike model to the ubike service ([badeb7e](https://gitlab.com/fi-sas/fisas-u-bike/commit/badeb7e0ed8dd7cb652d62fa1c4e6cb640f7e87d))
- **bikes:** add free filter to bikes find ([9c2b6cf](https://gitlab.com/fi-sas/fisas-u-bike/commit/9c2b6cfd352d69388152a5745c42ecf2b9c72ce2))
- **trips:** allow backoffice to insert trips ([3d0549d](https://gitlab.com/fi-sas/fisas-u-bike/commit/3d0549db065561cfb01ae04298d329872afa2380))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-02-15)

### Bug Fixes

- **applications:** change field contract_id to contract_file_id ([02e6a00](https://gitlab.com/fi-sas/fisas-u-bike/commit/02e6a00a0f75022106e1c1e04a5519d709c4d583))

### Features

- **application:** add contract file to application ([6f1dabd](https://gitlab.com/fi-sas/fisas-u-bike/commit/6f1dabd53e1e9e890e300071bbf951766f266d33))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-02-12)

### Bug Fixes

- **application:** verify if application dates are bewteen configured dates ([c244421](https://gitlab.com/fi-sas/fisas-u-bike/commit/c244421a0f5220ef3f4aac9ed87f4c4b3de5fcbe))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-02-12)

### Features

- **trips:** add trip_end and time calculation. Change is_weekend and is_holiday to boolean ([3838406](https://gitlab.com/fi-sas/fisas-u-bike/commit/3838406299109b43d0c21595e5fcda7c4ba0d2a6))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-11)

### Features

- **applications:** get dashboard information request ([3d13775](https://gitlab.com/fi-sas/fisas-u-bike/commit/3d13775dea0907303bbc01096ce81151ee5ed2fa))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-05)

### Bug Fixes

- **trips:** add date filters on list ([73b4bef](https://gitlab.com/fi-sas/fisas-u-bike/commit/73b4bef9a7140ab7296cb4c37d87813f86c5ba4f))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-01-28)

### Bug Fixes

- client/id/status valid actions ([eb4fd9d](https://gitlab.com/fi-sas/fisas-u-bike/commit/eb4fd9da3cc6ac056aba2309202962b90fd4cc47))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-01-28)

### Bug Fixes

- change applications_form to application-forms ([abc9faa](https://gitlab.com/fi-sas/fisas-u-bike/commit/abc9faaf4d85a14287ad9bd842ea333d3a765d35))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-27)

### Bug Fixes

- remove required fields and fix error on trips ([6752bea](https://gitlab.com/fi-sas/fisas-u-bike/commit/6752bea24b8a78a19f7ef2dd59909ad83c61f334))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-27)

### Bug Fixes

- fix typologies seed ([e3c5806](https://gitlab.com/fi-sas/fisas-u-bike/commit/e3c58064442d562042df356b2ac8849781ea742f))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-27)

### Bug Fixes

- change typology_first_preference to typology_first_preference_id ([bc14b24](https://gitlab.com/fi-sas/fisas-u-bike/commit/bc14b247e8a92cfa73b53c97d502674c7a40b78e))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-27)

### Bug Fixes

- minor fix ([7706c9e](https://gitlab.com/fi-sas/fisas-u-bike/commit/7706c9e76f77dbc594323567d4f0ce4a223a4b04))
- remove required application on client/:id/status ([fb4bc31](https://gitlab.com/fi-sas/fisas-u-bike/commit/fb4bc313fbf3058b8095f14b130ee411ad329ff4))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-26)

### Bug Fixes

- remove required field on application ([88d5f7f](https://gitlab.com/fi-sas/fisas-u-bike/commit/88d5f7f088b49bafc6414a2dee4b70ae1debfd6d))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-01-05)

### Bug Fixes

- **applications:** dashboard return empty on no active application ([bff6def](https://gitlab.com/fi-sas/fisas-u-bike/commit/bff6defdf8f57b0bb7873d069045b3c93a15c9c2))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-22)

### Bug Fixes

- **configs:** error on get null ([003cdbc](https://gitlab.com/fi-sas/fisas-u-bike/commit/003cdbce62873f79978a5884416716f652062749))
- error missing db on service init ([5a16dc2](https://gitlab.com/fi-sas/fisas-u-bike/commit/5a16dc219674d95bf77c7aa56b09e6ca4f0507d5))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-u-bike/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-21)

### Features

- **moleculer:** add new version ([9329b7d](https://gitlab.com/fi-sas/fisas-u-bike/commit/9329b7d54beb24588c7593b859b70139f7f23253))
