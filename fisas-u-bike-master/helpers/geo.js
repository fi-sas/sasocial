const geolib = require("geolib");

/**
 * Generated Watts
 * Font : https://www.omnicalculator.com/sports/cycling-wattage
 * @param weight user weigth in kg
 * @param avgSpeed avarage speed of the trip in km/h
 */
function getGeneratedWatts({ weight, avgSpeed }) {
	const equipmentWeight = 8;
	const g = 9.80655;
	const v = (avgSpeed * 1000) / 3600;
	const Fr = g * Math.cos(Math.atan(0)) * (weight + equipmentWeight) * 0.005; // Fr = g * cos(arctan(slope)) * (M + m) * Crr
	const Fa = 0.2499 * Math.sqrt(v); // Fa = 0.5 * Cd * A * ρ * (v + w)²
	return roundWithPrecision(((Fr + Fa) * v) / (1 - 0.05), 2);
}

function getAvgSpeed(distanceP, time) {
	return roundWithPrecision(distanceP / time, 2);
}

/**
 * Calculates the METS/H value accoring with this font
 * https://community.plu.edu/~chasega/met.html#1
 * https://keisan.casio.com/exec/system/1350958587
 * @param {number} distanceP The disatance of the trip in km
 * @param {number} time The duration of the trip in hours
 */
function getMets(distanceP, time) {
	const avgSpeed = getAvgSpeed(distanceP, time);

	switch (true) {
		case avgSpeed < 16.1:
			return 4;
		case avgSpeed >= 16.1 && avgSpeed <= 19.1:
			return 6.8;
		case avgSpeed >= 19.2 && avgSpeed <= 22.4:
			return 8;
		case avgSpeed >= 22.5 && avgSpeed <= 25.7:
			return 10;
		case avgSpeed >= 25.8 && avgSpeed <= 32.1:
			return 13;
		case avgSpeed >= 32.2:
			return 15.8;
		default:
			return 4;
	}
}

/**
 * Calculates the BMR of the user the font of the formula has
 * https://www.thecalculatorsite.com/articles/health/bmr-formula.php
 * MEN - 66.47 + ( 13.75 × weight in kg ) + ( 5.003 × height in cm ) − ( 6.755 × age in years )
 * WOMEN - 655.1 + ( 9.563 × weight in kg ) + ( 1.85 × height in cm ) − ( 4.676 × age in years )
 * @param {string} data.gender Either 'M' or 'F'.
 * @param {number} data.age The age of the bike rider (20 for instance).
 * @param {float} data.weight The weight of the bike rider (in kg).
 * @param {float} data.height The weight of the bike rider (in meters).
 */
function getBMR(gender, age, weight, height) {
	switch (gender) {
		case "M":
			return roundWithPrecision(66.47 + 13.75 * weight + 5.003 * (height / 0.01) - 6.755 * age, 2);
		case "F":
			return roundWithPrecision(655.1 + 9.563 * weight + 1.85 * (height / 0.01) - 4.676 * age, 2);
		default:
			return roundWithPrecision(66.47 + 13.75 * weight + 5.003 * (height / 0.01) - 6.755 * age, 2);
	}
}

/**
 * Rounds the provided float value with the provided number of decimal places (precision).
 *
 * @param {Float} num The float to round.
 * @param {Number} precision The number of decimal places that should be taken into account.
 * @returns {Float} The rounded value.
 */
function roundWithPrecision(num, precision) {
	const multiplier = 10 ** precision;
	return Math.round(num * multiplier) / multiplier;
}

/**
 * Calculates the distance between the two points provided.
 *
 * @param {Object} data - The data for calculating the energy,
 * @param {float} data.latA The latitude of point A.
 * @param {float} data.longA The longitude of point A.
 * @param {float} data.latB The latitude of point B.
 * @param {float} data.longB The longitude of point B.
 */
function distance({ latA, longA, latB, longB }) {
	return roundWithPrecision(
		geolib.getDistance({ latitude: latA, longitude: longA }, { latitude: latB, longitude: longB }),
		2,
	);
}

/**
 * Calculates the energy spent (in kcal) between the two points provided.
 * Formula source
 * https://keisan.casio.com/exec/system/1350958587
 * @param {Object} data - The data for calculating the energy,
 * @param {string} data.gender Either 'M' or 'F'.
 * @param {number} data.age The age of the bike rider (20 for instance).
 * @param {float} data.weight The weight of the bike rider (in kg).
 * @param {float} data.height The height of the bike rider (in meters).
 * @param {float} data.distanceP The distante of the ride (in km).
 * @param {float} data.time The time of the ride (in hours).
 */
function energy({ gender, age, weight, height, time, distanceP }) {
	const mets = getMets(distanceP, time);
	const bmr = getBMR(gender, age, weight, height);
	const result = bmr * (mets / 24) * time;
	return result.toFixed(2);
}

/**
 * Calculates the CO2 emissions saved from a bike ride, in function of the fuel type.
 *
 * Diesel: 132 g CO2/km
 * Gasoline: 120 g CO2/km
 * GPL: 83 g CO2/km
 * Electric:  38 g CO2/km
 *
 * (Assuming average consumption of fuel of 5 l / 100 km)
 * Source: http://ecoscore.be/en/info/ecoscore/co2
 *
 * The result is returned in g of CO2.
 *
 * @param {Object} data - The data for calculating the energy,
 * @param {string} data.commute - The commute veicule.
 * @param {string} data.fuelType the type of fuel the veicule use.
 * @param {float} data.dist The distance of the bike ride (in km).
 */
function co2({ commute, fuelType, dist }) {
	switch (commute) {
		case "Motorcycle":
		case "Car":
			switch (fuelType) {
				case "Diesel":
					return roundWithPrecision(132 * dist, 2);
				case "Gasoline":
					return roundWithPrecision(120 * dist, 2);
				case "GPL":
					return roundWithPrecision(83 * dist, 2);
				case "Electric":
					return roundWithPrecision(38 * dist, 2);
				default:
					return roundWithPrecision(0, 2);
			}
		case "Bus":
			return roundWithPrecision(75 * dist, 2);
		case "Train":
			return roundWithPrecision(28 * dist, 2);
		default:
			return 0;
	}
}

/**
 * Calculates the CO2 emissions saved from a bike ride, in function of the type of daily commute.
 *
 * Bus: 75 g CO2/km
 * Train: 28 g CO2/km
 *
 * Source: https://www.delijn.be/en/overdelijn/organisatie/zorgzaam-ondernemen/milieu/co2-uitstoot-voertuigen.html
 *
 * The result is returned in g of CO2.
 *
 * @param {float} dist The distance of the bike ride (in km).
 */
function daily_commute_co2(dist) {
	return {
		bus_saved_co2: roundWithPrecision(75 * dist, 2),
		train_saved_co2: roundWithPrecision(28 * dist, 2),
		gasoline_saved_co2: roundWithPrecision(120 * dist, 2),
		diesel_saved_co2: roundWithPrecision(132 * dist, 2),
		gpl_saved_co2: roundWithPrecision(83 * dist, 2),
		eletric_saved_co2: roundWithPrecision(38 * dist, 2),
	};
}

module.exports = {
	distance,
	energy,
	co2,
	getGeneratedWatts,
	getAvgSpeed,
	daily_commute_co2,
};
