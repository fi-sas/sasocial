module.exports.up = async db => db.schema
    .createTable('application_config', (table) => {
        table.increments();
        table.string('key', 255).notNullable();
        table.json('value');
    })
    .createTable('typology', (table) => {
        table.increments();
        table.string('name', 255).notNullable();
        table.boolean('active').notNullable();
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })

    .createTable('bike_model', (table) => {
        table.increments();
        table.string('model', 255).notNullable();
        table.integer('model_conformity_file_id').unsigned();
        table.string('brakes', 255);
        table.string('brake_types', 255);
        table.string('frame_type', 255);
        table.string('front_suspension', 255);
        table.string('tyres', 255);
        table.string('seat_fixation_mechanism', 255);
        table.string('front_wheel_fixation_mechanism', 255);
        table.string('back_wheel_fixation_mechanism', 255);
        table.string('handlebar_fixation_mechanism', 255);
        table.string('gear_mechanism', 255);
        table.string('chain_guard', 255);
        table.string('front_illumination', 255);
        table.string('back_illumination', 255);
        table.string('side_reflections', 255);
        table.string('pedal_reflections', 255);
        table.string('front_reflections', 255);
        table.string('back_reflections', 255);
        table.string('sound_warning', 255);
        table.string('instruction_manual', 255);
        table.string('rest', 255);
        table.string('mudguards', 255);
        table.string('luggage_support', 255);
        table.string('monitoring_system', 255);
        table.string('coating_requirements', 255);
        table.string('other_accessories', 255);
        table.integer('typology_id').unsigned().references('id').inTable('typology');
        table.datetime('created_at');
        table.datetime('updated_at').notNullable();
    })

    .createTable('bike', (table) => {
        table.increments();
        table.string('identification', 100).notNullable();
        table.string('size', 4);
        table.integer('bike_model_id').unsigned().references('id').inTable('bike_model');
        table.integer('asset_id').unsigned();
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
    })

    .createTable('application', (table) => {
        table.increments();
        table.string('full_name', 255).notNullable();
        table.datetime('birth_date').nullable();
        table.enu('gender', ['M', 'F']).nullable();
        table.float('weight').nullable();
        table.float('height').nullable();
        table.string('address', 255).nullable();
        table.string('postal_code', 255).nullable();
        table.string('email', 255).nullable();
        table.string('phone_number', 255).nullable();
        table.string('student_number', 255).nullable();
        table.datetime('start_date').nullable();
        table.datetime('end_date').nullable();
        table.integer('typology_first_preference').unsigned().references('id').inTable('typology')
            .notNullable();
        table.integer('other_typology_preference').defaultTo(0).notNullable();
        table.enu('daily_commute', ['Walk', 'Car', 'Motorcycle', 'Bus', 'Train', 'Bike', 'Other']).notNullable()
        table.enu('daily_consumption', ['Electric', 'GPL', 'Diesel', 'Gasoline']).nullable();
        table.float('daily_kms').notNullable();
        table.float('daily_co2', 12, 5).defaultTo(0).notNullable();
        table.string('manufacturer', 255);
        table.string('model', 255);
        table.integer('year_manufactured');
        table.float('engine_power');
        table.boolean('applicant_consent').notNullable();
        table.datetime('consent_date');
        table.integer('bike_id').unsigned().references('id').inTable('bike').nullable();
        table.text('pickup_location', 255).nullable();
        table.integer('user_id').unsigned();
        table.integer('number_of_times').unsigned().defaultTo(0).notNullable();
        table.float('avg_weekly_distance').defaultTo(0).notNullable();
        table.integer('total_weekly_distance').unsigned().defaultTo(0).notNullable();
        table.float('travel_time_school').defaultTo(0).notNullable();
        table.float('travel_time_school_bike').defaultTo(0).notNullable();
        table.float('avg_vehicle_consumption_100').defaultTo(0);
        table.boolean('already_complain').defaultTo(false);
        table.enu('decision', [
            'assigned',
            'enqueued',
            'unassigned'
        ]);
        table.enu('status', [
            'elaboration',
            'submitted',
            'analysis',
            'cancelled',
            'dispatch',
            'assigned',
            'enqueued',
            'unassigned',
            'suspended',
            'complaint',
            'accepted',
            'contracted',
            'rejected',
            'quiting',
            'withdrawal',
            'closed']).defaultTo('submitted').notNullable();
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })

    .createTable('application_history', (table) => {
        table.increments();
        table.integer('application_id').unsigned().references('id').inTable('application');
        table.string('status').notNullable();
        table.integer('user_id').unsigned();
        table.string('notes');
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })

    .createTable('trip', (table) => {
        table.increments();
        table.integer('bike_id').unsigned().references('id').inTable('bike')
            .notNullable();
        table.integer('bike_typology_id').unsigned().references('id').inTable('typology');
        table.string('typology_name', 255);
        table.string('manufacturer', 255);
        table.string('model', 255);
        table.enu('fuel_type', ['Electric', 'GPL', 'Diesel', 'Gasoline']);
        table.integer('year_manufactured');
        table.float('engine_power');
        table.integer('application_id').unsigned().references('id').inTable('application');
        table.integer('user_id').unsigned();
        table.integer('user_profile_id').unsigned();
        table.string('profile_name', 255);
        table.integer('user_age').unsigned();
        table.float('distance', 12, 5).notNullable();
        table.float('height_diff', 12, 5).notNullable();
        table.float('energy', 8, 2).notNullable();
        table.float('co2_emissions_saved', 12, 5).notNullable();
        table.float('time', 12, 5).notNullable();
        table.float('watts', 8, 2).defaultTo(0).notNullable();
        table.float('avg_speed', 5, 2).defaultTo(0).notNullable();
        table.text('description').nullable();
        table.datetime('trip_start').notNullable();
        table.integer('is_weekend').unsigned().notNullable();
        table.integer('is_holiday').unsigned().notNullable();
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
    })

    .createTable('application_form', (table) => {
        table.increments();
        table.enu('subject', [
            'THEFT',
            'WITHDRAWAL',
            'ABSENCE',
            'COMPLAINT',
            'CONTESTATION',
            'RENOVATION',
            'MAINTENANCE_REQUEST',
            'PRIVATE_MAINTENANCE',
            'ADDITIONAL_MAINTENANCE_KIT',
            'BREAKDOWN',
        ]).notNullable();
        table.integer('application_id').unsigned().references('id').inTable('application')
            .notNullable();
        table.text('request_description').notNullable();
        table.integer('file_id').unsigned();
        table.text('answer').nullable();
        table.boolean('answered').notNullable().defaultTo(false);
        table.boolean('read').notNullable().defaultTo(false);
        table.enu('decision', ['approved', 'rejected']);
        table.enu('status', ['submitted', 'analysis', 'approved', 'rejected', 'dispatch', 'closed']).notNullable();
        table.date('begin_date').nullable();
        table.date('end_date').nullable();
        table.datetime('created_at').notNullable();
        table.datetime('updated_at').notNullable();
    })

    .createTable('application_form_history', (table) => {
        table.increments();
        table.integer('application_form_id').unsigned().references('id').inTable('application_form');
        table.string('status').notNullable();
        table.integer('user_id').unsigned();
        table.string('notes');
        table.datetime('updated_at').notNullable();
        table.datetime('created_at').notNullable();
    })

    .createTable('application_notification_history', (table) => {
        table.increments();
        table.integer('application_id').unsigned().references('id').inTable('application');
        table.integer('alert_id').unsigned();
        table.string('alert_type_key');
        table.datetime('created_at').notNullable();
    })
    ;

module.exports.down = async db => db.schema
    .dropTable('application_form_history')
    .dropTable('application_form')
    .dropTable('trip')
    .dropTable('application_history')
    .dropTable('application')
    .dropTable('bike')
    .dropTable('bike_model')
    .dropTable('typology')
    .dropTable('application_config')
    .dropTable('application_notification_history');

module.exports.configuration = { transaction: true };
