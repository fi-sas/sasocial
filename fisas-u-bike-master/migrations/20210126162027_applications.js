module.exports.up = async db =>
	db.schema
		.alterTable('application', function (table) {
			table.boolean('other_typology_preference').alter();
			table.string('full_name', 255).nullable().alter();
		})

	;

module.exports.down = async db =>
	db.schema.alterTable('application', function (table) {
		table.integer('other_typology_preference').alter();
		table.string('full_name', 255).notNullable().alter();
	})
	;

module.exports.configuration = { transaction: true };
