module.exports.up = async db =>
	db.schema
		.table("application", (table) => {
			table.dropColumn("typology_first_preference");
			table.integer("typology_first_preference_id").unsigned().references("id").inTable("typology")
				.notNullable();
		});

module.exports.down = async db =>
	db.schema
		.table("application", (table) => {
			table.dropColumn("typology_first_preference_id");
			table.integer("typology_first_preference").unsigned().references("id").inTable("typology")
				.notNullable();
		});

module.exports.configuration = { transaction: true };
