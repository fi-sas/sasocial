
module.exports.up = async db =>
	db.schema
		.table("trip", (table) => {
			table.datetime("trip_end");
		}).alterTable("trip", (table) => {
			table.boolean("is_weekend").alter();
			table.boolean("is_holiday").alter();
		});

module.exports.down = async db =>
	db.schema
		.table("trip", (table) => {
			table.dropColumn("trip_end");
		}).alterTable("trip", (table) => {
			table.integer("is_weekend").alter();
			table.integer("is_holiday").alter();
		});

module.exports.configuration = { transaction: true };
