
module.exports.up = async db =>
	db.schema
		.table("application", (table) => {
			table.integer("contract_file_id");
		})

	;

module.exports.down = async db =>
	db.schema.
		table("application", (table) => {
			table.dropColumn("contract_file_id");
		})
	;

module.exports.configuration = { transaction: true };
