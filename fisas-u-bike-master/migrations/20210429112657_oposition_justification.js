
module.exports.up = (db) =>
	db.schema
		.table("application", (table) => {
			table.string("opposition_justification");
		});

module.exports.down = (db) =>
	db.schema.
		table("application", (table) => {
			table.dropColumn("opposition_justification");
		});

module.exports.configuration = { transaction: true };
