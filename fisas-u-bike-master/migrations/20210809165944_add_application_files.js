
module.exports.up = (db) =>
    db.schema
        .createTable('application_file', (table) => {
            table.increments();
            table.integer("application_id").unsigned().notNullable().references('id').inTable('application');
            table.integer("file_id").unsigned().notNullable();
            table.string("name", 255).notNullable();
            table.datetime('created_at').notNullable();
        })

module.exports.down = (db) =>
    db.schema
        .dropTable('application_file')

module.exports.configuration = { transaction: true };
