module.exports.up = (db) =>
	db.schema.alterTable("application", (table) => {
		table.float("daily_kms").nullable().alter();
		table.float("daily_co2", 12, 5).defaultTo(0).nullable().alter();
		table.boolean("applicant_consent").nullable().alter();
		table.integer("number_of_times").unsigned().defaultTo(0).nullable().alter();
		table.float("avg_weekly_distance").defaultTo(0).nullable().alter();
		table.integer("total_weekly_distance").unsigned().defaultTo(0).nullable().alter();
		table.float("travel_time_school").defaultTo(0).nullable().alter();
		table.float("travel_time_school_bike").defaultTo(0).nullable().alter();
		table.integer("typology_first_preference_id").unsigned().nullable().alter();
		table.text("daily_commute").nullable().alter();
	});

module.exports.down = (db) =>
	db.schema.alterTable("application", (table) => {
		table.float("daily_kms").notNullable().alter();
		table.float("daily_co2", 12, 5).defaultTo(0).notNullable().alter();
		table.boolean("applicant_consent").notNullable().alter();
		table.integer("number_of_times").unsigned().defaultTo(0).notNullable().alter();
		table.float("avg_weekly_distance").defaultTo(0).notNullable().alter();
		table.integer("total_weekly_distance").unsigned().defaultTo(0).notNullable().alter();
		table.float("travel_time_school").defaultTo(0).notNullable().alter();
		table.float("travel_time_school_bike").defaultTo(0).notNullable().alter();
		table.integer("typology_first_preference_id").unsigned().notNullable().alter();
		table.text("daily_commute").notNullable().alter();
	});

module.exports.configuration = { transaction: true };
