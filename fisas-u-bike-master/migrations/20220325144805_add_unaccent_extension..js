exports.up = (knex) => {
	return knex.raw("CREATE EXTENSION unaccent;");
};

exports.down = (knex) => {
	return knex.raw("DROP EXTENSION IF EXIST unaccent;");
};
