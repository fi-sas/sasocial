module.exports.up = async (db) =>
	db.schema
		.alterTable("application_config", (table) => {
			table.string("value").alter();
		})
		.alterTable("bike", (table) => {
			table.string("identification", 100).notNullable().alter();
		});

module.exports.down = async (db) =>
	db.schema
		.alterTable("application_config", (table) => {
			table.json("value").alter();
		})
		.alterTable("bike", (table) => {
			table.string("identification", 100).notNullable().alter();
		});

module.exports.configuration = { transaction: true };
