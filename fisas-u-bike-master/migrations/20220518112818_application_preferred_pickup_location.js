module.exports.up = async (db) =>
	db.schema.table("application", (table) => {
		table.integer("preferred_pickup_organict_unit_id").unsigned();
	});

module.exports.down = async (db) =>
	db.schema.table("application", (table) => {
		table.dropColumn("preferred_pickup_organict_unit_id");
	});

module.exports.configuration = { transaction: true };
