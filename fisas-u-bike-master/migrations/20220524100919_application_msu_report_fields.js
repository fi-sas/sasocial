module.exports.up = async (db) =>
	db.schema.table("application", (table) => {
		table.integer("delivery_reception_file_id").unsigned();
		table.integer("consent_terms_file_id").unsigned();
	});

module.exports.down = async (db) =>
	db.schema.table("application", (table) => {
		table.dropColumn("delivery_reception_file_id");
		table.dropColumn("consent_terms_file_id");
	});

module.exports.configuration = { transaction: true };
