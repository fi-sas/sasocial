exports.seed = async (knex) => {
	const data = [
		{
			key: "APPLICATION_START_DATE",
			value: "2021/01/01",
		},
		{
			key: "APPLICATION_END_DATE",
			value: "2021/12/31",
		},
		{
			key: "APPLICATION_NOTIFICATION_HOUR",
			value: "14:00",
		},
		{
			key: "APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS",
			value: "2",
		},
		{
			key: "APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY",
			value: "24",
		},
		{
			key: "APPLICATION_MISSING_TRIPS",
			value: "1",
		},
	];

	return Promise.all(
		data.map(async (d) => {
			// Check if item exist
			const rows = await knex("application_config").select().where("key", d.key);
			if (rows.length === 0) {
				await knex("application_config").insert(d);
			}
			return true;
		}),
	);
};
