exports.seed = async (knex) => {
	const data = [
		{
			name: "Elétrica",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		},
		{
			name: "Convencional",
			active: true,
			created_at: new Date(),
			updated_at: new Date(),
		}
	];

	return Promise.all(data.map(async (d) => {
		// Check if tax exist
		const rows = await knex("typology").select().where("name", d.name);
		if (rows.length === 0) {
			await knex("typology").insert(d);
		}
		return true;
	}));

};
