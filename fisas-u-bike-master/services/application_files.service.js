"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.application_files",
	table: "application_file",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "application_files")],

	/**
     * Settings
    */
	settings: {
		fields: [
			"id",
			"application_id",
			"name",
			"file_id",
			"created_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			}
		},
		entityValidator: {
			application_id: { type: "number", convert: true },
			name: { type: "string", max: 255 },
			file_id: { type: "number", convert: true },
			created_at: { type: "date", optional: true, convert: true },
		}
	},
	hooks: {
		before: {
			create: [
				"validate_application",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
				}
			],
		}
	},
	/**
     * Dependencies
     */
	dependencies: [],

	/**
     * Actions
     */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		}

	},

	/**
     * Events
     */
	events: {

	},

	/**
     * Methods
     */
	methods: {
		async validate_application(ctx) {
			const application = await ctx.call("u_bike.applications.get", { id: ctx.params.application_id });
			if (application[0].status !== "contracted") {
				throw new ValidationError(
					"This application is not contracted",
					"UBIKE_APPLICATION_NOT_CONTRACTED",
					{},
				);
			}
		}

	},

	/**
     * Service created lifecycle event handler
     */
	created() {

	},

	/**
     * Service started lifecycle event handler
     */
	async started() {

	},

	/**
     * Service stopped lifecycle event handler
     */
	async stopped() {

	}
};
