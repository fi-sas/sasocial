"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {
	APPLICATION_FORM_SUBJECT,
	APPLICATION_FORM_DECISION,
	APPLICATION_FORM_STATUS,
	APPLICATION_FORM_ACTIONS,
} = require("../values/enums");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const AbsenceStateMachine = require("../state-machines/absence.machine");
const AdditionalMaintenanceStateMachine = require("../state-machines/additional_maintenance.machine");
const ComplainStateMachine = require("../state-machines/complain.machine");
const MaintenanceRequestStateMachine = require("../state-machines/maintenance_request.machine");
const PrivateMaintenanceStateMachine = require("../state-machines/private_maintenance.machine");
const RenovationStateMachine = require("../state-machines/renovation.machine");
const TheftStateMachine = require("../state-machines/theft.machine");
const WithdrawalStateMachine = require("../state-machines/withdrawal.machine");
const ApplicationsStateMachine = require("../state-machines/application.machine");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "u_bike.application-forms",
	table: "application_form",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "application-forms")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"subject",
			"application_id",
			"request_description",
			"file_id",
			"answer",
			"answered",
			"read",
			"decision",
			"status",
			"begin_date",
			"end_date",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [/*"application",*/ "history", "file"],
		withRelateds: {
			application(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "u_bike.applications", "application", "application_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"u_bike.application_forms_history",
					"history",
					"id",
					"application_form_id",
				);
			},
		},
		entityValidator: {
			subject: { type: "enum", values: APPLICATION_FORM_SUBJECT },
			application_id: { type: "number", positive: true },
			request_description: { type: "string" },
			file_id: { type: "number", nullable: true, optional: true },
			answer: { type: "string", nullable: true, optional: true },
			answered: { type: "boolean", default: false },
			read: { type: "boolean", default: false },
			decision: { type: "enum", values: APPLICATION_FORM_DECISION, optional: true },
			status: { type: "enum", values: APPLICATION_FORM_STATUS },
			begin_date: { type: "date", convert: true, nullable: true, optional: true },
			end_date: { type: "date", convert: true, nullable: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "submitted";
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			adminApproveStatus: ["isBackofficeUser"],
			adminRejectStatus: ["isBackofficeUser"],
			list: [
				async function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query || {};
						const applications = await ctx.call("u_bike.applications.find", {
							query: { user_id: ctx.meta.user.id, status: "contracted" },
						});
						ctx.params.query.application_id = applications.length > 0 ? applications[0].id : null;
					}
				},
			],
		},
		after: {
			create: [
				async function sanatizeParams(ctx, response) {
					if (response[0].id != null) {
						await ctx.call("u_bike.application_forms_history.create", {
							application_form_id: response[0].id,
							status: response[0].status,
							user_id: ctx.meta.user.id,
							notes: null,
						});
					}
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#user.id",
				],
			},
		},
		create: {
			// REST: POST /
			rest: "POST /",
			scope: "u_bike:application-forms:create",
			visibility: "published",
			params: {
				subject: { type: "enum", values: APPLICATION_FORM_SUBJECT },
				application_id: { type: "number", positive: true },
				request_description: { type: "string" },
				file_id: { type: "number", optional: true },
				begin_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				const params = ctx.params;
				const application = await ctx.call("u_bike.applications.get", {
					id: params.application_id,
				});

				const stateMachine = await ApplicationsStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);

				if (params.subject === "COMPLAINT") {
					// Verify if application already have a complain
					if (application[0].already_complain) {
						throw new ValidationError(
							"Application already have a complain",
							"UBIKE_APPLICATION_ALREDY_COMPLAIN",
							{},
						);
					}
					// Verify if application status can be changed
					if (stateMachine.can("complain")) {
						const complaint = await this._create(ctx, params);

						application[0].already_complain = true;
						ctx.params.id = application[0].id;
						ctx.params.application = application[0];

						await ctx.call("u_bike.applications.patch", {
							...ctx.params.application,
							id: ctx.params.id,
						});

						return complaint;
					} else {
						throw new ValidationError(
							"Only can create a complaint on [assigned] or [unassigned] application status",
							"UBIKE_APPLICATION_BAD_STATUS",
							{},
						);
					}
				}

				if (params.subject === "WITHDRAWAL") {
					// Verify if application have rigth status
					if (stateMachine.can("quit")) {
						const withdrawal = await this._create(ctx, params);
						ctx.params.id = application[0].id;
						ctx.params.application = application[0];
						await stateMachine["quit"]();
						return withdrawal;
					} else {
						throw new ValidationError(
							"Only can create a withdrawal on [contracted] application status",
							"UBIKE_APPLICATION_NOT_CONTRACTED",
							{},
						);
					}
				}
				if (params.subject === "RENOVATION") {
					// Verify if application have rigth status
					if (application[0].status == "contracted") {
						return await this._create(ctx, params);
					} else {
						throw new ValidationError(
							"Only can create a renovation on [contracted] application status",
							"UBIKE_APPLICATION_NOT_CONTRACTED",
							{},
						);
					}
				}
				if (params.subject === "PRIVATE_MAINTENANCE") {
					// Verify if application have rigth status
					if (application[0].status == "contracted") {
						params.status = "closed";
						return await this._create(ctx, params);
					} else {
						throw new ValidationError(
							"Only can create a private maintenance on [contracted] application status",
							"UBIKE_APPLICATION_NOT_CONTRACTED",
							{},
						);
					}
				}
				if (params.subject === "BREAKDOWN") {
					if (application[0].status != "contracted") {
						throw new ValidationError(
							"Only can create " +
							params.subject.toLowerCase() +
							"on[contracted] application status",
							"UBIKE_APPLICATION_NOT_CONTRACTED",
							{},
						);
					}
					return await this._create(ctx, params);
				}
				if (
					["ABSENCE", "THEFT", "MAINTENANCE_REQUEST", "ADDITIONAL_MAINTENANCE_KIT"].includes(
						params.subject,
					)
				) {
					// Verify if application have rigth status
					if (application[0].status != "contracted") {
						throw new ValidationError(
							"Only can create " +
							params.subject.toLowerCase() +
							"on[contracted] application status",
							"UBIKE_APPLICATION_NOT_CONTRACTED",
							{},
						);
					}
					return await this._create(ctx, params);
				}
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		status: {
			rest: "POST /:id/status",
			scope: "u_bike:application-forms:status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				action: { type: "enum", values: APPLICATION_FORM_ACTIONS },
				answer: { type: "string", optional: true },
				decision: { type: "string", optional: true },
				notes: { type: "string", optional: true },
			},
			async handler(ctx) {
				const applicationForm = await this._get(ctx, { id: ctx.params.id });
				// Get valid state machine
				let stateMachine = await this.getStateMachineBySubject(
					ctx,
					applicationForm[0].subject,
					applicationForm[0].status,
				);
				if (stateMachine == null) {
					throw new ValidationError(
						"Application form invalid subject",
						"UBIKE_APPLICATION_FORM_INVALID_SUBJECT",
						{},
					);
				}
				if (stateMachine.can(ctx.params.action.toLowerCase())) {
					applicationForm[0].decision = ctx.params.decision;
					applicationForm[0].answer = ctx.params.answer;
					ctx.params.application_form = applicationForm[0];
					const result = await stateMachine[ctx.params.action]();
					if (result == null) {
						throw new ValidationError(
							"Don't have permissions to change state",
							"UBIKE_APPLICATION_FORM_UNAUTHORIZED",
							{},
						);
					}
					return result;
				} else {
					throw new ValidationError(
						"Impossible change the state",
						"UBIKE_APPLICATION_FORM_CHANGE_STATUS",
						{},
					);
				}
			},
		},
		adminApproveStatus: {
			rest: "POST /admin/:id/approve",
			scope: "u_bike:application-forms:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string" },
			},
			async handler(ctx) {
				const applicationForm = await this._get(ctx, { id: ctx.params.id });
				const subjectsNotIncluded = ['MAINTENANCE_REQUEST', 'ADDITIONAL_MAINTENANCE_KIT', 'THEFT', 'PRIVATE_MAINTENANCE'];

				if (applicationForm[0].status != "dispatch"
					&& !subjectsNotIncluded.includes(applicationForm[0].subject)) {
					throw new ValidationError(
						"Only can accept on [dispatch]  application form status",
						"UBIKE_APPLICATION_FORM_NOT_DISPATCH",
						{},
					);
				}
				// Get valid state machine
				let stateMachine = await this.getStateMachineBySubject(
					ctx,
					applicationForm[0].subject,
					applicationForm[0].status,
				);
				if (stateMachine == null) {
					throw new ValidationError(
						"Application form invalid subject",
						"UBIKE_APPLICATION_FORM_INVALID_SUBJECT",
						{},
					);
				}
				ctx.params.application_form = applicationForm[0];
				// TODO: Missing notification
				return await stateMachine["approve"]();
			},
		},
		adminRejectStatus: {
			rest: "POST /admin/:id/reject",
			scope: "u_bike:application-forms:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string" },
			},
			async handler(ctx) {
				const applicationForm = await this._get(ctx, { id: ctx.params.id });
				if (applicationForm[0].status != "dispatch") {
					throw new ValidationError(
						"Only can accept on [dispatch]  application form status",
						"UBIKE_APPLICATION_FORM_BAD_STATUS",
						{},
					);
				}
				// Get valid state machine
				let stateMachine = await this.getStateMachineBySubject(
					ctx,
					applicationForm[0].subject,
					applicationForm[0].status,
				);
				if (stateMachine == null) {
					throw new ValidationError(
						"Application form invalid subject",
						"UBIKE_APPLICATION_FORM_INVALID_SUBJECT",
						{},
					);
				}
				ctx.params.application_form = applicationForm[0];
				// TODO: Missing notification
				return await stateMachine["reject"]();
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async getStateMachineBySubject(ctx, subject, status) {
			let stateMachine = null;
			switch (subject) {
				case "COMPLAINT":
					stateMachine = await ComplainStateMachine.createStateMachine(status, ctx);
					break;
				case "THEFT":
					stateMachine = await TheftStateMachine.createStateMachine(status, ctx);
					break;
				case "WITHDRAWAL":
					stateMachine = await WithdrawalStateMachine.createStateMachine(status, ctx);
					break;
				case "ABSENCE":
					stateMachine = await AbsenceStateMachine.createStateMachine(status, ctx);
					break;
				case "RENOVATION":
					stateMachine = await RenovationStateMachine.createStateMachine(status, ctx);
					break;
				case "MAINTENANCE_REQUEST":
					stateMachine = await MaintenanceRequestStateMachine.createStateMachine(status, ctx);
					break;
				case "PRIVATE_MAINTENANCE":
					stateMachine = await PrivateMaintenanceStateMachine.createStateMachine(status, ctx);
					break;
				case "ADDITIONAL_MAINTENANCE_KIT":
					stateMachine = await AdditionalMaintenanceStateMachine.createStateMachine(status, ctx);
					break;
			}
			return stateMachine;
		},
		isBackofficeUser(ctx) {
			if (!ctx.meta.user.can_access_BO) {
				throw new ValidationError(
					"This operations if only disponible for the backofficer user.",
					"UBIKE_APPLICATION_UNAUTHORIZE_USER",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
