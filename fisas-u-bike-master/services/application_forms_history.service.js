"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.application_forms_history",
	table: "application_form_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "application_forms_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_form_id",
			"status",
			"user_id",
			"notes",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			application_form_id: { type: "number" },
			status: { type: "string" },
			user_id: { type: "number" },
			notes: { type: "string", optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
