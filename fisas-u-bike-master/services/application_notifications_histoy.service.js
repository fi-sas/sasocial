"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.application_notifications_history",
	table: "application_notification_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "application_notifications_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"alert_id",
			"alert_type_key",
			"created_at"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			application_id: { type: "number" },
			alert_id: { type: "number" },
			alert_type_key: { type: "string", max: 100 },
			created_at: { type: "date", optional: true, default: new Date() },
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
