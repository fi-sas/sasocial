"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {
	APPLICATION_ACTIONS,
	GENDER,
	DAILY_COMMUTE,
	FUEL_TYPE,
	APPLICATION_DECISION,
	APPLICATION_STATUS,
	DASHBOARD_PERIOD,
} = require("../values/enums");
const { CONFIGURATION_KEYS } = require("../values/constants");
const ApplicationStateMachine = require("../state-machines/application.machine");
const { ValidationError, ForbiddenError } = require("@fisas/ms_core/src/helpers/errors");
const Cron = require("moleculer-cron");
const cronLibrary = require("cron");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { co2 } = require("../helpers/geo");
const moment = require("moment");
const Validator = require("fastest-validator");
const { Errors } = require("@fisas/ms_core").Helpers;
const Stream = require("stream");

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "u_bike.applications",
	table: "application",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "applications"), Cron],
	dependencies: ["u_bike.configs"],
	/*
	 * Crons
	 */
	crons: [],
	/**
	 * Settings
	 */
	settings: {
		$dependencyTimeout: 30000,
		fields: [
			"id",
			"full_name",
			"birth_date",
			"gender",
			"weight",
			"height",
			"address",
			"postal_code",
			"email",
			"phone_number",
			"student_number",
			"start_date",
			"end_date",
			"typology_first_preference_id",
			"other_typology_preference",
			"daily_commute",
			"daily_consumption",
			"daily_kms",
			"daily_co2",
			"manufacturer",
			"model",
			"year_manufactured",
			"engine_power",
			"applicant_consent",
			"consent_date",
			"bike_id",
			"pickup_location",
			"user_id",
			"number_of_times",
			"avg_weekly_distance",
			"total_weekly_distance",
			"travel_time_school",
			"travel_time_school_bike",
			"avg_vehicle_consumption_100",
			"decision",
			"status",
			"created_at",
			"updated_at",
			"already_complain",
			"first_notification",
			"contract_file_id",
			"opposition_justification",
			"preferred_pickup_organict_unit_id",
			"delivery_reception_file_id",
			"consent_terms_file_id"
		],
		defaultWithRelateds: ["history", "typology_first_preference", "bike", "user"],
		withRelateds: {
			history(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "u_bike.applications_history", "history", "id", "application_id");
			},
			application_forms(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"u_bike.application-forms",
					"application_forms",
					"id",
					"application_id",
				);
			},

			typology_first_preference(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"u_bike.typologies",
					"typology_first_preference",
					"typology_first_preference_id",
					"id",
					{},
					"id,name",
					false,
				);
			},

			bike(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "u_bike.bikes", "bike", "bike_id");
			},

			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id",
					"id",
					{},
					"id,name,profile_id,tin,phone",
					false,
				);
			},
			contract(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "contract", "contract_file_id");
			},
			documents(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "u_bike.application_files", "documents", "id", "application_id");
			},
			preferred_pickup_organict_unit(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"infrastructure.organic-units",
					"preferred_pickup_organict_unit",
					"preferred_pickup_organict_unit_id",
				);
			},
		},
		entityValidator: {
			full_name: { type: "string", max: 255, optional: true },
			birth_date: { type: "date", convert: true, optional: true },
			gender: { type: "enum", values: GENDER, optional: true },
			weight: { type: "number", convert: true, optional: true },
			height: { type: "number", convert: true, optional: true },
			address: { type: "string", max: 255, optional: true },
			postal_code: { type: "string", max: 255, optional: true },
			email: { type: "string", max: 255, optional: true },
			phone_number: { type: "string", max: 255, optional: true },
			student_number: { type: "string", max: 255, optional: true },
			start_date: { type: "date", convert: true, optional: true },
			end_date: { type: "date", convert: true, optional: true },
			typology_first_preference_id: { type: "number", convert: true, optional: true },
			other_typology_preference: { type: "boolean", convert: true, default: false },
			daily_commute: { type: "enum", values: DAILY_COMMUTE, optional: true },
			daily_consumption: { type: "enum", values: FUEL_TYPE, optional: true },
			daily_kms: { type: "number", convert: true },
			daily_co2: { type: "number", convert: true, default: 0, optional: true },
			manufacturer: { type: "string", max: 255, nullable: true, optional: true },
			model: { type: "string", max: 255, nullable: true, optional: true },
			year_manufactured: { type: "number", positive: true, nullable: true, optional: true },
			engine_power: { type: "number", nullable: true, optional: true },
			applicant_consent: { type: "boolean", optional: true },
			consent_date: { type: "date", convert: true, optional: true },
			bike_id: { type: "number", convert: true, optional: true },
			pickup_location: { type: "string", max: 255, optional: true },
			user_id: { type: "number", nullable: true, optional: true, convert: true },
			number_of_times: { type: "number", default: 0, optional: true, convert: true },
			avg_weekly_distance: { type: "number", default: 0, optional: true, convert: true },
			total_weekly_distance: { type: "number", default: 0, optional: true, convert: true },
			travel_time_school: { type: "number", default: 0, optional: true, convert: true },
			travel_time_school_bike: { type: "number", default: 0, optional: true, convert: true },
			avg_vehicle_consumption_100: { type: "number", default: 0, optional: true, convert: true },
			already_complain: { type: "boolean", nullable: true, optional: true },
			decision: { type: "enum", values: APPLICATION_DECISION, optional: true },
			status: { type: "enum", values: APPLICATION_STATUS },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			first_notification: { type: "date", convert: true, optional: true, nullable: true },
			contract_file_id: { type: "number", convert: true, optional: true },
			opposition_justification: { type: "string", optional: true },
			preferred_pickup_organict_unit_id: { type: "number", convert: true, optional: true },
			delivery_reception_file_id: { type: "number", convert: true, optional: true },
			consent_terms_file_id: { type: "number", convert: true, optional: true },
		},
	},

	hooks: {
		before: {
			create: [
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "elaboration";
					ctx.params.already_complain = false;
					ctx.params.bike_id = null;
					ctx.params.daily_co2 = co2({
						commute: ctx.params.daily_commute,
						fuelType: ctx.params.daily_consumption,
						dist: ctx.params.daily_kms,
					});
					if (ctx.meta.isBackoffice) {
						ctx.params.status = "submitted";
						const validation = new Validator();
						const schema = {
							user_id: { type: "number", integer: true },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							const user = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
							ctx.params.gender = user[0].gender;
							ctx.params.full_name = user[0].name;
							ctx.params.birth_date = user[0].birth_date;
							ctx.params.address = user[0].address;
							ctx.params.postal_code = user[0].postal_code;
							ctx.params.email = user[0].email;
							ctx.params.phone_number = user[0].phone_number;
							ctx.params.student_number = user[0].student_number;
						}
					}
					if (!ctx.meta.isBackoffice) {
						await this.fetchUserData(ctx);
					}
					if (await this.haveOngoingApplicationsCreate(ctx)) {
						throw new ValidationError(
							"The user already have one application in progress",
							"UBIKE_APPLICATION_IN_PROGRESS",
							{},
						);
					}
					await this.validateApplicationPeriod(ctx);
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"checkIfCanUpdate",
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				"checkIfCanUpdate",
			],
			adminAccept: ["isBackofficeUser"],
			adminReject: ["isBackofficeUser"],
			adminCancel: ["isBackofficeUser"],
			list: [
				async function updateSearch(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (!ctx.meta.isBackoffice) {
						ctx.params.query.user_id = ctx.meta.user.id;
					}
					if (
						ctx.params.searchFields &&
						ctx.params.searchFields.split(",").includes("identification")
					) {
						const bikes = await ctx.call("u_bike.bikes.find", {
							searchFields: "identification",
							search: ctx.params.search,
							fields: "id",
							withRelated: false,
						});
						ctx.params.searchFields = ctx.params.searchFields
							.split(",")
							.filter((f) => f !== "identification")
							.join(",");
						const apps = await ctx.call("u_bike.applications.find", {
							query: {
								bike_id: bikes.map((b) => b.id),
							},
							withRelated: false,
							fields: "id",
						});
						ctx.params.searchIds = apps.map((ap) => ap.id);
					}
				},
			],
		},
		after: {
			create: [
				function sanatizeParams(ctx, response) {
					if (response[0].id != null) {
						ctx.call("u_bike.applications_history.create", {
							application_id: response[0].id,
							status: "elaboration",
							user_id: ctx.meta.user.id,
							notes: null,
						});
						return response;
					}
				},
			],
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						if (resp[0].user_id !== ctx.meta.user.id) {
							throw new ForbiddenError(
								"Unauthorized access to data",
								"UBIKE_APPLICATION_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},

	/**
	 * Actions
	 */
	actions: {
		canApply: {
			rest: "GET /canApply",
			scope: "u_bike:applications:create",
			visibility: "published",
			async handler(ctx) {
				if (await this.haveOngoingApplications(ctx)) {
					return {
						can: false,
						reason: "ALREADY_APPLIED",
					};
				}
				if ((await this.isApplicationPeriodOpen(ctx)) == false) {
					return {
						can: false,
						reason: "CLOSED_APPLICATIONS",
					};
				}
				return {
					can: true,
				};
			},
		},
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				const applications = await this._count(ctx, {
					query: { status: "contracted", user_id: ctx.meta.user.id },
				});
				if (applications == 0) {
					throw new ValidationError(
						"User dont have any contracted applications",
						"UBIKE_USER_DONT_HAVE_CONTRACTED_APPLICATION",
						{},
					);
				}
				return await ctx.call("u_bike.applications.dashboard", { period: "ever" });
			},
		},
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#user.id",
				],
			},
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		stats: {
			visibility: "published",
			rest: "GET /stats",
			scope: "u_bike:applications:read",
			handler() {
				return this.adapter
					.db(this.adapter.table)
					.select("status")
					.count("*")
					.groupBy("status")
					.then((result) => {
						let allStatus = {
							elaboration: "0",
							submitted: "0",
							analysis: "0",
							cancelled: "0",
							dispatch: "0",
							assigned: "0",
							enqueued: "0",
							unassigned: "0",
							suspended: "0",
							complaint: "0",
							accepted: "0",
							contracted: "0",
							rejected: "0",
							quiting: "0",
							withdrawal: "0",
							closed: "0",
						};
						result.map((r) => (allStatus[r.status] = r.count));
						return allStatus;
					});
			},
		},
		dashboard: {
			rest: "GET /dashboard",
			visibility: "published",
			scope: "u_bike:applications:read",
			params: {
				period: { type: "enum", values: DASHBOARD_PERIOD, default: "month" },
			},
			async handler(ctx) {
				let endDate = null,
					startDate = null,
					period = ctx.params.period,
					periodQuery = "";
				if (period != "ever") {
					const validation = new Validator();
					const schema = {
						start_date: { type: "date", convert: true },
						end_date: { type: "date", convert: true },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						startDate = new Date(ctx.params.start_date).toISOString();
						endDate = new Date(ctx.params.end_date).toISOString();
					}
					periodQuery = " and  trip_start  between '" + startDate + "' and  '" + endDate + "'";
				}
				return this.adapter
					.raw(
						"select count(id) as total_trips, COALESCE(sum(distance), 0) as total_kilometers, COALESCE(sum(co2_emissions_saved), 0) as total_saved_co2, COALESCE(sum(energy), 0) as total_burned_calories, COALESCE(sum(height_diff), 0) as gained_height, COALESCE(max(height_diff), 0) as max_gained_height, COALESCE(avg(time), 0) as avg_time, COALESCE(avg(avg_speed), 0) as avg_speed, COALESCE(sum(watts), 0) as watts from trip where user_id = " +
						ctx.meta.user.id +
						periodQuery,
					)
					.then((res) => {
						const data = {};
						data.application_id = null;
						data.total_points = 0;
						data.period = period;
						data.total_trips = +res.rows[0].total_trips;
						data.total_kilometers = +res.rows[0].total_kilometers.toFixed(2);
						data.total_saved_co2 = +res.rows[0].total_saved_co2.toFixed(2);
						data.total_burned_calories = +res.rows[0].total_burned_calories.toFixed(2);
						data.gained_height = +res.rows[0].gained_height.toFixed(2);
						data.max_gained_height = +res.rows[0].max_gained_height.toFixed(2);
						data.avg_time = +res.rows[0].avg_time.toFixed(2);
						data.avg_speed = +res.rows[0].avg_speed.toFixed(2);
						data.watts = +res.rows[0].watts.toFixed(2);
						return data;
					});
			},
		},
		status: {
			rest: "POST /:id/status",
			scope: "u_bike:applications:status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				action: {
					type: "enum",
					values: APPLICATION_ACTIONS,
				},
				notes: { type: "string", optional: true, convert: true },
				application: { type: "object", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				if (ctx.params.action === "dispatch") {
					const validation = new Validator();
					const schema = {
						application: {
							type: "object",
							props: {
								decision: { type: "string" },
							},
						},
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.application.decision === "assigned") {
							const schema = {
								application: {
									type: "object",
									props: {
										decision: { type: "string" },
										bike_id: { type: "number" },
										pickup_location: { type: "string" },
									},
								},
							};
							const check = validation.compile(schema);
							const res = check(ctx.params);
							if (res !== true) {
								return Promise.reject(
									new Errors.ValidationError("Entity validation error!", null, res),
								);
							} else {
								application[0].decision = ctx.params.application.decision;
								application[0].bike_id = ctx.params.application.bike_id;
								application[0].pickup_location = ctx.params.application.pickup_location;
							}
						} else {
							application[0].decision = ctx.params.application.decision;
							application[0].bike_id = null;
							application[0].pickup_location = null;
						}
					}
				}

				/*if (ctx.params.action === "hire") {
					const validation = new Validator();
					const schema = {
						application: {
							type: "object",
							props: {
								contract_file_id: { type: "number", optional: true },
							},
						},
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						application[0].contract_file_id = ctx.params.application.contract_file_id;
					}
				}*/

				ctx.params.application = application[0];

				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);

				// Validate Status
				if (stateMachine.cannot(ctx.params.action)) {
					throw new ValidationError(
						"Impossible change the state",
						"UBIKE_APPLICATION_STATUS_ERROR",
						{},
					);
				}
				if (ctx.params.action === "dispatch" && ctx.params.application.bike_id) {
					await this.validateIfBikeItsAvailable(ctx);
				}
				const result = await stateMachine[ctx.params.action]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user_id,
					result[0].id,
					result[0].full_name,
				);
				return result;
			},
		},
		client: {
			rest: "POST /client/:id/status",
			scope: "u_bike:applications:create",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				action: { type: "enum", values: ["submit", "cancel", "confirm", "reject", "stay", "quit"] },
				notes: { type: "string", optional: true },
				application: { type: "object", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				if (ctx.meta.user.id !== application[0].user_id) {
					throw new ValidationError(
						"This operations if only disponible for the application owner.",
						"UBIKE_APPLICATION_UNAUTHORIZE_USER",
						{},
					);
				}
				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);

				// Validate Status
				if (stateMachine.cannot(ctx.params.action)) {
					throw new ValidationError(
						"Impossible change the state",
						"UBIKE_APPLICATION_STATUS_ERROR",
						{},
					);
				}
				ctx.params.application = application[0];
				const result = await stateMachine[ctx.params.action]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user_id,
					result[0].id,
					result[0].full_name,
				);
				return result;
			},
		},
		adminAccept: {
			rest: "POST /admin/:id/accept",
			scope: "u_bike:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				// Validate Status
				if (stateMachine.cannot("accept")) {
					throw new ValidationError(
						"Impossible change the state",
						"UBIKE_APPLICATION_STATUS_ERROR",
						{},
					);
				}

				ctx.params.application = application[0];
				const result = await stateMachine["accept"]();
				if (result == null) {
					throw new ValidationError(
						"The bike is already in use on one application in progress",
						"UBIKE_BIKE_ALREADY_IN_USE",
						{},
					);
				}
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user_id,
					result[0].id,
					result[0].full_name,
				);
				return result;
			},
		},
		adminReject: {
			rest: "POST /admin/:id/reject",
			scope: "u_bike:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				// Validate Status
				if (stateMachine.cannot("reanalyse")) {
					throw new ValidationError(
						"Impossible change the state",
						"UBIKE_APPLICATION_STATUS_ERROR",
						{},
					);
				}
				ctx.params.application = application[0];
				const result = await stateMachine["reject"]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user_id,
					result[0].id,
					result[0].full_name,
				);
				return result;
			},
		},
		adminCancel: {
			rest: "POST /admin/:id/cancel",
			scope: "u_bike:applications:dispatch",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string", optional: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				// Validate Status
				if (application[0].status !== "assigned" && stateMachine.cannot("cancel")) {
					throw new ValidationError(
						"Admin can only cancel apllications on [Assigned] status",
						"UBIKE_APPLICATION_STATUS_ERROR",
						{},
					);
				}
				ctx.params.application = application[0];
				const result = await stateMachine["cancel"]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user_id,
					result[0].id,
					result[0].full_name,
				);
				return result;
			},
		},
		getAllActive: {
			handler(ctx) {
				return this._find(ctx, {
					withRelated: ["user"],
					query: (qb) => {
						qb.where("start_date", "<", new Date());
						qb.where("end_date", ">", new Date()).where("status", "in", [
							"submitted",
							"analisys",
							"dispatch",
							"assign",
							"enqueued",
							"complaint",
							"accepted",
							"contracted",
							"suspended",
							"quiting",
						]);
					},
				});
			},
		},
		refreshNotificationHour: {
			visibility: "public",
			params: {
				value: { type: "string" },
				lastValue: { type: "string" },
			},
			async handler(ctx) {
				const momentValue = moment(ctx.params.value, "HH:mm").isValid()
					? moment(ctx.params.value, "HH:mm")
					: moment();
				const momentOldValue = moment(ctx.params.lastValue, "HH:mm").isValid()
					? moment(ctx.params.value, "HH:mm")
					: moment();

				if (this.getJob("missingUserConfirmation")) this.getJob("missingUserConfirmation").stop();
				else this.missingUserConfirmationNotification(momentValue.hour(), momentValue.minutes());

				if (this.getJob("endingContract")) this.getJob("endingContract").stop();
				else this.endingContractNotifications(momentValue.hour(), momentValue.minutes());

				if (this.getJob("missingTrips")) this.getJob("missingTrips").stop();
				else this.missingTrips(momentValue.hour(), momentValue.minutes());

				/*const h = ctx.params.value.split(":")[0];
				const m = ctx.params.value.split(":")[1];
				const lh = ctx.params.lastValue.split(":")[0];
				const lm = ctx.params.lastValue.split(":")[1];
				const newDate = new Date();
				newDate.setHours(h, m);
				const oldDate = new Date();
				newDate.setHours(lh, lm);*/

				// If new hour definition is smaller than actual time, and oldDate is bigger that actual time, send notification (otherwise today"s notifications wont be send)
				if (
					momentValue.format("HH:mm") < moment(new Date()).format("HH:mm") &&
					momentOldValue.format("HH:mm") > moment(new Date()).format("HH:mm")
				) {
					// Missing user confirmation
					let applications = await this.adapter.db("application").where("status", "assigned");
					applications.forEach((element) => {
						this.sendMissingConfirmationNotification(element.user_id, element.id);
					});

					// Ending contract
					const configAntecedenceDays = await this.broker.call("u_bike.configs.find", {
						query: {
							key: CONFIGURATION_KEYS.APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS,
						},
					});
					const configFrequency = await this.broker.call("u_bike.configs.find", {
						query: {
							key: CONFIGURATION_KEYS.APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY,
						},
					});

					/* CHECK THIS !!! */
					const limitDate = new Date();
					limitDate.setDate(Number.parseInt(limitDate.getDate() + configAntecedenceDays[0].value));
					applications = await this.adapter
						.db("application")
						.where("status", "contracted")
						.whereBetween("end_date", [new Date(), limitDate]);
					applications.forEach(async (application) => {
						const notificationList = await this.adapter
							.db("application_notification_history")
							.where("application_id", application.id)
							.where("alert_type_key", "UBIKE_APPLICATION_ENDING_CONTRACT")
							.orderBy("created_at", "desc");
						if (notificationList.length == 0) {
							this.sendEndingContractNotification(application.user_id, application.id);
						} else {
							const lastNotification = new Date(notificationList[0].created_at);
							lastNotification.setHours(0, 0, 0, 0);
							lastNotification.setDate(lastNotification.getDate() + configFrequency[0].value);
							if (
								new Date(
									new Date().getFullYear(),
									new Date().getMonth(),
									new Date().getDate(),
								).getTime() == lastNotification.getTime()
							) {
								this.sendEndingContractNotification(application.user_id, application.id);
							}
						}
					});

					// Missing trips reports
					const tripdConfig = await this.broker.call("u_bike.configs.find", {
						query: {
							key: CONFIGURATION_KEYS.APPLICATION_MISSING_TRIPS,
						},
					});

					const daysConfig = tripdConfig[0].value;
					applications = await this.adapter
						.db("application")
						.where("status", "contracted")
						.where("end_date", ">=", new Date());
					applications.forEach(async (application) => {
						const lastTrip = await this.adapter
							.db("trip")
							.where("application_id", application.id)
							.orderBy("created_at", "desc");
						const today = new Date();
						today.setHours(0, 0, 0, 0);
						const dayInMiliseconds = 86400000 * Number.parseInt(daysConfig);
						// Compare date of last trip reported
						if (lastTrip.length > 0) {
							const lastTripDate = new Date(lastTrip[0].created_at);
							lastTripDate.setHours(0, 0, 0, 0);
							if (today.getTime() - lastTripDate.getTime() >= dayInMiliseconds) {
								this.sendMissingTripsNotification(application.user_id, application.id);
							}
						}
						// its first trip report
						else {
							const creationDate = new Date(application.start_date);
							creationDate.setHours(0, 0, 0, 0);
							if (today.getTime() - creationDate.getTime() >= dayInMiliseconds) {
								this.sendMissingTripsNotification(application.user_id, application.id);
							}
						}
					});
				}

				// Set new timming
				if (this.getJob("missingUserConfirmation")) {
					this.getJob("missingUserConfirmation").setTime(
						new cronLibrary.CronTime(momentValue.minutes() + " " + momentValue.hour() + "  * * *"),
					);
					this.getJob("missingUserConfirmation").start();
				}

				if (this.getJob("missingUserConfirmation")) {
					this.getJob("endingContract").setTime(
						new cronLibrary.CronTime(momentValue.minutes() + " " + momentValue.hour() + "  * * *"),
					);
				}
				this.getJob("endingContract").start();

				if (this.getJob("missingTrips")) {
					this.getJob("missingTrips").setTime(
						new cronLibrary.CronTime(momentValue.minutes() + " " + momentValue.hour() + "  * * *"),
					);
					this.getJob("missingTrips").start();
				}

				return;
			},
		},
		printApplications: {
			rest: "POST /print",
			visibility: "published",
			params: {
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
				order_by: {
					type: "string",
					values: ["created_at", "full_name", "weight", "height", "student_number"],
				},
				order: { type: "enum", values: ["ASC", "DESC"] },
				status: { type: "string", optional: true },
				doc_type: { type: "enum", values: ["xlsx", "pdf"] },
			},
			async handler(ctx) {
				const applications = await this._find(ctx, {
					query: (qb) => {
						qb.whereBetween(this.adapter.raw("\"created_at\"::date"), [
							moment(ctx.params.start_date).format("YYYY-MM-DD"),
							moment(ctx.params.end_date).format("YYYY-MM-DD"),
						]);
						if (ctx.params.status) qb.where("status", "=", ctx.params.status);
						qb.orderBy(ctx.params.order_by, ctx.params.order);
					},
				});
				// UBIKE_APPLICATIONS
				for (const app of applications) {
					app.birth_date = moment(app.birth_date).format("YYYY-MM-DD");
					app.start_date = moment(app.start_date).format("YYYY-MM-DD");
					app.end_date = moment(app.end_date).format("YYYY-MM-DD");
					app.created_at = moment(app.created_at).format("YYYY-MM-DD");
				}
				return ctx.call("reports.templates.print", {
					key: "UBIKE_APPLICATIONS",
					options: {
						convertTo: ctx.params.doc_type,
					},
					data: {
						applications,
					},
				});
			},
		},
		printQuest: {
			rest: "POST /print-quest",
			visibility: "published",
			params: {
				application_id: { type: "number", convert: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.application_id });
				const user = await ctx.call("authorization.users.get", {
					id: application[0].user_id,
					withRelated: ["profile", "organicUnit"],
				});
				application[0].institute = user[0].organicUnit ? user[0].organicUnit.name : "----";
				application[0].profile = user[0].profile ? user[0].profile.name : "----";
				application[0].course_year = user[0].course_year;
				application[0].daily_consumption = application[0].daily_consumption
					? application[0].daily_consumption
					: "--";
				return ctx.call("reports.templates.print", {
					key: "UBIKE_QUEST",
					options: {
						convertTo: "pdf",
					},
					data: {
						application: application[0],
					},
				});
			},
		},

		applications_report: {
			rest: "POST /applications-report",
			visibility: "published",
			params: {
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
				organic_unit_id: { type: "number", optional: true },
				user_id: { type: "number", optional: true },
				bike_id: { type: "number", optional: true },
			},
			async handler(ctx) {
				const q1 = this.adapter.db(this.adapter.table).select("*");

				if (ctx.params.start_date && ctx.params.end_date) {
					q1.whereBetween(this.adapter.raw("\"start_date\"::date"), [
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
						moment(ctx.params.end_date).format("YYYY-MM-DD"),
					]);

					q1.whereBetween(this.adapter.raw("\"end_date\"::date"), [
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
						moment(ctx.params.end_date).format("YYYY-MM-DD"),
					]);
				}
				if (ctx.params.user_id) q1.andWhere("user_id", "=", ctx.params.user_id);
				if (ctx.params.bike_id) q1.andWhere("bike_id", "=", ctx.params.bike_id);

				if (ctx.params.organic_unit_id) {
					await this.adapter.raw("select ap.user_id from application as ap ").then(async (data) => {
						const users = await ctx
							.call("authorization.users.find", {
								query: {
									id: data.rows.map((u) => u.user_id),
									organic_unit_id: ctx.params.organic_unit_id,
								},
								withRelated: false,
							})
							.then((response) => (response.length ? response.map((r) => r.id) : []));
						q1.whereIn("user_id", users);
					});
				}

				await Promise.all([q1]).then(async (applications) => {
					if (applications.length) {
						for (const app of applications[0]) {
							app.assignment_date = await ctx
								.call("u_bike.applications_history.find", {
									query: {
										application_id: app.id,
										status: "assigned",
									},
									withRelated: false,
								})
								.then((response) =>
									response.length ? moment(response[0].created_at).format("YYYY-MM-DD") : null,
								);
							const user = await ctx.call("authorization.users.get", {
								id: app.user_id,
								withRelated: false,
							});
							app.organic_unit =
								user.length && user[0].organic_unit_id
									? await ctx
										.call("infrastructure.organic-units.get", {
											id: user[0].organic_unit_id,
											withRelated: false,
										})
										.then((ou) => (ou.length ? ou[0].name : null))
									: null;
							app.tin = user.length && user[0].tin ? user[0].tin : null;
							app.bike = app.bike_id
								? await ctx
									.call("u_bike.bikes.get", { id: app.bike_id, withRelated: false })
									.then((response) => (response.length ? response[0] : null))
								: null;
							app.start_date = app.start_date ? moment(app.start_date).format("YYYY-MM-DD") : null;
							app.end_date = app.end_date ? moment(app.end_date).format("YYYY-MM-DD") : null;
							app.created_at = moment(app.created_at).format("YYYY-MM-DD");
						}

						ctx.call("reports.templates.print", {
							key: "UBIKE_APPLICATIONS_REPORT",
							options: {
								convertTo: "xlsx",
							},
							data: {
								applications: applications[0],
							},
						});
					}
				});
			},
		},

		get_users_with_bike_assigned: {
			visibility: "public",
			params: {},
			async handler(ctx) {
				const result = await this.adapter.find({
					query: (q) => {
						q.distinct("user_id as id");
						q.where("start_date", "<", new Date());
						q.where("end_date", ">", new Date());
						q.where("status", "in", [
							"submitted",
							"analisys",
							"dispatch",
							"assign",
							"enqueued",
							"complaint",
							"accepted",
							"contracted",
							"suspended",
							"quiting",
						]);
						q.whereNotNull("bike_id");

						return q;
					},
					withRelated: false,
				});

				return result;
			},
		},

		// Verify if there is some application in elaboration status by user
		verify_elaboration: {
			rest: "GET /verify_elaboration",
			scope: "u_bike:applications:read",
			visibility: "published",
			params: {
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
			},
			async handler(ctx) {
				ctx.params.query = {};
				ctx.params.query.user_id = ctx.meta.user.id;
				ctx.params.query.status = "elaboration";

				let params = this.sanitizeParams(ctx, ctx.params, true);
				return this._list(ctx, params);
			},
		},
		getConsentTermsReport: {
			rest: "GET /:id/consent-terms",
			visibility: "published",
			params: {
				id: { type: "number", convert: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id, withRelated: "bike" }).then(
					async (application) => {
						if (application.length) {
							if (application[0].status === "contracted") {
								const user = await ctx.call("authorization.users.get", {
									id: application[0].user_id,
									withRelated: "profile",
								});
								const serial_number = application[0].bike
									? await ctx
										.call("infrastructure.assets.get", { id: application[0].bike.asset_id })
										.then((asset) => (asset.length ? asset[0].serial_number : null))
									: null;
								application[0].bike.serial_number = serial_number ? serial_number : null;
								application[0].organic_unit =
									user.length && user[0].organic_unit_id
										? await ctx
											.call("infrastructure.organic-units.get", {
												id: user[0].organic_unit_id,
												withRelated: false,
											})
											.then((ou) => (ou.length ? ou[0].name : null))
										: null;
								application[0].perfil = user ? user[0].profile.name : null;
								application[0].identification = user ? user[0].identification : null;
								application[0].city = user ? user[0].city : null;
								application[0].city = user ? user[0].city : null;
								const date = new Date();
								application[0].emitted_day = date.getDay();
								application[0].emitted_month = date.toLocaleString("default", { month: "long" });
								application[0].emitted_year = date.getFullYear();

								const readable = new Stream.Readable();
								readable.push(
									JSON.stringify({
										key: "UBIKE_TERM_OF_ACCEPT",
										options: {
											convertTo: "pdf",
										},
										data: {
											application: application[0],
										},
									}),
								);
								// no more data
								readable.push(null);
								return ctx.call("reports.templates.printFromStream", readable);
							} else {
								throw new ValidationError(
									"The application must be in contracted status.",
									"UBIKE_APPLICATION_UNAUTHORIZE_USER",
									{},
								);
							}
						}
					},
				);
			},
		},

		getDeliveryReceptionReport: {
			rest: "GET /:id/delivery-reception",
			visibility: "published",
			params: {
				id: { type: "number", convert: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id, withRelated: "bike" }).then(
					async (application) => {
						if (application.length) {
							if (application[0].status === "contracted") {
								const user = await ctx.call("authorization.users.get", {
									id: application[0].user_id,
									withRelated: "profile",
								});
								const serial_number = application[0].bike
									? await ctx
										.call("infrastructure.assets.get", { id: application[0].bike.asset_id })
										.then((asset) => (asset.length ? asset[0].serial_number : null))
									: null;
								application[0].bike.serial_number = serial_number ? serial_number : null;
								application[0].organic_unit =
									user.length && user[0].organic_unit_id
										? await ctx
											.call("infrastructure.organic-units.get", {
												id: user[0].organic_unit_id,
												withRelated: false,
											})
											.then((ou) => (ou.length ? ou[0].name : null))
										: null;
								application[0].perfil = user ? user[0].profile.name : null;
								application[0].identification = user ? user[0].identification : null;
								application[0].city = user ? user[0].city : null;
								application[0].city = user ? user[0].city : null;
								const date = new Date();
								application[0].emitted_day = date.getDay();
								application[0].emitted_month = date.toLocaleString("default", { month: "long" });
								application[0].emitted_year = date.getFullYear();

								const readable = new Stream.Readable();
								readable.push(
									JSON.stringify({
										key: "UBIKE_DELIVERY_RECEPTION",
										options: {
											convertTo: "pdf",
										},
										data: {
											aplication: application[0],
										},
									}),
								);
								// no more data
								readable.push(null);
								return ctx.call("reports.templates.printFromStream", readable);
							} else {
								throw new ValidationError(
									"The application must be in contracted status.",
									"UBIKE_APPLICATION_UNAUTHORIZE_USER",
									{},
								);
							}
						}
					},
				);
			},
		},

		submit_consent_delivery_reports: {
			rest: "POST /:id/submit-reports",
			visibility: "published",
			params: {
				id: { type: "number", convert: true },
				consent_terms_file_id: { type: "number", convert: true, optional: true },
				delivery_reception_file_id: { type: "number", convert: true, optional: true },
			},
			handler(ctx) {
				return this._get(ctx, { id: ctx.params.id, withRelated: false }).then((application) => {
					if (application.length) {
						if (application[0].status === "contracted") {
							if (ctx.params.consent_terms_file_id && !application[0].consent_terms_file_id) {
								this._update(ctx, { id: application[0].id, consent_terms_file_id: ctx.params.consent_terms_file_id }, true);
							}
							if (ctx.params.delivery_reception_file_id && !application[0].delivery_reception_file_id) {
								this._update(ctx, { id: application[0].id, delivery_reception_file_id: ctx.params.delivery_reception_file_id }, true);
							}
							return application[0];
						} else {
							throw new ValidationError(
								"The application must be in contracted status.",
								"UBIKE_APPLICATION_UNAUTHORIZE_USER",
								{},
							);
						}
					}
				});
			},
		},
	},
	/**
	 * Events
	 */
	events: {
		"u_bike.application_files.*"() {
			this.clearCache();
		},
		"u_bike.applications.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		getStartDatePeriod(period) {
			const date = new Date(),
				y = date.getFullYear(),
				m = date.getMonth() + 1,
				d = date.getDate();
			if (period === "day") {
				return y + "-" + m + "-" + d;
			}
			if (period === "month") {
				return y + "-" + m + "-01";
			}

			if (period === "year") {
				return y + "-01-01";
			}
		},
		getEndDatePeriod(period) {
			const date = new Date(),
				y = date.getFullYear(),
				m = date.getMonth() + 1,
				d = date.getDate();
			if (period === "day") {
				return y + "-" + m + "-" + d;
			}
			if (period === "month") {
				return y + "-" + m + "-" + new Date(y, m, 0).getDate();
			}

			if (period === "year") {
				return y + "-12-31";
			}
		},
		isBackofficeUser(ctx) {
			if (!ctx.meta.user.can_access_BO) {
				throw new ValidationError(
					"This operations if only disponible for the backofficer user.",
					"UBIKE_APPLICATION_UNAUTHORIZE_USER",
					{},
				);
			}
		},
		async validateIfBikeItsAvailable(ctx) {
			// Validate if Bike is available
			if (ctx.params.application.decision === "assigned") {
				const bikeUtilization = await this.adapter
					.db("application")
					.where("bike_id", ctx.params.application.bike_id)
					.whereIn("status", [
						"submitted",
						"analysis",
						"dispatch",
						"assigned",
						"enqueued",
						"complaint",
						"accepted",
						"contracted",
						"suspended",
						"quiting",
					]);
				if (bikeUtilization.length > 0) {
					throw new ValidationError(
						"The bike is already in use on one application in progress",
						"UBIKE_BIKE_ALREADY_IN_USE",
						{},
					);
				}
			}
		},
		async sendNotification(ctx, status, user_id, application_id, name) {
			const notification = await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "UBIKE_APPLICATION_STATUS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name },
				variables: {},
				external_uuid: null,
				medias: [],
			});
			await ctx.call("u_bike.application_notifications_history.create", {
				application_id: application_id,
				alert_id: notification[0].id,
				alert_type_key: "UBIKE_APPLICATION_STATUS_" + status.toUpperCase(),
			});
		},

		// Missing user confirmation notifications
		async sendMissingConfirmationNotification(user_id, application_id, name) {
			const notification = await this.adapter.broker.call("notifications.alerts.create_alert", {
				alert_type_key: "UBIKE_APPLICATION_MISSING_CONFIRMATION",
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name },
				variables: {},
				external_uuid: null,
				medias: [],
			});
			await this.adapter.broker.call("u_bike.application_notifications_history.create", {
				application_id: application_id,
				alert_id: notification[0].id,
				alert_type_key: "UBIKE_APPLICATION_MISSING_CONFIRMATION",
			});
		},
		async missingUserConfirmationNotification(hour, minutes) {
			this.$crons.push(
				new cronLibrary.CronJob(
					minutes + " " + hour + " * * *",
					async () => {
						const applications = await this.adapter.db("application").where("status", "assigned");
						applications.forEach(async (element) => {
							this.sendMissingConfirmationNotification(
								element.user_id,
								element.id,
								element.full_name,
							);
						});
					},
					null,
					true,
				),
			);
			this.$crons[this.$crons.length - 1].name = "missingUserConfirmation";
		},

		// Contract ending notifications
		async endingContractNotifications(hour, minutes) {
			this.$crons.push(
				new cronLibrary.CronJob(
					minutes + " " + hour + " * * *",
					async () => {
						const configAntecedenceDays = await this.broker.call("u_bike.configs.find", {
							query: {
								key: CONFIGURATION_KEYS.APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS,
							},
						});
						const configFrequency = await this.broker.call("u_bike.configs.find", {
							query: {
								key: CONFIGURATION_KEYS.APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY,
							},
						});

						const limitDate = new Date();
						limitDate.setDate(
							Number.parseInt(limitDate.getDate() + configAntecedenceDays[0].value),
						);
						const applications = await this.adapter
							.db("application")
							.where("status", "contracted")
							.whereBetween("end_date", [new Date(), limitDate]);
						applications.forEach(async (application) => {
							const notificationList = await this.adapter
								.db("application_notification_history")
								.where("application_id", application.id)
								.where("alert_type_key", "UBIKE_APPLICATION_ENDING_CONTRACT")
								.orderBy("created_at", "desc");
							if (notificationList.length == 0) {
								this.sendEndingContractNotification(
									application.user_id,
									application.id,
									application.full_name,
								);
							} else {
								const lastNotification = new Date(notificationList[0].created_at);
								lastNotification.setHours(0, 0, 0, 0);
								lastNotification.setDate(lastNotification.getDate() + configFrequency[0].value);
								if (
									new Date(
										new Date().getFullYear(),
										new Date().getMonth(),
										new Date().getDate(),
									).getTime() == lastNotification.getTime()
								) {
									this.sendEndingContractNotification(
										application.user_id,
										application.id,
										application.full_name,
									);
								}
							}
						});
					},
					null,
					true,
				),
			);
			this.$crons[this.$crons.length - 1].name = "endingContract";
		},
		async sendEndingContractNotification(user_id, application_id, name) {
			const notification = await this.adapter.broker.call("notifications.alerts.create_alert", {
				alert_type_key: "UBIKE_APPLICATION_ENDING_CONTRACT",
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name },
				variables: {},
				external_uuid: null,
				medias: [],
			});
			await this.adapter.broker.call("u_bike.application_notifications_history.create", {
				application_id: application_id,
				alert_id: notification[0].id,
				alert_type_key: "UBIKE_APPLICATION_ENDING_CONTRACT",
			});
		},

		// Missing trips notifications
		async missingTrips(hour, minutes) {
			this.$crons.push(
				new cronLibrary.CronJob(
					minutes + " " + hour + " * * *",
					async () => {
						const tripdConfig = await this.broker.call("u_bike.configs.find", {
							query: {
								key: CONFIGURATION_KEYS.APPLICATION_MISSING_TRIPS,
							},
						});
						const daysConfig = tripdConfig[0].value.value;
						const applications = await this.adapter
							.db("application")
							.where("status", "contracted")
							.where("end_date", ">=", new Date());
						applications.forEach(async (application) => {
							const lastTrip = await this.adapter
								.db("trip")
								.where("application_id", application.id)
								.orderBy("created_at", "desc");
							const today = new Date();
							today.setHours(0, 0, 0, 0);
							const dayInMiliseconds = 86400000 * Number.parseInt(daysConfig);
							// Compare date of last trip reported
							if (lastTrip.length > 0) {
								const lastTripDate = new Date(lastTrip[0].created_at);
								lastTripDate.setHours(0, 0, 0, 0);
								if (today.getTime() - lastTripDate.getTime() >= dayInMiliseconds) {
									this.sendMissingTripsNotification(
										application.user_id,
										application.id,
										application.full_name,
									);
								}
							}
							// its first trip report
							else {
								const creationDate = new Date(application.start_date);
								creationDate.setHours(0, 0, 0, 0);
								if (today.getTime() - creationDate.getTime() >= dayInMiliseconds) {
									this.sendMissingTripsNotification(
										application.user_id,
										application.id,
										application.full_name,
									);
								}
							}
						});
					},
					null,
					true,
				),
			);
			this.$crons[this.$crons.length - 1].name = "missingTrips";
		},
		async sendMissingTripsNotification(user_id, application_id, name) {
			const notification = await this.adapter.broker.call("notifications.alerts.create_alert", {
				alert_type_key: "UBIKE_APPLICATION_MISSING_TRIPS",
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name },
				variables: {},
				external_uuid: null,
				medias: [],
			});
			await this.adapter.broker.call("u_bike.application_notifications_history.create", {
				application_id: application_id,
				alert_id: notification[0].id,
				alert_type_key: "UBIKE_APPLICATION_MISSING_TRIPS",
			});
		},

		async startNotificationCronJobs() {
			const hourConfig = await this.broker
				.call("u_bike.configs.find", {
					query: {
						key: CONFIGURATION_KEYS.APPLICATION_NOTIFICATION_HOUR,
					},
				})
				.then((response) =>
					response.length
						? moment(response[0].value).isValid()
							? moment(response[0].value)
							: null
						: null,
				);
			if (hourConfig) {
				this.missingUserConfirmationNotification(hourConfig.hour(), hourConfig.minutes());
				this.endingContractNotifications(hourConfig.hour(), hourConfig.minutes());
				this.missingTrips(hourConfig.hour(), hourConfig.minutes());
			}
		},
		createPromisse() {
			return new Promise((resolve, reject) => {
				resolve(this.startNotificationCronJobs());
				reject(this.logger.info("ERROR: cannot establish DB Connection"));
			});
		},

		retryPromise(fn, retriesLeft = 3, interval = 200) {
			this.logger.info("Start trying DB Connection");
			return new Promise((resolve, reject) => {
				return fn()
					.then(this.startNotificationCronJobs())
					.catch((error) => {
						this.logger.info("Retrying DB Connection");
						if (retriesLeft === 1) {
							// reject('maximum retries exceeded');
							this.logger.info("Maximun of retrys on DB connection ");
							reject(error);
							return;
						}
						setTimeout(() => {
							this.logger.info("retriesLeft: ", retriesLeft);
							// Passing on "reject" is the important part
							this.retryPromise(fn, retriesLeft - 1, interval).then(resolve, reject);
						}, interval);
					});
			});
		},

		bewteenTwoDates(dateToCompare, date1, date2) {
			dateToCompare = new Date(dateToCompare).getTime();
			date1 = new Date(date1).getTime();
			date2 = new Date(date2).getTime();

			if (dateToCompare < date1 || dateToCompare > date2) {
				return false;
			}
			return true;
		},
		async validateApplicationPeriod(ctx) {
			//let start_date = await ctx.call("u_bike.configs.read", { key: "APPLICATION_START_DATE" });
			//let end_date = await ctx.call("u_bike.configs.read", { key: "APPLICATION_END_DATE" });

			const start_date = await ctx.call("u_bike.configs.find", {
				query: {
					key: CONFIGURATION_KEYS.APPLICATION_START_DATE,
				},
			});
			const end_date = await ctx.call("u_bike.configs.find", {
				query: {
					key: CONFIGURATION_KEYS.APPLICATION_END_DATE,
				},
			});

			if (start_date == null || end_date == null) {
				throw new ValidationError(
					"The applications submission period its not defined",
					"UBIKE_APPLICATION_PERIOD_NOT_DEFINED",
					{},
				);
			}

			if (
				!this.bewteenTwoDates(ctx.params.start_date, start_date[0].value, end_date[0].value) ||
				!this.bewteenTwoDates(ctx.params.end_date, start_date[0].value, end_date[0].value)
			) {
				throw new ValidationError(
					"The application duration must be inside defined application period",
					"UBIKE_APPLICATION_INVALID_DATES",
					{},
				);
			}
		},
		async haveOngoingApplications(ctx) {
			const count = await this._count(ctx, {
				query: {
					status: [
						"submitted",
						"analysis",
						"dispatch",
						"assigned",
						"enqueued",
						"complaint",
						"accepted",
						"contracted",
						"suspended",
						"quiting",
					],
					user_id: ctx.meta.user.id,
				},
			});
			return count > 0;
		},
		async haveOngoingApplicationsCreate(ctx) {
			const count = await this._count(ctx, {
				query: {
					status: [
						"elaboration",
						"submitted",
						"analysis",
						"dispatch",
						"assigned",
						"enqueued",
						"complaint",
						"accepted",
						"contracted",
						"suspended",
						"quiting",
					],
					user_id: ctx.params.user_id,
				},
			});
			return count > 0;
		},
		async isApplicationPeriodOpen(ctx) {
			//let start_date = await ctx.call("u_bike.configs.read", { key: "APPLICATION_START_DATE" });
			//let end_date = await ctx.call("u_bike.configs.read", { key: "APPLICATION_END_DATE" });

			const start_date = await ctx.call("u_bike.configs.find", {
				query: {
					key: CONFIGURATION_KEYS.APPLICATION_START_DATE,
				},
			});
			const end_date = await ctx.call("u_bike.configs.find", {
				query: {
					key: CONFIGURATION_KEYS.APPLICATION_END_DATE,
				},
			});

			if (start_date == null || end_date == null) {
				return false;
			}

			return this.bewteenTwoDates(new Date(), start_date[0].value, end_date[0].value);
		},
		async fetchUserData(ctx) {
			ctx.params.user_id = ctx.meta.user.id;
			ctx.params.gender = ctx.meta.user.gender;
			ctx.params.full_name = ctx.meta.user.name;
			ctx.params.birth_date = ctx.meta.user.birth_date;
			ctx.params.address = ctx.meta.user.address;
			ctx.params.postal_code = ctx.meta.user.postal_code;
			ctx.params.email = ctx.meta.user.email;
			ctx.params.phone_number = ctx.meta.user.phone_number;
			ctx.params.student_number = ctx.meta.user.student_number;
		},
		async checkIfCanUpdate(ctx) {
			const application = await ctx.call("u_bike.applications.get", {
				id: ctx.params.id,
				withRelated: false,
			});

			if (!ctx.meta.isBackoffice) {
				if (
					!["cancelled", "accepted", "rejected", "quiting", "contracted"].includes(
						ctx.params.status,
					)
				) {
					delete ctx.params.status;
				}
				if (ctx.params.ready_to_submit === true && application[0].status === "elaboration") {
					ctx.params.status = "submitted";
					delete ctx.params.ready_to_submit;
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		this.retryPromise(this.createPromisse, 10, 5000);
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
