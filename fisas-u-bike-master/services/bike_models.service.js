"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.bike_models",
	table: "bike_model",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "bike_models")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"model",
			"model_conformity_file_id",
			"brakes",
			"brake_types",
			"frame_type",
			"front_suspension",
			"tyres",
			"seat_fixation_mechanism",
			"front_wheel_fixation_mechanism",
			"back_wheel_fixation_mechanism",
			"handlebar_fixation_mechanism",
			"gear_mechanism",
			"chain_guard",
			"front_illumination",
			"back_illumination",
			"side_reflections",
			"pedal_reflections",
			"front_reflections",
			"back_reflections",
			"sound_warning",
			"instruction_manual",
			"rest",
			"mudguards",
			"luggage_support",
			"monitoring_system",
			"coating_requirements",
			"other_accessories",
			"typology_id",
			"created_at",
			"updated_at"
		],
		defaultWithRelateds: ["typology"],
		withRelateds: {
			bikes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("u_bike.bikes.find", {
								query: {
									bike_model_id: doc.id,
								}
							})
							.then((res) => (doc.bikes = res));
					}),
				);
			},
			typology(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("u_bike.typologies.find", {
								query: {
									id: doc.typology_id,
								}
							})
							.then((res) => (doc.typology = res));
					}),
				);
			}
		},
		entityValidatopr: {
			model: { type: "string", max: 255 },
			model_conformity_file_id: { type: "number", positive: true },
			brakes: { type: "string", max: 2555 },
			brake_types: { type: "string", max: 255 },
			frame_type: { type: "string", max: 255 },
			front_suspension: { type: "string", max: 255 },
			tyres: { type: "string", max: 255 },
			seat_fixation_mechanism: { type: "string", max: 255 },
			front_wheel_fixation_mechanism: { type: "string", max: 255 },
			back_wheel_fixation_mechanism: { type: "string", max: 255 },
			handlebar_fixation_mechanism: { type: "string", max: 255 },
			gear_mechanism: { type: "string", max: 255 },
			chain_guard: { type: "string", max: 255 },
			front_illumination: { type: "string", max: 255 },
			back_illumination: { type: "string", max: 255 },
			side_reflections: { type: "string", max: 255 },
			pedal_reflections: { type: "string", max: 255 },
			front_reflections: { type: "string", max: 255 },
			back_reflections: { type: "string", max: 255 },
			sound_warning: { type: "string", max: 255 },
			instruction_manual: { type: "string", max: 255 },
			rest: { type: "string", max: 255 },
			mudguards: { type: "string", max: 255 },
			luggage_support: { type: "string", max: 255 },
			monitoring_system: { type: "string", max: 255 },
			coating_requirements: { type: "string", max: 255 },
			other_accessories: { type: "string", max: 255 },
			typology_id: { type: "number", positive: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		}

	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
