"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.bikes",
	table: "bike",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "bikes")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"identification",
			"size",
			"bike_model_id",
			"asset_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["bike_model"],
		withRelateds: {
			bike_model(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"u_bike.bike_models",
					"bike_model",
					"bike_model_id",
					"id",
					{},
					"id,model,typology_id",
					false,
				);
			},
			trips(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "u_bike.trips", "trips", "id", "bike_id");
			},
			asset(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.assets", "asset", "asset_id");
			},
		},
		entityValidator: {
			identification: { type: "string", max: 100 },
			size: { type: "string", max: 4 },
			bike_model_id: { type: "number", positive: true },
			asset_id: { type: "number", positive: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"checkIfBikeIdentification",
				async function sanatizeParams(ctx) {
					if (ctx.params.asset_id) {
						await ctx.call("infrastructure.assets.get", { id: ctx.params.asset_id });
					}
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"checkIfBikeIdentification",
				async function sanatizeParams(ctx) {
					if (ctx.params.asset_id) {
						await ctx.call("infrastructure.assets.get", { id: ctx.params.asset_id });
					}
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		disponible: {
			rest: "GET /disponible",
			visibility: "published",
			async handler(ctx) {
				const applications_with_bikes = await ctx.call("u_bike.applications.find", {
					query: {
						status: [
							"dispatch",
							"assigned",
							"complaint",
							"accepted",
							"contracted",
							"suspended",
							"quiting"
						],
						decision: "assigned",
					},
				});
				const in_use_bike_ids = applications_with_bikes.map((b) => b.bike_id);
				return this._find(ctx, {
					query: (qb) => {
						qb.where("id", "NOT IN", in_use_bike_ids);
					},
				});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		bike_kms_report: {
			rest: "POST /bike-kms-report",
			visibility: "published",
			params: {
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			handler(ctx) {
				let queryDate = "";
				if (ctx.params.start_date && ctx.params.end_date
					&& moment(ctx.params.start_date).isValid() && moment(ctx.params.end_date).isValid()) {
					queryDate = `and t.trip_start::date between '${moment(ctx.params.start_date).format("YYYY-MM-DD")}' and '${moment(ctx.params.end_date).format("YYYY-MM-DD")}' or t.trip_end::date between '${moment(ctx.params.start_date).format("YYYY-MM-DD")}' and '${moment(ctx.params.end_date).format("YYYY-MM-DD")}'`;
				}



				const query = `
				select b.identification, typ.name, a.full_name,
				round(coalesce((select sum(t.distance) from trip t where t.application_id  = a.id and t.bike_id = a.bike_id ${queryDate}),0 )::decimal,2) as total_user,
				round(coalesce((select sum(t.distance) from trip t where t.bike_id = a.bike_id ${queryDate}), 0)::decimal,2) as total_bike
				FROM bike b
				left join application a on b.id = a.bike_id AND a.status  = 'contracted'
				left join bike_model bm on bm.id = b.bike_model_id
				left join typology typ on bm.typology_id  = typ.id
				`;

				return this.adapter.raw(query).then(bikes => {
					return ctx.call("reports.templates.print", {
						key: "UBIKE_BIKE_STATS",
						options: {
							convertTo: "pdf",
						},
						data: {
							bikes: bikes.rows
						},
					});
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async checkIfBikeIdentification(ctx) {
			let check = await this._find(ctx, {
				query: {
					identification: ctx.params.identification.trim(),
				},
			});
			if (ctx.params.id) {
				check = check.filter((c) => c.id !== ctx.params.id);
			}
			if (check.length) {
				throw new ValidationError(
					"Bike Identification is unique!",
					"UBIKE_BIKE_IDENTIFICATION_UNIQUE",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
