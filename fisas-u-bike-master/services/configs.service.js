"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "u_bike.configs",
	table: "application_config",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("ubike", "configs")],
	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			scope: "u_bike:configs:list",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});
				return result;
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			scope: "u_bike:configs:create",
			params: {
				APPLICATION_START_DATE: { type: "date", optional: true, convert: true },
				APPLICATION_END_DATE: { type: "date", optional: true, convert: true },
				APPLICATION_NOTIFICATION_HOUR: { type: "date", optional: true, convert: true },
				APPLICATION_CONTRACT_NOTIFICATIONS_ANTECEDENCE_DAYS: {
					type: "number",
					convert: true,
					optional: true,
				},
				APPLICATION_CONTRACT_NOTIFICATIONS_FREQUENCY: {
					type: "number",
					convert: true,
					optional: true,
				},
				APPLICATION_MISSING_TRIPS: { type: "number", convert: true, optional: true },
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key: key } }).then(async (config) => {

						if ([
							"APPLICATION_START_DATE",
							"APPLICATION_END_DATE",
							"APPLICATION_NOTIFICATION_HOUR",
						].includes(config[0].key)) {
							ctx.params[config[0].key] = new Date(ctx.params[config[0].key]).toISOString();
						}

						if (config[0].key === "APPLICATION_NOTIFICATION_HOUR") {
							await ctx.call("u_bike.applications.refreshNotificationHour", {
								value: ctx.params[config[0].key],
								lastValue: config[0].value ? config[0].value : new Date(),
							});
						}


						if ([
							"APPLICATION_START_DATE",
							"APPLICATION_END_DATE",
							"APPLICATION_NOTIFICATION_HOUR",
						].includes(config[0].key)) {
							ctx.params[config[0].key] = new Date(ctx.params[config[0].key]).toISOString();
							return this._update(
								ctx,
								{
									id: config[0].id,
									value: ctx.params[config[0].key] ? ctx.params[config[0].key] : null,
								},
								true,
							);
						} else {
							return this._update(
								ctx,
								{
									id: config[0].id,
									value: ctx.params[config[0].key] ? JSON.stringify(ctx.params[config[0].key]) : null,
								},
								true,
							);
						}

					});
				});
				return Promise.all(promisses).then(() => ctx.call("u_bike.configs.list", {}));
			},
		},
		read: {
			// REST: GET /:id
			rest: "GET /:key",
			scope: "u_bike:configs:read",
			visibility: "published",
			params: {
				key: { type: "string" },
			},
			async handler(ctx) {
				const config = await this._find(ctx, { query: { key: ctx.params.key } });
				if (config[0].value && config[0].value.value) {
					config[0].value = config[0].value.value;
				}
				return config[0].value;
			},
		},

		terms: {
			rest: "GET /terms",
			scope: "u_bike:configs:read",
			visibility: "published",
			handler(ctx) {
				return ctx.call("configuration.terms.find", {
					query: {
						service_id: 13,
					},
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
