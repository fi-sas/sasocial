"use strict";

const { daily_commute_co2 } = require("../helpers/geo");
const moment = require("moment");
const { DASHBOARD_PERIOD } = require("../values/enums");
const Validator = require("fastest-validator");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "u_bike.stats",
	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		project: {
			rest: "GET /project",
			scope: "u_bike:stats:project",
			visibility: "published",
			async handler(ctx) {
				const profiles_count = await ctx.call("authorization.users.count_user_by_profile");
				let totalNumberUsers = 0;
				for (const x of profiles_count) {
					totalNumberUsers += x.count;
				}

				const data = [];
				const activeApplications = await ctx.call("u_bike.applications.getAllActive");
				for (const perfil of profiles_count) {
					const applications = activeApplications.filter(
						(app) => app.user.profile_id === perfil.profile_id,
					);
					const profile = await ctx.call("authorization.profiles.get", { id: perfil.profile_id });
					const aggregate_data = {};
					aggregate_data.profile_name = profile[0].name;
					aggregate_data.total_users_profile = perfil.count;
					aggregate_data.profile_users_perc_of_total = this.round2(
						parseFloat(perfil.count / parseFloat(totalNumberUsers)),
					);
					aggregate_data.profile_bike_users = applications.length;
					aggregate_data.profile_adherence_perc = this.round2(
						parseFloat(applications.length / parseFloat(perfil.count)),
					);
					data.push(aggregate_data);
				}

				return {
					totalNumberUsers: totalNumberUsers,
					users_enrolled: activeApplications.length,
					users_enrolled_perc: this.round2(
						parseFloat(activeApplications.length) / parseFloat(totalNumberUsers),
					),
					aggregate_data: data,
				};
			},
		},
		application_forms: {
			rest: "GET /application-forms",
			scope: "u_bike:stats:application-forms",
			visibility: "published",
			async handler(ctx) {
				const dl = require("datalib");

				// Get the total data for application forms
				const applicationFormData = await ctx.call("u_bike.application-forms.list");

				// Summarize dataset for the provided data
				const dataset = dl.read(applicationFormData.rows);
				const profAggData = dl
					.groupby(["subject"])
					.summarize({
						n_forms: ["count"],
					})
					.execute(dataset);
				// Return the built result object
				return {
					total_application_forms: applicationFormData.rows.length,
					aggregate_data: profAggData,
				};
			},
		},
		bikes: {
			rest: "GET /bikes",
			scope: "u_bike:stats:bikes",
			visibility: "published",
			params: {
				bike_id: { type: "number", optional: true, convert: true },
				user_id: { type: "number", optional: true, convert: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
				aggregation: {
					type: "string",
					enum: ["profile_name", "typology_name", "is_holiday", "is_weekend"],
					optional: true,
				},
			},
			async handler(ctx) {
				// Find filters provided
				const bikeId = ctx.params.bike_id;
				const userId = ctx.params.user_id;
				const result = {};

				if (
					(bikeId === undefined && userId === undefined) ||
					(bikeId === null && userId === null)
				) {
					// Count the number of currently active applications
					const nActiveApplications = await ctx.call("u_bike.applications.count", {
						query: { status: "contracted" },
					});
					const bikes = await ctx.call("u_bike.bikes.count");

					// Add values to the result object
					result.total_bikes = bikes;
					result.bikes_in_use = nActiveApplications;
					result.bikes_usage_perc = nActiveApplications / parseFloat(result.total_bikes);
				}

				// Execute aggregation and measure calculations
				const dataset = await ctx.call("u_bike.trips.dataset", ctx.params);

				result.aggregate_data = [dataset];

				if (result.aggregate_data.length !== 0) {
					result.aggregate_data = Object.assign(
						result.aggregate_data[0],
						daily_commute_co2(result.aggregate_data[0]["sum_distance"]),
					);

					for (const key of Object.keys(result.aggregate_data)) {
						// Round decimal numbers(2 decimal cases)
						if (String(result.aggregate_data[key]).includes("."))
							result.aggregate_data[key] = parseFloat(result.aggregate_data[key]).toFixed(2);
					}
				} else {
					result.aggregate_data = {
						average_avg_speed: 0,
						average_height_diff: 0,
						average_time: 0,
						bus_saved_co2: 0,
						count_trips: 0,
						diesel_saved_co2: 0,
						eletric_saved_co2: 0,
						gasoline_saved_co2: 0,
						gpl_saved_co2: 0,
						max_avg_speed: 0,
						max_height_diff: 0,
						sum_co2_emissions_saved: 0,
						sum_distance: 0,
						sum_energy: 0,
						sum_time: 0,
						sum_watts: 0,
						train_saved_co2: 0,
					};
				}

				return result;
			},
		},

		saving_co2: {
			rest: "GET /saving-co2",
			scope: "u_bike:stats:bikes",
			visibility: "published",
			params: {
				period: { type: "enum", values: DASHBOARD_PERIOD, default: "month" },
				aggregation: {
					type: "string",
					enum: ["profile_name", "typology_name", "is_holiday", "is_weekend"],
					optional: true,
				},
			},
			async handler(ctx) {
				// Find filters provided
				const userId = ctx.meta.user.id;
				const result = {};

				ctx.params.start_date = ctx.params.start_date || null;
				ctx.params.end_date = ctx.params.end_date || null;
				let period = ctx.params.period;
				if (period != "ever") {
					const validation = new Validator();
					const schema = {
						start_date: { type: "date", convert: true },
						end_date: { type: "date", convert: true },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						ctx.params.start_date = new Date(ctx.params.start_date).toISOString();
						ctx.params.end_date = new Date(ctx.params.end_date).toISOString();
					}
				}
				if (!userId) {
					// Count the number of currently active applications
					const nActiveApplications = await ctx.call("u_bike.applications.count", {
						query: { status: "contracted" },
					});
					const bikes = await ctx.call("u_bike.bikes.count");
					// Add values to the result object
					result.total_bikes = bikes;
					result.bikes_in_use = nActiveApplications;
					result.bikes_usage_perc = nActiveApplications / parseFloat(result.total_bikes);
				}
				// Execute aggregation and measure calculations
				const dataset = await ctx.call("u_bike.trips.dataset", ctx.params);
				result.aggregate_data = [dataset];
				if (result.aggregate_data.length !== 0) {
					result.aggregate_data = Object.assign(
						result.aggregate_data[0],
						daily_commute_co2(result.aggregate_data[0]["sum_distance"]),
					);
					for (const key of Object.keys(result.aggregate_data)) {
						// Round decimal numbers(2 decimal cases)
						if (String(result.aggregate_data[key]).includes("."))
							result.aggregate_data[key] = parseFloat(result.aggregate_data[key]).toFixed(2);
					}
				} else {
					result.aggregate_data = {
						average_avg_speed: 0,
						average_height_diff: 0,
						average_time: 0,
						bus_saved_co2: 0,
						count_trips: 0,
						diesel_saved_co2: 0,
						eletric_saved_co2: 0,
						gasoline_saved_co2: 0,
						gpl_saved_co2: 0,
						max_avg_speed: 0,
						max_height_diff: 0,
						sum_co2_emissions_saved: 0,
						sum_distance: 0,
						sum_energy: 0,
						sum_time: 0,
						sum_watts: 0,
						train_saved_co2: 0,
					};
				}
				return result;
			},
		},

		print_stats: {
			rest: "POST /bikes/print",
			visibility: "published",
			params: {
				bike_id: { type: "number", optional: true, convert: true },
				user_id: { type: "number", optional: true, convert: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				let user = null,
					bike = null;
				if (ctx.params.user_id) {
					user = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
				}
				if (ctx.params.bike_id) {
					bike = await ctx.call("u_bike.bikes.get", { id: ctx.params.bike_id });
				}
				return ctx.call("u_bike.stats.bikes", ctx.params).then((stats) => {
					stats.date = moment().format("YYYY-MM-DD");
					stats.start_date = ctx.params.start_date
						? moment(ctx.params.start_date).format("YYYY-MM-DD")
						: "--";
					stats.end_date = ctx.params.end_date
						? moment(ctx.params.end_date).format("YYYY-MM-DD")
						: "--";
					stats.user = ctx.params.user_id ? user[0].name : "--";

					stats.bike = ctx.params.bike_id ? bike[0].identification : "--";
					if (ctx.params.user_id || ctx.params.bike_id) {
						stats.bikes_in_use = "--";
						stats.total_bikes = "--";
					}
					return ctx.call("reports.templates.print", {
						key: "UBIKE_STATS",
						options: {
							convertTo: "pdf",
						},
						data: {
							stats,
						},
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		round2(num) {
			return Math.round(num * 100) / 100;
		},
		/**
		 * Returns the usage data for the bike with the provided id.
		 *
		 * @param {Number} bikeId The id of the bike to fetch the statistical data.
		 * @param {Number} userId The id of the user to fetch the statistical data.
		 * @param {String} start_date The trip start date to fetch the statistical data.
		 * @param {String} end_date The trip start date to fetch the statistical data.
		 * @returns {Array} An array with the statistical data for the bike provided.
		 */
		async getUsageData(ctx, bikeId, userId, start_date, end_date) {
			const whereClauseID = {};
			if (bikeId) {
				whereClauseID.bike_id = bikeId;
			}
			if (userId) {
				whereClauseID.user_id = userId;
			}
			return await ctx.call("u_bike.trips.getWithFilters", {
				whereClause: whereClauseID,
				start_date: start_date,
				end_date: end_date,
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
