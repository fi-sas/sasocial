"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { FUEL_TYPE } = require("../values/enums");
const { ValidationError } = require("@fisas/ms_core/src/helpers/errors");
const { energy, co2, getAvgSpeed, getGeneratedWatts } = require("../helpers/geo");
const got = require("got");
const xml2js = require("xml2js");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
const Knex = require("knex");
/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "u_bike.trips",
	table: "trip",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("u_bike", "trips")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"bike_id",
			"bike_typology_id",
			"typology_name",
			"manufacturer",
			"model",
			"fuel_type",
			"year_manufactured",
			"engine_power",
			"application_id",
			"user_id",
			"user_profile_id",
			"profile_name",
			"user_age",
			"distance",
			"height_diff",
			"energy",
			"co2_emissions_saved",
			"time",
			"watts",
			"avg_speed",
			"description",
			"trip_start",
			"trip_end",
			"is_weekend",
			"is_holiday",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			bike(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "u_bike.bikes", "bike", "bike_id");
			},
			bike_typology(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "u_bike.typologies", "bike_typology", "bike_typology_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			bike_id: { type: "number", positive: true, optional: true },
			bike_typology_id: { type: "number", positive: true },
			typology_name: { type: "string", max: 100, optional: true },
			manufacturer: { type: "string", max: 100, optional: true },
			model: { type: "string", max: 100, optional: true },
			fuel_type: { type: "enum", values: FUEL_TYPE, optional: true },
			year_manufactured: { type: "number", nullable: true, optional: true, convert: true },
			engine_power: { type: "number", nullable: true, optional: true, convert: true },
			application_id: { type: "number", positive: true, optional: true, convert: true },
			user_id: { type: "number", positive: true },
			user_profile_id: { type: "number", positive: true },
			profile_name: { type: "string", max: 100 },
			user_age: { type: "number", convert: true },
			distance: { type: "number", convert: true },
			height_diff: { type: "number", convert: true },
			energy: { type: "number", convert: true },
			co2_emissions_saved: { type: "number", convert: true },
			time: { type: "number", optional: true, convert: true },
			watts: { type: "number", default: 0, convert: true },
			avg_speed: { type: "number", default: 0, convert: true },
			description: { type: "string" },
			trip_start: { type: "date", convert: true },
			trip_end: { type: "date", convert: true, optional: true },
			is_weekend: { type: "boolean" },
			is_holiday: { type: "boolean" },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			list: [
				function sortBy(ctx) {
					ctx.params.extraQuery = (qb) => {
						if (ctx.params.sort && ctx.params.sort.includes("bike."))
							qb.innerJoin("bike", "trip.bike_id", "bike.id");
					};
				},
			],
			create: [
				"setCtxParams",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"setCtxParams",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				"setCtxParams",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
				],
			},
		},
		create: {
			// REST: GET /:id
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
				],
			},
		},
		getWithFilters: {
			cache: {
				keys: ["bikeId", "userId", "start_date", "end_date"],
			},
			params: {
				bikeId: { type: "number", convert: true, integer: true, optional: true },
				userId: { type: "number", convert: true, integer: true, optional: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				const query = {};
				if (ctx.params.bikeId) query.bike_id = ctx.params.bikeId;
				if (ctx.params.userId) query.user_id = ctx.params.userId;
				if (ctx.params.start_date && ctx.params.end_date) {
					query.trip_start = { gte: moment(ctx.params.start_date).format("YYYY-MM-DD") };
					query.trip_start = { lte: moment(ctx.params.end_date).format("YYYY-MM-DD") };
				} else if (ctx.params.start_date) {
					query.trip_start = { gte: moment(ctx.params.start_date).format("YYYY-MM-DD") };
				} else if (ctx.params.end_date) {
					query.trip_start = { lte: moment(ctx.params.end_date).format("YYYY-MM-DD") };
				}
				return await this._find(ctx, { query });
			},
		},

		dataset: {
			params: {
				bike_id: { type: "number", integer: true, convert: true, optional: true },
				user_id: { type: "number", integer: true, convert: true, optional: true },
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				let add_start_date = "";
				let add_end_date = "";
				let bike_id_query = "";
				let user_id_query = "";
				let bike_user_query = "";

				if (ctx.params.start_date) {
					add_start_date =
						" AND trip_start >= " + "'" + moment(ctx.params.start_date).toISOString() + "'";
				}

				if (ctx.params.end_date) {
					add_end_date =
						" AND trip_start <= " + "'" + moment(ctx.params.end_date).toISOString() + "'";
				}

				if (ctx.params.user_id && !ctx.params.bike_id) {
					user_id_query = " AND user_id=" + ctx.params.user_id;
				}

				if (ctx.params.bike_id && !ctx.params.user_id) {
					bike_id_query = " AND bike_id=" + ctx.params.bike_id;
				}

				if (ctx.params.bike_id && ctx.params.user_id) {
					bike_user_query =
						" AND user_id=" + ctx.params.user_id + " And bike_id= " + ctx.params.bike_id;
				}

				const a = await this.adapter.raw(
					"select count(id) as count_trips, sum(distance) as sum_distance, sum(energy ) as aum_energy,sum(time) as sum_time, " +
						"avg(time) as avg_time, sum(watts ) as sum_watts, sum(co2_emissions_saved ) as sum_co2_emissions_saved, " +
						"avg(height_diff) as avg_height_diff, max(height_diff ) as max_height_diff, avg(avg_speed) as avg_avg_speed, " +
						"max(avg_speed) as max_avg_speed from trip t " +
						"where user_id in (select user_id from application where status = 'contracted') " +
						user_id_query +
						bike_id_query +
						bike_user_query +
						add_start_date +
						add_end_date,
				);

				return a.rows[0];
			},
		},

		kms_report: {
			rest: "POST /kms-report",
			visibility: "published",
			params: {
				start_date: { type: "date", convert: true, optional: true },
				end_date: { type: "date", convert: true, optional: true },
			},
			async handler(ctx) {
				const q1 = this.adapter
					.db(this.adapter.table)
					.select("user_id", "application_id", "bike_id")
					.sum({ distance: "distance" })
					.groupBy("user_id", "application_id", "bike_id");
				if (ctx.params.start_date && ctx.params.end_date) {
					q1.whereBetween(this.adapter.raw("\"trip_start\"::date"), [
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
						moment(ctx.params.end_date).format("YYYY-MM-DD"),
					]);

					q1.whereBetween(this.adapter.raw("\"trip_end\"::date"), [
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
						moment(ctx.params.end_date).format("YYYY-MM-DD"),
					]);
				}

				await Promise.all([q1]).then(async (tripsResponse) => {
					if (tripsResponse.length) {
						const applications = await ctx.call("u_bike.applications.get", {
							id: tripsResponse[0].map((t) => t.application_id),
							withRelated: "user",
						});
						for (const trip of tripsResponse[0]) {
							const application = applications.find((ap) => ap.id === trip.application_id);
							trip.name =
								application && application.full_name
									? application.full_name
									: application && application.user
										? application.user[0].name
										: null;
							trip.email =
								application && application.email
									? application.email
									: application && application.user
										? application.user[0].email
										: null;
						}

						ctx.call("reports.templates.print", {
							key: "UBIKE_KMS_TRAVELED_REPORT",
							options: {
								convertTo: "xlsx",
							},
							data: {
								trips: tripsResponse[0],
							},
						});
					}
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		age(birthday) {
			const ageDifMs = Date.now() - new Date(birthday).getTime();
			const ageDate = new Date(ageDifMs);
			return Math.abs(ageDate.getUTCFullYear() - 1970);
		},
		calculateMinutes(ctx) {
			if (!ctx.params.trip_start || !ctx.params.trip_end) {
				return 0;
			}

			if (moment(ctx.params.trip_end).isSameOrBefore(moment(ctx.params.trip_start))) {
				throw new ValidationError(
					"Final time cannot be bigger that initial time",
					"U_BIKE_TRIPS_FINAL_INVALID_HOURS",
					{},
				);
			}
			if (moment(ctx.params.trip_start).isSameOrAfter(moment(ctx.params.trip_end))) {
				throw new ValidationError(
					"Initial time cannot be less that Final time",
					"U_BIKE_TRIPS_INITIAL_INVALID_HOURS",
					{},
				);
			}

			if (ctx.params.trip_start && ctx.params.trip_end) {
				return moment(ctx.params.trip_end).diff(moment(ctx.params.trip_start), "minutes");
			}
			return 0;
		},

		async setCtxParams(ctx) {
			let applications = [];
			if (ctx.meta.isBackoffice) {
				applications = await ctx.call("u_bike.applications.get", {
					id: ctx.params.application_id,
					withRelated: false,
				});
			} else {
				applications = await ctx.call("u_bike.applications.find", {
					query: {
						user_id: ctx.meta.user.id,
						status: [
							"submitted",
							"analysis",
							"dispatch",
							"assigned",
							"enqueued",
							"complaint",
							"accepted",
							"contracted",
							"quiting",
						],
					},
					withRelated: false,
				});
			}
			if (!applications.length) {
				throw new ValidationError(
					"The user donat have any application in progress",
					"UBIKE_APPLICATION_IN_PROGRESS_NOT_FOUND",
					{},
				);
			}

			if (ctx.params.trip_start && ctx.params.trip_end && !ctx.params.time) {
				ctx.params.time = this.calculateMinutes(ctx);
			}

			const bike = await ctx.call("u_bike.bikes.get", {
				id: applications[0].bike_id,
				withRelated: "bike_model",
			});

			if (!bike.length) {
				throw new ValidationError(
					"The user dont have associated bike",
					"UBIKE_APPLICATION_BIKE_NOT_FOUND",
					{},
				);
			}

			const typology = await ctx.call("u_bike.typologies.get", {
				id: bike[0].bike_model.typology_id,
			});

			const age = this.age(applications[0].birth_date);

			// Calculate the total energy spent on the trip
			const totalEnergy = await energy({
				gender: applications[0].gender,
				age: age,
				weight: applications[0].weight,
				height: applications[0].height,
				time: ctx.params.time / 60,
				distanceP: ctx.params.distance,
			});

			// Calculate the total CO2 emissions saved on the trip
			const totalCo2Emissions = co2({
				commute: applications[0].daily_commute,
				fuelType: applications[0].daily_consumption,
				dist: ctx.params.distance,
			});

			const avgSpeed = getAvgSpeed(ctx.params.distance, ctx.params.time / 60);

			const watts = getGeneratedWatts({
				weight: applications[0].weight,
				avgSpeed,
			});
			ctx.params.bike_id = bike[0].id;
			ctx.params.bike_typology_id = typology.length ? typology[0].id : null;
			ctx.params.typology_name = typology.length ? typology[0].name : null;
			ctx.params.application_id = applications[0].id;
			ctx.params.user_id = applications[0].user_id;
			ctx.params.manufacturer = applications[0].manufacturer;
			ctx.params.model = applications[0].model;
			ctx.params.year_manufactured = applications[0].year_manufactured;
			ctx.params.engine_power = applications[0].engine_power;
			ctx.params.user_profile_id = ctx.meta.user.profile_id;
			const profile = await ctx.call("authorization.profiles.get", {
				id: ctx.meta.user.profile_id,
				withRelated: false,
			});
			ctx.params.profile_name = profile[0].name;
			ctx.params.user_age = age;
			ctx.params.energy = totalEnergy;
			ctx.params.avg_speed = avgSpeed;
			ctx.params.watts = watts;
			ctx.params.co2_emissions_saved = totalCo2Emissions;
			ctx.params.fuel_type = applications[0].daily_consumption;
			const day = new Date().getDay();
			ctx.params.is_weekend = day === 6 || day === 0 ? true : false;
			const res = await got(
				"http://services.sapo.pt/Holiday/GetNationalHolidays?year=" + new Date().getFullYear(),
			);
			const parser = new xml2js.Parser();
			const json = await parser.parseStringPromise(res.body);
			const holidays = json.GetNationalHolidaysResponse.GetNationalHolidaysResult.filter((h) =>
				h.Holiday.find((holiday) =>
					moment(...holiday.Date, "YYYY-MM-DD").isSame(
						moment(new Date(ctx.params.trip_start)).format("YYYY-MM-DD"),
					),
				),
			);
			ctx.params.is_holiday = holidays.length ? true : false;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
