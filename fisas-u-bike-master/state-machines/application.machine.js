let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: "submit", from: "elaboration", to: "submitted" },
			{ name: "cancel", from: "elaboration", to: "cancelled" },
			{ name: "analyse", from: "submitted", to: "analysis" },
			//{ name: "resubmit", from: "analysis", to: "elaboration" },
			{ name: "cancel", from: "submitted", to: "cancelled" },
			{ name: "cancel", from: "analysis", to: "cancelled" },
			{ name: "dispatch", from: "analysis", to: "dispatch" },
			{ name: "reanalyse", from: "dispatch", to: "analysis" },
			{ name: "accept", from: "dispatch", to: "assigned" },
			{ name: "accept", from: "dispatch", to: "enqueued" },
			{ name: "accept", from: "dispatch", to: "unassigned" },
			{ name: "cancel", from: "enqueued", to: "cancelled" },
			{ name: "dispatch", from: "enqueued", to: "dispatch" },
			{ name: "complain", from: "unassigned", to: "complaint" },
			{ name: "confirm", from: "assigned", to: "accepted" },
			{ name: "reject", from: "assigned", to: "rejected" },
			{ name: "hire", from: "accepted", to: "contracted" },
			{ name: "suspend", from: "contracted", to: "suspended" },
			{ name: "unsuspend", from: "suspended", to: "contracted" },
			{ name: "close", from: "suspended", to: "closed" },
			{ name: "complain", from: "assigned", to: "complaint" },
			{ name: "quit", from: "contracted", to: "quiting" },
			{ name: "stay", from: "quiting", to: "contracted" },
			{ name: "withdrawal", from: "quiting", to: "withdrawal" },
			{ name: "close", from: "contracted", to: "closed" },
			{ name: "cancel", from: "assigned", to: "cancelled" },
		],
		methods: {
			onAfterInit: async function (lifecycle) {},
			onAfterSubmit: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterCancel: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterAnalyse: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			/*onAfterResubmit: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},*/
			onAfterDispatch: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterComplain: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterAccept: async function (lifecycle) {
				lifecycle.to = ctx.params.application.decision;
				return await this.saveApplication(lifecycle);
			},
			onAfterReanalyse: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterConfirm: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterReject: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterHire: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterSuspend: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterUnsuspend: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterClose: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterQuit: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterStay: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterWithdrawal: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			// Save new application state and operation in applications history
			saveApplication: async function (lifecycle) {
				const app = await ctx.call("u_bike.applications.patch", {
					...ctx.params.application,
					id: ctx.params.id,
					status: lifecycle.to,
				});
				await ctx.call("u_bike.applications_history.create", {
					application_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});
				return app;
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
