let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
		],
		methods: {

			// onAfterTransition: async function (lifecycle) {
			// 	return await this.saveApplicationForm(lifecycle);
			// },


			// // Save new application state and operation in applications history
			// saveApplicationForm: async function (lifecycle) {
			// 	const app = await ctx.call("u_bike.application_forms.patch", {
			// 		id: ctx.params.id,
			// 		status: lifecycle.to,
			// 		...ctx.params.application,
			// 	});
			// 	await ctx.call("u_bike.application_forms_history.create", {
			// 		application_form_id: ctx.params.id,
			// 		user_id: ctx.meta.user.id,
			// 		status: lifecycle.to,
			// 		notes: ctx.params.notes,
			// 	});
			// 	return app;
			// }
		}

	});
}

exports.createStateMachine = createStateMachine;
