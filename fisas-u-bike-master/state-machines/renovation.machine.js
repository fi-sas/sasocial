let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: 'analyse', from: 'submitted', to: 'analysis' },
			{ name: 'dispatch', from: 'analysis', to: 'dispatch' },
			{ name: 'approve', from: 'dispatch', to: 'approved' },
			{ name: 'reject', from: 'dispatch', to: 'rejected' },
		],
		methods: {

			onAfterAnalyse: async function (lifecycle) {
				return await this.saveApplicationForm(lifecycle);
			},

			onAfterDispatch: async function (lifecycle) {
				return await this.saveApplicationForm(lifecycle);
			},

			onAfterApprove: async function (lifecycle) {
				lifecycle.to = ctx.params.application_form.decision;
				return await this.saveApplicationForm(lifecycle);
			},

			onAfterReject: async function (lifecycle) {
				return await this.saveApplicationForm(lifecycle);
			},

			// Save new application state and operation in applications history
			saveApplicationForm: async function (lifecycle) {
				const app = await ctx.call("u_bike.application-forms.patch", {
					...ctx.params.application_form,
					id: ctx.params.id,
					status: lifecycle.to
				});
				await ctx.call("u_bike.application_forms_history.create", {
					application_form_id: ctx.params.application_form.id,
					user_id: ctx.meta.user.id,
					status: lifecycle.to,
					notes: ctx.params.notes,
				});
				return app;
			},
			// Change application status
			changeApplicationStatus: async function (action) {
				const app = await ctx.call("u_bike.applications.patch", {
					id: ctx.params.application_form.application_id,
					status: action,
				});
				await ctx.call("u_bike.applications_history.create", {
					application_id: ctx.params.application_form.application_id,
					status: action,
					user_id: ctx.meta.user.id,
					notes: null,
				});
			}

		}

	});
}

exports.createStateMachine = createStateMachine;
