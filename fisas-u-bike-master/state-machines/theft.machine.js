let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			{ name: 'analyse', from: 'submitted', to: 'analysis' },
			{ name: 'close', from: 'analysis', to: 'closed' },
		],
		methods: {

			onAfterAnalyse: async function (lifecycle) {
				return await this.saveApplicationForm(lifecycle);
			},
			onAfterClose: async function (lifecycle) {
				lifecycle.to = ctx.params.application_form.decision;
				return await this.saveApplicationForm(lifecycle);
			},

			// Save new application state and operation in applications history
			saveApplicationForm: async function (lifecycle) {
				const app = await ctx.call("u_bike.application-forms.patch", {
					...ctx.params.application_form,
					id: ctx.params.id,
					status: lifecycle.to,
				});
				await ctx.call("u_bike.application_forms_history.create", {
					application_form_id: ctx.params.id,
					user_id: ctx.meta.user.id,
					status: lifecycle.to,
					notes: ctx.params.notes,
				});
				return app;
			}
		}

	});
}

exports.createStateMachine = createStateMachine;
