const GENDER = ["M", "F"];

const DAILY_COMMUTE = ["Walk", "Car", "Motorcycle", "Bus", "Train", "Bike", "Other"];

const FUEL_TYPE = ["Electric", "GPL", "Diesel", "Gasoline"];

const APPLICATION_DECISION = ["assigned", "enqueued", "unassigned"];

const APPLICATION_STATUS = ["elaboration", "submitted", "analysis", "cancelled", "dispatch", "assigned", "enqueued", "unassigned", "suspended", "complaint", "accepted", "contracted", "rejected", "quiting", "withdrawal", "closed"];

const APPLICATION_ACTIONS = ["submit", "cancel", "analyse", "resubmit", "cancel", "cancel", "dispatch", "reanalyse", "accept", "accept", "accept", "cancel", "dispatch", "complain", "confirm", "reject", "hire", "suspend", "unsuspend", "close", "complain", "dispatch", "quit", "stay", "withdrawal", "close"];

const APPLICATION_FORM_SUBJECT = ["THEFT", "WITHDRAWAL", "ABSENCE", "COMPLAINT", "CONTESTATION", "RENOVATION", "MAINTENANCE_REQUEST", "PRIVATE_MAINTENANCE", "ADDITIONAL_MAINTENANCE_KIT", "BREAKDOWN"];
const APPLICATION_FORM_DECISION = ["approved", "rejected"];
const APPLICATION_FORM_STATUS = ["submitted", "analysis", "approved", "rejected", "dispatch", "closed"];

const APPLICATION_FORM_ACTIONS = ["analyse", "dispatch", "approve", "reject", "close"];
const DASHBOARD_PERIOD = ["day", "month", "year", "ever"];

module.exports = {
	GENDER,
	DAILY_COMMUTE,
	FUEL_TYPE,
	APPLICATION_DECISION,
	APPLICATION_STATUS,
	APPLICATION_FORM_SUBJECT,
	APPLICATION_FORM_DECISION,
	APPLICATION_FORM_STATUS,
	DASHBOARD_PERIOD,
	APPLICATION_FORM_ACTIONS,
	APPLICATION_ACTIONS
};

