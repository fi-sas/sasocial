## [1.22.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.22.0...v1.22.1) (2022-05-06)


### Performance Improvements

* **applications:** remove relateds from target notifications actions ([88f85cd](https://gitlab.com/fi-sas/fisas-volunteering/commit/88f85cd208694abdc284935880e31b6c0534ee20))

# [1.22.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.21.0...v1.22.0) (2022-05-04)


### Features

* **volunteering:** add application information to response ([a41e719](https://gitlab.com/fi-sas/fisas-volunteering/commit/a41e719798e726ac10abbd3c328850180e478683))
* **volunteering:** clear cache by user interest action ([4821441](https://gitlab.com/fi-sas/fisas-volunteering/commit/4821441a12e330c128615028168131077a4c6e14))
* **volunteering:** new error key and lint issue fix ([8d9750d](https://gitlab.com/fi-sas/fisas-volunteering/commit/8d9750d72fe0cb3e6ab42a64336e788ca66abd8c))

# [1.21.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.20.1...v1.21.0) (2022-04-26)


### Features

* **volunteering:** check experience publish end date ([0ffe443](https://gitlab.com/fi-sas/fisas-volunteering/commit/0ffe4432283c5eb433aaab2b4f060873528879bb))
* **volunteering:** new error key ([69fd578](https://gitlab.com/fi-sas/fisas-volunteering/commit/69fd5781b4575b505321176740aabfa2a4e37ae5))
* **volunteering:** show all complains by user ([7e4ca4c](https://gitlab.com/fi-sas/fisas-volunteering/commit/7e4ca4c391f74243b2261a3ccc611194f99e023d))
* **volunteering:** show all users in experiences ([1898e51](https://gitlab.com/fi-sas/fisas-volunteering/commit/1898e51ea9082a6f3ea06fc61ac47cee4827dfd3))

## [1.20.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.20.0...v1.20.1) (2022-04-19)


### Bug Fixes

* **volunteering:** refactor check experience date and hours method ([52b01dd](https://gitlab.com/fi-sas/fisas-volunteering/commit/52b01dd014c48d914360452e09716eed0c9e2e65))
* **volunteering:** remove rejected hours from validated ([5512de1](https://gitlab.com/fi-sas/fisas-volunteering/commit/5512de15c84eb9795e86d61d5e2bb17342e86ef6))

# [1.20.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.19.4...v1.20.0) (2022-04-06)


### Features

* **volunteering:** add unaccent extension and lint issues ([2b685f7](https://gitlab.com/fi-sas/fisas-volunteering/commit/2b685f7aa72193762979632b8a16f21938d7a92f))
* **volunteering:** express interest in approved,selection,colaboration ([b2d48ed](https://gitlab.com/fi-sas/fisas-volunteering/commit/b2d48ed9f26f863f07c5336a79da088e9cd45328))

## [1.19.4](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.19.3...v1.19.4) (2022-04-05)


### Bug Fixes

* **volunteering:** fix reject request ([03d1ec1](https://gitlab.com/fi-sas/fisas-volunteering/commit/03d1ec1f7917b13fe47c0a7917ea1e35dbeae7c1))
* **volunteering:** fix school_id field for not student ([9844ef4](https://gitlab.com/fi-sas/fisas-volunteering/commit/9844ef41dee6b95445e07a6e05318028f2c56259))

## [1.19.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.19.2...v1.19.3) (2022-04-01)


### Bug Fixes

* **volunterting:** check if email exists in authorization and refator code ([ba72be7](https://gitlab.com/fi-sas/fisas-volunteering/commit/ba72be776dc601ac2792c6e3c65d508d3a87b37a))

## [1.19.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.19.1...v1.19.2) (2022-02-17)


### Performance Improvements

* **applications:** remove awaits from send notifications ([321c342](https://gitlab.com/fi-sas/fisas-volunteering/commit/321c342b106f69cdf1deba3a8e634ce622050c6e))

## [1.19.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.19.0...v1.19.1) (2022-01-10)


### Bug Fixes

* add dispatch to active apps, user interests ([9e3fa8b](https://gitlab.com/fi-sas/fisas-volunteering/commit/9e3fa8bf199925064239fc31d80b2b43ec108418))

# [1.19.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.18.0...v1.19.0) (2021-12-03)


### Features

* address publications, new filters ([f23c545](https://gitlab.com/fi-sas/fisas-volunteering/commit/f23c5456572293d568061dfb8cab9e69d92d31e7))

# [1.18.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.17.0...v1.18.0) (2021-12-02)


### Features

* decision status correction ([09a5401](https://gitlab.com/fi-sas/fisas-volunteering/commit/09a5401b42e8a7a271d73948171870ea085e60f5))

# [1.17.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.16.0...v1.17.0) (2021-11-29)


### Features

* status management ([0d18551](https://gitlab.com/fi-sas/fisas-volunteering/commit/0d185519a3d36a2d6e75f43f3d98cc587bb97d36))

# [1.16.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.15.0...v1.16.0) (2021-11-17)


### Features

* editar candidatura ([42b5b19](https://gitlab.com/fi-sas/fisas-volunteering/commit/42b5b19b49d41ac0bc2ee087e556962b39f18f44))

# [1.15.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.14.4...v1.15.0) (2021-10-14)


### Bug Fixes

* **applicaton_machine:** new status dispatch ([b8a66b1](https://gitlab.com/fi-sas/fisas-volunteering/commit/b8a66b1478a38f6adad8bb6bf3fb2c7ee0bbd960))
* **experience_machine:** add dispatch ([f09d4c4](https://gitlab.com/fi-sas/fisas-volunteering/commit/f09d4c469917abc0fe5e857e1d3113276adbe303))
* **user-interest-machine:** add new status ([fdcd55b](https://gitlab.com/fi-sas/fisas-volunteering/commit/fdcd55b783a7b895715e222c43c54ed262dd7f77))


### Features

* **applications:** add dispatch, decision, reject_reason ([6e1c278](https://gitlab.com/fi-sas/fisas-volunteering/commit/6e1c278e6a7ceefff3f80f561a10fffd2b659c9e))
* **constants:** add dispatch ([a7c0d98](https://gitlab.com/fi-sas/fisas-volunteering/commit/a7c0d9823f103e05c887b93ac72a8f274822a214))
* **constants:** add dispatch ([47f8800](https://gitlab.com/fi-sas/fisas-volunteering/commit/47f88007b4c2b978c8fefe13885f517c8664ae7c))
* **constants:** add dispatch constants ([3f02bc2](https://gitlab.com/fi-sas/fisas-volunteering/commit/3f02bc2a53496ba1cf1e7714ba5681423eb1f902))
* **experience_user_interests:** add dispatch status ([e025846](https://gitlab.com/fi-sas/fisas-volunteering/commit/e0258469259894a58e56bbf8dcdc6dc8e992b036))
* **experiences:** add dispact, decision, reject_reason ([6d864d6](https://gitlab.com/fi-sas/fisas-volunteering/commit/6d864d6953c7c8af0d90b91ad3f51723aac6c79f))
* **migrations:** add dispatch in experience ([b779484](https://gitlab.com/fi-sas/fisas-volunteering/commit/b77948419b5ca153a08799f4c0f9065fb5efd1db))
* **migrations:** add dispatch, decision and reject_reason ([9e2c556](https://gitlab.com/fi-sas/fisas-volunteering/commit/9e2c5569b905ebc4f66117ad45b1cd493f46dfc6))
* **migrations:** add status dispatch application ([1e24c54](https://gitlab.com/fi-sas/fisas-volunteering/commit/1e24c54dc57db01958678506929310a84f890500))

## [1.14.4](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.14.3...v1.14.4) (2021-09-29)


### Bug Fixes

* **experiences:** fix experiences list scope ([71d8641](https://gitlab.com/fi-sas/fisas-volunteering/commit/71d8641f4560611d3b71ecc3c5a2a6263b318015))

## [1.14.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.14.2...v1.14.3) (2021-09-17)


### Bug Fixes

* **applications:** update endpoint stats ([aa7565f](https://gitlab.com/fi-sas/fisas-volunteering/commit/aa7565f78ecaa0a82cdf3f2ebcd07f2dcba10189))
* **experience_user_interests:** update endpoint stats ([aa4de89](https://gitlab.com/fi-sas/fisas-volunteering/commit/aa4de8959959e515931dc57cacb534fabd710eed))
* **experiences:** update endpoint stats ([4e65a9e](https://gitlab.com/fi-sas/fisas-volunteering/commit/4e65a9e9fa55458dc701d60f5f96632ffa92276d))

## [1.14.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.14.1...v1.14.2) (2021-09-17)


### Bug Fixes

* **applications:** error message ([a69266a](https://gitlab.com/fi-sas/fisas-volunteering/commit/a69266ae74ded6da681eb7bc9ff92f8998d242c3))
* **attendances:** calculate hours ([cee3d41](https://gitlab.com/fi-sas/fisas-volunteering/commit/cee3d41a7a822045b3d3819feb18c0563cb23c8f))
* **experience_user_interests:** update message error ([49f75f8](https://gitlab.com/fi-sas/fisas-volunteering/commit/49f75f8072e3c8a4fcdd73ad2d65e9f6de2f6e16))
* **experiences:** update message error ([3448e57](https://gitlab.com/fi-sas/fisas-volunteering/commit/3448e573d5d5017aad702ca3362ac8c26e5c5504))
* **general-reports:** erros in call endpoints ([53d23c6](https://gitlab.com/fi-sas/fisas-volunteering/commit/53d23c6aa1267576bf49687af86c5b2761eacec4))

## [1.14.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.14.0...v1.14.1) (2021-09-15)


### Bug Fixes

* **applications:** remove field ([b929f73](https://gitlab.com/fi-sas/fisas-volunteering/commit/b929f73e19ccde0981fb274bea30155b0e597697))
* **experience_user_interests:** update scope ([554d97c](https://gitlab.com/fi-sas/fisas-volunteering/commit/554d97c99b59defbb19756cdf5f821dbb9ff9cab))

# [1.14.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.13.3...v1.14.0) (2021-09-13)


### Bug Fixes

* **applications:** remove validation employee_number ([d581fb6](https://gitlab.com/fi-sas/fisas-volunteering/commit/d581fb6e00c8c59f0c225cdab53df8131c41c9a1))
* **migrations:** update migration error ([e8ab35c](https://gitlab.com/fi-sas/fisas-volunteering/commit/e8ab35c9589fb75ca5b2ba9b2d0858ce84ba6df1))


### Features

* **migration:** update table application ([801924b](https://gitlab.com/fi-sas/fisas-volunteering/commit/801924b4d315264f3b0d66f2a5ce1894c031a5af))

## [1.13.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.13.2...v1.13.3) (2021-09-09)


### Bug Fixes

* **experiences:** remove error endpoint dashboard ([a248597](https://gitlab.com/fi-sas/fisas-volunteering/commit/a248597f304d7f947b647f556b7293e28a4734ce))

## [1.13.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.13.1...v1.13.2) (2021-08-13)


### Bug Fixes

* **application:** add validation is_employee ([f77e716](https://gitlab.com/fi-sas/fisas-volunteering/commit/f77e7160c89778f2bfa28b8eac8c54a2f6324344))
* **applications:** add validation convert ([b87afee](https://gitlab.com/fi-sas/fisas-volunteering/commit/b87afee3db9a6705f0bae5cc3314a72406586d12))

## [1.13.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.13.0...v1.13.1) (2021-07-27)


### Bug Fixes

* **applications:** add validation is student ([100d93f](https://gitlab.com/fi-sas/fisas-volunteering/commit/100d93fccd4376df8faeff013526cd24886df4c3))

# [1.13.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.12.4...v1.13.0) (2021-07-13)


### Bug Fixes

* **absence_reason:** add field reject_reason, accept nullable, relation and validations ([ab63907](https://gitlab.com/fi-sas/fisas-volunteering/commit/ab639079051fa8bc8f06a2a319014d855a3ca07d))
* **experiences:** add validations, and number_interests in list of responsable ([dbed8f6](https://gitlab.com/fi-sas/fisas-volunteering/commit/dbed8f6e1d38dad2d8dc1a2e75eab03c58559204))
* **monthly-reports:** file_id optiona, add create validation ([f15ac54](https://gitlab.com/fi-sas/fisas-volunteering/commit/f15ac542f9ada6ef974702c07499b5e3bcf780bd))


### Features

* **experience_user_interests:** add count interests by experience_id ([1f746c9](https://gitlab.com/fi-sas/fisas-volunteering/commit/1f746c93136f79c9249bd789af131c4898d613b8))
* **migration:** update table monthly_report ([d6775e6](https://gitlab.com/fi-sas/fisas-volunteering/commit/d6775e63e5cfa1f1bd4cef32cdb6b5c6c262721d))
* **migrations:** update table absence_reason ([f31c8e4](https://gitlab.com/fi-sas/fisas-volunteering/commit/f31c8e47c795872e2a1a4457f01b7bb06a2be3a9))

## [1.12.4](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.12.3...v1.12.4) (2021-07-08)


### Bug Fixes

* **applications:** add validations on get application ([75b15f3](https://gitlab.com/fi-sas/fisas-volunteering/commit/75b15f31e4f06f4457a93b89adbd9d2e39547442))
* **experience_user_interests:** fix return in get ([adf8e6f](https://gitlab.com/fi-sas/fisas-volunteering/commit/adf8e6fa0a4959dff510c02af4cdbbb340ee28bd))

## [1.12.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.12.2...v1.12.3) (2021-07-07)


### Bug Fixes

* **experiences:** remove status from list ([4e3f0eb](https://gitlab.com/fi-sas/fisas-volunteering/commit/4e3f0ebb5af02577c0f24271cf4b23373a3a8b32))

## [1.12.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.12.1...v1.12.2) (2021-07-06)


### Bug Fixes

* **experience_user_interests:** new scopes ([0889520](https://gitlab.com/fi-sas/fisas-volunteering/commit/088952079db340fd5c3bfb2a7d4556efa7d113a7))
* **experiences:**  add validation start and end date ([0912a77](https://gitlab.com/fi-sas/fisas-volunteering/commit/0912a77cdb36bf807360f539b8a5eb053e066d3f))

## [1.12.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.12.0...v1.12.1) (2021-07-05)


### Bug Fixes

* **applicationHistory:** omit user info ([9bb19ca](https://gitlab.com/fi-sas/fisas-volunteering/commit/9bb19ca887aa8c55fd736b293c13a455733eef80))
* **applications:** omit user info ([edb0ea0](https://gitlab.com/fi-sas/fisas-volunteering/commit/edb0ea0f4cf7e722ed36508dfa34c18e5275a717))
* **attendances:** list, get, validate, create validate access ([2c2a234](https://gitlab.com/fi-sas/fisas-volunteering/commit/2c2a234b81f9b45a46f319ddb6fe011608f28105))
* **attendances:** type error access ([570aba1](https://gitlab.com/fi-sas/fisas-volunteering/commit/570aba15244aac275879f546c6f633b7d0309565))
* **complain_applications:** omit user info ([9230cda](https://gitlab.com/fi-sas/fisas-volunteering/commit/9230cda3fcd2f3b816a8195ff5559d829afa37c9))
* **complain_experiences:** omit user info ([72b3975](https://gitlab.com/fi-sas/fisas-volunteering/commit/72b397502a1a608f3b70d19b34c525162c68c432))
* **complain_user_intertest:** omit user info ([b5639f1](https://gitlab.com/fi-sas/fisas-volunteering/commit/b5639f1da867426f5bf093df6ce62a1ef0700476))
* **experience_history:** omit user info ([a8461d3](https://gitlab.com/fi-sas/fisas-volunteering/commit/a8461d37717a9027cbd082f8a60f5d5a62aacc6f))
* **experience_user_interests:** type error access ([c7e74ea](https://gitlab.com/fi-sas/fisas-volunteering/commit/c7e74eade5c8afc33c051657bdf78e00bb766821))
* **experience_user_interests_historic:** omit user info ([79f0d5c](https://gitlab.com/fi-sas/fisas-volunteering/commit/79f0d5c2fc1cf26ce25e59d4e25858d5f6771a73))
* **experiences:** type error access ([b09dd05](https://gitlab.com/fi-sas/fisas-volunteering/commit/b09dd05aa0dd8b4a6c8294c294f7bd9192957dd1))
* **experiences:** validate access and omit info user ([454f432](https://gitlab.com/fi-sas/fisas-volunteering/commit/454f432729ea5148ed8ce2a32eda63d4d58d223a))
* **external_entities:** omit user info ([3f3d4de](https://gitlab.com/fi-sas/fisas-volunteering/commit/3f3d4de4b1aa684f7fd551db57cc302ffc4de8ed))
* **general-complain:** omit user info ([e4d8c85](https://gitlab.com/fi-sas/fisas-volunteering/commit/e4d8c85e171fad8d630eb232ee96968024a7d581))
* **monthly-reports:** list and get validate acces ([5d671bb](https://gitlab.com/fi-sas/fisas-volunteering/commit/5d671bb49c6a49e5eafed8904e974c477881f8b7))
* **monthly-reports:** type error access ([121f5dd](https://gitlab.com/fi-sas/fisas-volunteering/commit/121f5dd57a11e0e8954be951bba696886011c51f))
* **package:** update core ([4a91684](https://gitlab.com/fi-sas/fisas-volunteering/commit/4a916842755dfcc633f6db7e0a110a0016f9da6b))

# [1.12.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.11.0...v1.12.0) (2021-07-02)


### Bug Fixes

* **experience_user_interest:** remove advisor ([f3ed0a4](https://gitlab.com/fi-sas/fisas-volunteering/commit/f3ed0a468f28a18e5451d9e3085f3f3ad5d10811))
* **experiences:** remove advisor id ([81b8fec](https://gitlab.com/fi-sas/fisas-volunteering/commit/81b8fec67ee57338851e8073fd5b562e0c376269))
* **monthly-reports:** remover experience_advisor ([7f212ed](https://gitlab.com/fi-sas/fisas-volunteering/commit/7f212ed2190ebeb9cbb39049039b193af678fa4d))


### Features

* **experiences:** add logged user is responsable in create ([70f6973](https://gitlab.com/fi-sas/fisas-volunteering/commit/70f69735de6a7f4f4fe11e1bd1c720a7d767934e))
* **migrations:** alter table experience remove experience_advisor_id ([8d694bd](https://gitlab.com/fi-sas/fisas-volunteering/commit/8d694bd5c20a6efe006f2348af2e919936e62640))

# [1.11.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.10.0...v1.11.0) (2021-07-01)


### Features

* **experiences:** add new flow status colaboration ([524b637](https://gitlab.com/fi-sas/fisas-volunteering/commit/524b637347859e12d6d8a77e459530fd117e2976))

# [1.10.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.9.2...v1.10.0) (2021-07-01)


### Bug Fixes

* **applications:** rename scopes ([96071ef](https://gitlab.com/fi-sas/fisas-volunteering/commit/96071efe3a1e3c8eb48055f7192d223b31b69033))
* **experience_user_interest:** rename scopes ([29789d5](https://gitlab.com/fi-sas/fisas-volunteering/commit/29789d56814ecd814bb91bb41f8587cfc23adef2))
* **experiences:** rename scopes ([db6b902](https://gitlab.com/fi-sas/fisas-volunteering/commit/db6b902769b9e556c7b0ca45b72f0688212f20ff))


### Features

* **applications:** add flow for cancel application ([1bee6e4](https://gitlab.com/fi-sas/fisas-volunteering/commit/1bee6e4cccff2ea73af3ca306ca6b68db95ac5ac))
* **migration:** alter check last status ([47ed8d8](https://gitlab.com/fi-sas/fisas-volunteering/commit/47ed8d891d0bb12b9cc8f8da91e40cb5bb5a5add))

## [1.9.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.9.1...v1.9.2) (2021-06-30)


### Bug Fixes

* **experiences:** update endpoint dashboard ([83fa3cc](https://gitlab.com/fi-sas/fisas-volunteering/commit/83fa3cc720b6e31b84cd26c08b269715ca91f7c4))

## [1.9.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.9.0...v1.9.1) (2021-06-29)


### Bug Fixes

* **experience_user_interests_historic:** add default relation ([ad40a94](https://gitlab.com/fi-sas/fisas-volunteering/commit/ad40a94209246aa5f91ffe70d0a02b1374260fa4))

# [1.9.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.8.4...v1.9.0) (2021-06-29)


### Bug Fixes

* **constants:** update constants ([d28970b](https://gitlab.com/fi-sas/fisas-volunteering/commit/d28970b89c2215ec9b4b4876eb596564fd8e41f6))
* **experience_user_interest:** add id cerificate_generated ([2f769b4](https://gitlab.com/fi-sas/fisas-volunteering/commit/2f769b4d9a21688b6402f1f317c22fcf81a00cd3))
* **user_interest_machine:** update status ([e22ca1f](https://gitlab.com/fi-sas/fisas-volunteering/commit/e22ca1f6fc77a95bc2562cff627a2796609a8bd6))


### Features

* **migration:** update status experience_user_interest ([2615659](https://gitlab.com/fi-sas/fisas-volunteering/commit/26156595470be80906110ce8f24e49b189484aa6))
* **migrations:** add fields user_manifest ([169807d](https://gitlab.com/fi-sas/fisas-volunteering/commit/169807d9ebe11842a45971e6cd457e58af7c86a3))
* **migrations:** update status history ([87a717a](https://gitlab.com/fi-sas/fisas-volunteering/commit/87a717a0a7b86f3cc7328636cfde7deb29205841))

## [1.8.4](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.8.3...v1.8.4) (2021-06-25)


### Bug Fixes

* **general_reports:** add function ([004eba1](https://gitlab.com/fi-sas/fisas-volunteering/commit/004eba1948af7dd51d36dc00f31ad092861c21f0))

## [1.8.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.8.2...v1.8.3) (2021-06-25)


### Bug Fixes

* **applications:** optimize withrelateds ([efb0485](https://gitlab.com/fi-sas/fisas-volunteering/commit/efb04853a1102e38f0cdf6402b37c0a8c6ff583f))
* **experiences_user_interest:** optimize withrelateds ([89ecb09](https://gitlab.com/fi-sas/fisas-volunteering/commit/89ecb0958811a72632d20ce91ab30fca1aa7ced7))

## [1.8.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.8.1...v1.8.2) (2021-06-23)


### Bug Fixes

* **general-reports:** update scope ([7563797](https://gitlab.com/fi-sas/fisas-volunteering/commit/7563797ebf2e1981f8ab86bf96e023fca57b54d7))

## [1.8.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.8.0...v1.8.1) (2021-06-23)


### Bug Fixes

* **attendances:** updted executed_service field no required ([eee2393](https://gitlab.com/fi-sas/fisas-volunteering/commit/eee23938b0e07c2cff3e9889670c1165351e4810))

# [1.8.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.7.0...v1.8.0) (2021-06-15)


### Bug Fixes

* **external_entity:** add enpoint update ([77f39f5](https://gitlab.com/fi-sas/fisas-volunteering/commit/77f39f5fb197eeb117ca9c4f624d35a9ab605878))
* **general-report:** update endpoint ([6d8ce18](https://gitlab.com/fi-sas/fisas-volunteering/commit/6d8ce18b62ea11aed917a5245c0cdfcc84476240))


### Features

* **experiences-user-interests:** add list unique users ([6f61651](https://gitlab.com/fi-sas/fisas-volunteering/commit/6f6165164714f7c8733d5db432cc909d6909e949))

# [1.7.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.6.1...v1.7.0) (2021-06-14)


### Bug Fixes

* **experience_user_interests:** update scopes ([dcf3f31](https://gitlab.com/fi-sas/fisas-volunteering/commit/dcf3f31db8a1537b64f646e089c940c06e5533e2))
* **external_entities:** update fro new flux ([e538470](https://gitlab.com/fi-sas/fisas-volunteering/commit/e538470b0f9059ca38352aa51f159611a056058a))


### Features

* **migration:** update external_entity table ([f483384](https://gitlab.com/fi-sas/fisas-volunteering/commit/f483384f8d8e37cb8b5901fc1d1ff063aceb12fc))

## [1.6.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.6.0...v1.6.1) (2021-06-09)


### Bug Fixes

* **applications:** scope name ([78e5257](https://gitlab.com/fi-sas/fisas-volunteering/commit/78e5257326aa559dc3c1bfbdb19c66336bd4688f))

# [1.6.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.5.1...v1.6.0) (2021-06-07)


### Features

* **experiences:** add endpoint report user-selection ([b0a567b](https://gitlab.com/fi-sas/fisas-volunteering/commit/b0a567bf535c47454848ee22967c734082f9ac41))

## [1.5.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.5.0...v1.5.1) (2021-06-01)


### Bug Fixes

* **external_entities:** add actions ([0e96129](https://gitlab.com/fi-sas/fisas-volunteering/commit/0e96129eaae3c8a8b47b614508ef4fb2e1c64ac7))

# [1.5.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.4.1...v1.5.0) (2021-05-31)


### Bug Fixes

* **applications:** fix problemas eslint ([1bd82c7](https://gitlab.com/fi-sas/fisas-volunteering/commit/1bd82c75f87762b76ef7c06b1c38a2a2ab2efaf6))
* **area_interests:** fix problem eslint ([9e3cab4](https://gitlab.com/fi-sas/fisas-volunteering/commit/9e3cab439fce7a59919f95eff026e1dac5c099a0))
* **experience_user_interests:** fix problem eslint ([3b773a3](https://gitlab.com/fi-sas/fisas-volunteering/commit/3b773a3ab081b518da500cfe9cf9bf31100f899e))
* **monthly_reports:** fix problem eslint ([26c53fe](https://gitlab.com/fi-sas/fisas-volunteering/commit/26c53fee9e319763fbf3de19ec82f436d23d1ec9))
* **skills:** fix problem eslint ([5734f32](https://gitlab.com/fi-sas/fisas-volunteering/commit/5734f32e957be17886808ec6cc3fb2e0ab01c912))


### Features

* **external_entities:** add active, addSearchRelation and clearCache ([8e5946b](https://gitlab.com/fi-sas/fisas-volunteering/commit/8e5946b2801505d9fe273119c60ccce2450b0726))

## [1.4.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.4.0...v1.4.1) (2021-05-27)


### Bug Fixes

* **applications:** problem com application-cancel ([61b6bae](https://gitlab.com/fi-sas/fisas-volunteering/commit/61b6bae172367be14fa08af9feef814912342237))
* **experiences:** add const addSearchRelation ([8016e0e](https://gitlab.com/fi-sas/fisas-volunteering/commit/8016e0efa4b45294de41c80c6cdc272c460ff7b0))

# [1.4.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.3.1...v1.4.0) (2021-05-26)


### Bug Fixes

* **experiences:** query ([5fe2b78](https://gitlab.com/fi-sas/fisas-volunteering/commit/5fe2b78740985a5718e8264300ae3e2f74790763))


### Features

* **applications:** add endpoints user-application ([a2ddf94](https://gitlab.com/fi-sas/fisas-volunteering/commit/a2ddf949e208333479b230772984747197ffb2ab))

## [1.3.1](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.3.0...v1.3.1) (2021-05-21)


### Bug Fixes

* **applications:** remove mandatory fields ([fef57d4](https://gitlab.com/fi-sas/fisas-volunteering/commit/fef57d4a6ad342ba03360eecf19be6e3d3fc7d7f))

# [1.3.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.2.0...v1.3.0) (2021-05-06)


### Bug Fixes

* **application-schedules:** add hasOne hasMany ([ce4689d](https://gitlab.com/fi-sas/fisas-volunteering/commit/ce4689d52d68594ed740a10334a14aae1f830458))
* **applicationhistory:** add hasOne hasMany ([d16bbe6](https://gitlab.com/fi-sas/fisas-volunteering/commit/d16bbe608b26a4e051d432e0bb2e854167ee048d))
* **schedules:** remove relation ([6a24858](https://gitlab.com/fi-sas/fisas-volunteering/commit/6a248586d061a4f8baceaaca6a3d80051afa0708))


### Features

* **area_interests:** add translations search ([cd091aa](https://gitlab.com/fi-sas/fisas-volunteering/commit/cd091aaee54fdb796e35dad970f992ab5fbede43))
* **experience_user_interests:** omit info ([0c36fc8](https://gitlab.com/fi-sas/fisas-volunteering/commit/0c36fc8ddf649a9ab810324436310369bfcc59d9))
* **experiences:** add search translations and status published ([fbc6092](https://gitlab.com/fi-sas/fisas-volunteering/commit/fbc6092bd9f5c44768cca072c49c65c4b161e34a))
* **external_entities:** create and inactive ([d1b2365](https://gitlab.com/fi-sas/fisas-volunteering/commit/d1b2365362a176e7a6a73b790917fa9ed17d08db))
* **migrations:** experience flag published ([0acb012](https://gitlab.com/fi-sas/fisas-volunteering/commit/0acb01299344b42afd207a1d818397ed8cfad18e))
* **skills:** search for translations ([9752c92](https://gitlab.com/fi-sas/fisas-volunteering/commit/9752c927e6b607ec3cfcfb661abafdde0bde94d6))

# [1.2.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.1.0...v1.2.0) (2021-04-28)


### Bug Fixes

* **absence_reason:** update fields and validations ([e4464e4](https://gitlab.com/fi-sas/fisas-volunteering/commit/e4464e40229df45844d3fc749a23630242ed25eb))
* **application:** update fields, endpoints ([f67da76](https://gitlab.com/fi-sas/fisas-volunteering/commit/f67da7666fce1a79e5753a8e3424054255926d25))
* **application-schedule:** add relation ([856a36c](https://gitlab.com/fi-sas/fisas-volunteering/commit/856a36ccd6a019861625ae2df041f6fb86874156))
* **applications:** add scopes ([2786e70](https://gitlab.com/fi-sas/fisas-volunteering/commit/2786e70fe17814f2b6a3050241344e58688d9eb0))
* **applications:** create and update validations ([dddd112](https://gitlab.com/fi-sas/fisas-volunteering/commit/dddd112fc99db18e7db139ac2b842d5e827a57a3))
* **attendances:** refactor reference application to user_interest ([5518f79](https://gitlab.com/fi-sas/fisas-volunteering/commit/5518f792bce4236254cf2091a8f7c0f8b055dd1f))
* **complain-applications:** add scopes and relations ([a1fcefc](https://gitlab.com/fi-sas/fisas-volunteering/commit/a1fcefcdce791744b37fa570d2790aeeb26abf24))
* **complain-experiences:** add scopes and relations ([d102a52](https://gitlab.com/fi-sas/fisas-volunteering/commit/d102a52648c03c1e6e7b516044245d347a59b728))
* **complain-user-interest:** fix scopes ([f9aa4da](https://gitlab.com/fi-sas/fisas-volunteering/commit/f9aa4da67921f88a97401327cafc2623c9a9370e))
* **constants:** update status experiences ([e3930a5](https://gitlab.com/fi-sas/fisas-volunteering/commit/e3930a54ecff4ef8107569b4106477b9305fadce))
* **experience_user_interests:** add update experiences ([67752c1](https://gitlab.com/fi-sas/fisas-volunteering/commit/67752c1b0ac4b0529ec63805493841ab9375da85))
* **experience-history:** add user relation ([56119a8](https://gitlab.com/fi-sas/fisas-volunteering/commit/56119a861b38d15c5145d506273c5df580ea21e2))
* **experiences:** add scopes ([a9b066f](https://gitlab.com/fi-sas/fisas-volunteering/commit/a9b066fb1b725c59bf1592db57b45ff81f266816))
* **experiences:** translation service name ([a2c60f9](https://gitlab.com/fi-sas/fisas-volunteering/commit/a2c60f9991ba333f3f465f35c7a26e85fcf7232c))
* **external_intities:** update fields ([203314d](https://gitlab.com/fi-sas/fisas-volunteering/commit/203314d2d9e1e17975d95df0afe9f42c6c5ca698))
* **general-complain:** add relation ([7b7f6eb](https://gitlab.com/fi-sas/fisas-volunteering/commit/7b7f6eb14dd72d9d961fb15c9befd9612251f667))
* **general-reports:** add scopes ([770a936](https://gitlab.com/fi-sas/fisas-volunteering/commit/770a9363ae55c6ab09b7fdd6152966ab36edecee))
* **general-reports:** refactor reference application to user_interest ([dbe08e0](https://gitlab.com/fi-sas/fisas-volunteering/commit/dbe08e04649e17bfb27fc15980003781e55a5206))
* **machine_experience:** remove publish status ([b6c07a2](https://gitlab.com/fi-sas/fisas-volunteering/commit/b6c07a2eb3e5f595c4df73883f9844ef451e534a))
* **monthly-reports:** add scope ([e1f6e79](https://gitlab.com/fi-sas/fisas-volunteering/commit/e1f6e79a8db8c271528b5ce7ee1cc9272d04b739))
* **monthly-reports:** refactor reference application to user_interest ([7f1f8fd](https://gitlab.com/fi-sas/fisas-volunteering/commit/7f1f8fd3872e67a73bd3796fc772b06a49fc6329))
* **state_machines:** update machine experiences ([5aa56c5](https://gitlab.com/fi-sas/fisas-volunteering/commit/5aa56c5bb063b21829ce98b91dbaeb5ad3e558cd))
* **state-machine-application:** update and correct application machine ([226e31c](https://gitlab.com/fi-sas/fisas-volunteering/commit/226e31c1f354e467863a7720fece4a0196ba05b1))
* **utils_constants:** update contants application ([3df15dc](https://gitlab.com/fi-sas/fisas-volunteering/commit/3df15dcd8ef3850cfd13c7b26d49f0338d44d3a9))


### Features

* **application:** add historic applications and colaborations ([4354928](https://gitlab.com/fi-sas/fisas-volunteering/commit/4354928d0f5f8a44bc3086fc69e7daf9441afd5d))
* **application_skills:** management application_skills ([c7ddd41](https://gitlab.com/fi-sas/fisas-volunteering/commit/c7ddd4119a2c7ba70ab5928c7d222f079d2d12fd))
* **application_user_interest:** add management application_user_interest ([0e1b4b7](https://gitlab.com/fi-sas/fisas-volunteering/commit/0e1b4b7542e9494686fd8fd1e857f0ff1cc3df5b))
* **applicationhistory:** add relation ([e60c3a3](https://gitlab.com/fi-sas/fisas-volunteering/commit/e60c3a37aecf33b202e37fc02da98ea5f758dc39))
* **applications:** add filter by user ([6f60614](https://gitlab.com/fi-sas/fisas-volunteering/commit/6f60614278bc4d4e5214c26dc838ee03972389a2))
* **applications:** add new fields and actions ([71f1129](https://gitlab.com/fi-sas/fisas-volunteering/commit/71f1129c6602c987dd62da36107ef4e564afacab))
* **applications:** update fields and add report ([301d047](https://gitlab.com/fi-sas/fisas-volunteering/commit/301d0473e6b73e9e60b67ffb1830002cc276cfa0))
* **area_interest:** add area_interest management ([4c7b0d5](https://gitlab.com/fi-sas/fisas-volunteering/commit/4c7b0d574840ba2677cb151aa1079f78b31bc080))
* **complain_applications:** add service ([0983a14](https://gitlab.com/fi-sas/fisas-volunteering/commit/0983a14f13588261c498a86ccb178b338c4c00c9))
* **complain-user-interest:** add service ([5830b31](https://gitlab.com/fi-sas/fisas-volunteering/commit/5830b31b4bd183395a347c14807d1ed3aaedc64a))
* **experience_user_interest:** add endpoints and new fields ([ca962c8](https://gitlab.com/fi-sas/fisas-volunteering/commit/ca962c88361b167bd9c9a355eb18e9b95f9baddb))
* **experience_user_interest:** add last_status and estatistics ([267a4ee](https://gitlab.com/fi-sas/fisas-volunteering/commit/267a4eeab8ffc282e023fbc2b662dd05730ad24f))
* **experience_user_interests:** add endpoints ([38afc57](https://gitlab.com/fi-sas/fisas-volunteering/commit/38afc57fd83d35998567743dcfb681c47a810f9a))
* **experience_user_interests_historic:** add historic of user interests ([182fc57](https://gitlab.com/fi-sas/fisas-volunteering/commit/182fc57d9ae89011ca88c939f86fac4b9ed49df2))
* **experience-translations:** update file ([306e1ec](https://gitlab.com/fi-sas/fisas-volunteering/commit/306e1ec361f52745a828586e3ac9df70ad02f82f))
* **experiences:** add last_status and estatistics ([170cbfc](https://gitlab.com/fi-sas/fisas-volunteering/commit/170cbfcb8ee73c68224b0ddaddcff8449036c98d))
* **experiences:** add notifications, user_interests ([c1799e6](https://gitlab.com/fi-sas/fisas-volunteering/commit/c1799e684694f2115cee61564a8487965c305628))
* **experiences:** add return reason ([58c1011](https://gitlab.com/fi-sas/fisas-volunteering/commit/58c1011262cb7c887c786133678442fa586e9450))
* **experiences:** add uuid field ([e9e9fad](https://gitlab.com/fi-sas/fisas-volunteering/commit/e9e9fad03d826381c0782e34425b0e04545f299a))
* **experiences:** remove fiels, relations, add change status by user_manifest ([e4d66db](https://gitlab.com/fi-sas/fisas-volunteering/commit/e4d66db0934457dbef129a0286dd18f6d8cfeb3d))
* **external_entitie:** add new fields ([acc7ce9](https://gitlab.com/fi-sas/fisas-volunteering/commit/acc7ce92b139174112ee9355a7315c999617774b))
* **general_complain:** add new service ([e06ac9f](https://gitlab.com/fi-sas/fisas-volunteering/commit/e06ac9f0c2578c5010ba308c4f55c6490a390650))
* **machine:** add state machie user interest ([ed9dd18](https://gitlab.com/fi-sas/fisas-volunteering/commit/ed9dd18033f4161455ca5457b561ff0e9e8cf6b2))
* **machine:** application add last_status ([57d1d61](https://gitlab.com/fi-sas/fisas-volunteering/commit/57d1d61ccf1c1bd139cbcc530470bcc63eb91f99))
* **machine:** experiences add last_status ([01e17f2](https://gitlab.com/fi-sas/fisas-volunteering/commit/01e17f239c30a94c3d6991fad5477ee71d5dcc49))
* **machine:** user-interests add last_status ([c532f40](https://gitlab.com/fi-sas/fisas-volunteering/commit/c532f40a09942efbb9680a815963794d10a42f41))
* **migration:** add complain tables ([8d5b4db](https://gitlab.com/fi-sas/fisas-volunteering/commit/8d5b4dbeae893adc818d9b36170a19c6d6fd271d))
* **migration:** add table user-interest-complain ([8e25fdb](https://gitlab.com/fi-sas/fisas-volunteering/commit/8e25fdb1c382e30e750666d647295cdcb5e54c58))
* **migration:** experiences, applications, user_interests add last_status ([f92bad9](https://gitlab.com/fi-sas/fisas-volunteering/commit/f92bad962a0adf102f51e1103d9bc4272df18d23))
* **migration:** migration external_entitie ([97b5599](https://gitlab.com/fi-sas/fisas-volunteering/commit/97b559975130d3bb3a31d615557a6cc7f4965ef8))
* **migration:** update table experience and application ([b108d42](https://gitlab.com/fi-sas/fisas-volunteering/commit/b108d426e0c2b444abc89f59ddc60669488b4f39))
* **migration:** update table, experience, user_interest, user_interest_historyc ([5c01ff7](https://gitlab.com/fi-sas/fisas-volunteering/commit/5c01ff788c0433e9ea09c70a40d299357498e9df))
* **migration:** update tables, application_attendances, absence_reason, application_monthly_reports ([526645b](https://gitlab.com/fi-sas/fisas-volunteering/commit/526645b6696f6ef4e3a7f39f1c89f697d9fc4f29))
* **migrations:** add migration skills and area_interest ([cf271ee](https://gitlab.com/fi-sas/fisas-volunteering/commit/cf271ee8d4073d63f939b4883f203d5b45dff22b))
* **migrations:** update status applications ([b14da06](https://gitlab.com/fi-sas/fisas-volunteering/commit/b14da06af806cad4a17ea0fa2ac0284af1460677))
* **migrations:** update status experience ([0ea560e](https://gitlab.com/fi-sas/fisas-volunteering/commit/0ea560e1e1c1a41e505da772de4a1aee819def00))
* **package:** update fisas core ([4e7a366](https://gitlab.com/fi-sas/fisas-volunteering/commit/4e7a3669433214e98c4bc9a3d525f945e7388ed2))
* **skill:** add skills management ([e5ebc59](https://gitlab.com/fi-sas/fisas-volunteering/commit/e5ebc5928bfe92818ee38fb52fddcc8e59486711))
* **translations:** add clear cache ([bc71e9f](https://gitlab.com/fi-sas/fisas-volunteering/commit/bc71e9fb0017aff8742832f7c90bd9a711ac2bbc))
* **utils:** add events and status user_interest ([a3b4d5c](https://gitlab.com/fi-sas/fisas-volunteering/commit/a3b4d5ca63e1210a47f99eaa2e75faa42cbee35d))

# [1.1.0](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.0.0...v1.1.0) (2021-03-09)


### Bug Fixes

* **experiences:** add empty validation to webpage widget endpoint ([1aa1c49](https://gitlab.com/fi-sas/fisas-volunteering/commit/1aa1c49f3e4390c2049a3f5abad8e0ce7d3f5d8c))


### Features

* **experiences:** add endpoint for WP widget information ([398fdd5](https://gitlab.com/fi-sas/fisas-volunteering/commit/398fdd5a0baaba7a848c29dbf75c326b3513ad09))

# 1.0.0 (2021-03-09)


### Bug Fixes

* **application:** missing with-related login ([a1341c1](https://gitlab.com/fi-sas/fisas-volunteering/commit/a1341c1cb185ac87bda187684b457db3522e91c4))
* **applications:** change language_skills type to JSON ([f49ffa3](https://gitlab.com/fi-sas/fisas-volunteering/commit/f49ffa3f9086287796383c3f5803b14270f134cd))
* **general-reports:** fixing errors ([4088c83](https://gitlab.com/fi-sas/fisas-volunteering/commit/4088c83c08cc14f6528fcde9239e3951e2335903))
* remove required from experience_id on application ([2b08284](https://gitlab.com/fi-sas/fisas-volunteering/commit/2b08284969933bb6e79e8a8454f0bdac913a25df))
* state machines and minor fixs ([ade33b2](https://gitlab.com/fi-sas/fisas-volunteering/commit/ade33b237bd904d64d7d66876c550e2b56b231b8))


### Features

* create external entites from BO volunteering ([0071dc4](https://gitlab.com/fi-sas/fisas-volunteering/commit/0071dc40df4587ab64a935a6031fa3b5fd303d91))
* migrate MS Volunteering ([bac28bd](https://gitlab.com/fi-sas/fisas-volunteering/commit/bac28bdf0049ad64dd0c929b9d4a87a5c12127d7))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-02-09)


### Bug Fixes

* **applications:** change language_skills type to JSON ([f49ffa3](https://gitlab.com/fi-sas/fisas-volunteering/commit/f49ffa3f9086287796383c3f5803b14270f134cd))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas-volunteering/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-02-09)


### Bug Fixes

* **application:** missing with-related login ([a1341c1](https://gitlab.com/fi-sas/fisas-volunteering/commit/a1341c1cb185ac87bda187684b457db3522e91c4))
* **general-reports:** fixing errors ([4088c83](https://gitlab.com/fi-sas/fisas-volunteering/commit/4088c83c08cc14f6528fcde9239e3951e2335903))
