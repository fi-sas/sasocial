module.exports.up = async db => db.schema
	.createTable("activity", (table) => {
		table.increments();
		table.string("name", 255).notNullable();
		table.boolean("active").notNullable();
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})

	// experience
	.createTable("experience", (table) => {
		table.increments();
		table.datetime("publish_date").notNullable();
		table.datetime("application_deadline_date").notNullable();
		table.string("academic_year", 255).notNullable();
		table.integer("organic_unit_id").notNullable();

		table.string("address", 255).notNullable();
		table.integer("experience_responsible_id").notNullable();
		table.integer("experience_advisor_id").notNullable();


		table.integer("number_candidates").nullable();
		table.integer("number_simultaneous_candidates").nullable();

		table.datetime("start_date").notNullable();
		table.datetime("end_date").notNullable();
		table.float("number_weekly_hours").notNullable();
		table.float("total_hours_estimation").notNullable();
		table.json("schedule").notNullable();
		table.boolean("holydays_availability").notNullable().defaultTo(false);
		table.integer("attachment_file_id");
		table.integer("contract_file_id");
		table.integer("certificate_file_id");

		table.enum("status", ["SUBMITTED", "CONFIRMED", "CHANGED", "APPROVED", "PUBLISHED", "REJECTED", "CANCELLED", "CLOSED"]).notNullable();
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();


	})
	.createTable("translation", (table) => {
		table.increments();
		table.string("title", 255).notNullable();
		table.string("proponent_service", 255).notNullable();
		table.string("applicant_profile", 255).notNullable();
		table.string("job", 255).notNullable();
		table.string("description", 255).notNullable();
		table.string("selection_criteria", 255).notNullable();
		table.integer("language_id").unsigned().notNullable();
		table.integer("experience_id").unsigned().notNullable().references("id").inTable("experience");
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})

	// application
	.createTable("application", (table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.string("academic_year", 255).notNullable();
		table.string("course_year", 255).notNullable();
		table.integer("school_id ").nullable();
		table.integer("course_degree_id ").nullable();
		table.integer("course_id ").nullable();
		table.boolean("has_collaborated_last_year").notNullable();
		table.string("previous_activity_description", 255).nullable();
		table.boolean("has_holydays_availability").notNullable();
		table.boolean("has_profissional_experience").notNullable();
		table.boolean("foreign_languages").notNullable();
		table.specificType("language_skills", " VARCHAR(25)[]");
		table.string("type_of_company", 255).nullable();
		table.string("job_at_company", 255).nullable();
		table.string("job_description", 255).nullable();
		table.text("observations ");
		table.string("status", ["SUBMITTED", "WAITING", "ANALYSED", "INTERVIEWED", "ACCEPTED", "ACKNOWLEDGED", "PUBLISHED", "WITHDRAWAL", "DECLINED", "CANCELLED", "CLOSED"]).notNullable();
		table.string("notification_guid", 36);
		table.unique("notification_guid");
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
		table.integer("experience_id").unsigned().references("id").inTable("experience");

	})

	// application status history
	.createTable("experience_history", (table) => {
		table.increments();
		table.integer("experience_id").unsigned().references("id").inTable("experience");
		table.string("status").notNullable();
		table.integer("user_id").unsigned();
		table.string("notes");
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
	})

	// application status history
	.createTable("application_history", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application");
		table.string("status").notNullable();
		table.integer("user_id").unsigned();
		table.string("notes");
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
	})

	// application_attendance
	.createTable("application_attendance", (table) => {
		table.increments();
		table.integer("application_id").notNullable().unsigned().references("id")
			.inTable("application");
		table.datetime("date").notNullable();
		table.boolean("was_present").notNullable();
		table.text("notes");
		table.float("n_hours").notNullable().defaultTo(0);
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
	})

	// application_organic_unit
	.createTable("application_organic_unit", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application");
		table.integer("organic_unit_id").unsigned().notNullable();
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
		table.unique(["application_id", "organic_unit_id"]);
	})

	// application_activity
	.createTable("application_activity", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application");
		table.integer("activity_id").unsigned().references("id").inTable("activity");
		table.unique(["application_id", "activity_id"]);
	})

	// application_monthly_report
	.createTable("application_monthly_report", (table) => {
		table.increments();
		table.integer("application_id").notNullable().unsigned().references("id")
			.inTable("application");
		table.integer("month").notNullable();
		table.integer("year").notNullable();
		table.text("report").notNullable();
		table.datetime("created_at").notNullable();
		table.datetime("updated_at").notNullable();
	})
	// schedule
	.createTable("schedule", (table) => {
		table.increments();
		table.enum("day_week", ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"]).notNullable();
		table.enum("day_period", ["MORNING", "AFTERNOON", "NIGHT"]).notNullable();
		table.unique(["day_week", "day_period"]);
	})

	// application_schedule
	.createTable("application_schedule", (table) => {
		table.increments();
		table.integer("application_id").unsigned().references("id").inTable("application");
		table.integer("schedule_id").unsigned().references("id").inTable("schedule");
		table.unique(["application_id", "schedule_id"]);
	})

	// absence_reason
	.createTable("absence_reason", (table) => {
		table.increments();
		table.integer("application_attendance_id").unsigned().references("id").inTable("application_attendance");
		table.datetime("start_date").notNullable();
		table.datetime("end_date").notNullable();
		table.string("reason");
		table.integer("attachment_file_id");
		table.boolean("accept");
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
		table.unique("application_attendance_id");
	})

	// experience_user_interest
	.createTable("experience_user_interest", (table) => {
		table.increments();
		table.integer("experience_id").unsigned().references("id").inTable("experience");
		table.integer("user_id").unsigned();
		table.boolean("is_interest").notNullable().defaultTo(true);
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
	})
	// configuration
	.createTable("configuration", (table) => {
		table.increments();
		table.string("key", 120).notNullable();
		table.string("value", 250).notNullable();
		table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
		table.unique("key");
	})
	;




module.exports.down = async db => db.schema
	.dropTable("application_history")
	.dropTable("experience_history")
	.dropTable("application")
	.dropTable("experience")
	.dropTable("activity")
	.dropTable("application_monthly_report")
	.dropTable("application_attendance")
	.dropTable("translation")
	.dropTable("configuration");




module.exports.configuration = { transaction: true };
