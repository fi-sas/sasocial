module.exports.up = async db =>

	await db.schema.alterTable("application_organic_unit", (table) => {
		table.dropColumn("created_at");
		table.dropColumn("updated_at");
	});

module.exports.down = async db =>

	db.schema.alterTable("application_organic_unit", (table) => {
		table.date("created_at");
		table.date("updated_at");
	});

module.exports.configuration = { transaction: true };
