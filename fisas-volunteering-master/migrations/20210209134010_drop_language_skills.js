module.exports.up = async db =>

	await db.schema.alterTable("application", (table) => {
		table.dropColumn("language_skills");
	});

module.exports.down = async db =>

	db.schema.alterTable("application", (table) => {
		table.specificType("language_skills", " VARCHAR(25)[]");
	});

module.exports.configuration = { transaction: true };

