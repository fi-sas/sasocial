module.exports.up = async db =>

	await db.schema.alterTable("application", (table) => {
        table.string("course_year", 255).nullable().alter();
		table.string("name").notNullable();
        table.string("address").notNullable();
        table.string("nationality").notNullable();
        table.string("genre", ["M", "F", "O"]).notNullable();
        table.integer("student_number").nullable();
        table.integer("employee_number").nullable();
	})
    .createTable("skill", (table) => {
        table.increments();
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    })
    .createTable("skill_translation", (table) => {
        table.increments();
        table.string("description").notNullable();
        table.integer("skill_id").unsigned().notNullable().references("id").inTable("skill");
        table.integer("language_id").unsigned().notNullable();
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    })
    .createTable("area_interest", (table) => {
        table.increments();
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    })
    .createTable("area_interest_translation", (table) => {
        table.increments();
        table.string("description").notNullable();
        table.integer("area_interest_id").unsigned().notNullable().references("id").inTable("area_interest");
        table.integer("language_id").unsigned().notNullable();
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    })
    .createTable("application_skills", (table) => {
        table.increments();
        table.integer("application_id").unsigned().notNullable().references("id").inTable("application");
        table.integer("skill_id").unsigned().nullable().references("id").inTable("skill");
        table.string("other_skill").nullable();
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    })
    .createTable("application_area_interest", (table) => {
        table.increments();
        table.integer("application_id").unsigned().notNullable().references("id").inTable("application");
        table.integer("area_interest_id").unsigned().notNullable().references("id").inTable("area_interest");
        table.datetime("updated_at").notNullable();
		table.datetime("created_at").notNullable();
    });

module.exports.down = async db =>

	db.schema.alterTable("application", (table) => {
        table.string("course_year", 255).notNullable().alter();
		table.dropColumn("name");
        table.dropColumn("address");
        table.dropColumn("nationality");
        table.dropColumn("genre");
        table.dropColumn("student_number");
        table.dropColumn("employee_number");
	})
    .dropTable("skill")
    .dropTable("skill_translation")
    .dropTable("area_interest")
    .dropTable("area_interest_translation")
    .dropTable("application_skills")
    .dropTable("application_area_interest");

module.exports.configuration = { transaction: true };
