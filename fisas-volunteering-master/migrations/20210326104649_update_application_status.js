module.exports.up = async db =>

	db.schema.alterTable("application", (table) => {
        table.string("status", ["SUBMITTED", "ANALYSED", "ACCEPTED", "DECLINED", "EXPIRED", "CANCELLED", "CLOSED"]).notNullable().alter();
		table.dropColumn("experience_id");
	});
    

module.exports.down = async db =>

	db.schema.alterTable("application", (table) => {
        table.string("status", ["SUBMITTED", "WAITING", "ANALYSED", "INTERVIEWED", "ACCEPTED", "ACKNOWLEDGED", "PUBLISHED", "WITHDRAWAL", "DECLINED", "CANCELLED", "CLOSED"]).notNullable().alter();
		table.integer("experience_id").unsigned().references("id").inTable("experience");
	});

module.exports.configuration = { transaction: true };
