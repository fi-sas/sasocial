module.exports.up = async db =>

	db.schema
        .alterTable("experience_user_interest", (table) => {
        table.dropColumn("is_interest");
        table.integer("certificate_file_id").unsigned().nullable();
        table.string("student_avaliation").nullable();
        table.integer("student_file_avaliation").nullable();
        table.string("uuid").nullable();
        table.string("status", ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "DECLINED", "CANCELLED", "ACCEPTED", "NOT_SELECTED", "COLABORATION", "WITHDRAWAL", "CLOSED"]).notNullable();
        table.integer("report_avaliation_file_id").unsigned().nullable();
        table.string("report_avaliation").nullable();
	})
        .createTable("user_interest_history", (table) => {
                table.increments();
                table.integer("user_interest_id").unsigned().references("id").inTable("experience_user_interest");
                table.enum("status", ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "DECLINED", "CANCELLED", "ACCEPTED", "NOT_SELECTED", "COLABORATION", "WITHDRAWAL", "CLOSED"]).notNullable();
                table.integer("user_id").unsigned().notNullable();
                table.string("notes").nullable();
                table.datetime("created_at").notNullable();
                table.datetime("updated_at").notNullable();
        })
        .alterTable("experience", (table) => {
                table.string("uuid").nullable();
                table.string("return_reason").nullable();
        });
    

module.exports.down = async db =>

	db.schema.alterTable("experience_user_interest", (table) => {
        table.boolean("is_interest").notNullable().defaultTo(true);
        table.dropColumn("certificate_file_id");
        table.dropColumn("student_avaliation");
        table.dropColumn("student_file_avaliation");
        table.dropColumn("uuid");
        table.dropColumn("status");
        table.dropColumn("report_avaliation_file_id");
        table.dropColumn("report_avaliation");
	})
        .dropTable("user_interest_history")
        .alterTable("experience", (table) => {
         table.dropColumn("uuid");
         table.dropColumn("return_reason");
        });

module.exports.configuration = { transaction: true };
