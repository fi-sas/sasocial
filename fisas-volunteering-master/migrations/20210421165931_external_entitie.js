
module.exports.up = async db =>

db.schema
.createTable("external_entity", (table) => {
    table.increments();
    table.integer("user_id").unsigned().notNullable();
    table.integer("file_id").unsigned().nullable();
    table.string("function_description").notNullable();
    table.string("description").notNullable();
});
module.exports.down = async db =>
db.schema
.dropTable("external_entity")

module.exports.configuration = { transaction: true };
