
module.exports.up = async db =>
    db.schema
        .alterTable("application_attendance", (table) => {
            table.dropColumn("application_id");
            table.integer("user_interest_id").notNullable().unsigned().references("id")
                .inTable("experience_user_interest");
            table.enum("status", ["PENDING", "ACCEPTED", "REJECTED", "LACK"]).notNullable();
            table.integer("file_id").nullable();
            table.float("n_hours_reject").nullable();
            table.string("reject_reason").nullable();
            table.string("executed_service").nullable();
            table.datetime("initial_time");
            table.datetime("final_time");
        })
        .renameTable("application_attendance", "experience_attendance")
        .alterTable("absence_reason", (table) => {
            table.dropColumn("application_attendance_id");
            table.integer("experience_attendance_id").unsigned().references("id").inTable("experience_attendance");
        })
        .alterTable("application_monthly_report", (table) => {
            table.dropColumn("application_id");
            table.integer("file_id").unsigned().notNullable();
            table.integer("user_interest_id").notNullable().unsigned().references("id")
            .inTable("experience_user_interest");
        })
        .renameTable("application_monthly_report", "experience_monthly_report");

module.exports.down = async db =>
    db.schema
    .renameTable("experience_attendance", "application_attendance")
    .alterTable("application_attendance", (table) => {
        table.integer("application_id").notNullable().unsigned().references("id")
        .inTable("application");
        table.dropColumn("user_interest_id");
        table.dropColumn("status");
        table.dropColumn("file_id");
        table.dropColumn("n_hours_reject");
        table.dropColumn("reject_reason");
        table.dropColumn("executed_service");
        table.dropColumn("initial_time");
        table.dropColumn("final_time");
    })
    .alterTable("absence_reason", (table) => {
        table.dropColumn("experience_attendance_id");
        table.integer("application_attendance_id").unsigned().references("id").inTable("application_attendance");
    })
    .renameTable("experience_monthly_report", "application_monthly_report")
    .alterTable("application_monthly_report", (table) => {
        table.dropColumn("user_interest_id");
        table.dropColumn("file_id");
        table.integer("application_id").notNullable().unsigned().references("id")
			.inTable("application");
    });

    module.exports.configuration = { transaction: true };