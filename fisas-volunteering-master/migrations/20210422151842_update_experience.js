module.exports.up = async db =>
	db.schema
        .alterTable("experience", (table) => {
        table.dropColumn("contract_file_id");
        table.dropColumn("certificate_file_id");
	})
    .alterTable("application", (table) => {
        table.string("code_postal").notNullable();
        table.string("location").notNullable();
        table.date("birthdate").notNullable();
        table.string("tin").notNullable();
        table.string("email").notNullable();
        table.string("mobile_phone").nullable();
        table.string("phone").nullable();
        table.string("identification_number").notNullable();
        table.string("student_number").alter();
        table.string("employee_number").alter();
    });


module.exports.down = async db =>
	db.schema.alterTable("experience", (table) => {
        table.integer("contract_file_id");
		table.integer("certificate_file_id");
	})
    .alterTable("application", (table) => {
        table.dropColumn("code_postal");
        table.dropColumn("location");
        table.dropColumn("birthdate");
        table.dropColumn("tin");
        table.dropColumn("email");
        table.dropColumn("mobile_phone");
        table.dropColumn("phone");
        table.dropColumn("identification_number");
        table.integer("student_number").alter();
        table.integer("employee_number").alter();
    })

module.exports.configuration = { transaction: true };
