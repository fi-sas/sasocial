
module.exports.up = async db =>
    db.schema
        .createTable("complain_application", (table) => {
            table.increments();
            table.integer("application_id").unsigned().notNullable().references("id").inTable("application");
            table.string("complain").notNullable();
            table.integer("file_id").unsigned().nullable();
            table.string("response").nullable();
            table.integer("user_id").unsigned().notNullable();
            table.enum("status", ["SUBMITTED", "ANALYSED", "REPLYED"]);
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("complain_experience", (table) => {
            table.increments();
            table.integer("experience_id").unsigned().notNullable().references("id").inTable("experience");
            table.string("complain").notNullable();
            table.integer("file_id").unsigned().nullable();
            table.string("response").nullable();
            table.integer("user_id").unsigned().notNullable();
            table.enum("status", ["SUBMITTED", "ANALYSED", "REPLYED"]);
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("general-complain", (table) => {
            table.increments();
            table.string("complain").notNullable();
            table.integer("file_id").unsigned().nullable();
            table.string("response").nullable();
            table.integer("user_id").unsigned().notNullable();
            table.enum("status", ["SUBMITTED", "ANALYSED", "REPLYED"]);
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        });

module.exports.down = async db =>
    db.schema
    .dropTable("complain_application")
    .dropTable("complain_experience")
    .dropTable("general-complain");

module.exports.configuration = { transaction: true };
