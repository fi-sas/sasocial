
module.exports.up = async db =>
db.schema
    .alterTable("application", (table) => {
       table.enum("last_status", ["SUBMITTED", "ANALYSED", "ACCEPTED", "DECLINED", "EXPIRED", "CANCELLED", "CLOSED"]).nullable();
    })
    .alterTable("experience", (table) => {
        table.enum("last_status", ["SUBMITTED", "RETURNED", "ANALYSED", "APPROVED", "PUBLISHED", "REJECTED", "SEND_SEEM", "EXTERNAL_SYSTEM", "SELECTION", "IN_COLABORATION", "CLOSED", "CANCELED"]).nullable();
    })
    .alterTable("experience_user_interest", (table) => {
       table.enum("last_status", ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "DECLINED", "CANCELLED", "ACCEPTED", "NOT_SELECTED", "COLABORATION", "WITHDRAWAL", "CLOSED"]).nullable();
    });

module.exports.down = async db =>
db.schema
.alterTable("application", (table) => {
    table.dropColumn("last_status");
})
.alterTable("experience", (table) => {
    table.dropColumn("last_status");
})
.alterTable("experience_user_interest", (table) => {
    table.dropColumn("last_status");
});

module.exports.configuration = { transaction: true };
