

module.exports.up = async db =>
db.schema
.createTable('complain_user_interest', (table) =>{
    table.increments();
    table.integer("user_interest_id").unsigned().notNullable().references("id").inTable("experience_user_interest");
    table.integer("user_id").unsigned().notNullable();
    table.string("response").nullable();
    table.enum("status", ["SUBMITTED", "ANALYSED", "REPLYED"]);
    table.string("complain").notNullable();
    table.integer("file_id").unsigned().nullable();
    table.datetime("created_at").notNullable();
    table.datetime("updated_at").notNullable();
});

module.exports.down = async db =>
db.schema
.dropTable("complain_user_interest");

module.exports.configuration = { transaction: true };