
exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "user_interest_history"
	  DROP CONSTRAINT "user_interest_history_status_check",
	  ADD CONSTRAINT "user_interest_history_status_check" 
	  CHECK (status IN ('SUBMITTED', 'ANALYSED', 'APPROVED', 'WAITING', 'DECLINED', 'CANCELLED', 'ACCEPTED', 'NOT_SELECTED', 'COLABORATION', 'WITHDRAWAL', 'CLOSED', 'WITHDRAWAL_ACCEPTED'))
	`);
  };

  exports.down = function (knex) {
};
