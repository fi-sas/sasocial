module.exports.up = db =>
db.schema
.alterTable("experience", (table) => {
    table.dropColumn("experience_advisor_id")
})

module.exports.down = db =>
db.schema
.alterTable("experience", (table) => {
    table.integer("experience_advisor_id").notNullable();
})
module.exports.configuration = { transaction: true };