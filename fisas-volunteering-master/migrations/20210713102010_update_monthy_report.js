module.exports.up = db =>
db.schema
.alterTable("experience_monthly_report", (table) => {
    table.integer("file_id").unsigned().nullable().alter();
})

module.exports.down = db =>
db.schema
.alterTable("experience_monthly_report", (table) => {
    table.integer("file_id").unsigned().notNullable().alter();
})
module.exports.configuration = { transaction: true };