module.exports.up = db =>
db.schema
.alterTable("absence_reason", (table) => {
    table.string("reject_reason").nullable();
    table.boolean("accept").nullable().alter();
})

module.exports.down = db =>
db.schema
.alterTable("absence_reason", (table) => {
    table.dropColumn("reject_reason");
    table.boolean("accept").notNullable().alter();
})
module.exports.configuration = { transaction: true };