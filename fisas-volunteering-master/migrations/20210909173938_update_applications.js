module.exports.up = db =>
db.schema
.alterTable("application", (table) => {
    table.string("student_number", 255).nullable().alter();
    table.dropColumn("employee_number");
})
module.exports.down = db =>
db.schema
.alterTable("application", (table) => {
    table.integer("student_number").nullable();
    table.integer("employee_number").nullable();
})
module.exports.configuration = { transaction: true };