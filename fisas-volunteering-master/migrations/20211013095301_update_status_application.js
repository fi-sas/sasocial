
module.exports.up = async db =>

db.schema.alterTable("application", (table) => {
    table.string("status", ["SUBMITTED", "ANALYSED", "ACCEPTED", "DECLINED", "EXPIRED", "CANCELLED", "CLOSED", "DISPATCH"]).notNullable().alter();
});


module.exports.down = async db =>

db.schema.alterTable("application", (table) => {
    table.string("status", ["SUBMITTED", "ANALYSED", "ACCEPTED", "DECLINED", "EXPIRED", "CANCELLED", "CLOSED"]).notNullable().alter();
});

module.exports.configuration = { transaction: true };
