exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "application"
	  DROP CONSTRAINT "application_last_status_check",
	  ADD CONSTRAINT "application_last_status_check" 
	  CHECK (status IN ('SUBMITTED', 'ANALYSED', 'ACCEPTED', 'DECLINED', 'EXPIRED', 'CANCELLED', 'CLOSED', 'DISPATCH', NULL))
	`);
  };

  exports.down = function (knex) {
};

