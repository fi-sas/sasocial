
module.exports.up = async db =>

db.schema.alterTable("application", (table) => {
    table.enum("decision", ["ACCEPTED", "REJECTED"]).nullable();
    table.string("reject_reason" ).nullable();
});


module.exports.down = async db =>

db.schema.alterTable("application", (table) => {
    table.dropColumn("decision");
    table.dropColumn("reject_reason");
});

module.exports.configuration = { transaction: true };
