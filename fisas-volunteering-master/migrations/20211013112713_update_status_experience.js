
exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "experience"
	  DROP CONSTRAINT "experience_status_check",
	  ADD CONSTRAINT "experience_status_check" 
	  CHECK (status IN ('SUBMITTED', 'RETURNED', 'ANALYSED', 'APPROVED', 'PUBLISHED', 'REJECTED', 'SEND_SEEM', 'EXTERNAL_SYSTEM', 'SELECTION', 'IN_COLABORATION', 'CLOSED', 'CANCELED', 'DISPATCH'))
	`);
  };

  exports.down = function (knex) {
};
