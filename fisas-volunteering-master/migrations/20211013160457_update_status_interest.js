
module.exports.up = db =>
db.schema
.alterTable("experience_user_interest", (table) => {
    table.string("status", ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "DECLINED", "CANCELLED", "ACCEPTED", "NOT_SELECTED", "COLABORATION", "WITHDRAWAL", "CLOSED", "WITHDRAWAL_ACCEPTED", "DISPATCH"]).notNullable().alter();
})

module.exports.down = db =>
db.schema
.alterTable("experience_user_interest", (table) => {
    table.string("status", ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "DECLINED", "CANCELLED", "ACCEPTED", "NOT_SELECTED", "COLABORATION", "WITHDRAWAL", "CLOSED", "WITHDRAWAL_ACCEPTED"]).notNullable().alter();
})
module.exports.configuration = { transaction: true };