exports.up = function(knex, Promise) {
	return knex.schema.raw(`
	  ALTER TABLE "experience_user_interest"
	  DROP CONSTRAINT "experience_user_interest_last_status_check",
	  ADD CONSTRAINT "experience_user_interest_last_status_check" 
	  CHECK (status IN ('SUBMITTED', 'ANALYSED', 'APPROVED', 'WAITING', 'DECLINED', 'CANCELLED', 'ACCEPTED', 'NOT_SELECTED', 'COLABORATION', 'WITHDRAWAL', 'CLOSED', 'WITHDRAWAL_ACCEPTED', 'DISPATCH'))
	`);
  };

  exports.down = function (knex) {
};

