exports.up = function (knex, Promise) {
	return knex.schema.raw(`
        ALTER TABLE "application"
        DROP CONSTRAINT "application_decision_check",
        ADD CONSTRAINT "application_decision_check" 
        CHECK (decision IN ('ACCEPTED', 'REJECTED', 'CANCELLED', NULL))
    `);
};

exports.down = function (knex) {};
