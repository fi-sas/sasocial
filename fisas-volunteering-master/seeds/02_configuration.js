
exports.seed = async (knex) => {
	const data = [
		{
			key: "admin_notification_email",
			value: "admin@sasocial.pt",
			created_at: new Date(),
			updated_at: new Date()
		},
		{
			key: "external_entity_profile_id",
			value: "5",
			created_at: new Date(),
			updated_at: new Date()
		}
	];

	return Promise.all(data.map(async (d) => {
		// Check if item exist
		const rows = await knex("configuration").select().where("key", d.key);
		if (rows.length === 0) {
			await knex("configuration").insert(d);
		}
		return true;
	}));
};
