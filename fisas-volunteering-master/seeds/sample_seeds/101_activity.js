
exports.seed = async (knex) => {
    const data = [
        {
            id: 1,
            name: "Informatica",
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        },
        {
            id: 2,
            name: "Área de alojamento - Lavandaria",
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        },
        {
            id: 3,
            name: "Manutenção",
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        }, {
            id: 4,
            name: "Limpezas",
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        },
        {
            id: 5,
            name: "SAS",
            active: true,
            created_at: new Date(),
            updated_at: new Date()
        }
    ]

    return Promise.all(data.map(async (d) => {
        // Check if item exist
        const rows = await knex("activity").select().where("id", d.id);
        if (rows.length === 0) {
            await knex("activity").insert(d);
        }
        return true;
    }));
};
