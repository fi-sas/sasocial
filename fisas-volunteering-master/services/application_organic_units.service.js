"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.application_organic_units",
	table: "application_organic_unit",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "application_organic_units")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"application_id",
			"organic_unit_id",
		],
		defaultWithRelateds: ["organic_unit"],
		withRelateds: {
			organic_unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organic_unit", "organic_unit_id");
			}
		},
		entityValidator: {
			application_id: { type: "number", positive: true, integer: true, convert: true },
			organic_unit_id: { type: "number", positive: true, integer: true, convert: true },
		}
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		organic_units_of_application: {
			rest: {
				method: "GET",
				path: "/:application_id/organic_units"
			},
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {

				return this._find(ctx, {
					query: {
						application_id: ctx.params.application_id,
					},
				}).then((res) => {
					return ctx.call("volunteering.application.find", {
						query: {
							application_id: res.map((d) => d.application_id),
						},
					});
				});
			}
		},
		create_relation_application_organic_units: {
			cache: {
				keys: ["application_id", "organic_unit_id"],
				ttl: 60
			},
			rest: {
				method: "POST",
				path: "/application_organic_units"
			},
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				},
				organic_unit_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {
				await this.removeRelationsBetweenApplicationOrganicUnits(ctx);
				this.clearCache();

				const entities = [{ application_id: ctx.params.application_id, organic_unit_id: ctx.params.organic_unit_id }];

				return this._insert(ctx, { entities });
			}
		},
		save_organic_units: {
			params: {
				application_id: { type: "number", positive: true, integer: true, convert: true },
				organic_unit_ids: {
					type: "array",
					item: {
						type: "number",
						positive: true,
						integer: true,
						convert: true
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					application_id: ctx.params.application_id
				});
				this.clearCache();

				const entities = ctx.params.schedules_ids.map(organic_unit_id => ({
					application_id: ctx.params.application_id,
					organic_unit_id,
				}));
				this.logger.info("save_application_organic_units/insert - entities:");
				this.logger.info(entities);
				return this._insert(ctx, { entities });
			}
		},
		remove_relation_application_organic_unit: {
			cache: {
				keys: ["application_id", "organic_unit_id"],
				ttl: 60
			},
			rest: {
				method: "DELETE",
				path: "/:application_id/:organic_unit_id"
			},
			params: {
				application_id: {
					type: "number", integer: true, positive: true, convert: true
				},
				organic_unit_id: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {
				return this.removeRelationsBetweenApplicationOrganicUnit(ctx);
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

		removeRelationsBetweenApplicationOrganicUnit(ctx) {
			return this.adapter.find({
				query: (q) => {
					q.where({
						application_id: ctx.params.application_id,
						organic_unit_id: ctx.params.schedule_id
					})
						.del();
					return q;
				}
			})
				.then(docs => this.transformDocuments(ctx, ctx.params, docs));
		},

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
