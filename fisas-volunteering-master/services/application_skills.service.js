"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {  hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.application_skills",
	table: "application_skills",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "application_skills")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "application_id", "skill_id", "other_skill", "updated_at", "created_at"],
		defaultWithRelateds: [
			"skill"
		],
		withRelateds: {
			skill(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "volunteering.skills", "skill", "skill_id");
			}
		},
		entityValidator: {
			other_skill: { type: "string", optional: true, nullable: true },
			skill_id: { type: "number", integer: true, convert: true, optional: true, nullable: true },
			application_id: { type: "number", integer: true, convert: true },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
