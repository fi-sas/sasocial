"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { APPLICATION_STATUS, APPLICATION_EVENTS } = require("./utils/constants.js");
const { Errors } = require("@fisas/ms_core").Helpers;
const ApplicationStateMachine = require("./state-machines/application.machine");
const UUIDV4 = require("uuid").v4;
const moment = require("moment");
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.applications",
	table: "application",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "applications")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_id",
			"name",
			"address",
			"nationality",
			"code_postal",
			"location",
			"birthdate",
			"tin",
			"email",
			"mobile_phone",
			"phone",
			"identification_number",
			"genre",
			"student_number",
			"academic_year",
			"course_year",
			"school_id",
			"course_degree_id",
			"course_id",
			"has_collaborated_last_year",
			"previous_activity_description",
			"has_holydays_availability",
			"has_profissional_experience",
			"foreign_languages",
			"language_skills",
			"type_of_company",
			"job_at_company",
			"job_description",
			"observations",
			"status",
			"notification_guid",
			"updated_at",
			"created_at",
			"last_status",
			"decision",
			"reject_reason",
		],
		defaultWithRelateds: [
			"history",
			"preferred_schedule",
			"user",
			"course_degree",
			"school",
			"course",
			"organic_units",
			"skills",
			"area_interests",
		],
		withRelateds: {
			course(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.courses", "course", "course_id");
			},
			school(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "school", "school_id");
			},
			course_degree(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"configuration.course-degrees",
					"course_degree",
					"course_degree_id",
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.application_history",
					"history",
					"id",
					"application_id",
				);
			},
			preferred_schedule(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.application_schedules",
					"preferred_schedule",
					"id",
					"application_id",
				);
			},
			cv_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "cv_file", "cv_file_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			organic_units(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.application_organic_units",
					"organic_units",
					"id",
					"application_id",
				);
			},
			skills(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.application_skills",
					"skills",
					"id",
					"application_id",
				);
			},
			area_interests(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.application_area_interests",
					"area_interests",
					"id",
					"application_id",
				);
			},
			notifications(ids, docs, rule, ctx) {
				let idss = docs.map((d) => d.notification_guid);
				idss = idss.filter((id) => id !== null);
				const query = {};
				query.external_uuid = idss;
				return ctx
					.call("notifications.alerts.find", {
						query,
					})
					.then((data) => {
						return docs.map(
							(d) =>
								(d.notifications = data.filter((dt) => dt.external_uuid === d.notification_guid)),
						);
					});
			},
			historic_applications(ids, applications, rule, ctx) {
				const user_ids = applications.map((app) => app.user_id);
				return this.getUserApplications(ctx, user_ids).then((historycs) => {
					applications.forEach((application) => {
						application.historic_applications = historycs.filter(
							(historyc) => historyc.user_id === application.user_id,
						);
					});
				});
			},
			historic_colaborations(ids, applications, rule, ctx) {
				const user_ids = applications.map((app) => app.user_id);
				return this.getUserExperiences(ctx, user_ids).then((historycs) => {
					applications.forEach((application) => {
						application.historic_colaborations = historycs.filter(
							(historyc) => historyc.user_id === application.user_id,
						);
					});
				});
			},
		},
		entityValidator: {
			user_id: { type: "number", integer: true, convert: true },
			academic_year: { type: "string", max: 255 },
			course_year: { type: "string", max: 255, convert: true, optional: true, nullable: true },
			school_id: { type: "number", integer: true, convert: true, optional: true },
			course_degree_id: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			course_id: { type: "number", integer: true, convert: true, optional: true, nullable: true },
			has_collaborated_last_year: { type: "boolean", convert: true },
			previous_activity_description: { type: "string", max: 255, optional: true },
			has_holydays_availability: { type: "boolean", convert: true },
			has_profissional_experience: { type: "boolean", convert: true },
			foreign_languages: { type: "boolean", convert: true },
			language_skills: "string|optional" /* { type: "array", items: { type: "string" } },*/,
			type_of_company: { type: "string", max: 255, optional: true },
			job_at_company: { type: "string", max: 255, optional: true },
			job_description: { type: "string", max: 255, optional: true },
			observations: { type: "string", optional: true },
			status: { type: "enum", optional: true, values: APPLICATION_STATUS },
			notification_guid: { type: "string", max: 36, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			preferred_schedule_ids: { type: "array", items: "number", optional: true },
			organic_units_ids: { type: "array", items: "number", optional: true },
			name: { type: "string" },
			address: { type: "string" },
			nationality: { type: "string" },
			genre: { type: "enum", values: ["M", "F", "O"] },
			student_number: { type: "string", optional: true, nullable: true },
			skills: { type: "array", items: { type: "number" }, optional: true, nullable: true },
			other_skills: { type: "string", optional: true, nullable: true },
			area_interests: { type: "array", items: { type: "number" }, optional: true, nullable: true },
			code_postal: { type: "string", max: 9 },
			location: { type: "string" },
			birthdate: { type: "date", convert: true },
			tin: { type: "string" },
			email: { type: "email" },
			mobile_phone: { type: "string", optional: true, nullable: true },
			phone: { type: "string", optional: true, nullable: true },
			identification_number: { type: "string" },
			last_status: { type: "enum", optional: true, nullable: true, values: APPLICATION_STATUS },
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "CANCELLED"],
				optional: true,
				nullable: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateActiveApplication",
				"parseJsonFields",
				async function sanatizeParams(ctx) {
					if (ctx.meta.user.is_student) {
						const validation = new Validator();
						const schema = {
							course_id: { type: "number", convert: true },
							course_degree_id: { type: "number", convert: true },
							student_number: { type: "string", convert: true },
							course_year: { type: "string", max: 255, convert: true },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							ctx.params.employee_number = null;
							await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
							await ctx.call("configuration.course-degrees.get", {
								id: ctx.params.course_degree_id,
							});
						}
						await ctx.call("infrastructure.organic-units.get", { id: ctx.params.school_id });
					}
					if (ctx.meta.user.is_employee) {
						ctx.params.course_year = null;
						ctx.params.course_id = null;
						ctx.params.course_degree_id = null;
						ctx.params.student_number = null;
					}

					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.status = "SUBMITTED";
					ctx.params.notification_guid = UUIDV4();

					if (ctx.params.skills) {
						for (const id of ctx.params.skills) {
							await ctx.call("volunteering.skills.get", { id: id });
						}
					}
					if (ctx.params.area_interests) {
						for (const id of ctx.params.area_interests) {
							await ctx.call("volunteering.area_interests.get", { id: id });
						}
					}
				},
			],
			update: [
				"validateStatus",
				"parseJsonFields",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					if (ctx.meta.user.is_student && !ctx.meta.isBackoffice) {
						await ctx.call("configuration.courses.get", { id: ctx.params.course_id });
						await ctx.call("configuration.course-degrees.get", { id: ctx.params.course_degree_id });
						await ctx.call("infrastructure.organic-units.get", { id: ctx.params.school_id });
					}

					if (ctx.params.skills) {
						for (const id of ctx.params.skills) {
							await ctx.call("volunteering.skills.get", { id: id });
						}
					}
					if (ctx.params.area_interests) {
						for (const id of ctx.params.area_interests) {
							await ctx.call("volunteering.area_interests.get", { id: id });
						}
					}
				},
			],
			patch: [
				"parseJsonFields",
				async function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["deleteRelatedSchedule"],
			list: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query["user_id"] = ctx.meta.user.id;
					}
				},
			],
		},
		after: {
			create: [
				"saveSchedule",
				"saveCreateHistory",
				"saveOrganicUnits",
				"saveSkills",
				"saveOtherSkills",
				"saveAreaInterest",
			],
			update: ["updateWithRelateds"],
			patch: ["updateWithRelateds"],
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						if (resp[0].user_id !== ctx.meta.user.id) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"VOLUNTEERING_APPLICATION_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true, nullable: true },
			},
			scope: "volunteering:applications:list",
			async handler(ctx) {
				let academic_year;
				const current_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
				);
				if (!ctx.params.academic_year) {
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}
				return this.adapter
					.getDB(this.adapter.table)
					.select("status")
					.count("*")
					.where("academic_year", "=", academic_year)
					.groupBy("status")
					.then((result) => {
						let allStatus = {
							SUBMITTED: 0,
							ANALYSED: 0,
							ACCEPTED: 0,
							DECLINED: 0,
							CANCELLED: 0,
							EXPIRED: 0,
							DISPATCH: 0,
						};
						result.map((r) => (allStatus[r.status] = r.count));
						return allStatus;
					});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#user.id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: APPLICATION_EVENTS,
					optional: true,
					nullable: true,
				},
				notes: { type: "string", optional: true },
				application: { type: "object", optional: true },
			},
			scope: "volunteering:applications:status",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);

				// Validate Status
				if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
					throw new Errors.ValidationError(
						"Unauthorized application status change",
						"VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED",
						{},
					);
				}
				if (application[0].status === "DISPATCH") {
					const validation = new Validator();
					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;
							if (application[0].decision === "ACCEPTED") {
								status = "accept";
							}
							if (application[0].decision === "REJECTED") {
								status = "decline";
							}
							if (application[0].decision === "CANCELLED") {
								status = "cancel";
							}
							const result = await stateMachine[status]();
							this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].notification_guid,
							);
							this.clearCache();
							return result;
						} else {
							const result = await stateMachine["analyse"]();
							this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].notification_guid,
							);
							this.clearCache();
							return result;
						}
					}
				}

				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();
					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "CANCELLED"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].notification_guid,
						);
						this.clearCache();
						return result;
					}
				}

				if (application[0].status === "ACCEPTED") {
					if (ctx.params.event.toLowerCase() === "cancel") {
						const colaborations = await ctx.call("volunteering.experience-user-interests.find", {
							query: {
								user_id: application[0].user_id,
								status: ["COLABORATION", "WITHDRAWAL"],
							},
							withRelated: false,
						});

						if (colaborations.length > 0) {
							throw new Errors.ValidationError(
								"Has active colaborations",
								"VOLUNTEERING_HAS_COLABORATIONS",
								{},
							);
						}
						const result = await stateMachine["cancel"]();
						this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].notification_guid,
						);
						await this.cancelUserInterests(ctx, application[0].user_id);
						return result;
					}
				}

				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].notification_guid,
				);
				this.clearCache();
				return result;
			},
		},

		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			scope: "volunteering:applications:list",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);
				return await stateMachine.transitions();
			},
		},

		"applicant-cancel": {
			rest: "POST /:id/status/application-cancel",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				notes: { type: "string", optional: true },
			},
			scope: "volunteering:applications:cancel",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });

				if (application[0].user_id != ctx.meta.user.id) {
					throw new Errors.ValidationError(
						"Unauthorized application status change",
						"VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED",
						{},
					);
				}
				let stateMachine = await ApplicationStateMachine.createStateMachine(
					application[0].status,
					ctx,
				);

				const colaborations = await ctx.call("volunteering.experience-user-interests.find", {
					query: {
						user_id: ctx.meta.user.id,
						status: ["COLABORATION", "WITHDRAWAL"],
					},
					withRelated: false,
				});

				if (colaborations.length > 0) {
					throw new Errors.ValidationError(
						"Has active colaborations",
						"VOLUNTEERING_HAS_COLABORATIONS",
						{},
					);
				}

				// Validate Status
				if (stateMachine.cannot("CANCEL")) {
					throw new Errors.ValidationError(
						"Unauthorized application status change",
						"VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED",
						{},
					);
				}

				const result = await stateMachine["cancel"]();
				this.sendNotification(ctx, result[0].status, ctx.meta.user.id, ctx.meta.user.name);
				this.cancelUserInterests(ctx, ctx.meta.user.id);
				return result;
			},
		},

		"close-applications-academic-year": {
			rest: "POST /close-applications",
			visibility: "published",
			params: {
				academic_year: { type: "string" },
			},
			scope: "volunteering:applications:list",
			async handler(ctx) {
				let resp = [];
				const applications = await this._find(ctx, {
					query: {
						academic_year: ctx.params.academic_year,
						status: ["ACCEPTED", "SUBMITTED", "ANALYSED"],
					},
				});
				for (const application of applications) {
					const expired = await this.expireApplication(ctx, application);
					resp.push(expired);
				}
				return resp;
			},
		},

		"generate-report-application": {
			rest: "GET /:id/report",
			visibility: "published",
			scope: "volunteering:applications:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				let skills = [];
				for (const skill of application[0].skills) {
					if (skill.skill_id === null) {
						skills.push({ name: skill.other_skill });
					} else {
						skills.push({ name: skill.skill.translations[0].description });
					}
				}
				let area_interests = [];
				for (const area of application[0].area_interests) {
					area_interests.push({ name: area.area_interest.translations[0].description });
				}
				const data = {
					date: moment().format("YYYY-MM-DD"),
					name: application[0].name,
					address: application[0].address,
					code_postal: application[0].code_postal,
					location: application[0].location,
					birthdate: moment(application[0].birthdate).format("YYYY-MM-DD"),
					nationality: application[0].nationality,
					tin: application[0].tin,
					identification: application[0].identification,
					student_number: application[0].student_number,
					mobile_phone: application[0].mobile_phone,
					phone: application[0].phone,
					email: application[0].email,
					academic_year: application[0].academic_year,
					course_year: application[0].course_year,
					course_name: application[0].course.name,
					course_degree: application[0].course_degree.name,
					school_name: application[0].school.name,
					iban: application[0].iban,
					status: application[0].status,
					organic_units: application[0].organic_units,
					preferred_activities: application[0].preferred_activities,
					has_collaborated_last_year: application[0].has_collaborated_last_year,
					previous_activity_description: application[0].previous_activity_description,
					has_profissional_experience: application[0].has_profissional_experience,
					job_description: application[0].job_description,
					foreign_languages: application[0].foreign_languages,
					language_skills: application[0].language_skills,
					area_interests: area_interests,
					skills: skills,
				};
				return ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_APPLICATION",
					data: data,
					name: "Informação de uma candidatura" + "(" + data.date + ")",
				});
			},
		},

		"verify-active-application": {
			rest: "GET /user-application",
			visibility: "published",
			scope: "volunteering:applications:create",
			async handler(ctx) {
				const application = await ctx.call("volunteering.applications.find", {
					query: {
						user_id: ctx.meta.user.id,
						status: ["SUBMITTED", "ANALYSED", "ACCEPTED", "DISPATCH"],
					},
				});
				if (application.length !== 0) {
					return { id: application[0].id, active: true };
				} else {
					return { id: null, active: false };
				}
			},
		},

		"get-report-list-applications": {
			rest: "POST /list-report",
			visibility: "published",
			params: {
				academic_year: { type: "string" },
			},
			scope: "volunteering:applications:list",
			async handler(ctx) {
				const applications = await this._find(ctx, {
					query: {
						academic_year: ctx.params.academic_year,
					},
				});
				for (let application of applications) {
					application.created_at = moment().format("YYYY-MM-DD");
				}
				const data = {
					application: applications,
					date: moment().format("YYYY-MM-DD"),
					academic_year: ctx.params.academic_year,
				};
				return ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_LIST_APPLICATIONS",
					data: data,
					name: "Lista de candidaturas ao voluntariado",
				});
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "volunteering:applications:update",
			async handler(ctx) {
				const application = await this._get(ctx, { id: ctx.params.id });
				if (application[0].last_status !== null) {
					const result = await ctx.call("volunteering.applications.patch", {
						id: ctx.params.id,
						status: application[0].last_status,
						last_status: application[0].status,
					});
					await ctx.call("volunteering.application_history.create", {
						application_id: ctx.params.id,
						status: application[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});

					this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].notification_guid,
					);
					return result;
				} else {
					const result = await ctx.call("volunteering.applications.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: application[0].status,
					});
					await ctx.call("volunteering.application_history.create", {
						application_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].notification_guid,
					);
					return result;
				}
			},
		},

		get_users_with_application_approved: {
			//rest: "GET /get-users-with-application-approved",
			//visibility: "published",
			visibility: "public",
			params: {},
			async handler(ctx) {
				const result = await this.adapter.find({
					query: (q) => {
						q.distinct("user_id as id");
						q.where("status", "=", "ACCEPTED");

						return q;
					},
					withRelated: false,
				});

				return result;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Function for get all experience of user
		 * @param {*} ctx
		 * @param {*} user_id
		 * @returns
		 */
		async getUserExperiences(ctx, user_id) {
			const user_manifest = await ctx.call("volunteering.experience-user-interests.find", {
				query: {
					user_id: user_id,
				},
				withRelated: ["experience"],
			});
			return user_manifest.map((user_interest) => {
				const container = {};
				container.academic_year = user_interest.experience.academic_year;
				container.user_manifest_id = user_interest.id;
				container.experience_translations = user_interest.experience.translations;
				container.user_id = user_interest.user_id;
				return container;
			});
		},

		/**
		 * Function for get all historic applications
		 * @param {*} ctx
		 * @param {*} user_id
		 * @returns
		 */
		async getUserApplications(ctx, user_id) {
			const applications = await this._find(ctx, {
				query: {
					user_id: user_id,
				},
				withRelated: false,
			});
			return applications.map((app) => {
				const container = {};
				container.academic_year = app.academic_year;
				container.status = app.status;
				container.id = app.id;
				container.user_id = app.user_id;
				return container;
			});
		},

		/**
		 * Expire application
		 * @param {*} ctx
		 * @param {*} application
		 * @returns
		 */
		async expireApplication(ctx, application) {
			return await ctx.call("volunteering.applications.patch", {
				id: application.id,
				status: "EXPIRED",
			});
		},

		/**
		 * Validate a active application by academic year
		 * @param {*} ctx
		 */
		async validateActiveApplication(ctx) {
			const application = await this._find(ctx, {
				query: {
					academic_year: ctx.params.academic_year,
					user_id: ctx.meta.user.id,
					status: ["ACCEPTED", "SUBMITTED", "ANALYSED", "DISPATCH"],
				},
			});
			if (application.length !== 0) {
				throw new Errors.ValidationError(
					"You have a active application",
					"VOLUNTEERING_APPLICATION_HAS_ACTIVE_APLICATION",
					{},
				);
			}
		},

		/**
		 * Validate a status in update, for not change status
		 * @param {*} ctx
		 */
		async validateStatus(ctx) {
			if (ctx.params.id) {
				const application = await this._get(ctx, { id: ctx.params.id });
				if (application[0].status != ctx.params.status) {
					throw new Errors.ValidationError(
						'Application status should be changed through "application / { id } / status".',
						"VOLUNTEERING_APPLICATION_STATUS_WRONG_ENDPOINT",
						{},
					);
				}
			}
		},

		/**
		 * Parse json form languages skills
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async parseJsonFields(ctx, res) {
			if (ctx.params.language_skills && Array.isArray(ctx.params.language_skills)) {
				ctx.params.language_skills = JSON.stringify(ctx.params.language_skills);
			}
			return res;
		},

		/**
		 * Save a schedule
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveSchedule(ctx, res) {
			if (ctx.params.preferred_schedule_ids) {
				res[0].preferred_schedule = await this.createWithRelatedSchedules(
					ctx,
					"volunteering.application_schedules.create",
					res[0].id,
					ctx.params.preferred_schedule_ids,
				);
			}
			return res;
		},

		/**
		 * Save organics units
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveOrganicUnits(ctx, res) {
			if (ctx.params.organic_units_ids) {
				res[0].organic_units = await this.createWithRelatedOrganicUnits(
					ctx,
					"volunteering.application_organic_units.create",
					res[0].id,
					ctx.params.organic_units_ids,
				);
			}
			return res;
		},

		/**
		 * Save skills
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveSkills(ctx, res) {
			if (ctx.params.skills) {
				res[0].skills = await this.createWithRelatedSkills(
					ctx,
					"volunteering.application_skills.create",
					res[0].id,
					ctx.params.skills,
				);
			}
			return res;
		},

		/**
		 * Save other skills
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveOtherSkills(ctx, res) {
			if (ctx.params.other_skills) {
				const skill = {
					application_id: res[0].id,
					other_skill: ctx.params.other_skills,
				};
				res[0].skills = await ctx.call("volunteering.application_skills.create", skill);
			}
			return res;
		},

		/**
		 * Save area interests
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveAreaInterest(ctx, res) {
			if (ctx.params.area_interests) {
				res[0].area_interests = await this.createWithRelatedAreaInterest(
					ctx,
					"volunteering.application_area_interests.create",
					res[0].id,
					ctx.params.area_interests,
				);
			}
			return res;
		},

		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedActivities(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const applicationActivity = {
					application_id: application_id,
					activity_id: id,
				};
				const created = await ctx.call(service, applicationActivity);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},

		/**
		 * Save historyc in creation application
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveCreateHistory(ctx, res) {
			if (res[0].id != null) {
				await ctx.call("volunteering.application_history.create", {
					application_id: res[0].id,
					status: "SUBMITTED",
					user_id: ctx.meta.user.id,
					notes: null,
				});
				this.sendNotification(
					ctx,
					"SUBMITTED",
					ctx.meta.user.id,
					ctx.meta.user.name,
					res[0].notification_guid,
				);
			}
			return res;
		},

		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedSchedules(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const applicationActivity = {
					application_id: application_id,
					schedule_id: id,
				};
				const created = await ctx.call(service, applicationActivity);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 */
		async createWithRelatedOrganicUnits(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const organic_unit = {
					application_id: application_id,
					organic_unit_id: id,
				};
				const created = await ctx.call(service, organic_unit);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},

		/**
		 * Save a withrelated skills in application
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 * @returns
		 */
		async createWithRelatedSkills(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const skills = {
					application_id: application_id,
					skill_id: id,
				};
				const created = await ctx.call(service, skills);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},

		/**
		 * Save a withrelated areainterest in application
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} application_id
		 * @param {*} list
		 * @returns
		 */
		async createWithRelatedAreaInterest(ctx, service, application_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const id of list) {
				const area_interest = {
					application_id: application_id,
					area_interest_id: id,
				};
				const created = await ctx.call(service, area_interest);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},
		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) {
				return;
			}
			for (const item of list) {
				await ctx.call(service, item);
			}
		},

		/**
		 * Update withrelateds in update application
		 * @param {*} ctx
		 * @param {*} response
		 * @returns
		 */
		async updateWithRelateds(ctx, response) {
			if (ctx.params.preferred_schedule_ids) {
				await this.deleteWithRelated(
					ctx,
					"volunteering.application_schedules.remove",
					await ctx.call("volunteering.application_schedules.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].preferred_schedule = await this.createWithRelatedSchedules(
					ctx,
					"volunteering.application_schedules.create",
					response[0].id,
					ctx.params.preferred_schedule_ids,
				);
			}
			if (ctx.params.organic_units_ids) {
				await this.deleteWithRelated(
					ctx,
					"volunteering.application_organic_units.remove",
					await ctx.call("volunteering.application_organic_units.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].organic_units = await this.createWithRelatedOrganicUnits(
					ctx,
					"volunteering.application_organic_units.create",
					response[0].id,
					ctx.params.organic_units_ids,
				);
			}
			if (ctx.params.skills) {
				await this.deleteWithRelated(
					ctx,
					"volunteering.application_skills.remove",
					await ctx.call("volunteering.application_skills.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].skills = await this.createWithRelatedSkills(
					ctx,
					"volunteering.application_skills.find",
					response[0].id,
					ctx.params.skills,
				);
			}
			if (ctx.params.other_skill) {
				await this.deleteWithRelated(
					ctx,
					"volunteering.application_skills.remove",
					await ctx.call("volunteering.application_skills.find", {
						query: { application_id: response[0].id },
					}),
				);
				const skill = {
					application_id: response[0].id,
					other_skill: ctx.params.other_skills,
				};
				response[0].skills = await ctx.call("volunteering.application_skills.create", skill);
			}
			if (ctx.params.area_interests) {
				await this.deleteWithRelated(
					ctx,
					"volunteering.application_area_interests.remove",
					await ctx.call("volunteering.application_area_interests.find", {
						query: { application_id: response[0].id },
					}),
				);
				response[0].area_interests = await this.createWithRelatedAreaInterest(
					ctx,
					"volunteering.application_area_interests.create",
					response[0].id,
					ctx.params.area_interests,
				);
			}
			return response;
		},

		/**
		 * Send notitication on change status application
		 * @param {*} ctx
		 * @param {*} status
		 * @param {*} user_id
		 * @param {*} name
		 * @param {*} notification_gui
		 */
		async sendNotification(ctx, status, user_id, name, notification_gui) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "VOLUNTEERING_APPLICATION_STATUS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name },
				variables: {},
				external_uuid: notification_gui,
				medias: [],
			});
		},

		async cancelUserInterests(ctx, user_id) {
			const users_interests = await ctx.call("volunteering.experience-user-interests.find", {
				query: {
					user_id: user_id,
					status: ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "ACCEPTED", "DECLINED"],
				},
				withRelated: false,
			});
			for (const user_interest of users_interests) {
				await ctx.call("volunteering.experience-user-interests.patch", {
					id: user_interest.id,
					status: "CANCELLED",
				});
				await ctx.call("volunteering.user_interest_history.create", {
					user_interest_id: user_interest.id,
					status: "CANCELLED",
					user_id: user_interest.user_id,
				});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
