"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.area_interest_translations",
	table: "area_interest_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "area_interest_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "description", "language_id", "area_interest_id", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {
		},
		entityValidator: {
			description: { type: "string" },
			area_interest_id: { type: "number", integer: true, convert: true },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_translations:{
			params:{
				area_interest_id : { type: "number", integer: true, convert: true },
				translations: {
					type: "array", items: {
						type: "object",
						props: {
							language_id: { type: "number", min: 1, integer: true, positive: true },
							description: { type: "string" },
						},
					},
				}
			},
			async handler(ctx){
				await this.adapter.removeMany({
					area_interest_id: ctx.params.area_interest_id
				});
				this.clearCache();
				let result = [];
				for(const translation of ctx.params.translations){
					const skill_translation = {
						area_interest_id : ctx.params.area_interest_id,
						language_id: translation.language_id,
						description: translation.description,
						created_at: new Date(),
						updated_at: new Date(),
					};
					const skill = await this._insert(ctx, { entity: skill_translation } );
					result.push(skill[0]);
				}
				return result;
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
