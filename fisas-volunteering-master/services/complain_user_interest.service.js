"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const {  hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { ValidationError } = require("@fisas/ms_core").Helpers.Errors;


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.complain-user-interests",
	table: "complain_user_interest",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "complain-user-interests")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"complain",
			"file_id",
			"response",
			"user_id",
			"status",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			user_interest(ids, docs, rule, ctx){
				return hasOne(docs, ctx, "volunteering.experience-user-interests", "user_interest", "user_interest_id");
			},
		},
		entityValidator: {
			user_interest_id: { type: "number", integer: true, convert: true },
			complain: { type: "string" },
			file_id: { type: "number", integer: true, convert: true, optional: true, nullable: true },
			response: { type: "string", optional: true, nullable: true },
			user_id: { type: "number", integer: true, convert: true },
			status: { type: "enum", values: ["SUBMITTED", "ANALYSED", "REPLYED"] },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateExperience",
				function sanatizeParams(ctx) {
					ctx.params.status = "SUBMITTED";
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		analyse : {
			rest: "POST /:id/analyse",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true }
			},
			scope: "volunteering:complain-user-interests:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0] !== "REPLYED"){
					complain[0].status = "ANALYSED";
					return await this._update(ctx, complain[0]);
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"VOLUNTEERING_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		},

		response : {
			rest: "POST /:id/response",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				response: { type: "string" }
			},
			scope: "volunteering:complain-user-interests:update",
			async handler(ctx) {
				const complain = await this._get(ctx, { id: ctx.params.id });
				if(complain[0].status !== "REPLYED"){
					complain[0].status = "REPLYED";
					complain[0].response = ctx.params.response;
					const updated = await this._update(ctx, complain[0]);
					await ctx.call("notifications.alerts.create_alert", {
						alert_type_key: "VOLUNTEERING_COMPLAIN",
						user_id: complain[0].user_id,
						user_data: {},
						data: { name: complain[0].user.name, response : ctx.params.response },
						variables: {},
						external_uuid: null,
						medias: [],
					});
					return updated;
				}
				else {
					throw new ValidationError(
						"This complain have a response",
						"VOLUNTEERING_COMPLAIN_RESPONSE_ERROR",
						{},
					);
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateExperience(ctx) {
			await ctx.call("volunteering.experience-user-interests.get", { id: ctx.params.user_interest_id });
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
