"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "updated_at", "created_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: "string|max:120",
			value: "string|max:250",
			updated_at: "date|convert|optional",
			created_at: "date|convert|optional",
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "protected",
		},
		get: {
			// REST: GET /:id
			visibility: "protected",
		},
		update: {
			// REST: PUT /:id
			visibility: "protected",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "protected",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "protected",
		},
		getAdminNotificationEmail: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/admin_notification_email",
			},
			scope: "volunteering:configurations:read",
			params: {},
			async handler(ctx) {
				return ctx.call("volunteering.configurations.find", {
					query: {
						key: "admin_notification_email",
					},
				});
			},
		},
		setAdminNotificationEmail: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/admin_notification_email",
			},
			scope: "volunteering:configurations:update",
			params: {},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "admin_notification_email",
					},
				}).then((res) => {
					if (res.length) {
						// UPDATE
						return ctx.call(`${this.name}.update`, {
							id: res[0].id,
							key: "admin_notification_email",
							value: ctx.params.value,
						});
					} else {
						// INSERT
						return ctx.call(`${this.name}.create`, {
							key: "admin_notification_email",
							value: ctx.params.value,
						});
					}
				});
			},
		},
		getExternalEntityProfileId: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/external-entity-profile-id",
			},
			scope: "volunteering:configurations:read",
			params: {},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "external_entity_profile_id",
					},
				});
			},
		},

		setExternalEntityProfileId: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/external-entity-profile-id",
			},
			scope: "volunteering:configurations:update",
			params: {
				value: {
					type: "number", integer: true, positive: true, convert: true
				}
			},
			async handler(ctx) {

				const externalProfileId = await this._find(ctx, {
					query: {
						key: "external_entity_profile_id",
					},
				});
				externalProfileId[0].value = ctx.params.value + "";
				externalProfileId[0].updated_at = new Date();
				return await this._update(ctx, externalProfileId[0]);
			},
		},

	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
