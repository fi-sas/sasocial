"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { EXPERIENCE_STATUS } = require("./utils/constants.js");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.experience_history",
	table: "experience_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "experience_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"experience_id",
			"status",
			"user_id",
			"notes",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			"experience"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "volunteering.experiences", "experience", "experience_id");
			},
			"user"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			experience_id: { type: "number", integer: true, convert: true },
			status: { type: "enum", optional: true, values: EXPERIENCE_STATUS },
			user_id: { type: "number", integer: true },
			notes: { type: "string", max: 255, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
