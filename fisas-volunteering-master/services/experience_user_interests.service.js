"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { USER_INTEREST_STATUS, USER_INTEREST_EVENTS } = require("./utils/constants.js");
const UUIDV4 = require("uuid").v4;
const UserInterestStateMachine = require("./state-machines/user-interest.machine");
const moment = require("moment");
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.experience-user-interests",
	table: "experience_user_interest",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "experience-user-interests")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"experience_id",
			"user_id",
			"certificate_file_id",
			"student_avaliation",
			"student_file_avaliation",
			"report_avaliation_file_id",
			"report_avaliation",
			"certificate_generated_id",
			"uuid",
			"status",
			"last_status",
			"decision",
			"reject_reason",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["user", "history"],
		withRelateds: {
			experience(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "volunteering.experiences", "experience", "experience_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.user_interest_history",
					"history",
					"id",
					"user_interest_id",
				);
			},
			student_avaliation_file(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"media.files",
					"student_avaliation_file",
					"student_file_avaliation",
				);
			},
			monthly_reports(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.monthly-reports",
					"monthly_reports",
					"id",
					"user_interest_id",
				);
			},
			certificate_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "certificate_file", "certificate_file_id");
			},
			report_avaliation_file(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"media.files",
					"report_avaliation_file",
					"report_avaliation_file_id",
				);
			},
			notifications(ids, docs, rule, ctx) {
				let idss = docs.map((d) => d.uuid);
				idss = idss.filter((id) => id !== null);
				const query = {};
				query.external_uuid = idss;
				return ctx
					.call("notifications.alerts.find", {
						query,
					})
					.then((data) => {
						return docs.map(
							(d) => (d.notifications = data.filter((dt) => dt.external_uuid === d.uuid)),
						);
					});
			},
			historic_applications(ids, experiences, rule, ctx) {
				const user_ids = experiences.map((app) => app.user_id);
				return this.getUserApplications(ctx, user_ids).then((historycs) => {
					experiences.forEach((experience) => {
						experience.historic_applications = historycs.filter(
							(historyc) => historyc.user_id === experience.user_id,
						);
					});
				});
			},
			historic_colaborations(ids, experiences, rule, ctx) {
				const user_ids = experiences.map((app) => app.user_id);
				return this.getUserExperiences(ctx, user_ids).then((historycs) => {
					experiences.forEach((experience) => {
						experience.historic_colaborations = historycs.filter(
							(historyc) => historyc.user_id === experience.user_id,
						);
					});
				});
			},
			application(ids, experiences, rule, ctx) {
				return Promise.all(
					experiences.map((experience) => {
						return this.getCurrentApplication(
							ctx,
							experience.experience_id,
							experience.user_id,
						).then((application) => {
							experience.application = application;
						});
					}),
				);
			},
			attendances(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.attendances",
					"attendances",
					"id",
					"user_interest_id",
				);
			},
			certificate_generated(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"reports.reports",
					"certificate_generated",
					"certificate_generated_id",
				);
			},
		},
		entityValidator: {
			experience_id: { type: "number", integer: true, convert: true },
			user_id: { type: "number", integer: true, convert: true },
			certificate_file_id: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			student_avaliation: { type: "string", optional: true, nullable: true },
			uuid: { type: "string", nullable: true, optional: true },
			status: { type: "enum", values: Object.keys(USER_INTEREST_STATUS) },
			report_avaliation_file_id: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
				nullable: true,
			},
			last_status: {
				type: "enum",
				optional: true,
				nullable: true,
				values: Object.keys(USER_INTEREST_STATUS),
			},
			report_avaliation: { type: "string", nullable: true, optional: true },
			certificate_generated_id: { type: "number", integer: true, optional: true, nullable: true },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "NOT_SELECTED", "WAITING"],
				optional: true,
				nullable: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateUserInterestExperience",
				"validateActiveApplication",
				"validateInterest",
				function sanatizeParams(ctx) {
					ctx.params.uuid = UUIDV4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.status = "SUBMITTED";
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.isBackoffice && !ctx.meta.no_validate_user) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query["user_id"] = ctx.meta.user.id;
					}
				},
			],
		},
		after: {
			create: ["saveCreateHistory"],
			get: [
				async function validateAccess(ctx, resp) {
					if (!ctx.meta.isBackoffice) {
						const experience = await ctx.call("volunteering.experiences.get", {
							id: resp[0].experience_id,
							withRelated: false,
						});
						if (
							resp[0].user_id !== ctx.meta.user.id &&
							experience[0].experience_responsible_id !== ctx.meta.user.id
						) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"VOLUNTEERING_ATTENDANCE_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice",
					"#user.id",
					"#no_validate_user",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#isBackoffice", "#user.id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		list_interest_experience: {
			rest: "GET /experience/:experience_id",
			visibility: "published",
			params: {
				experience_id: { type: "number", integer: true, convert: true },
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			scope: "volunteering:experience-user-interests:list",
			async handler(ctx) {
				const experience = await ctx.call("volunteering.experiences.get", {
					id: ctx.params.experience_id,
				});
				if (experience[0].experience_responsible_id !== ctx.meta.user.id) {
					throw new Errors.ForbiddenError(
						"Unauthorized access to experience, not advisor or responsible",
						"VOLUNTEERING_EXPERIENCE_FORBIDDEN",
						{},
					);
				} else {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["experience_id"] = ctx.params.experience_id;
					delete ctx.params.experience_id;
					ctx.meta.no_validate_user = true;
					return ctx.call("volunteering.experience-user-interests.list", ctx.params);
				}
			},
		},

		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true, nullable: true },
			},
			scope: "volunteering:experience-user-interests:list",
			async handler(ctx) {
				let academic_year;
				const current_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
				);
				if (!ctx.params.academic_year) {
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}
				const experience = await ctx.call("volunteering.experiences.find", {
					query: {
						academic_year: academic_year,
					},
				});
				return this.adapter
					.getDB(this.adapter.table)
					.select("status")
					.count("*")
					.whereIn(
						"experience_id",
						experience.map((ex) => ex.id),
					)
					.groupBy("status")
					.then((result) => {
						let allStatus = {
							SUBMITTED: 0,
							ANALYSED: 0,
							APPROVED: 0,
							WAITING: 0,
							NOT_SELECTED: 0,
							ACCEPTED: 0,
							COLABORATION: 0,
							WITHDRAWAL: 0,
							DECLINED: 0,
							CANCELLED: 0,
							WITHDRAWAL_ACCEPTED: 0,
							CLOSED: 0,
							DISPATCH: 0,
						};
						result.map((r) => (allStatus[r.status] = r.count));
						return allStatus;
					});
			},
		},

		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			scope: "volunteering:experience-user-interests:list",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				const user_manifest = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					user_manifest[0].status,
					ctx,
				);
				return await stateMachine.transitions();
			},
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			scope: "volunteering:experience-user-interests:status",
			params: {
				id: { type: "number", integer: true, convert: true },
				event: { type: "enum", values: USER_INTEREST_EVENTS, optional: true, nullable: true },
				notes: { type: "string", optional: true },
				manifest_interest: { type: "object", optional: true },
			},
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await UserInterestStateMachine.createStateMachine(
					user_interest[0].status,
					ctx,
				);

				// Validate Status
				if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
					throw new Errors.ValidationError(
						"Unauthorized user interest status change",
						"VOLUNTEERING_USER_INTEREST_STATUS_ERROR",
						{},
					);
				}

				if (user_interest[0].status === "DISPATCH") {
					const validation = new Validator();
					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;
							if (user_interest[0].decision === "REJECTED") {
								status = "decline";
							}
							if (user_interest[0].decision === "ACCEPTED") {
								status = "approve";
							}
							if (user_interest[0].decision === "NOT_SELECTED") {
								status = "notselect";
							}
							if (user_interest[0].decision === "WAITING") {
								status = "waiting";
							}
							const result = await stateMachine[status]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);
							return result;
						} else {
							const result = await stateMachine["analyse"]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);
							return result;
						}
					}
				}

				if (user_interest[0].status === "COLABORATION") {
					if (ctx.params.event.toLowerCase() === "close") {
						if (
							user_interest[0].certificate_file_id === null &&
							user_interest[0].certificate_generated_id === null
						) {
							if (ctx.params.manifest_interest) {
								if (ctx.params.manifest_interest.certificate_file_id) {
									const result = await stateMachine[ctx.params.event.toLowerCase()]();
									await this.sendNotification(
										ctx,
										result[0].status,
										result[0].user.id,
										result[0].user.name,
										result[0].uuid,
									);
									return result;
								} else {
									throw new Errors.ValidationError(
										"Certificate file needed for change the user interest status",
										"VOLUNTEERING_USER_INTEREST_STATUS_NO_CERTIFICATE",
										{},
									);
								}
							} else {
								throw new Errors.ValidationError(
									"Certificate file needed for change the user interest status",
									"VOLUNTEERING_USER_INTEREST_STATUS_NO_CERTIFICATE",
									{},
								);
							}
						} else {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].user.id,
								result[0].user.name,
								result[0].uuid,
							);
							return result;
						}
					}
				}
				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();
					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "NOT_SELECTED", "WAITING"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						await this.sendNotification(
							ctx,
							result[0].status,
							result[0].user.id,
							result[0].user.name,
							result[0].uuid,
						);
						return result;
					}
				}

				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].uuid,
				);
				if (ctx.params.event.toLowerCase() === "analyse") {
					await ctx.call("volunteering.experiences.change-status-by-manifest-interest", {
						id: result[0].experience_id,
						status: "SELECTION",
					});
				}
				if (ctx.params.event.toLowerCase() === "colaboration") {
					await ctx.call("volunteering.experiences.change-status-by-manifest-interest", {
						id: result[0].experience_id,
						status: "IN_COLABORATION",
					});
				}
				return result;
			},
		},

		"cancel-interest": {
			rest: "POST /:id/status/interest-cancel",
			visibility: "published",
			scope: "volunteering:experience-user-interests:cancel",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const manifest_interest = await this._get(ctx, { id: ctx.params.id });

				// Validate user_id
				if (manifest_interest[0].user_id !== ctx.meta.user.id) {
					throw new Errors.ValidationError(
						"Unauthorized user interest status change",
						"VOLUNTEERING_USER_INTEREST_UNAUTHORIZED",
						{},
					);
				}
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					manifest_interest[0].status,
					ctx,
				);
				// Validate status
				if (stateMachine.cannot("CANCEL")) {
					throw new Errors.ValidationError(
						"Unauthorized user interest status change",
						"VOLUNTEERING_USER_INTEREST_STATUS_ERROR",
						{},
					);
				}
				const result = await stateMachine["cancel"]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].uuid,
				);
				return result;
			},
		},

		"withdrawal-interest": {
			rest: "POST /:id/status/interest-withdrawal",
			scope: "volunteering:experience-user-interests:create",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const manifest_interest = await this._get(ctx, { id: ctx.params.id });

				// Validate user_id
				if (manifest_interest[0].user_id !== ctx.meta.user.id) {
					throw new Errors.ValidationError(
						"Unauthorized user interest status change",
						"VOLUNTEERING_USER_INTEREST_UNAUTHORIZED",
						{},
					);
				}
				let stateMachine = await UserInterestStateMachine.createStateMachine(
					manifest_interest[0].status,
					ctx,
				);

				// Validate status
				if (stateMachine.cannot("WITHDRAWAL")) {
					throw new Errors.ValidationError(
						"Unauthorized user interest status change",
						"VOLUNTEERING_USER_INTEREST_STATUS_ERROR",
						{},
					);
				}
				const result = await stateMachine["withdrawal"]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].user.id,
					result[0].user.name,
					result[0].uuid,
				);
				return result;
			},
		},

		"add-student-avaliation": {
			rest: "POST /:id/add-student-avaliation",
			scope: "volunteering:experience-user-interests:list",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				avaliation: { type: "string" },
				file: { type: "number", integer: true, optional: true, nullable: true },
			},
			async handler(ctx) {
				let file = null;
				if (ctx.params.file) {
					file = ctx.params.file;
				}
				return await ctx.call("volunteering.experience-user-interests.patch", {
					id: ctx.params.id,
					student_avaliation: ctx.params.avaliation,
					student_file_avaliation: file,
				});
			},
		},

		"add-certificate": {
			rest: "POST /:id/add-certificate",
			visibility: "published",
			params: {
				id: { type: "number", integer: true, convert: true },
				certificate_file_id: { type: "number", integer: true, convert: true },
			},
			scope: "volunteering:experience-user-interests:certificate",
			async handler(ctx) {
				return await ctx.call("volunteering.experience-user-interests.patch", {
					id: ctx.params.id,
					certificate_file_id: ctx.params.certificate_file_id,
				});
			},
		},

		"add-avaliation-report": {
			rest: "POST /:id/add-report-avaliation",
			visibility: "published",
			params: {
				avaliation_file: { type: "number", integer: true, convert: true },
				avaliation_text: { type: "string", optional: true, nullable: true },
			},
			scope: "volunteering:experience-user-interests:final_report",
			async handler(ctx) {
				return await ctx.call("volunteering.experience-user-interests.patch", {
					id: ctx.params.id,
					report_avaliation_file_id: ctx.params.avaliation_file,
					report_avaliation: ctx.params.avaliation_text,
				});
			},
		},

		"generate-certificate": {
			rest: "POST /:id/generate-certificate",
			visibility: "published",
			scope: "volunteering:experience-user-interests:certificate",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });
				const experience = await ctx.call("volunteering.experiences.get", {
					id: user_interest[0].experience_id,
				});
				const data = {
					experience_name: experience[0].translations[0].title,
					student_name: user_interest[0].user.name,
					school_name: experience[0].organic_unit.name,
					academic_year: experience[0].academic_year,
					responsable: experience[0].experience_responsible.name,
					date: moment().format("YYYY-MM-DD"),
				};

				const certificate = await ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_EXPERIENCE_CERTIFICATE",
					data: data,
					name: "Certificado de experiência" + "(" + data.date + ")",
				});

				await ctx.call("volunteering.experience-user-interests.patch", {
					id: ctx.params.id,
					certificate_generated_id: certificate[0].id,
				});

				return certificate;
			},
		},

		check_interest: {
			rest: "GET /check-interest/:id",
			visibility: "published",
			scope: "volunteering:experience-user-interests:create",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await ctx.call("volunteering.experiences.get", {
					id: ctx.params.id,
					withRelated: false,
				});
				const user_interest = await this._count(ctx, {
					query: {
						user_id: ctx.meta.user.id,
						experience_id: ctx.params.id,
					},
				});
				const application = await ctx.call("volunteering.applications.count", {
					query: {
						user_id: ctx.meta.user.id,
						academic_year: experience[0].academic_year,
						status: "ACCEPTED",
					},
				});

				const applcations_actives = await ctx.call("volunteering.applications.count", {
					query: {
						user_id: ctx.meta.user.id,
						academic_year: experience[0].academic_year,
						status: ["SUBMITTED", "ANALYSED", "ACCEPTED", "DISPATCH"],
					},
				});

				let application_active = false;
				let interest = true;
				let applications = false;
				if (user_interest > 0) {
					interest = false;
				}
				if (application > 0) {
					applications = true;
				}
				if (applcations_actives > 0) {
					application_active = true;
				}
				return {
					can_express_interest:
						["APPROVED", "SELECTION", "IN_COLABORATION"].includes(experience[0].status) && interest,
					experience_available: ["APPROVED", "SELECTION", "IN_COLABORATION"].includes(
						experience[0].status,
					),
					has_application: application_active,
					has_interest: !interest,
					has_application_accepted: applications,
				};
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "volunteering:experience-user-interests:update",
			visibility: "published",
			async handler(ctx) {
				const user_interest = await this._get(ctx, { id: ctx.params.id });
				if (user_interest[0].last_status !== null) {
					const result = await ctx.call("volunteering.experience-user-interests.patch", {
						id: ctx.params.id,
						status: user_interest[0].last_status,
						last_status: user_interest[0].status,
					});
					await ctx.call("volunteering.user_interest_history.create", {
						user_interest_id: ctx.params.id,
						status: user_interest[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].uuid,
					);
					return result;
				} else {
					const result = await ctx.call("volunteering.experience-user-interests.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: user_interest[0].status,
					});
					await ctx.call("volunteering.user_interest_history.create", {
						user_interest_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].user.id,
						result[0].user.name,
						result[0].uuid,
					);
					return result;
				}
			},
		},
		"get-all-users": {
			rest: "GET /users-colaboration",
			visibility: "published",
			scope: "volunteering:experience-user-interests:list",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			async handler(ctx) {
				const q = await this.adapter.raw("select DISTINCT user_id from experience_user_interest");
				ctx.params.query = ctx.params.query || {};
				ctx.params.query.user_id = q.rows.map((u) => u.user_id);
				ctx.params.query.status = "ACCEPTED";
				return ctx.call("volunteering.applications.list", ctx.params);
			},
		},
		count_interests: {
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				return await this._count(ctx, { query: { experience_id: ctx.params.id } });
			},
		},
		get_all_complains: {
			visibility: "published",
			cache: {
				keys: ["#user.id", "#isBackoffice"],
			},
			rest: "GET /get-all-complains",
			async handler(ctx) {
				let res = [];
				if (!ctx.meta.isBackoffice) {
					await ctx
						.call("volunteering.complain-applications.find", {
							query: {
								user_id: ctx.meta.user.id,
							},
							withRelated: "file",
						})
						.then((complain_applications) => {
							complain_applications.map((c) => {
								c.type = "APPLICATIONS";
								res.push(c);
							});
						});
					await ctx
						.call("volunteering.general-complains.find", {
							query: {
								user_id: ctx.meta.user.id,
							},
							withRelated: "file",
						})
						.then((complain_general) => {
							complain_general.map((c) => {
								c.type = "GENERAL";
								res.push(c);
							});
						});
					await this._find(ctx, {
						query: {
							user_id: ctx.meta.user.id,
						},
						withRelated: false,
					}).then(async (responseUserInterest) => {
						//application complains
						for (const userInterest of responseUserInterest) {
							//complain-user-interests complains
							await ctx
								.call("volunteering.complain-user-interests.find", {
									query: {
										user_interest_id: userInterest.id,
									},
									withRelated: "file",
								})
								.then((complain_user_interest) => {
									complain_user_interest.map((c) => {
										c.type = "USER_INTEREST";
										res.push(c);
									});
								});
							//experience complains
							await ctx
								.call("volunteering.complain-experiences.find", {
									query: {
										experience_id: userInterest.experience_id,
									},
									withRelated: "file",
								})
								.then((complain_experience) => {
									complain_experience.map((c) => {
										c.type = "EXPERIENCE";
										res.push(c);
									});
								});
						}
					});
				}
				return res;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Function for all applications;
		 * @param {*} ctx
		 * @param {*} user_id
		 * @returns
		 */
		async getUserApplications(ctx, user_id) {
			if (!ctx.meta.isBackoffice) {
				return [];
			} else {
				const applications = await ctx.call("volunteering.applications.find", {
					query: {
						user_id: user_id,
					},
					withRelated: false,
				});
				return applications.map((app) => {
					const container = {};
					container.academic_year = app.academic_year;
					container.id = app.id;
					container.user_id = app.user_id;
					return container;
				});
			}
		},

		/**
		 * Function for get user experinces
		 * @param {*} ctx
		 * @param {*} user_id
		 * @returns
		 */
		async getUserExperiences(ctx, user_id) {
			const user_manifest = await ctx.call("volunteering.experience-user-interests.find", {
				query: {
					user_id: user_id,
				},
				withRelated: ["experience"],
			});
			return user_manifest.map((user_interest) => {
				const container = {};
				if (user_interest.experience !== null) {
					if (!ctx.meta.isBackoffice) {
						if (user_interest.experience.experience_responsible_id === ctx.meta.user.id) {
							container.academic_year = user_interest.experience.academic_year;
							container.user_manifest_id = user_interest.id;
							container.user_id = user_interest.user_id;
							container.experience_translations = user_interest.experience.translations;
						}
					} else {
						container.academic_year = user_interest.experience.academic_year;
						container.user_manifest_id = user_interest.id;
						container.user_id = user_interest.user_id;
						container.experience_translations = user_interest.experience.translations;
					}
				}
				return container;
			});
		},

		/**
		 * Function for get currentApplication
		 * @param {*} ctx
		 * @param {*} experience_id
		 * @param {*} user_id
		 * @returns
		 */
		async getCurrentApplication(ctx, experience_id, user_id) {
			const experience = await ctx.call("volunteering.experiences.get", {
				id: experience_id,
				withRelated: false,
			});
			const application = await ctx.call("volunteering.applications.find", {
				query: {
					academic_year: experience[0].academic_year,
					user_id: user_id,
				},
				withRelated: [
					"preferred_activities",
					"preferred_schedule",
					"course_degree",
					"school",
					"course",
					"organic_units",
				],
			});
			if (!ctx.meta.isBackoffice) {
				if (application.length > 0) {
					delete application[0].has_collaborated_last_year;
					delete application[0].previous_activity_description;
					delete application[0].address;
					delete application[0].code_postal;
					delete application[0].nationality;
					delete application[0].location;
					delete application[0].birthdate;
					delete application[0].tin;
					delete application[0].genre;
					delete application[0].identification_number;
					return application[0];
				} else {
					return null;
				}
			} else {
				return application[0];
			}
		},

		/**
		 * Validate exist experience
		 * @param {*} ctx
		 */
		async validateUserInterestExperience(ctx) {
			await ctx.call("volunteering.experiences.get", {
				id: ctx.params.experience_id,
			});
		},

		/**
		 * Validate active application
		 * @param {*} ctx
		 */
		async validateActiveApplication(ctx) {
			const experience = await ctx.call("volunteering.experiences.get", {
				id: ctx.params.experience_id,
			});
			const application = await ctx.call("volunteering.applications.find", {
				query: {
					user_id: ctx.meta.user.id,
					academic_year: experience[0].academic_year,
					status: "ACCEPTED",
				},
			});
			if (application.length === 0) {
				throw new Errors.ValidationError(
					"This user not have application",
					"VOLUNTEERING_USER_NOT_APPLICATION",
					{},
				);
			}
		},

		/**
		 * Validate exist interest
		 * @param {*} ctx
		 */
		async validateInterest(ctx) {
			const alreadyInterested = await this._find(ctx, {
				query: { experience_id: ctx.params.experience_id, user_id: ctx.meta.user.id },
			});
			if (alreadyInterested.length > 0) {
				throw new Errors.ValidationError(
					"The Experience User Interested already exists.",
					"VOLUNTEERING_EXPERIENCE_INTERESTED_ALREADY_EXIST",
					{},
				);
			}
		},

		/**
		 * Save history in create
		 * @param {*} ctx
		 * @param {*} response
		 * @returns
		 */
		async saveCreateHistory(ctx, response) {
			response[0].history = [
				...(await ctx.call("volunteering.user_interest_history.create", {
					user_interest_id: response[0].id,
					status: response[0].status,
					user_id: ctx.meta.user.id,
					notes: null,
				})),
			];
			await this.sendNotification(
				ctx,
				response[0].status,
				response[0].user.id,
				response[0].user.name,
				response[0].uuid,
			);
			return response;
		},

		/**
		 * Send notifications
		 * @param {*} ctx
		 * @param {*} status
		 * @param {*} user_id
		 * @param {*} name
		 * @param {*} notification_gui
		 */
		async sendNotification(ctx, status, user_id, name, notification_gui) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "VOLUNTEERING_MANIFEST_INTEREST_USERS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name },
				variables: {},
				external_uuid: notification_gui,
				medias: [],
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
				return {
						source: `
								if (value < new Date())
										${this.makeError({ type: "dateMin",  actual: "value", messages })}
								return value;
						`
				};
		});*/
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
				return {
						source: `
								if (value < new Date())
										${this.makeError({ type: "dateMin",  actual: "value", messages })}
								return value;
						`
				};
		});*/
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
