"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { USER_INTEREST_STATUS } = require("./utils/constants.js");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.user_interest_history",
	table: "user_interest_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "user_interest_history")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"status",
			"user_id",
			"notes",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			"user"(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
		},
		entityValidator: {
			user_interest_id: { type: "number", integer: true, convert: true },
			status: { type: "enum", values:  Object.keys(USER_INTEREST_STATUS) },
			user_id: { type: "number", integer: true, convert: true },
			notes: { type: "string", optional: true, nullable: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
