"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const ExperiencesStateMachine = require("./state-machines/experiences.machine");
const { EXPERIENCE_STATUS, EXPERIENCE_EVENTS } = require("./utils/constants.js");
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");
const UUIDV4 = require("uuid").v4;
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.experiences",
	table: "experience",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "experiences")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"publish_date",
			"application_deadline_date",
			"academic_year",
			"organic_unit_id",
			"address",
			"experience_responsible_id",
			"number_candidates",
			"number_simultaneous_candidates",
			"start_date",
			"end_date",
			"number_weekly_hours",
			"total_hours_estimation",
			"schedule",
			"holydays_availability",
			"attachment_file_id",
			"uuid",
			"status",
			"return_reason",
			"last_status",
			"published",
			"updated_at",
			"created_at",
			"decision",
			"reject_reason",
		],
		defaultWithRelateds: ["history", "organic_unit", "translations", "experience_responsible"],
		withRelateds: {
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.experience_history",
					"history",
					"id",
					"experience_id",
				);
			},
			organic_unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "organic_unit", "organic_unit_id");
			},
			experience_responsible(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"experience_responsible",
					"experience_responsible_id",
					"id",
					{},
					"name,email,phone",
					false,
				);
			},
			attachment_file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "attachment_file", "attachment_file_id");
			},
			translations(ids, docs, rule, ctx) {
				if (ctx.meta.language_id) {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("volunteering.experience-translations.find", {
									query: { experience_id: doc.id, language_id: ctx.meta.language_id },
								})
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				} else {
					return Promise.all(
						docs.map((doc) => {
							return ctx
								.call("volunteering.experience-translations.find", {
									query: { experience_id: doc.id },
								})
								.then((translation) => {
									doc.translations = translation;
								});
						}),
					);
				}
			},
			users_interest(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("volunteering.experience-user-interests.find", {
								query: {
									experience_id: doc.id,
								},
								withRelated: ["user", "history", "certificate_file", "certificate_generated"],
							})
							.then(async (user_experience) => {
								for (const experience of user_experience) {
									experience.application = await ctx
										.call("volunteering.applications.find", {
											query: {
												academic_year: doc.academic_year,
												user_id: experience.user_id,
											},
											withRelated: false,
										})
										.then((response) => (response.length ? response[0] : null));
								}
								doc.users_interest = user_experience;
							});
					}),
				);
			},
			notifications(ids, docs, rule, ctx) {
				let idss = docs.map((d) => d.uuid);
				idss = idss.filter((id) => id !== null);
				const query = {};
				query.external_uuid = idss;
				return ctx
					.call("notifications.alerts.find", {
						query,
					})
					.then((data) => {
						return docs.map(
							(d) => (d.notifications = data.filter((dt) => dt.external_uuid === d.uuid)),
						);
					});
			},
			external_entity(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.external_entities",
					"external_entity",
					"experience_responsible_id",
					"user_id",
				);
			},
			users_colaboration(ids, experiences, rule, ctx) {
				return Promise.all(
					experiences.map((experience) => {
						return ctx
							.call("volunteering.experience-user-interests.find", {
								query: {
									experience_id: experience.id,
									status: "COLABORATION",
								},
							})
							.then(async (user_colaboration) => {
								for (const colaboration of user_colaboration) {
									colaboration.application = await ctx
										.call("volunteering.applications.find", {
											query: {
												academic_year: experience.academic_year,
												user_id: colaboration.user_id,
											},
											withRelated: false,
										})
										.then((response) => (response.length ? response[0] : null));
								}
								experience.users_colaboration = user_colaboration;
							});
					}),
				);
			},
		},
		entityValidator: {
			publish_date: { type: "date", convert: true },
			application_deadline_date: { type: "date", convert: true },
			academic_year: { type: "string", max: 255 },
			organic_unit_id: { type: "number", integer: true, convert: true },
			address: { type: "string", max: 255 },
			experience_responsible_id: { type: "number", integer: true, convert: true },
			number_candidates: { type: "number", integer: true, convert: true, optional: true },
			number_simultaneous_candidates: {
				type: "number",
				integer: true,
				convert: true,
				optional: true,
			},
			start_date: { type: "date", convert: true },
			end_date: { type: "date", convert: true },
			number_weekly_hours: { type: "number", integer: true, convert: true },
			total_hours_estimation: { type: "number", integer: true, convert: true },
			schedule: { type: "string" },
			holydays_availability: { type: "number", integer: true, convert: true },
			attachment_file_id: { type: "number", integer: true, convert: true, optional: true },
			status: { type: "enum", optional: true, values: EXPERIENCE_STATUS },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			uuid: { type: "string", optional: true, nullable: true },
			last_status: { type: "enum", optional: true, nullable: true, values: EXPERIENCE_STATUS },
			return_reason: { type: "string", optional: true, nullable: true },
			published: { type: "boolean", optional: true, nullable: true, default: true },
			translations: {
				type: "array",
				min: 1,
				items: {
					type: "object",
					props: {
						title: { type: "string", max: 255 },
						proponent_service: { type: "string", max: 255 },
						applicant_profile: { type: "string", max: 255 },
						job: { type: "string", max: 255 },
						description: { type: "string", max: 255 },
						selection_criteria: { type: "string", max: 255 },
						language_id: { type: "number", integer: true, convert: true },
					},
				},
			},
			decision: {
				type: "enum",
				values: ["ACCEPTED", "REJECTED", "RETURNED"],
				nullable: true,
				optional: true,
			},
			reject_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.uuid = UUIDV4();
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.schedule = JSON.stringify(ctx.params.schedule);
					ctx.params.status = "SUBMITTED";
					ctx.params.published = true;
					if (ctx.params.number_weekly_hours > ctx.params.total_hours_estimation) {
						throw new Errors.ValidationError(
							"The 'number weekly hours' need smaller 'total_hours_estimation'",
							"VOLUNTEERING_EXPERIENCE_NUM_WEEKLY_SMALLER",
							{},
						);
					}
					if (!ctx.meta.isBackoffice) {
						ctx.params.experience_responsible_id = ctx.meta.user.id;
					}
					if (ctx.params.start_date < new Date()) {
						this.logger.error("#####-----> Data tem que ser superior a xpto");
					}

					const validate_dates = moment(moment(ctx.params.end_date).format("YYYY-MM-DD")).isAfter(
						moment(ctx.params.start_date).format("YYYY-MM-DD"),
					);
					if (validate_dates === false) {
						throw new Errors.ValidationError(
							"The end_date need is after start_date",
							"VOLUNTEERING_EXPERIENCE_CREATE_ERROR",
							{},
						);
					}
					if (
						moment(ctx.params.application_deadline_date, "YYYY-MM-DD").isAfter(
							moment(ctx.params.end_date).format("YYYY-MM-DD"),
						)
					) {
						throw new Errors.ValidationError(
							"The expiration date must be equal to or less than the duration range",
							"VOLUNTEERING_EXPERIENCE_APPLICATION_DATE_ERROR",
							{},
						);
					}
				},
			],
			update: [
				async function sanatizeParams(ctx) {
					if (ctx.params.number_weekly_hours > ctx.params.total_hours_estimation) {
						throw new Errors.ValidationError(
							"The 'number weekly hours' need smaller 'total_hours_estimation'",
							"VOLUNTEERING_EXPERIENCE_NUM_WEEKLY_SMALLER",
							{},
						);
					}
					const experience = await this._get(ctx, { id: ctx.params.id });
					if (!ctx.meta.isBackoffice) {
						if (experience[0].experience_responsible_id !== ctx.meta.user.id) {
							throw new Errors.ForbiddenError(
								"You are not responsible for the experience",
								"VOLUNTEERING_EXPERIENCE_UPDATE_FORBIDDEN",
								{},
							);
						}
						ctx.params.experience_responsible_id = experience[0].experience_responsible_id;
						if (experience[0].experience_responsible_id === ctx.meta.user.id) {
							ctx.params.status = "SUBMITTED";
						}
					}
					if (ctx.meta.isBackoffice) {
						if (experience[0].experience_responsible_id === ctx.meta.user.id) {
							ctx.params.status = "SUBMITTED";
						}
					}
					ctx.params.updated_at = new Date();
					if (ctx.params.schedule) ctx.params.schedule = JSON.stringify(ctx.params.schedule);
				},
			],
			list: [
				function sanatizeParams(ctx) {
					ctx.params.query = ctx.params.query || {};
					if (ctx.params.query.isAvaliable) {
						ctx.params.query["publish_date"] = {};
						ctx.params.query["publish_date"]["lte"] = moment(new Date()).format("YYYY-MM-DD");
						ctx.params.query["application_deadline_date"] = {};
						ctx.params.query["application_deadline_date"]["gte"] = moment(new Date()).format(
							"YYYY-MM-DD",
						);
						ctx.params.query["published"] = true;
						delete ctx.params.query.isAvaliable;
					}
				},
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["title", "description"],
						"volunteering.experience-translations",
						"experience_id",
					);
				},
			],
		},
		after: {
			create: [
				"saveExperienceStatus",
				async function sanatizeParams(ctx, response) {
					response[0].translations = await this.createWithRelated(
						ctx,
						"volunteering.experience-translations.create",
						response[0].id,
						ctx.params.translations,
					);
					return response;
				},
			],
			update: [
				async function sanatizeParams(ctx, response) {
					await this.deleteWithRelated(
						ctx,
						"volunteering.experience-translations.remove",
						await ctx.call("volunteering.experience-translations.find", {
							query: { experience_id: response[0].id },
						}),
					);
					response[0].translations = await this.createWithRelated(
						ctx,
						"volunteering.experience-translations.create",
						response[0].id,
						ctx.params.translations,
					);
					return response;
				},
			],
			patch: [
				async function sanatizeParams(ctx, response) {
					if (ctx.params.translations) {
						await this.deleteWithRelated(
							ctx,
							"volunteering.experience-translations.remove",
							await ctx.call("volunteering.experience-translations.find", {
								query: { experience_id: response[0].id },
							}),
						);
						response[0].translations = await this.createWithRelated(
							ctx,
							"volunteering.experience-translations.create",
							response[0].id,
							ctx.params.translations,
						);
					}
					return response;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				let experiences_list;
				if (ctx.meta.isGuest) {
					experiences_list = await this._find(ctx, {
						query: (qb) => {
							qb.where(this.adapter.raw("publish_date::date"), "<=", moment().format("YYYY-MM-DD"));
							qb.whereIn("status", ["APPROVED", "SELECTION", "IN_COLABORATION"]);
							qb.where("published", "=", true);
							qb.where(
								this.adapter.raw("application_deadline_date::date"),
								">=",
								moment().format("YYYY-MM-DD"),
							);
							return qb;
						},
						limit: 3,
					});
				}
				if (!ctx.meta.isGuest) {
					let list = await this._find(ctx, {
						query: (qb) => {
							qb.where("experience_responsible_id", "=", ctx.meta.user.id);
							qb.whereIn("status", [
								"APPROVED",
								"SUBMITTED",
								"ANALYSED",
								"SELECTION",
								"IN_COLABORATION",
								"RETURNED",
							]);
							return qb;
						},
						limit: 3,
					});
					if (list.length > 0) {
						experiences_list = list;
					} else {
						experiences_list = await this._find(ctx, {
							query: (qb) => {
								qb.where(
									this.adapter.raw("publish_date::date"),
									"<=",
									moment().format("YYYY-MM-DD"),
								);
								qb.whereIn("status", ["APPROVED", "SELECTION", "IN_COLABORATION"]);
								qb.where("published", "=", true);
								qb.where(
									this.adapter.raw("application_deadline_date::date"),
									">=",
									moment().format("YYYY-MM-DD"),
								);
								return qb;
							},
							limit: 3,
						});
					}
				}
				if (experiences_list.length > 0) {
					return experiences_list;
				} else {
					throw new Errors.ValidationError(
						"Dont exist any published experience",
						"VOLUNTEERING_NO_PUBLISHED_EXPERIENCES",
						{},
					);
				}
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		stats: {
			visibility: "published",
			rest: "GET /stats",
			params: {
				academic_year: { type: "string", optional: true, nullable: true },
			},
			scope: "volunteering:experiences:list",
			async handler(ctx) {
				let academic_year;
				const current_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
				);
				if (!ctx.params.academic_year) {
					if (current_year.length !== 0) {
						academic_year = current_year[0].academic_year;
					} else {
						const validation = new Validator();
						const schema = {
							academic_year: { type: "string" },
						};
						const check = validation.compile(schema);
						const res = check(ctx.params);
						if (res !== true) {
							return Promise.reject(
								new Errors.ValidationError("Entity validation error!", null, res),
							);
						} else {
							academic_year = ctx.params.academic_year;
						}
					}
				} else {
					academic_year = ctx.params.academic_year;
				}
				return this.adapter
					.getDB(this.adapter.table)
					.select("status")
					.count("*")
					.where("academic_year", "=", academic_year)
					.groupBy("status")
					.then((result) => {
						let allStatus = {
							SUBMITTED: 0,
							RETURNED: 0,
							ANALYSED: 0,
							APPROVED: 0,
							PUBLISHED: 0,
							REJECTED: 0,
							SEND_SEEM: 0,
							EXTERNAL_SYSTEM: 0,
							SELECTION: 0,
							IN_COLABORATION: 0,
							CLOSED: 0,
							CANCELED: 0,
							DISPATCH: 0,
						};
						result.map((r) => (allStatus[r.status] = r.count));
						return allStatus;
					});
			},
		},

		"next-available-status": {
			rest: "GET /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
			},
			scope: "volunteering:experiences:list",
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				let stateMachine = await ExperiencesStateMachine.createStateMachine(
					experience[0].status,
					ctx,
				);
				return await stateMachine.transitions();
			},
		},

		status: {
			rest: "POST /:id/status",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				event: {
					type: "enum",
					values: EXPERIENCE_EVENTS,
					optional: true,
					nullable: true,
				},
				notes: { type: "string", optional: true },
				experience: { type: "object", optional: true },
			},
			scope: "volunteering:experiences:status",
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });

				let stateMachine = await ExperiencesStateMachine.createStateMachine(
					experience[0].status,
					ctx,
				);

				// Validate Status
				if (ctx.params.event && stateMachine.cannot(ctx.params.event)) {
					throw new Errors.ValidationError(
						"Unauthorized application status change",
						"VOLUNTEERING_EXPERIENCE_STATUS_ERROR_UNAUTHORIZED",
						{},
					);
				}

				if (experience[0].status === "DISPATCH") {
					const validation = new Validator();
					const schema = {
						decision_dispatch: { type: "enum", values: ["ACCEPT", "REJECT"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						if (ctx.params.decision_dispatch === "ACCEPT") {
							let status = null;
							if (experience[0].decision === "REJECTED") {
								status = "reject";
							}
							if (experience[0].decision === "RETURNED") {
								status = "return";
							}
							if (experience[0].decision === "ACCEPTED") {
								status = "approve";
							}
							const result = await stateMachine[status]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].experience_responsible.id,
								result[0].experience_responsible.name,
								result[0].uuid,
								result[0].translations[0].title,
							);
							this.clearCache();
							return result;
						} else {
							const result = await stateMachine["analyse"]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].experience_responsible.id,
								result[0].experience_responsible.name,
								result[0].uuid,
								result[0].translations[0].title,
							);
							this.clearCache();
							return result;
						}
					}
				}
				if (ctx.params.event.toLowerCase() === "dispatch") {
					const validation = new Validator();
					const schema = {
						decision: { type: "enum", values: ["ACCEPTED", "REJECTED", "RETURNED"] },
					};
					const check = validation.compile(schema);
					const res = check(ctx.params);
					if (res !== true) {
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);
					} else {
						const result = await stateMachine[ctx.params.event.toLowerCase()]();
						await this.sendNotification(
							ctx,
							result[0].status,
							result[0].experience_responsible.id,
							result[0].experience_responsible.name,
							result[0].uuid,
							result[0].translations[0].title,
						);
						return result;
					}
				}

				if (ctx.params.event.toLowerCase() === "colaboration") {
					const colaborations = await ctx.call("volunteering.experience-user-interests.find", {
						query: {
							experience_id: ctx.params.id,
							status: "COLABORATION",
						},
						withRelated: false,
					});
					if (colaborations.length === 0) {
						throw new Errors.ValidationError(
							"Unauthorized experience status change dont have users in colaborations",
							"VOLUNTEERING_EXPERIENCE_NO_USERS_IN_COLABORATION_ERROR",
							{},
						);
					}
				}

				if (ctx.params.event.toLowerCase() === "return") {
					if (ctx.params.experience) {
						if (ctx.params.experience.return_reason) {
							const result = await stateMachine[ctx.params.event.toLowerCase()]();
							await this.sendNotification(
								ctx,
								result[0].status,
								result[0].experience_responsible.id,
								result[0].experience_responsible.name,
								result[0].uuid,
							);
							return result;
						} else {
							throw new Errors.ValidationError(
								"Unauthorized application status change need send return reason",
								"VOLUNTEERING_EXPERIENCE_NEED_RETURN_REASON_ERROR",
								{},
							);
						}
					} else {
						throw new Errors.ValidationError(
							"Unauthorized application status change need send return reason",
							"VOLUNTEERING_EXPERIENCE_NEED_RETURN_REASON_ERROR",
							{},
						);
					}
				}
				const result = await stateMachine[ctx.params.event.toLowerCase()]();
				await this.sendNotification(
					ctx,
					result[0].status,
					result[0].experience_responsible.id,
					result[0].experience_responsible.name,
					result[0].uuid,
				);
				return result;
			},
		},

		"get-users-for-interest": {
			rest: "GET /:id/target-users",
			params: {
				id: { type: "number", integer: true, convert: true },
				school_id: { type: "number", integer: true, convert: true, optional: true },
				course_id: { type: "number", integer: true, convert: true, optional: true },
				course_year: { type: "number", integer: true, convert: true, optional: true },
			},
			visibility: "published",
			scope: "volunteering:experiences:list",
			async handler(ctx) {
				let resp = [];
				const experience = await this._get(ctx, { id: ctx.params.id });

				if (ctx.params.school_id) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["school_id"] = ctx.params.school_id;
				}
				if (ctx.params.course_id) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["course_id"] = ctx.params.course_id;
				}
				if (ctx.params.course_year) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["course_year"] = ctx.params.course_year;
				}
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["academic_year"] = experience[0].academic_year;
				ctx.params.query["status"] = "ACCEPTED";
				const applications = await ctx.call("volunteering.applications.find", ctx.params);
				for (const application of applications) {
					if (application.user !== null) {
						resp.push(application.user);
					}
				}
				return resp;
			},
		},

		"send-notification-user-target": {
			rest: "POST /:id/send-notifications",
			scope: "volunteering:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
				user_ids: { type: "array", items: "number" },
			},
			async handler(ctx) {
				let resp = [];
				const experience = await this._get(ctx, { id: ctx.params.id });
				for (const id of ctx.params.user_ids) {
					const user_info = await ctx.call("authorization.users.get", { id: id });
					this.sendNotificationUsers(
						ctx,
						user_info[0].id,
						user_info[0].name,
						experience[0].translations[0].title,
						experience[0].uuid,
					);
					resp.push(user_info);
				}
				return resp;
			},
		},

		"get-experiences-responsable": {
			rest: "GET /responsable",
			scope: "volunteering:experiences:responsable",
			visibility: "published",
			async handler(ctx) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["experience_responsible_id"] = ctx.meta.user.id;
				const experiences = await ctx.call("volunteering.experiences.find", ctx.params);
				for (const experience of experiences) {
					const count_interests = await ctx.call(
						"volunteering.experience-user-interests.count_interests",
						{ id: experience.id },
					);
					experience.number_interests = count_interests;
				}
				return experiences;
			},
		},

		"change-status-by-manifest-interest": {
			params: {
				id: { type: "number", integer: true, convert: true },
				status: { type: "string" },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				if (ctx.params.status === "IN_COLABORATION") {
					if (experience[0].status !== "IN_COLABORATION") {
						const experience_updated = await ctx.call("volunteering.experiences.patch", {
							id: ctx.params.id,
							status: ctx.params.status,
						});
						await ctx.call("volunteering.experience_history.create", {
							experience_id: ctx.params.id,
							status: ctx.params.status,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
						});
						await this.sendNotification(
							ctx,
							experience_updated[0].status,
							experience_updated[0].experience_responsible.id,
							experience_updated[0].experience_responsible.name,
							experience_updated[0].uuid,
						);
					}
				}
				if (ctx.params.status === "SELECTION") {
					if (experience[0].status !== "SELECTION") {
						const experience_updated = await ctx.call("volunteering.experiences.patch", {
							id: ctx.params.id,
							status: ctx.params.status,
						});
						await ctx.call("volunteering.experience_history.create", {
							experience_id: ctx.params.id,
							status: ctx.params.status,
							user_id: ctx.meta.user.id,
							notes: ctx.params.notes,
						});
						await this.sendNotification(
							ctx,
							experience_updated[0].status,
							experience_updated[0].experience_responsible.id,
							experience_updated[0].experience_responsible.name,
							experience_updated[0].uuid,
						);
					}
				}
			},
		},

		"generate-report-user-experience": {
			rest: "GET /:id/users-manifest",
			visibility: "published",
			scope: "volunteering:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
				status: { type: "string", optional: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				const user_interests = await ctx.call("volunteering.experience-user-interests.find", {
					query: {
						experience_id: ctx.params.id,
					},
					withRelated: ["application"],
				});
				for (let user of user_interests) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}
				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					experience_name: experience[0].translations[0].title,
					experience_responsable: experience[0].experience_responsible.name,
					organic_unit: experience[0].organic_unit.name,
					user_interests: user_interests,
				};
				return ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_EXPERIENCE_USER_INTEREST",
					data: data,
					name: "Ação de voluntariado - Manifestações de interesse",
				});
			},
		},

		"generate-report-user-selection-experience": {
			rest: "GET /:id/users-selection",
			visibility: "published",
			scope: "volunteering:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				const user_interests = await ctx.call("volunteering.experience-user-interests.find", {
					query: {
						experience_id: ctx.params.id,
						status: "COLABORATION",
					},
					withRelated: ["application"],
				});
				for (let user of user_interests) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}
				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					experience_name: experience[0].translations[0].title,
					experience_responsable: experience[0].experience_responsible.name,
					organic_unit: experience[0].organic_unit.name,
					user_interests: user_interests,
				};
				return ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_EXPERIENCE_USER_SELECTION",
					data: data,
					name: "Ação de voluntariado - Alunos selecionados",
				});
			},
		},

		"generate-report-user-interest-selection": {
			rest: "GET /:id/user-manifest-selection",
			visibility: "published",
			scope: "volunteering:experiences:list",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				const user_manifest_interst = await ctx.call(
					"volunteering.experience-user-interests.find",
					{
						query: {
							experience_id: ctx.params.id,
							status: ["SUBMITTED", "ANALYSED", "APPROVED", "WAITING", "NOT_SELECTED"],
						},
						withRelated: ["application"],
					},
				);
				for (let user of user_manifest_interst) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}
				const user_manifest_selection = await ctx.call(
					"volunteering.experience-user-interests.find",
					{
						query: {
							experience_id: ctx.params.id,
							status: "ACCEPTED",
						},
						withRelated: ["application"],
					},
				);

				for (let user of user_manifest_selection) {
					user.created_at = moment(user.created_at).format("YYYY-MM-DD");
				}

				const data = {
					date: moment(new Date()).format("YYYY-MM-DD"),
					experience_name: experience[0].translations[0].title,
					experience_responsable: experience[0].experience_responsible.name,
					organic_unit: experience[0].organic_unit.name,
					user_interests: user_manifest_interst,
					user_interests_selection: user_manifest_selection,
				};

				return ctx.call("reports.templates.print", {
					key: "VOLUNTEERING_EXPERIENCE_USER_INTEREST_AND_SELECTION",
					data: data,
					name: "Acção de voluntariado - lista de manifestações de interesse-seleção",
				});
			},
		},

		"set-last-status": {
			rest: "POST /:id/last-status",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			scope: "volunteering:experiences:update",
			visibility: "published",
			async handler(ctx) {
				const experience = await this._get(ctx, { id: ctx.params.id });
				if (experience[0].last_status !== null) {
					const result = await ctx.call("volunteering.experiences.patch", {
						id: ctx.params.id,
						status: experience[0].last_status,
						last_status: experience[0].status,
					});
					await ctx.call("volunteering.experience_history.create", {
						experience_id: ctx.params.id,
						status: experience[0].last_status,
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].experience_responsible.id,
						result[0].experience_responsible.name,
						result[0].uuid,
					);
					return result;
				} else {
					const result = await ctx.call("volunteering.experiences.patch", {
						id: ctx.params.id,
						status: "ANALYSED",
						last_status: experience[0].status,
					});
					await ctx.call("volunteering.experience_history.create", {
						experience_id: ctx.params.id,
						status: "ANALYSED",
						user_id: ctx.meta.user.id,
						notes: ctx.params.notes,
					});
					await this.sendNotification(
						ctx,
						result[0].status,
						result[0].experience_responsible.id,
						result[0].experience_responsible.name,
						result[0].uuid,
					);
					return result;
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"volunteering.experience-user-interests.*"() {
			this.logger.info("CAPTURED EVENT => volunteering.experience-user-interests.updated");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Save with related entities.
		 * Receive the create service of object type, foreign key, an array of objects.
		 * Returns saved entities list
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} experience_id
		 * @param {*} list
		 */
		async createWithRelated(ctx, service, experience_id, list) {
			if (!(list instanceof Array)) {
				return [];
			}
			const listToReturn = [];
			for (const item of list) {
				item.experience_id = experience_id;
				const created = await ctx.call(service, item);
				listToReturn.push(created[0]);
			}
			return listToReturn;
		},

		/**
		 * Delete with related entities.
		 * Receive the delete service of object type, an array of objects.
		 * Passed list is removed.
		 * @param {*} ctx
		 * @param {*} service
		 * @param {*} list
		 */
		async deleteWithRelated(ctx, service, list) {
			if (!(list instanceof Array)) {
				return;
			}
			for (const item of list) {
				await ctx.call(service, item);
			}
		},

		/**
		 * Save experience history created
		 * @param {*} ctx
		 * @param {*} res
		 * @returns
		 */
		async saveExperienceStatus(ctx, res) {
			if (res[0].id != null) {
				res[0].history.push(
					await ctx.call("volunteering.experience_history.create", {
						experience_id: res[0].id,
						status: "SUBMITTED",
						user_id: ctx.meta.user.id,
						notes: null,
					}),
				);
			}
			return res;
		},

		/**
		 * send notifications for responsable of experience
		 * @param {*} ctx
		 * @param {*} status
		 * @param {*} user_id
		 * @param {*} name
		 * @param {*} uuid
		 */
		async sendNotification(ctx, status, user_id, name, uuid) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "VOLUNTEERING_EXPERIENCE_STATUS_" + status.toUpperCase(),
				user_id: user_id,
				user_data: {},
				data: { name: name },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});
		},

		/**
		 * Send notifications for users
		 * @param {*} ctx
		 * @param {*} user_id
		 * @param {*} name
		 * @param {*} experience_title
		 * @param {*} uuid
		 * @returns
		 */
		async sendNotificationUsers(ctx, user_id, name, experience_title, uuid) {
			return await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "VOLUNTEERING_EXPERIENCE_SEND_TARGET_USERS",
				user_id: user_id,
				user_data: {},
				data: { name: name, title: experience_title },
				variables: {},
				external_uuid: uuid,
				medias: [],
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}

					return value;
				`
			};
		});*/
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		/*this.broker.validator.add("dateMinGENow", function({ schema, messages }, path, context) {
			return {
				source: `
					if (value < new Date())
						${this.makeError({ type: "dateMin",  actual: "value", messages })}

					return value;
				`
			};
		});*/
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
