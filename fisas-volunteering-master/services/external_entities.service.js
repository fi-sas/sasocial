"use strict";
const { Errors } = require("@fisas/ms_core").Helpers;
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "volunteering.external_entities",
	table: "external_entity",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "external_entities")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"email",
			"gender",
			"address",
			"postal_code",
			"city",
			"country",
			"phone",
			"tin",
			"status",
			"active",
			"user_id",
			"file_id",
			"function_description",
			"description",
		],
		defaultWithRelateds: ["user", "file"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			email: { type: "string" },
			birth_date: { type: "date", convert: true, optional: true },
			gender: { type: "enum", values: ["M", "F", "U"] },
			address: { type: "string" },
			postal_code: { type: "string" },
			city: { type: "string" },
			country: { type: "string" },
			phone: { type: "string" },
			tin: { type: "string" },
			status: { type: "enum", values: ["CREATED", "VALIDATED"] },
			active: { type: "boolean", default: false },
			user_id: { type: "number", integer: true, convert: true, optional: true, nullable: true },
			file_id: { type: "number", integer: true, convert: true, optional: true },
			function_description: { type: "string" },
			description: { type: "string" },
		},
	},

	hooks: {
		before: {
			create: [
				"validateEmail",
				"validateEmailAuthorization",
				async function sanatizeParams(ctx) {
					ctx.params.status = "CREATED";
				},
			],
			patch: [
				async function sanatizeParams(ctx) {
					await this._get(ctx, { id: ctx.params.id }).then((user) => {
						if (user.length && user[0].status === "VALIDATED") {
							if (ctx.params.email) {
								delete ctx.params.email;
							}
						}
					});
				},
			],
		},
		after: {
			patch: [
				async function updateAuthorization(ctx, res) {
					if (res[0].status === "VALIDATED") {
						await ctx
							.call("authorization.users.patch", {
								id: res[0].user_id,
								name: res[0].name,
								gender: res[0].gender,
								address: res[0].address,
								postal_code: res[0].postal_code,
								city: res[0].city,
								country: res[0].country,
								phone: res[0].phone,
								tin: res[0].tin,
							})
							.then(() => {
								return this._get(ctx, { id: res[0].id });
							});
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		inactive: {
			rest: "POST /:id/inactive",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "volunteering:external_entities:update",
			async handler(ctx) {
				const external_entity = await this._get(ctx, { id: ctx.params.id });
				if (external_entity[0].status === "CREATED") {
					throw new Errors.ValidationError(
						"External entity has not validated",
						"VOLUNTEERING_EXTERNAL_ENTITY_HAS_NOT_VALIDATED",
						{},
					);
				}
				await ctx.call("volunteering.external_entities.patch", {
					id: ctx.params.id,
					active: false,
				});
				return ctx
					.call("authorization.users.change_status", {
						user_id: external_entity[0].user_id,
						active: false,
					})
					.then(() => {
						this.clearCache();
						return this._get(ctx, { id: ctx.params.id });
					});
			},
		},

		active: {
			rest: "POST /:id/active",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "volunteering:external_entities:update",
			async handler(ctx) {
				const external_entity = await this._get(ctx, { id: ctx.params.id });
				if (external_entity[0].status === "CREATED") {
					throw new Errors.ValidationError(
						"External entity has not validated",
						"VOLUNTEERING_EXTERNAL_ENTITY_HAS_NOT_VALIDATED",
						{},
					);
				}
				await ctx.call("volunteering.external_entities.patch", { id: ctx.params.id, active: true });
				return ctx
					.call("authorization.users.change_status", {
						user_id: external_entity[0].user_id,
						active: true,
					})
					.then(() => {
						this.clearCache();
						return this._get(ctx, { id: ctx.params.id });
					});
			},
		},

		validate: {
			rest: "POST /:id/validate",
			params: {
				id: { type: "number", integer: true, convert: true },
			},
			visibility: "published",
			scope: "volunteering:external_entities:update",
			async handler(ctx) {
				const external_entetie = await this._get(ctx, { id: ctx.params.id });
				if (external_entetie[0].status === "VALIDATED") {
					throw new Errors.ValidationError(
						"External entity already is validated",
						"VOLUNTEERING_EXTERNAL_ENTITY_ALREADY_IS_VALIDATED",
						{},
					);
				}
				let user = {};
				user.user_name = external_entetie[0].email;
				user.name = external_entetie[0].name;
				user.email = external_entetie[0].email;
				user.birth_date = user.birth_date ? user.birth_date : new Date();
				user.gender = external_entetie[0].gender;
				user.address = external_entetie[0].address;
				user.postal_code = external_entetie[0].postal_code;
				user.city = external_entetie[0].city;
				user.country = external_entetie[0].country;
				user.phone = external_entetie[0].phone;
				user.tin = external_entetie[0].tin;
				user.can_access_BO = false;
				user.external = true;
				user.profile_id = await this.getProfileId(ctx);
				user.active = true;
				return await ctx
					.call("authorization.users.create", user)
					.then((user_created) => {
						this.clearCache();
						return ctx.call("volunteering.external_entities.patch", {
							id: ctx.params.id,
							user_id: user_created[0].id,
							status: "VALIDATED",
							active: true,
						});
					})
					.catch((err) => {
						throw new Errors.ValidationError(
							err,
							"VOLUNTEERING_EXTERNAL_ENTITY_CREATE_USER_ERROR",
							{},
						);
					});
			},
		},
		update: {
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateEmailAuthorization(ctx) {
			const checkEmailAuth = await ctx.call("authorization.users.find", {
				query: {
					email: ctx.params.email,
				},
			});
			if (checkEmailAuth.length) {
				throw new Errors.ValidationError(
					"The email already exist.",
					"VOLUNTEERING_EXTERNAL_ENTITY_EMAIL_ALREADY_EXIST",
					{},
				);
			}
		},

		async validateEmail(ctx) {
			const find_email = await this._find(ctx, {
				query: {
					email: ctx.params.email,
				},
			});
			if (find_email.length > 0) {
				throw new Errors.ValidationError(
					"The email already exist.",
					"VOLUNTEERING_EXTERNAL_ENTITY_EMAIL_ALREADY_EXIST",
					{},
				);
			}
		},

		makeid(length) {
			let result = "";
			const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			const charactersLength = characters.length;
			for (let i = 0; i < length; i++) {
				result += characters.charAt(Math.floor(Math.random() * charactersLength));
			}
			return result;
		},
		async getProfileId(ctx) {
			const profile = await ctx.call("volunteering.configurations.getExternalEntityProfileId");
			if (profile.length > 0) {
				return +profile[0].value;
			}
			throw new Errors.ValidationError(
				"The configuration for external profile doesn't found.",
				"VOLUNTEERING_CONFIGURATION_PROFILEID_NOT_FOUND",
				{},
			);
		},
		async sendNotification(ctx, user_id, name, email, password) {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "VOLUNTEERING_EXTERNAL_ENTITY_CREATED",
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { name: name, email: email, password: password },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
