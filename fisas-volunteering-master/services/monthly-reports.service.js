"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const moment = require("moment");
moment.locale("pt");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.monthly-reports",
	table: "experience_monthly_report",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "monthly-reports")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"user_interest_id",
			"month",
			"year",
			"file_id",
			"report",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			}
		},
		entityValidator: {
			user_interest_id: { type: "number", integer: true, convert: true },
			month: { type: "number", integer: true, convert: true },
			year: { type: "number", integer: true, convert: true },
			report: { type: "string" },
			file_id: { type: "number", integer: true, convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true }
		}
	},
	hooks: {
		before: {
			create: ["validateUserInterest",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					const monthly_report = await this._find(ctx, {
						query: {
							user_interest_id: ctx.params.user_interest_id,
							month: ctx.params.month,
							year: ctx.params.year,
						}
					});
					if (monthly_report.length > 0) {
						throw new Errors.ValidationError(
							"Already exist monthly report for send month, year and user_interest",
							"VOLUNTEERING_MONTHLY_REPORT_CREATE_ERROR",
							{},
						);
					}
				}
			],
			update: ["validateUserInterest",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			],
			list: [
				async function validateAccess(ctx){
					if(!ctx.meta.isBackoffice) {
						if (ctx.params.query) {
							if (ctx.params.query.user_interest_id) {
								const user_interest = await ctx.call("volunteering.experience-user-interests.get", { id: ctx.params.query.user_interest_id, withRelated: "experience" });
								if (user_interest[0].user_id !== ctx.meta.user.id && user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id) {
									throw new Errors.ForbiddenError(
										"Unauthorized access to data",
										"VOLUNTEERING_MONTHLY_REPORT_FORBIDDEN",
										{},
									);
								}
							}
							if(!ctx.params.query.user_interest_id) {
								throw new Errors.ForbiddenError(
									"Unauthorized access to data",
									"VOLUNTEERING_MONTHLY_REPORT_FORBIDDEN",
									{},
								);
							}
						}
						if(!ctx.params.query) {
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"VOLUNTEERING_MONTHLY_REPORT_FORBIDDEN",
								{},
							);
						}
					}
				}
			]
		},
		after: {
			get: [
				async function validateAccess(ctx, resp) {
					if(!ctx.meta.isBackoffice) {
						const user_interest = await ctx.call("volunteering.experience-user-interests.get", { id: resp[0].user_interest_id, withRelated: "experience" });
						if(user_interest[0].user_id !== ctx.meta.user.id && user_interest[0].experience.experience_responsible_id !== ctx.meta.user.id){
							throw new Errors.ForbiddenError(
								"Unauthorized access to data",
								"VOLUNTEERING_MONTHLY_REPORT_FORBIDDEN",
								{},
							);
						}
					}
					return resp;
				}
			]

		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#isBackoffice"
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "#isBackoffice", "mapping"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		generateReport: {
			visibility: "published",
			rest: "GET /generate/:experience_id/:user_interest_id/:start_date/:end_date",
			scope: "volunteering:monthly-reports:list",
			params: {
				experience_id: "number|integer|convert",
				user_interest_id: "number|integer|convert",
				start_date: "date|convert",
				end_date: "date|convert",
			},
			async handler(ctx) {
				let experience = await this.getExperience(ctx, ctx.params.experience_id);

				let experience_user = await ctx.call("volunteering.experience-user-interests.get", { id: ctx.params.user_interest_id });

				const application_user = await ctx.call("volunteering.applications.find", {
					query: {
						academic_year: experience.academic_year,
						status: "ACCEPTED",
						user_id: experience_user[0].user_id
					}
				});

				let application = await this.getApplication(ctx, application_user[0].id);

				let attendances = await this.getAttendances(ctx, experience_user[0].id);

				let all_attendances = await this.getAllAttendances(ctx, experience_user[0].id);

				await this.setMonthAndDayAttendancesHours(attendances);
				let totalHours_validated = this.calculateAttendancesHours(attendances);

				let totalHours = this.calculateAttendancesHours(all_attendances);

				const data = {
					experience: experience,
					application: application,
					attendances: attendances,
					all_attendances: all_attendances,
					totalHoursValidated: totalHours_validated,
					totalHours: totalHours,
					date: moment().format("DD/MM/YYYY"),
				};
				return data;
			},
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

		/**
		 * Function for validate user interest
		 * @param {*} ctx
		 */
		async validateUserInterest(ctx) {
			await ctx.call("volunteering.experience-user-interests.get", {
				id: ctx.params.user_interest_id
			});
		},

		/**
		 * Get the application by id
		 * @param {object} ctx
		 */
		async getApplication(ctx, id) {
			let application = await ctx.call("volunteering.applications.get", { id: id, withRelated: ["course", "user"] });
			return application[0];
		},

		/**
		 * Get the experience from the application
		 * @param {object} ctx
		 * @param {integer} experience_id
		 */
		async getExperience(ctx, experience_id) {
			if (experience_id !== null) {
				let experience = await ctx.call("volunteering.experiences.get", { id: experience_id, withRelated: ["organic_unit", "experience_responsible", "translations"] });
				return this.filterExperienceData(_.first(experience));
			} else {
				throw new Errors.ValidationError(
					"The application don't have assigned experience!",
					"APPLICATION_WITHOUT_EXPERIENCE",
					{
						application_id: ctx.params.application_id,
					},
				);
			}
		},

		/**
		 * Get the attendances from the application id
		 * @param {object} ctx
		 */
		async getAttendances(ctx, user_interest_id) {
			return ctx.call("volunteering.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: ctx.params.start_date,
				status: ["ACCEPTED", "REJECTED"],
				end_date: moment(ctx.params.end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},

		/**
		 * Function for get all attendances
		 * @param {*} ctx
		 * @param {*} user_interest_id
		 * @returns
		 */
		async getAllAttendances(ctx, user_interest_id) {
			return ctx.call("volunteering.attendances.attendancesBetweenDates", {
				user_interest_id: user_interest_id,
				start_date: ctx.params.start_date,
				status: ["ACCEPTED", "REJECTED", "PENDING"],
				end_date: moment(ctx.params.end_date)
					.add(23, "hours")
					.add(59, "minutes")
					.add(59, "seconds")
					.toISOString(),
				was_present: true,
			});
		},

		filterExperienceData(experience) {
			let experienceData = {};
			let organicUnit = {};
			let experienceResponsible = {};

			experienceData = Object.assign(
				experienceData,
				this.getObjectFields(experience, [
					"id",
					"translations",
					"academic_year",
					"address",
					"total_hours_estimation",
					"number_weekly_hours",
					"start_date",
					"end_date",
				]),
			);

			organicUnit = Object.assign(
				organicUnit,
				this.getObjectFields(experience.organic_unit, ["id", "name", "code"]),
			);

			experienceResponsible = Object.assign(
				experienceResponsible,
				this.getObjectFields(experience.experience_responsible, [
					"id",
					"name",
					"email",
					"phone",
					"gender",
				]),
			);

			experienceData.organic_unit = organicUnit;
			experienceData.experience_responsible = experienceResponsible;
			experienceData.start_date = moment(experience.start_date).format("DD/MM/YYYY");
			experienceData.end_date = moment(experience.end_date).format("DD/MM/YYYY");
			experienceData.payment_value = experience.payment_value;

			return experienceData;
		},

		/**
		 * Set month in text and day from the date attendance
		 * @param {array} attendances
		 */
		async setMonthAndDayAttendancesHours(attendances) {
			return attendances.forEach((attendance) => {
				attendance.month = moment(attendance.date).format("MMMM").toUpperCase();
				attendance.day = moment(attendance.date).format("DD");
			});
		},

		/**
		 * Calculate the total hours of attendances
		 * @param {array} attendances
		 */
		calculateAttendancesHours(attendances) {
			return _.sumBy(["n_hours"], _.partial(_.sumBy, attendances));
		},

		/**
		 * Gets the object properties selected in the fields
		 * @param {object} object
		 * @param {array} fields
		 */
		getObjectFields(object, fields) {
			let newObject = {};
			fields.forEach((key) => (newObject[key] = object[key]));
			return newObject;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
