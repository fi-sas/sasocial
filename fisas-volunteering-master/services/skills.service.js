"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "volunteering.skills",
	table: "skill",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("volunteering", "skills")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "updated_at", "created_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"volunteering.skill_translations",
					"translations",
					"id",
					"skill_id",
				);
			},
		},
		entityValidator: {
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
			translations: {
				type: "array", items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						description: { type: "string" },
					},
				},
			}
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["description"],
						"volunteering.skill_translations",
						"skill_id",
					);
				},
			]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			//timeout: 0,
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateLanguage(ctx){
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		async saveTranslations(ctx, res) {
			ctx.params.skill_id = res[0].id;
			res[0].translations = await ctx.call("volunteering.skill_translations.save_translations", ctx.params);
			return res;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
