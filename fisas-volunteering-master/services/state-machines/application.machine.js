let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			//update status to "em analise"
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DECLINED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CANCELLED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "EXPIRED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DISPATCH", to: "ANALYSED" },

			//update status to "rejeitada"
			{ name: "DECLINE", from: "DISPATCH", to: "DECLINED" },

			//update status to "cancelada"
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ACCEPTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" },

			//update status to "aceite"
			{ name: "ACCEPT", from: "DISPATCH", to: "ACCEPTED" },

			// update status to "expirada"
			{ name: "EXPIRE", from: "ACCEPTED", to: "EXPIRED" },

			// update status to "dispatch"
			{ name: "DISPATCH", from: "ANALYSED", to: "DISPATCH" },
		],
		methods: {
			onAfterAnalyse: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterCancel: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterAccept: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterDecline: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterRetry: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterExpire: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			onAfterDispatch: async function (lifecycle) {
				return await this.saveApplication(lifecycle);
			},
			saveApplication: async function (lifecycle) {
				await ctx.call("volunteering.applications.patch", {
					...ctx.params.application,
					id: ctx.params.id,
					status: lifecycle.to,
					decision: ctx.params.decision,
					reject_reason: ctx.params.reject_reason,
					last_status: lifecycle.from,
				});
				await ctx.call("volunteering.application_history.create", {
					application_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});

				return await ctx.call("volunteering.applications.get", { id: ctx.params.id });
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
