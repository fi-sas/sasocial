let StateMachine = require("javascript-state-machine");

function createStateMachine(currentState, ctx) {
	return new StateMachine({
		init: currentState,
		transitions: [
			//update status to "em analise"
			{ name: "ANALYSE", from: "SUBMITTED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DECLINED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CANCELLED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "CLOSED", to: "ANALYSED" },
			//{ name: "ANALYSE", from: "EXPIRED", to: "ANALYSED" },
			{ name: "ANALYSE", from: "DISPATCH", to: "ANALYSED" },
			{ name: "ANALYSE", from: "NOT_SELECTED", to: "ANALYSED" }, // New
			{ name: "ANALYSE", from: "WAITING", to: "ANALYSED" }, // New

			//update status to "aprovada"
			//{ name: "APPROVE", from: "WAITING", to: "APPROVED" },
			{ name: "APPROVE", from: "DISPATCH", to: "APPROVED" },

			//update status to "em fila de espera"
			//{ name: "WAITING", from: "ANALYSED", to: "WAITING" },
			{ name: "WAITING", from: "DISPATCH", to: "WAITING" }, // New

			//update status to "nao selecionado"
			//{ name: "NOTSELECT", from: "WAITING", to: "NOT_SELECTED" },
			//{ name: "NOTSELECT", from: "ANALYSED", to: "NOT_SELECTED" },
			{ name: "NOTSELECT", from: "DISPATCH", to: "NOT_SELECTED" }, // New

			//update status to "aceite"
			{ name: "ACCEPT", from: "APPROVED", to: "ACCEPTED" },

			//update status to "em participação"
			{ name: "COLABORATION", from: "ACCEPTED", to: "COLABORATION" },
			{ name: "COLABORATION", from: "WITHDRAWAL", to: "COLABORATION" },
			{ name: "COLABORATION", from: "CLOSED", to: "COLABORATION" },

			// update status to "fechada"
			{ name: "CLOSE", from: "COLABORATION", to: "CLOSED" },
			{ name: "CLOSE", from: "WITHDRAWAL", to: "CLOSED" }, // New
			{ name: "CLOSE", from: "NOT_SELECTED", to: "CLOSED" }, // New
			{ name: "CLOSE", from: "WAITING", to: "CLOSED" }, // New

			//update status to "em desistencia"
			{ name: "WITHDRAWAL", from: "COLABORATION", to: "WITHDRAWAL" },

			//update status to "desistecia aceite"
			{ name: "WITHDRAWALACCEPTED", from: "WITHDRAWAL", to: "WITHDRAWAL_ACCEPTED" },

			//update status to "rejeitada"
			{ name: "DECLINE", from: "DISPATCH", to: "DECLINED" },

			//update status to "cancelada"
			{ name: "CANCEL", from: "SUBMITTED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ANALYSED", to: "CANCELLED" },
			{ name: "CANCEL", from: "WAITING", to: "CANCELLED" },
			{ name: "CANCEL", from: "APPROVED", to: "CANCELLED" },
			{ name: "CANCEL", from: "ACCEPTED", to: "CANCELLED" },
			//{ name: "CANCEL", from: "COLABORATION", to: "CANCELLED" },
			//{ name: "CANCEL", from: "DISPATCH", to: "CANCELLED" },

			// update status to "despacho"
			{ name: "DISPATCH", from: "ANALYSED", to: "DISPATCH" },
		],
		methods: {
			onAfterAnalyse: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterApprove: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterAccept: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterColaboration: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterWaiting: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterNotselected: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterDecline: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterWithdrawal: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterCancel: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterClose: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterWithdrawalaccepted: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			onAfterDispatch: async function (lifecycle) {
				return await this.saveUserInterest(lifecycle);
			},
			saveUserInterest: async function (lifecycle) {
				await ctx.call("volunteering.experience-user-interests.patch", {
					...ctx.params.manifest_interest,
					id: ctx.params.id,
					status: lifecycle.to,
					decision: ctx.params.decision,
					reject_reason: ctx.params.reject_reason,
					last_status: lifecycle.from,
				});
				await ctx.call("volunteering.user_interest_history.create", {
					user_interest_id: ctx.params.id,
					status: lifecycle.to,
					user_id: ctx.meta.user.id,
					notes: ctx.params.notes,
				});

				return await ctx.call("volunteering.experience-user-interests.get", { id: ctx.params.id });
			},
		},
	});
}

exports.createStateMachine = createStateMachine;
