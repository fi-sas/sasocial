## [1.129.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.5...v1.129.6) (2022-06-23)


### Bug Fixes

* **private_accommodation:** fix complaint path ([90f6f1b](https://gitlab.com/fi-sas/fisas-webpage/commit/90f6f1b590a80388d82244f38ebb64408cd411c0))

## [1.129.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.4...v1.129.5) (2022-06-21)


### Bug Fixes

* **accommodation:** fix application status label ([2c6a5ae](https://gitlab.com/fi-sas/fisas-webpage/commit/2c6a5aea2d1d614423563803c50a32d265f585b5))

## [1.129.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.3...v1.129.4) (2022-06-21)


### Bug Fixes

* **core:** lateral menus validations ([9c078f9](https://gitlab.com/fi-sas/fisas-webpage/commit/9c078f9ee4e3905b6dfa65ea2efe587343addb74))

## [1.129.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.2...v1.129.3) (2022-06-20)


### Bug Fixes

* **food:** fix dropdown text ([3d90621](https://gitlab.com/fi-sas/fisas-webpage/commit/3d90621387653520b55c3765526493909fbac2a3))

## [1.129.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.1...v1.129.2) (2022-06-20)


### Bug Fixes

* **accommodation:** filling application user info ([cf19326](https://gitlab.com/fi-sas/fisas-webpage/commit/cf19326094ea785fed27f2af5e15a0f946432f2d))

## [1.129.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.129.0...v1.129.1) (2022-06-08)


### Bug Fixes

* **core:** remove accomodation submenus ([8808a50](https://gitlab.com/fi-sas/fisas-webpage/commit/8808a50788893ca970855eee62bd27779b6625ea))

# [1.129.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.128.3...v1.129.0) (2022-06-08)


### Bug Fixes

* **calendar:** saving terms text ([68d4919](https://gitlab.com/fi-sas/fisas-webpage/commit/68d4919ac4339b041e803137d2c96d625567b4a3))


### Features

* **calendar:** show calendar events ([4351232](https://gitlab.com/fi-sas/fisas-webpage/commit/4351232489714275944acc5f473d4ede3aef9baa))
* **calendar:** show events in calendar ([710a541](https://gitlab.com/fi-sas/fisas-webpage/commit/710a54129b9de6c484b66be435011a1dfd4f0954))
* **calendar:** show labels ([2fd30e2](https://gitlab.com/fi-sas/fisas-webpage/commit/2fd30e29a2d81383d119fe9a6de3683b59833a46))
* **events:** center event recurrency info ([65a9b6a](https://gitlab.com/fi-sas/fisas-webpage/commit/65a9b6ab1d0e896bfd4b017137ac9656ef614e35))

## [1.128.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.128.2...v1.128.3) (2022-06-06)


### Bug Fixes

* **scholarship:** fix offers pagination ([47cb198](https://gitlab.com/fi-sas/fisas-webpage/commit/47cb1980ab14d0e2330816158778c2271ccfc363))
* **volunteering:** fix offers pagination ([67c881e](https://gitlab.com/fi-sas/fisas-webpage/commit/67c881e2b7f7dfac9992280bf44acffc3374ceea))

## [1.128.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.128.1...v1.128.2) (2022-06-03)


### Bug Fixes

* **accommodation:** change renew buttons texts ([bcdcda9](https://gitlab.com/fi-sas/fisas-webpage/commit/bcdcda98f091893fdc4886b0925eef9b2e956db1))

## [1.128.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.128.0...v1.128.1) (2022-06-02)


### Bug Fixes

* **accommodation:** fix data picker communications ([a02c399](https://gitlab.com/fi-sas/fisas-webpage/commit/a02c399789803fb128c4d2c707020c4a0466be05))
* **sasocial:** fix description error ([37116d2](https://gitlab.com/fi-sas/fisas-webpage/commit/37116d2fe3e4e7c765d8cd93b2907f278eb58982))
* **ubike:** fix component properties ([984f150](https://gitlab.com/fi-sas/fisas-webpage/commit/984f1506d8e42fc787f3a8cfd53a6b649d46df4c))

# [1.128.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.127.0...v1.128.0) (2022-06-01)


### Bug Fixes

* **accommodation:** add form dependencies and corret texts ([e4c59a8](https://gitlab.com/fi-sas/fisas-webpage/commit/e4c59a8e01ca4d1cd414ae8bbee10cd3a16fbb68))


### Features

* **accommodation:** new ui application tabs system ([22c6c24](https://gitlab.com/fi-sas/fisas-webpage/commit/22c6c24dba4bc41a07b3b31e58c61d2f9528e17b))

# [1.127.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.126.0...v1.127.0) (2022-06-01)


### Bug Fixes

* **ubike:** missing translations added ([722a86e](https://gitlab.com/fi-sas/fisas-webpage/commit/722a86e7bbfd9ec7acadc29dcf92ec1af4cdbfac))
* **ubike:** show only the files already sent ([dc6ee8f](https://gitlab.com/fi-sas/fisas-webpage/commit/dc6ee8fa52c522de1263328a30f2ccb88136d9c6))


### Features

* **ubike:** allow consult report files ([397a94c](https://gitlab.com/fi-sas/fisas-webpage/commit/397a94c46c58152a6227accd8143a88fb9e47ec1))
* **ubike:** new reports action ([e803647](https://gitlab.com/fi-sas/fisas-webpage/commit/e8036477a56b939e1b8466be36d0327b74c33479))
* **ubike:** show and submit consent and delivery report ([26988e1](https://gitlab.com/fi-sas/fisas-webpage/commit/26988e13b77decf23be570be4a4b35a368b0cf10))

# [1.126.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.125.0...v1.126.0) (2022-06-01)


### Bug Fixes

* **accommodation:** remove console log ([bac1c8a](https://gitlab.com/fi-sas/fisas-webpage/commit/bac1c8a401afc71e8e4fe5b03097eab5b6b987ad))
* **accommodation:** show only residences available for application ([254d33d](https://gitlab.com/fi-sas/fisas-webpage/commit/254d33dfc6f93150691ec92ad1d7d4ee27ff0c49))


### Features

* **accommodation:** disable admition date ([991ec02](https://gitlab.com/fi-sas/fisas-webpage/commit/991ec024b48c3bb754fe71d7e4ee4de7fe0806f7))
* **accommodation:** improve renew and update application ([60541df](https://gitlab.com/fi-sas/fisas-webpage/commit/60541dfe9fdf0cd11d6382b7f220cd8a85bb73f6))

# [1.125.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.124.1...v1.125.0) (2022-06-01)


### Bug Fixes

* **mobility:** fix get current year ([0890701](https://gitlab.com/fi-sas/fisas-webpage/commit/08907015cdfff15e76897589c874a4329218d9bc))


### Features

* **mobility:** show tabs for navigation ([b29a13d](https://gitlab.com/fi-sas/fisas-webpage/commit/b29a13dd54b9258c19a52fa4532fc099904f9777))

## [1.124.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.124.0...v1.124.1) (2022-05-30)


### Bug Fixes

* **accommodation:** on renew prefill admission date ([4e99976](https://gitlab.com/fi-sas/fisas-webpage/commit/4e99976af3930dcc436950f2733a6ce6f0e159ad))

# [1.124.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.123.0...v1.124.0) (2022-05-30)


### Features

* **accommodation:** add hour to exit and entry communication ([0cb769b](https://gitlab.com/fi-sas/fisas-webpage/commit/0cb769b8d89b109a1aaea4f3d764884ec7a8b9eb))
* **accommodation:** application renew improvement ([16eddb7](https://gitlab.com/fi-sas/fisas-webpage/commit/16eddb7036922b3dfb5bc72b983457f5f9e8f0ab))

# [1.123.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.122.0...v1.123.0) (2022-05-26)


### Features

* **bus:** change buttons label ([decb38e](https://gitlab.com/fi-sas/fisas-webpage/commit/decb38e42ce5e7f6fddda1fb5dcb6013a2c73590))

# [1.122.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.121.1...v1.122.0) (2022-05-24)


### Features

* **ssocial:** improve widget size ([fe564af](https://gitlab.com/fi-sas/fisas-webpage/commit/fe564af49b514227810722cdd1bbd5d8e8fbde50))

## [1.121.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.121.0...v1.121.1) (2022-05-23)


### Bug Fixes

* **dashboard:** fix external services link ([68a08ea](https://gitlab.com/fi-sas/fisas-webpage/commit/68a08ea34a7a26c9b2d560d587e7d49dc63a74c9))

# [1.121.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.120.1...v1.121.0) (2022-05-18)


### Bug Fixes

* **emergency:** fix application change request ([15dea23](https://gitlab.com/fi-sas/fisas-webpage/commit/15dea23066fa317a9fbdee74f631e09acc4cf995))


### Features

* **ubike:** add preferred pickup location ([5dd29ea](https://gitlab.com/fi-sas/fisas-webpage/commit/5dd29eafb7716611e7b74a6baeebb9f934cda5ac))

## [1.120.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.120.0...v1.120.1) (2022-05-18)


### Bug Fixes

* **alimentation:** update generic allergens text message ([7a6e346](https://gitlab.com/fi-sas/fisas-webpage/commit/7a6e3461bfb23712dfc2a4e02ef0131de3144a85))

# [1.120.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.119.2...v1.120.0) (2022-05-13)


### Bug Fixes

* **accommodation:** removing unecessary ms call ([26de823](https://gitlab.com/fi-sas/fisas-webpage/commit/26de82372a12c49eee94d26a790a282032153f7a))


### Features

* **accommodation:** renew action improvement ([2d5b52a](https://gitlab.com/fi-sas/fisas-webpage/commit/2d5b52a7dfcca3409647d7da61fad087f65f5893))
* **accommodation:** renew application button and dialog ([706f741](https://gitlab.com/fi-sas/fisas-webpage/commit/706f74135aacd29466c13fc164206ca2f5b4891f))
* **accommodation:** renew form ([13ff9ae](https://gitlab.com/fi-sas/fisas-webpage/commit/13ff9aeb805141d04d51911eebc141f4651161be))

## [1.119.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.119.1...v1.119.2) (2022-05-13)


### Bug Fixes

* **accommodation:** fix accommodation phase message ([1e3f2e1](https://gitlab.com/fi-sas/fisas-webpage/commit/1e3f2e1cc468d573079ce04951bdead92f8661d2))

## [1.119.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.119.0...v1.119.1) (2022-05-12)


### Bug Fixes

* **core:** remove qrcode divider ([434b12d](https://gitlab.com/fi-sas/fisas-webpage/commit/434b12de1e2efacfa8df46bf2230ad9fd553d5b7))

# [1.119.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.118.4...v1.119.0) (2022-05-11)


### Features

* **accommodation:** show academic year in residences and applications ([40d8ee1](https://gitlab.com/fi-sas/fisas-webpage/commit/40d8ee1932eb04e6df53bd9c44166d5b4ee99a8e))

## [1.118.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.118.3...v1.118.4) (2022-05-11)


### Bug Fixes

* **bus:** chow withdrawal button on applications ([547f05b](https://gitlab.com/fi-sas/fisas-webpage/commit/547f05b8a2d7187df363f4131fafe34f813129a2))

## [1.118.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.118.2...v1.118.3) (2022-05-10)


### Bug Fixes

* **core:** qrcode link only show by envoirnment ([a81c785](https://gitlab.com/fi-sas/fisas-webpage/commit/a81c78586cdba2d1c5f4c69deeed8450c6e4fdbe))

## [1.118.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.118.1...v1.118.2) (2022-05-10)


### Bug Fixes

* **alimentation:** infinite loading on page dish list ([59dbad5](https://gitlab.com/fi-sas/fisas-webpage/commit/59dbad5088deb2fb28859f483f0db67a2659b417))

## [1.118.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.118.0...v1.118.1) (2022-05-06)


### Bug Fixes

* **food:** allergens text and contain  allergens ([0672bbe](https://gitlab.com/fi-sas/fisas-webpage/commit/0672bbeaf2bdde9b8a9e8d93a7c174cc98652158))

# [1.118.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.117.0...v1.118.0) (2022-05-04)


### Bug Fixes

* build errors ([b71c4b7](https://gitlab.com/fi-sas/fisas-webpage/commit/b71c4b7c46734720380edbadb15760246fb2b3f6))


### Features

* **food:** add alergen configuration ([6a0db6e](https://gitlab.com/fi-sas/fisas-webpage/commit/6a0db6ee32ac31ebc15c519d1acb8188ba6a38c0))

# [1.117.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.116.3...v1.117.0) (2022-05-04)


### Bug Fixes

* **scholarship:** missing field validator ([6c774b2](https://gitlab.com/fi-sas/fisas-webpage/commit/6c774b293531d97994388815bec4af9aad3d341f))


### Features

* **scholarship:** allow to fill one of the options in the modal ([285aa05](https://gitlab.com/fi-sas/fisas-webpage/commit/285aa059b97ed306e3fcdb6dd0a5bb70e71cf879))
* **scholarship:** change monthly report modal ([5ba2e4d](https://gitlab.com/fi-sas/fisas-webpage/commit/5ba2e4d61dd1d10cab8092329d3e4e32b41f5042))

## [1.116.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.116.2...v1.116.3) (2022-05-04)


### Bug Fixes

* **scholarship:** prevent error in modal ([e0f8de0](https://gitlab.com/fi-sas/fisas-webpage/commit/e0f8de0ab21b0f9f9b547324833c4e481d578412))
* **volunteering:** dispatch modal, actions and labeling ([84c11ee](https://gitlab.com/fi-sas/fisas-webpage/commit/84c11ee069738550e90cb48f17720d848b25a4eb))

## [1.116.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.116.1...v1.116.2) (2022-05-04)


### Bug Fixes

* **accommodation:** fix routing path of list applications ([5850bac](https://gitlab.com/fi-sas/fisas-webpage/commit/5850bac21811a8f7873a41625b16fb367a98e6d9))
* **ubike:** application history show closed applications ([aeda841](https://gitlab.com/fi-sas/fisas-webpage/commit/aeda841f39d720af5b4746dd2a3f493bbc9ed25e))

## [1.116.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.116.0...v1.116.1) (2022-05-03)


### Bug Fixes

* **scholarship:** fix import ([bc80860](https://gitlab.com/fi-sas/fisas-webpage/commit/bc80860f818b521372ce05909f7097b870045c0e))
* **scholarship:** fix interest status machine ([cfe5c02](https://gitlab.com/fi-sas/fisas-webpage/commit/cfe5c0222e1627a76d032c529ccb7845068dc799))
* **scholarship:** fix layout application form resize actions ([6bac61c](https://gitlab.com/fi-sas/fisas-webpage/commit/6bac61cb723dd912c37c9a9cebdb13d4362dd9e8))
* **scholarship:** fix status action´ ([2f9fabc](https://gitlab.com/fi-sas/fisas-webpage/commit/2f9fabc797502895a18daaac0612016f9cbf5490))
* **scholarship:** fix status machine options ([67257da](https://gitlab.com/fi-sas/fisas-webpage/commit/67257daab7ba65b6bb8bb53c77bd11ae55a126d2))
* **scholarship:** show the error on submit button action ([34369e0](https://gitlab.com/fi-sas/fisas-webpage/commit/34369e0e33caa1ab980766821239ec4a89e72c01))

# [1.116.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.115.1...v1.116.0) (2022-04-29)


### Features

* **emergencyfund:** emergency fund webpage - Expenses form ([0013d71](https://gitlab.com/fi-sas/fisas-webpage/commit/0013d71f7b2f49913d2c65c09dddc183c82ba599))
* **emergencyfund:** emergency fund webpage - Expenses LIST ([c3e3396](https://gitlab.com/fi-sas/fisas-webpage/commit/c3e3396aea60ff1c585bbe8688306e87248045b9))
* **emergencyfund:** emergency fund webpage - Language status fix ([8af9243](https://gitlab.com/fi-sas/fisas-webpage/commit/8af9243c95417101adfdd2c4e8e42b221cd06a5d))
* **emergencyfund:** emergency fund webpage - Language status fix V2 ([693e038](https://gitlab.com/fi-sas/fisas-webpage/commit/693e03820b5c3cb3de64814ce1fdb8992cea359f))
* **emergencyfund:** emergency fund webpage - Language status fix V3 ([b31cf5e](https://gitlab.com/fi-sas/fisas-webpage/commit/b31cf5ec333a2780a0e3b59c9f20f5dda4d69336))
* **emergencyfund:** emergency fund webpage - Multiple fixes ([ac41c0a](https://gitlab.com/fi-sas/fisas-webpage/commit/ac41c0a230f1b3991f68cde1e99b0798d9c8881d))

## [1.115.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.115.0...v1.115.1) (2022-04-27)


### Bug Fixes

* **dashboard:** fix volunteering widget ([12f13fa](https://gitlab.com/fi-sas/fisas-webpage/commit/12f13fa4487037d34c5c950eb834df29edc718ef))

# [1.115.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.114.1...v1.115.0) (2022-04-26)


### Bug Fixes

* **volunteering:** fix locations positions ([5a0a2fd](https://gitlab.com/fi-sas/fisas-webpage/commit/5a0a2fdb048d1a8b92aafdf9695f4d11764e6aa0))
* **volunteering:** labeling experiences detail ([5b42e0c](https://gitlab.com/fi-sas/fisas-webpage/commit/5b42e0c115ea6449ca7dd7b091515991ee6b42b3))


### Features

* **volunteering:** allow cancel withdrawal ([9100cd0](https://gitlab.com/fi-sas/fisas-webpage/commit/9100cd071d2377abef62e73aacd450d2e327c560))
* **volunteering:** back action refactor ([0b6d816](https://gitlab.com/fi-sas/fisas-webpage/commit/0b6d816a3334bb52c45e9625b2d5d169d8e87466))
* **volunteering:** change label acording with attendace status ([1c7015a](https://gitlab.com/fi-sas/fisas-webpage/commit/1c7015a1bde753d695f8b65d0740cd7a99f3fa7b))
* **volunteering:** complains module ([39bca84](https://gitlab.com/fi-sas/fisas-webpage/commit/39bca84cd77492759f8896ac8d2f5734e8c0846d))

## [1.114.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.114.0...v1.114.1) (2022-04-22)


### Bug Fixes

* **qrcode:** remove email: from qrcode value ([7c78927](https://gitlab.com/fi-sas/fisas-webpage/commit/7c78927789763c3accc9d70dde52aea51907a6c9))

# [1.114.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.113.0...v1.114.0) (2022-04-20)


### Features

* **emergencyfund:** emergency fund webpage - App Changes form ([854dcea](https://gitlab.com/fi-sas/fisas-webpage/commit/854dcead274bdb34c7b7a31c2ec10d4f3480ee54))
* **emergencyfund:** emergency fund webpage - Complains ([260b3f7](https://gitlab.com/fi-sas/fisas-webpage/commit/260b3f7f22ae71a47673d4cd301bace80fb8cbf2))
* **emergencyfund:** emergency fund webpage - List app changes ([091f7f8](https://gitlab.com/fi-sas/fisas-webpage/commit/091f7f8930923bca57fe1888c3d20b7927fc7a52))
* **emergencyfund:** emergency fund webpage - Review Application ([014f57c](https://gitlab.com/fi-sas/fisas-webpage/commit/014f57c1389ccc61947109099561a6107f930c22))

# [1.113.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.112.2...v1.113.0) (2022-04-19)


### Bug Fixes

* **volunteering:** fix user interest complaint ([a8eb2f0](https://gitlab.com/fi-sas/fisas-webpage/commit/a8eb2f00c57dcc6223e11be6a0e6cf5a0a094934))
* **volunteering:** modal attendance record ([b0cbec0](https://gitlab.com/fi-sas/fisas-webpage/commit/b0cbec0760eb372e19d5251f5ab12c40c4f65667))
* **volunteering:** refactor async methods ([bc46440](https://gitlab.com/fi-sas/fisas-webpage/commit/bc464405710175c8c426b44cca9e99c72a8599d0))
* **volunteering:** remove try await promise ([2cbc676](https://gitlab.com/fi-sas/fisas-webpage/commit/2cbc6765f61689a9d2e22bc1206357468fbd2a73))
* **volunteering:** send date and method refactor ([f80ce66](https://gitlab.com/fi-sas/fisas-webpage/commit/f80ce6690208d0d95b0eb5486b9e60d2d8c93791))


### Features

* **accommodation:** hide file upload iban request ([4422a8c](https://gitlab.com/fi-sas/fisas-webpage/commit/4422a8c77bfca5b1401513e4bd5d27c2fe5d45c2))
* **accommodation:** label for changing accommodation period ([8d9d5e4](https://gitlab.com/fi-sas/fisas-webpage/commit/8d9d5e490b9100d560f202f76b9c4373712edaf6))
* add qrcode user card page ([20caaf8](https://gitlab.com/fi-sas/fisas-webpage/commit/20caaf883ce53d4bd38b2bd24ba2372324a9361b))
* **volunteering:** allow see volunteering without login ([c69b471](https://gitlab.com/fi-sas/fisas-webpage/commit/c69b4714c36a4fe75daab3db8aa3370f4f958e41))
* **volunteering:** show edit button in accept status ([f3ebc40](https://gitlab.com/fi-sas/fisas-webpage/commit/f3ebc403b265f788dfbccacc6fcdd5deb7ea47b9))
* **volunteering:** show subtitle according with experience status ([af481f2](https://gitlab.com/fi-sas/fisas-webpage/commit/af481f2f64e788e287474c2a55f8eb811e94da62))

## [1.112.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.112.1...v1.112.2) (2022-04-19)


### Bug Fixes

* **accommodation:** add history to relateds in applications ([7349df7](https://gitlab.com/fi-sas/fisas-webpage/commit/7349df7c301787facec24f9321a8e20b0c1736c3))

## [1.112.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.112.0...v1.112.1) (2022-04-12)


### Bug Fixes

* **ubike:** variable name ([0726e02](https://gitlab.com/fi-sas/fisas-webpage/commit/0726e029fe0453ee1d755a032ace94d023923e76))

# [1.112.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.111.0...v1.112.0) (2022-04-12)


### Features

* **emergencyfund:** emergency fund webpage - Dashboard ([9faf8d2](https://gitlab.com/fi-sas/fisas-webpage/commit/9faf8d2ef768e8055c0891198d3aa0ede546f99f))

# [1.111.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.110.0...v1.111.0) (2022-04-12)


### Features

* **emergencyfund:** emergency fund webpage - Form Apply ([fa77284](https://gitlab.com/fi-sas/fisas-webpage/commit/fa772848cd7c0a8ac187e4e0fd178d6b4ae88369))
* **emergencyfund:** emergency fund webpage - Form Apply ([3e8c6e3](https://gitlab.com/fi-sas/fisas-webpage/commit/3e8c6e345a2a28504e9069defdf6f1d6d3b881fd))

# [1.110.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.109.0...v1.110.0) (2022-04-11)


### Bug Fixes

* **ubike:** remove unused service ([545b69c](https://gitlab.com/fi-sas/fisas-webpage/commit/545b69c18b514aa7ce129db39e1e951419277e38))
* **ubike:** show active tipologies in form ([0cf21c6](https://gitlab.com/fi-sas/fisas-webpage/commit/0cf21c6ebc2388a660ad076d962fdb674635327d))


### Features

* **ubike:** add terms to ubike ([d0de957](https://gitlab.com/fi-sas/fisas-webpage/commit/d0de95778f55cd3f41a526ebe55ebf60d2b0cb52))
* **ubike:** fix bugs and improvments ([53ee511](https://gitlab.com/fi-sas/fisas-webpage/commit/53ee5114e9a1bf0cc87a44534a0d53e661fa7e37))

# [1.109.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.108.1...v1.109.0) (2022-04-11)


### Features

* **emergencyfund:** emergency fund webpage sprint 32 ([67433b2](https://gitlab.com/fi-sas/fisas-webpage/commit/67433b2ea5fe4fa3a14d34bec7e8e0fc8b165559))
* **emergencyfund:** emergency fund webpage sprint 32 v2 ([8e8d7eb](https://gitlab.com/fi-sas/fisas-webpage/commit/8e8d7eb420fd35152cc9e405b275d53cbb133bde))

## [1.108.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.108.0...v1.108.1) (2022-04-07)


### Bug Fixes

* **private-accommodation:** fix infinite load on detail page ([8029c25](https://gitlab.com/fi-sas/fisas-webpage/commit/8029c2578922052e253bd0f4aba5e037e1d34830))

# [1.108.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.107.2...v1.108.0) (2022-04-06)


### Features

* **login:** add remaining login attemps message ([df8b13e](https://gitlab.com/fi-sas/fisas-webpage/commit/df8b13ef33653f5cb7c8797b7ebdea40d28dd20e))
* **login:** trim email and add new messagens ([21801bb](https://gitlab.com/fi-sas/fisas-webpage/commit/21801bb53d2de673dd99177f03c57f7cd8d960a8))
* **volunteering:** show application, action and interes in dispatch ([0ade277](https://gitlab.com/fi-sas/fisas-webpage/commit/0ade27706e80aefd4a5a4ca32fc259a9c962c435))
* **volunteering:** show experiences by can_express ([59d550e](https://gitlab.com/fi-sas/fisas-webpage/commit/59d550e9d95c57e530895e534651d0abb0a76d5e))

## [1.107.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.107.1...v1.107.2) (2022-04-05)


### Bug Fixes

* **volunteering:** fix profile, reject and accept actions ([0e17366](https://gitlab.com/fi-sas/fisas-webpage/commit/0e173660a50a4a1a13ea9b6b6fa28ef46359b60e))

## [1.107.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.107.0...v1.107.1) (2022-04-05)


### Bug Fixes

* **voluntering:** show school field for all profiles ([995517e](https://gitlab.com/fi-sas/fisas-webpage/commit/995517e411ac9b62d534bd1e7af606d4972fa8af))

# [1.107.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.9...v1.107.0) (2022-04-01)


### Bug Fixes

* **dashboard:** change style and performance of meal widget ([b088bca](https://gitlab.com/fi-sas/fisas-webpage/commit/b088bcade77dfb2b587e4842cfae722bb86e2686))


### Features

* **social scholarship:** removing bold label and check translations ([ebe93cc](https://gitlab.com/fi-sas/fisas-webpage/commit/ebe93ccfb3c2752b7752aec5f1c853ffa8b9e965))

## [1.106.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.8...v1.106.9) (2022-04-01)


### Bug Fixes

* **private-accommodation:** fix infinite loading screen ([40b7b6a](https://gitlab.com/fi-sas/fisas-webpage/commit/40b7b6a70aaa37882cbcc6ff16e55e5f1375c561))
* **social-support:** remove manifest interest button on old experiences ([9030f8f](https://gitlab.com/fi-sas/fisas-webpage/commit/9030f8f29f1105a1916d014d2764b504a692e948))
* **volunteering:** remove manifest interest button on old experiences ([fedd4eb](https://gitlab.com/fi-sas/fisas-webpage/commit/fedd4ebc89638c6edfcd2eb24c354fc90963bf6d))

## [1.106.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.7...v1.106.8) (2022-03-29)


### Bug Fixes

* **food:** return to page now works with date ([fe96981](https://gitlab.com/fi-sas/fisas-webpage/commit/fe96981ebe7b8ea0696418c3df720e64d622c1b4))

## [1.106.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.6...v1.106.7) (2022-03-28)


### Bug Fixes

* **accommodation:** add current regime field to applications ([2091a45](https://gitlab.com/fi-sas/fisas-webpage/commit/2091a45c917a0ef0db47b9398666f88d207851c4))

## [1.106.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.5...v1.106.6) (2022-03-25)


### Bug Fixes

* **ubike:** fix tag status ([218c14f](https://gitlab.com/fi-sas/fisas-webpage/commit/218c14fd082f91b5723aa4d0ed6ba70a04326a18))

## [1.106.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.4...v1.106.5) (2022-03-15)


### Bug Fixes

* **volunteering:** remove school field from non student users ([9a0256a](https://gitlab.com/fi-sas/fisas-webpage/commit/9a0256a6ca3a36421974cecab2db4e6b7dac6ec3))

## [1.106.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.3...v1.106.4) (2022-03-15)


### Bug Fixes

* **u-bike:** fix application form ([f1af2ba](https://gitlab.com/fi-sas/fisas-webpage/commit/f1af2ba8a5ae9bcb65d489361cde0a67f1fa7e2a))
* **u-bike:** fix form application step 3 ([788fa6f](https://gitlab.com/fi-sas/fisas-webpage/commit/788fa6f9cb3f83806338e283d97fce627f71235f))
* **ubike:** remove user_id from querys ([b7f0ee4](https://gitlab.com/fi-sas/fisas-webpage/commit/b7f0ee41689c3fdf7e094976d00f90f0bcc7f737))
* **ubike:** removing parameters from functions ([c6ca79b](https://gitlab.com/fi-sas/fisas-webpage/commit/c6ca79b2ca36ff854e188e091cb8aa48bdfb39e8))

## [1.106.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.2...v1.106.3) (2022-03-11)


### Bug Fixes

* **social-support:** change attendances utc hour to local ([199680d](https://gitlab.com/fi-sas/fisas-webpage/commit/199680d79d108a2de1bb7a251c43baaf5219b313))

## [1.106.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.1...v1.106.2) (2022-03-08)


### Bug Fixes

* **dashboard:** space around between items ([cee4df3](https://gitlab.com/fi-sas/fisas-webpage/commit/cee4df30906797bc6b1fb41bdc0af7a4d69e359c))

## [1.106.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.106.0...v1.106.1) (2022-03-08)


### Bug Fixes

* **dashboard:** menu_dish link mal formed ([e032b30](https://gitlab.com/fi-sas/fisas-webpage/commit/e032b30631ee0f97acfa05c328988b86a0771af4))

# [1.106.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.5...v1.106.0) (2022-03-03)


### Bug Fixes

* **accommodation:** fix buttons bugs and loadings ([67f9166](https://gitlab.com/fi-sas/fisas-webpage/commit/67f9166ca5281e37508cb4c34ac94f33fe61ad6a))
* **accommodation:** fix loading billing history ([39c5e8d](https://gitlab.com/fi-sas/fisas-webpage/commit/39c5e8d6da4f3c380cc146edb5fdcfd7477fb240))
* **accommodation:** fix showing extras ([3c47a2e](https://gitlab.com/fi-sas/fisas-webpage/commit/3c47a2e64490c31b0b7612f8d9df1cbf631b1dab))
* **accommodation:** labels and messages ([7a6bd1d](https://gitlab.com/fi-sas/fisas-webpage/commit/7a6bd1d6c7c9ed933481c3c4309343d1063643f1))
* **accommodation:** routing ([3423b2d](https://gitlab.com/fi-sas/fisas-webpage/commit/3423b2d963029d66c30a87ce8b42294837bec613))
* **private accommodation:** fix widget ([6c5a1dc](https://gitlab.com/fi-sas/fisas-webpage/commit/6c5a1dc7aae807a509065beebcf2ca0ca0df4f34))


### Features

* **buttons menu:** order buttons by name ([762eb5c](https://gitlab.com/fi-sas/fisas-webpage/commit/762eb5ca3a91182cbabb0a68c9c2ef33fdbf0de2))
* **layout:** order external service by name ([0626e4b](https://gitlab.com/fi-sas/fisas-webpage/commit/0626e4b52a635148588ac87242ccf9a16b38c211))

## [1.105.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.4...v1.105.5) (2022-02-28)


### Bug Fixes

* **volunteering:** change permission of openning page ([e2bfc48](https://gitlab.com/fi-sas/fisas-webpage/commit/e2bfc4899bcb864f6c65e7c0ee449e8ffd9f9cc3))

## [1.105.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.3...v1.105.4) (2022-02-28)


### Bug Fixes

* **auth:** calculate expire_in date ([5fc45e1](https://gitlab.com/fi-sas/fisas-webpage/commit/5fc45e1e6fabdec2d37000ef2399907fc77e823b))

## [1.105.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.2...v1.105.3) (2022-02-18)


### Bug Fixes

* add fixs to social support and volunteering ([2ca930c](https://gitlab.com/fi-sas/fisas-webpage/commit/2ca930cfce8f2bcfea8e834230c2e308a43c3d91))
* **social-support:** change application form ([a3f6d28](https://gitlab.com/fi-sas/fisas-webpage/commit/a3f6d28768adf7bcb41d3f776ddfced5dc128aee))

## [1.105.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.1...v1.105.2) (2022-02-15)


### Bug Fixes

* **social-support:** offer loading errors ([6c18332](https://gitlab.com/fi-sas/fisas-webpage/commit/6c18332455eb11d85dbd7b69eba4067d0d267239))

## [1.105.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.105.0...v1.105.1) (2022-02-10)


### Bug Fixes

* **header:** only show current account with movements read permission ([3078775](https://gitlab.com/fi-sas/fisas-webpage/commit/307877519b92caf72439589963d37c3d67d43104))

# [1.105.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.104.2...v1.105.0) (2022-02-10)


### Features

* **accommodation:** remove reason fields limit of chars ([36c211b](https://gitlab.com/fi-sas/fisas-webpage/commit/36c211bd5e618a011048cc5d77be73bb768f3529))

## [1.104.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.104.1...v1.104.2) (2022-02-09)


### Bug Fixes

* **news:** set image ratio 16:9 ([8b99e1b](https://gitlab.com/fi-sas/fisas-webpage/commit/8b99e1baa964e787e26f6f10d6f04338d9ea35ba))

## [1.104.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.104.0...v1.104.1) (2022-02-02)


### Bug Fixes

* **ubike:** change last step to submit application ([09aa98a](https://gitlab.com/fi-sas/fisas-webpage/commit/09aa98abc1f4171901c53198748cdcd83079b75c))

# [1.104.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.103.1...v1.104.0) (2022-02-02)


### Features

* **token:** new token refresh system ([d1d12e6](https://gitlab.com/fi-sas/fisas-webpage/commit/d1d12e62574275112a17e5f8668ae7989f3aa9cb))
* **update:** change new update system ([3ec3f87](https://gitlab.com/fi-sas/fisas-webpage/commit/3ec3f87fd45b5fe21aa3ff96c9bfae409447e744))

## [1.103.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.103.0...v1.103.1) (2022-02-02)


### Bug Fixes

* **layout:** fix layout sidebar ([176213f](https://gitlab.com/fi-sas/fisas-webpage/commit/176213f9e1332c327b98410cab4a52a4e72325a1))

# [1.103.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.102.0...v1.103.0) (2022-02-01)


### Bug Fixes

* **health:** appointment export calendar file ([fb595e6](https://gitlab.com/fi-sas/fisas-webpage/commit/fb595e6769f44f049d3107950893919b60a4823d))


### Features

* **events:** button export calendar ([675b7c3](https://gitlab.com/fi-sas/fisas-webpage/commit/675b7c31a78a873a991d823eeacbe14cbf37a887))
* **events:** export calendar action ([7c3aef6](https://gitlab.com/fi-sas/fisas-webpage/commit/7c3aef6f4f2d78d6f8246645176cb9e10cae836d))
* **events:** gestão da recorrencia nos eventos ([bb9df24](https://gitlab.com/fi-sas/fisas-webpage/commit/bb9df24882f8d3bf4eb764ecd0bda3164c6b562f))
* **layout:** show external services ([a2d2bb2](https://gitlab.com/fi-sas/fisas-webpage/commit/a2d2bb2cb41c08b0255a6feab1c039d62ac6ce67))

# [1.102.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.101.2...v1.102.0) (2022-01-25)


### Bug Fixes

* **health:** buy model box and import of it in an higher level ([123d717](https://gitlab.com/fi-sas/fisas-webpage/commit/123d717f4935a6f74e4d50a34511efe5e22344d6))
* **health:** correct spelling setp to step ([e9077c5](https://gitlab.com/fi-sas/fisas-webpage/commit/e9077c573ab9b5b0d26b4e33440b3bc2e8137816))
* **health:** dashboard and return button to previous page ([123d633](https://gitlab.com/fi-sas/fisas-webpage/commit/123d6333f259e7fcc598ed28f31b4041d42baab1))
* **health:** fix some bugs found in review ([24cb2d3](https://gitlab.com/fi-sas/fisas-webpage/commit/24cb2d3069f67dccef3b89df33e0d74d36b85dd8))
* **health:** sending correct appointment info to modal ([a9acc52](https://gitlab.com/fi-sas/fisas-webpage/commit/a9acc52b08cc8fff73e59d3607d15fe4b2b13dcc))


### Features

* **calendar:** terms modal ([8810464](https://gitlab.com/fi-sas/fisas-webpage/commit/88104649cb321b8d119c1d19682a002afe03fda5))
* **events:** terms pop up bigger ([044df6b](https://gitlab.com/fi-sas/fisas-webpage/commit/044df6b21dd1a493314410520bfb7beec770141b))
* **health:** buy box when professional create a new appointment ([1a86a5a](https://gitlab.com/fi-sas/fisas-webpage/commit/1a86a5a60f93512164ee92b8e51fbaf0a831a69c))
* **health:** buy modal improvement ([58a4caf](https://gitlab.com/fi-sas/fisas-webpage/commit/58a4cafc80e45f291bc52001d915d92202783370))
* **health:** showing map location according with city ([52e9e8b](https://gitlab.com/fi-sas/fisas-webpage/commit/52e9e8b2c41aacbaa7fc675cc6941f124fef1651))

## [1.101.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.101.1...v1.101.2) (2022-01-17)


### Bug Fixes

* **bus:** change qrcode library to fix build production error ([9478eee](https://gitlab.com/fi-sas/fisas-webpage/commit/9478eee1656349765e5b34cc37f57a2bae3fc0f5))

## [1.101.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.101.0...v1.101.1) (2022-01-16)


### Bug Fixes

* **dashboard:** add translation to bus widget ([3365896](https://gitlab.com/fi-sas/fisas-webpage/commit/3365896788b49cd122db6612bb32dac5b5e973ab))

# [1.101.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.100.1...v1.101.0) (2022-01-14)


### Bug Fixes

* **css:** padding label ([a315ba6](https://gitlab.com/fi-sas/fisas-webpage/commit/a315ba684584f098058cbc2ceb255e6361ea551f))


### Features

* **calendar:** export ics file ([d7d411b](https://gitlab.com/fi-sas/fisas-webpage/commit/d7d411b0ad34352ba7a5234381af87174b2217c6))
* **css:** css class modal ([6d01260](https://gitlab.com/fi-sas/fisas-webpage/commit/6d012606537e0ebb5f863a1928e8471721395713))
* **modal:** buy modal box ([2505b38](https://gitlab.com/fi-sas/fisas-webpage/commit/2505b38aea36ff1691a405c6f102803f833fd145))

## [1.100.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.100.0...v1.100.1) (2022-01-10)


### Bug Fixes

* **dashboard:** change mobility dashboard visual ([0f7ef14](https://gitlab.com/fi-sas/fisas-webpage/commit/0f7ef142e2c01208fab57cd98b09cfa494b93148))

# [1.100.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.99.1...v1.100.0) (2022-01-06)


### Features

* **attendance:** attendance information and fixs ([a6a46f4](https://gitlab.com/fi-sas/fisas-webpage/commit/a6a46f44521405db9f07ba6ca36fdd87f64bbd98))
* **buttons:** filter buttons and popup ([7c10d19](https://gitlab.com/fi-sas/fisas-webpage/commit/7c10d19326dd073353ae7a6a9b8cd66a8ea1581d))
* **various:** doctor calendar and fixs ([dad9af8](https://gitlab.com/fi-sas/fisas-webpage/commit/dad9af8243f0277533c917c5b8397fed5ff7a155))
* **various:** improvements and fixs ([24fd88a](https://gitlab.com/fi-sas/fisas-webpage/commit/24fd88a78ca89714551aacc32cfb69961c780abc))
* **various:** some improvements / corrections ([3ce918f](https://gitlab.com/fi-sas/fisas-webpage/commit/3ce918fba2ff05957d57e8e13608f565c669f262))

## [1.99.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.99.0...v1.99.1) (2022-01-04)


### Bug Fixes

* **files:** add filename field ([32fe848](https://gitlab.com/fi-sas/fisas-webpage/commit/32fe84841b9a635b272803f840534404e065a15c))

# [1.99.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.98.0...v1.99.0) (2022-01-03)


### Features

* **bus:** add bus validator and ticket view page ([704db6f](https://gitlab.com/fi-sas/fisas-webpage/commit/704db6fd7cd9ce740a67bb50670854bad0ddc45e))

# [1.98.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.97.0...v1.98.0) (2022-01-03)


### Features

* **widget:** health widget ([1c3bc27](https://gitlab.com/fi-sas/fisas-webpage/commit/1c3bc27b844f177fbc8737a1ef345fea3bd40fa3))

# [1.97.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.96.0...v1.97.0) (2021-12-23)


### Features

* **calendar:** subscription ([ae91d9d](https://gitlab.com/fi-sas/fisas-webpage/commit/ae91d9d4297b13fa003aafe125f028231d7e3257))

# [1.96.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.95.0...v1.96.0) (2021-12-23)


### Features

* **calendar:** expression of interest ([7b8e321](https://gitlab.com/fi-sas/fisas-webpage/commit/7b8e321c3a4f1f72b628a97737110f5caa062587))

# [1.95.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.5...v1.95.0) (2021-12-23)


### Bug Fixes

* **mouse:** add mouse event stopEventPropagation ([d7eb1a5](https://gitlab.com/fi-sas/fisas-webpage/commit/d7eb1a5b7451935c593604520147bb17f85fd6cf))


### Features

* **attendances:** attendances component ([9a618f3](https://gitlab.com/fi-sas/fisas-webpage/commit/9a618f3305f54b88f96941ee19bab212e0dce8bc))
* **attendances:** doctor attendances ([cf16578](https://gitlab.com/fi-sas/fisas-webpage/commit/cf16578a27be3100c59edcd9839ec551f143207f))
* **design:** elements responsive ([fa892a1](https://gitlab.com/fi-sas/fisas-webpage/commit/fa892a14b639372c297b5882b30ae7136513a1b6))
* **various:** new features health ([d88b36e](https://gitlab.com/fi-sas/fisas-webpage/commit/d88b36e35c3e333397020106128ce111b1a5e301))

## [1.94.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.4...v1.94.5) (2021-12-23)


### Bug Fixes

* **notification:** add calendar ([8d69748](https://gitlab.com/fi-sas/fisas-webpage/commit/8d697480da9acdb2d75cf3334067ee00c200f336))

## [1.94.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.3...v1.94.4) (2021-12-23)


### Bug Fixes

* **social-support:** general fixes ([19318ca](https://gitlab.com/fi-sas/fisas-webpage/commit/19318ca85f135cba35228c7bda16b0487e97503d))
* **volunteering:** general fixes ([2b636c8](https://gitlab.com/fi-sas/fisas-webpage/commit/2b636c8a0175ed23a60f945ef2adcbc3c8fbdea4))

## [1.94.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.2...v1.94.3) (2021-12-22)


### Bug Fixes

* **calendar:** error message bts subscriptions ([bac2d73](https://gitlab.com/fi-sas/fisas-webpage/commit/bac2d73f3cad28e6586db04bfd2a8ba029102e0e))

## [1.94.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.1...v1.94.2) (2021-12-22)


### Bug Fixes

* **private-accommodation:** fix ng2-charts version ([f553b8d](https://gitlab.com/fi-sas/fisas-webpage/commit/f553b8d2fe7a4932cffbf6832845dcb00a493d73))

## [1.94.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.94.0...v1.94.1) (2021-12-22)


### Bug Fixes

* **private-accommodation:** update packages ([ffe871c](https://gitlab.com/fi-sas/fisas-webpage/commit/ffe871c087e838a7d0db7e3a056c9073bd530670))

# [1.94.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.93.0...v1.94.0) (2021-12-22)


### Features

* **calendar:** add new pages ([5072573](https://gitlab.com/fi-sas/fisas-webpage/commit/5072573f9daa945a4e3ce4c5052923182c640300))

# [1.93.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.92.0...v1.93.0) (2021-12-21)


### Features

* **bus:** add ticket list ([45bbd0a](https://gitlab.com/fi-sas/fisas-webpage/commit/45bbd0a0856dc0453aa92bd7fc7d50afa00a7145))

# [1.92.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.91.0...v1.92.0) (2021-12-20)


### Features

* **private_accommodation:** listings add rooms price into filter ([99b8cb4](https://gitlab.com/fi-sas/fisas-webpage/commit/99b8cb4331aaea6454f77a0580b668c1f6067be5))

# [1.91.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.90.0...v1.91.0) (2021-12-20)


### Features

* **ubike:** allow to submit application step by step ([05ba59f](https://gitlab.com/fi-sas/fisas-webpage/commit/05ba59f48e6801eb503c44e03f2092ecbc1709e7))
* **ubike:** allow to submit application step by step ([140e6de](https://gitlab.com/fi-sas/fisas-webpage/commit/140e6deecd87a5ea3f6c3d576a71bb640765cefe))
* **ubike:** allow to submit application step by step ([a80a7bf](https://gitlab.com/fi-sas/fisas-webpage/commit/a80a7bf001ddce0bd9c2804f8d60ac37e6572646))
* **ubike:** allow to submit application step by step ([8702b52](https://gitlab.com/fi-sas/fisas-webpage/commit/8702b52d9b44485ca49dd6d9787bc68a5a790389))
* **ubike:** allow to submit application step by step ([1d4551a](https://gitlab.com/fi-sas/fisas-webpage/commit/1d4551a434660bdc1970967c84c7f7d0a2aa4871))

# [1.90.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.7...v1.90.0) (2021-12-17)


### Features

* **mobility:** add tickets page ([59d7c34](https://gitlab.com/fi-sas/fisas-webpage/commit/59d7c3408e89ce4259dd2a105078d3542aa74035))

## [1.89.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.6...v1.89.7) (2021-12-15)


### Bug Fixes

* **ubike:** values format and convert ([428aab4](https://gitlab.com/fi-sas/fisas-webpage/commit/428aab45353d67112d0d77a89d6d71682c75e13c))

## [1.89.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.5...v1.89.6) (2021-12-14)


### Bug Fixes

* **configuration:** icon external services ([8c082df](https://gitlab.com/fi-sas/fisas-webpage/commit/8c082df2167eef2060e3619e27a33b36bbc29192))

## [1.89.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.4...v1.89.5) (2021-12-10)


### Bug Fixes

* **news:** change file 16:9 ([2bacea1](https://gitlab.com/fi-sas/fisas-webpage/commit/2bacea1376fac55ae9f489d89380f4b95269b123))
* **private_accomm:** add date filter ([52ecc1a](https://gitlab.com/fi-sas/fisas-webpage/commit/52ecc1a1bfa1b9634f7cd13e0658d3bb28c497fb))
* **private_accomm:** changing mandatory fields ([9df11c0](https://gitlab.com/fi-sas/fisas-webpage/commit/9df11c0ff7f9493c95b2606386d5ee6ef9ffa2e0))
* **private_accomm:** redirect map ([c6777db](https://gitlab.com/fi-sas/fisas-webpage/commit/c6777db81b04fb738c1fee01158b2691cbaf87ba))
* **private_accom:** new MS to check properties ([41367e9](https://gitlab.com/fi-sas/fisas-webpage/commit/41367e930530150714a84d88cba4d485a922f875))

## [1.89.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.3...v1.89.4) (2021-12-10)


### Bug Fixes

* **social-support:** show correct hours on reject attendance ([0a462f8](https://gitlab.com/fi-sas/fisas-webpage/commit/0a462f870868451f351a9a895a09122ca48ee036))
* **volunteering:** show correct hours on reject attendance ([53c7e22](https://gitlab.com/fi-sas/fisas-webpage/commit/53c7e22738321102ccc1a2b8b40bacb32b49d31e))

## [1.89.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.2...v1.89.3) (2021-12-06)


### Bug Fixes

* **queue:** change of current date ([bf34e56](https://gitlab.com/fi-sas/fisas-webpage/commit/bf34e56d1e805df7837026455c137083addceda7))

## [1.89.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.1...v1.89.2) (2021-12-06)


### Bug Fixes

* **queue:** icon change ([cdb4a80](https://gitlab.com/fi-sas/fisas-webpage/commit/cdb4a805ecb3fa70ca03be152bb68150865fca98))

## [1.89.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.89.0...v1.89.1) (2021-12-03)


### Bug Fixes

* **geral:** add permission ([2050765](https://gitlab.com/fi-sas/fisas-webpage/commit/2050765af4d78bbfcc79d03dbdf9ed6238412047))

# [1.89.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.88.1...v1.89.0) (2021-12-03)


### Features

* **social-support:** allow to edit application form files ([fdb6f14](https://gitlab.com/fi-sas/fisas-webpage/commit/fdb6f14926f076097872a041fb812f1ff4a8d9a2))
* **social-support:** allow to edit application form files ([44d95de](https://gitlab.com/fi-sas/fisas-webpage/commit/44d95dee70aa9f03ef79f04e98b25807157d4ea2))
* **social-support:** allow to edit application form files ([829c54d](https://gitlab.com/fi-sas/fisas-webpage/commit/829c54d6a458babf90fa21e3816edfb44ce27a50))
* **social-support:** allow to edit application form files ([a5e07c7](https://gitlab.com/fi-sas/fisas-webpage/commit/a5e07c76487e3fe53d5532b63129204f288e0a0c))
* **social-support:** allow to edit application form files ([5f26334](https://gitlab.com/fi-sas/fisas-webpage/commit/5f26334802696953fb6f14e6f8b3808100f14919))

## [1.88.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.88.0...v1.88.1) (2021-12-03)


### Bug Fixes

* **geral:** pagination ([ff3d96a](https://gitlab.com/fi-sas/fisas-webpage/commit/ff3d96af3585dea4e466899c0b39e44f87f28941))

# [1.88.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.87.2...v1.88.0) (2021-12-03)


### Bug Fixes

* **social-support:** added inputs for real initial and final time ([b5e94be](https://gitlab.com/fi-sas/fisas-webpage/commit/b5e94be110c147d92adff09eea90f827a709c76a))
* **social-support:** added inputs for real initial and final time ([eb9eafd](https://gitlab.com/fi-sas/fisas-webpage/commit/eb9eafda3ef2e68437b372da9045236dc939b5f4))
* **social-support:** fix to files not being sent correctly ([08aceff](https://gitlab.com/fi-sas/fisas-webpage/commit/08aceff6b3db4fdd2562775cf6fce4dd37c58743))
* **social-support:** fix to files not being sent correctly ([e940c94](https://gitlab.com/fi-sas/fisas-webpage/commit/e940c948e7dcd1f7fdb6709c578750c4169629f2))
* **social-support:** fix to files not being sent correctly ([86fe64a](https://gitlab.com/fi-sas/fisas-webpage/commit/86fe64a77ec3198dfddf1c220c150710bf94f164))


### Features

* **social-support:** show ca percentagem conditionaly ([7fee69b](https://gitlab.com/fi-sas/fisas-webpage/commit/7fee69b70d4a56852eddcfa4804c707d7385a474))

## [1.87.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.87.1...v1.87.2) (2021-12-02)


### Bug Fixes

* **accommodation,current_account:** validation pay ([8f91672](https://gitlab.com/fi-sas/fisas-webpage/commit/8f91672e373b81f52362f2c1c9c9d81d419f6ac3))
* **geral:** sort nationalities ([4fb2ebf](https://gitlab.com/fi-sas/fisas-webpage/commit/4fb2ebfff7de9dea0ba924b0cfc03eea39365694))
* **queue:** icon change ([780a835](https://gitlab.com/fi-sas/fisas-webpage/commit/780a835bebcfe36cb94d5ba076a06f55db4db0e0))

## [1.87.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.87.0...v1.87.1) (2021-11-26)


### Bug Fixes

* **current_account:** add info meal ([906b591](https://gitlab.com/fi-sas/fisas-webpage/commit/906b5919742b35c2238feabfaa115c0e0f1e4ade))
* **geral:** css ([622edba](https://gitlab.com/fi-sas/fisas-webpage/commit/622edba11b36a4f566fe0ca5a14697d654d65a95))
* **geral:** css ([a14c540](https://gitlab.com/fi-sas/fisas-webpage/commit/a14c540995b5fcfa37e766292c2bd59d8395af29))
* **geral:** css ([92fb7ea](https://gitlab.com/fi-sas/fisas-webpage/commit/92fb7eae27ba8fe715acc937f520690eac9e0665))
* **geral:** css ([4b7f17e](https://gitlab.com/fi-sas/fisas-webpage/commit/4b7f17e9a8c13fe4559d96df3fb3bbb2c56e28ec))
* **geral:** delete icon ([026f983](https://gitlab.com/fi-sas/fisas-webpage/commit/026f9839c2f3fe26cf7e79546fc3891c38e16ced))
* **geral:** new generic styles part 1 ([69f6b13](https://gitlab.com/fi-sas/fisas-webpage/commit/69f6b131455294ab9e64a62797734034ac512123))
* **geral:** style ([ed9f3fd](https://gitlab.com/fi-sas/fisas-webpage/commit/ed9f3fddb5d0a842a81a17cca2595e82f720dbc3))
* **geral:** styles ([2c90ee8](https://gitlab.com/fi-sas/fisas-webpage/commit/2c90ee8a8ba879b0c05096ed8f7ab4b684d6d535))
* **geral:** styles ([e63206f](https://gitlab.com/fi-sas/fisas-webpage/commit/e63206fab01d193ac45e9fc0379f2269db9931b2))
* **geral:** styles ([5aef219](https://gitlab.com/fi-sas/fisas-webpage/commit/5aef219a554f8a7e88087c8bd4c554dd4ba633c4))
* **geral:** styles css ([15e6210](https://gitlab.com/fi-sas/fisas-webpage/commit/15e6210edfc314caa6cca8c97ddb598fd8e2d651))
* **notification:** add date ([f37b91a](https://gitlab.com/fi-sas/fisas-webpage/commit/f37b91a854abe4a43e952207f4eec7460916ea7d))
* **theme:** revert ([292af4c](https://gitlab.com/fi-sas/fisas-webpage/commit/292af4c03db1d7473326e7c3d6eb54f420845631))
* **ubike:** correction dates error ([588d461](https://gitlab.com/fi-sas/fisas-webpage/commit/588d461174fb8ba2b0cc5fec7eae7484aa1419c3))

# [1.87.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.86.1...v1.87.0) (2021-11-24)


### Features

* **core:** added diferent robots files to staging anmd productions ([41097c7](https://gitlab.com/fi-sas/fisas-webpage/commit/41097c72b88f4c596c9bab722113d52fec658e99))
* **core:** added diferent robots files to staging anmd productions ([e41333b](https://gitlab.com/fi-sas/fisas-webpage/commit/e41333b841f6bfebe6c00b7306292f7ab8043fb6))
* **core:** added SEO env vars to change page title and description ([8661933](https://gitlab.com/fi-sas/fisas-webpage/commit/8661933f089d495458aa0e8ffdafc5951b9a94e7))
* **core:** added SEO env vars to change page title and description ([4aaaf67](https://gitlab.com/fi-sas/fisas-webpage/commit/4aaaf67b6825d6c7e08479917f847aee511bfc50))
* **core:** added SEO service to change page title and description ([bf10fdf](https://gitlab.com/fi-sas/fisas-webpage/commit/bf10fdfc0cfd2773ab1d11ecfffbb94b61de83e9))
* **core:** added SEO service to change page title and description ([c4f5810](https://gitlab.com/fi-sas/fisas-webpage/commit/c4f5810cd354253386a40fdc910be2267d977f0f))

## [1.86.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.86.0...v1.86.1) (2021-11-24)


### Bug Fixes

* **social-support:** send n_hours and n_hours_reject as string ([025035c](https://gitlab.com/fi-sas/fisas-webpage/commit/025035cee677a0e22fae668f37a8822f4da122ea))
* **volunteering:** send n_hours and n_hours_reject as string ([2608313](https://gitlab.com/fi-sas/fisas-webpage/commit/2608313aedf78675f8fc6a85d0d7a413133ff2f7))

# [1.86.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.85.1...v1.86.0) (2021-11-24)


### Bug Fixes

* **social-support:** fix to reject attendance modal width ([fd69cf7](https://gitlab.com/fi-sas/fisas-webpage/commit/fd69cf79f9d9df1424cfa3455a8ac6959a86bf64))
* **ubike:** fix to occurrence information lacking ([fe0e982](https://gitlab.com/fi-sas/fisas-webpage/commit/fe0e9822ec7cab791e2b4e4af211c340188f015e))
* **volunteering:** fix to reject attendance modal width ([b332b3f](https://gitlab.com/fi-sas/fisas-webpage/commit/b332b3f01f629bae3d10c5fc2d671f0d9a3fcd1b))


### Features

* **social-support:** added past experiences list ([ea9cd51](https://gitlab.com/fi-sas/fisas-webpage/commit/ea9cd5154ff84f4859863b0703fab3f81cdeadf3))
* **social-support:** added past experiences list ([53f630a](https://gitlab.com/fi-sas/fisas-webpage/commit/53f630a721c23a3ff92694c803c651109547f69e))

## [1.85.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.85.0...v1.85.1) (2021-11-22)


### Bug Fixes

* **packs:** remove unused modal ([e5c7eb7](https://gitlab.com/fi-sas/fisas-webpage/commit/e5c7eb7200a7376439233f652b9fe486df940194))

# [1.85.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.8...v1.85.0) (2021-11-22)


### Features

* **packs:** add packs to tickets ([c1bd2f5](https://gitlab.com/fi-sas/fisas-webpage/commit/c1bd2f51770965a18719a97ef644339b83e937d9))

## [1.84.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.7...v1.84.8) (2021-11-17)


### Bug Fixes

* **menu:** add valid permission ([3551047](https://gitlab.com/fi-sas/fisas-webpage/commit/3551047a26a702b6cd46bb085739239821efab09))
* **menu:** add valid permission ([cb2d7a3](https://gitlab.com/fi-sas/fisas-webpage/commit/cb2d7a3c66f466bb28977b386f2d9e13415ad16f))
* **menu:** fixed menu scope check ([7757303](https://gitlab.com/fi-sas/fisas-webpage/commit/77573039981487fb85ac3ff9e99f0ac02d9fe004))

## [1.84.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.6...v1.84.7) (2021-11-15)


### Bug Fixes

* **auth:** error message email after the button ([6c8d5ad](https://gitlab.com/fi-sas/fisas-webpage/commit/6c8d5ad687a97716cd157fa5b6a0759cbc897cfa))
* **auth:** text correction ([c5e9896](https://gitlab.com/fi-sas/fisas-webpage/commit/c5e9896057604e0e25dac4f2ae5a627f3f70f6fb))
* **current_account:** add refectory_consume_at ([e32c6da](https://gitlab.com/fi-sas/fisas-webpage/commit/e32c6da553c6f1a6c44c25f4969b0a0cfd8fc9dc))
* **mobility:** change name btn ([9ffdf0e](https://gitlab.com/fi-sas/fisas-webpage/commit/9ffdf0ec88d62aebadcf2e740067d52021c8344b))
* **social_support:** change message form ([b56880f](https://gitlab.com/fi-sas/fisas-webpage/commit/b56880f3185a368238508e797f8eef94eedc4d8f))
* **ubike:** change text to button ([452ddf3](https://gitlab.com/fi-sas/fisas-webpage/commit/452ddf396075217a981ef29810aa6e90d45287ce))
* **ubike:** correction error ([e887f5b](https://gitlab.com/fi-sas/fisas-webpage/commit/e887f5b946eb2facba3cab6c61dc772f1cbb84e0))
* **ubike:** improve mobile occurrences aspects ([a4f34b2](https://gitlab.com/fi-sas/fisas-webpage/commit/a4f34b22ac8f4960cbca3c5c5ac3f4f5e9e05bbe))
* **volunteering:** change message form ([bcb2829](https://gitlab.com/fi-sas/fisas-webpage/commit/bcb2829b871f536f7297d33584a4157f067ed744))
* **volunteering:** standardize term ([43850a9](https://gitlab.com/fi-sas/fisas-webpage/commit/43850a96ce1d3a996cc469084ba58e32316f8fc4))

## [1.84.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.5...v1.84.6) (2021-11-12)


### Bug Fixes

* **perfil:** valid fields ([e8ca0db](https://gitlab.com/fi-sas/fisas-webpage/commit/e8ca0db6ad97f62e3e11311a532b32101a9515c7))

## [1.84.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.4...v1.84.5) (2021-11-12)


### Bug Fixes

* **accommodation:** validation for extras regimes ([d1d02e4](https://gitlab.com/fi-sas/fisas-webpage/commit/d1d02e43bfa6de519b5e9136744ea1b24092db6b))

## [1.84.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.3...v1.84.4) (2021-11-12)


### Bug Fixes

* **dashboard:** changing service buttons dashboard ([d4a88a1](https://gitlab.com/fi-sas/fisas-webpage/commit/d4a88a1eaac8fb82f75e2c4ebe5d16b7b032fcc7))

## [1.84.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.2...v1.84.3) (2021-11-11)


### Bug Fixes

* **perfil:** randon pin ([749fb3e](https://gitlab.com/fi-sas/fisas-webpage/commit/749fb3e59911a547418391bfb8eb74041e740468))

## [1.84.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.1...v1.84.2) (2021-11-10)


### Bug Fixes

* **geral:** radio color ([c161ae7](https://gitlab.com/fi-sas/fisas-webpage/commit/c161ae7111d6aa25e57c00cb685090b40d8502eb))
* **social_support,volt:** delete img modal ([0c4409b](https://gitlab.com/fi-sas/fisas-webpage/commit/0c4409bb1e9cc3b1896da49d88fea917ff433e41))
* **social_support,volunt:** add info responsable ([bee142e](https://gitlab.com/fi-sas/fisas-webpage/commit/bee142ebd67c6ca2fedea9fa2d8a3a6b815cd338))
* **social_support,volunt:** valid dates ([3c8d1a6](https://gitlab.com/fi-sas/fisas-webpage/commit/3c8d1a656c2323817e277a04f4c05e4a387c11d1))
* **ubike:** change status private maintenace ([b1029c3](https://gitlab.com/fi-sas/fisas-webpage/commit/b1029c33e615120a2f9736491a45c71803589917))
* **ubike:** message withdrawal form ([42d9388](https://gitlab.com/fi-sas/fisas-webpage/commit/42d9388ac816c9c8003e7cfbc77db162b377cc12))
* **volunt,social_support:** add column table type ([dcecbd8](https://gitlab.com/fi-sas/fisas-webpage/commit/dcecbd80de252411f4043963d65b8de861ffff4e))
* **volunt,social_support:** color btn ([3c3e46b](https://gitlab.com/fi-sas/fisas-webpage/commit/3c3e46b253bf2a685910dca585490ccf75006385))
* **volunt,social_support:** correct error loading ([8078048](https://gitlab.com/fi-sas/fisas-webpage/commit/807804850dfa95a5d9d732ca279c07a76807171c))

## [1.84.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.84.0...v1.84.1) (2021-11-10)


### Bug Fixes

* **u-bike:** remove complain button from card ([c96ed55](https://gitlab.com/fi-sas/fisas-webpage/commit/c96ed551ee11a53e0bc8ecf6acef7b690bf8d4cd))

# [1.84.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.83.0...v1.84.0) (2021-11-10)


### Bug Fixes

* **social-support:** show certificate file if uploaded or generated ([fcbfe2a](https://gitlab.com/fi-sas/fisas-webpage/commit/fcbfe2a59e425bd079c5d4faf5b900f76753d0a8))
* **volunteering:** show certificate file if uploaded or generated ([5fe55a2](https://gitlab.com/fi-sas/fisas-webpage/commit/5fe55a27b645ac7d151fbdbf9074cb28b4ebab0a))


### Features

* **social-support:** add interview observations ([1366036](https://gitlab.com/fi-sas/fisas-webpage/commit/13660364cb82bbb7abd03548de0a3ff85e2471b6))
* **social-support:** add interview observations ([86aab92](https://gitlab.com/fi-sas/fisas-webpage/commit/86aab9228812dbf631283aeeff270c819c080e7c))
* **social-support:** add interview observations ([b7f97b3](https://gitlab.com/fi-sas/fisas-webpage/commit/b7f97b38bcdce5d179c058fd000d9d4430d820d6))
* **social-support:** add interview observations ([67705ed](https://gitlab.com/fi-sas/fisas-webpage/commit/67705ed175699ebcc3a785629d397b57bede452a))
* **social-support:** validate if advisor email exists ([852a9c0](https://gitlab.com/fi-sas/fisas-webpage/commit/852a9c03c0261fe47091db5de95b577caf36f559))
* **social-support:** validate if advisor email exists ([924e1cd](https://gitlab.com/fi-sas/fisas-webpage/commit/924e1cdce4053a167e06af109799d61c98df3355))
* **social-support:** validate if advisor email exists ([e974763](https://gitlab.com/fi-sas/fisas-webpage/commit/e9747634a891303072e5b74fd5bed6867bfd0c9b))
* **social-support:** validate if advisor email exists ([ad09abf](https://gitlab.com/fi-sas/fisas-webpage/commit/ad09abf5fc28c92be34dc38bf91d3a456b562260))

# [1.83.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.82.0...v1.83.0) (2021-11-09)


### Features

* **social-support:** allow to edit application form ([50d5310](https://gitlab.com/fi-sas/fisas-webpage/commit/50d5310b453ca92efc309381a6e55e0143bd86f5))
* **social-support:** allow to edit application form ([67a23a2](https://gitlab.com/fi-sas/fisas-webpage/commit/67a23a24d6e3d4a408aa82bd9717f0fff3e35d2d))
* **social-support:** fix edit application form workflow ([a6138fc](https://gitlab.com/fi-sas/fisas-webpage/commit/a6138fc29915b54ac261d351e6454d257917861d))
* **social-support:** remove commented code ([606ead9](https://gitlab.com/fi-sas/fisas-webpage/commit/606ead9b21d8db0ca83fe82e932820d5642c6df7))
* **volunteering:** allow to edit application form ([5a77724](https://gitlab.com/fi-sas/fisas-webpage/commit/5a77724c42ed238a40aba8b87470df09a359f26e))
* **volunteering:** fix edit application form workflow ([38675a2](https://gitlab.com/fi-sas/fisas-webpage/commit/38675a258a527ed6401165b4e72d9445d6d04ce7))

# [1.82.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.81.0...v1.82.0) (2021-11-09)


### Features

* **social-support:** add translations to dispatch workflow ([de19fb5](https://gitlab.com/fi-sas/fisas-webpage/commit/de19fb5bdc9ea52a2ddcfd2be4e062b303e5cdfc))
* **social-support:** add translations to dispatch workflow ([8ca5cc4](https://gitlab.com/fi-sas/fisas-webpage/commit/8ca5cc46f73067d4f77493a8ad74881d3116136f))
* **social-support:** added dispatch state ([77b6798](https://gitlab.com/fi-sas/fisas-webpage/commit/77b6798ca040c68241cf9fb8f0c1ff1c4e9c1559))
* **social-support:** show interview observations ([339f70d](https://gitlab.com/fi-sas/fisas-webpage/commit/339f70d8e7b378bb2255f164413961bc1b25b384))

# [1.81.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.7...v1.81.0) (2021-11-09)


### Features

* **packs:** add initial version of packs service ([97ed1de](https://gitlab.com/fi-sas/fisas-webpage/commit/97ed1de9c43db056bf82f86547fa920da3999860))
* **packs:** add menu packs page ([2218809](https://gitlab.com/fi-sas/fisas-webpage/commit/2218809907e2ddb8c3e38b838233c087b4b52936))

## [1.80.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.6...v1.80.7) (2021-11-08)


### Bug Fixes

* **geral:** style cards ([41afb73](https://gitlab.com/fi-sas/fisas-webpage/commit/41afb73813e7c8a3259a237ee0bedc16394fb042))
* **volunt,social_support:** validation add report ([409e30a](https://gitlab.com/fi-sas/fisas-webpage/commit/409e30a889267596e1078d24d0ebb0b6a976e7f7))

## [1.80.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.5...v1.80.6) (2021-11-05)


### Bug Fixes

* **geral:** change text ([cd144ad](https://gitlab.com/fi-sas/fisas-webpage/commit/cd144adbe89f1dbdf61caf567d932a483a5c7b9e))
* **social_support,volunt:** edit file ([ba31212](https://gitlab.com/fi-sas/fisas-webpage/commit/ba31212bad3d6b4cb464bf6f530416c81b3da6a4))
* **social_support,volunt:** file required ([a34de1f](https://gitlab.com/fi-sas/fisas-webpage/commit/a34de1fbeaef3a9334604d707b051898c25f84a4))
* **social,volunt:** error correction dates ([3fa7236](https://gitlab.com/fi-sas/fisas-webpage/commit/3fa72367390213645402939cb9a44da13e0adfd4))
* **volunt,social_sup:** delete file, setvalue null ([563c0fd](https://gitlab.com/fi-sas/fisas-webpage/commit/563c0fd45e0b63ae7fba3c52e5cb91cb991a1078))
* **volunt,social_support:** valid respon & advisor ([2fa0199](https://gitlab.com/fi-sas/fisas-webpage/commit/2fa0199cfbad825f523cbd2cc9c1592f0d493db6))

## [1.80.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.4...v1.80.5) (2021-11-05)


### Bug Fixes

* **social_support,volunt:** modal confirm edit ([e3923b1](https://gitlab.com/fi-sas/fisas-webpage/commit/e3923b173a971d9d20edf46adde3f2b04e1bd02e))

## [1.80.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.3...v1.80.4) (2021-11-05)


### Bug Fixes

* **geral:** changing button names ([bca7828](https://gitlab.com/fi-sas/fisas-webpage/commit/bca78288ab318a929bb3dc9c2e0f4b276275d86e))
* **social_support,volunt:** change text button ([a355fe2](https://gitlab.com/fi-sas/fisas-webpage/commit/a355fe2ad0eecdca6a9110277529d65b4f15d9d5))
* **social_support:** error correction loading ([d8e7a6a](https://gitlab.com/fi-sas/fisas-webpage/commit/d8e7a6a84a2f5fc483d94c0f0b03a4c8bdbb740f))
* **social_support:** error correction schedules ([bd9994e](https://gitlab.com/fi-sas/fisas-webpage/commit/bd9994ead3dacc13a4583514c8d9f64814d86cab))

## [1.80.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.2...v1.80.3) (2021-11-04)


### Bug Fixes

* **geral:** change text interview ([d9a40d9](https://gitlab.com/fi-sas/fisas-webpage/commit/d9a40d9e7e51bc3fee8e8586bd6b5c937280e8f5))
* **social_support:** change input to input data ([e1e830d](https://gitlab.com/fi-sas/fisas-webpage/commit/e1e830dae826ec493ec31dfac09928288b706c4b))
* **volunteering:** change input to input data ([6a01cb9](https://gitlab.com/fi-sas/fisas-webpage/commit/6a01cb9c9e509b8a3a4ced1612f029a34ceb82d7))

## [1.80.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.1...v1.80.2) (2021-11-04)


### Bug Fixes

* **package:** delete version ([b4e1302](https://gitlab.com/fi-sas/fisas-webpage/commit/b4e1302922bfc613a14f4be7958714bf373489bb))

## [1.80.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.80.0...v1.80.1) (2021-11-04)


### Bug Fixes

* **social_support,volunt:** tooltip button ([c6ea572](https://gitlab.com/fi-sas/fisas-webpage/commit/c6ea572638625164cea8f166c35e66ca5d79a36d))
* **social_support:** change icon file ([dc859bb](https://gitlab.com/fi-sas/fisas-webpage/commit/dc859bb1e7ce378c7e468e44a188416344cc3587))
* **social_support:** collaboration dates info ([93b1201](https://gitlab.com/fi-sas/fisas-webpage/commit/93b12012099f6ef0e4a9f68d2cd568eac7d91c08))
* **social_support:** date validation ([d7e1787](https://gitlab.com/fi-sas/fisas-webpage/commit/d7e17873a6f55087d62c290137bdf7ee0dfa8d0f))
* **social_support:** edit attendance record ([3664f0b](https://gitlab.com/fi-sas/fisas-webpage/commit/3664f0bc679fb658b6132fbe5b939ce61b945c5b))
* **social_support:** improvement detail experience ([a590899](https://gitlab.com/fi-sas/fisas-webpage/commit/a5908995b9b7eaf67c68a1759125f9d38b48228c))
* **social_support:** table on mobile ([d7910c1](https://gitlab.com/fi-sas/fisas-webpage/commit/d7910c1a98b7b80496c27aed3e8b76401ee3d4a2))
* **volunteering:** change icon file ([dbde206](https://gitlab.com/fi-sas/fisas-webpage/commit/dbde206eb592448dceb95d2e33f1b2964894a7ee))
* **volunteering:** collaboration dates info ([57d6d48](https://gitlab.com/fi-sas/fisas-webpage/commit/57d6d488f4f1b70f85da7e792895ac0a4f1acbad))
* **volunteering:** date validation ([d505811](https://gitlab.com/fi-sas/fisas-webpage/commit/d505811b92a2ee185473c9fe292b2545d3ed3056))
* **volunteering:** edit attendance record ([4ab936c](https://gitlab.com/fi-sas/fisas-webpage/commit/4ab936cc25a61e67b877d48c0743009dd28ea25a))
* **volunteering:** improvement detail experience ([bd26f2c](https://gitlab.com/fi-sas/fisas-webpage/commit/bd26f2c70a669358788acc8a31b7fad1bf84bb2c))
* **volunteering:** table on mobile ([0f3e547](https://gitlab.com/fi-sas/fisas-webpage/commit/0f3e547821b9809799d65cffdc7a52553b64b85f))

# [1.80.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.11...v1.80.0) (2021-11-04)


### Features

* **social-support:** added attachments to application form ([7050ecd](https://gitlab.com/fi-sas/fisas-webpage/commit/7050ecd99fb8384df6621f18fb85d194f6040775))
* **social-support:** added attachments to application form ([e332711](https://gitlab.com/fi-sas/fisas-webpage/commit/e332711fcb7d5f7d2212e471ece0172b6b46f02f))
* **social-support:** added attachments to application form ([ad3e41f](https://gitlab.com/fi-sas/fisas-webpage/commit/ad3e41f6be68d9f9eaf1f5f0a865d44fe87b7470))
* **social-support:** added attachments to application form ([795dc2a](https://gitlab.com/fi-sas/fisas-webpage/commit/795dc2a0927239620d51c04ae7faf255b21bc376))

## [1.79.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.10...v1.79.11) (2021-11-03)


### Bug Fixes

* **dashboard:** external services ([15495d4](https://gitlab.com/fi-sas/fisas-webpage/commit/15495d44599df76d3e1b2340454984b956654cff))

## [1.79.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.9...v1.79.10) (2021-11-02)


### Bug Fixes

* **geral:** new logos ([7b61f26](https://gitlab.com/fi-sas/fisas-webpage/commit/7b61f2613e53de1e0d2511fbeb2b2de17e598dac))

## [1.79.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.8...v1.79.9) (2021-11-02)


### Bug Fixes

* **dashboard:** configuration text ([adcc529](https://gitlab.com/fi-sas/fisas-webpage/commit/adcc52976da3341b5d1db35958ffff11c2bd8d00))

## [1.79.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.7...v1.79.8) (2021-10-29)


### Bug Fixes

* **accommodation:** required file iban ([bf89671](https://gitlab.com/fi-sas/fisas-webpage/commit/bf89671fad1109bb69b6ee657d9246911a71e35d))

## [1.79.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.6...v1.79.7) (2021-10-29)


### Bug Fixes

* **alimentation:** style tablet ([a2ea34a](https://gitlab.com/fi-sas/fisas-webpage/commit/a2ea34ac21d12eaef499eaed00047e894e24bdb2))

## [1.79.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.5...v1.79.6) (2021-10-29)


### Bug Fixes

* **accommodation:** filter residence ([079c547](https://gitlab.com/fi-sas/fisas-webpage/commit/079c5478ea286a6e552ada294f3aa000786a88ba))

## [1.79.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.4...v1.79.5) (2021-10-28)


### Bug Fixes

* **accommodation:** valid equal regimes or extras ([0a2fecd](https://gitlab.com/fi-sas/fisas-webpage/commit/0a2fecd6ed98484991c265d46f1449b85695acb5))

## [1.79.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.3...v1.79.4) (2021-10-27)


### Bug Fixes

* **accommodation:** change button ([ab73032](https://gitlab.com/fi-sas/fisas-webpage/commit/ab730326500e025144e2a20ab5458c9f1c61c7a1))
* **mobility:** aspect improvement review ([496697b](https://gitlab.com/fi-sas/fisas-webpage/commit/496697b9c1ff49a07e3d4567778460d2e5af9a72))
* **mobility:** mobile aspect ([c16c836](https://gitlab.com/fi-sas/fisas-webpage/commit/c16c836231f2f87b143712cd2a20646ea3eb826b))
* **volunteering:** new fields review application ([db26250](https://gitlab.com/fi-sas/fisas-webpage/commit/db26250a234a67809f4723bddfce8b668cc46946))
* **volunteering:** style correction ([6bff802](https://gitlab.com/fi-sas/fisas-webpage/commit/6bff80240570b697bda4585115a31952267e360f))
* **volunteering:** top page after apply ([8fba021](https://gitlab.com/fi-sas/fisas-webpage/commit/8fba0210757cd7d2abfd759570fe8a525677cf4e))

## [1.79.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.2...v1.79.3) (2021-10-26)


### Bug Fixes

* **private_accommodation:** property title en ([9536fbb](https://gitlab.com/fi-sas/fisas-webpage/commit/9536fbb42472272f38c998b721cd6c2b28fb76ea))

## [1.79.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.1...v1.79.2) (2021-10-25)


### Bug Fixes

* **geral:** error message on top forms ([6d64c71](https://gitlab.com/fi-sas/fisas-webpage/commit/6d64c71601110baa954470a316170aa9ad165440))
* **geral:** maximum limit message mb files ([5adc1ca](https://gitlab.com/fi-sas/fisas-webpage/commit/5adc1ca3c89a4b216b94803c5bc8016b6947c14f))
* **geral:** message type of files allowed ([c5ed779](https://gitlab.com/fi-sas/fisas-webpage/commit/c5ed779bc12010baf220b1f85d06d6d986b23864))
* **geral:** removal of ? of the questions ([50c14c7](https://gitlab.com/fi-sas/fisas-webpage/commit/50c14c77c527503472e2be03c0fdf2f3079d92ca))
* **social_support:** change languages field ([f0354f6](https://gitlab.com/fi-sas/fisas-webpage/commit/f0354f62648409ed0651602fd6d8c3674427e0bf))
* **volunteering:** change languages field ([5b06f91](https://gitlab.com/fi-sas/fisas-webpage/commit/5b06f91f5e48d0e59351959271dacde904478720))

## [1.79.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.79.0...v1.79.1) (2021-10-22)


### Bug Fixes

* **acommodation:** delete swift ([ec5b913](https://gitlab.com/fi-sas/fisas-webpage/commit/ec5b9134572e21ce7d469af680588b3861b75c31))
* **auth:** change message isProfileDataCompleted ([0da6227](https://gitlab.com/fi-sas/fisas-webpage/commit/0da6227cbab31fe04823080ec7dfe148434e16c4))
* **private_accommodation:** change text en ([d24fa7d](https://gitlab.com/fi-sas/fisas-webpage/commit/d24fa7deb8c6508dd7aa17bd82d476225574e9b2))
* **private_accommodation:** icon filter ([f57a075](https://gitlab.com/fi-sas/fisas-webpage/commit/f57a07570cc3c697a31cecee66976585ff97c094))
* **private_accommodation:** remove readmore ([3f59d63](https://gitlab.com/fi-sas/fisas-webpage/commit/3f59d635087a59bc416f183050f0a991d52b8743))
* **sidebar:** add side menus ([9712fe3](https://gitlab.com/fi-sas/fisas-webpage/commit/9712fe349d1ed38c701fc9a9ea354746f97f9485))
* **ubike:** change message application closed ([9579741](https://gitlab.com/fi-sas/fisas-webpage/commit/9579741932c954ff1f9b704b913cb68b1c809721))
* **ubike:** change message modal ([9177f9d](https://gitlab.com/fi-sas/fisas-webpage/commit/9177f9df02585a98861a718b01d5c25672cd7c03))
* **ubike:** change radio to select ([0b08ca1](https://gitlab.com/fi-sas/fisas-webpage/commit/0b08ca1985b4e3ea02e5762f3dda94e75db48e0f))
* **ubike:** remove modal submit form ([33df364](https://gitlab.com/fi-sas/fisas-webpage/commit/33df3648e221055956b32ed8f9c9d81d2aa5680d))
* **ubike:** validation form nz-time-picker ([0965a2a](https://gitlab.com/fi-sas/fisas-webpage/commit/0965a2a754716d6921340a94c2195ab81c4a9172))
* **upload:** validation so as not to give an error ([9dae680](https://gitlab.com/fi-sas/fisas-webpage/commit/9dae6803351ee7daeae5c81b7620bfad35d26cfb))

# [1.79.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.5...v1.79.0) (2021-10-21)


### Bug Fixes

* **auth:** modal isProfileDataCompleted ([b7e83c6](https://gitlab.com/fi-sas/fisas-webpage/commit/b7e83c6a067682b3de2930160fe20a167a2b2414))
* **geral:** save change language refresh page ([8508910](https://gitlab.com/fi-sas/fisas-webpage/commit/850891066c7ee9cb2687ebd32f30ae7cdb9d546c))
* **queue:** correction error reason field ([edaaf59](https://gitlab.com/fi-sas/fisas-webpage/commit/edaaf5908b1eb1e310b5ed3f1ba9abe8bbb54aa9))
* **queue:** expert review 1, change route queue ([a3df264](https://gitlab.com/fi-sas/fisas-webpage/commit/a3df264fdb9e13d85364268dbf5e46b09a4cba01))


### Features

* **alimentantion:** new page pack settings ([1f41296](https://gitlab.com/fi-sas/fisas-webpage/commit/1f41296e21a64e434785d8ea86680da82c85fb4d))

## [1.78.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.4...v1.78.5) (2021-10-20)


### Bug Fixes

* **social_support:** change color status ([691da55](https://gitlab.com/fi-sas/fisas-webpage/commit/691da554e904def605e8b084be5d90b71bb28446))

## [1.78.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.3...v1.78.4) (2021-10-20)


### Bug Fixes

* **geral:** color expired status ([ce07a9d](https://gitlab.com/fi-sas/fisas-webpage/commit/ce07a9d6d6709bd49ee26d9fa08b7329defa2a6f))

## [1.78.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.2...v1.78.3) (2021-10-20)


### Bug Fixes

* **dashboard:** change color status ([1957fb6](https://gitlab.com/fi-sas/fisas-webpage/commit/1957fb66c33370e41c8f92f4c8b02dc31132b24e))
* **social_support:** status color change ([4db881d](https://gitlab.com/fi-sas/fisas-webpage/commit/4db881d8dfa2c32765f7deff38ce62bc66982cd7))
* **volunteering:** status color change ([dbdbf4b](https://gitlab.com/fi-sas/fisas-webpage/commit/dbdbf4b134458c5227c2d8cd7a4c38ed91edcd0c))

## [1.78.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.1...v1.78.2) (2021-10-20)


### Bug Fixes

* **alimentation:** select pedefined canteen ([ccb8ceb](https://gitlab.com/fi-sas/fisas-webpage/commit/ccb8ceb304e2365a967c1b814b0f2f3ea398ae86))
* **mobility:** change cards ([42edeb3](https://gitlab.com/fi-sas/fisas-webpage/commit/42edeb3eb855152beaa00782956ab6f1eef25f0d))

## [1.78.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.78.0...v1.78.1) (2021-10-19)


### Bug Fixes

* **accommodation:** modal history ([ad098c2](https://gitlab.com/fi-sas/fisas-webpage/commit/ad098c2909825ab64c47760f019a8481b593e278))

# [1.78.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.77.1...v1.78.0) (2021-10-18)


### Features

* accomodation application add swift field ([5eb3b88](https://gitlab.com/fi-sas/fisas-webpage/commit/5eb3b880ba3463d63b3237544ec71ccbaab37de6))

## [1.77.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.77.0...v1.77.1) (2021-10-18)


### Bug Fixes

* **geral:** change icons ([2530f55](https://gitlab.com/fi-sas/fisas-webpage/commit/2530f5580f10e91b8746bec22dd5bd8cdd806ed0))

# [1.77.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.5...v1.77.0) (2021-10-15)


### Bug Fixes

* **button:** correcção estilo do butão ([123cdd9](https://gitlab.com/fi-sas/fisas-webpage/commit/123cdd9b33e43866a169edb34712a951af322dc8))
* **design:** correcção de alguns elementos ([575bf39](https://gitlab.com/fi-sas/fisas-webpage/commit/575bf39f0af52982e5669e89800af3ca6fac2459))


### Features

* **appointments:** listar marcações e algumas correcções ([04c4862](https://gitlab.com/fi-sas/fisas-webpage/commit/04c4862ce32d756ee0c0562059b573d7d9171831))
* **specialty:** novo campo informação de preço ([8005b7b](https://gitlab.com/fi-sas/fisas-webpage/commit/8005b7b8a82fe31e140b9e465364bc1223309403))

## [1.76.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.4...v1.76.5) (2021-10-14)


### Bug Fixes

* **alimentation:** change problem dates ([7c0a2f5](https://gitlab.com/fi-sas/fisas-webpage/commit/7c0a2f582aa279b24a32566460793ef5198c77f6))

## [1.76.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.3...v1.76.4) (2021-10-14)


### Bug Fixes

* **alimentation:** commented list code ([ac829aa](https://gitlab.com/fi-sas/fisas-webpage/commit/ac829aa6d7144252c7242b219ddce6166f7eb505))
* **social-support:** allow to duplicate PT field to EN ([4ce179d](https://gitlab.com/fi-sas/fisas-webpage/commit/4ce179d33e95e4cb90e5b9f219dc3602b49627c1))
* **social-support:** allow to duplicate PT field to EN ([c258c06](https://gitlab.com/fi-sas/fisas-webpage/commit/c258c06b12f984a2ba7438d3ece01653871905b9))
* **social-support:** show new fields on application ([8b2e37a](https://gitlab.com/fi-sas/fisas-webpage/commit/8b2e37ab11bde3d05a73e28ef284193e694eecfc))
* **volunteering:** allow to duplicate PT field to EN ([0817154](https://gitlab.com/fi-sas/fisas-webpage/commit/0817154a5f490c3ffa082ed79c5115a5b64381c9))
* **volunteering:** allow to duplicate PT field to EN ([64b1673](https://gitlab.com/fi-sas/fisas-webpage/commit/64b1673268f1fa6ed37d8c1ed43e2e53c85ec196))

## [1.76.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.2...v1.76.3) (2021-10-13)


### Bug Fixes

* **ubike:** change text ([f2c38e4](https://gitlab.com/fi-sas/fisas-webpage/commit/f2c38e48be58f4acd6d4c6858ee9778d793217bc))
* **ubike:** documents in the form not mandatory ([a01d5e7](https://gitlab.com/fi-sas/fisas-webpage/commit/a01d5e72ec1bf0ab3aaebf559e6e804159a105c6))

## [1.76.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.1...v1.76.2) (2021-10-13)


### Bug Fixes

* **geral:** delete current acounts header ([d2ca156](https://gitlab.com/fi-sas/fisas-webpage/commit/d2ca1561b39666ecf117507ca09e98b04f353ac9))

## [1.76.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.76.0...v1.76.1) (2021-10-13)


### Bug Fixes

* **alimentation:** change text period ([bdadd32](https://gitlab.com/fi-sas/fisas-webpage/commit/bdadd323a6de9deeeb31896bee4ea30b7a3db307))
* **current_account:** width loading ([ccae291](https://gitlab.com/fi-sas/fisas-webpage/commit/ccae2918a403f1613fcfd4ac4d3829308de2857d))
* **queue:** change text ([381e34f](https://gitlab.com/fi-sas/fisas-webpage/commit/381e34f024692c768d977f2bc44e946d80b0891e))
* **ubike:** change card application ([d61b20b](https://gitlab.com/fi-sas/fisas-webpage/commit/d61b20b5ecd63de7a6bd3600374505d2390ff121))
* **ubike:** change disabled dates ([6dd78bd](https://gitlab.com/fi-sas/fisas-webpage/commit/6dd78bdd64ecdd4921b59d7e86e2af0281344426))
* **ubike:** change name status ([407fe0e](https://gitlab.com/fi-sas/fisas-webpage/commit/407fe0e610563b3dc6dfe6fb1b951aa7779a6af6))
* **ubike:** delete component ([c7fe3da](https://gitlab.com/fi-sas/fisas-webpage/commit/c7fe3da0947b5f23b5bc48d1f42792551ae37208))
* **ubike:** disabled date + text correction ([d197bbf](https://gitlab.com/fi-sas/fisas-webpage/commit/d197bbf1c980391e2c912216891972a9c00da4b1))
* **ubike:** pagination occurrences ([bc6e958](https://gitlab.com/fi-sas/fisas-webpage/commit/bc6e9580cba9cbd330aba9e4a7a419741c515f90))

# [1.76.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.75.1...v1.76.0) (2021-10-13)


### Features

* **social-support:** added new fields to application form ([90cace5](https://gitlab.com/fi-sas/fisas-webpage/commit/90cace533b31adfdc3e9bdcbce1a2af499b8d1dd))
* **social-support:** added new fields to application form ([7b6a190](https://gitlab.com/fi-sas/fisas-webpage/commit/7b6a190926fb72846339368e3fcc93189df2d29a))

## [1.75.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.75.0...v1.75.1) (2021-10-12)


### Bug Fixes

* **alimentation:** put empty values the 1st time ([69514ff](https://gitlab.com/fi-sas/fisas-webpage/commit/69514ff7139512961434764e554539a31c0e1524))
* **geral:** text change buttons confirm modal ([cb421c6](https://gitlab.com/fi-sas/fisas-webpage/commit/cb421c66b06033f57118bc472c5ac14d19063a2c))
* **geral:** textarea limit 255 caracters ([86c798d](https://gitlab.com/fi-sas/fisas-webpage/commit/86c798da022946924b6c794387c6ba53bcf66304))
* **ubike:** change type occurence mobile ([9bf1502](https://gitlab.com/fi-sas/fisas-webpage/commit/9bf1502f5c0172182a9a42f437dc960fcf1de34c))

# [1.75.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.74.0...v1.75.0) (2021-10-12)


### Features

* **general:** add presentation text to services ([5d4b17d](https://gitlab.com/fi-sas/fisas-webpage/commit/5d4b17d94f812503438a30023eca978add31cc65))
* **general:** add presentation text to services ([7a7c8d4](https://gitlab.com/fi-sas/fisas-webpage/commit/7a7c8d489670d1ff664e6b8a3c40dd8e82f4eac7))
* **general:** add presentation text to services ([d2f6136](https://gitlab.com/fi-sas/fisas-webpage/commit/d2f6136e42dbaa895d2d72840db8580050d50416))
* **general:** add presentation text to services ([206abe8](https://gitlab.com/fi-sas/fisas-webpage/commit/206abe880359ae8893b81c36a57ab9277e104a7d))
* **general:** add presentation text to services ([b9e5afb](https://gitlab.com/fi-sas/fisas-webpage/commit/b9e5afb953b3003d2fe5a3b1efaa36e50e7db574))
* **general:** add presentation text to services ([7927a98](https://gitlab.com/fi-sas/fisas-webpage/commit/7927a986fdfce824ccb8c79f3535aada5882147b))

# [1.74.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.14...v1.74.0) (2021-10-12)


### Bug Fixes

* **accommodation:** correction error ([a5f0069](https://gitlab.com/fi-sas/fisas-webpage/commit/a5f006954dbc417707b59c1218902c65acbba574))
* **current_account:** change message deposit ([1d6e3fd](https://gitlab.com/fi-sas/fisas-webpage/commit/1d6e3fd89460a0c397329f9e8a40cb28ecb7464b))
* **social_support:** change complain button ([98991e7](https://gitlab.com/fi-sas/fisas-webpage/commit/98991e7ac0f2888be08c3d298927a960c39344dc))
* **system-error:** change messages error ([c4e7da8](https://gitlab.com/fi-sas/fisas-webpage/commit/c4e7da88973e12996e6b11a511350f5c134ae30a))
* **volunteering:** change complain button ([5e9676e](https://gitlab.com/fi-sas/fisas-webpage/commit/5e9676e5f81095726bec017b572ed852d9846855))


### Features

* **current_account:** new current acount part top ([f7501b1](https://gitlab.com/fi-sas/fisas-webpage/commit/f7501b17c74482b2700e27477bf853164cd1bf01))

## [1.73.14](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.13...v1.73.14) (2021-10-11)


### Bug Fixes

* **alimentation:** change modal buy ([df09185](https://gitlab.com/fi-sas/fisas-webpage/commit/df091857cdac72f3cfb0a1f1415b00666ad6fda2))
* **alimentation:** change purchase button & detail ([f83fea7](https://gitlab.com/fi-sas/fisas-webpage/commit/f83fea7e5536fef3dd8d492b7555088a582bed70))
* **geral:** change step forms ([1bbb1bc](https://gitlab.com/fi-sas/fisas-webpage/commit/1bbb1bc583ee417f839cb9ec155f42296fb2294a))
* **geral:** message required fields ([e724e9a](https://gitlab.com/fi-sas/fisas-webpage/commit/e724e9a38cf1b5dbae3e5320983e88b03c03cf48))
* **geral:** modal success report ([0d84d01](https://gitlab.com/fi-sas/fisas-webpage/commit/0d84d01f56e6b1d375cf001c0f818e8c45ba6be5))
* **private_accommodation:** change step form ([18c4091](https://gitlab.com/fi-sas/fisas-webpage/commit/18c40917af3d121fc94eb0c6181e410efe2ef6fc))
* **ubike:** change color & text application status ([56f11ea](https://gitlab.com/fi-sas/fisas-webpage/commit/56f11ea4f82c8d36903c4f2ef3985573ba2f69db))
* **ubike:** change start dashboard ([3405e4e](https://gitlab.com/fi-sas/fisas-webpage/commit/3405e4e0d23834d31fad7c43cb4e8ef80d601d81))
* **ubike:** change text info dashboard ([873b78d](https://gitlab.com/fi-sas/fisas-webpage/commit/873b78d128bf722007e2dcc3937699c14d0d567d))
* **unauthorized:** change text ([a60ea86](https://gitlab.com/fi-sas/fisas-webpage/commit/a60ea86f6eb5f2be1c5552d088679aa763b7a74e))

## [1.73.13](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.12...v1.73.13) (2021-10-07)


### Bug Fixes

* **alimentation:** modal success purchase ticket ([e87bf61](https://gitlab.com/fi-sas/fisas-webpage/commit/e87bf615d9b3da97f9663478f5c202b15cc3b2d4))

## [1.73.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.11...v1.73.12) (2021-10-06)


### Bug Fixes

* **news:** add pagination ([4d4f314](https://gitlab.com/fi-sas/fisas-webpage/commit/4d4f31458bafd07fcbc176909f86168b8c9b8af3))

## [1.73.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.10...v1.73.11) (2021-10-01)


### Bug Fixes

* **alimentation:** error anullation date ([41bf049](https://gitlab.com/fi-sas/fisas-webpage/commit/41bf049bdd25b09d5a3fca7609a80284c2af475c))

## [1.73.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.9...v1.73.10) (2021-09-30)


### Bug Fixes

* **alimentation:** corretion size col ([9f99114](https://gitlab.com/fi-sas/fisas-webpage/commit/9f99114dd356210f0fd2b4b4e3331cb056a8c23e))

## [1.73.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.8...v1.73.9) (2021-09-30)


### Bug Fixes

* **alimentation:** title canteen, arrow accommodat ([85ee3e4](https://gitlab.com/fi-sas/fisas-webpage/commit/85ee3e44949218d6d1ea6db63533f784dfe98618))

## [1.73.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.7...v1.73.8) (2021-09-29)


### Bug Fixes

* **private-accommodation:** sort cities ([7e8343e](https://gitlab.com/fi-sas/fisas-webpage/commit/7e8343edf597f2df7ad32382a3ce01116f4fff4c))

## [1.73.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.6...v1.73.7) (2021-09-29)


### Bug Fixes

* **social-support:** fix to when user goes back on form ([54e67db](https://gitlab.com/fi-sas/fisas-webpage/commit/54e67db4efaad29e94718ac75acd9a22458d08d0))

## [1.73.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.5...v1.73.6) (2021-09-29)


### Bug Fixes

* **social_support:** color change required ([ce90fee](https://gitlab.com/fi-sas/fisas-webpage/commit/ce90feedca88f9f5322addc83d960448fac85f85))

## [1.73.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.4...v1.73.5) (2021-09-29)


### Bug Fixes

* **geral:** requested changes login ([04329d3](https://gitlab.com/fi-sas/fisas-webpage/commit/04329d313be2ae3dcaf1dd7aeed09f6d432ad907))

## [1.73.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.3...v1.73.4) (2021-09-29)


### Bug Fixes

* **volunteering:** fix fields not showing errors ([21124de](https://gitlab.com/fi-sas/fisas-webpage/commit/21124de23f1b95b5ad2ec607af5887a1d1458e6c))

## [1.73.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.2...v1.73.3) (2021-09-29)


### Bug Fixes

* **social-support:** fix fields not showing errors ([e0622b7](https://gitlab.com/fi-sas/fisas-webpage/commit/e0622b7ff1b0d2a55c79d0d16db7fc770d8e7065))
* **volunteering:** correction required form action ([d0f35f9](https://gitlab.com/fi-sas/fisas-webpage/commit/d0f35f98b6ddc019e1aa874d328ab794b964f28e))

## [1.73.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.1...v1.73.2) (2021-09-29)


### Bug Fixes

* **geral:** correction error forms ([5be65c1](https://gitlab.com/fi-sas/fisas-webpage/commit/5be65c180126cc733fe346bf5c60c4d86ea90084))

## [1.73.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.73.0...v1.73.1) (2021-09-29)


### Bug Fixes

* **accommodation:** add modal confirm, report ([c4d4074](https://gitlab.com/fi-sas/fisas-webpage/commit/c4d40742b8854e57fb593349bc18eccf61657d27))

# [1.73.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.6...v1.73.0) (2021-09-29)


### Features

* **social-support:** show only active activities ([6de89c9](https://gitlab.com/fi-sas/fisas-webpage/commit/6de89c977457c209839d407ae6aca9a61d1b901e))
* **volunteering:** show only active activities ([e863737](https://gitlab.com/fi-sas/fisas-webpage/commit/e8637373a1edb6f4b41b36735a8c6069c5ff025d))

## [1.72.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.5...v1.72.6) (2021-09-28)


### Bug Fixes

* **geral:** change menu mobile, add badge menu ([901e09b](https://gitlab.com/fi-sas/fisas-webpage/commit/901e09bfd429a779f94a305ed312a98297582c87))

## [1.72.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.4...v1.72.5) (2021-09-28)


### Bug Fixes

* **geral:** correction error fixed menu ([569d4fb](https://gitlab.com/fi-sas/fisas-webpage/commit/569d4fbc84f818fdfb8da688a08ce261f970c334))

## [1.72.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.3...v1.72.4) (2021-09-28)


### Bug Fixes

* **geral:** correction error ([f5f8e74](https://gitlab.com/fi-sas/fisas-webpage/commit/f5f8e74a1e8c3284f7801c76373088b68ca16037))

## [1.72.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.2...v1.72.3) (2021-09-28)


### Bug Fixes

* **geral:** fix side menu ([cb2eadc](https://gitlab.com/fi-sas/fisas-webpage/commit/cb2eadc474795ebf07494e3d08060f1e94fe227a))

## [1.72.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.1...v1.72.2) (2021-09-27)


### Bug Fixes

* **geral:** change switch to radio ([eb41564](https://gitlab.com/fi-sas/fisas-webpage/commit/eb41564568287fd2392d1e701e0cd0c9dc2b43c8))

## [1.72.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.72.0...v1.72.1) (2021-09-25)


### Bug Fixes

* **canttens:** on cancel reservation update data ([531370a](https://gitlab.com/fi-sas/fisas-webpage/commit/531370a3421d3d02c58aa280bfc0ea9a57dfe4aa))

# [1.72.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.71.2...v1.72.0) (2021-09-24)


### Features

* **bus:** add option of download declaration ([6458b36](https://gitlab.com/fi-sas/fisas-webpage/commit/6458b363e51d30dbac0d452e431b423934b1d555))

## [1.71.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.71.1...v1.71.2) (2021-09-24)


### Bug Fixes

* **accommodation:** add user_can_apply and unassigned validation ([d1c3732](https://gitlab.com/fi-sas/fisas-webpage/commit/d1c37325bb276c60789b25f4f80b518001fcd7c6))

## [1.71.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.71.0...v1.71.1) (2021-09-22)


### Bug Fixes

* build production errors fix ([dafc125](https://gitlab.com/fi-sas/fisas-webpage/commit/dafc125a73bce7e9490810fd5a5f4ccddb708f4c))

# [1.71.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.70.4...v1.71.0) (2021-09-21)


### Bug Fixes

* **label:** showing label ([410e965](https://gitlab.com/fi-sas/fisas-webpage/commit/410e965e0b13a4682222de2d0c44a10cda8f88f9))


### Features

* **specialty:** view specialty information ([cd36a13](https://gitlab.com/fi-sas/fisas-webpage/commit/cd36a133b35e8f97c2731490e48e9b452a5d23e9))

## [1.70.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.70.3...v1.70.4) (2021-09-20)


### Bug Fixes

* **mobility:** remove cc_emitted_in from sub23 declaration ([ba4ced1](https://gitlab.com/fi-sas/fisas-webpage/commit/ba4ced13bf83275be6fb094a35269e8661f58578))

## [1.70.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.70.2...v1.70.3) (2021-09-20)


### Bug Fixes

* **accommodation:** add withdrawal option ([47ba187](https://gitlab.com/fi-sas/fisas-webpage/commit/47ba18782df1c0afd8d281b8ca3259a88e4d5205))

## [1.70.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.70.1...v1.70.2) (2021-09-20)


### Bug Fixes

* **accommodation:** change name of premute room ([4805fa1](https://gitlab.com/fi-sas/fisas-webpage/commit/4805fa163b31a07bae09b154ff0b60aed3f01043))

## [1.70.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.70.0...v1.70.1) (2021-09-20)


### Bug Fixes

* **mobility:** change on sub23 declaration form ([d5d9f5a](https://gitlab.com/fi-sas/fisas-webpage/commit/d5d9f5a986f59797dfcc5246f33d5d4a7110981f))

# [1.70.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.69.2...v1.70.0) (2021-09-17)


### Bug Fixes

* **translations:** remove error ([f8f6275](https://gitlab.com/fi-sas/fisas-webpage/commit/f8f6275770e922a230066270dd2cc1a81d5a48b2))


### Features

* **accommodation:** add iban change request ([1027e3d](https://gitlab.com/fi-sas/fisas-webpage/commit/1027e3d70bdd96a5ec85dd2f161d8f642271ffcd))

## [1.69.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.69.1...v1.69.2) (2021-09-17)


### Bug Fixes

* **accommodation:** cancel application status subm ([b1d364e](https://gitlab.com/fi-sas/fisas-webpage/commit/b1d364e4527221e64087257a5e8c51e4f9236456))

## [1.69.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.69.0...v1.69.1) (2021-09-17)


### Bug Fixes

* **environment:** fix environemnt health url ([c96a1f2](https://gitlab.com/fi-sas/fisas-webpage/commit/c96a1f24365de3291c7159ba216b0e6300eba878))

# [1.69.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.68.0...v1.69.0) (2021-09-17)


### Bug Fixes

* **application-model-mobility:** update model ([5d0764e](https://gitlab.com/fi-sas/fisas-webpage/commit/5d0764e2548df9a4f57d03c76e8c30ce84eb16b3))
* **mobility-form-application:** add academic year ([94d3b73](https://gitlab.com/fi-sas/fisas-webpage/commit/94d3b730d6cdaada8c157018f8371b41b3b89403))
* **view-list-applications:** add academic_year ([92741a4](https://gitlab.com/fi-sas/fisas-webpage/commit/92741a4aaff1a086f781e2a6f5f00f6e6bf8e2bd))


### Features

* **fimr_sub23_declaration:** add cc_emitted_in ([56be033](https://gitlab.com/fi-sas/fisas-webpage/commit/56be03317a415f516c3973370cf10c8459719170))
* **translations:** add translations ([77809b7](https://gitlab.com/fi-sas/fisas-webpage/commit/77809b7fe11cfd9b6de5e3bdc6b37ca2f3b0fde1))
* **translations-form-sub23:** add cc_emitted_in ([e77a703](https://gitlab.com/fi-sas/fisas-webpage/commit/e77a70352e45c72773de5f1f3c0a2d80b97750ae))

# [1.68.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.67.2...v1.68.0) (2021-09-17)


### Bug Fixes

* **proxy:** reverter o endereço do target ([96356bb](https://gitlab.com/fi-sas/fisas-webpage/commit/96356bb0337c1b8b6f177040b71f986eaaa1de9d))


### Features

* **health:** ver especialidades saude ([4e69126](https://gitlab.com/fi-sas/fisas-webpage/commit/4e6912606c7bed68766e8e895ff021dcd350c704))

## [1.67.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.67.1...v1.67.2) (2021-09-16)


### Bug Fixes

* **accommodation:** remove the indication room ([3695c28](https://gitlab.com/fi-sas/fisas-webpage/commit/3695c2864575c5d7bc8f3b412f17229dd3d5df8c))

## [1.67.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.67.0...v1.67.1) (2021-09-15)


### Bug Fixes

* **accommodation:** color icon ([9c2fda4](https://gitlab.com/fi-sas/fisas-webpage/commit/9c2fda4472c3c0ff7a86046bb0233cf91eb59b50))

# [1.67.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.66.1...v1.67.0) (2021-09-15)


### Features

* **social-support:** disable withdraw button when in withdrawal ([e9ecd14](https://gitlab.com/fi-sas/fisas-webpage/commit/e9ecd14f4241270eadc3f1df7add9c4f9c267009))
* **volunteering:** disable withdraw button when in withdrawal ([e9f0c22](https://gitlab.com/fi-sas/fisas-webpage/commit/e9f0c228274f165190d5554048cb8fedf4735dfe))

## [1.66.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.66.0...v1.66.1) (2021-09-15)


### Bug Fixes

* **volunteering:** reload actions after widthrawal ([4ca4810](https://gitlab.com/fi-sas/fisas-webpage/commit/4ca48105105a3ef7182652de9e861a5d4f9bac33))
* **volunteering:** reload actions after widthrawal ([a0d6a36](https://gitlab.com/fi-sas/fisas-webpage/commit/a0d6a360166493344a10d4602e3ded551143ff33))

# [1.66.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.65.0...v1.66.0) (2021-09-15)


### Bug Fixes

* **accommodation:** missing files on last mr ([45b497a](https://gitlab.com/fi-sas/fisas-webpage/commit/45b497a362a0c70cbd883639784ee33568471477))


### Features

* **ubike:** added info texts to form application ([a275a88](https://gitlab.com/fi-sas/fisas-webpage/commit/a275a88f9b894d8c0d02cd5a1fedfc3f71c6108f))
* **ubike:** added info texts to form application ([8987b27](https://gitlab.com/fi-sas/fisas-webpage/commit/8987b27bfee13498d94726466387b4b8ab21e73f))
* **volunteering:** show message when not active application ([985a934](https://gitlab.com/fi-sas/fisas-webpage/commit/985a9344c8b0ba044b63080b476f34f21320fea5))
* **volunteering:** show message when not active application ([b9f8e31](https://gitlab.com/fi-sas/fisas-webpage/commit/b9f8e31b3a6c3b0ad73c4b06c9251bdbe9be347f))

# [1.65.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.64.1...v1.65.0) (2021-09-15)


### Features

* **mobility-sub-23:** add validations and reject reason ([aa810cd](https://gitlab.com/fi-sas/fisas-webpage/commit/aa810cd7585f83bd9f7e3223258577e4706565b9))

## [1.64.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.64.0...v1.64.1) (2021-09-15)


### Bug Fixes

* **accommodation:** validation observation ([9bff7ae](https://gitlab.com/fi-sas/fisas-webpage/commit/9bff7aed6938388455b91b644132426d7d3f8f5c))

# [1.64.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.63.3...v1.64.0) (2021-09-14)


### Features

* **accommodation:** add application entry/exit communications ([fb8b668](https://gitlab.com/fi-sas/fisas-webpage/commit/fb8b668f3be50759156bfd98f000d1eee7972ebf))
* **accommodation:** add application form IBAN and direct debit flag ([cb157c9](https://gitlab.com/fi-sas/fisas-webpage/commit/cb157c9dae2e536cc1990a117ce551003133e03b))
* **accommodation:** add show iban configuration ([d17bcec](https://gitlab.com/fi-sas/fisas-webpage/commit/d17bcec1903425ca88821b569c57af8a801ee526))

## [1.63.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.63.2...v1.63.3) (2021-09-13)


### Bug Fixes

* **geral:** change validation token ([b879f09](https://gitlab.com/fi-sas/fisas-webpage/commit/b879f09fd73688c743d8914740bec2f1c56ecbf6))

## [1.63.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.63.1...v1.63.2) (2021-09-10)


### Bug Fixes

* **geral:** change validation token module check ([b1c4ca0](https://gitlab.com/fi-sas/fisas-webpage/commit/b1c4ca068ad0933768a29d7041ec051522201945))

## [1.63.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.63.0...v1.63.1) (2021-09-10)


### Bug Fixes

* **geral:** document review ([6185a68](https://gitlab.com/fi-sas/fisas-webpage/commit/6185a68616ac2633b8affd31ead1c45e263127ac))

# [1.63.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.6...v1.63.0) (2021-09-09)


### Bug Fixes

* **volunteering:** remove trim validation from number fields ([eaf1c5e](https://gitlab.com/fi-sas/fisas-webpage/commit/eaf1c5e13a9fe8eaf37d176df1a3c02802778981))


### Features

* **ubike:** now complaint form assumes system date ([01fc551](https://gitlab.com/fi-sas/fisas-webpage/commit/01fc551dd5b6f162169bc5d8edc214c26bdaff6e))

## [1.62.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.5...v1.62.6) (2021-09-09)


### Bug Fixes

* **accommodation:** new page layout ([d3c4994](https://gitlab.com/fi-sas/fisas-webpage/commit/d3c4994897dc41794c7cb953bc77a5a75ccc79bb))

## [1.62.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.4...v1.62.5) (2021-09-08)


### Bug Fixes

* **ubike-application-form:** update required ([581a6ad](https://gitlab.com/fi-sas/fisas-webpage/commit/581a6adc78c043804855a1ac2cd1d03769b80757))

## [1.62.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.3...v1.62.4) (2021-09-08)


### Bug Fixes

* **volunteering:** mark as valid when all fields disabled ([a665095](https://gitlab.com/fi-sas/fisas-webpage/commit/a665095353140c40db7582925e2be01133183146))

## [1.62.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.2...v1.62.3) (2021-09-06)


### Bug Fixes

* **route-detail-html:** update view route ([efd6f61](https://gitlab.com/fi-sas/fisas-webpage/commit/efd6f6197e8644fc0c071b517656c183f87b7311))
* **route-row-html:** remove number ([85b1505](https://gitlab.com/fi-sas/fisas-webpage/commit/85b15055c08a63e9b966d8e02303f0ea3c67def2))

## [1.62.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.1...v1.62.2) (2021-09-03)


### Bug Fixes

* **accommodation:** blocked fields profile form ([aba9958](https://gitlab.com/fi-sas/fisas-webpage/commit/aba99581bde1e083e5884d740e3f454066b023c4))

## [1.62.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.62.0...v1.62.1) (2021-09-02)


### Bug Fixes

* **bus-route-detail:** add route name ([8da7c8e](https://gitlab.com/fi-sas/fisas-webpage/commit/8da7c8e2d4f0740ab296a312a44dfe61e103a7bf))
* **bus-route-detail:** update router info detail ([03f3394](https://gitlab.com/fi-sas/fisas-webpage/commit/03f339409c6b800c39d29d2a75418bc878349c44))
* **bus-route-search:** update route search ([e2ea6ea](https://gitlab.com/fi-sas/fisas-webpage/commit/e2ea6ea6fdb4e7fa3d463824e55115b7db080b5e))
* **bus-service:** update service ([049d956](https://gitlab.com/fi-sas/fisas-webpage/commit/049d9562a532bd4b2f5b19abf382e40c1c2ebe84))
* **mobilidade-route-detail:** update info model ([34446e6](https://gitlab.com/fi-sas/fisas-webpage/commit/34446e63a495daaebbd52a6ae86dd76064d608a2))

# [1.62.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.61.0...v1.62.0) (2021-09-02)


### Features

* **accommodation:** new part other documents form ([a72f2d5](https://gitlab.com/fi-sas/fisas-webpage/commit/a72f2d571643a3435bf0926d95e638ba34d1a653))

# [1.61.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.5...v1.61.0) (2021-09-01)


### Bug Fixes

* **ubike-application-service:** update services dashboard ([e1151d0](https://gitlab.com/fi-sas/fisas-webpage/commit/e1151d0a7e314da635257729d1255a2e00c12a63))


### Features

* **social-support:** fixes to errors reported ([b73f9b5](https://gitlab.com/fi-sas/fisas-webpage/commit/b73f9b500446123982f5e726d3e869fe413795d2))
* **social-support:** fixes to errors reported ([09a87d7](https://gitlab.com/fi-sas/fisas-webpage/commit/09a87d7d08450de6fee3177baa2ac2a0d98b6626))
* **volunteering:** fixes to errors reported ([1112fc1](https://gitlab.com/fi-sas/fisas-webpage/commit/1112fc1751fb3574025534f7d9fb3bb7e98eb591))

## [1.60.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.4...v1.60.5) (2021-08-31)


### Bug Fixes

* **accommodation:** validation field enrollment ([9221064](https://gitlab.com/fi-sas/fisas-webpage/commit/92210640aea5cde76ef08125b16a9340e9c496d0))

## [1.60.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.3...v1.60.4) (2021-08-30)


### Bug Fixes

* **alimentation:** filter tickets ([ffc67a7](https://gitlab.com/fi-sas/fisas-webpage/commit/ffc67a73cdc442586472358490f4a410724e8173))

## [1.60.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.2...v1.60.3) (2021-08-26)


### Bug Fixes

* **alimentation:** disabled buttons ([58bbc08](https://gitlab.com/fi-sas/fisas-webpage/commit/58bbc080e6fc7ecb1247f826ab4713fa77aaaa26))

## [1.60.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.1...v1.60.2) (2021-08-25)


### Bug Fixes

* **alimentation:** refresh list when cancel ticket ([2978866](https://gitlab.com/fi-sas/fisas-webpage/commit/29788667fc57caf73d0075ad9de5ddb07a9b6f62))

## [1.60.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.60.0...v1.60.1) (2021-08-25)


### Bug Fixes

* **applications-mobility:** fix translation and clear form of suspension ([40973a6](https://gitlab.com/fi-sas/fisas-webpage/commit/40973a607970bfabad9bc66b24d980b69dadfa8d))
* **sub-23-mobility:** remove step 4 ([ee2db05](https://gitlab.com/fi-sas/fisas-webpage/commit/ee2db058e6683edec29b6f320dc66b54a498a9a3))
* **translations:** update translations ([4b0a5fc](https://gitlab.com/fi-sas/fisas-webpage/commit/4b0a5fc6aec9b3eed19123a4a9b2aa8ce8c0e8d1))

# [1.60.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.59.3...v1.60.0) (2021-08-24)


### Bug Fixes

* **application-bus-model:** update status ([676a16e](https://gitlab.com/fi-sas/fisas-webpage/commit/676a16e97c48a7b51326a6372361d49e7c301c80))
* **applications-bus:** update for new status ([8f446be](https://gitlab.com/fi-sas/fisas-webpage/commit/8f446bedf80df10b819b5fda986bc6b47bcfc641))
* **sub23-declarations:** fix status ([4c51b4c](https://gitlab.com/fi-sas/fisas-webpage/commit/4c51b4ce5b61163a8a389f13dec27391a3150606))
* **translations:** update ([0aab7d9](https://gitlab.com/fi-sas/fisas-webpage/commit/0aab7d91506f4303f3b2449a2a81bc65d85fe044))


### Features

* **applications:** add service ([c5acc12](https://gitlab.com/fi-sas/fisas-webpage/commit/c5acc1272717ef694ac1e21f617ca38035ecd4cf))

## [1.59.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.59.2...v1.59.3) (2021-08-23)


### Bug Fixes

* **geral:** active menu color change ([97518cb](https://gitlab.com/fi-sas/fisas-webpage/commit/97518cbd4c5cd7b63f4cb495f8a7361b922da514))

## [1.59.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.59.1...v1.59.2) (2021-08-23)


### Bug Fixes

* **auth:** get scopes after token refresh ([7c3fb2a](https://gitlab.com/fi-sas/fisas-webpage/commit/7c3fb2ab8b3f90a9670881a29345b8f0f5a15ca3))

## [1.59.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.59.0...v1.59.1) (2021-08-20)


### Bug Fixes

* **alimentation:** filter tickets and ordering ([b1d7c8a](https://gitlab.com/fi-sas/fisas-webpage/commit/b1d7c8ae2b472a8378f7ceb1752f4ee6f6cd3843))

# [1.59.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.58.0...v1.59.0) (2021-08-19)


### Features

* **accommodation:** add typology to application details ([712f934](https://gitlab.com/fi-sas/fisas-webpage/commit/712f934d8c1fba2aff3923d5fab20b4abdb03066))

# [1.58.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.5...v1.58.0) (2021-08-19)


### Features

* **accommodation:** add change application requests ([8e73694](https://gitlab.com/fi-sas/fisas-webpage/commit/8e73694fe492a9f1e5a1cd6b8efd4739f3a60226))
* **accommodation:** add pay button to application billings list ([ac21b24](https://gitlab.com/fi-sas/fisas-webpage/commit/ac21b2406f094bc7b4aeacce0335c5207e3742f3))
* **accommodation:** add tariff change modal ([3dbe4e0](https://gitlab.com/fi-sas/fisas-webpage/commit/3dbe4e0b905996c1655e07af96decaabf4352cf2))

## [1.57.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.4...v1.57.5) (2021-08-17)


### Bug Fixes

* **current_account:** text correction ([f0ab146](https://gitlab.com/fi-sas/fisas-webpage/commit/f0ab14620d8242fb50fba70a69654a56bf8fc4dd))

## [1.57.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.3...v1.57.4) (2021-08-13)


### Bug Fixes

* **refmb:** format amount ([1db1257](https://gitlab.com/fi-sas/fisas-webpage/commit/1db12572ff6beb5f2efbf088041e9c1a85783e6e))

## [1.57.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.2...v1.57.3) (2021-08-13)


### Bug Fixes

* **application-status-history:** add historyc validation ([11c84b9](https://gitlab.com/fi-sas/fisas-webpage/commit/11c84b90f66751f14f5f7470e2bf64365dd58f98))
* **volunteering:** controll tabs if not access ([ad4a6c7](https://gitlab.com/fi-sas/fisas-webpage/commit/ad4a6c743e05a789c13ee72da90e93d1555261fb))

## [1.57.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.1...v1.57.2) (2021-08-11)


### Bug Fixes

* **cc:** refMB amount pipe number remove ([090a5ed](https://gitlab.com/fi-sas/fisas-webpage/commit/090a5ed62ec5e778aa35b3aaffb1ed438f47bb66))

## [1.57.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.57.0...v1.57.1) (2021-08-11)


### Bug Fixes

* **cc:** history charges column confirmed_at ([87dca6e](https://gitlab.com/fi-sas/fisas-webpage/commit/87dca6eab52c2b512bbd5478c82825dce60a96bf))

# [1.57.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.56.1...v1.57.0) (2021-08-06)


### Features

* **private-accommodation-form-property:** add message validations ([b946751](https://gitlab.com/fi-sas/fisas-webpage/commit/b946751265c960d636798c1965b344fb95d98cd2))
* **private-accommodation-form-property-css:** add class ([4cb6850](https://gitlab.com/fi-sas/fisas-webpage/commit/4cb68505a74d65c08548f9c4840c2e13f734ec17))
* **private-accommodation-from-property:** validation descript and room ([1db971b](https://gitlab.com/fi-sas/fisas-webpage/commit/1db971b23e673138b3bc022894c292e898487d60))
* **translations:** add en and pt translation need one description ([fb5386e](https://gitlab.com/fi-sas/fisas-webpage/commit/fb5386e46ef6e9f74b51f641e489db75ccf9a1ec))

## [1.56.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.56.0...v1.56.1) (2021-08-05)


### Bug Fixes

* **alimentation:** minor fixs ([746d980](https://gitlab.com/fi-sas/fisas-webpage/commit/746d9808889736eb27350f3d857252f8a4600f3c))

# [1.56.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.55.4...v1.56.0) (2021-08-04)


### Features

* **accommodation:** add application billing tab ([12dd745](https://gitlab.com/fi-sas/fisas-webpage/commit/12dd745d9602b1c26beb3f9625e5c96eecc50f4f))

## [1.55.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.55.3...v1.55.4) (2021-08-03)


### Bug Fixes

* **personal-data:** remove fields of student on others profiles ([991a8cf](https://gitlab.com/fi-sas/fisas-webpage/commit/991a8cfab4a79ffe93261e0c55f40812817ba74f))

## [1.55.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.55.2...v1.55.3) (2021-08-02)


### Bug Fixes

* **cart:** add max quantity validation ([1216e43](https://gitlab.com/fi-sas/fisas-webpage/commit/1216e43780122300d8078d7607f5afbdced91f0c))

## [1.55.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.55.1...v1.55.2) (2021-08-02)


### Bug Fixes

* **personal_data:** change permission on personal data ([c31ae9f](https://gitlab.com/fi-sas/fisas-webpage/commit/c31ae9f2886109cb9dc511d4a291991951ff93fb))

## [1.55.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.55.0...v1.55.1) (2021-08-02)


### Bug Fixes

* **current-account:** get devices by withRealted on movements ([5bfefb0](https://gitlab.com/fi-sas/fisas-webpage/commit/5bfefb091532dda94e371a0e637ffc3b114fdea0))

# [1.55.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.54.1...v1.55.0) (2021-07-30)


### Features

* **social-support:** show status change history ([8ba79c7](https://gitlab.com/fi-sas/fisas-webpage/commit/8ba79c7126aa17d584223ec23fdcb165d5098394))
* **social-support:** show status change history ([6e7ce1f](https://gitlab.com/fi-sas/fisas-webpage/commit/6e7ce1fed8c71d1a4698c2ae4fb0a9245ace6ccc))
* **volunteering:** show status change history ([4d957ae](https://gitlab.com/fi-sas/fisas-webpage/commit/4d957ae0ffb1834ff6fb69ec4b46eaa6f7696afa))
* **volunteering:** show status change history ([9754d55](https://gitlab.com/fi-sas/fisas-webpage/commit/9754d55cb7d5dbd5ff539c6001c347f062b5d25c))
* **volunteering:** show status change history ([d1cf3ec](https://gitlab.com/fi-sas/fisas-webpage/commit/d1cf3ecfe2ae60b7bb3343b7134bbe07c744a19b))

## [1.54.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.54.0...v1.54.1) (2021-07-28)


### Bug Fixes

* **ubike:** correction of errors requisition date ([510037f](https://gitlab.com/fi-sas/fisas-webpage/commit/510037fd5ea0cc631e862ffc8f4b71e84b86e58f))

# [1.54.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.53.1...v1.54.0) (2021-07-28)


### Features

* **social-support:** schedule order and 24h format ([e289e5f](https://gitlab.com/fi-sas/fisas-webpage/commit/e289e5fb7cb996c017a1aa004bd6fe33ed7e147a))
* **social-support:** schedule order and 24h format ([70bf11b](https://gitlab.com/fi-sas/fisas-webpage/commit/70bf11b61dcd592d354fc0d3dade07c522ccb26f))
* **social-support:** schedule order and 24h format ([14f9723](https://gitlab.com/fi-sas/fisas-webpage/commit/14f972351654d32076eba37b66284078c5b75189))
* **volunteering:** schedule order and 24h format ([624956b](https://gitlab.com/fi-sas/fisas-webpage/commit/624956b3854145af715362aa3818491a3a70267b))
* **volunteering:** schedule order and 24h format ([6867221](https://gitlab.com/fi-sas/fisas-webpage/commit/686722126973e8cedc1e0b00a3fc82d96c4bdffe))
* **volunteering:** schedule order and 24h format ([15f86a2](https://gitlab.com/fi-sas/fisas-webpage/commit/15f86a2c717e79ea3c79649f4c6837dc2474d0c9))
* **volunteering:** schedule order and 24h format ([5dcdde6](https://gitlab.com/fi-sas/fisas-webpage/commit/5dcdde631f0e3300a6862995cad6ef84d5054d38))

## [1.53.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.53.0...v1.53.1) (2021-07-27)


### Bug Fixes

* **misc:** presentation of justification rejection ([1641957](https://gitlab.com/fi-sas/fisas-webpage/commit/16419573fc68a06c976c6ae7af462011d7f478ca))
* **misc:** presentation of justification rejection ([5d19cdc](https://gitlab.com/fi-sas/fisas-webpage/commit/5d19cdc5f99617bdf89c89b87352bd1ac9f1068d))
* **misc:** removed can create offer external user validation ([8bb001d](https://gitlab.com/fi-sas/fisas-webpage/commit/8bb001d4e70a7c268edcf4bf1e5c39974f70b447))
* **social-support:** added can generate report validation ([5686307](https://gitlab.com/fi-sas/fisas-webpage/commit/56863072b989c00f9ae883712751fd0d2b26525b))
* **volunteering:** added can generate report validation ([cd662e0](https://gitlab.com/fi-sas/fisas-webpage/commit/cd662e00c0e78e0a84475e314c51b16131bf3ebc))

# [1.53.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.52.1...v1.53.0) (2021-07-27)


### Features

* **social-support:** only students can apply ([c0b1084](https://gitlab.com/fi-sas/fisas-webpage/commit/c0b1084a81d0a817c85e9c8d910428948191918b))
* **social-support:** only students can apply ([ef85fd2](https://gitlab.com/fi-sas/fisas-webpage/commit/ef85fd2ecdeae8fc2fb1e1f77cd8e7c038fce48e))
* **social-support:** only students can apply ([3f55721](https://gitlab.com/fi-sas/fisas-webpage/commit/3f55721386195ece93c252507eccea6f80b288cf))
* **social-support:** show certain fields depending if student/employee ([7469f1a](https://gitlab.com/fi-sas/fisas-webpage/commit/7469f1a33ea5f5fe3a60b035b51f87275a6f0e8c))
* **social-support:** show certain fields only to students ([b14ff51](https://gitlab.com/fi-sas/fisas-webpage/commit/b14ff5196c8d6e4d5ed5e189d6503e34dc2bb34d))
* **social-support:** show status change history ([8e53192](https://gitlab.com/fi-sas/fisas-webpage/commit/8e53192f3d798a14b67d6f11da36ee3ccac7a2e1))
* **volunteering:** show certain fields only to students ([6fdd987](https://gitlab.com/fi-sas/fisas-webpage/commit/6fdd987fdb2f079b2b532e59d25cbe150a27210d))
* **volunteering:** show status change history ([233fc62](https://gitlab.com/fi-sas/fisas-webpage/commit/233fc62e92b97a1da4c1294f78d8a60fde157cdf))

## [1.52.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.52.0...v1.52.1) (2021-07-27)


### Bug Fixes

* **private_accommodation:** add field form owner ([7d747ac](https://gitlab.com/fi-sas/fisas-webpage/commit/7d747acebe97d5912eb179d57ad98b7ee6962070))

# [1.52.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.51.4...v1.52.0) (2021-07-26)


### Bug Fixes

* **application-status-history:** add status translations ([2965b72](https://gitlab.com/fi-sas/fisas-webpage/commit/2965b72ae8fa34f7349017f4d5a5517be331810f))


### Features

* **translations:** translations history applications social support ([f5d2cc2](https://gitlab.com/fi-sas/fisas-webpage/commit/f5d2cc26abb86d2907a106c274f13f8ed449988c))

## [1.51.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.51.3...v1.51.4) (2021-07-26)


### Bug Fixes

* **organic-unit-service:** adicionar filtro pelas activas ([4fd951f](https://gitlab.com/fi-sas/fisas-webpage/commit/4fd951f8eb3e3ae888396b95bb39bcfd8711a8e1))

## [1.51.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.51.2...v1.51.3) (2021-07-23)


### Bug Fixes

* **private_accommodation:** text change €/month ([bbf0c65](https://gitlab.com/fi-sas/fisas-webpage/commit/bbf0c65d1adf032be0449c29e9d39d6a9fa7890c))

## [1.51.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.51.1...v1.51.2) (2021-07-23)


### Bug Fixes

* **menu:** fixing the language on mobile ([1864dca](https://gitlab.com/fi-sas/fisas-webpage/commit/1864dca466575d31e5200d922bc6c9d1b4aa9515))

## [1.51.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.51.0...v1.51.1) (2021-07-23)


### Bug Fixes

* **private_accommodation:** removal step 5 form ([e8bb118](https://gitlab.com/fi-sas/fisas-webpage/commit/e8bb118bd116c09106d143a77d0ca00b332dd276))

# [1.51.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.5...v1.51.0) (2021-07-23)


### Bug Fixes

* **owner-properties:** add delete confirmation ([5655bba](https://gitlab.com/fi-sas/fisas-webpage/commit/5655bbab15d5fcaea0ae020b06870aaf838335a9))


### Features

* **traductions:** add title model remove property ([59831e9](https://gitlab.com/fi-sas/fisas-webpage/commit/59831e9e22d00c73c47d4b96f485124d0d9cb31b))

## [1.50.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.4...v1.50.5) (2021-07-22)


### Bug Fixes

* **social-support:** allow employee profile to access social support ([5f12f2e](https://gitlab.com/fi-sas/fisas-webpage/commit/5f12f2e2361fe95b7a9890bb0583c61144ddd207))

## [1.50.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.3...v1.50.4) (2021-07-22)


### Bug Fixes

* **geral:** side menu new options ([d200b65](https://gitlab.com/fi-sas/fisas-webpage/commit/d200b65f864655a0f449f3eba4894caa9983ce0f))

## [1.50.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.2...v1.50.3) (2021-07-22)


### Bug Fixes

* **notification:** deleting the last notification ([95cd6d1](https://gitlab.com/fi-sas/fisas-webpage/commit/95cd6d1ffd3d9c1d27d65a3d153e87cf02656cac))

## [1.50.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.1...v1.50.2) (2021-07-21)


### Bug Fixes

* **news:** video validation ([1b0cd31](https://gitlab.com/fi-sas/fisas-webpage/commit/1b0cd312107361aec8907def6618a1689ded9eed))

## [1.50.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.50.0...v1.50.1) (2021-07-21)


### Bug Fixes

* **app:** add resolver to public pages ([0217d75](https://gitlab.com/fi-sas/fisas-webpage/commit/0217d75fc9bff65b30c3b8bb751656ee144ff43c))

# [1.50.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.49.1...v1.50.0) (2021-07-21)


### Features

* **misc:** removed clear button from required date pickers ([a3bea73](https://gitlab.com/fi-sas/fisas-webpage/commit/a3bea73545ce5eba4f012a88e0dee5990c624a31))
* **volunteering:** general complain ([e82417a](https://gitlab.com/fi-sas/fisas-webpage/commit/e82417ac594d95d81e6210de96cbb7c2f8edce95))
* **volunteering:** general complain ([45a0148](https://gitlab.com/fi-sas/fisas-webpage/commit/45a0148e8ed7a44e4d0d6fe708732880bf5a82f9))
* **volunteering:** show reject justification reason ([7c4b3c8](https://gitlab.com/fi-sas/fisas-webpage/commit/7c4b3c89d617326498564ee3ec15e8c415834775))
* **volunteering:** show reject justification reason ([759e3ea](https://gitlab.com/fi-sas/fisas-webpage/commit/759e3eab6e179c5210661b980dae03b535ee9796))
* **volunteering:** show reject justification reason ([39ad002](https://gitlab.com/fi-sas/fisas-webpage/commit/39ad002047603297e06b7dca13b1041605f0345c))

## [1.49.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.49.0...v1.49.1) (2021-07-20)


### Bug Fixes

* **private_accommodation:** change color radio ([45ecee5](https://gitlab.com/fi-sas/fisas-webpage/commit/45ecee53f7a9678da3372d58160ed70a1089ea31))

# [1.49.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.48.1...v1.49.0) (2021-07-20)


### Features

* **social-support:** validate absence justification ([73ed2a7](https://gitlab.com/fi-sas/fisas-webpage/commit/73ed2a7316b52a74edded9fb4328256826e1c582))
* **social-support:** validate absence justification ([0358c39](https://gitlab.com/fi-sas/fisas-webpage/commit/0358c39ac5d7b3c1c1423d28f6b274ed4fa51d48))
* **social-support:** validate absence justification ([9a5a617](https://gitlab.com/fi-sas/fisas-webpage/commit/9a5a617a7f7a5da5f01f242a4c4d216075001376))

## [1.48.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.48.0...v1.48.1) (2021-07-20)


### Bug Fixes

* **current_account:** color change text ([b1cfbd4](https://gitlab.com/fi-sas/fisas-webpage/commit/b1cfbd4407df86b2b403c8dfca02043be4c56817))

# [1.48.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.11...v1.48.0) (2021-07-20)


### Features

* **social-support:** fixes from tests as student feedback ([debc1f7](https://gitlab.com/fi-sas/fisas-webpage/commit/debc1f71253b31a98054f503829a147fe27a65a4))
* **social-support:** fixes from tests as student feedback ([1a10071](https://gitlab.com/fi-sas/fisas-webpage/commit/1a100718c9b86526838ee2179dc10081dfef7c83))
* **volunteering:** external user create offer validation ([11284eb](https://gitlab.com/fi-sas/fisas-webpage/commit/11284eb5cd7fda2d21f6a0f0e456bd417f33336a))
* **volunteering:** external user create offer validation ([2585da7](https://gitlab.com/fi-sas/fisas-webpage/commit/2585da784a70f8760be490a97c3e137ea1d13d21))
* **volunteering:** uniform express interest behaviour ([13d6797](https://gitlab.com/fi-sas/fisas-webpage/commit/13d6797eb110bfd47822b4ddf6a158115d0b3a8f))

## [1.47.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.10...v1.47.11) (2021-07-20)


### Bug Fixes

* **social-support:** express interest workflow ([c858771](https://gitlab.com/fi-sas/fisas-webpage/commit/c858771512bb6e4337cafd893b8bbf1620202efa))
* **volunteering:** express interest workflow ([19d17c2](https://gitlab.com/fi-sas/fisas-webpage/commit/19d17c292b2d844f4340823ee947f319a0ed1a11))
* **volunteering:** express interest workflow ([ae81fcd](https://gitlab.com/fi-sas/fisas-webpage/commit/ae81fcd1ec439ce9144b36d1d677bfd8c52276d5))
* **volunteering:** express interest workflow ([bae1626](https://gitlab.com/fi-sas/fisas-webpage/commit/bae162672861eee1ca363b7c8e9b8796730bbaad))

## [1.47.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.9...v1.47.10) (2021-07-20)


### Bug Fixes

* **alimentation:** cancel message ticket canceled ([77c01e7](https://gitlab.com/fi-sas/fisas-webpage/commit/77c01e7cc7808bdf7c0a6eb10bafcf1aff0ff2f2))

## [1.47.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.8...v1.47.9) (2021-07-19)


### Bug Fixes

* **current_account:** correction of errors in text ([7837031](https://gitlab.com/fi-sas/fisas-webpage/commit/7837031249ab257535b31be2bf2f5eaed61a2786))

## [1.47.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.7...v1.47.8) (2021-07-19)


### Bug Fixes

* **dashboard:** forward to current account charge ([d40965e](https://gitlab.com/fi-sas/fisas-webpage/commit/d40965ec67a0dcc82ecf4f6f82aade4b0ad0fd43))

## [1.47.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.6...v1.47.7) (2021-07-19)


### Bug Fixes

* **volunteering:** problema criar candidatura ([131f127](https://gitlab.com/fi-sas/fisas-webpage/commit/131f127275cef4f1503f458088f58d7be6460738))

## [1.47.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.5...v1.47.6) (2021-07-16)


### Bug Fixes

* **dashboard:** standardization rules ([5a2f85f](https://gitlab.com/fi-sas/fisas-webpage/commit/5a2f85f0a4fdd956568f7129092bc0adac6c3ba8))

## [1.47.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.4...v1.47.5) (2021-07-16)


### Bug Fixes

* **queue:** fixed error in variable hourselected ([65403e9](https://gitlab.com/fi-sas/fisas-webpage/commit/65403e9c4b18dee0e01cedcfc3d7423057ae5da0))

## [1.47.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.3...v1.47.4) (2021-07-16)


### Bug Fixes

* **mobility:** standardization rules ([b75b29b](https://gitlab.com/fi-sas/fisas-webpage/commit/b75b29b205fe545c415825a20e9d2b863cf60060))

## [1.47.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.2...v1.47.3) (2021-07-15)


### Bug Fixes

* **volunteering:** standardization rules ([85f02c6](https://gitlab.com/fi-sas/fisas-webpage/commit/85f02c62aba5d4dc881e7674e4fc6fd51d06ac6e))

## [1.47.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.1...v1.47.2) (2021-07-15)


### Bug Fixes

* **volunteering:** added visual clue when error on language tab ([da54690](https://gitlab.com/fi-sas/fisas-webpage/commit/da54690002c13e845733c75e33a70b6c3147bb54))

## [1.47.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.47.0...v1.47.1) (2021-07-15)


### Bug Fixes

* **social-support:** standardization rules ([50f67f6](https://gitlab.com/fi-sas/fisas-webpage/commit/50f67f6225141d975cca55ce24707c8c7aa18ac7))

# [1.47.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.14...v1.47.0) (2021-07-14)


### Features

* **core:** change html lang from selected translation ([f640bcc](https://gitlab.com/fi-sas/fisas-webpage/commit/f640bccaa1499dbe8e8d2aee3445a602d6c5e85f))

## [1.46.14](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.13...v1.46.14) (2021-07-14)


### Bug Fixes

* **profile:** standardization rules ([dcc97c9](https://gitlab.com/fi-sas/fisas-webpage/commit/dcc97c9167ef0ddf7b5f5346caaec00b778ba095))

## [1.46.13](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.12...v1.46.13) (2021-07-13)


### Bug Fixes

* **queue:** standardization rules ([05a5bf8](https://gitlab.com/fi-sas/fisas-webpage/commit/05a5bf8ebc30047a20dbdc8305b8e5b4e7705867))

## [1.46.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.11...v1.46.12) (2021-07-13)


### Bug Fixes

* **shopping-cart:** standardization rules ([473041e](https://gitlab.com/fi-sas/fisas-webpage/commit/473041e9bd7c4de8ed9a110d46173418f2c476d6))

## [1.46.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.10...v1.46.11) (2021-07-13)


### Bug Fixes

* **ubike:** standardization rules ([96ed2e4](https://gitlab.com/fi-sas/fisas-webpage/commit/96ed2e421e4bc0d5b420881de1e8016cabc8d0fa))

## [1.46.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.9...v1.46.10) (2021-07-13)


### Bug Fixes

* **news:** standardization of rules ([3f3da02](https://gitlab.com/fi-sas/fisas-webpage/commit/3f3da027e1aed5c362e9d68698d2992ecd45e460))

## [1.46.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.8...v1.46.9) (2021-07-12)


### Bug Fixes

* **current_account:** standardization of rules ([2927af7](https://gitlab.com/fi-sas/fisas-webpage/commit/2927af792b6699cd061959ad50c53267642ee0db))

## [1.46.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.7...v1.46.8) (2021-07-12)


### Bug Fixes

* **cookie:** add expire date of 1 year to cookie acceptance ([e640dd1](https://gitlab.com/fi-sas/fisas-webpage/commit/e640dd12dfd7b18c65d52b30fa53992d9fd54cc9))

## [1.46.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.6...v1.46.7) (2021-07-12)


### Bug Fixes

* **alimentation:** standardization of rules ([49df93c](https://gitlab.com/fi-sas/fisas-webpage/commit/49df93cb881007ec3358dd5ba3e37b70bf9feb97))

## [1.46.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.5...v1.46.6) (2021-07-12)


### Bug Fixes

* **core:** fix logo of menu and logo mobile ([4d9ddcf](https://gitlab.com/fi-sas/fisas-webpage/commit/4d9ddcfe0b5144887bc45781ea36200d3b890420))

## [1.46.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.4...v1.46.5) (2021-07-12)


### Bug Fixes

* **private_accommodation:** sandardization rules ([5042ca6](https://gitlab.com/fi-sas/fisas-webpage/commit/5042ca694c134700529374c08a5010469f096eef))

## [1.46.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.3...v1.46.4) (2021-07-09)


### Bug Fixes

* **accommodation:** standardization of rules ([4a53e1c](https://gitlab.com/fi-sas/fisas-webpage/commit/4a53e1c9edd9ed7efe8a7ff8c20e0fd2378134bb))

## [1.46.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.2...v1.46.3) (2021-07-08)


### Bug Fixes

* **ubike:** change ubike apresentation text ([7668726](https://gitlab.com/fi-sas/fisas-webpage/commit/766872621f5911aff1e7a3ed85fdb3905694e77b))

## [1.46.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.1...v1.46.2) (2021-07-08)


### Bug Fixes

* **ubike:** limit of 5 occurrences ([9f9d115](https://gitlab.com/fi-sas/fisas-webpage/commit/9f9d115c7063bd816e2554204ae0019ceef06051))

## [1.46.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.46.0...v1.46.1) (2021-07-08)


### Bug Fixes

* **accommodation:** out of date message ([1eef602](https://gitlab.com/fi-sas/fisas-webpage/commit/1eef6021d6c9f4ee6e0244c7cf6241283fe05f56))

# [1.46.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.45.0...v1.46.0) (2021-07-07)


### Features

* **bus:** add absence requests to suspend application ([28d5a0a](https://gitlab.com/fi-sas/fisas-webpage/commit/28d5a0a63a496981aec79eed6ac77e678f090e26))

# [1.45.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.44.0...v1.45.0) (2021-07-07)


### Features

* **social-support:** fixes from tests feedback ([0e0aaa1](https://gitlab.com/fi-sas/fisas-webpage/commit/0e0aaa1c49921e14ff1bbfaa30b004647643cfb5))
* **social-support:** fixes from tests feedback ([409ad7a](https://gitlab.com/fi-sas/fisas-webpage/commit/409ad7ab23f9a1770a484feeb07de8e44af4f549))
* **social-support:** fixes from tests feedback ([7f75c2d](https://gitlab.com/fi-sas/fisas-webpage/commit/7f75c2d398f39da4bd2b23cd922d2e7041b9cf3b))

# [1.44.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.43.3...v1.44.0) (2021-07-06)


### Features

* **misc:** added scopes to add documentation ([c73e6ba](https://gitlab.com/fi-sas/fisas-webpage/commit/c73e6ba0829cc39689de3f1b527126ae980b223a))
* **social-support:** fiex withdraw from collaboration endpoint ([7de9aae](https://gitlab.com/fi-sas/fisas-webpage/commit/7de9aaedbf359094f37923ff86d5869b22d8d120))
* **volunteering:** fixed withdraw from collaboration endpoint ([142e357](https://gitlab.com/fi-sas/fisas-webpage/commit/142e3574e2dd587217bb23c1f3908a09721dbf40))

## [1.43.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.43.2...v1.43.3) (2021-07-05)


### Bug Fixes

* **geral:** change footer image ([07a205e](https://gitlab.com/fi-sas/fisas-webpage/commit/07a205e4e0fe3f6423ca4c9d09375e61dd5293e8))

## [1.43.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.43.1...v1.43.2) (2021-07-05)


### Bug Fixes

* **accomodation:** nationality translations ([899ad7a](https://gitlab.com/fi-sas/fisas-webpage/commit/899ad7a752f9a916705a8616782e8aa55f478e8e))

## [1.43.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.43.0...v1.43.1) (2021-07-05)


### Bug Fixes

* **geral:** nationalities data ([c026fcb](https://gitlab.com/fi-sas/fisas-webpage/commit/c026fcbc7a1acef71e74dbadbe6866ac208fe6ff))

# [1.43.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.42.0...v1.43.0) (2021-07-01)


### Features

* change theme IPViseu ([021ccb1](https://gitlab.com/fi-sas/fisas-webpage/commit/021ccb14af9038813716a5e0542e7b3bbd144ed2))

# [1.42.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.41.0...v1.42.0) (2021-07-01)


### Bug Fixes

* **volunteering:** fix access collaboration certificate ([d3f8816](https://gitlab.com/fi-sas/fisas-webpage/commit/d3f881640345a9181b6e91c0a0fa17aa378c2db8))
* **volunteering:** volunteering action form ([d83920e](https://gitlab.com/fi-sas/fisas-webpage/commit/d83920eb105f1cbaa39efcc4329bca8237d68a6d))


### Features

* **misc:** fix access collaboration certificate ([a34590c](https://gitlab.com/fi-sas/fisas-webpage/commit/a34590ca0a5aa6b6954045ced3b8c02db9c41077))
* **misc:** fix access collaboration certificate ([1006e6e](https://gitlab.com/fi-sas/fisas-webpage/commit/1006e6e5960b0db75e933951a740d6221d88e107))
* **misc:** fix access collaboration certificate ([87481ea](https://gitlab.com/fi-sas/fisas-webpage/commit/87481eafb42531f4c2b8aa6ad0739c46808594a9))
* **misc:** fix access collaboration certificate ([042b3ef](https://gitlab.com/fi-sas/fisas-webpage/commit/042b3ef8c8b42419e1c987fe4624896cc05efda3))
* **misc:** fix access collaboration certificate ([693d5d0](https://gitlab.com/fi-sas/fisas-webpage/commit/693d5d014ff29f0e1d085c1229d4472bd9061aed))
* **misc:** fix access collaboration certificate ([9f6b350](https://gitlab.com/fi-sas/fisas-webpage/commit/9f6b350f826237e8917cd3309314cf0287db8ae2))

# [1.41.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.40.0...v1.41.0) (2021-07-01)


### Bug Fixes

* **bus:** change sub23 applications to sub23 declarations ([44d25f8](https://gitlab.com/fi-sas/fisas-webpage/commit/44d25f85dc8fb03562da9118dbc88e8f7a343e05))


### Features

* **bus:** add sub 23 applications form and list ([43497b4](https://gitlab.com/fi-sas/fisas-webpage/commit/43497b45a51a7d722e9b79a83c8f8a12659fc447))

# [1.40.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.39.1...v1.40.0) (2021-06-30)


### Bug Fixes

* **geral:** font size counter ([0550ebb](https://gitlab.com/fi-sas/fisas-webpage/commit/0550ebb5167bddc30467afb70d9cf192e81520e7))
* **offer-form:** update form, remove users ([38fa537](https://gitlab.com/fi-sas/fisas-webpage/commit/38fa53795449d053359e976ddf520eac5c9aa1d6))


### Features

* **traductions:** add new en, pt ([da48805](https://gitlab.com/fi-sas/fisas-webpage/commit/da4880587fcba162336231c1374d9c3250dd72c0))

## [1.39.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.39.0...v1.39.1) (2021-06-30)


### Bug Fixes

* **accommodation:** add auto fill of nationality ([d1faf8c](https://gitlab.com/fi-sas/fisas-webpage/commit/d1faf8c86ce37f4846bc5098bfee75f00466786b))
* **privateaccommodation:** correction errors files ([f2a1a45](https://gitlab.com/fi-sas/fisas-webpage/commit/f2a1a45bc285a2a8e75d1877acd421b8f9725086))

# [1.39.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.7...v1.39.0) (2021-06-29)


### Features

* update webpage logos ([8186e8a](https://gitlab.com/fi-sas/fisas-webpage/commit/8186e8ae8e41bcf24d0a02f68371049c654ed843))

## [1.38.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.6...v1.38.7) (2021-06-29)


### Bug Fixes

* **ubike:** filters states history and list ([a10aa12](https://gitlab.com/fi-sas/fisas-webpage/commit/a10aa122d164d915ccba3d4fd1badb7e8b095dee))

## [1.38.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.5...v1.38.6) (2021-06-29)


### Bug Fixes

* **reports, notifications:** reports & noti delete ([edbcafc](https://gitlab.com/fi-sas/fisas-webpage/commit/edbcafce7e4149b7e0ad259b211e351b7e781f26))

## [1.38.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.4...v1.38.5) (2021-06-25)


### Bug Fixes

* **general:** prevent error thrown by null object access ([386b212](https://gitlab.com/fi-sas/fisas-webpage/commit/386b212a4b779032ebcddb3fa430e85825c4c49a))

## [1.38.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.3...v1.38.4) (2021-06-25)


### Bug Fixes

* **style:** change the font family for body ([032cd4f](https://gitlab.com/fi-sas/fisas-webpage/commit/032cd4fa31e4c0a44ed3e7d5a30555f18899cc3e))

## [1.38.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.2...v1.38.3) (2021-06-24)


### Bug Fixes

* **bus:** add new application states ([eca1602](https://gitlab.com/fi-sas/fisas-webpage/commit/eca16025abd39739900a36f6ab9011af08111b20))

## [1.38.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.1...v1.38.2) (2021-06-24)


### Bug Fixes

* **geral:** remove console.log ([793cebf](https://gitlab.com/fi-sas/fisas-webpage/commit/793cebf9225f9b291c3f89082d2ff2cb82138a72))

## [1.38.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.38.0...v1.38.1) (2021-06-23)


### Bug Fixes

* **ubike:** filter date ([d1e5a2e](https://gitlab.com/fi-sas/fisas-webpage/commit/d1e5a2ed5bdd16d3ba7b12aad05176ee9948d91f))

# [1.38.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.37.3...v1.38.0) (2021-06-23)


### Features

* **style:** fixed ant divider style broken on chrome ([5bb4e1a](https://gitlab.com/fi-sas/fisas-webpage/commit/5bb4e1aebbda525b62d65e43ff5de9f23b9ef8a6))
* **style:** fixed ant divider style broken on chrome ([3e92da4](https://gitlab.com/fi-sas/fisas-webpage/commit/3e92da4238b4252a4637fa414a1f79ef70ba5d96))
* **volunteering:** manage attendaces general fixes ([fb23fb8](https://gitlab.com/fi-sas/fisas-webpage/commit/fb23fb8fdc67bc66474d2555456ee34947051a58))
* **volunteering:** manage attendaces general fixes ([7ad26a2](https://gitlab.com/fi-sas/fisas-webpage/commit/7ad26a21cccc186ef57e8218bd0a24689d70532a))
* **volunteering:** manage attendaces general fixes ([05b763d](https://gitlab.com/fi-sas/fisas-webpage/commit/05b763d0e72915accb2021e6e4ef1bd82ef27190))
* **volunteering:** manage attendaces general fixes ([be73f2e](https://gitlab.com/fi-sas/fisas-webpage/commit/be73f2e67499961ea982aaf01f3a7cb680c078b6))
* **volunteering:** manage attendaces general fixes ([c76664a](https://gitlab.com/fi-sas/fisas-webpage/commit/c76664a7d0aea3a592f1537a2a8263c27a4a541c))

## [1.37.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.37.2...v1.37.3) (2021-06-23)


### Bug Fixes

* **geral:** elimination of bold force ([63d0cac](https://gitlab.com/fi-sas/fisas-webpage/commit/63d0cac329cfa13d8e1742b3adc50d761a22f5c4))

## [1.37.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.37.1...v1.37.2) (2021-06-23)


### Bug Fixes

* **bus:** fix error on withdrawal ([80b3f8f](https://gitlab.com/fi-sas/fisas-webpage/commit/80b3f8fad3f2ae945b2b1e8fdceb53f515f7933a))

## [1.37.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.37.0...v1.37.1) (2021-06-23)


### Bug Fixes

* **geral:** correction errors detect in the review ([1f97313](https://gitlab.com/fi-sas/fisas-webpage/commit/1f97313c0c26b7c22efe3d8da886080e0e65f2d8))

# [1.37.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.36.1...v1.37.0) (2021-06-23)


### Features

* **volunteering:** manage volunteering action attendances ([cc729cb](https://gitlab.com/fi-sas/fisas-webpage/commit/cc729cb4f853e418f5e80f594343ec9d2c85c09d))
* **volunteering:** manage volunteering action attendances ([b811dcc](https://gitlab.com/fi-sas/fisas-webpage/commit/b811dcceb7f2079059fdb90abe3c0d24fa3c4203))
* **volunteering:** manage volunteering action attendances ([9806f51](https://gitlab.com/fi-sas/fisas-webpage/commit/9806f517d00285df338b1daafa6771e0db73794c))
* **volunteering:** manage volunteering action attendances ([cfd9d5b](https://gitlab.com/fi-sas/fisas-webpage/commit/cfd9d5b7e8aaa9411c77898ec6b57eece90d251c))
* **volunteering:** manage volunteering action attendances ([e03c537](https://gitlab.com/fi-sas/fisas-webpage/commit/e03c53764f20fefd25a2e09b083fe8dbd65110e0))
* **volunteering:** manage volunteering action attendances ([089bf27](https://gitlab.com/fi-sas/fisas-webpage/commit/089bf27a1e6f47463f640e6cd094ce19013647ca))
* **volunteering:** manage volunteering action attendances ([405762a](https://gitlab.com/fi-sas/fisas-webpage/commit/405762a3c55ccb18a39f84f061e55035012b3f36))
* **volunteering:** manage volunteering action attendances ([8dd473b](https://gitlab.com/fi-sas/fisas-webpage/commit/8dd473b218694028216984123cf314ac2629471b))
* **volunteering:** manage volunteering action attendances ([03ea01d](https://gitlab.com/fi-sas/fisas-webpage/commit/03ea01de9b24769fe69c4da92d36137e6de3789c))

## [1.36.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.36.0...v1.36.1) (2021-06-22)


### Bug Fixes

* **news:** getHtml ([975449d](https://gitlab.com/fi-sas/fisas-webpage/commit/975449d377d9947391ed766800d9a0de196cfedc))

# [1.36.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.35.3...v1.36.0) (2021-06-22)


### Features

* **general:** added nationality options to forms ([b00179f](https://gitlab.com/fi-sas/fisas-webpage/commit/b00179f95c316af13b0c01754340925ba146308e))

## [1.35.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.35.2...v1.35.3) (2021-06-22)


### Bug Fixes

* **accommodation:** change nationalities ([d7c2a4e](https://gitlab.com/fi-sas/fisas-webpage/commit/d7c2a4e048d739499b27e4100ae1cb8bae636629))
* **ubike:** change ms ([861bb77](https://gitlab.com/fi-sas/fisas-webpage/commit/861bb779ab13b551d0792452f3ea27329024c304))

## [1.35.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.35.1...v1.35.2) (2021-06-22)


### Bug Fixes

* **bus:** automatic fill user data on application form ([75d6e24](https://gitlab.com/fi-sas/fisas-webpage/commit/75d6e2450dc06554820dbb814dcd2c023ff9f481))
* **general:** fix check to show express interest logic ([2df1216](https://gitlab.com/fi-sas/fisas-webpage/commit/2df12166d0d0704bbf999ef87c3c6e3ed1d27807))

## [1.35.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.35.0...v1.35.1) (2021-06-21)


### Bug Fixes

* **widget:** change scope ([11b9b56](https://gitlab.com/fi-sas/fisas-webpage/commit/11b9b56db01f92bb3839df9ad9ce197834b5ef1c))

# [1.35.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.34.0...v1.35.0) (2021-06-21)


### Bug Fixes

* **form-owner:** remove pin and password ([35416c6](https://gitlab.com/fi-sas/fisas-webpage/commit/35416c63a399042ca1d6bddf9c19f8657c20dec0))
* **owner-step-five:** remove pin and password ([40c9b20](https://gitlab.com/fi-sas/fisas-webpage/commit/40c9b20d4d5c3701f939787a4962281b4157f458))


### Features

* **bus:** add form information to mobility application ([0c47873](https://gitlab.com/fi-sas/fisas-webpage/commit/0c47873c747c585451eca1425e9c0e50fd390074))
* **translations:** add description owner form ([c287e0c](https://gitlab.com/fi-sas/fisas-webpage/commit/c287e0ce1fd450305e3c94ffdfd8cf745684e394))
* **volunteering:** added pagination and fixed search ([92f24fd](https://gitlab.com/fi-sas/fisas-webpage/commit/92f24fd77c5de18993f1fdaaac590ab200ae128d))
* **volunteering:** added pagination and fixed search ([fb39fb2](https://gitlab.com/fi-sas/fisas-webpage/commit/fb39fb24a6eb34a5ea97575f37baefa551e8741e))
* **volunteering:** added pagination and fixed search ([c84a7c7](https://gitlab.com/fi-sas/fisas-webpage/commit/c84a7c75cb7ab24524f2c1c9b4374ef233efc37b))
* **volunteering:** added pagination and fixed search ([dee552c](https://gitlab.com/fi-sas/fisas-webpage/commit/dee552cb6ce45c6247d8436799402abf75c85520))
* **volunteering:** added pagination and fixed search ([514525d](https://gitlab.com/fi-sas/fisas-webpage/commit/514525df716cc05ca1109d7bb8be558d2f0c2a96))
* **volunteering:** added pagination and fixed search ([310609c](https://gitlab.com/fi-sas/fisas-webpage/commit/310609c35f08923605569f949239d821f30689db))

# [1.34.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.33.1...v1.34.0) (2021-06-21)


### Bug Fixes

* **shared:** remove logs from upload file component ([a17d7e4](https://gitlab.com/fi-sas/fisas-webpage/commit/a17d7e46a60bed934db44c6e2d5139d6a7b30b3b))


### Features

* **users:** add nationality field to personal data ([4a2fb04](https://gitlab.com/fi-sas/fisas-webpage/commit/4a2fb046be6c670cf0a7ceea0aec94df797b9afd))

## [1.33.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.33.0...v1.33.1) (2021-06-21)


### Bug Fixes

* **ubike:** correction of detected errors ([3126cee](https://gitlab.com/fi-sas/fisas-webpage/commit/3126cee0cc2fbea4f47fdda6c55ce58f0649d04e))

# [1.33.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.21...v1.33.0) (2021-06-21)


### Features

* **volunteering:** added manage volunteering action candidates ([2e0288a](https://gitlab.com/fi-sas/fisas-webpage/commit/2e0288a34c926e08420a011990aa8873abea5bc6))
* **volunteering:** added manage volunteering action candidates ([cfcdf46](https://gitlab.com/fi-sas/fisas-webpage/commit/cfcdf4653fdf2282665ad9ff9ac8e36491515186))
* **volunteering:** added manage volunteering action candidates ([7ad03e5](https://gitlab.com/fi-sas/fisas-webpage/commit/7ad03e5c10c83567c22ff993a1916b563416e81b))
* **volunteering:** added manage volunteering action candidates ([9821af6](https://gitlab.com/fi-sas/fisas-webpage/commit/9821af623f35a7e7cb56efcf3abf317c23376212))
* **volunteering:** added manage volunteering action candidates ([dcd29d0](https://gitlab.com/fi-sas/fisas-webpage/commit/dcd29d06b4743b86254f779cbc7aaadff0422659))
* **volunteering:** added manage volunteering action candidates ([5dd347d](https://gitlab.com/fi-sas/fisas-webpage/commit/5dd347d2701b7e2d3bcfc682d155c5092f4e896e))
* **volunteering:** added manage volunteering action candidates ([4aff516](https://gitlab.com/fi-sas/fisas-webpage/commit/4aff516bb3450ebdbfc598bedb35c09aca3f8cdb))

## [1.32.21](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.20...v1.32.21) (2021-06-18)


### Bug Fixes

* **general:** changed fixed languages to dynamic ([d2be4c0](https://gitlab.com/fi-sas/fisas-webpage/commit/d2be4c0aeb91f47834e1fd4c45ae64359192b614))
* **general:** changed fixed languages to dynamic ([efce477](https://gitlab.com/fi-sas/fisas-webpage/commit/efce4772c570f335a5ada4e689a35686ac61c1e1))

## [1.32.20](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.19...v1.32.20) (2021-06-18)


### Bug Fixes

* **shared:** add logs to file-upload component ([f6c4699](https://gitlab.com/fi-sas/fisas-webpage/commit/f6c4699b20d62dc95affde484cd7848b0330d20f))

## [1.32.19](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.18...v1.32.19) (2021-06-18)


### Bug Fixes

* **shared:** add logs to file-upload component ([55217f4](https://gitlab.com/fi-sas/fisas-webpage/commit/55217f4a17cdf0075f908b94236614d67821011c))

## [1.32.18](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.17...v1.32.18) (2021-06-18)


### Bug Fixes

* **general:** send academic_year as it is sent from WS ([1315c1d](https://gitlab.com/fi-sas/fisas-webpage/commit/1315c1d864238dc5c9bc8f016d3b15df60f05672))

## [1.32.17](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.16...v1.32.17) (2021-06-17)


### Bug Fixes

* **app:** firstLogin fix on SSO ([6e3e269](https://gitlab.com/fi-sas/fisas-webpage/commit/6e3e269947fada10d710f5f756f8f888683b21c9))

## [1.32.16](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.15...v1.32.16) (2021-06-17)


### Bug Fixes

* **ubike:** correction of detected errors ([5d5c452](https://gitlab.com/fi-sas/fisas-webpage/commit/5d5c4525ac9dc6e3d6867ed5d2be62fb684b9185))

## [1.32.15](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.14...v1.32.15) (2021-06-17)


### Bug Fixes

* **app:** close if bracket ([516ee6a](https://gitlab.com/fi-sas/fisas-webpage/commit/516ee6a2d9fec5394bf6167611de4aaa0d06dfa0))
* test firstLogin SSO and build configs ([d5f9331](https://gitlab.com/fi-sas/fisas-webpage/commit/d5f93318b94063e44aa20c0de817645e87d7a26b))

## [1.32.14](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.13...v1.32.14) (2021-06-17)


### Bug Fixes

* **app:** add logs test build configs ([3daba28](https://gitlab.com/fi-sas/fisas-webpage/commit/3daba2818a8a687e4bb7774cd127669cecb60873))

## [1.32.13](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.12...v1.32.13) (2021-06-17)


### Bug Fixes

* **pt:** change misspelled text ([90cd53f](https://gitlab.com/fi-sas/fisas-webpage/commit/90cd53f40ea55826dfcbc4765ac2741b5f24b8af))

## [1.32.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.11...v1.32.12) (2021-06-17)


### Bug Fixes

* **app:** test firstLogin if vars ([5a63a93](https://gitlab.com/fi-sas/fisas-webpage/commit/5a63a93ac4fc2bd11c17da69c54c317dc6f56082))

## [1.32.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.10...v1.32.11) (2021-06-17)


### Bug Fixes

* **app:** change firstLogin SSO Logs ([cdb2662](https://gitlab.com/fi-sas/fisas-webpage/commit/cdb266250a8c32b7f60b561306e2e1615f9418ac))

## [1.32.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.9...v1.32.10) (2021-06-17)


### Bug Fixes

* **app:** add logs to firstLogin SSO ([b86f531](https://gitlab.com/fi-sas/fisas-webpage/commit/b86f531d917bf9d0e6891ebd71e2d9b3f906ca14))

## [1.32.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.8...v1.32.9) (2021-06-16)


### Bug Fixes

* **social-support:** don't allow to register future attendance dates ([d71c317](https://gitlab.com/fi-sas/fisas-webpage/commit/d71c3174123d39525a9dcc1c0648d62026b2c384))
* **volunteering:** volunteering application form schedule ([d3cdb71](https://gitlab.com/fi-sas/fisas-webpage/commit/d3cdb71af8fe62bb48d1e4594bc9f486a90c4790))
* **volunteering:** volunteering application form schedule ([19d7281](https://gitlab.com/fi-sas/fisas-webpage/commit/19d7281ebaac67d5f4fa3dfd711133bf14f9ab67))
* **volunteering:** volunteering application form schedule ([24ceb38](https://gitlab.com/fi-sas/fisas-webpage/commit/24ceb38c980c453c9a16b4a7ded1076357688464))
* **volunteering:** volunteering application form schedule ([94e3e03](https://gitlab.com/fi-sas/fisas-webpage/commit/94e3e03a4affa3ae5548760f3611800d32254143))

## [1.32.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.7...v1.32.8) (2021-06-15)


### Bug Fixes

* **accommodation:** delete info when not visible ([df1b991](https://gitlab.com/fi-sas/fisas-webpage/commit/df1b991a0a3ecc148d60d41ab09660961def14a4))

## [1.32.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.6...v1.32.7) (2021-06-15)


### Bug Fixes

* **accommodation:** date of accommodation period ([3167b18](https://gitlab.com/fi-sas/fisas-webpage/commit/3167b180e4dee55e0164a014356055560b080836))

## [1.32.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.5...v1.32.6) (2021-06-15)


### Bug Fixes

* **accommodation:** autocomplete and tab form ([b0fa472](https://gitlab.com/fi-sas/fisas-webpage/commit/b0fa4725fd25a6bbbab96593e1c5b2ecb16eddd4))

## [1.32.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.4...v1.32.5) (2021-06-15)


### Bug Fixes

* **news:** width img ([79362c1](https://gitlab.com/fi-sas/fisas-webpage/commit/79362c11edd79102b27e67e150fb9eb0235110b3))

## [1.32.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.3...v1.32.4) (2021-06-14)


### Bug Fixes

* **accommodation:** application ([a49058c](https://gitlab.com/fi-sas/fisas-webpage/commit/a49058c95d53ffdd935b9eb87c55ad480daf3076))
* **ubike:** width button ([b5879f7](https://gitlab.com/fi-sas/fisas-webpage/commit/b5879f78854a717d758f06412dfb2d8985d6d493))

## [1.32.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.2...v1.32.3) (2021-06-11)


### Bug Fixes

* **accommodation:** remove withRelated ([fb4dcd2](https://gitlab.com/fi-sas/fisas-webpage/commit/fb4dcd27c358d1a2aae29750f761617a1e0ec1e5))

## [1.32.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.1...v1.32.2) (2021-06-11)


### Bug Fixes

* **accommodation:** more changes requested ([a098dca](https://gitlab.com/fi-sas/fisas-webpage/commit/a098dca71ad4a42d1df2f159f81965e30d23e5de))

## [1.32.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.32.0...v1.32.1) (2021-06-11)


### Bug Fixes

* **ubike:** correction form errors ([4529464](https://gitlab.com/fi-sas/fisas-webpage/commit/45294646fb44af45ab7e241b79e5156fe3b37003))

# [1.32.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.31.0...v1.32.0) (2021-06-09)


### Bug Fixes

* **accommodation:** change the info on the form ([198f46f](https://gitlab.com/fi-sas/fisas-webpage/commit/198f46f531ef14a56915d81079b76b0ca8c7c3f7))


### Features

* **volunteering:** improved desing of volunteering actions card ([fa15d2c](https://gitlab.com/fi-sas/fisas-webpage/commit/fa15d2c7925fca68a28a38d6732855bb911174a8))

# [1.31.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.30.1...v1.31.0) (2021-06-08)


### Features

* **volunteering:** added experience detail page ([defe4f1](https://gitlab.com/fi-sas/fisas-webpage/commit/defe4f180c22b02d3e92e0df5e134c843b8d3dce))
* **volunteering:** added experience detail page ([bb655f3](https://gitlab.com/fi-sas/fisas-webpage/commit/bb655f3d2b60679dd087c9d58a4ff06c3d605092))
* **volunteering:** added experience detail page ([797083f](https://gitlab.com/fi-sas/fisas-webpage/commit/797083f924e772ab86c3ab68faec41ca59701332))
* **volunteering:** added experience detail page ([13e93f7](https://gitlab.com/fi-sas/fisas-webpage/commit/13e93f7d6752060e716e15ed7eb58da0b8baa5f6))
* **volunteering:** added experience detail page ([a88a45b](https://gitlab.com/fi-sas/fisas-webpage/commit/a88a45bc951078724f78ee96afe712ad98eb3e37))

## [1.30.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.30.0...v1.30.1) (2021-06-08)


### Bug Fixes

* **geral:** erros correction ([d8ec227](https://gitlab.com/fi-sas/fisas-webpage/commit/d8ec2275a3bba7a1b5fb310e6816a6ae871adbe8))

# [1.30.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.5...v1.30.0) (2021-06-08)


### Bug Fixes

* **social-support:** application form invalid numeric fields to string ([fa4fad9](https://gitlab.com/fi-sas/fisas-webpage/commit/fa4fad97c8a07d230341fd6233b4b63f5f3cd49a))


### Features

* **volunteering:** added interest history page ([dad02d8](https://gitlab.com/fi-sas/fisas-webpage/commit/dad02d8e70f26d081b3e8dca9882a53cf2544ee3))
* **volunteering:** added interest history page ([1931282](https://gitlab.com/fi-sas/fisas-webpage/commit/19312822b8934c746048211e59822310bcd2d41d))
* **volunteering:** added interest history page ([716ee63](https://gitlab.com/fi-sas/fisas-webpage/commit/716ee635824c8110ebfc53e5ea8d4693bf9c73a3))

## [1.29.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.4...v1.29.5) (2021-06-02)


### Bug Fixes

* **volutering:** fix entry text ([bf09d60](https://gitlab.com/fi-sas/fisas-webpage/commit/bf09d60eb5aa69021dc422ce7bb665ed60b2d1fb))
* minor texts fix ([62901ee](https://gitlab.com/fi-sas/fisas-webpage/commit/62901eef6321abd9279d2517224a4f67a4a076a2))

## [1.29.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.3...v1.29.4) (2021-06-02)


### Bug Fixes

* **alimentation,accommodation:** fixes ([0f11621](https://gitlab.com/fi-sas/fisas-webpage/commit/0f116213ea03b251e64ef50cea56b76ce8f3180c))

## [1.29.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.2...v1.29.3) (2021-06-01)


### Bug Fixes

* **accommodation:** fixes ([f17f84f](https://gitlab.com/fi-sas/fisas-webpage/commit/f17f84f5565e21b5cd5aaf310305af17aad2cf2b))
* **shared:** added custom input for schedule choice ([4311127](https://gitlab.com/fi-sas/fisas-webpage/commit/4311127c436ec8d4b2284adb5343b28bd55edeb5))

## [1.29.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.1...v1.29.2) (2021-06-01)


### Bug Fixes

* **geral:** spelling correction ([2f7ffc9](https://gitlab.com/fi-sas/fisas-webpage/commit/2f7ffc948167c6d431c1dd23b66b66d10c6c5c0f))
* **geral:** spelling correction ([079a601](https://gitlab.com/fi-sas/fisas-webpage/commit/079a60198e772e7d13b1b4aea8d270ee1447a439))

## [1.29.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.29.0...v1.29.1) (2021-06-01)


### Bug Fixes

* **authentication:** fix authentication texts ([aadcb77](https://gitlab.com/fi-sas/fisas-webpage/commit/aadcb77b28cb7fc0220c3e9c355c326614dcbf28))

# [1.29.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.28.4...v1.29.0) (2021-05-31)


### Bug Fixes

* **accommodation:** add course validation ([72a09b0](https://gitlab.com/fi-sas/fisas-webpage/commit/72a09b07c1d69732953b8cba1313efabfb66c561))
* **social-support:** only allow to add one certificate ([91447c3](https://gitlab.com/fi-sas/fisas-webpage/commit/91447c3e30a15a7da373aea3f4943fd1d4c897c3))
* **ubike:** permission ([c3fd786](https://gitlab.com/fi-sas/fisas-webpage/commit/c3fd7862740cd4ac94defc6c7fbea05b9dafd3d9))
* **volunteering:** added create volunteering action page ([aa382dd](https://gitlab.com/fi-sas/fisas-webpage/commit/aa382ddbc1055ac26fb45ca3a2594a718a2580e1))
* **volunteering:** added create volunteering action page ([b4723f1](https://gitlab.com/fi-sas/fisas-webpage/commit/b4723f117e48eccae8242390bffefc49b7ee3e07))
* **volunteering:** added create/edit permissions ([b542f8e](https://gitlab.com/fi-sas/fisas-webpage/commit/b542f8e6e6c98aec2cfa319e37d170dbfed422eb))
* **volunteering:** added edit volunteering action page ([088e6f9](https://gitlab.com/fi-sas/fisas-webpage/commit/088e6f98b5800bd1885263735c914c9487f3d9e0))
* **volunteering:** added edit volunteering action page ([6c7a433](https://gitlab.com/fi-sas/fisas-webpage/commit/6c7a4337b30c63f8ca35baddab65cafc2323babd))
* **volunteering:** added edit volunteering action page ([d3d8674](https://gitlab.com/fi-sas/fisas-webpage/commit/d3d8674d1f099aaa3a9010b1cea3c2291a9790e7))


### Features

* **authentication:** change authentication description text ([1b0845b](https://gitlab.com/fi-sas/fisas-webpage/commit/1b0845b81ad6e9a1e21ccbc43df9cb3924d0a5de))

## [1.28.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.28.3...v1.28.4) (2021-05-31)


### Bug Fixes

* **accommodation:** unlock gender when u ([c07329b](https://gitlab.com/fi-sas/fisas-webpage/commit/c07329b72d8dd03d25ac91d9ead5d881753e0460))
* **mobility:** button color change ([f6d4ba2](https://gitlab.com/fi-sas/fisas-webpage/commit/f6d4ba2636db63b0cad81632fa77420c5ca3c60c))

## [1.28.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.28.2...v1.28.3) (2021-05-31)


### Bug Fixes

* **accommodation:** course correction ([7a863ea](https://gitlab.com/fi-sas/fisas-webpage/commit/7a863ead48e60da1e17856f0ce217c6e9a8e8182))
* **accommodation:** new field typology ([aff5144](https://gitlab.com/fi-sas/fisas-webpage/commit/aff51440f38e23aca998b7e4b4f284aa7e32f004))

## [1.28.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.28.1...v1.28.2) (2021-05-28)


### Bug Fixes

* **profile:** fix change-pin on profile ([e125aa4](https://gitlab.com/fi-sas/fisas-webpage/commit/e125aa4b9538a3ddee731c9ba91035730d7c6ef7))

## [1.28.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.28.0...v1.28.1) (2021-05-28)


### Bug Fixes

* **webpage:** change IPViseu logo ([fe9935f](https://gitlab.com/fi-sas/fisas-webpage/commit/fe9935fdc842d3a2afa4f090d869ba311ee097a0))

# [1.28.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.27.2...v1.28.0) (2021-05-28)


### Bug Fixes

* **accommodation:** school filter ([30cd6d0](https://gitlab.com/fi-sas/fisas-webpage/commit/30cd6d06c8ba01384995f9d01fd66a98a0be9bd7))
* **general:** revert proxy-config target ([9aa313d](https://gitlab.com/fi-sas/fisas-webpage/commit/9aa313df927551e41fb0ea68d8cafe19c64b7bb9))
* **ubike:** remove duplicated import ([5c4eb92](https://gitlab.com/fi-sas/fisas-webpage/commit/5c4eb92c5e6511ddf2aaafbb5ff13a43daaa5101))


### Features

* **u_bike:** add webpage permissions ([255b665](https://gitlab.com/fi-sas/fisas-webpage/commit/255b66545ad1b1ab0badd8f4e7b7aaab51b47838))

## [1.27.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.27.1...v1.27.2) (2021-05-27)


### Bug Fixes

* **shared:** change extension of logo files ([f7f0fa3](https://gitlab.com/fi-sas/fisas-webpage/commit/f7f0fa3c95914457596f6f0a6d9bf3727bc41689))

## [1.27.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.27.0...v1.27.1) (2021-05-27)


### Bug Fixes

* **alimentation:** school name ([bb51a79](https://gitlab.com/fi-sas/fisas-webpage/commit/bb51a79d81abe1d0bc761a091ef7e14df84fbc1b))

# [1.27.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.26.3...v1.27.0) (2021-05-26)


### Features

* **social-support:** check if attendance exceeds max monthly hours ([c49d3cb](https://gitlab.com/fi-sas/fisas-webpage/commit/c49d3cb1ba3820e3d0272cb7c2317c46c8378afb))
* **social-support:** check if attendance exceeds max monthly hours ([5dfb2ed](https://gitlab.com/fi-sas/fisas-webpage/commit/5dfb2ede0bbb84ae22d011cd6ed570f57822f916))
* **social-support:** display generated certificate ([bb0bc1d](https://gitlab.com/fi-sas/fisas-webpage/commit/bb0bc1d18a23a820b51c9666bc29abf8416c056d))
* **social-support:** display generated certificate ([87d3748](https://gitlab.com/fi-sas/fisas-webpage/commit/87d3748c304c72e338ca2ab3dba3a9ee3382a473))
* **social-support:** display generated certificate ([dfea0be](https://gitlab.com/fi-sas/fisas-webpage/commit/dfea0be5a862386836d96906354a14965f3ee39b))
* **volunteering:** refactor my applications page ([18f99f5](https://gitlab.com/fi-sas/fisas-webpage/commit/18f99f523864e988ec573f37d97adb74b66bc2de))
* **volunteering:** refactor my applications page ([f9caeee](https://gitlab.com/fi-sas/fisas-webpage/commit/f9caeeeff14131bf35b8a5ed8a9adadbafd2c9e7))
* **volunteering:** refactor my applications page ([a28f13f](https://gitlab.com/fi-sas/fisas-webpage/commit/a28f13f2d5ec05fa2b35c00ddc1ad7d2c42e53a3))
* **volunteering:** refactor my applications page ([e1f92df](https://gitlab.com/fi-sas/fisas-webpage/commit/e1f92df471b32bd654eef66296ae364c69ab95cb))

## [1.26.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.26.2...v1.26.3) (2021-05-26)


### Bug Fixes

* **geral:** change text ([eb95636](https://gitlab.com/fi-sas/fisas-webpage/commit/eb956362dce7865e966b7d3ed6c9af59841140d3))
* **widgets:** text correction ([6a405e7](https://gitlab.com/fi-sas/fisas-webpage/commit/6a405e7449c925d056ff4b47d3b5421f59a2c486))

## [1.26.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.26.1...v1.26.2) (2021-05-25)


### Bug Fixes

* **geral:** text change ([09a8656](https://gitlab.com/fi-sas/fisas-webpage/commit/09a865628df285823fe7b822bee335026b41185c))

## [1.26.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.26.0...v1.26.1) (2021-05-25)


### Bug Fixes

* **geral:** change text ([90d32a0](https://gitlab.com/fi-sas/fisas-webpage/commit/90d32a05c221b70b1a07304e334490e8de7a8452))

# [1.26.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.25.3...v1.26.0) (2021-05-25)


### Features

* **volunteering:** refactor application form ([79b92ca](https://gitlab.com/fi-sas/fisas-webpage/commit/79b92cabeeb7fb7b77886b4072ef938915f5012d))
* **volunteering:** refactor application form ([1ab39f0](https://gitlab.com/fi-sas/fisas-webpage/commit/1ab39f03cd8447d9737a9f4c51382cea6102887f))
* **volunteering:** refactor application form ([0560251](https://gitlab.com/fi-sas/fisas-webpage/commit/05602515d61f37c2943b3c09db58b9f764dbe0d6))
* **volunteering:** refactor application form ([07c8a5e](https://gitlab.com/fi-sas/fisas-webpage/commit/07c8a5e069d5d6fba7883a5ae518ef06058c51a7))
* **volunteering:** refactor application form ([c44448c](https://gitlab.com/fi-sas/fisas-webpage/commit/c44448c097c86325aa412a8bdb0c6249e187aeb3))
* **volunteering:** refactor application form ([e691b18](https://gitlab.com/fi-sas/fisas-webpage/commit/e691b18fc717c3539c5f0d334c553d9b1997073a))
* **volunteering:** refactor application form ([65a7ca3](https://gitlab.com/fi-sas/fisas-webpage/commit/65a7ca3a0a0223faf9b249522ffaf0c04c223f06))
* **volunteering:** refactor application form ([10b1637](https://gitlab.com/fi-sas/fisas-webpage/commit/10b163728c42e3c321c8015cf0bcd0b4cbf09005))

## [1.25.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.25.2...v1.25.3) (2021-05-25)


### Bug Fixes

* **acommodation:** change permission ([1cdc805](https://gitlab.com/fi-sas/fisas-webpage/commit/1cdc805027a6fffd8401aee68f758b1be9652801))

## [1.25.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.25.1...v1.25.2) (2021-05-24)


### Bug Fixes

* **accommodation:** length price ([b0c6d4f](https://gitlab.com/fi-sas/fisas-webpage/commit/b0c6d4f903115ec5f02d2348c03ad845d1fc96d3))

## [1.25.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.25.0...v1.25.1) (2021-05-24)


### Bug Fixes

* **accommodation:** correction of reported errors ([3d4a736](https://gitlab.com/fi-sas/fisas-webpage/commit/3d4a736720fd0dcea6b8c936dd54286b26c7d2c0))

# [1.25.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.24.1...v1.25.0) (2021-05-24)


### Features

* **social-support:** fix attendances list ([b89614f](https://gitlab.com/fi-sas/fisas-webpage/commit/b89614f3ea02a2aca5d97afca4a71cb32e798c33))
* **social-support:** fix attendances list ([f48830d](https://gitlab.com/fi-sas/fisas-webpage/commit/f48830dd181d20c7f2030c057b0b4d0dfc1ba659))
* **social-support:** fix attendances list ([1c44176](https://gitlab.com/fi-sas/fisas-webpage/commit/1c44176436171be09cc5d87e06e983aaa4c8e8cc))
* **social-support:** fix attendances list ([df422a1](https://gitlab.com/fi-sas/fisas-webpage/commit/df422a1f4c07e6646332f7c7545d6f799fe0b2a7))
* **social-support:** fix attendances list ([d30a2c1](https://gitlab.com/fi-sas/fisas-webpage/commit/d30a2c128a2cf58d9fa248d44083cce1df38114b))

## [1.24.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.24.0...v1.24.1) (2021-05-21)


### Bug Fixes

* **accommodation:** field course_degree_id ([17d53bc](https://gitlab.com/fi-sas/fisas-webpage/commit/17d53bca9c499e71d42ba0a9aee2ebae930d84d4))

# [1.24.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.23.1...v1.24.0) (2021-05-21)


### Bug Fixes

* **geral:** changing service names for settings ([5bb9016](https://gitlab.com/fi-sas/fisas-webpage/commit/5bb901675ad54a9ee05efca263002651266d3ffb))


### Features

* **personal-page:** add document_type field ([4e394c7](https://gitlab.com/fi-sas/fisas-webpage/commit/4e394c7d28212c4acdb86eda5228441491a90142))

## [1.23.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.23.0...v1.23.1) (2021-05-21)


### Bug Fixes

* **geral:** error correction notifications reports ([eb35da2](https://gitlab.com/fi-sas/fisas-webpage/commit/eb35da201c6eb0b045d2a2e3d76fc7b4b4161380))

# [1.23.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.7...v1.23.0) (2021-05-20)


### Features

* **social-scholarship:** add missing permissions ([5b8b8c2](https://gitlab.com/fi-sas/fisas-webpage/commit/5b8b8c212a0f92001d6fffe51a541824b8039fe6))
* **social-scholarship:** add missing scopes ([9ba851f](https://gitlab.com/fi-sas/fisas-webpage/commit/9ba851fdbc318fac6290d69fdbb2775e71cbde56))

## [1.22.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.6...v1.22.7) (2021-05-19)


### Bug Fixes

* **accommodation:** gender ([03267b5](https://gitlab.com/fi-sas/fisas-webpage/commit/03267b54fccbd3206349db148af948ec63c3e695))

## [1.22.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.5...v1.22.6) (2021-05-18)


### Bug Fixes

* **shopping-cart:** shopping cart correction ([b24267a](https://gitlab.com/fi-sas/fisas-webpage/commit/b24267adf1830a459dbe7e224fe6a00abbb5d786))

## [1.22.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.4...v1.22.5) (2021-05-18)


### Bug Fixes

* **alimentation:** error correction ([b6b8443](https://gitlab.com/fi-sas/fisas-webpage/commit/b6b8443da7b8d788fc85495a5c57b432fed0a2ba))

## [1.22.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.3...v1.22.4) (2021-05-17)


### Bug Fixes

* **accommodation:** show only visible_for_users residences ([47185d0](https://gitlab.com/fi-sas/fisas-webpage/commit/47185d034becba2342db75bafc0e3e6578a376dc))

## [1.22.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.2...v1.22.3) (2021-05-14)


### Bug Fixes

* **geral:** notifications, reports, buildings ([7a7543e](https://gitlab.com/fi-sas/fisas-webpage/commit/7a7543e3b06e9879ef6e943b93d003310e3def46))

## [1.22.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.1...v1.22.2) (2021-05-13)


### Bug Fixes

* **accommodation:** correction of form errors ([e81761a](https://gitlab.com/fi-sas/fisas-webpage/commit/e81761aac846b32eaaa5e90c7c730f7ca7f44c17))
* **ipleiria:** change logos file names ([cb8cd68](https://gitlab.com/fi-sas/fisas-webpage/commit/cb8cd681203c370552124ca683366ecd5a7829a1))

## [1.22.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.22.0...v1.22.1) (2021-05-12)


### Bug Fixes

* **alimentation:** document error correction ([8580506](https://gitlab.com/fi-sas/fisas-webpage/commit/85805064208cd7bf3051fdaed67ecc92aba29bb6))

# [1.22.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.21.0...v1.22.0) (2021-05-12)


### Features

* **social-support:** validate attendances ([eb4afbf](https://gitlab.com/fi-sas/fisas-webpage/commit/eb4afbfd5a068491e4f0eaaf5c96ffd23b313c9b))
* **social-support:** validate attendances ([72e6575](https://gitlab.com/fi-sas/fisas-webpage/commit/72e65758afc2c1ed47dfab22976dbef66838ee94))
* **social-support:** validate attendances ([90e46c2](https://gitlab.com/fi-sas/fisas-webpage/commit/90e46c2e5a95da6b1aa6a67af1f892ef583a36c2))

# [1.21.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.5...v1.21.0) (2021-05-07)


### Bug Fixes

* **accommodation:** change default values ([9b0c52d](https://gitlab.com/fi-sas/fisas-webpage/commit/9b0c52ddc643b6324e46de78208ec6188e77e565))
* **accommodation:** fix get accommodation period ([8cded64](https://gitlab.com/fi-sas/fisas-webpage/commit/8cded64c80b54fdd4852df369b30c48b3d403c93))


### Features

* **core:** add new SASocial Logo ([00dbafb](https://gitlab.com/fi-sas/fisas-webpage/commit/00dbafb6f9bdc4939a8b8ff5db29f9c0c66816f1))

## [1.20.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.4...v1.20.5) (2021-05-06)


### Bug Fixes

* **dashboard:** widgets ([7ec4ba6](https://gitlab.com/fi-sas/fisas-webpage/commit/7ec4ba6c62b512d15597738e652fb9595b2f0bac))

## [1.20.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.3...v1.20.4) (2021-05-05)


### Bug Fixes

* **mobility:** canceled status fixed ([666161f](https://gitlab.com/fi-sas/fisas-webpage/commit/666161f09cc380bd7e4b6840f01830dabff325f6))
* **mobility:** fixes corrected ([44f089d](https://gitlab.com/fi-sas/fisas-webpage/commit/44f089d0eb669f76cbff31a7153353170a03f53e))

## [1.20.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.2...v1.20.3) (2021-05-05)


### Bug Fixes

* **dashboard:** widgets ([0e628ba](https://gitlab.com/fi-sas/fisas-webpage/commit/0e628ba66b4ed9249320a00dc449b1533b72dd8f))

## [1.20.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.1...v1.20.2) (2021-05-04)


### Bug Fixes

* **accommodation:** info form ([b8cd22d](https://gitlab.com/fi-sas/fisas-webpage/commit/b8cd22d010948d11cfa7d6b7f41775ee7a3ca28e))

## [1.20.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.20.0...v1.20.1) (2021-05-03)


### Bug Fixes

* **accommodation:** final part of accommodation ([f0c719d](https://gitlab.com/fi-sas/fisas-webpage/commit/f0c719dcb129693e1ba77e6ae425823ef16c3c75))

# [1.20.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.19.0...v1.20.0) (2021-05-03)


### Bug Fixes

* **list-monthly-reports-advisor:** add  module ([b4fb746](https://gitlab.com/fi-sas/fisas-webpage/commit/b4fb746beb75120931a17fe92f06ea4b9817a9ba))
* **management-attendances:** add service ([8fd3bf3](https://gitlab.com/fi-sas/fisas-webpage/commit/8fd3bf30f39ee1b9c791065ad8054869b5f14d4c))
* **offer-status:** change status offer ([1f1cb6c](https://gitlab.com/fi-sas/fisas-webpage/commit/1f1cb6cd96a1472721e3c583e9e7c49443ccae79))
* **offer-status-history:** update modules ([863f733](https://gitlab.com/fi-sas/fisas-webpage/commit/863f73354592fef534fbbb20f9b3dc1f4932a19d))
* **offers-card:** add compoments ([1edd404](https://gitlab.com/fi-sas/fisas-webpage/commit/1edd404d37aba8c165c9e7bf1e81342775e4e6ea))
* **service:** update service ([3da5065](https://gitlab.com/fi-sas/fisas-webpage/commit/3da50655422677ea3b2e84a9040ce5a3add9edaf))
* **social-support:** update routing ([12b3411](https://gitlab.com/fi-sas/fisas-webpage/commit/12b341160d6e22882827197bf4b5ccbbd8c47061))


### Features

* **list-attendances-advisor:** add module ([ed4266f](https://gitlab.com/fi-sas/fisas-webpage/commit/ed4266fc93172b9a383ee14967ba0c2f6d80fdb7))
* **services:** add services ([f1583de](https://gitlab.com/fi-sas/fisas-webpage/commit/f1583defa61e6d964dce31fcfff33ddda5cb4393))
* **social-support:** reorganized components to use in multiple modules ([4652498](https://gitlab.com/fi-sas/fisas-webpage/commit/4652498ad3b2138b95d5cac81358b2ff6f4fec2c))
* **social-support:** reorganized components to use in multiple modules ([fa593c7](https://gitlab.com/fi-sas/fisas-webpage/commit/fa593c757a4dd1362601370b3dd4bcf60beabfaf))

# [1.19.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.18.1...v1.19.0) (2021-05-03)


### Features

* **social-support:** auto fill current academic year ([f469d40](https://gitlab.com/fi-sas/fisas-webpage/commit/f469d40990cce49b8d064ed29ab4102bc6f96b8f))
* **social-support:** auto fill current academic year ([fbec6d2](https://gitlab.com/fi-sas/fisas-webpage/commit/fbec6d22021d7debb41309237b223de77ac362a6))
* **social-support:** current academic year endpoint ([52516da](https://gitlab.com/fi-sas/fisas-webpage/commit/52516da1a18021ecde8f316d8b138910008513f4))

## [1.18.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.18.0...v1.18.1) (2021-04-29)


### Bug Fixes

* **accommodation:** permissions ([1bb1198](https://gitlab.com/fi-sas/fisas-webpage/commit/1bb11981ba8f54c62f6b2e9f2ae6669d5f89d0a0))

# [1.18.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.17.0...v1.18.0) (2021-04-29)


### Bug Fixes

* **disable-account:** return to login page on disabled account ([c0f9ac5](https://gitlab.com/fi-sas/fisas-webpage/commit/c0f9ac5a28bbadf92d5c2589b6a3202c43ad7828))


### Features

* **social-support:** fix to grid not being aligned ([5038d26](https://gitlab.com/fi-sas/fisas-webpage/commit/5038d267648a396efc340efad03d76217f1f8258))
* **social-support:** fix to grid not being aligned ([b434265](https://gitlab.com/fi-sas/fisas-webpage/commit/b43426563c24a27bf0b096d7b11bff463def8c0c))
* **social-support:** fixed some bugs on offers list ([65bbb20](https://gitlab.com/fi-sas/fisas-webpage/commit/65bbb20708425611f4786feb130ffa870add5a25))
* **social-support:** fixed some bugs on offers list ([5c6c1e0](https://gitlab.com/fi-sas/fisas-webpage/commit/5c6c1e02db277bcc137a88306271c5884a9259bb))
* **social-support:** removed GDPR protected fields ([fdffda3](https://gitlab.com/fi-sas/fisas-webpage/commit/fdffda3b56c07d31e2d5e83ca34d65562bdd6886))

# [1.17.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.16.0...v1.17.0) (2021-04-28)


### Features

* **social-support:** added attendances validated hours ([3869f43](https://gitlab.com/fi-sas/fisas-webpage/commit/3869f4323bd5d985620936d747c8834f253abac0))

# [1.16.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.15.3...v1.16.0) (2021-04-28)


### Features

* **social-support:** fix to user interest state change event ([3bf4583](https://gitlab.com/fi-sas/fisas-webpage/commit/3bf4583a091757d583bec97eaa560a2fa67100ad))
* **social-support:** manage selected candidates for each offer ([ad299ce](https://gitlab.com/fi-sas/fisas-webpage/commit/ad299ce34e7502822a793b0ffe5f681673feb6c2))
* **social-support:** perform actions on selected candidates ([7aed4d4](https://gitlab.com/fi-sas/fisas-webpage/commit/7aed4d451982fb3f821a71c761af0ba88386e3bc))
* **social-support:** selected candidates actions ([abcbc36](https://gitlab.com/fi-sas/fisas-webpage/commit/abcbc365978c9e4537b27b3a754a1cda703e0b26))
* **social-support:** selected candidates actions ([b308b91](https://gitlab.com/fi-sas/fisas-webpage/commit/b308b91b98e4a9723e2cf5b53a01f645165467dc))
* **social-support:** selected candidates actions ([b979079](https://gitlab.com/fi-sas/fisas-webpage/commit/b9790790cc502274f3bb1d98c355e4998493a5b0))

## [1.15.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.15.2...v1.15.3) (2021-04-28)


### Bug Fixes

* **accommodation:** correction of errors ([95a58d3](https://gitlab.com/fi-sas/fisas-webpage/commit/95a58d37beeb30479000aa7ce7f47f755d3504ff))

## [1.15.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.15.1...v1.15.2) (2021-04-28)


### Bug Fixes

* **accommodation:** final accommodation changes ([312a609](https://gitlab.com/fi-sas/fisas-webpage/commit/312a609021d210b4fa5195229e0e71eb52c88ba3))

## [1.15.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.15.0...v1.15.1) (2021-04-27)


### Bug Fixes

* **bus:** fix application form ([c909246](https://gitlab.com/fi-sas/fisas-webpage/commit/c9092465f760d31514c1c449a3793ca6a97119d8))

# [1.15.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.14.1...v1.15.0) (2021-04-27)


### Bug Fixes

* **social-support:** added page to edit/create offers ([e362292](https://gitlab.com/fi-sas/fisas-webpage/commit/e362292d87e200106b38f98d7c83646ab4ff5caa))
* **social-support:** added page to edit/create offers ([9f69047](https://gitlab.com/fi-sas/fisas-webpage/commit/9f69047eadd3141a1b1d73710dc31b4427350bdb))
* **social-support:** added page to edit/create offers ([8ac966f](https://gitlab.com/fi-sas/fisas-webpage/commit/8ac966f1caa7751b4a56589c4782e2df919af20e))


### Features

* **social-support:** added create/edit form ([0a2ec6e](https://gitlab.com/fi-sas/fisas-webpage/commit/0a2ec6ed71df8cd2457756189eaee57607fda206))
* **social-support:** added customizable hours to schedule ([8a3fbe9](https://gitlab.com/fi-sas/fisas-webpage/commit/8a3fbe9929c5107ca063cd231cb46c8808c967c9))
* **social-support:** added fieldsets ([3b5ce77](https://gitlab.com/fi-sas/fisas-webpage/commit/3b5ce77a46f9dfa5ccc26c12aac127fe35248829))
* **social-support:** added form validation and errors ([c83e744](https://gitlab.com/fi-sas/fisas-webpage/commit/c83e744d9ac40168ddad0d3c329f89dc79854b4c))
* **social-support:** added models ([c247180](https://gitlab.com/fi-sas/fisas-webpage/commit/c2471807f4d4c690aba4f1fbcec21e0227547170))
* **social-support:** added more fields to form ([14e94e9](https://gitlab.com/fi-sas/fisas-webpage/commit/14e94e9a88ed49de9e583c7671352d2a30db1a25))
* **social-support:** added translatable fields ([965ffda](https://gitlab.com/fi-sas/fisas-webpage/commit/965ffda18d323dc799e3050f9c399b8d19912450))
* **social-support:** added translatable fields ([0e56841](https://gitlab.com/fi-sas/fisas-webpage/commit/0e568416bbd51665b0f90c786e2351bc93f4b285))
* **social-support:** added user select input ([26589d5](https://gitlab.com/fi-sas/fisas-webpage/commit/26589d550cc261cce3ba775f471ee1074baaf2bf))
* **social-support:** added user select input ([a267b05](https://gitlab.com/fi-sas/fisas-webpage/commit/a267b0501aa3064e3c7f542d5842397d5a214356))
* **social-support:** fix to edit offer not working ([2f1db3f](https://gitlab.com/fi-sas/fisas-webpage/commit/2f1db3f71961c383ccddd7979ba20c695ed94634))
* **social-support:** fix to edit offer not working ([1a34780](https://gitlab.com/fi-sas/fisas-webpage/commit/1a34780e92eed80f3a9df00f6a37f196a4591946))
* **social-support:** fix to edit offer not working ([8866377](https://gitlab.com/fi-sas/fisas-webpage/commit/88663778da8f492395a4f597fe4cc7af882659e7))
* **social-support:** fix to page permissions ([3281fc3](https://gitlab.com/fi-sas/fisas-webpage/commit/3281fc31cb6486c5e38fa4fa4068668ac8ce8be6))
* **social-support:** fix to page permissions ([50d9f04](https://gitlab.com/fi-sas/fisas-webpage/commit/50d9f046667b3bc8a72419bc51bbacec1d05b227))
* **social-support:** offers form ([04e50db](https://gitlab.com/fi-sas/fisas-webpage/commit/04e50dbda7065cfbb42ea74864b4104e24ec1be3))

## [1.14.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.14.0...v1.14.1) (2021-04-26)


### Bug Fixes

* **accommodation:** remove maxmedia ([6c3a0a7](https://gitlab.com/fi-sas/fisas-webpage/commit/6c3a0a7f92ebe677a21c2fc28ca9c36950c122b0))

# [1.14.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.12...v1.14.0) (2021-04-26)


### Bug Fixes

* **profile:** add password match verification ([84cd1b9](https://gitlab.com/fi-sas/fisas-webpage/commit/84cd1b9ef51150c0ff8724e514adfdd8b1c74ec6))


### Features

* **users:** add new pages for verification of password and recovery ([5467f5f](https://gitlab.com/fi-sas/fisas-webpage/commit/5467f5fa57ffdd369c046e06531071e717812298))

## [1.13.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.11...v1.13.12) (2021-04-26)


### Bug Fixes

* **private_accommodation:** fix configiration endpoints ([70767e5](https://gitlab.com/fi-sas/fisas-webpage/commit/70767e51228bef5f9c35ae3094fd6f8b84e21187))

## [1.13.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.10...v1.13.11) (2021-04-23)


### Bug Fixes

* **accommodation:** review and more fields applica ([cffd95a](https://gitlab.com/fi-sas/fisas-webpage/commit/cffd95a2fbf7950cb80198af0a92fd3bb55961d3))

## [1.13.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.9...v1.13.10) (2021-04-23)


### Bug Fixes

* **ubike:** all pages ajusted and fixed ([f61f1c6](https://gitlab.com/fi-sas/fisas-webpage/commit/f61f1c640779bd102ad9fef4be6ded081029021b))
* **ubike:** changes done ([2bca434](https://gitlab.com/fi-sas/fisas-webpage/commit/2bca434c154293be81dc71768705961c9300cbd7))
* **ubike:** styles fixed ([6f58fe1](https://gitlab.com/fi-sas/fisas-webpage/commit/6f58fe1e42d1603c72deb5ecf00a1b01a953480a))

## [1.13.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.8...v1.13.9) (2021-04-21)


### Bug Fixes

* **accommodation:** step 5 application ([ff01e97](https://gitlab.com/fi-sas/fisas-webpage/commit/ff01e978ee692fe590d06176f949cd2c657c9383))

## [1.13.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.7...v1.13.8) (2021-04-21)


### Bug Fixes

* **list_reservations:** fix small bug ([08791ac](https://gitlab.com/fi-sas/fisas-webpage/commit/08791ac8ccd7c3183efdb17dc1fb0bec2b76f698))

## [1.13.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.6...v1.13.7) (2021-04-20)


### Bug Fixes

* **accommodation:** validations step 3 ([f819c32](https://gitlab.com/fi-sas/fisas-webpage/commit/f819c32cd22e04bf7a3d9c22f88770a4b9201ef1))

## [1.13.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.5...v1.13.6) (2021-04-19)


### Bug Fixes

* **accommodation:** step 1 and 2 application ([178ac66](https://gitlab.com/fi-sas/fisas-webpage/commit/178ac668985ecc228c7fdb35b0530d16c76ffe86))

## [1.13.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.4...v1.13.5) (2021-04-16)


### Bug Fixes

* **accommodation:** application regime and extras ([b1d86c1](https://gitlab.com/fi-sas/fisas-webpage/commit/b1d86c12033d59073f7385bc6216fc53c7bf42ff))

## [1.13.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.3...v1.13.4) (2021-04-16)


### Bug Fixes

* **accommodation:** add new fields to application ([2032e9b](https://gitlab.com/fi-sas/fisas-webpage/commit/2032e9b93590a0d1df949ee57d50ff99a059ae7f))
* **accommodation:** change admission_date to admission_year ([3d78943](https://gitlab.com/fi-sas/fisas-webpage/commit/3d789432eb0b5ea8b15225407a4a740157ef6e54))
* **accommodation:** change admission_year to admission_date ([1d677b7](https://gitlab.com/fi-sas/fisas-webpage/commit/1d677b7d9b47856b0c61c93fa824bb3d8c6b8a14))

## [1.13.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.2...v1.13.3) (2021-04-14)


### Bug Fixes

* **social_support:** change id ([dbe8734](https://gitlab.com/fi-sas/fisas-webpage/commit/dbe8734043209514ec6bf0f58b8c6ec21a588008))

## [1.13.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.1...v1.13.2) (2021-04-14)


### Bug Fixes

* **accommodation:** change kinship only to household elements-1 ([a9e3c46](https://gitlab.com/fi-sas/fisas-webpage/commit/a9e3c46115e3a23dab6a4bca590049d7680a24fa))

## [1.13.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.13.0...v1.13.1) (2021-04-14)


### Bug Fixes

* **social_support:** candidate selection list ([51f5cac](https://gitlab.com/fi-sas/fisas-webpage/commit/51f5cacd9a0fcbe10c58488ba691e3fd84f5338a))

# [1.13.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.12.0...v1.13.0) (2021-04-14)


### Features

* **social-support:** fix end time not having validation ([5849c48](https://gitlab.com/fi-sas/fisas-webpage/commit/5849c48a25c6a4983aaf61d6ebc14adbb5f5eb06))
* **social-support:** fix end time not having validation ([1e7a161](https://gitlab.com/fi-sas/fisas-webpage/commit/1e7a1614fbdec30757cdc35f5382e50619e933c7))

# [1.12.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.11.0...v1.12.0) (2021-04-14)


### Features

* **social-support:** added contract file button component ([4b0b8ea](https://gitlab.com/fi-sas/fisas-webpage/commit/4b0b8ea466c0d464b3b06e2fec4fba2d617e6a13))
* **social-support:** fix to contract file info being static ([a955c23](https://gitlab.com/fi-sas/fisas-webpage/commit/a955c234d472ecf734688f451d763e033fcd6f45))

# [1.11.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.9...v1.11.0) (2021-04-13)


### Features

* **accommodation:** add organic_unit(school) to residence application ([2ecc467](https://gitlab.com/fi-sas/fisas-webpage/commit/2ecc467eaac17eb5dacff606e97e795744cc5d74))

## [1.10.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.8...v1.10.9) (2021-04-12)


### Bug Fixes

* **social_support:** page offer ([ee0c1d4](https://gitlab.com/fi-sas/fisas-webpage/commit/ee0c1d4f1be4a0c9b8b10e106f68bf8e3a92e68c))
* **social_support:** state history ([439ed5f](https://gitlab.com/fi-sas/fisas-webpage/commit/439ed5fc7fcd2f57f8e67c43198939d1552329b9))

## [1.10.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.7...v1.10.8) (2021-04-12)


### Bug Fixes

* **accommodation:** fixed method being called with invalid parameter ([9064cd2](https://gitlab.com/fi-sas/fisas-webpage/commit/9064cd23f62e759683acaeafa3bc6bee75c74fec))
* **accommodation:** fixed method being called with invalid parameter ([132a22f](https://gitlab.com/fi-sas/fisas-webpage/commit/132a22faf1537cfca6eb3ae5d8755e2c20cc7e30))
* **attendance:** addapt pagination to respect theme color ([8f73d7b](https://gitlab.com/fi-sas/fisas-webpage/commit/8f73d7be9e10e68bb50898733a223bfdbb8995d6))
* **attendance:** addapt pagination to respect theme color ([7196c3f](https://gitlab.com/fi-sas/fisas-webpage/commit/7196c3f7b0e11308687cd9008028962e0f0057c3))
* **geral:** color correction css ([9482b91](https://gitlab.com/fi-sas/fisas-webpage/commit/9482b913873067898fe33b5988359a3f01fd82c1))
* **social-support:** added back button to application review page ([b991499](https://gitlab.com/fi-sas/fisas-webpage/commit/b991499395995d0027d924a4383e34b484413e87))
* **social-support:** changed attendance page tabs ([b21a231](https://gitlab.com/fi-sas/fisas-webpage/commit/b21a2313df4eb0db8011e356beb37cbe8f57d922))
* **social-support:** changed attendance page tabs ([00b1d4e](https://gitlab.com/fi-sas/fisas-webpage/commit/00b1d4e6bed8c2ceeb95b12d67e80e910d98182c))
* **social-support:** disable buttons when closed/cancelled ([ede66be](https://gitlab.com/fi-sas/fisas-webpage/commit/ede66be9ba075fd7e21be0ff1f4333766eab154f))
* **social-support:** disable buttons when closed/cancelled ([8167d10](https://gitlab.com/fi-sas/fisas-webpage/commit/8167d10e51d56a858dcb6ebb552d7d3972242b80))
* **social-support:** general style/functional fixes to record page ([0c21597](https://gitlab.com/fi-sas/fisas-webpage/commit/0c21597742fad38e3488a2f57e626269920d86d6))
* **social-support:** general style/functional fixes to record page ([3593cc3](https://gitlab.com/fi-sas/fisas-webpage/commit/3593cc31c4e63e0da5c411a422d2d7a2ad4992a8))
* **social-support:** removed fixme comment ([44b3a82](https://gitlab.com/fi-sas/fisas-webpage/commit/44b3a8291c6fc8435056cdbc0a12aba77b8138d7))

## [1.10.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.6...v1.10.7) (2021-04-12)


### Bug Fixes

* **social_support:** application status history ([c1a4775](https://gitlab.com/fi-sas/fisas-webpage/commit/c1a4775af3e59d94f620cc2362cd57a49d2da737))

## [1.10.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.5...v1.10.6) (2021-04-12)


### Bug Fixes

* **social_support:** new pages appli and collab ([e825e7e](https://gitlab.com/fi-sas/fisas-webpage/commit/e825e7eacc5543f7ce5d991c72f4b612da630677))

## [1.10.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.4...v1.10.5) (2021-04-12)


### Bug Fixes

* **accommodation:** change valdiation to if else blocks ([5339081](https://gitlab.com/fi-sas/fisas-webpage/commit/5339081f5308281902720cdaf799634c358dbf0f))
* **accommodation:** display assigned residence name ([6bdde21](https://gitlab.com/fi-sas/fisas-webpage/commit/6bdde2106807dff75b77a61d77c9355e35cfd78c))
* **interceptor:** fix unhandled 422 error code ([6839eb8](https://gitlab.com/fi-sas/fisas-webpage/commit/6839eb8adb9d41bf37de690216554c73de759b68))

## [1.10.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.3...v1.10.4) (2021-04-08)


### Bug Fixes

* fixes on shopping cart and private accomodation form ([de2cb19](https://gitlab.com/fi-sas/fisas-webpage/commit/de2cb19ca1c10641fe7e8e9fd9a3f4ca043b7252))
* **social-support:** adapt attendance actions modals ([1786166](https://gitlab.com/fi-sas/fisas-webpage/commit/1786166674231cf7bf0bed9a5bd1cedaa1d7a94e))
* **social-support:** adapt attendance actions modals ([0bc0eaa](https://gitlab.com/fi-sas/fisas-webpage/commit/0bc0eaa9b228ae49dd01ffc00ccc2da80c959827))
* **social-support:** adapt attendance actions modals ([b5f118c](https://gitlab.com/fi-sas/fisas-webpage/commit/b5f118ce3e5fea3a36ed2548f664f3f56b132ec3))
* **social-support:** adapt attendance actions modals ([cc8872b](https://gitlab.com/fi-sas/fisas-webpage/commit/cc8872b6a3f2e21e1319a339903de46d89c3e492))
* **social-support:** adapt attendance actions modals ([87dcaad](https://gitlab.com/fi-sas/fisas-webpage/commit/87dcaad1982e6a977ae4132cad9b281cd46e2dc4))
* **social-support:** adapt attendance actions modals ([854d7ac](https://gitlab.com/fi-sas/fisas-webpage/commit/854d7ace26f45e0047bda2cced3d53d47b15f36f))
* **social-support:** adapt attendance actions modals ([fe580c9](https://gitlab.com/fi-sas/fisas-webpage/commit/fe580c9652954d060c65143a0805499abcba8788))
* **social-support:** adapt attendance actions modals ([5bd2b82](https://gitlab.com/fi-sas/fisas-webpage/commit/5bd2b823be684679da987240530b8b846864c23a))
* **social-support:** adapt attendance actions modals ([be6d38d](https://gitlab.com/fi-sas/fisas-webpage/commit/be6d38de44bca92fce5e53cc037c5afd196f617a))
* **social-support:** adapt attendance actions modals ([6ab7994](https://gitlab.com/fi-sas/fisas-webpage/commit/6ab799433a24f86237c2785ffe4b10ef58971813))
* **social-support:** adapt attendance actions modals ([17e4a6c](https://gitlab.com/fi-sas/fisas-webpage/commit/17e4a6c4290d48d295f782580632d472ae857317))
* **social-support:** fix build not workin ([543d52a](https://gitlab.com/fi-sas/fisas-webpage/commit/543d52a0c6e718e9c1bea3b7c60a802b1c3a8c96))
* **social-support-application:** fixed form and review flow ([a38b24f](https://gitlab.com/fi-sas/fisas-webpage/commit/a38b24f8e41ec22649a46a8bb27bebe61404e60c))
* **social-support-application:** fixed form and review flow ([c5f6c9b](https://gitlab.com/fi-sas/fisas-webpage/commit/c5f6c9b00d7926a1f0dfbdf9211cfc928c106671))
* **social-support-application:** prevent memory leak ([06568ab](https://gitlab.com/fi-sas/fisas-webpage/commit/06568ab71da6083dbc048f206c7250eb997f614c))

## [1.10.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.2...v1.10.3) (2021-04-07)


### Bug Fixes

* **accomodation:** remove restricted date on applicaiton absence submit ([d4844e1](https://gitlab.com/fi-sas/fisas-webpage/commit/d4844e1e7a3f49e5e64ccfeabfa2292e8bdf156f))

## [1.10.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.1...v1.10.2) (2021-04-07)


### Bug Fixes

* **ubike:** multiple fixes on various pages ([e89be65](https://gitlab.com/fi-sas/fisas-webpage/commit/e89be6505d347c414852edeb482aecdc04eac3f6))

## [1.10.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.10.0...v1.10.1) (2021-04-06)


### Bug Fixes

* **reports:** color button ([229ca98](https://gitlab.com/fi-sas/fisas-webpage/commit/229ca98835825eff853c907c2a200eaa853650d7))

# [1.10.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.9.4...v1.10.0) (2021-03-31)


### Features

* **social-support:** tweak record attendance layout and style ([c6e60cf](https://gitlab.com/fi-sas/fisas-webpage/commit/c6e60cff5277076e49ab1853e767d5dbdca4e95a))
* **social-support:** tweak record attendance layout and style ([070970b](https://gitlab.com/fi-sas/fisas-webpage/commit/070970bd9cf3e6838e66ce3fe1444f93ec7f1691))
* **social-support:** tweak record attendance layout and style ([f37cea2](https://gitlab.com/fi-sas/fisas-webpage/commit/f37cea2fa8503bd35e84fa8c183daacd3fbd48c9))
* **social-support:** tweak record attendance layout and style ([a86b4a9](https://gitlab.com/fi-sas/fisas-webpage/commit/a86b4a91d942efcdd7db3a4c65cac017365de284))
* **social-support:** tweak record attendance layout and style ([5b88ec0](https://gitlab.com/fi-sas/fisas-webpage/commit/5b88ec0023da164a7305b7c1451543a75537927d))
* **social-support:** tweak record attendance layout and style ([bb849cb](https://gitlab.com/fi-sas/fisas-webpage/commit/bb849cb4387f44a40d5719847482fd95a9db946e))
* **social-support:** tweak record attendance layout and style ([4a2f1f4](https://gitlab.com/fi-sas/fisas-webpage/commit/4a2f1f4120335b723c399f5dfe9b1f5acfc83766))
* **social-support:** tweak record attendance layout and style ([f5a6f15](https://gitlab.com/fi-sas/fisas-webpage/commit/f5a6f15f0ac4852a92e8b2895278bd3e7d848aac))

## [1.9.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.9.3...v1.9.4) (2021-03-29)


### Bug Fixes

* **theme:** minor fix on global theme ([7b16a93](https://gitlab.com/fi-sas/fisas-webpage/commit/7b16a93a2fdfa83c089d5ca2b0a0fec893d0f346))

## [1.9.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.9.2...v1.9.3) (2021-03-26)


### Bug Fixes

* **dashboard:** remove red background from federate login ([49a8fcc](https://gitlab.com/fi-sas/fisas-webpage/commit/49a8fccd34b706aa28db4f98188ebf165e6cf573))

## [1.9.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.9.1...v1.9.2) (2021-03-26)


### Bug Fixes

* **current account:** layout fixed according BO and addictions maded ([b433aab](https://gitlab.com/fi-sas/fisas-webpage/commit/b433aab4f6718981bc1d6a802af30d44e0282895))

## [1.9.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.9.0...v1.9.1) (2021-03-26)


### Bug Fixes

* **social:** new responsible advisor page ([9729079](https://gitlab.com/fi-sas/fisas-webpage/commit/9729079b154e15587ab39108b7723aff98bf7b22))

# [1.9.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.5...v1.9.0) (2021-03-26)


### Features

* **social-support:** added social support application detail page ([5693b6b](https://gitlab.com/fi-sas/fisas-webpage/commit/5693b6b930434d0129d8b025d85bf595e245d57e))
* **social-support:** added social support application detail page ([233e1f7](https://gitlab.com/fi-sas/fisas-webpage/commit/233e1f73dd8f3e0116170c647c92d9b4ccc3e044))
* **social-support:** added social support application detail page ([5b39c9e](https://gitlab.com/fi-sas/fisas-webpage/commit/5b39c9ebf8ba50929dfc4c1a34a0a36e0ba1660f))
* **social-support:** added social support application detail page ([368801b](https://gitlab.com/fi-sas/fisas-webpage/commit/368801b0102d5725e634856fa71523022b9b5106))
* **social-support:** added social support application detail page ([50f4673](https://gitlab.com/fi-sas/fisas-webpage/commit/50f4673a32dff4390b9af79edab40993f7b92871))
* **social-support:** added social support application detail page ([06d09a9](https://gitlab.com/fi-sas/fisas-webpage/commit/06d09a90f99aa0ed5b650b06be9a456e174bce90))
* **social-support:** added social support application detail page ([384b9a2](https://gitlab.com/fi-sas/fisas-webpage/commit/384b9a2479bafc2b053125fcb153894aedf0d400))

## [1.8.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.4...v1.8.5) (2021-03-26)


### Bug Fixes

* **privateaccommodation:** orthography and positioning fixes ([8af1405](https://gitlab.com/fi-sas/fisas-webpage/commit/8af140529f747bb8f92e9c2b9bca2c5b6320dd49))

## [1.8.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.3...v1.8.4) (2021-03-25)


### Bug Fixes

* **notifications:** error correction ([4b0fb9b](https://gitlab.com/fi-sas/fisas-webpage/commit/4b0fb9b1f85c91a9d0b7eb3f4a937e8c6c7b779e))

## [1.8.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.2...v1.8.3) (2021-03-25)


### Bug Fixes

* **social s:** history page, error reports ([4403d0b](https://gitlab.com/fi-sas/fisas-webpage/commit/4403d0b92ae22a291901f42a1d76809ddedb82b0))

## [1.8.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.1...v1.8.2) (2021-03-25)


### Bug Fixes

* **shoppingcart:** color and variable fixed ([210845c](https://gitlab.com/fi-sas/fisas-webpage/commit/210845c9d2fa4741daf89e17e27bbe1f156d8c65))

## [1.8.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.8.0...v1.8.1) (2021-03-24)


### Bug Fixes

* **alimentation:** list tickets available fixed ([838c633](https://gitlab.com/fi-sas/fisas-webpage/commit/838c633b787927086bd8019944459d7c52185f4c))

# [1.8.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.7.3...v1.8.0) (2021-03-24)


### Features

* **social-support:** social support application consent revision ([2f61977](https://gitlab.com/fi-sas/fisas-webpage/commit/2f6197701dd741d05e5ca8d10c77ad8fa51196a1))
* **social-support:** social support application consent revision ([6735136](https://gitlab.com/fi-sas/fisas-webpage/commit/67351361550f1298630ad224054adfe9b1404768))
* **social-support:** social support application consent revision ([7398d97](https://gitlab.com/fi-sas/fisas-webpage/commit/7398d9797e87f39bfc9748c4f17e5dfc216d3f21))
* **social-support:** social support application consent revision ([a545593](https://gitlab.com/fi-sas/fisas-webpage/commit/a5455930929d7717558afc96d771902ab4ef2adb))
* **social-support:** social support application consent revision ([4da4980](https://gitlab.com/fi-sas/fisas-webpage/commit/4da49801a502511b031edc07b3bee7904b8304d8))
* **social-support:** social support application consent revision ([4829775](https://gitlab.com/fi-sas/fisas-webpage/commit/4829775d571c32e63acb066c07e576b701e85a5b))
* **social-support:** social support application consent submission ([93c41c7](https://gitlab.com/fi-sas/fisas-webpage/commit/93c41c7df40ca82cb89d681c9041623eb7f11af6))
* **social-support:** social support application consent submission ([ecfb038](https://gitlab.com/fi-sas/fisas-webpage/commit/ecfb0385c81bc23ab45b44bd27c6487c4890d7f4))
* **social-support:** social support application form revision ([9677978](https://gitlab.com/fi-sas/fisas-webpage/commit/96779786384b1cdec7d52da8db612e22337996ab))

## [1.7.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.7.2...v1.7.3) (2021-03-24)


### Bug Fixes

* **shopping-cart:** fix css vars ([a7e1db2](https://gitlab.com/fi-sas/fisas-webpage/commit/a7e1db2c553674bf94e727801398ff968d86f7ed))
* **shopping-cart:** fix css vars ([a482914](https://gitlab.com/fi-sas/fisas-webpage/commit/a4829148382857e16e027a09d0f4b5e7e0f16379))

## [1.7.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.7.1...v1.7.2) (2021-03-24)


### Bug Fixes

* **shopping-cart:** checkout fixed ([489100b](https://gitlab.com/fi-sas/fisas-webpage/commit/489100b5a03fc239cdd545c2c73eb09bc8ef9520))
* **shoppingcart:** cart completed ([325b289](https://gitlab.com/fi-sas/fisas-webpage/commit/325b289dcd4d7cc5e302fe9375fbd81d8a127f9d))
* **shoppingcart:** code variables cleaning ([59a1e3e](https://gitlab.com/fi-sas/fisas-webpage/commit/59a1e3ead7a6ea00e4e1106254bef36e8436b9a0))
* **shoppingcart:** ngdeep fixed and encapsulated ([da4d242](https://gitlab.com/fi-sas/fisas-webpage/commit/da4d2421ea3769c68f069ebc959491ea2b4a567d))
* **shoppingcart:** small fix ([b31c4ab](https://gitlab.com/fi-sas/fisas-webpage/commit/b31c4abaee4a0031d89da25b66d3258d973a0923))

## [1.7.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.7.0...v1.7.1) (2021-03-23)


### Bug Fixes

* **bolsa:** permission, expression interest list ([e63411f](https://gitlab.com/fi-sas/fisas-webpage/commit/e63411f987cbff755ca2a339b2c92315eda6a404))
* **social:** page detail experience, permission ([bf85ef7](https://gitlab.com/fi-sas/fisas-webpage/commit/bf85ef7e0d944938edb8ab5c2df01ccdb1f419f5))

# [1.7.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.7...v1.7.0) (2021-03-19)


### Features

* **style:** add new theming system ([e8fa289](https://gitlab.com/fi-sas/fisas-webpage/commit/e8fa28989afa42ddbb0aa8657245d884ff4a3681))

## [1.6.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.6...v1.6.7) (2021-03-19)


### Bug Fixes

* **geral:** permission, change layout experince ss ([1536f24](https://gitlab.com/fi-sas/fisas-webpage/commit/1536f244d29e9ef05641722a6f0b80d122da2b03))
* **ubike:** change and fix errors on create/edit trips manual registration ([402488d](https://gitlab.com/fi-sas/fisas-webpage/commit/402488dd2203874dcd062ba0c5b278981c2fbbf6))

## [1.6.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.5...v1.6.6) (2021-03-18)


### Bug Fixes

* **geral:** correction tradutions ([2c0bf89](https://gitlab.com/fi-sas/fisas-webpage/commit/2c0bf89f3ce5f3e9cb16c623b278090d15d33231))

## [1.6.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.4...v1.6.5) (2021-03-17)


### Bug Fixes

* **ubike:** occurrence type ([9ebd985](https://gitlab.com/fi-sas/fisas-webpage/commit/9ebd9850c02b7b34e0388595ec586fbc0eaecd7c))

## [1.6.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.3...v1.6.4) (2021-03-17)


### Bug Fixes

* **ubike:** application and correction filter ([fba6a47](https://gitlab.com/fi-sas/fisas-webpage/commit/fba6a47b37ef6a4ca35bf527d5d2e55aecbdd398))

## [1.6.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.2...v1.6.3) (2021-03-15)


### Bug Fixes

* **dashboard:** translations queue, page news ([37eab1b](https://gitlab.com/fi-sas/fisas-webpage/commit/37eab1b49ed33aa179e860a786dbe9c548d85367))

## [1.6.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.1...v1.6.2) (2021-03-11)


### Bug Fixes

* **dashboard:** improvements layout ([67b9055](https://gitlab.com/fi-sas/fisas-webpage/commit/67b90552ab636c1c4f32f625b1ec57b8d8b80138))

## [1.6.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0...v1.6.1) (2021-03-11)


### Bug Fixes

* **dashboard:** finish widgets, change field ubike ([353e4ac](https://gitlab.com/fi-sas/fisas-webpage/commit/353e4ac0d798171c2ef0d6603d9c98d5eb59db0a))

# [1.6.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.1...v1.6.0) (2021-03-09)


### Bug Fixes

* **accomodation:** minor fix on application form ([1d8fa5b](https://gitlab.com/fi-sas/fisas-webpage/commit/1d8fa5baf25b557b4358279c86a5c01750539276))
* **account:** new layout deposits, valid transl ([6ab01ea](https://gitlab.com/fi-sas/fisas-webpage/commit/6ab01eabc3515e69a601134892bc70b5fc389f58))
* **alimentation:** add message error from server cancel reservation ([ea61135](https://gitlab.com/fi-sas/fisas-webpage/commit/ea61135a81ce8b92dd090ec38e3f501bbbab93c0))
* **alimentation:** dish_card update files ([81c4eeb](https://gitlab.com/fi-sas/fisas-webpage/commit/81c4eeb16695499a67cb3f099c0f08548d0e4ed1))
* **alimentation:** fix file in dish-card details ([a7ecbea](https://gitlab.com/fi-sas/fisas-webpage/commit/a7ecbeae651ffdf1d67b412ea140453ab855419f))
* **alimentation:** list-tickest update info ([6cf3fa0](https://gitlab.com/fi-sas/fisas-webpage/commit/6cf3fa05415b68b4b23e7c12d9bfe5c34e2cd301))
* **alimentation:** reservations fix problems ([657942a](https://gitlab.com/fi-sas/fisas-webpage/commit/657942a6d35b50d59cd6fb1a4e743cd33ccea101))
* **alimentation:** service of menus add withrelateds ([433b66d](https://gitlab.com/fi-sas/fisas-webpage/commit/433b66d15fe8b327aeb795396cbb5600a3b43282))
* **aloj.priv.:** form prop,map,filter,initial text ([685b076](https://gitlab.com/fi-sas/fisas-webpage/commit/685b07656b94f37b2bf7592b61ec3dede75a2ba8))
* **alojamentoprivado:** map ([3dac7f8](https://gitlab.com/fi-sas/fisas-webpage/commit/3dac7f8bcd19386782cfcdff871ec7edaf8c53a2))
* **bolsa:** convert title MS ([3dd1ec7](https://gitlab.com/fi-sas/fisas-webpage/commit/3dd1ec7a8bdb14ed9c8c3de4deacbc5aab69284d))
* **bolsa:** title with MS ([105774b](https://gitlab.com/fi-sas/fisas-webpage/commit/105774bcf769b7350b29c779c6831076647e7651))
* **caccount:** change "restituição" to "devolução" ([015cd45](https://gitlab.com/fi-sas/fisas-webpage/commit/015cd45288d2cf4aaee841eb8d8fb01878fab755))
* **current_account:** add CREDIT_NOTE ([3b32a92](https://gitlab.com/fi-sas/fisas-webpage/commit/3b32a92497a1dde0f9e000d09d6f7bc94c7cc8bb))
* **current_account:** add credit_note operation ([4b80233](https://gitlab.com/fi-sas/fisas-webpage/commit/4b802333bbc59885cff9ddef4059c4c51f785ed2))
* **current_account:** add REFUND operation ([d91042d](https://gitlab.com/fi-sas/fisas-webpage/commit/d91042d676d227ca3f6cc7579706afc3384fceeb))
* **current_account:** new history deposits ([1b87b9b](https://gitlab.com/fi-sas/fisas-webpage/commit/1b87b9b92688af7f388ea4dbf75a8166e81eb6a2))
* **currentaccount:** pay, queue MS, remove school ([e59fff0](https://gitlab.com/fi-sas/fisas-webpage/commit/e59fff0d8394944f90c8936f3385b3a63cfe5a2f))
* **dashboard:** widget curraccount,accommod,ubike ([e7d0542](https://gitlab.com/fi-sas/fisas-webpage/commit/e7d054216e6ef0ec27f40eeccea52aca60644d88))
* **dashboard:** widgets volunt,socialsupport,queue ([4c5f0c9](https://gitlab.com/fi-sas/fisas-webpage/commit/4c5f0c9648b02e6402193ac599fc01a9ca3a6812))
* **ubike:** correction errors in the pages ([7f4b239](https://gitlab.com/fi-sas/fisas-webpage/commit/7f4b2398f11c86fb19b9741d704d655bdd73a32c))
* array strocture for form accommodation ([4525216](https://gitlab.com/fi-sas/fisas-webpage/commit/452521606cc3b9e6a0bdfce699c9ca3062dc22fc))
* begining of reguamentation ([3888625](https://gitlab.com/fi-sas/fisas-webpage/commit/3888625e783e6ee543a9860582a5701093021a8c))
* change proxy to suport http cookie ([1d1a263](https://gitlab.com/fi-sas/fisas-webpage/commit/1d1a26375ac90592ca09ac68d7e3bd98839b2be4))
* inforation page fixed ([ef49e65](https://gitlab.com/fi-sas/fisas-webpage/commit/ef49e6531b8ccbc7acdb7b263577f076dca00104))
* **accommodation:** change form change contract ([79aa728](https://gitlab.com/fi-sas/fisas-webpage/commit/79aa728a7f55052030e100c7a30fde3868e720e4))
* **mobility:** adaptation to the new micro service ([6375c5e](https://gitlab.com/fi-sas/fisas-webpage/commit/6375c5e6eadeaed187ba1a05a2c2c03a5880d91f))
* **private-accommodation:** adaptation to the new micro service ([9298827](https://gitlab.com/fi-sas/fisas-webpage/commit/92988275ae6d244a1837d9e05787b66629c97db9))
* correction translations; delete form shared ([817fc81](https://gitlab.com/fi-sas/fisas-webpage/commit/817fc818af7f73dfcbba04165f1f90248a5868d2))
* **accommodation:** fix applocation model ([c3f8367](https://gitlab.com/fi-sas/fisas-webpage/commit/c3f836717937df50d066c5c123f6725484770f29))
* deleted old way ([702e7ad](https://gitlab.com/fi-sas/fisas-webpage/commit/702e7adab48b522250ed80d242a8762c92d679f0))
* small fixes and form reformulated ([fb0a059](https://gitlab.com/fi-sas/fisas-webpage/commit/fb0a059c3812d80d7de36f6acb5218324e298867))
* small things I forgot to take out ([d376626](https://gitlab.com/fi-sas/fisas-webpage/commit/d376626e77e95c5f76591ad3e3c0f8e8cba3eb1a))
* **accommodation:** fix minor issues ([5012812](https://gitlab.com/fi-sas/fisas-webpage/commit/5012812d85ebd5da2bdea5c2f56910d98c81ac4e))
* **accommodation:** fix offset of regimes ([7d96362](https://gitlab.com/fi-sas/fisas-webpage/commit/7d96362538fcb81c92ce105b9cdcccea235a44af))
* **accommodation:** fix withdrawal model ([fc7646c](https://gitlab.com/fi-sas/fisas-webpage/commit/fc7646c3352131cf6dd3d46c091cd9250e248e0f))
* **accommodation:** lazy loading; breadcrumb ([ad5be4a](https://gitlab.com/fi-sas/fisas-webpage/commit/ad5be4aa09c20ff078e49a7d0740a8deba827d94))
* **accommodation:** student validation ([c43fb65](https://gitlab.com/fi-sas/fisas-webpage/commit/c43fb65bf443358e4b106f38c0f8c540549d4b33))
* **alimentation:** lazy loading ([5c2938f](https://gitlab.com/fi-sas/fisas-webpage/commit/5c2938f7ac50fc7e1dbc1a761b1c299e5f42c70e))
* **build:** fix minor issues ([b0b0e66](https://gitlab.com/fi-sas/fisas-webpage/commit/b0b0e66b8c51c2f0b317d0bd86ecef2f74f64a86))
* **dashboard:** fix routing or dashboard ([378d972](https://gitlab.com/fi-sas/fisas-webpage/commit/378d972b885aad9dc31003e254a45e2c51df85e9))
* **food:** reservation information- nullable_until ([0c5ce9d](https://gitlab.com/fi-sas/fisas-webpage/commit/0c5ce9d9fed781cb17bdecb221a3ae5beb84c741))
* **login:** temporary login fix ([e6a9d11](https://gitlab.com/fi-sas/fisas-webpage/commit/e6a9d119564a498091c6926f3122ccc59724e7e3))
* mandatory symbol; data summary application ([d5a3b31](https://gitlab.com/fi-sas/fisas-webpage/commit/d5a3b312b16dc1f29faa46bc16bbcadc579ae4ae))
* minor fixs ([908dccb](https://gitlab.com/fi-sas/fisas-webpage/commit/908dccb2bef6299733520868700514ebdf5976f9))
* minor fixs ([2252ae4](https://gitlab.com/fi-sas/fisas-webpage/commit/2252ae407bfb8480131a01e8df01591408d287d1))
* model fix ([38f950c](https://gitlab.com/fi-sas/fisas-webpage/commit/38f950cfb626f86a6ae22ca84381672d73661d7d))
* regulations connected and corrected ([7483c58](https://gitlab.com/fi-sas/fisas-webpage/commit/7483c5852ca085640042fd77141ccca4aaeef7a5))
* u-bike contract and adding Km fixes ([0e8e6c3](https://gitlab.com/fi-sas/fisas-webpage/commit/0e8e6c36128ffa659df84dd9822d6fff5d342293))
* **accommodation:** change breadcrumb ([1895801](https://gitlab.com/fi-sas/fisas-webpage/commit/1895801d5321136967dece2c5dfda4a3ed87d33d))
* **accommodation:** fix change contracto form validation ([667bbff](https://gitlab.com/fi-sas/fisas-webpage/commit/667bbffd984865c90c9d53aeab84ec0b51afd14c))
* **corrections:** forms ubike; manual ubike; text ([bc9c127](https://gitlab.com/fi-sas/fisas-webpage/commit/bc9c1276602a6198d2068433ebf1bb4a5e1a6ce6))
* **current-account:** lazy loading ([25d0be1](https://gitlab.com/fi-sas/fisas-webpage/commit/25d0be19c206e28a82aa9df5b598bc67414197c6))
* **geral:** correction language regulations ([36332c4](https://gitlab.com/fi-sas/fisas-webpage/commit/36332c4ceb0c7ace536e99f7599a0aa636e147e3))
* **geral:** menu, reports, notifications ([8072774](https://gitlab.com/fi-sas/fisas-webpage/commit/807277486dd6316f3a0946ca00e42cdb618ddc11))
* **movement-detail:** add status PAID ([9d081fc](https://gitlab.com/fi-sas/fisas-webpage/commit/9d081fc2db75834077acab3f7489d259358246c8))
* **movements:** change sort by -id to -created_at ([de897cd](https://gitlab.com/fi-sas/fisas-webpage/commit/de897cd618ee7a948bfff95fb1df1dcef70a1fa7))
* **private accommodation:** permission validation ([9c1f205](https://gitlab.com/fi-sas/fisas-webpage/commit/9c1f205336433759f614417d81547418c07ed147))
* **private_accommodation:** fix issues ([213efc8](https://gitlab.com/fi-sas/fisas-webpage/commit/213efc83c56762dde1e4b8b74e116cff1d01f3b6))
* **private-accommodation:** fix maps ([df57adc](https://gitlab.com/fi-sas/fisas-webpage/commit/df57adc093ec5cbec19c43ace2e98841eaa4e137))
* **private-accommodation:** lazy loading ([32321b9](https://gitlab.com/fi-sas/fisas-webpage/commit/32321b97008854048558e74dcdee9d1d00daf236))
* **privateaccommodation:** formowner issuesreceipt ([cad3e07](https://gitlab.com/fi-sas/fisas-webpage/commit/cad3e07d42f12f1cb6dbff720639617fef7bc687))
* **privateaccommodation:** new form property ([b3cc9a6](https://gitlab.com/fi-sas/fisas-webpage/commit/b3cc9a6631030cd11150317cb4dddf4cd33262ab))
* **privateaccommodation:** postal code correction ([cd653b7](https://gitlab.com/fi-sas/fisas-webpage/commit/cd653b7a2a0530c70a7808e78fb0857ad189803a))
* **privateaccommodation:** postalcode correction ([f4a63e1](https://gitlab.com/fi-sas/fisas-webpage/commit/f4a63e11345caef4254ea2885004fce6fb393d12))
* **privateaccommodation:** student validation ([8afbaa9](https://gitlab.com/fi-sas/fisas-webpage/commit/8afbaa95fc5e029f05f881fed4f173254db472a3))
* **privateaccommodation:** title apresentation ([c987dcc](https://gitlab.com/fi-sas/fisas-webpage/commit/c987dcc181ef983c5e8bcdd10b289a6c8f24883b))
* **profile:** change form personal data ([3520073](https://gitlab.com/fi-sas/fisas-webpage/commit/3520073dad9c5fff990693e524744066348ca2ec))
* **profile:** change img; responsive ([683f232](https://gitlab.com/fi-sas/fisas-webpage/commit/683f232966b6f0eb7f29ecff1355e52f4e6844a8))
* **routing:** fox lazyLoad routing ([09861b6](https://gitlab.com/fi-sas/fisas-webpage/commit/09861b63f2792183b6e4ed4ebe3b9b95e2e535e6))
* **social_schollarship:** minor fox for migration ([aaa969d](https://gitlab.com/fi-sas/fisas-webpage/commit/aaa969d1bfccc85cee339a3ac32226220a0d4778))
* **social_support:** building error missing buttonStyle ([bf08fc9](https://gitlab.com/fi-sas/fisas-webpage/commit/bf08fc9cfc063ab5da34981cbaef04e70b29e85c))
* **social_support:** fix error on form attendance ([1b2ad16](https://gitlab.com/fi-sas/fisas-webpage/commit/1b2ad166d88152bc4dc31d3c574780aa13d99c18))
* **social_support:** fix error on form attendance ([9666977](https://gitlab.com/fi-sas/fisas-webpage/commit/96669773137afbf340befd2f715a9d7320cb591f))
* **social_support:** resolve merge conflits ([e20f723](https://gitlab.com/fi-sas/fisas-webpage/commit/e20f723ada8a575e42617ab192f64c599a150a9c))
* **social_support:** some language errors ([47142fd](https://gitlab.com/fi-sas/fisas-webpage/commit/47142fd8bc9e1abbebbb712a698ad1c296240be3))
* **social-support:** lazy loading ([233acd4](https://gitlab.com/fi-sas/fisas-webpage/commit/233acd491de75d465e7f0c8b55500fc6d4c41210))
* **u-bike:** add km button validation ([7a063bc](https://gitlab.com/fi-sas/fisas-webpage/commit/7a063bcf5ff27d60fb2dfa813790f2309cc5d4f2))
* **u-bike:** decimal places km ([def2b4a](https://gitlab.com/fi-sas/fisas-webpage/commit/def2b4ad21eb0b2de14fb0340c901859ce7d2041))
* **ubike:** change form and delete simple form ([9f4a34c](https://gitlab.com/fi-sas/fisas-webpage/commit/9f4a34cf1d11f1473832c1bde2851909faee0250))
* **ubike:** correction of form errors ([3cbcbc9](https://gitlab.com/fi-sas/fisas-webpage/commit/3cbcbc98cbf7ebaa8f1c0ce61819c366989e605e))
* **ubike:** new application form ([b96f5a8](https://gitlab.com/fi-sas/fisas-webpage/commit/b96f5a84f7cf50c223345f2546cbfccb193d3046))
* change text; service error message ([d7ad658](https://gitlab.com/fi-sas/fisas-webpage/commit/d7ad658418010c9207da424943b87cb2fde88d6d))
* error correction lazy loading ([73500c2](https://gitlab.com/fi-sas/fisas-webpage/commit/73500c23f6a94280e0e1bb1f651076b52542f7d9))
* fix on form application accomudation ([5f32b15](https://gitlab.com/fi-sas/fisas-webpage/commit/5f32b15e1d3590e8e52bf615a4c1623fb11a83ab))
* form accomodation application ([bfbe666](https://gitlab.com/fi-sas/fisas-webpage/commit/bfbe6663442371ae0d0d974955bb28a1e0d089da))
* impledmented maps and fixed numbers ([0922e68](https://gitlab.com/fi-sas/fisas-webpage/commit/0922e68aaa081367668a05c863e870c7feec4cc7))
* minor fixs ([d973a5f](https://gitlab.com/fi-sas/fisas-webpage/commit/d973a5fa2114a0173d98dc04ed8eb160e5e5dad1))
* revert changes ([cdbdcb4](https://gitlab.com/fi-sas/fisas-webpage/commit/cdbdcb495e433201afe0e12719330e9c20c2b906))
* small fixes on steps and form ([c552f8b](https://gitlab.com/fi-sas/fisas-webpage/commit/c552f8b47cff9ca97b1b943ee14a41e0fd4f3596))
* step 4 ([b2a9052](https://gitlab.com/fi-sas/fisas-webpage/commit/b2a9052d9a06980f4634f90ab699f78586d23ac2))
* ubike fix and other fixes ([6ab551d](https://gitlab.com/fi-sas/fisas-webpage/commit/6ab551d472fcf67c46dec5cbf867e9c905d4d79a))
* **ubike:** validation form ([2bf9998](https://gitlab.com/fi-sas/fisas-webpage/commit/2bf99983057a6928a8bdd93aed29e0b223715858))
* **volunteering:** fix lazy load ([830f59c](https://gitlab.com/fi-sas/fisas-webpage/commit/830f59ccc7c2753374af12fb8a39ce0b31c17567))
* **volunteering:** lazy loading error correction ([943b51e](https://gitlab.com/fi-sas/fisas-webpage/commit/943b51e4381ecebf5e5965a6008d645d4a66092e))
* **volunteering:** lazy loading; change menu ([7bd94e0](https://gitlab.com/fi-sas/fisas-webpage/commit/7bd94e0abf3ecc986a7bcfc78d9b6da9cb86ba4e))


### Features

* **auth:** change auth method to refresh token ([79f4c71](https://gitlab.com/fi-sas/fisas-webpage/commit/79f4c7155de42f8bb1f159ecbfaad6be8c1db169))
* **cookie_consent:** initial cookie consent notification ([08f527d](https://gitlab.com/fi-sas/fisas-webpage/commit/08f527d362d35d7e204ea2b259b2e7a6d06df528))
* **cookies_consent:** addd multilang to cookie consent ([b15250c](https://gitlab.com/fi-sas/fisas-webpage/commit/b15250ce7e4b8ca2301289cc017ac7e3d2bebe77))
* **core:** add improvent form link to footer ([5047cbf](https://gitlab.com/fi-sas/fisas-webpage/commit/5047cbfa55cc1871df500d596afc91dfc2f03bfc))
* **social_support:** add executed service and hours to attendances ([0385fe0](https://gitlab.com/fi-sas/fisas-webpage/commit/0385fe07e86846d629be0adbac9170364792a687))
* **social_support:** add translations ([a65e068](https://gitlab.com/fi-sas/fisas-webpage/commit/a65e068896fb02a07f58412b14be7cc4f6f7c68b))

# [1.6.0-dev.37](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.36...v1.6.0-dev.37) (2021-03-09)


### Bug Fixes

* **dashboard:** widgets volunt,socialsupport,queue ([4c5f0c9](https://gitlab.com/fi-sas/fisas-webpage/commit/4c5f0c9648b02e6402193ac599fc01a9ca3a6812))

# [1.6.0-dev.36](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.35...v1.6.0-dev.36) (2021-03-05)


### Bug Fixes

* **dashboard:** widget curraccount,accommod,ubike ([e7d0542](https://gitlab.com/fi-sas/fisas-webpage/commit/e7d054216e6ef0ec27f40eeccea52aca60644d88))

# [1.6.0-dev.35](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.34...v1.6.0-dev.35) (2021-03-05)


### Bug Fixes

* **ubike:** correction errors in the pages ([7f4b239](https://gitlab.com/fi-sas/fisas-webpage/commit/7f4b2398f11c86fb19b9741d704d655bdd73a32c))

# [1.6.0-dev.34](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.33...v1.6.0-dev.34) (2021-03-03)


### Bug Fixes

* **current_account:** new history deposits ([1b87b9b](https://gitlab.com/fi-sas/fisas-webpage/commit/1b87b9b92688af7f388ea4dbf75a8166e81eb6a2))

# [1.6.0-dev.33](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.32...v1.6.0-dev.33) (2021-03-02)


### Bug Fixes

* inforation page fixed ([ef49e65](https://gitlab.com/fi-sas/fisas-webpage/commit/ef49e6531b8ccbc7acdb7b263577f076dca00104))
* **alimentation:** add message error from server cancel reservation ([ea61135](https://gitlab.com/fi-sas/fisas-webpage/commit/ea61135a81ce8b92dd090ec38e3f501bbbab93c0))
* **alimentation:** dish_card update files ([81c4eeb](https://gitlab.com/fi-sas/fisas-webpage/commit/81c4eeb16695499a67cb3f099c0f08548d0e4ed1))
* **alimentation:** fix file in dish-card details ([a7ecbea](https://gitlab.com/fi-sas/fisas-webpage/commit/a7ecbeae651ffdf1d67b412ea140453ab855419f))
* **alimentation:** list-tickest update info ([6cf3fa0](https://gitlab.com/fi-sas/fisas-webpage/commit/6cf3fa05415b68b4b23e7c12d9bfe5c34e2cd301))
* **alimentation:** reservations fix problems ([657942a](https://gitlab.com/fi-sas/fisas-webpage/commit/657942a6d35b50d59cd6fb1a4e743cd33ccea101))
* **alimentation:** service of menus add withrelateds ([433b66d](https://gitlab.com/fi-sas/fisas-webpage/commit/433b66d15fe8b327aeb795396cbb5600a3b43282))

# [1.6.0-dev.32](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.31...v1.6.0-dev.32) (2021-03-01)


### Bug Fixes

* **accomodation:** minor fix on application form ([1d8fa5b](https://gitlab.com/fi-sas/fisas-webpage/commit/1d8fa5baf25b557b4358279c86a5c01750539276))

# [1.6.0-dev.31](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.30...v1.6.0-dev.31) (2021-02-25)


### Bug Fixes

* **caccount:** change "restituição" to "devolução" ([015cd45](https://gitlab.com/fi-sas/fisas-webpage/commit/015cd45288d2cf4aaee841eb8d8fb01878fab755))

# [1.6.0-dev.30](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.29...v1.6.0-dev.30) (2021-02-24)


### Bug Fixes

* u-bike contract and adding Km fixes ([0e8e6c3](https://gitlab.com/fi-sas/fisas-webpage/commit/0e8e6c36128ffa659df84dd9822d6fff5d342293))

# [1.6.0-dev.29](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.28...v1.6.0-dev.29) (2021-02-23)


### Bug Fixes

* **currentaccount:** pay, queue MS, remove school ([e59fff0](https://gitlab.com/fi-sas/fisas-webpage/commit/e59fff0d8394944f90c8936f3385b3a63cfe5a2f))

# [1.6.0-dev.28](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.27...v1.6.0-dev.28) (2021-02-23)


### Bug Fixes

* **current_account:** add CREDIT_NOTE ([3b32a92](https://gitlab.com/fi-sas/fisas-webpage/commit/3b32a92497a1dde0f9e000d09d6f7bc94c7cc8bb))
* **current_account:** add REFUND operation ([d91042d](https://gitlab.com/fi-sas/fisas-webpage/commit/d91042d676d227ca3f6cc7579706afc3384fceeb))

# [1.6.0-dev.27](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.26...v1.6.0-dev.27) (2021-02-22)


### Bug Fixes

* **current_account:** add credit_note operation ([4b80233](https://gitlab.com/fi-sas/fisas-webpage/commit/4b802333bbc59885cff9ddef4059c4c51f785ed2))

# [1.6.0-dev.26](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.25...v1.6.0-dev.26) (2021-02-19)


### Bug Fixes

* **geral:** correction language regulations ([36332c4](https://gitlab.com/fi-sas/fisas-webpage/commit/36332c4ceb0c7ace536e99f7599a0aa636e147e3))

# [1.6.0-dev.25](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.24...v1.6.0-dev.25) (2021-02-18)


### Bug Fixes

* begining of reguamentation ([3888625](https://gitlab.com/fi-sas/fisas-webpage/commit/3888625e783e6ee543a9860582a5701093021a8c))
* regulations connected and corrected ([7483c58](https://gitlab.com/fi-sas/fisas-webpage/commit/7483c5852ca085640042fd77141ccca4aaeef7a5))

# [1.6.0-dev.24](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.23...v1.6.0-dev.24) (2021-02-15)


### Features

* **cookies_consent:** addd multilang to cookie consent ([b15250c](https://gitlab.com/fi-sas/fisas-webpage/commit/b15250ce7e4b8ca2301289cc017ac7e3d2bebe77))

# [1.6.0-dev.23](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.22...v1.6.0-dev.23) (2021-02-15)


### Features

* **cookie_consent:** initial cookie consent notification ([08f527d](https://gitlab.com/fi-sas/fisas-webpage/commit/08f527d362d35d7e204ea2b259b2e7a6d06df528))

# [1.6.0-dev.22](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.21...v1.6.0-dev.22) (2021-02-12)


### Bug Fixes

* **account:** new layout deposits, valid transl ([6ab01ea](https://gitlab.com/fi-sas/fisas-webpage/commit/6ab01eabc3515e69a601134892bc70b5fc389f58))

# [1.6.0-dev.21](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.20...v1.6.0-dev.21) (2021-02-11)


### Bug Fixes

* **social_support:** building error missing buttonStyle ([bf08fc9](https://gitlab.com/fi-sas/fisas-webpage/commit/bf08fc9cfc063ab5da34981cbaef04e70b29e85c))
* **social_support:** some language errors ([47142fd](https://gitlab.com/fi-sas/fisas-webpage/commit/47142fd8bc9e1abbebbb712a698ad1c296240be3))

# [1.6.0-dev.20](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.19...v1.6.0-dev.20) (2021-02-10)


### Bug Fixes

* **bolsa:** convert title MS ([3dd1ec7](https://gitlab.com/fi-sas/fisas-webpage/commit/3dd1ec7a8bdb14ed9c8c3de4deacbc5aab69284d))
* **bolsa:** title with MS ([105774b](https://gitlab.com/fi-sas/fisas-webpage/commit/105774bcf769b7350b29c779c6831076647e7651))

# [1.6.0-dev.19](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.18...v1.6.0-dev.19) (2021-02-08)


### Bug Fixes

* **social_support:** fix error on form attendance ([1b2ad16](https://gitlab.com/fi-sas/fisas-webpage/commit/1b2ad166d88152bc4dc31d3c574780aa13d99c18))
* **social_support:** resolve merge conflits ([e20f723](https://gitlab.com/fi-sas/fisas-webpage/commit/e20f723ada8a575e42617ab192f64c599a150a9c))


### Features

* **social_support:** add translations ([a65e068](https://gitlab.com/fi-sas/fisas-webpage/commit/a65e068896fb02a07f58412b14be7cc4f6f7c68b))

# [1.6.0-dev.18](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.17...v1.6.0-dev.18) (2021-02-08)


### Bug Fixes

* **aloj.priv.:** form prop,map,filter,initial text ([685b076](https://gitlab.com/fi-sas/fisas-webpage/commit/685b07656b94f37b2bf7592b61ec3dede75a2ba8))
* **alojamentoprivado:** map ([3dac7f8](https://gitlab.com/fi-sas/fisas-webpage/commit/3dac7f8bcd19386782cfcdff871ec7edaf8c53a2))

# [1.6.0-dev.17](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.16...v1.6.0-dev.17) (2021-02-08)


### Bug Fixes

* **social_support:** fix error on form attendance ([9666977](https://gitlab.com/fi-sas/fisas-webpage/commit/96669773137afbf340befd2f715a9d7320cb591f))

# [1.6.0-dev.16](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.15...v1.6.0-dev.16) (2021-02-08)


### Features

* **social_support:** add executed service and hours to attendances ([0385fe0](https://gitlab.com/fi-sas/fisas-webpage/commit/0385fe07e86846d629be0adbac9170364792a687))

# [1.6.0-dev.15](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.14...v1.6.0-dev.15) (2021-02-07)


### Bug Fixes

* change proxy to suport http cookie ([1d1a263](https://gitlab.com/fi-sas/fisas-webpage/commit/1d1a26375ac90592ca09ac68d7e3bd98839b2be4))

# [1.6.0-dev.14](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.13...v1.6.0-dev.14) (2021-02-06)


### Bug Fixes

* minor fixs ([908dccb](https://gitlab.com/fi-sas/fisas-webpage/commit/908dccb2bef6299733520868700514ebdf5976f9))
* minor fixs ([2252ae4](https://gitlab.com/fi-sas/fisas-webpage/commit/2252ae407bfb8480131a01e8df01591408d287d1))

# [1.6.0-dev.13](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.12...v1.6.0-dev.13) (2021-02-05)


### Features

* **auth:** change auth method to refresh token ([79f4c71](https://gitlab.com/fi-sas/fisas-webpage/commit/79f4c7155de42f8bb1f159ecbfaad6be8c1db169))

# [1.6.0-dev.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.11...v1.6.0-dev.12) (2021-02-04)


### Bug Fixes

* array strocture for form accommodation ([4525216](https://gitlab.com/fi-sas/fisas-webpage/commit/452521606cc3b9e6a0bdfce699c9ca3062dc22fc))
* fix on form application accomudation ([5f32b15](https://gitlab.com/fi-sas/fisas-webpage/commit/5f32b15e1d3590e8e52bf615a4c1623fb11a83ab))
* form accomodation application ([bfbe666](https://gitlab.com/fi-sas/fisas-webpage/commit/bfbe6663442371ae0d0d974955bb28a1e0d089da))
* model fix ([38f950c](https://gitlab.com/fi-sas/fisas-webpage/commit/38f950cfb626f86a6ae22ca84381672d73661d7d))
* small fixes on steps and form ([c552f8b](https://gitlab.com/fi-sas/fisas-webpage/commit/c552f8b47cff9ca97b1b943ee14a41e0fd4f3596))
* step 4 ([b2a9052](https://gitlab.com/fi-sas/fisas-webpage/commit/b2a9052d9a06980f4634f90ab699f78586d23ac2))

# [1.6.0-dev.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.10...v1.6.0-dev.11) (2021-02-04)


### Bug Fixes

* **movement-detail:** add status PAID ([9d081fc](https://gitlab.com/fi-sas/fisas-webpage/commit/9d081fc2db75834077acab3f7489d259358246c8))

# [1.6.0-dev.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.9...v1.6.0-dev.10) (2021-02-04)


### Bug Fixes

* **movements:** change sort by -id to -created_at ([de897cd](https://gitlab.com/fi-sas/fisas-webpage/commit/de897cd618ee7a948bfff95fb1df1dcef70a1fa7))

# [1.6.0-dev.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.8...v1.6.0-dev.9) (2021-02-03)


### Bug Fixes

* **geral:** menu, reports, notifications ([8072774](https://gitlab.com/fi-sas/fisas-webpage/commit/807277486dd6316f3a0946ca00e42cdb618ddc11))

# [1.6.0-dev.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.7...v1.6.0-dev.8) (2021-01-26)


### Bug Fixes

* **privateaccommodation:** new form property ([b3cc9a6](https://gitlab.com/fi-sas/fisas-webpage/commit/b3cc9a6631030cd11150317cb4dddf4cd33262ab))

# [1.6.0-dev.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.6...v1.6.0-dev.7) (2021-01-19)


### Bug Fixes

* **privateaccommodation:** title apresentation ([c987dcc](https://gitlab.com/fi-sas/fisas-webpage/commit/c987dcc181ef983c5e8bcdd10b289a6c8f24883b))

# [1.6.0-dev.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.5...v1.6.0-dev.6) (2021-01-18)


### Bug Fixes

* **private_accommodation:** fix issues ([213efc8](https://gitlab.com/fi-sas/fisas-webpage/commit/213efc83c56762dde1e4b8b74e116cff1d01f3b6))

# [1.6.0-dev.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.4...v1.6.0-dev.5) (2021-01-18)


### Bug Fixes

* **social_schollarship:** minor fox for migration ([aaa969d](https://gitlab.com/fi-sas/fisas-webpage/commit/aaa969d1bfccc85cee339a3ac32226220a0d4778))

# [1.6.0-dev.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.3...v1.6.0-dev.4) (2021-01-14)


### Bug Fixes

* **accommodation:** fix change contracto form validation ([667bbff](https://gitlab.com/fi-sas/fisas-webpage/commit/667bbffd984865c90c9d53aeab84ec0b51afd14c))

# [1.6.0-dev.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.2...v1.6.0-dev.3) (2021-01-13)


### Bug Fixes

* **mobility:** adaptation to the new micro service ([6375c5e](https://gitlab.com/fi-sas/fisas-webpage/commit/6375c5e6eadeaed187ba1a05a2c2c03a5880d91f))
* **private-accommodation:** adaptation to the new micro service ([9298827](https://gitlab.com/fi-sas/fisas-webpage/commit/92988275ae6d244a1837d9e05787b66629c97db9))

# [1.6.0-dev.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.6.0-dev.1...v1.6.0-dev.2) (2021-01-07)


### Bug Fixes

* correction translations; delete form shared ([817fc81](https://gitlab.com/fi-sas/fisas-webpage/commit/817fc818af7f73dfcbba04165f1f90248a5868d2))

# [1.6.0-dev.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.11...v1.6.0-dev.1) (2021-01-07)


### Features

* **core:** add improvent form link to footer ([5047cbf](https://gitlab.com/fi-sas/fisas-webpage/commit/5047cbfa55cc1871df500d596afc91dfc2f03bfc))

## [1.5.2-dev.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.10...v1.5.2-dev.11) (2020-12-16)


### Bug Fixes

* **accommodation:** fix applocation model ([c3f8367](https://gitlab.com/fi-sas/fisas-webpage/commit/c3f836717937df50d066c5c123f6725484770f29))

## [1.5.2-dev.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.9...v1.5.2-dev.10) (2020-12-16)


### Bug Fixes

* **accommodation:** fix minor issues ([5012812](https://gitlab.com/fi-sas/fisas-webpage/commit/5012812d85ebd5da2bdea5c2f56910d98c81ac4e))
* **accommodation:** fix offset of regimes ([7d96362](https://gitlab.com/fi-sas/fisas-webpage/commit/7d96362538fcb81c92ce105b9cdcccea235a44af))
* **accommodation:** fix withdrawal model ([fc7646c](https://gitlab.com/fi-sas/fisas-webpage/commit/fc7646c3352131cf6dd3d46c091cd9250e248e0f))
* **login:** temporary login fix ([e6a9d11](https://gitlab.com/fi-sas/fisas-webpage/commit/e6a9d119564a498091c6926f3122ccc59724e7e3))
* minor fixs ([d973a5f](https://gitlab.com/fi-sas/fisas-webpage/commit/d973a5fa2114a0173d98dc04ed8eb160e5e5dad1))

## [1.5.2-dev.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.8...v1.5.2-dev.9) (2020-12-14)


### Bug Fixes

* deleted old way ([702e7ad](https://gitlab.com/fi-sas/fisas-webpage/commit/702e7adab48b522250ed80d242a8762c92d679f0))
* small fixes and form reformulated ([fb0a059](https://gitlab.com/fi-sas/fisas-webpage/commit/fb0a059c3812d80d7de36f6acb5218324e298867))
* small things I forgot to take out ([d376626](https://gitlab.com/fi-sas/fisas-webpage/commit/d376626e77e95c5f76591ad3e3c0f8e8cba3eb1a))

## [1.5.2-dev.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.7...v1.5.2-dev.8) (2020-12-11)


### Bug Fixes

* mandatory symbol; data summary application ([d5a3b31](https://gitlab.com/fi-sas/fisas-webpage/commit/d5a3b312b16dc1f29faa46bc16bbcadc579ae4ae))

## [1.5.2-dev.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.6...v1.5.2-dev.7) (2020-12-10)


### Bug Fixes

* **ubike:** new application form ([b96f5a8](https://gitlab.com/fi-sas/fisas-webpage/commit/b96f5a84f7cf50c223345f2546cbfccb193d3046))

## [1.5.2-dev.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.5...v1.5.2-dev.6) (2020-12-09)


### Bug Fixes

* **private-accommodation:** fix maps ([df57adc](https://gitlab.com/fi-sas/fisas-webpage/commit/df57adc093ec5cbec19c43ace2e98841eaa4e137))

## [1.5.2-dev.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.4...v1.5.2-dev.5) (2020-12-09)


### Bug Fixes

* **ubike:** correction of form errors ([3cbcbc9](https://gitlab.com/fi-sas/fisas-webpage/commit/3cbcbc98cbf7ebaa8f1c0ce61819c366989e605e))

## [1.5.2-dev.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.3...v1.5.2-dev.4) (2020-12-07)


### Bug Fixes

* **ubike:** change form and delete simple form ([9f4a34c](https://gitlab.com/fi-sas/fisas-webpage/commit/9f4a34cf1d11f1473832c1bde2851909faee0250))

## [1.5.2-dev.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.2...v1.5.2-dev.3) (2020-12-07)


### Bug Fixes

* ubike fix and other fixes ([6ab551d](https://gitlab.com/fi-sas/fisas-webpage/commit/6ab551d472fcf67c46dec5cbf867e9c905d4d79a))
* **accommodation:** change breadcrumb ([1895801](https://gitlab.com/fi-sas/fisas-webpage/commit/1895801d5321136967dece2c5dfda4a3ed87d33d))
* **accommodation:** change form change contract ([79aa728](https://gitlab.com/fi-sas/fisas-webpage/commit/79aa728a7f55052030e100c7a30fde3868e720e4))
* **accommodation:** student validation ([c43fb65](https://gitlab.com/fi-sas/fisas-webpage/commit/c43fb65bf443358e4b106f38c0f8c540549d4b33))
* **corrections:** forms ubike; manual ubike; text ([bc9c127](https://gitlab.com/fi-sas/fisas-webpage/commit/bc9c1276602a6198d2068433ebf1bb4a5e1a6ce6))
* **food:** reservation information- nullable_until ([0c5ce9d](https://gitlab.com/fi-sas/fisas-webpage/commit/0c5ce9d9fed781cb17bdecb221a3ae5beb84c741))
* **private accommodation:** permission validation ([9c1f205](https://gitlab.com/fi-sas/fisas-webpage/commit/9c1f205336433759f614417d81547418c07ed147))
* **privateaccommodation:** formowner issuesreceipt ([cad3e07](https://gitlab.com/fi-sas/fisas-webpage/commit/cad3e07d42f12f1cb6dbff720639617fef7bc687))
* **privateaccommodation:** postal code correction ([cd653b7](https://gitlab.com/fi-sas/fisas-webpage/commit/cd653b7a2a0530c70a7808e78fb0857ad189803a))
* **privateaccommodation:** postalcode correction ([f4a63e1](https://gitlab.com/fi-sas/fisas-webpage/commit/f4a63e11345caef4254ea2885004fce6fb393d12))
* **privateaccommodation:** student validation ([8afbaa9](https://gitlab.com/fi-sas/fisas-webpage/commit/8afbaa95fc5e029f05f881fed4f173254db472a3))
* **profile:** change form personal data ([3520073](https://gitlab.com/fi-sas/fisas-webpage/commit/3520073dad9c5fff990693e524744066348ca2ec))
* **profile:** change img; responsive ([683f232](https://gitlab.com/fi-sas/fisas-webpage/commit/683f232966b6f0eb7f29ecff1355e52f4e6844a8))
* change text; service error message ([d7ad658](https://gitlab.com/fi-sas/fisas-webpage/commit/d7ad658418010c9207da424943b87cb2fde88d6d))
* impledmented maps and fixed numbers ([0922e68](https://gitlab.com/fi-sas/fisas-webpage/commit/0922e68aaa081367668a05c863e870c7feec4cc7))
* revert changes ([cdbdcb4](https://gitlab.com/fi-sas/fisas-webpage/commit/cdbdcb495e433201afe0e12719330e9c20c2b906))
* **u-bike:** add km button validation ([7a063bc](https://gitlab.com/fi-sas/fisas-webpage/commit/7a063bcf5ff27d60fb2dfa813790f2309cc5d4f2))
* **u-bike:** decimal places km ([def2b4a](https://gitlab.com/fi-sas/fisas-webpage/commit/def2b4ad21eb0b2de14fb0340c901859ce7d2041))
* **ubike:** validation form ([2bf9998](https://gitlab.com/fi-sas/fisas-webpage/commit/2bf99983057a6928a8bdd93aed29e0b223715858))

## [1.5.2-dev.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.2-dev.1...v1.5.2-dev.2) (2020-12-03)


### Bug Fixes

* **routing:** fox lazyLoad routing ([09861b6](https://gitlab.com/fi-sas/fisas-webpage/commit/09861b63f2792183b6e4ed4ebe3b9b95e2e535e6))

## [1.5.2-dev.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.1...v1.5.2-dev.1) (2020-12-02)


### Bug Fixes

* **build:** fix minor issues ([b0b0e66](https://gitlab.com/fi-sas/fisas-webpage/commit/b0b0e66b8c51c2f0b317d0bd86ecef2f74f64a86))
* **dashboard:** fix routing or dashboard ([378d972](https://gitlab.com/fi-sas/fisas-webpage/commit/378d972b885aad9dc31003e254a45e2c51df85e9))
* error correction lazy loading ([73500c2](https://gitlab.com/fi-sas/fisas-webpage/commit/73500c23f6a94280e0e1bb1f651076b52542f7d9))
* **accommodation:** lazy loading; breadcrumb ([ad5be4a](https://gitlab.com/fi-sas/fisas-webpage/commit/ad5be4aa09c20ff078e49a7d0740a8deba827d94))
* **alimentation:** lazy loading ([5c2938f](https://gitlab.com/fi-sas/fisas-webpage/commit/5c2938f7ac50fc7e1dbc1a761b1c299e5f42c70e))
* **current-account:** lazy loading ([25d0be1](https://gitlab.com/fi-sas/fisas-webpage/commit/25d0be19c206e28a82aa9df5b598bc67414197c6))
* **private-accommodation:** lazy loading ([32321b9](https://gitlab.com/fi-sas/fisas-webpage/commit/32321b97008854048558e74dcdee9d1d00daf236))
* **social-support:** lazy loading ([233acd4](https://gitlab.com/fi-sas/fisas-webpage/commit/233acd491de75d465e7f0c8b55500fc6d4c41210))
* **volunteering:** fix lazy load ([830f59c](https://gitlab.com/fi-sas/fisas-webpage/commit/830f59ccc7c2753374af12fb8a39ce0b31c17567))
* **volunteering:** lazy loading error correction ([943b51e](https://gitlab.com/fi-sas/fisas-webpage/commit/943b51e4381ecebf5e5965a6008d645d4a66092e))
* **volunteering:** lazy loading; change menu ([7bd94e0](https://gitlab.com/fi-sas/fisas-webpage/commit/7bd94e0abf3ecc986a7bcfc78d9b6da9cb86ba4e))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.5.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.5.0...v1.5.1) (2020-11-25)


### Bug Fixes

* **dashboard:** fix import from dashboard ([1a3da81](https://gitlab.com/fi-sas/fisas-webpage/commit/1a3da81))



## [1.5.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.4.3...v1.5.0) (2020-11-25)


### Bug Fixes

* small dashboard fixes ([441179d](https://gitlab.com/fi-sas/fisas-webpage/commit/441179d))
* **assets:** translations in english ([74d3f90](https://gitlab.com/fi-sas/fisas-webpage/commit/74d3f90))
* **social-support:** minor errors ([cb28711](https://gitlab.com/fi-sas/fisas-webpage/commit/cb28711))
* **ubike:** add applications to dashboard ([991afb9](https://gitlab.com/fi-sas/fisas-webpage/commit/991afb9))
* **ubike:** change menu ([a46538b](https://gitlab.com/fi-sas/fisas-webpage/commit/a46538b))
* **ubike:** error correction ng-deep ([ce5b314](https://gitlab.com/fi-sas/fisas-webpage/commit/ce5b314))
* **ubike:** forms; menu change; page occurrences ([d0723ad](https://gitlab.com/fi-sas/fisas-webpage/commit/d0723ad))
* **ubike:** lazy loading ubike ([94191cc](https://gitlab.com/fi-sas/fisas-webpage/commit/94191cc))
* **ubike:** mobile occurrences, status occurrences, status applications ([56f03c9](https://gitlab.com/fi-sas/fisas-webpage/commit/56f03c9))
* **ubike:** revert shared; new forms ([0c4a867](https://gitlab.com/fi-sas/fisas-webpage/commit/0c4a867))


### Build System

* **ipvc:** change staging ip address ([ee31cc5](https://gitlab.com/fi-sas/fisas-webpage/commit/ee31cc5))
* **ubike:** development of simple forms ([c2fdbb0](https://gitlab.com/fi-sas/fisas-webpage/commit/c2fdbb0))


### Features

* **dashboard:** add new dashboard page ([655196e](https://gitlab.com/fi-sas/fisas-webpage/commit/655196e))



### [1.4.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.4.2...v1.4.3) (2020-11-12)


### Bug Fixes

* **voluteering:** fix attachment file ([0d460f2](https://gitlab.com/fi-sas/fisas-webpage/commit/0d460f2))



### [1.4.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.4.1...v1.4.2) (2020-11-12)


### Bug Fixes

* **voluteering:** minor text fixs ([087a37e](https://gitlab.com/fi-sas/fisas-webpage/commit/087a37e))



### [1.4.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.4.0...v1.4.1) (2020-11-12)


### Bug Fixes

* **volunteering:** fix text ([c3f79fc](https://gitlab.com/fi-sas/fisas-webpage/commit/c3f79fc))



## [1.4.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.3.0...v1.4.0) (2020-10-27)


### Bug Fixes

* **accommodation:** fix application opposition loop ([0f22baa](https://gitlab.com/fi-sas/fisas-webpage/commit/0f22baa))
* **mobility:** minor fix withdrawal status ([772e591](https://gitlab.com/fi-sas/fisas-webpage/commit/772e591))
* **shared:** fix loaing sereval tima the same data ([3ceddde](https://gitlab.com/fi-sas/fisas-webpage/commit/3ceddde))


### Features

* **mobility:** add withdrawal and cancel option to applications ([56786d3](https://gitlab.com/fi-sas/fisas-webpage/commit/56786d3))



## [1.3.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.13...v1.3.0) (2020-10-09)


### Bug Fixes

* **u-bike:** fix orthographic error ([059173f](https://gitlab.com/fi-sas/fisas-webpage/commit/059173f))
* **volunteering:** fix accept experience ([27a8708](https://gitlab.com/fi-sas/fisas-webpage/commit/27a8708))
* **volunteering:** fix minor issues ([08aa665](https://gitlab.com/fi-sas/fisas-webpage/commit/08aa665))


### Features

* **social-support:** show monthly statistics ([33f13ca](https://gitlab.com/fi-sas/fisas-webpage/commit/33f13ca))



### [1.2.13](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.12...v1.2.13) (2020-10-01)


### Bug Fixes

* **accommodation:** fix isseu with outdate  phase ([5362a6a](https://gitlab.com/fi-sas/fisas-webpage/commit/5362a6a))
* **social-support:** fix justification absence date ([27c2f4b](https://gitlab.com/fi-sas/fisas-webpage/commit/27c2f4b))
* **social-support:** show types files accept in justification ([ddb7d0f](https://gitlab.com/fi-sas/fisas-webpage/commit/ddb7d0f))
* **ubike:** fix bug when show status in application ([bbaa34c](https://gitlab.com/fi-sas/fisas-webpage/commit/bbaa34c))



### [1.2.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.11...v1.2.12) (2020-09-29)


### Bug Fixes

* **accommodation:** fix validations in application form ([5f8cd22](https://gitlab.com/fi-sas/fisas-webpage/commit/5f8cd22))
* **environments:** enable private accommodation in ipca ([6742b72](https://gitlab.com/fi-sas/fisas-webpage/commit/6742b72))



### [1.2.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.10...v1.2.11) (2020-09-28)


### Bug Fixes

* **accommodation:** disabled inputs name and email in application ([1e1c4e3](https://gitlab.com/fi-sas/fisas-webpage/commit/1e1c4e3))
* **environment:** disable private accommodation ([3b2523b](https://gitlab.com/fi-sas/fisas-webpage/commit/3b2523b))



### [1.2.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.9...v1.2.10) (2020-09-22)


### Build System

* **envs:** update IPB env ([203ece5](https://gitlab.com/fi-sas/fisas-webpage/commit/203ece5))



### [1.2.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.8...v1.2.9) (2020-09-22)


### Build System

* **envs:** update environments ([33daf73](https://gitlab.com/fi-sas/fisas-webpage/commit/33daf73))



### [1.2.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.7...v1.2.8) (2020-09-21)


### Bug Fixes

* **private accommodation:** set terms for owner ([0b85a2d](https://gitlab.com/fi-sas/fisas-webpage/commit/0b85a2d))
* **private-accomodation:** add  issues receipt in owner application ([4579e0b](https://gitlab.com/fi-sas/fisas-webpage/commit/4579e0b))
* **private-accomodation:** fix owner form ([0fd9fd0](https://gitlab.com/fi-sas/fisas-webpage/commit/0fd9fd0))



### [1.2.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.6...v1.2.7) (2020-09-15)


### Bug Fixes

* **mobility:** fix applicartion locals ([a66ab76](https://gitlab.com/fi-sas/fisas-webpage/commit/a66ab76))
* **social-support:** add attendance date and fix withdrawl ([b8d981f](https://gitlab.com/fi-sas/fisas-webpage/commit/b8d981f))
* **social-support:** set prefix in iban and limit in scholarship value ([11b2e47](https://gitlab.com/fi-sas/fisas-webpage/commit/11b2e47))



### [1.2.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.5...v1.2.6) (2020-09-04)


### Bug Fixes

* **accommodation:** remove unactive items ([6af5b4c](https://gitlab.com/fi-sas/fisas-webpage/commit/6af5b4c))
* **accommodation:** set link for regulation file ([7f52d1a](https://gitlab.com/fi-sas/fisas-webpage/commit/7f52d1a))



### [1.2.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.4...v1.2.5) (2020-09-03)


### Bug Fixes

* **accommodation:** change to global application phases ([7dd3522](https://gitlab.com/fi-sas/fisas-webpage/commit/7dd3522))



### [1.2.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.3...v1.2.4) (2020-08-31)


### Bug Fixes

* **bus:** add the module link in the lateral menu ([c357531](https://gitlab.com/fi-sas/fisas-webpage/commit/c357531))
* **bus:** add valitions per step on form ([9a3f5bb](https://gitlab.com/fi-sas/fisas-webpage/commit/9a3f5bb))
* **environment:** fix dev environment ([aaa41af](https://gitlab.com/fi-sas/fisas-webpage/commit/aaa41af))



### [1.2.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.2...v1.2.3) (2020-08-31)


### Build System

* **ipvc:** activate the mobility module ([f989ac4](https://gitlab.com/fi-sas/fisas-webpage/commit/f989ac4))



### [1.2.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.1...v1.2.2) (2020-08-25)


### Bug Fixes

* **users:** add 'Other' gender ([47799a3](https://gitlab.com/fi-sas/fisas-webpage/commit/47799a3))



### [1.2.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.2.0...v1.2.1) (2020-08-21)


### Bug Fixes

* **mobility:** minor fixs ([03bb0fd](https://gitlab.com/fi-sas/fisas-webpage/commit/03bb0fd))



## [1.2.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.16...v1.2.0) (2020-08-18)


### Bug Fixes

* **core:** fix the screen scrool on mobile screen's ([3ea07e1](https://gitlab.com/fi-sas/fisas-webpage/commit/3ea07e1))
* **dev:** fix environment ([267b8f4](https://gitlab.com/fi-sas/fisas-webpage/commit/267b8f4))
* **shared:** move countries to shared module ([c1b81be](https://gitlab.com/fi-sas/fisas-webpage/commit/c1b81be))


### Features

* **mobility:** add initial structure ([90bee3c](https://gitlab.com/fi-sas/fisas-webpage/commit/90bee3c))
* **mobility:** add list, view and form applications ([43b7a6d](https://gitlab.com/fi-sas/fisas-webpage/commit/43b7a6d))
* **mobility:** add route detail page ([8633005](https://gitlab.com/fi-sas/fisas-webpage/commit/8633005))
* **mobility:** add route search page ([e1f17cc](https://gitlab.com/fi-sas/fisas-webpage/commit/e1f17cc))
* **shared:** add disable feature to buttons ([e58704a](https://gitlab.com/fi-sas/fisas-webpage/commit/e58704a))



### [1.1.16](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.15...v1.1.16) (2020-07-27)


### Bug Fixes

* **dashboard:** remove unsused active vars ([b99702c](https://gitlab.com/fi-sas/fisas-webpage/commit/b99702c))


### Build System

* **dev:** change dev gateway address ([8907e99](https://gitlab.com/fi-sas/fisas-webpage/commit/8907e99))
* **ipb:** update dashboard enable modules ([0695af5](https://gitlab.com/fi-sas/fisas-webpage/commit/0695af5))
* **ipb:** update dashboard enabled modules ([d8fba02](https://gitlab.com/fi-sas/fisas-webpage/commit/d8fba02))



### [1.1.15](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.14...v1.1.15) (2020-07-15)


### Bug Fixes

* **personal-data:** remove validation in identification ([28e5b5a](https://gitlab.com/fi-sas/fisas-webpage/commit/28e5b5a))



### [1.1.14](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.12...v1.1.14) (2020-07-14)


### Bug Fixes

* **accommodation:** change residence list ([b18bb1f](https://gitlab.com/fi-sas/fisas-webpage/commit/b18bb1f))
* **accommodation:** residence now appear selected ([7aae468](https://gitlab.com/fi-sas/fisas-webpage/commit/7aae468))
* **auth:** enable or disable button by institute  ([759ca4a](https://gitlab.com/fi-sas/fisas-webpage/commit/759ca4a))
* **auth:** fix cc login form ([726a4b4](https://gitlab.com/fi-sas/fisas-webpage/commit/726a4b4))
* **auth:** setTimeout added redirecting saml to AMA ([fb3f99f](https://gitlab.com/fi-sas/fisas-webpage/commit/fb3f99f))
* **environment:** fix environment name ([011162f](https://gitlab.com/fi-sas/fisas-webpage/commit/011162f))
* **environment:** merge conflict missings ([cdd0a12](https://gitlab.com/fi-sas/fisas-webpage/commit/cdd0a12))
* **pernonal-data:** remove valitation for indentification ([0089cdc](https://gitlab.com/fi-sas/fisas-webpage/commit/0089cdc))


### Build System

* **ipb:** update gateway_url ([da48a71](https://gitlab.com/fi-sas/fisas-webpage/commit/da48a71))


### Features

* **auth:** add initial development of cc login ([cf4a550](https://gitlab.com/fi-sas/fisas-webpage/commit/cf4a550))
* **auth:** change IPB gateway_url ([87a57e7](https://gitlab.com/fi-sas/fisas-webpage/commit/87a57e7))
* **auth:** gateway_url to IPB institute ([8639bd8](https://gitlab.com/fi-sas/fisas-webpage/commit/8639bd8))
* **core:** add loading on page changing ([a1a5eaf](https://gitlab.com/fi-sas/fisas-webpage/commit/a1a5eaf))



### [1.1.12](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.11...v1.1.12) (2020-07-07)


### Bug Fixes

* **accommodation:** add cancel button on queued application stat ([df341f4](https://gitlab.com/fi-sas/fisas-webpage/commit/df341f4))
* **accommodation:** show only visible extras ([a231dfa](https://gitlab.com/fi-sas/fisas-webpage/commit/a231dfa))
* **social-suport:** change text in button application change status ([dc7a6b0](https://gitlab.com/fi-sas/fisas-webpage/commit/dc7a6b0))
* **social-support:** alignment of options in the form ([d3ff9b6](https://gitlab.com/fi-sas/fisas-webpage/commit/d3ff9b6))
* **social-support:** application button activation ([8458e2a](https://gitlab.com/fi-sas/fisas-webpage/commit/8458e2a))
* **social-support:** hidden phone input in application ([f697e3a](https://gitlab.com/fi-sas/fisas-webpage/commit/f697e3a))
* **social-support:** labels to identify the information in experience ([8fcabab](https://gitlab.com/fi-sas/fisas-webpage/commit/8fcabab))
* **social-support:** show all applicaons status ([e4fbaf0](https://gitlab.com/fi-sas/fisas-webpage/commit/e4fbaf0))
* **social-support:** sort languages ([28fdf59](https://gitlab.com/fi-sas/fisas-webpage/commit/28fdf59))
* **social-support:** upper scale first letter ([ce9f7ba](https://gitlab.com/fi-sas/fisas-webpage/commit/ce9f7ba))
* **translations:** fix gender ([e80d09c](https://gitlab.com/fi-sas/fisas-webpage/commit/e80d09c))
* **volunteering:** alignment of options in the form ([71bc559](https://gitlab.com/fi-sas/fisas-webpage/commit/71bc559))
* **volunteering:** application button activation ([50f2d83](https://gitlab.com/fi-sas/fisas-webpage/commit/50f2d83))
* **volunteering:** change text in button application change status ([9ada149](https://gitlab.com/fi-sas/fisas-webpage/commit/9ada149))
* **volunteering:** hidden phone input in application ([abc7c94](https://gitlab.com/fi-sas/fisas-webpage/commit/abc7c94))
* **volunteering:** labels to identify the information in experience ([3d30bf5](https://gitlab.com/fi-sas/fisas-webpage/commit/3d30bf5))
* **volunteering:** show all applicaons status ([c34c97c](https://gitlab.com/fi-sas/fisas-webpage/commit/c34c97c))
* **volunteering:** sort languages ([3fb1ed1](https://gitlab.com/fi-sas/fisas-webpage/commit/3fb1ed1))
* **volunteering:** upper scale first letter ([fa4981e](https://gitlab.com/fi-sas/fisas-webpage/commit/fa4981e))



### [1.1.11](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.10...v1.1.11) (2020-06-29)


### Features

* **accommodation:** add household_distance and out_of_date fields ([38a294c](https://gitlab.com/fi-sas/fisas-webpage/commit/38a294c))



### [1.1.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.10...v1.1.10) (2020-06-25)



### [0.1.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.9...v0.1.10) (2020-06-25)


### Bug Fixes

* **accommodation:** fix translation error ([310c0b3](https://gitlab.com/fi-sas/fisas-webpage/commit/310c0b3))
* **assets:** fix icon for volunteering ([ad15ec3](https://gitlab.com/fi-sas/fisas-webpage/commit/ad15ec3))
* **dashboard:** remove inative modules ([a1547de](https://gitlab.com/fi-sas/fisas-webpage/commit/a1547de))


### Build System

* **ipb:** add ipb to ci/cd ([658b3ac](https://gitlab.com/fi-sas/fisas-webpage/commit/658b3ac))


### Features

* **dashborad:** hidden modules incactives ([beaae3c](https://gitlab.com/fi-sas/fisas-webpage/commit/beaae3c))



### [1.1.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.8...v1.1.9) (2020-06-22)


### Bug Fixes

* **social-support:** accept application ([b2411b7](https://gitlab.com/fi-sas/fisas-webpage/commit/b2411b7))
* **u-bike:** add new inputs in application form ([595602d](https://gitlab.com/fi-sas/fisas-webpage/commit/595602d))



### [1.1.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.7...v1.1.8) (2020-06-08)


### Bug Fixes

* **accommodation:** create application conditions ([f3b5977](https://gitlab.com/fi-sas/fisas-webpage/commit/f3b5977))



### [1.1.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.6...v1.1.7) (2020-06-06)


### Bug Fixes

* **core:** fix mobile header ([a6d538a](https://gitlab.com/fi-sas/fisas-webpage/commit/a6d538a))
* **private-accomodation:** fix layout issues ([701593b](https://gitlab.com/fi-sas/fisas-webpage/commit/701593b))


### Features

* **accmmoodation:** change payment_method by residence ([c698920](https://gitlab.com/fi-sas/fisas-webpage/commit/c698920))
* **accommodation:** add alternative residence ([1bccf47](https://gitlab.com/fi-sas/fisas-webpage/commit/1bccf47))
* **accommodation:** add extras field to accommodation application form ([93ceb57](https://gitlab.com/fi-sas/fisas-webpage/commit/93ceb57))
* **accommodation:** add new fields to application ([9fe3f32](https://gitlab.com/fi-sas/fisas-webpage/commit/9fe3f32))
* **social-support:** add application change status and fix issues ([270f491](https://gitlab.com/fi-sas/fisas-webpage/commit/270f491))
* **social-support:** monthly report page ([e62710c](https://gitlab.com/fi-sas/fisas-webpage/commit/e62710c))
* **volunteering:** create module ([f784fe9](https://gitlab.com/fi-sas/fisas-webpage/commit/f784fe9))



### [1.1.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.5...v1.1.6) (2020-05-15)


### Features

* **social-support:** application-status and history ([b252083](https://gitlab.com/fi-sas/fisas-webpage/commit/b252083))



### [1.1.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.4...v1.1.5) (2020-05-07)


### Bug Fixes

* **attendaces:** add new limit in list ([5685dcd](https://gitlab.com/fi-sas/fisas-webpage/commit/5685dcd))
* **list-expericience:** fix issues in experience list ([1676ae4](https://gitlab.com/fi-sas/fisas-webpage/commit/1676ae4))
* **social-support:** fix application errors ([1894259](https://gitlab.com/fi-sas/fisas-webpage/commit/1894259))
* **social-support:** fix submit application ([9e15af2](https://gitlab.com/fi-sas/fisas-webpage/commit/9e15af2))
* **webpage:** text corrections ([0f38d7a](https://gitlab.com/fi-sas/fisas-webpage/commit/0f38d7a))


### Features

* **experience-detail:** page to view experience detail ([2cc5792](https://gitlab.com/fi-sas/fisas-webpage/commit/2cc5792))



### [1.1.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.3...v1.1.4) (2020-05-05)


### Bug Fixes

* **core:** fix recursive balances request ([6e8a49a](https://gitlab.com/fi-sas/fisas-webpage/commit/6e8a49a))


### Build System

* **nginx:** add cache to every static files ([3d2ef8d](https://gitlab.com/fi-sas/fisas-webpage/commit/3d2ef8d))
* **nginx:** fix nginx conf file ([e46554d](https://gitlab.com/fi-sas/fisas-webpage/commit/e46554d))



### [1.1.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.2...v1.1.3) (2020-05-05)


### Bug Fixes

* **justification-absences:** add justification service ([fe93157](https://gitlab.com/fi-sas/fisas-webpage/commit/fe93157))
* **listings:** fix the issues in form to create listings ([46a56d3](https://gitlab.com/fi-sas/fisas-webpage/commit/46a56d3))
* **private accomodation:** fix the form to create owner ([68ba556](https://gitlab.com/fi-sas/fisas-webpage/commit/68ba556))
* **social-support:** add new code and merge ([1121f94](https://gitlab.com/fi-sas/fisas-webpage/commit/1121f94))
* **social-support:** fix reviews and form issues ([09d5445](https://gitlab.com/fi-sas/fisas-webpage/commit/09d5445))


### Build System

* **nginx:** add nginx configs compression and cache ([deb59d5](https://gitlab.com/fi-sas/fisas-webpage/commit/deb59d5))


### Features

* **record-attendance:** forms and list for attendance ([a1ac32f](https://gitlab.com/fi-sas/fisas-webpage/commit/a1ac32f))
* **socail-support:** application ([44a9837](https://gitlab.com/fi-sas/fisas-webpage/commit/44a9837))
* **social-support:** application form and review ([267314e](https://gitlab.com/fi-sas/fisas-webpage/commit/267314e))



### [1.1.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.1...v1.1.2) (2020-03-17)


### Build System

* **ipvc:** add api prefix to ipvc endpoints ([b3ac5bc](https://gitlab.com/fi-sas/fisas-webpage/commit/b3ac5bc))



### [1.1.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.1.0...v1.1.1) (2020-03-16)


### Build System

* **envs:** update envs of project ([4a45a6d](https://gitlab.com/fi-sas/fisas-webpage/commit/4a45a6d))



## [1.1.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.10...v1.1.0) (2020-03-13)


### Features

* **current-account:** add list current accounts and movements detail ([bac7ada](https://gitlab.com/fi-sas/fisas-webpage/commit/bac7ada))
* **current:account:** add item in menu ([ac3cfab](https://gitlab.com/fi-sas/fisas-webpage/commit/ac3cfab))



### [1.0.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.9...v1.0.10) (2020-03-06)


### Bug Fixes

* **u-bike and change pin:** fix u-bike issues and change pin form ([e98acc0](https://gitlab.com/fi-sas/fisas-webpage/commit/e98acc0))



### [1.0.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.8...v1.0.9) (2020-02-27)


### Bug Fixes

* **auth:** fix update user data on token validation ([198a704](https://gitlab.com/fi-sas/fisas-webpage/commit/198a704))
* **change-pin:** change method to reset pin ([c8f1e33](https://gitlab.com/fi-sas/fisas-webpage/commit/c8f1e33))
* **core:** add back to login option ([c77f774](https://gitlab.com/fi-sas/fisas-webpage/commit/c77f774))
* **docker-compose:** add always restart ([7f6d383](https://gitlab.com/fi-sas/fisas-webpage/commit/7f6d383))
* **enviroments:** disable private accomodation in prodution ([e47f8c3](https://gitlab.com/fi-sas/fisas-webpage/commit/e47f8c3))
* **paccomodation:** resulucao de traducoes ([f497b21](https://gitlab.com/fi-sas/fisas-webpage/commit/f497b21))
* **private_accommodation:** minor fixes ([e4ed0a9](https://gitlab.com/fi-sas/fisas-webpage/commit/e4ed0a9))
* **private_accommodation:** minor fixes ([f69390b](https://gitlab.com/fi-sas/fisas-webpage/commit/f69390b))
* **private_accommodation:** remove login of private accomodation ([c3e8f35](https://gitlab.com/fi-sas/fisas-webpage/commit/c3e8f35))
* **routing:** remove new pin in public route ([cee5782](https://gitlab.com/fi-sas/fisas-webpage/commit/cee5782))
* **side menu:** fix issue ([4533783](https://gitlab.com/fi-sas/fisas-webpage/commit/4533783))
* **ubike:** application info unclosed divs ([1db54bd](https://gitlab.com/fi-sas/fisas-webpage/commit/1db54bd))


### Build System

* **paccommodation:** updated ([2558b5b](https://gitlab.com/fi-sas/fisas-webpage/commit/2558b5b))
* **paccommodation:** updated ([35fdb1c](https://gitlab.com/fi-sas/fisas-webpage/commit/35fdb1c))


### Features

* **app:** new version paccommodation ([33411f3](https://gitlab.com/fi-sas/fisas-webpage/commit/33411f3))
* **auth:** add public/guest pages access ([bd87d49](https://gitlab.com/fi-sas/fisas-webpage/commit/bd87d49))
* **core:** add Auth for public services ([fab77e1](https://gitlab.com/fi-sas/fisas-webpage/commit/fab77e1))
* **core:** creation of unauthorizaed page ([89503d0](https://gitlab.com/fi-sas/fisas-webpage/commit/89503d0))
* **private-accomodation:** merge to develop ([bb5289a](https://gitlab.com/fi-sas/fisas-webpage/commit/bb5289a))



### [1.0.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.7...v1.0.8) (2020-02-10)


### Bug Fixes

* **reset-pin:** remove attribute in html ([f7ca0fb](https://gitlab.com/fi-sas/fisas-webpage/commit/f7ca0fb))



### [1.0.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.6...v1.0.7) (2020-02-05)


### Features

* **pin:** reset and change pin ([b43a5fd](https://gitlab.com/fi-sas/fisas-webpage/commit/b43a5fd))



### [1.0.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.5...v1.0.6) (2020-01-30)


### Bug Fixes

* **reservations:** add new cancel reservation method ([b091330](https://gitlab.com/fi-sas/fisas-webpage/commit/b091330))


### Features

* **change-pin:** change pin ([162ba47](https://gitlab.com/fi-sas/fisas-webpage/commit/162ba47))



### [1.0.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.4...v1.0.5) (2020-01-17)


### Bug Fixes

* **auth:** fix typo ([0bbf574](https://gitlab.com/fi-sas/fisas-webpage/commit/0bbf574))
* **core:** force to reload translation on new version ([d2cd6c4](https://gitlab.com/fi-sas/fisas-webpage/commit/d2cd6c4))
* **ubike:** application button disabled condition ([37703a3](https://gitlab.com/fi-sas/fisas-webpage/commit/37703a3))
* **ubike:** disable temporarily request maintenance button ([004bf02](https://gitlab.com/fi-sas/fisas-webpage/commit/004bf02))
* **ubike:** homepage apresentation styling ([dd51099](https://gitlab.com/fi-sas/fisas-webpage/commit/dd51099))


### Features

* **auth:** check if user as required data on profile ([fee37d2](https://gitlab.com/fi-sas/fisas-webpage/commit/fee37d2))



### [1.0.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.3...v1.0.4) (2020-01-13)


### Bug Fixes

* **u_bike:** fix date input in application ([f7f7b4e](https://gitlab.com/fi-sas/fisas-webpage/commit/f7f7b4e))


### Build System

* **ubike:** enable ubike on ipvc ([54bed11](https://gitlab.com/fi-sas/fisas-webpage/commit/54bed11))


### Features

* **alimentation:** add cancel reservations feature ([1a271f5](https://gitlab.com/fi-sas/fisas-webpage/commit/1a271f5))



### [1.0.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.2...v1.0.3) (2019-12-23)


### Bug Fixes

* **app:** change ng-zorro imports, mainjs size reduction ([09ed788](https://gitlab.com/fi-sas/fisas-webpage/commit/09ed788))
* **ubike:** correction of application states and translations ([43379b2](https://gitlab.com/fi-sas/fisas-webpage/commit/43379b2))
* **ubike:** modify translations to 3rd persons singular ([414d2b8](https://gitlab.com/fi-sas/fisas-webpage/commit/414d2b8))


### Features

* **ubike:** add field total_burned_calories to DashboardModel model ([d0f8d6a](https://gitlab.com/fi-sas/fisas-webpage/commit/d0f8d6a))
* **ubike:** add total_burned_calories to dashboard card ([6ae991d](https://gitlab.com/fi-sas/fisas-webpage/commit/6ae991d))
* **ubike:** dashboard change service ([c1f5b52](https://gitlab.com/fi-sas/fisas-webpage/commit/c1f5b52))
* **ubike:** dashboard style basic indicators ([9f7899c](https://gitlab.com/fi-sas/fisas-webpage/commit/9f7899c))
* **ubike:** dashboard style other indicators ([2f04bf2](https://gitlab.com/fi-sas/fisas-webpage/commit/2f04bf2))
* **ubike:** dashboard units convert and style ([3618bb8](https://gitlab.com/fi-sas/fisas-webpage/commit/3618bb8))



### [1.0.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v1.0.0...v1.0.2) (2019-11-08)


### Bug Fixes

* **auth:** fix SSO first_login validation ([0c92bd8](https://gitlab.com/fi-sas/fisas-webpage/commit/0c92bd8))



## [1.0.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.5.3...v1.0.0) (2019-11-07)


### Bug Fixes

* **ubike:** change fields on ubike application ([4515769](https://gitlab.com/fi-sas/fisas-webpage/commit/4515769))



### [0.5.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.5.2...v0.5.3) (2019-10-31)


### Bug Fixes

* **accommodation:** fix scholarship texts ([1d1394b](https://gitlab.com/fi-sas/fisas-webpage/commit/1d1394b))
* **sidebar:** fix options in sidebar ([09670eb](https://gitlab.com/fi-sas/fisas-webpage/commit/09670eb))


### Features

* **form_steps:** add modal box to confirm submit and update ([9233ab9](https://gitlab.com/fi-sas/fisas-webpage/commit/9233ab9))



### [0.5.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.5.1...v0.5.2) (2019-10-30)


### Build System

* **ipvc:** add new endpiont to the ipvc build ([4923669](https://gitlab.com/fi-sas/fisas-webpage/commit/4923669))



### [0.5.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.5.0...v0.5.1) (2019-10-30)


### Bug Fixes

* **accommodation:** remove applicationData ([375ec72](https://gitlab.com/fi-sas/fisas-webpage/commit/375ec72))



## [0.5.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.10...v0.5.0) (2019-10-30)


### Bug Fixes

* **app:** on first login invalid form ([85342cf](https://gitlab.com/fi-sas/fisas-webpage/commit/85342cf))


### Features

* **accomodation:** new version ([a0eb397](https://gitlab.com/fi-sas/fisas-webpage/commit/a0eb397))
* **app:** add sso error page ([775dd30](https://gitlab.com/fi-sas/fisas-webpage/commit/775dd30))



### [0.4.10](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.9...v0.4.10) (2019-10-17)


### Bug Fixes

* **alimentation:** fix images ans services ([cbec670](https://gitlab.com/fi-sas/fisas-webpage/commit/cbec670))
* **app:** on SSO enable try to login with idp always ([625b03c](https://gitlab.com/fi-sas/fisas-webpage/commit/625b03c))
* **social-support:** add bar menu and fix list ([82bc205](https://gitlab.com/fi-sas/fisas-webpage/commit/82bc205))
* **u-bike:** delete trip ([82cf050](https://gitlab.com/fi-sas/fisas-webpage/commit/82cf050))


### Features

* **profile:** add personal data form ([53e9864](https://gitlab.com/fi-sas/fisas-webpage/commit/53e9864))



### [0.4.9](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.8...v0.4.9) (2019-10-04)



### [0.4.8](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.7...v0.4.8) (2019-10-04)



### [0.4.7](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.6...v0.4.7) (2019-10-04)


### Bug Fixes

* **core:** header name of institute ([0890e91](https://gitlab.com/fi-sas/fisas-webpage/commit/0890e91))



### [0.4.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.5...v0.4.6) (2019-10-03)


### Bug Fixes

* **accommodation:** fix application view ([af484a1](https://gitlab.com/fi-sas/fisas-webpage/commit/af484a1))


### Features

* **u-bike:** maintenance and manual registration ([74b0fe1](https://gitlab.com/fi-sas/fisas-webpage/commit/74b0fe1))



### [0.4.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.4...v0.4.5) (2019-10-03)


### Bug Fixes

* **assets:** fix languages ([6345e76](https://gitlab.com/fi-sas/fisas-webpage/commit/6345e76))
* **login:** fix login sso back button ([bb76640](https://gitlab.com/fi-sas/fisas-webpage/commit/bb76640))
* **login:** on erro dont open the password field ([0dc70da](https://gitlab.com/fi-sas/fisas-webpage/commit/0dc70da))


### Build System

* **ipca:** add ipca environment ([ec08172](https://gitlab.com/fi-sas/fisas-webpage/commit/ec08172))


### Features

* **accommodation:** list residences and list applications pages ([131fa48](https://gitlab.com/fi-sas/fisas-webpage/commit/131fa48))
* **application-view:** creation of application view page ([e5af8c8](https://gitlab.com/fi-sas/fisas-webpage/commit/e5af8c8))
* **u-bike:** dashboard ([e32b525](https://gitlab.com/fi-sas/fisas-webpage/commit/e32b525))



### [0.4.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.4...v0.4.5) (2019-10-03)


### Bug Fixes

* **assets:** fix languages ([6345e76](https://gitlab.com/fi-sas/fisas-webpage/commit/6345e76))
* **login:** fix login sso back button ([bb76640](https://gitlab.com/fi-sas/fisas-webpage/commit/bb76640))
* **login:** on erro dont open the password field ([0dc70da](https://gitlab.com/fi-sas/fisas-webpage/commit/0dc70da))


### Features

* **accommodation:** list residences and list applications pages ([131fa48](https://gitlab.com/fi-sas/fisas-webpage/commit/131fa48))
* **application-view:** creation of application view page ([e5af8c8](https://gitlab.com/fi-sas/fisas-webpage/commit/e5af8c8))
* **u-bike:** dashboard ([e32b525](https://gitlab.com/fi-sas/fisas-webpage/commit/e32b525))



### [0.4.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.3...v0.4.4) (2019-09-27)


### Build System

* **webpage:** change ng memory limit ([34a50cf](https://gitlab.com/fi-sas/fisas-webpage/commit/34a50cf))



### [0.4.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.2...v0.4.3) (2019-09-27)


### Bug Fixes

* **core:** fix unamed var on mobile menu ([c54680e](https://gitlab.com/fi-sas/fisas-webpage/commit/c54680e))



### [0.4.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.1...v0.4.2) (2019-09-26)


### Bug Fixes

* **shared:** initialize variable on module ([4d6e576](https://gitlab.com/fi-sas/fisas-webpage/commit/4d6e576))



### [0.4.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.4.0...v0.4.1) (2019-09-26)


### Bug Fixes

* **webpage:** fix config import ([502d078](https://gitlab.com/fi-sas/fisas-webpage/commit/502d078))



## [0.4.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.6...v0.4.0) (2019-09-26)


### Bug Fixes

* **core:** add language to the slide menu ([323d869](https://gitlab.com/fi-sas/fisas-webpage/commit/323d869))
* **core:** drawer menu get language option ([361fd9d](https://gitlab.com/fi-sas/fisas-webpage/commit/361fd9d))
* **core:** fixes on layout ([69bb615](https://gitlab.com/fi-sas/fisas-webpage/commit/69bb615))
* **dashbord:** ajust design ([c239a98](https://gitlab.com/fi-sas/fisas-webpage/commit/c239a98))
* **shared:** add layout color to the buttons and types ([cdb7595](https://gitlab.com/fi-sas/fisas-webpage/commit/cdb7595))
* **themes:** fix themes pallets ([5dee3c7](https://gitlab.com/fi-sas/fisas-webpage/commit/5dee3c7))
* **u-bike:** applications ([15ced73](https://gitlab.com/fi-sas/fisas-webpage/commit/15ced73))
* **u-bike:** fix application apply ([0016f04](https://gitlab.com/fi-sas/fisas-webpage/commit/0016f04))
* **u-bike:** fix applications ([ef81e16](https://gitlab.com/fi-sas/fisas-webpage/commit/ef81e16))
* **u-bike:** fix bugs ([26d8ec3](https://gitlab.com/fi-sas/fisas-webpage/commit/26d8ec3))


### Build System

* **app:** add institute to environment ([79bb485](https://gitlab.com/fi-sas/fisas-webpage/commit/79bb485))


### Features

* **auth:** add new layout to the login page ([f3dcc83](https://gitlab.com/fi-sas/fisas-webpage/commit/f3dcc83))
* **auth:** new auth layout and process ([9b25fc6](https://gitlab.com/fi-sas/fisas-webpage/commit/9b25fc6))
* **social-support:** experiences ([ef4a7ab](https://gitlab.com/fi-sas/fisas-webpage/commit/ef4a7ab))
* **u-bike:** create applications and review ([d2768e5](https://gitlab.com/fi-sas/fisas-webpage/commit/d2768e5))
* **webpage:** modules activation by institute ([fe979cc](https://gitlab.com/fi-sas/fisas-webpage/commit/fe979cc))


### Tests

* **shared:** delete translation pipe ([73973ec](https://gitlab.com/fi-sas/fisas-webpage/commit/73973ec))



### [0.2.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.5...v0.2.6) (2019-08-09)


### Bug Fixes

* **app:** fix version checker url ([2f00b7f](https://gitlab.com/fi-sas/fisas-webpage/commit/2f00b7f))



### [0.2.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.4...v0.2.5) (2019-08-09)


### Bug Fixes

* **accommodation:** update application form ([495b3de](https://gitlab.com/fi-sas/fisas-webpage/commit/495b3de))


### Features

* **webpage:** add translations ([4791d44](https://gitlab.com/fi-sas/fisas-webpage/commit/4791d44))


### Tests

* **core:** test of version checker ([3cad72a](https://gitlab.com/fi-sas/fisas-webpage/commit/3cad72a))



### [0.2.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.3...v0.2.4) (2019-08-08)


### Bug Fixes

* **header:** fix position ([17ca920](https://gitlab.com/fi-sas/fisas-webpage/commit/17ca920))


### Features

* **accommodation:** add new verification of  application available ([ab36540](https://gitlab.com/fi-sas/fisas-webpage/commit/ab36540))
* **core:** add version checker to the webpage ([a723209](https://gitlab.com/fi-sas/fisas-webpage/commit/a723209))



### [0.2.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.2...v0.2.3) (2019-08-07)



### [0.2.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.1...v0.2.2) (2019-08-07)


### Bug Fixes

* **accommodation:** add cource ([394ffe0](https://gitlab.com/fi-sas/fisas-webpage/commit/394ffe0))
* **sidebar:** fix options ([2bcc7f7](https://gitlab.com/fi-sas/fisas-webpage/commit/2bcc7f7))
* **sidebar:** fix test file ([ada53ae](https://gitlab.com/fi-sas/fisas-webpage/commit/ada53ae))


### Features

* **shared:** add modal box for system error ([7b1444e](https://gitlab.com/fi-sas/fisas-webpage/commit/7b1444e))
* **webpage:** New sidebar, header and footer ([c8a5b42](https://gitlab.com/fi-sas/fisas-webpage/commit/c8a5b42))


### Tests

* **core:** add imports ([a4a4780](https://gitlab.com/fi-sas/fisas-webpage/commit/a4a4780))
* **core:** fix core ([b791142](https://gitlab.com/fi-sas/fisas-webpage/commit/b791142))
* **core:** remove imports ([48e8129](https://gitlab.com/fi-sas/fisas-webpage/commit/48e8129))



### [0.2.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.2.0...v0.2.1) (2019-07-27)


### Bug Fixes

* **accommodation:** fix confirm reject options for student ([635904a](https://gitlab.com/fi-sas/fisas-webpage/commit/635904a))


### Features

* **alimentation:** list reservations ([23a7340](https://gitlab.com/fi-sas/fisas-webpage/commit/23a7340))
* **alimentation:** user allergens  ([649e233](https://gitlab.com/fi-sas/fisas-webpage/commit/649e233))



## [0.2.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.5...v0.2.0) (2019-07-22)


### Bug Fixes

* **accommodation:** add draft status to post request application ([646a172](https://gitlab.com/fi-sas/fisas-webpage/commit/646a172))
* **user-header:** add balances and new design ([cd14ee4](https://gitlab.com/fi-sas/fisas-webpage/commit/cd14ee4))


### Build System

* **antd:** update antd version and minor fixes ([8fb3698](https://gitlab.com/fi-sas/fisas-webpage/commit/8fb3698))
* **index:** add no-cache meta tags to index ([e65d14a](https://gitlab.com/fi-sas/fisas-webpage/commit/e65d14a))


### Features

* **alimentation:** dish detail ([24e494a](https://gitlab.com/fi-sas/fisas-webpage/commit/24e494a))



### [0.1.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.4...v0.1.5) (2019-07-15)


### Bug Fixes

* **accommodation:** update layout ([e53a710](https://gitlab.com/fi-sas/fisas-webpage/commit/e53a710))
* **accommodation:** viewing attachments ([487c79f](https://gitlab.com/fi-sas/fisas-webpage/commit/487c79f))


### Features

* **accommodation:** add parents profession fields ([07af978](https://gitlab.com/fi-sas/fisas-webpage/commit/07af978))
* **alimentation:** list dishs ([4ba721d](https://gitlab.com/fi-sas/fisas-webpage/commit/4ba721d))
* **alimentation:** purchase tickets  ([2ac3a81](https://gitlab.com/fi-sas/fisas-webpage/commit/2ac3a81))
* **shared:** add file upload component ([ed53320](https://gitlab.com/fi-sas/fisas-webpage/commit/ed53320))



### [0.1.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.3...v0.1.4) (2019-06-24)


### Bug Fixes

* **accommodation:** add academic_year to application form ([902a99d](https://gitlab.com/fi-sas/fisas-webpage/commit/902a99d))
* **accommodation:** fix extension date ([0c0eb52](https://gitlab.com/fi-sas/fisas-webpage/commit/0c0eb52))
* **accommodation:** fix sidebar ([a25a373](https://gitlab.com/fi-sas/fisas-webpage/commit/a25a373))
* **accommodation:** minor fix ([59dfc9d](https://gitlab.com/fi-sas/fisas-webpage/commit/59dfc9d))
* **accommodation-status:** add actions to user ([2fa1a0b](https://gitlab.com/fi-sas/fisas-webpage/commit/2fa1a0b))
* **view-accommodation:** disable route ([a2346b2](https://gitlab.com/fi-sas/fisas-webpage/commit/a2346b2))


### Features

* **accommodation:** create extension form ([4d72094](https://gitlab.com/fi-sas/fisas-webpage/commit/4d72094))
* **accommodation:** create list and form for absences ([b796330](https://gitlab.com/fi-sas/fisas-webpage/commit/b796330))
* **accommodation:** create list and form for withdrawals ([a81c701](https://gitlab.com/fi-sas/fisas-webpage/commit/a81c701))
* **accommodation:** create list extension ([5ea44b9](https://gitlab.com/fi-sas/fisas-webpage/commit/5ea44b9))
* **accommodation:** services and models for abences and withdrawals ([24a4e69](https://gitlab.com/fi-sas/fisas-webpage/commit/24a4e69))



### [0.1.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.2...v0.1.3) (2019-06-04)


### Bug Fixes

* **accommodation:** fix errors ([83b9c55](https://gitlab.com/fi-sas/fisas-webpage/commit/83b9c55))
* **accommodation:** fix errors in application form ([b38a399](https://gitlab.com/fi-sas/fisas-webpage/commit/b38a399))



### [0.1.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.1...v0.1.2) (2019-06-04)


### Bug Fixes

* **applications:** fix application form ([cd21e98](https://gitlab.com/fi-sas/fisas-webpage/commit/cd21e98))



### [0.1.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.1.0...v0.1.1) (2019-06-04)


### Bug Fixes

* **accommodation:** minor fixes ([7bdd08f](https://gitlab.com/fi-sas/fisas-webpage/commit/7bdd08f))



## [0.1.0](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.6...v0.1.0) (2019-06-04)


### Bug Fixes

* **accommodation:** payments methods ([c3a40e0](https://gitlab.com/fi-sas/fisas-webpage/commit/c3a40e0))
* **accommodation:** remove buttons ([d9a2493](https://gitlab.com/fi-sas/fisas-webpage/commit/d9a2493))
* **accomodation:** fix issues ([6e5f201](https://gitlab.com/fi-sas/fisas-webpage/commit/6e5f201))
* **webpage:** minor fixes ([e9e5f7d](https://gitlab.com/fi-sas/fisas-webpage/commit/e9e5f7d))


### Features

* **accomodation:** view application status ([0eadd3a](https://gitlab.com/fi-sas/fisas-webpage/commit/0eadd3a))



### [0.0.6](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.5...v0.0.6) (2019-06-03)



### [0.0.5](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.4...v0.0.5) (2019-06-03)



### [0.0.4](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.3...v0.0.4) (2019-06-03)



### [0.0.3](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.2...v0.0.3) (2019-06-03)



### [0.0.2](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.1...v0.0.2) (2019-06-03)



### [0.0.1](https://gitlab.com/fi-sas/fisas-webpage/compare/v0.0.0...v0.0.1) (2019-06-03)



## 0.0.0 (2019-05-31)


### Bug Fixes

* **accommodation:** fix application form ([fa25dd7](https://gitlab.com/fi-sas/fisas-webpage/commit/fa25dd7))
* **accomodation:** add new features in form ([827a786](https://gitlab.com/fi-sas/fisas-webpage/commit/827a786))
* **accomodation:** fix application ([9fea5dd](https://gitlab.com/fi-sas/fisas-webpage/commit/9fea5dd))
* **accomodation:** fix errors in test ([e6ef071](https://gitlab.com/fi-sas/fisas-webpage/commit/e6ef071))
* **accomodation:** fix errors test ([5b97823](https://gitlab.com/fi-sas/fisas-webpage/commit/5b97823))
* **accomodation:** issues fixed ([5faa0bb](https://gitlab.com/fi-sas/fisas-webpage/commit/5faa0bb))
* **login:** add button to auth with gov ([66cdedb](https://gitlab.com/fi-sas/fisas-webpage/commit/66cdedb))


### Features

* **accommodation:** add translation to form application ([5dfcbb5](https://gitlab.com/fi-sas/fisas-webpage/commit/5dfcbb5))
* **accommodation:** create models and services ([2abfbc5](https://gitlab.com/fi-sas/fisas-webpage/commit/2abfbc5))
* **accommodation:** create of form ([11fc6b7](https://gitlab.com/fi-sas/fisas-webpage/commit/11fc6b7))
* **accommodation:** creation of accommodation module ([5a4fc0a](https://gitlab.com/fi-sas/fisas-webpage/commit/5a4fc0a))
* **accommodation:** creation of application serice ([a850e2e](https://gitlab.com/fi-sas/fisas-webpage/commit/a850e2e))
* **accommodation:** creation of form application page ([71f5b81](https://gitlab.com/fi-sas/fisas-webpage/commit/71f5b81))
* **accommodation:** form applications final page success application ([76e974e](https://gitlab.com/fi-sas/fisas-webpage/commit/76e974e))
* **accomodation:** select residence form ([a3076bf](https://gitlab.com/fi-sas/fisas-webpage/commit/a3076bf))
* **auth:** creation of auth service ([b0863c9](https://gitlab.com/fi-sas/fisas-webpage/commit/b0863c9))
* **core:** add translation to slide menu ([d81bca2](https://gitlab.com/fi-sas/fisas-webpage/commit/d81bca2))
* **core:** creation of auth template (Login layout) ([649cb87](https://gitlab.com/fi-sas/fisas-webpage/commit/649cb87))
* **core:** creation of breadcrumb component ([7dd6a75](https://gitlab.com/fi-sas/fisas-webpage/commit/7dd6a75))
* **core:** creation of core module ([c879e7d](https://gitlab.com/fi-sas/fisas-webpage/commit/c879e7d))
* **core:** creation of full template (Main layout) ([bdfb05d](https://gitlab.com/fi-sas/fisas-webpage/commit/bdfb05d))
* **core:** creation of header component ([2219533](https://gitlab.com/fi-sas/fisas-webpage/commit/2219533))
* **core:** creation of slide component ([3be14c3](https://gitlab.com/fi-sas/fisas-webpage/commit/3be14c3))
* **core:** creation of ui service ([eb6486c](https://gitlab.com/fi-sas/fisas-webpage/commit/eb6486c))
* **core:** creation of user header component ([ddc96b3](https://gitlab.com/fi-sas/fisas-webpage/commit/ddc96b3))
* **errors:** show errors for user ([9231b10](https://gitlab.com/fi-sas/fisas-webpage/commit/9231b10))
* **login:** creation of login page ([df0ddc5](https://gitlab.com/fi-sas/fisas-webpage/commit/df0ddc5))
* **pages:** add page not found ([ac8c7b2](https://gitlab.com/fi-sas/fisas-webpage/commit/ac8c7b2))
* **shared:** add token interceptor ([4fb60df](https://gitlab.com/fi-sas/fisas-webpage/commit/4fb60df))
* **shared:** creation on shared module ([8eb821f](https://gitlab.com/fi-sas/fisas-webpage/commit/8eb821f))
* **spinner:** add loading spinner ([8771c55](https://gitlab.com/fi-sas/fisas-webpage/commit/8771c55))
* **style:** add styling to web page ([90d6759](https://gitlab.com/fi-sas/fisas-webpage/commit/90d6759))
* **styling:** add custom style to antd ([8ff6d1c](https://gitlab.com/fi-sas/fisas-webpage/commit/8ff6d1c))
* **translation:** add translation to webpage ([298139e](https://gitlab.com/fi-sas/fisas-webpage/commit/298139e))


### Tests

* **accommodation:** add translation test to form application ([a2a5aaf](https://gitlab.com/fi-sas/fisas-webpage/commit/a2a5aaf))
* **dashboard:** add dashboard tests imports ([1dad00f](https://gitlab.com/fi-sas/fisas-webpage/commit/1dad00f))
* **shared:** add import HttpClientModule ([71ebf5c](https://gitlab.com/fi-sas/fisas-webpage/commit/71ebf5c))
* **translation:** add translations to tests ([332ff65](https://gitlab.com/fi-sas/fisas-webpage/commit/332ff65))
* **webpage:** add imports to tests ([4f79ffa](https://gitlab.com/fi-sas/fisas-webpage/commit/4f79ffa))
