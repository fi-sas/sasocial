import { async, TestBed } from '@angular/core/testing';
import { FiConfiguratorModule } from './configurator.module';

describe('ConfiguratorModule', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiConfiguratorModule]
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(FiConfiguratorModule).toBeDefined();
  });
});
