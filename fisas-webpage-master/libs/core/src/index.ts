export * from './lib/core.module';
export * from './lib/state/store';
export * from './lib/state/types';
export * from './lib/state/action';

export { FiUrlService, DomainHostType } from './lib/url.service';
export {
  FiResourceService,
  ResourceError,
  HttpOptions,
  Resource,
  Link
} from './lib/resource.service';
export { FiStorageService } from './lib/storage.service';
