import { async, TestBed } from '@angular/core/testing';
import { FiCoreModule } from './core.module';

describe('CoreModule', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiCoreModule]
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(FiCoreModule).toBeDefined();
  });
});
