import { async, TestBed } from '@angular/core/testing';
import { FiShareModule } from './share.module';

describe('ShareModule', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiShareModule]
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(FiShareModule).toBeDefined();
  });
});
