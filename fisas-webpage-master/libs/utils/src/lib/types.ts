export interface GenericType {
  [key: string]: any;
}

export interface ErrorDetails {
  [key: string]: any;
}

/**/
