import { PrivateGuard } from "./auth/guards/private.guard";
import { PublicGuard } from "./auth/guards/public.guard";
import { FullTemplateComponent } from "./core/templates/full-template/full-template.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { AppResolver } from "./app.resolver";

const routes: Routes = [
  {
    path: "auth",
    component: FullTemplateComponent,
    resolve: [AppResolver],
    children: [
      {
        path: "login",
        loadChildren: "src/app/pages/login/login.module#LoginModule",
        data: { breadcrumb: "LOGIN.AUTHENTICATION" },
      },
      {
        path: "reset-password",
        loadChildren:
          "src/app/pages/reset-password/reset-password.module#ResetPasswordModule",
        data: { breadcrumb: "RESET_PASSWORD.TITLE" },
      },
      {
        path: "new-password",
        loadChildren:
          "src/app/pages/new-password/new-password.module#NewPasswordModule",
        data: { breadcrumb: "CHANGE_PASSWORD.NEW_PASSWORD" },
      },
      {
        path: "new-verification-code",
        loadChildren:
          "src/app/pages/new-verification-code/new-verification-code.module#NewVerificationCodeModule",
        data: { breadcrumb: "NEW_VERIFICATION.TITLE" },
      },
      {
        path: "ssoError",
        loadChildren: "src/app/pages/sso-error/sso-error.module#SsoErrorModule",
        data: { breadcrumb: "SSO_ERROR.BREADCRUMB" },
      },
      { path: "", redirectTo: "login", pathMatch: "full" },
    ],
    canActivate: [PublicGuard],
  },
  {
    path: "",
    component: FullTemplateComponent,
    resolve: [AppResolver],
    children: [
      {
        path: "privateaccommodation",
        loadChildren:
          "./modules/private-accommodation/private-accommodation.module#PrivateAccommodationModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.PRIVATEACCOMMODATION" },
      },
      {
        path: "calendar",
        loadChildren:
          "./modules/calendar/calendar.module#CalendarModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.CALENDAR" },
      },
      {
      path: "volunteering",
      loadChildren:"./modules/volunteering/volunteering.module#VolunteeringModule",
      data: { breadcrumb: "DASHBOARD.BUTTON.VOLUNTEERING" },
      },
      {
        path: "dashboard",
        loadChildren:
          "src/app/pages/dashboard/dashboard.module#DashboardModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.SERVICES" },
      },
      {
        path: "news",
        loadChildren: "./modules/news/news.module#NewsModule",
        data: { breadcrumb: "DASHBOARD.WIDGETS.POSTS.TITLE" },
      },
      {
        path: "account-verification",
        loadChildren:
          "src/app/pages/account-verification/account-verification.module#AccountVerificationModule",
        data: { breadcrumb: "ACCOUNT_VERIFICATION.TITLE" },
      },
      {
        path: "unauthorized",
        loadChildren:
          "src/app/pages/unauthorized/unauthorized.module#UnauthorizedModule",
        data: { breadcrumb: "UNAUTHORIZED.UNAUTHORIZED" },
      },
      {
        path: "regulations",
        loadChildren:
          "./pages/regulations/regulations.module#RegulationsModule",

      },
      {
        path: "bus-validator",
        loadChildren:
          "src/app/modules/mobility/modules/ticket-validator/ticket-validator.module#BusTicketValidatorModule",
        data: { breadcrumb: "MOBILITY.BREADCRUMB_BUS_VALIDATOR" },
      },
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
    ],
    canActivateChild: [PublicGuard],
    data: { breadcrumb: null },
  },
  {
    path: "",
    component: FullTemplateComponent,
    resolve: [AppResolver],
    children: [
      {
        path: "profile",
        loadChildren: "src/app/pages/profile/profile.module#ProfileModule",
        data: { breadcrumb: "CORE.PROFILE" },
        canActivate: [PrivateGuard],
      },
      {
        path: "personal-data",
        loadChildren:
          "src/app/pages/personal-data/personal-data.module#PersonalDataModule",
        data: { breadcrumb: "PROFILE_PAGE.PERSONAL_DATA" },
        canActivate: [PrivateGuard],
      },
      {
        path: "firstLogin",
        loadChildren:
          "src/app/pages/first-login/first-login.module#FirstLoginModule",
        data: { breadcrumb: "LOGIN.FIRST_LOGIN.TITLE" },
        canActivate: [PrivateGuard],
      },
      {
        path: "change-pin",
        loadChildren:
          "src/app/pages/change-pin/change-pin.module#ChangePinModule",
        data: { breadcrumb: "PROFILE_PAGE.CHANGE_PIN" },
        canActivate: [PrivateGuard],
      },
      {
        path: "change-password",
        loadChildren:
          "src/app/pages/change-password/change-password.module#ChangePasswordModule",
        data: { breadcrumb: "PROFILE_PAGE.CHANGE_PASSWORD" },
        canActivate: [PrivateGuard],
      },
      {
        path: "disable-account",
        loadChildren:
          "src/app/pages/disable-account/disable-account.module#DisableAccountModule",
        data: { breadcrumb: "PROFILE_PAGE.DISABLE_ACCOUNT" },
        canActivate: [PrivateGuard],
      },
      {
        path: "qrcode",
        loadChildren:
          "src/app/pages/qrcode-ticket/qrcode-ticket.module#QrcodeTicketModule",
          data: { breadcrumb: "QRCODE.TITLE" },
        canActivate: [PrivateGuard],
      },
      {
        path: "not-found",
        loadChildren:
          "src/app/pages/not-found-page/not-found-page.module#NotFoundPageModule",
        data: { breadcrumb: "MISC.PAGE_NOT_FOUND" },
      },
      {
        path: "accommodation",
        loadChildren:
          "./modules/accommodation/accommodation.module#AccommodationModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.ACCOMMODATION",
          scope: "accommodation",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "queue",
        loadChildren: "./modules/queue/queue.module#QueueModule",
        data: { breadcrumb: "QUEUE.TITLE_QUEUE", scope: "queue" },
        canActivate: [PrivateGuard],
      },
      {
        path: "privateaccommodation",
        loadChildren:
          "./modules/private-accommodation/private-accommodation.module#PrivateAccommodationModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.PRIVATEACCOMMODATION",
          scope: "private_accommodation",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "calendar",
        loadChildren:
          "./modules/calendar/calendar.module#CalendarModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.CALENDAR",
          scope: "calendar",
        },
        canActivate: [PrivateGuard],
      },

      {
        path: "alimentation",
        loadChildren:
          "./modules/alimentation/alimentation.module#AlimentationModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.ALIMENTATION",
          scope: "alimentation",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "social-support",
        loadChildren:
          "./modules/social-support/social-support.module#SocialSupportModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.SOCIAL_SUPPORT",
          scope: "social_scholarship",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "volunteering",
        loadChildren:
          "./modules/volunteering/volunteering.module#VolunteeringModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.VOLUNTEERING",
          scope: "volunteering",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "current-account",
        loadChildren:
          "./modules/current-account/current-account.module#CurrentAccountModule",
        data: {
          breadcrumb: "DASHBOARD.BUTTON.CURRENT_ACCOUNT",
          scope: "current_account",
        },
        canActivate: [PrivateGuard],
      },
      {
        path: "u-bike",
        loadChildren: "./modules/u-bike/u-bike.module#UBikeModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.UBIKE", scope: "u_bike" },
        canActivate: [PrivateGuard],
      },
      {
        path: "emergency-fund",
        loadChildren: "./modules/emergency-fund/emergency-fund.module#EmergencyFundModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.EMERGENCY_FUND", scope: "emergency_fund" },
        canActivate: [PrivateGuard],
      },
      {
        path: "mobility",
        loadChildren: "./modules/mobility/mobility.module#MobilityModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.MOBILITY", scope: "bus" },
        canActivate: [PrivateGuard],
      },
      {
        path: "shopping-cart",
        loadChildren:
          "./modules/cart-shopping/cart-shopping.module#CartShoppingModule",
        data: { breadcrumb: "SHOPPING_CART.PAGE.BREADCRUMB", scope: 'payments:cart' },
        canActivate: [PrivateGuard],
      },
      {
        path: "health",
        loadChildren: "./modules/health/health.module#HealthModule",
        data: { breadcrumb: "DASHBOARD.BUTTON.HEALTH", scope: "health" },
        canActivate: [PrivateGuard],
      },
    ],
    canActivateChild: [PrivateGuard],
    data: { breadcrumb: null },
  },
  {
    path: "",
    component: FullTemplateComponent,
    resolve: [AppResolver],
    children: [
      {
        path: "**",
        loadChildren:
          "src/app/pages/not-found-page/not-found-page.module#NotFoundPageModule",
        data: { breadcrumb: "MISC.PAGE_NOT_FOUND" },
      },
    ],
    data: { breadcrumb: null },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: "disabled" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
