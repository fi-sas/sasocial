import { TokenInterceptor } from './shared/interceptor/token.interceptor';
import { AuthService } from './auth/services/auth.service';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { FiCoreModule } from '@fi-sas/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N, pt_PT, en_US } from 'ng-zorro-antd';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxMaskModule } from 'ngx-mask';
import { ApisErrorsInterceptor } from '@fi-sas/webpage/shared/interceptor/apis-errors.interceptor';
import { WebpageErrorHandle } from '@fi-sas/webpage/shared/errors/error-handle';
import { CookieModule } from 'ngx-cookie';
import { AppResolver } from './app.resolver';
import { environment } from './../environments/environment';
import { QrcodeTicketModule } from './pages/qrcode-ticket/qrcode-ticket.module';


registerLocaleData(pt);

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(
    httpClient,
    './assets/i18n/',
     `.json?version=${environment.version}`,
  );
}

export function initializeApp(authService: AuthService) {
  return (): Promise<any> => {
    return authService.refreshToken().toPromise();
  }
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FiCoreModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SharedModule,
    CookieModule.forRoot(),
    NgxMaskModule.forRoot(),
    QrcodeTicketModule,
  ],
  providers: [
    AuthService,
    { provide: APP_INITIALIZER,useFactory: initializeApp, deps: [AuthService], multi: true},
    { provide: ErrorHandler, useClass: WebpageErrorHandle },
    { provide: NZ_I18N, useValue: pt_PT },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ApisErrorsInterceptor, multi: true },
    AppResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
