import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TestBed, async, inject } from '@angular/core/testing';

import { PrivateGuard } from './private.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';

describe('PrivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        NgZorroAntdModule,
        RouterTestingModule,
        FiCoreModule
      ],
      providers: [PrivateGuard]
    });
  });

  it('should ...', inject([PrivateGuard], (guard: PrivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
