import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, async, inject } from '@angular/core/testing';

import { PublicGuard } from './public.guard';
import { OverlayModule } from '@angular/cdk/overlay';

describe('PublicGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        NgZorroAntdModule,
        RouterTestingModule,
        FiCoreModule
      ],
      providers: [PublicGuard]
    });
  });

  it('should ...', inject([PublicGuard], (guard: PublicGuard) => {
    expect(guard).toBeTruthy();
  }));
});
