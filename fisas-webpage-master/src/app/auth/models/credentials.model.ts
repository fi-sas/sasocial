export class CredentialsModel {
  id: number;
  token: string;
  type: TypeCredential;
  changed: boolean;
  updated_at: string;
  created_at: string;
}

export enum TypeCredential {
  PIN = 'PIN',
  PASSWORD = 'PASSWORD',
  RFID = 'RFID'
}
