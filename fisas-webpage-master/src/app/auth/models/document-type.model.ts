import { TranslationModel } from "@fi-sas/webpage/shared/models/translation.model";

export class DocumentTypeModel {
    id: number;
    active: boolean;
    translations: TranslationModel[];
}
