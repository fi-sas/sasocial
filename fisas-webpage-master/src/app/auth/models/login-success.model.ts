export class LoginSuccessModel {
  token: string;
  expires_in: Date;
  expires_in_ms: number;
}
