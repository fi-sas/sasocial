import { Gender } from '@fi-sas/webpage/modules/u-bike/models/application.model';

export class PersonalDataModel {
  name: string;
  phone: string;
  identification: string;
  tin: string;
  gender: Gender;
  address: string;
  city: string;
  postal_code: string;
  country: string;
}
