import { Gender } from '@fi-sas/webpage/modules/u-bike/models/application.model';

export const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$&|.+_-])[A-Za-z\d!@#$&|.+_-]{9,}$/;

export class UserModel {
  id: number;
  rfid: string;
  name: string;
  email: string;
  phone: string;
  user_name: string;
  institute: string;
  student_number: string;
  gender: Gender;
  profile_id: number;
  profile: any;
  external: boolean;
  active: boolean;
  first_login: boolean;
  can_access_BO: boolean;
  address: string;
  city: string;
  country: string;
  postal_code: string;
  tin: string;
  identification: string;
  document_type_id: number;
  birth_date: string;
  scopes: any[];
  can_change_password: boolean;
  user_groups: any[];
  updated_at: Date;
  created_at: Date;
  organic_unit_id: number;
  course_degree_id: number;
  course_id: number;
  course_year: number;
  telephone?: string;
  nationality?: string;
  blocked_fields: any[];
}
