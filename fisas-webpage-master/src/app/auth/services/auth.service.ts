import { LoginSuccessModel } from "./../models/login-success.model";
import { Observable, BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { UserModel } from "../models/user.model";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { first } from "rxjs/operators";
import { HttpHeaders, HttpParams } from "@angular/common/http";
import { SSOurl } from "../models/sso-url.model";
import { FirstLogin } from "../models/first-login.model";
import { ChangePin } from "../models/change-pin.model";
import { ChangePassword } from "../models/change-password.model";
import { PersonalDataModel } from "../models/personal-data.model";
import { Router } from "@angular/router";
import { NzModalService } from "ng-zorro-antd";
import { TranslateService } from "@ngx-translate/core";
import { CredentialsModel } from "@fi-sas/webpage/auth/models/credentials.model";
import { ResetCredentialsModel } from "@fi-sas/webpage/auth/models/reset-credentials.model";
import { NewPasswordModel } from "@fi-sas/webpage/auth/models/new-password.model";
import { CCSamlRequest } from "../models/cc-saml-request.model";
import { DocumentTypeModel } from "../models/document-type.model";
import { Location } from "@angular/common";

export class AuthObject {
  user: UserModel;
  scopes: string[];
  token: string;
  expires_in: Date;
  authDate: Date;
}

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private _token: string;
  private _authObj: AuthObject;

  userSubject: BehaviorSubject<any> = new BehaviorSubject<any>(new UserModel());

  isLogged = false;
  isLoggedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  isGuest = false;
  isGuestSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  isRefreshing = false;
  $isRefreshing: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private location: Location
  ) {
    this.changeIsGuest(false);
    this.changeIsLogged(false);
  }

  /***
   * Check's the credentials and add to the storage the login data
   * @param email
   * @param password
   */
  login(email: string, password: string): Observable<AuthObject> {
    return new Observable<AuthObject>((observer) => {
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.LOGIN"),
          {
            email,
            password,
          },
          {
            headers: new HttpHeaders()
              .append("no-error", "true")
              .append("no-token", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {
            this._token = value.data[0].token;
            this.createAuthObject(value.data[0]).subscribe(
              (res) => {
                observer.next(res);
                observer.complete();
              },
              (err) => {
                observer.error(err);
                observer.complete();
              }
            );
          },
          (error) => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  /***
   * Creates a token guest
   */
  loginGuest(): Promise<AuthObject> {
    return new Promise<AuthObject>((resolve, reject) => {
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.LOGIN"),
          {},
          {
            headers: new HttpHeaders()
              .append("no-error", "true")
              .append("no-token", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {
            this.createAuthObject(value.data[0]).subscribe(
              (res) => {
                resolve(res);
              },
              (err) => {
                reject(err);
              }
            );
          },
          (error) => {
            reject(error);
          }
        );
    });
  }

  /***
   * Get url to reddirect to the login page of the idp
   */
  ssoLoginUrl(): Observable<Resource<SSOurl>> {
    return this.resourceService
      .read<SSOurl>(this.urlService.get("AUTH.SSO_URL"))
      .pipe(first());
  }

  ssoCCLoginSAMLRequest(): Observable<Resource<CCSamlRequest>> {
    return this.resourceService
      .read<CCSamlRequest>(this.urlService.get("AUTH.SSO_CC_URL"))
      .pipe(first());
  }

  private changeIsGuest(isGuest: boolean) {
    this.isGuest = isGuest;
    this.isGuestSubject.next(this.isGuest);
  }

  getIsGuest() {
    return this.isGuest;
  }

  getIsGuestObservable() {
    return this.isGuestSubject.asObservable();
  }

  private changeIsLogged(isLogged: boolean) {
    this.isLogged = isLogged;
    this.isLoggedSubject.next(this.isLogged);
  }

  getIsLogged() {
    return this.isLogged;
  }

  getIsLoggedObservable() {
    return this.isLoggedSubject.asObservable();
  }

  /***
   * Creates the auth objects after authentication
   */
  createAuthObject(result: LoginSuccessModel): Observable<AuthObject> {
    return new Observable<AuthObject>((observer) => {
      const authObj = new AuthObject();

      authObj.authDate = new Date();
      authObj.token = result.token;
      if (result.expires_in_ms) {
        authObj.expires_in = new Date(Date.now() + result.expires_in_ms);
      } else {
        authObj.expires_in = new Date(result.expires_in);
      }
      authObj.scopes =
        this._authObj && this._authObj.scopes ? this._authObj.scopes : [];
      this.saveAuthObj(authObj);

      // ALERT TOKEN REFRESH
      this.setRefreshingToken(false);

      this.resourceService
        .read<UserModel>(this.urlService.get("AUTH.LOGIN_USER"), {})
        .pipe(first())
        .subscribe(
          (value1) => {
            const isGuest = value1.data[0].user_name === "guest";

            if (!isGuest) {
              authObj.user = value1.data[0];
              this.userSubject.next(authObj.user);
            }

            if (isGuest) {
              this.changeIsGuest(true);
              this.changeIsLogged(false);
            } else {
              this.changeIsGuest(false);
              this.changeIsLogged(true);
            }

            this.validateToken()
              .pipe(first())
              .subscribe(
                () => {
                  observer.next(authObj);
                  observer.complete();
                },
                (error) => {
                  observer.error(error);
                  observer.complete();
                }
              );
          },
          (error) => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }

  /***
   * Remove the data of the login
   */
  logout(): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this._token = null;
      this._authObj = null;
      this.userSubject.next(null);

      this.changeIsGuest(false);
      this.changeIsLogged(false);

      this.resourceService
        .read<any>(this.urlService.get("AUTH.LOGOUT", { type: "WEB" }), {
          headers: new HttpHeaders().append("no-error", "true"),
          withCredentials: true,
        })
        .pipe(first())
        .subscribe((value) => {
          this.loginGuest()
            .then(() => {
              observer.next(true);
              observer.complete();
            })
            .catch(() => {
              observer.next(false);
              observer.complete();
            });
        }),
        (error) => {};
    });
  }

  /***
   * Return the user data of the login data
   */
  getAuth(): AuthObject {
    return this._authObj;
  }

  /***
   * Return the user data of the login data
   */
  getUser(): UserModel {
    if (this.getAuth() !== null && this.getAuth() !== undefined) {
      return this.getAuth().user;
    } else {
      return null;
    }
  }

  /***
   * Update the user object
   * @param user
   */
  setUser(user: UserModel): UserModel {
    if (this.getAuth() !== null) {
      const authObj = this.getAuth();
      authObj.user = user;
      this.userSubject.next(authObj.user);
      this.saveAuthObj(authObj);
      return this.getAuth().user;
    } else {
      return null;
    }
  }

  /***
   * Return the scopes of the user
   */
  getScopes(): string[] {
    if (this.getAuth() !== null) {
      return this.getAuth().scopes;
    } else {
      return null;
    }
  }

  /***
   * Update the scopes of the user
   */
  setScopes(scopes: string[]): string[] {
    if (this.getAuth() !== null) {
      this.getAuth().scopes = scopes;
      return this.getAuth().scopes;
    } else {
      return null;
    }
  }

  /***
   * Return true if the user have the scope send by param
   * if send only scope microservice:entity checks if have all
   * permissions if send microservice:entity:(crud operation) check if has
   * this permission
   * @param scope
   */
  hasPermission(scope: string): boolean {
    if (scope == null || scope == "" || scope == undefined) {
      return false;
    }
    return this.getScopes() != undefined &&
      this.getScopes().includes(scope) != undefined
      ? this.getScopes().includes(scope)
      : false;
  }

  /***
   * Return a Observable of the user object
   */
  getUserObservable(): Observable<UserModel> {
    return this.userSubject.asObservable();
  }

  /***
   * Returns the token of the user logged
   */
  getToken(): string {
    return this.getAuth() ? this.getAuth().token : null;
  }

  private setRefreshingToken(isRefreshing: boolean) {
    this.isRefreshing = isRefreshing;
    this.$isRefreshing.next(this.isRefreshing);
  }

  refreshToken(): Observable<any> {
    return new Observable((observer) => {
      this.setRefreshingToken(true);

      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.REFRESH_TOKEN", { type: "WEB" }),
          {},
          {
            headers: new HttpHeaders()
              .append("no-error", "true")
              .append("no-token", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {
            this._token = value.data[0].token;
            this.createAuthObject(value.data[0])
              .pipe(first())
              .subscribe(
                (res) => {
                  observer.next(res);
                  observer.complete();
                },
                (err) => {
                  observer.error(err);
                  observer.complete();
                }
              );
          },
          (err) => {
            this.loginGuest()
              .then((res) => {
                observer.next(false);
                observer.complete();
                this.setRefreshingToken(false);
              })
              .catch((err1) => {
                observer.next(false);
                observer.complete();
                this.setRefreshingToken(false);
              });
          }
        );
    });
  }

  /***
   * Validate the token and updates the scopes of the user
   */
  validateToken(): Observable<boolean> {
    return Observable.create((observable) => {
      if (!this.getToken()) {
        observable.next(false);
      } else {
        this.resourceService
          .create<{ user: UserModel; scopes: string[] }>(
            this.urlService.get("AUTH.VALIDATE_TOKEN"),
            { token: this.getToken() }
          )
          .pipe(first())
          .subscribe(
            (value) => {
              if (value.data.length === 0) {
                observable.next(false);
              } else {
                this.setUser(value.data[0].user);
                this.setScopes(value.data[0].scopes);
                // this.saveAuthObj();
                observable.next(true);
              }
            },
            (err) => {
              observable.error(err);
            }
          );
      }
    });
  }

  /***
   * Save the auth obj to local storage
   */
  private saveAuthObj(authObj: AuthObject) {
    this._authObj = authObj;
  }

  changePassword(
    changePassword: ChangePassword
  ): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.CHANGE_PASSWORD"),
      changePassword
    );
  }
  changePin(changePin: ChangePin): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get("USERS.CHANGE_PIN"),
      changePin
    );
  }

  randomPin(): Observable<Resource<any>> {
    return this.resourceService.create<any>(
      this.urlService.get("USERS.RANDOM_PIN"),
      ""
    );
  }

  resetPin(
    resetPin: ResetCredentialsModel
  ): Observable<Resource<CredentialsModel>> {
    return this.resourceService.create<CredentialsModel>(
      this.urlService.get("USERS.RESET_PIN"),
      resetPin
    );
  }

  confirmPin(token: string): Observable<Resource<UserModel>> {
    let params = new HttpParams();
    params = params.set("token_validate", token);
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.CONFIRM_PIN"),
      {},
      { params }
    );
  }

  resetPassword(
    resetPassword: ResetCredentialsModel
  ): Observable<Resource<CredentialsModel>> {
    return this.resourceService.create<CredentialsModel>(
      this.urlService.get("USERS.RESET_PASSWORD"),
      resetPassword
    );
  }

  newPassword(newPassword: NewPasswordModel): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.NEW_PASSWORD"),
      newPassword
    );
  }

  firstLogin(firstLogin: FirstLogin): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.FIRST_LOGIN"),
      firstLogin
    );
  }

  isProfileDataCompleted(
    router: Router,
    translateService: TranslateService,
    modalService: NzModalService
  ): Promise<boolean> {
    const requiredFields = [
      "name",
      "birth_date",
      "gender",
      "phone",
      "identification",
      "tin",
      "address",
      "city",
      "postal_code",
      "country",
    ];
    const user = this.getUser();
    for (const field of requiredFields) {
      if (!user[field]) {
        setTimeout(() => {
          translateService
            .get([
              "MISC.CANCEL",
              "CORE.UPDATE_PROFILE.EDIT_PROFILE",
              "CORE.UPDATE_PROFILE.TITLE",
              "CORE.UPDATE_PROFILE.CONTENT",
            ])
            .pipe(first())
            .subscribe((translation) => {
              modalService.confirm({
                nzTitle: translation["CORE.UPDATE_PROFILE.TITLE"],
                nzContent: translation["CORE.UPDATE_PROFILE.CONTENT"],
                nzCancelText: translation["MISC.CANCEL"],
                nzOkText: translation["CORE.UPDATE_PROFILE.EDIT_PROFILE"],
                nzOnCancel: () => {
                  this.location.back();
                },
                nzOnOk: () => {
                  router.navigate(["/personal-data"]);
                },
              });
            });
        }, 500);
        return Promise.resolve(false);
      }
    }
    return Promise.resolve(true);
  }

  updateMe(personalData: PersonalDataModel): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.UPDATE_ME"),
      personalData
    );
  }

  getUserInfoByToken(): Observable<Resource<UserModel>> {
    return this.resourceService.read<UserModel>(
      this.urlService.get("AUTH.LOGIN_USER")
    );
  }

  verifyAccountToken(
    token: string,
    password: string
  ): Observable<Resource<UserModel>> {
    return this.resourceService.create<UserModel>(
      this.urlService.get("USERS.VERIFY_ACCOUNT"),
      { token, password },
      {
        headers: new HttpHeaders().append("no-error", "true"),
      }
    );
  }

  newVerificationCode(email: string): Observable<Resource<UserModel>> {
    return this.resourceService.create<any>(
      this.urlService.get("USERS.NEW_VERIFICATION_CODE"),
      { email }
    );
  }

  userDisableAccount(password: string): Observable<Resource<UserModel>> {
    return this.resourceService.create<any>(
      this.urlService.get("USERS.USER_DISABLE"),
      { password }
    );
  }

  getDocumentTypes(): Observable<Resource<DocumentTypeModel>> {
    return this.resourceService.list<DocumentTypeModel>(
      this.urlService.get("USERS.DOCUMENT_TYPES")
    );
  }
}
