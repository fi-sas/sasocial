import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutCurrentAccountComponent } from './current-account.component';


describe('LayoutCurrentAccountComponent', () => {
  let component: LayoutCurrentAccountComponent;
  let fixture: ComponentFixture<LayoutCurrentAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutCurrentAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutCurrentAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
