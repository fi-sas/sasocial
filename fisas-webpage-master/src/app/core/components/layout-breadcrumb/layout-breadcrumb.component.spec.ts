import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutBreadcrumbComponent } from './layout-breadcrumb.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('LayoutBreadcrumbComponent', () => {
  let component: LayoutBreadcrumbComponent;
  let fixture: ComponentFixture<LayoutBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgZorroAntdModule,
        TranslateModule,
        RouterTestingModule
      ],
      declarations: [ LayoutBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
