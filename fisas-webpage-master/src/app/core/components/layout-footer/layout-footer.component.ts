import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-layout-footer',
  templateUrl: './layout-footer.component.html',
  styleUrls: ['./layout-footer.component.less']
})
export class LayoutFooterComponent implements OnInit {

  current_date = new Date();
  version = environment.version;

  constructor(
  ) { }

  ngOnInit() {
  }
}
