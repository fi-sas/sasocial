import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderUserComponent } from './layout-header-user.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

describe('LayoutHeaderUserComponent', () => {
  let component: LayoutHeaderUserComponent;
  let fixture: ComponentFixture<LayoutHeaderUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgZorroAntdModule,
        RouterTestingModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        FiCoreModule,
        SharedModule
      ],
      declarations: [ LayoutHeaderUserComponent ],
      providers: [
        TranslateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
