import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHeaderComponent } from './layout-header.component';
import { LayoutBreadcrumbComponent } from '../layout-breadcrumb/layout-breadcrumb.component';
import { LayoutHeaderUserComponent } from '../layout-header-user/layout-header-user.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { WP_CONFIGURATION } from '@fi-sas/webpage/app.config';
import { LayoutMobileMenuComponent } from '../layout-mobile-menu/layout-mobile-menu.component';
import { LayoutReportsComponent } from '../reports/reports.component';

describe('LayoutHeaderComponent', () => {
  let component: LayoutHeaderComponent;
  let fixture: ComponentFixture<LayoutHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgZorroAntdModule,
        RouterTestingModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        SharedModule,
        FiCoreModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      declarations: [
        LayoutHeaderComponent,
        LayoutBreadcrumbComponent,
        LayoutHeaderUserComponent,
        LayoutReportsComponent,
        LayoutMobileMenuComponent
      ],
      providers: [
        { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
