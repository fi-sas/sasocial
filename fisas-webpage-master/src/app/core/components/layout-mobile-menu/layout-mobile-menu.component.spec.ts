import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutMobileMenuComponent } from './layout-mobile-menu.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('LayoutMobileMenuComponent', () => {
  let component: LayoutMobileMenuComponent;
  let fixture: ComponentFixture<LayoutMobileMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgZorroAntdModule,
        RouterTestingModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        SharedModule,
        FiCoreModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      declarations: [ LayoutMobileMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutMobileMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
