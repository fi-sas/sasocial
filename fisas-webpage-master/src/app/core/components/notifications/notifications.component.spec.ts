import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutNotificationsComponent } from './notifications.component';


describe('LayoutNotificationsComponent', () => {
  let component: LayoutNotificationsComponent;
  let fixture: ComponentFixture<LayoutNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
