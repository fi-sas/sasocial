import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutReportsComponent } from './reports.component';


describe('LayoutReportsComponent', () => {
  let component: LayoutReportsComponent;
  let fixture: ComponentFixture<LayoutReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
