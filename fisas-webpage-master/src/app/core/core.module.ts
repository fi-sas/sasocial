import { SharedModule } from './../shared/shared.module';
import { AuthTemplateComponent } from './templates/auth-template/auth-template.component';
import { UiService } from './services/ui.service';
import { NgModule, Optional, SkipSelf, APP_BOOTSTRAP_LISTENER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { FullTemplateComponent } from './templates/full-template/full-template.component';
import { LayoutHeaderComponent } from './components/layout-header/layout-header.component';
import { LayoutSlideMenuComponent } from './components/layout-slide-menu/layout-slide-menu.component';
import { LayoutBreadcrumbComponent } from './components/layout-breadcrumb/layout-breadcrumb.component';
import { RouterModule } from '@angular/router';
import { LayoutHeaderUserComponent } from './components/layout-header-user/layout-header-user.component';
import { LayoutFooterComponent } from './components/layout-footer/layout-footer.component';
import { SidebarOptionsChangeService } from '@fi-sas/webpage/core/services/sidebar-options-change.service';
import { VersionCheckService } from './services/version-check.service';
import { LayoutMobileMenuComponent } from './components/layout-mobile-menu/layout-mobile-menu.component';
import { LayoutReportsComponent } from './components/reports/reports.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { LayoutNotificationsComponent } from './components/notifications/notifications.component';
import { LayoutCurrentAccountComponent } from './components/current-account/current-account.component';
@NgModule({
  declarations: [
    FullTemplateComponent,
    LayoutHeaderComponent,
    LayoutSlideMenuComponent,
    LayoutBreadcrumbComponent,
    AuthTemplateComponent,
    LayoutHeaderUserComponent,
    LayoutReportsComponent,
    LayoutFooterComponent,
    LayoutMobileMenuComponent,
    ShoppingCartComponent,
    LayoutNotificationsComponent,
    LayoutCurrentAccountComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  providers: [
    UiService,
    SidebarOptionsChangeService,
    VersionCheckService,
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('Core Module is already loaded use only on Root Module');
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [UiService]
    };
  }
}
