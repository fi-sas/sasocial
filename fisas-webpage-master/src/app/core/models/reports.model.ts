import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { FileModel } from "@fi-sas/webpage/modules/accommodation/models/file.model";

export enum ReportStatus {
  "WAITING", "PROCCESSING", "READY", "FAILED"
}

export class ReportModel {
  id: number;
  name: string;
  status: ReportStatus;
  template_id: number;
  user_id: number;
  user?: UserModel;
  file_id: number;
  file?: FileModel;
  created_at: Date;
  unread: boolean;
  total_unread: number;
}
