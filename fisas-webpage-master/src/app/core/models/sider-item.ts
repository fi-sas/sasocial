export interface SiderItem {
  code: string;
  name: any;
  link: string;
  icon?: string;
  disable?: boolean;
  children?: SiderItem[];
  serviceId?: number;
  type?: string;
  scope?:string | string[];
  visible?: boolean;
}

