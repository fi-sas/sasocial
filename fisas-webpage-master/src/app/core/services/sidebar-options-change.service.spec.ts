import { TestBed } from '@angular/core/testing';

import { SidebarOptionsChangeService } from './sidebar-options-change.service';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { WP_CONFIGURATION } from '@fi-sas/webpage/app.config';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';


describe('SidebarOptionsChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
        FiCoreModule,
        NgZorroAntdModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      providers: [
        { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION },
      ]
    }));

  it('should be created', () => {
    const service: SidebarOptionsChangeService = TestBed.get(SidebarOptionsChangeService);
    expect(service).toBeTruthy();
  });
});
