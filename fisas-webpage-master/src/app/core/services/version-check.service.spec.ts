import { TestBed } from '@angular/core/testing';

import { VersionCheckService } from './version-check.service';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';


describe('VersionCheckService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      FiCoreModule,
      HttpClientTestingModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
      SharedModule,
    ]
  }));

  it('should be created', () => {
    const service: VersionCheckService = TestBed.get(VersionCheckService);
    expect(service).toBeTruthy();
  });
});
