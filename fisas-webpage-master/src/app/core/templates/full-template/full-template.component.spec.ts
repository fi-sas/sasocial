import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutFooterComponent } from './../../components/layout-footer/layout-footer.component';
import { FiCoreModule } from './../../../../../libs/core/src/lib/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZorroAntdModule, NzSelectComponent } from 'ng-zorro-antd';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullTemplateComponent } from './full-template.component';
import { LayoutHeaderComponent } from '../../components/layout-header/layout-header.component';
import { LayoutSlideMenuComponent } from '../../components/layout-slide-menu/layout-slide-menu.component';
import { LayoutBreadcrumbComponent } from '../../components/layout-breadcrumb/layout-breadcrumb.component';
import { LayoutHeaderUserComponent } from '../../components/layout-header-user/layout-header-user.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { LayoutMobileMenuComponent } from '../../components/layout-mobile-menu/layout-mobile-menu.component';
import { LayoutReportsComponent } from '../../components/reports/reports.component';

describe('FullTemplateComponent', () => {
  let component: FullTemplateComponent;
  let fixture: ComponentFixture<FullTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NgZorroAntdModule,
        RouterTestingModule,
        FiCoreModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      declarations: [
        FullTemplateComponent,
        LayoutHeaderComponent,
        LayoutSlideMenuComponent,
        LayoutBreadcrumbComponent,
        LayoutHeaderUserComponent,
        LayoutReportsComponent,
        LayoutFooterComponent,
        LayoutMobileMenuComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
