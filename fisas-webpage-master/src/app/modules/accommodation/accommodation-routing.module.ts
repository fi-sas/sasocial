import { AccommodationComponent } from "./accommodation.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ViewApplicationResolver } from "@fi-sas/webpage/modules/accommodation/resolvers/view-application.resolver";

const routes: Routes = [
  {
    path: "",
    component: AccommodationComponent,
    children: [
      { path: "", redirectTo: "list-residences", pathMatch: "full" },
      {
        path: "list-residences",
        loadChildren:
          "./pages/list-residences/list-residences.module#ListResidencesModule",
        data: { breadcrumb: "ACCOMMODATION.MENU.RESIDENCE" },
      },
      {
        path: "residence-detail/:id",
        loadChildren:
          "./pages/residence-detail/residence-detail.module#ResidenceDetailModule",
        data: { breadcrumb: "ACCOMMODATION.MENU.RESIDENCE" },
      },
      {
        path: "list-applications",
        loadChildren:
          "src/app/modules/accommodation/pages/list-applications/list-applications.module#ListApplicationsModule",
        data: { breadcrumb: "ACCOMMODATION.SEE_APPLICATIONS" },
      },
      {
        path: "application-view/:id",
        loadChildren:
          "./pages/application-view/application-view.module#ApplicationViewModule",
        data: { breadcrumb: "ACCOMMODATION.SEE_APPLICATIONS" },
        resolve: { application: ViewApplicationResolver },
      },
      {
        path: "list-residences/form-application",
        loadChildren:
          "./pages/form-application/form-application.module#FormApplicationModule",
        data: { breadcrumb: "ACCOMMODATION.FORM.TITLE" },
      },
      {
        path: "list-residences/form-application/:id",
        loadChildren:
          "./pages/form-application/form-application.module#FormApplicationModule",
        data: { breadcrumb: "ACCOMMODATION.FORM.TITLE" },
        resolve: { application: ViewApplicationResolver },
      },

      {
        path: "change-contract/:id",
        loadChildren:
          "./pages/application-change-requests/application-change-requests.module#ApplicationChangeRequestModule",
        data: { breadcrumb: "ACCOMMODATION.CHANGE_REQUESTS.PAGE_TITLE" },
        resolve: { application: ViewApplicationResolver },
      },
      {
        path: "history-change-contract/:id",
        loadChildren:
          "./pages/history-change-requests/history-change-requests.module#HistoryChangeRequestsModule",
        data: { breadcrumb: "ACCOMMODATION.CHANGE_REQUESTS.TITLE" },
        resolve: { application: ViewApplicationResolver },
      },
      {
        path: "billings/:id",
        loadChildren:
          "./pages/application-billings/application-billings.module#ApplicationBillingsModule",
        data: { breadcrumb: "ACCOMMODATION.BILLINGS_TITLE" },
        resolve: { application: ViewApplicationResolver },
      },
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccommodationRoutingModule {}
