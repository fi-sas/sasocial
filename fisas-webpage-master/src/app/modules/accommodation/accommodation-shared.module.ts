import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ApplicationChangesModalComponent } from "./components/application-changes-modal/application-changes-modal.component";
import { ApplicationRoomChangesModalComponent } from "./components/application-room-changes-modal/application-room-changes-modal.component";
import { ApplicationExtrasChangeModalComponent } from "./components/application-extras-change-modal/application-extras-change-modal.component";
import { ApplicationRegimeChangeModalComponent } from "./components/application-regime-change-modal/application-regime-change-modal.component";
import { ApplicationMaintenanceRequestModalComponent } from "./components/application-maintenance-request-modal/application-maintenance-request-modal.component";
import { ApplicationScholarshipChangeModalComponent } from "./components/application-scholarship-change-modal/application-scholarship-change-modal.component";
import { ApplicationCommunicationDateModalComponent } from "./components/application-communication-date-modal/application-communication-date-modal.component";
import { ApplicationIbanChangeModalComponent } from "./components/application-iban-change-modal/application-iban-change-modal.component";

@NgModule({
  declarations: [
    ApplicationChangesModalComponent,
    ApplicationRoomChangesModalComponent,
    ApplicationExtrasChangeModalComponent,
    ApplicationRegimeChangeModalComponent,
    ApplicationMaintenanceRequestModalComponent,
    ApplicationScholarshipChangeModalComponent,
    ApplicationCommunicationDateModalComponent,
    ApplicationIbanChangeModalComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: [
    ApplicationChangesModalComponent,
    ApplicationRoomChangesModalComponent,
    ApplicationExtrasChangeModalComponent,
    ApplicationRegimeChangeModalComponent,
    ApplicationMaintenanceRequestModalComponent,
    ApplicationScholarshipChangeModalComponent,
    ApplicationCommunicationDateModalComponent,
    ApplicationIbanChangeModalComponent,
  ],
  entryComponents: [
    ApplicationChangesModalComponent,
    ApplicationRoomChangesModalComponent,
    ApplicationExtrasChangeModalComponent,
    ApplicationRegimeChangeModalComponent,
    ApplicationMaintenanceRequestModalComponent,
    ApplicationScholarshipChangeModalComponent,
    ApplicationCommunicationDateModalComponent,
    ApplicationIbanChangeModalComponent,
  ],
})
export class AccommodationSharedModule {}
