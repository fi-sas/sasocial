import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AccommodationComponent } from './accommodation.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { UiService } from '@fi-sas/webpage/core/services/ui.service';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {OPTIONS_TOKEN} from '@fi-sas/configurator';
import {WP_CONFIGURATION} from '@fi-sas/webpage/app.config';


describe('AccommodationComponent', () => {
  let component: AccommodationComponent;
  let fixture: ComponentFixture<AccommodationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        OverlayModule,
        FiCoreModule,
        HttpClientTestingModule
      ],
      providers: [
        UiService,
        { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION }
      ],
      declarations: [ AccommodationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccommodationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
