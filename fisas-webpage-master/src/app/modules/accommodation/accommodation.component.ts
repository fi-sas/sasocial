import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-accommodation',
  templateUrl: 'accommodation.component.html',
  styleUrls: ['./accommodation.component.less']
})
export class AccommodationComponent implements OnInit {

  barOptions:any[] = [];


  constructor(
    private authService: AuthService,
  ) { }

  // getApplicationsStatus(): void {
  //
  //    this.applicationService.applicationStatus().subscribe( status => {
  //     if (status) {
  //       if (status.data[0].status) {
  //       // if (status.data[0].status === 'closed') {
  //
  //         this.uiService.addSiderItem({name: 'WITHDRAWALS.WITHDRAWAL', link: '/accommodation/list-withdrawals'});
  //         this.uiService.addSiderItem({name: 'EXTENSIONS.EXTENSIONS', link: '/accommodation/list-extensions'});
  //         this.uiService.addSiderItem({name: 'ABSENCES.ABSENCES', link: '/accommodation/list-absences'});
  //       }
  //     }
  //   }, () => {
  //   });
  // }

  ngOnInit() {
    this.barOptions = [
      {
        translate: 'ACCOMMODATION.MENU.RESIDENCE',
        disable: true,
        routerLink: '/accommodation/list-residences',
        scope: this.authService.hasPermission('accommodation:residences:read')
      },
      {
        translate: 'ACCOMMODATION.MENU.MY_APPLICATIONS',
        disable: false,
        routerLink: '/accommodation/list-applications',
        scope: this.authService.hasPermission('accommodation:applications:read')
      },
      {
        translate: 'ACCOMMODATION.CHANGE_REQUESTS.PAGE_TITLE',
        disable: false,
        routerLink: '/accommodation/change-contract',
        scope: this.authService.hasPermission('accommodation:applications:create')
      },
      {
        translate: 'ACCOMMODATION.CHANGE_REQUESTS.TITLE',
        disable: false,
        routerLink: '/accommodation/history-change-contract',
        scope: this.authService.hasPermission('accommodation:applications:read')
      },
      {
        translate: 'ACCOMMODATION.BILLINGS_TITLE',
        disable: false,
        routerLink: '/accommodation/billings',
        scope: this.authService.hasPermission('accommodation:billings:read')
      }
    ]
  }

}
