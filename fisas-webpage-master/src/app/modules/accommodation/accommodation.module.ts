import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccommodationRoutingModule } from './accommodation-routing.module';
import { AccommodationComponent } from './accommodation.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { RoomsService } from '@fi-sas/webpage/modules/accommodation/services/rooms.service';
import { ApplicationsService } from '@fi-sas/webpage/modules/accommodation/services/applications.service';
import { ResidencesService } from '@fi-sas/webpage/modules/accommodation/services/residences.service';
import { ExtensionsService } from '@fi-sas/webpage/modules/accommodation/services/extensions.service';
import { WithdrawalsService } from '@fi-sas/webpage/modules/accommodation/services/withdrawals.service';
import { ViewApplicationResolver } from '@fi-sas/webpage/modules/accommodation/resolvers/view-application.resolver';


@NgModule({
  declarations: [
    AccommodationComponent,
  ],
  imports: [
    CommonModule,
    AccommodationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    RoomsService,
    ApplicationsService,
    ResidencesService,
    ExtensionsService,
    WithdrawalsService,
    ViewApplicationResolver
  ]
})
export class AccommodationModule { }
