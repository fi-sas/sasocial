import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationChangesModalComponent } from './application-changes-modal.component';

describe('ApplicationChangesModalComponent', () => {
  let component: ApplicationChangesModalComponent;
  let fixture: ComponentFixture<ApplicationChangesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationChangesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationChangesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
