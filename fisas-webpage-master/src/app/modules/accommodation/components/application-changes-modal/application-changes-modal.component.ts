import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { AbsencesService } from '@fi-sas/webpage/modules/accommodation/services/absences.service';
import { ExtensionsService } from '@fi-sas/webpage/modules/accommodation/services/extensions.service';
import { WithdrawalsService } from '@fi-sas/webpage/modules/accommodation/services/withdrawals.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';
import { ApplicationChangeModel } from '@fi-sas/webpage/modules/accommodation/models/application-change.model';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-changes-modal',
  templateUrl: './application-changes-modal.component.html',
  styleUrls: ['./application-changes-modal.component.less']
})
export class ApplicationChangesModalComponent implements OnInit {
  isLoading = false;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  file = null;
  type: string = null;
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  application: ApplicationModel = null;
  applicationChangeForm = new FormGroup({
    start_date: new FormControl(null),
    end_date: new FormControl(null),
    reason: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
    allow_booking: new FormControl(false, []),
    application_id: new FormControl(null, [Validators.required]),
  });
  submitted = false;
  get f() { return this.applicationChangeForm.controls; }
  constructor(
    private modalRef: NzModalRef,
    private withdrawalsService: WithdrawalsService,
    private absencesService: AbsencesService,
    private extensionsService: ExtensionsService,
    private uiService: UiService,
    private translateService: TranslateService,
    private fileService: FilesService
  ) {

  }

  ngOnInit() {
    if (this.edit || this.consult) {
      if (this.type == 'ABSENCE') {
        this.applicationChangeForm.get('start_date').setValue(this.changesEdit.start_date);
        this.applicationChangeForm.get('end_date').setValue(this.changesEdit.end_date);

      }else {
        this.applicationChangeForm.get('end_date').setValue(this.changesEdit.end_date);
      }
      this.applicationChangeForm.get('reason').setValue(this.changesEdit.reason);
      this.applicationChangeForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if(this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }

    }
    switch (this.type) {
      case 'WITHDRAWAL':
        this.applicationChangeForm.get("end_date").setValidators([Validators.required]);
        break;
      case 'ABSENCE':
        this.applicationChangeForm.get("start_date").setValidators([Validators.required]);
        this.applicationChangeForm.get("end_date").setValidators([Validators.required]);
        break;
      case 'EXTENSION':
        this.applicationChangeForm.get("end_date").setValidators([Validators.required]);
        break;
    }
    this.applicationChangeForm.get('application_id').setValue(this.application.id);
  }

  disabledDate = (current: Date) => {
    if (this.type == 'WITHDRAWAL' && moment(current).isAfter(this.application.start_date)) {
      return moment(current).isBefore(new Date()) || moment(current).isAfter(this.application.end_date);
    }
    else if (this.type != 'EXTENSION') {
      return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
    } else {
      return moment(current).isBefore(this.application.end_date);
    }
  }

  close(): void {
    this.modalRef.close(false);
  }



  onSubmit() {

    this.checkValidityOfControls([
      this.applicationChangeForm.get('start_date'),
      this.applicationChangeForm.get('end_date'),
      this.applicationChangeForm.get('reason'),
      this.applicationChangeForm.get('application_id'),
      this.applicationChangeForm.get('file_id'),
    ]);
    this.submitted = true;
    if (this.applicationChangeForm.invalid) return;
    this.isLoading = true;
    if(!this.edit) {
      switch (this.type) {

        case 'WITHDRAWAL':
          this.withdrawalsService.create(this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.WITHDRAWAL_CREATED'));
            this.modalRef.close(true);
          });
          break;
        case 'ABSENCE':
          this.absencesService.create(this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.ABSENCE_CREATED'));
            this.modalRef.close(true);
          });
          break;
        case 'EXTENSION':
          this.extensionsService.create(this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.EXTENSION_CREATED'));
            this.modalRef.close(true);
          });
          break;
      }
    }else {
      switch (this.type) {

        case 'WITHDRAWAL':
          this.withdrawalsService.patch(this.changesEdit.id,this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.WITHDRAWAL_EDIT'));
            this.modalRef.close(true);
          });
          break;
        case 'ABSENCE':
          this.absencesService.patch(this.changesEdit.id,this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.ABSENCE_EDIT'));
            this.modalRef.close(true);
          });
          break;
        case 'EXTENSION':
          this.extensionsService.patch(this.changesEdit.id,this.applicationChangeForm.value).pipe(
            first(), finalize(() => this.isLoading = false)
          ).subscribe(changes => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.EXTENSION_EDIT'));
            this.modalRef.close(true);
          });
          break;
      }
    }

    return false;
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }
}
