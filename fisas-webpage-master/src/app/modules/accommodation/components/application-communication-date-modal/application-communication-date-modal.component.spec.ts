import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationCommunicationDateModalComponent } from './application-communication-date-modal.component';

describe('ApplicationCommunicationDateModalComponent', () => {
  let component: ApplicationCommunicationDateModalComponent;
  let fixture: ComponentFixture<ApplicationCommunicationDateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationCommunicationDateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationCommunicationDateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
