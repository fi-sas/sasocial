import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { ApplicationModel } from '../../models/application.model';
import * as moment from 'moment';
import { ApplicationCommunicationsService } from '../../services/application-communications.service';
import { finalize, first } from 'rxjs/operators';
import { CommunicationRequestModel } from '../../models/application-communication.model';

@Component({
  selector: 'app-application-communication-date-modal',
  templateUrl: './application-communication-date-modal.component.html',
  styleUrls: ['./application-communication-date-modal.component.less']
})
export class ApplicationCommunicationDateModalComponent implements OnInit {
  submitted = false;
  edit: boolean = null;
  consult: boolean = null;
  isLoading = false;

  application: ApplicationModel = null;
  changesEdit: ApplicationChangeModel = null;

  type: string = null;
  applicationCommunicationForm = new FormGroup({
    start_date: new FormControl(null, []),
    end_date: new FormControl(null, []),
    entry_hour: new FormControl(null, []),
    exit_hour: new FormControl(null, []),
    type: new FormControl(null, [Validators.required]),
    response: new FormControl(null, []),
    observations: new FormControl('', []),
    application_id: new FormControl(null, [Validators.required]),
  });
  get f() { return this.applicationCommunicationForm.controls; }
  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private translateService: TranslateService,
    private applicationCommunicationService: ApplicationCommunicationsService
  ) { }

  ngOnInit() {
    if (this.changesEdit) {
      this.applicationCommunicationForm.get('start_date').setValue(this.changesEdit.start_date);
      this.applicationCommunicationForm.get('end_date').setValue(this.changesEdit.end_date);
      this.applicationCommunicationForm.get('entry_hour').setValue(this.changesEdit.entry_hour ? moment(this.changesEdit.entry_hour, "HH:mm:ss").toDate() : null);
      this.applicationCommunicationForm.get('exit_hour').setValue(this.changesEdit.exit_hour ? moment(this.changesEdit.exit_hour, "HH:mm:ss").toDate(): null);
      this.applicationCommunicationForm.get('response').setValue(this.changesEdit.response);
      this.applicationCommunicationForm.get('observations').setValue(this.changesEdit.observations);
    }
    this.applicationCommunicationForm.get('application_id').setValue(this.application.id);
    this.applicationCommunicationForm.get('type').setValue(this.type);

    if (this.type == 'ENTRY') {
      this.applicationCommunicationForm.get('start_date').setValidators(Validators.required);
      this.applicationCommunicationForm.get('entry_hour').setValidators(Validators.required);
    } else {
      this.applicationCommunicationForm.get('end_date').setValidators(Validators.required);
      this.applicationCommunicationForm.get('exit_hour').setValidators(Validators.required);
    }
  }
  onSubmit() {
    this.submitted = true;
    this.checkValidityOfControls(this.applicationCommunicationForm.controls);
    if (this.applicationCommunicationForm.invalid) { return; }
    this.isLoading = true;
    if (!this.edit) {
      this.applicationCommunicationService.create(this.fillChangesObject()).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.APPLICATION_' + this.type + '_COMMUNICATION_CREATED'));
        this.modalRef.close(true);
      });
    } else {
      this.applicationCommunicationService.edit(this.changesEdit.id, this.fillChangesObject()).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.APPLICATION_' + this.type + '_COMMUNICATION_CHANGED'));
        this.modalRef.close(true);
      });
    }
  }
  close(): void {
    this.modalRef.close(false);
  }
  disabledDate = (current: Date) => {
    return !moment(current, "YYYY-MM-DD").isBetween(moment(this.application.start_date).format("YYYY-MM-DD"), moment(this.application.end_date).format("YYYY-MM-DD"), null, "[]")
      || moment(current, "YYYY-MM-DD").isBefore(moment(new Date()).format("YYYY-MM-DD"))
    //return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  checkValidityOfControls(controls) {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
  }

  fillChangesObject(){
    const obj = new CommunicationRequestModel();
    obj.start_date = this.applicationCommunicationForm.value.start_date;
    obj.end_date = this.applicationCommunicationForm.value.end_date;
    obj.entry_hour = this.applicationCommunicationForm.value.entry_hour && moment(this.applicationCommunicationForm.value.entry_hour).isValid() ? moment(this.applicationCommunicationForm.value.entry_hour).format("HH:mm:ss") : null ;
    obj.exit_hour = this.applicationCommunicationForm.value.exit_hour && moment(this.applicationCommunicationForm.value.exit_hour).isValid() ? moment(this.applicationCommunicationForm.value.exit_hour).format("HH:mm:ss") : null ;
    obj.type = this.applicationCommunicationForm.value.type;
    obj.application_id = this.applicationCommunicationForm.value.application_id;
    obj.response = this.applicationCommunicationForm.value.response;
    obj.observations = this.applicationCommunicationForm.value.observations;
    return obj;
  }

}
