import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationExtrasChangeModalComponent } from './application-extras-change-modal.component';

describe('ApplicationExtrasChangeModalComponent', () => {
  let component: ApplicationExtrasChangeModalComponent;
  let fixture: ComponentFixture<ApplicationExtrasChangeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationExtrasChangeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationExtrasChangeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
