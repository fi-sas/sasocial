import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { ExtraModel } from '@fi-sas/webpage/modules/accommodation/models/extra.model';
import { ResidencesService } from '@fi-sas/webpage/modules/accommodation/services/residences.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';

import * as moment from 'moment';
import { ExtraChangeRequestsService } from '@fi-sas/webpage/modules/accommodation/services/extra-change-requests.service';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-extras-change-modal',
  templateUrl: './application-extras-change-modal.component.html',
  styleUrls: ['./application-extras-change-modal.component.less']
})
export class ApplicationExtrasChangeModalComponent implements OnInit {
  file = null;
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  application: ApplicationModel = null;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  extras: ExtraModel[] = null;
  submitted = false;
  isLoading = false;
  extrasLoading = false;
  applicationChangeExtrasForm = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    extra_ids: new FormControl([]),
    reason: new FormControl('', [Validators.required]),
    application_id: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
  });
  get f() { return this.applicationChangeExtrasForm.controls; }

  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private translateService: TranslateService,
    private fileService:FilesService,
    private residencesService: ResidencesService,
    private applicationExtraChanges: ExtraChangeRequestsService
  ) { }

  ngOnInit() {
    
    if(!this.extras){
      this.extrasLoading = true;
      this.residencesService.getResidenceById(this.application.assigned_residence_id).pipe(
        first(), finalize(() => this.extrasLoading = false)
      ).subscribe(residences => {
        this.extras = residences.data[0].extras.filter((extra)=> {
          return extra.extra.visible && extra.extra.active;
        })
      });
    }
    if (this.edit || this.consult) {
      this.applicationChangeExtrasForm.get('start_date').setValue(this.changesEdit.start_date);
      this.applicationChangeExtrasForm.get('reason').setValue(this.changesEdit.reason);
      this.applicationChangeExtrasForm.get('extra_ids').patchValue(this.changesEdit.extras.map((e) => e.extra.id));
      this.applicationChangeExtrasForm.get('file_id').patchValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if(this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }
    }else{
      this.applicationChangeExtrasForm.patchValue({
        extra_ids: this.application.extras.map((e) => e.id)
      });
    }

    this.applicationChangeExtrasForm.get('application_id').setValue(this.application.id);
  }

  onSubmit() {
    let error = false;
    this.checkValidityOfControls([
      this.applicationChangeExtrasForm.get('start_date'),
      this.applicationChangeExtrasForm.get('extra_ids'),
      this.applicationChangeExtrasForm.get('reason'),
    ]);
    this.submitted = true;
    if (this.applicationChangeExtrasForm.invalid) { return; }

    if(this.application.extras.length>0 && this.application.extras.length == this.applicationChangeExtrasForm.get('extra_ids').value.length) {
      let aux = [];
      this.application.extras.forEach((appl)=>{
        this.applicationChangeExtrasForm.get('extra_ids').value.forEach(form => {
          if(appl.id == form) {
            aux.push(form);
          }
        });
      })
      if(aux.length>0 && aux.length == this.application.extras.length){
        error = true;
      }

    }

    if(error) {
      this.uiService.showMessage(MessageType.error, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.EXTRAS_EDIT_ERROR'));
      return;
    }
    this.isLoading = true;
    if(!this.edit) {
      this.applicationExtraChanges.create(this.applicationChangeExtrasForm.value).pipe(
        first(),finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.EXTRAS_CREATED'));
        this.modalRef.close(true);
      });
    }else {
      this.applicationExtraChanges.edit(this.changesEdit.id,this.applicationChangeExtrasForm.value).pipe(
        first(),finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.EXTRAS_EDIT'));
        this.modalRef.close(true);
      });
    }

  }

  close(): void {
    this.modalRef.close(false);
  }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }

}
