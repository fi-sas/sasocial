import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationIbanChangeModalComponent } from './application-iban-change-modal.component';

describe('ApplicationIbanChangeModalComponent', () => {
  let component: ApplicationIbanChangeModalComponent;
  let fixture: ComponentFixture<ApplicationIbanChangeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationIbanChangeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationIbanChangeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
