import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { ApplicationModel } from '../../models/application.model';
import { ApplicationIbanChangesService } from '../../services/application-iban-changes.service';

@Component({
  selector: 'app-application-iban-change-modal',
  templateUrl: './application-iban-change-modal.component.html',
  styleUrls: ['./application-iban-change-modal.component.less']
})
export class ApplicationIbanChangeModalComponent implements OnInit {


  changesEdit: ApplicationChangeModel;
  application: ApplicationModel = null;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  edit: boolean = false;
  consult: boolean = false;
  submitted = false;
  isLoading = false;
  file = null;
  applicationChangeIbanForm = new FormGroup({
    iban: new FormControl(null),
    allow_direct_debit: new FormControl(null, [Validators.required]),
    application_id: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
  });

  get f() { return this.applicationChangeIbanForm.controls; }

  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private fileService: FilesService,
    private applicationIbanChangesService: ApplicationIbanChangesService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    if (this.edit || this.consult) {
      this.applicationChangeIbanForm.get('allow_direct_debit').setValue(this.changesEdit.allow_direct_debit);
      this.applicationChangeIbanForm.get('iban').setValue(this.changesEdit.iban);
      this.applicationChangeIbanForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if (this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }

    }
    this.applicationChangeIbanForm.get('application_id').setValue(this.application.id);
  }

  changeValue(event) {
    if(event == true) {
      this.applicationChangeIbanForm.controls.iban.setValidators([Validators.required]);
      this.applicationChangeIbanForm.controls.file_id.setValidators([Validators.required]);
    }else {
      this.applicationChangeIbanForm.controls.iban.clearValidators();
      this.applicationChangeIbanForm.controls.file_id.clearValidators();
    }
    this.applicationChangeIbanForm.controls.iban.updateValueAndValidity();
    this.applicationChangeIbanForm.controls.file_id.updateValueAndValidity();
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file) => {
      this.file = file.data[0];
    })
  }

  close(): void {
    this.modalRef.close(false);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  onSubmit() {


    this.checkValidityOfControls([
      this.applicationChangeIbanForm.get('iban'),
      this.applicationChangeIbanForm.get('application_id'),
      this.applicationChangeIbanForm.get('allow_direct_debit'),
      this.applicationChangeIbanForm.get('file_id'),
    ]);

    this.submitted = true;
    if (this.applicationChangeIbanForm.invalid) { return; }
    this.isLoading = true;
    if (!this.edit) {
      this.applicationIbanChangesService.create(this.applicationChangeIbanForm.value).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.IBAN_CHANGE_CREATED'));
        this.modalRef.close(true);
      });
    }
    else {
      this.applicationIbanChangesService.edit(this.changesEdit.id, this.applicationChangeIbanForm.value).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.IBAN_CHANGE_EDITED'));
        this.modalRef.close(true);
      });
    }

  }


}
