import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationMaintenanceRequestModalComponent } from './application-maintenance-request-modal.component';

describe('ApplicationMaintenanceRequestModalComponent', () => {
  let component: ApplicationMaintenanceRequestModalComponent;
  let fixture: ComponentFixture<ApplicationMaintenanceRequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationMaintenanceRequestModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationMaintenanceRequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
