import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { MaintenanceRequestsService } from '@fi-sas/webpage/modules/accommodation/services/maintenance-requests.service';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ApplicationChangeModel } from '../../models/application-change.model';

@Component({
  selector: 'app-application-maintenance-request-modal',
  templateUrl: './application-maintenance-request-modal.component.html',
  styleUrls: ['./application-maintenance-request-modal.component.less']
})
export class ApplicationMaintenanceRequestModalComponent implements OnInit {
  file = null;
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  application: ApplicationModel = null;
  isLoading = false;
  submitted = false;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  applicationMaintenanceForm = new FormGroup({
    local: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required]),
    file_id: new FormControl(null, []),
    application_id: new FormControl('', [Validators.required]),
  });

  get f() { return this.applicationMaintenanceForm.controls; }


  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private fileService: FilesService,
    private translateService: TranslateService,
    private maintenanceRequestServices: MaintenanceRequestsService
  ) { }

  ngOnInit() {
    if (this.edit || this.consult) {
      this.applicationMaintenanceForm.get('local').setValue(this.changesEdit.local);
      this.applicationMaintenanceForm.get('description').setValue(this.changesEdit.description);
      this.applicationMaintenanceForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if(this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }
     
    }
    this.applicationMaintenanceForm.get('application_id').setValue(this.application.id);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }

  onSubmit() {
    this.checkValidityOfControls([
      this.applicationMaintenanceForm.get('local'),
      this.applicationMaintenanceForm.get('description'),
    ]);
    this.submitted = true;
    if (this.applicationMaintenanceForm.invalid) { return; }
    this.isLoading = true;
    if(!this.edit) {
      this.maintenanceRequestServices.create(this.applicationMaintenanceForm.value).pipe(
        first(), finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.MAINTENANCE_CREATED'));
        this.modalRef.close(true);
      });
    }else {
      this.maintenanceRequestServices.patch(this.changesEdit.id,this.applicationMaintenanceForm.value).pipe(
        first(), finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.MAINTENANCE_EDIT'));
        this.modalRef.close(true);
      });
    }
    
  }

  close(): void {
    this.modalRef.close(false);
  }

}
