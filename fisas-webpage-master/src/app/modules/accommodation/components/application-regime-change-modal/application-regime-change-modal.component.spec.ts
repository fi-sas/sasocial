import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationRegimeChangeModalComponent } from './application-regime-change-modal.component';

describe('ApplicationRegimeChangeModalComponent', () => {
  let component: ApplicationRegimeChangeModalComponent;
  let fixture: ComponentFixture<ApplicationRegimeChangeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationRegimeChangeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationRegimeChangeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
