import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { RegimeModel } from '@fi-sas/webpage/modules/accommodation/models/regime.model';
import { RegimeChangeRequestsService } from '@fi-sas/webpage/modules/accommodation/services/regime-change-requests.service';
import { ResidencesService } from '@fi-sas/webpage/modules/accommodation/services/residences.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import * as moment from 'moment';
import { finalize, first } from 'rxjs/operators';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-regime-change-modal',
  templateUrl: './application-regime-change-modal.component.html',
  styleUrls: ['./application-regime-change-modal.component.less']
})
export class ApplicationRegimeChangeModalComponent implements OnInit {
  application: ApplicationModel = null;
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  regimes: RegimeModel[] = null;
  submitted = false;
  isLoading = false;
  file = null;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  applicationChangeRegimeForm = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    regime_id: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
    application_id: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
  });
  get f() { return this.applicationChangeRegimeForm.controls; }

  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private fileService:FilesService,
    private translateService: TranslateService,
    private residencesService: ResidencesService,
    private regimeChangeRequestsService: RegimeChangeRequestsService
  ) { }


  ngOnInit() {
    if (this.edit || this.consult) {
      this.applicationChangeRegimeForm.get('start_date').setValue(this.changesEdit.start_date);
      this.applicationChangeRegimeForm.get('reason').setValue(this.changesEdit.reason);
      this.applicationChangeRegimeForm.get('regime_id').setValue(this.changesEdit.regime_id);
      this.applicationChangeRegimeForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if(this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }

    }
    this.applicationChangeRegimeForm.get('application_id').setValue(this.application.id);
  }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }
  onSubmit() {
    this.checkValidityOfControls([
      this.applicationChangeRegimeForm.get('start_date'),
      this.applicationChangeRegimeForm.get('regime_id'),
      this.applicationChangeRegimeForm.get('reason'),
    ]);
    this.submitted = true;
    if (this.applicationChangeRegimeForm.invalid) { return; }
    this.isLoading = true;
    if(!this.edit) {
      this.regimeChangeRequestsService.create(this.applicationChangeRegimeForm.value).pipe(
        first(), finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.REGIME_CREATED'));
        this.modalRef.close(true);
      });
    }else {
      this.regimeChangeRequestsService.edit(this.changesEdit.id, this.applicationChangeRegimeForm.value).pipe(
        first(), finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.REGIME_EDIT'));
        this.modalRef.close(true);
      });
    }

  }

  close(): void {
    this.modalRef.close(false);
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }

}
