import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationRoomChangesModalComponent } from './application-room-changes-modal.component';

describe('ApplicationRoomChangesModalComponent', () => {
  let component: ApplicationRoomChangesModalComponent;
  let fixture: ComponentFixture<ApplicationRoomChangesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationRoomChangesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationRoomChangesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
