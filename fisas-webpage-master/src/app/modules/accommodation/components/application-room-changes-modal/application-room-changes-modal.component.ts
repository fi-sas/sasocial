import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { ResidenceModel } from '@fi-sas/webpage/modules/accommodation/models/residence.model';
import { TypologyModel } from '@fi-sas/webpage/modules/accommodation/models/typology.model';
import { ResidencesService } from '@fi-sas/webpage/modules/accommodation/services/residences.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';
import { RoomChangeRequestsService } from '@fi-sas/webpage/modules/accommodation/services/room-change-requests.service';
import { ApplicationsService } from '@fi-sas/webpage/modules/accommodation/services/applications.service';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-room-changes-modal',
  templateUrl: './application-room-changes-modal.component.html',
  styleUrls: ['./application-room-changes-modal.component.less']
})
export class ApplicationRoomChangesModalComponent implements OnInit {
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  type: string = null;
  application: ApplicationModel = null;
  isLoading = false;
  typologies: TypologyModel[] = null;
  residences: ResidenceModel[] = null;
  submitted = false;
  loadingRoom = false;
  applicationChangeRoomForm = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    residence_id: new FormControl(null, []),
    typology_id: new FormControl(null, []),
    permute_user_tin: new FormControl(null, []),
    reason: new FormControl('', [Validators.required]),
    application_id: new FormControl('', [Validators.required]),
    type: new FormControl('', [Validators.required]),
    file_id: new FormControl(null, []),
  });
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  file = null;
  get f() { return this.applicationChangeRoomForm.controls; }
  constructor(
    private modalRef: NzModalRef,
    private applicationService: ApplicationsService,
    private uiService: UiService,
    private translateService: TranslateService,
    private residencesService: ResidencesService,
    private fileService: FilesService,
    private applicationRoomChangeRequests: RoomChangeRequestsService
  ) { }

  ngOnInit() {
    if (this.edit || this.consult) {
      if (this.type == 'RESIDENCE') {
        this.applicationChangeRoomForm.get('residence_id').setValue(this.changesEdit.residence_id);

      } else if (this.type == 'TYPOLOGY') {
        this.applicationChangeRoomForm.get('typology_id').setValue(this.changesEdit.typology_id);
      } else {
        this.applicationChangeRoomForm.get('permute_user_tin').setValue(this.changesEdit.permute_user_tin);
      }
      this.applicationChangeRoomForm.get('start_date').setValue(this.changesEdit.start_date);
      this.applicationChangeRoomForm.get('reason').setValue(this.changesEdit.reason);
      this.applicationChangeRoomForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if (this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }

    }
    switch (this.type) {
      case 'RESIDENCE':
        this.applicationChangeRoomForm.get('residence_id').setValidators([Validators.required]);
        break;
      case 'TYPOLOGY':
        this.applicationChangeRoomForm.get('typology_id').setValidators([Validators.required]);
        break;
      case 'PERMUTE':
        this.applicationChangeRoomForm.get('permute_user_tin').setValidators([Validators.required]);
        break;


    }
    this.residencesService.list(0, -1, "name", "ASC", "typologies").pipe(
      first(),
    ).subscribe(residences => {
      const actual_residence = residences.data.find(x => x.id == this.application.assigned_residence_id)
      this.typologies = actual_residence ? actual_residence.typologies : [];
      this.residences = residences.data.filter((data)=> {
        return data.id != this.application.assignedResidence.id;
      });
    });
    this.applicationChangeRoomForm.get('application_id').setValue(this.application.id);
    this.applicationChangeRoomForm.get('type').setValue(this.type);
  }

  onSubmit() {
    this.checkValidityOfControls([
      this.applicationChangeRoomForm.get('start_date'),
      this.applicationChangeRoomForm.get('residence_id'),
      this.applicationChangeRoomForm.get('typology_id'),
      this.applicationChangeRoomForm.get('permute_user_tin'),
      this.applicationChangeRoomForm.get('reason'),
      this.applicationChangeRoomForm.get('application_id'),
      this.applicationChangeRoomForm.get('type')
    ]);
    this.submitted = true;
    if (this.applicationChangeRoomForm.invalid) { return; }
    this.isLoading = true;
    if (!this.edit) {
      this.applicationRoomChangeRequests.create(this.applicationChangeRoomForm.value).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        switch (this.type) {
          case 'RESIDENCE':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.RESIDENCE_CREATED'));
            break;
          case 'PERMUTE':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.PERMUTE_CREATED'));
            break;
          case 'TYPOLOGY':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.TYPOLOGY_CREATED'));
            break;
          case 'ROOM':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.ROOM_CREATED'));
            break;
        }
        this.modalRef.close(true);
      });
    } else {
      this.applicationRoomChangeRequests.edit(this.changesEdit.id, this.applicationChangeRoomForm.value).pipe(
        first(), finalize(() => this.isLoading = false)
      ).subscribe(changes => {
        switch (this.type) {
          case 'RESIDENCE':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.RESIDENCE_EDIT'));
            break;
          case 'PERMUTE':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.PERMUTE_EDIT'));
            break;
          case 'TYPOLOGY':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.TYPOLOGY_EDIT'));
            break;
          case 'ROOM':
            this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.ROOM_EDIT'));
            break;
        }
        this.modalRef.close(true);
      });
    }

  }

  close(): void {
    this.modalRef.close(false);
  }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

 /* filterNif() {

    if (this.applicationChangeRoomForm.get('permute_user_tin').value) {
      this.loadingRoom = true;
      this.applicationService.list(1, -1).pipe(first(), finalize(() => this.loadingRoom = false)).subscribe((data) => {

        this.filterRoom = true;
        if (data.data.length > 0) {
          const aux = data.data.filter((dataApp) => {
            return dataApp.status === 'contracted' && dataApp.tin === this.applicationChangeRoomForm.get('permute_user_tin').value
          });

          if (aux.length > 0) {
            this.room = aux[0].room.name;
          } else {
            this.room = '';
          }
        }
      })
    } else {
      this.checkValidityOfControls([
        this.applicationChangeRoomForm.get('permute_user_tin'),
      ]);
    }
  }*/

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file) => {
      this.file = file.data[0];
    })
  }
}
