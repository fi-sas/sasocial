import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationScholarshipChangeModalComponent } from './application-scholarship-change-modal.component';

describe('ApplicationScholarshipChangeModalComponent', () => {
  let component: ApplicationScholarshipChangeModalComponent;
  let fixture: ComponentFixture<ApplicationScholarshipChangeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationScholarshipChangeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationScholarshipChangeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
