import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import * as moment from 'moment';
import { TariffChangeRequestsService } from '@fi-sas/webpage/modules/accommodation/services/tariff-change-requests.service';
import { finalize, first } from 'rxjs/operators';
import { ApplicationChangeModel } from '../../models/application-change.model';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-scholarship-change-modal',
  templateUrl: './application-scholarship-change-modal.component.html',
  styleUrls: ['./application-scholarship-change-modal.component.less']
})
export class ApplicationScholarshipChangeModalComponent implements OnInit {
  file = null;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  edit: boolean = false;
  consult: boolean = false;
  changesEdit: ApplicationChangeModel;
  application: ApplicationModel = null;
  submitted = false;
  isLoading = false;
  applicationChangeScholarshipForm = new FormGroup({
    start_date: new FormControl(null, [Validators.required]),
    has_scholarship: new FormControl(null, [Validators.required]),
    reason: new FormControl('', [Validators.required]),
    application_id: new FormControl('', [Validators.required]),
    file_id: new FormControl(null, []),
  });

  get f() { return this.applicationChangeScholarshipForm.controls; }


  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private fileService: FilesService,
    private translateService: TranslateService,
    private tariffChangeRequestsService: TariffChangeRequestsService
  ) { }

  ngOnInit() {
    if (this.edit || this.consult) {
      this.applicationChangeScholarshipForm.get('start_date').setValue(this.changesEdit.start_date);
      this.applicationChangeScholarshipForm.get('reason').setValue(this.changesEdit.reason);
      this.applicationChangeScholarshipForm.get('has_scholarship').setValue(this.changesEdit.has_scholarship);
      this.applicationChangeScholarshipForm.get('file_id').setValue(this.changesEdit.file_id ? this.changesEdit.file_id : null);
      if(this.consult && this.changesEdit.file_id) {
        this.loadFile();
      }

    }
    this.applicationChangeScholarshipForm.get('application_id').setValue(this.application.id);
  }

  loadFile() {
    this.fileService.file(this.changesEdit.file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }

  close(): void {
    this.modalRef.close(false);
  }

  disabledDate = (current: Date) => {
    return moment(current).isBefore(this.application.start_date) || moment(current).isAfter(this.application.end_date);
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }

  onSubmit() {
    this.checkValidityOfControls([
      this.applicationChangeScholarshipForm.get('start_date'),
      this.applicationChangeScholarshipForm.get('has_scholarship'),
      this.applicationChangeScholarshipForm.get('reason'),
      this.applicationChangeScholarshipForm.get('application_id'),
    ]);
    this.submitted = true;
    if (this.applicationChangeScholarshipForm.invalid) { return; }
    this.isLoading = true;
    if(!this.edit) {
      this.tariffChangeRequestsService.create(this.applicationChangeScholarshipForm.value).pipe(
        first(),finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.SCHOLARSHIP_CREATED'));
        this.modalRef.close(true);
      });
    }else {
      this.tariffChangeRequestsService.edit(this.changesEdit.id,this.applicationChangeScholarshipForm.value).pipe(
        first(),finalize(()=>this.isLoading = false)
      ).subscribe(changes => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('ACCOMMODATION.CHANGE_REQUESTS.SCHOLARSHIP_EDIT'));
        this.modalRef.close(true);
      });
    }

  }


}
