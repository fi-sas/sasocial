import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";

@Component({
  selector: "app-application-navbar",
  templateUrl: "./application-navbar.component.html",
  styleUrls: ["./application-navbar.component.less"],
})
export class ApplicationNavbarComponent implements OnInit {
  barOptions: any[] = [];

  constructor(private authService: AuthService, private route: ActivatedRoute) {
    const id = this.route.snapshot.params.id;

    this.barOptions = [
      {
        translate: "ACCOMMODATION.CHANGE_REQUESTS.PAGE_TITLE",
        disable: false,
        routerLink: "/accommodation/application-view/" + id,
        scope: this.authService.hasPermission(
          "accommodation:applications:create"
        ),
      },
      {
        translate: "ACCOMMODATION.CHANGE_REQUESTS.PAGE_TITLE",
        disable: false,
        routerLink: "/accommodation/change-contract/" + id,
        scope: this.authService.hasPermission(
          "accommodation:applications:create"
        ),
      },
      {
        translate: "ACCOMMODATION.CHANGE_REQUESTS.TITLE",
        disable: false,
        routerLink: "/accommodation/history-change-contract/" + id,
        scope: this.authService.hasPermission(
          "accommodation:applications:read"
        ),
      },
      {
        translate: "ACCOMMODATION.BILLINGS_TITLE",
        disable: false,
        routerLink: "/accommodation/billings/" + id,
        scope: this.authService.hasPermission("accommodation:billings:read"),
      },
    ];
  }

  ngOnInit(): void {}
}
