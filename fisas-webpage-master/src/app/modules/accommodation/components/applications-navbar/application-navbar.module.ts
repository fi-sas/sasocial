import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ApplicationNavbarComponent } from "./application-navbar.component";
import { RouterModule } from "@angular/router";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";

@NgModule({
  declarations: [ApplicationNavbarComponent],
  imports: [CommonModule, SharedModule, RouterModule],
  exports: [ApplicationNavbarComponent],
})
export class ApplicationNavbarModule {}
