import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApplicationCardPresentAccommodationComponent } from './card-application-present.component';


describe('ApplicationCardPresentAccommodationComponent', () => {
  let component: ApplicationCardPresentAccommodationComponent;
  let fixture: ComponentFixture<ApplicationCardPresentAccommodationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationCardPresentAccommodationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationCardPresentAccommodationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
