import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { TranslateService } from "@ngx-translate/core";
import { NzModalService } from "ng-zorro-antd";
import { hasOwnProperty } from "tslint/lib/utils";
import {
  ApplicationModel,
  ApplicationStatus,
  ConfigStatus,
} from "../../models/application.model";

@Component({
  selector: "app-application-card-present-accommodation",
  templateUrl: "./card-application-present.component.html",
  styleUrls: ["./card-application-present.component.less"],
})
export class ApplicationCardPresentAccommodationComponent {
  @Input() application: ApplicationModel = null;
  @Input() canRenew: boolean = false;
  @Output() cancel = new EventEmitter();
  @Output() oppose = new EventEmitter();
  @Output() reject = new EventEmitter();
  @Output() accept = new EventEmitter();
  @Output() withdrawal = new EventEmitter();

  configStatus = ConfigStatus;
  applicationsStatus = ApplicationStatus;
  daycurrent: Date = new Date();
  modalDetail = false;
  userId = 0;
  buttonReject = {
    "text-align": "center",
    "margin-top": "5px",
  };

  buttonAccept = {
    "text-align": "center",
    "margin-top": "5px",
  };

  constructor(
    private router: Router,
    public translate: TranslateService,
    private userService: AuthService,
    private modalService: NzModalService,
    private translateService: TranslateService
  ) {
    const user = this.userService.getUser();

    if (hasOwnProperty(user, "id")) {
      this.userId = user.id;
    }
  }

  showAssignedResidence(): boolean {
    const states = [
      "assigned",
      "confirmed",
      "rejected",
      "contracted",
      "closed",
    ];
    return (
      states.includes(this.application.status) &&
      this.application.assigned_residence_id != null
    );
  }

  editApplication() {
    this.router.navigateByUrl(
      "accommodation/list-residences/form-application/" + this.application.id
    );
  }

  goToReview(id: number) {
    this.router.navigateByUrl("accommodation/application-view/" + id);
  }

  modalCancel() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CANCEL_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancelApplication(),
    });
  }

  cancelApplication() {
    this.cancel.emit(this.application.id);
  }

  modalOppose() {
    this.oppose.emit(this.application.id);
  }

  modalRejected() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.REJECTED"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.rejectedApplication(),
    });
  }

  rejectedApplication() {
    this.reject.emit(this.application.id);
  }

  modalConfirm() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CONFIRM_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.acceptedApplication(),
    });
  }
  modalWithdrawal() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.WITHDRAWAL_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.withdrawalApplication(),
    });
  }
  withdrawalApplication() {
    this.withdrawal.emit(this.application.id);
  }

  acceptedApplication() {
    this.accept.emit(this.application.id);
  }

  openModal($event: MouseEvent) {
    $event.stopPropagation();

    this.modalDetail = true;
  }

  isPair(val) {
    return val % 2 == 0;
  }

  renewApplication() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_NO"
      ),
      nzOkText: this.translateService.instant(
        "ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_YES"
      ),
      nzOnOk: () => this.confirmRenewApplication(),
    });
  }

  confirmRenewApplication() {
    this.router.navigate(
      ["accommodation", "list-residences", "form-application"],
      {
        queryParams: {
          application_id: this.application.id,
        },
      }
    );
  }
}
