import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApplicationCardAccommodationComponent } from './card-application.component';


describe('ApplicationCardAccommodationComponent', () => {
  let component: ApplicationCardAccommodationComponent;
  let fixture: ComponentFixture<ApplicationCardAccommodationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationCardAccommodationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationCardAccommodationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
