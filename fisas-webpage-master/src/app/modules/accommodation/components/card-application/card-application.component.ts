import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { TranslateService } from "@ngx-translate/core";
import { hasOwnProperty } from "tslint/lib/utils";
import { ApplicationModel, ConfigStatus } from "../../models/application.model";

@Component({
  selector: "app-application-card-accommodation",
  templateUrl: "./card-application.component.html",
  styleUrls: ["./card-application.component.less"],
})
export class ApplicationCardAccommodationComponent {
  @Input() application: ApplicationModel = null;
  daycurrent: Date = new Date();
  configStatus = ConfigStatus;
  modalDetail = false;
  userId = 0;
  constructor(
    private router: Router,
    public translate: TranslateService,
    private userService: AuthService
  ) {
    const user = this.userService.getUser();
    if (hasOwnProperty(user, "id")) {
      this.userId = user.id;
    }
  }

  goToReview(id: number) {
    this.router.navigateByUrl("accommodation/application-view/" + id);
  }

  showAssignedResidence(): boolean {
    const states = [
      "assigned",
      "confirmed",
      "rejected",
      "contracted",
      "closed",
    ];
    return (
      states.includes(this.application.status) &&
      this.application.assigned_residence_id != null
    );
  }

  openModal($event: MouseEvent) {
    $event.stopPropagation();

    this.modalDetail = true;
  }

  isPair(val) {
    return val % 2 == 0;
  }

  consultHistoryChanges($event: MouseEvent) {
    $event.stopPropagation();
    this.router.navigate([
      "accommodation",
      "history-change-contract",
      this.application.id,
    ]);
  }

  consultBillings($event: MouseEvent) {
    $event.stopPropagation();
    this.router.navigate(["accommodation", "billings", this.application.id]);
  }
}
