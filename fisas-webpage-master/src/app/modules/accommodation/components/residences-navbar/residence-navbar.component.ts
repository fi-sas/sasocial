import { Component, OnInit } from "@angular/core";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";

@Component({
  selector: "app-residence-navbar",
  templateUrl: "./residence-navbar.component.html",
  styleUrls: ["./residence-navbar.component.less"],
})
export class ResidenceNavbarComponent implements OnInit {
  barOptions: any[] = [];

  constructor(private authService: AuthService) {
    this.barOptions = [
      {
        translate: "ACCOMMODATION.MENU.RESIDENCE",
        disable: true,
        routerLink: "/accommodation/list-residences",
        scope: this.authService.hasPermission("accommodation:residences:read"),
      },
      {
        translate: "ACCOMMODATION.MENU.MY_APPLICATIONS",
        disable: false,
        routerLink: "/accommodation/list-applications",
        scope: this.authService.hasPermission(
          "accommodation:applications:read"
        ),
      },
    ];
  }

  ngOnInit(): void {}
}
