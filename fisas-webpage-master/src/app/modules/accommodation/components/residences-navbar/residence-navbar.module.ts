import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ResidenceNavbarComponent } from "./residence-navbar.component";
import { RouterModule } from "@angular/router";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";

@NgModule({
  declarations: [ResidenceNavbarComponent],
  imports: [CommonModule, SharedModule, RouterModule],
  exports: [ResidenceNavbarComponent],
})
export class ResidenceNavbarModule {}
