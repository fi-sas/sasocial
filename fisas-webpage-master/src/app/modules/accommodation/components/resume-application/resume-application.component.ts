import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { ReportsService } from "@fi-sas/webpage/core/services/reports.service";
import { UiService, MessageType } from "@fi-sas/webpage/core/services/ui.service";
import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import { TranslateService } from "@ngx-translate/core";
import { finalize, first } from "rxjs/operators";
import { ApplicationModel } from "../../models/application.model";
import { ExtraModel } from "../../models/extra.model";
import { RegimeModel } from "../../models/regime.model";
import { TypologyModel } from "../../models/typology.model";
import { ApplicationPhasesService } from "../../services/application-phases.service";
import { ApplicationsService } from "../../services/applications.service";

@Component({
    selector: 'app-resume-application',
    templateUrl: './resume-application.component.html',
    styleUrls: ['./resume-application.component.less']
})
export class ResumeApplicationComponent implements OnInit {
    @Input() form: boolean = false;
    @Input() application: ApplicationModel = new ApplicationModel();
    @Input() nameResidenceSelected: string = '';
    @Input() nameUsedResidenceSelected: string = '';
    @Output() currentValue = new EventEmitter;
    @Input() nameRoom: string = '';
    @Input() nameOrganic: string = ''
    @Input() nameCourseDegree: string = '';
    @Input() nameCourse: string = '';
    @Input() descPatrimony: string = '';
    @Input() sendRegime: RegimeModel;
    @Input() sendTypologies: TypologyModel;
    @Input() residenceName: string = '';
    @Input() extrasSend: ExtraModel[] = [];
    @Input() documentType: DocumentTypeModel;

    loadingFiles: boolean = false;
    loadingPrint: boolean = false;
    filesIncomeStatement = [];
    filesOthers = [];
    successModalReport = false;
    year: number;

    constructor(private fileService: FilesService,
        private uiService:UiService, private translateService: TranslateService, private reportsService: ReportsService,
        private applicationService: ApplicationsService, private applicationPhasesService: ApplicationPhasesService) {
            this.getPhases();
         }

    ngOnInit() {
        if (!this.form) {
            if (this.application.income_statement_files.length > 0) {
                this.application.income_statement_files.forEach(element => {
                    this.filesIncomeStatement.push(element.file);
                });
            }
            if (this.application.files && this.application.files.length > 0) {
                this.application.files.forEach(element => {
                    this.filesOthers.push(element);
                });
            }
        } else {
            if(this.application.income_statement_files_ids && this.application.income_statement_files_ids.length>0) {
                 this.loadMedias();
            }
            if(this.application.file_ids && this.application.file_ids.length>0) {
                this.loadFiles();
           }
           
        }

    }

    loadMedias() {
        this.fileService
            .listfiles(this.convertGalleryToFilesIDS())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.filesIncomeStatement = files.data;
                    this.loadingFiles = false;
                },
                () => {
                    this.loadingFiles = false;
                }
            );
    }

    loadFiles() {
        this.fileService
            .listfiles(this.convertGalleryToFilesIDSOthers())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.filesOthers = files.data;
                    this.loadingFiles = false;
                },
                () => {
                    this.loadingFiles = false;
                }
            );
    }

    convertGalleryToFilesIDS() {
        let filesIDS = [];
        this.application.income_statement_files_ids.forEach((fileID) => {
            filesIDS.push(fileID);
        });

        return filesIDS;
    }

    convertGalleryToFilesIDSOthers() {
        let filesIDS = [];
        this.application.file_ids.forEach((fileID) => {
            filesIDS.push(fileID);
        });

        return filesIDS;
    }

    changeValue(value: number) {
        this.currentValue.emit(value);
    }

    print() {
        this.loadingPrint = true;
        this.applicationService.print(this.application.id).pipe(first(),
            finalize(() => this.loadingPrint = false)).subscribe(() => {
                this.successModalReport = true;
                this.reportsService.loadData(false);
            })
    }

    getPhases() {
        this.applicationPhasesService.openApplication().pipe(first()).subscribe((data) => {
          if (data.data.length > 0) {
            this.year = Number(data.data[0].academic_year.split('-')[0]);
          }
    
        })
      }
}