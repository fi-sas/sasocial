import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ResumeApplicationComponent } from "./resume-application.component";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";

@NgModule({
  declarations: [ResumeApplicationComponent],
  exports: [ResumeApplicationComponent],
  imports: [CommonModule, SharedModule],
})
export class ResumeApplicationModule {}
