import { ApplicationModel } from "../../u-bike/models/application.model";

export class ApplicationIbanChangeRequestModel {
    id: number;
    iban: string;
    allow_direct_debit: boolean;
    application_id: number;
    application: ApplicationModel;
    file_id: number;
    file: File;
    created_at: Date;
    update_at: Date;
}