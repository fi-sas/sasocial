export class AbsenceModel {
  id?: number;
  application_id?: number;
  user_id?: number;
  start_date: string;
  end_date: string;
  reason: string;
  file_id: number;
  allow_booking: boolean;
  updated_at?: string;
  created_at?: string;
}
