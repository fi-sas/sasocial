export class ApplicantInfoModel {
  full_name: string;
  identification: string;
  tin: string;
  student_number: string;
  course: string;
  course_degree: string;
  course_year: number;
  registry_validity: Date;
  admission_date: Date;
  email: string;
  phone_1: string;
  phone_2: string;
  address: string;
  city: string;
  postal_code: string;
  country: string;
  applicant_birth_date: Date;
  course_degree_id: number;
  course_id: number;
  organic_unit_id: number;
  gender: string;
  document_type_id: number;
  nationality: string;
}
