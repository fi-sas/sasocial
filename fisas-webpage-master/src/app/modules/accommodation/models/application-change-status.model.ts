import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';

export class ApplicationChangeStatusModel {
  event: string;
  notes: string;
  application?: ApplicationModel;
}
