import { FileModel } from "../../media/models/file.model";
import { ApplicationModel } from "../../u-bike/models/application.model";
import { ExtraModel } from "./extra.model";
import { RegimeModel } from "./regime.model";
import { ResidenceModel } from "./residence.model";
import { RoomModel } from "./room.model";
import { TariffModel } from "./tariff.model";
import { TypologyModel } from "./typology.model";

export class ExtrasDTO {
    application_extra_change_id: number;
    extra: ExtraModel;
    extra_id: number;
    id: number;
}


export enum ApplicationChangeStatus {
    SUBMITTED = 'SUBMITTED',
    ANALYSIS = 'ANALYSIS',
    DISPATCH = 'DISPATCH',
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED',
    CANCELLED = 'CANCELLED',
    APPROVED_WITH_SUSPENSION = 'APPROVED_WITH_SUSPENSION',
    APPROVED_WITHOUT_SUSPENSION = 'APPROVED_WITHOUT_SUSPENSION',
    RESOLVING = "RESOLVING",
    RESOLVED = "RESOLVED",
    CLOSED = "CLOSED",
    EXPIRED = "EXPIRED"
}

export class ApplicationChangeModel {
    id?: number;
    start_date?: Date;
    has_scholarship: boolean;
    end_date?: Date;
    exit_date?: Date;
    reason?: string;
    application?: ApplicationModel;
    application_id?: number;
    file?: FileModel;
    file_id?: number;
    allow_booking?: number;
    status?: string;
    decision?: string;
    updated_at?: Date;
    created_at?: Date;
    history?: any[];
    observations?: string;
    residence_id?: number
    residence?: ResidenceModel;
    room_id?: number
    room?: RoomModel;
    permute_user_tin?: string;
    type?: string;
    typology_id?: number;
    typology?: TypologyModel;
    user_id?: number;
    extras?: ExtrasDTO[];
    regime_id?: number;
    regime?: RegimeModel;
    description?: string;
    local?: string;
    tariff_id?: number;
    tariff: TariffModel;
    old_tariff_id?: number;
    old_tariff: TariffModel;
    has_scholarhip?: boolean;
    response: string;
    iban?: string;
    allow_direct_debit?: boolean;
    entry_hour?: string;
    exit_hour?: string;
}


export const ApplicationChangeStatusColor = [
    {
        name: ApplicationChangeStatus.SUBMITTED,
        color: '#076743'
    },
    {
        name: ApplicationChangeStatus.ANALYSIS,
        color: '#f8ca00'
    },
    {
        name: ApplicationChangeStatus.DISPATCH,
        color: '#0d69dd'
    },
    {
        name: ApplicationChangeStatus.APPROVED,
        color: '#076743'
    },
    {
        name: ApplicationChangeStatus.APPROVED_WITH_SUSPENSION,
        color: '#076743'
    },
    {
        name: ApplicationChangeStatus.APPROVED_WITH_SUSPENSION,
        color: '#d0021b'
    },
    {
        name: ApplicationChangeStatus.REJECTED,
        color: '#d0021b'
    },
    {
        name: ApplicationChangeStatus.CANCELLED,
        color: '#d0021b'
    },
    {
        name: ApplicationChangeStatus.RESOLVING,
        color: '#f8ca00'
    },
    {
        name: ApplicationChangeStatus.RESOLVED,
        color: '#076743'
    },
    {
        name: ApplicationChangeStatus.CLOSED,
        color: '#d0021b'
    },
    {
        name: ApplicationChangeStatus.EXPIRED,
        color: '#d0021b'
    },
];


