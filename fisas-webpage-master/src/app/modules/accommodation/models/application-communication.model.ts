import { ApplicationModel } from "../../u-bike/models/application.model";

export enum CommunicationRequestStatus {
    SUBMITTED = 'SUBMITTED',
    REJECTED = 'CLOSED',
}

export class CommunicationRequestModel {
    id: number;
    application_id: number;
    application: ApplicationModel;
    start_date?: Date;
    end_date?: Date;
    observations: string;
    response: string;
    status: CommunicationRequestStatus;
    created_at: Date;
    type: String;
    entry_hour: string;
    exit_hour: string;
}
