import { ResidenceModel } from './residence.model';

export class ApplicationPhase {
    id: number;
    start_date: string;
    end_date: string;
    out_of_date_from: string;
    academic_year: string;
    application_phase: number;
    updated_at: string;
    created_at: string;
    out_of_date_validation: boolean;
    out_of_date_validation_days: number;
    fixed_accommodation_period: boolean;
}