import { ResidenceModel } from '@fi-sas/webpage/modules/accommodation/models/residence.model';
import { RoomModel } from '@fi-sas/webpage/modules/accommodation/models/room.model';
import { ApplicationStatus } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { HistoryModel } from '@fi-sas/webpage/modules/accommodation/models/history.model';

export class ApplicationStatusModel {
  status: ApplicationStatus;
  start_date: Date;
  end_date: Date;
  history: HistoryModel[];
  residence: ResidenceModel;
  room: RoomModel;
}
