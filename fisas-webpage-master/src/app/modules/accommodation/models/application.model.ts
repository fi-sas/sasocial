import { ResidenceModel } from './residence.model';
import { RegimeModel } from './regime.model';
import { HistoryModel } from '@fi-sas/webpage/modules/accommodation/models/history.model';
import { RoomModel } from '@fi-sas/webpage/modules/accommodation/models/room.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { ExtraModel } from './extra.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { TypologyModel } from './typology.model';

export enum ApplicationStatus {
  DRAFT = 'drafted',
  SUBMITTED = 'submitted',
  ANALYSED = 'analysed',
  CANCELLED = 'cancelled',
  PENDING = 'pending',
  NOTACCEPTED = 'notAccepted',
  UNASSIGNED = 'unassigned',
  QUEUED = 'queued',
  ASSIGNED = 'assigned',
  CONFIRMED = 'confirmed',
  REJECTED = 'rejected',
  OPPOSITION = 'opposition',
  CONTRACTED = 'contracted',
  REOPENED = 'reopened',
  CLOSED = 'closed',
  WITHDRAWAL = "withdrawal",
}

export enum AccommodationRegime {
  PC = 'PC', MP = 'MP', ALO = 'ALO'
}

export enum AccomodationPaymentMethod {
  CC = 'CC', MB = 'MB', DD = 'DD', 'NUM' = 'NUM'
}

export class ApplicationModel {
  house_hould_elements: HouseHoldElementsModel[];
  id: number;
  file_ids: any[];
  files: any[];
  full_name: string;
  identification: number;
  document_type_id: number;
  document_type: DocumentTypeModel;
  birth_date: Date;
  applicant_birth_date: Date;
  tin: number;
  nationality: string;
  gender: string;
  father_name: string;
  mother_name: string;
  incapacity: boolean;
  incapacity_type: string;
  incapacity_degree: string;
  student_number: string;
  course_id: number;
  course_degree_id: number;
  course_degree: string;
  course_year: number;
  admission_date: string;
  academic_year: string;
  email: string;
  secundary_email: string;
  phone_1: string;
  phone_2: string;
  address: string;
  city: string;
  postal_code: string;
  country: string;
  residence_id: number;
  assigned_residence_id: number;
  second_option_residence_id: number;
  accommodation_regime: AccommodationRegime;
  has_used_residences_before: boolean;
  international_student: boolean;
  which_residence_id: number;
  whichResidence?: ResidenceModel;
  which_room_id: number;
  whichRoom: RoomModel;
  income_source: string[];
  other_income_source: string;
  applied_scholarship: boolean;
  scholarship_value: number;
  income_statement_delivered: boolean;
  income_statement_files_ids: [];
  income_statement_files: any[];
  payment_method_id: AccomodationPaymentMethod;
  iban: string;
  allow_direct_debit: boolean;
  regime_id: number;
  current_regime_id: number;
  applicant_consent: boolean;
  consent_date: string;
  household_distance: number;
  household_elements_number: number;
  patrimony_category_id: number;
  application_phase: string;
  patrimony_category: string;
  household_total_income: number;
  househould_elements_in_university: number;
  start_date: Date;
  end_date: Date;
  room_id: number;
  user_id: number;
  signed_contract_file_id: number;
  liability_term_file_id: number;
  observations: string;
  updated_at: Date;
  created_at: Date;
  status: ApplicationStatus;
  residence: ResidenceModel;
  //second_option_residence: number;
  second_option_residence: ResidenceModel;
  assignedResidence: ResidenceModel;
  regime: RegimeModel;
  currentRegime: RegimeModel;
  history?: HistoryModel[];
  room?: RoomModel;
  course?: CourseModel;
  extras?: ExtraModel[];
  extra_ids?: number[];
  has_opposed: boolean;
  opposition_answer: string;
  opposition_request: string;
  organic_unit_id: number;
  organic_unit: OrganicUnitModel;
  preferred_typology: TypologyModel;
  preferred_typology_id: number;
  unassigned_date?: Date;
}

export class HouseHoldElementsModel {
  kinship: string;
  profession: string;
}

export const ConfigStatus = [
  {
    name: ApplicationStatus.ANALYSED,
    color: '#7768AE'
  },
  {
    name: ApplicationStatus.SUBMITTED,
    color: '#4D9DE0'
  },
  {
    name: ApplicationStatus.CANCELLED,
    color: '#A4A4A4'
  },
  {
    name: ApplicationStatus.ASSIGNED,
    color: '#076743'
  },
  {
    name: ApplicationStatus.CLOSED,
    color: '#000000'
  },
  {
    name: ApplicationStatus.CONFIRMED,
    color: '#1ba974'
  },
  {
    name: ApplicationStatus.CONTRACTED,
    color: '#1ba974'
  },
  {
    name: ApplicationStatus.NOTACCEPTED,
    color: '#C1666B'
  },
  {
    name: ApplicationStatus.PENDING,
    color: '#f8ca00'
  },
  {
    name: ApplicationStatus.REJECTED,
    color: '#D0021B'
  },
  {
    name: ApplicationStatus.UNASSIGNED,
    color: '#C1666B'
  },
  {
    name: ApplicationStatus.OPPOSITION,
    color: '#ed9600'
  },
  {
    name: ApplicationStatus.REOPENED,
    color: '#076743'
  },
  {
    name: ApplicationStatus.QUEUED,
    color: '#D4B483',
  },
  {
    name: ApplicationStatus.DRAFT,
    color: '#f8ca00',
  },
  {
    name: ApplicationStatus.WITHDRAWAL,
    color: '#246A73',
  }
];


export class UserCanApplyModel {
  can: boolean;
  reason: string;
  renew: boolean;
  renew_application_id: number;
}

