import { TaxModel } from "@fi-sas/webpage/shared/models/tax.model";
import { ExtraModel } from "./extra.model";
import { RegimeModel } from "./regime.model";
import { TypologyModel } from "./typology.model";

export enum TypologyPriceLinePeriod {
    MONTH = 'MONTH',
    WEEK = 'WEEK',
    DAY = 'DAY',
}
export class BillingItemModel {
    id?: number;
    billing_id: number;
    product_code: string;
    name: string;
    description: string;
    typology_id: number;
    typology?: TypologyModel;
    regime_id: number;
    regime?: RegimeModel;
    extra_id: number;
    extra?: ExtraModel;
    vat_id: number;
    vat?: TaxModel;
    vat_value: number;
    quantity: number;
    unit_price: number;
    price: number;
    period: TypologyPriceLinePeriod;
    possible_retroactive?: number;
    created_at?: Date;
    updated_at?: Date;
}
