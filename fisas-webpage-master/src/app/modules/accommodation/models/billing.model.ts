import { ApplicationModel } from './application.model';
import { BillingItemModel } from './billing-item.model';

export class BillingModel {
    id?: number;
    application?: ApplicationModel;
    application_id: number;
    year: number;
    month: number;
    billing_start_date: Date;
    billing_end_date: Date;
    status: string;
    possible_retroactive: number;
    due_date: Date;
    paid_at: Date;
    movement_id: number;
    created_at?: Date;
    updated_at?: Date;
    billing_items?: BillingItemModel[];
}

export const BillingStatusTranslations = {
    PROCESSED: { label: 'Processado', color: 'blue' },
    REVIEWED: { label: 'Validado', color: 'lime' },
    BILLED: { label: 'Faturado', color: 'orange' },
    PAID: { label: 'Pago', color: 'green' },
    CANCELLED: { label: 'Cancelado', color: 'red' },
};
