export enum ContractActions {
  EXTENSIONS = 'extensions',
  WITHDRAWALS = 'withdrawals',
  ABSENCES = 'absences'
}

export class ContractModel {
  contract_action: ContractActions;
  application_id: number;
  date: string[];
  start_date?: string;
  end_date?: string;
  reason: string;
  file_id?: number;
  allow_booking?: boolean;
}



