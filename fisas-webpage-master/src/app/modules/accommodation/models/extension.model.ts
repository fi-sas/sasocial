import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';

export enum ExtensionStatus {
  WAITING_APPROVAL = 'Waiting Approval',
  APPROVED = 'Approved',
  CANCELLED = 'Cancelled'
}

export class ExtensionModel {
  id?: number;
  start_date: string;
  end_date: string;
  reason: string;
  application_id?: number;
  user_id?: number;
  status?: ExtensionStatus;
  application?: ApplicationModel;
  history?: ExtensionHistory[];
  updated_at?: string;
  created_at?: string;
}

export class ExtensionHistory {
  id: number;
  extension_id: number;
  status: ExtensionStatus;
  user_id: number;
  notes: string;
  updated_at: Date;
  created_at: Date;
}
