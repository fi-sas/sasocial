import { ApplicationModel } from "../../u-bike/models/application.model";
import { ExtraModel } from "./extra.model";

export enum ExtraChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    ANALYSIS = 'ANALYSIS',
    DISPATCH = 'DISPATCH',
    APPROVED = 'APPROVED',
    CANCELLED = 'CANCELLED',
    REJECTED = 'REJECTED',
}
export enum ExtraChangeRequestDecision {
    APPROVE = 'APPROVE',
    REJECT = 'REJECT',
}

export class ExtraChangeRequestHistoryModel {
    id: number;
    status: string;
    user_id: number;
    notes: string;
    created_at: Date;
    update_at: Date;
}

export class ExtrasDTO {
    application_extra_change_id: number;
    extra: ExtraModel;
    extra_id: number;
}

export class ExtraChangeRequestModel {
    id: number;
    reason: string;
    application_id: number;
    application: ApplicationModel;

    start_date: Date;
    observations: string;
    decision: ExtraChangeRequestDecision;
    status: ExtraChangeRequestStatus;
    created_at: Date;
    update_at: Date;
    history: ExtraChangeRequestHistoryModel[];
    extras: ExtrasDTO[];
}
