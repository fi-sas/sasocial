import { TaxModel } from '@fi-sas/webpage/shared/models/tax.model';

export class ExtraTranslationModel  {
    language_id: number;
    name: string;
}

export class ExtraModel {
    id: number;
    translations: ExtraTranslationModel[];
    product_code: string;
    billing_name: string;
    vat_id: number;
    vat?: TaxModel;
    price: number;
    visible: boolean;
    active: boolean;
    extra_id: number;
    extra: ExtraModel;
}