export class GeralSettingsModel {
    ALLOW_OPTIONAL_RESIDENCE: boolean;
    SHOW_IBAN_ON_APPLICATION_FORM: boolean;
    UNASSIGNED_OPPOSITION_LIMIT: number;
}