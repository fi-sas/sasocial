import { ApplicationStatus } from '@fi-sas/webpage/modules/accommodation/models/application.model';

export class HistoryModel {
  id: number;
  application_id?: number;
  status: ApplicationStatus;
  user_id: number;
  notes: string;
  updated_at: Date;
  created_at: Date;
}
