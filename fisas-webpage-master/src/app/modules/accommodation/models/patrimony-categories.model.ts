export class PatrimonyCategoriesModel {
    active: boolean;
    code: number;
    id: number;
    description: string;
}