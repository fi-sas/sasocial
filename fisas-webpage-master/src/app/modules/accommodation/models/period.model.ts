export class PeriodModel {
    academic_year: string;
    start_date: Date;
    end_date: Date;
}