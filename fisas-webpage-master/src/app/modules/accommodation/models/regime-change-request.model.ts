import { ApplicationModel } from "../../u-bike/models/application.model";
import { RegimeModel } from "./regime.model";

export enum RegimeChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    ANALYSIS = 'ANALYSIS',
    DISPATCH = 'DISPATCH',
    APPROVED = 'APPROVED',
    CANCELLED = 'CANCELLED',
    REJECTED = 'REJECTED',
}
export enum RegimeChangeRequestDecision {
    APPROVE = 'APPROVE',
    REJECT = 'REJECT',
}
export class RegimeChangeRequestHistoryModel {
    id: number;
    status: string;
    user_id: number;
    notes: string;
    created_at: Date;
    update_at: Date;
}

export class RegimeChangeRequestModel {
    id: number;
    reason: string;
    application_id: number;
    application: ApplicationModel;
    start_date: Date;
    observations: string;
    decision: RegimeChangeRequestDecision;
    status: RegimeChangeRequestStatus;
    created_at: Date;
    update_at: Date;
    history: RegimeChangeRequestHistoryModel[];
    regime_id: number;
    regime: RegimeModel;
}