export class RegimeModel {
  id: number;
  translations: RegimeTranslationModel[];
  active: boolean;
  billing_name: string;
  regime_id: number;
  regime: RegimeModel;
  priceLines: any;
}

export class RegimeTranslationModel {
  language_id: number;
  name: string;
  description: string;
}
