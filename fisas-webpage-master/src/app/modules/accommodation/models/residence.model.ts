import { ApplicationPhase } from './application-phase.model';
import { ExtraModel } from './extra.model';
import { FileModel } from './file.model';
import { RegimeModel } from './regime.model';
import { TypologyModel } from './typology.model';

export class ResidenceModel {
  id: number;
  available_for_assign: boolean;
  available_for_application: boolean;
  name: string;
  active: boolean;
  address: string;
  city: string;
  postal_code: string;
  phone_1: string;
  phone_2: string;
  email: string;
  application_start: Date;
  application_end: Date;
  building_id: number;
  updated_at: Date;
  created_at: Date;
  typology_ids: number[];
  service_ids: number[];
  price_line_ids: number[];
  current_account_id: number;
  services: any[];
  residence_responsible_ids: number[];
  wing_responsible_ids: number[];
  kitchen_responsible_ids: number[];
  regulation_file_id: number;
  regulationFile: FileModel;
  contract_file_id: number;
  contractFile: FileModel;
  mediaIds?: FileModel[];
  regimes: RegimeModel[];
  extras: ExtraModel[];
  typologies: TypologyModel[];
  visible_for_users: boolean;
}

