import { ApplicationModel } from "../../u-bike/models/application.model";
import { ResidenceModel } from "./residence.model";
import { RoomModel } from "./room.model";
import { TypologyModel } from "./typology.model";


export enum RoomChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    ANALYSIS = 'ANALYSIS',
    DISPATCH = 'DISPATCH',
    APPROVED = 'APPROVED',
    CANCELLED = 'CANCELLED',
    REJECTED = 'REJECTED',
}
export enum RoomChangeRequestDecision {
    APPROVE = 'APPROVE',
    REJECT = 'REJECT',
}
export class RoomChangeRequestHistoryModel {
    id: number;
    status: string;
    user_id: number;
    notes: string;
    created_at: Date;
    update_at: Date;
}

export class RoomChangeRequestModel {
    id: number;
    reason: string;
    application_id: number;
    application: ApplicationModel;
    start_date: Date;
    observations: string;
    decision: RoomChangeRequestDecision;
    status: RoomChangeRequestStatus;
    created_at: Date;
    update_at: Date;
    history: RoomChangeRequestHistoryModel[];
    residence_id: number
    residence: ResidenceModel;
    room_id: number
    room: RoomModel;
    permute_user_tin: string;
    type: string;
    typology_id: number;
    typology: TypologyModel;
}
