export class RoomMapModel {
  id: number;
  name: string;
  max_occupants_number: number;
  occupants: number;
  available: number;
}
