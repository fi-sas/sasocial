import { TypologyModel } from "./typology.model";

export class RoomModel {
  id: number;
  name: string;
  active: boolean;
  room_id: number;
  typology_id: number;
  typology: TypologyModel;
  residence_id: number;
  allow_booking: boolean;
  updated_at: Date;
  created_at: Date;
}
