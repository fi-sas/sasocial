import { ApplicationModel } from "./application.model";
import { TariffModel } from "./tariff.model";

export enum TariffChangeRequestStatus {
    SUBMITTED = 'SUBMITTED',
    ANALYSIS = 'ANALYSIS',
    DISPATCH = 'DISPATCH',
    APPROVED = 'APPROVED',
    CANCELLED = 'CANCELLED',
    REJECTED = 'REJECTED',
}
export enum TariffChangeRequestDecision {
    APPROVE = 'APPROVE',
    REJECT = 'REJECT',
}
export class TariffChangeRequestHistoryModel {
    id: number;
    status: string;
    user_id: number;
    notes: string;
    created_at: Date;
    update_at: Date;
}

export class TariffChangeRequest {
    id: number;
    reason: string;
    application_id: number;
    application: ApplicationModel;
    start_date: Date;
    has_scholarship: boolean;
    decision: TariffChangeRequestDecision;
    status: TariffChangeRequestStatus;
    created_at: Date;
    update_at: Date;
    history: TariffChangeRequestHistoryModel[];
    tariff_id: number;
    tariff: TariffModel;
    old_tariff_id: number;
    old_tariff: TariffModel;
}
