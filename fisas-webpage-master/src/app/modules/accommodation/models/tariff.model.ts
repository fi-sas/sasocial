export class TariffModel {
    id: number;
    name: string;
    active: boolean;
    created_at: Date;
    updated_at: Date;
}
