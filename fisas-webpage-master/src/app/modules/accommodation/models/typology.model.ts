
export class TypologyTranslation {
    name: string;
    language_id: number;
}
export class TypologyModel {
    id: number;
    max_occupants_number: number;
    product_code: string;
    translations: TypologyTranslation[];
    priceLines: any;
    active: boolean;
    updated_at: Date | string;
    created_at: Date | string;
}
