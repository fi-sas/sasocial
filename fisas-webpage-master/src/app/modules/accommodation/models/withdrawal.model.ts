import { FileModel } from '@fi-sas/webpage/modules/accommodation/models/file.model';

export enum WithdrawalStatus {
  SUBMITTED = 'SUBMITTED',
  ANALISYS = 'ANALISYS',
  DISPATCH = 'DISPATCH',
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
  CANCELLED = 'CANCELLED',
}

export class WithdrawalModel {
  id?: number;
  application_id?: number;
  user_id?: number;
  end_date: string;
  reason: string;
  file_id: number;
  allow_booking: boolean;
  status?: WithdrawalStatus;
  history?: WithdrawalHistory[];
  file?: FileModel;
  updated_at?: string;
  created_at?: string;
}

export class WithdrawalHistory {
  id: number;
  application_id: number;
  status: WithdrawalStatus;
  user_id: number;
  notes: string;
  updated_at: Date;
  created_at: Date;
}


export const ConfigStatus = [
  {
    name: WithdrawalStatus.SUBMITTED,
    color: '#076743'
  },
  {
    name: WithdrawalStatus.ANALISYS,
    color: '#f8ca00'
  },
  {
    name: WithdrawalStatus.DISPATCH,
    color: '#ffa400',
  },

  {
    name: WithdrawalStatus.APPROVED,
    color: '#0d69dd'
  },
  {
    name: WithdrawalStatus.REJECTED,
    color: '#d1d1d1'
  },
  {
    name: WithdrawalStatus.CANCELLED,
    color: '#d0021b'
  },
];

