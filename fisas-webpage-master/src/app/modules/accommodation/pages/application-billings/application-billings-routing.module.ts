import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationBillingsComponent } from './application-billings.component';


const routes: Routes = [
  {
    path: '',
    component: ApplicationBillingsComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:billings:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationBillingsRoutingModule { }
