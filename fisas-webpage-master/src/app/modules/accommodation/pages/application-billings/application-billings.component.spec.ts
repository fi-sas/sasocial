import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationBillingsComponent } from './application-billings.component';

describe('ApplicationBillingsComponent', () => {
  let component: ApplicationBillingsComponent;
  let fixture: ComponentFixture<ApplicationBillingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationBillingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationBillingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
