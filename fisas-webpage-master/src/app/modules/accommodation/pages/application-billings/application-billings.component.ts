import { Component, Input, OnInit } from "@angular/core";
import { first, finalize } from "rxjs/operators";
import { ApplicationModel } from "@fi-sas/webpage/modules/accommodation/models/application.model";
import { BillingItemModel } from "@fi-sas/webpage/modules/accommodation/models/billing-item.model";
import { ApplicationsService } from "@fi-sas/webpage/modules/accommodation/services/applications.service";
import { BillingsService } from "@fi-sas/webpage/modules/accommodation/services/billings.service";
import { BillingModel } from "@fi-sas/webpage/modules/accommodation/models/billing.model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-application-billings",
  templateUrl: "./application-billings.component.html",
  styleUrls: ["./application-billings.component.less"],
})
export class ApplicationBillingsComponent implements OnInit {
  application: ApplicationModel = null;
  loading = false;

  billings: BillingModel[] = [];

  // Table
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  sortValue = null;
  sortKey = null;
  sizeChanger = false;
  pagination = true;
  expandable = true;
  checkbox = true;
  position = "bottom";
  noApplication = false;

  constructor(
    private billingService: BillingsService,
    private applicationsService: ApplicationsService,
    private route: ActivatedRoute
  ) {
    if (this.route.snapshot.data.application)
      this.application = this.route.snapshot.data.application.data[0];
  }

  ngOnInit() {
    this.getUserApplication();
  }

  getUserApplication() {
    if (this.application) {
      this.noApplication = true;
      this.getBillings();
    } else {
      this.noApplication = false;
    }
  }

  getExtras(items: BillingItemModel[]) {
    return items.filter((x) => x.extra_id != null);
  }
  getTyplogies(items: BillingItemModel[]) {
    return items.filter((x) => x.typology_id != null);
  }
  getRegimes(items: BillingItemModel[]) {
    return items.filter((x) => x.regime_id != null);
  }

  getTotalPrice(i: number): string {
    let sum_price = 0;
    for (const item of this.billings[i].billing_items) {
      sum_price += item.price;
    }
    return sum_price.toFixed(2);
  }

  getBillings(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;

    this.billingService
      .list(this.application.id, this.pageIndex, this.pageSize)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((billings) => {
        this.total = billings.link.total;
        this.billings = billings.data;
      });
  }
}
