import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { AccommodationSharedModule } from "../../accommodation-shared.module";
import { ApplicationBillingsRoutingModule } from "./application-billings-routing.module";
import { ApplicationBillingsComponent } from "./application-billings.component";
import { ApplicationNavbarModule } from "../../components/applications-navbar/application-navbar.module";

@NgModule({
  declarations: [ApplicationBillingsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationBillingsRoutingModule,
    AccommodationSharedModule,
    ApplicationNavbarModule,
  ],
})
export class ApplicationBillingsModule {}
