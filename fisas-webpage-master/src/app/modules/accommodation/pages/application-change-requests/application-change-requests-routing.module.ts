import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationChangeRequestsComponent } from './application-change-requests.component';


const routes: Routes = [
  {
    path: '',
    component: ApplicationChangeRequestsComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:create' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationChangeRequestRoutingModule { }
