import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationChangeRequestsComponent } from './application-change-requests.component';

describe('ApplicationChangeRequestsComponent', () => {
  let component: ApplicationChangeRequestsComponent;
  let fixture: ComponentFixture<ApplicationChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
