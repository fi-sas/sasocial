import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ApplicationModel } from "@fi-sas/webpage/modules/accommodation/models/application.model";
import { ExtensionModel } from "@fi-sas/webpage/modules/accommodation/models/extension.model";
import { WithdrawalModel } from "@fi-sas/webpage/modules/accommodation/models/withdrawal.model";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { ApplicationChangesModalComponent } from "../../components/application-changes-modal/application-changes-modal.component";
import { ApplicationCommunicationDateModalComponent } from "../../components/application-communication-date-modal/application-communication-date-modal.component";
import { ApplicationExtrasChangeModalComponent } from "../../components/application-extras-change-modal/application-extras-change-modal.component";
import { ApplicationIbanChangeModalComponent } from "../../components/application-iban-change-modal/application-iban-change-modal.component";
import { ApplicationMaintenanceRequestModalComponent } from "../../components/application-maintenance-request-modal/application-maintenance-request-modal.component";
import { ApplicationRegimeChangeModalComponent } from "../../components/application-regime-change-modal/application-regime-change-modal.component";
import { ApplicationRoomChangesModalComponent } from "../../components/application-room-changes-modal/application-room-changes-modal.component";
import { ApplicationScholarshipChangeModalComponent } from "../../components/application-scholarship-change-modal/application-scholarship-change-modal.component";
import { GeralSettingsService } from "../../services/geral-settings.service";
import { ResidencesService } from "../../services/residences.service";

@Component({
  selector: "app-application-change-requests",
  templateUrl: "./application-change-requests.component.html",
  styleUrls: ["./application-change-requests.component.less"],
})
export class ApplicationChangeRequestsComponent implements OnInit {
  withdrawals: WithdrawalModel[] = null;
  extensions: ExtensionModel[] = null;
  application: ApplicationModel = null;
  noApplication = false;
  loading = false;
  show_iban_change_option = false;
  loadingInfo = true;
  listExtras = [];
  listRegimes = [];
  constructor(
    private modalService: NzModalService,
    private geralSettingsService: GeralSettingsService,
    private residencesService: ResidencesService,
    private route: ActivatedRoute
  ) {
    if (this.route.snapshot.data.application)
      this.application = this.route.snapshot.data.application.data[0];
    this.getUserApplication();
  }

  ngOnInit() {
    this.loading = true;
    this.getConfigurations();
  }

  getConfigurations() {
    this.geralSettingsService
      .list(0, 1)
      .pipe(first())
      .subscribe((configs) => {
        this.show_iban_change_option =
          configs.data[0].SHOW_IBAN_ON_APPLICATION_FORM;
      });
  }

  getUserApplication() {
    if (this.application.status != "contracted") {
      this.noApplication = true;
    } else {
      this.noApplication = false;
      this.getInfo();
    }
  }

  getInfo() {
    this.residencesService
      .getResidenceById(this.application.assigned_residence_id)
      .pipe(
        first(),
        finalize(() => (this.loadingInfo = false))
      )
      .subscribe((residences) => {
        this.listExtras = residences.data[0].extras.filter((extra) => {
          return extra.extra.visible && extra.extra.active;
        });
        this.listRegimes = residences.data[0].regimes.filter((data) => {
          return (
            data.regime_id != this.application.current_regime_id &&
            data.regime.active
          );
        });
      });
  }

  openApplicationChangesModal(type: string) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationChangesModalComponent,
        nzFooter: null,
        nzComponentParams: {
          type: type,
          application: this.application,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }

  openApplicationRoomChangesModal(type: string) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationRoomChangesModalComponent,
        nzFooter: null,
        nzComponentParams: {
          type: type,
          application: this.application,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }

  openApplicationExtrasChangesModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationExtrasChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          extras: this.listExtras,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }
  openApplicationRegimeChangesModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationRegimeChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          regimes: this.listRegimes,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }

  openMaintenanceRequestModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationMaintenanceRequestModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }

  openScholarshipRequestModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationScholarshipChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }
  openCommunicationRequestModal(type: string) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationCommunicationDateModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          type,
          edit: false,
          consult: false,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }
  openApplicationIbanChangesModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationIbanChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          edit: false,
          consult: false,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {});
  }
}
