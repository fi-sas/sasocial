import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ApplicationChangeRequestsComponent } from "./application-change-requests.component";
import { ApplicationChangeRequestRoutingModule } from "./application-change-requests-routing.module";
import { AccommodationSharedModule } from "../../accommodation-shared.module";
import { ApplicationNavbarModule } from "../../components/applications-navbar/application-navbar.module";

@NgModule({
  declarations: [ApplicationChangeRequestsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationChangeRequestRoutingModule,
    ApplicationNavbarModule,
    AccommodationSharedModule,
  ],
})
export class ApplicationChangeRequestModule {}
