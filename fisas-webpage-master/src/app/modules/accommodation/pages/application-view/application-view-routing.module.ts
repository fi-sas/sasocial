import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationViewComponent } from './application-view.component';


const routes: Routes = [
  {
    path: '',
    component: ApplicationViewComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationViewRRoutingModule { }
