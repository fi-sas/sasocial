import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  ApplicationModel,
  ApplicationStatus,
} from "@fi-sas/webpage/modules/accommodation/models/application.model";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { TranslateService } from "@ngx-translate/core";
import { ApplicationsService } from "@fi-sas/webpage/modules/accommodation/services/applications.service";
import { ConfigStatus } from "@fi-sas/webpage/modules/accommodation/models/application.model";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { NzModalService } from "ng-zorro-antd";
import { first, finalize } from "rxjs/operators";
import { ApplicationChangeStatusModel } from "@fi-sas/webpage/modules/accommodation/models/application-change-status.model";
import * as moment from "moment";
import * as _ from "lodash";
import { GeralSettingsService } from "../../services/geral-settings.service";
@Component({
  selector: "app-application-view",
  templateUrl: "./application-view.component.html",
  styleUrls: ["./application-view.component.less"],
})
export class ApplicationViewComponent implements OnInit {
  application: ApplicationModel = null;
  applicationsStatus = ApplicationStatus;
  daycurrent: Date = new Date();
  configStatus = ConfigStatus;

  buttonOppose = {
    color: "#ffffff",
    "text-align": "center",
    "background-color": "#800000",
    "margin-top": "5px",
  };

  buttonReject = {
    color: "#ffffff",
    "text-align": "center",
    "background-color": "#d0021b",
    "margin-top": "5px",
  };

  buttonAccept = {
    color: "#ffffff",
    "text-align": "center",
    "margin-top": "5px",
  };

  buttonCancel = {
    color: "rgb(208, 2, 27)",
    "border-color": "#d0021b",
    "text-align": "center",
    "margin-top": "5px",
  };

  // Opposition modal
  oppositionModalVisible = false;
  oppositionModalRequest = "";
  oppositionModalLoading = false;
  unassigned_limit_days = 0;

  constructor(
    private route: ActivatedRoute,
    private applicationDataService: ApplicationDataService,
    public translate: TranslateService,
    private router: Router,
    private applicationService: ApplicationsService,
    private translateService: TranslateService,
    private uiService: UiService,
    private modalService: NzModalService,
    private settingsService: GeralSettingsService
  ) {
    this.settingsService
      .list()
      .pipe(first())
      .subscribe((data) => {
        this.unassigned_limit_days = data.data[0].UNASSIGNED_OPPOSITION_LIMIT;
      });
  }

  ngOnInit() {
    if (this.route.snapshot.data.application)
      this.application = this.route.snapshot.data.application.data[0];
    this.applicationDataService.createApplication(this.application);
  }

  validateUnassignedDate(date) {
    return (
      moment(new Date()).diff(moment(date), "d") < this.unassigned_limit_days
    );
  }

  backToListApplications(): void {
    this.router.navigateByUrl("accommodation/list-applications");
  }

  editApplication() {
    this.router.navigateByUrl(
      "accommodation/list-residences/form-application/" + this.application.id
    );
  }

  getApplication() {
    this.applicationService
      .read(this.application.id)
      .pipe(first())
      .subscribe((application) => {
        this.application = application.data[0];
      });
  }

  cancel() {
    const applicationStatus: ApplicationChangeStatusModel = {
      event: "CANCEL",
      notes: "Cancelado pelo estudante",
    };
    this.applicationService
      .changeStatus(this.application.id, applicationStatus)
      .subscribe(
        (status) => {
          if (status) {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS"
              )
            );
            this.backToListApplications();
          }
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR"
            )
          );
        }
      );
  }

  rejected() {
    this.applicationService.studentReject(this.application.id).subscribe(
      (status) => {
        if (status) {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.REJECT.SUCCESS"
            )
          );
          this.backToListApplications();
        }
      },
      () => {
        this.uiService.showMessage(
          MessageType.error,
          this.translateService.instant(
            "VIEW_APPLICATION_STATUS.MESSAGE.REJECT.ERROR"
          )
        );
      }
    );
  }

  accepted() {
    this.applicationService.studentApprove(this.application.id).subscribe(
      (status) => {
        if (status) {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.SUCCESS"
            )
          );
          this.backToListApplications();
        }
      },
      () => {
        this.uiService.showMessage(
          MessageType.error,
          this.translateService.instant(
            "VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.ERROR"
          )
        );
      }
    );
  }

  modalCancel() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CANCEL_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancel(),
    });
  }

  modalConfirm() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CONFIRM_TEXT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.accepted(),
    });
  }

  modalRejected() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.REJECTED"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.rejected(),
    });
  }

  modalOppose() {
    this.oppositionModalVisible = true;
    this.oppositionModalLoading = false;
    this.oppositionModalRequest = "";
  }

  modalOpposeOk() {
    if (this.oppositionModalRequest === "") {
      this.uiService.showMessage(
        MessageType.warning,
        this.translateService.instant(
          "VIEW_APPLICATION_STATUS.OPPOSITION_MODAL.ERRORS.EMPTY_REQUEST"
        )
      );
      return;
    }

    this.oppositionModalLoading = true;
    this.applicationService
      .studentOppose(this.application.id, this.oppositionModalRequest)
      .pipe(
        first(),
        finalize(() => (this.oppositionModalLoading = false))
      )
      .subscribe(
        (status) => {
          if (status) {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "VIEW_APPLICATION_STATUS.MESSAGE.OPPOSE.SUCCESS"
              )
            );
            this.modalOpposeCancel();
            this.backToListApplications();
          }
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.OPPOSE.ERROR"
            )
          );
        }
      );
  }

  modalOpposeCancel() {
    this.oppositionModalVisible = false;
    this.oppositionModalLoading = false;
    this.oppositionModalRequest = "";
  }

  showAssignedResidence(): boolean {
    const states = [
      "assigned",
      "confirmed",
      "rejected",
      "contracted",
      "closed",
    ];
    return (
      this.application.assigned_residence_id != null &&
      states.includes(this.application.status)
    );
  }
}
