import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ApplicationViewComponent } from "./application-view.component";
import { ApplicationViewRRoutingModule } from "./application-view-routing.module";
import { ApplicationNavbarModule } from "../../components/applications-navbar/application-navbar.module";
import { AccommodationPipesModule } from "../../pipes/accommodation-pipes.module";
import { ResumeApplicationModule } from "../../components/resume-application/resume-application.module";

@NgModule({
  declarations: [ApplicationViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationViewRRoutingModule,
    ApplicationNavbarModule,
    AccommodationPipesModule,
    ResumeApplicationModule,
  ],
})
export class ApplicationViewModule {}
