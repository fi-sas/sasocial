import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormApplicationComponent } from './form-application.component';


const routes: Routes = [
  {
    path: '',
    component: FormApplicationComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:create' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormApplicationRoutingModule { }
