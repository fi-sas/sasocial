import { Component, OnInit } from "@angular/core";
import { ApplicationModel } from "@fi-sas/webpage/modules/accommodation/models/application.model";
import { debounce, debounceTime, finalize, first } from "rxjs/operators";
import { ApplicationsService } from "../../services/applications.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { ResidencesService } from "../../services/residences.service";
import { RoomsService } from "../../services/rooms.service";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import * as moment from "moment";
import { ResidenceModel } from "../../models/residence.model";
import { RegimeModel } from "../../models/regime.model";
import { ExtraModel } from "../../models/extra.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { OrganicUnitsService } from "@fi-sas/webpage/shared/services/organic-units.service";
import { ApplicantInfoModel } from "../../models/applicant-info.model";
import {
  compareTwoFieldsValidation,
  trimValidation,
} from "@fi-sas/webpage/shared/validators/validators.validator";
import { GeralSettingsService } from "../../services/geral-settings.service";
import { PatrimonyCategoriesModel } from "../../models/patrimony-categories.model";
import { nationalities } from "@fi-sas/webpage/shared/data/nationalities";
import { ApplicationPhasesService } from "../../services/application-phases.service";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import {
  FormInformationTextsModel,
  TranslationsModelFormInformation,
} from "@fi-sas/webpage/shared/models/form-information-texts.model";
import { DomSanitizer } from "@angular/platform-browser";
import { TypologyModel } from "../../models/typology.model";
import { RoomModel } from "../../models/room.model";
import { TranslateService } from "@ngx-translate/core";
import { FiConfigurator } from "@fi-sas/configurator";
import { Subscription } from "rxjs";

@Component({
  selector: "app-form-application",
  templateUrl: "./form-application.component.html",
  styleUrls: ["./form-application.component.less"],
})
export class FormApplicationComponent implements OnInit {
  isSubmited = false;
  showErrorMessage = false;
  documentTypes: DocumentTypeModel[] = [];
  documentType: DocumentTypeModel;
  fileListIncomeStatement = [];
  fileListOthers = [];
  loadingRoom = false;
  current = 0;
  index = "identification";
  year: number = 0;
  get f() {
    return this.aplicationForm.controls;
  }
  listPatrimony: PatrimonyCategoriesModel[] = [];
  isRenew = false;
  isUpdate = false;
  idToUpdate = null;
  loading = false;
  loadingResidence = false;
  disabledIdentification;
  modalSuccess = false;
  emailRegex =
    "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|" +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\" +
    "[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]" +
    "?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])";

  aplicationForm: FormGroup;
  residence_id: number;
  fixed: boolean = false;
  residencesAssign: ResidenceModel[] = [];
  residencesApplication: ResidenceModel[] = [];
  residences: ResidenceModel[] = [];
  residenceSelected: ResidenceModel[] = [];
  residencesExceptSelect: ResidenceModel[] = [];
  organicUnits: OrganicUnitModel[] = [];
  regimes: RegimeModel[] = [];
  typologies: TypologyModel[] = [];
  roomsList: RoomModel[] = [];
  extrasList: ExtraModel[] = [];
  infoUser: ApplicantInfoModel;
  residenceApplicationModel = new ApplicationModel();
  loadingButton = false;
  nameResidenceSelected: string;
  nameUsedResidenceSelected: string;
  nameRoom: string;
  nameOrganic: string;
  nameCourseDegree: string;
  nameCourse: string;
  descPatrimony: string;
  sendRegime: RegimeModel;
  sendTypologies: TypologyModel;
  extrasSend: ExtraModel[] = [];
  residenceName: string;

  courseDegreesList: any;
  coursesList: any;
  academic_year: string = "";
  startDate: Date;
  endDate: Date;
  applicantInfo: ApplicationModel;
  organicUnitLoading = false;
  courseLoading = false;
  previousNumberElements = 0;
  allow_optional_residence = false;
  show_iban_on_application_form = false;
  nationalities = nationalities;
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  other_income: boolean = false;
  openInfo: boolean = true;
  info: FormInformationTextsModel[] = [];
  infoApresent: TranslationsModelFormInformation[] = [];
  currentLang;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private applicationService: ApplicationsService,
    private router: Router,
    private route: ActivatedRoute,
    private roomsService: RoomsService,
    private configurationService: ConfigurationsService,
    private residencesService: ResidencesService,
    private organicUnitsService: OrganicUnitsService,
    private settings: GeralSettingsService,
    private applicationPhasesService: ApplicationPhasesService,
    private sanitizer: DomSanitizer,
    private translate: TranslateService,
    private config: FiConfigurator
  ) {
    this.getConfigurations();
    this.getPatrimonyCategories();
    this.getPhases();
    this.getInfo();
    let languagesIDS;
    if (config !== undefined) {
      languagesIDS = Object.values(config.getOptionTree("LANGUAGES"))[0];
      this.currentLang = translate.getDefaultLang();
      this.validSortNati();
    } else {
      languagesIDS = undefined;
    }

    translate.onLangChange.subscribe(() => {
      if (languagesIDS !== undefined) {
        this.currentLang = languagesIDS.find(
          (lang) => lang.acronym === this.translate.currentLang
        ).acronym;
      } else {
        this.currentLang = undefined;
      }
      this.validSortNati();
    });
  }

  ngOnInit() {
    this.aplicationForm = this.fb.group(
      {
        // 1 step
        full_name: new FormControl("", [Validators.required, trimValidation]),
        applicant_birth_date: new FormControl("", [Validators.required]),
        gender: ["", [Validators.required]],
        father_name: ["", [Validators.required, trimValidation]],
        mother_name: ["", [Validators.required, trimValidation]],
        incapacity: [null, [Validators.required]],
        incapacity_type: ["", [trimValidation]],
        incapacity_degree: ["", [trimValidation]],
        nationality: ["", [Validators.required]],
        identification: ["", [Validators.required, trimValidation]],
        document_type_id: ["", [Validators.required]],
        tin: ["", [Validators.required, Validators.minLength(9)]],
        iban: ["", [Validators.maxLength(6), Validators.pattern("[A-Z0-9]")]],
        allow_direct_debit: [null, []],
        address: ["", [Validators.required, trimValidation]],
        city: ["", [Validators.required, trimValidation]],
        postal_code: ["", [Validators.required]],
        country: ["", [Validators.required, trimValidation]],
        household_distance: ["", [Validators.required]],
        // 2 step
        email: [
          "",
          [
            Validators.pattern(this.emailRegex),
            Validators.required,
            trimValidation,
          ],
        ],
        secundary_email: [
          "",
          [Validators.pattern(this.emailRegex), trimValidation],
        ],
        phone_1: [
          "",
          [
            Validators.required,
            Validators.minLength(9),
            Validators.maxLength(18),
          ],
        ],
        phone_2: [
          "",
          [
            Validators.required,
            Validators.minLength(9),
            Validators.maxLength(18),
          ],
        ],
        // 3 step
        residence_id: [null, [Validators.required]],
        second_option_residence_id: [],
        dateRange: new FormControl([], Validators.required),
        regime_id: [null, [Validators.required]],
        extra_ids: [[]],
        preferred_typology_id: [null],
        has_used_residences_before: [null],

        which_residence_id: [null],
        which_room_id: [null],
        // 4 step
        international_student: new FormControl(null, [Validators.required]),
        organic_unit_id: new FormControl("", [Validators.required]),
        course_degree_id: new FormControl("", [Validators.required]),
        course_id: new FormControl("", [Validators.required]),
        course_year: new FormControl("", [Validators.required]),
        student_number: new FormControl("", [
          Validators.required,
          trimValidation,
        ]),
        admission_date: new FormControl("", [Validators.required]),
        // 5 step
        income_statement_delivered: [null, [Validators.required]],
        income_statement_files_ids: [],
        patrimony_category_id: [null, [Validators.required]],
        income_source: [null, [Validators.required]],
        other_income_source: [""],
        household_elements_number: ["", [Validators.min(0.01)]],
        household_total_income: ["", [Validators.required, Validators.min(1)]],
        house_hould_elements: this.fb.array([]),
        househould_elements_in_university: [
          "",
          [Validators.required, Validators.min(1)],
        ],
        // 7 step
        applied_scholarship: [null, Validators.required],
        scholarship_value: [""],
        // 9 step
        observations: ["", [trimValidation, Validators.maxLength(255)]],
        file_ids: [],
        // 10 step
        applicant_consent: new FormControl(false, [Validators.requiredTrue]),
      },
      {
        validator: [
          compareTwoFieldsValidation("phone_1", "phone_2"),
          compareTwoFieldsValidation("email", "secundary_email"),
        ],
      }
    );

    this.applicationService
    .applicantInfo()
    .pipe(first())
    .subscribe((infoUser) => {
      this.infoUser = infoUser.data[0];
      this.getDocumentTypes();
      const id = this.route.snapshot.paramMap.get("id");
      if (id) {
        //EDIT
        this.loading = true;
        this.disabledIdentification = true;
        this.applicationService
          .read(parseInt(id))
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.isUpdate = true;
            this.idToUpdate = result.data[0].id;
            this.applicantInfo = result.data[0];
            if (result.data[0].course) {
              this.applicantInfo.course_degree_id =
                result.data[0].course.course_degree_id;
            }
            this.applicationValues(result.data[0]);
          });
      } else if (this.route.snapshot.queryParamMap.get("application_id")) {
        //RENEW
        this.loading = true;
        this.isRenew = true;
        this.applicationService
          .read(parseInt(this.route.snapshot.queryParamMap.get("application_id")))
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((result) => {
            this.applicantInfo = result.data[0];
            if (result.data[0].course) {
              this.applicantInfo.course_degree_id =
                result.data[0].course.course_degree_id;
            }
            this.applicationValues(result.data[0]);
          });
      } else {
        this.newApplicationStartingValues();
        if (this.route.snapshot.queryParamMap.get("residence_id")) {
          const data = this.route.snapshot.queryParamMap.get("residence_id");
          this.residence_id = +data;
          this.getResidenceById(this.residence_id);
          this.getDocumentTypes();
          this.getOrganicUnits();
          this.getDegrees();
          this.addNewGroup();
        }
      }
    });
  }

  validSortNati() {
    if (this.currentLang == "en") {
      this.nationalities = this.nationalities.sort((a, b) => {
        if (a.translate > b.translate) {
          return 1;
        }
        if (a.translate < b.translate) {
          return -1;
        }
        return 0;
      });
    } else {
      this.nationalities = nationalities;
    }
  }

  getConfigurations() {
    this.settings
      .list()
      .pipe(first())
      .subscribe((data) => {
        this.allow_optional_residence = data.data[0].ALLOW_OPTIONAL_RESIDENCE;
        this.show_iban_on_application_form =
          data.data[0].SHOW_IBAN_ON_APPLICATION_FORM;
        if (!this.show_iban_on_application_form) {
          this.f.allow_direct_debit.setValue(null);
          this.f.iban.setValue(null);
        }
      });
  }

  getPatrimonyCategories() {
    this.applicationService
      .getPatrimonyCategories()
      .pipe(first())
      .subscribe((data) => {
        this.listPatrimony = data.data;
      });
  }

  labelExtras(name, price): string {
    return name + " - " + price + "€";
  }

  getResidenceById(idResidence: number) {
    this.loadingResidence = true;
    this.residencesService
      .getResidenceById(idResidence)
      .pipe(
        first(),
        finalize(() => (this.loadingResidence = false))
      )
      .subscribe((result) => {
        this.residenceSelected = result.data;
        this.regimes = this.residenceSelected[0].regimes;
        this.extrasList = this.residenceSelected[0].extras;
        this.typologies = this.residenceSelected[0].typologies;
        this.getResidences();
        this.getResidencesAvailableForApplication();
        this.aplicationForm.controls.residence_id.setValue(idResidence);
        if(this.isUpdate || this.isRenew){
          this.setResidenceAndRoomInfo();
        }
      });
  }

  getResidencesAvailableForApplication() {
    this.residencesService
      .listAvailableForApplication()
      .pipe(first())
      .subscribe((result) => {
        this.residencesExceptSelect = result.data.filter((residence) => {
          return residence.id != this.residence_id;
        });
      });
  }

  getResidences() {
    this.residencesService
      .list()
      .pipe(first())
      .subscribe((result) => {
        this.residences = result.data.filter((res) => {
          return res.visible_for_users == true;
        });
        this.residencesAssign = result.data.filter((res) => {
          return res.available_for_assign == true;
        });
        this.residencesApplication = result.data.filter((res) => res.available_for_application);
      });
  }

  getDocumentTypes() {
    let idCC = false;
    this.authService
      .getDocumentTypes()
      .pipe(first())
      .subscribe((data) => {
        this.documentTypes = data.data;
        this.documentTypes.forEach((data) => {
          if (data.id == 2) {
            idCC = true;
          }
        });
        if (idCC) {
          if (!this.infoUser.document_type_id && !this.idToUpdate) {
            this.aplicationForm.get("document_type_id").setValue(2);
          }
        }
      });
  }

  getPhases() {
    this.applicationPhasesService
      .openApplication()
      .pipe(first())
      .subscribe((data) => {
        if (data.data.length > 0) {
          this.academic_year = data.data[0].academic_year;
          this.year = Number(data.data[0].academic_year.split("-")[0]);
          this.fixed = data.data[0].fixed_accommodation_period;
          this.getPeriodsByAcademicYear();
        }
      });
  }

  getInfo() {
    this.configurationService
      .getInfoForms(1)
      .pipe(first())
      .subscribe((data) => {
        this.info = data.data;
      });
  }

  existInfoStep(key: string): boolean {
    this.infoApresent = [];
    let aux = this.info.find((data) => data.key == key);
    if (aux && aux.visible) {
      this.infoApresent = aux.translations;
      return true;
    }
    return false;
  }

  getHTML(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  getPeriodsByAcademicYearEdit(
    startDateApplication: Date,
    endDateApplication: Date
  ) {
    this.applicationPhasesService
      .getPeriodsByAcademicYear(this.academic_year)
      .pipe(first())
      .subscribe((data) => {
        if (data.data.length > 0) {
          this.startDate = data.data[0].start_date;
          this.endDate = data.data[0].end_date;
          if (
            startDateApplication < this.startDate &&
            endDateApplication < this.endDate
          ) {
            this.aplicationForm
              .get("dateRange")
              .setValue([this.startDate, this.endDate]);
          } else {
            this.aplicationForm
              .get("dateRange")
              .setValue([startDateApplication, endDateApplication]);
          }
        } else {
          this.aplicationForm
            .get("dateRange")
            .setValue([startDateApplication, endDateApplication]);
        }
      });
  }

  getPeriodsByAcademicYear() {
    this.applicationPhasesService
      .getPeriodsByAcademicYear(this.academic_year)
      .pipe(first())
      .subscribe((data) => {
        if (data.data.length > 0) {
          this.startDate = data.data[0].start_date;
          this.endDate = data.data[0].end_date;
          this.aplicationForm
            .get("dateRange")
            .setValue([this.startDate, this.endDate]);
        }
      });
  }

  createItem() {
    return this.fb.group({
      kinship: ["", [Validators.required, trimValidation]],
      profession: ["", [Validators.required, trimValidation]],
    });
  }

  addNewGroup() {
    let add: FormArray;
    add = this.aplicationForm.get("house_hould_elements") as FormArray;
    add.push(this.createItem());
  }

  deleteGroup(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get("house_hould_elements") as FormArray;
    add.removeAt(index);
  }

  newApplicationStartingValues() {
    if (this.infoUser) {
      this.aplicationForm.get("full_name").patchValue(this.infoUser.full_name);
      this.aplicationForm.get("full_name").updateValueAndValidity();
    }
    if (this.infoUser.applicant_birth_date) {
      this.aplicationForm
        .get("applicant_birth_date")
        .patchValue(this.infoUser.applicant_birth_date);
      this.aplicationForm.get("applicant_birth_date").updateValueAndValidity();
    }
    if (this.infoUser.email) {
      this.aplicationForm.get("email").patchValue(this.infoUser.email);
      this.aplicationForm.get("email").updateValueAndValidity();
    }
    if (this.infoUser.identification) {
      this.aplicationForm
        .get("identification")
        .patchValue(this.infoUser.identification);
      this.disabledIdentification = true;
      this.aplicationForm.get("identification").updateValueAndValidity();
    }

    if (this.infoUser.document_type_id) {
      this.aplicationForm
        .get("document_type_id")
        .patchValue(this.infoUser.document_type_id);
      this.aplicationForm.get("document_type_id").updateValueAndValidity();
    }

    if (this.infoUser.nationality) {
      this.aplicationForm
        .get("nationality")
        .patchValue(this.infoUser.nationality);
      this.aplicationForm.get("nationality").updateValueAndValidity();
    }

    if (this.infoUser.gender) {
      this.aplicationForm.get("gender").patchValue(this.infoUser.gender);
      this.aplicationForm.get("gender").updateValueAndValidity();
    }
    if (this.infoUser.phone_1) {
      this.aplicationForm.get("phone_1").patchValue(this.infoUser.phone_1);
      this.aplicationForm.get("phone_1").updateValueAndValidity();
    }
    if (this.infoUser.tin) {
      this.aplicationForm.get("tin").patchValue(this.infoUser.tin);
      this.aplicationForm.get("tin").updateValueAndValidity();
    }

    if (this.infoUser.address) {
      this.aplicationForm.get("address").patchValue(this.infoUser.address);
      this.aplicationForm.get("address").updateValueAndValidity();
    }
    if (this.infoUser.postal_code) {
      this.aplicationForm
        .get("postal_code")
        .patchValue(this.infoUser.postal_code);
      this.aplicationForm.get("postal_code").updateValueAndValidity();
    }
    if (this.infoUser.city) {
      this.aplicationForm.get("city").patchValue(this.infoUser.city);
      this.aplicationForm.get("city").updateValueAndValidity();
    }

    if (this.infoUser.country) {
      this.aplicationForm.get("country").patchValue(this.infoUser.country);
      this.aplicationForm.get("country").updateValueAndValidity();
    }

    if (this.infoUser.organic_unit_id) {
      this.aplicationForm
        .get("organic_unit_id")
        .patchValue(this.infoUser.organic_unit_id);
      this.aplicationForm.get("organic_unit_id").updateValueAndValidity();
    }

    if (this.infoUser.student_number) {
      this.aplicationForm
        .get("student_number")
        .patchValue(this.infoUser.student_number);
      this.aplicationForm.get("student_number").updateValueAndValidity();
    }

    if (this.infoUser.course_degree_id) {
      this.aplicationForm
        .get("course_degree_id")
        .patchValue(this.infoUser.course_degree_id);
      this.aplicationForm.get("course_degree_id").updateValueAndValidity();
      this.clickDegree();
    }
    if (this.infoUser.course_id) {
      this.aplicationForm.get("course_id").patchValue(this.infoUser.course_id);
      this.aplicationForm.get("course_id").updateValueAndValidity();
    }
    if (this.infoUser.course_year) {
      this.aplicationForm
        .get("course_year")
        .patchValue(this.infoUser.course_year);
      this.aplicationForm.get("course_year").updateValueAndValidity();
    }
    if (this.infoUser.admission_date) {
      this.aplicationForm
        .get("admission_date")
        .patchValue(this.infoUser.admission_date);
      this.aplicationForm.get("admission_date").updateValueAndValidity();
    }
  }

  getDegrees() {
    this.configurationService
      .courseDegrees()
      .pipe(first())
      .subscribe((arg) => {
        this.courseDegreesList = arg.data;
      });
  }

  changeValue() {
    this.f.course_id.setValue(null);
    this.clickDegree();
  }

  clickDegree() {
    if (
      this.aplicationForm.get("course_degree_id").value &&
      this.aplicationForm.get("organic_unit_id").value
    ) {
      this.courseLoading = true;
      this.configurationService
        .courses(
          this.aplicationForm.get("course_degree_id").value,
          this.aplicationForm.get("organic_unit_id").value
        )
        .pipe(
          first(),
          finalize(() => {
            this.courseLoading = false;
          })
        )
        .subscribe((arg) => {
          this.coursesList = arg.data;
        });
    }
  }

  allowDebitDirectChange(event) {
    if (event) {
      this.f.iban.setValidators(Validators.required);
    } else {
      this.f.iban.setValidators(null);
    }
    this.f.iban.updateValueAndValidity();
  }

  disabledDatePeriod = (current: Date) => {
    const end = new Date(this.endDate);
    end.setDate(end.getDate());
    return (
      moment(current).isAfter(end) || moment(current).isBefore(this.startDate)
    );
  };

  disableEndDate = (endDate: Date) => {
    return moment(endDate).isAfter();
  };

  disableStartDate = (startDate: Date) => {
    if (!this.isRenew) {
      const currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - 1);
      return moment(startDate).isBefore(currentDate);
    } else {
      return moment(this.endDate).isAfter();
    }
  };

  onChangeValueResidence() {
    if (!this.aplicationForm.controls.has_used_residences_before.value) {
      this.aplicationForm.controls.which_residence_id.clearValidators();
      this.aplicationForm.controls.which_room_id.clearValidators();
    } else {
      this.aplicationForm.controls.which_residence_id.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.which_room_id.setValidators([
        Validators.required,
      ]);
    }

    this.aplicationForm.controls.which_residence_id.updateValueAndValidity();
    this.aplicationForm.controls.which_room_id.updateValueAndValidity();
  }

  onSelectValueRoom(id: any) {
    if (!id) return;
    this.loadingRoom = true;
    this.aplicationForm.patchValue({ which_room_id: null });
    this.roomsService
      .list(id, 0, -1, "name", "ascend")
      .pipe((first(), finalize(() => (this.loadingRoom = false))))
      .subscribe((result) => {
        this.roomsList = result.data;
      });
  }

  applicationValues(application: ApplicationModel) {
    this.getOrganicUnits();
    this.getDegrees();
    this.aplicationForm.patchValue(application);
    this.setUserInfoApplication(application);

    if (application.course) {
      this.clickDegree();
      //VERIFICAR

      /*this.aplicationForm
        .get("course_degree_id")
        .patchValue(application.course.course_degree_id);*/
    }

    /*this.aplicationForm.patchValue({
      course_id: application.course_id,
    });*/

    this.residence_id = this.residencesApplication.filter(r => r.id === application.residence_id) ? application.residence_id : null

    this.getResidenceById(this.residence_id);
    this.getPeriodsByAcademicYearEdit(
      application.start_date,
      application.end_date
    );

    if (!this.isRenew) {
      if (application.house_hould_elements.length == 0) {
        this.addNewGroup();
      } else {
        application.house_hould_elements.forEach((he) => this.addNewGroup());
      }
      this.aplicationForm.patchValue({
        which_room_id: application.which_room_id,
        house_hould_elements: application.house_hould_elements,
        extra_ids: application.extras.map((e) => e.id),
      });

      this.internationalValueChange();
      if (
        application.income_source &&
        application.income_source.includes("OTHER")
      ) {
        this.other_income = true;
      }
      if (
        application.income_statement_files &&
        application.income_statement_files.length > 0
      ) {
        application.income_statement_files.forEach((media) =>
          this.fileListIncomeStatement.push(media.file_id)
        );
      }
      if (application.files && application.files.length > 0) {
        application.files.forEach((file) => this.fileListOthers.push(file.id));
      }
    } else {
      this.resetRenewFieldsForm(application);
    }
  }

  resetRenewFieldsForm(application: ApplicationModel) {
    this.onSelectValueRoom(application.residence_id);
    this.addNewGroup();
    this.aplicationForm.patchValue({
      has_used_residences_before: true,
      //preferred_typology_id:
      which_residence_id: application.residence_id,
      which_room_id: application.room_id,
      household_total_income: null,
      income_source: null,
      patrimony_category_id: null,
      income_statement_delivered: null,
      income_statement_files_ids: null,
      applied_scholarship: null,
      scholarship_value: null,
      applicant_consent: null,
    });
  }

  setUserInfoApplication(application: ApplicationModel){
    this.aplicationForm.patchValue({
      full_name: this.infoUser && this.infoUser.full_name ? this.infoUser.full_name : application.full_name,
      applicant_birth_date: this.infoUser && this.infoUser.applicant_birth_date ?  this.infoUser.applicant_birth_date : application.applicant_birth_date,
      gender: this.infoUser && this.infoUser.gender ? this.infoUser.gender : application.gender,
      nationality: this.infoUser && this.infoUser.nationality ? this.infoUser.nationality : application.nationality,
      document_type_id: this.infoUser && this.infoUser.document_type_id ? this.infoUser.document_type_id : application.document_type_id,
      identification: this.infoUser && this.infoUser.identification ? this.infoUser.identification : application.identification,
      tin: this.infoUser && this.infoUser.tin ? this.infoUser.tin : application.tin,
      address : this.infoUser && this.infoUser.address ? this.infoUser.address : application.address,
      postal_code: this.infoUser && this.infoUser.postal_code ? this.infoUser.postal_code : application.postal_code,
      city: this.infoUser && this.infoUser.city ? this.infoUser.city : application.city,
      country: this.infoUser && this.infoUser.country ? this.infoUser.country : application.country,
      phone_1: this.infoUser && this.infoUser.phone_1 ? this.infoUser.phone_1 : application.phone_1,
      phone_2: this.infoUser && this.infoUser.phone_2 ? this.infoUser.phone_2 : application.phone_2,
      email: this.infoUser && this.infoUser.email ? this.infoUser.email : application.email,
      organic_unit_id: this.infoUser && this.infoUser.organic_unit_id ? this.infoUser.organic_unit_id : application.organic_unit_id,
      course_degree_id: this.infoUser && this.infoUser.course_degree_id ? this.infoUser.course_degree_id : application.course_degree_id,
      course_id: this.infoUser && this.infoUser.course_id ? this.infoUser.course_id : application.course_id,
      course_year: this.infoUser && this.infoUser.course_year ? this.infoUser.course_year : application.course_year,
      student_number: this.infoUser && this.infoUser.student_number ? this.infoUser.student_number : application.student_number,
      admission_date: this.infoUser && this.infoUser.admission_date ? this.infoUser.admission_date : application.admission_date,
    });
  }

  setResidenceAndRoomInfo(){
    this.aplicationForm.patchValue({
      room_id: this.roomsList.filter(room => room.room_id === this.applicantInfo.room_id).length ? this.applicantInfo.room_id : null,
      extra_ids: this.extrasList.filter(el => this.applicantInfo.extras.find(e => el.extra_id === e.id)).length ? this.extrasList.filter(el => this.applicantInfo.extras.find(e => el.extra_id === e.id)).map(e => e.extra_id) : [] ,
      regime_id: this.regimes.filter(regime => regime.regime_id === this.applicantInfo.regime_id).length ? this.applicantInfo.regime_id : null,
      preferred_typology_id: this.typologies.filter(typology => typology.id === this.applicantInfo.preferred_typology_id).length ? this.applicantInfo.preferred_typology_id: null
    });
  }

  setSelectedResidence(event: any) {
    this.loadingResidence = true;
    this.aplicationForm.get("extra_ids").setValue([]);
    this.aplicationForm.get("regime_id").setValue(null);
    this.aplicationForm.get("preferred_typology_id").setValue(null);

    this.residencesService
      .getResidenceById(event)
      .pipe(
        first(),
        finalize(() => (this.loadingResidence = false))
      )
      .subscribe((result) => {
        this.residenceSelected = result.data;
        this.regimes = this.residenceSelected[0].regimes;
        this.extrasList = this.residenceSelected[0].extras;
        this.typologies = this.residenceSelected[0].typologies;
      });
  }

  cancelApplication(): void {
    this.router.navigateByUrl("accommodation/list-residences");
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== "VALID");
  }

  getOrganicUnits() {
    if (this.organicUnits.length === 0) {
      this.organicUnitLoading = true;
      this.organicUnitsService.organicUnitsSchools().subscribe((result) => {
        this.organicUnits = result.data;
        this.organicUnitLoading = false;
      });
    }
  }

  incapacityValueChange() {
    if (this.aplicationForm.get("incapacity").value == true) {
      this.aplicationForm.controls.incapacity_type.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.incapacity_degree.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.incapacity_type.clearValidators();
      this.aplicationForm.controls.incapacity_degree.clearValidators();
    }
    this.aplicationForm.controls.incapacity_type.updateValueAndValidity();
    this.aplicationForm.controls.incapacity_degree.updateValueAndValidity();
  }

  next(): void {
    this.isSubmited = true;
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("full_name"),
          this.aplicationForm.get("applicant_birth_date"),
          this.aplicationForm.get("gender"),
          this.aplicationForm.get("father_name"),
          this.aplicationForm.get("mother_name"),
          this.aplicationForm.get("incapacity"),
          this.aplicationForm.get("incapacity_type"),
          this.aplicationForm.get("incapacity_degree"),
          this.aplicationForm.get("nationality"),
          this.aplicationForm.get("identification"),
          this.aplicationForm.get("document_type_id"),
          this.aplicationForm.get("tin"),
          this.aplicationForm.get("address"),
          this.aplicationForm.get("city"),
          this.aplicationForm.get("postal_code"),
          this.aplicationForm.get("household_distance"),
          this.aplicationForm.get("country"),
          this.aplicationForm.get("iban"),
        ]);
        break;
      }
      case 1: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("email"),
          this.aplicationForm.get("secundary_email"),
          this.aplicationForm.get("phone_1"),
          this.aplicationForm.get("phone_2"),
        ]);
        break;
      }
      case 2: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("residence_id"),
          this.aplicationForm.get("second_option_residence_id"),
          this.aplicationForm.get("dateRange"),
          this.aplicationForm.get("regime_id"),
          this.aplicationForm.get("extra_ids"),
          this.aplicationForm.get("has_used_residences_before"),
          this.aplicationForm.get("which_residence_id"),
          this.aplicationForm.get("which_room_id"),
        ]);
        break;
      }
      case 3: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("international_student"),
          this.aplicationForm.get("organic_unit_id"),
          this.aplicationForm.get("course_degree_id"),
          this.aplicationForm.get("course_id"),
          this.aplicationForm.get("course_year"),
          this.aplicationForm.get("student_number"),
          this.aplicationForm.get("admission_date"),
        ]);
        break;
      }
      case 4: {
        valid =
          this.checkValidityOfControls([
            this.aplicationForm.get("house_hould_elements"),
            this.aplicationForm.get("household_elements_number"),
            this.aplicationForm.get("household_total_income"),
            this.aplicationForm.get("income_source"),
            this.aplicationForm.get("other_income_source"),
            this.aplicationForm.get("patrimony_category_id"),
            this.aplicationForm.get("income_statement_delivered"),
            this.aplicationForm.get("househould_elements_in_university"),
          ]) && this.validIncomeStatementDelivered();
        break;
      }
      case 5: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("applied_scholarship"),
          this.aplicationForm.get("scholarship_value"),
        ]);
        break;
      }
      case 6:
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("observations"),
        ]);
        break;
    }

    if (valid) {
      this.isSubmited = false;
      this.openInfo = true;
      this.current += 1;
      this.changeContent();
      this.showErrorMessage = false;
    } else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }
  }

  first = true;
  changeTypeDoc() {
    if (this.first) {
      this.first = false;
      return;
    }
    this.disabledIdentification = false;
  }

  validIncomeStatementDelivered(): boolean {
    if (this.aplicationForm.get("income_statement_delivered").value) {
      if (this.fileListIncomeStatement.length == 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  onChangeIncomeSource(event) {
    this.other_income = false;
    if (event.length > 0) {
      event.forEach((income) => {
        if (income == "OTHER") {
          this.other_income = true;
        }
      });
      this.aplicationForm.get("income_source").setValue(event);
    } else {
      this.aplicationForm.get("income_source").setValue("");
    }

    if (this.other_income) {
      this.aplicationForm.controls.other_income_source.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.other_income_source.clearValidators();
    }
    this.aplicationForm.controls.other_income_source.updateValueAndValidity();
  }

  check(value): boolean {
    if (this.aplicationForm.get("income_source").value) {
      return this.aplicationForm.get("income_source").value.includes(value);
    } else {
      return false;
    }
  }

  changeCurrent(event) {
    this.current = Number(event);
    this.changeContent();
  }

  valuesForm() {
    this.isSubmited = true;
    let valid = this.checkValidityOfControls([
      this.aplicationForm.get("applicant_consent"),
    ]);
    if (valid) {
      this.residenceApplicationModel = new ApplicationModel();
      this.extrasSend = [];
      this.getNamesStep1();
      this.getNamesStep2();
      this.getNamesStep3();
      this.getNamesStep4();
      this.isSubmited = false;
      this.current += 1;
      this.changeContent();
      // 0 step
      // Identification
      this.residenceApplicationModel.full_name =
        this.aplicationForm.controls.full_name.value;
      this.residenceApplicationModel.applicant_birth_date =
        this.aplicationForm.controls.applicant_birth_date.value;
      this.residenceApplicationModel.identification =
        this.aplicationForm.controls.identification.value;
      this.residenceApplicationModel.document_type_id =
        this.aplicationForm.controls.document_type_id.value;
      this.residenceApplicationModel.tin =
        this.aplicationForm.controls.tin.value;
      this.residenceApplicationModel.iban =
        this.aplicationForm.controls.iban.value;
      this.residenceApplicationModel.allow_direct_debit =
        this.aplicationForm.controls.allow_direct_debit.value;
      this.residenceApplicationModel.nationality =
        this.aplicationForm.controls.nationality.value;
      this.residenceApplicationModel.gender =
        this.aplicationForm.controls.gender.value;
      // Affiliation
      this.residenceApplicationModel.father_name =
        this.aplicationForm.controls.father_name.value;
      this.residenceApplicationModel.mother_name =
        this.aplicationForm.controls.mother_name.value;
      // Incapacity
      this.residenceApplicationModel.incapacity =
        this.aplicationForm.controls.incapacity.value;
      if (this.residenceApplicationModel.incapacity) {
        this.residenceApplicationModel.incapacity_type =
          this.aplicationForm.controls.incapacity_type.value;
        this.residenceApplicationModel.incapacity_degree =
          this.aplicationForm.controls.incapacity_degree.value;
      } else {
        this.residenceApplicationModel.incapacity_type = null;
        this.residenceApplicationModel.incapacity_degree = null;
      }
      // Household address
      this.residenceApplicationModel.address =
        this.aplicationForm.controls.address.value;
      this.residenceApplicationModel.city =
        this.aplicationForm.controls.city.value;
      this.residenceApplicationModel.country =
        this.aplicationForm.controls.country.value;
      this.residenceApplicationModel.postal_code =
        this.aplicationForm.controls.postal_code.value;
      this.residenceApplicationModel.household_distance =
        +this.aplicationForm.controls.household_distance.value;

      // 1 step
      this.residenceApplicationModel.email =
        this.aplicationForm.controls.email.value;
      this.residenceApplicationModel.secundary_email = this.aplicationForm
        .controls.secundary_email.value
        ? this.aplicationForm.controls.secundary_email.value
        : null;
      this.residenceApplicationModel.phone_1 =
        this.aplicationForm.controls.phone_1.value;
      this.residenceApplicationModel.phone_2 =
        this.aplicationForm.controls.phone_2.value;
      // 2 step
      this.residenceApplicationModel.residence_id =
        this.aplicationForm.controls.residence_id.value;
      if (
        this.aplicationForm.controls.second_option_residence_id.value &&
        this.allow_optional_residence
      ) {
        this.residenceApplicationModel.second_option_residence_id =
          this.aplicationForm.controls.second_option_residence_id.value;
      } else {
        this.residenceApplicationModel.second_option_residence_id = null;
      }
      this.residenceApplicationModel.start_date =
        this.aplicationForm.controls.dateRange.value[0];
      this.residenceApplicationModel.end_date =
        this.aplicationForm.controls.dateRange.value[1];
      this.residenceApplicationModel.regime_id =
        this.aplicationForm.controls.regime_id.value;
      if (this.aplicationForm.controls.preferred_typology_id.value) {
        this.residenceApplicationModel.preferred_typology_id =
          this.aplicationForm.controls.preferred_typology_id.value;
      }
      if (this.aplicationForm.controls.extra_ids.value) {
        this.residenceApplicationModel.extra_ids =
          this.aplicationForm.controls.extra_ids.value;
      }
      if (
        this.isRenew &&
        this.aplicationForm.value.which_room_id &&
        this.applicantInfo &&
        this.aplicationForm.value.residence_id ===
          this.applicantInfo.residence_id
      ) {
        this.residenceApplicationModel.room_id =
          this.aplicationForm.value.which_room_id;
      }

      if (this.aplicationForm.controls.has_used_residences_before.value) {
        this.residenceApplicationModel.has_used_residences_before = true;
        this.residenceApplicationModel.which_residence_id =
          this.aplicationForm.controls.which_residence_id.value;
        this.residenceApplicationModel.which_room_id =
          this.aplicationForm.controls.which_room_id.value;
      } else {
        this.residenceApplicationModel.has_used_residences_before = false;
        this.residenceApplicationModel.which_residence_id = null;
        this.residenceApplicationModel.which_room_id = null;
      }
      // 3 step
      this.residenceApplicationModel.international_student =
        this.aplicationForm.controls.international_student.value;
      this.residenceApplicationModel.organic_unit_id =
        this.aplicationForm.controls.organic_unit_id.value;
      this.residenceApplicationModel.course_id =
        this.aplicationForm.controls.course_id.value;
      this.residenceApplicationModel.course_year =
        this.aplicationForm.controls.course_year.value;
      this.residenceApplicationModel.student_number =
        this.aplicationForm.controls.student_number.value;
      this.residenceApplicationModel.admission_date =
        this.aplicationForm.controls.admission_date.value;

      // 4 step
      this.residenceApplicationModel.house_hould_elements =
        this.aplicationForm.controls.house_hould_elements.value;
      this.residenceApplicationModel.household_elements_number =
        this.aplicationForm.controls.house_hould_elements.value.length;
      this.residenceApplicationModel.income_source =
        this.aplicationForm.controls.income_source.value;
      if (
        this.aplicationForm.controls.income_source.value &&
        this.aplicationForm.controls.income_source.value.includes("OTHER")
      ) {
        this.residenceApplicationModel.other_income_source =
          this.aplicationForm.controls.other_income_source.value;
      } else {
        this.residenceApplicationModel.other_income_source = null;
      }

      this.residenceApplicationModel.househould_elements_in_university = Number(
        this.aplicationForm.controls.househould_elements_in_university.value
      );
      this.residenceApplicationModel.patrimony_category_id =
        this.aplicationForm.controls.patrimony_category_id.value;

      if (
        typeof this.aplicationForm.controls.household_total_income.value ===
        "string"
      ) {
        this.residenceApplicationModel.household_total_income = parseFloat(
          Number(
            this.aplicationForm.controls.household_total_income.value.replace(
              ",",
              "."
            )
          ).toFixed(2)
        );
      } else {
        this.residenceApplicationModel.household_total_income =
          this.aplicationForm.controls.household_total_income.value;
      }
      this.residenceApplicationModel.income_statement_delivered = this
        .aplicationForm.controls.income_statement_delivered.value
        ? true
        : false;
      if (this.aplicationForm.controls.income_statement_delivered.value) {
        this.aplicationForm
          .get("income_statement_files_ids")
          .setValue(this.fileListIncomeStatement);
        this.residenceApplicationModel.income_statement_files_ids =
          this.aplicationForm.controls.income_statement_files_ids.value;
      } else {
        this.residenceApplicationModel.income_statement_files_ids = [];
      }

      // 5 step
      this.residenceApplicationModel.applied_scholarship =
        this.aplicationForm.controls.applied_scholarship.value;

      if (
        typeof this.aplicationForm.controls.scholarship_value.value === "string"
      ) {
        this.residenceApplicationModel.scholarship_value = parseFloat(
          Number(
            this.aplicationForm.controls.scholarship_value.value.replace(
              ",",
              "."
            )
          ).toFixed(2)
        );
      } else {
        this.residenceApplicationModel.scholarship_value =
          this.aplicationForm.controls.scholarship_value.value;
      }

      // 6 step
      if (this.aplicationForm.controls.observations.value) {
        this.residenceApplicationModel.observations =
          this.aplicationForm.controls.observations.value;
      } else {
        this.residenceApplicationModel.observations = "";
      }

      this.aplicationForm.get("file_ids").setValue(this.fileListOthers);
      this.residenceApplicationModel.file_ids =
        this.aplicationForm.controls.file_ids.value;

      // 7 step
      this.residenceApplicationModel.applicant_consent =
        this.aplicationForm.controls.applicant_consent.value;
      this.residenceApplicationModel.consent_date = new Date().toDateString();
    }
  }

  getNamesStep1() {
    this.documentTypes.forEach((data) => {
      if (data.id == this.aplicationForm.get("document_type_id").value)
        this.documentType = data;
    });
  }

  getNamesStep2() {
    this.residenceName = this.residenceSelected[0].name;
    this.residenceSelected[0].regimes.forEach((regime) => {
      if (regime.regime_id == this.aplicationForm.get("regime_id").value) {
        this.sendRegime = regime;
      }
    });
    this.residenceSelected[0].typologies.forEach((typ) => {
      if (typ.id == this.aplicationForm.get("preferred_typology_id").value) {
        this.sendTypologies = typ;
      }
    });
    if (
      this.residenceSelected[0].extras.length > 0 &&
      this.aplicationForm.get("extra_ids").value
    ) {
      this.residenceSelected[0].extras.forEach((extra) => {
        this.aplicationForm.get("extra_ids").value.forEach((extraApp) => {
          if (extra.extra_id == extraApp) {
            this.extrasSend.push(extra);
          }
        });
      });
    }
    if (this.aplicationForm.get("second_option_residence_id").value) {
      this.residencesExceptSelect.forEach((res) => {
        if (
          res.id == this.aplicationForm.get("second_option_residence_id").value
        ) {
          this.nameResidenceSelected = res.name;
        }
      });
    }
    if (this.aplicationForm.get("has_used_residences_before").value) {
      this.residences.forEach((res) => {
        if (res.id == this.aplicationForm.get("which_residence_id").value) {
          this.nameUsedResidenceSelected = res.name;
        }
      });
      this.roomsList.forEach((room) => {
        if (room.id == this.aplicationForm.get("which_room_id").value) {
          this.nameRoom = room.name;
        }
      });
    }
  }

  getNamesStep3() {
    this.organicUnits.forEach((organic) => {
      if (organic.id == this.aplicationForm.get("organic_unit_id").value) {
        this.nameOrganic = organic.name;
      }
    });
    this.courseDegreesList.forEach((course_degree) => {
      if (
        course_degree.id == this.aplicationForm.get("course_degree_id").value
      ) {
        this.nameCourseDegree = course_degree.name;
      }
    });

    this.coursesList.forEach((course) => {
      if (course.id == this.aplicationForm.get("course_id").value) {
        this.nameCourse = course.name;
      }
    });
  }

  getNamesStep4() {
    if (this.aplicationForm.get("patrimony_category_id").value) {
      this.listPatrimony.forEach((patrimony) => {
        if (
          patrimony.id == this.aplicationForm.get("patrimony_category_id").value
        ) {
          this.descPatrimony = patrimony.description;
        }
      });
    }
  }

  submitForm() {
    if (!this.aplicationForm.valid) return;

    if (this.idToUpdate && this.isUpdate) {
      this.loadingButton = true;
      this.applicationService
        .update(this.idToUpdate, this.residenceApplicationModel)
        .pipe(
          first(),
          finalize(() => (this.loadingButton = false))
        )
        .subscribe(() => {
          this.modalSuccess = true;
        });
    } else {
      this.loadingButton = true;
      this.applicationService
        .create(this.residenceApplicationModel)
        .pipe(
          first(),
          finalize(() => (this.loadingButton = false))
        )
        .subscribe(() => {
          this.modalSuccess = true;
        });
    }
  }

  closeModal() {
    this.modalSuccess = false;
    this.router.navigateByUrl("accommodation/list-residences");
  }

  previous(): void {
    this.current -= 1;
    this.openInfo = true;
    this.changeContent();
    this.showErrorMessage = false;
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = "identification";
        break;
      }
      case 1: {
        this.index = "contacts";
        break;
      }
      case 2: {
        this.index = "residence";
        break;
      }
      case 3: {
        this.index = "academic_data";
        break;
      }
      case 4: {
        this.index = "family";
        break;
      }
      case 5: {
        this.index = "social_benefits";
        break;
      }
      case 6: {
        this.index = "observations";
        break;
      }
      case 7: {
        this.index = "revision";
        break;
      }
      default: {
        this.index = "error";
      }
    }
  }

  onFileDeleted(fileId) {
    this.fileListIncomeStatement = this.fileListIncomeStatement.filter(
      (f) => f !== fileId
    );
    if (this.isUpdate) {
      let dataSend: any = {};
      dataSend.income_statement_files_ids = this.fileListIncomeStatement;
      dataSend.income_statement_delivered = true;
      this.applicationService
        .changeFileApplication(dataSend, this.applicantInfo.id)
        .pipe(first())
        .subscribe();
    }
  }

  onFileAdded(fileId) {
    this.fileListIncomeStatement.push(fileId);
  }

  onFileDeletedOther(fileId) {
    this.fileListOthers = this.fileListOthers.filter((f) => f !== fileId);
  }

  onFileAddedOther(fileId) {
    this.fileListOthers.push(fileId);
  }

  getResidenceRegulationLink() {
    const selectedResidence = this.residences.find(
      (r) => r.id === this.aplicationForm.value.residence_id
    );
    var win = window.open(selectedResidence.regulationFile.url, "_blank");
    win.focus();
  }

  internationalValueChange() {
    let add: FormArray;
    add = this.aplicationForm.get("house_hould_elements") as FormArray;
    if (this.aplicationForm.value.international_student == true) {
      this.aplicationForm.controls.househould_elements_in_university.clearValidators();
      this.aplicationForm.controls.income_source.clearValidators();
      this.aplicationForm.controls.household_total_income.clearValidators();
      this.aplicationForm.controls.patrimony_category_id.clearValidators();
      this.aplicationForm.controls.income_statement_delivered.clearValidators();
      add.controls.forEach((control) => {
        control.get("kinship").clearValidators();
        control.get("profession").clearValidators();
      });
    } else {
      this.aplicationForm.controls.househould_elements_in_university.setValidators(
        [Validators.required]
      );
      this.aplicationForm.controls.income_source.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.household_total_income.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.patrimony_category_id.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.income_statement_delivered.setValidators([
        Validators.required,
      ]);
      add.controls.forEach((control) => {
        control.get("kinship").setValidators([Validators.required]);
        control.get("profession").setValidators([Validators.required]);
      });
    }
    this.aplicationForm.controls.househould_elements_in_university.updateValueAndValidity();
    this.aplicationForm.controls.income_source.updateValueAndValidity();
    this.aplicationForm.controls.household_total_income.updateValueAndValidity();
    this.aplicationForm.controls.patrimony_category_id.updateValueAndValidity();
    this.aplicationForm.controls.income_statement_delivered.updateValueAndValidity();
    add.controls.forEach((control) => {
      control.get("kinship").updateValueAndValidity();
      control.get("profession").updateValueAndValidity();
    });
  }
}
