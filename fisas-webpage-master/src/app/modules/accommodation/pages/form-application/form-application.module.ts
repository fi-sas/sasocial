import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { FormApplicationComponent } from "./form-application.component";
import { FormApplicationRoutingModule } from "./form-application-routing.module";
import { AccommodationSharedModule } from "../../accommodation-shared.module";
import { ResumeApplicationModule } from "../../components/resume-application/resume-application.module";
@NgModule({
  declarations: [FormApplicationComponent],
  imports: [
    CommonModule,
    SharedModule,
    AccommodationSharedModule,
    ResumeApplicationModule,
    FormApplicationRoutingModule,
  ],
})
export class FormApplicationModule {}
