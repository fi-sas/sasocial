import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryChangeRequestsComponent } from './history-change-requests.component';


const routes: Routes = [
  {
    path: '',
    component: HistoryChangeRequestsComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryChangeRequestsRoutingModule { }
