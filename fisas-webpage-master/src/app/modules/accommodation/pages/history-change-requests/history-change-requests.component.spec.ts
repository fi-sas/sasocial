import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HistoryChangeRequestsComponent } from './history-change-requests.component';


describe('HistoryChangeRequestsComponent', () => {
  let component: HistoryChangeRequestsComponent;
  let fixture: ComponentFixture<HistoryChangeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryChangeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryChangeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
