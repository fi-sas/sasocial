import { Component, Input, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import {
  ApplicationChangeModel,
  ApplicationChangeStatusColor,
} from "../../models/application-change.model";
import { ApplicationModel } from "../../models/application.model";
import { AbsencesService } from "../../services/absences.service";
import { ApplicationsService } from "../../services/applications.service";
import { ExtensionsService } from "../../services/extensions.service";
import { ExtraChangeRequestsService } from "../../services/extra-change-requests.service";
import { MaintenanceRequestsService } from "../../services/maintenance-requests.service";
import { RegimeChangeRequestsService } from "../../services/regime-change-requests.service";
import { RoomChangeRequestsService } from "../../services/room-change-requests.service";
import { TariffChangeRequestsService } from "../../services/tariff-change-requests.service";
import { WithdrawalsService } from "../../services/withdrawals.service";
import { ApplicationChangesModalComponent } from "../../components/application-changes-modal/application-changes-modal.component";
import { ApplicationRoomChangesModalComponent } from "../../components/application-room-changes-modal/application-room-changes-modal.component";
import { ApplicationExtrasChangeModalComponent } from "../../components/application-extras-change-modal/application-extras-change-modal.component";
import { ApplicationRegimeChangeModalComponent } from "../../components/application-regime-change-modal/application-regime-change-modal.component";
import { ApplicationMaintenanceRequestModalComponent } from "../../components/application-maintenance-request-modal/application-maintenance-request-modal.component";
import { ApplicationScholarshipChangeModalComponent } from "../../components/application-scholarship-change-modal/application-scholarship-change-modal.component";
import { ApplicationCommunicationDateModalComponent } from "../../components/application-communication-date-modal/application-communication-date-modal.component";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { ApplicationIbanChangeModalComponent } from "../../components/application-iban-change-modal/application-iban-change-modal.component";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-history-change-requests",
  templateUrl: "./history-change-requests.component.html",
  styleUrls: ["./history-change-requests.component.less"],
})
export class HistoryChangeRequestsComponent implements OnInit {
  changeRequests: ApplicationChangeModel[] = null;
  loading = true;
  application: ApplicationModel = null;
  noApplication = false;
  ApplicationChangeStatusColor = ApplicationChangeStatusColor;
  loadingChanges = false;
  constructor(
    private applicationsService: ApplicationsService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private withdrawalsService: WithdrawalsService,
    private absencesService: AbsencesService,
    private extensionsService: ExtensionsService,
    private uiService: UiService,
    private extraChangeRequestsService: ExtraChangeRequestsService,
    private regimeChangeRequestsService: RegimeChangeRequestsService,
    private roomChangeRequestsService: RoomChangeRequestsService,
    private maintenanceRequestsService: MaintenanceRequestsService,
    private tariffRequestservice: TariffChangeRequestsService,
    private route: ActivatedRoute
  ) {
    if (this.route.snapshot.data.application)
      this.application = this.route.snapshot.data.application.data[0];
  }

  ngOnInit() {
    this.getUserApplication();
  }

  getUserApplication() {
    if (this.application) {
      this.noApplication = true;
      this.loadApplicationChangeRequests(this.application.id);
    } else {
      this.noApplication = false;
    }
  }

  loadApplicationChangeRequests(id) {
    this.loadingChanges = true;
    this.applicationsService
      .getAllApplicationChanges(id)
      .pipe(
        first(),
        finalize(() => (this.loadingChanges = false))
      )
      .subscribe((changes) => {
        this.changeRequests = changes.data;
      });
  }

  isPar(val) {
    return val % 2 === 0;
  }

  confirmCancel(id: number, type: string) {
    return this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "ACCOMMODATION.CHANGE_REQUESTS.CANCEL_MODAL_TITLE"
      ),
      nzContent: this.translateService.instant(
        "ACCOMMODATION.CHANGE_REQUESTS.CANCEL_MODAL_CONTENT"
      ),
      nzCancelText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT"
      ),
      nzOkText: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancel(id, type),
    });
  }

  cancel(id: number, type: string) {
    switch (type) {
      case "WITHDRAWAL":
        this.withdrawalsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "ABSENCE":
        this.absencesService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "EXTENSION":
        this.extensionsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "EXTRA":
        this.extraChangeRequestsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "REGIME":
        this.regimeChangeRequestsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "MAINTENANCE":
        this.maintenanceRequestsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "TARIFF":
        this.tariffRequestservice
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
      case "PERMUTE":
      case "RESIDENCE":
      case "TYPOLOGY":
      case "ROOM":
        this.roomChangeRequestsService
          .status(id, "CANCEL")
          .pipe(first())
          .subscribe((changes) => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "ACCOMMODATION.CHANGE_REQUESTS.SUCESSFULY_CANCELED"
              )
            );
            this.loadApplicationChangeRequests(this.application.id);
          });
        break;
    }
  }

  openDetailEdit(
    type: string,
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    if (type == "WITHDRAWAL" || type == "ABSENCE" || type == "EXTENSION") {
      this.openApplicationChangesModal(type, change, edit, consult);
    } else if (
      type == "TYPOLOGY" ||
      type == "RESIDENCE" ||
      type == "PERMUTE" ||
      type == "ROOM"
    ) {
      this.openApplicationRoomChangesModal(type, change, edit, consult);
    } else if (type == "EXTRA") {
      this.openApplicationExtrasChangesModal(change, edit, consult);
    } else if (type == "REGIME") {
      this.openApplicationRegimeChangesModal(change, edit, consult);
    } else if (type == "MAINTENANCE") {
      this.openMaintenanceRequestModal(change, edit, consult);
    } else if (type == "TARIFF") {
      this.openScholarshipRequestModal(change, edit, consult);
    } else if (type == "ENTRY_COMMUNICATION") {
      this.openCommunicationRequestModal(change, edit, consult, "ENTRY");
    } else if (type == "EXIT_COMMUNICATION") {
      this.openCommunicationRequestModal(change, edit, consult, "EXIT");
    } else if (type == "IBAN_CHANGE") {
      this.openIbanChangeRequestModal(change, edit, consult);
    }
  }

  openApplicationChangesModal(
    type: string,
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationChangesModalComponent,
        nzFooter: null,
        nzComponentParams: {
          type: type,
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openApplicationRoomChangesModal(
    type: string,
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationRoomChangesModalComponent,
        nzFooter: null,
        nzComponentParams: {
          type: type,
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openApplicationExtrasChangesModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationExtrasChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openApplicationRegimeChangesModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationRegimeChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openMaintenanceRequestModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationMaintenanceRequestModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openScholarshipRequestModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationScholarshipChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult: consult,
          edit: edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }

  openCommunicationRequestModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean,
    type: string
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationCommunicationDateModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult,
          edit,
          type,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }
  openIbanChangeRequestModal(
    change: ApplicationChangeModel,
    edit: boolean,
    consult: boolean
  ) {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: ApplicationIbanChangeModalComponent,
        nzFooter: null,
        nzComponentParams: {
          application: this.application,
          changesEdit: change,
          consult,
          edit,
        },
        nzWidth: 650,
        nzWrapClassName: "vertical-center-modal",
        nzOkText: "Submeter",
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) this.loadApplicationChangeRequests(this.application.id);
      });
  }
}
