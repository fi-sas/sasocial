import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { HistoryChangeRequestsRoutingModule } from "./history-change-requests-routing.module";
import { HistoryChangeRequestsComponent } from "./history-change-requests.component";
import { ApplicationNavbarModule } from "../../components/applications-navbar/application-navbar.module";
import { AccommodationPipesModule } from "../../pipes/accommodation-pipes.module";

@NgModule({
  declarations: [HistoryChangeRequestsComponent],
  imports: [
    CommonModule,
    SharedModule,
    AccommodationPipesModule,
    ApplicationNavbarModule,
    HistoryChangeRequestsRoutingModule,
  ],
})
export class HistoryChangeRequestsModule {}
