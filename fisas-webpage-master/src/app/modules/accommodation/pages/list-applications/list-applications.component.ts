import { Component, OnInit } from '@angular/core';
import { ApplicationModel, ApplicationStatus } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';
import { finalize, first } from 'rxjs/operators';
import { ConfigStatus } from '../../models/application.model';
import { ApplicationChangeStatusModel } from '../../models/application-change-status.model';
import { TranslateService } from '@ngx-translate/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { GeralSettingsService } from '../../services/geral-settings.service';
import { ApplicationPhasesService } from '../../services/application-phases.service';


@Component({
  selector: 'app-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less']
})
export class ListApplicationsComponent implements OnInit {
  oppositionModalVisible = false;
  applicationsPresent: ApplicationModel[] = [];
  applicationsHistory: ApplicationModel[] = [];
  panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];
  loading = true;
  configStatus = ConfigStatus;
  oppositionModalRequest = '';
  oppositionModalLoading = false;
  idOposition;
  unassigned_limit_days = 0;
 
  canRenew: boolean = false;
  applicationToRenewID: number = null;

  constructor(
    private applicationsService: ApplicationsService,
    private translateService: TranslateService,
    private uiService: UiService,
    private settingsService: GeralSettingsService,
    public applicationPhasesService: ApplicationPhasesService,
  ) { }

  ngOnInit() {
    this.getApplications();
    this.getConfigurations();
    this.canApply();
  }
  getConfigurations() {

  }

  canApply() {
    this.applicationsService.userCanApply().pipe(first()).subscribe(response => {
      if(response.data.length){
        this.canRenew = response.data[0].renew;
        this.applicationToRenewID = response.data[0].renew_application_id;
      }
    }, (error) => {
      console.log(error)
    });
  }

  cancelApplication(id) {
    this.applicationsService.studentCancel(id).subscribe(status => {
      if (status) {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS')
        );
        this.getApplications();
      }
    }, () => {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR')
      );
    });
  }


  getApplications() {
    this.settingsService.list().pipe(first()).subscribe((data) => {
      this.unassigned_limit_days = data.data[0].UNASSIGNED_OPPOSITION_LIMIT;
      this.applicationsService.list(0, -1).pipe(first(), finalize(() => this.loading = false)).subscribe(val => {
        this.applicationsPresent = val.data.filter((application) => {
          return !['closed', 'rejected', 'cancelled', 'withdrawal', 'unassigned'].includes(application.status) ||
            (application.status == 'unassigned' && application.unassigned_date != null && moment(new Date()).diff(moment(application.unassigned_date), "d") < this.unassigned_limit_days);
        });

        this.applicationsHistory = val.data.filter((application) => {
          return ['closed', 'rejected', 'cancelled', 'withdrawal'].includes(application.status) ||
            (application.status == 'unassigned' && !application.unassigned_date) ||
            (application.status == 'unassigned' && moment(new Date()).diff(moment(application.unassigned_date), "d") >= this.unassigned_limit_days);
        });
      });
    });
  }


  modalOpposeOk() {
    if (this.oppositionModalRequest === '') {
      this.uiService.showMessage(MessageType.warning,
        this.translateService.instant('VIEW_APPLICATION_STATUS.OPPOSITION_MODAL.ERRORS.EMPTY_REQUEST'));
      return;
    }

    this.oppositionModalLoading = true;
    this.applicationsService
      .studentOppose(this.idOposition, this.oppositionModalRequest)
      .pipe(
        first(),
        finalize(() => this.oppositionModalLoading = false)
      )
      .subscribe(status => {
        if (status) {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.OPPOSE.SUCCESS')
          );
          this.modalOpposeCancel();
          this.getApplications();
        }
      }, () => {
        this.uiService.showMessage(
          MessageType.error,
          this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.OPPOSE.ERROR')
        );
      });

  }

  modalOppose() {
    this.oppositionModalVisible = true;
    this.oppositionModalLoading = false;
    this.oppositionModalRequest = '';
  }

  modalOpposeCancel() {
    this.oppositionModalVisible = false;
    this.oppositionModalLoading = false;
    this.oppositionModalRequest = '';
  }

  opposeApplication(id) {
    this.idOposition = id;
    this.modalOppose();
  }

  rejectApplication(id) {
    this.applicationsService.studentReject(id).subscribe(status => {
      if (status) {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.SUCCESS')
        );
        this.getApplications();
      }
    }, () => {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.ERROR')
      );
    });
  }

  acceptedApplication(id) {
    this.applicationsService.studentApprove(id).subscribe(status => {
      if (status) {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.SUCCESS')
        );
        this.getApplications();
      }
    }, () => {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.ERROR')
      );
    });
  }

  withdrawalApplication(id) {
    const applicationStatus: ApplicationChangeStatusModel = { event: 'WITHDRAWAL', notes: 'Desistência submetida pelo estudante' };
    this.applicationsService.changeStatus(id, applicationStatus).subscribe(status => {
      if (status) {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAWAL.SUCCESS')
        );
        this.getApplications();
      }
    }, () => {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAWAL.ERROR')
      );
    });
  }



}
