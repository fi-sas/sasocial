import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ListApplicationsComponent } from "./list-applications.component";
import { ListApplicationsRoutingModule } from "./list-applications-routing.module";
import { ApplicationCardAccommodationComponent } from "../../components/card-application/card-application.component";
import { ApplicationCardPresentAccommodationComponent } from "../../components/card-application-present/card-application-present.component";
import { ResidenceNavbarModule } from "../../components/residences-navbar/residence-navbar.module";
import { AccommodationPipesModule } from "../../pipes/accommodation-pipes.module";

@NgModule({
  declarations: [
    ListApplicationsComponent,
    ApplicationCardAccommodationComponent,
    ApplicationCardPresentAccommodationComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ResidenceNavbarModule,
    ListApplicationsRoutingModule,
    AccommodationPipesModule,
  ],
})
export class ListApplicationsModule {}
