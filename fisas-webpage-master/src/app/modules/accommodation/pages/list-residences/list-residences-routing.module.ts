import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListResidencesComponent } from './list-residences.component';

const routes: Routes = [
  {
    path: '',
    component: ListResidencesComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:residences:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListResidencesRoutingModule { }
