import { StepDataService } from '@fi-sas/webpage/shared/services/step-data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ResidenceModel } from '../../models/residence.model';
import { ResidencesService } from '../../services/residences.service';
import { first, finalize } from 'rxjs/operators';
import { ApplicationsService } from '../../services/applications.service';
import { ApplicationDataService } from '@fi-sas/webpage/shared/services/application-data.service';
import { Router } from '@angular/router';
import { ApplicationModel, ApplicationStatus } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NzCarouselComponent, NzModalService } from 'ng-zorro-antd';
import { ApplicationPhase } from '../../models/application-phase.model';
import { ApplicationPhasesService } from '../../services/application-phases.service';
import { GeralSettingsService } from '../../services/geral-settings.service';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-list-residences',
  templateUrl: './list-residences.component.html',
  styleUrls: ['./list-residences.component.less']
})
export class ListResidencesComponent implements OnInit {

  residences: ResidenceModel[] = [];
  applicationsNumber = 0;
  applications: ApplicationModel[] = [];

  loadingButton = false;
  loading = false;

  open_phase: ApplicationPhase = null;
  loading_phase = false;
  userCanApply: boolean = false;
  unassigned_limit_days = 0;

  canRenew: boolean = false;
  applicationToRenewID: number = null;

  readonly serviceId = SERVICE_IDS.ACCOMODATION;

  constructor(
    public residencesService: ResidencesService,
    public applicationPhasesService: ApplicationPhasesService,
    public applicationsService: ApplicationsService,
    private applicationData: ApplicationDataService,
    private router: Router,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private settingsService: GeralSettingsService,
  ) {

  }

  ngOnInit() {
    this.loadingButton = true;
    this.getConfigurations();
    this.getOpenApplicatioPhase();
    this.getResidences();
    this.canApply();
  }
  getConfigurations() {
    this.settingsService.list().pipe(first()).subscribe((data) => {
      this.unassigned_limit_days = data.data[0].UNASSIGNED_OPPOSITION_LIMIT;
    });
  }
  canApply() {
    this.applicationsService.userCanApply().pipe(first()).subscribe(response => {
      if(response.data.length){
        this.userCanApply = response.data[0].can;
        this.canRenew = response.data[0].renew;
        this.applicationToRenewID = response.data[0].renew_application_id;
      }
    }, (error) => {
      console.log(error)
    });
  }

  getApplicationsNumber() {
    this.applicationsService.list(0, -1).pipe(first()).subscribe(res => {
      const applications = res.data;
      this.applications = applications.filter(application =>
        application.status !== ApplicationStatus.WITHDRAWAL &&
        application.status !== ApplicationStatus.CANCELLED &&
        application.status !== ApplicationStatus.REJECTED &&
        application.status !== ApplicationStatus.CLOSED &&
        application.status !== ApplicationStatus.UNASSIGNED || (
          application.status == ApplicationStatus.UNASSIGNED &&
          application.unassigned_date != null &&
          moment(new Date()).diff(moment(application.unassigned_date), "d") < this.unassigned_limit_days
        )
      );
      this.applicationsNumber = this.applications.length;
      this.loadingButton = false;
    });
  }

  getOpenApplicatioPhase() {
    this.loading_phase = true;
    this.applicationPhasesService.openApplication().pipe(
      first(),
      finalize(() => this.loading_phase = false)
    ).subscribe(result => {
      this.open_phase = result.data[0];
    });


  }

  getLastApplication(){

  }

  getResidences(): void {
    this.loading = true;
    this.residencesService.listVisibleUser(0, -1).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(residences => {
      this.residences = residences.data;
      this.getApplicationsNumber();
    });
  }

  // checkIfAsActivApplication(phase: ApplicationPhase): boolean {
  //   if (phase) {
  //     return !!this.applications.find(app => app.academic_year === phase.academic_year);
  //   }

  //   return false;
  // }


  arrowClick($event: MouseEvent, type: string, carourel:NzCarouselComponent) {
    $event.stopPropagation();
    if(type == 'next') {
      carourel.next();
    }else {
      carourel.pre();
    }
  }

  residenceSelected(residence: ResidenceModel, $event: MouseEvent) {
    $event.stopPropagation();

    if (!this.open_phase) {
      this.modalService.warning({
        nzTitle: this.translateService.instant('ACCOMMODATION.LIST_RESIDENCES.CLOSE_APPLICATION')
      });
      return;
    }

    this.applicationData.storeData({ formControlName: 'residence', data: [residence] });

    this.router.navigate(['accommodation', 'list-residences','form-application'], {
      queryParams: {
        residence_id: residence.id
      }
    });
  }

  isOutOfValidation(applicationPhase: ApplicationPhase): boolean {
    if (applicationPhase.out_of_date_validation) {
      return true;
    }
    return false;
  }

  isOutOfDate(applicationPhase: ApplicationPhase): boolean {

    if (!applicationPhase.out_of_date_from) {
      return false;
    }

    if (moment().isAfter(applicationPhase.out_of_date_from)) {
      return true;
    }

    return false;
  }

  getResidenceDetail(id) {
    this.router.navigateByUrl('accommodation/residence-detail/' + id);
  }

  renewApplication(){
    this.modalService.confirm({
      nzTitle: this.translateService.instant('ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_TEXT'),
      nzCancelText: this.translateService.instant('ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_NO'),
      nzOkText: this.translateService.instant('ACCOMMODATION.RENEW_APPLICATION_DIALOG.RENEW_YES'),
      nzOnOk: () => this.confirmRenewApplication()
    });
  }

  confirmRenewApplication(){
    this.router.navigate(['accommodation', 'list-residences','form-application'], {
      queryParams: {
        application_id: this.applicationToRenewID
      }
    });
  }

}
