import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ListResidencesComponent } from "./list-residences.component";
import { ListResidencesRoutingModule } from "./list-residences-routing.module";
import { ResidenceNavbarModule } from "../../components/residences-navbar/residence-navbar.module";

@NgModule({
  declarations: [ListResidencesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ResidenceNavbarModule,
    ListResidencesRoutingModule,
  ],
})
export class ListResidencesModule {}
