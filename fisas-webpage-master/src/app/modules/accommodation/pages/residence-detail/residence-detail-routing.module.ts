import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResidenceDetailComponent } from './residence-detail.component';



const routes: Routes = [
  {
    path: '',
    component: ResidenceDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidenceDetailRoutingModule { }
