import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { TranslateService } from "@ngx-translate/core";
import { NzCarouselComponent, NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { ApplicationPhase } from "../../models/application-phase.model";
import { ApplicationModel, ApplicationStatus } from "../../models/application.model";
import { ResidenceModel } from "../../models/residence.model";
import { ApplicationPhasesService } from "../../services/application-phases.service";
import { ApplicationsService } from "../../services/applications.service";
import { ResidencesService } from "../../services/residences.service";
import * as moment from 'moment';

@Component({
    selector: 'app-residence-detail',
    templateUrl: './residence-detail.component.html',
    styleUrls: ['./residence-detail.component.less'],
})
export class ResidenceDetailComponent implements OnInit {
    idDetail;
    loading = true;
    dataResidence: ResidenceModel;
    open_phase: ApplicationPhase = null;
    loading_phase = false;
    applications: ApplicationModel[] = [];

    constructor(private route: ActivatedRoute, public residencesService: ResidencesService,
        private modalService: NzModalService, private translateService: TranslateService,
        private applicationData: ApplicationDataService, public applicationsService: ApplicationsService,
        private router: Router, public applicationPhasesService: ApplicationPhasesService,) {

    }

    ngOnInit() {
        this.getOpenApplicatioPhase();
        this.getApplications();
        this.route.params.subscribe(async (params) => {
            this.idDetail = params['id'];
            if (this.idDetail) {
                this.getDetail(this.idDetail);
            }
        })
    }

    getDetail(id) {
        this.residencesService.getResidenceById(id).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
            this.dataResidence = data.data[0];
        })
    }

    getApplications() {
        this.applicationsService.list(0, -1).pipe(first()).subscribe(res => {
            const applications = res.data;
            this.applications = applications.filter(application =>
                application.status !== ApplicationStatus.WITHDRAWAL &&
                application.status !== ApplicationStatus.CANCELLED &&
                application.status !== ApplicationStatus.REJECTED &&
                application.status !== ApplicationStatus.CLOSED);
        });
    }

    getOpenApplicatioPhase() {
        this.loading_phase = true;
        this.applicationPhasesService.openApplication().pipe(
            first(),
            finalize(() => this.loading_phase = false)
        ).subscribe(result => {
            this.open_phase = result.data[0];
        });
    }

    backList() {
        this.router.navigate(['accommodation/list-residences']);
    }

    residenceSelected(residence: ResidenceModel) {

        if (!this.open_phase) {
            this.modalService.warning({
                nzTitle: this.translateService.instant('ACCOMMODATION.LIST_RESIDENCES.CLOSE_APPLICATION')
            });
            return;
        }

        this.applicationData.storeData({ formControlName: 'residence', data: [residence] });

        this.router.navigate(['accommodation', 'list-residences','form-application'], {
            queryParams: {
                residence_id: residence.id
            }
        });
    }

    checkIfAsActivApplication(phase: ApplicationPhase): boolean {
        if (phase) {
            return !!this.applications.find(app => app.academic_year === phase.academic_year);
        }

        return false;
    }

    isOutOfValidation(applicationPhase: ApplicationPhase): boolean {
        if (applicationPhase.out_of_date_validation) {
            return true;
        }
        return false;
    }

    isOutOfDate(applicationPhase: ApplicationPhase): boolean {

        if (!applicationPhase.out_of_date_from) {
            return false;
        }

        if (moment().isAfter(applicationPhase.out_of_date_from)) {
            return true;
        }

        return false;
    }

    arrowClick($event: MouseEvent, type: string, carourel:NzCarouselComponent) {
        $event.stopPropagation();
        if(type == 'next') {
          carourel.next();
        }else {
          carourel.pre();
        }
      }
}