import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ResidenceDetailComponent } from "./residence-detail.component";
import { ResidenceDetailRoutingModule } from "./residence-detail-routing.module";
import { ResidenceNavbarModule } from "../../components/residences-navbar/residence-navbar.module";

@NgModule({
  declarations: [ResidenceDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ResidenceNavbarModule,
    ResidenceDetailRoutingModule,
  ],
})
export class ResidenceDetailModule {}
