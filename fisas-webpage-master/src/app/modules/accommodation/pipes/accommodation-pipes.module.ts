import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FilterStatusConfigPipe } from "./filter-status-config.pipe";

@NgModule({
  declarations: [FilterStatusConfigPipe],
  exports: [FilterStatusConfigPipe],
  imports: [CommonModule],
})
export class AccommodationPipesModule {}
