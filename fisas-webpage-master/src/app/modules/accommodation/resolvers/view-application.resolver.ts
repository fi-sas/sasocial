import { Injectable } from '@angular/core';

import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Observable } from 'rxjs';
import { Resource } from '@fi-sas/core';
import { first } from 'rxjs/operators';
import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/accommodation/services/applications.service';

@Injectable()
export class ViewApplicationResolver
  implements Resolve<Observable<Resource<ApplicationModel>>> {
  constructor(
    private applicationService: ApplicationsService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    const id: number = parseInt(route.paramMap.get('id'), 10);

    return this.applicationService.read(id).pipe(first());

  }
}
