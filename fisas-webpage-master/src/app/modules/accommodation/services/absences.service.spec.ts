import { TestBed } from '@angular/core/testing';

import { AbsencesService } from './absences.service';
import { FiCoreModule } from '@fi-sas/core';

describe('AbsencesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: AbsencesService = TestBed.get(AbsencesService);
    expect(service).toBeTruthy();
  });
});
