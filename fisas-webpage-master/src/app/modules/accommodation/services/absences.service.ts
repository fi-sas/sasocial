import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AbsenceModel } from '@fi-sas/webpage/modules/accommodation/models/absence.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AbsencesService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(pageIndex: number, pageSize: number, applicationId: number, withRelated = 'file', sortKey?: string, sortValue?: string):
    Observable<Resource<AbsenceModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('application_id', applicationId.toString());
    params = params.set('withRelated', withRelated);
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<AbsenceModel>(this.urlService.get('ACCOMMODATION.ABSENCES'), { params });
  }

  create(absence: AbsenceModel): Observable<Resource<AbsenceModel>> {
    return this.resourceService.create<AbsenceModel>(this.urlService.get('ACCOMMODATION.ABSENCES', {}), absence);
  }

  patch(id, withdrawal: AbsenceModel): Observable<Resource<AbsenceModel>> {
    return this.resourceService.patch<AbsenceModel>(this.urlService.get('ACCOMMODATION.ABSENCES_ID', { id }), withdrawal);
  }

  status(id: number, event: string): Observable<Resource<AbsenceModel>> {
    return this.resourceService.create<AbsenceModel>(this.urlService.get('ACCOMMODATION.ABSENCES_STATUS', { id }), { event });
  }

}
