import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { CommunicationRequestModel } from '../models/application-communication.model';
import { ExtraChangeRequestModel } from '../models/extra-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationCommunicationsService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }


  create(communicationRequest: CommunicationRequestModel): Observable<Resource<CommunicationRequestModel>> {
    return this.resourceService.create<CommunicationRequestModel>(this.urlService.get('ACCOMMODATION.COMMUNICATION_REQUEST'), communicationRequest);
  }

  edit(id, communicationRequest: CommunicationRequestModel): Observable<Resource<CommunicationRequestModel>> {
    return this.resourceService.update<CommunicationRequestModel>(this.urlService.get('ACCOMMODATION.COMMUNICATION_REQUEST_ID', { id }), communicationRequest);
  }

}
