import { TestBed } from '@angular/core/testing';

import { ApplicationIbanChangesService } from './application-iban-changes.service';

describe('ApplicationIbanChangesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationIbanChangesService = TestBed.get(ApplicationIbanChangesService);
    expect(service).toBeTruthy();
  });
});
