import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ApplicationIbanChangeRequestModel } from '../models/Application-iban-change.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationIbanChangesService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }


  create(communicationRequest: ApplicationIbanChangeRequestModel): Observable<Resource<ApplicationIbanChangeRequestModel>> {
    return this.resourceService.create<ApplicationIbanChangeRequestModel>(this.urlService.get('ACCOMMODATION.IBAN_CHANGE_REQUEST'), communicationRequest);
  }

  edit(id, communicationRequest: ApplicationIbanChangeRequestModel): Observable<Resource<ApplicationIbanChangeRequestModel>> {
    return this.resourceService.update<ApplicationIbanChangeRequestModel>(this.urlService.get('ACCOMMODATION.IBAN_CHANGE_REQUEST_ID', { id }), communicationRequest);
  }

}
