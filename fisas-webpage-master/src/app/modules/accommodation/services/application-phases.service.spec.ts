import { TestBed } from '@angular/core/testing';

import { ApplicationPhasesService } from './application-phases.service';

describe('ApplicationPhasesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationPhasesService = TestBed.get(ApplicationPhasesService);
    expect(service).toBeTruthy();
  });
});
