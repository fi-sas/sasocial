import { Injectable } from '@angular/core';
import { FiUrlService, FiResourceService, Resource } from '@fi-sas/core';
import { ApplicationPhase } from '../models/application-phase.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { PeriodModel } from '../models/period.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationPhasesService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  openApplication(): Observable<Resource<ApplicationPhase>> {
    return this.resourceService.read<ApplicationPhase>(
      this.urlService.get('ACCOMMODATION.APPLICATION_PHASE_OPEN')
    );
  }

  getPeriodsByAcademicYear(academic_year: string) {
    let params = new HttpParams();
    params = params.set('query[academic_year]', academic_year);
    return this.resourceService.read<PeriodModel>(
      this.urlService.get('ACCOMMODATION.PERIODS'),
      { params }
    );
  }
}
