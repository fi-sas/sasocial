import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApplicationModel, UserCanApplyModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';
import { ApplicantInfoModel } from '@fi-sas/webpage/modules/accommodation/models/applicant-info.model';
import { ApplicationStatusModel } from '@fi-sas/webpage/modules/accommodation/models/application-status.model';
import { ApplicationChangeStatusModel } from '@fi-sas/webpage/modules/accommodation/models/application-change-status.model';
import { HttpParams } from '@angular/common/http';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { PatrimonyCategoriesModel } from '../models/patrimony-categories.model';
import { ApplicationChangeModel } from '../models/application-change.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  constructor(private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService) { }

  userApplicationIDSelected = new BehaviorSubject(null);

  list(offset: number, limit: number, contracted?: boolean) {
    let params = new HttpParams()
      .append('offset', offset.toString())
      .append('limit', limit.toString())
      .append('withRelated', 'currentRegime,assignedResidence,residence,room,extras,history');

    if(contracted)  {
      params = params.append('query[status]', 'contracted')
    }

    //    .append('user_id', this.authService.getUser().id.toString());
    return this.resourceService.list<ApplicationModel>(this.urlService.get('ACCOMMODATION.APPLICATIONS'), {
      params
    });
  }

  getPatrimonyCategories(): Observable<Resource<PatrimonyCategoriesModel>> {
    let params = new HttpParams();
    params = params.set('query[active]', 'true');
    params = params.set('sort', 'description');
    return this.resourceService.list<PatrimonyCategoriesModel>(
      this.urlService.get('ACCOMMODATION.PATRIMONY_CATEGORIES'),
      {
        params
      }
    );
  }

  create(application: ApplicationModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('ACCOMMODATION.APPLICATIONS', {}), application);
  }

  update(id: number, application: ApplicationModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.update<ApplicationModel>(this.urlService.get('ACCOMMODATION.APPLICATIONS_ID', { id }), application);
  }

  read(id: number): Observable<Resource<ApplicationModel>> {
    const params = new HttpParams()
      .append('withRelated', 'history,residence,regime,regime.translations,paymentMethod,room,course,house_hould_elements,extras,assignedResidence,second_option_residence,organic_unit,income_statement_files,whichRoom,whichResidence,document_type,preferred_typology,files');
    return this.resourceService.read<ApplicationModel>(this.urlService.get('ACCOMMODATION.APPLICATIONS_ID', { id }), { params });
  }

  changeStatus(id: number, applicationChangeStatus: ApplicationChangeStatusModel): Observable<Resource<ApplicationChangeStatusModel>> {
    return this.resourceService.create<ApplicationChangeStatusModel>
      (this.urlService.get('ACCOMMODATION.APPLICATION_CHANGE_STATUS', { id }), applicationChangeStatus);
  }

  studentApprove(id: number) {
    return this.resourceService.create<ApplicationModel>
      (this.urlService.get('ACCOMMODATION.STUDENT_APPROVE', { id }), {});
  }

  studentReject(id: number) {
    return this.resourceService.create<ApplicationModel>
      (this.urlService.get('ACCOMMODATION.STUDENT_REJECT', { id }), {});
  }

  studentOppose(id: number, complain: string) {
    return this.resourceService.create<ApplicationModel>
      (this.urlService.get('ACCOMMODATION.STUDENT_OPPOSE', { id }), {
        notes: 'Reclamação enviada por utilizador',
        application: {
          opposition_request: complain
        }
      });
  }
  studentCancel(id: number) {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('ACCOMMODATION.STUDENT_CANCEL', { id }), {
      notes: 'Cancelada pelo utilizador'
    });
  }

  applicantInfo(): Observable<Resource<ApplicantInfoModel>> {
    return this.resourceService.read<ApplicantInfoModel>(this.urlService.get('ACCOMMODATION.APPLICANT_INFO', {}), {});
  }

  changeFileApplication(data: any, id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.patch<ApplicationModel>(this.urlService.get('ACCOMMODATION.APPLICATIONS_ID', { id }), data);
  }

  print(id: number) {
    return this.resourceService.create<any>(this.urlService.get('ACCOMMODATION.PRINT', {}), { id });
  }

  applicationStatus(): Observable<Resource<ApplicationStatusModel>> {
    return this.resourceService.read<ApplicationStatusModel>(this.urlService.get('ACCOMMODATION.APPLICATION_STATUS', {}), {});
  }

  saveApplicationSelected(id: number) {
    this.userApplicationIDSelected.next(id);
  }

  getApplicationSelected(): ApplicationModel {
    return this.userApplicationIDSelected.getValue();
  }

  applicationSelectedObservable(): Observable<ApplicationModel> {
    return this.userApplicationIDSelected.asObservable();
  }

  getAllApplicationChanges(id: number): Observable<Resource<ApplicationChangeModel>> {
    return this.resourceService.list<ApplicationChangeModel>(this.urlService.get('ACCOMMODATION.APPLICATION_CHANGES', { id }), {});
  }

  userCanApply(): Observable<Resource<UserCanApplyModel>> {
    return this.resourceService.read<UserCanApplyModel>(this.urlService.get('ACCOMMODATION.APPLICATION_USER_CAN_APPLY', {}), {});
  }
}


