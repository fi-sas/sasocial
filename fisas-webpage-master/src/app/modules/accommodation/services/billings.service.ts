import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { Observable } from 'rxjs';
import { BillingModel } from '../models/billing.model';

@Injectable({
  providedIn: 'root'
})
export class BillingsService {

  constructor(private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService) { }

  list(application_id: number, pageIndex: number, pageSize: number): Observable<Resource<BillingModel>> {
    let params = new HttpParams()
    params = params.append('sort', 'billing_start_date');
    params = params.append('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.append('limit', pageSize.toString());
    params = params.append('withRelated', 'billing_items')
    params = params.append('query[application_id]', application_id.toString())
    params = params.append('query[status]', 'BILLED')
    params = params.append('query[status]', 'PAID');
    params = params.append('query[status]', 'CANCELLED');
    //params = params.append('query[status]', 'PROCESSED');
    return this.resourceService.list<BillingModel>(this.urlService.get('ACCOMMODATION.BILLINGS'), { params });
  }
}
