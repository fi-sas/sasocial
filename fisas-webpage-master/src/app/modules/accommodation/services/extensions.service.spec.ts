import { TestBed } from '@angular/core/testing';

import { ExtensionsService } from './extensions.service';
import { FiCoreModule } from '@fi-sas/core';

describe('ExtensionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: ExtensionsService = TestBed.get(ExtensionsService);
    expect(service).toBeTruthy();
  });
});
