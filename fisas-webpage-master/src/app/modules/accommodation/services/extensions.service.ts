import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { ExtensionModel } from '@fi-sas/webpage/modules/accommodation/models/extension.model';

@Injectable({
  providedIn: 'root'
})
export class ExtensionsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(pageIndex: number, pageSize: number, applicationId: number, sortKey?: string, sortValue?: string):
    Observable<Resource<ExtensionModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('application_id', applicationId.toString());
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<ExtensionModel>(this.urlService.get('ACCOMMODATION.EXTENSIONS'), { params });
  }

  create(extension: ExtensionModel): Observable<Resource<ExtensionModel>> {
    return this.resourceService.create<ExtensionModel>(this.urlService.get('ACCOMMODATION.EXTENSIONS', {}), extension);
  }

  patch(id, withdrawal: ExtensionModel): Observable<Resource<ExtensionModel>> {
    return this.resourceService.patch<ExtensionModel>(this.urlService.get('ACCOMMODATION.EXTENSIONS_ID', { id }), withdrawal);
  }

  status(id: number, event: string): Observable<Resource<ExtensionModel>> {
    return this.resourceService.create<ExtensionModel>(this.urlService.get('ACCOMMODATION.EXTENSIONS_STATUS', { id }), { event });
  }
}
