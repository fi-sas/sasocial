import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ExtraChangeRequestModel } from '../models/extra-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class ExtraChangeRequestsService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  status(id: number, event: string): Observable<Resource<ExtraChangeRequestModel>> {
    return this.resourceService.create<ExtraChangeRequestModel>(this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST_STATUS', { id }), { event });
  }

  create(extraChange: ExtraChangeRequestModel): Observable<Resource<ExtraChangeRequestModel>> {
    return this.resourceService.create<ExtraChangeRequestModel>(this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST'), extraChange);
  }

  edit(id, extraChange: ExtraChangeRequestModel): Observable<Resource<ExtraChangeRequestModel>> {
    return this.resourceService.update<ExtraChangeRequestModel>(this.urlService.get('ACCOMMODATION.EXTRA_CHANGE_REQUEST_ID', {id}), extraChange);
  }

}
