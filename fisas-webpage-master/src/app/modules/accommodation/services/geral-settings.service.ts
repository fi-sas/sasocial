import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Injectable } from '@angular/core';
import { GeralSettingsModel } from '../models/geral-settings.model';

@Injectable({
  providedIn: 'root'
})
export class GeralSettingsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  list(
    pageIndex: number = 0,
    pageSize: number = -1,
    sortKey?: string,
    sortValue?: string
  ): Observable<Resource<GeralSettingsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex) * pageSize).toString());
    params = params.set('limit', pageSize.toString());

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }

    return this.resourceService.list<GeralSettingsModel>(
      this.urlService.get('ACCOMMODATION.CONFIGURATIONS'),
      { params }
    );
  }
}
