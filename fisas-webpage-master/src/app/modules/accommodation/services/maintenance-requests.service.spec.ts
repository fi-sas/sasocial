import { TestBed } from '@angular/core/testing';

import { MaintenanceRequestsService } from './maintenance-requests.service';

describe('MaintenanceRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaintenanceRequestsService = TestBed.get(MaintenanceRequestsService);
    expect(service).toBeTruthy();
  });
});
