import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { MaintenanceRequestModel } from '../models/maintenance-request.model';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceRequestsService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  status(id: number, event: string): Observable<Resource<MaintenanceRequestModel>> {
    return this.resourceService.create<MaintenanceRequestModel>(this.urlService.get('ACCOMMODATION.MAINTENANCE_REQUEST_STATUS', { id }), { event });
  }

  create(maintenanceRequest: MaintenanceRequestModel): Observable<Resource<MaintenanceRequestModel>> {
    return this.resourceService.create<MaintenanceRequestModel>(this.urlService.get('ACCOMMODATION.MAINTENANCE_REQUEST'), maintenanceRequest);
  }

  patch(id, maintenanceRequest: MaintenanceRequestModel): Observable<Resource<MaintenanceRequestModel>> {
    return this.resourceService.patch<MaintenanceRequestModel>(this.urlService.get('ACCOMMODATION.MAINTENANCE_REQUEST_ID', {id}), maintenanceRequest);
  }

}
