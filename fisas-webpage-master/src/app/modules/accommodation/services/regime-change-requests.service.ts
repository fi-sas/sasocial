import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { RegimeChangeRequestModel } from '../models/regime-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class RegimeChangeRequestsService {


  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  status(id: number, event: string): Observable<Resource<RegimeChangeRequestModel>> {
    return this.resourceService.create<RegimeChangeRequestModel>(this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST_STATUS', { id }), { event });
  }

  create(changes: RegimeChangeRequestModel): Observable<Resource<RegimeChangeRequestModel>> {
    return this.resourceService.create<RegimeChangeRequestModel>(this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST'), changes);
  }

  edit(id, changes: RegimeChangeRequestModel): Observable<Resource<RegimeChangeRequestModel>> {
    return this.resourceService.update<RegimeChangeRequestModel>(this.urlService.get('ACCOMMODATION.REGIME_CHANGE_REQUEST_ID', {id}), changes);
  }

}
