import { TestBed } from '@angular/core/testing';
import { FiCoreModule } from '@fi-sas/core';
import { ResidencesService } from './residences.service';

describe('ResidencesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: ResidencesService = TestBed.get(ResidencesService);
    expect(service).toBeTruthy();
  });
});

