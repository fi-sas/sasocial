import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ResidenceModel } from '@fi-sas/webpage/modules/accommodation/models/residence.model';
import { PaymentMethodModel } from '../../payments/models/payment-method.model';

@Injectable({
  providedIn: 'root'
})
export class ResidencesService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  listVisibleUser(pageIndex: number = 0, pageSize: number = -1, sortKey?: string, sortValue?: string, withRelated: string = 'regulationFile,mediaIds'): Observable<Resource<ResidenceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', pageSize.toString());
    params = params.set('withRelated', withRelated.toString());
    params = params.set('query[active]', 'true');
    params = params.set('query[visible_for_users]', 'true');

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES'), { params });
  }

  list(pageIndex: number = 0, pageSize: number = -1, sortKey?: string, sortValue?: string, withRelated: string = 'regulationFile,mediaIds'): Observable<Resource<ResidenceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', pageSize.toString());
    params = params.set('withRelated', withRelated.toString());
    params = params.set('query[active]', 'true');

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES'), { params });
  }

  listAvailableForApplication(pageIndex: number = 0, pageSize: number = -1, sortKey?: string, sortValue?: string, withRelated: string = 'regulationFile,mediaIds'): Observable<Resource<ResidenceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', pageSize.toString());
    params = params.set('withRelated', withRelated.toString());
    params = params.set('query[active]', 'true');
    params = params.set('query[available_for_application]', 'true');

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES'), { params });
  }

  getResidenceById(id: number): Observable<Resource<ResidenceModel>> {

    return this.resourceService.read<ResidenceModel>(this.urlService.get('ACCOMMODATION.RESIDENCES_ID', { id }));
  }

  listPaymentMethods(residenceId: number): Observable<Resource<PaymentMethodModel>> {
    return this.resourceService.list<PaymentMethodModel>(
      this.urlService.get('ACCOMMODATION.RESIDENCES_PAYMENT_METHODS', {
        id: residenceId
      }), {});
  }

}
