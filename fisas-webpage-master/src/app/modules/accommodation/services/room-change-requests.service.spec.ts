import { TestBed } from '@angular/core/testing';

import { RoomChangeRequestsService } from './room-change-requests.service';

describe('RoomChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomChangeRequestsService = TestBed.get(RoomChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
