import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { RoomChangeRequestModel } from '../models/room-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class RoomChangeRequestsService {


  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  status(id: number, event: string): Observable<Resource<RoomChangeRequestModel>> {
    return this.resourceService.create<RoomChangeRequestModel>(this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST_STATUS', { id }), { event });
  }

  create(roomChange: RoomChangeRequestModel): Observable<Resource<RoomChangeRequestModel>> {
    return this.resourceService.create<RoomChangeRequestModel>(this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST'), roomChange);
  }

  edit(id, roomChange: RoomChangeRequestModel): Observable<Resource<RoomChangeRequestModel>> {
    return this.resourceService.update<RoomChangeRequestModel>(this.urlService.get('ACCOMMODATION.ROOM_CHANGE_REQUEST_ID', { id }), roomChange);
  }

}
