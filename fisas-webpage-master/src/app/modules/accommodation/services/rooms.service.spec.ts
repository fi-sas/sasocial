import { TestBed } from '@angular/core/testing';
import { FiCoreModule } from '@fi-sas/core';
import { RoomsService } from './rooms.service';

describe('RoomsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]}));
  it('should be created', () => {
    const service: RoomsService = TestBed.get(RoomsService);
    expect(service).toBeTruthy();
  });
});
