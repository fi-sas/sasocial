import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { RoomModel } from '@fi-sas/webpage/modules/accommodation/models/room.model';
import { RoomMapModel } from '../models/room-map.model';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }
  list(
      residenceId: number,
      pageIndex: number = 0,
      pageSize: number = -1,
      sortKey?: string,
      sortValue?: string): Observable<Resource<RoomModel>> {
    let params = new HttpParams();
    params = params.set('offset', "0");
    params = params.set('limit', pageSize.toString());
    params = params.set('query[residence_id]', residenceId.toString());
    params = params.set('query[active]', 'true');
    params = params.set('withRelated', null);
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<RoomModel>(this.urlService.get('ACCOMMODATION.ROOMS'), { params });
  }

  roomsMap(residence_id: number): Observable<Resource<RoomMapModel>> {
    return this.resourceService.list(this.urlService.get('ACCOMMODATION.ROOMS_MAP_RESIDENCE_ID',
      {
        id: residence_id,
      }));
  }
}
