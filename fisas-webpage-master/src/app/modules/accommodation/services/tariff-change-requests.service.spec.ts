import { TestBed } from '@angular/core/testing';

import { TariffChangeRequestsService } from './tariff-change-requests.service';

describe('TariffChangeRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TariffChangeRequestsService = TestBed.get(TariffChangeRequestsService);
    expect(service).toBeTruthy();
  });
});
