import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { TariffChangeRequest } from '../models/tariff-change-request.model';
import { TariffModel } from '../models/tariff.model';

@Injectable({
  providedIn: 'root'
})
export class TariffChangeRequestsService {


  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  status(id: number, event: string): Observable<Resource<TariffChangeRequest>> {
    return this.resourceService.create<TariffChangeRequest>(this.urlService.get('ACCOMMODATION.TARIFF_REQUEST_STATUS', { id }), { event });
  }

  create(tariffChange: TariffChangeRequest): Observable<Resource<TariffChangeRequest>> {
    return this.resourceService.create<TariffChangeRequest>(this.urlService.get('ACCOMMODATION.TARIFF_REQUEST'), tariffChange);
  }

  edit(id,tariffChange: TariffChangeRequest): Observable<Resource<TariffChangeRequest>> {
    return this.resourceService.update<TariffChangeRequest>(this.urlService.get('ACCOMMODATION.TARIFF_REQUEST_ID', {id}), tariffChange);
  }

}
