import { TestBed } from '@angular/core/testing';

import { WithdrawalsService } from './withdrawals.service';
import { FiCoreModule } from '@fi-sas/core';

describe('WithdrawalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: WithdrawalsService = TestBed.get(WithdrawalsService);
    expect(service).toBeTruthy();
  });
});
