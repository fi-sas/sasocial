import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { WithdrawalModel } from '@fi-sas/webpage/modules/accommodation/models/withdrawal.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WithdrawalsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(pageIndex: number, pageSize: number, applicationId: number, withRelated = 'file', sortKey?: string, sortValue?: string):
    Observable<Resource<WithdrawalModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('application_id', applicationId.toString());
    params = params.set('withRelated', withRelated);
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<WithdrawalModel>(this.urlService.get('ACCOMMODATION.WITHDRAWALS'), { params });
  }

  create(withdrawal: WithdrawalModel): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(this.urlService.get('ACCOMMODATION.WITHDRAWALS', {}), withdrawal);
  }

  patch(id, withdrawal: WithdrawalModel): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.patch<WithdrawalModel>(this.urlService.get('ACCOMMODATION.WITHDRAWALS_ID', { id }), withdrawal);
  }

  status(id: number, event: string): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(this.urlService.get('ACCOMMODATION.WITHDRAWALS_STATUS', { id }), { event });
  }
}
