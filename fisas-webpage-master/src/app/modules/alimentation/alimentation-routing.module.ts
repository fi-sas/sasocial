import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlimentationComponent } from '@fi-sas/webpage/modules/alimentation/alimentation.component';
import { DishMenuResolver } from './resolvers/dish-menu.resolver';


const routes: Routes = [
  {
    path: '',
    component: AlimentationComponent,
    children: [
      { path: '', redirectTo: 'menu', pathMatch: 'full' },
      {
        path: 'menu',
        loadChildren: './pages/meals/meals.module#MealsModule',
        data: { breadcrumb: 'ALIMENTATION.MENU' }
      },
      {
        path: 'menu_pack',
        loadChildren: './pages/meals-packs/meals-packs.module#MealsPacksModule',
        data: { breadcrumb: 'ALIMENTATION.MEALS_MENU_PACKS' }
      },
      
      {
        path: 'menu/:service_id/:menu_dish_id/:meal/:date',
        loadChildren: './pages/dish-detail/dish-detail.module#DishDetailModule',
        data: { breadcrumb: 'MISC.DETAIL' },
        resolve: { dish: DishMenuResolver }
      },
      {
        path: 'user-allergens',
        loadChildren: './pages/user-allergens/user-allergens.module#UserAllergensModule',
        data: { breadcrumb: 'ALIMENTATION.USER_ALLERGENS.FOOD_ALLERGIES' }
      },
      {
        path: 'pack-settings',
        loadChildren: './pages/pack-settings/pack-settings.module#PackSettingsModule',
        data: { breadcrumb: 'ALIMENTATION.ENTITIES_PREDEFINED.PACK_CONF' }
      },
      {
        path: 'purchased-tickets',
        loadChildren: './pages/purchased-tickets/purchased-tickets.module#PurchasedTicketsModule',
        data: { breadcrumb: 'ALIMENTATION.PURCHASED_TICKETS.PURCHASED_TICKETS' }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlimentationRoutingModule { }
