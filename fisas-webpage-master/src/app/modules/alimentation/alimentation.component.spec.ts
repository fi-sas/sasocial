import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlimentationComponent } from './alimentation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AlimentationComponent', () => {
  let component: AlimentationComponent;
  let fixture: ComponentFixture<AlimentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AlimentationComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        FiCoreModule,
        HttpClientTestingModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlimentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
