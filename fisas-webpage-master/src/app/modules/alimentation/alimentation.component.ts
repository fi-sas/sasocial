import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alimentation',
  template: '<router-outlet></router-outlet>'
})
export class AlimentationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
