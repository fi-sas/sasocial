import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlimentationComponent } from './alimentation.component';
import { AlimentationRoutingModule } from '@fi-sas/webpage/modules/alimentation/alimentation-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MenusService } from '@fi-sas/webpage/modules/alimentation/services/menus.service';
import { DishMenuResolver } from '@fi-sas/webpage/modules/alimentation/resolvers/dish-menu.resolver';
import { InsufficientFundsComponent } from './components/insufficient-funds/insufficient-funds.component';

@NgModule({
  declarations: [
    AlimentationComponent,
    InsufficientFundsComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    AlimentationRoutingModule
  ],
  providers: [
    MenusService,
    DishMenuResolver
  ]
})
export class AlimentationModule { }
