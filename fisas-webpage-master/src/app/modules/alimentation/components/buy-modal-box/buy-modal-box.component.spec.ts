import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyModalBoxComponent } from './buy-modal-box.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TicketComponent } from '@fi-sas/webpage/modules/alimentation/components/ticket/ticket.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CancelModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/cancel-modal-box/cancel-modal-box.component';
import { InsufficientFundsComponent } from '@fi-sas/webpage/modules/alimentation/components/insufficient-funds/insufficient-funds.component';

describe('BuyModalBoxComponent', () => {
  let component: BuyModalBoxComponent;
  let fixture: ComponentFixture<BuyModalBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BuyModalBoxComponent,
        TicketComponent,
        CancelModalBoxComponent,
        InsufficientFundsComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        SharedModule,
        NoopAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyModalBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
