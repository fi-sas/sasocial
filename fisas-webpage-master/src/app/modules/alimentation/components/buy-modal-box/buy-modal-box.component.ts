import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { CartService } from '@fi-sas/webpage/modules/payments/services/cart.service';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { MovementsService } from '@fi-sas/webpage/modules/current-account/services/movements.service';
import { CartService as CartService2 } from '@fi-sas/webpage/shared/services/cart.service';
import { ReservationCountService } from '@fi-sas/webpage/core/services/reservation-count.service';

@Component({
  selector: 'app-buy-modal-box',
  templateUrl: './buy-modal-box.component.html',
  styleUrls: ['./buy-modal-box.component.less']
})
export class BuyModalBoxComponent implements OnInit {

  @Input() dish;
  @Input() meal;
  @Input() date;
  @Output() successBuy = new EventEmitter();
  @Output() newBuy = new EventEmitter();
  @Output() refresh = new EventEmitter();
  languages;
  isVisible = false;
  isloadingPurchase = false;
  isloadingCard = false;
  paymentMethodId = 1;
  cartQuantity = 0;
  validProduct = '';
  stock = 999;

  constructor(private cartService: CartService,
    private translateService: TranslateService,
    private reservationCountService: ReservationCountService,
    private uiService: UiService,
    private config: FiConfigurator,
    private cartServices: CartService2,
    private movementsService: MovementsService) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  showBuyModal(): void {
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }


  ngOnInit() {
  }


  purchaseTicket() {

    if(this.isloadingPurchase)
      return;

    this.isloadingPurchase = true;
    this.cartServices.addToCartAlimentation(this.dish.id).pipe(
      first()).subscribe((data) => {
        this.goCheckout(data.data[0].account_id, data.data[0].items[data.data[0].items.length-1].id);
        
      })

  }

  goCheckout(idAccount: number, id_item: number) {
    let body: any = {};
    body.account_id = idAccount;
    body.item_id = id_item;
    this.cartService.cartCheckoutItems(body).pipe(first(), finalize(() => this.isloadingPurchase = false)).subscribe((data) => {
      this.successBuy.emit(true)
      this.handleCancel();
      this.reservationCountService.loadData();
      this.movementsService.getBalances();
      this.refresh.emit(true);
      this.newBuy.emit({ purchased: true });
    })
  }

  existProduct() {
    this.validProduct = '';
    this.cartQuantity = 0;
    this.stock = 999;
    this.cartServices.cartData$.subscribe(
      (cartObjects) => {
        cartObjects.forEach(cart => {
          if (cart.items.length) {
            cart.items.forEach(item => {
              if (item.extra_info && item.extra_info.menu_dish_id === this.dish.id) {
                this.cartQuantity = item.quantity;
                this.validProduct = item.id;
                this.stock = item.max_quantity;
              }
            });
          }
        });
      }
    );
  }

  addToCart() {
    this.isloadingCard = true;
    this.existProduct();
    this.cartQuantity = this.cartQuantity + 1;
    if (this.stock <= this.cartQuantity) {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('ALIMENTATION.STOCK')
      );
      this.isloadingCard = false;
    } else {
      if (this.validProduct == '') {
        this.cartServices.addToCartAlimentation(this.dish.id).pipe(
          first(), finalize(() => this.isloadingCard = false)
        ).subscribe(
          () => {
            this.cartServices.getAllProducts()
              .pipe(first())
              .subscribe(
                (result) => {
                  this.cartServices.getDataNotification();
                  this.handleCancel();
                }
              );
          },
          (error) => {
          }
        );
      } else {
        this.cartServices.changeQtdItem(this.validProduct, this.cartQuantity).pipe(first(), finalize(() => this.isloadingCard = false)).subscribe(() => {
          this.cartServices.getDataNotification();
          this.handleCancel();
        })
      }
    }
  }


}
