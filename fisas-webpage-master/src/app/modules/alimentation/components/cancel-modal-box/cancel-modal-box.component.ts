import { MovementsService } from './../../../current-account/services/movements.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { MenusService } from '@fi-sas/webpage/modules/alimentation/services/menus.service';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { ReservationCountService } from '@fi-sas/webpage/core/services/reservation-count.service';

@Component({
  selector: 'app-cancel-modal-box',
  templateUrl: './cancel-modal-box.component.html',
  styleUrls: ['./cancel-modal-box.component.less']
})
export class CancelModalBoxComponent implements OnInit {

  @Input() reservationID: number;
  @Input() nullable: Date;
  @Output() refresh = new EventEmitter;
  date = new Date();
  isVisible = false;
  isloadingCancel = false;
  modalResult: BehaviorSubject<boolean>;

  constructor(
              private menuService: MenusService,
              private movementsService: MovementsService,
              private translateService: TranslateService,
              private reservationCountService:ReservationCountService,
              private uiService: UiService) { }

  ngOnInit() {
  }

  showCancelModal(): BehaviorSubject<boolean> {
    this.modalResult = new BehaviorSubject(false);
    this.isVisible = true;
    return this.modalResult;
  }

  handleCancel(): void {
    this.modalResult.next(false);
    this.isVisible = false;
  }

  cancelTicket(): void {
    this.isloadingCancel = true;
    this.menuService.cancelTicket(this.reservationID).pipe(first()).subscribe(() => {
       this.uiService.showMessage(
         MessageType.success,
         this.translateService.instant('ALIMENTATION.CANCEL_TICKET.SUCCESS_CANCEL')
       );
       this.movementsService.getBalances();
       this.modalResult.next(true);
       this.refresh.emit(true);
       this.reservationCountService.loadData();
       this.handleCancel();
       this.isloadingCancel = false;
     }, (err) => {
        this.uiService.showMessage(
          MessageType.error,
          err.error.errors[0].message
        );
       this.isloadingCancel = false;

    });
  }

  validDate(dateCancel): boolean {
    if(!dateCancel || new Date(dateCancel) > this.date) {
      return true;
    }
    return false;
  }
}
