import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateBarComponent } from './date-bar.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FiCoreModule } from '@fi-sas/core';

describe('DateBarComponent', () => {
  let component: DateBarComponent;
  let fixture: ComponentFixture<DateBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DateBarComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        NoopAnimationsModule,
        SharedModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
