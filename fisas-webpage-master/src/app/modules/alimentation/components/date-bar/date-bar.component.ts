import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import * as moment from "moment";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-date-bar",
  templateUrl: "./date-bar.component.html",
  styleUrls: ["./date-bar.component.less"],
})
export class DateBarComponent implements OnInit {
  fifteenDays = [];
  fifteenMonth = [];

  startIndex;
  startDate;

  @Output() newDate = new EventEmitter();

  /*
   * Date Bar Types
   *  - weekday
   *  - months
   * */
  @Input() dateBarType = "weekday";

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    this.setDays();
  }

  daySelected(day): void {
    this.newDate.emit({ date: day });
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    if (this.dateBarType === "weekday") {
      const maxDate = moment().add(15, "day");

      while (currentDate.isBefore(maxDate)) {
        this.fifteenDays.push(indexDate.toDate());
        indexDate = indexDate.add(1, "day");
      }

      if (this.startDate) {
        const tdate = this.fifteenDays.find((d) =>
          moment(d).isSame(moment(this.startDate), "day")
        );

        this.startIndex = this.fifteenDays.indexOf(tdate);
      } else {
        this.startIndex = 0;
        this.newDate.emit({ date: this.fifteenDays[0] });
      }
    } else if (this.dateBarType === "months") {
      const maxMonth = moment().subtract(14, "months");

      while (currentDate.isAfter(maxMonth)) {
        this.fifteenMonth.push(indexDate.toDate());
        indexDate = indexDate.subtract(1, "months");
      }

      this.fifteenMonth.unshift(moment().add(1, "months").toDate());

      this.startIndex = 1;
      this.newDate.emit({ date: this.fifteenMonth[1] });
    }
  }
}
