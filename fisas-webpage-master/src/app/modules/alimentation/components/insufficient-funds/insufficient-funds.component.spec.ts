import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsufficientFundsComponent } from './insufficient-funds.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';

describe('InsufficientFundsComponent', () => {
  let component: InsufficientFundsComponent;
  let fixture: ComponentFixture<InsufficientFundsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InsufficientFundsComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        SharedModule,
        FiCoreModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsufficientFundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
