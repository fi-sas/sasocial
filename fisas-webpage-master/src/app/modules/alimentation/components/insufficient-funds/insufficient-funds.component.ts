import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-insufficient-funds',
  templateUrl: './insufficient-funds.component.html',
  styleUrls: ['./insufficient-funds.component.less']
})
export class InsufficientFundsComponent implements OnInit {

  isVisible = false;

  constructor() { }

  ngOnInit() {
  }

  showCancelModal(): void {
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

}
