import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishCardDetailComponent } from './dish-card-detail.component';
import { BuyModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/buy-modal-box/buy-modal-box.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FiCoreModule } from '@fi-sas/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TicketComponent } from '@fi-sas/webpage/modules/alimentation/components/ticket/ticket.component';
import { CancelModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/cancel-modal-box/cancel-modal-box.component';

describe('DishCardDetailComponent', () => {
  let component: DishCardDetailComponent;
  let fixture: ComponentFixture<DishCardDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DishCardDetailComponent,
        BuyModalBoxComponent,
        TicketComponent,
        CancelModalBoxComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        NoopAnimationsModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishCardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
