import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';
import { FiUrlService } from '@fi-sas/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-dish-card-detail',
  templateUrl: './dish-card-detail.component.html',
  styleUrls: ['./dish-card-detail.component.less']
})
export class DishCardDetailComponent implements OnInit {
  modalSuccess = false;
  public urlMedia = this.urlService.get('MEDIA.UPLOAD');

  @ViewChild('modalBox') buy;

  @Input() dish: MenuModel;
  @Input() meal;
  @Input() date;

  isAllergen = false;

  buyButton = {
    'margin-left': '10px',
    width: '56px',
    height: '24px',
    color: 'black',
    'font-size': '11px',
    'font-weight': 'bold',
    'line-height': '1.64',
    'padding-left': '11px'
  };

  constructor(private urlService: FiUrlService, private translateService:TranslateService) { }

  ngOnInit() {
    this.dish.allergens.forEach((dish)=> {
      if(dish.allergic) {
        this.isAllergen = true;
      }
    })
  }

  showBuyModal(): void {
    this.buy.showBuyModal();
  }

  nameBuy(translation: string, price: string) {
    return this.translateService.instant(translation) + ' ' + price + '€';
  }

  successBuy(event){
    if(event){
      this.modalSuccess = true;
    }
  }
}
