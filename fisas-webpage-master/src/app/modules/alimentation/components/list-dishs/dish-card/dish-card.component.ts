import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MenuModel } from '../../../models/menu.model';
import { formatDate } from '@angular/common';
import { FiUrlService } from '@fi-sas/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dish-card',
  templateUrl: './dish-card.component.html',
  styleUrls: ['./dish-card.component.less']
})
export class DishCardComponent implements OnInit {

  public urlMedia = this.urlService.get('MEDIA.FILE');
  @ViewChild('modalBox') buy;

  @Input() dish: MenuModel;
  @Input() meal;
  @Input() date;
  @Input() serviceID;

  @Output() newBuy = new EventEmitter();
  @Output() modalSuccess = new EventEmitter();
  @Output() refresh = new EventEmitter();


  buyButton = {
    float: 'right',
    height: '24px',
    'font-weight': 'bold',
    'line-height': '1.64',
    'padding-left': '11px'
  };

  constructor(private urlService: FiUrlService, private translateService:TranslateService) { }

  ngOnInit() {

    if (this.date) {
      this.date = formatDate(this.date, 'yyyy-MM-dd', 'pt-PT');
    } else {
      this.date = null;
    }
  }

  showBuyModal(): void {
    this.buy.showBuyModal();
  }

  returnBuy(purchased): void {
    this.newBuy.emit(purchased);
  }

  refreshMenu(event) {
    if(event){
      this.refresh.emit(true);
    }
  
  }

  getValue(name): string {
    return name.length>60 ? (name.substring(0,60) + '...') : name;
  }

  nameBuy(translation: string, price: string) {
    return this.translateService.instant(translation) + ' ' + price + '€';
  }

  successBuy(event){
    if(event){
      this.modalSuccess.emit(true);
    }
  }
}
