import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDishsComponent } from './list-dishs.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { DishCardComponent } from '@fi-sas/webpage/modules/alimentation/components/list-dishs/dish-card/dish-card.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FiCoreModule } from '@fi-sas/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { TicketComponent } from '@fi-sas/webpage/modules/alimentation/components/ticket/ticket.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BuyModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/buy-modal-box/buy-modal-box.component';
import { CancelModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/cancel-modal-box/cancel-modal-box.component';

describe('ListDishsComponent', () => {
  let component: ListDishsComponent;
  let fixture: ComponentFixture<ListDishsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListDishsComponent,
        DishCardComponent,
        TicketComponent,
        BuyModalBoxComponent,
        CancelModalBoxComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDishsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
