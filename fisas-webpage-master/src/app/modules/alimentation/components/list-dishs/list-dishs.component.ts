import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { MenusService } from "@fi-sas/webpage/modules/alimentation/services/menus.service";
import { finalize, first } from "rxjs/operators";
import { MenuModel } from "@fi-sas/webpage/modules/alimentation/models/menu.model";
import { hasOwnProperty } from "tslint/lib/utils";
import * as moment from "moment";

@Component({
  selector: "app-list-dishs",
  templateUrl: "./list-dishs.component.html",
  styleUrls: ["./list-dishs.component.less"],
})
export class ListDishsComponent implements OnChanges, OnInit {
  @Input() serviceId;
  @Input() meal;
  @Input() date;
  @Input() nameService;
  @Output() newBuy = new EventEmitter();
  modalSuccess = false;
  menus: MenuModel[] = [];
  isLoading = true;

  constructor(private menuService: MenusService) {}

  ngOnChanges(changes: SimpleChanges) {
    if (hasOwnProperty(changes, "date")) {
      this.date = changes.date.currentValue;
    }
    if (hasOwnProperty(changes, "meal")) {
      this.meal = changes.meal.currentValue;
    }
    if (hasOwnProperty(changes, "serviceId")) {
      this.serviceId = parseInt(changes.serviceId.currentValue, 10);
    }

    if (
      (changes.serviceId &&
        changes.serviceId.previousValue != changes.serviceId.currentValue) ||
      (changes.meal &&
        changes.meal.previousValue != changes.meal.currentValue) ||
      (changes.date && changes.date.previousValue != changes.date.currentValue)
    ) {
      this.getMenu();
    }
  }

  ngOnInit() {}

  getMenu() {
    this.isLoading = true;
    if (this.date !== undefined) {
      this.menuService
        .menus(
          this.serviceId,
          moment(this.date.date).format("YYYY-MM-DD"),
          this.meal
        )
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe((menus) => {
          this.menus = menus.data;
          this.isLoading = false;
        });
    } else {
      this.isLoading = false;
    }
  }

  refreshMenu(event) {
    if (event) {
      this.getMenu();
    }
  }

  returnBuy(purchased): void {
    this.newBuy.emit(purchased);
  }

  modalSuccessChange(event) {
    if (event) {
      this.modalSuccess = true;
    }
  }
}
