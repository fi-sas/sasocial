import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishCardPackComponent } from './dish-card-pack.component';

describe('DishCardPackComponent', () => {
  let component: DishCardPackComponent;
  let fixture: ComponentFixture<DishCardPackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishCardPackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishCardPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
