import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FiUrlService } from '@fi-sas/core';
import { TranslateService } from '@ngx-translate/core';
import { finalize, first } from 'rxjs/operators';
import { PackMenuModel } from '../../../models/pack-menu.model';
import { PacksMenusService } from '../../../services/packs_menus.service';

@Component({
  selector: 'app-dish-card-pack',
  templateUrl: './dish-card-pack.component.html',
  styleUrls: ['./dish-card-pack.component.less']
})
export class DishCardPackComponent implements OnInit {

  public urlMedia = this.urlService.get('MEDIA.FILE');
  @ViewChild('modalBox') buy;

  @Input() dish: PackMenuModel;
  @Input() meal;
  @Input() date;
  @Input() serviceID;

  @Output() refresh = new EventEmitter();


  buyButton = {
    float: 'right',
    height: '24px',
    'font-size': '11px',
    'font-weight': 'bold',
    'line-height': '1.64',
    'padding-left': '11px'
  };

  constructor(
    private urlService: FiUrlService,
    private packMenuService: PacksMenusService) { }

  ngOnInit() {

    if (this.date) {
      this.date = formatDate(this.date, 'yyyy-MM-dd', 'pt-PT');
    } else {
      this.date = null;
    }
  }

  reserveMeal(dish: PackMenuModel): void {
    this.packMenuService.changeSelection(dish.user_meal_id, dish.id)
    .pipe(first(), finalize(() => null))
    .subscribe(rsp => {
      this.refreshMenu(true);
    });
  }


  refreshMenu(event) {
    if(event){
      this.refresh.emit(true);
    }

  }

  getValue(name): string {
    return name.length>60 ? (name.substring(0,60) + '...') : name;
  }


}
