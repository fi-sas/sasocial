import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPackDishsComponent } from './list-pack-dishs.component';

describe('ListPackDishsComponent', () => {
  let component: ListPackDishsComponent;
  let fixture: ComponentFixture<ListPackDishsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPackDishsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPackDishsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
