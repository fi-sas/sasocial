import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
import { first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import { PackMenuModel } from '../../models/pack-menu.model';
import { PacksMenusService } from '../../services/packs_menus.service';

@Component({
  selector: 'app-list-pack-dishs',
  templateUrl: './list-pack-dishs.component.html',
  styleUrls: ['./list-pack-dishs.component.less']
})
export class ListPackDishsComponent implements OnChanges, OnInit {

  @Input() serviceId;
  @Input() meal;
  @Input() date;
  @Input() nameService;

  menus: PackMenuModel[] = [];
  isLoading = true;

  modalSuccess = false;

  constructor(
    private packMenuService: PacksMenusService
  ) { }

  ngOnInit() {
    this.getMenu();
  }


  ngOnChanges(changes: SimpleChanges) {

    this.isLoading = true;

    if (hasOwnProperty(changes, 'date')) { this.date = changes.date.currentValue; }
    if (hasOwnProperty(changes, 'meal')) { this.meal = changes.meal.currentValue; }
    if (hasOwnProperty(changes, 'serviceId')) { this.serviceId = parseInt(changes.serviceId.currentValue, 10); }

    this.getMenu();
  }

  getMenu() {
    if (this.date !== undefined) {
        this.packMenuService.menus(this.serviceId, moment(this.date.date).format('YYYY-MM-DD'), this.meal).pipe(first()).subscribe(menus => {
          this.menus = menus.data;
          this.isLoading = false;
        }, () => {
          this.isLoading = false;
        });
    } else {
        this.isLoading = false;
    }
  }

  refreshMenu(event){
    if(event) {
      this.getMenu();
    }
  }

  returnBuy(purchased): void {
    //this.newBuy.emit(purchased);
  }

  modalSuccessChange(event){
    if(event){
      this.modalSuccess = true;
    }
  }

}
