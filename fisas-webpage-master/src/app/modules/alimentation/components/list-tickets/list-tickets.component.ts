import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MenusService } from '@fi-sas/webpage/modules/alimentation/services/menus.service';
import { first } from 'rxjs/operators';
import { ReservationModel } from '@fi-sas/webpage/modules/alimentation/models/reservation.model';
import * as moment from 'moment';
@Component({
  selector: 'app-list-tickets',
  templateUrl: './list-tickets.component.html',
  styleUrls: ['./list-tickets.component.less']
})
export class ListTicketsComponent implements OnChanges, OnInit {

  @Input() date = null;
  radioValue = 'NO_CONSUMED';
  purchased = true;
  isLoading = false;
  reservations: ReservationModel[] = [];
  reservationsFilter: ReservationModel[] = [];

  constructor(private menuService: MenusService) { }

  ngOnInit() {
    this.getReservations();
    this.radioValue = 'NO_CONSUMED';
  }

  ngOnChanges(changes: SimpleChanges) {

    this.isLoading = true;

    this.date = changes.date.currentValue;
    this.getReservations();
    this.radioValue = 'NO_CONSUMED';
  }

  changeRadio() {
    if (this.radioValue == 'CONSUMED') {
      this.filter('is_served');
    } else if (this.radioValue == 'EXPIRED') {
      this.filter('is_expired');
    } else if (this.radioValue == 'CANCELED') {
      this.filter('has_canceled');
    } else if (this.radioValue == 'ALL') {
      this.filter('');
    } else {
      this.filter('no_served');
    }
  }

  getReservations() {
    this.isLoading = true;

    if (this.date !== null) {
      this.menuService
        .userReservations(new Date(this.date.date).getFullYear(), new Date(this.date.date).getMonth() + 1, '').pipe(first())
        .subscribe(reservations => {
          this.reservations = reservations.data;
          this.reservations = this.reservations.filter(
            reservation =>
              reservation.menu_dish.dish.dish_type_translation = reservation.menu_dish.type.translations
          );
          this.reservations = this.reservations.filter(
            reservation => reservation.menu_dish.dish.location = reservation.menu_dish.menu.service.name
          );
          this.filter('no_served');
          this.isLoading = false;
        });
    } else {
      this.isLoading = false;
    }
  }

  filter(filter: string) {
    if (filter == 'is_served') {
      this.reservationsFilter = this.reservations.filter((res) => {
        return res.is_served;
      })
    } else if (filter == 'has_canceled') {
      this.reservationsFilter = this.reservations.filter((res) => {
        return res.has_canceled;
      })
    } else if (filter == 'is_expired') {
      this.reservationsFilter = this.reservations.filter((res) => {
        return res.is_available && moment().isAfter(moment(res.date).endOf('day'));
      })
    } else if (filter == 'no_served') {
      this.reservationsFilter = this.reservations.filter((res) => {
        return !res.is_served && !res.has_canceled && (!res.is_available || moment().isBefore(moment(res.date).endOf('day')));
      });
    }else {
      this.reservationsFilter = this.reservations;
    }
  }

  refreshReservations(event) {
    if (event) {
      this.getReservations();
    }
  }

}
