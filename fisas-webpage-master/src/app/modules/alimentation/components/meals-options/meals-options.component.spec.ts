import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MealsOptionsComponent } from './meals-options.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { FiCoreModule } from '@fi-sas/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

describe('MealsOptionsComponent', () => {
  let component: MealsOptionsComponent;
  let fixture: ComponentFixture<MealsOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MealsOptionsComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        NoopAnimationsModule,
        FiCoreModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
