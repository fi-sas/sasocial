import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MenusService } from "@fi-sas/webpage/modules/alimentation/services/menus.service";
import { first } from "rxjs/operators";
import { Meal } from "@fi-sas/webpage/modules/alimentation/models/menu.model";
import { hasOwnProperty } from "tslint/lib/utils";
import { SchoolModel } from "@fi-sas/webpage/modules/alimentation/models/school.model";

@Component({
  selector: "app-meals-options",
  templateUrl: "./meals-options.component.html",
  styleUrls: ["./meals-options.component.less"],
})
export class MealsOptionsComponent implements OnInit {
  @Input() showPurchasedTickets = true;
  @Input() showPacksSettings = false;
  @Input() schoolId = 7;
  @Output() newSuch = new EventEmitter();
  @Output() nameService = new EventEmitter();
  options = [
    {
      value: "lunch",
      translation: "ALIMENTATION.MEALS.LUNCH",
    },
    {
      value: "dinner",
      translation: "ALIMENTATION.MEALS.DINNER",
    },
  ];

  disabledOptions = true;
  schoolsLoading = true;
  schools: SchoolModel[] = [];
  ticketsCount = 0;
  meal = Meal;

  serviceValue: number;

  mealValue = null;

  constructor(private menusService: MenusService) {}

  ngOnInit() {
    this.getSchools();
    this.getReservationsCount();
  }

  onChangeService(serviceID) {
    this.serviceValue = serviceID;
    this.updateValues();
  }

  onChangeMeal(mealValue) {
    this.mealValue = mealValue;
    this.updateValues();
  }

  updateValues() {
    this.newSuch.emit({ service_id: this.serviceValue, meal: this.mealValue });
  }

  getReservationsCount() {
    this.menusService
      .userReservationsCount()
      .pipe(first())
      .subscribe((reservationsCount) => {
        this.ticketsCount = reservationsCount.data[0].total;
      });
  }

  getSchools() {
    this.menusService
      .schools()
      .pipe(first())
      .subscribe(
        (schools) => {
          this.schools = schools.data;
          this.schools = this.schools.filter(
            (school) =>
              (school.services = school.services.filter(
                (service) => service.type === "canteen"
              ))
          );
          this.schools = this.schools.filter(
            (school) => school.services.length !== 0
          );
          /*
          if (this.schools.length > 0) {
            if (hasOwnProperty(this.schools[0].services[0], "id")) {
              this.activatedRoute.queryParams
                .pipe(first())
                .subscribe((params) => {
                  const service = params["service_id"];
                  const meal = params["meal"];

                  if (service && meal) {
                    this.serviceValue = parseInt(service);
                    this.mealValue = meal;
                  }
                });
              }
            }
            */

          if (this.disabledOptions) this.updateValues();

          this.disabledOptions = false;
          this.schoolsLoading = false;
          this.findNameService(this.serviceValue);
        },
        () => {
          this.schoolsLoading = false;
        }
      );
  }

  findNameService(serviceId) {
    let name = "";
    this.schools.forEach((data) => {
      if (data.services.length > 0) {
        if (data.services.find((fil) => fil.id == serviceId)) {
          name = data.services.find((fil) => fil.id == serviceId).name;
        }
      }
    });
    this.nameService.emit(name);
  }

  getName(name): string {
    return name.length > 45 ? name.substring(0, 45) + "..." : name;
  }
}
