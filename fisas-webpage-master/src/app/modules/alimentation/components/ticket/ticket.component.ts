import { Subscription } from 'rxjs';
import { Component, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';
import * as moment from 'moment';
import {FiUrlService} from '@fi-sas/core';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.less']
})
export class TicketComponent implements OnInit {

  subs: Subscription;
  public urlMedia = this.urlService.get('MEDIA.UPLOAD');

  @ViewChild('modalBox') cancelTicket;
  @Input() disabled: boolean = false;
  @Input() dish: MenuModel = null;
  @Input() meal = 'lunch';
  @Input() nullable: Date;
  @Input() date = moment().format('YYYY-MM-DD');

  @Input() purchased = false;
  @Input() isServed = false;
  @Input() isAvailable = true;
  @Input() isFromPack = false;
  @Input() reservationId = 0;
  @Input() ticket = false;
  @Output() refresh = new EventEmitter;
  constructor(private urlService: FiUrlService) { }

  cancelPurchased(): void {
    this.subs = this.cancelTicket.showCancelModal().subscribe( result => {
      if (result) {
        
        this.isAvailable = false;
        this.disabled = true;
        this.refreshReservations(true);
      }
      if(this.subs) {
        this.subs.unsubscribe();
      }
    });
  }

  ngOnInit() { 
  }

  isExpired(): boolean {
    return this.isAvailable && moment().isAfter(moment(this.date).endOf('day'));
  }

  refreshReservations(event) {
    if(event) {
      this.refresh.emit(true);
    }
  }

}
