import { MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';

export class MenuMock {
  static dish: MenuModel = {
    id: 320,
    code: 'ALMOCOCARNE',
    price: 3.2,
    tax_id: 2,
    prices: [],
    available: false,
    show_derivatives: false,
    translations: [
      {
        language_id: 3,
        name: 'Arroz de Pato',
        description: 'Um arroz de pato é sempre uma boa ideia para uma festa'
      },
      {
        language_id: 4,
        name: 'Duck Rice',
        description: ''
      }
      ],
    location: 'cantina 1',
    user_allergic: false,
    show_allergens: true,
    nutrients: [],
    complements: [],
    isStockAvailable: true,
    stockQuantity: 999,
    file: {
      id: 137,
      file_category_id: 1,
      public: true,
      type: 'IMAGE',
      mime_type: 'image/png',
      path: 'e79a08c504bf6ab42092f4e337d6f681-1545068743422.png',
      filename: 'imagem.png',
      weight: 23594,
      width: 395,
      height: 310,
      updated_at: new Date('2018-12-17T17:45:44.000Z'),
      created_at: new Date('2018-12-17T17:45:44.000Z'),
      translations: [
        {
          language_id: 3,
          file_id: 137,
          name: 'arroz-de-pato-516x310.jpg',
          updated_at: new Date('2018-12-17T17:45:44.000Z'),
          created_at: new Date('2018-12-17T17:45:44.000Z'),
          id: 101
        }
        ],
      category: {
        id: 1,
        file_category_id: null,
        name: 'Media File Category #1',
        description: 'Objectively plagiarize focused action items after innovative deliverables. ' +
          'Monotonectally incentivize low-risk high-yield portals whereas end-to-end e-business. ' +
          'Competently restore next-generation bandwidth vis-a-vis user-centric action items. ' +
          'Dynamically exploit multifunctional niche markets whereas resource sucking content. ' +
          'Seamlessly facilitate high-quality networks after collaborative e-markets.',
        updated_at: new Date('2018-08-01T17:31:07.000Z'),
        created_at: new Date('2018-08-01T17:31:07.000Z')
      },
      url: 'http://62.28.241.42:8070/uploads/e79a08c504bf6ab42092f4e337d6f681-1545068743422.png'
    },
    file_id: 137,
    service_id: 2,
    account_id: 1,
    dish_type_translation: [
      {
        language_id: 3,
        name: 'Carne'
      },
      {
        language_id: 4,
        name: 'Meat'
      }
      ],
    dish_type_id: 1,
    allergens: [],
    tax: {
      id: 2,
      name: 'IVA 0%',
      description: 'Taxa para artigos isentos de IVA',
      tax_value: 0,
      active: true,
      created_at: new Date('2018-12-12T16:55:12.000Z'),
      updated_at: new Date('2018-12-12T16:57:37.000Z')
    }
  };
}
