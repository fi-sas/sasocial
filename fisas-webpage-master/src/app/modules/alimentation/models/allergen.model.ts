import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';

export class AllergenModel {
  id: number;
  active: boolean;
  allergic?: boolean;
  created_at: Date;
  updated_at: Date;
  translations: TranslationModel[];
}
