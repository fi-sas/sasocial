import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';

export class ComplementModel {
  id: number;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  price: number;
  translations: TranslationModel[];
}
