import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';
import { NutrientModel } from '@fi-sas/webpage/modules/alimentation/models/nutrient.model';
import { ComplementModel } from '@fi-sas/webpage/modules/alimentation/models/complement.model';
import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { AllergenModel } from '@fi-sas/webpage/modules/alimentation/models/allergen.model';
import { TaxModel } from '@fi-sas/webpage/shared/models/tax.model';

export enum Meal {
  LUNCH = 'lunch',
  DINNER = 'dinner'
}

export class MenuModel {
  id: number;
  code: string;
  price: number;
  tax_id: number;
  prices: number[];
  available: boolean;
  show_derivatives: boolean;
  translations: TranslationModel[];
  show_allergens: boolean;
  user_allergic: boolean;
  nutrients: NutrientModel[];
  complements: ComplementModel[];
  isStockAvailable: boolean;
  stockQuantity: number;
  file: FileModel;
  file_id: number;
  service_id: number;
  account_id: number;
  dish_type_translation: TranslationModel[];
  dish_type_id: number;
  allergens: AllergenModel[];
  tax?: TaxModel;
  location: string;
}
