import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';
import { UnitModel } from '@fi-sas/webpage/modules/alimentation/models/unit.model';

export class NutrientModel {
  id: number;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  unit_id: number;
  ddr: number;
  nutrient: any;
  quantity: number;
  translations: TranslationModel[];
  unit?: UnitModel[];
}
