import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';
import { NutrientModel } from '@fi-sas/webpage/modules/alimentation/models/nutrient.model';
import { AllergenModel } from '@fi-sas/webpage/modules/alimentation/models/allergen.model';
import { Meal } from './menu.model';
import { FileModel } from '../../media/models/file.model';


export class PackMenuModel {
  meal: Meal;
	date: Date;
	id: number;
	file_id: number;
	dish_type_translation: TranslationModel[];
	type: any;
	dish_type_id: number;
  nutrients: NutrientModel[];
  allergens: AllergenModel[];
	user_allergic: boolean;
	service_id: number;
	translations: TranslationModel[];
	location: string;
	dish_id: number;
	available: boolean;
	pack_available: boolean;
	user_meal_id: number;
	pre_confirmed: boolean;
  	file?: FileModel;
	confirmed: boolean;
	reserved: boolean;
}
