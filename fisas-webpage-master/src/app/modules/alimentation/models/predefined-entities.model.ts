import { TranslationModel } from "@fi-sas/webpage/shared/models/translation.model";

export class PredefinedEntitiesModel {
    name: string;
    id: number;
    canteens: CanteensModel[];
    dishs_types: DishTypesModel[];
    user_defaults: UserDefaultsModel = null;
}

export class CanteensModel {
    name: string;
    id: number;
}

export class DishTypesModel {
    id: number;
    translations: TranslationModel;
}

export class UserDefaultsModel {
    id: number;
    user_id: number;
    canteen_lunch_id: number;
    canteen_dinner_id: number;
    dish_type_lunch_id: number;
    dish_type_dinner_id: number;
    entity_id: number;
}