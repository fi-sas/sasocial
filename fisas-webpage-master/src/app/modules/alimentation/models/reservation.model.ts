import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';
import { ServiceModel } from '@fi-sas/webpage/modules/alimentation/models/service.model';

export class ReservationModel {
  id: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  date: string;
  user_id: number;
  is_from_pack: boolean;
  is_available: boolean;
  is_served: boolean;
  has_canceled: boolean;
  menuDishId: number;
  fentityId: number;
  menu_dish: MenuDishModel;
  dish: Dish
}

export class ResevationCountModel {
  total: number;
}

class MenuDishModel {
  id: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  menuId: number;
  dishId: number;
  typeId: number;
  doses_available: number;
  available: boolean;
  nullable_until: Date;
  dish: Dish;
  type: Type;
  menu: Menu;
}

class Dish {
  id: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  file_id: number;
  translations: TranslationModel[];
  recipes_ids: number[];
  types_ids: number[];
  dish_type_translation?: TranslationModel[];
  location?: string;
  type: Type;
}

class Type {
    id: number;
    active: boolean;
    created_at: string;
    updated_at: string;
    tax_id: number;
    code: string;
    price: number;
    translations: TranslationModel[];
}

class Menu {
  id: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  service_id: number;
  meal: string;
  date: string;
  validated: boolean;
  service: ServiceModel;
}
