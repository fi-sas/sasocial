import { ServiceModel } from '@fi-sas/webpage/modules/alimentation/models/service.model';

export class SchoolModel {
  id: number;
  name: string;
  acronym: string;
  isDefault: boolean;
  active: boolean;
  services?: ServiceModel[];
}
