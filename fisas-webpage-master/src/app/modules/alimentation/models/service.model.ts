export class ServiceModel {
  id: number;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  wharehouse_id: number;
  name: string;
  maintenance: boolean;
  type: string;
  school_id: string;
  service_id: number;
  device_ids: DeviceIDModel[];
}

class DeviceIDModel {
  device_id: number;
  service: {
    id: number
  };
}
