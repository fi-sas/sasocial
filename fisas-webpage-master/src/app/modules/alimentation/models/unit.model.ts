import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';

export class UnitModel {
  id: number;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  acronym: string;
  translations: TranslationModel[];
}
