import { TranslationModel } from '@fi-sas/webpage/modules/alimentation/models/translation.model';

export class UserAllergenModel {
  id: number;
  translations: TranslationModel[];
  allergic: boolean;
}
