import { Meal } from "./menu.model";

export class UserMealPackModel {
    user_pack_id: number;
    user_id: number;
    service_id: number;
    dish_type_id: number;
    menu_dish_id: number;
    date: Date;
    meal: Meal;
    reserved: boolean;
    reservation_id: number;
    created_at: Date;
    updated_at: Date;
}