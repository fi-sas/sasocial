import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DishDetailComponent } from './dish-detail.component';


const routes: Routes = [
  {
    path: '',
    component: DishDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishDetailRoutingModule { }
