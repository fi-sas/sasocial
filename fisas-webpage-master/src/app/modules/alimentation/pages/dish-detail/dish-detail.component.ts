import { Component, OnInit } from '@angular/core';
import { MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';
import { ActivatedRoute } from '@angular/router';
import { hasOwnProperty } from 'tslint/lib/utils';
import { Location } from '@angular/common';

@Component({
  selector: 'app-dish-detail',
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.less']
})
export class DishDetailComponent implements OnInit {

  dish: MenuModel = null;
  meal: string;
  date: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private location:Location) { }

  ngOnInit() {
    this.meal = this.activatedRoute.snapshot.params.meal;
    this.date = this.activatedRoute.snapshot.params.date;

    

    if ( hasOwnProperty(this.activatedRoute.snapshot.data, 'dish')) {
      this.dish = this.activatedRoute.snapshot.data.dish.data[0];
    }

  }

  back() {
    this.location.back();
  }

}
