import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { DishDetailRoutingModule } from './dish-detail-routing.module';
import { DishDetailComponent } from './dish-detail.component';
import { DishCardDetailComponent } from '../../components/list-dishs/dish-card-detail/dish-card-detail.component';
import { MealsModule } from '../meals/meals.module';


@NgModule({
  declarations: [
      DishDetailComponent, 
      DishCardDetailComponent,
    ],
  imports: [
    CommonModule,
    SharedModule,
    DishDetailRoutingModule,
    MealsModule
  ]
})
export class DishDetailModule { }
