import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MealsPacksComponent } from './meals-packs.component';

const routes: Routes = [
  {
    path: '',
    component: MealsPacksComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MealsPacksRoutingModule { }
