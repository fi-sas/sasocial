import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsPacksComponent } from './meals-packs.component';

describe('MealsPacksComponent', () => {
  let component: MealsPacksComponent;
  let fixture: ComponentFixture<MealsPacksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealsPacksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsPacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
