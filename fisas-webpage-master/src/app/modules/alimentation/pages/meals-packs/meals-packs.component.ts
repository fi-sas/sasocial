import { Component, OnInit, ViewChild } from '@angular/core';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-meals-packs',
  templateUrl: './meals-packs.component.html',
  styleUrls: ['./meals-packs.component.less']
})
export class MealsPacksComponent implements OnInit {

  @ViewChild('mealOptions') reservationCount;
  nameService: string = '';
  meal: string = null;
  serviceID: string = null;
  // '2019-07-01';
  date: string = null;

  readonly serviceId = SERVICE_IDS.FOOD;

  constructor() { }

  ngOnInit() {
  }

  getDate(date) {
    this.date = date;
  }

  reloadMeals(response): void {
    if(response.meal && response.service_id ) {
      this.meal = response.meal;
      this.serviceID = response.service_id;
    }    
  }

  returnBuy(purchased): void {

    if (purchased.purchased) {
      this.reservationCount.getReservationsCount();
    }
  }

  getName(event) {
    this.nameService = event;
  }

}
