import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MealsPacksRoutingModule } from './meals-packs-routing.module';
import { MealsPacksComponent } from './meals-packs.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedAlimentationModule } from '../../share-alimentation/shared-alimentation.module';
import { ListPackDishsComponent } from '../../components/list-pack-dishs/list-pack-dishs.component';
import { DishCardPackComponent } from '../../components/list-pack-dishs/dish-card-pack/dish-card-pack.component';

@NgModule({
  declarations: [
    MealsPacksComponent,
    ListPackDishsComponent,
    DishCardPackComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SharedAlimentationModule,
    MealsPacksRoutingModule
  ]
})
export class MealsPacksModule { }
