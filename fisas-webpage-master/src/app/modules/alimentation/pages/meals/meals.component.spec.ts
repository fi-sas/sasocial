import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsComponent } from './meals.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { DateBarComponent } from '@fi-sas/webpage/modules/alimentation/components/date-bar/date-bar.component';
import { MealsOptionsComponent } from '@fi-sas/webpage/modules/alimentation/components/meals-options/meals-options.component';
import { ListDishsComponent } from '@fi-sas/webpage/modules/alimentation/components/list-dishs/list-dishs.component';
import { DishCardComponent } from '@fi-sas/webpage/modules/alimentation/components/list-dishs/dish-card/dish-card.component';
import { TicketComponent } from '@fi-sas/webpage/modules/alimentation/components/ticket/ticket.component';
import { FiCoreModule } from '@fi-sas/core';
import { RouterTestingModule } from '@angular/router/testing';
import { BuyModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/buy-modal-box/buy-modal-box.component';
import { CancelModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/cancel-modal-box/cancel-modal-box.component';
import { SelectCanteensComponent } from '@fi-sas/webpage/modules/alimentation/components/select-canteens/select-canteens.component';

describe('MealsComponent', () => {
  let component: MealsComponent;
  let fixture: ComponentFixture<MealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MealsComponent,
        DateBarComponent,
        MealsOptionsComponent,
        ListDishsComponent,
        DishCardComponent,
        TicketComponent,
        BuyModalBoxComponent,
        CancelModalBoxComponent,
        SelectCanteensComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        NoopAnimationsModule,
        FiCoreModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
