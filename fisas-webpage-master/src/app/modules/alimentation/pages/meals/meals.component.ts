import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SERVICE_IDS } from "@fi-sas/webpage/shared/models";

import * as moment from "moment";
import { first } from "rxjs/operators";
import { DateBarComponent } from "../../components/date-bar/date-bar.component";
import { MealsOptionsComponent } from "../../components/meals-options/meals-options.component";

@Component({
  selector: "app-meals",
  templateUrl: "./meals.component.html",
  styleUrls: ["./meals.component.less"],
})
export class MealsComponent implements OnInit {
  @ViewChild("mealOptions") mealOptions: MealsOptionsComponent;
  @ViewChild("appDateBar") appDateBar: DateBarComponent;
  nameService: string = "";
  meal: string = null;
  serviceID: string = null;
  // '2019-07-01';
  date = null;

  readonly serviceId = SERVICE_IDS.FOOD;

  constructor(private route: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.pipe(first()).subscribe((params) => {
      if (params.service_id) {
        this.serviceID = params.service_id;
      }
      if (params.date) {
        this.date = { date: moment(params.date).toDate() };
      }
      if (params.meal) {
        this.meal = params.meal;
      }
    });
  }

  ngOnInit() {
    if (this.date) this.appDateBar.startDate = this.date.date;
    if (this.serviceID)
      this.mealOptions.serviceValue = parseInt(this.serviceID);
    if (this.meal) this.mealOptions.mealValue = this.meal;
  }

  getDate(date) {
    this.date = date;
    this.updatUrlValues();
  }

  reloadMeals(response): void {
    if (response.meal && response.service_id) {
      this.meal = response.meal;
      this.serviceID = response.service_id;
    }
    this.updatUrlValues();
  }

  returnBuy(purchased): void {
    if (purchased.purchased) {
      this.mealOptions.getReservationsCount();
    }
  }

  getName(event) {
    this.nameService = event;
  }

  updatUrlValues() {
    this.route.navigate([], {
      queryParams: {
        service_id: this.serviceID,
        meal: this.meal,
        date: moment(this.date.date).format("yyyy-MM-DD"),
      },
      relativeTo: this.activatedRoute,
    });
  }
}
