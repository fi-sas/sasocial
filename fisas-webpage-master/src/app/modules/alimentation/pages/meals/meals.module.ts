import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { DateBarComponent } from '../../components/date-bar/date-bar.component';
import { MealsComponent } from './meals.component';
import { MealsRoutingModule } from './meals-routing.module';

import { BuyModalBoxComponent } from '../../components/buy-modal-box/buy-modal-box.component';
import { CancelModalBoxComponent } from '../../components/cancel-modal-box/cancel-modal-box.component';
import { TicketComponent } from '../../components/ticket/ticket.component';
import { ListDishsComponent } from '../../components/list-dishs/list-dishs.component';
import { DishCardComponent } from '../../components/list-dishs/dish-card/dish-card.component';
import { PacksMenusService } from '../../services/packs_menus.service';
import { SharedAlimentationModule } from '../../share-alimentation/shared-alimentation.module';


@NgModule({
  declarations: [
      MealsComponent, 
      ListDishsComponent,
      BuyModalBoxComponent,
      TicketComponent,
      DishCardComponent,
      CancelModalBoxComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    SharedAlimentationModule,
    MealsRoutingModule,
  ],
  providers: [
    PacksMenusService
  ],
  exports: [
    DateBarComponent,
    BuyModalBoxComponent,
    TicketComponent,
    CancelModalBoxComponent
  ]
})
export class MealsModule { }
