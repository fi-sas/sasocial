import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackSettingsComponent } from './pack-settings.component';

const routes: Routes = [
  {
    path: '',
    component: PackSettingsComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PackSettingsRoutingModule { }