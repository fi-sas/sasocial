import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { WP_CONFIGURATION } from '@fi-sas/webpage/app.config';
import { PackSettingsComponent } from './pack-settings.component';

describe('PackSettingsComponent', () => {
  let component: PackSettingsComponent;
  let fixture: ComponentFixture<PackSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PackSettingsComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      providers: [
        { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
