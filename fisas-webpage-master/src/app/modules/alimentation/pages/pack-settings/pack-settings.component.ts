import { Component, OnInit } from "@angular/core";
import { Location } from '@angular/common';
import { MenusService } from "../../services/menus.service";
import { finalize, first } from "rxjs/operators";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { UiService, MessageType } from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { CanteensModel, DishTypesModel, PredefinedEntitiesModel, UserDefaultsModel } from "../../models/predefined-entities.model";

@Component({
    selector: 'app-pack-settings',
    templateUrl: './pack-settings.component.html',
    styleUrls: ['./pack-settings.component.less']
})

export class PackSettingsComponent implements OnInit {
    entitiesLoading = true;
    formPredefined: FormGroup;
    isLoading = false;
    submitted = false;
    entities: PredefinedEntitiesModel[] = [];
    canteens: CanteensModel[] = [];
    dishTypes: DishTypesModel[] = [];
    userDefaul: UserDefaultsModel = null;
    constructor(private fb: FormBuilder,
        private location: Location, private menusService: MenusService,
        private uiService: UiService, private translateService: TranslateService
    ) {
        this.getEntities();
    }

    ngOnInit() {
        this.formPredefined = this.fb.group({
            entity_id: new FormControl(null, [Validators.required]),
            canteen_lunch_id: new FormControl(null, [Validators.required]),
            canteen_dinner_id: new FormControl(null, [Validators.required]),
            dish_type_lunch_id: new FormControl(null, [Validators.required]),
            dish_type_dinner_id: new FormControl(null, [Validators.required]),
        })

    }
    get f() { return this.formPredefined.controls; }

    back() {
        this.location.back();
    }

    getName(name): string {
        return name.length > 45 ? (name.substring(0, 45) + '...') : name;
    }

    getEntities() {
        this.menusService.getPackConfigs().pipe(first(), finalize(() => this.entitiesLoading = false)).subscribe((data) => {
            this.entities = data.data;
        })
    }

    findValues(idEntity) {
        this.formPredefined.get('canteen_lunch_id').setValue(null);
        this.formPredefined.get('canteen_dinner_id').setValue(null);
        this.formPredefined.get('dish_type_lunch_id').setValue(null);
        this.formPredefined.get('dish_type_dinner_id').setValue(null);
        this.userDefaul = this.entities.find((data) => data.id == idEntity) ? this.entities.find((data) => data.id == idEntity).user_defaults : null;
        this.canteens = this.entities.find((data) => data.id == idEntity) ? this.entities.find((data) => data.id == idEntity).canteens : [];
        this.dishTypes = this.entities.find((data) => data.id == idEntity) ? this.entities.find((data) => data.id == idEntity).dishs_types : [];
        if (this.userDefaul) {
            this.formPredefined.get('canteen_dinner_id').setValue(this.userDefaul.canteen_dinner_id);
            this.formPredefined.get('canteen_lunch_id').setValue(this.userDefaul.canteen_lunch_id);
            this.formPredefined.get('dish_type_lunch_id').setValue(this.userDefaul.dish_type_lunch_id);
            this.formPredefined.get('dish_type_dinner_id').setValue(this.userDefaul.dish_type_dinner_id);
        }
    }


    onSubmit() {
        this.submitted = true;
        if (this.formPredefined.valid) {
            this.isLoading = true;
            this.submitted = false;
            const user_defaults: UserDefaultsModel = new UserDefaultsModel();
            user_defaults.canteen_dinner_id = this.formPredefined.get('canteen_dinner_id').value;
            user_defaults.canteen_lunch_id = this.formPredefined.get('canteen_lunch_id').value;
            user_defaults.dish_type_lunch_id = this.formPredefined.get('dish_type_lunch_id').value;
            user_defaults.dish_type_dinner_id = this.formPredefined.get('dish_type_dinner_id').value;
            user_defaults.entity_id = this.formPredefined.get('entity_id').value;
            this.menusService.postPackConfigs(user_defaults).pipe(first(), finalize(() => this.isLoading = false)).subscribe((data) => {
                this.uiService.showMessage(
                    MessageType.success,
                    this.translateService.instant('ALIMENTATION.USER_ALLERGENS.CHANGE_SAVED')
                );
                this.formPredefined.reset();
                this.getEntities();
            })

        }
    }
}