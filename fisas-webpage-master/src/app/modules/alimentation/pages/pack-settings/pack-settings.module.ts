import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { PackSettingsComponent } from './pack-settings.component';
import { PackSettingsRoutingModule } from './pack-settings-routing.module';


@NgModule({
  declarations: [
    PackSettingsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PackSettingsRoutingModule
  ]
})
export class PackSettingsModule { }