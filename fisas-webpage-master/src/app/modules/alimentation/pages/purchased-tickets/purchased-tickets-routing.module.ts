import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchasedTicketsComponent } from './purchased-tickets.component';


const routes: Routes = [
  {
    path: '',
    component: PurchasedTicketsComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchasedTicketsRoutingModule { }
