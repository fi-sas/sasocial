import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedTicketsComponent } from './purchased-tickets.component';
import { TicketComponent } from '@fi-sas/webpage/modules/alimentation/components/ticket/ticket.component';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FiCoreModule } from '@fi-sas/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { DateBarComponent} from '@fi-sas/webpage/modules/alimentation/components/date-bar/date-bar.component';
import { ListTicketsComponent } from '@fi-sas/webpage/modules/alimentation/components/list-tickets/list-tickets.component';
import { CancelModalBoxComponent } from '@fi-sas/webpage/modules/alimentation/components/cancel-modal-box/cancel-modal-box.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PurchasedTicketsComponent', () => {
  let component: PurchasedTicketsComponent;
  let fixture: ComponentFixture<PurchasedTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PurchasedTicketsComponent,
        TicketComponent,
        DateBarComponent,
        ListTicketsComponent,
        CancelModalBoxComponent
      ],
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        FiCoreModule,
        NoopAnimationsModule,
        SharedModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasedTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
