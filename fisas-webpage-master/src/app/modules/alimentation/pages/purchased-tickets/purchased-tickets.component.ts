import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-purchased-tickets',
  templateUrl: './purchased-tickets.component.html',
  styleUrls: ['./purchased-tickets.component.less']
})
export class PurchasedTicketsComponent implements OnInit {

  date = null;

  constructor(private location: Location) { }

  getDate(date) {
    this.date = date;
  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }

}
