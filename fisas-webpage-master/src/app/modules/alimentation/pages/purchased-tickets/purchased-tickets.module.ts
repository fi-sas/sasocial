import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { PurchasedTicketsComponent } from './purchased-tickets.component';
import { PurchasedTicketsRoutingModule } from './purchased-tickets-routing.module';
import { ListTicketsComponent } from '../../components/list-tickets/list-tickets.component';
import { MealsModule } from '../meals/meals.module';
import { DishDetailModule } from '../dish-detail/dish-detail.module';


@NgModule({
  declarations: [
    PurchasedTicketsComponent, 
    ListTicketsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PurchasedTicketsRoutingModule,
    MealsModule,
    DishDetailModule
  ],

})
export class PurchasedTicketsModule { }
