import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAllergensComponent } from './user-allergens.component';


const routes: Routes = [
  {
    path: '',
    component: UserAllergensComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAllergensRoutingModule { }