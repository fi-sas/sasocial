import { Component, OnInit } from '@angular/core';
import { MenusService } from '@fi-sas/webpage/modules/alimentation/services/menus.service';
import { first } from 'rxjs/operators';
import { UserAllergenModel } from '@fi-sas/webpage/modules/alimentation/models/user-allergen.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-allergens',
  templateUrl: './user-allergens.component.html',
  styleUrls: ['./user-allergens.component.less']
})
export class UserAllergensComponent implements OnInit {

  userAllergens: UserAllergenModel[] = [];
  loading = false;
  isAllergic = false;
  isloading = false;

  constructor(private menuService: MenusService,
    private location: Location,
    private uiService: UiService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.getUserAllergens();
  }

  getUserAllergens(): void {
    this.loading = true;
    this.menuService.userAllergens().pipe(first()).subscribe(allergens => {
      this.userAllergens = allergens.data;
      this.isAllergic = !!this.userAllergens.find(allergen => allergen.allergic);
      this.loading = false;
    });
  }

  changeAllergic(disableAll = false) {
    let allergensTrue;
    if (!disableAll) {
      allergensTrue = this.userAllergens.filter(a => a.allergic).map(a => a.id);
    } else {
      this.userAllergens.map(a => a.allergic = false);
      allergensTrue = [];
    }
    this.menuService.changeUserAllergen(allergensTrue).pipe(first()).subscribe(() => {

      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('ALIMENTATION.USER_ALLERGENS.CHANGE_SAVED')
      );
      this.isloading = false;

    }, () => {
      this.isloading = false;
    });
  }

  iHaveAllergic(isAllergic: boolean): void {
    this.isAllergic = isAllergic;
    if (!isAllergic) { this.changeAllergic(true); }
  }

  back() {
    this.location.back();
  }

}
