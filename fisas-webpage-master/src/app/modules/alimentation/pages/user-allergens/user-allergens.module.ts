import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { UserAllergensComponent } from './user-allergens.component';
import { UserAllergensRoutingModule } from './user-allergens-routing.module';


@NgModule({
  declarations: [
    UserAllergensComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UserAllergensRoutingModule
  ]
})
export class UserAllergensModule { }