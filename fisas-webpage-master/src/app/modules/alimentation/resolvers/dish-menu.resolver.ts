import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { first } from 'rxjs/operators';
import { MenusService } from '../services/menus.service';
import { MenuModel } from '../models/menu.model';

@Injectable()
export class DishMenuResolver
  implements Resolve<{data: MenuModel[]}> {
  constructor(
    private menusService: MenusService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {

    const service_id: number = parseInt(route.paramMap.get('service_id'), 10);
    const menu_dish_id: number = parseInt(route.paramMap.get('menu_dish_id'), 10);

    return this.menusService.menuDish(service_id, menu_dish_id).pipe(first());

  }
}
