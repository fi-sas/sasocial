import { TestBed } from '@angular/core/testing';

import { MenusService } from './menus.service';
import { FiCoreModule } from '@fi-sas/core';

describe('MenusService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: MenusService = TestBed.get(MenusService);
    expect(service).toBeTruthy();
  });
});
