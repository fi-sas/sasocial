import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { Meal, MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';
import { ServiceModel } from '@fi-sas/webpage/modules/alimentation/models/service.model';
import { SchoolModel } from '@fi-sas/webpage/modules/alimentation/models/school.model';
import { UserAllergenModel } from '@fi-sas/webpage/modules/alimentation/models/user-allergen.model';
import { ReservationModel, ResevationCountModel } from '@fi-sas/webpage/modules/alimentation/models/reservation.model';
import { PredefinedEntitiesModel, UserDefaultsModel } from '../models/predefined-entities.model';

@Injectable({
  providedIn: 'root'
})
export class MenusService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  /**
   * withRelated possible values:
   * - taxes
   */

  menus(service_id: number, date: string, meal: Meal, withRelated?: string):
    Observable<Resource<MenuModel>> {
    let params = new HttpParams();
    withRelated = 'taxes,file';
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    return this.resourceService.list<MenuModel>(this.urlService.get('ALIMENTATION.MENUS', { service_id, date, meal }), { params });
  }

  menuDish(service_id: number, MenuDish_id: number):
    Observable<Resource<MenuModel>> {
    let params = new HttpParams();
    const withRelated = 'taxes,file';
    params = params.set('withRelated', withRelated);

    return this.resourceService.read<MenuModel>(this.urlService.get('ALIMENTATION.MENU_DISH', { service_id, MenuDish_id }), { params });
  }

  services(school_id: number):
    Observable<Resource<ServiceModel>> {
    return this.resourceService.read<ServiceModel>(this.urlService.get('ALIMENTATION.MENU_SERVICES', { school_id }), {});
  }

  schools():
    Observable<Resource<SchoolModel>> {
    let params = new HttpParams();
    const withRelated = 'services';
    params = params.set('withRelated', withRelated);
    return this.resourceService.list<SchoolModel>(this.urlService.get('ALIMENTATION.MENU_SCHOOLS'), { params });
  }

  userAllergens(): Observable<Resource<UserAllergenModel>> {
    return this.resourceService.list<UserAllergenModel>(this.urlService.get('ALIMENTATION.USER_ALLERGENS', {}), {});
  }

  getPackConfigs(): Observable<Resource<PredefinedEntitiesModel>> {
    return this.resourceService.list<PredefinedEntitiesModel>(this.urlService.get('ALIMENTATION.PACK_CONFIG_GET', {}), {});
  }

  postPackConfigs(data: UserDefaultsModel): Observable<Resource<UserDefaultsModel>> {
    return this.resourceService.create<UserDefaultsModel>(this.urlService.get('ALIMENTATION.PACK_CONFIG_POST'), data );
  }

  changeUserAllergen(allergens: number[]): Observable<Resource<UserAllergenModel>> {
    return this.resourceService.update<UserAllergenModel>(this.urlService.get('ALIMENTATION.USER_ALLERGENS'), { allergens });
  }

  userReservations(year?: number, month?: number, onlyAvailables?: any): Observable<Resource<ReservationModel>> {
    let params = new HttpParams();
    if (year) {
      params = params.set('year', year.toString());
    }
    if (month) {
      params = params.set('month', month.toString());
    }
    if (onlyAvailables !== '') {
      params = params.set('onlyAvailables', onlyAvailables.toString());
    }
    params = params.set('sort', '-date');

    return this.resourceService.list<ReservationModel>(this.urlService.get('ALIMENTATION.RESERVATIONS', {}), { params });
  }

  userReservationsCount(): Observable<Resource<ResevationCountModel>> {

    return this.resourceService.read<ResevationCountModel>(this.urlService.get('ALIMENTATION.RESERVATIONS_COUNT', {}), {});
  }

  cancelTicket(reservationID: number): Observable<Resource<ReservationModel>> {
    return this.resourceService
      .create<ReservationModel>(this.urlService.get('ALIMENTATION.RESERVATIONS_CANCEL',
        { id: reservationID }),
        { payment_method_id: 1 });
  }
}
