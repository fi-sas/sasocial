import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { Meal, MenuModel } from '@fi-sas/webpage/modules/alimentation/models/menu.model';
import { PackMenuModel } from '../models/pack-menu.model';
import { UserMealPackModel } from '../models/user-meal-pack.model';

@Injectable({
  providedIn: 'root'
})
export class PacksMenusService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  menus(service_id: number, date: string, meal: Meal, withRelated?: string):
    Observable<Resource<PackMenuModel>> {
    let params = new HttpParams();
    withRelated = 'file';
    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }
    return this.resourceService.list<PackMenuModel>(this.urlService.get('ALIMENTATION_PACKS.MENUS', { service_id, date, meal }), { params });
  }

  menuDish(service_id:number, MenuDish_id: number):
    Observable<Resource<MenuModel>> {
    let params = new HttpParams();
    const withRelated = 'file';
    params = params.set('withRelated', withRelated);

    return this.resourceService.read<MenuModel>(this.urlService.get('ALIMENTATION_PACKS.MENU_DISH', { service_id, MenuDish_id }), { params });
  }

  changeSelection(user_meal_id:number, menu_dish_id: number):
  Observable<Resource<UserMealPackModel>> {

  return this.resourceService.create<UserMealPackModel>(
    this.urlService.get('ALIMENTATION_PACKS.CHANGE_RESERVE', {}),
    { 
      user_meal_id,
      menu_dish_id

    }, {});
}
}
