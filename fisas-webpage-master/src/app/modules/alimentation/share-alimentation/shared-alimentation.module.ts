import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateBarComponent } from '../components/date-bar/date-bar.component';
import { MealsOptionsComponent } from '../components/meals-options/meals-options.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    DateBarComponent,
    MealsOptionsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    DateBarComponent,
    MealsOptionsComponent,
  ],
})
export class SharedAlimentationModule { }
