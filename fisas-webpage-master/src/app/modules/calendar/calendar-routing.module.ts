import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'calendar', pathMatch: 'full' },
  {
    path: '',
    loadChildren: './pages/calendar/calendar.module#CalendarListModule',
    data: { breadcrumb: null, title: null }
  },
  {
    path: 'detail',
    loadChildren: './pages/detail-calendar/detail-calendar.module#CalendarDetailModule',
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }
