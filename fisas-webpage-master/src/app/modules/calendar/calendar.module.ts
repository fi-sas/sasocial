import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { CalendarRoutingModule } from './calendar-routing.module';
import { EventRecurrenceComponent } from './components/event-recurrence/event-recurrence.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    CalendarRoutingModule,
  ],
})
export class CalendarModule { }
