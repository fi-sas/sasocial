import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarListNormalComponent } from './calendar-list.component';


describe('CalendarListNormalComponent', () => {
  let component: CalendarListNormalComponent;
  let fixture: ComponentFixture<CalendarListNormalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarListNormalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarListNormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
