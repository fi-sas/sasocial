import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NzCarouselComponent } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { EventsCalendarModel } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";

@Component({
    selector: 'fi-list-normal-events',
    templateUrl: './calendar-list.component.html',
    styleUrls: ['./calendar-list.component.less']
})
export class CalendarListNormalComponent implements OnInit {
    @Input() localFilter = null;
    @Input() categoryFilter = null;
    
    
    pageIndex = 1;
    pageSize = 10;
    total = 0;
    events: EventsCalendarModel[] = [];
    loadingEvents = false;

    constructor(private router: Router,
        public eventsService: EventsCalendarService) {

    }

    ngOnInit() {
        this.getEvents();
    }

    getEvents() {
        this.loadingEvents = true;
        this.eventsService.listEvents(this.pageIndex,
            this.pageSize, this.localFilter, this.categoryFilter).pipe(first(), finalize(() => this.loadingEvents = false)).subscribe(events => {
                this.events = events.data;
                this.total = events.link.total;
            });
    }

    goDetail(id) {
        this.router.navigateByUrl('calendar/detail/' + id);
    }

    arrowClick($event: MouseEvent, type: string, carourel: NzCarouselComponent) {
        $event.stopPropagation();
        if (type == 'next') {
            carourel.next();
        } else {
            carourel.pre();
        }
    }

    paginationSearch(reset: boolean = false): void {
        if (reset) {
            this.pageIndex = 1;
        }
        this.getEvents();
    }
}