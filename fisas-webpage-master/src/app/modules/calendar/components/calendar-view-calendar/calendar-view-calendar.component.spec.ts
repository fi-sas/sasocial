import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarViewCalendarComponent } from './calendar-view-calendar.component';


describe('CalendarViewCalendarComponent', () => {
  let component: CalendarViewCalendarComponent;
  let fixture: ComponentFixture<CalendarViewCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarViewCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarViewCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
