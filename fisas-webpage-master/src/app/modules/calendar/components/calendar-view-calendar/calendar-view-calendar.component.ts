import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { EventObject, FullCalendarComponent, FullCalendarOptions } from "ngx-fullcalendar";
import { finalize, first } from "rxjs/operators";
import { EventCalendarViewModel, EventsCalendarModel } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";

@Component({
    selector: 'fi-calendar-view-calendar',
    templateUrl: './calendar-view-calendar.component.html',
    styleUrls: ['./calendar-view-calendar.component.less']
})
export class CalendarViewCalendarComponent implements OnInit {
    @ViewChild('calendar') calendarRef: FullCalendarComponent;
    @Input() localFilter = null;
    @Input() categoryFilter = null;

    options: FullCalendarOptions;
    eventsInCalendar: EventObject[] = [];;
    events: EventCalendarViewModel[] = [];

    selectType = 'month';
    currentLang;
    showView = false;
    loadingEvents = false;
    constructor(public eventsService: EventsCalendarService, private router: Router,
        public translate: TranslateService) {
        this.currentLang = translate.currentLang;
        translate.onLangChange.subscribe(() => {
            this.currentLang = translate.currentLang;
            this.calendarRef["calendar"].setOption('locale',this.currentLang)
            this.getEvents();
          });
    }

    ngOnInit() {
        this.getEvents();
        this.options = {
            defaultDate: new Date(),
            header: {
                left: '',
                center: '',
                right: '',
            },
            editable: false,
            locale: this.currentLang,
            droppable: false,
            eventResizableFromStart: true,
            eventStartEditable: false,
            eventDurationEditable: false
        };
    }

    getEvents() {
        this.showView = false;
        this.loadingEvents = true;

        this.eventsService.getCalendarEventsView(this.localFilter, this.categoryFilter).pipe(first(), finalize(() => {
            this.loadingEvents = false
            this.showView = true;
        }))
          .subscribe(response => {
            this.events = response.data;
            this.eventsInCalendar = [];
            this.events.forEach(event => this.eventsInCalendar.push({
              id: event.id,
              title: this.currentLang == 'pt' ? event.title.find(lang => lang.language_id === 3).title : event.title.find(lang => lang.language_id === 4).title,
              start: event.start,
              end: event.end,
              backgroundColor: `${event.backgroundColor}`,
              borderColor: `${event.borderColor}`,
              startEditable: false,
              allDay: event.all_day,
              })
            );
          });
    }

    getEventModal(event) {
      if(event && this.events){
        const eventFilter = this.events.find(e => e.id === event.id);
        if(eventFilter){
          this.router.navigateByUrl('calendar/detail/' + eventFilter.event_id);
        }
      }
    }

}
