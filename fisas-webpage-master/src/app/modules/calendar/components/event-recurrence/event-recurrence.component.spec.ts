import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventRecurrenceComponent } from './event-recurrence.component';

describe('EventRecurrenceComponent', () => {
  let component: EventRecurrenceComponent;
  let fixture: ComponentFixture<EventRecurrenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventRecurrenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventRecurrenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
