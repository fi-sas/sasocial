import { Component, Input, OnInit } from '@angular/core';
import { EventsCalendarModel } from '../../models/event.model';

@Component({
  selector: 'app-event-recurrence',
  templateUrl: './event-recurrence.component.html',
  styleUrls: ['./event-recurrence.component.less']
})
export class EventRecurrenceComponent implements OnInit {

  @Input() event : EventsCalendarModel;

  constructor() { }

  ngOnInit() {
  }

}
