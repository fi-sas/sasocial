import { TranslationModel } from "@fi-sas/webpage/shared/models/translation.model";


export class CategoriesCalendarModel {
    color: string;
    active: boolean;
    translations: TranslationModel[];
    id: number;
}