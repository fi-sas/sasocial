import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { cities } from "@fi-sas/webpage/shared/data/cities";
import { NzCarouselComponent } from "ng-zorro-antd";
import { EventObject, FullCalendarOptions } from "ngx-fullcalendar";
import { first } from "rxjs/operators";
import { CalendarListNormalComponent } from "../../components/calendar-list/calendar-list.component";
import { CalendarViewCalendarComponent } from "../../components/calendar-view-calendar/calendar-view-calendar.component";
import { CategoriesCalendarModel } from "../../models/category.model";
import { EventsCalendarModel } from "../../models/event.model";
import { CategoryCalendarService } from "../../services/category.events";

@Component({
    selector: 'fi-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.less']
})
export class CalendarComponent implements OnInit {
    @ViewChild("dataListNormalEvent") dataListNormalEvent: CalendarListNormalComponent = null;
    @ViewChild("dataListCalendarEvent") dataListCalendarEvent: CalendarViewCalendarComponent = null;
    categories: CategoriesCalendarModel[] = [];

    categoryFilter: number = null;
    colorCategory = '';
    localFilter = null;
    viewSelected: string = 'list';
    cities = cities;

    constructor(private categoryService: CategoryCalendarService,
        private router: Router) { }

    ngOnInit() {
        this.getCategories();

        this.cities = this.cities.sort((a, b) => {
            if (a.city > b.city) {
                return 1;
            }
            if (a.city < b.city) {
                return -1;
            }
            return 0;

        });
    }

    getCategories() {
        this.categoryService.listCategories().pipe(first()).subscribe((data) => {
            this.categories = data.data;
        })
    }


    filterCategory(value): void {
        const category = this.categories.find((category) => category.id == value);
        this.colorCategory = category ? category.color : '';
        this.categoryFilter = value;
        if (this.viewSelected == 'list') {
            this.dataListNormalEvent.categoryFilter = this.categoryFilter;
            this.dataListNormalEvent.getEvents();
        } else {
            this.dataListCalendarEvent.categoryFilter = this.categoryFilter;
            this.dataListCalendarEvent.getEvents();
        }
    }

    filterLocal(value) {
        this.localFilter = value;
        if (this.viewSelected == 'list') {
            this.dataListNormalEvent.localFilter = this.localFilter;
            this.dataListNormalEvent.getEvents();
        } else {
            this.dataListCalendarEvent.localFilter = this.localFilter;
            this.dataListCalendarEvent.getEvents();
        }
    }

}