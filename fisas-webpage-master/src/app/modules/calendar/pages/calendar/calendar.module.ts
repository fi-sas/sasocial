import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { CalendarComponent } from './calendar.component';
import { CalendarListRoutingModule } from './calendar-routing.module';
import { NgxFullCalendarModule } from 'ngx-fullcalendar';
import { CalendarListNormalComponent } from '../../components/calendar-list/calendar-list.component';
import { CalendarViewCalendarComponent } from '../../components/calendar-view-calendar/calendar-view-calendar.component';

@NgModule({
    declarations: [
        CalendarComponent,
        CalendarListNormalComponent,
        CalendarViewCalendarComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        CalendarListRoutingModule,
        NgxFullCalendarModule
    ]
})
export class CalendarListModule { }
