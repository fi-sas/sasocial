import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailCalendarComponent } from './detail-calendar.component';


const routes: Routes = [
  {
    path: ':id',
    component: DetailCalendarComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarDetailRoutingModule { }
