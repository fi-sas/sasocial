import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FiConfigurator } from "@fi-sas/configurator";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { MessageType, UiService } from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { NzCarouselComponent, NzModalService } from "ng-zorro-antd";
import { finalize, first } from "rxjs/operators";
import { EventsCalendarModel } from "../../models/event.model";
import { EventsCalendarService } from "../../services/events.service";
import * as moment from 'moment';
import { Location } from '@angular/common';

@Component({
    selector: 'fi-calendar-detail',
    templateUrl: './detail-calendar.component.html',
    styleUrls: ['./detail-calendar.component.less']
})
export class DetailCalendarComponent implements OnInit {
    event: EventsCalendarModel = new EventsCalendarModel();
    loadingDetail = true;
    logged = false;
    modalMi = false;
    modalSub = false;
    modalTerms = false;
    termsRead = false;
    loading = false;
    eventId;
    subscriptions = null;
    userId;

    languages: any;
    modalTermsInfo = false;

    constructor(private router: Router, private route: ActivatedRoute, private eventsCalendarService: EventsCalendarService,
        private authService: AuthService, private uiService: UiService, private translateService: TranslateService, private modalService: NzModalService,
        config: FiConfigurator, private location: Location) {
        this.logged = this.authService.getIsLogged();
        this.authService.getUserInfoByToken().subscribe((user) => {
            this.userId = user.data[0].id;
        })
        this.route.params.subscribe(async (params) => {
            this.eventId = params['id'];
            if (this.eventId) {
                this.getEventId();
            }
        })
        this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
    }

    ngOnInit() {

    }

    backList() {
      this.location.back();
    }

    getEventId() {
        this.eventsCalendarService.getEventById(this.eventId).pipe(first(), finalize(() => this.loadingDetail = false)).subscribe((data) => {
            this.event = data.data[0];
            if (data.data[0].subscriptions.length > 0) {
                this.subscriptions = data.data[0].subscriptions.find((sub) => sub.user_id == this.userId && !sub.has_removed);
            }

        })
    }

    arrowClick($event: MouseEvent, type: string, carourel: NzCarouselComponent) {
        $event.stopPropagation();
        if (type == 'next') {
            carourel.next();
        } else {
            carourel.pre();
        }
    }



    removeMi() {
        this.termsRead = false;
        this.modalService.confirm({
            nzTitle: this.translateService.instant('EVENTS.ALERTS.REMOVE_MI'),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
            nzOnOk: () => {
                this.loading = true;
                this.eventsCalendarService.removeSubscription(this.subscriptions.id).pipe(first(),finalize(()=> this.loading = false)).subscribe(() => {
                    this.uiService.showMessage(
                        MessageType.success,
                        this.translateService.instant('EVENTS.ALERTS.REMOVE_SUCCESS')
                    );
                    this.getEventId();
                })
            }
        });
    }

    removeSub() {
        this.termsRead = false;
        this.modalService.confirm({
            nzTitle: this.translateService.instant('EVENTS.ALERTS.REMOVE_SUB'),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
            nzOnOk: () => {
                this.loading = true;
                this.eventsCalendarService.removeSubscription(this.subscriptions.id).pipe(first(),finalize(()=> this.loading = false)).subscribe(() => {
                    this.uiService.showMessage(
                        MessageType.success,
                        this.translateService.instant('EVENTS.ALERTS.REMOVE_SUCCESS')
                    );
                    this.getEventId();
                })
            }
        });
    }

    openModalSub(event) {
        this.termsRead = false;
        if (event.status == 'CANCELLED') {
            this.uiService.showMessage(
                MessageType.error,
                this.translateService.instant('EVENTS.ALERTS.SUB_CANCELLED')
            );
            return;
        } else if (!this.logged) {
            this.uiService.showMessage(
                MessageType.error,
                this.translateService.instant('EVENTS.ALERTS.SUB_LOGGED')
            );
            return;
        } else if (!this.authService.hasPermission('calendar:event_subscriptions')) {
            this.uiService.showMessage(
                MessageType.error,
                this.translateService.instant('EVENTS.ALERTS.SUB_PERMISSION')
            );
            return;
        }
        this.modalTerms = true;
    }

    openModalMi(event) {
        this.termsRead = false;
        if (event.status == 'CANCELLED') {
            this.uiService.showMessage(
                MessageType.error, this.translateService.instant('EVENTS.ALERTS.MI_CANCELLED')
            );
            return;
        } else if (!this.logged) {
            this.uiService.showMessage(
                MessageType.error,
                this.translateService.instant('EVENTS.ALERTS.MI_LOGGED')
            );
            return;
        } else if (!this.authService.hasPermission('calendar:event_subscriptions')) {
            this.uiService.showMessage(
                MessageType.error,
                this.translateService.instant('EVENTS.ALERTS.MI_PERMISSION')
            );
            return;
        }

        this.modalTerms = true;
    }

    subscription() {
        if(this.termsRead){
            const consent_terms_text = this.event.terms_conditions.find(terms => terms.language_id === this.getCurrentLanguage());
            const data = {
                consent_terms: this.termsRead,
                consent_terms_text: consent_terms_text ? consent_terms_text.text : null ,
                event_id: this.event.id,
                event_date: this.event.start_date
            }
            this.loading = true;
            this.eventsCalendarService.saveSubscription(data).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
                this.modalTerms = false;
                if(this.event.type_subscription == 'MI') {
                    this.modalMi = true;
                }else {
                    this.modalSub = true;
                }

                this.getEventId();
            })
        }
    }

    openModalTerms(){
        this.modalTermsInfo = true;
    }

    getCurrentLanguage() {
        const currentLang = this.languages.find(lang => lang.acronym === this.translateService.currentLang);
        return currentLang.id;
    }

    getCalendarExport(event: EventsCalendarModel){
        let filename =  event.translations.find(t => t.language_id === this.getCurrentLanguage()).title + "_" + moment(event.start_date).format("YYYY-MM-DD") + "_"+ moment(event.end_date, "HH:mm:ss") +".ics";
        this.eventsCalendarService.generateCalendarExport(event.id, filename).subscribe(()=>{
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('EVENTS.ALERTS.MESSAGE_EXPORT_OK')
        );
      })
    }
}
