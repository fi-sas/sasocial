import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { DetailCalendarComponent } from './detail-calendar.component';
import { CalendarDetailRoutingModule } from './detail-calendar-routing.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { EventRecurrenceComponent } from '../../components/event-recurrence/event-recurrence.component';

@NgModule({
    declarations: [
        DetailCalendarComponent,
        EventRecurrenceComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        CalendarDetailRoutingModule,
        AngularOpenlayersModule
    ]
})
export class CalendarDetailModule { }
