import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { Observable } from "rxjs";
import { CategoriesCalendarModel } from "../models/category.model";

@Injectable({
    providedIn: 'root',
})
export class CategoryCalendarService {

    constructor(private resourceService: FiResourceService,
        private urlService: FiUrlService) { }

    listCategories(): Observable<Resource<CategoriesCalendarModel>> {
        let params = new HttpParams();
        params = params.set('offset', '0');
        params = params.set('limit', '-1');
        return this.resourceService.list<CategoriesCalendarModel>(this.urlService.get('EVENTS.CATEGORIES'), { params });
    }

}