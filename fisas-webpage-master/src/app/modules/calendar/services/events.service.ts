import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { Observable } from "rxjs";
import { first, map } from "rxjs/operators";
import { EventCalendarViewModel, EventsCalendarModel } from "../models/event.model";

@Injectable({
    providedIn: 'root',
})
export class EventsCalendarService {
    constructor(private resourceService: FiResourceService,
        private urlService: FiUrlService) {

    }

    listEvents(
        pageIndex: number,
        pageSize: number,
        localFilter?: string,
        categoryFilter?: string,
    ): Observable<Resource<EventsCalendarModel>> {

        let params = new HttpParams();
        params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
        params = params.set('limit', pageSize.toString());
        params = categoryFilter ? params.set('query[category_id]', categoryFilter.toString()) : params;
        params = localFilter ? params.set('query[city]', localFilter.toString()) : params;

        params = params.set('withRelated', 'images,category,translations,file,number_subscriptions,subscriptions');

        return this.resourceService.list<EventsCalendarModel>(this.urlService.get('EVENTS.EVENTS'), {
            params
        });
    }

    getEventById(id) {
        let params = new HttpParams();
        params = params.set('withRelated', 'images,category,translations,file,number_subscriptions,subscriptions,terms_conditions');
        return this.resourceService.read<EventsCalendarModel>(this.urlService.get('EVENTS.EVENTS_ID', {id}), {params});
    }

    saveSubscription(data): Observable<Resource<any>> {
        return this.resourceService.create<any>(
            this.urlService.get("EVENTS.SUBSCRIPTION"),
            data
          );
    }

    removeSubscription(id): Observable<Resource<any>> {
        return this.resourceService.create<any>(
            this.urlService.get("EVENTS.REMOVE_SUBSCRIPTION", { id }),
            {}
          );
    }

    generateCalendarExport(id: number, filename: string){
        const url = this.urlService.get('EVENTS.CALENDAR_EXPORT', {id})
        return this.resourceService.read(url, {}).pipe(first(), map((data => {
          this.downLoadFile(data.data, filename);
        })));
      }

      downLoadFile(file, filenameEvent) {
        let filename = filenameEvent ? filenameEvent : "event.ics";
        const downloadLink = document.createElement("a");
        downloadLink.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(file);
        downloadLink.download = filename;
        console.log(downloadLink)
        downloadLink.click();
      }

    getCalendarEventsView(localFilter?: string, categoryFilter?: string): Observable<Resource<EventCalendarViewModel>>{
      let params = new HttpParams();
      params = categoryFilter ? params.set('query[category_id]', categoryFilter.toString()) : params;
      params = localFilter ? params.set('query[city]', localFilter.toString()) : params;
      return this.resourceService.list<EventCalendarViewModel>(this.urlService.get('EVENTS.CALENDAR_VIEW'), { params });
    }
}
