import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { CartShoppingRoutingModule } from './cart-shopping-routing.module';
import { CartComponent } from './pages/cart/cart.component';

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CartShoppingRoutingModule,
  ],
  providers: [
  ]
})
export class CartShoppingModule { }
