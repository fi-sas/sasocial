import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { MovementsService } from '@fi-sas/webpage/modules/current-account/services/movements.service';
import { CartService } from '@fi-sas/webpage/shared/services/cart.service';
import { BalanceModel } from '@fi-sas/webpage/modules/current-account/models/balance.model';
import { NzMessageService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { ReservationCountService } from '@fi-sas/webpage/core/services/reservation-count.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit {

  cartData: any;
  toPayID = '';
  loadingCheckout = false;
  accounts: BalanceModel[];
  currentBalance: number = 0;
  constructor(
    private movementsService: MovementsService,
    public cartService: CartService,
    private translateService: TranslateService,
    private uiService: UiService,
    private message: NzMessageService,
    private reservationCountService:ReservationCountService
  ) {
    this.loadData();
  }

  ngOnInit() {
    this.getCartsAccount();
  }

  getCartTotal(cart) {
    let total = 0;
    for (const item of cart.items) {
      total += item.total_value;
    }
    return total;
  }

  getCartTotalInt(cart) {
    let total = 0;
    for (const item of cart.items) {
      total += item.total_value;
    }
    return total;
  }

  getCartsAccount() {
    this.movementsService.balances()
      .pipe(
        first(),
        finalize(
          () => { }
        )
      )
      .subscribe(
        (result) => {
          if (result.data.length) {
            this.accounts = result.data;
          }
        },
        (error) => { }
      );
  }

  getAccountName(id) {
    if (this.accounts) {
      for (const account of this.accounts) {
        if (id === account.account_id) {
          this.currentBalance = account.current_balance;
          return account.account_name;
        }
      }
    }
  }


  setAccountDetails(id, account_id, cart_id) {
    this.movementsService.balancesById(account_id)
      .pipe(
        first(),
        finalize(() => { })
      )
      .subscribe(
        (result) => {
          if (result.data.length) {
            this.cartData[id].current_balance = result.data[0].current_balance;
            this.toPayID = cart_id;
          }
        },
        (error) => { }
      );
  }

  goToCheckout(account_id) {

    /*
     *id -> account_id
     * gets the account and the cart and verify if the operation is duable.
     * if so proceeds to checkout
    */
    this.loadingCheckout = true;
    this.movementsService.balancesById(account_id)
      .pipe(
        first(),
        finalize(() => { })
      )
      .subscribe(
        (account) => {
          if (account.data.length) {
            const balance = account.data[0].current_balance;
            this.cartService.getCartById(account_id)
              .pipe(
                first(),
                finalize(() => { })
              )
              .subscribe(
                (cartList) => {
                  let total = 0;
                  for (const item of cartList.data[0].items) {
                    total += item.unit_value;
                  }
                  if (balance - total >= 0) {
                    this.cartService.checkout(account_id)
                      .pipe(
                        first(),
                        finalize(() => {
                          this.loadingCheckout = false;
                        })
                      )
                      .subscribe(
                        (result) => {
                          this.message.create(
                            'success',
                            this.translateService.instant(
                              'VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.SUCCESS'
                            )
                          );
                          this.cartService.getDataNotification();
                          this.reservationCountService.loadData();
                        },
                        (error) => { }
                      );
                  } else {
                    this.loadingCheckout = false;
                  }
                },
                (error) => { }
              );
          }
        },
        (error) => { }
      );
  }

  clearCart(id_account: number) {
    this.cartService.removeItemsCart(id_account).pipe(
      first()
    ).subscribe(() => {
      this.cartService.getDataNotification();
      this.loadData();
    })
  }

  loadData() {
    this.cartService.cartData$.subscribe(
      (cartData) => {
        this.cartData = cartData;
      }
    );
  }

  quantityChange(id: string, quantity: number, action: string, max_quantity) {
      switch (action) {
        case 'INCREMENT':
          quantity = quantity + 1;
          if(max_quantity < quantity) {
            this.uiService.showMessage(
              MessageType.error,
              this.translateService.instant('ALIMENTATION.STOCK')
            );
          }else {
            this.addToCartQtd(id, quantity);
          }
        
          break;

        case 'DECREMENT':
          quantity = quantity - 1;
          if(quantity == 0) {
            this.removeItemCard(id);
          }else {
            this.addToCartQtd(id, quantity);
          }
          break;

        case 'REMOVE':
          this.removeItemCard(id);
          break;
    }
  }

  addToCartQtd(id: string, quantity: number) {
    this.cartService.changeQtdItem(id, quantity).pipe(first()).subscribe(()=> {
      this.cartService.getDataNotification();
      this.loadData();
    })
  }

  removeItemCard(idItem) {
    this.cartService.removeItemCart(idItem).pipe(first()).subscribe(() => {
      this.cartService.getDataNotification();
      this.loadData();
    });
  }
}
