import { Component, Input, OnInit, Output,EventEmitter } from "@angular/core";
import { PaymentsATMModel } from "@fi-sas/webpage/modules/payments/models/payment-atm.model";
import { finalize, first } from "rxjs/operators";
import { BalanceModel, MethodsPaymentaModel } from "../../models/balance.model";
import { ChargeAccountModel } from "../../models/charge-account.model";
import { MovementsService } from "../../services/movements.service";

@Component({
    selector: 'app-deposit',
    templateUrl: './deposit.component.html',
    styleUrls: ['./deposit.component.less']
})
export class DepositComponent implements OnInit {

    @Input() balance: BalanceModel = new BalanceModel();
    @Input() methods: MethodsPaymentaModel[];
    @Output() close = new EventEmitter();
    method: string = '';
    valueCharge: number = 0;
    totalValue: number = 0;
    loadingPayment: boolean = false;
    modalATM = false;
    dataPaymentRef: PaymentsATMModel;
    mobileSend;

    constructor(private movementsService:MovementsService) {

    }
    
    ngOnInit() {

    }

    changeValue(valueAccount: number) {
        this.totalValue = valueAccount + this.valueCharge;
    }

    validPaymentMethod(tagMethod: string) {
        let count: number = 0;
        if (this.methods) {
            this.methods.forEach(element => {
                if (element.tag === tagMethod) {
                    count++;
                }
            });
        }
        if (count == 0) {
            return false;
        } else {
            return true;
        }
    }

    isATM(account_id: number) {
        this.loadingPayment = true;
        let sendValue: ChargeAccountModel = new ChargeAccountModel();
        sendValue.account_id = account_id;
        sendValue.payment_method_id = 'ebaffe5f-db1d-48b6-944d-fb33f4596bb1';
        sendValue.transaction_value = this.valueCharge;
        this.movementsService.chargeAccount(sendValue).pipe(first(), finalize(()=> this.loadingPayment = false)).subscribe((res) => {
          this.modalATM = true;
          this.dataPaymentRef = res.data[0].payment.payment_method_data;
          this.close.emit(true);
        })
      }
    
      isPaypal() {
      }
    
      isMbway() {
      }
    
      isVisa() {
      }

      closeModel(){
          this.close.emit(true);
      }
}