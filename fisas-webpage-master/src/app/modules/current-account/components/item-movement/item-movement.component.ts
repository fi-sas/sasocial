import { Component, Input, OnInit } from "@angular/core";
import { ItemsModel } from "../../models/items.model";

@Component({
    selector: 'app-item-movements',
    templateUrl: './item-movement.component.html',
    styleUrls: ['./item-movement.component.less']
})
export class ItemMovementsComponent implements OnInit {

    @Input() items: ItemsModel[] = [];
    @Input() transaction_value: number;

    ngOnInit() {

    }

    getPositiveNumber(value: number) {
        if (value < 0) {
            let aux = value.toString();
            let auxNumberPositive = aux.split('-')[1];
            return Number(auxNumberPositive);
        } else {
            return value;
        }
    }

    getValue(name): string {
        return name.length>10 ? (name.substring(0,10) + '...') : name;
    }
}