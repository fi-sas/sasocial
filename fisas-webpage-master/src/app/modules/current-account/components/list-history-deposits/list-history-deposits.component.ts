import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ReportsService } from "@fi-sas/webpage/core/services/reports.service";
import {
  UiService,
  MessageType,
} from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { finalize, first } from "rxjs/operators";
import { BalanceModel } from "../../models/balance.model";
import { ArrayData, MovementsModel } from "../../models/movements.model";
import { MovementsService } from "../../services/movements.service";

@Component({
  selector: "app-list-history-deposits",
  templateUrl: "./list-history-deposits.component.html",
  styleUrls: ["./list-history-deposits.component.less"],
})
export class ListHistoryDepositsComponent implements OnInit {
  // Table
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  listOfData: MovementsModel[] = [];
  loading = true;
  sortValue = null;
  sortKey = null;
  sizeChanger = false;
  pagination = true;
  expandable = true;
  checkbox = true;
  position = "bottom";

  //Filter
  statusFilter = "";
  operationFilter = "";

  withRelated = "account,document,items,payment,payment_method,device";
  minDate = "";
  maxDate = "";
  dateRange = [];
  successModalReport = false;
  statusList: ArrayData = new ArrayData();
  operationList: ArrayData = new ArrayData();

  loadingReport = false;
  accountId;
  accountBalaceById: BalanceModel[] = [];
  movementId: string = "";
  modalDeposit = false;

  constructor(
    public datepipe: DatePipe,
    private uiService: UiService,
    private translateService: TranslateService,
    private movementsService: MovementsService,
    private route: ActivatedRoute,
    private reportsService: ReportsService
  ) {}

  ngOnInit() {
    this.accountId = this.route.snapshot.paramMap.get("idAccount");
    this.getHistoryDeposits(this.accountId);
    this.getAccoutsBalance(this.accountId);
  }

  getHistoryDeposits(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;

    this.movementsService
      .listHistoryDeposits(
        this.pageIndex,
        this.pageSize,
        this.sortKey,
        this.sortValue,
        this.statusFilter,
        this.operationFilter,
        this.withRelated,
        this.accountId,
        this.minDate,
        this.maxDate
      )
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((movements) => {
        this.total = movements.link.total;
        this.listOfData = movements.data;
      });
  }

  getReportHistoryDeposits(): void {
    this.loadingReport = true;

    this.movementsService
      .listReportHistoryDeposits(
        this.pageIndex,
        this.pageSize,
        this.sortKey,
        this.sortValue,
        this.statusFilter,
        this.operationFilter,
        this.withRelated,
        this.accountId,
        this.minDate,
        this.maxDate
      )
      .pipe(
        first(),
        finalize(() => (this.loadingReport = false))
      )
      .subscribe((report) => {
        this.successModalReport = true;
        this.reportsService.loadData(false);
      });
  }

  getAccoutsBalance(accountId: number): void {
    this.movementsService
      .balancesById(accountId, true)
      .pipe(first())
      .subscribe((accountBalace) => {
        this.accountBalaceById = accountBalace.data;
      });
  }

  filterStatus(value: string): void {
    this.statusFilter = value;
    this.getHistoryDeposits(this.accountId);
  }

  filterOperation(value: string): void {
    this.operationFilter = value;
    this.getHistoryDeposits(this.accountId);
  }

  onChangeDate(result: Date): void {
    this.minDate = this.datepipe.transform(result[0], "yyyy-MM-dd");
    this.maxDate = this.datepipe.transform(result[1], "yyyy-MM-dd");
    this.getHistoryDeposits(this.accountId);
  }

  closeModal(event) {
    this.modalDeposit = false;
  }
}
