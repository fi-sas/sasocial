import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ReportsService } from "@fi-sas/webpage/core/services/reports.service";
import { UiService, MessageType } from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { finalize, first } from "rxjs/operators";
import { BalanceModel } from "../../models/balance.model";
import { ArrayData, MovementsModel } from "../../models/movements.model";
import { PayMovementModel } from "../../models/pay-movement.model";
import { MovementsService } from "../../services/movements.service";


@Component({
  selector: 'app-list-movements',
  templateUrl: './list-movements.component.html',
  styleUrls: ['./list-movements.component.less']
})
export class ListMovementsComponent implements OnInit {
  // Table
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  listOfData: MovementsModel[] = [];
  loading = true;
  sortValue = null;
  sortKey = null;
  sizeChanger = false;
  pagination = true;
  expandable = true;
  checkbox = true;
  position = 'bottom';

  //Filter
  statusFilter = '';
  operationFilter = '';
  successModalReport = false;
  withRelated = 'account,document,items,payment_method,device';
  minDate = '';
  maxDate = '';
  dateRange = [];

  statusList: ArrayData = new ArrayData();
  operationList: ArrayData = new ArrayData();

  loadingReport = false;
  accountId;
  accountBalaceById: BalanceModel[] = [];
  movementId: string = '';
  transactionAfterValue: number = 0;
  loadingPayment = false;
  successModal = false;
  modalDeposit = false;

  constructor(
    public datepipe: DatePipe,
    private movementsService: MovementsService,
    private route: ActivatedRoute,
    private uiService:UiService,
    private translateService: TranslateService,
    private reportsService: ReportsService,
  ) {
   }

  ngOnInit() {
    this.accountId = this.route.snapshot.paramMap.get('idAccount');
    this.getMovements(this.accountId);
    this.getAccoutsBalance(this.accountId);
   
  }


  getMovements(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.loading = true;

    this.movementsService
      .listMovements(
        this.pageIndex,
        this.pageSize,
        this.sortKey,
        this.sortValue,
        this.statusFilter,
        this.operationFilter,
        this.withRelated,
        this.accountId,
        this.minDate,
        this.maxDate,

      ).pipe(first(), finalize(() => this.loading = false))
      .subscribe(movements => {
        this.total = movements.link.total;
        this.listOfData = movements.data;
      });
  }

  getReportMovements(): void {
    this.loadingReport = true;

    this.movementsService
      .listReportMovements(
        this.pageIndex,
        this.pageSize,
        this.sortKey,
        this.sortValue,
        this.statusFilter,
        this.operationFilter,
        this.withRelated,
        this.accountId,
        this.minDate,
        this.maxDate
      ).pipe(first(), finalize(() => this.loadingReport = false))
      .subscribe(report => {
        this.successModalReport = true;
        this.reportsService.loadData(false);
      });
  }


  getPositiveNumber(value: number) {
    if (value < 0) {
      let aux = value.toString();
      let auxNumberPositive = aux.split('-')[1];
      return Number(auxNumberPositive);
    } else {
      return value;
    }
  }

  getAccoutsBalance(accountId: number): void {
    this.movementsService.balancesById(
      accountId
    ).pipe(first())
      .subscribe(accountBalace => {
        this.accountBalaceById = accountBalace.data;
      });
  }

  filterStatus(value: string): void {
    this.statusFilter = value;
    this.getMovements(this.accountId);
  }

  filterOperation(value: string): void {
    this.operationFilter = value;
    this.getMovements(this.accountId);
  }

  onChangeDate(result: Date): void {
    this.minDate = this.datepipe.transform(result[0], 'yyyy-MM-dd');
    this.maxDate = this.datepipe.transform(result[1], 'yyyy-MM-dd');
    this.getMovements(this.accountId);
  }

  payValue(amountCharge: number, valueCurrentAccount: number, id: string) {
    this.transactionAfterValue = 0;
    this.transactionAfterValue = valueCurrentAccount + amountCharge;
    this.movementId = id;
  }

  paymentValue(idMovement: string) {
    this.loadingPayment = true;
    let sendValue: PayMovementModel = new PayMovementModel();
    sendValue.payment_method_id = '0bbd1f3f-8720-4075-880c-bf3d789efceb';
    sendValue.id = idMovement;
    this.movementsService.payMovement(sendValue).pipe(first(), finalize(() => this.loadingPayment = false)).subscribe((data) => {
      this.successModal = true;
      this.movementId = '';
      this.getMovements();
    })

  }


  closeModal(event) {
    this.modalDeposit = false;
  }
}
