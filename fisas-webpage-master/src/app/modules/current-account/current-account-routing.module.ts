import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrentAccountComponent } from '@fi-sas/webpage/modules/current-account/current-account.component';

const routes: Routes = [
  {
    path: '',
    component: CurrentAccountComponent,
    children: [
          { path: '', redirectTo: 'accounts', pathMatch: 'full' },
          { 
            path: 'accounts', 
            loadChildren: './pages/accounts/accounts.module#AccountsModule',
            data: { scope: 'current_account:accounts' }
          },
          { 
            path: 'movements/:idAccount', 
            loadChildren: './pages/movements/movements.module#MovementsModule',
            data: { breadcrumb: 'CURRENT_ACCOUNT.MOVEMENTS', scope: 'current_account:movements' } 
          }
        ],
        data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CurrentAccountRoutingModule { }
