import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { Router } from '@angular/router';
import { hasOwnProperty } from 'tslint/lib/utils';
import { BarOptions } from '@fi-sas/webpage/shared/interfaces/bar-options';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
// import { first } from 'rxjs/operators';

@Component({
  selector: 'app-current-account',
  template: '<router-outlet></router-outlet>'
})

export class CurrentAccountComponent implements OnInit {

  // index = 0;

  // barOptions: BarOptions[] = [
  //   {
  //     translate: 'CURRENT_ACCOUNT.ACCOUNT',
  //     content: AccountsComponent,
  //     disable: false
  //   },
  //   {
  //     translate: 'CURRENT_ACCOUNT.MOVEMENTS',
  //     content: MovementsComponent,
  //     disable: true
  //   }
  // ];

  constructor(
    private applicationsService: ApplicationsService,
    private userService: AuthService
  ) {}

  ngOnInit() {
  }

}
