import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentAccountRoutingModule } from '@fi-sas/webpage/modules/current-account/current-account-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MovementsService } from '@fi-sas/webpage/modules/current-account/services/movements.service';
import { AccountsService } from '@fi-sas/webpage/modules/current-account/services/accounts.service';
import { CurrentAccountComponent } from '@fi-sas/webpage/modules/current-account/current-account.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    CurrentAccountComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CurrentAccountRoutingModule
  ],
  providers: [
    AccountsService,
    MovementsService,
    DatePipe
  ]
})
export class CurrentAccountModule { }
