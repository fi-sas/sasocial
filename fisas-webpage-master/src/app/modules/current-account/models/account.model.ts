export class AccountModel {
  id: number;
  name: string;
  institute: string;
  tin: string;
  iban: string;
  allow_partial_payments: boolean;
  plafond_type: string;
  plafond_value: number;
  public_document_types: any;
  erp_company: string;
  erp_user: string;
  updated_at: string;
  created_at: string;
  erp_api: any;
}