export class BalanceModel {
  account_name: string;
  account_id: number;
  current_balance: number;
  in_debt: number;
  total_charges: number;
  available_methods: MethodsPaymentaModel[];
}

export class MethodsPaymentaModel{
  id: string;
  name: string;
  tag: string;
}