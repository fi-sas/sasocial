export enum DeviceType {
  KIOSK = 'KIOSK',
  TV = 'TV',
  MOBILE = 'MOBILE',
  BO = 'BO',
  POS = 'POS',
  GENERIC = 'GENERIC'
}

export enum DeviceOrientation {
  LANDSCAPE = 'LANDSCAPE',
  PORTRAIT = 'PORTRAIT'
}

export class DeviceModel {
  id: number;
  uuid: string;
  name: string;
  type: DeviceType;
  orientation: DeviceOrientation;
  active: boolean;
  secondsToInactivity: number;
  groups: GroupModel[];
  components: ComponentModel[];
}


export class GroupModel {
  id: number;
  name: string;
  active: boolean;
  devices: DeviceModel[];
}


export class ComponentModel {
  id: number;
  name: string;
  active: boolean;
  device_id: number;
}
