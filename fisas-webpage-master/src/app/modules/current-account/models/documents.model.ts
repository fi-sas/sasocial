export class DocumentsModel {
  id: number;
  type: string;
  series: number;
  number: number;
  expiration_date: string;
  date: string;
  value: number;
  file_id: number;
  updated_at: string;
  created_at: string;
  file: any;
}