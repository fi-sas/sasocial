export class ExtraInfo {
  refectory_id: number;
  refectory_consume_at: string;
  refectory_meal_category: string;
  refectory_meal_type: string;
}