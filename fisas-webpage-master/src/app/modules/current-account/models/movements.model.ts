import { PaymentsATMModel } from '../../payments/models/payment-atm.model';
import { AccountModel } from './account.model';
import { DocumentsModel } from './documents.model';
import { ItemsModel } from './items.model';

export class MovementsModel {
    id: number;
    operation: string;
    expiration_date: string;
    account_id: number;
    user_id: number;
    entity: string;
    tin: string;
    email: string;
    address: string;
    postal_code: string;
    city: string;
    country: string;
    transaction_id: number;
    description: string;
    transaction_type: string;
    total_value: number;
    paid_value: number;
    in_debt_value: number;
    advanced_value: number;
    status: string;
    payment_method_id: number;
    payment_method_name: string;
    erp_payment_method: string;
    erp_payment_account_code: string;
    payment: PaymentDataModel;
    affects_current_balance: boolean;
    current_balance: number;
    updated_at: string;
    created_at: string;
    value_to_pay: number;
    account: AccountModel;
    documents: DocumentsModel[];
    items: ItemsModel[];
}

export class PaymentDataModel {
  payment_method_data: PaymentsATMModel;
}

export class ArrayData {

  status = [
    {key: '', label: 'CURRENT_ACCOUNT.FILTER.STATUS'},
    {key: 'CONFIRMED', label: 'CURRENT_ACCOUNT.FILTER.CONFIRMED'},
    {key: 'PENDING', label: 'CURRENT_ACCOUNT.FILTER.PENDING'},
    {key: 'CANCELLED', label: 'CURRENT_ACCOUNT.FILTER.CANCELLED'},
    {key: 'CANCELLED_LACK_PAYMENT', label: 'CURRENT_ACCOUNT.FILTER.CANCELLED_DUE_LACK_PAYMENT'},
    {key: 'PAID', label: 'CURRENT_ACCOUNT.FILTER.PAID'}
  ];

  operations = [
    {key: '', label: 'CURRENT_ACCOUNT.FILTER.OPERATIONS'},
    {key: 'CHARGE', label: 'CURRENT_ACCOUNT.FILTER.CHARGE'},
    {key: 'CANCEL', label: 'CURRENT_ACCOUNT.FILTER.CANCELLED'},
    {key: 'RECEIPT', label: 'CURRENT_ACCOUNT.FILTER.RECEIPT'},
    {key: 'INVOICE', label: 'CURRENT_ACCOUNT.FILTER.INVOICE'},
    {key: 'INVOICE_RECEIPT', label: 'CURRENT_ACCOUNT.FILTER.INVOICE_RECEIPT'},
    {key: 'CHARGE_NOT_IMMEDIATE', label: 'CURRENT_ACCOUNT.FILTER.CHARGE_NOT_IMMEDIATE'},
    {key: 'LEND', label: 'CURRENT_ACCOUNT.FILTER.LEND'},
    {key: 'REFUND', label: 'CURRENT_ACCOUNT.FILTER.REFUND'},
    {key: 'CREDIT_NOTE', label: 'CURRENT_ACCOUNT.FILTER.CREDIT_NOTE'},
  ];

  operations_history = [
    {key: '', label: 'CURRENT_ACCOUNT.FILTER.OPERATIONS'},
    {key: 'CHARGE', label: 'CURRENT_ACCOUNT.FILTER.CHARGE'},
    {key: 'CHARGE_NOT_IMMEDIATE', label: 'CURRENT_ACCOUNT.FILTER.CHARGE_NOT_IMMEDIATE'},
  ];

  status_history = [
    {key: '', label: 'CURRENT_ACCOUNT.FILTER.STATUS'},
    {key: 'CONFIRMED', label: 'CURRENT_ACCOUNT.FILTER.CONFIRMED'},
    {key: 'PENDING', label: 'CURRENT_ACCOUNT.FILTER.PENDING'},
    {key: 'CANCELLED', label: 'CURRENT_ACCOUNT.FILTER.CANCELLED'},
    {key: 'CANCELLED_LACK_PAYMENT', label: 'CURRENT_ACCOUNT.FILTER.CANCELLED_DUE_LACK_PAYMENT'},
  ];


  documents = [
    {key: '', label: 'CURRENT_ACCOUNT.TABLE.TYPEDOC'},
    {key: 'INVOICE_RECEIPT', label: 'CURRENT_ACCOUNT.FILTER.INVOICE_RECEIPT'},
    {key: 'RECEIPT', label: 'CURRENT_ACCOUNT.FILTER.RECEIPT'},
    {key: 'CREDIT_NOTE', label: 'CURRENT_ACCOUNT.FILTER.CREDIT_NOTE'},
    {key: 'DEPOSIT', label: 'CURRENT_ACCOUNT.FILTER.DEPOSIT'},
    {key: 'CANCEL_DEPOSIT', label: 'CURRENT_ACCOUNT.FILTER.CANCEL_DEPOSIT'},
    {key: 'REFUND', label: 'CURRENT_ACCOUNT.FILTER.REFUND'},
    {key: 'LEND', label: 'CURRENT_ACCOUNT.FILTER.LEND'},
    {key: 'CANCEL_LEND', label: 'CURRENT_ACCOUNT.FILTER.CANCEL_LEND'},

  ];


}
