export class PaymentsMethodsModel {
    id: number;
    name: string;
    tag: string;
    description: string;
    path: string;
    is_immediate: boolean;
    active: boolean;
    charge: boolean;
    gateway_data: Date;
    created_at: Date;
    updated_at: Date;
}