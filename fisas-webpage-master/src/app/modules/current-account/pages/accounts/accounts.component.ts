import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { BalanceModel, MethodsPaymentaModel } from '@fi-sas/webpage/modules/current-account/models/balance.model';
import { ChargeAccountModel } from '../../models/charge-account.model';
import { MovementsService } from '../../services/movements.service';
import { PaymentsATMModel } from '@fi-sas/webpage/modules/payments/models/payment-atm.model';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.less']
})
export class AccountsComponent implements OnInit {

  movementsBalance: BalanceModel[] = null;
  loadingDetails = false;
  movementIndex: number = -1;
  method: string = '';
  valueCharge: number = 0;
  totalValue: number = 0;
  mobileNumber: string;
  mobileSend: string;
  successModal: boolean = false;
  modalATM: boolean = false;
  dataPaymentRef: PaymentsATMModel;
  loadingPayment: boolean = false;

  readonly serviceId = SERVICE_IDS.CURRENT_ACCOUNT;

  constructor(
    private router: Router, private authService: AuthService, private movementsService: MovementsService
  ) {
    this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      this.mobileNumber = res.data[0].phone;
    });

  }

  ngOnInit() {
    this.getUserAccount();
  }

  moveToAccount(idAccount: number) {
    this.router.navigateByUrl('/current-account/accounts/movements/' + idAccount);
  }

  changeValue(valueAccount: number) {
    this.totalValue = valueAccount + this.valueCharge;
  }

  isATM(account_id: number) {
    this.loadingPayment = true;
    let sendValue: ChargeAccountModel = new ChargeAccountModel();
    sendValue.account_id = account_id;
    sendValue.payment_method_id = 'ebaffe5f-db1d-48b6-944d-fb33f4596bb1';
    sendValue.transaction_value = this.valueCharge;
    this.movementsService.chargeAccount(sendValue).pipe(first(), finalize(()=> this.loadingPayment = false)).subscribe((res) => {
      this.modalATM = true;
      this.dataPaymentRef = res.data[0].payment.payment_method_data;
    })
  }

  isPaypal() {
  }

  isMbway() {
  }

  isVisa() {
  }

  validPaymentMethod(methods: MethodsPaymentaModel[], tagMethod: string) {
    let count: number = 0;
    methods.forEach(element => {
      if (element.tag === tagMethod) {
        count++;
      }
    });
    if (count == 0) {
      return false;
    } else {
      return true;
    }
  }

  getUserAccount() {
    this.movementsService.balances().pipe(first()).subscribe(results => {
      this.movementsBalance = results.data;
    });
  }

  closeModel() {
    this.modalATM = false;
    this.getUserAccount();
    this.method = '';
    this.movementIndex = -1;
  }
}
