import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovementsComponent } from './movements.component';


const routes: Routes = [
  {
    path: '',
    component: MovementsComponent,
    data: { breadcrumb: null, title: null, scope: 'current_account:movements:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovementsRoutingModule { }
