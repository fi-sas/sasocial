import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.less']
})
export class MovementsComponent implements OnInit {
  isMovements = true;

  constructor(private router: Router) { }

  ngOnInit() {
    
  }

  
  backtoCurrentAccount() {
    this.router.navigateByUrl('current-account/accounts');
  }

}
