import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MovementsComponent } from './movements.component';
import { MovementsRoutingModule } from './movements-routing.module';
import { ListMovementsComponent } from '../../components/list-movements/list-movements.component';
import { ListHistoryDepositsComponent } from '../../components/list-history-deposits/list-history-deposits.component';
import { ItemMovementsComponent } from '../../components/item-movement/item-movement.component';
import { DepositComponent } from '../../components/deposit/deposit.component';

@NgModule({
  declarations: [
      MovementsComponent,
      ListMovementsComponent,
      ListHistoryDepositsComponent,
      ItemMovementsComponent,
      DepositComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    MovementsRoutingModule
  ],
  exports: [
    DepositComponent
  ]
})
export class MovementsModule { }
