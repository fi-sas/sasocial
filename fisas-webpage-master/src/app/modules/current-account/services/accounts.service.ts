import { Params } from '@angular/router';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { AccountModel } from '@fi-sas/webpage/modules/current-account/models/account.model';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  listAccounts(): Observable<Resource<AccountModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');

    return this.resourceService.list<AccountModel>(this.urlService.get('CURRENT_ACCOUNT.MULTI_ACCOUNTS'), { params });
  }
}
