import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { Observable } from "rxjs";
import { DeviceModel } from "../models/device.model";

@Injectable({
  providedIn: 'root'
})

export class DevicesService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private authService: AuthService
  ) {}

  getAllDevices(): Observable<Resource<DeviceModel>> {
    return this.resourceService.list<DeviceModel>(this.urlService.get('CONFIGURATION.DEVICES'));
  }

  getDeviceById(id): Observable<Resource<DeviceModel>> {
    return this.resourceService.read<DeviceModel>(this.urlService.get('CONFIGURATION.DEVICES_ID', id));
  }

}
