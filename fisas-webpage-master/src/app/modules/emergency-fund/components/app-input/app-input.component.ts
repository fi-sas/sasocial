import { Component, Input, OnInit, forwardRef } from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from "@angular/forms";

@Component({
  selector: "app-input",
  templateUrl: "./app-input.component.html",
  styleUrls: ["./app-input.component.less"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AppInputComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: AppInputComponent,
    },
  ],
})
export class AppInputComponent implements ControlValueAccessor, Validator {
  @Input() label = "";
  @Input() placeholder = "";
  @Input() errors = "";
  @Input() id = "";
  @Input() showErro = false;
  @Input() disabledInput = false;
  @Input() readOnlyValue = "";
  @Input() maskInput = "";
  @Input() money = false;

  disabled = false;

  value = "";

  onChange: any = (value: string) => {};
  onTouched: any = () => {};

  constructor() {
    console.log(this.disabledInput);
  }
  validate(control: AbstractControl): ValidationErrors {
    const value = control.value;
    return {};
  }
  registerOnValidatorChange?(fn: () => void): void {}

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
