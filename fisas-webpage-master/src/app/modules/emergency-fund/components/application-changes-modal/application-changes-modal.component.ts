import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';

import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import * as moment from 'moment';

import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import { ApplicationChangesModel } from '../../models/application-changes.model';
import { ApplicationsChangesService } from '../../services/applications_changes.service';

@Component({
  selector: 'app-application-changes-modal',
  templateUrl: './application-changes-modal.component.html',
  styleUrls: ['./application-changes-modal.component.less']
})
export class ApplicationChangesModalComponent implements OnInit {
  isLoading = false;
  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  file = null;
  consult: boolean = false;
  applicationChangeForm = new FormGroup({
    reason: new FormControl('', [Validators.required]),
    file_id: new FormControl(null),
    response: new FormControl('')
  });

  submitted = false;
  changesEdit: ApplicationChangesModel;
  applicationChangesList: ApplicationChangesModel[] = [];
  @Input() data: any = null;
  doc: any;
  application: ApplicationChangesModel = null;


  get f() { return this.applicationChangeForm.controls; }
  constructor(
    private modalRef: NzModalRef,
    private uiService: UiService,
    private translate: TranslateService,
    private fileService: FilesService,
    private applicationsChangesService: ApplicationsChangesService,

  ) {

  }

  ngOnInit() {
    if (this.consult) {
          console.log(this.changesEdit);
          this.applicationChangeForm.get('reason').setValue(this.changesEdit.application_change_exposition);
          if( this.changesEdit.documents.length > 0 ) {
            this.applicationChangeForm.get('file_id').setValue(this.changesEdit.documents ? this.changesEdit.documents[0].file_id : null);
          }
          if( this.changesEdit.application_change_response != null ){
            this.applicationChangeForm.get('response').setValue(this.changesEdit.application_change_response);
          }

      if(this.consult && this.changesEdit.documents.length > 0) {
        this.loadFile();
      }

    }
  }

  close(): void {
    this.modalRef.close(false);
  }

  loadFile() {
    this.fileService.file(this.changesEdit.documents[0].file_id).pipe(first()).subscribe((file)=>{
      this.file = file.data[0];
    })
  }

  onSubmit() {

    this.checkValidityOfControls([
      this.applicationChangeForm.get('reason'),
    ]);
    this.submitted = true;
    if (this.applicationChangeForm.invalid) return;
    this.isLoading = true;
    this.applicationsChangesService
      .add({
        user_id: this.data.userId,
        application_id: this.data.activatedSubmitionId,
        application_change_exposition: this.applicationChangeForm.controls.reason.value,
      })
      .pipe(first())
      .subscribe(() => {
        if( this.applicationChangeForm.controls.attachment.value !== "" ){
          this.applicationsChangesService
          .getListById( Number(this.data.activatedSubmitionId))
          .pipe(first())
          .subscribe((val) => {
              this.applicationChangesList = val.data;
              const maximo = Math.max.apply(null, this.applicationChangesList.map(item => item.id));
              this.applicationsChangesService
              .addDoc({
                application_change_id: maximo,
                document_type_id: 2,
                file_id: this.applicationChangeForm.controls.attachment.value,
                notes: "Inserido pelo utilizador"
              })
              .pipe(first())
              .subscribe(() => {
                this.modalRef.close(false);
                this.uiService.showMessage(
                  MessageType.success,
                  this.translate.instant(
                    "EMERGENCY_FUND.MESSAGES.APPLICATION_CHANGES_SUCCESS"
                  )
                );
              });
        });
      }else{
        this.modalRef.close(false);
        this.uiService.showMessage(
          MessageType.success,
          this.translate.instant(
            "EMERGENCY_FUND.MESSAGES.APPLICATION_CHANGES_SUCCESS"
          )
        );
      }
    });

    return false;
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID')
  }
}
