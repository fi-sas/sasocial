import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";

@Component({
  selector: "consult-reports-interview",
  templateUrl: "./consult-reports-interview.component.html",
  styleUrls: ["./consult-reports-interview.component.less"],
})
export class ConsultReportsInterviewComponent implements OnInit {
  loadingSubmit = false;

  @Input() interview: any = null;
  filterTypes = ["application/pdf", "image/png", "image/jpeg"];

  justificationAbsencesForm: FormGroup = new FormGroup({
    reason: new FormControl("", [Validators.required, trimValidation]),
    attachment_file_id: new FormControl(null),
  });

  doc: any;

  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService
  ) {}

  ngOnInit() {}
}
