import { Component, Input, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { ApplicationsService } from "../../services/applications.service";
import { NzModalRef } from "ng-zorro-antd";
import { ComplationsService } from "../../services/complains.service";
import { ComplainsModel } from "../../models/complains.model";

@Component({
  selector: "form-complain",
  templateUrl: "./form-complain.component.html",
  styleUrls: ["./form-complain.component.less"],
})
export class FormComplainComponent implements OnInit {
  aplicationForm: FormGroup;
  loadingSubmit = false;
  complainsList: ComplainsModel[] = [];
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  valid: boolean = false;

  @Input() data: any = null;
  doc: any;

  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private complainService: ComplationsService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {
    this.aplicationForm = this.fb.group({
      reason: new FormControl("", [Validators.required, trimValidation]),
      attachment: new FormControl("", [Validators.required]),
    });
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== "VALID");
  }

  submitForm() {
    this.valid = this.checkValidityOfControls([
      this.aplicationForm.get("reason"),
    ]);
    if(this.valid){

    
    this.complainService
      .add({
        id: this.data.activatedSubmitionId,
        user_id: this.data.userId,
        application_id: this.data.activatedSubmitionId,
        complaint_reason: this.aplicationForm.controls.reason.value,
      })
      .pipe(first())
      .subscribe(() => {
        if( this.aplicationForm.controls.attachment.value !== "" ){
          this.complainService
          .getById( Number(this.data.activatedSubmitionId))
          .pipe(first())
          .subscribe((val) => {
              this.complainsList = val.data;
              const maximo = Math.max.apply(null, this.complainsList.map(item => item.id));
              this.complainService
              .addDoc({
                complain_id: maximo,
                document_type_id: 2,
                file_id: this.aplicationForm.controls.attachment.value,
                notes: "Inserido pelo utilizador"
              })
              .pipe(first())
              .subscribe((val) => {
                this.modalRef.close(false);
                this.uiService.showMessage(
                  MessageType.success,
                  this.translate.instant(
                    "EMERGENCY_FUND.MESSAGES.COMPLAIN_SUCCESS"
                  )
                );
              });

          });
        }else{
          this.modalRef.close(false);
            this.uiService.showMessage(
              MessageType.success,
              this.translate.instant(
              "EMERGENCY_FUND.MESSAGES.COMPLAIN_SUCCESS"
            )
          );
        }
      });

    }
  }

  close(): void {
    this.modalRef.close(false);
  }
}
