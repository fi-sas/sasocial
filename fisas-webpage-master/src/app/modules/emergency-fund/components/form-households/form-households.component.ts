import { Component, Input, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { ApplicationsService } from "../../services/applications.service";
import { DocumentsModel } from "../../models/documents";
import { NzModalRef } from "ng-zorro-antd";
@Component({
  selector: "form-households",
  templateUrl: "./form-households.component.html",
  styleUrls: ["./form-households.component.less"],
})
export class FormHouseholdsComponent implements OnInit {
  aplicationForm: FormGroup;
  loadingSubmit = false;

  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];

  doc: any;
  @Input() addHouseholds: any = null;

  isSubmited = false;
  documentsTypes: DocumentsModel[];
  get f() {
    return this.aplicationForm.controls;
  }
  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private appService: ApplicationsService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit() {
    this.aplicationForm = this.fb.group({
      person_name: ["", [Validators.required, trimValidation]],
      kinship: ["", [Validators.required, trimValidation]],
      birth_date: ["", [Validators.required]],
      marital_status: ["", [Validators.required, trimValidation]],
      dependent_labor_value: ["", [Validators.required, trimValidation]],
      self_employment_value: ["", [Validators.required, trimValidation]],
      retirement_pension_value: ["", [Validators.required, trimValidation]],
    });
  }
  submitForm() {
    this.addHouseholds({
      name: this.f.person_name.value,
      kinship: this.f.kinship.value,
      birth_date: this.f.birth_date.value,
      marital_status: this.f.marital_status.value,
      dependent_labor_value: this.f.dependent_labor_value.value,
      self_employment_value: this.f.self_employment_value.value,
      retirement_pension_value: this.f.retirement_pension_value.value,
    });
    this.modalRef.close(false);
  }
}
