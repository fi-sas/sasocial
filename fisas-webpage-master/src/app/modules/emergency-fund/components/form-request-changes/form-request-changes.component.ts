import { Component, Input, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { ApplicationsService } from "../../services/applications.service";
import { NzModalRef } from "ng-zorro-antd";
import { Router } from "@angular/router";
import { ApplicationsChangesService } from "../../services/applications_changes.service";
import { ApplicationChangesModel } from "../../models/application-changes.model";

@Component({
  selector: "form-request-changes",
  templateUrl: "./form-request-changes.component.html",
  styleUrls: ["./form-request-changes.component.less"],
})
export class FormRequestChangesComponent implements OnInit {
  aplicationForm: FormGroup;
  application_change_reportGroup: FormGroup;

  loadingSubmit = false;
  submitted: boolean = false;
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  objeto: any;
  valid: boolean = false;
  applicationChangesList: ApplicationChangesModel[] = [];
  file_ids = [];

  @Input() data: any = null;
  doc: any;

  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private applicationsChangesService: ApplicationsChangesService,
    private modalRef: NzModalRef,
    private router: Router
  ) {}

  ngOnInit() {
    this.aplicationForm = this.fb.group({
      user_id: new FormControl(null),
      application_id: new FormControl(null),
      application_change_exposition: new FormControl(null, [Validators.required, trimValidation]),
      application_change_reports: new FormControl([]),
    });
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== "VALID");
  }

  
  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }

  submitForm() {
    this.valid = this.checkValidityOfControls([
      this.aplicationForm.get("application_change_exposition"),
    ]);
    if(this.valid){
    const application_change_reports = [];
    for(const file of this.file_ids){
      const obj: any = {};
      obj.file_id = file;
      obj.notes = "Inserido pelo utilizador"
      obj.document_type_id = 2;
      application_change_reports.push(obj);
    }
    
    this.aplicationForm.patchValue({
      user_id: this.data.userId,
      application_id: this.data.activatedSubmitionId,
      application_change_reports: application_change_reports
    });
        this.applicationsChangesService.add(this.aplicationForm.value).pipe().subscribe(() => {
          this.modalRef.close(false);
          this.uiService.showMessage(
            MessageType.success,
              this.translate.instant("EMERGENCY_FUND.MESSAGES.APPLICATION_CHANGES_SUCCESS")
            );
        });
      }
  }

}
