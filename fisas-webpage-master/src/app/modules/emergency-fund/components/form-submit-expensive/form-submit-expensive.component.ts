import { Component, Input, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { ApplicationsService } from "../../services/applications.service";
import { DocumentsModel } from "../../models/documents";
import { NzModalRef } from "ng-zorro-antd";
import { ExpensesDocModel, ExpensesModel } from "../../models/expenses";
import { Subscription } from "rxjs";
import { nationalities } from "@fi-sas/webpage/shared/data/nationalities";
import { FiConfigurator } from "@fi-sas/configurator";
import { intendedSupports } from "../../models/intended-supports.model";
import { SupportsModel } from "../../models/supports";
import { ExpensesService } from "../../services/expenses.service";
@Component({
  selector: "form-submit-expensive",
  templateUrl: "./form-submit-expensive.component.html",
  styleUrls: ["./form-submit-expensive.component.less"],
})
export class FormSubmitExpensiveComponent implements OnInit {
  aplicationForm: FormGroup;
  loadingSubmit = false;
  currentApp: any;
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  expensesForm = new ExpensesModel();
  expensesDoc = new ExpensesDocModel();
  onLangChange: Subscription = undefined;
  currentLang;
  nationalities = nationalities;
  loading: boolean = true;

  @Input() data: any = null;
  doc: any;

  intendedSupports: intendedSupports[];
  Supports: SupportsModel[];
  documentsTypes: DocumentsModel[];

  get f() {
    return this.aplicationForm.controls;
  }
  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService,
    private fb: FormBuilder,
    private appService: ApplicationsService,
    private expensesService: ExpensesService,
    private modalRef: NzModalRef,
    private config: FiConfigurator
  ) {
    let languagesIDS;
    if (config !== undefined) {
      languagesIDS = Object.values(config.getOptionTree('LANGUAGES'))[0];
      this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;
    } else {
      languagesIDS = undefined;
    }

    this.onLangChange = translate.onLangChange.subscribe(() => {
      if (languagesIDS !== undefined) {
        this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;

      } else {
        this.currentLang = undefined;
      }
    });
  }

  ngOnInit() {
    this.aplicationForm = this.fb.group({
      expense_description: new FormControl("", [
        Validators.required,
        trimValidation,
      ]),
      document_number: new FormControl("", [
        Validators.required,
        trimValidation,
      ]),
      document_date: new FormControl("", [Validators.required]),
      document_type: new FormControl("", [Validators.required]),
      intendedSupports: new FormControl("", [Validators.required]),
      supplier_name: new FormControl("", [Validators.required, trimValidation]),
      supplier_nif: new FormControl("", [Validators.required, trimValidation]),
      document_value: new FormControl("", [
        Validators.required,
        trimValidation,
      ]),
      attachment: new FormControl("", [Validators.required]),
    });

    this.appService
      .getDocumentsTypes(3)
      .pipe(first())
      .subscribe((documents) => {
        this.documentsTypes = documents.data
          .filter((doc) => doc.active)
          .map((doc) => {
            return { ...doc, checked: false };
          });
      });

      this.appService
      .applicationVerifyActive()
      .pipe(
        first()
      )
      .subscribe((val) => {
        this.currentApp = val.data.filter((value) => value.active = true)[0].id;
        if( this.currentApp ){
          this.appService
          .getIntendedSupports(this.currentApp)
          .pipe(first())
          .subscribe((is) => {
            console.log(is.data);
            this.intendedSupports = is.data
              .filter((is) => is.active_support)
              .map((is) => {
                return { ...is, checked: false };
              });
          });
          this.appService
          .getSupports()
          .pipe(first(), finalize(() => (this.loading = false)))
          .subscribe((s) => {
            this.Supports = s.data
              .filter((s) => s.active)
              .map((s) => {
                return { ...s, checked: false };
              });
          });
        }
      });
      
  }

  ngOnDestroy() {
    if( this.onLangChange !== undefined ){
      this.onLangChange.unsubscribe();

    }
  }

  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentsTypes.find(
      (x) => x.id == document_type_id
    );
    return document_type
      ? document_type.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
  }

  getSupportDescription(support_id) {
    const support = this.Supports.find(
      (x) => x.id == support_id
    );
    return support
      ? support.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
  }


  submitForm() {
    this.expensesForm.document_number = this.aplicationForm.controls.document_number.value;
    this.expensesForm.document_date = this.aplicationForm.controls.document_date.value;
    this.expensesForm.supplier_name = this.aplicationForm.controls.supplier_name.value;
    this.expensesForm.supplier_nif = this.aplicationForm.controls.supplier_nif.value;
    this.expensesForm.document_value = this.aplicationForm.controls.document_value.value;
    this.expensesForm.expense_description = this.aplicationForm.controls.expense_description.value;
    this.expensesForm.application_id = Number(this.data.activatedSubmitionId.id);
    this.expensesForm.intended_support_id = this.aplicationForm.controls.intendedSupports.value;
    this.expensesForm.amount_to_pay = this.aplicationForm.controls.document_value.value;
    this.expensesForm.share_percentage = 10;

    this.expensesService
      .add(this.expensesForm)
      .pipe(first())
      .subscribe((content) => {
        this.expensesDoc.id = content.data[0].id;
        this.expensesDoc.file_id = this.aplicationForm.controls.attachment.value;
        this.expensesDoc.document_type_id = this.aplicationForm.controls.document_type.value;
        this.expensesDoc.notes = "Inserido pelo utilizador.";

        this.expensesService
        .addDoc(this.expensesDoc)
        .pipe(first())
        .subscribe(() => {
          this.modalRef.close(false);
          this.uiService.showMessage(
            MessageType.success,
            this.translate.instant(
              "EMERGENCY_FUND.MESSAGES.COMPLAIN_SUCCESS"
            )
          );
        });
        
      });
  }

  close(): void {
    this.modalRef.close(false);
  }


}
