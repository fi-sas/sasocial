import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";

import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { ConfigStatus } from "../../models/application.model";

@Component({
  selector: "historic",
  templateUrl: "./historic.component.html",
  styleUrls: ["./historic.component.less"],
})
export class HistoricComponent implements OnInit {
  loadingSubmit = false;

  @Input() data: any = null;
  filterTypes = ["application/pdf", "image/png", "image/jpeg"];

  configStatus = ConfigStatus;
  doc: any;

  listHistoric: any[];

  constructor(
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.listHistoric = this.mixHistoric();
  }

  isPair(val) {
    return val % 2 == 0;
  }

  mixHistoric() {
    let auxi = this.data[0].history.reverse().map((value) => {
      return {
        created_at: value.created_at,
        status: value.status,
        type: 0,
        data: value,
      };
    });
    if( this.data[0].complains !== undefined){
      this.data[0].complains.map((value) => {
        auxi.push({
          created_at: value.created_at,
          status: value.status,
          type: 1,
          data: value,
        });
      });
    }


    return [].concat.apply([], auxi).sort(this.compare).reverse();
  }

  compare(a, b) {
    return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
  }
}
