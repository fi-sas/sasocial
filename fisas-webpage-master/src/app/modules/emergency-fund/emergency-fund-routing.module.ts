import { EmergencyFundComponent } from "./emergency-fund.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";

const routes: Routes = [
  {
    path: "",
    component: EmergencyFundComponent,
    children: [
      { path: "", redirectTo: "home", pathMatch: "full" },
      {
        path: "home",
        loadChildren: "./pages/home/home.module#HomeModule",
        data: { scope: "emergency-fund" },
      },
      {
        path: "form-apply-emergency-fund",
        loadChildren: "./pages/form-apply-emergency-fund/form-apply-emergency-fund.module#FormApplyEmergencyFundModule",
        data: { scope: "emergency-fund" },
      },
      {
        path: "list-applications",
        loadChildren: "./pages/list-applications/list-applications.module#ListApplicationsEmergencyFundModule",
        data: { scope: "emergency-fund" },
      },
      {
        path: "list-applications/:id",
        loadChildren: "./pages/review-application/review-application.module#ReviewApplicationModule",
        data: { scope: 'emergency-fund:applications' }
      },
      {
        path: "list-applications-changes",
        loadChildren: "./pages/list-applications-changes/list-applications-changes.module#ListApplicationsChangesEmergencyFundModule",
        data: { scope: "emergency-fund" },
      },
      {
        path: "list-applications-changes/:id",
        loadChildren: "./pages/review-application-changes/review-application-changes.module#ReviewApplicationChangesModule",
        data: { scope: 'emergency-fund:applications' }
      },
      {
        path: "list-expenses",
        loadChildren: "./pages/list-expenses/list-expenses.module#ListExpensesEmergencyFundModule",
        data: { scope: "emergency-fund" },
      }
    ],
    data: { breadcrumb: null, title: null },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmergencyFundRoutingModule {}
