import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { FilterStatusConfigPipe } from "./pipes/filter-status-config.pipe";
import { ConsultReportsInterviewComponent } from "./components/consult-reports-interview/consult-reports-interview.component";
import { FormComplainComponent } from "./components/form-complain/form-complain.component";
import { HistoricComponent } from "./components/historic/historic.component";
import { FormRequestChangesComponent } from "./components/form-request-changes/form-request-changes.component";
import { FormSubmitExpensiveComponent } from "./components/form-submit-expensive/form-submit-expensive.component";
import { FormHouseholdsComponent } from "./components/form-households/form-households.component";
import { AppInputComponent } from "./components/app-input/app-input.component";
import { TruncatePipe } from "./pipes/truncate.pipe";
import { ApplicationChangesModalComponent } from "./components/application-changes-modal/application-changes-modal.component";

@NgModule({
  declarations: [
    FilterStatusConfigPipe,
    TruncatePipe,
    ConsultReportsInterviewComponent,
    FormComplainComponent,
    HistoricComponent,
    FormRequestChangesComponent,
    FormSubmitExpensiveComponent,
    ApplicationChangesModalComponent,
    FormHouseholdsComponent,
    AppInputComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: [
    FilterStatusConfigPipe,
    TruncatePipe,
    ConsultReportsInterviewComponent,
    FormComplainComponent,
    HistoricComponent,
    FormRequestChangesComponent,
    FormSubmitExpensiveComponent,
    ApplicationChangesModalComponent,
    FormHouseholdsComponent,
    AppInputComponent,
  ],
  entryComponents: [
    ConsultReportsInterviewComponent,
    FormComplainComponent,
    HistoricComponent,
    FormRequestChangesComponent,
    FormSubmitExpensiveComponent,
    ApplicationChangesModalComponent,
    FormHouseholdsComponent,
  ],
})
export class EmergencyFundSharedModule {}
