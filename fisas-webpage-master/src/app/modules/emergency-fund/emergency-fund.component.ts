import { UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emergency-fund',
  templateUrl: './emergency-fund.component.html',
  styleUrls: ['./emergency-fund.component.less']
})
export class EmergencyFundComponent implements OnInit {


  barOptions:any[] = [];

  constructor(
              private userService: AuthService) { }

  ngOnInit() {
    this.barOptions = [
      {
        translate: 'EMERGENCY_FUND.MENU.DASHBOARD',
        disable: true,
        routerLink: '/emergency-fund/home',
        scope: this.userService.hasPermission('emergency_fund')
      },
      {
        translate: 'EMERGENCY_FUND.MENU.APPLICATIONS',
        disable: false,
        routerLink: '/emergency-fund/list-applications',
        scope: this.userService.hasPermission('emergency_fund:applications:read')
      },
      {
        translate: 'EMERGENCY_FUND.MENU.CHANGES',
        disable: false,
        routerLink: '/emergency-fund/list-applications-changes',
        scope: this.userService.hasPermission('emergency_fund:applications:read')
      },
      {
        translate: 'EMERGENCY_FUND.MENU.EXPENSES',
        disable: false,
        routerLink: '/emergency-fund/list-expenses',
        scope: this.userService.hasPermission('emergency_fund:applications:read')
      }
    ]
  }
  
}