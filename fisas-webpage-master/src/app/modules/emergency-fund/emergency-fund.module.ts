import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EmergencyFundRoutingModule } from "./emergency-fund-routing.module";
import { EmergencyFundComponent } from "./emergency-fund.component";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";

@NgModule({
  declarations: [EmergencyFundComponent],
  imports: [
    CommonModule,
    EmergencyFundRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class EmergencyFundModule {}
