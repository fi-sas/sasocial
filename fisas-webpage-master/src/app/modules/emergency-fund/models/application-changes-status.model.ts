import { ApplicationModel } from '@fi-sas/webpage/modules/accommodation/models/application.model';

export class ApplicationChangesStatusModel {
  id: number;
  event: string;
  notes: string;
  application?: ApplicationModel;
}
