import { DateRange } from "fullcalendar";
import { DocumentsModel } from "./documents";

export class ApplicationChangesModel {
  application_change_exposition: string;
  application_change_response: string;
  application_change_response_date: Date;
  application_id: string;
  documents: [{
    file_id: number;
  }];
  id: string;
  last_status: string;
  status: string;
  user_id: string;
}
