import { HistoryModel } from "@fi-sas/webpage/modules/accommodation/models/history.model";
import { RoomModel } from "@fi-sas/webpage/modules/accommodation/models/room.model";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { RegimeModel } from "./regime.model";
import { TranslateService } from "@ngx-translate/core";
import { SupportsModel } from "./supports";

export enum ApplicationStatus {
  DRAFT = "DRAFTED",
  SUBMITTED = "SUBMITTED",
  ORDERED = "ORDERED",
  ANALYSED = "ANALYZED",
  INTERVIEW = "INTERVIEWED",
  CANCELLED = "CANCELLED",
  PENDING = "PENDING",
  NOTACCEPTED = "NOTACCEPTED",
  UNASSIGNED = "UNASSIGNED",
  QUEUED = "QUEUED",
  ASSIGNED = "ASSIGNED",
  CONFIRMED = "CONFIRMED",
  REJECTED = "REJECTED",
  OPPOSITION = "OPPOSITION",
  CONTRACTED = "CONTRACTED",
  REOPENED = "REOPENED",
  CLOSED = "CLOSED",
  APPROVED = "APPROVED",
  PROCESSED = "PROCESSED",
}

export enum AccommodationRegime {
  PC = "PC",
  MP = "MP",
  ALO = "ALO",
}

export enum AccomodationPaymentMethod {
  CC = "CC",
  MB = "MB",
  DD = "DD",
  "NUM" = "NUM",
}
export class ApplicationModel {
  id: number;
  student_name: string;
  student_address: string;
  student_zip_code: string;
  student_location: string;
  student_nationality: string;
  student_birthdate: Date;
  student_type_identif_doc: string;
  student_identif_number: string;
  student_nif: string;
  student_niss: string;
  student_iban: string;
  student_number: string;
  school_id: string;
  course_id: string;
  course_degree_id: string;
  academic_year: string;
  course_year: string;
  student_email: string;
  student_mobile_phone: string;
  student_phone: string;
  student_address_classes: string;
  student_add_class_zip_code: string;
  student_add_class_location: string;
  student_schedule: string;
  student_regime: string;
  student_regime_description: string;
  has_rustic_property: string;
  rustic_property_value: string;
  has_urban_property: string;
  urban_property_value: string;
  has_furniture_patrimony: string;
  total_patrimony_value: string;
  monthly_earnings_value: string;
  has_own_housing: string;
  has_bank_loan: string;
  bank_loan_installment: string;
  has_rented_house: string;
  income_amount: string;
  has_ceded_housing: string;
  housing_provided_whom: string;
  has_serious_illnesses: string;
  serious_illnesses_person: string;
  has_people_deficiency: string;
  people_deficiency_person: string;
  has_health_expenses: string;
  average_expense_value: string;
  nanny_value: string;
  creche_value: string;
  kindergarten_value: string;
  school_value: string;
  atl_value: string;
  household_water_value: string;
  household_electricity_value: string;
  household_gas_value: string;
  accommodation_value: string;
  student_water_value: string;
  student_electricity_value: string;
  student_gas_value: string;
  has_canteen_food: string;
  food_value: string;
  school_supplies_value: string;
  has_public_transport: string;
  has_social_pass: string;
  social_pass_value: string;
  has_transport: string;
  transport_value: string;
  has_serious_illnesses_student: string;
  has_deficiency: string;
  has_health_expenses_student: string;
  expense_value_student: string;
  total_monthly_expenses: string;
  other_informations: string;
  reason_request: string;
  status: string;
  last_status: string;
  user_id: string;
  notification_guid: string;
  organic_units_ids: string;
  intended_supports: SupportsModel[];
  other_expenses: [{
    id: number;
    application_id: number;
    expense_whom: string;
    expense_type: string;
    expense_description: string;
    expense_value: number;
  }];
  documents: [];
}

export class HouseHoldElementsModel {
  kinship: string;
  profession: string;
}

export const FormSteps = [
  "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_TWO.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_SIX.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_SEVEN.P_TITLE",
  "EMERGENCY_FUND.APPLICATIONS.STEP_EIGHT.P_TITLE",
];

export const FormItens = [
  /* {
    labelName: "",
    inputName: "",
    step: 0,
    type: 0,
    valueDescription: (fn) => fn(),
    //hide: (fn) => fn(),
  },*/
  {
    step: 0,
    inputName: "student_name",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NAME.TITLE",
    typeInput: 0,
    size: 24
  },
  {
    step: 0,
    inputName: "student_address",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADDRESS.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_zip_code",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ZIP_CODE.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_location",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_LOCATION.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    typeInput: -1,
    size: 24
  },
  {
    step: 0,
    inputName: "student_email",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_EMAIL.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_mobile_phone",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_MOBILE_PHONE.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    typeInput: -1,
    size: 24
  },
  {
    step: 0,
    inputName: "student_birthdate",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_BIRTHDATE.TITLE",
    typeInput: 1,
    size: 8
  },
  {
    step: 0,
    inputName: "student_nationality",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NATIONALITY.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    typeInput: -1,
    size: 24
  },
  {
    step: 0,
    inputName: "student_type_identif_doc",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_TYPE_IDENTIF_DOC.TITLE",
    typeInput: 5,
    size: 8
  },
  {
    step: 0,
    inputName: "student_identif_number",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_IDENTIF_NUMBER.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_nif",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NIF.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_niss",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NISS.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_iban",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_IBAN.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    typeInput: -1,
    size: 24
  },
  {
    step: 0,
    inputName: "school",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.SCHOOL_ID.TITLE",
    typeInput: 6,
    size: 8
  },
  {
    step: 0,
    inputName: "course",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_ID.TITLE",
    typeInput: 6,
    size: 8
  },
  {
    step: 0,
    inputName: "course_degree",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_DEGREE.TITLE",
    typeInput: 6,
    size: 8
  },
  {
    step: 0,
    inputName: "student_number",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NUMBER.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "academic_year",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.ACADEMIC_YEAR.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "course_year",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_YEAR.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_schedule",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.TITLE",
    typeInput: 21,
    size: 8
  },
  {
    step: 0,
    inputName: "student_regime",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.TITLE",
    typeInput: 22,
    size: 8
  },
  {
    step: 0,
    typeInput: -1,
    size: 24
  },
  //{step:0,inputName:/*"student_phone",labelName:,typeInput:0},*/
  {
    step: 0,
    inputName: "student_address_classes",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADDRESS_CLASSES.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_add_class_zip_code",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADD_CLASS_ZIP_CODE.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 0,
    inputName: "student_add_class_location",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADD_CLASS_LOCATION.TITLE",
    typeInput: 0,
    size: 8
  },
  {
    step: 1,
    inputName: "households",
    labelName: null,
    typeInput: 99,
    size: 24
  },
  {
    step: 1,
    inputName: "other_incomes",
    labelName: null,
    typeInput: 98,
    size: 24
  },
  {
    step: 2,
    inputName: "has_rustic_property",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.RUSTIC_PROPERTY.TITLE",
    typeInput: 3,
    size: 8,
    expectedValue: true
  },
  {
    step: 2,
    inputName: "rustic_property_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
    typeInput: 7,
    size: 16,
    field: "has_rustic_property",
    expectedValue: true,
    money: true
  },
  {
    step: 2,
    typeInput: -3,
    size: 24
  },
  {
    step: 2,
    inputName: "has_urban_property",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.URBAN_PROPERTY.TITLE",
    typeInput: 3,
    size: 8,
    expectedValue: true
  },
  {
    step: 2,
    inputName: "urban_property_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
    typeInput: 7,
    size: 16,
    field: "has_urban_property",
    expectedValue: true,
    money: true
  },
  {
    step: 2,
    typeInput: -3,
    size: 24
  },
  {
    step: 2,
    inputName: "has_furniture_patrimony",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.FORNITURE_PROPERTY.TITLE",
    typeInput: 3,
    size: 8,
    expectedValue: true
  },
  {
    step: 2,
    inputName: "total_patrimony_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
    typeInput: 7,
    size: 16,
    field: "has_furniture_patrimony",
    expectedValue: true,
    money: true
  },
  {
    step: 2,
    inputName: "monthly_earnings_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.MONTHLY_EARNINGS_VALUE.TITLE",
    typeInput: 0,
    size: 24,
    money: true
    //hide: this.f["has_furniture_patrimony"].value === "true" ? false : true,
  },
  {
    step: 3,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HOUSE.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 3,
    inputName: "has_own_housing",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HOUSE.TITLE",
    labelName2:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.OWN_HOUSE.TITLE",
    typeInput: 8,
    size: 6,
    expectedValue: true 
  },
  {
    step: 3,
    inputName: "has_bank_loan",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.BANK_LOAN.TITLE",
    typeInput: 9,
    size: 6,
    field: "has_own_housing",
    expectedValue: true
  },
  {
    step: 3,
    inputName: "bank_loan_installment",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.INCOME_AMOUNT.TITLE",
    typeInput: 0,
    size: 6,
    field: "has_bank_loan",
    expectedValue: true
    //hide: this.f["has_furniture_patrimony"].value === "true" ? false : true,
  },
  {
    step: 3,
    typeInput: -3,
    size: 24
  },
  {
    step: 3,
    inputName: "has_rented_house",
    labelName:
    "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HOUSE.TITLE",
    labelName2:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.RENTED_HOUSE.TITLE",
    typeInput: 8,
    size: 6,
    expectedValue: true
  },
  {
    step: 3,
    inputName: "income_amount",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.INCOME_AMOUNT.TITLE",
    typeInput: 0,
    size: 6,
    field: "has_rented_house",
    expectedValue: true
  },
  {
    step: 3,
    inputName: "housing_provided_whom",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.PROVIDED_WHOM.TITLE",
    typeInput: 0,
    size: 6,
    field: "has_rented_house",
    expectedValue: true
  },
  {
    step: 3,
    typeInput: -3,
    size: 24
  },
  {
    step: 3,
    inputName: "has_ceded_housing",
    labelName:
    "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HOUSE.TITLE",
    labelName2:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.CEDED_HOUSE.TITLE",
    typeInput: 8,
    size: 6,
    expectedValue: true
  },
  {
    step: 3,
    inputName: "housing_provided_whom",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.PROVIDED_WHOM.TITLE",
    //hide: this.f["has_rented_house"].value === "true" ? false : true,
    typeInput: 0,
    size: 6,
    field: "has_ceded_housing",
    expectedValue: true
  },
  {
    step: 3,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HEALTH.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 3,
    inputName: "has_serious_illnesses",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.SERIOUS_ILLNESSES.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 3,
    inputName: "serious_illnesses_person",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.WHOSE.TITLE",
    //hide: this.f["has_serious_illnesses"].value === "true" ? false : true,
    typeInput: 7,
    size: 6,
    field: "has_serious_illnesses",
    expectedValue: true
  },
  {
    step: 3,
    typeInput: -3,
    size: 24
  },
  {
    step: 3,
    inputName: "has_people_deficiency",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.PEOPLE_DEFICIENCY.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 3,
    inputName: "people_deficiency_person",
    labelName: "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.WHOM.TITLE",
    typeInput: 7,
    size: 6,
    field: "has_people_deficiency",
    expectedValue: true
    //hide: this.f["has_people_deficiency"].value === "true" ? false : true,
  },
  {
    step: 3,
    inputName: "has_health_expenses",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HEALTH_EXPENSES.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 3,
    inputName: "average_expense_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.AVERAGE_EXPENSE.TITLE",
    typeInput: 7,
    size: 6,
    field: "has_health_expenses",
    expectedValue: true,
    money: true
    //hide: this.f["has_health_expenses"].value === "true" ? false : true,
  },
  {
    step: 3,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EDUCATION.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 3,
    inputName: "nanny_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.NANNY.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "creche_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.CRECHE.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "kindergarten_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.KINDERGARTEN.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "school_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.SCHOOL.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "atl_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ATL.TITLE",
    typeInput: 0,
    size: 4,
    money: true
  },
  {
    step: 3,
    inputName: "other_expenses",
    inputName2: "EDUCATION",
    typeInput: 10,
    size: 24,
    money: true
  },
  {
    step: 3,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.FIXED_EXPENSES.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 3,
    inputName: "household_water_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.WATER.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "household_electricity_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ELECTRICITY.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "household_gas_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.GAS.TITLE",
    typeInput: 0,
    size: 5,
    money: true
  },
  {
    step: 3,
    inputName: "other_expenses",
    inputName2: "FIXED",
    typeInput: 11,
    size: 24,
    money: true
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.ACCOMMODATION_VALUE.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "accommodation_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.ACCOMMODATION_VALUE.TITLE",
    typeInput: 0,
    size: 6,
    money: true
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.EXPENSE.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "student_water_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_WATER.TITLE",
    typeInput: 0,
    size: 6,
    money: true
  },
  {
    step: 4,
    inputName: "student_electricity_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_ELECTRICITY.TITLE",
    typeInput: 0,
    size: 6,
    money: true
  },
  {
    step: 4,
    inputName: "student_gas_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_ELECTRICITY.TITLE",
    typeInput: 0,
    size: 6,
    money: true
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.FOOD.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "has_canteen_food",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.CANTEEN_FOOD.TITLE",
    typeInput: 3,
    size: 6
  },
  {
    step: 4,
    inputName: "food_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
    typeInput: 0,
    size: 6,
    field: "has_canteen_food",
    expectedValue: true,
    money: true
    //hide: this.f["has_canteen_food"].value === "true" ? false : true,
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.SCHOOL_SUPPLIES.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "school_supplies_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
    typeInput: 0,
    size: 6,
    money: true
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.TRANSPORT.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "has_public_transport",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.PUBLIC_TRANSPORT.TITLE",
    typeInput: 3,
    size: 24
  },
  {
    step: 4,
    inputName: "has_social_pass",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.SOCIAL_PASS.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 4,
    inputName: "social_pass_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
    typeInput: 7,
    size: 6,
    field: "has_social_pass",
    expectedValue: true,
    money: true
    //hide: this.f["has_social_pass"].value === "true" ? false : true,
  },
  {
    step: 4,
    inputName: "has_transport",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OWN_TRANSPORT.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 4,
    inputName: "transport_value",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
    typeInput: 7,
    size: 6,
    field: "has_transport",
    expectedValue: true,
    money: true
    //hide: this.f["has_transport"].value === "true" ? false : true,
  },
  {
    step: 4,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.HEALTH.TITLE",
    typeInput: -2,
    size: 24
  },
  {
    step: 4,
    inputName: "has_serious_illnesses_student",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.SERIOUS_ILLNESSES_STUDENT.TITLE",
    typeInput: 3,
    size: 24
  },
  {
    step: 4,
    inputName: "has_deficiency",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.DEFICIENCY.TITLE",
    typeInput: 3,
    size: 24
  },
  {
    step: 4,
    inputName: "has_health_expenses_student",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.HEALTH_EXPENSES_STUDENT.TITLE",
    typeInput: 3,
    size: 6,
    expectedValue: true
  },
  {
    step: 4,
    inputName: "expense_value_student",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
    typeInput: 7,
    size: 6,
    field: "expense_value_student",
    expectedValue: true,
    money: true
    //hide: this.f["has_health_expenses_student"].value === "true" ? false : true,
  },
  {
    step: 4,
    inputName: "other_expenses",
    inputName2: "OTHER",
    typeInput: 12,
    size: 24
  },
  {
    step: 4,
    inputName: "total_monthly_expenses",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.TOTAL_MONTHLY_EXPENSES.TITLE",
    typeInput: 0,
    size: 24,
    money: true
  },
  {
    step: 4,
    inputName: "other_informations",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OTHER_INFORMATIONS.TITLE",
    typeInput: 0,
    size: 24
  },
  {
    step: 5,
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OTHER_INFORMATIONS.TITLE",
    typeInput: 13,
    size: 24
  },
  {
    step: 6,
    inputName: ["intended_supports", ":{id}", "support", "description"],
    labelName: "EMERGENCY_FUND.APPLICATIONS.STEP_SEVEN.TITLE",
    typeInput: 4,
    size: 6
  },
  {
    step: 7,
    inputName: "reason_request",
    labelName:
      "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OTHER_INFORMATIONS.TITLE",
    typeInput: 0,
    size: 24
  },
];

export const ConfigStatus = [
  {
    name: ApplicationStatus.ANALYSED,
    color: "#7768ae",
  },
  {
    name: ApplicationStatus.INTERVIEW,
    color: "#076743aa",
  },
  {
    name: ApplicationStatus.ORDERED,
    color: "#88ccf1",
  },
  {
    name: ApplicationStatus.SUBMITTED,
    color: "#076743",
  },
  {
    name: ApplicationStatus.CANCELLED,
    color: "#d0021b",
  },
  {
    name: ApplicationStatus.ASSIGNED,
    color: "#0d69dd",
  },
  {
    name: ApplicationStatus.CLOSED,
    color: "#d1d1d1",
  },
  {
    name: ApplicationStatus.CONFIRMED,
    color: "#076743",
  },
  {
    name: ApplicationStatus.APPROVED,
    color: "#076743",
  },
  {
    name: ApplicationStatus.CONTRACTED,
    color: "#076743",
  },
  {
    name: ApplicationStatus.NOTACCEPTED,
    color: "#ffa400",
  },
  {
    name: ApplicationStatus.PENDING,
    color: "#ffa400",
  },
  {
    name: ApplicationStatus.REJECTED,
    color: "#d0021b",
  },
  {
    name: ApplicationStatus.UNASSIGNED,
    color: "#d0021b",
  },
  {
    name: ApplicationStatus.OPPOSITION,
    color: "#ed9600",
  },
  {
    name: ApplicationStatus.REOPENED,
    color: "#076743",
  },
  {
    name: ApplicationStatus.QUEUED,
    color: "#ffa400",
  },
  {
    name: ApplicationStatus.DRAFT,
    color: "#f8ca00",
  },
  {
    name: ApplicationStatus.PROCESSED,
    color: "#ffa400"
  }
];
