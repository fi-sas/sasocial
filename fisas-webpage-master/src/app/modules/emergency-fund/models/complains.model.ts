export class ComplainsModel {
    id: number;
    application_id: number;
    status: string;
    user_id: number;
    complaint_reason: string;
    complaint_response: string;
    complaint_response_date: Date;
    documents: [];
}
  