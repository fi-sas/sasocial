export class ExpensesModel {
  id: number;
  application_id: number;
  intended_support_id: number;
  payment_map_id: number;
  report_id: number;
  status: string;
  last_status: string;
  user_id: number;
  document_number: number;
  document_date: Date;
  supplier_name: string;
  supplier_nif: number;
  document_value: number;
  expense_description: string;
  share_percentage: number;
  amount_to_pay: number;
  registration_date: Date;
  updated_at: Date;
  created_at: Date;
}

export class ExpensesDocModel {
  id: number;
  document_type_id: number;
  file_id: number;
  notes: string;
}