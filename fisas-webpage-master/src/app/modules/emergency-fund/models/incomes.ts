export class IncomesModel {
  id: number;
  description: string;
  active: boolean;
  created_at: Date;
  updated_at: Date;
}
