export class SupportsModel {
  id: number;
  description: string;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  checked: boolean;
  translations: [{
    description: string;
    document_type_id: number;
    id: number;
    language_id: number;
  }];
}
