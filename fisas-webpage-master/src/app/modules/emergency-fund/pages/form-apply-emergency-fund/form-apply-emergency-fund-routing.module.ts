import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FormApplyEmergencyFundComponent } from "./form-apply-emergency-fund.component";

const routes: Routes = [
  {
    path: "",
    component: FormApplyEmergencyFundComponent,
    data: {
      breadcrumb: null,
      title: null,
      scope: "accommodation:applications:create",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormApplyEmergencyFundRoutingModule {}
