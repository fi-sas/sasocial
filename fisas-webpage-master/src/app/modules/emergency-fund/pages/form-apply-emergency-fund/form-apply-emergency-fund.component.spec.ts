import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplyEmergencyFundComponent } from './form-apply-emergency-fund.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { WP_CONFIGURATION } from '@fi-sas/webpage/app.config';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxMaskModule } from 'ngx-mask';
import { HttpClient } from '@angular/common/http';

describe('FormApplyEmergencyFundComponent', () => {
  let component: FormApplyEmergencyFundComponent;
  let fixture: ComponentFixture<FormApplyEmergencyFundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgZorroAntdModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        FormsModule,
        ReactiveFormsModule,
        FiCoreModule,
        NgxMaskModule,
      ],
      declarations: [ FormApplyEmergencyFundComponent ],
      providers: [
        { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplyEmergencyFundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
