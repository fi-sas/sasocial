import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { FiConfigurator } from "@fi-sas/configurator";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { ItemsModel } from "@fi-sas/webpage/modules/current-account/models/items.model";
import { nationalities } from "@fi-sas/webpage/shared/data/nationalities";
import { CourseDegreeModel } from "@fi-sas/webpage/shared/models/course-degree.model";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import {
  FormInformationTextsModel,
  TranslationsModelFormInformation,
} from "@fi-sas/webpage/shared/models/form-information-texts.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { OrganicUnitsService } from "@fi-sas/webpage/shared/services/organic-units.service";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { TranslateService } from "@ngx-translate/core";
import { Console } from "console";
import { IncomingMessage } from "http";
import { NzModalService } from "ng-zorro-antd";
import { Subscription } from "rxjs";
import { finalize, first } from "rxjs/operators";
import { FormHouseholdsComponent } from "../../components/form-households/form-households.component";
import { ApplicantInfoModel } from "../../models/applicant-info.model";
import {
  ApplicationModel,
  FormItens,
  FormSteps,
} from "../../models/application.model";
import { DocumentsModel } from "../../models/documents";
import { SupportsModel } from "../../models/supports";
import { ApplicationsService } from "../../services/applications.service";


@Component({
  selector: "app-form-apply-emergency-fund",
  templateUrl: "./form-apply-emergency-fund.component.html",
  styleUrls: ["./form-apply-emergency-fund.component.less"],
})
export class FormApplyEmergencyFundComponent implements OnInit {
  current = 0;
  isSubmited = false;
  showErrorMessage = false;
  openInfo: boolean = false;
  formSteps = FormSteps;
  formItens = FormItens;

  get f() {
    return this.aplicationForm.controls;
  }
  infoApresent: TranslationsModelFormInformation[] = [];
  aplicationForm: FormGroup;
  other_income: boolean = false;
  documentTypes: DocumentTypeModel[] = [];
  supportsTypes: SupportsModel[] = [];
  fileList = [];
  listOfDocuments = [];
  listOfSteps = [];
  isUpdate = false;
  idToUpdate = null;
  info: FormInformationTextsModel[] = [];
  loadingButton = false;
  emergencyFundApplicationModel = new ApplicationModel();
  infoUser: UserModel;
  applicantInfo: ApplicationModel;
  documentsTypes: DocumentsModel[];
  courseDegreesList: any;
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  onLangChange: Subscription = undefined;
  currentLang;
  otherSupport: boolean = false;

  organicUnits: OrganicUnitModel[] = [];
  organicUnitLoading = false;

  nationalities = nationalities;

  courseLoading = false;

  loading_schools = false;
  schools: OrganicUnitModel[] = [];
  coursesList: any;
  expenseDefaultValue: any = [];
  fakeData = false;
  values_expense_type = ["EDUCATION", "FIXED", "OTHER"];
  stepsTitles = [
    "EMERGENCY_FUND.APPLICATIONS.STEP_ZERO.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_TWO.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_SIX.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_SEVEN.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_EIGHT.TITLE",
    "EMERGENCY_FUND.APPLICATIONS.STEP_NINE.TITLE",
  ];

  cancelApplication() {
    this.router.navigate(["emergency-fund", "home"]);
  }
  getSchool(id: number): OrganicUnitModel {
    if (this.schools) return this.schools.find((s) => s.id === id);
  }
  getCourse(id: number): CourseModel {
    if (this.coursesList) return this.coursesList.find((s) => s.id === id);
  }
  getCourseDegree(id: number): CourseDegreeModel {
    if (this.courseDegreesList)
      return this.courseDegreesList.find((s) => s.id === id);
  }
  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private authService: AuthService,
    private translateService: TranslateService,
    private applicationService: ApplicationsService,
    private route: ActivatedRoute,
    private router: Router,
    private configurationService: ConfigurationsService,
    private organicUnitsService: OrganicUnitsService,
    private sanitizer: DomSanitizer,
    private modalService: NzModalService,
    private config: FiConfigurator
  ) {
    let languagesIDS;
    if (config !== undefined) {
      languagesIDS = Object.values(config.getOptionTree('LANGUAGES'))[0];
      this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;
    } else {
      languagesIDS = undefined;
    }

    this.onLangChange = translate.onLangChange.subscribe(() => {
      if (languagesIDS !== undefined) {
        this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;

      } else {
        this.currentLang = undefined;
      }
    });
    this.getInfo();
  }
  fethDefaultDataForm() {
    /*this.reviewForm().map((item, index) => {
      if (
        this.f[item.inputName] !== undefined &&
        !this.f[item.inputName].value
      ) {
        //this.aplicationForm.get(item.inputName).patchValue("teste");
      } else {
      }
    });*/
  }

  addHouseholdsModal() {
    this.modalService.create({
      nzWidth: 600,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormHouseholdsComponent,
      nzFooter: null,
      nzComponentParams: {
        addHouseholds: (props) => this.createItemHouseholds(props),
      },
    });
  }
  modalApplicationVerifyActive() {
    this.modalService.warning({
      nzTitle: this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.APPLICATION_VERIFY"
      ),
      nzOnOk: () => {
        this.router.navigate(["emergency-fund", "list-applications"]);
      },
    });
  }
  ngOnInit(): void {
    this.listOfSteps = [
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_TWO.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_SIX.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_SEVEN.TITLE"
      ),
      this.translateService.instant(
        "EMERGENCY_FUND.APPLICATIONS.STEP_EIGHT.TITLE"
      ),
    ];
    this.aplicationForm = this.fb.group(
      {
        // 1 step
        student_name: new FormControl("", [
          Validators.required,
          trimValidation,
        ]),
        list_documents: ["", [Validators.required]],
        student_address: ["", [Validators.required, trimValidation]],
        student_zip_code: ["", [Validators.required]],
        student_location: ["", [Validators.required, trimValidation]],
        student_nationality: ["", [Validators.required]],
        student_birthdate: ["", [Validators.required]],
        student_type_identif_doc: ["", [Validators.required]],
        student_identif_number: ["", [Validators.required, trimValidation]],
        student_nif: ["", [Validators.required, trimValidation]],
        student_niss: [
          !this.fakeData ? "" : "12345678",
          [Validators.required, trimValidation],
        ],
        student_iban: [
          !this.fakeData ? "" : "PT50000785590458642224407",
          [Validators.required, trimValidation],
        ],
        student_number: ["", [Validators.required, trimValidation]],
        school_id: ["", [Validators.required]],
        course_degree_id: ["", [Validators.required]],
        course_id: ["", [Validators.required]],
        academic_year: ["", [Validators.required]],
        other_intended_support: [""],
        course_year: [!this.fakeData ? "" : "2019", [Validators.required]],
        student_email: ["", [Validators.required, trimValidation]],
        student_mobile_phone: [
          "",
          [
            Validators.required,
            trimValidation,
            Validators.minLength(9),
            Validators.maxLength(18),
          ],
        ],
        student_address_classes: [
          "Rua Gremio Lusitano 17",
          [Validators.required, trimValidation],
        ],
        student_add_class_zip_code: [
          "1200-211",
          [Validators.required, trimValidation],
        ],
        student_add_class_location: [
          "Lisbodadaa",
          [Validators.required, trimValidation],
        ],
        student_schedule: [
          !this.fakeData ? "" : "DAYTIME",
          [Validators.required],
        ],
        student_regime: [
          !this.fakeData ? null : "ORDINARY",
          [Validators.required],
        ],
        student_other_regime: [!this.fakeData ? "" : ""],
        has_rustic_property: [false, [Validators.required]],
        rustic_property_value: [""],
        has_urban_property: [false, [Validators.required]],
        urban_property_value: [""],
        has_furniture_patrimony: [false, [Validators.required]],
        total_patrimony_value: [""],
        monthly_earnings_value: [""],
        households: this.fb.array(
          !this.fakeData
            ? []
            : [
                this.fb.group({
                  person_name: [
                    !this.fakeData ? "" : "Matheus",
                    [Validators.required, trimValidation],
                  ],
                  kinship: [
                    !this.fakeData ? "" : "STUDENT",
                    [Validators.required, trimValidation],
                  ],
                  birth_date: [
                    !this.fakeData ? "" : new Date(),
                    [Validators.required],
                  ],
                  marital_status: [
                    !this.fakeData ? "" : "Solteiro",
                    [Validators.required, trimValidation],
                  ],
                  dependent_labor_value: [
                    !this.fakeData ? "" : "11",
                    [Validators.required, trimValidation],
                  ],
                  self_employment_value: [
                    !this.fakeData ? "" : "11",
                    [Validators.required, trimValidation],
                  ],
                  retirement_pension_value: [
                    !this.fakeData ? "" : "11",
                    [Validators.required, trimValidation],
                  ],
                }),
              ]
        ),
        documents: this.fb.array([]),
        other_incomes: this.fb.array([]),
        other_expenses: this.fb.array([]),
        student_other_expenses: this.fb.array([]),
        house_type: [
          !this.fakeData ? "" : "own_house",
          [Validators.required, trimValidation],
        ],
        has_bank_loan: [false, [Validators.required]],
        bank_loan_installment: [""],
        income_amount: [""],
        housing_provided_whom: [""],
        has_serious_illnesses: [false, [Validators.required]],
        has_people_deficiency: [false, [Validators.required]],
        has_health_expenses: [false, [Validators.required]],
        serious_illnesses_person: [""],
        people_deficiency_person: [""],
        average_expense_value: [""],

        nanny_value: [""],
        creche_value: [""],
        kindergarten_value: [""],
        school_value: [""],
        atl_value: [""],
        household_water_value: [""],
        household_electricity_value: [""],
        household_gas_value: [""],

        accommodation_value: [""],
        student_water_value: [""],
        student_electricity_value: [""],
        student_gas_value: [""],
        has_canteen_food: [false, [Validators.required]],
        food_value: [""],
        school_supplies_value: [""],
        has_public_transport: [false, [Validators.required]],
        has_social_pass: [false, [Validators.required]],
        social_pass_value: [""],
        has_transport: [false, [Validators.required]],
        transport_value: [""],

        has_serious_illnesses_student: [false, [Validators.required]],
        has_deficiency: [false, [Validators.required]],
        has_health_expenses_student: [false, [Validators.required]],
        expense_value_student: [""],
        total_monthly_expenses: [""],
        other_informations: [""],
        observations: [""],
        intended_supports: [
          !this.fakeData ? [] : [1],
          [Validators.required],
        ],

        has_own_housing: [false],
        has_rented_house: [false],
        has_ceded_housing: [false],
      },
      {
        validator: [],
      }
    );

    this.applicationService
      .applicationVerifyActive()
      .pipe(first())
      .subscribe((value) => {
        if (value.data[0]["active"]) {
          this.modalApplicationVerifyActive();
        }
      });
    this.applicationService
      .getIncomes()
      .pipe(first())
      .subscribe((incomes) =>
        incomes.data
          .filter((inc) => inc.active)
          .map((inc) => {
            this.addNewGroupOtherIncomesDefaultValues(inc.description, inc.id);
          })
      );

    this.applicationService
      .getDocumentsTypes(4)
      .pipe(first())
      .subscribe((documents) => {
        this.documentsTypes = documents.data
          .filter((doc) => doc.active)
          .map((doc) => {
            this.addNewGroupDocuments({
              label: doc.translations.find((x) => x.language_id == this.currentLang).description,
              document_type_id: doc.id,
            });
            return { ...doc, checked: false };
          });
      });
      
    this.applicationService
      .getSupports()
      .pipe(first())
      .subscribe((supports) => {
        this.supportsTypes = supports.data
          .filter((doc) => doc.active)
          .map((sup) => {
            return { ...sup, checked: false };
          });
      });

    this.expenseDefaultValue = [
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.NANNY.TITLE"
        ),
        formControlName: "nanny_value",
        typeExpense: this.values_expense_type[0],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.CRECHE.TITLE"
        ),
        formControlName: "creche_value",
        typeExpense: this.values_expense_type[0],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.KINDERGARTEN.TITLE"
        ),
        formControlName: "kindergarten_value",
        typeExpense: this.values_expense_type[0],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.SCHOOL.TITLE"
        ),
        formControlName: "school_value",
        typeExpense: this.values_expense_type[0],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ATL.TITLE"
        ),
        formControlName: "atl_value",
        typeExpense: this.values_expense_type[0],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.WATER.TITLE"
        ),
        formControlName: "household_water_value",
        typeExpense: this.values_expense_type[1],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ELECTRICITY.TITLE"
        ),
        formControlName: "household_electricity_value",
        typeExpense: this.values_expense_type[1],
      },
      {
        label: this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.GAS.TITLE"
        ),
        formControlName: "household_gas_value",
        typeExpense: this.values_expense_type[1],
      },
    ];

    if (!this.fakeData) {
      //this.addNewGroup();
    }

    this.loadOrganicUnits();
    this.getDocumentTypes();

    const id = this.route.snapshot.paramMap.get("id");
    if (id) {
    } else {
      this.authService
        .getUserInfoByToken()
        .pipe(first())
        .subscribe((infoUser) => {
          this.infoUser = infoUser.data[0];
          this.newApplicationStartingValues();

          this.fethDefaultDataForm();
          this.getOrganicUnits();
          this.getDegrees();
        });
    }
  }

  validField(field, expectedValue){
    if (this.aplicationForm.get(field).value == expectedValue){
      return true;
    }else{
      return false;
    }
  }

  getInfo() {
    /* 29 */
    this.configurationService.getInfoForms(30).pipe(first()).subscribe((data) => {
        this.info = data.data;
      });
  }

  getSupportDescription(support_id) {
    const support = this.supportsTypes.find(
      (x) => x.id == support_id
    );
    return support
      ? support.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
  }

  
  getDocumentsTypeDescription(document_type_id) {
    const document_type = this.documentsTypes.find(
      (x) => x.id == document_type_id
    );
    return document_type
      ? document_type.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
  }

  existInfoStep(key: string): boolean {
    this.infoApresent = [];
    let aux = this.info.find((data) => data.key == key);
    if (aux && aux.visible) {
      this.infoApresent = aux.translations;
      return true;
    }
    return !false;
  }

  getHTML(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  createItemDocuments(item) {
    return this.fb.group({
      label: [item.label],
      file_id: ["", [Validators.required]],
      document_type_id: [item.document_type_id, [Validators.required]],
      application_history_id: [null],
      interview_id: [null],
      complain_id: [null],
      general_complain_id: [null],
      application_change_id: [null],
      expense_id: [null],
      payment_map_id: [null],
    });
  }
  addNewGroupDocuments(item) {
    let add: FormArray;
    add = this.aplicationForm.get("documents") as FormArray;
    add.push(this.createItemDocuments(item));
  }

  getValue(input){
    return this.aplicationForm.get(input).value;
  }

  loadOrganicUnits() {
    this.loading_schools = true;
    this.organicUnitsService
      .organicUnitsbySchools()
      .pipe(
        first(),
        finalize(() => (this.loading_schools = false))
      )
      .subscribe((result) => {
        this.schools = result.data;
      });
  }
  filterDegrees() {
    this.aplicationForm.get("course_id").value
      ? this.aplicationForm.get("course_id").setValue(null)
      : "";
    this.aplicationForm.get("course_degree_id").value
      ? this.aplicationForm.get("course_degree_id").setValue(null)
      : "";
  }
  getDocumentTypes() {
    this.authService
      .getDocumentTypes()
      .pipe(first())
      .subscribe((data) => {
        let idCC = false;
        this.documentTypes = data.data;
        this.documentTypes.forEach((data) => {
          if (data.id == 2) {
            idCC = true;
          }
        });
        if (idCC) {
          if (!this.idToUpdate) {
            this.aplicationForm.get("student_type_identif_doc").setValue(2);
          } else {
            if (!this.applicantInfo.student_type_identif_doc) {
              this.aplicationForm.get("student_type_identif_doc").setValue(2);
            } else {
              this.aplicationForm
                .get("student_type_identif_doc")
                .setValue(this.applicantInfo.student_type_identif_doc);
            }
          }
        }
      });
  }

  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentTypes.find(
      (x) => x.id == document_type_id
    );
    return document_type
      ? document_type.translations.find((x) => x.language_id == 3).description
      : "--";
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== "VALID");
  }

  next(): void {
    this.isSubmited = true;
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = true;
        //valid = this.documentsTypes.filter((doc) => doc.checked === false).length === 0;
        break;
      }
      case 1: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("student_name"),
          this.aplicationForm.get("student_address"),
          this.aplicationForm.get("student_zip_code"),
          this.aplicationForm.get("student_location"),
          this.aplicationForm.get("student_nationality"),
          this.aplicationForm.get("student_birthdate"),
          this.aplicationForm.get("student_type_identif_doc"),
          this.aplicationForm.get("student_identif_number"),
          this.aplicationForm.get("student_nif"),
          this.aplicationForm.get("student_niss"),
          this.aplicationForm.get("student_iban"),
          this.aplicationForm.get("student_number"),
          this.aplicationForm.get("school_id"),
          this.aplicationForm.get("course_degree_id"),
          this.aplicationForm.get("course_id"),
          this.aplicationForm.get("academic_year"),
          this.aplicationForm.get("course_year"),
          this.aplicationForm.get("student_email"),
          this.aplicationForm.get("student_mobile_phone"),
          this.aplicationForm.get("student_address_classes"),
          this.aplicationForm.get("student_add_class_zip_code"),
          this.aplicationForm.get("student_add_class_location"),
          this.aplicationForm.get("student_schedule"),
          this.aplicationForm.get("student_regime"),
          this.aplicationForm.get("student_other_regime"),
        ]);
        break;
      }

      case 2: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("households"),
          this.aplicationForm.get("other_incomes"),
        ]);
        break;
      }
      case 3: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("rustic_property_value"),
          this.aplicationForm.get("urban_property_value"),
          this.aplicationForm.get("total_patrimony_value"),
          this.aplicationForm.get("monthly_earnings_value"),
        ]);
        break;
      }
      case 4: {
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("house_type"),
          this.aplicationForm.get("bank_loan_installment"),
          this.aplicationForm.get("income_amount"),
          this.aplicationForm.get("housing_provided_whom"),

          this.aplicationForm.get("serious_illnesses_person"),
          this.aplicationForm.get("people_deficiency_person"),
          this.aplicationForm.get("average_expense_value"),

          /*this.aplicationForm.get("nanny_value"),
          this.aplicationForm.get("creche_value"),
          this.aplicationForm.get("kindergarten_value"),
          this.aplicationForm.get("school_value"),
          this.aplicationForm.get("atl_value"),
          this.aplicationForm.get("household_water_value"),
          this.aplicationForm.get("household_electricity_value"),
          this.aplicationForm.get("household_gas_value"),
          this.aplicationForm.get("other_expenses"),*/
        ]);
        break;
      }
      case 5: {
        valid = this.checkValidityOfControls([
          //this.aplicationForm.get("accommodation_value"),
          //this.aplicationForm.get("student_water_value"),
          //this.aplicationForm.get("student_electricity_value"),
          //this.aplicationForm.get("student_gas_value"),
          this.aplicationForm.get("has_canteen_food"),
          this.aplicationForm.get("food_value"),
          //this.aplicationForm.get("school_supplies_value"),
          this.aplicationForm.get("has_public_transport"),
          this.aplicationForm.get("has_social_pass"),
          this.aplicationForm.get("social_pass_value"),
          this.aplicationForm.get("has_transport"),
          this.aplicationForm.get("transport_value"),
          this.aplicationForm.get("student_other_expenses"),
          this.aplicationForm.get("has_serious_illnesses_student"),
          this.aplicationForm.get("has_deficiency"),
          this.aplicationForm.get("has_health_expenses_student"),
          this.aplicationForm.get("expense_value_student"),
          //this.aplicationForm.get("total_monthly_expenses"),
          //this.aplicationForm.get("other_informations"),
        ]);
        break;
      }
      case 6: {
        /*valid = this.checkValidityOfControls([
          this.aplicationForm.get("documents"),
        ]);*/
        valid = true;
        break;
      }
      case 7:
        valid = this.checkValidityOfControls([
          this.aplicationForm.get("intended_supports"),
        ]);
        break;
      case 8:
        valid = true;
        break;
    }

    //this.current += 1;
    if (valid) {
      this.isSubmited = false;
      this.current += 1;
    }
  }

  previous(): void {
    this.current -= 1;
  }
  check(value): boolean {
    if (value !== null)
      return this.aplicationForm.get("student_schedule").value.includes(value);
    if (this.aplicationForm.get("student_schedule").value) {
      return this.aplicationForm.get("student_schedule").value.includes(value);
    } else {
      return false;
    }
  }

  checkSupports() {
    this.aplicationForm
      .get("intended_supports")
      .setValue(
        this.supportsTypes.filter((sup) => sup.checked).map((sup) => sup.id)
      );
      const prov = this.aplicationForm.get("intended_supports").value;
      this.otherSupport = prov.filter((x) => x == 8).length > 0 ? true : false;
  }

  checkOption(inputName, value): boolean {
    if (this.aplicationForm.get(inputName).value) {
      return this.aplicationForm.get(inputName).value.includes(value);
    } else {
      return false;
    }
  }

  checkDocumentList(value): boolean {
    if (this.aplicationForm.get("list_documents").value) {
      return this.aplicationForm.get("list_documents").value.includes(value);
    } else {
      return false;
    }
  }
  onChangeStudentSchedule(event) {
    if (event.length > 0) {
      this.aplicationForm.get("student_schedule").setValue(event);
    } else {
      this.aplicationForm.get("student_schedule").setValue("");
    }
  }

  onChangeStudentRegime() {
    if (this.f.student_regime.value == "OTHER") {
      this.other_income = true;
    } else {
      this.other_income = false;
    }

    if (this.other_income) {
      this.aplicationForm.controls.student_other_regime.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.student_other_regime.clearValidators();
    }
    this.aplicationForm.controls.student_other_regime.updateValueAndValidity();
  }

  checkRegime(value): boolean {
    if (this.aplicationForm.get("student_regime").value) {
      return this.aplicationForm.get("student_regime").value.includes(value);
    } else {
      return false;
    }
  }

  hasRusticPropertyValueChange() {
    if (this.aplicationForm.get("has_rustic_property").value) {
      this.aplicationForm.controls.has_rustic_property.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.rustic_property_value.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.has_rustic_property.clearValidators();
      this.aplicationForm.controls.rustic_property_value.clearValidators();
    }
    this.aplicationForm.controls.has_rustic_property.updateValueAndValidity();
    this.aplicationForm.controls.rustic_property_value.updateValueAndValidity();
  }
  hasUrbanPropertyValueChange() {
    if (this.aplicationForm.get("has_urban_property").value) {
      this.aplicationForm.controls.has_urban_property.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.urban_property_value.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.has_urban_property.clearValidators();
      this.aplicationForm.controls.urban_property_value.clearValidators();
    }
    this.aplicationForm.controls.has_urban_property.updateValueAndValidity();
    this.aplicationForm.controls.urban_property_value.updateValueAndValidity();
  }

  hasFurniturePatrimonyValueChange() {
    if (this.aplicationForm.get("has_furniture_patrimony").value) {
      this.aplicationForm.controls.has_furniture_patrimony.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.total_patrimony_value.setValidators([
        Validators.required,
      ]);
      this.aplicationForm.controls.monthly_earnings_value.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.has_furniture_patrimony.clearValidators();
      this.aplicationForm.controls.total_patrimony_value.clearValidators();
      this.aplicationForm.controls.monthly_earnings_value.clearValidators();
    }
    this.aplicationForm.controls.has_furniture_patrimony.updateValueAndValidity();
    this.aplicationForm.controls.total_patrimony_value.updateValueAndValidity();
    this.aplicationForm.controls.monthly_earnings_value.updateValueAndValidity();
  }

  createItem() {
    return this.fb.group({
      person_name: ["", [Validators.required, trimValidation]],
      kinship: ["", [Validators.required, trimValidation]],
      birth_date: ["", [Validators.required]],
      marital_status: ["", [Validators.required, trimValidation]],
      dependent_labor_value: ["", [Validators.required, trimValidation]],
      self_employment_value: ["", [Validators.required, trimValidation]],
      retirement_pension_value: ["", [Validators.required, trimValidation]],
    });
  }

  createItemHouseholds({
    name,
    kinship,
    birth_date,
    marital_status,
    dependent_labor_value,
    self_employment_value,
    retirement_pension_value,
  }) {
    let add: FormArray;
    add = this.aplicationForm.get("households") as FormArray;
    add.push(
      this.fb.group({
        person_name: [name, [Validators.required, trimValidation]],
        kinship: [kinship, [Validators.required, trimValidation]],
        birth_date: [birth_date, [Validators.required]],
        marital_status: [marital_status, [Validators.required, trimValidation]],
        dependent_labor_value: [
          dependent_labor_value,
          [Validators.required, trimValidation],
        ],
        self_employment_value: [
          self_employment_value,
          [Validators.required, trimValidation],
        ],
        retirement_pension_value: [
          retirement_pension_value,
          [Validators.required, trimValidation],
        ],
      })
    );
  }

  addNewGroup() {
    let add: FormArray;
    add = this.aplicationForm.get("households") as FormArray;
    add.push(this.createItem());
  }

  deleteGroup(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get("households") as FormArray;
    add.removeAt(index);
  }

  createItemOtherIncomesDefaultValues(income_description, id) {
    return this.fb.group({
      id: id,
      income_id: id,
      readOnly: [true],
      income_description: [
        income_description,
        [Validators.required, trimValidation],
      ],
      income_value: [
        !this.fakeData ? "" : "11",
        [Validators.required, trimValidation],
      ],
    });
  }
  addNewGroupOtherIncomesDefaultValues(income_description, id) {
    let add: FormArray;
    add = this.aplicationForm.get("other_incomes") as FormArray;
    add.push(this.createItemOtherIncomesDefaultValues(income_description, id));
  }

  createItemOtherIncomes() {
    return this.fb.group({
      id: null,
      income_id: null,
      readOnly: [false],
      income_description: ["", [Validators.required, trimValidation]],
      income_value: [
        !this.fakeData ? "" : "11",
        [Validators.required, trimValidation],
      ],
    });
  }

  addNewGroupOtherIncomes() {
    let add: FormArray;
    add = this.aplicationForm.get("other_incomes") as FormArray;
    add.push(this.createItemOtherIncomes());
  }

  deleteGroupOtherIncomes(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get("other_incomes") as FormArray;
    add.removeAt(index);
  }

  createItemOtherExpensesDefaultValues(expense_description, type) {
    return this.fb.group({
      readOnly: [true],
      expense_whom: "HOUSEHOLD",
      expense_type: [type, [Validators.required]],
      expense_description: [
        expense_description,
        [Validators.required, trimValidation],
      ],
      expense_value: [
        !this.fakeData ? "" : "11",
        [Validators.required, trimValidation],
      ],
    });
  }
  addNewGroupOtherExpensesDefaultValues(expense_description, type) {
    let add: FormArray;
    add = this.aplicationForm.get("other_expenses") as FormArray;
    add.push(
      this.createItemOtherExpensesDefaultValues(expense_description, type)
    );
  }

  createItemOtherExpenses(type) {
    return this.fb.group({
      readOnly: [false],
      expense_whom: "STUDENT",
      expense_type: [type, [Validators.required]],
      expense_description: [
        !this.fakeData ? "" : "Teste descrição",
        [Validators.required, trimValidation],
      ],
      expense_value: [
        !this.fakeData ? "" : "11",
        [Validators.required, trimValidation],
      ],
    });
  }

  addNewGroupStudentOtherExpenses() {
    let add: FormArray;
    add = this.aplicationForm.get("student_other_expenses") as FormArray;
    add.push(this.createItemOtherExpenses("FIXED"));
  }
  deleteGroupStudentOtherExpenses(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get("student_other_expenses") as FormArray;
    add.removeAt(index);
  }
  addNewGroupOtherExpenses(type) {
    let add: FormArray;
    add = this.aplicationForm.get("other_expenses") as FormArray;
    add.push(this.createItemOtherExpenses(type));
  }

  deleteGroupOtherExpenses(index: number) {
    let add: FormArray;
    add = this.aplicationForm.get("other_expenses") as FormArray;
    add.removeAt(index);
  }

  filterOtherExpenses(itens, idSection) {
    return itens.filter((item) => item.get("expense_type").value === idSection);
  }

  getValueForm(id) {
    if (this.aplicationForm.controls[id]) {
      if (id === "other_incomes") {
        this.emergencyFundApplicationModel[id] = this.aplicationForm.controls[
          id
        ].value.map((value) => {
          return {
            income_id: value.income_id,
            income_description: value.income_description,
            income_value: value.income_value,
          };
        });
      } else {
        if (id === "other_expenses") {
          this.emergencyFundApplicationModel["other_expenses"] =
            this.aplicationForm.controls["other_expenses"].value;
          this.emergencyFundApplicationModel["other_expenses"].concat(
            this.aplicationForm.controls["student_other_expenses"].value
          );
        } else {
          this.emergencyFundApplicationModel[id] =
            this.aplicationForm.controls[id].value;
        }
      }
    } else {
    }
  }
  getStudentSchedule(id) {
    switch (id) {
      case "DAYTIME":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.DAYTIME"
        );
        break;
      case "LABOR_POST":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.OTHERDAY"
        );
        break;
    }
  }
  getStudentRegime(id) {
    switch (id) {
      case "ORDINARY":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.ORDINARY"
        );
        break;
      case "STUDENT_WORKER":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.STUDENT_WORKER"
        );
        break;
    }
  }

  getStudentRequestItens(array) {
    let result = "";

    this.aplicationForm.get("intended_supports");
    array.forEach((id) => {
      result += this.getSupportDescription(id) + " ";
    });
    return result;
  }
  getFormItensBySteps(step) {
    //console.log(this.formItens.filter((item) => item.step === step));
    return this.formItens.filter((item) => item.step === step);
  }
  
  reviewForm(index) {
    // typeInput -> 0=nomal 1=date
    let itensList = [
      {
        step: 0,
        inputName: "student_name",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NAME.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_address",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADDRESS.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_zip_code",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ZIP_CODE.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_location",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_LOCATION.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_nationality",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NATIONALITY.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_birthdate",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_BIRTHDATE.TITLE",
        typeInput: 1,
      },
      {
        step: 0,
        inputName: "student_type_identif_doc",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_TYPE_IDENTIF_DOC.TITLE",
        typeInput: 2,
        valueDescription: (id) => this.getDocumentTypeDescription(id),
      },
      {
        step: 0,
        inputName: "student_identif_number",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_IDENTIF_NUMBER.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_nif",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NIF.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_niss",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NISS.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_iban",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_IBAN.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_number",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_NUMBER.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "school_id",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.SCHOOL_ID.TITLE",
        typeInput: 2,
        valueDescription: (id) => this.getSchool(id) && this.getSchool(id).name,
      },
      {
        step: 0,
        inputName: "course_id",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_ID.TITLE",
        typeInput: 2,
        valueDescription: (id) => this.getCourse(id) && this.getCourse(id).name,
      },
      {
        step: 0,
        inputName: "course_degree_id",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_DEGREE.TITLE",
        typeInput: 2,
        valueDescription: (id) =>
          this.getCourseDegree(id) && this.getCourseDegree(id).name,
      },
      {
        step: 0,
        inputName: "academic_year",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.ACADEMIC_YEAR.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "course_year",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.COURSE_YEAR.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_email",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_EMAIL.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_mobile_phone",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_MOBILE_PHONE.TITLE",
        typeInput: 0,
      },
      //{step:0,inputName:/*"student_phone",labelName:,typeInput:0},*/
      {
        step: 0,
        inputName: "student_address_classes",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADDRESS_CLASSES.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_add_class_zip_code",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADD_CLASS_ZIP_CODE.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_add_class_location",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_ADD_CLASS_LOCATION.TITLE",
        typeInput: 0,
      },
      {
        step: 0,
        inputName: "student_schedule",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.TITLE",
        typeInput: 2,
        valueDescription: (id) =>
          this.getStudentSchedule(id) && this.getStudentSchedule(id),
      },
      {
        step: 0,
        inputName: "student_regime",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.TITLE",
        typeInput: 2,
        valueDescription: (id) =>
          this.getStudentRegime(id) && this.getStudentRegime(id),
      },
      {
        step: 2,
        inputName: "has_rustic_property",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.RUSTIC_PROPERTY.TITLE",
        typeInput: 3,
      },
      {
        step: 2,
        inputName: "rustic_property_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
        typeInput: 0,
        hide: this.f["has_rustic_property"].value === "true" ? false : true,
      },
      {
        step: 2,
        inputName: "has_urban_property",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.URBAN_PROPERTY.TITLE",
        typeInput: 3,
      },
      {
        step: 2,
        inputName: "urban_property_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
        typeInput: 0,
        hide: this.f["has_urban_property"].value === "true" ? false : true,
      },
      {
        step: 2,
        inputName: "has_furniture_patrimony",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
        typeInput: 3,
      },
      {
        step: 2,
        inputName: "total_patrimony_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.PROPERTY_VALUE.TITLE",
        typeInput: 0,
        hide: this.f["has_furniture_patrimony"].value === "true" ? false : true,
      },
      {
        step: 2,
        inputName: "monthly_earnings_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_THREE.FORM_ELEMENTS.MONTHLY_EARNINGS_VALUE.TITLE",
        typeInput: 0,
        hide: this.f["has_furniture_patrimony"].value === "true" ? false : true,
      },
      {
        step: 3,
        inputName: "has_own_housing",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.OWN_HOUSE.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "has_bank_loan",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.BANK_LOAN.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "bank_loan_installment",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.INCOME_AMOUNT.TITLE",
        typeInput: 0,
        hide: this.f["has_furniture_patrimony"].value === "true" ? false : true,
      },
      {
        step: 3,
        inputName: "has_rented_house",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.RENTED_HOUSE.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "income_amount",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.INCOME_AMOUNT.TITLE",
        hide: this.f["has_rented_house"].value === "true" ? false : true,
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "has_serious_illnesses",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.SERIOUS_ILLNESSES.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "serious_illnesses_person",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.WHOSE.TITLE",
        hide: this.f["has_serious_illnesses"].value === "true" ? false : true,
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "has_people_deficiency",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.PEOPLE_DEFICIENCY.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "people_deficiency_person",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.WHOM.TITLE",
        typeInput: 0,
        hide: this.f["has_people_deficiency"].value === "true" ? false : true,
      },
      {
        step: 3,
        inputName: "has_health_expenses",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.HEALTH_EXPENSES.TITLE",
        typeInput: 3,
      },
      {
        step: 3,
        inputName: "average_expense_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.AVERAGE_EXPENSE.TITLE",
        typeInput: 0,
        hide: this.f["has_health_expenses"].value === "true" ? false : true,
      },
      {
        step: 3,
        inputName: "nanny_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.NANNY.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "creche_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.CRECHE.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "kindergarten_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.KINDERGARTEN.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "school_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.SCHOOL.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "atl_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ATL.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "household_water_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.WATER.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "household_electricity_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.ELECTRICITY.TITLE",
        typeInput: 0,
      },
      {
        step: 3,
        inputName: "household_gas_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FOUR.FORM_ELEMENTS.EXPENSE_DEFAULT_VALUES.GAS.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "accommodation_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.ACCOMMODATION_VALUE.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "student_water_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_WATER.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "student_electricity_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_ELECTRICITY.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "student_gas_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.STUDENT_ELECTRICITY.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "has_canteen_food",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.CANTEEN_FOOD.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "food_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
        typeInput: 0,
        hide: this.f["has_canteen_food"].value === "true" ? false : true,
      },
      {
        step: 4,
        inputName: "school_supplies_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "has_public_transport",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.PUBLIC_TRANSPORT.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "has_social_pass",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.SOCIAL_PASS.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "social_pass_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
        typeInput: 0,
        hide: this.f["has_social_pass"].value === "true" ? false : true,
      },
      {
        step: 4,
        inputName: "has_transport",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OWN_TRANSPORT.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "transport_value",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
        typeInput: 0,
        hide: this.f["has_transport"].value === "true" ? false : true,
      },
      {
        step: 4,
        inputName: "has_serious_illnesses_student",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.SERIOUS_ILLNESSES_STUDENT.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "has_deficiency",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.DEFICIENCY.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "has_health_expenses_student",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.HEALTH_EXPENSES_STUDENT.TITLE",
        typeInput: 3,
      },
      {
        step: 4,
        inputName: "expense_value_student",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.MONTHLY_ESTIMATE.TITLE",
        typeInput: 0,
        hide:
          this.f["has_health_expenses_student"].value === "true" ? false : true,
      },
      {
        step: 4,
        inputName: "total_monthly_expenses",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.TOTAL_MONTHLY_EXPENSES.TITLE",
        typeInput: 0,
      },
      {
        step: 4,
        inputName: "other_informations",
        labelName:
          "EMERGENCY_FUND.APPLICATIONS.STEP_FIVE.FORM_ELEMENTS.OTHER_INFORMATIONS.TITLE",
        typeInput: 0,
      },
      {
        step: 6,
        inputName: "intended_supports",
        labelName: "EMERGENCY_FUND.APPLICATIONS.STEP_SEVEN.TITLE",
        typeInput: 2,
        valueDescription: (id) =>
          this.getStudentRequestItens(id) && this.getStudentRequestItens(id),
      },
    ];

    return itensList.filter((item) => item.step === index);
  }
  valuesForm() {
    this.isSubmited = true;
    /*let valid = this.checkValidityOfControls([
      this.aplicationForm.get("applicant_consent"),
    ]);*/
    if (true) {
      this.emergencyFundApplicationModel = new ApplicationModel();
      this.isSubmited = false;
      this.current += 1;
      // 0 step
      // Identification
      let listIndex = [
        "student_name",
        "student_address",
        "student_zip_code",
        "student_location",
        "student_nationality",
        "student_birthdate",
        "student_type_identif_doc",
        "student_identif_number",
        "student_nif",
        "student_niss",
        "student_iban",
        "student_number",
        "school_id",
        "course_id",
        "course_degree_id",
        "academic_year",
        "course_year",
        "student_email",
        "student_mobile_phone",
        /*"student_phone",*/
        "student_address_classes",
        "student_add_class_zip_code",
        "student_add_class_location",
        "student_schedule",
        "student_regime",
        /*"student_regime_description",*/
        "has_rustic_property",
        "rustic_property_value",
        "has_urban_property",
        "urban_property_value",
        "has_furniture_patrimony",
        "total_patrimony_value",
        "monthly_earnings_value",
        "has_own_housing",
        "has_bank_loan",
        /*"bank_loan_installment",*/
        "has_rented_house",
        "income_amount",
        "has_ceded_housing",
        "housing_provided_whom",
        "has_serious_illnesses",
        "serious_illnesses_person",
        "has_people_deficiency",
        "people_deficiency_person",
        "has_health_expenses",
        "average_expense_value",
        "nanny_value",
        "creche_value",
        "kindergarten_value",
        "school_value",
        "atl_value",
        "household_water_value",
        "household_electricity_value",
        "household_gas_value",
        "accommodation_value",
        "student_water_value",
        "student_electricity_value",
        "student_gas_value",
        "has_canteen_food",
        "food_value",
        "school_supplies_value",
        "has_public_transport",
        "has_social_pass",
        "social_pass_value",
        "has_transport",
        "transport_value",
        "has_serious_illnesses_student",
        "has_deficiency",
        "has_health_expenses_student",
        "expense_value_student",
        "total_monthly_expenses",
        "other_informations",
        "reason_request",
        "status",
        "last_status",
        "user_id",
        "notification_guid",
        "organic_units_ids",
        "intended_supports",
        "households",
        "other_incomes",
        "other_expenses",
        "documents",
        "other_intended_support",
      ];

      listIndex.map((value) => this.getValueForm(value));
    }
  }

  submitForm() {
    
    this.valuesForm();
    this.emergencyFundApplicationModel.intended_supports = this.supportsTypes;
    //if (!this.aplicationForm.valid) return;
    if (this.idToUpdate && this.isUpdate) {
      this.loadingButton = true;
      this.applicationService
        .update(this.idToUpdate, this.emergencyFundApplicationModel)
        .pipe(
          first(),
          finalize(() => (this.loadingButton = false))
        )
        .subscribe(() => {
          this.router.navigateByUrl("emergency-fund/list-applications");
        });
    } else {
      this.loadingButton = true;
      this.applicationService
        .create(this.emergencyFundApplicationModel)
        .pipe(
          first(),
          finalize(() => (this.loadingButton = false))
        )
        .subscribe(() => {
          this.router.navigateByUrl("emergency-fund/list-applications");
        });
    }

  }

  onChangeValueHasBankLoan() {
    if (this.aplicationForm.controls.has_bank_loan.value) {
      this.aplicationForm.controls.bank_loan_installment.setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls.bank_loan_installment.clearValidators();
    }

    this.aplicationForm.controls.bank_loan_installment.updateValueAndValidity();
  }

  setInputValidatorRequired(toggleName, inputName) {
    if (this.aplicationForm.controls[toggleName].value) {
      this.aplicationForm.controls[inputName].setValidators([
        Validators.required,
      ]);
    } else {
      this.aplicationForm.controls[inputName].clearValidators();
    }

    this.aplicationForm.controls[inputName].updateValueAndValidity();
  }

  onChangeHouseType(event) {
    if (event == "ceded_house") {
      this.f.housing_provided_whom.setValidators([Validators.required]);

      this.aplicationForm.get("has_own_housing").setValue(false);
      this.aplicationForm.get("has_rented_house").setValue(false);
      this.aplicationForm.get("has_ceded_housing").setValue(true);

      this.aplicationForm.get("has_bank_loan").setValue(false);
    } else if (event == "rented_house") {
      this.f.income_amount.setValidators([Validators.required]);
      this.aplicationForm.get("has_bank_loan").setValue(false);

      this.aplicationForm.get("has_own_housing").setValue(false);
      this.aplicationForm.get("has_rented_house").setValue(true);
      this.aplicationForm.get("has_ceded_housing").setValue(false);
    } else {
      this.f.housing_provided_whom.clearValidators();
      this.f.income_amount.clearValidators();
      this.f.bank_loan_installment.clearValidators();

      this.aplicationForm.get("has_own_housing").setValue(true);
      this.aplicationForm.get("has_rented_house").setValue(false);
      this.aplicationForm.get("has_ceded_housing").setValue(false);
    }
    this.f.housing_provided_whom.updateValueAndValidity();
    this.f.income_amount.updateValueAndValidity();
    this.f.bank_loan_installment.updateValueAndValidity();
  }

  newApplicationStartingValues() {
    
    this.getDegrees();
    if (this.infoUser) {
      this.aplicationForm.get("student_name").patchValue(this.infoUser.name);
      this.aplicationForm.get("student_name").updateValueAndValidity();
    }
    if (this.infoUser.birth_date) {
      this.aplicationForm
        .get("student_birthdate")
        .patchValue(this.infoUser.birth_date);
      this.aplicationForm.get("student_birthdate").updateValueAndValidity();
    }
    if (this.infoUser.email) {
      this.aplicationForm.get("student_email").patchValue(this.infoUser.email);
      this.aplicationForm.get("student_email").updateValueAndValidity();
    }
    
    if (this.infoUser.document_type_id) {
      this.aplicationForm
        .get("student_type_identif_doc")
        .patchValue(this.infoUser.document_type_id);
      this.aplicationForm
        .get("student_type_identif_doc")
        .updateValueAndValidity();
    }
    if (this.infoUser.identification) {
      this.aplicationForm
        .get("student_identif_number")
        .patchValue(this.infoUser.identification);
      this.aplicationForm
        .get("student_identif_number")
        .updateValueAndValidity();
    }

    if (this.infoUser.nationality) {
      this.aplicationForm
        .get("student_nationality")
        .patchValue(this.infoUser.nationality);
      this.aplicationForm.get("student_nationality").updateValueAndValidity();
    }

    if (this.infoUser.phone) {
      this.aplicationForm
        .get("student_mobile_phone")
        .patchValue(this.infoUser.phone);
      this.aplicationForm.get("student_mobile_phone").updateValueAndValidity();
    }

    if (this.infoUser.address) {
      this.aplicationForm
        .get("student_address")
        .patchValue(this.infoUser.address);
      this.aplicationForm.get("student_address").updateValueAndValidity();
    }
    if (this.infoUser.postal_code) {
      this.aplicationForm
        .get("student_zip_code")
        .patchValue(this.infoUser.postal_code);
      this.aplicationForm.get("student_zip_code").updateValueAndValidity();
    }
    if (this.infoUser.city) {
      this.aplicationForm
        .get("student_location")
        .patchValue(this.infoUser.city);
      this.aplicationForm.get("student_location").updateValueAndValidity();
    }

    if (this.infoUser.student_number) {
      this.aplicationForm
        .get("student_number")
        .patchValue(this.infoUser.student_number);
      this.aplicationForm.get("student_number").updateValueAndValidity();
    }

    if (this.infoUser.organic_unit_id) {
      this.aplicationForm
        .get("school_id")
        .patchValue(this.infoUser.organic_unit_id);
      this.aplicationForm.get("school_id").updateValueAndValidity();
    }
    if (this.infoUser.course_degree_id) {
      this.aplicationForm
        .get("course_degree_id")
        .patchValue(this.infoUser.course_degree_id);
      this.aplicationForm.get("course_degree_id").updateValueAndValidity();
      this.clickDegree();
    }
    if (this.infoUser.course_id) {
      this.aplicationForm.get("course_id").patchValue(this.infoUser.course_id);
      this.aplicationForm.get("course_id").updateValueAndValidity();
    }
    if (this.infoUser.course_year) {
      this.aplicationForm
        .get("academic_year")
        .patchValue(this.infoUser.course_year.toString());
      this.aplicationForm.get("academic_year").updateValueAndValidity();
    }
    /*if (this.infoUser.admission_date) {
      this.aplicationForm
        .get("course_year")
        .patchValue(this.infoUser.admission_date);
      this.aplicationForm.get("course_year").updateValueAndValidity();
    }*/
    if (this.infoUser.tin) {
      this.aplicationForm.get("student_nif").patchValue(this.infoUser.tin);
      this.aplicationForm.get("student_nif").updateValueAndValidity();
    }
  }

  getDegrees() {
    this.configurationService
      .courseDegrees()
      .pipe(first())
      .subscribe((arg) => {
        this.courseDegreesList = arg.data;
      });
  }

  changeValue() {
    this.f.course_id.setValue(null);
    this.clickDegree();
  }

  clickDegree() {
    if (this.aplicationForm.get("course_degree_id").value) {
      this.courseLoading = true;
      this.configurationService
        .courses(this.aplicationForm.get("course_degree_id").value)
        .pipe(
          first(),
          finalize(() => {
            this.courseLoading = false;
          })
        )
        .subscribe((arg) => {
          this.coursesList = arg.data;
        });
    }
  }
  getOrganicUnits() {
    if (this.organicUnits.length === 0) {
      this.organicUnitLoading = true;
      this.organicUnitsService.organicUnitsSchools().subscribe((result) => {
        this.organicUnits = result.data;
        this.organicUnitLoading = false;
      });
    }
  }
}
