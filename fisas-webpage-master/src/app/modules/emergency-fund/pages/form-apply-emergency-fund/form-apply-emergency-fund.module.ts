import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { FormApplyEmergencyFundComponent } from "./form-apply-emergency-fund.component";
import { FormApplyEmergencyFundRoutingModule } from "./form-apply-emergency-fund-routing.module";
import { EmergencyFundModule } from "../../emergency-fund.module";
import { EmergencyFundSharedModule } from "../../emergency-fund-shared.module";
@NgModule({
  declarations: [FormApplyEmergencyFundComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormApplyEmergencyFundRoutingModule,
    EmergencyFundModule,
    EmergencyFundSharedModule,
  ],
})
export class FormApplyEmergencyFundModule {}
