import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { TextApresentationsModel } from "@fi-sas/webpage/shared/models/text-apresentation.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { finalize, first } from "rxjs/operators";
import { ApplicationsService } from "../../services/applications.service";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.less"],
})
export class HomeComponent implements OnInit {
  info: TextApresentationsModel[] = [];
  appActive: boolean = false;

  constructor(
    private applicationsService: ApplicationsService,
    private configurationsService: ConfigurationsService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.getInfoApresentation();
    this.ApplicationVerifyActive();
  }

  ApplicationVerifyActive(){
    this.applicationsService
    .applicationVerifyActive()
    .pipe(first())
    .subscribe((val) => {
      this.appActive = (val.data[0].id == null) ? false : true;
    });
  }

  getInfoApresentation() {
    this.configurationsService
      .listTextApresentations(30)
      .pipe(first())
      .subscribe((data) => {
        this.info = data.data;
      });
  }

  getHTML(value) {
    if (value) {
      value = value.replace("line-height: 1.17", "line-height: 0.17");
    }
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }
}
