import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListApplicationsChangesComponent } from './list-applications-changes.component';


const routes: Routes = [
  {
    path: '',
    component: ListApplicationsChangesComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListApplicationsChangesRoutingModule { }
