import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListApplicationsChangesComponent } from './list-applications-changes.component';

describe('ListApplicationsChangesComponent', () => {
  let component: ListApplicationsChangesComponent;
  let fixture: ComponentFixture<ListApplicationsChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListApplicationsChangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListApplicationsChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
