import { Component, Input, OnInit } from "@angular/core";
import {
  ApplicationModel,
  ApplicationStatus,
} from "../../models/application.model";
import { ApplicationsService } from "../../services/applications.service";
import { finalize, first } from "rxjs/operators";
import { ConfigStatus } from "../../models/application.model";
import { TranslateService } from "@ngx-translate/core";
import { NzModalRef, NzModalService } from "ng-zorro-antd";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApplicationChangesStatusModel } from "../../models/application-changes-status.model";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { controllers } from "chart.js";
import { ConsultReportsInterviewComponent } from "../../components/consult-reports-interview/consult-reports-interview.component";
import { HistoricComponent } from "../../components/historic/historic.component";
import { FormComplainComponent } from "../../components/form-complain/form-complain.component";
import { FormRequestChangesComponent } from "../../components/form-request-changes/form-request-changes.component";
import { FormSubmitExpensiveComponent } from "../../components/form-submit-expensive/form-submit-expensive.component";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { ApplicationChangesModel } from "../../models/application-changes.model";
import { ApplicationsChangesService } from "../../services/applications_changes.service";
import { ApplicationChangesModalComponent } from "../../components/application-changes-modal/application-changes-modal.component";

@Component({
  selector: "app-list-applications-changes",
  templateUrl: "./list-applications-changes.component.html",
  styleUrls: ["./list-applications-changes.component.less"],
})
export class ListApplicationsChangesComponent implements OnInit {
  @Input() applicationCard = null;
  applications: ApplicationChangesModel[] = [];
  loading = true;
  loadingChanges = false;
  configStatus = ConfigStatus;
  applicationStatus = ApplicationStatus;
  consultReportsIntervewModal: NzModalRef = null;
  currentActivatedApplicationChange: any;
  currentApplication: any;
  info: any;
  lista: ApplicationChangesModel[] = [];
  noApplication = false;

  panels = [
    {
      active: true,
      disabled: false,
      name: "SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE",
      customStyle: {
        background: "#f0f2f5",
        padding: "0px",
        border: "0px",
        margin: "0px",
      },
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private applicationDataService: ApplicationDataService,
    private applicationsChangesService: ApplicationsChangesService,
    private applicationsService: ApplicationsService,
    private translateService: TranslateService,
    private uiService: UiService,
    private router: Router,
    private modalService: NzModalService
  ) {}

  ngOnInit() {
    if(!this.applicationCard) {
      this.getUserApplication();
    }else {
        this.noApplication = true;
        this.loadApplicationChangeRequests(this.applicationCard.id);
    }
  }

  getUserApplication() {
    this.applicationsService
    .applicationVerifyActive()
    .pipe(
      first(), 
      finalize(() => this.loading = false)
    )
    .subscribe((val) => {
      this.currentApplication = val.data.filter((value) => value.active = true)[0].id;
      if( this.currentApplication ){
        this.noApplication = true;
        this.loadApplicationChangeRequests(this.currentApplication);
      }else{
        this.noApplication = false;
      }
    });
  }

  loadApplicationChangeRequests(id) {
    this.loadingChanges = true;
    this.applicationsChangesService
      .getListById(id)
      .pipe(
        first(),
        finalize(() => this.loadingChanges = false)
      )
      .subscribe((val) => {
        this.lista = val.data.reverse();
      });
  }

  isPar(val) {
    return (val % 2 === 0);
}

  modalCancel(id_) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancel(id_),
    });
  }

  cancel(_id) {
    const applicationStatus: ApplicationChangesStatusModel = {
      id: _id,
      event: "CANCEL",
      notes: "Cancelado pelo Aluno",
    };
 
    this.applicationsService
      .changeStatusApplicationChanges(applicationStatus)
      .subscribe(
        (status) => {
          if (status) {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS"
              )
            );
            // this.getApplication();
            this.loadApplicationChangeRequests(this.currentApplication);
          }
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR"
            )
          );
        }
      );
  }

  consultHistoric(data): void {
    this.applicationsChangesService
    .getACById(data.id)
    .pipe(
      first()
    )
    .subscribe((val) => {
      this.info = val.data[0];
      this.consultReportsIntervewModal = this.modalService.create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: HistoricComponent,
        nzFooter: null,
        nzComponentParams: { data: [this.info,  "EMERGENCY_FUND.LIST_APPLICATIONS_CHANGES.TITLE"] },
      });
    });
    
  }


  reviewApplication(id) {
    this.router.navigateByUrl("emergency-fund/list-applications-changes/" + id);
  }

  openApplicationChangesModal(change: ApplicationChangesModel, consult: boolean) {
    this.modalService
        .create({
            nzTitle: null,
            nzContent: ApplicationChangesModalComponent,
            nzFooter: null,
            nzComponentParams: {
                application: this.applicationCard ? this.applicationCard : this.currentApplication,
                changesEdit: change,
                consult: consult,
            },
            nzWidth: 650,
            nzWrapClassName: 'vertical-center-modal',
            nzOkText: "Submeter"
        }).afterClose.pipe(first())
        .subscribe((success: boolean) => {
            if (success)
                this.loadApplicationChangeRequests(this.currentApplication);
        });
}


/*
complainModal() {
  let app = this.applications.filter(
    (value) => value.status !== this.applicationStatus.CANCELLED
  )[0];
  this.consultReportsIntervewModal = this.modalService.create({
    nzWidth: 500,
    nzMaskClosable: false,
    nzClosable: true,
    nzContent: FormComplainComponent,
    nzFooter: null,
    nzComponentParams: {
      data: {
        activatedSubmitionId: this.currentApplication,
        userId: app.user_id,
        applicationId: 29,
      },
    },
  });
}

requestChangesModal() {
  let app = this.applications.filter(
    (value) => value.status !== this.applicationStatus.CANCELLED
  )[0];
  this.consultReportsIntervewModal = this.modalService.create({
    nzWidth: 500,
    nzMaskClosable: false,
    nzClosable: true,
    nzContent: FormRequestChangesComponent,
    nzFooter: null,
    nzComponentParams: {
      data: {
        activatedSubmitionId: this.currentApplication,
        userId: app.user_id,
      },
    },
  });  
  this.consultReportsIntervewModal.afterClose.subscribe(result => {  
      this.getUserApplication(); 
  });
}*/


}
