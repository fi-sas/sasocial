import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ListApplicationsChangesComponent } from "./list-applications-changes.component";
import { ListApplicationsChangesRoutingModule } from "./list-applications-changes-routing.module";
import { EmergencyFundModule } from "../../emergency-fund.module";
import { EmergencyFundSharedModule } from "../../emergency-fund-shared.module";

@NgModule({
  declarations: [ListApplicationsChangesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ListApplicationsChangesRoutingModule,
    EmergencyFundModule,
    EmergencyFundSharedModule,
  ],
  exports: [],
})
export class ListApplicationsChangesEmergencyFundModule {}
