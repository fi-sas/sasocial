import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListApplicationsComponent } from './list-applications.component';


const routes: Routes = [
  {
    path: '',
    component: ListApplicationsComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListApplicationsRoutingModule { }
