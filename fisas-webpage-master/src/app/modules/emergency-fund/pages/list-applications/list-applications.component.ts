import { Component, OnInit } from "@angular/core";
import {
  ApplicationModel,
  ApplicationStatus,
} from "../../models/application.model";
import { ApplicationsService } from "../../services/applications.service";
import { finalize, first } from "rxjs/operators";
import { ConfigStatus } from "../../models/application.model";
import { TranslateService } from "@ngx-translate/core";
import { NzModalRef, NzModalService } from "ng-zorro-antd";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApplicationChangeStatusModel } from "../../models/application-change-status.model";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { controllers } from "chart.js";
import { ConsultReportsInterviewComponent } from "../../components/consult-reports-interview/consult-reports-interview.component";
import { HistoricComponent } from "../../components/historic/historic.component";
import { FormComplainComponent } from "../../components/form-complain/form-complain.component";
import { FormRequestChangesComponent } from "../../components/form-request-changes/form-request-changes.component";
import { FormSubmitExpensiveComponent } from "../../components/form-submit-expensive/form-submit-expensive.component";
import { ApplicationsChangesService } from "../../services/applications_changes.service";

@Component({
  selector: "app-list-applications",
  templateUrl: "./list-applications.component.html",
  styleUrls: ["./list-applications.component.less"],
})
export class ListApplicationsComponent implements OnInit {
  applications: ApplicationModel[] = [];
  loading = true;
  configStatus = ConfigStatus;
  applicationStatus = ApplicationStatus;
  consultReportsIntervewModal: NzModalRef = null;
  currentActivatedApplication: any;
  currentActivatedApplicationChange: any;

  panels = [
    {
      active: true,
      disabled: false,
      name: "SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE",
      customStyle: {
        background: "#f0f2f5",
        padding: "0px",
        border: "0px",
        margin: "0px",
      },
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private applicationDataService: ApplicationDataService,
    private applicationsService: ApplicationsService,
    private applicationsChangesService: ApplicationsChangesService,
    private translateService: TranslateService,
    private uiService: UiService,
    private router: Router,
    private modalService: NzModalService
  ) {}
  
  complainModal() {
    let app = this.applications.filter(
      (value) => value.status !== this.applicationStatus.CANCELLED
    )[0];
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormComplainComponent,
      nzFooter: null,
      nzComponentParams: {
        data: {
          activatedSubmitionId: app.id,
          userId: app.user_id,
          applicationId: 29,
        },
      },
    });
  }

  requestChangesModal() {
    let app = this.applications.filter(
      (value) => value.status !== this.applicationStatus.CANCELLED
    )[0];
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormRequestChangesComponent,
      nzFooter: null,
      nzComponentParams: {
        data: {
          activatedSubmitionId: app.id,
          userId: app.user_id,
        },
      },
    });
  }

  submitExpensiveModal() {
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 1000,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormSubmitExpensiveComponent,
      nzFooter: null,
      nzComponentParams: {
        data: {
          activatedSubmitionId: this.currentActivatedApplication
        },
      },
    });
  }

  openApplication() {
    if( this.currentActivatedApplication === undefined ){
      this.router.navigateByUrl("emergency-fund/form-apply-emergency-fund");
    }
  }



  ngOnInit() {
    this.loadPage();
  }


  loadPage() {
    this.applicationsService
      .list(0, 99)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((val) => {
        this.applications = val.data.reverse();

        this.currentActivatedApplication = this.applications.filter(
          (value) => value.status !== this.applicationStatus.CANCELLED
        )[0];  
        if(this.currentActivatedApplication != undefined) {
          this.applicationsChangesService
          .getACById(this.currentActivatedApplication.id)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe((val_) => {
            this.currentActivatedApplicationChange = val_.data.reverse();
            this.currentActivatedApplicationChange = this.currentActivatedApplicationChange.filter( (value_) => value_.status == this.applicationStatus.SUBMITTED)[0];
            
          });
        }
      });

    this.applicationDataService.createApplication(this.applications);
  }


  modalCancel() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancel(),
    });
  }

  cancel() {
    const applicationStatus: ApplicationChangeStatusModel = {
      event: "CANCEL",
      notes: "Cancelado pelo Aluno",
    };
 
    this.applicationsService
      .changeStatus(this.applications[0].id, applicationStatus)
      .subscribe(
        (status) => {
          if (status) {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS"
              )
            );
            // this.getApplication();
            this.backToListApplications();
          }
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR"
            )
          );
        }
      );
  }

  consultHistoric(data): void {
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: HistoricComponent,
      nzFooter: null,
      nzComponentParams: { data: [data, "EMERGENCY_FUND.LIST_APPLICATIONS.CARD.TITLE"] },
    });
  }

  consultReportsInterview(interview): void {
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: ConsultReportsInterviewComponent,
      nzFooter: null,
      nzComponentParams: { interview: interview },
    });
  }

  reviewApplication(id) {
    this.router.navigateByUrl("emergency-fund/list-applications/" + id);
  }

  backToListApplications(): void {
    this.loadPage();
  }
  
  showAssignedResidence(index: number): boolean {
    const application = this.applications[index];
    const states = [
      "assigned",
      "confirmed",
      "rejected",
      "contracted",
      "closed",
    ];
    return false;
    //return states.includes(application.status) && application.assigned_residence_id != null;
  }
}
