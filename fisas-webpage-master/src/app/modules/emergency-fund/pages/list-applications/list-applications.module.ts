import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ListApplicationsComponent } from "./list-applications.component";
import { ListApplicationsRoutingModule } from "./list-applications-routing.module";
import { EmergencyFundModule } from "../../emergency-fund.module";
import { EmergencyFundSharedModule } from "../../emergency-fund-shared.module";

@NgModule({
  declarations: [ListApplicationsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ListApplicationsRoutingModule,
    EmergencyFundModule,
    EmergencyFundSharedModule,
  ],
  exports: [],
})
export class ListApplicationsEmergencyFundModule {}
