import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListExpensesComponent } from './list-expenses.component';


const routes: Routes = [
  {
    path: '',
    component: ListExpensesComponent,
    data: { breadcrumb: null, title: null, scope: 'accommodation:applications:read' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListExpensesRoutingModule { }
