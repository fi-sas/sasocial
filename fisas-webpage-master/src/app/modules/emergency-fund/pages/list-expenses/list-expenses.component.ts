import { Component, OnInit } from "@angular/core";
import {
  ApplicationModel,
  ApplicationStatus,
} from "../../models/application.model";
import { ApplicationsService } from "../../services/applications.service";
import { finalize, first } from "rxjs/operators";
import { ConfigStatus } from "../../models/application.model";
import { TranslateService } from "@ngx-translate/core";
import { NzModalRef, NzModalService } from "ng-zorro-antd";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApplicationChangesStatusModel } from "../../models/application-changes-status.model";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { controllers } from "chart.js";
import { ConsultReportsInterviewComponent } from "../../components/consult-reports-interview/consult-reports-interview.component";
import { HistoricComponent } from "../../components/historic/historic.component";
import { FormComplainComponent } from "../../components/form-complain/form-complain.component";
import { FormRequestChangesComponent } from "../../components/form-request-changes/form-request-changes.component";
import { FormSubmitExpensiveComponent } from "../../components/form-submit-expensive/form-submit-expensive.component";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { ApplicationChangesModel } from "../../models/application-changes.model";
import { ExpensesModel } from "../../models/expenses";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { DocumentsModel } from "../../models/documents";
import { SupportsModel } from "../../models/supports";
import { intendedSupports } from "../../models/intended-supports.model";
import { FiConfigurator } from "@fi-sas/configurator";
import { Subscription } from "rxjs";
import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import { ExpensesService } from "../../services/expenses.service";

@Component({
  selector: "app-list-expenses",
  templateUrl: "./list-expenses.component.html",
  styleUrls: ["./list-expenses.component.less"],
})

export class ListExpensesComponent implements OnInit {
  applications: ExpensesModel[] = [];
  loading = true;
  configStatus = ConfigStatus;
  applicationStatus = ApplicationStatus;
  consultReportsIntervewModal: NzModalRef = null;
  onLangChange: Subscription = undefined;
  currentLang;
  file = [];


  currentApp: any;
  lista: any[];
  intendedSupports: intendedSupports[];
  Supports: SupportsModel[];
  documentsTypes: DocumentsModel[];
  noApplication = false;
  loadingChanges = false;


  panels = [
    {
      active: true,
      disabled: false,
      name: "SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE",
      customStyle: {
        background: "#f0f2f5",
        padding: "0px",
        border: "0px",
        margin: "0px",
      },
    },
  ];

  constructor(
    private route: ActivatedRoute,
    private applicationDataService: ApplicationDataService,
    private applicationsService: ApplicationsService,
    private expensesService: ExpensesService,
    private translateService: TranslateService,
    private translate: TranslateService,
    private uiService: UiService,
    private router: Router,
    private modalService: NzModalService,
    private fileService: FilesService,
    private config: FiConfigurator
  ) {
    let languagesIDS;
    if (config !== undefined) {
      languagesIDS = Object.values(config.getOptionTree('LANGUAGES'))[0];
      this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;
    } else {
      languagesIDS = undefined;
    }

    this.onLangChange = translate.onLangChange.subscribe(() => {
      if (languagesIDS !== undefined) {
        this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;

      } else {
        this.currentLang = undefined;
      }
    });
  }

  complainModal() {
    let app = this.applications.filter(
      (value) => value.status !== this.applicationStatus.CANCELLED
    )[0];
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormComplainComponent,
      nzFooter: null,
      nzComponentParams: {
        data: {
          activatedSubmitionId: this.currentApp,
          userId: app.user_id,
          applicationId: this.currentApp,
        },
      },
    });
  }

  requestChangesModal() {
    let app = this.applications.filter(
      (value) => value.status !== this.applicationStatus.CANCELLED
    )[0];
    this.consultReportsIntervewModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormRequestChangesComponent,
      nzFooter: null,
      nzComponentParams: {
        data: {
          activatedSubmitionId: this.currentApp,
          userId: app.user_id,
        },
      },
    });  
    this.consultReportsIntervewModal.afterClose.subscribe(result => {  
        this.loadPage(); 
    });


  }

  ngOnInit() {
    this.applicationsService
    .getDocumentsTypes(3)
    .pipe(first())
    .subscribe((documents) => {
      this.documentsTypes = documents.data
        .filter((doc) => doc.active)
        .map((doc) => {
          return { ...doc, checked: false };
        });
    });

    this.applicationsService
    .applicationVerifyActive()
    .pipe(
      first()
    )
    .subscribe((val) => {
      this.currentApp = val.data.filter((value) => value.active = true)[0].id;
      if( this.currentApp ){
        this.applicationsService
        .getIntendedSupports(this.currentApp)
        .pipe(first())
        .subscribe((is) => {
          this.intendedSupports = is.data
            .filter((is) => is.active_support)
            .map((is) => {
              return { ...is, checked: false };
            });
        });
        this.applicationsService
        .getSupports()
        .pipe(first(), finalize(() => (this.loading = false)))
        .subscribe((s) => {
          this.Supports = s.data
            .filter((s) => s.active)
            .map((s) => {
              return { ...s, checked: false };
            });
            this.loadPage();
        });
      }
    });
  }

  
  loadFile(data) {
    this.fileService.file(data[0].file_id).pipe(first()).subscribe((file)=>{
      window.open(file.data[0].url, "_blank");;
    });
  }

  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentsTypes.find(
      (x) => x.id == document_type_id
    );
    return document_type
      ? document_type.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
  }

  getSupportDescription(support_id) {
    console.log(this.intendedSupports);
    console.log(support_id);
    if(support_id != 3 ){
    const support_is = this.intendedSupports.find(
      (x) => x.id == support_id
    ).support_id;
    const support = this.Supports.find(
      (x) => x.id == support_is
    );
    return support
      ? support.translations.find((x) => x.language_id == this.currentLang).description
      : "--";
    }else{
      return "--";
    }
  }

  
  loadPage() {
    this.loadingChanges = true;
    this.applicationsService
      .listExpenses(this.currentApp)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((val) => {
        this.loadingChanges = false;
        this.lista = val.data.reverse(); 
        this.noApplication = true;
        console.log(this.lista);
        /*this.currentActivatedApplicationChange = this.applications.filter( (value) => value.status == this.applicationStatus.SUBMITTED)[0];*/

      });

    /*this.applicationDataService.createApplication(this.applications);*/
  }

  isPar(val) {
    return (val % 2 === 0);
  }
  
  modalCancel(id_) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(
        "VIEW_APPLICATION_STATUS.CANCEL_TEXT"
      ),
      nzOnOk: () => this.cancel(id_),
    });
  }

  cancel(_id) {
    const applicationStatus: ApplicationChangesStatusModel = {
      id: _id,
      event: "CANCEL",
      notes: "Cancelado pelo Aluno",
    };
 
    this.applicationsService
      .changeStatusApplicationChanges(applicationStatus)
      .subscribe(
        (status) => {
          if (status) {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant(
                "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS"
              )
            );
            // this.getApplication();
            this.backToListApplications();
          }
        },
        () => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant(
              "VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR"
            )
          );
        }
      );
  }

  consultHistoric(data): void {
    this.expensesService
    .get(data.id)
    .pipe(
      first()
    )
    .subscribe((val) => {
      this.consultReportsIntervewModal = this.modalService.create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: HistoricComponent,
        nzFooter: null,
        nzComponentParams: { data: [val.data[0],  "EMERGENCY_FUND.MENU.EXPENSES"] },
      });
    });


  }



  backToListApplications(): void {
    this.router.navigateByUrl("emergency-fund/list-applications", {state: {teste: "teste"}});
  }

}
