import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ListExpensesComponent } from "./list-expenses.component";
import { ListExpensesRoutingModule } from "./list-expenses-routing.module";
import { EmergencyFundModule } from "../../emergency-fund.module";
import { EmergencyFundSharedModule } from "../../emergency-fund-shared.module";

@NgModule({
  declarations: [ListExpensesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ListExpensesRoutingModule,
    EmergencyFundModule,
    EmergencyFundSharedModule,
  ],
  exports: [],
})
export class ListExpensesEmergencyFundModule {}
