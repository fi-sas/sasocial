import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReviewApplicationChangesComponent } from "./review-application-changes.component";

const routes: Routes = [
  {
    path: "",
    component: ReviewApplicationChangesComponent,
    data: {
      breadcrumb: null,
      title: null,
      scope: "accommodation:applications:read",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewApplicationChangesRoutingModule {}
