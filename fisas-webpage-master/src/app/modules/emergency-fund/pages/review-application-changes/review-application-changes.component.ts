import { Component, OnInit } from "@angular/core";
import {
  FormItens,
  FormSteps,
} from "../../models/application.model";
import { ConfigStatus } from "../../models/application.model";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";

import { TextApresentationsModel } from "@fi-sas/webpage/shared/models/text-apresentation.model";
import { first } from "rxjs/operators";
import { ApplicationChangesModel } from "../../models/application-changes.model";
import { ApplicationsChangesService } from "../../services/applications_changes.service";

@Component({
  selector: "app-home",
  templateUrl: "./review-application-changes.component.html",
  styleUrls: ["./review-application-changes.component.less"],
})
export class ReviewApplicationChangesComponent implements OnInit {
  info: TextApresentationsModel[] = [];

  formSteps = FormSteps;
  formItens = FormItens;
  application: ApplicationChangesModel = null;
  documentTypes: DocumentTypeModel[] = [];
  configStatus = ConfigStatus;

  constructor(
    private router: Router,
    private applicationsChangesService: ApplicationsChangesService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    this.getInfoApresentation();
  }

  getInfoApresentation() {
    let id = parseInt(this.route.snapshot.paramMap.get("id"));
    return this.applicationsChangesService
      .getACById(id)
      .pipe(first())
      .subscribe((val) => {
        this.application = val.data[0];
        console.log(this.application);
      });
  }

  back() {
    this.router.navigate(["emergency-fund", "list-applications-changes"]);
  }

  reviewApplication(id) {
    this.router.navigateByUrl("emergency-fund/list-applications/" + id);
  }
  
}
