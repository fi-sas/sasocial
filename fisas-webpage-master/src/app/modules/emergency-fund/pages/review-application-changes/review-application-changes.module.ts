import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ReviewApplicationChangesComponent } from "./review-application-changes.component";
import { ReviewApplicationChangesRoutingModule } from "./review-application-changes-routing.module";
@NgModule({
  declarations: [ReviewApplicationChangesComponent],
  imports: [CommonModule, SharedModule, ReviewApplicationChangesRoutingModule],
})
export class ReviewApplicationChangesModule {}
