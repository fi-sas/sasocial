import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReviewApplicationComponent } from "./review-application.component";

const routes: Routes = [
  {
    path: "",
    component: ReviewApplicationComponent,
    data: {
      breadcrumb: null,
      title: null,
      scope: "accommodation:applications:read",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewApplicationRoutingModule {}
