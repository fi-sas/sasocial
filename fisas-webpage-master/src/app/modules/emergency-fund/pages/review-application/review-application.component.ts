import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { FiConfigurator } from "@fi-sas/configurator";
import { Resource } from "@fi-sas/core";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { nationalities } from "@fi-sas/webpage/shared/data/nationalities";
import { CourseDegreeModel } from "@fi-sas/webpage/shared/models/course-degree.model";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { TextApresentationsModel } from "@fi-sas/webpage/shared/models/text-apresentation.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { OrganicUnitsService } from "@fi-sas/webpage/shared/services/organic-units.service";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

import { finalize, first } from "rxjs/operators";
import {
  ApplicationModel,
  FormItens,
  FormSteps,
} from "../../models/application.model";
import { ApplicationsService } from "../../services/applications.service";
@Component({
  selector: "app-home",
  templateUrl: "./review-application.component.html",
  styleUrls: ["./review-application.component.less"],
})
export class ReviewApplicationComponent implements OnInit {
  info: TextApresentationsModel[] = [];
  isLoading: boolean = true;
  formSteps = FormSteps;
  formItens = FormItens;
  application: ApplicationModel = null;
  documentTypes: DocumentTypeModel[] = [];
  otherExpenses_Education: any[] = [];
  otherExpenses_FIXED: any[] = [];
  otherExpenses: any[] = [];
  loadingFiles: boolean = false;
  filesOthers = [];

  onLangChange: Subscription = undefined;

  courseDegreesList: any;
  schools: OrganicUnitModel[] = [];
  coursesList: any;
  currentLang;
  nationalities = nationalities;

  constructor(
    private router: Router,
    private applicationsService: ApplicationsService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private authService: AuthService,
    private translate: TranslateService,
    private translateService: TranslateService,
    private configurationService: ConfigurationsService,
    private organicUnitsService: OrganicUnitsService,
    private config: FiConfigurator

  ) {
    let languagesIDS;
    if (config !== undefined) {
      languagesIDS = Object.values(config.getOptionTree('LANGUAGES'))[0];
      this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;
    } else {
      languagesIDS = undefined;
    }

    this.onLangChange = translate.onLangChange.subscribe(() => {
      if (languagesIDS !== undefined) {
        this.currentLang = languagesIDS.find(lang => lang.acronym === this.translate.currentLang).id;

      } else {
        this.currentLang = undefined;
      }
    });
  }

  ngOnInit() {
    this.getInfoApresentation();
    this.getDocumentTypes();
  }

  ngOnDestroy() {
    if( this.onLangChange !== undefined ){
      this.onLangChange.unsubscribe();

    }
  }

  getFormItensBySteps(step) {
    //console.log(this.formItens.filter((item) => item.step === step));
    return this.formItens.filter((item) => item.step === step);
  }

  getInfoApresentation() {
    let id = parseInt(this.route.snapshot.paramMap.get("id"));
    return this.applicationsService
      .read(id)
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
        )
      .subscribe((val) => {

        this.application = val.data[0];
      
        this.otherExpenses_Education = this.application.other_expenses.filter((item) => item.expense_type === "EDUCATION");
        this.otherExpenses_FIXED = this.application.other_expenses.filter((item) => item.expense_type === "FIXED");

        if (this.application.documents && this.application.documents.length > 0) {
          console.log(this.currentLang);
          console.log(this.application.documents);
          this.application.documents.forEach ( (element: any) => {
              if(element.file !== null){
                this.filesOthers.push(element);
              }
          });
      }
      });
  }

  back() {
    this.router.navigate(["emergency-fund", "list-applications"]);
  }

  getHTML(value) {
    if (value) {
      value = value.replace("line-height: 1.17", "line-height: 0.17");
    }
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  showValueItemTypeFour(nameItem) {
    /*return this.application[nameItem[0]]
      .map((item) => item[nameItem[2]][nameItem[3]])
      .join(",");*/
      //console.log(nameItem);
      return '';
  }

  getDocumentTypes() {
    this.authService
      .getDocumentTypes()
      .pipe(first())
      .subscribe((data) => {
        this.documentTypes = data.data;
      });
  }

  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentTypes.find((x) => {
      return x.id == document_type_id.toString();
    });
    return document_type
      ? document_type.translations.find((x) => x.language_id == 3).description
      : "--";
  }


  validField(field, expectedValue){
    if (this.application[field] == expectedValue){
      return true;
    }else{
      return false;
    }
  }



  getSchool(id: number): OrganicUnitModel {
    if (this.schools) return this.schools.find((s) => s.id === id);
  }
  getCourse(id: number): CourseModel {
    if (this.coursesList) return this.coursesList.find((s) => s.id === id);
  }
  getCourseDegree(id: number): CourseDegreeModel {
    if (this.courseDegreesList)
      return this.courseDegreesList.find((s) => s.id === id);
  }

  getDegrees() {
    this.configurationService
      .courseDegrees()
      .pipe(first())
      .subscribe((arg) => {
        this.courseDegreesList = arg.data;
      });
  }

  getStudentSchedule(id) {
    switch (id) {
      case "DAYTIME":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.DAYTIME"
        );
        break;
      case "LABOR_POST":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_SCHEDULE.OTHERDAY"
        );
        break;
    }
  }
  getStudentRegime(id) {
    switch (id) {
      case "ORDINARY":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.ORDINARY"
        );
        break;
      case "STUDENT_WORKER":
        return this.translateService.instant(
          "EMERGENCY_FUND.APPLICATIONS.STEP_ONE.FORM_ELEMENTS.STUDENT_REGIME.STUDENT_WORKER"
        );
        break;
      case "OTHER":
        return this.application.student_regime_description;
        break;
    }
  }

  loadCurseDegree() {
    /*      this.configurationService
        .courses(this.aplicationForm.get("course_degree_id").value)
        .pipe(
          first()
        )
        .subscribe((arg) => {
          this.coursesList = arg.data;
        });
    */
  }
//item.valueDescription(application[item.inputName])


  getTranslate(item){
    return(item.find(i => i.language_id === this.currentLang).description);
  }

  loadOrganicUnits() {
    this.organicUnitsService
      .organicUnitsbySchools()
      .pipe(first())
      .subscribe((result) => {
        this.schools = result.data;
      });
  }
}
