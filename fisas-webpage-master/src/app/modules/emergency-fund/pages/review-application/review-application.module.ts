import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@fi-sas/webpage/shared/shared.module";
import { ReviewApplicationComponent } from "./review-application.component";
import { ReviewApplicationRoutingModule } from "./review-application-routing.module";
@NgModule({
  declarations: [ReviewApplicationComponent],
  imports: [CommonModule, SharedModule, ReviewApplicationRoutingModule],
})
export class ReviewApplicationModule {}
