import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { BehaviorSubject, Observable } from "rxjs";
import { ApplicantInfoModel } from "@fi-sas/webpage/modules/accommodation/models/applicant-info.model";
import { ApplicationStatusModel } from "@fi-sas/webpage/modules/accommodation/models/application-status.model";
import { ApplicationChangeStatusModel } from "@fi-sas/webpage/modules/accommodation/models/application-change-status.model";
import { HttpParams } from "@angular/common/http";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { PatrimonyCategoriesModel } from "../../accommodation/models/patrimony-categories.model";
import { ApplicationModel } from "../models/application.model";
import { IncomesModel } from "../models/incomes";
import { DocumentsModel } from "../models/documents";
import { SupportsModel } from "../models/supports";
import { ApplicationChangesModel } from "../models/application-changes.model";
import { ExpensesModel } from "../models/expenses";
import { intendedSupports } from "../models/intended-supports.model";

@Injectable({
  providedIn: "root",
})
export class ApplicationsService {
  constructor(
    private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService
  ) {}

  userApplicationIDSelected = new BehaviorSubject(null);

  list(offset: number, limit: number) {
    const params = new HttpParams();
    //    .append('user_id', this.authService.getUser().id.toString());
    return this.resourceService.list<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS"),
      {
         params: { limit: limit.toString() }
      }
    );
  }

  create(
    application: ApplicationModel
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS", {}),
      application
    );
  }

  update(
    id: number,
    application: ApplicationModel
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.update<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS_ID", { id }),
      application
    );
  }

  read(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS_ID", { id })
    );
  }

  applicationVerifyActive(): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS_VERIFY_ACTIVE", {})
    );
  }

  changeStatus(
    id: number,
    applicationChangeStatus: ApplicationChangeStatusModel
  ): Observable<Resource<ApplicationChangeStatusModel>> {
    return this.resourceService.create<ApplicationChangeStatusModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATION_CHANGE_STATUS", { id }),
      applicationChangeStatus
    );
  }

  changeStatusApplicationChanges(
    applicationChangeStatus: ApplicationChangeStatusModel
  ): Observable<Resource<ApplicationChangeStatusModel>> {
    return this.resourceService.create<ApplicationChangeStatusModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATION_CHANGE_CHANGE_STATUS", {}),
      applicationChangeStatus
    );
  }

  studentApprove(id: number) {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.STUDENT_APPROVE", { id }),
      {}
    );
  }

  studentCancel(id: number) {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.STUDENT_REJECT", { id }),
      {}
    );
  }

  studentOppose(id: number, complain: string) {
    return this.resourceService.create<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.STUDENT_OPPOSE", { id }),
      {
        notes: "Reclamação enviada por utilizador",
        application: {
          opposition_request: complain,
        },
      }
    );
  }

  applicantInfo(): Observable<Resource<ApplicantInfoModel>> {
    return this.resourceService.read<ApplicantInfoModel>(
      this.urlService.get("ACCOMMODATION.APPLICANT_INFO", {}),
      {}
    );
  }

  getIncomes() {
    return this.resourceService.list<IncomesModel>(
      this.urlService.get("EMERGENCY_FUND.INCOMES_LIST", {}),
      {}
    );
  }

  getDocuments() {
    return this.resourceService.list<DocumentsModel>(
      this.urlService.get("EMERGENCY_FUND.DOCUMENTS_LIST", { id: 2 }),
      { params: { document_category_id: "2" } }
    );
  }

  getSupports() {
    return this.resourceService.list<SupportsModel>(
      this.urlService.get("EMERGENCY_FUND.SUPPORT_LIST", {}),
      {}
    );
  }
  getIntendedSupports(id: number) {
    return this.resourceService.list<intendedSupports>(
      this.urlService.get("EMERGENCY_FUND.INTENDED_SUPPORT_LIST", {id}),
      {}
    );
  }

  getDocumentsTypes(id: number) {
    return this.resourceService.list<DocumentsModel>(
      this.urlService.get("EMERGENCY_FUND.DOCUMENTS_LIST", { id: id }),
      { params: { document_category_id: id.toString() } }
    );
  }

  changeFileApplication(
    data: any,
    id: number
  ): Observable<Resource<ApplicationModel>> {
    return this.resourceService.patch<ApplicationModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATIONS_ID", { id }),
      data
    );
  }

  addComplain(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN", {}),
      data
    );
  }

  addComplainReport(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN_REPORT", {}),
      data
    );
  }

  listExpenses(id: number) {
    return this.resourceService.list<ExpensesModel>(
      this.urlService.get("EMERGENCY_FUND.LIST_EXPENSES", { id })
    );
  }

  addExpenses(expenses: ExpensesModel): Observable<Resource<ExpensesModel>> {
      return this.resourceService.create<ExpensesModel>(
        this.urlService.get("EMERGENCY_FUND.ADD_EXPENSE", {}),
        expenses
      );
    }

  complainDoc(data: any){
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN_DOC" , {}),
      data
    );
  }

}
