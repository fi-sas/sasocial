import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { PatrimonyCategoriesModel } from "../../accommodation/models/patrimony-categories.model";
import {     } from "../models/application.model";
import { ApplicationChangesModel } from "../models/application-changes.model";

@Injectable({
  providedIn: "root",
})
export class ApplicationsChangesService {
  constructor(
    private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService
  ) {}

  userApplicationIDSelected = new BehaviorSubject(null);

  add(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_APPLICATION_CHANGE", {}),
      data
    );
  }

  addReport(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_APPLICATION_CHANGE_REPORT", {}),
      data
    );
  }

  getListById(id: number) {
    return this.resourceService.list<ApplicationChangesModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATION_CHANGE_LIST_ID", { id })
    );
  }


  getACById(id: number) {
    return this.resourceService.list<ApplicationChangesModel>(
      this.urlService.get("EMERGENCY_FUND.APPLICATION_CHANGE_REVIEW_ID", { id })
    );
  }

  addDoc(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_APPLICATION_CHANGE_DOCUMENT", {}),
      data
    );
  }

}
