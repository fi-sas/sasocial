import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { PatrimonyCategoriesModel } from "../../accommodation/models/patrimony-categories.model";
import {     } from "../models/application.model";
import { ComplainsModel } from "../models/complains.model";

@Injectable({
  providedIn: "root",
})
export class ComplationsService {
  constructor(
    private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService
  ) {}

  userApplicationIDSelected = new BehaviorSubject(null);

  add(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN", {}),
      data
    );
  }

  addReport(data: any) {
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN_REPORT", {}),
      data
    );
  }

  addDoc(data: any){
    return this.resourceService.create<any>(
      this.urlService.get("EMERGENCY_FUND.ADD_COMPLAIN_DOC" , {}),
      data
    );
  }

  getById(id: number): Observable<Resource<ComplainsModel>> {
    return this.resourceService.list<ComplainsModel>(
      this.urlService.get("EMERGENCY_FUND.GET_COMPLAIN_BY_APP_ID", { id })
    );
  }

}
