import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { PatrimonyCategoriesModel } from "../../accommodation/models/patrimony-categories.model";
import {     } from "../models/application.model";
import { ExpensesDocModel, ExpensesModel } from "../models/expenses";

@Injectable({
  providedIn: "root",
})
export class ExpensesService {
  constructor(
    private resourceService: FiResourceService,
    private authService: AuthService,
    private urlService: FiUrlService
  ) {}

    userApplicationIDSelected = new BehaviorSubject(null);

    list(id: number) {
        return this.resourceService.list<ExpensesModel>(
        this.urlService.get("EMERGENCY_FUND.LIST_EXPENSES", { id })
        );
    }

    get(id: number) {
      return this.resourceService.list<ExpensesModel>(
      this.urlService.get("EMERGENCY_FUND.GET_EXPENSES", { id })
      );
  }

    add(expenses: ExpensesModel): Observable<Resource<ExpensesModel>> {
      return this.resourceService.create<ExpensesModel>(
        this.urlService.get("EMERGENCY_FUND.EXPENSES", {}),
        expenses
      );
    }

    addDoc(expenses: ExpensesDocModel): Observable<Resource<ExpensesDocModel>> {
      console.log(expenses);
        return this.resourceService.create<ExpensesDocModel>(
          this.urlService.get("EMERGENCY_FUND.ADD_EXPENSE_DOC", {}),
          expenses
        );
      }


}