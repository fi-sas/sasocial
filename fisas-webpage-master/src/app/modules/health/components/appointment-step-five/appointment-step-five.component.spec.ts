import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentStepFiveComponent } from './appointment-step-five.component';

describe('AppointmentStepFiveComponent', () => {
  let component: AppointmentStepFiveComponent;
  let fixture: ComponentFixture<AppointmentStepFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentStepFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentStepFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
