import { Component, Input, OnInit } from '@angular/core';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { DoctorModel } from '../../models/doctor.model';
import { PriceVariationsModel } from '../../models/prive-variation.model';
import { SpecialtyModel } from '../../models/specialty.model';

@Component({
  selector: 'app-appointment-step-five',
  templateUrl: './appointment-step-five.component.html',
  styleUrls: ['./appointment-step-five.component.less']
})
export class AppointmentStepFiveComponent implements OnInit {

  @Input() steps = [];
  @Input() arrayValues: {
    specialty: SpecialtyModel;
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: PriceVariationsModel[];
    loggedUser : UserModel;
    } = null;

  specialtyFound: SpecialtyModel;
  documentTypes: DocumentTypeModel[] = [];
  
  constructor(
    private authService: AuthService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.getDocumentTypes();
    this.specialtyFound = this.arrayValues.specialty;
  }

  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

  findDoctor(id: number){
    let doctorFound = null;
    if (this.arrayValues !== null) {
      doctorFound = this.arrayValues.doctors.find(
        (value) => value.id === id
      );
      if (
        doctorFound !== undefined &&
        hasOwnProperty(doctorFound, 'user')
      ) {
        return doctorFound.user.name;
      }
    }
    return id;
  }

  findAppointmentType(id: number){
    let appointmentTypeFound = null;
    if (this.arrayValues !== null) {
      appointmentTypeFound = this.arrayValues.appointmentTypes.find(
        (value) => value.id === id
      );
      return appointmentTypeFound;
    }
    return id;
  }

  findTariff(id: number){
    let tariffFound = null;
    if (this.arrayValues !== null) {
      tariffFound = this.arrayValues.tariffs.find(
        (value) => value.id === id
      );
      if (tariffFound !== undefined) {
        return tariffFound.description;
      }
    }
    return id;
  }

  findTax(id: number){
    const priceVariation: PriceVariationsModel = this.getTariff(id);
    if(priceVariation){
      return priceVariation.vat.name.replace("IVA", "");
    }
    return id;
  }

  findDocumentType(id: number){
    if(this.documentTypes.length > 0){
      return this.documentTypes.find(document => document.id === id).translations;
    }
  }

  getTariff(id: number){
    let tariffFound: PriceVariationsModel = null;
    if (this.arrayValues !== null) {
      tariffFound = this.arrayValues.specialty.price_variations.find(
        (value) => value.tariff_id === id
      );
      if (tariffFound !== undefined) {
        return tariffFound;
      }
    }
  }

}
