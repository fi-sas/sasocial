import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { finalize, first } from 'rxjs/operators';

import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { DoctorModel } from '../../models/doctor.model';
import { SpecialtiesFormModel, SpecialtyModel } from '../../models/specialty.model';
import { TariffModel } from '../../models/tariff.model';

@Component({
  selector: 'app-appointment-step-four',
  templateUrl: './appointment-step-four.component.html',
  styleUrls: ['./appointment-step-four.component.less']
})
export class AppointmentStepFourComponent implements OnInit {

  @Input() payment: SpecialtiesFormModel = null;

  @Input()  arrayValues: {
    specialty: SpecialtyModel;
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: TariffModel[];
    loggedUser : UserModel;
  } = null;

 
  loggedUser: UserModel = null;
  tariffsLoading = false;
  tariffs: TariffModel[] = [];
  filteredTariffs: TariffModel[] = [];

  selectedSpecialty: SpecialtyModel = null;

  paymentForm = new FormGroup({
    payment_type:new FormControl(null, [Validators.required]),
    tariff_id: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required]),
  })

  get f() { return this.paymentForm.controls; }
  submitted = false;

  constructor() { }

  ngOnInit() {
    this.loggedUser = this.arrayValues.loggedUser;
    this.tariffs = this.arrayValues.tariffs;
    this.selectedSpecialty = this.arrayValues.specialty;
    this.paymentForm.patchValue({
        payment_type: this.selectedSpecialty.payment_type
    });

    this.getTariffs();

    if (this.payment !== null) {
      this.paymentForm.patchValue({
        ...this.payment,
      });
    }
  }

  getTariffs(){
   /* this.authService.getUserInfoByToken().pipe(first(), finalize(() => this.tariffsLoading = false)).subscribe(res => {
      this.loggedUser = res.data[0];
      
    })*/

    this.filteredTariffs = this.tariffs.filter(tariff => tariff.profile_ids.includes(this.loggedUser.profile_id) && this.selectedSpecialty.price_variations.find(price => price.tariff_id === tariff.id))
      
    
  }

  getPriceInformation(){
    const tariffSelected = this.paymentForm.get('tariff_id').value;
    if(tariffSelected){
      this.paymentForm.patchValue({
        price: this.selectedSpecialty.price_variations.find(price => price.tariff_id === tariffSelected).price
      })
    }
  }

  getInputError(field: string) {
    return this.paymentForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.paymentForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.paymentForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.paymentForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.paymentForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.paymentForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.paymentForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.paymentForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  submitForm(): any {
    this.submitted = true;
    if (this.paymentForm.valid) {
      this.submitted = false;
      this.paymentForm.updateValueAndValidity();
      return this.paymentForm.value;
    }
    for (const i in this.paymentForm.controls) {
      if (this.paymentForm.controls[i]) {
        this.paymentForm.controls[i].markAsDirty();
        this.paymentForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

}
