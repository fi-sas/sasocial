import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { DoctorModel } from '../../models/doctor.model';
import { SpecialtiesFormModel, SpecialtyModel } from '../../models/specialty.model';
import { AppointmentTypeService } from '../../services/appointment-type.service';
import { DoctorsService } from '../../services/doctors.service';
import { SpecialtiesService } from '../../services/specialties.service';
import * as moment from 'moment';
import { AppointmentEventModel } from '../../models/appointment.model';
import { TranslateService } from '@ngx-translate/core';
import { TariffModel } from '../../models/tariff.model';
import { TariffService } from '../../services/tariff.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';

@Component({
  selector: 'app-appointment-step-one',
  templateUrl: './appointment-step-one.component.html',
  styleUrls: ['./appointment-step-one.component.less']
})
export class AppointmentStepOneComponent implements OnInit, AfterContentChecked  {

  @Input() specialtyInfo: SpecialtiesFormModel = null;
  
  loading = false;
  specialtySelected: SpecialtyModel;
  minuteStep: number = 0;

  doctorsLoading = false;
  doctors: DoctorModel[] = [];
  doctorsFiltered: DoctorModel[] = [];
  doctorSelected: DoctorModel;
  
  appointmentTypesLoading = false;
  appointmentTypesList: AppointmentTypeModel[] = [];
  appointmentTypeIdSelected: number;

  tariffs: TariffModel[] = [];

  loadingAppointments = false;
  appointments: AppointmentEventModel [] = [];
  appointmentsDoctor: AppointmentEventModel [] = [];
  appointmentSelected: AppointmentEventModel;

  loggedUser: UserModel = null;

  isEdit = false;
  submitted = false;
  fifteenDays = [];
  dayCurrent: Date = new Date();

  specialtyForm = new FormGroup({
    doctor_id: new FormControl(null, Validators.required),
    specialty_id: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
    appointment_type_id: new FormControl(null, Validators.required),
  });
  
  constructor(
    private specialtiesService: SpecialtiesService,
    private doctorsService: DoctorsService,
    private appointmentTypeService: AppointmentTypeService,
    private tariffsService: TariffService,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    public translate: TranslateService,
    private authService: AuthService,
  ) { }

  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      this.loggedUser = res.data[0];
    })
    if (this.specialtyInfo && this.specialtyInfo.specialty_id) {
      this.dayCurrent = new Date(this.specialtyInfo.date);
      this.getDetail(this.specialtyInfo.specialty_id);
      this.specialtyForm.patchValue({
        ...this.specialtyInfo,
        start_hour: moment(this.specialtyInfo.start_hour, "HH:mm").toDate(),

      });
     // this.getDoctorSchedule();
      this.appointmentTypeIdSelected = this.specialtyInfo.appointment_type_id;
      const appointmentId = moment(this.specialtyInfo.date).format("YYYY-MM-DD") + "T" + moment(this.specialtyInfo.start_hour).format("HH:mm:ss");
      this.appointmentSelected = this.appointmentsDoctor.find( appointment => appointment.id === appointmentId);
    
    }else{
      this.route.params.subscribe(async (params) => {
        if ( params['id']) {
            this.getDetail( params['id']);
        }
      });
    }
    this.setDays();
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    const maxDate = moment().add(15, 'day');

    while (currentDate.isBefore(maxDate)) {
      this.fifteenDays.push(indexDate.toDate());
      indexDate = indexDate.add(1, 'day');
    }
    this.daySelected(this.dayCurrent);
  }

  daySelected(day) {
    this.dayCurrent = day;
    if(this.appointmentsDoctor.length > 0){
      this.appointments = this.appointmentsDoctor.filter(appointment => moment(moment(this.dayCurrent).format("YYYY-MM-DD")).isBetween(moment(appointment.start_date).format("YYYY-MM-DD"), moment(appointment.end_date).format("YYYY-MM-DD"), null, '[]') )
    }
  }

  appointmentSelect(appointment) {
    if( appointment.is_available){
      this.appointmentSelected = this.appointmentsDoctor.find(a => a.id === appointment.id);
    }
  }

  appointmentTypeSelect(appointmentType){
    if(!this.isAppointmentTypeDisabled(appointmentType.id)){
      this.appointmentTypeIdSelected = this.appointmentTypesList.find(t => t.id === appointmentType.id).id;
    }
  }
  
  getDetail(id){
    this.loading = true;
      this.specialtiesService.getSpecialtyById(id).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
        this.specialtySelected = data.data[0];
        this.minuteStep = this.specialtySelected.avarage_time;
        this.getAllDoctors();
        this.getAppointmentTypes();
        this.getTariffs();
      })
  }

  getInputError(field: string) {
    return this.specialtyForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.specialtyForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.specialtyForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.specialtyForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.specialtyForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.specialtyForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.specialtyForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.specialtyForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  getAllDoctors(){
    this.doctorsLoading = true;
    this.doctorsService.list().pipe(
      first(),
      finalize(() => this.doctorsLoading = false)).subscribe(results => {
        this.doctorsFiltered = results.data.filter(doctor => this.specialtySelected.doctors.find( d => d.id === doctor.id));
      });
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list().pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypesList = results.data;
      });
  }

  getTariffs(){
    this.tariffsService.list().pipe(
      first()).subscribe(results => {
        this.tariffs = results.data;
      });
  }

  isDoctorDisabled(id){
    return this.doctorsFiltered.find( doctor => doctor.id === id && this.loggedUser.id === doctor.id) ? true : false
  }

  isAppointmentTypeDisabled(value:any){
    if(this.specialtySelected){
      return this.specialtySelected.appointment_types.find(d => d.id === value) ? false : true
    }
  }

  disableDate = (endDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(endDate).isSameOrBefore(currentDate);
  }

  getDoctorSchedule(){
    this.doctorSelected = this.doctorsFiltered.find(d => d.id ===  this.specialtyForm.get('doctor_id').value);
    if(this.doctorSelected && this.specialtySelected && !this.specialtySelected.is_external){
        this.clearFormValues();
        this.loadingAppointments = true;
        this.appointmentsDoctor = [];
        this.appointments = [];
        this.specialtiesService.getScheduleDoctor(this.specialtySelected.id, this.doctorSelected.id).pipe(
          first(),
          finalize(() => this.loadingAppointments = false)).subscribe(results => {
            this.appointmentsDoctor = results.data;
            this.daySelected(this.dayCurrent);
          });
    }
    this.clearFormValues();
  }

  clearFormValues(){
    this.specialtyForm.patchValue({
      date: null,
      start_hour: null,
      end_hour: null,
    })
  }

  submitForm(): any {
    this.submitted = true;
    this.specialtyForm.patchValue({
      specialty_id: this.specialtySelected.id,
      date: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("YYYY-MM-DD") : this.specialtyForm.controls.date.value ? moment(this.specialtyForm.controls.date.value).format("YYYY-MM-DD") : null,
      start_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("HH:mm") :  this.specialtyForm.controls.start_hour.value ? moment(this.specialtyForm.controls.start_hour.value).format("HH:mm") : null,
      end_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).add(this.specialtySelected.avarage_time, "minutes").format("HH:mm") : this.specialtyForm.controls.start_hour.value ? moment(this.specialtyForm.controls.start_hour.value).add(this.specialtySelected.avarage_time, "minutes").format("HH:mm"): null,
      appointment_type_id: this.appointmentTypeIdSelected ? this.appointmentTypeIdSelected : null,
    });
    if (this.specialtyForm.valid) {
      this.submitted = false;
      this.specialtyForm.updateValueAndValidity();
      return this.specialtyForm.value;
    }
    for (const i in this.specialtyForm.controls) {
      if (this.specialtyForm.controls[i]) {
        this.specialtyForm.controls[i].markAsDirty();
        this.specialtyForm.controls[i].updateValueAndValidity();
      }
    }

    return null;
  }

  getArrayValues(): {
    specialty: SpecialtyModel;
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: TariffModel[];
    loggedUser : UserModel;
  } {
    return {
      specialty: this.specialtySelected,
      doctors: this.doctors,
      appointmentTypes: this.appointmentTypesList,
      tariffs: this.tariffs,
      loggedUser: this.loggedUser
    };
  }
}