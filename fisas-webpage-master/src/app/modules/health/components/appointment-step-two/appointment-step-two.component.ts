import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { SpecialtiesFormModel, SpecialtyModel } from '../../models/specialty.model';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { nationalities } from "@fi-sas/webpage/shared/data/nationalities";
import { DoctorModel } from '../../models/doctor.model';
import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { TariffModel } from '../../models/tariff.model';

@Component({
  selector: 'app-appointment-step-two',
  templateUrl: './appointment-step-two.component.html',
  styleUrls: ['./appointment-step-two.component.less']
})
export class AppointmentStepTwoComponent implements OnInit {

  @Input() userInformation: SpecialtiesFormModel = null;
  @Input()  arrayValues: {
    specialty: SpecialtyModel;
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: TariffModel[];
    loggedUser : UserModel;
  } = null;

  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
  '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
  '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
  '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
  '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  submitted = false;
  loggedUser: UserModel = null;

  
  courses: CourseModel[] = [];
  filtered_courses: CourseModel[] = [];
  loading_courses = false;

  loading_schools = false;
  schools: OrganicUnitModel[] = [];
  loading_course_degrees = false;

  nationalities = nationalities;

  course_degrees: CourseDegreeModel[] = [];
  documentTypes: DocumentTypeModel[] = [];
  
  userForm = new FormGroup({
    user_id: new FormControl(null, [Validators.required]),
    name: new FormControl(null, [trimValidation]),
    student_number: new FormControl(null),
    niss: new FormControl(null),
    document_type_id: new FormControl(null),
    identification_number: new FormControl(null),
    nif: new FormControl(null),
    address: new FormControl(null, [trimValidation]),
    postal_code: new FormControl('', [Validators.pattern('\\d{4}-\\d{3}$')]),
    city: new FormControl(null, [trimValidation]),
    nationality: new FormControl(null),
    email: new FormControl('', [trimValidation, Validators.pattern(this.emailRegex)]),
    mobile_phone_number: new FormControl(null),
    telephone_number: new FormControl(null),
    course_id: new FormControl(null),
    organic_unit_id: new FormControl(null),
    course_degree_id: new FormControl(null),
    course_year: new FormControl(null),
    birth_date: new FormControl(null),
    has_scholarship: new FormControl(null),
  })

  constructor(
    private authService: AuthService,
    private configurationsService: ConfigurationsService,
    private organicUnitsService: OrganicUnitsService,
  ) { }

  ngOnInit() {
    if(this.arrayValues.loggedUser){
      this.loggedUser = this.arrayValues.loggedUser;
      this.userForm.patchValue({
        user_id: this.loggedUser.id,
        name: this.loggedUser.name,
        student_number: this.loggedUser.student_number,
        document_type_id: this.loggedUser.document_type_id,
        identification_number: this.loggedUser.identification,
        nif: this.loggedUser.tin,
        address: this.loggedUser.address,
        postal_code: this.loggedUser.postal_code,
        city: this.loggedUser.city,
        nationality: this.loggedUser.nationality,
        email: this.loggedUser.email,
        mobile_phone_number: this.loggedUser.phone,
        course_id: this.loggedUser.course_id,
        organic_unit_id: this.loggedUser.organic_unit_id,
        course_year: this.loggedUser.course_year,
        birth_date: this.loggedUser.birth_date 
      });
    }
    /*this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      this.loggedUser = res.data[0];

      //this.filterCourses();
    });*/
    this.loadCourseDegrees();
    this.loadOrganicUnits();
    this.getDocumentTypes();
  }

  getInputError(field: string) {
    return this.userForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.userForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.userForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.userForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.userForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.userForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.userForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.userForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }


  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

  loadCourseDegrees() {
    this.loading_course_degrees = true;
    this.configurationsService.courseDegrees().pipe(
      first(),
      finalize(() => this.loading_course_degrees = false)
    ).subscribe(result => {
      this.course_degrees = result.data;
    });
  }

  loadOrganicUnits() {
    this.loading_schools = true;
    this.organicUnitsService.organicUnitsbySchools().pipe(
      first(),
      finalize(() => this.loading_schools = false)
    ).subscribe(result => {
      this.schools = result.data;
    });
  }

  filterDegrees() {
    this.userForm.get('course_id').value ? this.userForm.get('course_id').setValue(null) : '';
    this.userForm.get('course_degree_id').value ? this.userForm.get('course_degree_id').setValue(null) : '';
  }

  filterCourses() {
    /*this.configurationsService.courses(this.userForm.get("course_degree_id").value, this.userForm.get("organic_unit_id").value).pipe(
      first(),
      finalize(() => this.loading_courses = false)
    ).subscribe(result => {
      this.courses = result.data;
      this.filtered_courses = this.courses;
    });*/
    this.configurationsService.courses().pipe(
      first(),
      finalize(() => this.loading_courses = false)
    ).subscribe(result => {
      this.courses = result.data;
      this.filtered_courses = this.courses;
    });
  }

  submitForm(): any {
    this.submitted = true;
    if (this.userForm.valid) {
      this.submitted = false;
      this.userForm.updateValueAndValidity();
      return this.userForm.value;
    }
    for (const i in this.userForm.controls) {
      if (this.userForm.controls[i]) {
        this.userForm.controls[i].markAsDirty();
        this.userForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }
}
