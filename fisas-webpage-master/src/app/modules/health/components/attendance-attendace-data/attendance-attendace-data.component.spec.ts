import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceAttendaceDataComponent } from './attendance-attendace-data.component';

describe('AttendanceAttendaceDataComponent', () => {
  let component: AttendanceAttendaceDataComponent;
  let fixture: ComponentFixture<AttendanceAttendaceDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceAttendaceDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceAttendaceDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
