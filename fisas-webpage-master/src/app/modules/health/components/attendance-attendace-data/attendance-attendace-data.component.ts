import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { AttendanceFormModel, AttendanceModel } from '../../models/attendance.model';

@Component({
  selector: 'app-attendance-attendace-data',
  templateUrl: './attendance-attendace-data.component.html',
  styleUrls: ['./attendance-attendace-data.component.less']
})
export class AttendanceAttendaceDataComponent implements OnInit {

  @Input() attendance: AttendanceModel = null;
  @Input() attendanceDataInfo: AttendanceFormModel = null;

  attendanceDataForm = new FormGroup({
    observations: new FormControl(null, [trimValidation]),
    attachments_ids: new FormControl(null, []),
  });

  get f() { return this.attendanceDataForm.controls; }

  submitted = false;
  file_ids = [];
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];
  
  constructor() { }

  ngOnInit() {
  }

  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }

  submitForm(){
    this.submitted = true;
    this.attendanceDataForm.get("attachments_ids").setValue(this.file_ids);
    if (this.attendanceDataForm.valid) {
      this.submitted = false;
      this.attendanceDataForm.updateValueAndValidity();
      return this.attendanceDataForm.value;
    }
    for (const i in this.attendanceDataForm.controls) {
      if (this.attendanceDataForm.controls[i]) {
        this.attendanceDataForm.controls[i].markAsDirty();
        this.attendanceDataForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

}
