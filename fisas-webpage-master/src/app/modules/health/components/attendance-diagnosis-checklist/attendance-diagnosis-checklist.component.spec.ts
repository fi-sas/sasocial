import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceDiagnosisChecklistComponent } from './attendance-diagnosis-checklist.component';

describe('AttendanceDiagnosisChecklistComponent', () => {
  let component: AttendanceDiagnosisChecklistComponent;
  let fixture: ComponentFixture<AttendanceDiagnosisChecklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceDiagnosisChecklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceDiagnosisChecklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
