import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { AttendanceFormModel, AttendanceModel, ChecklistFormModel, QuestionModel } from '../../models/attendance.model';

@Component({
  selector: 'app-attendance-diagnosis-checklist',
  templateUrl: './attendance-diagnosis-checklist.component.html',
  styleUrls: ['./attendance-diagnosis-checklist.component.less']
})
export class AttendanceDiagnosisChecklistComponent implements OnInit {

  @Input() attendance: AttendanceModel = null;
  @Input() checklistInfo: AttendanceFormModel = null;

  checklistForm = new FormGroup({
    notes: new FormControl(null, [trimValidation]),
    attachments_ids: new FormControl(null, []),
    answers: new FormArray([]),
  });

  get f() { return this.checklistForm.controls; }
  answers = this.checklistForm.get('answers') as FormArray;

  submitted = false;
  file_ids = [];
  filterFileTypes = ["application/pdf", "image/png", "image/jpeg"];

  constructor() { }

  ngOnInit() {
    if(this.attendance && this.attendance.checklist){
      this.attendance.checklist.map((checklist) => {
        this.addChecklistLine(checklist.question, checklist.answer_type);
      });
    }
  }

  onFileDeleted(fileId) {
    this.file_ids = this.file_ids.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids.push(fileId);
  }

  addChecklistLine(question: string, answer_type: string){
    const checklist = this.checklistForm.controls.answers as FormArray;
    checklist.push(
      new FormGroup({
        question: new FormControl(question, [Validators.required,trimValidation]),
        answer_type: new FormControl(answer_type),
        answer: new FormControl(null, [Validators.required]),
      })
    );
  }

  submitForm(){
    this.submitted = true;
    if (this.checklistForm.valid) {
      this.submitted = false;
      this.checklistForm.updateValueAndValidity();
      return this.fillChecklistObject();
    }
    for (const i in this.checklistForm.controls) {
      if (this.checklistForm.controls[i]) {
        this.checklistForm.controls[i].markAsDirty();
        this.checklistForm.controls[i].updateValueAndValidity();
      }
    }
    return null;
  }

  fillChecklistObject(){
    const checklist = [];
    for(const formGroup of this.answers.controls){
      let answerModel = new QuestionModel()
      const answer = formGroup.get('answer');
      if(formGroup && answer.value){
        const value = answer.value;
        answerModel.question = formGroup.get('question').value;
        answerModel.answer = value.toString();
        checklist.push(answerModel);
      }else{
        formGroup.markAsPristine();
      }
    }

    const checkListResponse = new ChecklistFormModel();
    checkListResponse.attachments_ids = this.file_ids;
    checkListResponse.notes =  this.checklistForm.controls.notes.value;
    checkListResponse.answers = checklist;

    return checkListResponse;
  }
}
