import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceDiagnosisHistoricComponent } from './attendance-diagnosis-historic.component';

describe('AttendanceDiagnosisHistoricComponent', () => {
  let component: AttendanceDiagnosisHistoricComponent;
  let fixture: ComponentFixture<AttendanceDiagnosisHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceDiagnosisHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceDiagnosisHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
