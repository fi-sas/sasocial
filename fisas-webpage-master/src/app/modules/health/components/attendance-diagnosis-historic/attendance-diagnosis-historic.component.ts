import { Component, Input, OnInit } from '@angular/core';
import { AttendanceModel } from '../../models/attendance.model';

@Component({
  selector: 'app-attendance-diagnosis-historic',
  templateUrl: './attendance-diagnosis-historic.component.html',
  styleUrls: ['./attendance-diagnosis-historic.component.less']
})
export class AttendanceDiagnosisHistoricComponent implements OnInit {

  @Input() attendance: AttendanceModel = null;
  
  constructor() { }

  ngOnInit() {}

}
