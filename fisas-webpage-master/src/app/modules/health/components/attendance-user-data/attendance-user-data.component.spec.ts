import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceUserDataComponent } from './attendance-user-data.component';

describe('AttendanceUserDataComponent', () => {
  let component: AttendanceUserDataComponent;
  let fixture: ComponentFixture<AttendanceUserDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceUserDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceUserDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
