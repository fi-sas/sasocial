import { Component, Input, OnInit } from '@angular/core';
import { AppointmentModel } from '../../models/appointment.model';

@Component({
  selector: 'app-attendance-user-data',
  templateUrl: './attendance-user-data.component.html',
  styleUrls: ['./attendance-user-data.component.less']
})
export class AttendanceUserDataComponent implements OnInit {

  @Input() userInformation: AppointmentModel = null;
  
  constructor() { }

  ngOnInit() {
  }

}
