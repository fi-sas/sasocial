import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { AppointmentModel } from '../../models/appointment.model';
import { SpecialtyModel } from '../../models/specialty.model';

@Component({
  selector: 'app-buy-modal-box',
  templateUrl: './buy-modal-box.component.html',
  styleUrls: ['./buy-modal-box.component.less']
})
export class BuyModalBoxComponent implements OnInit {

  @Input() appointment: AppointmentModel;
  @Input() specialty: SpecialtyModel;

  constructor(
    private modal: NzModalRef,
  ) { }

  ngOnInit() {
  }

  getAppointmentType(appointment_type_id){
    if (this.specialty !== null) {
      return this.specialty.appointment_types.find(
        (value) => value.id === appointment_type_id
      );
    }
    return appointment_type_id;
  }
  
  confirmAppointment(){
    this.modal.close(true);
  }

  handleCancel(){
    this.modal.close(false);
  }
}
