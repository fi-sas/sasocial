import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardAppointmentHistoricComponent } from './card-appointment-historic.component';

describe('CardAppointmentHistoricComponent', () => {
  let component: CardAppointmentHistoricComponent;
  let fixture: ComponentFixture<CardAppointmentHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardAppointmentHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAppointmentHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
