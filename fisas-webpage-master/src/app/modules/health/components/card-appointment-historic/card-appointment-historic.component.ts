import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { AppointmentHistoricResponseModel, AppointmentModel, ConfigStatus } from '../../models/appointment.model';
import { AppointmentsService } from '../../services/appointments.service';

@Component({
  selector: 'app-card-appointment-historic',
  templateUrl: './card-appointment-historic.component.html',
  styleUrls: ['./card-appointment-historic.component.less']
})
export class CardAppointmentHistoricComponent implements OnInit {

  @Input() appointment: AppointmentModel;
  @Output() refresh = new EventEmitter<boolean>();
  appointmentHistoric: AppointmentHistoricResponseModel;

  readonly configStatus = ConfigStatus;
  infoModal: NzModalRef;
  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  appointment_id: number;
  isLoading= false;
  modalDetail = false;

  complaintForm = new FormGroup({
    observation: new FormControl(null, [Validators.required]),
    files_ids: new FormControl([]),
  });

  get f() { return this.complaintForm.controls; }
  submitted = false;
  file_ids_array = [];
  
  constructor(
    private modalService: NzModalService,
    private translateService: TranslateService,
    private appointmentsService: AppointmentsService,
    private uiService: UiService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onFileDeleted(fileId) {
    this.file_ids_array = this.file_ids_array.filter((f) => f !== fileId);
  }

  onFileAdded(fileId) {
    this.file_ids_array.push(fileId);
  }

  complaint(id: number,  tplContent: TemplateRef<{}>, $event: MouseEvent){
    $event.stopPropagation();
    this.appointment_id = id;
    this.infoModal = this.modalService.create({
      nzTitle: null,
      nzContent: tplContent,
      nzFooter: null,
      nzWidth: 350,
      nzWrapClassName: 'vertical-center-modal',
    });
  }

  close() {
    this.complaintForm.reset();
    this.infoModal.close();
  }

  getInputError(inputName: string): string {
    if (this.complaintForm.get(inputName).errors.required || this.complaintForm.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  onSubmit() {
    this.isLoading = true;
    if (this.complaintForm.valid) {
      this.complaintForm.get("files_ids").setValue(this.file_ids_array);
      this.appointmentsService.createComplaint(this.appointment_id, this.complaintForm.value).pipe(first(),finalize(() => this.isLoading = false))
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VOLUNTEERING.GENERAL_COMPLAIN.SUBMIT_SUCCESS')
        );
        this.close();
      });
    }
  }

  openHistoric($event: MouseEvent) {
    this.isLoading = true
    $event.stopPropagation();
    this.appointmentHistoric = null;
    if(this.appointment){
      this.appointmentsService.getHistoric(this.appointment.id).pipe(first(), finalize(()=>this.isLoading = false)).subscribe((result) => {
        this.appointmentHistoric = result.data[0];
        this.modalDetail = true;
      });
    }
  }

  openDetail(id: number){
    this.router.navigateByUrl('health/appointments/detail/'+id);
  }

  isPair(val) {
    return (val % 2 === 0);
  }
}
