import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalService } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { AppointmentHistoricResponseModel, AppointmentModel, ConfigStatus } from '../../models/appointment.model';
import { AppointmentsService } from '../../services/appointments.service';

@Component({
  selector: 'app-card-appointment',
  templateUrl: './card-appointment.component.html',
  styleUrls: ['./card-appointment.component.less']
})
export class CardAppointmentComponent implements OnInit {

  @Input() appointment: AppointmentModel;
  @Output() refresh = new EventEmitter<boolean>();
  appointmentHistoric: AppointmentHistoricResponseModel;
  modalDetail = false;

  readonly configStatus = ConfigStatus;
  
  constructor(
    private translateService: TranslateService,
    private router: Router,
    private uiService: UiService,
    private appointmentsService: AppointmentsService,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
  }

  cancelAppointment(id){
    this.modalService.confirm({
      nzTitle: this.translateService.instant('HEALTH.APPOINTMENTS_TAB.LIST_APPOINTMENTS.CANCEL_TEXT_MODAL'),
      nzOkText: this.translateService.instant('HEALTH.APPOINTMENT_FORM.YES'),
      nzCancelText: this.translateService.instant('HEALTH.APPOINTMENT_FORM.NO'),
      nzOnOk: () => {
        this.appointmentsService.cancel(id).pipe(first()).subscribe(()=>{
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('HEALTH.APPOINTMENTS_TAB.LIST_APPOINTMENTS.APPOINTMENT_CANCELED_TEXT')
        );
          this.refresh.emit(true);
        });
      }});
  }

  isPair(val) {
    return (val % 2 === 0);
  }

  openDetail(id: number){
    this.router.navigateByUrl('health/appointments/detail/'+id);
  }

  openHistoric($event: MouseEvent) {
    $event.stopPropagation();
    this.appointmentHistoric = null;
    if(this.appointment){
      this.appointmentsService.getHistoric(this.appointment.id).subscribe((result) => {
        this.appointmentHistoric = result.data[0];
        this.modalDetail = true;
      });
    }
  }

  getCalendarExport(id: number){
    this.appointmentsService.getAppointmentById(id).subscribe(responseAppointment => {
      const appointment = responseAppointment.data[0];
      if(appointment){
        let filename = appointment.specialty.translations[0].name + "_" + appointment.name + "_" + moment(appointment.date).format("YYYY-MM-DD") + "_"+ moment(appointment.start_hour, "HH:mm:ss") +".ics";
        this.appointmentsService.generateCalendarExport(id, filename).subscribe(()=>{
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('HEALTH.APPOINTMENTS_TAB.CALENDAR.MESSAGE_OK')
        );
      })
    };
  });
}

}
