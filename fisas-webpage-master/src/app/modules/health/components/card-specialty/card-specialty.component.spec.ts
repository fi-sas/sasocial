import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSpecialtyComponent } from './card-specialty.component';

describe('CardSpecialtyComponent', () => {
  let component: CardSpecialtyComponent;
  let fixture: ComponentFixture<CardSpecialtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSpecialtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
