import { Component, Input } from '@angular/core';
import { SpecialtyModel, ConfigStatus } from '../../models/specialty.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-card-specialty',
  templateUrl: './card-specialty.component.html',
  styleUrls: ['./card-specialty.component.less']
})
export class CardSpecialtyComponent {

  @Input() specialty: SpecialtyModel = null;
  configStatus = ConfigStatus;
  userId: number = 0;

  constructor(public translate: TranslateService) { }

}
