import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeAppointmentModalComponent } from './change-appointment-modal.component';

describe('ChangeAppointmentModalComponent', () => {
  let component: ChangeAppointmentModalComponent;
  let fixture: ComponentFixture<ChangeAppointmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeAppointmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeAppointmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
