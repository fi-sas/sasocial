import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize, first } from 'rxjs/operators';
import { AppointmentEventModel, AppointmentModel } from '../../models/appointment.model';
import { SpecialtiesService } from '../../services/specialties.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef } from 'ng-zorro-antd';
import { AppointmentsService } from '../../services/appointments.service';

@Component({
  selector: 'app-change-appointment-modal',
  templateUrl: './change-appointment-modal.component.html',
  styleUrls: ['./change-appointment-modal.component.less']
})
export class ChangeAppointmentModalComponent implements OnInit, AfterContentChecked{

  @Input() appointment: AppointmentModel;
  
  dayCurrent: Date = new Date();
  fifteenDays = [];
  minuteStep: number = 0;

  loadingAppointments = false;
  submitted = false;
  appointments: AppointmentEventModel [] = [];
  appointmentsDoctor: AppointmentEventModel [] = [];
  appointmentSelected: AppointmentEventModel;

  changeAppointmentForm = new FormGroup({
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
    observations: new FormControl(null, Validators.required),
  });
  
  constructor(
    private specialtiesService: SpecialtiesService,
    public translate: TranslateService,
    private changeDetector: ChangeDetectorRef,
    private modal: NzModalRef,
    private appointmentsService: AppointmentsService,
  ) { }

  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.setDays();
    this.getDoctorAppointments();
    this.minuteStep = this.appointment.specialty.avarage_time;
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    const maxDate = moment().add(15, 'day');

    while (currentDate.isBefore(maxDate)) {
      this.fifteenDays.push(indexDate.toDate());
      indexDate = indexDate.add(1, 'day');
    }
    this.daySelected(this.dayCurrent);
  }

  daySelected(day) {
    this.dayCurrent = day;
    if(this.appointmentsDoctor.length > 0){
      this.appointments = this.appointmentsDoctor.filter(appointment => moment(moment(this.dayCurrent).format("YYYY-MM-DD")).isBetween(moment(appointment.start_date).format("YYYY-MM-DD"), moment(appointment.end_date).format("YYYY-MM-DD"), null, '[]') )
    }
  }

  getDoctorAppointments(){
    if(this.appointment && !this.appointment.specialty.is_external){
      this.loadingAppointments = true;
      this.specialtiesService.getScheduleDoctor(this.appointment.specialty_id, this.appointment.doctor_id).pipe(
        first(),
        finalize(() => this.loadingAppointments = false)).subscribe(results => {
          this.appointmentsDoctor = results.data;
          this.daySelected(this.dayCurrent);
        });
    }
  }


  getInputError(field: string) {
    return this.changeAppointmentForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.changeAppointmentForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.changeAppointmentForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.changeAppointmentForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.changeAppointmentForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.changeAppointmentForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.changeAppointmentForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.changeAppointmentForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  onSubmit(){
    this.submitted = true;
    this.changeAppointmentForm.patchValue({
      date: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("YYYY-MM-DD") : this.changeAppointmentForm.controls.date.value ? moment(this.changeAppointmentForm.controls.date.value).format("YYYY-MM-DD") : null,
      start_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("HH:mm:ss") :  this.changeAppointmentForm.controls.start_hour.value ? moment(this.changeAppointmentForm.controls.start_hour.value).format("HH:mm:ss") : null,
      end_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).add(this.appointment.specialty.avarage_time, "minutes").format("HH:mm:ss") : this.changeAppointmentForm.controls.start_hour.value ? moment(this.changeAppointmentForm.controls.start_hour.value).add(this.appointment.specialty.avarage_time, "minutes").format("HH:mm:ss"): null,
      observations:  this.changeAppointmentForm.controls.observations.value
    });
    if (this.changeAppointmentForm.valid) {
      this.appointmentsService.changeAppointment(this.appointment.id, this.changeAppointmentForm.value).pipe(
        first())
        .subscribe(() => {
          this.close();
      });
    }
  }

  close() {
    this.modal.destroy();
  }

  appointmentSelect(appointment) {
    if( appointment.is_available){
      this.appointmentSelected = this.appointmentsDoctor.find(a => a.id === appointment.id);
    }
  }

}
