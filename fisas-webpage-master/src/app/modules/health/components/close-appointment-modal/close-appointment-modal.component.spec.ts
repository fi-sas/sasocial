import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseAppointmentModalComponent } from './close-appointment-modal.component';

describe('CloseAppointmentModalComponent', () => {
  let component: CloseAppointmentModalComponent;
  let fixture: ComponentFixture<CloseAppointmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseAppointmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseAppointmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
