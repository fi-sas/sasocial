import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { AppointmentModel } from '../../models/appointment.model';
import { AttendanceFormModel } from '../../models/attendance.model';
import { AppointmentsService } from '../../services/appointments.service';
import * as moment from 'moment';

@Component({
  selector: 'app-close-appointment-modal',
  templateUrl: './close-appointment-modal.component.html',
  styleUrls: ['./close-appointment-modal.component.less']
})
export class CloseAppointmentModalComponent implements OnInit {

  @Input() appointment: AppointmentModel;
  @Input() attendance: AttendanceFormModel;
  
  closeAppointmentForm = new FormGroup({
    date: new FormControl(null, [Validators.required]),
    start_hour: new FormControl( null, [Validators.required]),
    end_hour: new FormControl( null, [Validators.required]),
  })

  defaultOpenValue = moment().hour(0).minute(0).second(0).toDate();
  loadingSubmit = false;

  constructor(
    private modal: NzModalRef,
    private appointmentsService: AppointmentsService
  ) { }

  ngOnInit() {}

  disableDate = (endDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(endDate, "YYYY-MM-DD").isBefore(moment(currentDate).format("YYYY-MM-DD"));
  }

  close() {
    this.modal.destroy();
  }
  
  getInputError(field: string) {
    return this.closeAppointmentForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.closeAppointmentForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.closeAppointmentForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.closeAppointmentForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.closeAppointmentForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.closeAppointmentForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.closeAppointmentForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.closeAppointmentForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  onSubmit() {
    if (this.closeAppointmentForm.valid) {
      this.loadingSubmit = true;
      this.attendance.date = moment(this.closeAppointmentForm.controls.date.value).format("YYYY-MM-DD");
      this.attendance.start_hour = moment(this.closeAppointmentForm.controls.start_hour.value).format("HH:MM");
      this.attendance.end_hour = moment(this.closeAppointmentForm.controls.end_hour.value).format("HH:MM");
      this.appointmentsService.closeAttendance(this.appointment.id, this.attendance).pipe(first(),
      finalize(()=> { this.loadingSubmit = false })).subscribe(()=> {
        this.modal.close()
      })
    }
  }
}
