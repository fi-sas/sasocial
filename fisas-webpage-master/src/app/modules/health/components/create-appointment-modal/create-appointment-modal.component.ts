import { AfterContentChecked, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { AppointmentEventModel, AppointmentModel } from '../../models/appointment.model';
import { DoctorModel } from '../../models/doctor.model';
import { SpecialtyModel } from '../../models/specialty.model';
import { AppointmentTypeService } from '../../services/appointment-type.service';
import { AppointmentsService } from '../../services/appointments.service';
import { DoctorsService } from '../../services/doctors.service';
import { SpecialtiesService } from '../../services/specialties.service';
import * as moment from 'moment';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TariffModel } from '../../models/tariff.model';
import { TariffService } from '../../services/tariff.service';
import * as _ from 'lodash';
import { BuyModalBoxComponent } from '../buy-modal-box/buy-modal-box.component';

@Component({
  selector: 'app-create-appointment-modal',
  templateUrl: './create-appointment-modal.component.html',
  styleUrls: ['./create-appointment-modal.component.less']
})
export class CreateAppointmentModalComponent implements OnInit, AfterContentChecked {

  @Input() appointment: AppointmentModel;
  @ViewChild('modalBox') buy;
  
  specialtiesLoading = false;
  specialtySelected: SpecialtyModel;
  specialties: SpecialtyModel[] = [];

  appointmentTypesLoading = false;
  appointmentTypesList: AppointmentTypeModel[] = [];
  appointmentTypeIdSelected: number;

  doctorsLoading = false;
  doctors: DoctorModel[] = [];
  doctorsFiltered: DoctorModel[] = [];
  doctorSelected: DoctorModel;
  
  dayCurrent: Date = new Date();
  fifteenDays = [];

  loadingAppointments = false;
  submitted = false;
  appointments: AppointmentEventModel [] = [];
  appointmentsDoctor: AppointmentEventModel [] = [];
  appointmentSelected: AppointmentEventModel;

  tariffsLoading = false;
  tariffs: TariffModel[] = [];
  filteredTariffs: TariffModel[] = [];
  
  minuteStep: number = 0;

  step = 0;
  buttonActionName = 'HEALTH.BUTTONS.NEXT_STEP';

  newAppointment: AppointmentModel;
  infoModal: NzModalRef;
  
  createAppointmentForm = new FormGroup({
    specialty_id: new FormControl(null, Validators.required),
    doctor_id: new FormControl(null, Validators.required),
    allow_historic: new FormControl(null),
    date: new FormControl(null, Validators.required),
    start_hour: new FormControl(null, Validators.required),
    end_hour: new FormControl(null),
    appointment_type_id: new FormControl(null, Validators.required),
    payment_type:new FormControl(null, [Validators.required]),
    tariff_id: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required]),
  })
  
  constructor(
    private translate: TranslateService,
    private changeDetector: ChangeDetectorRef,
    private modal: NzModalRef,
    private appointmentTypeService: AppointmentTypeService,
    private specialtiesService: SpecialtiesService,
    private doctorsService: DoctorsService,
    private appointmentsService: AppointmentsService,
    private uiService: UiService,
    private tariffsService: TariffService,
    private modalService: NzModalService,
  ) { }

  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.getSpecialties();
    this.getAppointmentTypes();
    this.getDoctors();
    this.setDays();
    this.getTariffs();
  }

  close() {
    if( this.createAppointmentForm.value && this.createAppointmentForm.touched){ 
      this.modalService.confirm({
        nzTitle: this.translate.instant('HEALTH.BUTTONS.INFO_CLOSE_TEXT_MODAL'),
        nzOkText: this.translate.instant('HEALTH.APPOINTMENT_FORM.YES'),
        nzCancelText: this.translate.instant('HEALTH.APPOINTMENT_FORM.NO'),
        nzOnOk: () => {
          this.modal.destroy();
        }});
    }else{
      this.modal.destroy();
    }
  }

  nextStep(): void {
    if (this.step < 1) {
        this.step++;
        this.buttonActionName = this.step === 1 ? 'HEALTH.BUTTONS.SUBMIT' : 'HEALTH.BUTTONS.NEXT_STEP';
    } else {
      this.onSubmit();
    }
  }

  appointmentTypeSelect(appointmentType){
    if(!this.isAppointmentTypeDisabled(appointmentType.id)){
      this.appointmentTypeIdSelected = this.appointmentTypesList.find(t => t.id === appointmentType.id).id;
    }
  }

  isAppointmentTypeDisabled(value:any){
    if(this.specialtySelected){
      return this.specialtySelected.appointment_types.find(d => d.id === value) ? false : true
    }
  }

  getSpecialties(){  
    this.specialtiesLoading = true;
    this.specialtiesService.list().pipe(
      first(),
      finalize(() => this.specialtiesLoading = false)
    ).subscribe(specialtiesResponse => {
      this.specialties = specialtiesResponse.data;
    });
  }

  getTariffs(){
    this.tariffsService.list().pipe(
      first()).subscribe(results => {
        this.tariffs = results.data;
      });
  }

  filterTariffs(){
    if(this.specialtySelected){
      this.filteredTariffs = this.tariffs.filter(tariff => this.specialtySelected.price_variations.find(price => price.tariff_id === tariff.id))
    }
  }

  getDoctors(){
    this.doctorsLoading = true
    this.doctorsService.list().pipe(first(), finalize(() => this.doctorsLoading = false))
    .subscribe(response => {
      this.doctors = response.data;
    })
  }

  getAppointmentTypes(){
    this.appointmentTypesLoading = true;
    this.appointmentTypeService.list().pipe(
      first(),
      finalize(() => this.appointmentTypesLoading = false)).subscribe(results => {
        this.appointmentTypesList = results.data;
      });
  }

  getInputError(field: string) {
    return this.createAppointmentForm.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.createAppointmentForm.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.createAppointmentForm.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.createAppointmentForm.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.createAppointmentForm.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.createAppointmentForm.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.createAppointmentForm.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.createAppointmentForm.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  selectSpecialty(){
    this.specialtySelected = this.specialties.find(s => s.id === this.createAppointmentForm.get('specialty_id').value);
    this.doctorsFiltered = this.doctors.filter(doctor => this.specialtySelected.doctors.find( d => d.id === doctor.id));
    this.minuteStep = this.specialtySelected.avarage_time;
    this.filterTariffs();
    this.createAppointmentForm.patchValue({
      payment_type: this.specialtySelected.payment_type
  });
  }

  getDoctorSchedule(){
    this.doctorSelected = this.doctors.find(d => d.id ===  this.createAppointmentForm.get('doctor_id').value);
    this.getAppointments();
  }

  getAppointments(){
    if(this.doctorSelected && this.specialtySelected && !this.specialtySelected.is_external){
      this.loadingAppointments = true;
      this.appointmentsDoctor = [];
      this.appointments = [];
      this.specialtiesService.getScheduleDoctor(this.specialtySelected.id, this.doctorSelected.id).pipe(
        first(),
        finalize(() => this.loadingAppointments = false)).subscribe(results => {
          this.appointmentsDoctor = results.data;
          this.daySelected(this.dayCurrent);
        });
    }
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    const maxDate = moment().add(15, 'day');

    while (currentDate.isBefore(maxDate)) {
      this.fifteenDays.push(indexDate.toDate());
      indexDate = indexDate.add(1, 'day');
    }
    this.daySelected(this.dayCurrent);
  }

  daySelected(day) {
    this.dayCurrent = day;
    if(this.appointmentsDoctor.length > 0){
      this.appointments = this.appointmentsDoctor.filter(appointment => moment(moment(this.dayCurrent).format("YYYY-MM-DD")).isBetween(moment(appointment.start_date).format("YYYY-MM-DD"), moment(appointment.end_date).format("YYYY-MM-DD"), null, '[]') )
    }
  }

  appointmentSelect(appointment) {
    if( appointment.is_available){
      this.appointmentSelected = this.appointmentsDoctor.find(a => a.id === appointment.id);
    }
  }

  onSubmit(){
    this.submitted = true;
    this.createAppointmentForm.patchValue({
      date: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("YYYY-MM-DD") : this.createAppointmentForm.controls.date.value ? moment(this.createAppointmentForm.controls.date.value).format("YYYY-MM-DD") : null,
      start_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).format("HH:mm") :  this.createAppointmentForm.controls.start_hour.value ? moment(this.createAppointmentForm.controls.start_hour.value, "HH:mm") : null,
      end_hour: this.appointmentSelected ? moment(this.appointmentSelected.start_date).add(this.specialtySelected.avarage_time, "minutes").format("HH:mm") : this.createAppointmentForm.controls.start_hour.value ? moment(this.createAppointmentForm.controls.start_hour.value).add(this.specialtySelected.avarage_time, "minutes").format("HH:mm"): null,
      appointment_type_id: this.appointmentTypeIdSelected ? this.appointmentTypeIdSelected : null,
    });
    if (this.createAppointmentForm.valid) {
      this.newAppointment = this.setNewAppointmentValues(this.createAppointmentForm.controls);
      if(this.createAppointmentForm.controls.price.value > 0 && this.createAppointmentForm.controls.payment_type.value === "Pré Pagamento"){
        let appointment = this.newAppointment;
        let specialty = this.specialtySelected;
        this.infoModal = this.modalService.create({
          nzMask: true,
          nzWrapClassName: 'vertical-center-modal',
          nzContent: BuyModalBoxComponent,
          nzComponentParams: { appointment, specialty },
          nzFooter: null,
          nzWidth: 350
       
      });
      this.infoModal.afterClose.pipe().subscribe((result) => {
          this.confirmAppointment(result)
      });
      }else if(this.newAppointment && (this.newAppointment.price === 0 ||this.newAppointment.payment_type === "Ato Consulta")){
        this.confirmAppointment(true);
      }
    }
  }

  confirmAppointment(event: any){
    if(event){
      this.appointmentsService.create(this.newAppointment).pipe(first()).subscribe( result => { 
        this.uiService.showMessage(
          MessageType.success,
          this.translate.instant('HEALTH.APPOINTMENT_FORM.MODAL.DESCRIPTION')
        );
        this.modal.destroy();
        })
    }else{
      this.submitted = false;
    }
  }

  getPriceInformation(){
    const tariffSelected = this.createAppointmentForm.get('tariff_id').value;
    if(tariffSelected){
      this.createAppointmentForm.patchValue({
        price: this.specialtySelected.price_variations.find(price => price.tariff_id === tariffSelected).price
      })
    }
  }

  
  disableDate = (endDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate());
    return moment(endDate).isSameOrBefore(currentDate);
  }
  
  setNewAppointmentValues(form: any){
    let newAppointment = {} as AppointmentModel;
    newAppointment.doctor_id = form.doctor_id.value;
    newAppointment.specialty_id = form.specialty_id.value;
    newAppointment.date = form.date.value;
    newAppointment.start_hour = form.start_hour.value;
    newAppointment.end_hour = form.end_hour.value;
    newAppointment.appointment_type_id = form.appointment_type_id.value;
    newAppointment.user_id = this.appointment.user_id;
    newAppointment.name = this.appointment.name;
    newAppointment.student_number = this.appointment.name;
    newAppointment.niss = this.appointment.name;
    newAppointment.document_type_id = this.appointment.document_type_id;
    newAppointment.identification_number = this.appointment.identification_number;
    newAppointment.nif = this.appointment.nif;
    newAppointment.address = this.appointment.address;
    newAppointment.postal_code = this.appointment.postal_code;
    newAppointment.city = this.appointment.city;
    newAppointment.nationality = this.appointment.nationality;
    newAppointment.email = this.appointment.email;
    newAppointment.mobile_phone_number = this.appointment.mobile_phone_number;
    newAppointment.telephone_number = this.appointment.telephone_number;
    newAppointment.course_id = this.appointment.course_id;
    newAppointment.organic_unit_id = this.appointment.organic_unit_id;
    newAppointment.course_year = this.appointment.course_year;
    newAppointment.has_scholarship = this.appointment.has_scholarship;
    newAppointment.attachments_ids = [];
    newAppointment.allow_historic = form.allow_historic.value;
    newAppointment.payment_type = form.payment_type.value,
    newAppointment.tariff_id = form.tariff_id.value,
    newAppointment.price = form.price.value;
    return newAppointment;
  }

}