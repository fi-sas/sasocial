import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpSpecialtyComponent } from './pop-up-specialty.component';

describe('PopUpSpecialtyComponent', () => {
  let component: PopUpSpecialtyComponent;
  let fixture: ComponentFixture<PopUpSpecialtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopUpSpecialtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopUpSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
