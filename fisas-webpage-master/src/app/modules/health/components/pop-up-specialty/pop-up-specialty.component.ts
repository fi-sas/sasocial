import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpecialtyiesDoctorModel, SpecialtyTranslationModel } from '../../models/specialty.model';

@Component({
  selector: 'app-pop-up-specialty',
  templateUrl: './pop-up-specialty.component.html',
  styleUrls: ['./pop-up-specialty.component.less']
})

export class PopUpSpecialtyComponent implements OnInit {

  @Output() specialty = new EventEmitter<number[]>();
  @Input() specialties: SpecialtyiesDoctorModel[];

  loadingPopUp= false;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { 
    this.activatedRoute.queryParams.subscribe((params) => {
      if(this.specialties){
        this.specialties.forEach((option) => {
         option.checked = !!(params['specialties'] || '').split(',').find((a: string) => {
            if(!isNaN(parseInt(a)) && parseInt(a) === option.value){
              return option;
            }
          });
        });
      }
    });
  }

  ngOnInit() {}
  
  confirm() {
    this.specialty.emit(this.specialties.filter((b) => b.checked).map((b) => b.value));
  }

  setSpecialty(event: any){
  for(const e of event){
      const option = this.specialties.find(s => s.value === e);
      if(option){
        option.checked = true;
      }
    }
  }

}
