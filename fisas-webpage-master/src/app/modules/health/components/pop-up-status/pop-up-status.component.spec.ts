import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpStatusComponent } from './pop-up-status.component';

describe('PopUpStatusComponent', () => {
  let component: PopUpStatusComponent;
  let fixture: ComponentFixture<PopUpStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopUpStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopUpStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
