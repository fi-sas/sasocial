import { ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Output } from '@angular/core';

import { TranslatePipe } from '@ngx-translate/core';

interface IOption {
  checked: boolean;
  label: string;
  value: string;
}

@Component({
  selector: 'app-pop-up-status',
  templateUrl: './pop-up-status.component.html',
  styleUrls: ['./pop-up-status.component.less'],
  providers: [TranslatePipe]
})

export class PopUpStatusComponent {

  @Output() status = new EventEmitter<string[]>();

  options: IOption[] = [
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.CLOSED'),
      value: 'CLOSED',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.APPROVED'),
      value: 'APPROVED',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.PENDING'),
      value: 'PENDING',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.CHANGED'),
      value: 'CHANGED',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.CANCELED'),
      value: 'CANCELED',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.MISSED'),
      value: 'MISSED',
      checked: false,
    },
    {
      label: this.translate.transform('HEALTH.APPOINTMENTS_TAB.STATUS.STARTED'),
      value: 'STARTED',
      checked: false,
    }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private translate: TranslatePipe
    ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.options.forEach((option) => {
        option.checked = !!(params['status'] || '').split(',').find((a: string) => a === option.value);
        return option;
      });
    });
  }

  confirm() {
    this.status.emit(this.options.filter((b) => b.checked).map((b) => b.value));
  }
}
