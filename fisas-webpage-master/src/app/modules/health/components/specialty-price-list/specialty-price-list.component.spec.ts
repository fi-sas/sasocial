import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyPriceListComponent } from './specialty-price-list.component';

describe('SpecialtyPriceListComponent', () => {
  let component: SpecialtyPriceListComponent;
  let fixture: ComponentFixture<SpecialtyPriceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialtyPriceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialtyPriceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
