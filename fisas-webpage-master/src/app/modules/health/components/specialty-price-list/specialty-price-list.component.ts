import { Component, Input } from '@angular/core';
import { SpecialtyModel } from '../../models/specialty.model';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-specialty-price-list',
  templateUrl: './specialty-price-list.component.html',
  styleUrls: ['./specialty-price-list.component.less']
})
export class SpecialtyPriceListComponent {

  @Input() priceVariations: SpecialtyModel = null;
  @Input() payment_information: string = null;
  
  constructor(public translate: TranslateService) { }


}
