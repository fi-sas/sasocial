import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyTechnicalDataComponent } from './specialty-technical-data.component';

describe('SpecialtyTechnicalDataComponent', () => {
  let component: SpecialtyTechnicalDataComponent;
  let fixture: ComponentFixture<SpecialtyTechnicalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialtyTechnicalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialtyTechnicalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
