import { Component, Input, OnInit } from '@angular/core';
import { SpecialtyModel } from '../../models/specialty.model';
import { TranslateService } from '@ngx-translate/core';
import { cities } from "@fi-sas/webpage/shared/data/cities";

@Component({
  selector: 'app-specialty-technical-data',
  templateUrl: './specialty-technical-data.component.html',
  styleUrls: ['./specialty-technical-data.component.less']
})
export class SpecialtyTechnicalDataComponent implements OnInit{

  @Input() specialty: SpecialtyModel = null;
  cities = cities;
  latitude: number = 39.3999;
  longitude: number = -8.2245;
  latitudeInit: number = 39.3999;
  longitudeInit: number = -8.2245;
  map: any;
  
  constructor(public translate: TranslateService) { }

  ngOnInit(): void {
    if(this.specialty.place){
      const findCity = this.cities.find((city) => city.city == this.specialty.place.city);
      if (findCity) {
          this.longitude = Number(findCity.lng);
          this.latitude = Number(findCity.lat);
          this.latitudeInit = Number(findCity.lat);
          this.longitudeInit = Number(findCity.lng);
      }
    }
  }

}
