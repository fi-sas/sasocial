import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HealthComponent } from './health.component';

const routes: Routes = [
    {
        path: '',
        component: HealthComponent,
        children: [
            { path: '', redirectTo: 'specialties', pathMatch: 'full' },
            {
                path: 'specialties',
                loadChildren: './pages/specialties/specialties.module#SpecialtiesModule',
                data: { breadcrumb: null }
            },
            {
                path: 'specialties/detail/:id',
                loadChildren: './pages/specialty-detail/specialty-detail.module#SpecialtyDetailModule',
                data: { breadcrumb: null }
            },
            {
                path: 'specialties/detail/:id/appointment-form',
                loadChildren: './pages/form-appointment/form-appointment.module#FormAppointmentModule',
                data: { breadcrumb: null }
            },
            {
                path: 'appointments',
                loadChildren: './pages/appointments/appointments.module#AppointmentsModule',
                data: { breadcrumb: null }
            },
            {
                path: 'appointments/detail/:id',
                loadChildren: './pages/appointment-detail/appointments-detail.module#AppointmentDetailModule',
                data: { breadcrumb: null }
            },
            {
                path: 'complaints',
                loadChildren: './pages/complaints/complaints.module#ComplaintsHealthModule',
                data: { breadcrumb: null }
            },
            {
                path: 'attendances',
                loadChildren: './pages/attendances/attendances.module#AttendancesModule',
                data: { breadcrumb: null }
            },
            {
                path: 'attendances/detail/:id',
                loadChildren: './pages/attendance-detail/attendance-detail.module#AttendanceDetailModule',
                data: { breadcrumb: null }
            },
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HealthRoutingModule { }
