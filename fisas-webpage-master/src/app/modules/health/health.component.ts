import { UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorsService } from './services/doctors.service';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.less']
})
export class HealthComponent implements OnInit {

  isLoading = false;
  isProfessional: boolean = false;
  barOptions: any = []

  constructor(
    private router: Router,
    private uiService: UiService,
    private doctorsService: DoctorsService,
  ) { }

  ngOnInit() {
    this.checkLoggedUser()
  }

  checkLoggedUser(){
    this
    this.isLoading = true;
    this.doctorsService.checkIfIsProfessional().pipe(first(), finalize(() => this.isLoading = false)).subscribe(res => {
     
    this.barOptions = [
      {
        translate: 'HEALTH.MENU.SPECIALTIES',
        disable: true,
        routerLink: '/health/specialties',
        allow: true,
      },
      {
        translate: 'HEALTH.MENU.APPOINTMENTS',
        disable: false,
        routerLink: '/health/appointments',
        allow: true,
      },
      {
        translate: 'HEALTH.MENU.COMPLAINTS',
        disable: false,
        routerLink: '/health/complaints',
        allow: true,
      },
      {
        translate: 'HEALTH.MENU.ATTENDANCES',
        disable: false,
        routerLink: '/health/attendances',
        allow: res.data? res.data[0] : this.isProfessional
      }
    ];
    });
  }

}
