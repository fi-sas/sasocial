import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthComponent } from './health.component';
import { HealthRoutingModule } from './health-routing.module';

@NgModule({
  declarations: [
    HealthComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HealthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class HealthModule { }
