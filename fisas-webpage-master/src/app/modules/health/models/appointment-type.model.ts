export class AppointmentTypeModel {
    id: number;
    translations: AppointmentTypeTranslationModel[];
}

export class AppointmentTypeTranslationModel {
    id: number;
    specialty_id: number;
    language_id: number;
    name: string;
  }