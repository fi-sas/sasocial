import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { AppointmentTypeModel } from "./appointment-type.model";
import { AttendanceModel } from "./attendance.model";
import { DoctorModel } from "./doctor.model";
import { SpecialtyModel } from "./specialty.model";

export class AppointmentModel {
  id?: number;
  user_id: number;
  corporate_id: number;
  student_number: string;
  name: string;
  gender: string
  email: string;
  mobile_phone_number: string;
  telephone_number: string;
  document_type_id: number;
  identification_number: number;
  nif: string;
  niss: string;
  address: string;
  postal_code: string;
  city: string;
  nationality: string;
  course_id: number;
  course_year: number;
  department_id: number;
  organic_unit_id: number;
  specialty_id: number;
  date: Date
  start_hour: string;
  end_hour: string;
  has_scholarship: boolean;
  notes: string;
  allow_historic: boolean;
  appointment_type_id: number;
  tariff_id: number;
  doctor_id: number;
  attachments_ids:[];
  course?: CourseModel;
  organicUnit?: OrganicUnitModel;
  documentType?: DocumentTypeModel;
  specialty?: SpecialtyModel;
  status: string;
  price: number;
  payment_type?: string;
  attendance?: AttendanceModel;
  doctor?: DoctorModel;
  appointment_type?: AppointmentTypeModel;
  birth_date?: Date;
  }

  export class AppointmentsOpenClosedModel{
    closed: AppointmentModel[];
    open: AppointmentModel[];
    older_open: AppointmentModel[];
  }

  export class AppointmentEventModel{
    id: string;
    title: string;
    start_date: string;
    end_date: string;
    is_available: boolean;
    is_holiday: boolean;
  }

export enum Gender {
    M = 'M',
    F = 'F',
    U = 'u',
  }
  export enum AppointmentStatus {
    CLOSED = 'CLOSED',
    APPROVED = 'APPROVED',
    PENDING = 'PENDING',
    CHANGED = 'CHANGED',
    CANCELED = 'CANCELED',
    MISSED = 'MISSED',
    STARTED = 'STARTED',
  }

  export class AppointmentChangeModel {
    date: Date;
    start_hour: string;
    end_hour: string;
    observations: string;
  };

  export class AppointmentHistoricResponseModel {
    appointment: AppointmentModel;
    historic: AppointmentHistoricModel[];
  }

  export class AppointmentHistoricModel {
    appointment_id: number;
    created_at: Date
    date: Date
    doctor_id: number
    end_hour: string
    id: number
    observations: string
    specialty_id: number
    start_hour: string
    status: string
    updated_at: Date
  }
  
  export const ConfigStatus = [
    {
      name: AppointmentStatus.CLOSED,
      color: '#0a5f38'
    },
    {
      name: AppointmentStatus.APPROVED,
      color: '#008452'
    },
    {
      name: AppointmentStatus.PENDING,
      color: '#88ccf1'
    },
    {
      name: AppointmentStatus.CHANGED,
      color: '#FF0000'
    },
    {
      name: AppointmentStatus.CANCELED,
      color: '#FF2400'
    },
    {
      name: AppointmentStatus.MISSED,
      color: '#FF2400'
    },
    {
      name: AppointmentStatus.STARTED,
      color: '#000000'
    }
  ]