import { HttpParams } from '@angular/common/http';
import { Params } from '@angular/router';
import * as moment from 'moment';

export interface IAttendanceListingQueryParams {
  specialties?: string;
  status?: string;
  start_date?: string;
  end_date?: string;
}

export class AttendanceListingQueryParams {
    specialties: string[];
    status: string[];
    start_date: string;
    end_date: string;

  constructor(queryParams: Params = {}) {
    this.specialties =  this.stringToList(queryParams.specialties);
    this.status = this.stringToList(queryParams.status);
    this.start_date = queryParams.start_date || null;
    this.end_date = queryParams.end_date || null;
  }

  clear() {
    this.specialties = [];
    this.status = [];
    this.start_date = null;
    this.end_date = null;
  }

  toUrlParams(): IAttendanceListingQueryParams {
    const params: IAttendanceListingQueryParams = {};
    if (this.specialties.length) {
      params.specialties =  this.specialties.join(',');
    }
    if (this.status.length) {
      params.status = this.status.join(',');
    }
    if (this.start_date !== null) {
      params.start_date = this.start_date;
    }
    if (this.end_date !== null) {
      params.end_date = this.end_date;
    }
    return params;
  }

  toHttpParams(): HttpParams {
    let params = new HttpParams();

    if (this.specialties.length) {
      params = params.set('query[specialties]', JSON.stringify(this.specialties));
    }
    if (this.status.length) {
      params = params.set('query[status]', JSON.stringify(this.status));
    }
    if (this.start_date !== null) {
      params = params.set('query[date][gte]', this.start_date);
    }
    if (this.end_date !== null) {
      params = params.set('query[date][lte]', this.end_date);
    }

    return params;
  }

  private stringToList(value?: string) {
    return value ? value.split(',') : [];
  }
}
