import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { AppointmentModel } from "../../queue/models/appointment-subject.model";

export class AttendanceModel {
    id?: number;
    appointment_id: number;
    observations: string;
    date: Date;
    start_hour: string;
    end_hour: string;
    status: string;
    user?: UserModel;
    checklist?: ChecklistModel[];
    historic?: AppointmentModel[];
}

export class ChecklistModel {
    id?: number;
    specialty_id?: number;
    question: string;
    answer_type?: string;
    answer?: string;
}

export class AttendanceFormModel{
    observations: string;
    date: string;
    start_hour: string;
    end_hour: string;
    attachments_ids: [];
    checklist_data: ChecklistFormModel;
}

export class ChecklistFormModel {
    notes: string;
    attachments_ids: number [];
    answers: QuestionModel [];
}

export class QuestionModel {
    question: string;
    answer: string;
}

