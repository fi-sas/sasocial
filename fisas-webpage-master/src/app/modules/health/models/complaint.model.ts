import { AppointmentModel } from "./appointment.model";

export class ComplaintModel{
    id: number;
    observation: string;
    files_ids?: [];
    answered_by: string;
    appointment?: AppointmentModel
    appointment_id: number;
    user_id: number;
    complaint_answer?: string;
    files: []
    status: string;
    created_at: Date;
    updated_at: Date;
}

export enum ComplaintStatus {
    CREATED = 'CREATED',
    SOLVED = 'SOLVED'
  }
  
export const ConfigStatusComplaint = [
    {
      name: ComplaintStatus.CREATED,
      color: '#008452'
    },
    {
      name: ComplaintStatus.SOLVED,
      color: '#0a5f38'
    }
  ]