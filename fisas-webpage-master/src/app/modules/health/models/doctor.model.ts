export class DoctorModel {
    id: number;
    user: User;
    user_id: number;
    allow_historic: boolean;
    is_external: boolean;
    created_at: Date;
    update_at: Date;
}

export class User {
    id: number;
    name: string;
    email: string;
    gender: Gender;
    phone: number;
    address: string;
    city: string;
    postal_code: string;
    country: string;
  }

export enum Gender {
    M = 'M',
    F = 'F',
    U = 'u',
}