export class Place {
    id: number;
    name: string;
    email: string;
    address: string;
    postal_code: string;
    city: string;
    phone: number;
    fax: number;
}