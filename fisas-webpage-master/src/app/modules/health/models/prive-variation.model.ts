import { TariffModel } from "./tariff.model";

export class PriceVariationsModel {
    id: number;
    specialty_id: number;
    tariff_id: number;
    tariff?: TariffModel;
    tariffs?: TariffModel [];
    vat_id: number;
    vat: VatModel;
    price: number;
    created_at: Date;
    update_at: Date;
}

export class VatModel{
    id: number;
    name: string;
    description: string;
    tax_value: number;
    active: boolean;
    created_at: Date;
    update_at: Date;
}

