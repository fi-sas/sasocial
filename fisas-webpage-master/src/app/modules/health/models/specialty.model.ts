import { AppointmentTypeModel } from "./appointment-type.model";
import { PriceVariationsModel } from "./prive-variation.model";
import { DoctorModel } from "./doctor.model";
import { Place } from "./place.model";
import { SpecialtyChecklist } from "./specialty-checklist.model";

export class SpecialtyModel {
  id: number;
  translations: SpecialtyTranslationModel[];
  appointment_types: AppointmentTypeModel[];
  price_variations: PriceVariationsModel[];
  doctors: DoctorModel[];
  place: Place;
  checklist: SpecialtyChecklist
  is_external: boolean;
  description: string;
  regulation: string;
  avarage_time: number;
  product_code: string;
  place_id: number;
  payment_type: string;
  payment_information: string;
  organic_unit_id: number;
  is_active: boolean;
  active: boolean;
  created_at: Date;
  update_at: Date;
}

export class SpecialtiesFormModel{
  id: number;
  specialty_id: number;
  doctor_id: number;
  start_hour: string;
  date: string;
  appointment_type_id: number;
}

export class SpecialtyTranslationModel {
  id: number;
  specialty_id: number;
  language_id: number;
  name: string;
}

export class SpecialtyiesDoctorModel{
  checked: boolean;
  translations: SpecialtyTranslationModel[];
  value: number;
}

export enum SpecialtyStatus {
  ACTIVE = 'true',
  INACTIVE = 'false',
}
  
export const ConfigStatus = [
  {
    name: SpecialtyStatus.ACTIVE,
    color: '#008000'
  },
  {
    name: SpecialtyStatus.INACTIVE,
    color: '#FF0000'
  }
]