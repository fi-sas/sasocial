export class Vat {
    id: number;
    specialty_id: number;
    language_id: number;
    name: string;
}