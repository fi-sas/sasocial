import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { AppointmentModel, ConfigStatus } from '../../models/appointment.model';
import { PriceVariationsModel } from '../../models/prive-variation.model';
import { AppointmentsService } from '../../services/appointments.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.less']
})
export class AppointmentDetailComponent implements OnInit {

  loading = false;
  appointment: AppointmentModel;
  readonly configStatus = ConfigStatus;
  documentTypes: DocumentTypeModel[] = [];
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appointmentsService: AppointmentsService,
    private authService: AuthService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getDocumentTypes();
    this.route.params.subscribe(async (params) => {
      if ( params['id']) {
          this.getDetail( params['id']);
      }
    });
  }

  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

  getDetail(id){
    this.loading = true;
    this.appointmentsService.getAppointmentById(id).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
      this.appointment = data.data[0];
    })
  }

  findDocumentType(id: number){
    if(this.documentTypes.length > 0){
      return this.documentTypes.find(document => document.id === id).translations;
    }
  }

  findTariff(tariff_id : number){
    const priceVariation: PriceVariationsModel = this.getTariff(tariff_id);
    if(priceVariation){
      return priceVariation.tariffs.find(tariff => tariff.id === tariff_id).description
    }
  }

  findTax(id: number){
    const priceVariation: PriceVariationsModel = this.getTariff(id);
    if(priceVariation){
      return priceVariation.vat.name.replace("IVA", "");
    }
    return id;
  }

  getTariff(id: number){
    let tariffFound: PriceVariationsModel = null;
    if (this.appointment) {
      tariffFound = this.appointment.specialty.price_variations.find(
        (value) => value.tariff_id === id
      );
      if (tariffFound !== undefined) {
        return tariffFound;
      }
    }
  }

  getAppointmentFinalPrice(id: number){
    const priceVariation: PriceVariationsModel = this.getTariff(id);
    if(priceVariation){
      return priceVariation.price + (priceVariation.price * priceVariation.vat.tax_value)
    }
    return this.appointment.price;
  }

  back(){
    //this.router.navigate(['health/appointments']);
    this.location.back();
  }

}
