import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { AppointmentDetailComponent } from './appointment-detail.component';
import { AppointmentDetailRoutingModule } from './appointments-detail.routing.module';

@NgModule({
  declarations: [
    AppointmentDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HealthSharedModule,
    AngularOpenlayersModule,
    AppointmentDetailRoutingModule
  ]
})
export class AppointmentDetailModule { }
