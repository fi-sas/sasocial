import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentDetailComponent } from './appointment-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AppointmentDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentDetailRoutingModule { }
