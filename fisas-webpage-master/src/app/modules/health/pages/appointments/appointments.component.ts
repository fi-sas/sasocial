import { Component, OnInit } from '@angular/core';
import { AppointmentsService } from '../../services/appointments.service';
import { AppointmentModel, AppointmentsOpenClosedModel, ConfigStatus } from '../../models/appointment.model';
import { PANNEL_APPOINTMENT } from '../../utils/util.model'
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.less']
})
export class AppointmentsComponent implements OnInit {

  appointments: AppointmentsOpenClosedModel;
  appointmentsOpen: AppointmentModel [] = [];
  appointmentsClose: AppointmentModel [] = [];

  loading = true;
  configStatus = ConfigStatus;
  panels = PANNEL_APPOINTMENT;
  
  constructor(
    private appointmentsService: AppointmentsService,
  ) { }

  ngOnInit() {
    this.getAppointments();
  }

  getAppointments(){
    this.appointmentsService.historic().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(appointmentsResponse => {
      this.appointments = appointmentsResponse.data[0];
      this.appointmentsOpen = this.appointments.open;
      this.appointmentsClose = this.appointments.closed;
    });
  }

  refresh(event) {
    if(event){
      this.getAppointments();
    }
  }

  isPar(val) {
    return (val % 2 === 0);
  }

}
