import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { AppointmentsComponent } from './appointments.component';
import { AppointmentsRoutingModule } from './appointments-routing.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { CardAppointmentComponent } from '../../components/card-appointment/card-appointment.component';
import { CardAppointmentHistoricComponent } from '../../components/card-appointment-historic/card-appointment-historic.component';

@NgModule({
  declarations: [
    AppointmentsComponent,
    CardAppointmentComponent,
    CardAppointmentHistoricComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppointmentsRoutingModule,
    HealthSharedModule,
  ]
})
export class AppointmentsModule { }
