import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { CloseAppointmentModalComponent } from '../../components/close-appointment-modal/close-appointment-modal.component';
import { CreateAppointmentModalComponent } from '../../components/create-appointment-modal/create-appointment-modal.component';
import { AppointmentModel } from '../../models/appointment.model';
import { AttendanceFormModel, AttendanceModel } from '../../models/attendance.model';
import { AppointmentsService } from '../../services/appointments.service';

@Component({
  selector: 'app-attendance-detail',
  templateUrl: './attendance-detail.component.html',
  styleUrls: ['./attendance-detail.component.less']
})
export class AttendanceDetailComponent implements OnInit , OnDestroy{

  loading = false;
  loadingAttendance = false;

  attendanceStart = false;
  allowAttendance = true;
  appointment: AppointmentModel;
  attendance: AttendanceModel;

  infoModal: NzModalRef;

  arrayValues: {
  } = null;

  @ViewChild('tplSuccess') sucessTemplate: TemplateRef<any>;
  @ViewChild('checklistForm', null) checklistForm: AttendanceFormModel;
  @ViewChild('attendanceDataForm', null) attendanceDataForm: AttendanceFormModel;
  
  steps = [
    {
      name: 'checklistForm',
      formValues: {},
    },
    {
      name: 'attendanceDataForm',
      formValues: {},
    }
  ];
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appointmentsService : AppointmentsService,
    private uiService: UiService,
    private modalService: NzModalService,
    private translateService: TranslateService,
  ) { }

  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.route.params.subscribe(async (params) => {
      if ( params['id']) {
          this.getAppointment( params['id']);
      }
    });
  }
  
  getAppointment(id){
    this.loading = true;
    this.appointmentsService.getAppointmentById(id).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
      this.appointment = data.data[0];
      if(this.appointment.status === 'CLOSED' || this.appointment.status === 'PENDING' || this.appointment.status === 'MISSED'){
        this.allowAttendance = false;
        this.attendanceStart = false;
      }else{
        this.loadingAttendance = true;
        this.appointmentsService.getAttendanceInformation(id).pipe(first(), finalize(() => this.loadingAttendance = false)).subscribe((data) => {
          this.attendance = data.data[0];
          if(this.attendance){
            this.attendanceStart = true;
          }
        })
      }
    });
  }

  back(){
    this.router.navigate(['health/attendances']);
  }

  checkIfAttendanceIsToday(){
    if(this.appointment && moment(this.appointment.date, "YYYY-MM-DD").isSameOrBefore(moment(new Date()).format("YYYY-MM-DD"))){
      return false
    }
    return true;
  }

  startAttendance(){
    if(!this.checkIfAttendanceIsToday()){
      this.loadingAttendance = true;
      this.appointmentsService.startAttendance(this.appointment.id).pipe(first(), finalize(() => this.loadingAttendance = false)).subscribe((data) => {
        this.attendance = data.data[0];
        this.attendanceStart = true;
      });
    }
  }

  registerAbsence(){
    if(this.appointment){
      this.appointmentsService.absence(this.appointment.id).subscribe((result) => {
        this.uiService.showMessage(
          MessageType.success,
            'Falta marcada'
          );
          this.allowAttendance = false;
        });
    }
  }

  newAppointment(){
    this.infoModal = this.createTplModal(this.appointment, null, CreateAppointmentModalComponent);
  }

  /*changeAppointment(){
    this.infoModal = this.createTplModal(this.appointment, ChangeAppointmentModalComponent);
  }*/

  createTplModal(appointment: AppointmentModel, attendance: AttendanceFormModel, component: any){
    return this.modalService.create({
      nzTitle: null,
      nzContent: component,
      nzFooter: null,
      nzWidth: 350,
      nzComponentParams: { appointment , attendance},
      nzWrapClassName: 'vertical-center-modal',
    });
  }

  closeAttendance(){
    this.steps.forEach((step, index) => {
      const form = this[this.steps[index].name].submitForm();
      if (form !== null) {
        this.steps[index].formValues = form;
      }   
    })

    let attendanceResponse = {} as AttendanceFormModel;
    attendanceResponse = Object.assign(this.steps[1].formValues)
    attendanceResponse.checklist_data = Object.assign(this.steps[0].formValues)

    this.infoModal = this.createTplModal(this.appointment, attendanceResponse, CloseAppointmentModalComponent);
    this.infoModal.afterClose.pipe(first()).subscribe(() => {
      this.route.params.subscribe(async (params) => {
        if ( params['id']) {
            this.getAppointment( params['id']);
        }
      });
    });
  }

}