import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { AttendanceDetailRoutingModule } from './attendance-detail.routing.module';
import { AttendanceDetailComponent } from './attendance-detail.component';
import { AttendanceUserDataComponent } from '../../components/attendance-user-data/attendance-user-data.component';
import { AttendanceDiagnosisChecklistComponent } from '../../components/attendance-diagnosis-checklist/attendance-diagnosis-checklist.component';
import { AttendanceDiagnosisHistoricComponent } from '../../components/attendance-diagnosis-historic/attendance-diagnosis-historic.component';
import { AttendanceAttendaceDataComponent } from '../../components/attendance-attendace-data/attendance-attendace-data.component';

@NgModule({
  declarations: [
    AttendanceDetailComponent,
    AttendanceUserDataComponent,
    AttendanceDiagnosisChecklistComponent,
    AttendanceDiagnosisHistoricComponent,
    AttendanceAttendaceDataComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    AttendanceDetailRoutingModule,
    HealthSharedModule,
    AngularOpenlayersModule
  ]
})
export class AttendanceDetailModule { }
