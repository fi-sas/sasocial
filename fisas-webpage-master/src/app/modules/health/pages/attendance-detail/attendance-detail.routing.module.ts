import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttendanceDetailComponent } from './attendance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AttendanceDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AttendanceDetailRoutingModule { }
