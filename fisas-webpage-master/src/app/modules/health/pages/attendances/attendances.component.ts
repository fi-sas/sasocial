import { DatePipe } from '@angular/common';
import { AfterContentChecked, ChangeDetectorRef, Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';
import { ChangeAppointmentModalComponent } from '../../components/change-appointment-modal/change-appointment-modal.component';
import { CreateAppointmentModalComponent } from '../../components/create-appointment-modal/create-appointment-modal.component';
import {  AppointmentEventModel, AppointmentModel, AppointmentsOpenClosedModel, ConfigStatus } from '../../models/appointment.model';
import { AttendanceListingQueryParams } from '../../models/attendance-query-params.model';
import { SpecialtyiesDoctorModel } from '../../models/specialty.model';
import { AppointmentsService } from '../../services/appointments.service';
import { DoctorsService } from '../../services/doctors.service';

@Component({
  selector: 'app-attendances',
  templateUrl: './attendances.component.html',
  styleUrls: ['./attendances.component.less']
})
export class AttendancesComponent implements OnInit , AfterContentChecked{

  
  pageIndex = 1;
  pageSize = 12;
  totalProperties = 0;
  totalPages = 0;
  start_date = '';
  end_date = '';
  dateRange = [];

  fifteenDays = [];
  dayCurrent: Date = new Date();

  attendancesDoctor : AppointmentsOpenClosedModel;
  attendances: AppointmentModel[] = [];
  attendancesDailyHistoric: AppointmentModel[] = [];
  attendancesHistoric: AppointmentModel[] = [];

  appointmentSelected: AppointmentEventModel;

  loading = false;
  infoModal: NzModalRef;
  configStatus = ConfigStatus;
  selectedAttendance: AppointmentModel;
  loggedUser: UserModel = null;

  doctorSpecialties: SpecialtyiesDoctorModel[] = [];

  queryParams: AttendanceListingQueryParams;

  disableDate = (currentDate: Date): boolean => {
    return moment(currentDate).isBefore(moment(), 'day');
  };

  constructor(
    public translate: TranslateService,
    private appointmentsService: AppointmentsService,
    private router: Router,
    private changeDetector: ChangeDetectorRef,
    private uiService: UiService,
    private modalService: NzModalService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private doctorService: DoctorsService,
  ) { 
    this.queryParams = new AttendanceListingQueryParams(this.activatedRoute.snapshot.queryParams);
    this.activatedRoute.queryParams.subscribe(() => {
      this.setDays();
      this.getDoctorAppointments()
    });
    
  }
  
  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

  ngOnInit() {
    this.getLoggedUser();
    this.setDays();
  }

  getLoggedUser() {
    this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      this.loggedUser = res.data[0];
      this.getSpecialties();
    })
  }

  getSpecialties(){
    if(this.loggedUser){
      this.doctorService.getSpecialtiesByDoctor(this.loggedUser.id).pipe(first()).subscribe(res => {
        this.doctorSpecialties = res.data;
      });
    }
  }

  getDoctorAppointments(){
    this.loading = true;
    this.appointmentsService.doctorAppointments(this.pageIndex, this.pageSize, this.queryParams.toHttpParams()).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(appointmentsResponse => {
      this.attendancesDoctor = appointmentsResponse.data[0];
      this.attendancesHistoric = appointmentsResponse.data[0].older_open;
      this.daySelected(this.dayCurrent);
    });
  }

  setDays() {
    let currentDate;
    //let days = 15;
    if(this.queryParams.start_date){
      currentDate = moment(this.queryParams.start_date);
      this.dateRange[0] =  moment(this.queryParams.start_date).toDate();
      this.dateRange[1] =  moment(this.queryParams.end_date).toDate();
      //days = moment(this.queryParams.start_date).diff(moment(this.queryParams.end_date), "days");
    }else{
      currentDate = moment(new Date());
    }

    let indexDate = currentDate;

    const maxDate = this.queryParams.end_date ? moment(this.queryParams.end_date) : moment().add(15, 'day');

    while (currentDate.isBefore(maxDate)) {
      this.fifteenDays.push(indexDate.toDate());
      indexDate = indexDate.add(1, 'day');
    }
    this.daySelected(this.dayCurrent);
  }
  
  daySelected(day) {
    this.dayCurrent = day;
    if(this.attendancesDoctor){
      this.attendances = this.attendancesDoctor.open.filter(appointment => moment(appointment.date, "YYYY-MM-DD").isSame(moment(this.dayCurrent).format("YYYY-MM-DD")));
      this.attendancesDailyHistoric = this.attendancesDoctor.closed.filter(historic =>moment(historic.date, "YYYY-MM-DD").isSame(moment(this.dayCurrent).format("YYYY-MM-DD")));
    }
  }

  openDetail(id){
    if(this.attendancesDoctor.open.find(a => a.id === id)){
      this.router.navigateByUrl('health/attendances/detail/'+id);
    }else if(this.attendancesHistoric.find(a => a.id === id)){
      this.router.navigateByUrl('health/attendances/detail/'+id);
    }
  }


  generateDeclaration(id){
    this.appointmentsService.generateAttendanceCertificate(id).subscribe((result) => {
      /*this.uiService.showMessage(
        MessageType.success,
          'Falta marcada'
        );*/
        this.getDoctorAppointments();
      });
  }

  newAppointment(id){
    if(this.attendances.length > 0 || this.attendancesDailyHistoric.length > 0 || this.attendancesHistoric.length > 0){
      const openAttendance = this.attendances.find(attendance => attendance.id === id);
      if(this.attendances.length > 0 && openAttendance){
        this.infoModal = this.createTplModal(openAttendance, CreateAppointmentModalComponent);
      }else if(this.attendancesDailyHistoric.length > 0){
        const closedAttendance = this.attendancesDailyHistoric.find(attendance => attendance.id === id);
        if(closedAttendance){
          this.infoModal = this.createTplModal(closedAttendance, CreateAppointmentModalComponent);
        }
      }else{
        const olderAttendance = this.attendancesHistoric.find(attendance => attendance.id === id);
        if(olderAttendance){
          this.infoModal = this.createTplModal(olderAttendance, CreateAppointmentModalComponent);
        }
      }
      this.infoModal.afterClose.pipe(first()).subscribe(() => this.getDoctorAppointments());
    }
  }

  absenceAppointment(id){
    this.appointmentsService.absence(id).subscribe((result) => {
      this.uiService.showMessage(
        MessageType.success,
          'Falta marcada'
        );
        this.getDoctorAppointments();
      });
  }

  changeAppointment(id: number){
    if(this.attendances){
      this.infoModal = this.createTplModal(this.attendances.find(attendance => attendance.id === id), ChangeAppointmentModalComponent);
      this.loading = true;
      this.infoModal.afterClose.pipe(first(), finalize(() =>  {
        this.loading = false;
        this.dayCurrent = new Date();
      })).subscribe(() => this.getDoctorAppointments());
    }
  }

  createTplModal(appointment: AppointmentModel, component: any){
    return this.modalService.create({
      nzTitle: null,
      nzContent: component,
      nzFooter: null,
      nzWidth: 350,
      nzComponentParams: { appointment },
      nzWrapClassName: 'vertical-center-modal',
    });
  }

  setSpecialties(values: string[]) {
    this.queryParams.specialties = values;
    this.setUrlParams();
  }

  setStatus(values: string[]) {
    this.queryParams.status = values;
    this.setUrlParams();
  }

  setDatesAttendances(result): void {
    if(result.length){
      this.start_date = moment(result[0]).format("YYYY-MM-DD");
      this.end_date = moment(result[1]).format("YYYY-MM-DD");
      this.queryParams.start_date = this.start_date;
      this.queryParams.end_date = this.end_date;
      this.dayCurrent = moment(this.start_date).toDate();
    }else{
      this.dayCurrent = new Date();
      this.queryParams.start_date = null;
      this.queryParams.end_date = null;
    }
    this.fifteenDays = [];
    this.setUrlParams();
  }
  
  private setUrlParams() {
    this.router.navigate(['health/attendances'], { queryParams: this.queryParams.toUrlParams() });
  }

  onClear() {
    this.dateRange = [];
    this.fifteenDays = [];
    this.dayCurrent = new Date();
    this.queryParams.clear();
    this.setUrlParams();
  }

}
