import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { AttendancesRoutingModule } from './attendances-routing.module';
import { AttendancesComponent } from './attendances.component';
import { PopUpSpecialtyComponent } from '../../components/pop-up-specialty/pop-up-specialty.component';
import { PopUpStatusComponent } from '../../components/pop-up-status/pop-up-status.component';

@NgModule({
  declarations: [
    AttendancesComponent,
    PopUpSpecialtyComponent,
    PopUpStatusComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AttendancesRoutingModule,
    HealthSharedModule,
  ]
})
export class AttendancesModule { }
