import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { finalize, first } from 'rxjs/operators';
import { ConfigStatus } from '../../models/appointment.model';
import { ComplaintModel, ConfigStatusComplaint } from '../../models/complaint.model';
import { ComplaintsService } from '../../services/complaints.service';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.less']
})
export class ComplaintsComponent implements OnInit {
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  complaints: ComplaintModel[] = [];
  loading = false;
  readonly configStatus = ConfigStatus;
  readonly configStatusComplaint = ConfigStatusComplaint;
  mapOfExpandData: { [key: string]: boolean } = {};

  constructor(
    private translateService: TranslateService,
    private complaintsService: ComplaintsService
  ) { }

  ngOnInit() {
    this.getComplaints();
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
        this.pageIndex = 1;
    }
  }

  isPar(val) {
    return (val % 2 === 0);
  }

  getComplaints(){
    this.loading = true
    this.complaintsService.getComplaintsByUser().pipe(first(),finalize(()=> this.loading = false))
      .subscribe(response => {
        this.complaints = response.data;
        this.total = this.complaints.length;
      })
  }

}
