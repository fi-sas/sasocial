import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { ComplaintsComponent } from './complaints.component';
import { ComplaintsRoutingModule } from './complaints-routing.module';

@NgModule({
  declarations: [
    ComplaintsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HealthSharedModule,
    ComplaintsRoutingModule,
  ]
})
export class ComplaintsHealthModule { }
