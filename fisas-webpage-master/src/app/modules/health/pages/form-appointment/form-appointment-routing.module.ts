import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAppointmentComponent } from './form-appointment.component';


const routes: Routes = [
  {
    path: '',
    component: FormAppointmentComponent,
    data: { breadcrumb: 'HEALTH.APPOINTMENT_FORM.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormAppointmentRoutingModule { }
