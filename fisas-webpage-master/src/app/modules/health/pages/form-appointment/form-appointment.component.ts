import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { first } from 'rxjs/operators';
import { BuyModalBoxComponent } from '../../components/buy-modal-box/buy-modal-box.component';
import { AppointmentTypeModel } from '../../models/appointment-type.model';
import { AppointmentModel } from '../../models/appointment.model';
import { DoctorModel } from '../../models/doctor.model';
import { PriceVariationsModel } from '../../models/prive-variation.model';
import { SpecialtiesFormModel, SpecialtyModel } from '../../models/specialty.model';
import { AppointmentsService } from '../../services/appointments.service';


@Component({
  selector: 'app-form-appointment',
  templateUrl: './form-appointment.component.html',
  styleUrls: ['./form-appointment.component.less']
})
export class FormAppointmentComponent implements OnInit {

  loading = true;
  submitted = false;
  showErrorMessage = false;
  step = 0;
  appointmentForm = new FormGroup({
  })
  
  appointment: AppointmentModel = null;
  specialty: SpecialtyModel = null;

  buttonActionName = 'HEALTH.BUTTONS.NEXT_STEP';
  infoModal: NzModalRef;
  
  arrayValues: {
    specialty: SpecialtyModel;
    doctors: DoctorModel[];
    appointmentTypes: AppointmentTypeModel[];
    tariffs: PriceVariationsModel[];
    loggedUser : UserModel;
  } = null;

  @ViewChild('tplSuccess') sucessTemplate: TemplateRef<any>;
  @ViewChild('specialtyForm', null) specialtyForm: SpecialtiesFormModel;
  @ViewChild('userForm', null) userForm: SpecialtiesFormModel;
  @ViewChild('attachmentsForm', null) attachmentsForm: SpecialtiesFormModel;
  @ViewChild('paymentForm', null) paymentForm: SpecialtiesFormModel;

  steps = [
    {
      name: 'specialtyForm',
      formValues: {},
    },
    {
      name: 'userForm',
      formValues: {},
    },
    {
      name: 'attachmentsForm',
      formValues: {},
    },
    {
      name: 'paymentForm',
      formValues: {},
    },
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private appointmentService: AppointmentsService
  ) { }

  ngOnInit() {
  }

  back() {
    this.appointment = {} as AppointmentModel;
    this.steps.forEach((step) => {
      this.appointment = Object.assign( this.appointment, step.formValues);
    })
    if( !_.isEmpty( this.appointment)){
      this.modalService.confirm({
        nzTitle: this.translateService.instant('HEALTH.BUTTONS.INFO_CLOSE_TEXT_MODAL'),
        nzOkText: this.translateService.instant('HEALTH.APPOINTMENT_FORM.YES'),
        nzCancelText: this.translateService.instant('HEALTH.APPOINTMENT_FORM.NO'),
        nzOnOk: () => {
          this.redirect();
        }});
    }else{
      this.redirect();
    }
  }
  
  redirect(){
    this.route.params.subscribe(async (params) => {
      if ( params['id']) {
        this.router.navigate(['health', 'specialties', 'detail', params['id']]);
      }else{
        this.router.navigate(['health', 'specialties']);
      }
    });
  }

  
  nextStep(): void {
    if (this.step < this.steps.length) {
      const form = this[this.steps[this.step].name].submitForm();
      if (form !== null) {
        if (this.step === 0) {
          this.arrayValues = this[this.steps[this.step].name].getArrayValues();
        }
        this.steps[this.step].formValues = form;
        this.step++;
        this.buttonActionName = this.step === 4 ? 'HEALTH.BUTTONS.SUBMIT' : 'HEALTH.BUTTONS.NEXT_STEP';
      }
    } else {
      this.submitAppointment();
    }
  }

  getData(raw: object, fields: string[]) {
    const data = {};
    fields.forEach((key) => (data[key] = raw[key]));
    return data;
  }
  
  previousStep(): void {
    if (this.step > 0) {
      this.step--;
      this.buttonActionName = this.step === 4 ? 'HEALTH.BUTTONS.SUBMIT' : 'HEALTH.BUTTONS.NEXT_STEP';
    }
  }

  
  submitAppointment() {
    this.submitted = true;
    this.appointment = {} as AppointmentModel;
    this.steps.forEach((step) => {
      this.appointment = Object.assign(this.appointment, step.formValues);
    })
    this.specialty = this.arrayValues.specialty;
    if(this.appointment && this.appointment.price > 0 && this.appointment.payment_type === "Pré Pagamento"){
      let appointment = this.appointment;
      let specialty = this.specialty;
      this.infoModal = this.modalService.create({
        nzMask: true,
        nzWrapClassName: 'vertical-center-modal',
        nzContent: BuyModalBoxComponent,
        nzComponentParams: { appointment, specialty },
        nzFooter: null,
        nzWidth: 350
     
    });
    this.infoModal.afterClose.pipe().subscribe((result) => {
        this.confirmAppointment(result)
    });
    }else if(this.appointment && (this.appointment.price === 0 ||this.appointment.payment_type === "Ato Consulta")){
      this.confirmAppointment(true);
    }


  }

  changeStep(event) {
    if(event>=0) {
      this.step = event;
      this.buttonActionName = 'HEALTH.BUTTONS.NEXT_STEP';
    }
  }

  
  confirmAppointment(event: any){
    if(event){
      this.appointmentService.create(this.appointment).pipe(first())
      .subscribe(
        () => {
          this.modalService
            .create({
              nzTitle: null,
              nzContent: this.sucessTemplate,
              nzFooter: null,
              nzWidth: 350,
              nzWrapClassName: 'vertical-center-modal',
            }).afterClose.pipe(first()).subscribe(() => this.router.navigate(['health', 'appointments']));
        });
    }else{
      this.submitted = false;
    }
  }

}
