import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { HealthSharedModule } from '../../shared/health-shared.module';
import { FormAppointmentRoutingModule } from './form-appointment-routing.module';
import { FormAppointmentComponent } from './form-appointment.component';
import { AppointmentStepOneComponent } from '../../components/appointment-step-one/appointment-step-one.component';
import { AppointmentStepTwoComponent } from '../../components/appointment-step-two/appointment-step-two.component';
import { AppointmentStepThreeComponent } from '../../components/appointment-step-three/appointment-step-three.component';
import { AppointmentStepFourComponent } from '../../components/appointment-step-four/appointment-step-four.component';
import { AppointmentStepFiveComponent } from '../../components/appointment-step-five/appointment-step-five.component';
import { BuyModalBoxComponent } from '../../components/buy-modal-box/buy-modal-box.component';


@NgModule({
  declarations: [
      FormAppointmentComponent,
      AppointmentStepOneComponent,
      AppointmentStepTwoComponent,
      AppointmentStepThreeComponent,
      AppointmentStepFourComponent,
      AppointmentStepFiveComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormAppointmentRoutingModule,
    HealthSharedModule,
  ]
})
export class FormAppointmentModule { }
