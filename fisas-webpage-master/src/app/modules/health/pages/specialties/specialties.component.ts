import { Component, OnInit, ViewChild } from '@angular/core';
import { SpecialtyModel } from '../../models/specialty.model';
import { first, finalize } from 'rxjs/operators';
import { SpecialtiesService } from '../../services/specialties.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-specialties',
  templateUrl: './specialties.component.html',
  styleUrls: ['./specialties.component.less']
})

export class SpecialtiesComponent implements OnInit {

  selectedButton: string;

  specialties: SpecialtyModel[] = [];
  saveSpecialties: SpecialtyModel[] = [];

  loading = true;

  constructor(
    public specialtiesService: SpecialtiesService,
    private router: Router, 
  ) { }

  ngOnInit() {
    this.getSpecialties();
  }

  openDetails(id: number){
    this.router.navigateByUrl('health/specialties/detail/'+id);
  }

  getSpecialties(){
    this.specialtiesService.list().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(specialtiesResponse => {
      this.saveSpecialties = specialtiesResponse.data;
      this.filterSpecialties(this.selectedButton);
    });
  }

  filterSpecialties(button){
    if(button === this.selectedButton){
      this.specialties = this.saveSpecialties.filter(s => s.is_active)
      this.selectedButton = null;
    }else{
      switch(button.id){
        case 'internal': 
          this.specialties = this.saveSpecialties.filter(s => !s.is_external)
          this.selectedButton = button;
          break;
        case 'external': 
          this.specialties = this.saveSpecialties.filter(s => s.is_external)
          this.selectedButton = button;
          break;
        default:
          this.specialties = this.saveSpecialties.filter(s => s.is_active)
          this.selectedButton = null;
          break;
      }
    }
  }

}
