import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

import { SpecialtiesComponent } from './specialties.component';
import { SpecialtiesRoutingModule } from './specialties-routing.module';
import { CardSpecialtyComponent } from '../../components/card-specialty/card-specialty.component'
import { HealthSharedModule } from '../../shared/health-shared.module';

@NgModule({
  declarations: [
    SpecialtiesComponent,
    CardSpecialtyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SpecialtiesRoutingModule,
    HealthSharedModule,
  ]
})
export class SpecialtiesModule { }
