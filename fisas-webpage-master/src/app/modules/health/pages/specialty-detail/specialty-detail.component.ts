import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpecialtyModel } from '../../models/specialty.model';
import { SpecialtiesService } from '../../services/specialties.service';
import { finalize, first } from "rxjs/operators";
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-specialty-detail',
  templateUrl: './specialty-detail.component.html',
  styleUrls: ['./specialty-detail.component.less']
})
export class SpecialtyDetailComponent implements OnInit {
  
  loading = true;
  specialty: SpecialtyModel;

    constructor(private route: ActivatedRoute,  public specialtiesService: SpecialtiesService,
      private authService: AuthService, private router: Router) {

    }


  ngOnInit() {
    this.route.params.subscribe(async (params) => {
      if ( params['id']) {
          this.getDetail( params['id']);
      }
    })
  }

  getDetail(id){
    this.specialtiesService.getSpecialtyById(id).pipe(first(), finalize(() => this.loading = false)).subscribe((data) => {
      this.specialty = data.data[0];
    })
  }
  
  back(){
    this.router.navigate(['health/specialties']);
  }

  startAppointmentForm(){
    if (!this.authService.hasPermission('health:appointment:create')) {
      return;
    }
    this.router.navigateByUrl('health/specialties/detail/'+ this.specialty.id + '/appointment-form');
  }

}
