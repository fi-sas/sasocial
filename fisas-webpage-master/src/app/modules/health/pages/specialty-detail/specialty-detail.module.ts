import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SpecialtyDetailComponent } from './specialty-detail.component';
import { SpecialtyDetailRoutingModule } from './specialty-detail.routing.module';
import { SpecialtyTechnicalDataComponent } from '../../components/specialty-technical-data/specialty-technical-data.component'
import { SpecialtyPriceListComponent } from '../../components/specialty-price-list/specialty-price-list.component'
import { HealthSharedModule } from '../../shared/health-shared.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';

@NgModule({
  declarations: [
    SpecialtyDetailComponent,
    SpecialtyTechnicalDataComponent,
    SpecialtyPriceListComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    SpecialtyDetailRoutingModule,
    HealthSharedModule,
    AngularOpenlayersModule
  ]
})
export class SpecialtyDetailModule { }
