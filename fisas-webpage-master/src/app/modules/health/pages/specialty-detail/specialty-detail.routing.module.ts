import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialtyDetailComponent } from './specialty-detail.component';



const routes: Routes = [
  {
    path: '',
    component: SpecialtyDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialtyDetailRoutingModule { }
