import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'utcHourConvert'
})
export class UtcHourConvertPipe implements PipeTransform {

  transform(hour: string): any {
    return moment.utc(hour,"HH:mm").format('HH:mm');
    //return moment.duration(minutes, 'minutes').hours() + ":" + moment.duration(minutes, 'minutes').minutes();
  }

}
