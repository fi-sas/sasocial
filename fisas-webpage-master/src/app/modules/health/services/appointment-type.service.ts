import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { AppointmentTypeModel } from '../models/appointment-type.model';

@Injectable({
  providedIn: 'root'
})
export class AppointmentTypeService {
  
  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

    list(
      pageIndex: number = 0,
      pageSize: number = -1,
      sortKey?: string,
      sortValue?: string
    ): Observable<Resource<AppointmentTypeModel>> {
      let params = new HttpParams();
      params = params.set('offset', ((pageIndex) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
  
      if (sortKey) {
        if (sortValue === 'ascend') {
          params = params.set('sort', sortKey);
        } else {
          params = params.set('sort', '-' + sortKey);
        }
      }
      return this.resourceService.list<AppointmentTypeModel>(
        this.urlService.get('HEALTH.APPOINTMENT_TYPES'),
        { params }
      );
    }
}
