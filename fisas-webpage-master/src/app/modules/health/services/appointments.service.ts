import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { FiResourceService, FiUrlService, Resource} from '@fi-sas/core';
import { AppointmentChangeModel, AppointmentHistoricModel, AppointmentHistoricResponseModel, AppointmentModel, AppointmentsOpenClosedModel } from '../models/appointment.model'
import { AttendanceFormModel, AttendanceModel } from '../models/attendance.model';
import { ComplaintModel } from '../models/complaint.model';
import { first, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppointmentsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

historic(
      pageIndex: number = 0,
      pageSize: number = -1,
      sortKey?: string,
      sortValue?: string
    ): Observable<Resource<AppointmentsOpenClosedModel>> {
      let params = new HttpParams();
      params = params.set('offset', ((pageIndex) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
  
      if (sortKey) {
        if (sortValue === 'ascend') {
          params = params.set('sort', sortKey);
        } else {
          params = params.set('sort', '-' + sortKey);
        }
      }
      return this.resourceService.list<AppointmentsOpenClosedModel >(this.urlService.get('HEALTH.APPOINTMENTS_HISTORIC'),{ params }).pipe();
    }

    
    doctorAppointments(
      pageIndex: number,
      pageSize: number,
      params: HttpParams
    ): Observable<Resource<AppointmentsOpenClosedModel>> {
      params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
      return this.resourceService.list<AppointmentsOpenClosedModel >(this.urlService.get('HEALTH.DOCTOR_APPOINTMENTS'),{ params }).pipe();
    }

    getAppointmentById(id:number): Observable<Resource<AppointmentModel>>{
      return this.resourceService.read<AppointmentModel>(this.urlService.get('HEALTH.APPOINTMENT_DETAIL', {id}), {})
    }

    generateAttendanceCertificate(id: number): Observable<Resource<AppointmentModel>> {
      return this.resourceService.create<AppointmentModel>(this.urlService.get('HEALTH.APPOINTMENT_ATTENDANCE_CERTIFICATE',{id}), {})
    }

    absence(id: number): Observable<Resource<AppointmentModel>> {
      return this.resourceService.create<AppointmentModel>(this.urlService.get('HEALTH.ABSENCE_APPOINTMENT', {id}),{});
    }
    
    create(appointment: AppointmentModel): Observable<Resource<AppointmentModel>> {
      return this.resourceService.create<AppointmentModel>(this.urlService.get('HEALTH.CREATE_APPOINTMENT', {}), appointment);
    }

    cancel(id: number): Observable<Resource<AppointmentModel>> {
      return this.resourceService.create<AppointmentModel>(this.urlService.get('HEALTH.CANCEL_APPOINTMENT',{id}), {})
    }

    startAttendance(id: number): Observable<Resource<AttendanceModel>>{
      return this.resourceService.read<AttendanceModel>(this.urlService.get('HEALTH.START_ATTENDANCE', {id}), {});
    }

    getAttendanceInformation(id: number): Observable<Resource<AttendanceModel>>{
      return this.resourceService.read<AttendanceModel>(this.urlService.get('HEALTH.ATTENDANCE_INFORMATION', {id}), {});
    }

    createComplaint(id: number, form: any): Observable<Resource<ComplaintModel>>{
      return this.resourceService.create<ComplaintModel>(this.urlService.get('HEALTH.CREATE_COMPLAINT', {id}), {...form});
    }

    changeAppointment(id: number, newAppointment: AppointmentChangeModel): Observable<Resource<AppointmentChangeModel>> {
      return this.resourceService.create<AppointmentChangeModel>(this.urlService.get('HEALTH.CHANGE_APPOINTMENT', {id}), {...newAppointment})
    }

    getHistoric(id: number): Observable<Resource<AppointmentHistoricResponseModel>> {
      return this.resourceService.list<AppointmentHistoricResponseModel>(this.urlService.get('HEALTH.HISTORIC_APPOINTMENT', {id}),{});
    }

    closeAttendance(id: number, attendance: AttendanceFormModel): Observable<Resource<AttendanceFormModel>>{
      return this.resourceService.create<AttendanceFormModel>(this.urlService.get('HEALTH.CLOSE_ATTENDANCE', {id}), {...attendance})
    }

    generateCalendarExport(id: number, filename: string){
      const url = this.urlService.get('HEALTH.CALENDAR_EXPORT', {id})
      return this.resourceService.read(url, {}).pipe(first(), map((data => {
        this.downLoadFile(data.data, filename);
      })));
    }
    

    downLoadFile(file, filenameEvent) {
      let filename = filenameEvent ? filenameEvent : "event.ics";
      const downloadLink = document.createElement("a");
      downloadLink.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(file);
      downloadLink.download = filename;
      console.log(downloadLink)
      downloadLink.click();
    }  
    
}