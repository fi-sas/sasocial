import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { ComplaintModel } from '../models/complaint.model';

@Injectable({
  providedIn: 'root'
})
export class ComplaintsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  getComplaintsByUser(): Observable<Resource<ComplaintModel>> {
    return this.resourceService.list<ComplaintModel>(
      this.urlService.get('HEALTH.GET_USER_COMPLAINTS'),
      {}
    );
  }
}
