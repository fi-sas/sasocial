import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { DoctorModel } from '../models/doctor.model';
import { SpecialtyiesDoctorModel } from '../models/specialty.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

    list(
      pageIndex: number = 0,
      pageSize: number = -1,
      sortKey?: string,
      sortValue?: string
    ): Observable<Resource<DoctorModel>> {
      let params = new HttpParams();
      params = params.set('offset', ((pageIndex) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
  
      if (sortKey) {
        if (sortValue === 'ascend') {
          params = params.set('sort', sortKey);
        } else {
          params = params.set('sort', '-' + sortKey);
        }
      }
      return this.resourceService.list<DoctorModel>(
        this.urlService.get('HEALTH.DOCTOR'),
        { params }
      );
    }


    getSpecialtiesByDoctor(id: number): Observable<Resource<SpecialtyiesDoctorModel>>{
      return this.resourceService.list<SpecialtyiesDoctorModel>(this.urlService.get('HEALTH.DOCTOR_SPECIALTIES', {id}), {})
    }

    checkIfIsProfessional(): Observable<Resource<boolean>>{
      return this.resourceService.read<boolean>(this.urlService.get('HEALTH.IS_PROFESSIONAL',{}),{})
    }
}
