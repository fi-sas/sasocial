import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { SpecialtyModel } from '../models/specialty.model'
import { AppointmentEventModel } from '../models/appointment.model';

@Injectable({
  providedIn: 'root'
})
export class SpecialtiesService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(
    pageIndex: number = 0,
    pageSize: number = -1,
    sortKey?: string,
    sortValue?: string
  ): Observable<Resource<SpecialtyModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex) * pageSize).toString());
    params = params.set('limit', pageSize.toString());

    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }
    return this.resourceService.list<SpecialtyModel>(
      this.urlService.get('HEALTH.SPECIALTIES'),
      { params }
    );
  }

  getSpecialtyById(id: number): Observable<Resource<SpecialtyModel>> {
    return this.resourceService.read<SpecialtyModel>(this.urlService.get('HEALTH.SPECIALTY_ID', { id }));
  }

  getScheduleDoctor(id: number, doctor_id: number): Observable<Resource<AppointmentEventModel>> {
    let params = new HttpParams();
    params = params.set('doctor_id', doctor_id.toString());
    return this.resourceService.list<AppointmentEventModel>(this.urlService.get('HEALTH.SPECIALTY_SCHEDULE_DOCTOR', {id}), {params});
  }

}
