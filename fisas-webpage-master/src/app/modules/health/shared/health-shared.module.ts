import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterStatusConfigPipe } from '../pipes/filter-status-config.pipe';
import { UtcHourConvertPipe } from '../pipes/utc-hour-converted.pipe';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ChangeAppointmentModalComponent } from '../components/change-appointment-modal/change-appointment-modal.component';
import { CreateAppointmentModalComponent } from '../components/create-appointment-modal/create-appointment-modal.component';
import { CloseAppointmentModalComponent } from '../components/close-appointment-modal/close-appointment-modal.component';
import { BuyModalBoxComponent } from '../components/buy-modal-box/buy-modal-box.component';



@NgModule({
    declarations: [
        BuyModalBoxComponent,
        FilterStatusConfigPipe,
        UtcHourConvertPipe,
        ChangeAppointmentModalComponent,
        CreateAppointmentModalComponent,
        CloseAppointmentModalComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        FilterStatusConfigPipe,
        UtcHourConvertPipe
    ],
    entryComponents: [
        BuyModalBoxComponent,
        ChangeAppointmentModalComponent,
        CreateAppointmentModalComponent,
        CloseAppointmentModalComponent
    ]

})

export class HealthSharedModule { }