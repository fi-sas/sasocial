export const PANNEL_APPOINTMENT = [
    {
        active: false,
        disabled: false,
        name: 'HEALTH.APPOINTMENTS_TAB.HISTORY_APPOINTMENTS.TITLE',
        customStyle: {
          background: '#f0f2f5',
          padding: '0px',
          border: '0px',
          margin: '0px',
        },
      },
];
