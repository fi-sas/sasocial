import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    FilesService
  ]
})
export class MediaModule { }
