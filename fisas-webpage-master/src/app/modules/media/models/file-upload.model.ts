export class FileUploadModel {
  id?: number;
  file: FormData;
  file_category_id?: number;
  public?: boolean;
  weight?: number;
  name?: string;
  language_id?: number;
  created_at?: Date;
  updated_at?: Date;
}
