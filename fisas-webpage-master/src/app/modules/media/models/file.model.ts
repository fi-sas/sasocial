export class FileModel {
  file_category_id: number;
  public: boolean;
  type: string;
  mime_type: string;
  path: string;
  filename: string;
  weight:number;
  updated_at: Date;
  created_at: Date;
  id: number;
  width: number;
  height: number;
  translations: TranslationModel[];
  category: CategoryModel;
  url: string;
}

export interface NamedFileModel {
  file_id: number;
  name: string;
  file: FileModel;
}

export class TranslationModel {
  language_id: number;
  file_id: number;
  name: string;
  updated_at: Date;
  created_at: Date;
  id: number;
}

export class CategoryModel {
  id: number;
  file_category_id: number;
  name: string;
  description: string;
  updated_at: Date;
  created_at: Date;
}

export interface ReportFileModel {
  file: FileModel;
  status?: 'WAITING' | 'PROCCESSING' | 'READY' | 'FAILED';
  created_at: string;
  file_id: number;
  id: number;
  name: string;
  template_id: number;
  unread: boolean;
  user_id: number;
}
