import { TestBed } from '@angular/core/testing';

import { FilesService } from './files.service';
import { FiCoreModule } from '@fi-sas/core';

describe('FilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [
      FiCoreModule
    ]
  }));

  it('should be created', () => {
    const service: FilesService = TestBed.get(FilesService);
    expect(service).toBeTruthy();
  });
});
