import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MobilityComponent } from './mobility.component';

const routes: Routes = [
  {
    path: '',
    component: MobilityComponent,
    children: [
      { path: '', redirectTo: 'route-search', pathMatch: 'full' },
      {
        path: 'route-search',
        loadChildren: './modules/route-search/route-search.module#RouteSearchModule',
        data: { scope: 'bus:route_search' }
      },
      {
        path: 'route-detail',
        loadChildren: './modules/route-detail/route-detail.module#RouteDetailModule',
        data: { scope: 'bus:routes:read' }
      },
      {
        path: 'application-form',
        loadChildren: './modules/form-application/form-application.module#FormApplicationModule',
        data: { scope: 'bus:applications:create' }
      },

      {
        path: 'sub-23-declaration-form',
        loadChildren: './modules/form-sub23-declaration/form-sub23-declaration.module#FormSub23DeclarationModule',
        data: { scope: 'bus:sub23_declarations:create' }
      },
      {
        path: 'application-view',
        loadChildren: './modules/view-application/view-application.module#ViewApplicationModule',
        data: { scope: 'bus:applications:read' }
      },
      {
        path: 'applications',
        loadChildren: './modules/applications/applications.module#ApplicationsModule',
        data: { scope: 'bus:applications' }
      },
      {
        path: 'sub-23-declarations',
        loadChildren: './modules/sub23-declarations/sub23-declarations.module#Sub23DeclarationsModule',
        data: { scope: 'bus:sub23_declarations' }
      },
      {
        path: 'sub-23-declaration-view',
        loadChildren: './modules/view-sub23-declaration/view-sub23-declaration.module#ViewSub23DeclarationModule',
        data: { scope: 'bus:sub23_declarations:read' }
      },
      {
        path: 'tickets',
        loadChildren: './modules/bus-tickets/bus-tickets.module#BusTicketsModule',
        data: { scope: 'bus:tickets_bougth:read' }
      },
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MobilityRoutingModule { }
