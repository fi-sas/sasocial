import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-mobility',
  templateUrl: 'mobility.component.html',
  styleUrls: ['./mobility.component.less']
})

export class MobilityComponent implements OnInit {

  barOptions:any[] = [];

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.barOptions = [
      {
        translate: 'MOBILITY.ROUTE_SEARCH.ROUTE_SEARCH',
        disable: true,
        routerLink: '/mobility/route-search',
        scope: this.authService.hasPermission('bus:route_search')
      },
      {
        translate: 'MOBILITY.ROUTE_SEARCH.TICKETS',
        disable: true,
        routerLink: '/mobility/tickets',
        scope: this.authService.hasPermission('bus:tickets_bought:read')
      },
      {
        translate: 'MOBILITY.ROUTE_SEARCH.SUB23_PASS_DECLARATION',
        disable: false,
        routerLink: '/mobility/sub-23-declarations',
        scope: this.authService.hasPermission('bus:applications:create')
      },
      {
        translate: 'MOBILITY.ROUTE_SEARCH.PASS_APPLICATION',
        disable: false,
        routerLink: '/mobility/applications',
        scope: this.authService.hasPermission('bus:applications:create')
      }
    ]
  }

}
