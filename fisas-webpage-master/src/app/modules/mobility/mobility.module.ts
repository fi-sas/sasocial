import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobilityComponent } from './mobility.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MobilityRoutingModule } from './mobility-routing.module';
import { MobilityService } from './services/mobility.service';
import { RoutesService } from './services/routes.service';
import { LocalsService } from './services/locals.service';

@NgModule({
  declarations: [MobilityComponent],
  imports: [
    CommonModule,
    SharedModule,
    MobilityRoutingModule
  ],
  providers: [
    LocalsService,
    RoutesService,
    MobilityService,
  ]
})
export class MobilityModule { }
