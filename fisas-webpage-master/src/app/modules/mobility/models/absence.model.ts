export class AbsenceModel {
    id?: number;
    application_id?: number;
    user_id?: number;
    start_date: string;
    end_date: string;
    justification: string;
    file_id: number;
    updated_at?: string;
    created_at?: string;
}
