import { SchoolModel } from '@fi-sas/webpage/shared/models/school.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { LocalModel } from './local.model';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { FileModel } from '../../media/models/file.model';

export enum ApplicationStatus {
    SUBMITTED = 'submitted',
    ACCEPTED = 'accepted',
    REJECTED = 'rejected',
    CLOSED = 'closed',
    WITHDRAWAL = 'withdrawal',
    QUITING = 'quiting',
    SUSPENDED = 'suspended',
    APPROVED = "approved",
    NOT_APPROVED = "not_approved"
};

export class ApplicationModel {
    id?: number;
    academic_year: string;
    name: string;
    birth_date: Date;
    student_number: string;
    tin: string;
    identification: string;
    nationality: string;
    course_year: number;
    date: Date;
    email: string;
    phone_1: string;
    phone_2: string;
    course_id: number;
    course?: CourseModel;
    renovation: boolean;
    school_id: number;
    school?: SchoolModel;
    observations: string;
    accept_procedure: boolean;
    morning_local_id_from: number;
    morning_local_from?: LocalModel;
    morning_local_id_to: number;
    morning_local_to?: LocalModel;
    afternoon_local_id_from: number;
    afternoon_local_from?: LocalModel;
    afternoon_local_id_to: number;
    afternoon_local_to?: LocalModel;
    status: ApplicationStatus;
    document_type: DocumentTypeModel;
    declaration_file_id: number;
    declaration?: FileModel;
}