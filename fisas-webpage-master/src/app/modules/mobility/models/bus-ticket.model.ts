import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { LocalModel } from "./local.model";
import { RouteModel } from "./route.modal";

export class BusTicketModel {
  id: number;
  price_id: number;
  route_id: number;
  departure_local_id: number;
  departure_local: LocalModel;
  arrival_local_id: number;
  arrival_local: LocalModel;
  user_id: number;
  movement_id: number;
  has_canceled: boolean;
  validate_date: Date;
  used: boolean;
  hash: string;
  created_at: Date;
  updated_at: Date;
  user?: UserModel;
  route?: RouteModel;
}
