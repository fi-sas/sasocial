export class LocalModel {
    id: number;
    latitude: number;
    longitude: number;
    local: string;
    region: string;
    address: string;
    institute: boolean;
}