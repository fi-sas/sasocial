import { ContactModel } from '@fi-sas/webpage/shared/models/contact.model';

export class RouteStepModel {
    local: string;
    local_id: number;
    region: string;
    lat: number;
    lng: number;
    instruction: string;
    departure_time: string;
    duration: string;
    distance: string;
    route_line: string;
    vehicle: string
}

export class RoutePriceModel {
    prod_cod: number;
    tax_id: number;
    value: string;
    symbol: string;
    type: string;
    account_id: number;
    purchase_type: string;
    ticket_type_id: number;
}

export class RoutePriceDataModel {
    route_id: number;
    route: string;
    departure_local: string;
    arrival_local: string;
    departure_hour: string;
    arrival_hour: string;
    purchare_active: boolean;
    price: RoutePriceModel[];
}
export class RouteDataModel {
    route_id: number;
    route_line: string;
    steps: RouteStepModel[];
    prices: RoutePriceDataModel;
    contacts: ContactModel;
}

export class RouteModel {
    route: RouteDataModel[];
    name: string;
    stops: number;
    duration: string;
    departure_hour: string;
    arrival_hour: string;
    departure_place: string;
    arrival_place: string;
    date: string;
    tickets_prices_total: number;
    steps: RouteStepModel[];
    prices: { ticket_type: string, hash: string, value: number }[];
    route_line: string;
    contacts: ContactModel;
}
