import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";

export class Sub23BusApplicationModel {
    id: number;
    academic_year: string;
    name: string;
    address: string;
    postal_code: string;
    country: string;
    document_type_id: number;
    identification: string;
    birth_date: boolean;
    organic_unit_id: number;
    course_id: number;
    course_year: number;
    has_social_scholarship: boolean;
    status: string;
    user_id: number;
    created_at: string;
    updated_at: string;
    course: CourseModel;
    user: UserModel;
    organic_unit: OrganicUnitModel;
    document_type: DocumentTypeModel;
    observations: string;
}

