
export enum WithdrawalStatus {
    PENDING = "pending",
    ACCEPTED = "accepted",
    REJECTED = "rejected"
};


export class WithdrawalModel {
    application_id: number;
    date_withdrawal: Date;
    id: number;
    justification: string;
    status: WithdrawalStatus;
    created_at: Date;
    updated_at: Date;
}