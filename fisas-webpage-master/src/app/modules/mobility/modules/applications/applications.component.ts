import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { first, finalize } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { WithdrawalService } from '../../services/withdrawal.service';
import { WithdrawalModel } from '../../models/withdrawal.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbsenceModel } from '../../models/absence.model';
import { AbsencesService } from '../../services/absences.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.less']
})
export class ApplicationsComponent implements OnInit {
  tplModal: NzModalRef;
  tplModalButtonLoading = false;
  withdrawalDecision = '';
  application: ApplicationModel = null;
  loading = false;
  academic_year;

  abcenseFormGroup: FormGroup = this.formBuilder.group({
    range_date: ['', [Validators.required]],
    justification: ['', [Validators.required, trimValidation, Validators.maxLength(255)]],
    file: [null]
  });
  applications: ApplicationModel[] = [];
  ApplicationStatusLabels = {
    'approved': { color: '#0d69dd', translation: 'MOBILITY.STATUS.APPROVED' },
    'not_approved': { color: '#C1666B', translation: 'MOBILITY.STATUS.NOT_APPROVED' },
    'accepted': { color: '#9DBBAE', translation: 'MOBILITY.STATUS.ACCEPTED' },
    'rejected': { color: '#D0021B', translation: 'MOBILITY.STATUS.NOT_APPROVED' },
    'submitted': { color: '#4D9DE0', translation: 'MOBILITY.STATUS.SUBMITTED' },
    'canceled': { color: '#A4A4A4', translation: 'MOBILITY.STATUS.CANCELLED' },
    'withdrawal': { color: '#246A73', translation: 'MOBILITY.STATUS.WITHDRAWAL' },
    'closed': { color: '#000000', translation: 'MOBILITY.STATUS.CLOSED' },
    'quiting': { color: '#EF476F', translation: 'MOBILITY.STATUS.QUITING' },
    'suspended': { color: '#8EA4D2', translation: 'MOBILITY.STATUS.SUSPENDED' },
  }

  constructor(
    private router: Router,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private applicationsService: ApplicationsService,
    private withdrawalsService: WithdrawalService,
    private uiService: UiService,
    private formBuilder: FormBuilder,
    private absencesService: AbsencesService,
    private configurationsServices: ConfigurationsService,
  ) { }

  ngOnInit() {
    this.getCurrentAcademicYear();
    this.loadApplications();
  }

  getCurrentAcademicYear(){
    this.configurationsServices.getCurrentAcademicYear().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.academic_year = result.data[0].academic_year;
    });
  }

  loadApplications() {
    this.loading = true;
    this.applicationsService.list().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.applications = result.data;
    });
  }

  back() {
    this.router.navigate(['mobility', 'route-search']);
  }


  canCreateApplication() {
    if (this.loading) {
      return true;
    }
    for (const applitcation of this.applications) {
      if (
        ( applitcation.status === 'submitted' ||
        applitcation.status === 'accepted' ||
        applitcation.status === 'quiting') && applitcation.academic_year === this.academic_year
      ) {
        return true;
      }
    }
    return false;
  }

  cancelApplication(application: ApplicationModel) {
    if (application.status === 'submitted') {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MOBILITY.CANCEL_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('MOBILITY.CANCEL_APPLICATION.DESCRIPTION'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOkType: 'primary',
        nzOnOk: () => {
          return new Promise((resolve, reject) => {
            this.applicationsService.cancel(application.id)
              .pipe(
                first(),
                finalize(() => {
                  this.loading = false;
                  resolve();
                })
              ).subscribe(() => {
                this.loadApplications();
                resolve();
              });
          });
        },
        nzOnCancel: () => { }
      });
    }
  }

  resumeApplication(application: ApplicationModel) {
    if (application.status === 'suspended') {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MOBILITY.RESUME_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('MOBILITY.RESUME_APPLICATION.DESCRIPTION'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOkType: 'primary',
        nzOnOk: () => {
          return new Promise((resolve, reject) => {

            this.applicationsService.resume(application.id)
              .pipe(
                first(),
                finalize(() => {
                  this.loading = false;
                  resolve();
                })
              ).subscribe(() => {
                this.loadApplications();
                resolve();
              });
          });
        },
        nzOnCancel: () => { }
      });
    }
  }

  acceptApplication(application: ApplicationModel) {
    if (application.status === "approved") {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MOBILITY.ACCEPT_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('MOBILITY.ACCEPT_APPLICATION.DESCRIPTION'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOkType: 'primary',
        nzOnOk: () => {
          return new Promise((resolve, reject) => {

            this.applicationsService.accept_reject_application(application.id, true)
              .pipe(
                first(),
                finalize(() => {
                  this.loading = false;
                  resolve();
                })
              ).subscribe(() => {
                this.loadApplications();
                resolve();
              });
          });
        },
        nzOnCancel: () => { }
      });
    }

  }
  rejectApplication(application: ApplicationModel) {
    if (application.status === "approved") {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MOBILITY.REJECT_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('MOBILITY.REJECT_APPLICATION.DESCRIPTION'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOkType: 'primary',
        nzOnOk: () => {
          return new Promise((resolve, reject) => {

            this.applicationsService.accept_reject_application(application.id, false)
              .pipe(
                first(),
                finalize(() => {
                  this.loading = false;
                  resolve();
                })
              ).subscribe(() => {
                this.loadApplications();
                resolve();
              });
          });
        },
        nzOnCancel: () => { }
      });


    }

  }


  withdrawalApplicationModal(application: ApplicationModel, tplContent: TemplateRef<{}>): void {
    this.application = application;
    this.withdrawalDecision = '';
    if (application.status === 'accepted') {
      this.tplModal = this.modalService.create({
        nzTitle: null,
        nzContent: tplContent,
        nzFooter: null,
        nzMaskClosable: false,
        nzClosable: true
      });
    }
  }

  withdrawalApplicationSave(): void {
    this.tplModalButtonLoading = true;
    const withdrawal = new WithdrawalModel();
    withdrawal.application_id = this.application.id;
    withdrawal.justification = this.withdrawalDecision;
    this.withdrawalsService.create(withdrawal)
      .pipe(
        first(),
        finalize(() => this.tplModalButtonLoading = false))
      .subscribe(() => {
        this.loadApplications();
        this.withdrawalApplicationClose();
        this.uiService.showMessage(MessageType.success, this.translateService.instant('MOBILITY.WITHDRAWALS.SUBMITTED_SUCCESSFULLY'));
      });
  }

  withdrawalApplicationClose(): void {
    this.application = null;
    this.withdrawalDecision = '';
    this.tplModalButtonLoading = false;
    this.tplModal.destroy();
  }


  abcenceApplicationModal(application: ApplicationModel,tplContent: TemplateRef<{}>): void {
    this.abcenseFormGroup.reset();
    this.application = application;
    if (application.status === 'accepted') {
      this.tplModal = this.modalService.create({
        nzTitle: null,
        nzMaskClosable: false,
        nzContent: tplContent,
        nzClosable: true,
        nzFooter: null
      });
    }
  }

  valid = true;
  abcenseApplicationSave() {
    this.valid = this.checkValidityOfControls([
      this.abcenseFormGroup.get('range_date'),
      this.abcenseFormGroup.get("justification"),
      this.abcenseFormGroup.get("file"),
    ]);
    if (!this.valid) return;
    this.tplModalButtonLoading = true;

    const absence = new AbsenceModel();
    absence.application_id = this.application.id;
    absence.start_date = this.abcenseFormGroup.controls.range_date.value[0];
    absence.end_date = this.abcenseFormGroup.controls.range_date.value[1];
    absence.file_id = this.abcenseFormGroup.controls.file.value;
    absence.justification = this.abcenseFormGroup.controls.justification.value;

    this.absencesService.create(absence).pipe(
      first(),
      finalize(() => this.tplModalButtonLoading = false))
      .subscribe(() => {
        this.abcenseApplicationClose();
        this.uiService.showMessage(MessageType.success, this.translateService.instant('MOBILITY.APPLICATIONS.ABSENCE_SUCCEFULY_CREATED'));
      });
  }
  abcenseApplicationClose(): void {
    this.application = null;
    this.tplModalButtonLoading = false;
    this.tplModal.destroy();
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find((c) => c.status !== "VALID");
  }

  get f() { return this.abcenseFormGroup.controls; }

  filesList = [];
  onFileDeleted(fileId) {
    this.abcenseFormGroup.get('file').setValue(null);
  }


}
