import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationsComponent } from './applications.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ApplicationsService } from '../../services/applications.service';

@NgModule({
  declarations: [ApplicationsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ApplicationsRoutingModule
  ], providers: [
    ApplicationsService
  ]
})
export class ApplicationsModule { }
