import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusTicketsComponent } from './bus-tickets.component';
import { BusViewTicketComponent } from './bus-view-ticket/view-ticket.component';

const routes: Routes = [
  {
    path: '',
    component: BusTicketsComponent,
    data: { breadcrumb: 'MOBILITY.ROUTE_SEARCH.TICKETS' },
  },
  {
    path: ':id',
    component: BusViewTicketComponent,
    data: { breadcrumb: 'MOBILITY.ROUTE_SEARCH.TICKETS' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusTicketsRoutingModule { }
