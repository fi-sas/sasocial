import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { TicketsModel } from "@fi-sas/webpage/modules/queue/models/tickets.model";
import * as moment from "moment";
import { delay, finalize, first } from "rxjs/operators";
import { BusTicketModel } from "../../models/bus-ticket.model";
import { TicketsService } from "../../services/tickets.service";

@Component({
  selector: "app-bus-tickets",
  templateUrl: "./bus-tickets.component.html",
  styleUrls: ["./bus-tickets.component.less"],
})
export class BusTicketsComponent implements OnInit {
  used_filter = false;
  month_filter = new Date();

  tickets: BusTicketModel[] = [];
  loading = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private ticketsBoughtService: TicketsService
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(first()).subscribe(() => {
      const params = this.activatedRoute.snapshot.data;

      if (params.used) {
        this.used_filter = params.used;
      }

      if (params.month) {
        this.month_filter = moment(params.month, "YYYY-MM").toDate();
      }

      const errorRoutingTimeout = setTimeout(() => {
        clearTimeout(errorRoutingTimeout);
        this.updateQuery();
      }, 200)

      this.loadTickets();
    });
  }

  loadTickets() {
    this.loading = true;

    this.ticketsBoughtService
      .getTickets(this.used_filter, this.month_filter)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((res) => (this.tickets = res.data));
  }

  back() {
    this.router.navigate(["mobility", "route-search"]);
  }

  canCreateDeclaration() {
    return false;
  }

  filterChanged() {
    this.updateQuery();
    this.loadTickets();
  }

  updateQuery() {
    const queryParams: Params = {
      used: this.used_filter,
      month: moment(this.month_filter).format("YYYY-MM"),
    };
    this.router.navigate([], { queryParams, relativeTo: this.activatedRoute  });

  }

}
