import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusTicketsRoutingModule } from './bus-tickets-routing.module';
import { BusTicketsComponent } from './bus-tickets.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { BusViewTicketComponent } from './bus-view-ticket/view-ticket.component';

@NgModule({
  declarations: [
    BusTicketsComponent,
    BusViewTicketComponent
  ],
  imports: [
    CommonModule,
    BusTicketsRoutingModule,
    SharedModule,
  ]
})
export class BusTicketsModule { }
