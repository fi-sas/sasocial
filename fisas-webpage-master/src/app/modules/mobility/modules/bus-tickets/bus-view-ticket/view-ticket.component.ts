import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { BusTicketModel } from '../../../models/bus-ticket.model';
import { TicketsService } from '../../../services/tickets.service';
import QRCode from 'qrcode'


@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.less']
})
export class BusViewTicketComponent implements OnInit {

  ticket: BusTicketModel;
  ticket_data = null;
  ticket_url = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ticketsService: TicketsService,
    private locationService: Location,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(first()).subscribe(params => {
      this.ticketsService.getTicket(params.id).pipe(first()).subscribe(res => {
        this.ticket = res.data[0];
        if(res.data[0].hash) {
          this.ticket_data = res.data[0].hash;
          this.drawQRCode();
        }
      });
    })
  }

  drawQRCode() {
    QRCode.toDataURL(this.ticket_data)
    .then(url => {
      this.ticket_url = url;
    })
    .catch(err => {
      console.error(err)
    })
  }

  back() {
    this.locationService.back();
  }

}
