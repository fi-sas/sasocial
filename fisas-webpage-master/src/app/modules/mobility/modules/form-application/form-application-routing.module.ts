import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormApplicationComponent } from './form-application.component';

const routes: Routes = [
  {
    path: '',
    component: FormApplicationComponent,
    data: { breadcrumb: 'MOBILITY.APPLICATION_FORM.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormApplicationRoutingModule { }
