import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from '../../services/applications.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first, finalize } from 'rxjs/operators';
import { countries } from '../../../../shared/data/countries';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { LocalModel } from '../../models/local.model';
import { LocalsService } from '../../services/locals.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { SchoolModel } from '@fi-sas/webpage/shared/models/school.model';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { environment } from 'src/environments/environment';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { FormInformationTextsModel, TranslationsModelFormInformation } from '@fi-sas/webpage/shared/models/form-information-texts.model';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { Resource } from '@fi-sas/core';
import { AcademicYear } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less']
})
export class FormApplicationComponent implements OnInit {
  loading = false;

  countries = countries;

  loading_courses = false;
  courses: CourseModel[] = [];
  filtered_courses: CourseModel[] = [];
  submitted = false
  loading_course_degrees = false;
  course_degrees: CourseDegreeModel[] = [];

  loading_schools = false;
  schools: OrganicUnitModel[] = [];

  loading_locals = false;
  locals: LocalModel[] = [];
  showErrorMessage = false;
  current = 0;
  index = 'identification';

  student_number_disabled = true;
  course_disabled = false;


  regulation_url = environment.mobility_regulation_url;

  applicationGroup = new FormGroup({
    academic_year: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required,trimValidation]),
    student_number: new FormControl('', [Validators.required,trimValidation]),
    birth_date: new FormControl(new Date(), [Validators.required]),
    tin: new FormControl('', [Validators.required,trimValidation]),
    document_type_id: new FormControl(null, [Validators.required]),
    identification: new FormControl('', [Validators.required,trimValidation]),
    nationality: new FormControl(null, [Validators.required]),
    course_year: new FormControl(1, [Validators.required]),
    date: new FormControl(new Date(), [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email,trimValidation]),
    phone_1: new FormControl('', [Validators.required,trimValidation]),
    phone_2: new FormControl('', [trimValidation]),
    course_degree_id: new FormControl(null, [Validators.required]),
    course_id: new FormControl(null, [Validators.required]),
    renovation: new FormControl(null, [Validators.required]),
    school_id: new FormControl(null, [Validators.required]),
    observations: new FormControl('', [Validators.maxLength(255),trimValidation]),
    accept_procedure: new FormControl(false, [Validators.requiredTrue]),
    morning_local_id_from: new FormControl(null, [Validators.required]),
    morning_local_id_to: new FormControl(null, [Validators.required]),
    afternoon_local_id_from: new FormControl(null, [Validators.required]),
    afternoon_local_id_to: new FormControl(null, [Validators.required]),
  });
  openInfo: boolean = false;
  info: FormInformationTextsModel[] = [];
  infoApresent: TranslationsModelFormInformation[] = [];
  documentTypes: DocumentTypeModel[] = [];
  loggedUser: UserModel = null;
  academicYear: AcademicYear = null;

  constructor(
    private router: Router,
    private applicationsService: ApplicationsService,
    private authService: AuthService,
    private configurationsService: ConfigurationsService,
    private localsService: LocalsService,
    private uiService: UiService,
    private organicUnitsService: OrganicUnitsService,
    private sanitizer: DomSanitizer,
    private configurationsServices: ConfigurationsService,
  ) { }

  ngOnInit() {
    this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      const user = res.data[0];
      this.loggedUser = user;
      if (!user.student_number) {
        this.student_number_disabled = false;
      }

      this.applicationGroup.patchValue({
        name: user.name,
        student_number: user.student_number,
        birth_date: user.birth_date,
        tin: user.tin,
        document_type_id: user.document_type_id,
        identification: user.identification,
        nationality: user.country,
        email: user.email,
        phone_1: user.phone,
        course_year: user.course_year,
        school_id: user.organic_unit_id,
        course_degree_id: user.course_degree_id,
        course_id: user.course_id,
      });
      this.filterCourses();
    });
    this.getCurrentAcademicYear();
    this.loadCourseDegrees();
    this.loadOrganicUnits();
    this.loadLocals();
    this.getInfo();
    this.getDocumentTypes();
   
  }


  getCurrentAcademicYear() {
    this.configurationsServices.getCurrentAcademicYear().pipe(first()).subscribe(response => {
      if(response.data.length){
        this.academicYear = response.data[0];
        this.applicationGroup.get('academic_year').disable();
        this.applicationGroup.get('academic_year').setValue(this.academicYear.academic_year);
      }
    });
  }

  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }

  getInfo() {
    this.configurationsService.getInfoForms(3).pipe(first()).subscribe((data) => {
      this.info = data.data;
      if (this.existInfoStep('MOBILITY_APPLICATION_STEP_1')) {
        this.openInfo = true;
      }
    })
  }
  getHTML(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  existInfoStep(key: string): boolean {
    this.infoApresent = [];
    let aux = this.info.find((data) => data.key == key);
    if (aux && aux.translations.length > 0 && aux.visible) {
      this.infoApresent = aux.translations;
      return true;
    }
    return false;
  }

  loadCourseDegrees() {
    this.loading_course_degrees = true;
    this.configurationsService.courseDegrees().pipe(
      first(),
      finalize(() => this.loading_course_degrees = false)
    ).subscribe(result => {
      this.course_degrees = result.data;
    });
  }


  filterDegrees() {
    this.applicationGroup.get('course_id').value ? this.applicationGroup.get('course_id').setValue(null) : '';
    this.applicationGroup.get('course_degree_id').value ? this.applicationGroup.get('course_degree_id').setValue(null) : '';
  }

  filterCourses() {
    this.configurationsService.courses(this.applicationGroup.get("course_degree_id").value, this.applicationGroup.get("school_id").value).pipe(
      first(),
      finalize(() => this.loading_courses = false)
    ).subscribe(result => {
      this.courses = result.data;
      this.filtered_courses = this.courses;
    });
  }

  loadOrganicUnits() {
    this.loading_schools = true;
    this.organicUnitsService.organicUnitsbySchools().pipe(
      first(),
      finalize(() => this.loading_schools = false)
    ).subscribe(result => {
      this.schools = result.data;
    });
  }

  loadLocals() {
    this.loading_locals = true;
    this.localsService.list(true).pipe(
      first(),
      finalize(() => this.loading_locals = false)
    ).subscribe(result => {
      this.locals = result.data;
    });
  }

  pre(): void {
    this.openInfo = false;
    this.current -= 1;
    this.changeContent();
    this.showErrorMessage = false;
  }

  next(): void {
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = this.checkValidityOfControls([
          this.applicationGroup.get('academic_year'),
          this.applicationGroup.get('name'),
          this.applicationGroup.get('birth_date'),
          this.applicationGroup.get('nationality'),
          this.applicationGroup.get('tin'),
          this.applicationGroup.get('identification'),
          this.applicationGroup.get('student_number'),
          this.applicationGroup.get('course_year'),
          this.applicationGroup.get('school_id'),
          this.applicationGroup.get('course_degree_id'),
          this.applicationGroup.get('course_id'),
        ]);
        break;
      }
      case 1: {
        valid = this.checkValidityOfControls([
          this.applicationGroup.get('email'),
          this.applicationGroup.get('phone_1'),
          this.applicationGroup.get('phone_2'),
        ]);
        break;
      }
      case 2: {
        valid = this.checkValidityOfControls([
          this.applicationGroup.get('renovation'),
          this.applicationGroup.get('morning_local_id_from'),
          this.applicationGroup.get('morning_local_id_to'),
          this.applicationGroup.get('afternoon_local_id_from'),
          this.applicationGroup.get('afternoon_local_id_to'),
        ]);
        break;
      }
      case 3: {
        valid = this.checkValidityOfControls([
          this.applicationGroup.get('observations'),
        ]);
        break;
      }
      case 4: {
        valid = this.checkValidityOfControls([
          this.applicationGroup.get('accept_procedure'),
          this.applicationGroup.get('phone_1'),
          this.applicationGroup.get('phone_2'),
        ]);
        this.submitted = true;
        break;
      }
      case 5: {
        valid = true;
        break;
      }
    }

    if (valid) {
      this.openInfo = false;
      this.submitted = false;
      this.current += 1;
      this.changeContent();
      this.showErrorMessage = false;
    }else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }

  }


  checkValidityOfControls(controls: AbstractControl[]): boolean {

    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }
    return !controls.find(c => c.status !== 'VALID' && c.status !== 'DISABLED' )
  }

  done() {
    if (!this.authService.hasPermission('bus:applications:create')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    for (const i in this.applicationGroup.controls) {
      if (i) {
        this.applicationGroup.controls[i].markAsDirty();
        this.applicationGroup.controls[i].updateValueAndValidity();
      }
    }

    if (!this.applicationGroup.valid) {
      return;
    }

    this.loading = true;
    this.applicationsService.create(this.applicationGroup.getRawValue()).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      this.uiService.showMessage(MessageType.success, 'Candidatura criada com sucesso!');
      this.router.navigate(['mobility', 'applications']);
    });
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'identification';
        if (this.existInfoStep('MOBILITY_APPLICATION_STEP_1')) {
          this.openInfo = true;
        }
        break;
      }
      case 1: {
        this.index = 'contacts';
        if (this.existInfoStep('MOBILITY_APPLICATION_STEP_2')) {
          this.openInfo = true;
        }
        break;
      }
      case 2: {
        this.index = 'application';
        if (this.existInfoStep('MOBILITY_APPLICATION_STEP_3')) {
          this.openInfo = true;
        }
        break;
      }
      case 3: {
        this.index = 'observations';
        if (this.existInfoStep('MOBILITY_APPLICATION_STEP_4')) {
          this.openInfo = true;
        }
        break;
      }
      case 4: {
        this.index = 'regulamentation';
        if (this.existInfoStep('MOBILITY_APPLICATION_STEP_5')) {
          this.openInfo = true;
        }
        break;
      }
      case 5: {
        this.index = 'revision';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }
  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentTypes.find(x => x.id == document_type_id)
    return document_type ? document_type.translations.find(x => x.language_id == 3).description : "--";
  }

  goTo(current: number) {
    this.current = current;
    this.changeContent();
  }

  back() {
    this.router.navigate(['mobility', 'applications']);
  }

  getInputError(field: string) {

    return this.applicationGroup.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.applicationGroup.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.applicationGroup.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.applicationGroup.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.applicationGroup.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.applicationGroup.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.applicationGroup.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.applicationGroup.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  getSchool(id: number): OrganicUnitModel {
    return this.schools.find(s => s.id === id);
  }
  getCourse(id: number): CourseModel {
    return this.courses.find(s => s.id === id);
  }
  getCourseDegree(id: number): CourseDegreeModel {
    return this.course_degrees.find(s => s.id === id);
  }
  getLocal(id: number): LocalModel {
    return this.locals.find(s => s.id === id);
  }
}
