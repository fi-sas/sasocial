import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormApplicationRoutingModule } from './form-application-routing.module';
import { FormApplicationComponent } from './form-application.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [FormApplicationComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormApplicationRoutingModule
  ]
})
export class FormApplicationModule { }
