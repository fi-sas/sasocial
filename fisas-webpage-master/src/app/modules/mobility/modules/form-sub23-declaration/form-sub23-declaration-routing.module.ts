import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormSub23DeclarationComponent } from './form-sub23-declaration.component';

const routes: Routes = [
  {
    path: '',
    component: FormSub23DeclarationComponent,
    data: { breadcrumb: 'MOBILITY.APPLICATION_FORM.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormSub23DeclarationRoutingModule { }
