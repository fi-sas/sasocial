import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSub23DeclarationComponent } from './form-sub23-declaration.component';

describe('FormSub23DeclarationComponent', () => {
  let component: FormSub23DeclarationComponent;
  let fixture: ComponentFixture<FormSub23DeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSub23DeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSub23DeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
