import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { FormInformationTextsModel, TranslationsModelFormInformation } from '@fi-sas/webpage/shared/models/form-information-texts.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { Sub23DeclarationsService } from '../../services/sub23-declarations.service';

@Component({
  selector: 'app-form-sub23-declaration',
  templateUrl: './form-sub23-declaration.component.html',
  styleUrls: ['./form-sub23-declaration.component.less']
})
export class FormSub23DeclarationComponent implements OnInit {

  loading = true;
  showErrorMessage = false;
  openInfo = false;
  info: FormInformationTextsModel[] = [];
  infoApresent: TranslationsModelFormInformation[] = [];
  submitted = false;
  loggedUser: UserModel = null;
  declarationGroup = new FormGroup({
    academic_year: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, trimValidation]),
    address: new FormControl('', [Validators.required,trimValidation]),
    postal_code: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required,trimValidation]),
    locality: new FormControl('', [Validators.required,trimValidation]),
    document_type_id: new FormControl(null, [Validators.required]),
    identification: new FormControl('', [Validators.required,trimValidation]),
    birth_date: new FormControl(new Date(), [Validators.required]),
    organic_unit_id: new FormControl(null, [Validators.required]),
    course_id: new FormControl(null, [Validators.required]),
    course_degree_id: new FormControl(null, [Validators.required]),
    course_year: new FormControl(null, [Validators.required]),
    has_social_scholarship: new FormControl(null, [Validators.required]),
    applied_social_scholarship: new FormControl(null, [Validators.required]),
    observations: new FormControl('', [trimValidation]),
    cc_emitted_in: new FormControl('')
  });

  courses: CourseModel[] = [];
  filtered_courses: CourseModel[] = [];
  loading_courses = false;

  loading_schools = false;
  schools: OrganicUnitModel[] = [];
  loading_course_degrees = false;

  course_degrees: CourseDegreeModel[] = [];
  documentTypes: DocumentTypeModel[] = [];

  current = 0;
  index = 'identification';


  constructor(
    private router: Router,
    private authService: AuthService,
    private configurationsService: ConfigurationsService,
    private uiService: UiService,
    private organicUnitsService: OrganicUnitsService,
    private sanitizer: DomSanitizer,
    private sub23DeclarationServices: Sub23DeclarationsService
  ) { }

  ngOnInit() {
    this.authService.getUserInfoByToken().pipe(first()).subscribe(res => {
      const user = res.data[0];
      this.loggedUser = user;
      this.declarationGroup.patchValue({
        name: user.name,
        birth_date: user.birth_date,
        document_type_id: user.document_type_id,
        identification: user.identification,
        course_year: user.course_year,
        organic_unit_id: user.organic_unit_id,
        course_degree_id: user.course_degree_id,
        course_id: user.course_id,
        address: user.address,
        postal_code: user.postal_code,
      });
      this.filterCourses();
      this.loading = false;
    });
    this.loadCourseDegrees();
    this.loadOrganicUnits();
    this.getInfo();
    this.getDocumentTypes();
    this.getActualAcademicYear();
  }
  getInfo() {
    this.configurationsService.getInfoForms(3).pipe(first()).subscribe((data) => {
      this.info = data.data;
      if (this.existInfoStep('MOBILITY_SUB23_DECLARATION_STEP_1')) {
        this.openInfo = true;
      }
    })
  }
  getActualAcademicYear() {
    this.configurationsService.getCurrentAcademicYear().subscribe(res => {
      if (res.data.length > 0)
        this.declarationGroup.controls.academic_year.setValue(res.data[0].academic_year);
    });
  }
  getHTML(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }
  existInfoStep(key: string): boolean {
    this.infoApresent = [];
    let aux = this.info.find((data) => data.key == key);
    if (aux && aux.translations.length > 0 && aux.visible) {
      this.infoApresent = aux.translations;
      return true;
    }
    return false;
  }

  filterCourses() {
    this.configurationsService.courses(this.declarationGroup.get("course_degree_id").value, this.declarationGroup.get("organic_unit_id").value).pipe(
      first(),
      finalize(() => this.loading_courses = false)
    ).subscribe(result => {
      this.courses = result.data;
      this.filtered_courses = this.courses;
    });
  }
  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;
    })
  }
  loadCourseDegrees() {
    this.loading_course_degrees = true;
    this.configurationsService.courseDegrees().pipe(
      first(),
      finalize(() => this.loading_course_degrees = false)
    ).subscribe(result => {
      this.course_degrees = result.data;
    });
  }

  loadOrganicUnits() {
    this.loading_schools = true;
    this.organicUnitsService.organicUnitsbySchools().pipe(
      first(),
      finalize(() => this.loading_schools = false)
    ).subscribe(result => {
      this.schools = result.data;
    });
  }

  back() {
    this.router.navigate(['mobility', 'sub-23-declarations']);
  }
  filterDegrees() {
    this.declarationGroup.get('course_id').value ? this.declarationGroup.get('course_id').setValue(null) : '';
    this.declarationGroup.get('course_degree_id').value ? this.declarationGroup.get('course_degree_id').setValue(null) : '';
  }

  goTo(current: number) {
    this.current = current;
    this.changeContent();
  }

  pre(): void {
    this.openInfo = false;
    this.current -= 1;
    this.changeContent();
    this.showErrorMessage = false;
  }

  next(): void {
    let valid = false;
    switch (this.current) {
      case 0: {
        valid = this.checkValidityOfControls([
          this.declarationGroup.get('cc_emitted_in'),
          this.declarationGroup.get('academic_year'),
          this.declarationGroup.get('name'),
          this.declarationGroup.get('document_type_id'),
          this.declarationGroup.get('identification'),
          this.declarationGroup.get('birth_date'),
          this.declarationGroup.get('organic_unit_id'),
          this.declarationGroup.get('course_degree_id'),
          this.declarationGroup.get('course_id'),
          this.declarationGroup.get('course_year'),
          this.declarationGroup.get('has_social_scholarship'),
          this.declarationGroup.get('applied_social_scholarship'),
        ]);
        break;
      }
      case 1: {
        valid = this.checkValidityOfControls([
          this.declarationGroup.get('address'),
          this.declarationGroup.get('country'),
          this.declarationGroup.get('locality'),
          this.declarationGroup.get('postal_code'),
        ]);
        break;
      }
      case 2: {
        valid = this.checkValidityOfControls([
          this.declarationGroup.get('observations'),
        ]);
        break;
      }
      case 3: {
        valid = true;
        break;
      }
    }

    if (valid) {
      this.openInfo = false;
      this.submitted = false;
      this.current += 1;
      this.changeContent();
      this.showErrorMessage = false;
    }else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }

  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'identification';
        if (this.existInfoStep('MOBILITY_SUB23_DECLARATION_STEP_1')) {
          this.openInfo = true;
        }
        break;
      }
      case 1: {
        this.index = 'address';
        if (this.existInfoStep('MOBILITY_SUB23_DECLARATION_STEP_2')) {
          this.openInfo = true;
        }
        break;
      }
      case 2: {
        this.index = 'observations';
        if (this.existInfoStep('MOBILITY_SUB23_DECLARATION_STEP_3')) {
          this.openInfo = true;
        }
        break;
      }
      case 3: {
        this.index = 'revision';
        break;
      }
      default: {
        this.index = 'error';
      }
    }
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {

    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find(c => c.status !== 'VALID')
  }

  getInputError(field: string) {

    return this.declarationGroup.get(field).errors.required
      ? `FORM_INPUTS.VALIDATIONS.REQUIRED` :
      this.declarationGroup.get(field).errors.requiredTrue ? 'FORM_INPUTS.VALIDATIONS.REQUIRED_TRUE' :
        this.declarationGroup.get(field).errors.minlength ? 'FORM_INPUTS.VALIDATIONS.MIN_LENGTH' :
          this.declarationGroup.get(field).errors.maxlength ? 'FORM_INPUTS.VALIDATIONS.MAX_LENGTH' :
            this.declarationGroup.get(field).errors.min ? 'FORM_INPUTS.VALIDATIONS.MIN' :
              this.declarationGroup.get(field).errors.man ? 'FORM_INPUTS.VALIDATIONS.MAX' :
                this.declarationGroup.get(field).errors.email ? 'FORM_INPUTS.VALIDATIONS.EMAIL' :
                  this.declarationGroup.get(field).errors.pattern ? 'FORM_INPUTS.VALIDATIONS.PATTERN' :
                    null;
  }

  getSchool(id: number): OrganicUnitModel {
    return this.schools.find(s => s.id === id);
  }
  getCourse(id: number): CourseModel {
    return this.courses.find(s => s.id === id);
  }
  getCourseDegree(id: number): CourseDegreeModel {
    return this.course_degrees.find(s => s.id === id);
  }
  getDocumentTypeDescription(document_type_id) {
    const document_type = this.documentTypes.find(x => x.id == document_type_id)
    return document_type ? document_type.translations.find(x => x.language_id == 3).description : "--";
  }

  done() {
    if (!this.authService.hasPermission('bus:sub23_declarations:create')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }
    for (const i in this.declarationGroup.controls) {
      if (i) {
        this.declarationGroup.controls[i].markAsDirty();
        this.declarationGroup.controls[i].updateValueAndValidity();
      }
    }

    if (!this.declarationGroup.valid) {
      return;
    }

    this.loading = true;
    this.sub23DeclarationServices.create(this.declarationGroup.value).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      this.uiService.showMessage(MessageType.success, 'Pedido de declaração criado com sucesso!');
      this.router.navigate(['mobility', 'sub-23-declarations']);
    });
  }

}
