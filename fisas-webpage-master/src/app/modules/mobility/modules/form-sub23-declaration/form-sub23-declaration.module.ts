import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormSub23DeclarationRoutingModule } from './form-sub23-declaration-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FormSub23DeclarationComponent } from './form-sub23-declaration.component';

@NgModule({
  declarations: [FormSub23DeclarationComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormSub23DeclarationRoutingModule
  ]
})
export class FormSub23DeclarationModule { }
