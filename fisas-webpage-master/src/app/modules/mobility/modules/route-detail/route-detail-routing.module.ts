import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteDetailComponent } from './route-detail.component';

const routes: Routes = [
  {
    path: '',
    component: RouteDetailComponent,
    data: { breadcrumb: 'MOBILITY.ROUTE_DETAIL.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteDetailRoutingModule { }
