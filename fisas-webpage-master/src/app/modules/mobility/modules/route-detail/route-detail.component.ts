import { ContactModel } from '@fi-sas/webpage/shared/models/contact.model';
import { Component, OnInit } from '@angular/core';
import { RouteModel, RouteStepModel } from '../../models/route.modal';
import { FiConfigurator } from '@fi-sas/configurator';
import { Router } from '@angular/router';
import { MobilityService } from '../../services/mobility.service';
import { flatten, compact, uniqBy } from 'lodash';

@Component({
  selector: 'app-route-detail',
  templateUrl: './route-detail.component.html',
  styleUrls: ['./route-detail.component.less']
})
export class RouteDetailComponent implements OnInit {

  route: RouteModel;
  steps: RouteStepModel[] = [];
  contacts: ContactModel;
  languages: any;

  constructor(
    private router: Router,
    private mobilityService: MobilityService,
    config: FiConfigurator,
  ) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  ngOnInit() {
    this.route = this.mobilityService.getSelectedRoute();
    if(this.route) {
      this.contacts = this.route.contacts;
    }

    if(!this.route){
      setTimeout(() => this.back(), 100);
    }
    if(this.route) {
      this.steps = compact(flatten(this.route.steps));

    } else {
      setTimeout(() => this.back(), 100);
    }
  }

  back() {
    this.router.navigate(['mobility', 'route-search']);
  }

  convertDuration(value): string{
    const hours = Math.floor(value / 60);
    const minutes = value % 60;
    let end = hours != 0 ? ((hours.toString()).split('.')[0] + ' h ' + (minutes.toString()).split('.')[0] + ' m') : ((minutes.toString()).split('.')[0] + ' m');
    return end;
  }

  formatHour(value):string {
    if(value) {
      return value.split(':')[0]+':'+value.split(':')[1];
    }
    return '';
  }

  getTicketPrice() {
    console.log( this.route.prices);
    const pticket = this.route.prices.find(p => p.ticket_type === "TICKET");

    if(pticket) {
      return pticket.value;
    } else {
      return 0;
    }
  }
}
