import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteDetailRoutingModule } from './route-detail-routing.module';
import { RouteDetailComponent } from './route-detail.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MobilityService } from '../../services/mobility.service';

@NgModule({
  declarations: [RouteDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouteDetailRoutingModule
  ]
})
export class RouteDetailModule { }
