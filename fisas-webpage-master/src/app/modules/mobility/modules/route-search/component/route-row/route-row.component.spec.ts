import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteRowComponent } from './route-row.component';

describe('RouteRowComponent', () => {
  let component: RouteRowComponent;
  let fixture: ComponentFixture<RouteRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
