import { Component, OnInit, Input } from '@angular/core';
import { RouteModel } from '@fi-sas/webpage/modules/mobility/models/route.modal';
import { MobilityService } from '@fi-sas/webpage/modules/mobility/services/mobility.service';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-route-row',
  templateUrl: './route-row.component.html',
  styleUrls: ['./route-row.component.less']
})
export class RouteRowComponent implements OnInit {

  @Input() number = 0;
  @Input() route: any = null;
  @Input() stops = 0;
  @Input() prices: { ticket_type: string, hash: string, value: number }[] = [];
  @Input() departure;
  @Input() arrival;
  @Input() date;
  @Input() duration;

  cartLoading = false;

  constructor(
    private mobilityService: MobilityService,
    private router: Router,
    ) { }

  ngOnInit() {
  }

  addToBasket() {
    if(this.prices) {
      const price = this.prices.find(f => f.ticket_type === 'TICKET');

      if(price) {
        this.cartLoading = true;
        this.mobilityService.addToCart(price.hash).pipe(
          first(),
          finalize(() => this.cartLoading = false))
          .subscribe(() => {

          });
      }

    }
  }

  viewDetail() {
    this.mobilityService.selectedIndex(this.number -1);
    this.mobilityService.selectedRoute(this.route);
    this.route.prices = this.prices;
    this.route.stops = this.stops;
    this.route.duration = this.duration;
    this.route.departure_place = this.departure;
    this.route.arrival_place = this.arrival;
    this.route.date = this.date;

    this.router.navigate(['mobility', 'route-detail']);
  }

  getPrice() {
    if(this.prices) {
      const price = this.prices.find(f => f.ticket_type === 'TICKET');

      if(price) {
        return price.value;
      }
      return null;
    }

    return null;
  }

  isAvailabletoBuy() {
    return !!this.getPrice();
  }
}
