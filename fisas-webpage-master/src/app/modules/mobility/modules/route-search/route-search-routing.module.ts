import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteSearchComponent } from './route-search.component';

const routes: Routes = [
  {
    path: '',
    component: RouteSearchComponent,
    data: { breadcrumb: 'MOBILITY.ROUTE_SEARCH.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteSearchRoutingModule { }
