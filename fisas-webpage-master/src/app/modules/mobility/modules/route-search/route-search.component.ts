import { Component } from '@angular/core';

import { MobilityService } from '../../services/mobility.service';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-route-search',
  templateUrl: './route-search.component.html',
  styleUrls: ['./route-search.component.less']
})
export class RouteSearchComponent {
  readonly serviceId = SERVICE_IDS.MOBILITY;

  constructor(public mobilityService: MobilityService) { }
}
