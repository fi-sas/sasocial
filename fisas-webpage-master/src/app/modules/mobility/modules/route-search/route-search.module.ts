import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteSearchRoutingModule } from './route-search-routing.module';
import { RouteSearchComponent } from './route-search.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { RouteRowComponent } from './component/route-row/route-row.component';

@NgModule({
  declarations: [RouteSearchComponent, RouteRowComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouteSearchRoutingModule
  ]
})
export class RouteSearchModule { }
