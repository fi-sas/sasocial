import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Sub23DeclarationsComponent } from './sub23-declarations.component';

const routes: Routes = [
  {
    path: '',
    component: Sub23DeclarationsComponent,
    data: { breadcrumb: 'MOBILITY.ROUTE_SEARCH.SUB23_PASS_DECLARATION' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Sub23DeclarationsRoutingModule { }
