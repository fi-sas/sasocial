import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sub23DeclarationsComponent } from './sub23-declarations.component';

describe('Sub23DeclarationsComponent', () => {
  let component: Sub23DeclarationsComponent;
  let fixture: ComponentFixture<Sub23DeclarationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sub23DeclarationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sub23DeclarationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
