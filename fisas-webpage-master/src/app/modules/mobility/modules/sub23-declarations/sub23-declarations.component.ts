import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { finalize, first } from 'rxjs/operators';
import { Sub23BusDeclarationModel } from '../../models/sub23_bus_declarations.model';
import { Sub23DeclarationsService } from '../../services/sub23-declarations.service';

@Component({
  selector: 'app-sub23-declarations',
  templateUrl: './sub23-declarations.component.html',
  styleUrls: ['./sub23-declarations.component.less']
})
export class Sub23DeclarationsComponent implements OnInit {

  current_academic_year = null;
  declarations: Sub23BusDeclarationModel[] = [];
  loading = false;
  constructor(
    private router: Router,
    private sub23DeclarationService: Sub23DeclarationsService,
    private configurationService: ConfigurationsService
  ) { }
  DeclarationStatusLabels = {
    'validated': { color: '#1ba974', translation: 'MOBILITY.STATUS.VALIDATED' },
    'in_validation': { color: "#f8ca00", translation: 'MOBILITY.STATUS.IN_VALIDATION'},
    'submitted': { color: '#4D9DE0', translation: 'MOBILITY.STATUS.SUBMITTED' },
    'emitted': { color: '#7768AE', translation: 'MOBILITY.STATUS.EMITTED' },
    'rejected': { color: '#D0021B', translation: 'MOBILITY.STATUS.REJECT' }
  }
  ngOnInit() {
    this.loadDeclarations();
    this.loadCurrentAcademicYear();
  }

  loadDeclarations() {
    this.loading = true;
    this.sub23DeclarationService.list().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.declarations = result.data;
    });
  }
  loadCurrentAcademicYear() {
    this.configurationService.getCurrentAcademicYear().subscribe(res => {
      if (res.data.length > 0) {
        this.current_academic_year = res.data[0].academic_year;
      }
    });
  }

  back() {
    this.router.navigate(['mobility', 'route-search']);
  }

  canCreateDeclaration() {
    if (this.loading) {
      return true;
    }
    for (const app of this.declarations) {
      if (
        (app.status === 'submitted' && app.academic_year == this.current_academic_year) ||
        (app.status === 'validated' && app.academic_year == this.current_academic_year) ||
        (app.status === 'in_validation' && app.academic_year == this.current_academic_year)
      ) {
        return true;
      }
    }
    return false;
  }



}
