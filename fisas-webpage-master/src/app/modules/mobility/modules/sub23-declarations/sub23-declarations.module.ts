import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sub23DeclarationsRoutingModule } from './sub23-declarations-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { Sub23DeclarationsComponent } from './sub23-declarations.component';

@NgModule({
  declarations: [Sub23DeclarationsComponent],
  imports: [
    CommonModule,
    SharedModule,
    Sub23DeclarationsRoutingModule
  ]
})
export class Sub23DeclarationsModule { }
