import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusTicketValidatorComponent } from './bus-ticket-validator.component';

describe('BusTicketValidatorComponent', () => {
  let component: BusTicketValidatorComponent;
  let fixture: ComponentFixture<BusTicketValidatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusTicketValidatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusTicketValidatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
