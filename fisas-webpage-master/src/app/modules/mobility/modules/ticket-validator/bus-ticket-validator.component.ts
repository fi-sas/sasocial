import { Component, OnInit } from '@angular/core';
import { Html5QrcodeScanner } from 'html5-qrcode';
import { finalize, first } from 'rxjs/operators';
import { TicketsService, ValidatingResult } from '../../services/tickets.service';

@Component({
  templateUrl: './bus-ticket-validator.component.html',
  styleUrls: ['./bus-ticket-validator.component.less']
})
export class BusTicketValidatorComponent implements OnInit {
  html5QrcodeScanner: Html5QrcodeScanner = null;

  lastDecodeText: string;
  resultModalVisible = false;
  validating = false;
  validatingResult: ValidatingResult = null;

  constructor(
    private ticketsService: TicketsService,
  ) { }

  ngOnInit() {

    this.html5QrcodeScanner = new Html5QrcodeScanner("reader",{ fps: 10, qrbox: {width: 250, height: 250} },/* verbose= */ false);

    this.html5QrcodeScanner.render((decodedText, decodedResult) => {
      // handle the scanned code as you like, for example:+
      this.html5QrcodeScanner.pause(true);
      this.lastDecodeText = decodedText;
      this.resultModalVisible = true;
      this.validateHash();

    }, (error) => {
      // handle scan failure, usually better to ignore and keep scanning.
      // for example:
      console.warn(`Code scan error = ${error}`);
    });
  }


  validateHash() {
    this.validating = true;
    this.validatingResult = null;

    this.ticketsService.validateTicket(this.lastDecodeText).pipe(
      first(),
      finalize(() => this.validating = false)
    ).subscribe(res => {
      this.validatingResult = res.data[0];
    })
  }

  onModalClose() {
    this.resultModalVisible = false;
    this.html5QrcodeScanner.resume();
  }

  getErrorTranslation(exception: string) {
    switch(exception) {
      case 'UNABLE_TO_USE':
        return 'Não foi possivel terminar a validação';
        case 'ALREADY_USED':
          return 'O bilhete já foi utilizado';
        case 'TICKET_NOT_FOUND':
          return 'O bilhete não foi encontrado';
        case 'INVALID_TICKET':
          return 'O bilhete invalido';
        default:
          return 'Ocorreu um erro na valição do bilhete';
    }
  }
}
