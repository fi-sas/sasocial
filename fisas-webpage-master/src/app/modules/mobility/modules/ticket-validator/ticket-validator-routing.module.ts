import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusTicketValidatorComponent } from './bus-ticket-validator.component';

const routes: Routes = [
  {
    path: '',
    component: BusTicketValidatorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusTicketValidatorRoutingModule { }
