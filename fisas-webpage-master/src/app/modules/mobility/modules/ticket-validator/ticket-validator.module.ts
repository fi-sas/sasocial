import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusTicketValidatorRoutingModule } from './ticket-validator-routing.module';
import { BusTicketValidatorComponent } from './bus-ticket-validator.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';


@NgModule({
  declarations: [
    BusTicketValidatorComponent
  ],
  imports: [
    CommonModule,
    BusTicketValidatorRoutingModule,
    SharedModule,
  ]
})
export class BusTicketValidatorModule { }
