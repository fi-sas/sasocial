import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewApplicationComponent } from './view-application.component';

const routes: Routes = [
  {
    path: ':id',
    component: ViewApplicationComponent,
    data: { breadcrumb: 'MOBILITY.APPLICATION_VIEW.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewApplicationRoutingModule { }
