import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { finalize, first } from 'rxjs/operators';
import { ApplicationModel } from '../../models/application.model';
import { WithdrawalService } from '../../services/withdrawal.service';
import { WithdrawalModel } from '../../models/withdrawal.model';
import { AbsencesService } from '../../services/absences.service';
import { AbsenceModel } from '../../models/absence.model';

@Component({
  selector: 'app-view-application',
  templateUrl: './view-application.component.html',
  styleUrls: ['./view-application.component.less']
})
export class ViewApplicationComponent implements OnInit {

  loading = false;
  application: ApplicationModel = null;
  applicationId = null;
  withdrawals: WithdrawalModel[] = [];
  absences: AbsenceModel[] = [];

  withdrawalStatusLabels = {
    'accepted': { color: '#9DBBAE', translation: 'MOBILITY.STATUS.ACCEPTED' },
    'rejected': { color: '#D0021B', translation: 'MOBILITY.STATUS.REJECTED' },
    'pending': { color: '#f8ca00', translation: 'MOBILITY.STATUS.SUBMITTED' }
  }
  absenceStatusLabels = {
    'approved': { color: '#0d69dd', translation: 'MOBILITY.ABSENCE_STATUS.APPROVED' },
    'rejected': { color: '#D0021B', translation: 'MOBILITY.ABSENCE_STATUS.REJECTED' },
    'submitted': { color: '#4D9DE0', translation: 'MOBILITY.ABSENCE_STATUS.SUBMITTED' },
    'ongoing': { color: '#076743', translation: 'MOBILITY.ABSENCE_STATUS.ONGOING' },
    'completed': { color: '#1ba974', translation: 'MOBILITY.ABSENCE_STATUS.COMPLETED' },
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private applicationService: ApplicationsService,
    private withdrawalsService: WithdrawalService,
    private absencesService: AbsencesService
  ) { }

  ngOnInit() {
    this.applicationId = this.route.snapshot.paramMap.get("id");
    this.loadApplication();
    this.loadAbsences();
  }
  loadAbsences() {
    if (!this.applicationId) {
      this.back();
      return;
    }

    this.absencesService.list(this.applicationId).pipe(
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.absences = result.data;
    });
  }

  loadApplication() {
    if (!this.applicationId) {
      this.back();
      return;
    }

    this.loading = true;
    this.applicationService.read(this.applicationId).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.application = result.data[0];
    });
    this.withdrawalsService.list(this.applicationId).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.withdrawals = result.data;
    });


  }
  getDocumentTypeDescription() {
    return this.application.document_type ? (this.application.document_type.translations.find(x => x.language_id == 3) ? this.application.document_type.translations.find(x => x.language_id == 3).description : '') : '';
  }
  back() {
    this.router.navigate(['mobility', 'applications']);
  }

}
