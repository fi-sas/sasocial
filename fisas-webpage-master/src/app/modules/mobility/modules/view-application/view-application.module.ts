import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewApplicationRoutingModule } from './view-application-routing.module';
import { ViewApplicationComponent } from './view-application.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [ViewApplicationComponent],
  imports: [
    CommonModule,
    SharedModule,
    ViewApplicationRoutingModule
  ]
})
export class ViewApplicationModule { }
