import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewSub23DeclarationComponent } from './view-sub23-declaration.component';

const routes: Routes = [
  {
    path: ':id',
    component: ViewSub23DeclarationComponent,
    data: { breadcrumb: 'MOBILITY.SUB_23_DECLARATION_VIEW.BREADCRUMB' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewSub23DeclarationRoutingModule { }
