import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSub23DeclarationComponent } from './view-sub23-declaration.component';

describe('ViewSub23DeclarationComponent', () => {
  let component: ViewSub23DeclarationComponent;
  let fixture: ComponentFixture<ViewSub23DeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSub23DeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSub23DeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
