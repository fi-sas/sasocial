import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { Sub23BusDeclarationModel } from '../../models/sub23_bus_declarations.model';
import { Sub23DeclarationsService } from '../../services/sub23-declarations.service';

@Component({
  selector: 'app-view-sub23-declaration',
  templateUrl: './view-sub23-declaration.component.html',
  styleUrls: ['./view-sub23-declaration.component.less']
})
export class ViewSub23DeclarationComponent implements OnInit {

  loading = false;
  declaration: Sub23BusDeclarationModel = null;
  declarationId = null;

  Sub23BusDeclarationStatusLabels = {
    'validated': { color: '#1ba974', translation: 'MOBILITY.STATUS.VALIDATED' },
    'in_validation': { color: "#f8ca00", translation: 'MOBILITY.STATUS.IN_VALIDATION'},
    'submitted': { color: '#4D9DE0', translation: 'MOBILITY.STATUS.SUBMITTED' },
    'emitted': { color: '#7768AE', translation: 'MOBILITY.STATUS.EMITTED' },
    'rejected': { color: '#D0021B', translation: 'MOBILITY.STATUS.REJECT' }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sub23DeclarationsService: Sub23DeclarationsService
  ) { }

  ngOnInit() {
    this.loadApplication();
  }

  loadApplication() {
    this.declarationId = this.route.snapshot.paramMap.get("id");

    if (!this.declarationId) {
      this.back();
      return;
    }

    this.loading = true;
    this.sub23DeclarationsService.read(this.declarationId).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(result => {
      this.declaration = result.data[0];
    });
  }
  back() {
    this.router.navigate(['mobility', 'sub-23-declarations']);
  }
  getDocumentTypeDescription() {
    return this.declaration.document_type ? (this.declaration.document_type.translations.find(x => x.language_id == 3) ? this.declaration.document_type.translations.find(x => x.language_id == 3).description : '') : '';
  }


}
