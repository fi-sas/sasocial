import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewSub23DeclarationRoutingModule } from './view-sub23-declaration-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ViewSub23DeclarationComponent } from './view-sub23-declaration.component';

@NgModule({
  declarations: [ViewSub23DeclarationComponent],
  imports: [
    CommonModule,
    SharedModule,
    ViewSub23DeclarationRoutingModule
  ]
})
export class ViewSub23DeclarationModule { }
