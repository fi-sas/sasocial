import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { AbsenceModel } from '../models/absence.model';

@Injectable({
  providedIn: 'root'
})
export class AbsencesService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  create(absence: AbsenceModel): Observable<Resource<AbsenceModel>> {
    return this.resourceService.create<AbsenceModel>(this.urlService.get('MOBILITY.ABSENCES'), absence);
  }

  list(applicationId: number):
    Observable<Resource<AbsenceModel>> {
    let params = new HttpParams();
    params = params.set('offset', "0");
    params = params.set('limit', "-1");
    params = params.set('query[application_id]', applicationId.toString());

    return this.resourceService.list<AbsenceModel>(this.urlService.get('MOBILITY.ABSENCES'), { params });
  }
}
