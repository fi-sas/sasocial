import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ApplicationModel } from '../models/application.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    params = params.set('limit', '-1');
    params = params.set('offset', '0');
    params = params.set('sort', 'created_at');
    return this.resourceService.list<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATIONS'), { params });
  }

  create(application: ApplicationModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATIONS'), application);
  }

  read(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATIONS_ID', { id }));
  }

  cancel(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATION_CANCEL_ID', { id }), { event: 'cancel' });
  }

  withdrawal(id: number, justification: string): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATION_WITHDRAWAL_ID', { id }), {
      justification
    });
  }

  resume(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('MOBILITY.APPLICATION_RESUME_ID', { id }), {});
  }

  accept_reject_application(id: number, accept: boolean): Observable<Resource<ApplicationModel>>{
      return this.resourceService.create<ApplicationModel>(
        this.urlService.get('MOBILITY.APPLICATION_CANCEL_ID', { id }),
        {
          "event": accept ? "accept" : "reject"
        }
      );

  }

}
