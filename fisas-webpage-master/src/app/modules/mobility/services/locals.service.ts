import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { LocalModel } from '../models/local.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(instituteOnly?: boolean): Observable<Resource<LocalModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort','local');

    if(instituteOnly)  {
      params = params.set('institute', 'true');
    }

    return this.resourceService.list<LocalModel>(this.urlService.get('MOBILITY.LOCALS'), { params });
  }
}
