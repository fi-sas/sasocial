import { Injectable } from '@angular/core';
import { RouteModel } from '../models/route.modal';
import { LocalModel } from '../models/local.model';
import { LocalsService } from './locals.service';
import { first, finalize } from 'rxjs/operators';
import { RoutesService } from './routes.service';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { CartService } from '@fi-sas/webpage/shared/services/cart.service';

@Injectable({
  providedIn: 'root'
})
export class MobilityService {

  public route1 = null;
  public route2 = null;

  public date: Date = null;

  public loading_locals = false;
  public locals: LocalModel[] = [];

  public loading_routes = false;
  public routes: RouteModel[] = [];
  public route_detail: RouteModel;

  public selected_index = null;

  constructor(
    private localsService: LocalsService,
    private routesService: RoutesService,
    private cartService: CartService,
    private uiService: UiService,
    private translateService: TranslateService) {
    this.loadLocals();
  }

  loadLocals() {
    if (this.locals.length === 0) {
      this.loading_locals = true;
      this.localsService.list().pipe(
        first(),
        finalize(() => this.loading_locals = false)
      ).subscribe(result => {
        this.locals = result.data;
      });
    }
  }

  selectedIndex(index: number) {
    this.selected_index = index;
  }

  getSelectedRoute() {
    return this.route_detail;
  }

  selectedRoute(route){
    return this.route_detail =  route;
  }

  routeSearch() {

    if(!this.route1 && !this.route2) {
      this.uiService.showMessage(
        MessageType.warning,
        this.translateService.instant('MOBILITY.ROUTE_SEARCH.SELECT_POINTS')
      );
      return;
    }

    if(this.route1 == this.route2) {
      this.uiService.showMessage(
        MessageType.warning,
        this.translateService.instant('MOBILITY.ROUTE_SEARCH.SAME_DEPARTURE_ARRIVAL')
      );
      return;
    }

    this.loading_routes = true;
    this.routesService.routeSearch(this.route1, this.route2, this.date ? this.date : null).pipe(
      first(),
      finalize(() => this.loading_routes = false)
    ).subscribe(result => {
      this.routes = result.data;
    });
  }

  addToCart(hash) {
    return this.routesService.addCart(hash).pipe(finalize(() => {
      this.cartService.getDataNotification();
    }));
  }

  revert() {
    const routeTemp = this.route1;
    this.route1 = this.route2;
    this.route2 = routeTemp;
  }



}
