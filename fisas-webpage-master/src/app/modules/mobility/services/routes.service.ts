import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resource, FiUrlService, FiResourceService } from '@fi-sas/core';
import { RouteModel } from '../models/route.modal';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  routeSearch(origin: string, destination: string, date?: Date): Observable<Resource<RouteModel>> {
    let params = new HttpParams();
    params = params.set('origin', origin);
    params = params.set('destination', destination);
    if(date) {
      params = params.set('date', moment(date).format('YYYY/MM/DD hh:mm:ss'));
    }

    return this.resourceService.list<RouteModel>(this.urlService.get('MOBILITY.ROUTE_SEARCH'), { params });
  }

  addCart(hash): Observable<Resource<RouteModel>> {
    return this.resourceService.create<RouteModel>(this.urlService.get('MOBILITY.ADD_CART'), { hash });
  }
}
