import { TestBed } from '@angular/core/testing';

import { Sub23DeclarationsService } from './sub23-declarations.service';

describe('Sub23DeclarationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Sub23DeclarationsService = TestBed.get(Sub23DeclarationsService);
    expect(service).toBeTruthy();
  });
});
