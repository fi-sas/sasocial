import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { Sub23BusDeclarationModel } from '../models/sub23_bus_declarations.model';

@Injectable({
  providedIn: 'root'
})
export class Sub23DeclarationsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  list(): Observable<Resource<Sub23BusDeclarationModel>> {
    let params = new HttpParams();
    params = params.set('limit', '-1');
    params = params.set('offset', '0');
    params = params.set('sort', 'created_at');
    return this.resourceService.list<Sub23BusDeclarationModel>(this.urlService.get('MOBILITY.SUB23_DECLARATIONS'), { params });
  }
  status(id: number, event: string): Observable<Resource<Sub23BusDeclarationModel>> {
    return this.resourceService.create<Sub23BusDeclarationModel>(this.urlService.get('MOBILITY.SUB23_DECLARATIONS_STATUS', { id }), { event });
  }

  create(declaration: Sub23BusDeclarationModel): Observable<Resource<Sub23BusDeclarationModel>> {
    return this.resourceService.create<Sub23BusDeclarationModel>(this.urlService.get('MOBILITY.SUB23_DECLARATIONS'), declaration);
  }

  read(id: number): Observable<Resource<Sub23BusDeclarationModel>> {
    return this.resourceService.read<Sub23BusDeclarationModel>(this.urlService.get('MOBILITY.SUB23_DECLARATIONS_ID', { id }));
  }

}
