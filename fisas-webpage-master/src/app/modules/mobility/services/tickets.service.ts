import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { BusTicketModel } from '../models/bus-ticket.model';
import * as moment from 'moment';

export interface ValidatingResult {
  valid: boolean,
  exception_code: string,
  id: number,
  name: string,
  route: string,
  created_at: Date,
  departure_local: string,
  arrival_local: string,
  validate_date: Date,
}

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  getTickets(used: boolean, date_moth: Date): Observable<Resource<BusTicketModel>> {
    const params = new HttpParams()
      .append('query[created_at][gte]', moment(date_moth).startOf('month').format('YYYY-MM-DD'))
      .append('query[created_at][lte]', moment(date_moth).endOf('month').format('YYYY-MM-DD'))
      .append('query[used]', used.toString());
    return this.resourceService.list<BusTicketModel>(this.urlService.get('MOBILITY.TICKETS_BOUGHT'), {
        params
    });
  }

  getTicket(id: number): Observable<Resource<BusTicketModel>> {
    return this.resourceService.list<BusTicketModel>(this.urlService.get('MOBILITY.TICKETS_BOUGHT_ID', { id }), {});
  }

  validateTicket(hash: string): Observable<Resource<ValidatingResult>> {
    return this.resourceService.create<ValidatingResult>(this.urlService.get('MOBILITY.TICKETS_BOUGHT_VALIDATE', {}), { hash });
  }
}
