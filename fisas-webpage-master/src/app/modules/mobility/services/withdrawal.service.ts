import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { WithdrawalModel } from '../models/withdrawal.model';

@Injectable({
  providedIn: 'root'
})
export class WithdrawalService {
  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService) { }

  create(withdrawal: WithdrawalModel): Observable<Resource<WithdrawalModel>> {
    return this.resourceService.create<WithdrawalModel>(this.urlService.get('MOBILITY.WITHDRAWALS', {}), withdrawal);
  }

  list(applicationId: number):
    Observable<Resource<WithdrawalModel>> {
    let params = new HttpParams();
    params = params.set('offset', "0");
    params = params.set('limit', "-1");
    params = params.set('query[application_id]', applicationId.toString());

    return this.resourceService.list<WithdrawalModel>(this.urlService.get('MOBILITY.WITHDRAWALS'), { params });
  }

}
