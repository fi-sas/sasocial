export class NewsModel {
    created_at: Date;
    date: Date;
    date_begin: Date;
    date_end: Date;
    status: string;
    translations: TranlationModel[];
}

export class TranlationModel {
    title: string;
    summary: string;
    post_id: number;
    language_id: number;
    id: number;
    file_16_9: any;
    body: string;
}