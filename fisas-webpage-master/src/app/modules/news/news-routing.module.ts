import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news.component';

const routes: Routes = [
    {
        path: '',
        component: NewsComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            {
                path: 'list',
                loadChildren: './pages/list-news/list-news.module#ListNewsModule',
            },
            {
                path: 'detail/:id',
                loadChildren: './pages/detail-news/detail-news.module#DetailNewsModule',
            },
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class NewsRoutingModule { }
