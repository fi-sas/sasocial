

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NewsComponent } from './news.component'
import { NewsRoutingModule } from './news-routing.module';


@NgModule({
    declarations: [
        NewsComponent
    ],
    imports: [
        NewsRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,

    ]
})

export class NewsModule { }
