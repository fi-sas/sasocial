import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailNewsComponent } from './detail-news.component';

const routes: Routes = [
  {
    path: '',
    component: DetailNewsComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailNewsRoutingModule { }
