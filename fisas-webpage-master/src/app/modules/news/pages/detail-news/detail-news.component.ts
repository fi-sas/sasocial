
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { NewsModel } from '../../models/news.model';
import { NewsService } from '../../services/news.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';

@Component({
    selector: 'app-detail-news',
    templateUrl: './detail-news.component.html',
    styleUrls: ['./detail-news.component.less']
})
export class DetailNewsComponent implements OnInit {
    loadingDetailNews = true;
    idNews: number;
    detailNews: NewsModel;

    constructor(private newsService: NewsService, private route: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private location: Location,
        private router: Router) {
        this.route.params.subscribe(async (params) => {
            this.idNews = params['id'];
            this.getDetail();
        });
    }

    ngOnInit() {

    }

    getHTML(value) {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    }

    getDetail() {
        this.newsService.getNews(this.idNews).pipe(first(), finalize(() => this.loadingDetailNews = false)).subscribe((data) => {
            this.detailNews = data.data[0];
        })
    }

    backList() {
        this.location.back();
    }

    existVideo(data) {
        let aux = false;
        data.translations.map(control => {
            if ((control.file_id_1_1 && control.file_id_1_1.type == 'VIDEO') || (control.file_9_16 && control.file_9_16.type == 'VIDEO') ||
                (control.file_16_9 && control.file_16_9.type == 'VIDEO')) {
                aux = true;
            }
        });
        if (aux) {
           return true;
        } 
        return false;
    }
}
