

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { DetailNewsComponent } from './detail-news.component';
import { DetailNewsRoutingModule } from './detail-news-routing.module';

@NgModule({
    declarations: [
        DetailNewsComponent
    ],
    imports: [
        DetailNewsRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,

    ]
})

export class DetailNewsModule { }
