
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import { NewsModel } from '../../models/news.model';
import { NewsService } from '../../services/news.service'

@Component({
    selector: 'app-list-news',
    templateUrl: './list-news.component.html',
    styleUrls: ['./list-news.component.less']
})
export class ListNewsComponent implements OnInit {
    loadingNews = true;
    listNews: NewsModel[] = [];
    type = '';
    pageIndex = 1;
    pageSize = 10;
    totalNews = 0;
    constructor(private newsService: NewsService, private router: Router) { }

    ngOnInit() {
        this.getNews();
    }

    getNews() {
        this.newsService.listNews(this.pageIndex, this.pageSize).pipe(first(), finalize(() => this.loadingNews = false)).subscribe((data) => {
            this.listNews = data.data;
            this.totalNews = data.link.total;
        })
    }

    goDetail(id: number) {
        this.router.navigateByUrl('/news/detail/' + id);
    }

    getName(name): string {
        return name.length > 30 ? (name.substring(0, 30) + '...') : name;
    }

    existVideo(data) {
        let aux = false;
        data.translations.map(control => {
            if ((control.file_id_1_1 && control.file_id_1_1.type == 'VIDEO') || (control.file_9_16 && control.file_9_16.type == 'VIDEO') ||
                (control.file_16_9 && control.file_16_9.type == 'VIDEO')) {
                aux = true;
            }
        });
        if (aux) {
           return true;
        } 
        return false;
    }

    
    paginationSearch(reset: boolean = false): void {
        if (reset) {
            this.pageIndex = 1;
        }
        this.getNews();
    }
}
