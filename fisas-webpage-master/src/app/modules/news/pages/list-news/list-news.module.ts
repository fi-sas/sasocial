

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { ListNewsComponent } from './list-news.component';
import { ListNewsRoutingModule } from './list-news-routing.module';

@NgModule({
    declarations: [
        ListNewsComponent
    ],
    imports: [
        ListNewsRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,

    ]
})

export class ListNewsModule { }
