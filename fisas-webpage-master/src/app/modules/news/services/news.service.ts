

import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { NewsModel } from '../models/news.model';

@Injectable({
  providedIn: 'root',
})
export class NewsService {

  loading = false;

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  listNews(pageIndex: number, pageSize: number): Observable<Resource<NewsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    return this.resourceService.list<NewsModel>(this.urlService.get('COMMUNICATION.LIST_NEWS'),{params});
  }

  getNews(id: number){
    return this.resourceService.read<NewsModel>(this.urlService.get('COMMUNICATION.LIST_NEWS_ID', {id}));
  }

}
