export class CartCheckoutModel {
  tin?: string;
  email?: string;
  address?: string;
  postal_code?: string;
  city?: string;
  country?: string;
  payment_method_id: number;
  payment_method_data?: {};
}


