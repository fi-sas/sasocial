export class CartModel {
  id?: number;
  uuid?: string;
  account_id: number;
  user_id: number;
  updated_at?: string;
  created_at?: string;
  items: ItemModel[];
}

export class ItemModel {
  id?: number;
  location: string;
  cart_id: number;
  service_id: number;
  product_code: string;
  article_type: string;
  name: string;
  description: string;
  extra_info: {};
  quantity: number;
  liquid_value: number;
  vat_id: number;
  vat: number;
  vat_value: number;
  unit_value: number;
  total_value: number;
  erp_budget?: string;
  erp_sncap?: string;
  erp_income_cost_center?: string;
  erp_funding_source?: string;
  erp_program?: string;
  erp_measure?: string;
  erp_project?: string;
  erp_activity?: string;
  erp_action?: string;
  erp_functional_classifier?: string;
  erp_organic?: string;
  updated_at?: string;
  created_at?: string;
}


