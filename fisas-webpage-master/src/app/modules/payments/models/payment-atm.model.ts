export class PaymentsATMModel {
    OperationAuthorization: string;
    Amount: string;
    RepeatableOperation: string;
    EntityNumber: string;
    Reference: string;
    ExpirationDateAvailability: boolean;
    ExpirationDate: Date;
    ActiveExpiration: boolean;
}