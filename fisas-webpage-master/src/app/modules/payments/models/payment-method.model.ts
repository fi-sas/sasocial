export class PaymentMethodModel {
  id: number;
  name: string;
  tag: string;
  description: string;
  immediate: boolean;
  active: boolean;
  charge: boolean;
  data?: Date;
}


