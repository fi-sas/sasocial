import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentMethodsService } from '@fi-sas/webpage/modules/payments/services/payment-methods.service';
import { CartService } from '@fi-sas/webpage/modules/payments/services/cart.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    PaymentMethodsService,
    CartService
  ]
})
export class PaymentsModule { }
