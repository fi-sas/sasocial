import { TestBed } from '@angular/core/testing';

import { CartService } from './cart.service';
import { FiCoreModule } from '@fi-sas/core';

describe('CartService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: CartService = TestBed.get(CartService);
    expect(service).toBeTruthy();
  });
});
