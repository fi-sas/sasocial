import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { CartModel } from '@fi-sas/webpage/modules/payments/models/cart.model';
import { CartCheckoutModel } from '@fi-sas/webpage/modules/payments/models/cart-checkout.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  createCart(cart: CartModel): Observable<Resource<CartModel>> {
    return this.resourceService.create<CartModel>(this.urlService.get('PAYMENTS.CART', {}), cart);
  }

  cartCheckout(id: number, checkout: CartCheckoutModel): Observable<Resource<CartCheckoutModel>> {
    return this.resourceService.create<CartCheckoutModel>(this.urlService.get('PAYMENTS.CART_CHECKOUT', { id }), checkout);
  }

  cartCheckoutItems(checkout: any): Observable<Resource<CartCheckoutModel>> {
    return this.resourceService.create<CartCheckoutModel>(this.urlService.get('PAYMENTS.CART_CHECKOUT'), checkout);
  }
}
