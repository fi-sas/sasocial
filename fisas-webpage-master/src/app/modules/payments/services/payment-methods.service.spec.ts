import { TestBed } from '@angular/core/testing';

import { PaymentMethodsService } from './payment-methods.service';
import { FiCoreModule } from '@fi-sas/core';

describe('PaymentMethodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: PaymentMethodsService = TestBed.get(PaymentMethodsService);
    expect(service).toBeTruthy();
  });
});
