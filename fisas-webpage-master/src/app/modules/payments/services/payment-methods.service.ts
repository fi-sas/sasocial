import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { PaymentMethodModel } from '@fi-sas/webpage/modules/payments/models/payment-method.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  list(immediate?: boolean, active?: boolean, charge?: boolean): Observable<Resource<PaymentMethodModel>> {
    let params = new HttpParams();

    if (immediate) {
        params = params.set('immediate', '' + immediate);
    }

    if (active) {
      params = params.set('immediate', '' + active);
    }

    if (charge) {
      params = params.set('immediate', '' + charge);
    }

    return this.resourceService.list<PaymentMethodModel>(this.urlService.get('PAYMENTS.PAYMENT_METHODS'), { params });
  }

  read(id: number): Observable<Resource<PaymentMethodModel>> {
    return this.resourceService.read<PaymentMethodModel>(this.urlService.get('PAYMENTS.PAYMENT_METHODS_ID', { id }), {});
  }

}
