export class ListingAmenitiesModel {
    kitchen_access: boolean;
    microwave: boolean;
    stove: boolean;
    fridge: boolean;
    dishwasher: boolean;
    washing_machine: boolean;
    elevator: boolean;
    heating_system: boolean;
    cooling_system: boolean;
    tv: boolean;
    cable_tv: boolean;
    internet: boolean;
    parking_garage: boolean;
    private_wc: boolean;
}
