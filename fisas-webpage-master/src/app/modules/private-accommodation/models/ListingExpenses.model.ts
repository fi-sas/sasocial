export class ListingExpensesModel {
    water: boolean; // Expense 8º
    gas: boolean; // Expense 7º
    condo: boolean; // Expense 9º
    electricity: boolean; // Expense 1º
    cable_tv: boolean; // Expense 3º
    internet: boolean; // Expense 2º
    furniture: boolean; // Expense 4º
    cleaning: boolean; // Expense 5º
    bedding: boolean; // Expense 6º
}
