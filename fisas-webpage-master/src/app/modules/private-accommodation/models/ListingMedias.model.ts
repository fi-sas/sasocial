export class ListingMediasModel {
    file_id: string;
    updated_at: string;
    created_at: string;
    file: any;
}
