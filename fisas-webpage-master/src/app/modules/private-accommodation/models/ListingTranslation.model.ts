export class ListingTranslationModel {
    language_id: number;
    description: string;

    constructor(language_id, description) {
        this.language_id = language_id;
        this.description = description;
    }
}
