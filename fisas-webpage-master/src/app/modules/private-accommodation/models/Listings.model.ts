import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { ListingExpensesModel } from './ListingExpenses.model';
import { ListingAmenitiesModel } from './LIstingAmenities.model';
import { OwnerApplicationsModel } from './OwnerApplication.model';

export class ListingsModel {
  typology_id: number;
  usable_space: string;
  house_typology: string;
  price: number;
  area: string;
  floor: string;
  rooms: RoomModel[];
  rooms_number: number;
  address: string;
  address_no: string;
  latitude: string;
  longitude: string;
  city: string;
  postal_code: string;
  allows_smokers: boolean;
  allows_pets: boolean;
  gender: string;
  occupation: string;
  start_date: string;
  end_date: string;
  visualizations?: number;
  show?: boolean;
  amenities: ListingAmenitiesModel;
  expenses: ListingExpensesModel;
  medias: MediasModel[];
  owner_id: number;
  owner?: OwnerApplicationsModel;
  status: string;
  id: number;
  translations: TranslationModel[];
}

export class MediasModel {
  file_id: number;
  file: FileModel;
}

export class RoomModel {
  price: string;
  room_type: string;
  shared_wc: boolean;
  translations: TranslationModel[];
  area: string;
}

export class TranslationModel {
  language_id: number;
  description: string;
}
