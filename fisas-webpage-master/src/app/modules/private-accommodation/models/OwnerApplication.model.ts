import { UserModel } from '@fi-sas/webpage/auth/models/user.model';

export class OwnerApplicationsModel {
    name: string;
    user_name: string;
    password: string;
    pin: string;
    identification: string;
    tin: string;
    birth_date: string;
    gender: string;
    nationality: string;
    document_type_id: number;
    email: string;
    phone: string;
    address: string;
    postal_code: string;
    city: string;
    country: string;
    description: string;
    issues_receipt: boolean;
    applicant_consent: boolean;
    user?: UserModel;
}
