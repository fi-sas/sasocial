export class LanguageModel {
    id: number;
    name: string;
    acronym: string;
    active: boolean;
}
