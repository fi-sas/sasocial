import { HttpParams } from '@angular/common/http';
import { Params } from '@angular/router';

export interface IListingQueryParams {
  amenities?: string;
  city?: string;
  expenses?: string;
  maxPrice?: string;
  minPrice?: string;
  typology?: string;
}

export class ListingQueryParams {
  amenities: string[];
  city: string[];
  expenses: string[];
  maxPrice: number;
  minPrice: number;
  typology: number;

  constructor(queryParams: Params = {}) {
    this.amenities = this.stringToList(queryParams.amenities);
    this.city = this.stringToList(queryParams.city);
    this.expenses = this.stringToList(queryParams.expenses);
    this.maxPrice = queryParams.maxPrice || null;
    this.minPrice = queryParams.minPrice || null;
    this.typology = queryParams.typology || null;
  }

  clear() {
    this.amenities = [];
    this.city = [];
    this.expenses = [];
    this.maxPrice = null;
    this.minPrice = null;
    this.typology = null;
  }

  toUrlParams(): IListingQueryParams {
    const params: IListingQueryParams = {};
    if (this.amenities.length) {
      params.amenities = this.amenities.join(',');
    }
    if (this.city.length) {
      params.city = this.city.join(',');
    }
    if (this.expenses.length) {
      params.expenses = this.expenses.join(',');
    }
    if (this.maxPrice !== null) {
      params.maxPrice = this.maxPrice.toString();
    }
    if (this.minPrice !== null) {
      params.minPrice = this.minPrice.toString();
    }
    if (this.typology !== null) {
      params.typology = this.typology.toString();
    }
    return params;
  }

  toHttpParams(): HttpParams {
    let params = new HttpParams();

    if (this.amenities.length) {
      const amenities = {};
      this.amenities.forEach((a) => amenities[a] = true);
      params = params.set('query[amenities]', JSON.stringify(amenities));
    }

    if (this.city.length) {
      this.city.forEach((c) => params = params.append('query[city]', c));
    }

    if (this.expenses.length) {
      const expenses = {};
      this.expenses.forEach((a) => expenses[a] = true);
      params = params.set('query[expenses]', JSON.stringify(expenses));
    }

    if (this.maxPrice !== null) {
      params = params.set("query[max_price]", this.maxPrice.toString()); 
    }

    if (this.minPrice !== null) {
      params = params.set("query[min_price]", this.minPrice.toString());
    }

    if (this.typology !== null) {
      params = params.set('query[typology_id]', this.typology.toString());
    }
    return params;
  }

  private stringToList(value?: string) {
    return value ? value.split(',') : [];
  }
}
