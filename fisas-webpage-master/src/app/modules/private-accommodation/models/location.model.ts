export interface LocationsModel {
  label: string;
  value: string;
  checked: boolean;
}
