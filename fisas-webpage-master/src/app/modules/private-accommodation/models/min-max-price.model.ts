export interface IMinMaxPrice {
  min: number;
  max: number;
}
