export interface PropertiesPriceInterval {
  min_price: number;
  max_price: number;
}
