export interface ITypology {
  id: number;
  name: string;
}
