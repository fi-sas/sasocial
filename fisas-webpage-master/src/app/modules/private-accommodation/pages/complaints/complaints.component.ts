import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormControl,
  FormBuilder,
} from '@angular/forms';
import { ComplaintsModel } from '../../models/Complaints.model';
import { ComplaintsService } from '../../services/complaints.service';
import { PropertiesService } from '../../services/properties.service';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MessageType,
  UiService,
} from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.less'],
})
export class ComplaintsComponent implements OnInit {
  listProperties = [];
  loadingButton = false;
  descriptiondescription: string;
  details = {};
  id: number;
  submitted = false;
  @Output() nextPress = new EventEmitter();
  complaintsForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private propertiesService: PropertiesService,
    private complaintsService: ComplaintsService,
    private uiService: UiService,
    private translateService: TranslateService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.propertiesService.get(this.id).subscribe((data) => {
        this.details = data;
      });

      this.complaintsForm = this.fb.group({
        description: new FormControl('', [Validators.required, trimValidation]),
        listing_id:  new FormControl(this.id, Validators.required),
      });
    });
  }

  get f() { return this.complaintsForm.controls; }

  submitForm(): void {
    if(!this.authService.hasPermission('private_accommodation:complaints:create')){
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    this.submitted = true;
    if(this.complaintsForm.valid) {
      this.loadingButton = true;
      const model = new ComplaintsModel();
  
      model.description = this.complaintsForm.controls.description.value;
      model.listing_id = this.complaintsForm.controls.listing_id.value;
  
      this.complaintsService.save(model).subscribe(
        () => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant(
              'PRIVATEACCOMMODATION.COMPLAINTS.SUCCESS_SEND'
            )
          );
          this.loadingButton = false;
          this.router.navigateByUrl('privateaccommodation/list-detail/' + this.id);
        },
        () => {
          this.loadingButton = false;
        }
      );
    }
   
  }
}
