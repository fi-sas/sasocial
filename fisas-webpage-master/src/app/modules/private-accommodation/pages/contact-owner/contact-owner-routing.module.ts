import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactOwnerComponent } from './contact-owner.component';



const routes: Routes = [
  {
    path: '',
    component: ContactOwnerComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactOwnerRoutingModule { }
