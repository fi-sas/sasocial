import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PropertiesService } from '../../services/properties.service';
import { forkJoin } from 'rxjs';
import { TypologiespaService } from '../../services/typoligiespa.service';
import { ListingsModel } from '../../models/Listings.model';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-contact-owner',
  templateUrl: './contact-owner.component.html',
  styleUrls: ['./contact-owner.component.less'],
})
export class ContactOwnerComponent implements OnInit {
  listProperties = [];
  details: ListingsModel = null;
  show = false;
  typologies = [];
  owner = {};

  id: number;

  loadingContacts = false;
  loadingTypologies = false;

  constructor(
    private propertiesService: PropertiesService,
    private route: ActivatedRoute,
    private typologiespaService: TypologiespaService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      if(this.id){
        this.getContactDetails(this.id);
      }
      this.getTypologies();
    });
  }

  getContactDetails(id: number){
    this.loadingContacts = true;
    this.propertiesService.get(id).pipe(first(), finalize(() => this.loadingContacts = false)).subscribe(response => {
      this.details = response.data.length ? response.data[0] : null
    })
  }

  getTypologies(){
    this.loadingTypologies = true;
    this.typologiespaService.list().pipe(first(), finalize(() => this.loadingTypologies = false)).subscribe(response => {
      this.typologies = response.data
    })
  }

  getTypologyName(typeid) {
    const obj = this.typologies.find((t) => t.id === typeid);
    return obj ? obj.name : '';
  }
}
