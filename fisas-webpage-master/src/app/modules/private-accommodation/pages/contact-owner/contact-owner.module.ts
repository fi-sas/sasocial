import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ContactOwnerComponent } from './contact-owner.component';
import { ContactOwnerRoutingModule } from './contact-owner-routing.module';



@NgModule({
  declarations: [
        ContactOwnerComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    ContactOwnerRoutingModule
  ]
})
export class ContactOwnerModule { }
