import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { OwnersService } from "../../services/owners.service";
import { OwnerApplicationsModel } from "../../models/OwnerApplication.model";
import { finalize, first } from "rxjs/operators";
import {
  MessageType,
  UiService,
} from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { ConfigsService } from "../../services/configs.service";
import { FileModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: "app-form-owner",
  templateUrl: "./form-owner.component.html",
  styleUrls: ["./form-owner.component.less"],
})
export class FormOwnerComponent implements OnInit {
  step = 0;
  OwnerForm: FormGroup;

  regulation_file_id: any = null;
  regulation_file: FileModel = null;

  loading = false;

  formsCache = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private ownersService: OwnersService,
    private uiService: UiService,
    private translateService: TranslateService,
    private configsService: ConfigsService,
    private filesService: FilesService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getOwnerRegulation();
    this.OwnerForm = this.fb.group({
      owner_name: [],
      owner_birth_date: [],
      owner_gender: [],
      owner_nationality: [],
      owner_document_type_id: [],
      owner_identification: [],
      owner_tin: [],
      owner_phone: [],
      owner_email: [],
      owner_address: [],
      owner_post_code: [],
      owner_city: [],
      owner_country: [],
      owner_description: [],
      owner_term: [],
    });
  }

  getOwnerRegulation() {
    this.configsService
      .ownerRegulation()
      .pipe(first())
      .subscribe((regulation) => {
        this.regulation_file_id = regulation.data[0];
        if (this.regulation_file_id) {
          this.filesService.file(this.regulation_file_id).subscribe((file) => {
            this.regulation_file = file.data[0];
          });
        }
      });
  }

  changeStep(event) {
    this.onStepChangeEdit(event);
  }

  onStepChange(step: number, formValue: any = null) {
    if (formValue) {
      this.formsCache[step - 1] = formValue;
    }
    this.step = !formValue.controls.back.value ? step : step - 2;
  }

  onStepChangeEdit(step: number) {
    this.step = step;
  }

  submitForm(): void {
    if (!this.authService.hasPermission('private_accommodation:owners:create')) {
      this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
      return;
    }

    this.loading = true;

    const model = new OwnerApplicationsModel();

    model.name = this.formsCache[0].controls.name.value;
    model.birth_date = this.formsCache[0].controls.birth_date.value;

    if (this.formsCache[0].controls.gender.value === "Feminine") {
      model.gender = "F";
    } else if (this.formsCache[0].controls.gender.value === "Male") {
      model.gender = "M";
    } else if (this.formsCache[0].controls.gender.value === "Other") {
      model.gender = "U";
    }

    model.identification = this.formsCache[0].controls.identification.value;

    if (model.identification === null || model.identification === "") {
      delete model.identification;
    }

    model.tin =
      this.formsCache[0].controls.tin.value === ""
        ? null
        : this.formsCache[0].controls.tin.value;

    model.document_type_id = this.formsCache[0].controls.document_type_id.value;
    model.nationality = this.formsCache[0].controls.nationality.value;
    model.phone = this.formsCache[1].controls.phone.value;
    model.email = this.formsCache[1].controls.email.value;

    model.address = this.formsCache[2].controls.address.value;
    model.postal_code = this.formsCache[2].controls.postal_code.value;
    model.city = this.formsCache[2].controls.city.value;
    model.country = this.formsCache[2].controls.country.value;

    model.description = this.formsCache[3].controls.description.value;

   // model.user_name = this.formsCache[1].controls.email.value;

    model.applicant_consent = this.formsCache[4].controls.applicant_consent.value;
    model.issues_receipt = this.formsCache[4].controls.issues_receipt.value;

    this.ownersService
      .save(model)
      .pipe(first(),finalize(()=> this.loading = false))
      .subscribe(
        (result) => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant(
              "PRIVATEACCOMMODATION.FORM_OWNER.SUCCESS_CREATE"
            )
          );
          this.router.navigateByUrl("privateaccommodation/owner-area");
        
        }

      );
  }

  cancelApplication(): void {
    this.router.navigateByUrl("privateaccommodation/owner-area");
  }
}
