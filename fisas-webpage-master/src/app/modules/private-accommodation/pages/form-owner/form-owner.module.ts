import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FormOwnerComponent } from './form-owner.component';
import { FormOwnerRoutingModule } from './form-owner-routing.module';
import { OwnerStepOneComponent } from '../form-owner/owner-step-one/owner-step-one.component'
import { OwnerStepTwoComponent } from '../form-owner/owner-step-two/owner-step-two.component'
import { OwnerStepThreeComponent } from '../form-owner/owner-step-three/owner-step-three.component'
import { OwnerStepFourComponent } from '../form-owner/owner-step-four/owner-step-four.component'
import { OwnerStepSixComponent } from '../form-owner/owner-step-six/owner-step-six.component'
import { OwnerStepSevenComponent } from '../form-owner/owner-step-seven/owner-step-seven.component'


@NgModule({
  declarations: [
        FormOwnerComponent,
        OwnerStepOneComponent,
        OwnerStepTwoComponent,
        OwnerStepThreeComponent,
        OwnerStepFourComponent,
        OwnerStepSixComponent,
        OwnerStepSevenComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    FormOwnerRoutingModule
  ]
})
export class FormOwnerModule { }
