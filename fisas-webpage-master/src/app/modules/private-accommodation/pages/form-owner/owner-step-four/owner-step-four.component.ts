import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
    selector: 'app-owner-step-four',
    templateUrl: './owner-step-four.component.html',
    styleUrls: ['./owner-step-four.component.less']
})

export class OwnerStepFourComponent implements OnInit {
    submitted = false;
    description: string;
    back = false;
    showErrorMessage = false;
    @Output() nextPress = new EventEmitter();
    @Input() formValues: FormGroup = undefined;
    OwnerForm: FormGroup = new FormGroup({});
    get f() {
      return this.OwnerForm.controls;
    }
    constructor(private fb: FormBuilder) {
      this.OwnerForm = this.fb.group({
        description: [null, [Validators.required,trimValidation]],
        back: [false]
      });
    }

    ngOnInit(): void {
      if (this.formValues !== undefined) {
        this.description = this.formValues.controls.description.value;
      }
    }

    nextStep() {
      this.submitted = true;
      this.back = false;
    }

    backStep() {
      this.back = true;
    }

    submitForm() {
        if (this.OwnerForm.valid || this.back ) {
            this.nextPress.emit(this.OwnerForm);
            this.OwnerForm.updateValueAndValidity();
            this.showErrorMessage = false;
          } else {
            this.showErrorMessage = true;
            window.scrollTo(0, 0);
          }
    }
}
