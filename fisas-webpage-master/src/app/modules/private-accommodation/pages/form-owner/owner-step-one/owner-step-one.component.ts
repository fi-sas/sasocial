import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { nationalities } from '@fi-sas/webpage/shared/data/nationalities';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-owner-step-one',
  templateUrl: './owner-step-one.component.html',
  styleUrls: ['./owner-step-one.component.less'],
})
export class OwnerStepOneComponent implements OnInit {
  submitted = false;
  back = false;
  name: string;
  identification: string;
  tin: string;
  birth_date: string;
  gender: string;
  nationality: string;
  document_type_id: number;
  showErrorMessage = false;
  nationalities = nationalities;
  documentTypes: DocumentTypeModel[] = [];
  @Output() nextPress = new EventEmitter();
  @Input() formValues: FormGroup = undefined;

  OwnerForm: FormGroup = new FormGroup({});
  get f() {
    return this.OwnerForm.controls;
  }

  constructor(private fb: FormBuilder, private authService: AuthService, public translateService: TranslateService) {
    this.OwnerForm = this.fb.group({
      name: [null, [Validators.required, trimValidation]],
      birth_date: [null],
      gender: [null],
      nationality: [null],
      document_type_id: [null],
      identification: ['', [Validators.minLength(7)]],
      tin: ['', [Validators.min(100000000), Validators.max(999999999), Validators.required]],
      back: [false],
    });
  }

  ngOnInit(): void {
    if (this.formValues !== undefined) {
      this.name = this.formValues.controls.name.value;
      this.identification = this.formValues.controls.identification.value;
      this.birth_date = this.formValues.controls.birth_date.value;
      this.gender = this.formValues.controls.gender.value;
      this.tin = this.formValues.controls.tin.value;
      this.nationality = this.formValues.controls.nationality.value;
      this.document_type_id = this.formValues.controls.document_type_id.value;
    }
    this.getDocumentTypes();
    this.translateService.onLangChange.subscribe(() => {
      this.validSortNati();
    });
    this.validSortNati();
  }

  validSortNati() {
    if (this.translateService.currentLang == 'en') {
      this.nationalities = this.nationalities.sort((a, b) => {
        if (a.translate > b.translate) {
          return 1;
        }
        if (a.translate < b.translate) {
          return -1;
        }
        return 0;

      });
    }else{
      this.nationalities = nationalities;
    }
  }

  getDocumentTypes() {
    this.authService
      .getDocumentTypes()
      .pipe(first())
      .subscribe((data) => {
        this.documentTypes = data.data;
      });
  }

  nextStep() {
    this.submitted = true;
    this.back = false;
  }

  submitForm() {
    if (this.OwnerForm.valid) {
      this.nextPress.emit(this.OwnerForm);
      this.OwnerForm.updateValueAndValidity();
      this.showErrorMessage = false;
    }else{
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }
  }
}
