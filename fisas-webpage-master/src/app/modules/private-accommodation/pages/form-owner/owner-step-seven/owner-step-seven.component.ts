import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DocumentTypeModel } from '@fi-sas/webpage/auth/models/document-type.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-owner-step-seven',
  templateUrl: './owner-step-seven.component.html',
  styleUrls: ['./owner-step-seven.component.less']
})

export class OwnerStepSevenComponent implements OnInit {

  _formsCache = [];
  documentTypes: DocumentTypeModel[] = [];
  @Input()
  set formsCache(formsCache) {
    this._formsCache = formsCache;
  }
  @Input() loading = false;
  @Output() nextPress = new EventEmitter();
  @Output() backStep = new EventEmitter();
  @Output() change = new EventEmitter();

  OwnerForm: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private authService:AuthService) {
    this.OwnerForm = this.fb.group({
      back: [true]
    });
  }

  ngOnInit() {
    this.getDocumentTypes();
  }

  getDocumentTypes() {
    this.authService.getDocumentTypes().pipe(first()).subscribe((data) => {
      this.documentTypes = data.data;

    })
  }

  backToStep() {

    this.backStep.emit(this.OwnerForm);
  }

  changeStep(value: number) {
    this.change.emit(value);
  }

  submitForm() {
    this.nextPress.emit();
  }

  findDocument(id) {
    if (id) {
      return this.documentTypes.find(data => data.id == id) ? this.documentTypes.find(data => data.id == id).translations : [];
    }
    return [];
  }

}
