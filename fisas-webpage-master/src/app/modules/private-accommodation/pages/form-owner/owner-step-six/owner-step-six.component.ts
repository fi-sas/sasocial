import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-owner-step-six',
  templateUrl: './owner-step-six.component.html',
  styleUrls: ['./owner-step-six.component.less']
})

export class OwnerStepSixComponent implements OnInit {
  submitted = false;
  issues_receipt: boolean;
  applicant_consent: boolean;
  back = false;
  showErrorMessage = false;
  _regulation: any = null;

  @Output() nextPress = new EventEmitter();
  @Input() formValues: FormGroup = undefined;
  @Input() set regulation(value: any) {
    this._regulation = value;
 }
  OwnerForm: FormGroup = new FormGroup({});
  get f() {
    return this.OwnerForm.controls;
  }
  constructor(private fb: FormBuilder) {
    this.OwnerForm = this.fb.group({
      issues_receipt: [true, [Validators.required]],
      applicant_consent: [null, [Validators.required]],
      back: [false]
    });
  }

  ngOnInit(): void {
    if (this.formValues !== undefined) {
      this.issues_receipt = this.formValues.controls.issues_receipt.value
      this.applicant_consent = this.formValues.controls.applicant_consent.value;
    }
  }

  nextStep() {
    this.submitted = true;
    this.back = false;
  }

  backStep() {
    this.back = true;
  }

  submitForm() {
    let receipt = this.OwnerForm.get('issues_receipt').value;
    if ((this.OwnerForm.valid && receipt == true) || this.back) {
      this.nextPress.emit(this.OwnerForm);
      this.OwnerForm.updateValueAndValidity();
      this.showErrorMessage = false;
    } else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }
  }
}
