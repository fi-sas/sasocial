import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { countries } from './countries-array';

@Component({
    selector: 'app-owner-step-three',
    templateUrl: './owner-step-three.component.html',
    styleUrls: ['./owner-step-three.component.less']
})

export class OwnerStepThreeComponent implements OnInit {
    submitted = false;
    showErrorMessage = false;
    address: string;
    postal_code: string;
    city: string;
    country: string;
    back = false;
    countries = countries;
    mask: string = '0000-000';
    firstEdit = false;

    @Output() nextPress = new EventEmitter();
    @Input() formValues: FormGroup = undefined;
    OwnerForm: FormGroup = new FormGroup({});
    get f() {
      return this.OwnerForm.controls;
    }
    constructor(private fb: FormBuilder) {
      this.OwnerForm = this.fb.group({
        address: [null, [Validators.required,trimValidation]],
        postal_code: [null, Validators.required],
        city: [null, [Validators.required,trimValidation]],
        country: [null, [Validators.required]],
        back: [false]
      });
    }

    ngOnInit(): void {
      if (this.formValues !== undefined) {
        this.firstEdit = true;
        this.address = this.formValues.controls.address.value;
        this.postal_code = this.formValues.controls.postal_code.value;
        this.city = this.formValues.controls.city.value;
        this.country = this.formValues.controls.country.value;
      }
    }

    validCodPost(event){
      if(this.firstEdit) {
        return;
      }
      if(event == 'Portugal' || event == '' || event == undefined || event == null){
        this.mask = "0000-000";
      }else{
        this.mask = "A*";
      }
    }

    nextStep() {
      this.submitted = true;
      this.back = false;
    }

    backStep() {
      this.back = true;
    }

    submitForm() {
        if (this.OwnerForm.valid || this.back) {
            this.nextPress.emit(this.OwnerForm);
            this.OwnerForm.updateValueAndValidity();
            this.showErrorMessage = false;
        } else {
          this.showErrorMessage = true;
          window.scrollTo(0, 0);
        }
    }
}
