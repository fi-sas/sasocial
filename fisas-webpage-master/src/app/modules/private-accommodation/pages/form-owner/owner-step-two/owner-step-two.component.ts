import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-owner-step-two',
  templateUrl: './owner-step-two.component.html',
  styleUrls: ['./owner-step-two.component.less']
})

export class OwnerStepTwoComponent implements OnInit {
  submitted = false;
  email: string;
  phone: string;
  back = false;
  emailRegex = '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|' +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  @Output() nextPress = new EventEmitter();
  @Input() formValues: FormGroup = undefined;
  showErrorMessage = false;
  OwnerForm: FormGroup = new FormGroup({});
  get f() {
    return this.OwnerForm.controls;
  }
  constructor(
    private fb: FormBuilder
  ) {
    this.OwnerForm = this.fb.group({
      phone: [null, [Validators.required]],
      email: [null, [Validators.pattern(this.emailRegex), Validators.required, trimValidation]],
      back: [false]
    });
  }

  ngOnInit(): void {
    if (this.formValues !== undefined) {
      this.phone = this.formValues.controls.phone.value;
      this.email = this.formValues.controls.email.value;
    }
  }

  nextStep() {
    this.submitted = true;
    this.back = false;
  }

  backStep() {
    this.back = true;
  }

  submitForm() {

    if (this.OwnerForm.valid || this.back) {
      this.nextPress.emit(this.OwnerForm);
      this.OwnerForm.updateValueAndValidity(); 
      this.showErrorMessage = false;
    } else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }
  }
}
