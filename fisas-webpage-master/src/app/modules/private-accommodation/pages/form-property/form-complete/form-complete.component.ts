import { Component, OnInit, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize, first } from "rxjs/operators";
import { hasOwnProperty } from "tslint/lib/utils";
import { LanguageModel } from "../../../models/language.model";
import { LanguagesService } from "../../../services/languages.service";
import * as _ from 'lodash';
import { TypologiespaService } from "../../../services/typoligiespa.service";
import * as moment from 'moment';
import { ConfigsService } from "../../../services/configs.service";
import { ListingsModel } from "../../../models/Listings.model";
import { ListingAmenitiesModel } from "../../../models/LIstingAmenities.model";
import { ListingExpensesModel } from "../../../models/ListingExpenses.model";
import { PropertiesService } from "../../../services/properties.service";
import { UiService, MessageType } from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { proj } from 'openlayers';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";
import { NzTabSetComponent } from "ng-zorro-antd";
import { cities } from "@fi-sas/webpage/shared/data/cities";

@Component({
    selector: 'app-form-property-complete',
    templateUrl: './form-complete.component.html',
    styleUrls: ['./form-complete.component.less'],
})
export class FormPropertyCompleteComponent implements OnInit {
    @ViewChild('translationTabsTitle', null)
    translationTabsTitle: NzTabSetComponent;

    @ViewChild('translationTabsWhole', null)
    translationTabsWhole: NzTabSetComponent;

    @ViewChildren('tabRoomTrans') tabRoomTrans: QueryList<NzTabSetComponent>;

    loadingButton = false;
    latitude: number = 39.3999;
    longitude: number = -8.2245;
    latitudeInit: number = 39.3999;
    longitudeInit: number = -8.2245;
    step: number = 0;
    editStart: boolean = false;
    cities = cities;
    maxPublishDays: number = 1;
    maxMedias: number = 1;
    fileList = [];
    loadingSubmit = false;
    owner: number;
    showErrorMessage = false;
    amenitiesData = [
        { formName: 'dishwasher', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN1' },
        { formName: 'private_wc', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN2' },
        { formName: 'parking_garage', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN3' },
        { formName: 'elevator', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN4' },
        { formName: 'internet', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN5' },
        { formName: 'kitchen_access', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN6' },
        { formName: 'fridge', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN7' },
        { formName: 'stove', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN8' },
        { formName: 'microwave', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN9' },
        { formName: 'washing_machine', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN10' },
        { formName: 'heating_system', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN11' },
        { formName: 'cooling_system', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN12' },
        { formName: 'cable_tv', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN13' },
        { formName: 'tv', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_THREE.FORM_ELEMENTS.AMEN14' },
    ];
    expensesData = [
        { formName: 'electricity', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP1' },
        { formName: 'internet', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP2' },
        { formName: 'cable_tv', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP3' },
        { formName: 'furniture', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP4' },
        { formName: 'cleaning', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP5' },
        { formName: 'bedding', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP6' },
        { formName: 'gas', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP7' },
        { formName: 'water', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP8' },
        { formName: 'condo', label: 'PRIVATEACCOMMODATION.FORM_PROPERTY.FORM.STEP_FOUR.FORM_ELEMENTS.EXP9' },
    ];
    formProperty: FormGroup;
    formLocation: FormGroup;
    formAmenities: FormGroup;
    formExpenses: FormGroup;
    formReq: FormGroup;
    formGalary: FormGroup;
    submitted: boolean = false;
    errorRoom: boolean = false;
    languages_loading = false;
    listOfSelectedLanguages = [];
    listOfSelectedLanguagesRooms = [];
    languages: LanguageModel[] = [];
    typologies: any = [];
    applicationID = 0;

    constructor(private router: Router, private fb: FormBuilder, private languagesService: LanguagesService,
        private typologyService: TypologiespaService, private configsService: ConfigsService, private propertiesService: PropertiesService,
        private activedRoute: ActivatedRoute, private uiService: UiService, private translateService: TranslateService, private authService: AuthService) {

        this.formProperty = this.fb.group({
            usable_space: [null, [Validators.required]],
            typology_id: [null, [Validators.required]],
            house_typology: [null, [Validators.required]],
            translations: this.fb.array([]),
            price: [null],
            area: [null],
            floor: [null, [Validators.required]],
            rooms: this.fb.array([])
        });

        this.formLocation = this.fb.group({
            address: [null, [Validators.required, trimValidation]],
            address_no: [null, [Validators.required, trimValidation]],
            postal_code: [null, [Validators.required]],
            city: [null, [Validators.required]],
            latitude: [null],
            longitude: [null],
        });

        this.formAmenities = this.fb.group({
            kitchen_access: [],
            microwave: [],
            stove: [],
            fridge: [],
            dishwasher: [],
            washing_machine: [],
            elevator: [],
            heating_system: [],
            cooling_system: [],
            tv: [],
            cable_tv: [],
            internet: [],
            parking_garage: [],
            private_wc: [],
        });

        this.formExpenses = this.fb.group({
            water: [],
            gas: [],
            condo: [],
            electricity: [],
            cable_tv: [],
            internet: [],
            furniture: [],
            cleaning: [],
            bedding: []
        });
        this.formReq = this.fb.group({
            allows_smokers: [null, Validators.required],
            allows_pets: [null, Validators.required],
            gender: [null, Validators.required],
            occupation: [null, Validators.required],
            start_date: [null, Validators.required],
            end_date: [null, Validators.required]
        });

        this.formGalary = this.fb.group({
            medias: [],
        });

        this.cities = this.cities.sort((a, b) => {
            if (a.city > b.city) {
              return 1;
            }
            if (a.city < b.city) {
              return -1;
            }
            return 0;
    
          });
    }


    ngOnInit() {
        this.activedRoute.queryParams.subscribe((params) => {
            let propertyId = params.property_id || 0;

            this.applicationID = propertyId;
        })
        this.getLanguages();
        this.getTypologies();
        this.maxPublishD();
        this.getMaxMedia();  
    }
    

    get fp() { return this.formProperty.controls; }
    get fl() { return this.formLocation.controls; }
    get fa() { return this.formAmenities.controls; }
    get fe() { return this.formExpenses.controls; }
    get fr() { return this.formReq.controls; }
    get fg() { return this.formGalary.controls; }

    cancelApplication(): void {
        this.router.navigateByUrl('privateaccommodation/owner-properties');
    }

    changeStep(event) {
        this.step = event;
    }

    changeCity(event){
        const findCity = this.cities.find((city)=>city.city == event);
        if(findCity) {
            this.longitude = Number(findCity.lng);
            this.latitude = Number(findCity.lat);
            this.latitudeInit = Number(findCity.lat);
            this.longitudeInit = Number(findCity.lng);
            this.formLocation.get('latitude').setValue(this.latitude);
            this.formLocation.get('longitude').setValue(this.longitude);
        }
    }

    getValueFormat(value) {
        return Number(value).toFixed(6);
    }
    
    getProperty(id) {
        this.propertiesService
            .get(id)
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.setValueForm(results.data[0]);
            });
    }

    getLanguages() {
        this.languagesService
            .listLanguages()
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.languages = results.data;
                //edit
                if (this.applicationID != 0) {
                    this.editStart = true;
                    this.getProperty(this.applicationID);
                }else{
                    this.listOfSelectedLanguages = [];
                    this.startTranslation('');
                }
            });
    }

    getTypologies() {
        this.typologyService
            .list()
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.typologies = results.data;
            });
    }

    maxPublishD() {
        this.configsService
            .maxPublishDay()
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.maxPublishDays = results.data[0];
            });
    }

    getMaxMedia() {
        this.configsService
            .maxMedias()
            .pipe(
                first()
            )
            .subscribe((results) => {
                this.maxMedias = results.data[0];
            });
    }

    getLatLong(event) {
        var lonlat = proj.transform(event.coordinate, 'EPSG:3857', 'EPSG:4326');
        
        this.longitude = lonlat[0];
        this.latitude = lonlat[1];
        this.formLocation.get('latitude').setValue(this.latitude);
        this.formLocation.get('longitude').setValue(this.longitude);
    }

    setValueForm(value) {
        //Part1
        this.formProperty.patchValue({
            typology_id: value.typology_id ? value.typology_id : null,
            house_typology: value.house_typology ? (value.house_typology == 'HOUSE' ? 'House' : 'Apartment') : null,
            floor: value.floor ? value.floor : null,
            usable_space: value.usable_space ? (value.usable_space) : null,
            price: value.price ? value.price : null,
            area: value.area ? value.area : null
        })

        this.listOfSelectedLanguages = [];
        this.startTranslation(value);
        if (value.usable_space == "SHARED_ROOM") {
            this.listOfSelectedLanguagesRooms = [];

            value.rooms.map(room => {
                this.addNewRoom(room, value)
            })
        }

        //Part2
        this.formLocation.patchValue({
            address: value.address ? value.address : null,
            address_no: value.address_no ? value.address_no : null,
            postal_code: value.postal_code ? value.postal_code : null,
            city: value.city ? value.city : null,
            longitude: value.longitude ? value.longitude : null,
            latitude: value.latitude ? value.latitude : null,
        })
        if (this.formLocation.get('latitude').value != null) {
            this.latitudeInit = this.formLocation.get('latitude').value;
            this.latitude = this.formLocation.get('latitude').value;
        } else {
            this.latitude = 39.3999;
            this.latitudeInit = 39.3999;
        }

        if (this.formLocation.get('longitude').value != null) {
            this.longitudeInit = this.formLocation.get('longitude').value;
            this.longitude = this.formLocation.get('longitude').value;
        } else {
            this.longitudeInit = -8.2245;
            this.longitude = -8.2245;
        }



        //Part3
        if (value.amenities) {
            this.formAmenities.patchValue({
                kitchen_access: (value.amenities.kitchen_access === false) ? null : true,
                microwave: (value.amenities.microwave === false) ? null : true,
                stove: (value.amenities.stove === false) ? null : true,
                fridge: (value.amenities.fridge === false) ? null : true,
                dishwasher: (value.amenities.dishwasher === false) ? null : true,
                washing_machine: (value.amenities.washing_machine === false) ? null : true,
                elevator: (value.amenities.elevator === false) ? null : true,
                heating_system: (value.amenities.heating_system === false) ? null : true,
                cooling_system: (value.amenities.cooling_system === false) ? null : true,
                tv: (value.amenities.tv === false) ? null : true,
                cable_tv: (value.amenities.cable_tv === false) ? null : true,
                internet: (value.amenities.internet === false) ? null : true,
                parking_garage: (value.amenities.parking_garage === false) ? null : true,
                private_wc: (value.amenities.private_wc === false) ? null : true,
            })
        }


        //Part4
        if (value.expenses) {
            this.formExpenses.patchValue({
                water: (value.expenses.water === false) ? null : true,
                gas: (value.expenses.gas === false) ? null : true,
                condo: (value.expenses.condo === false) ? null : true,
                cable_tv: (value.expenses.cable_tv === false) ? null : true,
                internet: (value.expenses.internet === false) ? null : true,
                electricity: (value.expenses.electricity === false) ? null : true,
                cleaning: (value.expenses.cleaning === false) ? null : true,
                bedding: (value.expenses.bedding === false) ? null : true,
                furniture: (value.expenses.furniture === false) ? null : true,
            })
        }


        //part5
        this.formReq.patchValue({
            allows_smokers: (value.allows_smokers === true) ? 'Smokers allowed' : 'No smokers allowed',
            allows_pets: (value.allows_pets === true) ? 'Pets allowed' : 'No pets allowed',
            gender: value.gender == 'I' ? 'Indifferent' : value.gender == 'F' ? 'Feminine' : 'Male',
            occupation: value.occupation == 'I' ? 'Indifferent' : value.occupation == 'N' ? 'National' : 'International',
            start_date: value.start_date,
            end_date: value.end_date,
        })

        //part6
        value.medias.forEach(media => this.fileList.push(media.file_id));
        this.owner = value.owner.id;

    }

    changeUsableSpace(event) {
        let add: FormArray;
        add = this.formProperty.get('translations') as FormArray;

        if (event == 'WHOLE_HOUSE') {
            this.formProperty.get('area').setValidators(Validators.required);
            this.formProperty.get('price').setValidators(Validators.required);
            add.controls.forEach(control => {
                control.get('description').setValidators([Validators.required]);
              });
            this.getRooms().controls = [];
            this.errorRoom = false;
            this.editStart = false;
        } else {
            this.listOfSelectedLanguagesRooms = [];
            add.controls.forEach(control => {
                control.get('description').clearValidators();
                control.get('description').setValue('');
              });
            this.formProperty.get('area').setValue(null);
            this.formProperty.get('price').setValue(null);
            this.formProperty.get('area').setValidators(null);
            this.formProperty.get('price').setValidators(null);
            this.editStart = false;
        }
        add.controls.forEach(control => {
            control.get('description').updateValueAndValidity();
        });
        this.formProperty.get('area').updateValueAndValidity();
        this.formProperty.get('price').updateValueAndValidity();

    }

    getRooms(): FormArray {
        return this.formProperty.get("rooms") as FormArray;
    }

    getTranslations(): FormArray {
        return this.formProperty.get("translations") as FormArray;
    }

    addNewRoom(room?: any, form?: any) {
        this.errorRoom = false;
        let rooms: FormArray;
        rooms = this.formProperty.get('rooms') as FormArray;
        rooms.push(this.fb.group({
            price: [room ? room.price : '', [Validators.required]],
            room_type: [room ? room.room_type : '', [Validators.required]],
            shared_wc: [room ? room.shared_wc : null, [Validators.required]],
            translations: this.fb.array([]),
            area: [room ? room.area : '', [Validators.required]]
        }));
        if (room == undefined) {
            this.startTranslationRooms(rooms.length - 1, form, true);
        } else {
            this.startTranslationRooms(rooms.length - 1, form, false);
        }
    }

    removeRoom(i: number) {
        this.getRooms().removeAt(i);
    }

    startTranslation(value) {
        if (value !== '') {
                value.translations.map((translation) => {
                    this.addTranslation(
                        translation.language_id,
                        translation.description,
                        translation.name
                    );
                    this.listOfSelectedLanguages.push(translation.language_id);
                });

        } else {
            if (hasOwnProperty(this.languages[0], 'id')) {
                this.addTranslation(this.languages[0].id, '');
                this.listOfSelectedLanguages.push(this.languages[0].id);
            }
        }
    }

    startTranslationRooms(index: number, value, newRoom: boolean) {

        this.listOfSelectedLanguagesRooms[index] = [];
        if (value !== '' && value != undefined && newRoom == false) {
            if (value.usable_space == "SHARED_ROOM") {
                value.rooms[index].translations.map((translation) => {
                    this.addTranslationRooms(
                        index,
                        translation.language_id,
                        translation.description
                    );
                    this.listOfSelectedLanguagesRooms[index].push(translation.language_id);
                });
            } else {
                if (this.languages.length > 0 && hasOwnProperty(this.languages[0], 'id')) {
                    this.addTranslationRooms(index, this.languages[0].id, '');
                    this.listOfSelectedLanguagesRooms[index].push(this.languages[0].id);
                }
            }

        } else {
            if (this.languages.length > 0 && hasOwnProperty(this.languages[0], 'id')) {
                this.addTranslationRooms(index, this.languages[0].id, '');
                this.listOfSelectedLanguagesRooms[index].push(this.languages[0].id);
            }
        }
    }

    getDescriptionTranslation(index: number): FormArray {
        const rooms = this.formProperty.get('rooms') as FormArray;
        let aux = rooms.at(index) as FormGroup;
        return aux.controls.translations as FormArray;
    }

    getLanguageName(language_id: number) {
        const language = this.languages.find((l) => l.id === language_id);
        return language ? language.name : 'Sem informação';
    }

    addTranslation(language_id: number, description?: string,name?:string) {
        let translations = this.formProperty.controls.translations as FormArray;
        translations.push(
            this.fb.group({
                language_id: [language_id ? language_id : '', [Validators.required]],
                description: [description ? description : ''],
                name: [name ? name : '', [Validators.required, trimValidation]],
            })
        );
    }

    addTranslationRooms(index: number, language_id: number, description?: string) {
        const translations = this.getDescriptionTranslation(index);
        translations.push(
            this.fb.group({
                language_id: [language_id ? language_id : '', [Validators.required]],
                description: [description ? description : '', [Validators.required, trimValidation]],
            })
        );
    }

    changeLanguage() {
        const translations = this.formProperty.controls.translations as FormArray;
        const languagesIDS = this.convertTranslationsToLanguageIDS(translations);

        if (this.listOfSelectedLanguages.length > languagesIDS.length) {
            this.addTranslation(
                _.difference(this.listOfSelectedLanguages, languagesIDS)[0],
                '',''
            );
        } else {
            this.getTranslations().removeAt(
                this.getTranslations().value.findIndex(
                    (trans: any) =>
                        trans.language_id ===
                        _.difference(languagesIDS, this.listOfSelectedLanguages)[0]
                )
            );
        }
    }

    changeLanguageRooms(index: number) {
        const translations = this.getDescriptionTranslation(index);
        const languagesIDS = this.convertTranslationsToLanguageIDS(translations);

        if (this.listOfSelectedLanguagesRooms[index].length > languagesIDS.length) {
            this.addTranslationRooms(index,
                _.difference(this.listOfSelectedLanguagesRooms[index], languagesIDS)[0],
                ''
            );
        } else {
            this.getDescriptionTranslation(index).removeAt(
                this.getDescriptionTranslation(index).value.findIndex(
                    (trans: any) =>
                        trans.language_id ===
                        _.difference(languagesIDS, this.listOfSelectedLanguagesRooms[index])[0]
                )
            );
        }
    }

    convertTranslationsToLanguageIDS(translations: any) {
        let languagesIDS = [];
        translations.value.forEach((languageID: any) => {
            languagesIDS.push(languageID.language_id);
        });
        return languagesIDS;
    }

    nextStep(stepNow: number) {
        this.submitted = true;
        switch (stepNow) {
            case 1: {
                this.translationValid();
                if (this.fp.usable_space.value == 'SHARED_ROOM') {
                    if (this.fp.rooms.value.length == 0) {
                        this.errorRoom = true;
                    } else {
                        this.errorRoom = false;
                        for (const room of this.fp.rooms.value) {
                            if (room.translations.length === 0) {
                                this.errorRoom = true;
                            }
                        }

                    }
                }
                if (this.fp.translations.value.length === 0 && this.fp.usable_space.value == 'WHOLE_HOUSE') {
                    this.fp.translations.setErrors({ 'incorrect': true });
                }
                if (this.formProperty.valid && !this.errorRoom) {
                    this.step = 1;
                    this.submitted = false;
                    this.scrollTop();
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
            case 2: {
                if (this.formLocation.valid) {
                    this.step = 2;
                    this.submitted = false;
                    this.scrollTop();
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
            case 3: {
                if (this.formAmenities.valid) {
                    this.step = 3;
                    this.submitted = false;
                    this.scrollTop();
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
            case 4: {
                if (this.formExpenses.valid) {
                    this.step = 4;
                    this.submitted = false;
                    this.scrollTop();
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
            case 5: {
                if (this.formReq.valid) {
                    this.step = 5;
                    this.submitted = false;
                    this.scrollTop();
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
            case 6: {
                if (this.fileList.length !== 0) {
                    this.step = 6;
                    this.submitted = false;
                    this.scrollTop();
                    this.formGalary.get("medias").setValue(this.fileList);
                    this.showErrorMessage = false;
                } else {
                    this.showErrorMessage = true;
                    this.scrollTop();
                }
                break;
            }
        }
    }

    translationValid() {

        let tabIndex = 0;
        for (const t in this.getTranslations().controls) {
          if (t) {
            const tt = this.getTranslations().get(t) as FormGroup;
            if (!tt.valid) {
              this.translationTabsTitle.nzSelectedIndex = tabIndex;
              break;
            }
          }
          tabIndex++;
        }
    
        let tabIndexTranWhole = 0;
        for (const t in this.getTranslations().controls) {
          if (t) {
            const tt = this.getTranslations().get(t) as FormGroup;
            if (!tt.valid) {
              if(this.translationTabsWhole) {
                this.translationTabsWhole.nzSelectedIndex = tabIndexTranWhole;
                break;
              }
              
            }
          }
          tabIndexTranWhole++;
        }
    
       let tabIndexTranRoom = 0;
        for (const t of this.getRooms().controls) {
          const trans = t.get('translations');
          if (t) {
            const tt = trans as FormArray;
            if (!tt.valid) {
              for(let translation of tt.controls) {
                if(!translation.valid) {
                  this.tabRoomTrans.toArray()[this.getRooms().controls.indexOf(t)].nzSelectedIndex = tabIndexTranRoom;
                }
                tabIndexTranRoom++;
              }
            }
          }
         
        }
      }

    scrollTop() {
        window.scrollTo(0, 0);
    }

    submit() {

        if (!this.authService.hasPermission('private_accommodation:listings:create')) {
            this.uiService.showMessage(MessageType.warning, 'O utilizador não têm acesso ao serviço solicitado');
            return;
        }

        //create
        let sendValues: ListingsModel = new ListingsModel();
        //Part1
        sendValues.house_typology = this.fp.house_typology.value == 'House' ? 'HOUSE' : 'APARTMENT';
        sendValues.typology_id = this.fp.typology_id.value;
        sendValues.floor = this.fp.floor.value;
        sendValues.usable_space = this.fp.usable_space.value;
        sendValues.translations = this.fp.translations.value;
        if (this.fp.usable_space.value == 'WHOLE_HOUSE') {
            sendValues.price = this.fp.price.value;
            sendValues.area = this.fp.area.value;
            sendValues.rooms_number = 0;
        } else {
            sendValues.rooms = this.fp.rooms.value;
            sendValues.rooms_number = this.fp.rooms.value.length;
        }

        //Part2
        sendValues.address = this.fl.address.value;
        sendValues.address_no = this.fl.address_no.value;
        sendValues.longitude = this.fl.longitude.value;
        sendValues.latitude = this.fl.latitude.value;
        sendValues.city = this.fl.city.value;
        sendValues.postal_code = this.fl.postal_code.value;

        //Part3
        sendValues.amenities = new ListingAmenitiesModel();
        sendValues.amenities = new ListingAmenitiesModel();
        sendValues.amenities.kitchen_access = this.fa.kitchen_access.value == null ? false : this.fa.kitchen_access.value;
        sendValues.amenities.microwave = this.fa.microwave.value == null ? false : this.fa.microwave.value;
        sendValues.amenities.stove = this.fa.stove.value == null ? false : this.fa.stove.value;
        sendValues.amenities.fridge = this.fa.fridge.value == null ? false : this.fa.fridge.value;
        sendValues.amenities.dishwasher = this.fa.dishwasher.value == null ? false : this.fa.dishwasher.value;
        sendValues.amenities.washing_machine = this.fa.washing_machine.value == null ? false : this.fa.washing_machine.value;
        sendValues.amenities.elevator = this.fa.elevator.value == null ? false : this.fa.elevator.value;
        sendValues.amenities.heating_system = this.fa.heating_system.value == null ? false : this.fa.heating_system.value;
        sendValues.amenities.cooling_system = this.fa.cooling_system.value == null ? false : this.fa.cooling_system.value;
        sendValues.amenities.tv = this.fa.tv.value == null ? false : this.fa.tv.value;
        sendValues.amenities.cable_tv = this.fa.cable_tv.value == null ? false : this.fa.cable_tv.value;
        sendValues.amenities.internet = this.fa.internet.value == null ? false : this.fa.internet.value;
        sendValues.amenities.parking_garage = this.fa.parking_garage.value == null ? false : this.fa.parking_garage.value;
        sendValues.amenities.private_wc = this.fa.private_wc.value == null ? false : this.fa.private_wc.value;

        //part4
        sendValues.expenses = new ListingExpensesModel();
        sendValues.expenses.water = this.fe.water.value == null ? false : this.fe.water.value;
        sendValues.expenses.gas = this.fe.gas.value == null ? false : this.fe.gas.value;
        sendValues.expenses.condo = this.fe.condo.value == null ? false : this.fe.condo.value;
        sendValues.expenses.electricity = this.fe.electricity.value == null ? false : this.fe.electricity.value;
        sendValues.expenses.cable_tv = this.fe.cable_tv.value == null ? false : this.fe.cable_tv.value;
        sendValues.expenses.internet = this.fe.internet.value == null ? false : this.fe.internet.value;
        sendValues.expenses.furniture = this.fe.furniture.value == null ? false : this.fe.furniture.value;
        sendValues.expenses.cleaning = this.fe.cleaning.value == null ? false : this.fe.cleaning.value;
        sendValues.expenses.bedding = this.fe.bedding.value == null ? false : this.fe.bedding.value;

        //part5
        sendValues.allows_pets = this.fr.allows_pets.value === 'Pets allowed';
        sendValues.allows_smokers = this.fr.allows_smokers.value === 'Smokers allowed';
        if (this.fr.gender.value === 'Indifferent') {
            sendValues.gender = 'I';
        } else if (this.fr.gender.value === 'Feminine') {
            sendValues.gender = 'F';
        } else {
            sendValues.gender = 'M';
        }

        if (this.fr.occupation.value === 'Indifferent') {
            sendValues.occupation = 'I';
        } else if (this.fr.occupation.value === 'National') {
            sendValues.occupation = 'N';
        } else {
            sendValues.occupation = 'F';
        }

        sendValues.start_date = this.fr.start_date.value;
        sendValues.end_date = this.fr.end_date.value;

        //Part6
        sendValues.medias = this.fileList.reduce((acc, file) => {
            acc.push({ file_id: file });
            return acc;
        }, []);

        this.loadingButton = true;
        if (this.applicationID === 0) {
            this.propertiesService
                .save(sendValues)
                .pipe(first(), finalize(() => this.loadingButton = false))
                .subscribe(
                    () => {
                        this.uiService.showMessage(
                            MessageType.success,
                            this.translateService.instant(
                                'PRIVATEACCOMMODATION.FORM_PROPERTY.SUCCESS_CREATE'
                            )
                        );
                        this.loadingSubmit = false;
                        this.router.navigateByUrl('privateaccommodation/owner-properties');
                    },
                    () => {
                        this.loadingSubmit = false;
                    }
                );
        } else {
            sendValues.owner_id = this.owner;
            sendValues.status = 'Pending';
            this.propertiesService
                .update(this.applicationID, sendValues)
                .pipe(first())
                .subscribe(
                    () => {
                        this.uiService.showMessage(
                            MessageType.success,
                            this.translateService.instant(
                                'PRIVATEACCOMMODATION.FORM_PROPERTY.SUCCESS_UPDATE'
                            )
                        );
                        this.loadingSubmit = false;
                        this.router.navigateByUrl('privateaccommodation/owner-properties');
                    },
                    () => {
                        this.loadingSubmit = false;
                    }
                );
        }
    }

    clearEndDate() {
        this.formReq.get("end_date").setValue(null);
    }

    disableStartDate = (startDate: Date) => {
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() - 1);
        return moment(startDate).isBefore(currentDate) || moment(this.formReq.get("end_date").value).isBefore(startDate);
    }

    disableEndDate = (endDate: Date) => {
        if (this.fr.start_date.value !== null) {
            return !moment(endDate).isBetween(moment(this.fr.start_date.value), moment(this.fr.start_date.value).add(this.maxPublishDays, 'days'));
        } else {
            return moment(endDate).isAfter(moment(new Date()).add(this.maxPublishDays, 'days'));
        }
    }

    onFileAdded(fileId) {
        this.submitted = false;
        this.fileList.push(fileId);
    }

    onFileDeleted(fileId) {
        this.fileList = this.fileList.filter(f => f !== fileId);
    }
}
