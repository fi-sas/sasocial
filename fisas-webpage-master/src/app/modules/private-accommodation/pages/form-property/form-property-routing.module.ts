import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPropertyCompleteComponent } from './form-complete/form-complete.component';





const routes: Routes = [
  {
    path: '',
    component: FormPropertyCompleteComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormPropertyRoutingModule { }
