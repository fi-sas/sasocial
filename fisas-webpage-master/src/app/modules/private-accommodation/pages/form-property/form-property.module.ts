import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FormPropertyRoutingModule } from './form-property-routing.module';
import { FormPropertyCompleteComponent } from './form-complete/form-complete.component';
import { PropertyFormReviewComponent } from './form-review/form-review.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';


@NgModule({
  declarations: [
        PropertyFormReviewComponent,
        FormPropertyCompleteComponent,
        
    ],
  imports: [
    CommonModule,
    SharedModule,
    FormPropertyRoutingModule,
    AngularOpenlayersModule
  ],
})
export class FormPropertyModule { }
