import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import { first } from "rxjs/operators";
import { LanguageModel } from "../../../models/language.model";

@Component({
    selector: 'app-property-form-review',
    templateUrl: './form-review.component.html',
    styleUrls: ['./form-review.component.less']
})

export class PropertyFormReviewComponent implements OnInit {
    @Input() languages: LanguageModel[] = [];
    @Input() formProperty;
    @Input() formLocation;
    @Input() formAmenities;
    @Input() formExpenses;
    @Input() formReq;
    @Input() formGalary;
    @Input() typologies: any = [];
    @Output() stepValue = new EventEmitter();

    medias = [];
    loadingFiles: boolean = false;

    constructor(private fileService: FilesService) { }

    ngOnInit() {
        this.loadMedias();
    }

    loadMedias() {
        this.fileService
            .listfiles(this.convertGalleryToFilesIDS())
            .pipe(first())
            .subscribe(
                (files) => {
                    this.medias = files.data;
                    this.loadingFiles = false;
                },
                () => {
                    this.loadingFiles = false;
                }
            );
    }

    convertGalleryToFilesIDS() {
        let filesIDS = [];
        this.formGalary.value.medias.forEach((fileID) => {
            filesIDS.push(fileID);
        });
        return filesIDS;
    }


    filterLanguage(id: number) {
        let aux: string;
        if (this.languages.length > 0) {
            this.languages.filter((fil) => {
                if (fil.id == id) {
                    aux = fil.name;
                }
            })
        }
        return aux;
    }

    getTypology(typology) {
        return this.typologies.find(t => t.id === typology).name;
    }

    changeStep(value) {
        this.stepValue.emit(value);
    }
}
