import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDetailComponent } from './list-detail.component';



const routes: Routes = [
  {
    path: '',
    component: ListDetailComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListDetailRoutingModule { }
