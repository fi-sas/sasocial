import { tap, catchError, finalize, first } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { PropertiesService } from '../../services/properties.service';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { TypologiespaService } from '../../services/typoligiespa.service';
import { ListingsModel } from '../../models/Listings.model';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { LanguagesService } from '../../services/languages.service';
import { LanguageModel } from '../../models/language.model';
import { NzCarouselComponent } from 'ng-zorro-antd';


@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.less'],
})
export class ListDetailComponent implements OnInit {


  listProperties = [];
  details: ListingsModel = null; // { owner_id: 0 };
  languages: LanguageModel[] = [];
  typologies = [];
  owner = {};
  map: any;
  firstTime = true;
  // layer: VectorLayer = new VectorLayer();

  listingID: number;

  loadingListing = false;
  logged: boolean;

  listDetail;

  constructor(
    private propertiesService: PropertiesService,
    private route: ActivatedRoute,
    private rout: Router,
    private typologiespaService: TypologiespaService,
    private authService: AuthService,
    private languagesService: LanguagesService
  ) {
  }



  ngOnInit() {
    this.loadLanguages();
    this.loadingListing = true;
    this.logged = this.authService.getIsLogged();
    this.listDetail = localStorage.getItem('detail-owner-property');
    localStorage.removeItem('detail-owner-property');

    this.route.params.subscribe((params) => {
      this.listingID = params['id'];
      forkJoin([
        this.propertiesService.get(this.listingID).pipe(first()),
        this.typologiespaService.list().pipe(first()),
      ]) .pipe(
        first(),
        finalize(() => {
        this.loadingListing = false
        }),
      ).subscribe(
        ([properties, typologies]) => {
          this.details =
            properties.data && properties.data.length > 0
              ? properties.data[0]
              : null;
          this.typologies = typologies.data || [];
          this.saveVisualization();

        },
      );

    });

  }

  backList(){
    if(this.listDetail == 'true'){
      this.rout.navigate(['privateaccommodation/owner-properties']);
    }else{
      this.rout.navigate(['privateaccommodation/list-host']);
    }
  }

  loadLanguages() {
    this.languagesService
      .listLanguages()
      .pipe(
        first()
      )
      .subscribe((results) => {
        this.languages = results.data;
      });
  }

  getTypologyName(typeid) {
    const obj = this.typologies.find((t) => t.id === typeid);
    return obj ? obj.name : '';
  }

  saveVisualization() {
    this.propertiesService.visualizations(this.listingID).pipe(first()).subscribe(() => { });
  }

  arrowClick($event: MouseEvent, type: string, carourel:NzCarouselComponent) {
    $event.stopPropagation();
    if(type == 'next') {
      carourel.next();
    }else {
      carourel.pre();
    }
  }
}



/**
 * MORE ABOUT MAPS:::
 *
 * ->Get localization-----------------------
 * //this.firstRequestForm.controls.longitude.setValue(lonlat[0]);
 * //this.firstRequestForm.controls.latitude.setValue(lonlat[1]);
 *
 * -> put pin in location------------------
 * add this code::
 * setPinLocation(event) {
 *  const lonlat = transform(this.map.getEventCoordinate(event), 'EPSG:3857', 'EPSG:4326');
 *   if (!this.firstTime) { this.map.removeLayer(this.layer); }
 *   const iconFeature = new Feature({ geometry: new Point(fromLonLat([lonlat[0], lonlat[1]])), });
 *   const iconStyle = new Style({ image: new Icon({ anchor: [0.5, 30], anchorXUnits: 'fraction', anchorYUnits: 'pixels', src: 'assets/imgs/map-marker-icon.png' }) });
 *   iconFeature.setStyle(iconStyle);
 *   this.layer = new VectorLayer({ source: new VectorSource({ features: [iconFeature] }) });
 *   this.map.addLayer(this.layer);
 * }
 *
 * also add  this to html::
 * (dblclick)="setPinLocation()"
 */
