import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ListDetailRoutingModule } from './list-detail-routing.module';
import { ListDetailComponent } from './list-detail.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';


@NgModule({
  declarations: [
        ListDetailComponent
    ],
  imports: [
    CommonModule,
    SharedModule,
    ListDetailRoutingModule,
    AngularOpenlayersModule
  ]
})
export class ListDetailModule { }
