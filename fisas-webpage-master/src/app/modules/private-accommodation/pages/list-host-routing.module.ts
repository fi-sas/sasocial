import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListHostComponent } from './list-host.component';

const routes: Routes = [
  {
    path: '',
    component: ListHostComponent,
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHostRoutingModule {}
