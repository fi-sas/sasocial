import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import {
  IMinMaxPrice,
  ITypology,
  ListingQueryParams,
  ListingsModel,
  PropertiesPriceInterval,
} from "../models";
import { PropertiesService } from "../services/properties.service";
import { Resource } from "@fi-sas/core";
import { TypologiespaService } from "../services/typoligiespa.service";
import { SERVICE_IDS } from "@fi-sas/webpage/shared/models";
import { finalize, first } from "rxjs/operators";

@Component({
  selector: "app-list-host",
  templateUrl: "./list-host.component.html",
  styleUrls: ["./list-host.component.less"],
})
export class ListHostComponent {
  minMaxPrice: IMinMaxPrice = { min: 0, max: 0 };
  mapPrice: { [price: number]: number } = {};

  typologies: ITypology[] = [];

  pageIndex = 1;
  pageSize = 12;
  totalProperties = 0;
  totalPages = 0;
  listProp = false;
  propertiesList: ListingsModel[] = [];

  isLoading: boolean;

  queryParams: ListingQueryParams;

  readonly serviceId = SERVICE_IDS.PRIVATE_ACCOMMODATION;

  constructor(
    private activatedRoute: ActivatedRoute,
    private propertiesService: PropertiesService,
    private route: Router,
    private typologyService: TypologiespaService
  ) {
    this.getTopologies();
    this.getPricesInterval();
    this.checkProperties();
    this.queryParams = new ListingQueryParams(
      this.activatedRoute.snapshot.queryParams
    );
    this.activatedRoute.queryParams.subscribe(() => this.getProperties());
  }

  changePageIndex(pageIndex: number): void {
    this.pageIndex = pageIndex;
    this.getProperties();
  }

  setLocalities(values: string[]) {
    this.queryParams.city = values;
    this.setUrlParams();
  }

  setPrice(price: IMinMaxPrice) {
    this.queryParams.minPrice = price.min;
    this.queryParams.maxPrice = price.max;
    this.setUrlParams();
  }

  setTypology(typology: number) {
    this.queryParams.typology = typology;
    this.setUrlParams();
  }

  setAmenities(amenities: string[]) {
    this.queryParams.amenities = amenities;
    this.setUrlParams();
  }

  setExpenses(expenses: string[]) {
    this.queryParams.expenses = expenses;
    this.setUrlParams();
  }

  onClear() {
    this.queryParams.clear();
    this.setUrlParams();
  }

  private setUrlParams() {
    this.route.navigate(["privateaccommodation/list-host"], {
      queryParams: this.queryParams.toUrlParams(),
    });
  }

  private async getProperties() {
    this.isLoading = true;

    this.propertiesService
      .listSearch(
        this.pageIndex,
        this.pageSize,
        this.queryParams.toHttpParams()
      )
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((result) => {
        this.propertiesList = result.data;
        this.totalProperties = result.link.total;
        this.totalPages = Math.ceil(result.link.total / this.pageSize);
        this.setPriceMap();
      });
  }

  private async getTopologies() {
    let result: Resource<ITypology>;
    try {
      result = await this.typologyService.list().toPromise();
    } catch (_) {
      return;
    }
    this.typologies = result.data;
  }

  private setPriceMap() {
    const mapPrice = {};
    this.propertiesList.forEach((p) => {
      if (!(p.price in mapPrice)) {
        mapPrice[p.price] = 0;
      }
      mapPrice[p.price] += 1;
    });
    this.mapPrice = mapPrice;
  }

  private async getPricesInterval() {
    let result: Resource<PropertiesPriceInterval>;
    try {
      result = await this.propertiesService
        .getPropertiesPriceInterval()
        .toPromise();
    } catch (_) {
      return;
    }
    this.minMaxPrice = {
      min: result.data[0].min_price,
      max: result.data[0].max_price,
    };
  }

  checkProperties() {
    this.propertiesService
      .checkProperties()
      .pipe(first())
      .subscribe((data) => {
        if (data.data.length > 0) {
          this.listProp = data.data[0];
        }
      });
  }
}
