import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ListHostComponent } from './list-host.component';
import { ListHostRoutingModule } from './list-host-routing.module';
import { PpLocalityComponent } from '../pop-up-pa/pp-locality/pp-locality.component';
import { PpPriceComponent } from '../pop-up-pa/pp-price/pp-price.component';
import { PpTypeComponent } from '../pop-up-pa/pp-type/pp-type.component';
import { PpAmenitiesComponent } from '../pop-up-pa/pp-amenities/pp-amenities.component';
import { PpExpensesComponent } from '../pop-up-pa/pp-expenses/pp-expenses.component';
import { ListPrivateResidencesComponent } from '../pages/list-private-residences/list-private-residences.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    ListHostComponent,
    PpLocalityComponent,
    PpPriceComponent,
    PpTypeComponent,
    PpAmenitiesComponent,
    PpExpensesComponent,
    ListPrivateResidencesComponent,
  ],
  imports: [CommonModule, SharedModule, ListHostRoutingModule, ChartsModule],
})
export class ListHostModule {}
