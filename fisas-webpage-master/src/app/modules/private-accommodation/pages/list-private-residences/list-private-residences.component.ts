import { Component, Input } from '@angular/core';
import { NzCarouselComponent } from 'ng-zorro-antd';

import { ListingsModel } from '../../models/Listings.model';

@Component({
  selector: 'app-list-private-residences',
  templateUrl: './list-private-residences.component.html',
  styleUrls: ['./list-private-residences.component.less'],
})
export class ListPrivateResidencesComponent {
  @Input() listProperties: ListingsModel[] = [];

  
  arrowClick($event: MouseEvent, type: string, carourel:NzCarouselComponent) {
    $event.stopPropagation();
    if(type == 'next') {
      carourel.next();
    }else {
      carourel.pre();
    }
  }
}
