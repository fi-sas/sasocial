import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerAreaComponent } from './owner-area.component';


const routes: Routes = [
  {
    path: '',
    component: OwnerAreaComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerAreaRoutingModule { }
