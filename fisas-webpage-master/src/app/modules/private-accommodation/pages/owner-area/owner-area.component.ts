import { first } from 'rxjs/operators';
import { ConfigsService } from './../../services/configs.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import * as _ from 'lodash';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { TextApresentationsModel } from '@fi-sas/webpage/shared/models/text-apresentation.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-owner-area',
  templateUrl: './owner-area.component.html',
  styleUrls: ['./owner-area.component.less']
})

export class OwnerAreaComponent implements OnInit {
  info: TextApresentationsModel[] = [];
  logged = false;
  constructor(
    private router: Router,
    private authService: AuthService,
    private configsService: ConfigsService,
    private configurationsService: ConfigurationsService,
    private sanitizer: DomSanitizer
  ) {
    this.logged = this.authService.getIsLogged();
  }

  buttonOwnerRegisterEnable = true;
  buttonOwnerRegisterLoading = false;
  ownerLogin = false;

  ngOnInit() {
    this.isOwnerConfigured();
    this.getInfoApresentation();
  }

  isOwnerConfigured() {
    this.buttonOwnerRegisterEnable = false;
    this.buttonOwnerRegisterLoading = true;
    this.configsService.isConfigured().pipe(first()).subscribe(configured => {
      let config = _.find(configured.data, ['key', 'OWNERS'])
      this.buttonOwnerRegisterEnable = config.configured;
      if (config.configured) {
        this.configsService.ownerProfile().pipe(first()).subscribe(ownerProfile => {
          this.isOwnerLogged(ownerProfile.data[0]);
          this.buttonOwnerRegisterLoading = false;
        });
      }
    }, () => {
      this.buttonOwnerRegisterLoading = false;
    })

  }

  getInfoApresentation() {
    this.configurationsService.listTextApresentations(15).pipe(first()).subscribe((data) => {
      this.info = data.data;
    })
  }

  getHTML(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  isOwnerLogged(ownerProfileId: number) {
    if (this.authService.getUser() !== undefined && this.authService.getUser() != null && this.authService.getUser().profile_id === ownerProfileId) {
      this.ownerLogin = true;
      this.buttonOwnerRegisterEnable = false;
    } else {
      this.buttonOwnerRegisterEnable = true;
    }
  }

  formOwner() {
    this.router.navigateByUrl('privateaccommodation/form-owner');
  }
}


