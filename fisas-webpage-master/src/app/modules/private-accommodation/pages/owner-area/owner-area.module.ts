import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { OwnerAreaComponent } from './owner-area.component';
import { OwnerAreaRoutingModule } from './owner-area-routing.module';

@NgModule({
  declarations: [
        OwnerAreaComponent, 
    ],
  imports: [
    CommonModule,
    SharedModule,
    OwnerAreaRoutingModule,
  ]
})
export class OwnerAreaModule { }
