import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerPropertiesComponent } from './owner-properties.component';

const routes: Routes = [
  {
    path: '',
    component: OwnerPropertiesComponent,
    data: { breadcrumb: null, title: null, scope:'sasocial:is_private_accommodation_owner' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OwnerPropertiesRoutingModule {}
