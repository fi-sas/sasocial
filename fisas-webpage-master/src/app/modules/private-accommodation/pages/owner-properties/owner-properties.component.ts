import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ListingsModel } from '../../models';
import { PropertiesService } from '../../services/properties.service';
import { Resource } from '@fi-sas/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-owner-properties',
  templateUrl: './owner-properties.component.html',
  styleUrls: ['./owner-properties.component.less'],
})
export class OwnerPropertiesComponent {
  listProperties: ListingsModel[] = [];
  loadingResidences = false;

  pageIndex = 1;
  pageSize = 12;
  totalProperties = 0;
  totalPages = 0;

  constructor(private propertiesService: PropertiesService, private route: Router, private messageService:NzMessageService,
    private translateService: TranslateService, private modalService: NzModalService) {
    this.getProperties();
  }

  changePageIndex(pageIndex: number): void {
    this.pageIndex = pageIndex;
    this.getProperties();
  }

  checkTagStatusColor(status) {
    const css = {};
    css[status] = true;
    return css;
  }

  private async getProperties() {
    this.loadingResidences = true;
    let result: Resource<ListingsModel>;
    try {
      result = await this.propertiesService.listOwner(this.pageIndex, this.pageSize).toPromise();
    } catch (_) {
      return;
    } finally {
      this.loadingResidences = false;
    }
    this.listProperties = result.data;
  }

  delete(id) {
    this.propertiesService
      .delete(id)
      .subscribe(() => {
        (this.listProperties = this.listProperties.filter((v) => v.id !== id));
        this.messageService.success(this.translateService.instant('PRIVATEACCOMMODATION.DELETE_PROP'));
      });
  }


  confirm_delete(id){
    this.modalService.confirm({
      nzTitle: this.translateService.instant('PRIVATEACCOMMODATION.CONFIRM_DELETE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.delete(id)
    });
  }

  edit(id: any) {
    this.route.navigate(['privateaccommodation/form-property'], { queryParams: { property_id: id } });
  }

  detail() {
    localStorage.setItem('detail-owner-property', 'true');
  }
}
