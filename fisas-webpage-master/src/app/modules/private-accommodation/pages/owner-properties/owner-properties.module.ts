import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { OwnerPropertiesComponent } from './owner-properties.component';
import { OwnerPropertiesRoutingModule } from './owner-properties-routing.module';

@NgModule({
  declarations: [OwnerPropertiesComponent],
  imports: [CommonModule, SharedModule, OwnerPropertiesRoutingModule],
})
export class OwnerPropertiesModule {}
