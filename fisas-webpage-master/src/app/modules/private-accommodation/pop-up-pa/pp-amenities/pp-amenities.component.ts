import { Component, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TranslatePipe } from '@ngx-translate/core';

interface IOption {
  checked: boolean;
  label: string;
  value: string;
}

@Component({
  selector: 'app-pp-amenities',
  templateUrl: './pp-amenities.component.html',
  styleUrls: ['./pp-amenities.component.less'],
  providers: [TranslatePipe],
})
export class PpAmenitiesComponent {
  options: IOption[] = [
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN1'),
      value: 'dishwasher',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN2'),
      value: 'private_wc',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN3'),
      value: 'parking_garage',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN4'),
      value: 'elevator',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN5'),
      value: 'internet',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN6'),
      value: 'kitchen_access',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN7'),
      value: 'fridge',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN8'),
      value: 'stove',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN9'),
      value: 'microwave',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN10'),
      value: 'washing_machine',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN11'),
      value: 'heating_system',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN12'),
      value: 'cooling_system',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN13'),
      value: 'cable_tv',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_AMENITIES.AMEN14'),
      value: 'tv',
      checked: false,
    },
  ];

  @Output() amenities = new EventEmitter<string[]>();

  constructor(private activatedRoute: ActivatedRoute, private translate: TranslatePipe) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.options.map((option) => {
        option.checked = !!(params['amenities'] || '').split(',').find((a: string) => a === option.value);
        return option;
      });
    });
  }

  confirm() {
    this.amenities.emit(this.options.filter((d) => d.checked).map((d) => d.value));
  }
}
