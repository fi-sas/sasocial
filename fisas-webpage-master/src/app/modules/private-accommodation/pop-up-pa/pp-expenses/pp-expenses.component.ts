import { ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Output } from '@angular/core';

import { TranslatePipe } from '@ngx-translate/core';

interface IOption {
  checked: boolean;
  label: string;
  value: string;
}

@Component({
  selector: 'app-pp-expenses',
  templateUrl: './pp-expenses.component.html',
  styleUrls: ['./pp-expenses.component.less'],
  providers: [TranslatePipe],
})
export class PpExpensesComponent {
  options: IOption[] = [
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP1'),
      value: 'electricity',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP2'),
      value: 'internet',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP3'),
      value: 'cable_tv',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP4'),
      value: 'furniture',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP5'),
      value: 'cleaning',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP6'),
      value: 'bedding',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP7'),
      value: 'gas',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP8'),
      value: 'water',
      checked: false,
    },
    {
      label: this.translate.transform('PRIVATEACCOMMODATION.POP_EXPENSES.EXP9'),
      value: 'condo',
      checked: false,
    },
  ];

  @Output() expenses = new EventEmitter<string[]>();

  constructor(private activatedRoute: ActivatedRoute, private translate: TranslatePipe) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.options.forEach((option) => {
        option.checked = !!(params['expenses'] || '').split(',').find((a: string) => a === option.value);
        return option;
      });
    });
  }

  confirm() {
    this.expenses.emit(this.options.filter((b) => b.checked).map((b) => b.value));
  }
}
