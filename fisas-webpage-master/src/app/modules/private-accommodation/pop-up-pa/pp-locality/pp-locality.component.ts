import { ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Output } from '@angular/core';

import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { LocationsModel } from '../../models/location.model';
import { PropertiesService } from '../../services/properties.service';

@Component({
  selector: 'app-pp-locality',
  templateUrl: './pp-locality.component.html',
  styleUrls: ['./pp-locality.component.less'],
})
export class PpLocalityComponent {
  @Output() locality = new EventEmitter<string[]>();

  private destroy$: Subject<void> = new Subject<void>();

  locations: LocationsModel[] = [];

  constructor(private activatedRoute: ActivatedRoute, private propertiesService: PropertiesService) {
    this.getLocations();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  confirm() {
    this.locality.emit(this.locations.filter((l) => l.checked).map((l) => l.value));
  }

  getLocations() {
    this.propertiesService
      .getLocations()
      .pipe(first())
      .subscribe((locations) => {
        locations.data.forEach((location) => this.locations.push({ checked: false, label: location, value: location }));

        this.activatedRoute.queryParams.pipe(takeUntil(this.destroy$)).subscribe((params) => {
          this.locations.map((location) => {
            location.checked = !!(params['city'] || '').split(',').find((c) => c === location.value);
            return location;
          });
        });
      });
  }
}
