import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

import { IListingQueryParams, IMinMaxPrice } from '../../models';

@Component({
  selector: 'app-pp-price',
  templateUrl: './pp-price.component.html',
  styleUrls: ['./pp-price.component.less'],
})
export class PpPriceComponent {
  rangeValue: [number, number] = [0, 0];
  _minMaxPrice: IMinMaxPrice = { min: 0, max: 0 };

  @Output() price = new EventEmitter<IMinMaxPrice>();

  @Input() set minMaxPrice(minMaxPrice: IMinMaxPrice) {
    this._minMaxPrice = minMaxPrice;
    this.rangeValue = [minMaxPrice.min, minMaxPrice.max];
    this.xAxes.ticks = this._minMaxPrice;
    this.lineChartOptions.scales.xAxes = [this.xAxes];
  }

  @Input() set dataMap(map: { [price: number]: number }) {
    this.lineChartData[0].data = [];
    this.lineChartLabels = [];
    if (map) {
      const keys = Object.keys(map).map((k) => k + '€');
      const values = Object.values(map);

      this.lineChartData[0].data = values;
      this.lineChartLabels = keys;
    }
  }

  lineChartData: ChartDataSets[] = [{ data: [], lineTension: 0 }];
  lineChartLabels: Label[] = [];
  lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(51, 148, 112, 0.4)',
      pointRadius: 10,
      pointHoverRadius: 2,
      pointBackgroundColor: 'rgba(0,33,79,1)',
      pointBorderWidth: 2,
      pointBorderColor: 'rgba(255,255,255,1)',
    },
  ];

  xAxes = {
    barThickness: 5,
    ticks: {
      min: 0,
      max: 500,
    },
    scaleShowLabels: false,
    display: false,
    gridLines: {
      color: 'rgba(0, 0, 0, 0)',
    },
  };

  yAxes = {
    ticks: {
      min: 0,
      max: 20,
    },
    display: false,
    gridLines: {
      color: 'rgba(0, 0, 0, 0)',
    },
  };

  lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        backgroundColor: 'rgba(0,33,79,1)',
        color: 'white',
        borderRadius: 4,
        font: {
          weight: 'bold',
        },
      },
    },
    scales: {
      yAxes: [this.yAxes],
      xAxes: [this.xAxes],
    },
  };
  public lineChartLegend = false;
  public lineChartType = 'bar';
  public lineChartPlugins = [];

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe((params: IListingQueryParams) => {
      this.rangeValue = [this._minMaxPrice.min, this._minMaxPrice.max];
      if (params.minPrice !== null && !isNaN(parseInt(params.minPrice, 10))) {
        this.onChangeBox0(parseInt(params.minPrice, 10));
      }
      if (params.maxPrice !== null && !isNaN(parseInt(params.maxPrice, 10))) {
        this.onChangeBox1(parseInt(params.maxPrice, 10));
      }
    });
  }

  onChangeBox0(value: number) {
    this.rangeValue = [value, this.rangeValue[1]];
  }

  onChangeBox1(value: number) {
    this.rangeValue = [this.rangeValue[0], value];
  }

  confirm() {
    this.price.emit({ min: this.rangeValue[0], max: this.rangeValue[1] });
  }
}
