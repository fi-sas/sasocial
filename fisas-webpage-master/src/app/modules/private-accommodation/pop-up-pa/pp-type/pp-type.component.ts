import { ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IListingQueryParams, ITypology } from '../../models';

@Component({
  selector: 'app-pp-type',
  templateUrl: './pp-type.component.html',
  styleUrls: ['./pp-type.component.less'],
})
export class PpTypeComponent {
  @Input() typologies: ITypology[] = [];

  @Output() typology = new EventEmitter<number>();

  typologyId: number = -1;

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe((params: IListingQueryParams) => {
      this.typologyId = -1;
      if (params.typology !== null && !isNaN(parseInt(params.typology, 10))) {
        this.typologyId = parseInt(params.typology, 10);
      }
    });
  }

  confirm() {
    this.typology.emit(this.typologyId);
  }
}
