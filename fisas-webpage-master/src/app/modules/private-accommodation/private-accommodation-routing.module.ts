import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateAccommodationComponent } from './private-accommodation.component';

const routes: Routes = [
    {
        path: '',
        component: PrivateAccommodationComponent,
        children: [
            { path: '', redirectTo: 'list-host', pathMatch: 'full' },
            {
                path: 'list-host',
                loadChildren: './pages/list-host.module#ListHostModule',
                data: { scope: 'private_accommodation:listings:read', breadcrumb: 'PRIVATEACCOMMODATION.LIST_HOST.HOST' }
            },
            {
                path: 'owner-area',
                loadChildren: './pages/owner-area/owner-area.module#OwnerAreaModule',
                data: { scope: 'private_accommodation:owners', breadcrumb: 'PRIVATEACCOMMODATION.OWNER_AREA.OWNER' }
            },
            {
                path: 'list-detail/:id',
                loadChildren: './pages/list-detail/list-detail.module#ListDetailModule',
                data: { scope: 'private_accommodation:listings:read', breadcrumb: 'PRIVATEACCOMMODATION.LIST_DETAIL.MAIN' }
            },
            {
                path: 'contact-owner/:id',
                loadChildren: './pages/contact-owner/contact-owner.module#ContactOwnerModule',
                data: { scope: 'private_accommodation:listings:read', breadcrumb: 'PRIVATEACCOMMODATION.CONTACT_OWNER.MAIN' }
            },
            {
                path: 'complaints/:id',
                loadChildren: 'src/app/modules/private-accommodation/pages/complaints/complaints.module#ComplaintsModule',
                data: { scope: 'private_accommodation:complaints:create', breadcrumb: 'PRIVATEACCOMMODATION.COMPLAINTS.MAIN' }
            },
            {
                path: 'owner-properties',
                loadChildren: './pages/owner-properties/owner-properties.module#OwnerPropertiesModule',
                data: { scope: 'private_accommodation:listings:read', breadcrumb: 'PRIVATEACCOMMODATION.OWNER_PROPERTIES.MAIN' }
            },
            {
                path: 'form-owner',
                loadChildren: './pages/form-owner/form-owner.module#FormOwnerModule',
                data: { scope: 'private_accommodation:owners:create', breadcrumb: 'PRIVATEACCOMMODATION.FORM_OWNER.MAIN' }
            },
            {
                path: 'form-property',
                loadChildren: './pages/form-property/form-property.module#FormPropertyModule',
                data: { scope: 'private_accommodation:listings:create', breadcrumb: 'PRIVATEACCOMMODATION.FORM_PROPERTY.MAIN' }
            },
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PrivateAccommodationRoutingModule { }
