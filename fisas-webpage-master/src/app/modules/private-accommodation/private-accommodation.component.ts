import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-private-accommodation',
    template: '<router-outlet></router-outlet>'
})
export class PrivateAccommodationComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }
}
