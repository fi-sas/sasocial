
import { PrivateAccommodationComponent } from './private-accommodation.component';
import { NgModule } from '@angular/core';
import { PrivateAccommodationRoutingModule } from './private-accommodation-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';


@NgModule({
    declarations: [
        PrivateAccommodationComponent
    ],
    imports: [
        PrivateAccommodationRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,

    ]
})

export class PrivateAccommodationModule { }
