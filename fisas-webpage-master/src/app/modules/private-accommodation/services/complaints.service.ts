import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { ComplaintsModel } from '../models/Complaints.model';


@Injectable({
    providedIn: 'root'
})
export class ComplaintsService {

    constructor(
        private resourceService: FiResourceService,
        private urlService: FiUrlService
    ) { }

    list() {
        let httpParams = new HttpParams();
        httpParams = httpParams.append('offset', '0');
        httpParams = httpParams.append('limit', '10');

        return this.resourceService.list<ComplaintsModel>(
            this.urlService.get('PRIVATE_ACCOMMODATION.COMPLAINTS', {}),
            { params: httpParams }
        );
    }

    save(model: ComplaintsModel) {
        return this.resourceService.create<ComplaintsModel>(
            this.urlService.get('PRIVATE_ACCOMMODATION.COMPLAINTS', {}),
            model
        );
    }
}
