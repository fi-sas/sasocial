import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { IsConfiguredModel } from '@fi-sas/webpage/modules/private-accommodation/models/IsConfigured.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigsService {

  constructor( private resourceService: FiResourceService,
               private urlService: FiUrlService) { }

  maxMedias(): Observable<Resource<number>> {
    return this.resourceService.read<number>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIG_MAX_MEDIAS', {}), {});
  }

  maxPublishDay(): Observable<Resource<number>> {
    return this.resourceService.read<number>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIG_MAX_PUBLISH_DAYS', {}), {});
  }

  ownerProfile(): Observable<Resource<number>> {
    return this.resourceService.read<number>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIG_OWNER_PROFILE', {}), {});
  }

  ownerRegulation(): Observable<Resource<any>> {
    return this.resourceService.read<any>(this.urlService.get('PRIVATE_ACCOMMODATION.CONFIG_OWNER_REGULATION', {}), {});
  }

  isConfigured() {
    return this.resourceService.list<IsConfiguredModel>(
        this.urlService.get('PRIVATE_ACCOMMODATION.CONFIG_IS_CONFIGURED', {}),
        {}
    );
}
}
