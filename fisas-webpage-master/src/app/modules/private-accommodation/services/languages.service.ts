import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { LanguageModel } from '../models/language.model';


@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  listLanguages(): Observable<Resource<LanguageModel>> {
    return this.resourceService.list<LanguageModel>(this.urlService.get('CONFIGURATION.LANGUAGES', { }), {});
  }
}
