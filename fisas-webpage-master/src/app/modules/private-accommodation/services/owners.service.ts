import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { HttpParams } from '@angular/common/http';
import { OwnerApplicationsModel } from '../models/OwnerApplication.model';

@Injectable({
    providedIn: 'root'
})
export class OwnersService {

    constructor(
        private resourceService: FiResourceService,
        private urlService: FiUrlService
    ) { }

    list() {
        let httpParams = new HttpParams();
        httpParams = httpParams.append('offset', '0');
        httpParams = httpParams.append('limit', '-1');

        return this.resourceService.list<OwnerApplicationsModel>(
            this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS', {}),
            { params: httpParams }
        );
    }

    get(id) {
        return this.resourceService.list<OwnerApplicationsModel>(
            this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS', {}) + '/' + id,
            {}
        );
    }


    save(model: OwnerApplicationsModel) {
        return this.resourceService.create<OwnerApplicationsModel>(
            this.urlService.get('PRIVATE_ACCOMMODATION.OWNERS_APPLICATIONS', {}),
            model
        );
    }
}
