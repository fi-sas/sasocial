import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';

import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ListingsModel, PropertiesPriceInterval } from '../models';

@Injectable({
  providedIn: 'root',
})
export class PropertiesService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  listOwner(pageIndex: number, pageSize: number): Observable<Resource<ListingsModel>> {
    return this.listSearchOwner(pageIndex, pageSize, new HttpParams());
  }

  list(pageIndex: number, pageSize: number): Observable<Resource<ListingsModel>> {
    return this.listSearch(pageIndex, pageSize, new HttpParams());
  }

  listSearch(
    pageIndex: number,
    pageSize: number,
    params: HttpParams
  ): Observable<Resource<ListingsModel>> {
    const dateNow = moment(new Date()).format("YYYY-MM-DD");
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.append('withRelated', 'owner,medias,amenities,expenses,translations,rooms');
    params = params.append('query[start_date][lte]', dateNow);
    params = params.append('query[end_date][gte]', dateNow);
    return this.resourceService.list<ListingsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS'), { params });
  }

  listSearchOwner(
    pageIndex: number,
    pageSize: number,
    params: HttpParams,
    endpoint: string = 'PRIVATE_ACCOMMODATION.LISTINGS_SEARCH'
  ): Observable<Resource<ListingsModel>> {
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.append('withRelated', 'owner,medias,amenities,expenses,translations,rooms');
    return this.resourceService.list<ListingsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_OWNER'), { params });
  }

  get(id) {
    return this.resourceService.read<ListingsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_ID', { id }), {
      params: { withRelated: 'owner,medias,amenities,expenses,translations,rooms' },
    });
  }

  delete(id) {
    return this.resourceService.delete(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_ID', { id }));
  }

  save(model: ListingsModel) {
    return this.resourceService.create<ListingsModel>(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS'), model);
  }

  update(id: number, model: ListingsModel) {
    return this.resourceService.update<ListingsModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_ID', { id }),
      model
    );
  }

  visualizations(id: number) {
    return this.resourceService.create<ListingsModel>(
      this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_INCREMENT', { id }),
      {}
    );
  }

  getLocations() {
    return this.resourceService.list<any>(this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_LOCATION'));
  }

  getPropertiesPriceInterval() {
    return this.resourceService.read<PropertiesPriceInterval>(
      this.urlService.get('PRIVATE_ACCOMMODATION.LISTINGS_PRICE_INTERVAL')
    );
  }

  checkProperties(){
    return this.resourceService.read<boolean>(
      this.urlService.get('PRIVATE_ACCOMMODATION.CHECK_PROPERTIES')
    );
  }
}
