import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ITypology } from '../models';

@Injectable({
  providedIn: 'root',
})
export class TypologiespaService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  list(): Observable<Resource<ITypology>> {
    return this.resourceService.list<ITypology>(this.urlService.get('PRIVATE_ACCOMMODATION.TYPOLOGIES'));
  }
}
