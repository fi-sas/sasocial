export class dayAppointmentResponseModel {
    appointments: boolean;
    hours: [];
}

export class AppointmentModel {
    appointment_schedule: Date;
    subject_id: number;
    appointment_reason: string;
}