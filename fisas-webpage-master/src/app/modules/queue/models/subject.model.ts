import { ScheduleModelQueue } from "./schedule.model";
import { ServiceModelQueue } from "./service.model";

export class SubjectModel {
    id?: number;
    active: boolean;
    updated_at?: Date;
    created_at?: Date;
    code: string;
    avg_time: number;
    max_tolerance: number;
    appointments: boolean;
    appointments_interval: number;
    estimated_time: number;
    service_id?: number;
    schedule: ScheduleModelQueue[]=[];
    n_tickets: number;
    out_of_goal: number;
    last_desk: string;
    ongoing_ticket: string;
    waiting_time: number;
    tickets_in_queue: number;
    service: ServiceModelQueue[]=[];
    translations: TranslationsModel[] = [];
    is_open: boolean;
    user_open_tickets: boolean;
}

export class TranslationsModel {
    id: number;
    name: string;
    subject_id: number;
    language_id: number;
    updated_at?: Date;
    created_at?: Date;
    schedule_description: string;
}
