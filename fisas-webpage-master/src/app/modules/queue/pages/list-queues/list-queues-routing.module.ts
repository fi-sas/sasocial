import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListQueuesComponent } from './list-queues.component';



const routes: Routes = [
  {
    path: '',
    component: ListQueuesComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListQueuesRoutingModule { }
