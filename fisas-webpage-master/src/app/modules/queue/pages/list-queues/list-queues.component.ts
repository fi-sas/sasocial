import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { finalize, first } from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AppointmentModel } from "../../models/appointment-subject.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { issueTicketModel } from "../../models/issue-ticket.model";
import { QueueService } from "../../services/queue.service";
import { SERVICE_IDS } from "@fi-sas/webpage/shared/models";
import { ServiceConfigurationModel } from "@fi-sas/webpage/shared/models/service-configurations.model";
import { ServiceModelQueue } from "../../models/service.model";
import { SubjectModel } from "../../models/subject.model";
import { TicketsModel } from "../../models/tickets.model";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";


@Component({
  selector: 'app-list-queues-queue',
  templateUrl: './list-queues.component.html',
  styleUrls: ['./list-queues.component.less'],
})
export class ListQueuesComponent implements OnInit {
  appointment = false;
  loadingCreateAppointment = false;
  loading = true;
  services: ServiceModelQueue[] = [];
  detailServiceSelected: ServiceModelQueue = new ServiceModelQueue();
  itemSubjectSelected: SubjectModel = new SubjectModel();
  modalBooking = false;
  modalSuccess = false;
  modalSuccessAppointment = false;
  responseDataTicket: TicketsModel = new TicketsModel();
  dayCurrent: Date;
  formAppointment: FormGroup;
  submitted = false;
  subjects: SubjectModel[];
  hoursAvailable = [];
  translations: TranslationModel[] = [];
  buttonSubmit = {
    color: '#ffffff',
    width: '250px',
    height: '48px',
    'text-align': 'center',
    'font-size': '17px',
    'font-weight': '500',
    'line-height': '1.41'
  };
  loadingAppointmentHours = true;
  hourSelected: Date;

  fifteenDays = [];
  imgIP: string;

  readonly serviceId = SERVICE_IDS.QUEUE;

  constructor(private queueService: QueueService, private formBuilder: FormBuilder, public translate: TranslateService,
    private configurationsService: ConfigurationsService) {

  }

  ngOnInit() {
    this.getTranslations();
    this.getServices();

  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(21);
    this.translations = service.translations;
  }

  getServices() {
    this.queueService.listServices().pipe(first(), finalize(() => (this.loading = false))).subscribe((data) => {
      this.services = data.data;
    })
  }

  transformSecontToHour(value: number) {
    let h = Math.floor(value / 3600);
    let m = Math.floor(value % 3600 / 60);
    let hDisplay;
    let mDisplay;
    if (h == 0) {
      hDisplay = '00';
    } else if (h < 10) {
      hDisplay = '0' + h;
    } else {
      hDisplay = h;
    }
    if (m == 0) {
      mDisplay = '00';
    } else if (m < 10) {
      mDisplay = '0' + m;
    } else {
      mDisplay = m;
    }
    return hDisplay + 'h' + mDisplay + 'm';
  }

  openModelDetail(service: ServiceModelQueue, subject: SubjectModel) {
    this.modalBooking = true;
    this.dayCurrent = new Date();
    this.detailServiceSelected = service;
    this.itemSubjectSelected = subject;
  }

  closeModal() {
    this.modalSuccess = false;
    this.appointment = false;
    this.getServices();
  }

  issueTicket() {
    this.modalBooking = false;
    let sendValues: issueTicketModel = new issueTicketModel();
    sendValues.priority = false;
    sendValues.subject_id = this.itemSubjectSelected.id;
    this.dayCurrent = new Date();
    this.queueService.issueTicket(sendValues).pipe(first()).subscribe((data) => {
      this.responseDataTicket = data.data[0].ticket;
      this.modalSuccess = true;
    })
  }

  isAppointment() {
    this.appointment = true;
    this.modalBooking = false;
    this.formAppointment = this.formBuilder.group({
      service: [this.detailServiceSelected.id],
      subject: [this.itemSubjectSelected.id],
      date: [null],
      appointment_reason: [''],
    })
    this.subjects = this.detailServiceSelected.subjects;
    this.setDays();
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    const maxDate = moment().add(15, 'day');

    while (currentDate.isBefore(maxDate)) {
      this.fifteenDays.push(indexDate.toDate());
      indexDate = indexDate.add(1, 'day');
    }
    this.daySelected(new Date());
  }

  daySelected(day) {
    this.queueService.getAppointmentSubject(day.getTime(), this.itemSubjectSelected.id).pipe(first(),
      finalize(() => this.loadingAppointmentHours = false)).subscribe((data) => {
        this.hoursAvailable = data.data[0].hours;
        this.hourSelected = null;
      })
  }

  hourSelect(hour) {
    this.hourSelected = hour;
  }

  submitAppointment() {
    this.submitted = true;
    if (this.hourSelected != null && this.hourSelected != undefined) {
      let sendValues: AppointmentModel = new AppointmentModel();
      sendValues.appointment_schedule = this.hourSelected;
      sendValues.subject_id = this.itemSubjectSelected.id;
      sendValues.appointment_reason = this.formAppointment.get('appointment_reason').value;
      this.queueService.postAppointment(sendValues).pipe(first(),
        finalize(() => this.loadingCreateAppointment = false)).subscribe((data) => {
          this.responseDataTicket = data.data[0];
          this.submitted = false;
          this.modalSuccess = true;
        })
    }

  }
}
