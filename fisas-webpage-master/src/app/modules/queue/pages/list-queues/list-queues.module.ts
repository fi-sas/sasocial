import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ListQueuesRoutingModule } from './list-queues-routing.module';
import { ListQueuesComponent } from './list-queues.component';




@NgModule({
    declarations: [
        ListQueuesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ListQueuesRoutingModule
    ]
})
export class ListQueuesModule { }
