import { Component, OnInit } from "@angular/core";
import { finalize, first } from "rxjs/operators";
import { QueueService } from "../../services/queue.service";
import * as moment from 'moment';
import { MessageType, UiService } from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { NzModalService } from "ng-zorro-antd";
import * as  humanizeDuration from "humanize-duration";
import { TicketsModel } from "../../models/tickets.model";

@Component({
    selector: 'app-my-tickets-queue',
    templateUrl: './my-tickets.component.html',
    styleUrls: ['./my-tickets.component.less'],
})
export class MyTicketsComponent implements OnInit {
    loading = true;
    loadingHistoryTickets = true;
    loadingBookings = true;
    loadingHistoryBookings = true;
    current = new Date();

    //tables
    pageIndexTableTickets = 1;
    pageSizeTableTickets = 5;
    totalTableTickets = 0;
    pageIndexTableBookings = 1;
    pageSizeTableBookings = 5;
    totalTableBookings = 0;
    pageIndexTableHistoryTickets = 1;
    pageSizeTableHistoryTickets = 5;
    totalTableHistoryTickets = 0;
    pageIndexTableHistoryBookings = 1;
    pageSizeTableHistoryBookings = 5;
    totalTableHistoryBookings = 0;
    pagination = true;
    sizeChanger = false;
    position = 'bottom';

    listTicketsData: TicketsModel[] = [];
    listBookingsData: TicketsModel[] = [];
    listHistoryTicketsData: TicketsModel[] = [];
    listHistoryBookingsData: TicketsModel[] = [];

    panelTicket = [
        {
            active: false,
            disabled: false,
            name: 'QUEUE.MY_TICKETS.HISTORY_TICKET_TITLE',
            customStyle: {
                background: '#f0f2f5',
                padding: '0px',
                border: '0px',
                margin: '0px'
            }
        }

    ];

    panelMarking = [
        {
            active: false,
            disabled: false,
            name: 'QUEUE.MY_TICKETS.HISTORY_MARKINGS_TITLE',
            customStyle: {
                background: '#f0f2f5',
                padding: '0px',
                border: '0px',
                margin: '0px',
            }
        }
    ];

    constructor(private queueService: QueueService, private uiService: UiService, private translateService: TranslateService,
        private modalService: NzModalService) { }

    ngOnInit() {
        this.getTickets();
        this.getBookings();
        this.getHistoryTickets();
        this.getHistoryBookings();
    }

    getTickets() {
        this.queueService.listTickets(this.pageIndexTableTickets, this.pageSizeTableTickets).pipe(first(), finalize(() => (this.loading = false))).subscribe((data) => {
            this.listTicketsData = data.data;
            this.totalTableTickets = data.link.total;
        })
    }

    getBookings() {
        this.queueService.listBookings(this.pageIndexTableBookings, this.pageSizeTableBookings).pipe(first(), finalize(() => (this.loadingBookings = false))).subscribe((data) => {
            this.listBookingsData = data.data;
            this.totalTableBookings = data.link.total;
        })
    }

    getHistoryTickets() {
        this.queueService.listHistoryTickets(this.pageIndexTableHistoryTickets, this.pageSizeTableHistoryTickets).pipe(first(), finalize(() => (this.loadingHistoryTickets = false))).subscribe((data) => {
            this.listHistoryTicketsData = data.data;
            this.totalTableHistoryTickets = data.link.total;
        })
    }

    getHistoryBookings() {
        this.queueService.listHistoryBookings(this.pageIndexTableHistoryBookings, this.pageSizeTableHistoryBookings).pipe(first(), finalize(() => (this.loadingHistoryBookings = false))).subscribe((data) => {
            this.listHistoryBookingsData = data.data;
            this.totalTableHistoryBookings = data.link.total;
        })
    }

    paginationSearchTableTickets(reset: boolean = false): void {
        if (reset) {
            this.pageIndexTableTickets = 1;
        }
        this.getTickets();
    }

    paginationSearchTableBookings(reset: boolean = false): void {
        if (reset) {
            this.pageIndexTableBookings = 1;
        }
        this.getBookings();
    }

    paginationSearchTableHistoryTickets(reset: boolean = false): void {
        if (reset) {
            this.pageIndexTableHistoryTickets = 1;
        }
        this.getHistoryTickets();
    }

    paginationSearchTableHistoryBookings(reset: boolean = false): void {
        if (reset) {
            this.pageIndexTableHistoryBookings = 1;
        }
        this.getHistoryBookings();
    }

    transformSecontToHour(value: number) {
        let h = Math.floor(value / 3600);
        let m = Math.floor(value % 3600 / 60);
        let hDisplay;
        let mDisplay;
        if (h == 0) {
            hDisplay = '00';
        } else if (h < 10) {
            hDisplay = '0' + h;
        } else {
            hDisplay = h;
        }
        if (m == 0) {
            mDisplay = '00';
        } else if (m < 10) {
            mDisplay = '0' + m;
        } else {
            mDisplay = m;
        }
        return hDisplay + 'h' + mDisplay + 'm';
    }


    convertTime(date: Date) {
        let diff = moment(date).diff(this.current);
        if (diff <= 0) {
            return humanizeDuration(0, { language: this.translateService.currentLang, round: true });
        } else {
            return humanizeDuration(diff, { language: this.translateService.currentLang, round: true });
        }

    }

    deleteTicket(idTicket: number) {
        this.modalService.confirm({
            nzTitle: this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.DELETETICKET'),
            nzOkText: this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.DELETE_OK'),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOnOk: () => this.deleteTicketConfirm(idTicket, 'T')
        });

    }

    deleteTicketConfirmM(idTicket: number) {
        this.modalService.confirm({
            nzTitle: this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.DELETEBOOKINGS'),
            nzOkText: this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.BOOKING_OK'),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOnOk: () => this.deleteTicketConfirm(idTicket, 'M')
        });

    }

    deleteTicketConfirm(idTicket: number, type: string) {
        this.queueService.deleteTicket(idTicket).pipe(first()).subscribe((data) => {
            if(type == 'T') {
                this.uiService.showMessage(
                    MessageType.success,
                    this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.DELETETICKETCONFIRM')
                );
                this.getTickets();
            }else {
                this.uiService.showMessage(
                    MessageType.success,
                    this.translateService.instant('QUEUE.MY_TICKETS.TABLE_TICKETS.DELETETICKETCONFIRM')
                );
                this.getBookings();
            }
            
           
        })
    }

    convertStatus(status: string) {
        if (status == "CANCELADO") {
            return this.translateService.instant('QUEUE.MY_TICKETS.TABLE_HISTORY_TICKETS.STATUS_CANCEL');
        } else if (status == "NAO_COMPARECEU") {
            return this.translateService.instant('QUEUE.MY_TICKETS.TABLE_HISTORY_TICKETS.STATUS_NOT_ATTEND');
        } else if (status == "TRANSFERIDO"){
            return this.translateService.instant('QUEUE.MY_TICKETS.TABLE_HISTORY_TICKETS.STATUS_TRANSFER');
        }
    }

    convertDuration(startDate: Date, endDate: Date) {
        let diferenceInMs = new Date(endDate).getTime() - new Date(startDate).getTime();
        let seconds = diferenceInMs / 1000;
        const hours = seconds / 3600; 
        seconds = seconds % 3600;
        const minutes = seconds / 60;
        let end = (hours.toString()).split('.')[0] + 'h' + (minutes.toString()).split('.')[0] + 'm';
        return end;
    }
}