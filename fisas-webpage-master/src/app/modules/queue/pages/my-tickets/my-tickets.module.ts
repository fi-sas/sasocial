import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { MyTicketsComponent } from './my-tickets.component';
import { MyTicketsRoutingModule } from './my-tickets-routing.module';



@NgModule({
    declarations: [
        MyTicketsComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MyTicketsRoutingModule
    ]
})
export class MyTicketsModule { }
