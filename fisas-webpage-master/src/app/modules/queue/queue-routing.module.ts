import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QueueComponent } from './queue.component';

const routes: Routes = [
    {
        path: '',
        component: QueueComponent,
        children: [
            { path: '', redirectTo: 'list-queues', pathMatch: 'full' },
            {
                path: 'my-tickets',
                loadChildren: './pages/my-tickets/my-tickets.module#MyTicketsModule',
            },
            {
                path: 'list-queues',
                loadChildren: './pages/list-queues/list-queues.module#ListQueuesModule',
            },
        ],
        data: { breadcrumb: null, title: null }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class QueueRoutingModule { }
