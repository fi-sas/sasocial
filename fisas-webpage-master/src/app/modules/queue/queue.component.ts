
import { Component, OnInit } from '@angular/core';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';

@Component({
    selector: 'app-queue',
    templateUrl: './queue.component.html',
    styleUrls: ['./queue.component.less']
})
export class QueueComponent implements OnInit {
    data: any[];
    barOptions: any[] = [];

    constructor(
      private configurationsService: ConfigurationsService) { 
    }

    ngOnInit() {
      this.data = this.configurationsService.getLocalStorageTotalConfiguration();
      this.barOptions = [
        {
          translate: this.validTitleTraductions(21),
          disable: true,
          routerLink: '/queue/list-queues',
          type: 'array'
        },
        {
          translate: 'QUEUE.TITLE_MY_TICKET',
          disable: false,
          routerLink: '/queue/my-tickets',
          type: 'string',
        },
      ]
    }

    validTitleTraductions(id: number) {
      return this.data.find(t => t.id === id) != null && this.data.find(t => t.id === id) != undefined ? this.data.find(t => t.id === id).translations : '';
    }

}
