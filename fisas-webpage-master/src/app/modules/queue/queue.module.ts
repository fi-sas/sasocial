

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { QueueComponent } from './queue.component';
import { QueueRoutingModule } from './queue-routing.module';


@NgModule({
    declarations: [
        QueueComponent
    ],
    imports: [
        QueueRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NzCheckboxModule,

    ]
})

export class QueueModule { }
