
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { AppointmentModel, dayAppointmentResponseModel } from '../models/appointment-subject.model';
import { issueTicketModel } from '../models/issue-ticket.model';
import { ResponseTicketModel } from '../models/response-isseu-ticket.model';
import { ServiceModelQueue } from '../models/service.model';
import { SubjectModel } from '../models/subject.model';
import { TicketsModel } from '../models/tickets.model';


@Injectable({
  providedIn: 'root',
})
export class QueueService {

  loading = false;

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  listTickets(pageIndex: number, pageSize: number): Observable<Resource<TicketsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    return this.resourceService.list<TicketsModel>(this.urlService.get('QUEUE.TICKETS_DIGITAL'), {params});
  }

  listBookings(pageIndex: number, pageSize: number): Observable<Resource<TicketsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort', 'estimated_time');
    return this.resourceService.list<TicketsModel>(this.urlService.get('QUEUE.MARKING'), {params});
  }

  listHistoryTickets(pageIndex: number, pageSize: number): Observable<Resource<TicketsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort','-created_at');
    return this.resourceService.list<TicketsModel>(this.urlService.get('QUEUE.HISTORY_TICKETS_DIGITAL'), {params});
  }

  listHistoryBookings(pageIndex: number, pageSize: number): Observable<Resource<TicketsModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('sort', '-estimated_time');
    return this.resourceService.list<TicketsModel>(this.urlService.get('QUEUE.HISTORY_MARKING'), {params});
  }

  deleteTicket(id: number){
    return this.resourceService.delete(this.urlService.get('QUEUE.TICKET', {id}));
  }

  listSubjects(): Observable<Resource<SubjectModel>> {
    return this.resourceService.list<SubjectModel>(this.urlService.get('QUEUE.SUBJECTS'), {});
  }

  listServices(): Observable<Resource<ServiceModelQueue>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'subjects,translations');
    params = params.set('query[active]', 'true');
    return this.resourceService.list<ServiceModelQueue>(this.urlService.get('QUEUE.SERVICES'), {params});
  }

  issueTicket(sendValue: issueTicketModel){
    return this.resourceService.create<ResponseTicketModel>(
      this.urlService.get("QUEUE.ISSUETICKET"),
      sendValue
    );
  }

  getAppointmentSubject(day, id: number){
    let params = new HttpParams();
    params = params.set('day', day.toString());
    return this.resourceService.list<dayAppointmentResponseModel>(this.urlService.get('QUEUE.APPOINTMENTS_SUBJECT', {id}), {params});
  }

  postAppointment(data: AppointmentModel){
    return this.resourceService.create<TicketsModel>(
      this.urlService.get("QUEUE.APPOINTMENT"),
      data
    );
  }


}
