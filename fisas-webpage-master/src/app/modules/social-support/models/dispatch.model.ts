export type DISPATCH_DECISION = 'ACCEPT' | 'REJECT';

export const DISPATCH_ACTIONS = {
  ACCEPTED: 'SOCIAL_SUPPORT.DISPATCH.ACCEPT',
  REJECTED: 'SOCIAL_SUPPORT.DISPATCH.REFUSE',
};


interface ChangeToDispatch<T> {
  event: 'DISPATCH';
  decision: T;
  notes?: string;
}

interface ValidateDispatchDecision {
  decision_dispatch: DISPATCH_DECISION
}

export type SendDispatchData = ChangeToDispatch<string> | ValidateDispatchDecision;
