import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { ScheduleExperienceModel } from './schedule-experience.model';
import { HistoryModel } from './interest.model';

export class ExperienceModel {
  id: number;
  academic_year: string;
  title: string;
  proponent_service: string;
  organic_unit_id: number;
  address: string;
  number_candidates: number;
  number_simultaneous_candidates: number;
  applicant_profile: string;
  job: string;
  number_weekly_hours: number;
  total_hours_estimation: number;
  holydays_availability: boolean;
  payment_model: string;
  payment_value: number;
  perc_student_iban: number;
  perc_student_ca: number;
  current_account_id: number;
  experience_responsible_id: number;
  experience_advisor_id: number;
  description: string;
  start_date: string;
  end_date: string;
  publish_date: string;
  application_deadline_date: string;
  selection_criteria: string;
  attachment_file_id: number;
  contract_file_id: number;
  certificate_file_id: number;
  status: string;
  finances_email: string;
  organic_unit?: OrganicUnitModel;
  experience_responsible?: UserModel;
  experience_advisor?: UserModel;
  attachment_file?: FileModel;
  schedule?: ScheduleExperienceModel[];
  created_at: string;
  history: HistoryModel[];
  translations: [
    {
      language?: {
        id: number;
        name: string;
        acronym: string;
      };
      language_id: number;
      name: string;
      applicant_profile: string;
      description: string;
      job: string;
      proponent_service: string;
      selection_criteria: string;
      title: string;
    }
  ];
  updated_at: string;
  uuid?: string;
  number_interests: number;
}
