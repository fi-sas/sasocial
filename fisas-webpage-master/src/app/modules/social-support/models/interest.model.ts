import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { FileModel, ReportFileModel } from "../../media/models/file.model";
import { ExperienceModel } from "./experiences.model";

export class InterestModel {
  user: UserModel;
  user_id: number;
  uuid: string;
  updated_at: Date;
  student_file_avaliation: string;
  student_avaliation: string;
  status: string;
  id: number;
  report_avaliation: string;
  report_avaliation_file: FileModel;
  experience_id: number;
  experience: ExperienceModel;
  created_at: Date;
  history: HistoryModel[];
  interview: InterviewModel[];
  contract_file: FileModel;
  certificate_file?: FileModel;
  certificate_file_id?: number;
  certificate_generated?: ReportFileModel;
  certificate_generated_id?: number;
}

export class HistoryModel {
  created_at: Date;
  id: number;
  status: string;
}

export class InterviewModel {
  created_at: Date;
  date: Date;
  file_id: number;
  file: FileModel;
  id: number;
  local: string;
  notes: string;
  observations?: string;
  responsable_id: number;
  responsable: UserModel;
  updated_at: Date;
  user_interest_id: number;
}

export enum INTEREST_STATUS {
  // Aceite
  ACCEPTED = "ACCEPTED",
  // Em Análise
  ANALYSED = "ANALYSED",
  // Aprovada
  APPROVED = "APPROVED",
  // Cancelada
  CANCELLED = "CANCELLED",
  // FECHADA
  CLOSED = "CLOSED",
  // Em Colaboração
  COLABORATION = "COLABORATION",
  // Rejeitada
  DECLINED = "DECLINED",
  // Entrevista
  INTERVIEWED = "INTERVIEWED",
  // Não Selecionado
  NOT_SELECTED = "NOT_SELECTED",
  // Submetida
  SUBMITTED = "SUBMITTED",
  // Lista de Espera
  WAITING = "WAITING",
  // Em Desistência
  WITHDRAWAL = "WITHDRAWAL",
  // Desistência Aceite
  WITHDRAWAL_ACCEPTED = "WITHDRAWAL_ACCEPTED",
  // Desistência Aceite
  DISPATCH = "DISPATCH",
  DISPATCH_ACCEPT = "DISPATCH_ACCEPT",
  DISPATCH_REJECT = "DISPATCH_REJECT",
}

export enum INTEREST_STATUS_EVENT {
  ACCEPT = "ACCEPT",
  ANALYSE = "ANALYSE",
  APPROVE = "APPROVE",
  CANCEL = "CANCEL",
  CLOSE = "CLOSE",
  COLABORATION = "COLABORATION",
  DECLINE = "DECLINE",
  INTERVIEW = "INTERVIEW",
  NOTSELECT = "NOTSELECT",
  SUBMIT = "SUBMIT",
  WAITING = "WAITING",
  WITHDRAWAL = "WITHDRAWAL",
  WITHDRAWALACCEPTED = "WITHDRAWALACCEPTED",
  DISPATCH = "DISPATCH",
}

export const INTEREST_STATUS_STATE_MACHINE: {
  [status: string]: INTEREST_STATUS[];
} = {
  ACCEPTED: [INTEREST_STATUS.CANCELLED, INTEREST_STATUS.COLABORATION],
  ANALYSED: [
    INTEREST_STATUS.INTERVIEWED,
    INTEREST_STATUS.CANCELLED,
  ],
  APPROVED: [INTEREST_STATUS.CANCELLED, INTEREST_STATUS.ACCEPTED],
  CANCELLED: [],
  CLOSED: [],
  COLABORATION: [
    INTEREST_STATUS.CLOSED,
    INTEREST_STATUS.WITHDRAWAL,
  ],
  DECLINED: [],
  INTERVIEWED: [
    //INTEREST_STATUS.APPROVED,
    INTEREST_STATUS.CANCELLED,
    //INTEREST_STATUS.DECLINED,
    //INTEREST_STATUS.NOT_SELECTED,
    //INTEREST_STATUS.WAITING,
  ],
  NOT_SELECTED: [
    INTEREST_STATUS.ANALYSED,
    INTEREST_STATUS.CLOSED
  ],
  SUBMITTED: [INTEREST_STATUS.ANALYSED, INTEREST_STATUS.CANCELLED],
  WAITING: [
    INTEREST_STATUS.ANALYSED,
    //INTEREST_STATUS.INTERVIEWED,
    //INTEREST_STATUS.APPROVED,
    INTEREST_STATUS.CLOSED,
    INTEREST_STATUS.CANCELLED,
  ],
  WITHDRAWAL: [
    INTEREST_STATUS.COLABORATION,
    INTEREST_STATUS.WITHDRAWAL_ACCEPTED,
  ],
  WITHDRAWAL_ACCEPTED: [],
  DISPATCH: [
    //INTEREST_STATUS.DISPATCH_ACCEPT,
    //INTEREST_STATUS.DISPATCH_REJECT,
    INTEREST_STATUS.ACCEPTED,
    INTEREST_STATUS.DECLINED,
    INTEREST_STATUS.NOT_SELECTED,
    INTEREST_STATUS.WAITING,
  ],
};

export interface IHasInterest {
  can_express_interest: boolean;
  experience_avaiable: boolean;
  has_application: boolean;
  has_interest: boolean;
  has_application_accepted: boolean;
}

export enum ExperienceUserInterestDispatchDecision {
  REJECTED = 'REJECTED',
  ACCEPTED = 'ACCEPTED',
  NOT_SELECTED = 'NOT_SELECTED',
  WAITING = 'WAITING',
}
export class ExperienceUserInterestData {
  static eventToDispatchDecisionMapper: Partial<Record<INTEREST_STATUS, ExperienceUserInterestDispatchDecision>> = {
    ACCEPTED: ExperienceUserInterestDispatchDecision.ACCEPTED,
    DECLINED: ExperienceUserInterestDispatchDecision.REJECTED,
    NOT_SELECTED: ExperienceUserInterestDispatchDecision.NOT_SELECTED,
    WAITING: ExperienceUserInterestDispatchDecision.WAITING,
  }
}