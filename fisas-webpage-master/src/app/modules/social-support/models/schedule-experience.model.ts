export class ScheduleExperienceModel {
  schedule_id: number;
  time_begin: string;
  time_end: string;
}
