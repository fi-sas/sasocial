export class ScheduleModel {
  id?: number;
  day_week: string;
  day_period: string;
  created_at?: Date;
  updated_at?: Date;
}
