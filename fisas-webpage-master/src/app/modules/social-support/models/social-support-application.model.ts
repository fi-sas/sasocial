import { ActivityModel } from '@fi-sas/webpage/modules/social-support/models/activity.model';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from './../../../shared/models/course.model';
import { ExperienceModel } from './experiences.model';
import { Gender } from '@fi-sas/webpage/shared/models';
import { HistoryModel } from './history.model';
import { InterviewModel } from './interest.model';
import { SchoolModel } from './../../../shared/models/school.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { FileModel } from '../../media/models/file.model';

export interface ApplicationFile {
  file_id: number;
  name: string;
  file?: FileModel;
}

export class SocialSupportApplicationModel {
  id?: number;
  academic_year: string;
  mentor_or_mentee: string;
  course_year: number;
  experience_id?: number;
  course_id: number;
  course_degree_id: number;
  school_id: number;
  iban: string;
  has_scholarship: boolean;
  has_applied_scholarship: boolean;
  scholarship_monthly_value: number;
  has_emergency_fund: boolean;
  has_profissional_experience: boolean;
  is_waiting_scholarship_response: boolean;
  type_of_company: string;
  job_at_company: string;
  job_description: string;
  preferred_activities?: any[];
  preferred_activities_ids: number[];
  has_collaborated_last_year: boolean;
  previous_activity_description: string;
  preferred_schedule: any[];
  preferred_schedule_ids: number[];
  language_skills: string[];
  has_holydays_availability: boolean;
  foreign_languages: boolean;
  observations: string;
  created_at?: string;
  updated_at?: string;
  cv_file_id?: number;
  user_id?: number;
  notification_guid?: string;
  status?: string;
  course?: CourseModel;
  course_degree?: CourseDegreeModel;
  school?: SchoolModel;
  history?: HistoryModel[];
  preferred_activity?: ActivityModel[];
  organic_units?: any[];
  organic_units_ids: number[];
  experience?: ExperienceModel;
  user?: UserModel;
  student_address: string;
  student_birthdate: string;
  student_code_postal: string;
  student_email: string;
  student_identification: number;
  student_number: number;
  student_location: string;
  student_name: string;
  student_nationality: string;
  student_tin: number;
  student_mobile_phone: string;
  interviews: InterviewModel[];
  genre: Gender;
  housed_residence: boolean;
  other_scholarship: boolean;
  other_scholarship_values?: number;
  files?: Required<ApplicationFile>[];
  file_ids?: ApplicationFile[];
}

export class AuxDataApplication {
  course_degree: string;
  course: string;
  school: string;
  organic_units: string[];
  organic_units_ids: number[];
  preferred_activities: string[];
}
