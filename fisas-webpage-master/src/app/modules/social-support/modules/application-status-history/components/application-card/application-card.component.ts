import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { SocialSupportApplicationModel } from '../../../../models/social-support-application.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-application-card',
  templateUrl: './application-card.component.html',
  styleUrls: ['./application-card.component.less'],
})
export class ApplicationCardComponent {
  @Input() application: SocialSupportApplicationModel = null;
  @Input() translations: TranslationModel[] = [];

  buttonStyle = {
    'font-size': '12px',
    color: 'var(--primary-color)',
    'border-color': 'var(--primary-color)',
    height: 'auto',
  };

  status = {
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    EXPIRED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXPIRED',
  };

  modalDetail = false;

  constructor(private router: Router) {}

  goToApplicationReview(idApplication: number) {
    this.router.navigateByUrl('social-support/applications/review/' + idApplication);
  }

  checkTagStatusColor() {
    const css = {};
    css[this.application.status] = true;
    return css;
  }

  isPair(val) {
    return (val % 2 == 0);
  }

  openModal($event: MouseEvent) {
    // To prevent card click to being triggered
    $event.stopPropagation();

    this.modalDetail = true;
  }
}
