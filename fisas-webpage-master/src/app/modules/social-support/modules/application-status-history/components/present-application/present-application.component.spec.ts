import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PresentApplicationComponent } from './present-application.component';


describe('PresentApplicationComponent', () => {
  let component: PresentApplicationComponent;
  let fixture: ComponentFixture<PresentApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
