import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { ChangeStatusService } from '@fi-sas/webpage/modules/social-support/services/change-status.service';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';

@Component({
  selector: 'app-present-application',
  templateUrl: './present-application.component.html',
  styleUrls: ['./present-application.component.less'],
})
export class PresentApplicationComponent implements OnInit, OnDestroy {
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this._application = application;
      this.canEditApplication = application.id && ['SUBMITTED', 'ANALYSED'].includes(application.status);
    }
  }
  get application() {
    return this._application;
  }
  @Input() translations: TranslationModel[] = [];
  @Output() refresh = new EventEmitter();


  modalDetail = false;

  canEditApplication: boolean;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  buttonStyleCancel = {
    'font-size': '12px',
    'border-color': '#d0021b',
    color: 'white',
    'background-color': '#d0021b',
    height: 'auto',
    'padding-top': '2px',
    'padding-bottom': '2px'
  };

  status = {
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    DISPATCH: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DISPATCH',
    EXPIRED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXPIRED',
    INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
  };


  constructor(
    private changeStatusService: ChangeStatusService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService,
  ) {
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {

  }

  checkTagStatusColor() {
    const css = {};
    css[this.application.status] = true;
    return css;
  }

  cancel(idApplication: number) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_MODAL_TITLE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelConfirm(idApplication)
    });
  }

  cancelConfirm(idApplication: number) {
    this.changeStatusService.applicationCancel(idApplication).pipe(first()).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_SUCCESS'));
      this.refresh.emit(true);
    })
  }

  reviewApplication(idApplication: number) {
    this.router.navigateByUrl(`social-support/applications/review/${idApplication}`);
  }

  isPair(val) {
    return (val % 2 == 0);
  }

}
