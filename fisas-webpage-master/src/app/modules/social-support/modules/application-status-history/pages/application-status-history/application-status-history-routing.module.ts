import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationStatusHistoryComponent } from './application-status-history.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationStatusHistoryComponent,
    data: { breadcrumb: null, title: null, scope: ['social_scholarship:applications:list', 'sasocial:is_student'] },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationStatusHistorySocialSupportRoutingModule {}
