import { Component, OnInit, OnDestroy } from '@angular/core';

import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ReviewService } from './../../services/review.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { SocialApplicationsListService } from './../../services/social-applications-list.service';
import { SocialSupportApplicationModel } from './../../../../models/social-support-application.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-application-status-history',
  templateUrl: './application-status-history.component.html',
  styleUrls: ['./application-status-history.component.less'],
})
export class ApplicationStatusHistoryComponent implements OnInit, OnDestroy {
  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }
  loadingApplications = false;
  applications: SocialSupportApplicationModel[] = [];
  applicationsHistory: SocialSupportApplicationModel[] = [];
  isApplications = true;
  translations: TranslationModel[] = [];
  panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  constructor(
    private configurationsService: ConfigurationsService,
    private reviewService: ReviewService,
    private socialApplicationsListService: SocialApplicationsListService
  ) {
    this.getTranslations();
    this.getApplications();
    this.getApplicationsHistory();
  }

  ngOnInit() {}

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
    this.reviewService.saveApplication(null);
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }

  getApplications(): void {
    this.loadingApplications = true;
    this.socialApplicationsListService
      .applicationsActive()
      .pipe(first())
      .subscribe(
        (applications) => {
          this.applications = applications.data;
          this.loadingApplications = false;
        },
        () => {
          this.loadingApplications = false;
        }
      );
  }

  getApplicationsHistory(): void {
    this.socialApplicationsListService
      .applicationsHistory()
      .pipe(first())
      .subscribe((applications) => (this.applicationsHistory = applications.data));
  }

  refresh(event) {
    if (event == true) {
      this.getApplications();
      this.getApplicationsHistory();
    }
  }

}
