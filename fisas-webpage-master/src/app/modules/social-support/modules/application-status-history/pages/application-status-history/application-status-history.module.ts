import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationCardComponent, PresentApplicationComponent } from '../../components';
import { ApplicationStatusHistoryComponent } from './application-status-history.component';
import { ApplicationStatusHistorySocialSupportRoutingModule } from './application-status-history-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [
    ApplicationStatusHistoryComponent,
    ApplicationCardComponent,
    PresentApplicationComponent,
  ],
  imports: [CommonModule, SharedModule, ApplicationStatusHistorySocialSupportRoutingModule, SharedSocialSupportModule],
})
export class ApplicationStatusHistorySocialSupportModule {}
