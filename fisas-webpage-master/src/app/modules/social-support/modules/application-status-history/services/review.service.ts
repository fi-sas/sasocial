import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SocialSupportApplicationModel } from '../../../models/social-support-application.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  applicationToReview = new BehaviorSubject<SocialSupportApplicationModel>(null);
  constructor() {}

  applicationToReviewObservable(): Observable<SocialSupportApplicationModel> {
    return this.applicationToReview.asObservable();
  }

  saveApplication(application: SocialSupportApplicationModel) {
    this.applicationToReview.next(application);
  }

  getApplication(): SocialSupportApplicationModel {
    return this.applicationToReview.getValue();
  }

  getApplicationProperty(property: string): any {

    const application = this.applicationToReview.getValue();

    if (application !== null) {

      if (application.hasOwnProperty(property)) {
        return application[property];
      } else {
        return null;
      }

    } else {
      return null;
    }
  }
}
