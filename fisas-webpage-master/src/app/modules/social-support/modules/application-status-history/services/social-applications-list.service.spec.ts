import { TestBed } from '@angular/core/testing';

import { SocialApplicationsListService } from './social-applications-list.service';

describe('SocialApplicationsListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialApplicationsListService = TestBed.get(SocialApplicationsListService);
    expect(service).toBeTruthy();
  });
});
