import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SocialApplicationsListService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  applicationsActive(): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', '-created_at');
    params = params.set(
      'withRelated',
      'history,experience,course,course_degree,school,preferred_activities,organic_units,preferred_schedule,interviews,history'
    );
    params = params.append('query[status]', 'SUBMITTED');
    params = params.append('query[status]', 'ACCEPTED');
    params = params.append('query[status]', 'ANALYSED');
    params = params.append('query[status]', 'INTERVIEWED');
    params = params.append('query[status]', 'SUBMITTED');
    params = params.append('query[status]', 'DISPATCH');
    return this.resourceService.list<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      { params }
    );
  }

  applicationsHistory(): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', '-created_at');
    params = params.append('query[status]', 'DECLINED');
    params = params.append('query[status]', 'EXPIRED');
    params = params.append('query[status]', 'CANCELLED');
    params = params.set(
      'withRelated',
      'history,experience,course,course_degree,school,preferred_activities,organic_units,preferred_schedule'
    );
    return this.resourceService.list<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      { params }
    );
  }

  getApplicationById(id: number): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'history');
    return this.resourceService.read<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_ID', {id}), {params}
    );
  }

}
