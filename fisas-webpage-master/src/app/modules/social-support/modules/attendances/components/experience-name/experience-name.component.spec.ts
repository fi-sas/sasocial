import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceNameComponent } from './experience-name.component';

describe('ExperienceNameComponent', () => {
  let component: ExperienceNameComponent;
  let fixture: ComponentFixture<ExperienceNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExperienceNameComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
