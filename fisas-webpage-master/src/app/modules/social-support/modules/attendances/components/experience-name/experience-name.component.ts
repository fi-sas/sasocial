import { Component, OnInit, Input } from '@angular/core';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-experience-name',
  templateUrl: './experience-name.component.html',
  styleUrls: ['./experience-name.component.less'],
})
export class ExperienceNameComponent implements OnInit {
  @Input() experienceName = '...';
  @Input() translations: TranslationModel[] = [];

  constructor() {}

  ngOnInit() {}
}
