import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormJustificationAbsencesComponent } from './form-justification-absences.component';

describe('FormJustificationAbsencesComponent', () => {
  let component: FormJustificationAbsencesComponent;
  let fixture: ComponentFixture<FormJustificationAbsencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormJustificationAbsencesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormJustificationAbsencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
