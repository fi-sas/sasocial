import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAttendancesAdvisorComponent } from './list-attendances-advisor.component';

describe('ListAttendancesAdvisorComponent', () => {
  let component: ListAttendancesAdvisorComponent;
  let fixture: ComponentFixture<ListAttendancesAdvisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListAttendancesAdvisorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAttendancesAdvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
