import { Component, Input } from '@angular/core';

import { finalize, first, takeUntil } from 'rxjs/operators';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import {
  AbsenceReasonModel,
  AttendanceModel,
  AttendanceStatus,
  IAttendancesTableRow,
  ITableRowIcon,
} from '../../models';
import { AttendancesService } from './../../services/attendances.service';
import {
  FormAttendanceComponent,
  RejectAbsenceJustificationComponent,
  RejectAttendanceComponent,
} from '../../../shared-social-support/components';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';

@Component({
  selector: 'app-list-attendances-advisor',
  templateUrl: './list-attendances-advisor.component.html',
  styleUrls: ['./list-attendances-advisor.component.less'],
})
export class ListAttendancesAdvisorComponent {
  private _collaborationId: number;
  @Input() set collaborationId(id: number) {
    if (id !== null && !isNaN(id)) {
      this._collaborationId = id;
      this.getAttendances();
    }
  }

  get collaborationId() {
    return this._collaborationId;
  }

  readonly buttonStyle = {
    'background-color': 'var(--primary-color)',
    'border-color': 'var(--primary-color)',
    'font-size': '12px',
    'padding-bottom': '2px',
    'padding-top': '2px',
    color: '#fff',
    height: 'auto',
    width: '100%',
  };

  readonly buttonStyleCancel = {
    'background-color': '#fff',
    'border-color': '#d0021b',
    'font-size': '12px',
    'padding-bottom': '2px',
    'padding-top': '2px',
    color: '#000',
    height: 'auto',
    width: '100%',
  };

  private destroy$: Subject<void> = new Subject<void>();

  pageIndex = 1;
  pageSize = 10;
  total = 0;

  loadingAttendances = false;

  attendances: AttendanceModel[] = [];
  justificationAbsencesFormModal: NzModalRef = null;

  table: IAttendancesTableRow[] = [];

  loadingValidate: boolean;
  loadingValidateJustification: boolean;

  constructor(
    private attendancesService: AttendancesService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private uiService: UiService
  ) {
    this.translateService.onLangChange
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => (this.table = this.getAttendancesTable(this.attendances)));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  validate(attendance: AttendanceModel) {
    this.loadingValidate = true;
    this.attendancesService.validate(attendance.id).pipe(first(), finalize(() => this.loadingValidate = false))
    .subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.FORM.VALIDATE_SUCCESS')
      );
      this.getAttendances();
    });
  }

  validateJustification(attendance: AttendanceModel) {
    this.loadingValidateJustification = true;
    this.attendancesService
      .validateJustification(attendance.absence_reason[0].id)
      .pipe(
        first(),
        finalize(() => (this.loadingValidateJustification = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.FORM.VALIDATE_SUCCESS')
        );
        this.getAttendances();
      });
  }

  reject(attendance: AttendanceModel): void {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: RejectAttendanceComponent,
        nzFooter: null,
        nzComponentParams: { attendance },
        nzWidth: 650,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.getAttendances();
        }
      });
  }

  rejectJustification(attendance: AttendanceModel): void {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: RejectAbsenceJustificationComponent,
        nzFooter: null,
        nzComponentParams: { attendance },
        nzWidth: 650,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.getAttendances();
        }
      });
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.getAttendances();
  }

  isEmpty(object: object): boolean {
    if (object == null) {
      return true;
    }
    return Object.keys(object).length === 0;
  }

  private getAttendances(): void {
    this.loadingAttendances = true;
    this.attendancesService
      .list(this.pageIndex, this.pageSize, this.collaborationId)
      .pipe(first())
      .subscribe(
        (attendances) => {
          this.total = attendances.link.total;
          this.attendances = attendances.data;
          this.table = this.getAttendancesTable(this.attendances);
          this.loadingAttendances = false;
        },
        () => {
          this.loadingAttendances = false;
        }
      );
  }

  private getAttendancesTable(attendances: AttendanceModel[]): IAttendancesTableRow[] {
    const result: IAttendancesTableRow[] = [];
    attendances.forEach((attendance: AttendanceModel) => {
      let showHasAttachments = false;
      let absenceReason: ITableRowIcon;
      if ((attendance.absence_reason || []).length) {
        showHasAttachments = attendance.absence_reason[0].attachment_file_id !== null;
      }
      if (
        attendance.status === AttendanceStatus.ACCEPTED ||
        ((attendance.absence_reason || []).length && attendance.absence_reason[0].accept === true)
      ) {
        absenceReason = { icon: 'icons:paid', elemetClasses: 'paid-icon' };
      }
      if (
        attendance.status === AttendanceStatus.REJECTED ||
        ((attendance.absence_reason || []).length && attendance.absence_reason[0].accept === false)
      ) {
        absenceReason = { icon: 'icons:icons-close', elemetClasses: 'close-icon' };
      }
      result.push({
        hours: attendance.n_hours,
        submissionDate: attendance.created_at,
        justificationPeriod: this.getJustificationPeriod(attendance),
        justification: this.getJustification(attendance),
        assignment: this.getAssignement(attendance),
        attachments: {
          icon: showHasAttachments ? 'icons:icons-radiobutton-checked-2' : 'icons:icons-radiobutton-unchecked',
          elemetClasses: showHasAttachments ? 'checked-icon' : 'unchecked-icon',
          url: showHasAttachments ? attendance.absence_reason[0].attachment_file.url || null : null,
        },
        accepted: absenceReason,
        showJustifyButton: this.isEmpty(attendance.absence_reason) && !attendance.was_present,
        attendance,
        status: attendance.status
      });
    });
    return result;
  }

  private getJustification(attendance: AttendanceModel): string {
    if (attendance.was_present) {
      return;
    }

    if ((attendance.absence_reason || []).length) {
      return attendance.absence_reason[0].reason;
    }

    return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.NON_JUSTIFIED_ABSENCE');
  }

  private getAssignement(attendance: AttendanceModel): string {
    return attendance.executed_service;
  }

  private getJustificationPeriod(attendance: AttendanceModel): string[] {
    if ((attendance.absence_reason || []).length) {
      return attendance.absence_reason.map((reason: AbsenceReasonModel) => {
        return this.getJustificationPeriodLine(reason.start_date, reason.end_date);
      });
    }

    return [this.getJustificationPeriodLine(attendance.initial_time, attendance.final_time)];
  }

  private getJustificationPeriodLine(start: string, end: string): string {
    const startDate = moment(start);
    const endDate = moment(end);

    if (!startDate.isValid() || !endDate.isValid) {
      return;
    }

    const timeFormat = 'H:mm';
    const dateFormat = 'DD-MM-YYYY';

    if (startDate.format('L') === endDate.format('L')) {
      return `${startDate.format(dateFormat)}\n${startDate.format(timeFormat)} - ${endDate.format(timeFormat)}`;
    }

    return `${startDate.format(dateFormat)} ${this.translateService.instant(
      'SOCIAL_SUPPORT.ATTENDANCES.TABS.ATTENDANCES_ABSENCE.DATE_SEPARATOR'
    )} ${endDate.format(dateFormat)}`;
  }
}
