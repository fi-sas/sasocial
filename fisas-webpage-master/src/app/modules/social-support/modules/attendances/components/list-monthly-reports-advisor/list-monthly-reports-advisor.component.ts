import { Component, Input, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AddMonthlyReportComponent } from '../../../shared-social-support/components';
import { GeneralReportsApplicationService } from '../../services/general-reports-application.service';
import { IMonthlyReport } from '../../models';
import { Resource } from '@fi-sas/core';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';

@Component({
  selector: 'app-list-monthly-reports-advisor',
  templateUrl: './list-monthly-reports-advisor.component.html',
  styleUrls: ['./list-monthly-reports-advisor.component.less'],
})
export class ListMonthlyReportsAdvisorComponent implements OnInit{
  private _collaborationId: number;

  @Input() set collaborationId(id: number) {
    if (id !== null && !isNaN(id)) {
      this._collaborationId = id;
      this.getGeneralReportsMonthly();
    }
  }

  @Input() offer: ExperienceModel = null;

  day = new Date();
  get collaborationId() {
    return this._collaborationId;
  }

  isLoading: boolean;
  validDate = false;
  reports: IMonthlyReport[] = [];

  totalReports: number = 0;
  pageIndex: number = 1;
  pageSize: number = 10;

  tableRows = [];

  constructor(
    private generalReportsApplicationService: GeneralReportsApplicationService,
    private modalService: NzModalService,
    public translateService: TranslateService
  ) {}

  ngOnInit(){


    if(this.offer) {
      if(moment(this.day, "YYYY-MM-DD").isSameOrAfter(moment(this.offer.start_date).format("YYYY-MM-DD"))){
        this.validDate = true;
      }
    }
  }

  getGeneralReportsMonthly() {
    this.isLoading = true;
    this.generalReportsApplicationService
      .getMonthlyReports(this.pageIndex, this.pageSize, this.collaborationId)
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(result => {
        this.reports = result.data;
        this.tableRows = this.getRows(result.data);
        this.totalReports = result.link.total;
      });
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.getGeneralReportsMonthly();
  }

  openAddReportModal() {
    this.modalService
      .create({
        nzTitle: null,
        nzContent: AddMonthlyReportComponent,
        nzComponentParams: { collaborationId: this.collaborationId },
        nzFooter: null,
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.getGeneralReportsMonthly();
        }
      });
  }

  private getRows(data: IMonthlyReport[]) {
    const result = [];
    data.forEach((elem) => {
      result.push({
        date: moment().set({ month: elem.month - 1, year: elem.year }),
        created_at: elem.created_at,
        file: elem.file,
      });
    });
    return result;
  }
}
