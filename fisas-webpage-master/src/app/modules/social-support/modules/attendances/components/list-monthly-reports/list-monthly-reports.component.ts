import { Component, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { Resource } from '@fi-sas/core';
import { GeneralReportsApplicationService } from '../../services/general-reports-application.service';
import { IMonthlyReport } from '../../models';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-list-monthly-reports',
  templateUrl: './list-monthly-reports.component.html',
  styleUrls: ['./list-monthly-reports.component.less'],
})
export class ListMonthlyReportsComponent {
  private _collaborationId: number;

  @Input() set collaborationId(id: number) {
    if (id !== null && !isNaN(id)) {
      this._collaborationId = id;
      this.getGeneralReportsMonthly();
    }
  }

  get collaborationId() {
    return this._collaborationId;
  }

  isLoading: boolean;

  reports: IMonthlyReport[] = [];

  totalReports: number = 0;
  pageIndex: number = 1;
  pageSize: number = 10;

  tableRows = [];

  constructor(
    private generalReportsApplicationService: GeneralReportsApplicationService,
    public translateService: TranslateService
  ) {}

  getGeneralReportsMonthly() {
    this.isLoading = true;
    this.generalReportsApplicationService.getMonthlyReports(this.pageIndex, this.pageSize, this.collaborationId)
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(result => {
        this.reports = result.data;
        this.tableRows = this.getRows(result.data);
        this.totalReports = result.link.total;
      });
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.getGeneralReportsMonthly();
  }

  private getRows(data: IMonthlyReport[]) {
    const result = [];
    data.forEach((elem) => {
      result.push({
        date: moment().set({ month: elem.month - 1, year: elem.year }),
        created_at: elem.created_at,
        file: elem.file,
      });
    });
    return result;
  }
}
