import { Component, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { ApplicationCertificateService } from '@fi-sas/webpage/modules/social-support/services/application-certificate.service';
import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { GeneralReportsApplicationService } from '../../services/general-reports-application.service';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { Resource } from '@fi-sas/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-management-attendance-documentation',
  templateUrl: './management-attendance-documentation.component.html',
  styleUrls: ['./management-attendance-documentation.component.less'],
})
export class ManagementAttendanceDocumentationComponent extends FormHandler implements OnDestroy{
  private _collaborationId: number;

  @Input() set collaborationId(id: number) {
    if (id !== null && !isNaN(id)) {
      this._collaborationId = id;
      this.fetchData();
    }
  }

  get collaborationId() {
    return this._collaborationId;
  }

  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  isLoading: boolean;

  certificate: FileModel;
  certificateStatus?: 'pending' | 'failed';
  certificateForm = this.getForm();
  isLoadingCertificate: boolean;
  isLoadingGenerateCertificate: boolean;
  openForm = false;
  report: FileModel;
  reportForm = this.getForm();
  isLoadingReport: boolean;
  submittedCertificate: boolean = false;
  submittedReport: boolean = false;

  constructor(
    private applicationCertificateService: ApplicationCertificateService,
    private generalReportsApplicationService: GeneralReportsApplicationService,
    private interestService: InterestService,
    private translateService: TranslateService,
    private uiService: UiService
  ) {
    super();
  }

  onSubmitCertificate() {
    this.submittedCertificate = true;
    if (this.isFormValid(this.certificateForm)) {
      this.isLoadingCertificate = true;
      this.applicationCertificateService
        .addCertificate(this.collaborationId, this.certificateForm.value.file)
        .pipe(first(), finalize(() => this.isLoadingCertificate = false))
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.TABS.DOCUMENTATION.SUCCESS.ADD_CERTIFICATE')
          );
          this.submittedCertificate = true;
          this.fetchData();
        });
    }
  }

  generateCertificate() {
    this.isLoadingGenerateCertificate = true;
    this.applicationCertificateService.generateCertificate(this.collaborationId)
      .pipe(first(), finalize(() => this.isLoadingGenerateCertificate = false))
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.TABS.DOCUMENTATION.SUCCESS.GEN_CERTIFICATE')
        );
        this.fetchData();
      });
  }

  onSubmitReport() {
    this.submittedReport = true;
    if (this.isFormValid(this.reportForm)) {
      this.isLoadingReport = true;
      this.generalReportsApplicationService
        .addCollaborationEvaluationReport(this.collaborationId, this.reportForm.value.file)
        .pipe(first(), finalize(() => this.isLoadingReport = false))
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.TABS.DOCUMENTATION.SUCCESS.ADD_REPORT')
          );
          this.openForm = false;
          this.submittedReport = false;
          this.fetchData();
        });  
    }
  }

  getInputError(form: FormGroup, inputName: string): string {
    if (form.get(inputName).errors.required) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  private fetchData() {
    this.isLoading = true;
    this.interestService.getInterestById(this.collaborationId)
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(result => {
        if ((result.data || []).length) {
          this.certificate = result.data[0].certificate_file;
          if (result.data[0].certificate_generated) {
            if (result.data[0].certificate_generated.status === 'FAILED') {
              this.certificateStatus = 'failed';
              this.certificate = new FileModel();
            } else if (
              ['WAITING', 'PROCCESSING'].includes(result.data[0].certificate_generated.status) ||
              !result.data[0].certificate_generated.file
            ) {
              this.certificateStatus = 'pending';
              this.certificate = new FileModel();
            } else {
              this.certificate = result.data[0].certificate_generated.file;
            }
          }
          this.report = result.data[0].report_avaliation_file;
          if(!this.report) {
            this.openForm = true;
          }
          this.reportForm.reset();
        }
      });
  }

  private getForm() {
    return new FormGroup({
      file: new FormControl(null, Validators.required),
    });
  }

  update(){
    this.openForm = true;
    this.reportForm = this.getForm()
  }

  ngOnDestroy() {
    this.submittedCertificate = false;
    this.submittedReport = false;
  }
  
}
