import { FileModel } from "@fi-sas/webpage/modules/media/models/file.model";

export class AbsenceReasonModel {
  id?: number;
  application_id?: number;
  application_attendance_id?: number;
  experience_attendance_id?: number;
  start_date: string;
  end_date: string;
  attachment_file?: FileModel;
  attachment_file_id: number;
  reason: string;
  accept: boolean;
  created_at?: string;
  updated_at?: string;
  reject_reason?: string;
}
