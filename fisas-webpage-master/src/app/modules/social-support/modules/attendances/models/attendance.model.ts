import { AbsenceReasonModel } from './absence-reason.model';
import { SocialSupportApplicationModel } from '../../../models/social-support-application.model';
import { UserInterestModel } from '../../experiences/models/user-interest.model';

export enum AttendanceStatus {
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
  PENDING = 'PENDING',
  LACK = 'LACK',
}

export class AttendanceModel {
  id?: number;
  application_id: number;
  date: string;
  was_present: boolean;
  n_hours: string;
  initial_time?: any;
  final_time?: any;
  executed_service?: string;
  notes: string;
  application?: SocialSupportApplicationModel;
  absence_reason?: AbsenceReasonModel[];
  created_at?: string;
  updated_at?: string;
  file_id: number;
  user_interest_id: number;
  user_interest?: UserInterestModel;
  status: AttendanceStatus;
}
