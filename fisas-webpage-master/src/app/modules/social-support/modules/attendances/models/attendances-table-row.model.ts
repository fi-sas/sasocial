import { AttendanceModel } from './attendance.model';

export interface ITableRowIcon {
  icon: string;
  elemetClasses: string;
  url?: string;
  reason?: string
}

export interface IAttendancesTableRow {
  accepted?: ITableRowIcon;
  attachments: ITableRowIcon;
  attendance: AttendanceModel;
  hours: string;
  justification: string;
  assignment?: string;
  justificationPeriod: string[];
  showJustifyButton: boolean;
  submissionDate: string;
  status: string;
}
