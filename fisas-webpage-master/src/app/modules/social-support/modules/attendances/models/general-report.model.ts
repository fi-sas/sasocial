export class GeneralReportModel {
  total_hours_experience: number;
  total_hours_attendance: number;
  total_absences_with_accepted_reason: number;
  total_absences_without_accepted_reason: number;
  total_hours_validated: number;
}
