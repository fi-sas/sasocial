export class GeneralReportsMonthlyStatisticsModel {
  year_date: number;
  month_date: number;
  count_attendances: number;
  total_hours: number;
  count_absence_reason_accepted: number;
  count_absence_reason_not_accepted: number;
}
