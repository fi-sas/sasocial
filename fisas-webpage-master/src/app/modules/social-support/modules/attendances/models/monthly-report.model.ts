import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';

export interface IMonthlyReport {
  report: string;
  month: number;
  year: number;
  file?: FileModel;
  created_at?: string;
}

export interface IMonthlyReportAlert {
  alert: boolean;
  total_hours: number;
}

export interface IMonthlyReportAlerts {
  current_month: IMonthlyReportAlert;
  last_month: IMonthlyReportAlert;
}
