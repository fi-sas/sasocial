import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { first } from 'rxjs/operators';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { OffersService } from '@fi-sas/webpage/modules/social-support/services/offers.services';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';

@Component({
  selector: 'app-management-attendances',
  templateUrl: './management-attendances.component.html',
  styleUrls: ['./management-attendances.component.less'],
})
export class ManagementAttendancesComponent implements OnInit {
  isResponsable = true;

  offer: ExperienceModel;
  manifestations: InterestModel[] = [];
  selectedCollaborationId: number = null;

  isLoading: boolean;
  translations: TranslationModel[] = [];


  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private interestService: InterestService,
    private offersService: OffersService,
    private configurationsService:ConfigurationsService
  ) {
    const experience_id: number = this.activatedRoute.snapshot.params.id;
    this.getExperience(experience_id);
    this.getUserInterest(experience_id);
    this.translations = this.getTranslations();
    this.isResponsable = this.authService.hasPermission('social_scholarship:experiences:responsable');
  }

  ngOnInit() {
   
  }

  selectUser(interest: number) {
    this.selectedCollaborationId = interest;
  }

  private getTranslations(): TranslationModel[] {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    return service.translations;
  }

  private getExperience(id: number) {
    this.offersService
      .getOffer(id)
      .pipe(first())
      .subscribe((offer) => (this.offer = offer.data[0]));
  }

  private getUserInterest(id: number) {
    this.interestService
      .userInterestColaboration(id)
      .pipe(first())
      .subscribe((users_interest) => {
        if ((users_interest.data || []).length) {
          this.manifestations = users_interest.data;
          this.selectUser(this.manifestations[0].id);
        }
      });
  }
}
