import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import {
  ListAttendancesAdvisorComponent,
  ListMonthlyReportsAdvisorComponent,
  ManagementAttendanceDocumentationComponent,
} from '../../components';
import { ManagementAttendancesComponent } from './management-attendances.component';
import { ManagementAttendancesRoutingModule } from './management-attendances-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '@fi-sas/webpage/modules/social-support/modules/shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [
    ListAttendancesAdvisorComponent,
    ListMonthlyReportsAdvisorComponent,
    ManagementAttendancesComponent,
    ManagementAttendanceDocumentationComponent,
  ],
  imports: [CommonModule, ManagementAttendancesRoutingModule, SharedModule, SharedSocialSupportModule],
  providers: [DatePipe],
})
export class ManagementAttendancesModule {}
