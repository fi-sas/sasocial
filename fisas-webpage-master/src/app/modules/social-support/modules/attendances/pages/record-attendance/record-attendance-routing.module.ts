import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecordAttendanceComponent } from './record-attendance.component';

const routes: Routes = [
  {
    path: '',
    component: RecordAttendanceComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:attendances' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecordAttendanceRoutingModule {}
