import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { Resource } from '@fi-sas/core';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-record-attendance',
  templateUrl: './record-attendance.component.html',
  styleUrls: ['./record-attendance.component.less'],
})
export class RecordAttendanceComponent implements OnInit {
  isLoading: boolean;

  currentColaboration: InterestModel;

  translations: TranslationModel[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private configurationsService: ConfigurationsService,
    private interestService: InterestService
  ) { }

  ngOnInit() {
    this.translationsTitle();
    const collaborationId: number = this.activatedRoute.snapshot.params.id;
    this.getCollaboration(collaborationId);
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }

  getCollaboration(id: number) {
    this.isLoading = true;
    this.interestService.getInterestById(id)
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(result => {
        if(result.data.length){
          this.currentColaboration = result.data[0];
        }
      });    
  }
}
