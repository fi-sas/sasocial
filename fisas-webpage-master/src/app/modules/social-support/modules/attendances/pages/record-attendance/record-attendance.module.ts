import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { RecordAttendanceComponent } from './record-attendance.component';
import { RecordAttendanceRoutingModule } from './record-attendance-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import {
  ExperienceNameComponent,
  FormJustificationAbsencesComponent,
  ListAttendancesComponent,
  ListMonthlyReportsComponent,
} from '../../components';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [
    ExperienceNameComponent,
    FormJustificationAbsencesComponent,
    ListAttendancesComponent,
    ListMonthlyReportsComponent,
    RecordAttendanceComponent,
  ],
  imports: [CommonModule, RecordAttendanceRoutingModule, SharedModule, SharedSocialSupportModule],
  entryComponents: [FormJustificationAbsencesComponent],
  providers: [DatePipe],
})
export class RecordAttendanceModule {}
