import { TestBed } from '@angular/core/testing';

import { CurrentApplicationService } from './current-application.service';

describe('CurrentApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CurrentApplicationService = TestBed.get(CurrentApplicationService);
    expect(service).toBeTruthy();
  });
});
