import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SocialSupportApplicationModel } from '../../../models/social-support-application.model';

@Injectable({
  providedIn: 'root',
})
export class CurrentApplicationService {
  CurrentApplication = new BehaviorSubject<SocialSupportApplicationModel>(null);
  constructor() {}

  currentApplicationObservable(): Observable<SocialSupportApplicationModel> {
    return this.CurrentApplication.asObservable();
  }

  saveCurrentApplication(application: SocialSupportApplicationModel) {
    this.CurrentApplication.next(application);
  }

  getApplication(): SocialSupportApplicationModel {
    return this.CurrentApplication.getValue();
  }

  getApplicationProperty(property: string): any {
    const application = this.CurrentApplication.getValue();

    if (application !== null) {
      if (application.hasOwnProperty(property)) {
        return application[property];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
