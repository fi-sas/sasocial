import { TestBed } from '@angular/core/testing';

import { GeneralReportsMonthlyStatisticsService } from './general-reports-monthly-statistics.service';

describe('GeneralReportsMonthlyStatisticsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralReportsMonthlyStatisticsService = TestBed.get(GeneralReportsMonthlyStatisticsService);
    expect(service).toBeTruthy();
  });
});
