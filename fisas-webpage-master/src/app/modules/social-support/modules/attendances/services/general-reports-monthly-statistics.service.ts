import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { GeneralReportsMonthlyStatisticsModel } from '../models/general-reports-month-statistics.model';

@Injectable({
  providedIn: 'root',
})
export class GeneralReportsMonthlyStatisticsService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  generalReportsMonthlyStatistics(
    user_id: number,
    experience_id: number
  ): Observable<Resource<GeneralReportsMonthlyStatisticsModel>> {
    return this.resourceService.list<GeneralReportsMonthlyStatisticsModel>(
      this.urlService.get('SOCIAL_SUPPORT.GENERAL_REPORTS_MONTHLY_STATISTICS', {
        user_id,
        experience_id,
      }),
      {}
    );
  }
}
