import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { SocialSupportApplicationModel } from '../../../models/social-support-application.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserApplicationsService {
  constructor(
    private authService: AuthService,
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  userApplications(): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', 'id');
    params = params.set('query[user_id]', this.authService.getUser().id.toString());
    params = params.set('withRelated', 'experience');

    return this.resourceService.list<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      { params }
    );
  }
}
