import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { ExperiencesService } from '../../services/experiences.service';
import { ExpressInterestComponent } from '../../../shared-social-support/components';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-experience-card',
  templateUrl: './experience-card.component.html',
  styleUrls: ['./experience-card.component.less'],
})
export class ExperienceCardComponent {
  protected _experience: ExperienceModel = null;
  user;
  @Input() set experience(experience: ExperienceModel) {
    if (experience) {
      this._experience = experience;

      this.interestService.canExpressInterest(experience.id).pipe(first()).subscribe((result) => {
        this.canExpressInterest = result.canExpresssInteres && this.isExpressInterestPeriodOpen(experience);
        this.hasApplication = result.hasApplication;
        this.applicationActive = result.applicationActive;
      });
    }
  }
  get experience() {
    return this._experience;
  }

  withFooter: boolean = true;

  @Output() experienceChanged = new EventEmitter();

  translations: TranslationModel[] = [];

  canExpressInterest: boolean;
  hasApplication: boolean;
  applicationActive: boolean;

  constructor(
    protected configurationsService: ConfigurationsService,
    protected experiencesService: ExperiencesService,
    protected interestService: InterestService,
    protected modalService: NzModalService,
    protected router: Router,
    protected userService: AuthService,
    protected translateService: TranslateService
  ) {
    this.user = this.userService.getUser();
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations || [];
  }

  experienceDetail() {
    this.experiencesService.saveExperience(this.experience);
  }

  expressInterest($event: MouseEvent) {
    // To prevent card click to being triggered
    $event.stopPropagation();

    if (!this.hasApplication) {
      return this.modalService.confirm({
        nzTitle: this.translateService.instant('SOCIAL_SUPPORT.NO_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('SOCIAL_SUPPORT.NO_APPLICATION.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: this.translateService.instant('MISC.APPLY'),
        nzOnOk: () => this.router.navigateByUrl('social-support/applications'),
      });
    }

    if (!this.applicationActive) {
      return this.modalService.warning({
        nzTitle: this.translateService.instant('SOCIAL_SUPPORT.APPLICATION_NOT_ACTIVE.TITLE'),
        nzContent: this.translateService.instant('SOCIAL_SUPPORT.APPLICATION_NOT_ACTIVE.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: null,
      });
    }

    this.modalService
      .create({
        nzTitle: null,
        nzContent: ExpressInterestComponent,
        nzFooter: null,
        nzComponentParams: { experience: this.experience, translations: this.translations },
        nzWidth: 350,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.experienceChanged.emit(true);
        }
      });
  }

  private isExpressInterestPeriodOpen(experience: ExperienceModel): boolean {
    const d = moment();
    return d.isSameOrAfter(experience.publish_date) && d.isSameOrBefore(experience.application_deadline_date);
  }
}
