import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services';
import { ExperienceCardComponent } from './experience-card.component';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { ExperiencesService } from '../../services/experiences.service';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';

@Component({
  selector: 'app-small-experience-card',
  templateUrl: './experience-card.component.html',
  styleUrls: ['./experience-card.component.less'],
})
export class SmallExperienceCardComponent extends ExperienceCardComponent {
  protected _experience: ExperienceModel = null;
  @Input() set experience(experience: ExperienceModel) {
    if (experience) {
      this._experience = experience;
    }
  }
  get experience() {
    return this._experience;
  }

  withFooter = false;

  constructor(
    protected configurationsService: ConfigurationsService,
    protected experiencesService: ExperiencesService,
    protected interestService: InterestService,
    protected modalService: NzModalService,
    protected router: Router,
    protected userService: AuthService,
    protected translateService: TranslateService
  ) {
    super(configurationsService, experiencesService, interestService, modalService, router, userService, translateService);
  }
}
