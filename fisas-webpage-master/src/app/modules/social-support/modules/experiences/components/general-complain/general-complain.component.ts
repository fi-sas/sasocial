import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ComplainService } from '@fi-sas/webpage/modules/social-support/services';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-general-complain',
  templateUrl: './general-complain.component.html',
  styleUrls: ['./general-complain.component.less'],
})
export class GeneralComplainComponent extends FormHandler {
  @Input() translations: TranslationModel[] = [];

  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];

  form: FormGroup = new FormGroup({
    complain: new FormControl(null, [Validators.required]),
    file_id: new FormControl(null),
  });

  isLoading: boolean;

  constructor(
    private complainService: ComplainService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,
    private uiService: UiService
  ) {
    super();
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  close() {
    this.modalRef.close(false);
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      this.complainService
        .general(this.form.value)
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VOLUNTEERING.GENERAL_COMPLAIN.SUBMIT_SUCCESS')
          );
          this.modalRef.close(true);
        });
    }
  }
}
