import { Component, OnInit, OnDestroy } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { ExperiencesService } from '../../services/experiences.service';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-list-experiences',
  templateUrl: './list-experiences.component.html',
  styleUrls: ['./list-experiences.component.less'],
})
export class ListExperiencesComponent implements OnInit, OnDestroy {
  isLoading = false;
  isPastExperiencesLoading = false;

  experiences: ExperienceModel[] = [];
  pastExperiences: ExperienceModel[] = [];

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  readonly serviceId = SERVICE_IDS.SOCIAL_SUPPORT;

  constructor(private experiencesService: ExperiencesService) { }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    this.getExperiences();
    this.getPastExperiences();
  }

  getExperiences() {
    this.isLoading = true;
    this.experiencesService
      .experiences(
        1,
        -1,
        'id',
        'DESC',
        'experience_responsible,experience_advisor,organic_unit,attachment_file,translations',
        ['APPROVED', 'SELECTION', 'IN_COLABORATION'],
        true
      )
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((experiences) => (this.experiences = experiences.data));
  }

  getPastExperiences() {
    this.isPastExperiencesLoading = true;
    this.experiencesService
      .experiences(1, -1, 'id', 'DESC', 'translations', ['CANCELLED', 'CLOSED'])
      .pipe(
        first(),
        finalize(() => (this.isPastExperiencesLoading = false))
      )
      .subscribe((response) => (this.pastExperiences = response.data));
  }
}
