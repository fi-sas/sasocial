export class ScheduleTableModel {
  id: number;
  day_week: string;
  day_period: string;
  day_week_translation?: String;
  day_period_translation?: String;
}
