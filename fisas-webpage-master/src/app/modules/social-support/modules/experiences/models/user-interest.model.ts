import { UserModel } from "@fi-sas/webpage/auth/models/user.model";

export class UserInterestModel {
  id?: number;
  experience_id: number;
  user_id?: number;
  user?: UserModel;
  is_interest?: true;
}
