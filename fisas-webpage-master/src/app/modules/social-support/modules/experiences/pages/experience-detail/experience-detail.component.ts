import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from './../../../../models/experiences.model';
import { ExperiencesService } from './../../services/experiences.service';
import { ExpressInterestComponent } from '../../../shared-social-support/components';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-experience-detail',
  templateUrl: './experience-detail.component.html',
  styleUrls: ['./experience-detail.component.less'],
})
export class ExperienceDetailComponent implements OnInit {
  experience: ExperienceModel = null;
  translations: TranslationModel[] = [];
  idExperience: number;
  interest: any = {};
  modalInterest = false;
  loading = true;

  loadingButton = false;

  buttonStyle = {
    width: '100%',
    height: '50px',
    'font-size': '17.5px',
    'margin-top': '100px',
    'margin-bottom': '31.5px',
  };

  canExpressInterest: boolean;
  applicationActive: boolean;
  hasApplication: boolean;
  user;
  constructor(
    private configurationsService: ConfigurationsService,
    private experiencesService: ExperiencesService,
    private interestService: InterestService,
    private modalService: NzModalService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: AuthService,
    private translateService: TranslateService
  ) {
    this.user = this.userService.getUser();
    this.route.params.subscribe((params) => {
      this.idExperience = params['id'];
      this.scrollTop();
      this.getExperienceById();
    });
  }

  ngOnInit() {
    this.getTranslations();
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }

  backToExperiences() {
    this.router.navigateByUrl('social-support/experiences');
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }

  isEmpty(experience: ExperienceModel) {
    if (experience == null) {
      return true;
    }
    return experience.attachment_file ? Object.keys(experience.attachment_file).length === 0 : true;
  }

  getExperienceById() {

    this.experiencesService
      .experience(this.idExperience)
      .pipe(first(), finalize(() => this.loading = false))
      .subscribe((data) => {
        this.experience = data.data[0];
        this.checkInterest();
      });
  }

  checkInterest() {
    this.interestService.canExpressInterest(this.idExperience).subscribe((result) => {
      this.canExpressInterest = result.canExpresssInteres;
      this.hasApplication = result.hasApplication;
      this.applicationActive = result.applicationActive;
    });
  }

  expressInterest() {
    if (!this.hasApplication) {
      return this.modalService.confirm({
        nzTitle: this.translateService.instant('SOCIAL_SUPPORT.NO_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('SOCIAL_SUPPORT.NO_APPLICATION.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: this.translateService.instant('MISC.APPLY'),
        nzOnOk: () => this.router.navigateByUrl('social-support/applications'),
      });
    }

    if (!this.applicationActive) {
      return this.modalService.warning({
        nzTitle: this.translateService.instant('SOCIAL_SUPPORT.APPLICATION_NOT_ACTIVE.TITLE'),
        nzContent: this.translateService.instant('SOCIAL_SUPPORT.APPLICATION_NOT_ACTIVE.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: null,
      });
    }

    this.modalService
      .create({
        nzTitle: null,
        nzContent: ExpressInterestComponent,
        nzFooter: null,
        nzComponentParams: { experience: this.experience, translations: this.translations },
        nzWidth: 350,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.checkInterest();
        }
      });
  }
}
