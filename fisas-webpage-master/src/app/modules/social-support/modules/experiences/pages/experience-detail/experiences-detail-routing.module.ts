import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExperienceDetailComponent } from './experience-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ExperienceDetailComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:experiences:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExperiencesDetailSocialSupportRoutingModule {}
