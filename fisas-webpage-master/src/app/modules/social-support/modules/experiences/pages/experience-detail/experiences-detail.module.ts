import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ExperienceDetailComponent } from './experience-detail.component';
import { ExperiencesDetailSocialSupportRoutingModule } from './experiences-detail-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [ExperienceDetailComponent],
  imports: [CommonModule, SharedModule, SharedSocialSupportModule, ExperiencesDetailSocialSupportRoutingModule],
})
export class ExperiencesDetailSocialSupportModule {}
