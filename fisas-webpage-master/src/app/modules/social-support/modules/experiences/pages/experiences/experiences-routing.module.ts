import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExperiencesComponent } from './experiences.component';

const routes: Routes = [
  {
    path: '',
    component: ExperiencesComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:experiences:list' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExperiencesSocialSupportRoutingModule {}
