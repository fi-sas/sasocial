import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ApplicationsToAcceptService } from '../../services/applications-to-accept.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { ExperiencesService } from './../../services/experiences.service';
import { GeneralComplainComponent } from '../../components/general-complain/general-complain.component';
import { NzModalService } from 'ng-zorro-antd';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.less'],
})
export class ExperiencesComponent implements OnDestroy {
  experience: ExperienceModel;
  showExperienceDetail = false;
  translations: TranslationModel[] = [];
  activeApplicationButton = false;
  idApplication: number;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(
    private applicationsToAcceptService: ApplicationsToAcceptService,
    private authService: AuthService,
    private modalService:NzModalService,
    private configurationsService: ConfigurationsService,
    private experiencesService: ExperiencesService,
    private router: Router,
  ) {
    this.getTranslations();
    this.applicationUser();
    this.subscriptions = this.experiencesService.experienceSelectedObservable().subscribe((experience) => {
      if (experience !== null) {
        this.experience = experience;
        this.showExperienceDetail = true;
        this.router.navigateByUrl(`social-support/experiences/${this.experience.id}`);
      } else {
        this.showExperienceDetail = false;
        this.experience = null;
      }
    });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
    this.experiencesService.saveExperience(null);
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }

  applicationUser() {
    if (!this.authService.hasPermission('social_scholarship:applications:create')) {
      return;
    }
    this.applicationsToAcceptService
      .applicationUser()
      .pipe(first())
      .subscribe((data) => {
        this.activeApplicationButton = data.data[0].active;
        this.idApplication = data.data[0].id;
      });
  }

  complain() {
    this.modalService.create({
      nzTitle: null,
      nzContent: GeneralComplainComponent,
      nzFooter: null,
      nzWidth: 350,
      nzWrapClassName: 'vertical-center-modal',
    });
  }

  goToApplication() {
    this.router.navigateByUrl('social-support/applications');
  }

  goToOffer() {
    this.router.navigateByUrl('social-support/offers');
  }

  goToReview() {
    this.router.navigateByUrl(`social-support/applications/review/${this.idApplication}`);
  }
}
