import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ExperienceCardComponent } from '../../components/experience-card/experience-card.component';
import { ExperiencesComponent } from './experiences.component';
import { ExperiencesSocialSupportRoutingModule } from './experiences-routing.module';
import { GeneralComplainComponent } from '../../components/general-complain/general-complain.component';
import { ListExperiencesComponent } from '../../components/list-experiences/list-experiences.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';
import { SmallExperienceCardComponent } from '../../components/experience-card/small-experience-card.component';

@NgModule({
  declarations: [
    ExperienceCardComponent,
    ExperiencesComponent,
    GeneralComplainComponent,
    ListExperiencesComponent,
    SmallExperienceCardComponent,
  ],
  imports: [CommonModule, ExperiencesSocialSupportRoutingModule, SharedModule, SharedSocialSupportModule],
  entryComponents: [GeneralComplainComponent],
})
export class SocialSupportExperiencesModule {}
