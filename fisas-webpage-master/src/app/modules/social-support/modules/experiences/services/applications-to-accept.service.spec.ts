import { TestBed } from '@angular/core/testing';

import { ApplicationsToAcceptService } from './applications-to-accept.service';

describe('ApplicationsToAcceptService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationsToAcceptService = TestBed.get(ApplicationsToAcceptService);
    expect(service).toBeTruthy();
  });
});
