import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { Observable, BehaviorSubject, of } from 'rxjs';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { SocialSupportApplicationModel } from '../../../models/social-support-application.model';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsToAcceptService {
  accept = new BehaviorSubject(false);

  constructor(
    private authService: AuthService,
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  applicationsToAccept(): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[status]', 'ACCEPTED');
    params = params.set('withRelated', 'experience');

    return this.resourceService.list<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      { params }
    );
  }

  acceptObservable(): Observable<boolean> {
    return this.accept.asObservable();
  }

  submittedAccept(submitted: boolean) {
    this.accept.next(submitted);
  }

  applicationUser(): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('SOCIAL_SUPPORT.USER_APPLICATION')
    );
  }
}
