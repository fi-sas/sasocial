import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ExperienceModel } from '../../../models/experiences.model';
import { HttpParams } from '@angular/common/http';
import { UserInterestModel } from '../models/user-interest.model';
import { InterestModel } from '../../../models/interest.model';

@Injectable({
  providedIn: 'root',
})
export class ExperiencesService {
  experienceSelected = new BehaviorSubject<ExperienceModel>(null);

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  experienceSelectedObservable(): Observable<ExperienceModel> {
    return this.experienceSelected.asObservable();
  }

  saveExperience(experience: ExperienceModel) {
    this.experienceSelected.next(experience);
  }

  getExperienceSelected() {
    this.experienceSelected.getValue();
  }

  getExperienceProperty(property: string): any {
    const application = this.experienceSelected.getValue();

    if (application !== null) {
      if (application.hasOwnProperty(property)) {
        return application[property];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  /**
   * withRelated possible values:
   * - history
   * - applications
   */

  experiences(
    pageIndex: number,
    pageSize: number,
    sortKey?: string,
    sortValue?: string,
    withRelated?: string,
    status?: string[],
    isAvaliable?: boolean
  ): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    if (sortKey) {
      if (sortValue === 'ascend') {
        params = params.set('sort', sortKey);
      } else {
        params = params.set('sort', '-' + sortKey);
      }
    }

    if (withRelated) {
      params = params.set('withRelated', withRelated);
    }

    if (status && status.length > 0) {
      (status || []).forEach((s) => params = params.append('query[status]', s));
    }

    if(isAvaliable) {
      params = params.set('query[isAvaliable]', isAvaliable.toString());
    }

    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES'),
      { params }
    );
  }

  experience(id: number): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'attachment_file,translations,organic_unit,experience_responsible,experience_advisor');
    return this.resourceService.read<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_ID', { id }) ,
      {params}
    );
  }
}
