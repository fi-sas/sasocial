import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestationCardComponent } from './manifestation-card.component';

describe('ManifestationCardComponent', () => {
  let component: ManifestationCardComponent;
  let fixture: ComponentFixture<ManifestationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManifestationCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
