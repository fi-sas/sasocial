import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';

@Component({
  selector: 'app-manifestation-card',
  templateUrl: './manifestation-card.component.html',
  styleUrls: ['./manifestation-card.component.less'],
})
export class ManifestationCardComponent {
  @Input() manifestation: InterestModel = null;

  showStatusHistoryModal: boolean;

  status = {
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    CLOSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CLOSED',
    COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.COLABORATION',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
    NOT_SELECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.NOT_SELECTED',
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    WAITING: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WAITING',
    WITHDRAWAL: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WITHDRAWAL',
    WITHDRAWAL_ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WITHDRAWAL_ACCEPTED',
  };

  constructor(private router: Router) {}

  goToManifestationReview(id: number) {
    this.router.navigateByUrl('social-support/experiences/' + id);
  }

  goToManifestationDetail($event: MouseEvent,id: number) {
    $event.stopPropagation();
    this.router.navigateByUrl('social-support/record/' + id);
  }

  checkTagStatusColor() {
    const css = {};
    css[this.manifestation.status] = true;
    return css;
  }

  isPair(val) {
    return val % 2 == 0;
  }

  openModal($event: MouseEvent) {
    // To prevent card click to being triggered
    $event.stopPropagation();

    this.showStatusHistoryModal = true;
  }
}
