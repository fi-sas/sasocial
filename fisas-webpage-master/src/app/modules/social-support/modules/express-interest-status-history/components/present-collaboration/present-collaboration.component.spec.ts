import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentCollaborationComponent } from './present-collaboration.component';

describe('PresentCollaborationComponent', () => {
  let component: PresentCollaborationComponent;
  let fixture: ComponentFixture<PresentCollaborationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PresentCollaborationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentCollaborationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
