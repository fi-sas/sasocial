import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first } from 'rxjs/operators';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { formContestModel } from '@fi-sas/webpage/modules/social-support/models/form-contest.model';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd';
import { FormWithdrawalComponent } from '../../../shared-social-support/components';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-present-collaboration',
  templateUrl: './present-collaboration.component.html',
  styleUrls: ['./present-collaboration.component.less'],
})
export class PresentCollaborationComponent implements OnInit, OnDestroy {
  @Input() presentCollaboration: InterestModel = null;
  @Input() translations: TranslationModel[] = [];
  @Output() refresh = new EventEmitter();
  @Output() refreshAccepted = new EventEmitter();

  modalDetail = false;
  modalAccepted = false;
  visibleModalContest = false;
  userName: string;
  formContest: FormGroup;
  filterTypes = ['application/pdf'];
  dataSend: formContestModel = new formContestModel();

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  status = {
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    WAITING: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WAITING',
    NOT_SELECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.NOT_SELECTED',
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.COLABORATION',
    WITHDRAWAL: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WITHDRAWAL',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    CLOSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CLOSED',
    DISPATCH: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DISPATCH',
  };

  constructor(
    private authService: AuthService,
    private interestService: InterestService,
    private formBuilder: FormBuilder,
    private uiService: UiService,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private router: Router
  ) {
    this.userName = this.authService.getUser().name;
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    this.formContest = new FormGroup({
      complain: new FormControl('', [trimValidation]),
      file_id: new FormControl(),
    });
  }

  checkTagStatusColor() {
    const css = {};
    css[this.presentCollaboration.status] = true;
    return css;
  }

  isPair(val) {
    return val % 2 == 0;
  }

  withdrawal(idInterest: number) {
    this.modalService
      .create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: FormWithdrawalComponent,
        nzComponentParams: {
          applicationID: idInterest,
          action: 'WITHDRAWAL',
        },
        nzFooter: null,
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.refresh.emit(true);
        }
      });
  }

  cancel(idInterest: number) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.CONFIRM_CANCEL'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelConfirm(idInterest),
    });
  }

  cancelConfirm(idInterest: number) {
    this.interestService
      .changeStatusInterestCancel(idInterest)
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('SOCIAL_SUPPORT.ORDER_CANCEL'));
        this.refresh.emit(true);
      });
  }

  acceptedFinally() {
    let data: any = {};
    data.event = 'ACCEPT';
    this.interestService
      .changeStatusInterest(data, this.presentCollaboration.id)
      .pipe(first())
      .subscribe(() => {
        this.modalAccepted = false;
        this.uiService.showMessage(MessageType.success, this.translateService.instant('SOCIAL_SUPPORT.ORDER_ACCEPTED'));
        this.refresh.emit(true);
      });
  }

  contest(idInterest: number) {
    this.visibleModalContest = true;
    this.dataSend.user_interest_id = idInterest;
  }

  submitContest() {
    this.dataSend.complain = this.formContest.get('complain').value;
    this.dataSend.file_id = this.formContest.get('file_id').value;
    this.interestService
      .changeStatusInterestComplain(this.dataSend.user_interest_id, this.dataSend)
      .pipe(first())
      .subscribe(() => {
        this.visibleModalContest = false;
        this.formContest.reset();
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.SUCCESS_COMPLAI')
        );
        this.refresh.emit(true);
      });
  }

  reviewExperience(idExperience: number) {
    this.router.navigateByUrl('social-support/experiences/' + idExperience);
  }

  manage(id: number) {
    this.router.navigateByUrl('social-support/record/' + id);
  }
}
