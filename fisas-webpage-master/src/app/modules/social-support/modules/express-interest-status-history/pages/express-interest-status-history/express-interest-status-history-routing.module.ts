import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpressInterestStatusHistoryComponent } from './express-interest-status-history.component';

const routes: Routes = [
  {
    path: '',
    component: ExpressInterestStatusHistoryComponent,
    data: { breadcrumb: null, title: null, scope: ['social_scholarship:experience-user-interest:list'] },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpressInterestStatusHistorySocialSupportRoutingModule {}
