import { Component, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-express-interest-status-history',
  templateUrl: './express-interest-status-history.component.html',
  styleUrls: ['./express-interest-status-history.component.less'],
})
export class ExpressInterestStatusHistoryComponent implements OnInit {
  presentCollaborations: InterestModel[] = [];
  loadingCollaboration = true;
  manifestations: InterestModel[] = [];
  translations: TranslationModel[] = [];
  panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.HISTORY_MANIFESTATIONS.TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  constructor(private interestService: InterestService, private configurationsService: ConfigurationsService) {}

  ngOnInit() {
    this.getPresentCollaboration();
    this.getTranslations();
    this.getManifestationsClosedCancelled();
  }

  getPresentCollaboration() {
    this.interestService
      .listInterest()
      .pipe(
        first(),
        finalize(() => (this.loadingCollaboration = false))
      )
      .subscribe((data) => this.presentCollaborations = data.data);
  }

  refresh(event) {
    if (event == true) {
      this.getPresentCollaboration();
      this.getManifestationsClosedCancelled();
    }
  }

  getManifestationsClosedCancelled() {
    this.interestService
      .listInterestClosedCancelled()
      .pipe(first())
      .subscribe((result) => this.manifestations = result.data);
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }
}
