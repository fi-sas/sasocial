import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';
import { ExpressInterestStatusHistorySocialSupportRoutingModule } from './express-interest-status-history-routing.module';
import { ExpressInterestStatusHistoryComponent } from './express-interest-status-history.component';
import { PresentCollaborationComponent } from '../../components/present-collaboration/present-collaboration.component';
import { ManifestationCardComponent } from '../../components/manifestation-card/manifestation-card.component';

@NgModule({
  declarations: [ExpressInterestStatusHistoryComponent, PresentCollaborationComponent, ManifestationCardComponent],
  imports: [
    CommonModule,
    SharedModule,
    ExpressInterestStatusHistorySocialSupportRoutingModule,
    SharedSocialSupportModule,
  ],
})
export class ExpressInterestStatusHistorySocialSupportModule {}
