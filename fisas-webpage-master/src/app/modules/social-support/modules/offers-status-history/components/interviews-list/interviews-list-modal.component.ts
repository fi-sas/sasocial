import { Component, Input } from '@angular/core';

import { NzModalRef } from 'ng-zorro-antd';

import { InterviewModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';

@Component({
  selector: 'app-interviews-list-modal',
  templateUrl: './interviews-list-modal.component.html',
  styleUrls: [],
})
export class InterviewsListModalComponent {
  @Input() interviews: InterviewModel[] = [];

  constructor(private modalRef: NzModalRef) { }

  close(success: boolean) {
    this.modalRef.close(success);
  }
}
