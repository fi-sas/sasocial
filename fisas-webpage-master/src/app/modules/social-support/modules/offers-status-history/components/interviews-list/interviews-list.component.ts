import { Component, EventEmitter, Input, Output } from '@angular/core';

import { InterviewModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';

@Component({
  selector: 'app-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.less'],
})
export class InterviewsListComponent {
  @Input() interviews: InterviewModel[] = [];

  @Output() canClose = new EventEmitter<boolean>();

  constructor() { }

  close() {
    this.canClose.emit(true);
  }
}
