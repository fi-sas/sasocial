import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { InterviewModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-register-interview',
  templateUrl: './register-interview.component.html',
  styleUrls: ['./register-interview.component.less'],
})
export class RegisterInterviewComponent extends FormHandler {
  private _interview: InterviewModel;
  @Input() set interview(interview: InterviewModel) {
    this._interview = interview;
    this.form = this.getForm(interview);
  }
  get interview() {
    return this._interview;
  }

  @Output() hasChanged = new EventEmitter<boolean>();

  form: FormGroup;

  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  isLoading = false;

  constructor(
    private translateService: TranslateService,
    private interestService: InterestService,
    private uiService: UiService
  ) {
    super();
    this.form = new FormGroup({
      notes: new FormControl(null, [Validators.required, trimValidation]),
      file_id: new FormControl(null),
    });
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      const report = new InterviewModel();
      report.notes = this.form.value.notes;
      report.file_id = this.form.value.file_id;
      this.interestService
        .addInterviewReport(this.interview.id, report)
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(
          () => {
            this.isLoading = false;
            this.uiService.showMessage(MessageType.success, this.translateService.instant('MISC.OPERATION_SUCCESSFUL'));
            this.hasChanged.emit(true);
          },
          () => {
            this.isLoading = false;
          }
        );
    }
  }

  private getForm(interview?: InterviewModel) {
    if (interview) {
      this.form.get('notes').setValue(interview.notes || null);
      this.form.get('file_id').setValue(interview.file_id || null);
    }
    return this.form;
  }
}
