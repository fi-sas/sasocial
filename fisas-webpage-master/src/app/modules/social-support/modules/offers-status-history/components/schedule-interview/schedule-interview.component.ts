import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ExperienceCandiate } from '@fi-sas/webpage/modules/social-support/models/experience-candiate.model';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { INTEREST_STATUS_EVENT } from '@fi-sas/webpage/modules/social-support/models/interest.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';

interface SubmitData {
  date: string;
  local: string;
  responsable_id: number;
}

@Component({
  selector: 'app-schedule-interview',
  templateUrl: './schedule-interview.component.html',
  styleUrls: ['./schedule-interview.component.less'],
})
export class ScheduleInterviewComponent extends FormHandler {
  @Input() candidate: ExperienceCandiate;

  protected readonly range24 = this.range(24);
  protected readonly range60 = this.range(60);

  form: FormGroup;

  isLoading: boolean;

  constructor(
    private authService: AuthService,
    private interestService: InterestService,
    private modalRef: NzModalRef,
    private translateService: TranslateService
  ) {
    super();

    this.form = new FormGroup({
      local: new FormControl(null, [Validators.required, trimValidation]),
      date: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      const data: SubmitData = {
        local: this.form.get('local').value,
        date: moment(this.form.get('date').value).toISOString(),
        responsable_id: this.authService.getUser().id,
      };

      this.isLoading = true;
      this.interestService.changeStatusInterestScheduleInterview(data, this.candidate.id)
        .pipe(first(), finalize(() => this.isLoading = false))
        .subscribe(() => {
          this.modalRef.close(true);
        });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  disableDate = (currentDate: Date): boolean => {
    return moment(currentDate).isBefore(moment(), 'day');
  };

  disabledTime = (currentDate: Date) => {
    return {
      nzDisabledHours: (): number[] => this.disabledHours(currentDate),
      nzDisabledMinutes: (hour: number): number[] => this.disabledMinutes(currentDate, hour),
      nzDisabledSeconds: (): number[] => [],
    };
  };

  private disabledHours(current: Date): number[] {
    const d = moment(current);
    return d.isSameOrAfter(moment(), 'hour') ? this.range(d.get('hour')) : this.range24;
  }

  private disabledMinutes(current: Date, hour: number): number[] {
    if (hour === undefined) {
      return this.range60;
    }
    const now = moment();
    if ((moment(current).isSame(now, 'day'), now.get('hour') === hour)) {
      return this.range(now.get('minute'));
    }
    return [];
  }

  private range(to: number, from = 0): number[] {
    const r: number[] = [];
    for (let i = from; i < to; i++) {
      r.push(i);
    }
    return r;
  }
}
