import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { DispatchModalComponent } from '../../../shared-social-support/components';
import { ExperienceCandiate } from '@fi-sas/webpage/modules/social-support/models/experience-candiate.model';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { InterviewsListModalComponent } from '../interviews-list';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Resource } from '@fi-sas/core';
import { ScheduleInterviewComponent } from '../schedule-interview';
import { SendDispatchData } from '@fi-sas/webpage/modules/social-support/models';
import {
  INTEREST_STATUS_STATE_MACHINE,
  INTEREST_STATUS,
  INTEREST_STATUS_EVENT,
  ExperienceUserInterestData,
} from '@fi-sas/webpage/modules/social-support/models/interest.model';

interface ChangeStatusAction {
  label: string;
  action: (candidate: ExperienceCandiate) => Promise<any>;
  skipConfirmation?: boolean;
}

@Component({
  selector: 'app-selected-candidates',
  templateUrl: './selected-candidates.component.html',
  styleUrls: ['./selected-candidates.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedCandidatesComponent {
  private _offerId: number;
  @Input() set offerId(id: number) {
    this._offerId = id;
    this.getListCandidates();
  }

  @Output() candidatesChange = new EventEmitter();

  tableConfig = {
    pageIndex: 1,
    pageSize: 10,
    total: 0,
    showPagination: true,
    position: 'bottom',
    showSizeChanger: true,
    expandable: true,
    checkbox: true,
  };

  expandedLines: { [key: string]: boolean } = {};

  readonly INTEREST_STATUS = INTEREST_STATUS;

  private readonly actionsMapper: { [key: string]: ChangeStatusAction } = {
    ANALYSED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.ANALYZE',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.ANALYSE),
    },
    COLABORATION: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.COLABORATION',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.COLABORATION),
    },
    APPROVED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.APPROVE',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.APPROVE),
    },
    CANCELLED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.CANCEL),
    },
    CLOSED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.CLOSE),
    },
    DECLINED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.REJECT',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.DECLINE),
    },
    INTERVIEWED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.INTERVIEW',
      action: (candidate: ExperienceCandiate) => this.scheduleInterview(candidate),
      skipConfirmation: true,
    },

    WAITING: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.WAITING',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WAITING),
    },
    NOT_SELECTED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.NOT_SELECTED',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.NOTSELECT),
    },
    ACCEPTED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.ACCEPTED',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.ACCEPT),
    },
    WITHDRAWAL: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.WITHDRAWAL',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WITHDRAWAL),
    },
    WITHDRAWAL_ACCEPTED: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.ACCEPT_WITHDRAWAL',
      action: (candidate: ExperienceCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WITHDRAWALACCEPTED),
    },
    DISPATCH_ACCEPT: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.DISPATCH_ACCEPT',
      action: (candidate: ExperienceCandiate) => this.dispatchChangeStatues(candidate, true),
    },
    DISPATCH_REJECT: {
      label: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.DISPATCH_REJECT',
      action: (candidate: ExperienceCandiate) => this.dispatchChangeStatues(candidate, false),
    },
  };

  allowedActions: { [key: number]: ChangeStatusAction[] } = {};

  selectedCandidates: ExperienceCandiate[] = [];

  isLoading: boolean;

  multiActions = [];
  isAllDisplayDataChecked = false;
  isIndeterminate = false;
  mapOfCheckedId: { [key: string]: boolean } = {};

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private interestService: InterestService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  getListCandidates(resetPagination = false) {
    this.isLoading = true;
    this.changeDetectorRef.markForCheck();
    if (resetPagination) {
      this.tableConfig.pageIndex = 1;
    }
    this.interestService
      .getListCandidates(this._offerId, this.tableConfig.pageIndex, this.tableConfig.pageSize)
      .pipe(first(), finalize(() => {
        this.isLoading = false;
        this.changeDetectorRef.markForCheck();
      }))
      .subscribe(result => {
        this.selectedCandidates = result.data;
        this.tableConfig.total = result.link.total;
        //this.tableConfig.showPagination = result.data.length === this.tableConfig.pageSize;
        this.tableConfig.showSizeChanger = this.tableConfig.showPagination;
        this.allowedActions = this.getAllowedActions();
        this.changeDetectorRef.markForCheck();
      });
  }

  checkAll(value: boolean): void {
    this.selectedCandidates.forEach((item) => (this.mapOfCheckedId[item.id] = value));
    this.refreshStatus();
  }

  refreshStatus(): void {
    this.isAllDisplayDataChecked = this.selectedCandidates.every((item) => this.mapOfCheckedId[item.id]);
    this.isIndeterminate =
      !this.isAllDisplayDataChecked && this.selectedCandidates.some((item) => this.mapOfCheckedId[item.id]);
    this.multiActions = this.getCommonAllowedActions();
    this.changeDetectorRef.markForCheck();
  }

  performConfirmationAction(action: ChangeStatusAction, data: ExperienceCandiate) {
    if (action.skipConfirmation) {
      return action.action(data);
    }
    let title = '';
    if(action.label == 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL') {
      title = 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL_TEXT';
    } else if (action.label == 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED') {
      title = 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED_TEXT';
    }else{
      title = 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM';
    }
    this.modalService.confirm({
      nzTitle: this.translateService.instant(title),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () =>
        action.action(data).then(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.CHANGE_STATUS.SUCCESS')
          );
          this.getListCandidates();
          this.candidatesChange.emit(true);
        }),
    });
  }

  registerInterview(candidate: ExperienceCandiate) {
    this.modalService.create({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.BUTTON.RESGISTER_INTERVIEW'),
      nzContent: InterviewsListModalComponent,
      nzComponentParams: { interviews: candidate.interview },
      nzFooter: null,
    }).afterClose.pipe(first()).subscribe((value) => {
      if (value) {
        this.getListCandidates();
        this.candidatesChange.emit(true);
      }
    });
  }

  dispatchModal(application: ExperienceCandiate) {
    const actions = [];
    INTEREST_STATUS_STATE_MACHINE.DISPATCH.forEach((status: INTEREST_STATUS) => {
      if (this.actionsMapper[status]) {
        actions.push({
          key: ExperienceUserInterestData.eventToDispatchDecisionMapper[status],
          label: this.actionsMapper[status].label
        });
      }
    });
    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.STATE'),
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.interestService.sendDispatch(application.id, data),
        actions: actions
      },
      nzFooter: [
        { label: this.translateService.instant('MISC.CLOSE'), onClick: (_) => modalRef.close() },
        {
          label: this.translateService.instant('DASHBOARD.SAVECHANGE'),
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.onSubmit()
            .then(() => {
              this.getListCandidates();
              this.candidatesChange.emit(true);
              modalRef.close();
            }).catch(() => {});
          },
        },
      ],
    });
  }

  private getCommonAllowedActions() {
    let actions: ChangeStatusAction[] = null;
    const candidates: ExperienceCandiate[] = [];
    this.selectedCandidates.forEach((candidate) => {
      if (this.mapOfCheckedId[candidate.id]) {
        candidates.push(candidate);
        if (actions === null) {
          actions = this.allowedActions[candidate.id].filter((a) => a.label !== this.actionsMapper.INTERVIEWED.label);
        } else {
          actions = actions.filter((v) => this.allowedActions[candidate.id].some((x) => x.label === v.label));
        }
      }
    });

    return (actions || []).map((action) => {
      return {
        text: this.translateService.instant(action.label),
        onSelect: () => {
          this.modalService.confirm({
            nzTitle: this.translateService.instant(
              'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM_MULTIPLE'
            ),
            nzContent: this.translateService.instant(
              'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM_MULTIPLE_BODY',
              { n_candidates: candidates.length }
            ),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
            nzOnOk: () => {
              const promises: Promise<any>[] = [];
              candidates.forEach((candidate) => {
                promises.push(action.action(candidate));
              });
              Promise.all(promises).then(() => this.getListCandidates());
            },
          });
        },
      };
    });
  }

  private changeStatus(userInterest: ExperienceCandiate, newStatus: string) {
    this.isLoading = true;
    this.changeDetectorRef.markForCheck();
   return this.interestService.changeStatusInterest({ event: newStatus }, userInterest.id)
      .pipe(first(), finalize(() => {
        this.isLoading = false;
        this.changeDetectorRef.markForCheck();
      })).toPromise();
  }

  private dispatchChangeStatues(application: ExperienceCandiate, acceptDecision: boolean) {
    this.isLoading = true;
    this.changeDetectorRef.markForCheck();
    return this.interestService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(
        first(),
        finalize(() => {
          this.isLoading = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .toPromise();
  }

  sendDispatch(application: ExperienceCandiate, acceptDecision: boolean){
    this.dispatchChangeStatues(application, acceptDecision).then(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.CHANGE_STATUS.SUCCESS')
      );
      this.getListCandidates();
      this.candidatesChange.emit(true);
    });
  }

  private async scheduleInterview(candidate: ExperienceCandiate) {
    this.isLoading = true;
    this.changeDetectorRef.markForCheck();

    this.modalService
      .create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: ScheduleInterviewComponent,
        nzComponentParams: { candidate },
        nzFooter: null,
      })
      .afterClose.pipe(
        first(),
        finalize(() => {
          this.isLoading = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe((success: boolean) => {
        if (success) {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.SCHEDULE_INTERVIEW.SUCCESS_MESSAGE')
          );
          this.getListCandidates();
        }
      });
  }

  private getAllowedActions() {
    const allowedActions = {};
    this.selectedCandidates.forEach((candidate) => {
      const candidateActions = [];
      (INTEREST_STATUS_STATE_MACHINE[candidate.status] || []).forEach((status: INTEREST_STATUS) => {
        if (this.actionsMapper[status]) {
          candidateActions.push(this.actionsMapper[status]);
        }
      });
      allowedActions[candidate.id] = candidateActions;
    });
    return allowedActions;
  }
}
