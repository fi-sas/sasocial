import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { hasOwnProperty } from 'tslint/lib/utils';

import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { ExperienceCandiate } from '@fi-sas/webpage/modules/social-support/models/experience-candiate.model';
import { Resource } from '@fi-sas/core';
import { CertificateModel } from '../../../attendances/models';
import { ApplicationCertificateService } from '@fi-sas/webpage/modules/social-support/services/application-certificate.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { finalize, first } from 'rxjs/operators';

interface ITab {
  title: string;
  tempalteRef: TemplateRef<any>;
  collapseActive: boolean;
  show?: () => boolean;
}

@Component({
  selector: 'app-view-candidates',
  templateUrl: './view-candidates.component.html',
  styleUrls: ['./view-candidates.component.less'],
})
export class ViewCandidatesComponent implements OnInit {
  @Input() listing: ExperienceCandiate = null;
  @Input() application: SocialSupportApplicationModel = null;

  @ViewChild('tabProfile') tabProfile: TemplateRef<any>;
  @ViewChild('tabApplication') tabApplication: TemplateRef<any>;
  @ViewChild('tabProfessionalExper') tabProfessionalExper: TemplateRef<any>;
  @ViewChild('tabInterviews') tabInterviews: TemplateRef<any>;
  @ViewChild('tabCollaborationHistory') tabCollaborationHistory: TemplateRef<any>;
  @ViewChild('tabApplicationStatusChanges') tabApplicationStatusChanges: TemplateRef<any>;

  readonly scheduleRows = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.MORNING',
      keys: [4, 7, 10, 13, 16, 19, 1],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.AFTERNOON',
      keys: [5, 8, 11, 14, 17, 20, 2],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.NIGHT',
      keys: [6, 9, 12, 15, 18, 21, 3],
    },
  ];

  readonly applicationStatusLabelMapper = {
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    ACKNOWLEDGED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACKNOWLEDGED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    CLOSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CLOSED',
    COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.COLABORATION',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    EXPIRED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXPIRED',
    EXTERNAL_SYSTEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXTERNAL_SYSTEM',
    IN_COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.IN_COLABORATION',
    INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
    NOT_SELECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.NOT_SELECTED',
    PUBLISHED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.PUBLISHED',
    REJECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.REJECTED',
    RETURNED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.RETURNED',
    SELECTION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SELECTION',
    SEND_SEEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SEND_SEEM',
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    WAITING: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WAITING',
    WITHDRAWAL: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WITHDRAWAL',
  };

  tabs: ITab[] = [];
  selectedTabIndex: number = 0;

  loadingCertificateButton: boolean;

  constructor(
    private router: Router,
    private applicationCertificateService: ApplicationCertificateService,
    private uiService: UiService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.application.preferred_schedule = (this.application.preferred_schedule || []).map((schedule) =>
      hasOwnProperty(schedule, 'id') ? schedule.schedule.id : schedule
    );
    this.tabs = this.getTabs();
  }

  onTabIndexChange(selectedIndex: number) {
    this.selectedTabIndex = Math.min(selectedIndex, (this.tabs.length || 1) - 1);
  }

  goReviewApplication(id: number) {
    this.router.navigateByUrl(`social-support/applications/review/${id}`);
  }

  getExperienceCertificate(user_interest_id: number) {
    this.loadingCertificateButton = true;
    this.applicationCertificateService.generateCertificate(user_interest_id)
      .pipe(first(), finalize(() => this.loadingCertificateButton = false))
      .subscribe(result => {
        if(result.data.length && result.data[0].url){
          window.open(result.data[0].url, '_blank');
        }else{
          this.uiService.showMessage(
            MessageType.info,
            this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.NO_COLAB_CERTIFICATE')
          );
        }
      });
  }

  private getTabs(): ITab[] {
    return [
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.PROFILE',
        tempalteRef: this.tabProfile,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.APPLICATION',
        tempalteRef: this.tabApplication,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.PROFESSIONAL_EXPER',
        tempalteRef: this.tabProfessionalExper,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.INTERVIEWS',
        tempalteRef: this.tabInterviews,
        collapseActive: false,
        show: () => (this.listing.interview || []).length > 0,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.HISTORY_COLLABORATION',
        tempalteRef: this.tabCollaborationHistory,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.APPLICATION_STATUS_CHANGES',
        tempalteRef: this.tabApplicationStatusChanges,
        collapseActive: false,
        show: () => true,
      },
    ];
  }
}
