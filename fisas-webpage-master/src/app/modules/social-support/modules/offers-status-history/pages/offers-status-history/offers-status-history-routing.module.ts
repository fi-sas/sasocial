import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OffersStatusHistoryComponent } from './offers-status-history.component';

const routes: Routes = [
  {
    path: '',
    component: OffersStatusHistoryComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:experiences:my_offers' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OffersStatusHistorySocialSupportRoutingModule {}
