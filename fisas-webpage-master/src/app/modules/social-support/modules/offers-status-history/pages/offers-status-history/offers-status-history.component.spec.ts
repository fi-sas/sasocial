import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OffersStatusHistoryComponent } from './offers-status-history.component';

describe('OffersStatusHistoryComponent', () => {
  let component: OffersStatusHistoryComponent;
  let fixture: ComponentFixture<OffersStatusHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OffersStatusHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersStatusHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
