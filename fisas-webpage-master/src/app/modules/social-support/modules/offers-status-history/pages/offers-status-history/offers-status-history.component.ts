import { Component } from '@angular/core';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { OffersService } from '@fi-sas/webpage/modules/social-support/services/offers.services';
import { Resource } from '@fi-sas/core';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-offers-status-history',
  templateUrl: './offers-status-history.component.html',
  styleUrls: ['./offers-status-history.component.less'],
})
export class OffersStatusHistoryComponent {
  private _isAdvisor = true;
  private _isResponsible = false;

  set isAdvisor(isAdvisor: boolean) {
    if (isAdvisor && !this._isAdvisor) {
      this.getAdvisorData();
    }

    this._isAdvisor = isAdvisor;
    this._isResponsible = !isAdvisor;
  }

  get isAdvisor(): boolean {
    return this._isAdvisor;
  }

  set isResponsible(isResponsible: boolean) {
    if (isResponsible && !this._isResponsible) {
      this.getResponsibleData();
    }

    this._isAdvisor = !isResponsible;
    this._isResponsible = isResponsible;
  }

  get isResponsible(): boolean {
    return this._isResponsible;
  }

  offers: ExperienceModel[] = [];
  offersHistory: ExperienceModel[] = [];

  isLoading: boolean;
  isHistoryLoading: boolean;

  panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.OFFERS.HISTORY',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  offerToManage: number;

  translations: TranslationModel[] = [];

  private readonly offerStatusWeight = {
    RETURNED: 0,
    SEND_SEEM: 1,
    EXTERNAL_SYSTEM: 2,
    IN_COLABORATION: 3,
    SELECTION: 4,
    DISPATCH: 5,
    PUBLISHED: 6,
    APPROVED: 7,
    ANALYSED: 8,
    SUBMITTED: 9,
    REJECTED: 10,
    CLOSED: 11,
    CANCELLED: 12,
  };

  constructor(
    private authService: AuthService,
    private offersService: OffersService,
    private configurationsService: ConfigurationsService
  ) {
    this.translations = this.getTranslations();

    if (this.authService.hasPermission('social_scholarship:experiences:responsable')) {
      this.isResponsible = true;
    } else {
      this.isAdvisor = true;
    }
  }

  onManage(id: number) {
    this.offerToManage = id;
  }

  onCancel() {
    this.getResponsibleData();
  }

  onCandidatesChange() {
    if (this.isResponsible) {
      return this.getResponsibleData();
    }

    return this.getAdvisorData();
  }

  private getAdvisorData() {
    this.getAdvisorOffers();
    this.getAdvisorOffersHistory();
  }

  private getResponsibleData() {
    this.getResponsibleOffers();
    this.getResponsibleOffersHistory();
  }

  private getAdvisorOffers(){
    this.isLoading = true;
    this.offersService.listOffersByAdvisor()
      .pipe(first(), finalize(() =>  this.isLoading = false))
      .subscribe(response => {
        if(response.data.length){
          this.offers = response.data.sort(this.offerSortFn.bind(this))
        }
      });
  }

  private getAdvisorOffersHistory() {
    this.isHistoryLoading = true;
    this.offersService.listOffersByAdvisorClosedCancelled()
      .pipe(first(), finalize(() => this.isHistoryLoading = false))
      .subscribe(response => {
        if(response.data.length){
          this.offersHistory = response.data.sort(this.offerSortFn.bind(this))
        }
      });
  }

  private getResponsibleOffers() {
    this.isLoading = true;
    this.offersService.listOffersByResponsible().pipe(first())
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(response => {
        if(response.data.length){
          this.offers = response.data.sort(this.offerSortFn.bind(this))
        }
      });
  }

  private getResponsibleOffersHistory() {
    this.isHistoryLoading = true;
    this.offersService.listOffersByResponsibleClosedCancelled().pipe(first())
    .pipe(first(), finalize(() => this.isHistoryLoading = false))
    .subscribe(response => {
      if(response.data.length){
        this.offersHistory = response.data.sort(this.offerSortFn.bind(this))
      }
    });
  }

  private getTranslations(): TranslationModel[] {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    return service.translations;
  }

  private offerSortFn(a: ExperienceModel, b: ExperienceModel): number {
    const aWeight = this.normalizeWeight(this.offerStatusWeight[a.status]);
    const bWeight = this.normalizeWeight(this.offerStatusWeight[b.status]);
    return aWeight - bWeight;
  }

  private normalizeWeight(weight: number): number {
    if (weight == null || isNaN(weight)) {
      return Number.MAX_VALUE;
    }

    return weight;
  }
}
