import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersStatusHistoryComponent } from './offers-status-history.component';
import { OffersStatusHistorySocialSupportRoutingModule } from './offers-status-history-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';
import {
  InterviewsListComponent,
  InterviewsListModalComponent,
  RegisterInterviewComponent,
  ScheduleInterviewComponent,
  SelectedCandidatesComponent,
  ViewCandidatesComponent,
} from '../../components';

@NgModule({
  declarations: [
    InterviewsListComponent,
    InterviewsListModalComponent,
    OffersStatusHistoryComponent,
    RegisterInterviewComponent,
    ScheduleInterviewComponent,
    SelectedCandidatesComponent,
    ViewCandidatesComponent,
  ],
  imports: [CommonModule, OffersStatusHistorySocialSupportRoutingModule, SharedModule, SharedSocialSupportModule],
  entryComponents: [ScheduleInterviewComponent, InterviewsListModalComponent],
})
export class OffersStatusHistorySocialSupportModule {}
