import { AsyncValidatorFn, FormGroup, ValidatorFn } from '@angular/forms';

import { ExperienceModel } from '../../../models/experiences.model';
import { OfferFormData, OfferSubmitData } from './offer-data.model';

export interface ISelectOption<T = any> {
  label: string;
  value: T;
}

export interface IFields {
  name: string;
  required?: boolean;
  disabled?: boolean;
  value?: any;
  translatable?: boolean;
  validators?: ValidatorFn[];
  asyncValidators?: AsyncValidatorFn[];
  toExternalValue?: (formData: Partial<OfferFormData>, currentData: OfferSubmitData) => void;
  toInternalValue?: (offer: ExperienceModel, form: FormGroup) => void;
}
