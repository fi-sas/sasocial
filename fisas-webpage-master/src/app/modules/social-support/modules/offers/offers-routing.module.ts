import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfferFormComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: OfferFormComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:experiences:responsable' },
  },
  {
    path: ':id',
    component: OfferFormComponent,
    data: { breadcrumb: null, title: null, scope: 'social_scholarship:experiences:responsable' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OffersRoutingModule {}
