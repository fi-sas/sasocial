import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferFormComponent } from './pages';
import { OffersRoutingModule } from './offers-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [OfferFormComponent],
  imports: [CommonModule, SharedModule, OffersRoutingModule],
})
export class OffersModule {}
