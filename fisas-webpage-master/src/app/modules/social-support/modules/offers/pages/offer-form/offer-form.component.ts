import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AsyncValidatorFn, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { debounceTime, finalize, first, map, takeUntil } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AcademicYear } from '@fi-sas/webpage/shared/models';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { emailExistsValidator } from '@fi-sas/webpage/shared/validators/async-validators.validator';
import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { ISelectOption, IFields, OfferSubmitData } from '../../models';
import { LanguageModel } from '@fi-sas/webpage/modules/private-accommodation/models/language.model';
import { LanguagesService } from '@fi-sas/webpage/modules/private-accommodation/services/languages.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { OffersService } from '@fi-sas/webpage/modules/social-support/services/offers.services';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { Resource } from '@fi-sas/core';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { UsersService } from '@fi-sas/webpage/shared/services';
import { SocialSupportService } from '@fi-sas/webpage/modules/social-support/services/social-support.service';

@Component({
  selector: 'app-offer-form',
  templateUrl: './offer-form.component.html',
  styleUrls: ['./offer-form.component.less'],
})
export class OfferFormComponent extends FormHandler implements OnInit {
  readonly buttonBackStyle = {
    width: 'auto',
    height: '30px',
    'font-size': '15px',
    'margin-top': '33px',
    'margin-bottom': '30px',
  };
  errorDates = false;
  readonly buttonStyle = {
    width: '100%',
    height: '50px',
    overflow: 'hidden',
    'text-overflow': 'ellipsis',
    'font-size': '17.5px',
    'margin-bottom': '20px',
    'margin-top': '20px',
    'padding-left': '40px',
    'padding-right': '40px',
  };
  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];

  readonly fields: IFields[] = [
    { name: 'academic_year', required: true, value: '', disabled: true },
    {
      translatable: true,
      name: 'title',
      required: true,
      validators: [trimValidation],
      toInternalValue: (offer, form) => {
        offer.translations.forEach((t) => {
          if (this.translateService.langs.includes(t.language.acronym)) {
            form.get(`translations.${t.language.acronym}.title`).setValue(t.title);
          }
        });
      },
      toExternalValue: (formData, currentData) => {
        this.translateService.langs.forEach((lang) => {
          currentData.translations[lang].title = formData.translations[lang].title;
        });
      },
    },
    { name: 'organic_unit_id', required: true },
    { name: 'address', required: true, validators: [trimValidation] },
    {
      name: 'initial_time',
      required: true,
      toInternalValue: (offer, form) =>
        form.get('initial_time').setValue(offer.start_date),
      toExternalValue: (formData, currentData) => {
        currentData.start_date = formData.initial_time;
      },
    },
    {
      name: 'final_time',
      required: true,
      toInternalValue: (offer, form) =>
        form.get('final_time').setValue(offer.end_date),
      toExternalValue: (formData, currentData) => {
        currentData.end_date = formData.final_time;
      },
    },
    { name: 'number_candidates', validators: [Validators.min(0), Validators.max(10000)] },
    { name: 'number_simultaneous_candidates', validators: [Validators.min(0), Validators.max(10000)] },
    { name: 'proponent_service', required: true, translatable: true, validators: [trimValidation] },
    { name: 'number_weekly_hours', required: true, validators: [Validators.min(0), Validators.max(168)] },
    { name: 'total_hours_estimation', required: true, validators: [Validators.min(0)] },
    { name: 'schedule', required: true },
    { name: 'holydays_availability', value: null, required: true },
    {
      name: 'perc_student_ca',
      disabled: true,
      toInternalValue: (offer, form) => form.get('perc_student_ca').setValue((offer.perc_student_ca || 0) * 100),
      toExternalValue: (formData, currentData) => (currentData.perc_student_ca = (formData.perc_student_ca || 0) / 100),
    },
    /*{
      name: 'perc_student_iban',
      required: true,
      disabled: true,
      validators: [Validators.min(0), Validators.max(100)],
      toInternalValue: (offer, form) => form.get('perc_student_iban').setValue(offer.perc_student_iban * 100),
      toExternalValue: (formData, currentData) => (currentData.perc_student_iban = (formData.perc_student_iban || 100) / 100),
    },
    { name: 'payment_value', required: true, validators: [Validators.min(0)] },
    { name: 'payment_model', required: true },*/
    { name: 'job', required: true, translatable: true, validators: [trimValidation] },
    {
      name: 'email_search_advisor',
      required: true,
      validators: [Validators.email],
    },
    { name: 'description', required: true, translatable: true, validators: [trimValidation] },
    {
      name: 'publish_date',
      value: new Date(),
      required: true,
      toExternalValue: (formData, currentData) =>
        (currentData.publish_date = moment(formData.publish_date).toISOString()),
    },
    {
      name: 'application_deadline_date',
      required: true,
      toExternalValue: (formData, currentData) =>
        (currentData.application_deadline_date = moment(formData.application_deadline_date).toISOString()),
    },
    { name: 'selection_criteria', required: true, translatable: true, validators: [trimValidation] },
    { name: 'applicant_profile', required: true, translatable: true, validators: [trimValidation] },
    { name: 'attachment_file_id' },
  ];

  offer: ExperienceModel;
  form: FormGroup;
  isLoading: boolean;
  isLoadingOffer: boolean;
  languages: LanguageModel[] = [];
  showErrorMessage: boolean;
  languages$: ReplaySubject<LanguageModel[]> = new ReplaySubject<LanguageModel[]>();

  organicUnitsLoading = false;
  organicUnits: OrganicUnitModel[] = [];

  scheduleHoursForm = new FormGroup({
    begin: new FormControl(null, Validators.required),
    end: new FormControl(null, Validators.required),
  });

  canUseCurrentAccount: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private configurationsService: ConfigurationsService,
    private languagesService: LanguagesService,
    private modalService: NzModalService,
    private offersService: OffersService,
    private organicUnitsService: OrganicUnitsService,
    private router: Router,
    private socialSupportService: SocialSupportService,
    private translateService: TranslateService,
    private uiService: UiService,
  ) {
    super();
    this.getLangages();
  }

  ngOnInit() {
    this.getOrganicUnitOptions();

    this.form = this.getOfferForm();

    //this.handleNumericValueDependency('perc_student_iban', 'perc_student_ca', 0, 100);
    //this.handleNumericValueDependency('perc_student_ca', 'perc_student_iban', 0, 100);

    this.languagesService
      .listLanguages()
      .pipe(takeUntil(this.destroy$))
      .subscribe((langs) => (this.languages = langs.data));

    if (isNaN(this.activatedRoute.snapshot.params.id)) {

      this.configurationsService.getCurrentAcademicYear().pipe(first())
      .subscribe(res => {
        this.form.get('academic_year').setValue(res.data[0].academic_year);
      }, () => {
        this.form.get('academic_year').enable();
      })


      /*this.getCanUseCurrentAccountConfig().subscribe((canUse) => {
        this.canUseCurrentAccount = canUse;
        if (!canUse) {
          this.form.get('perc_student_iban').setValue(100);
          this.form.get('perc_student_iban').disable();
          return;
        }

        this.form.get('perc_student_ca').setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
        this.form.get('perc_student_iban').enable();
        this.form.get('perc_student_ca').enable();
      });*/
    } else {
      this.getOffer(this.activatedRoute.snapshot.params.id);
    }
  }

  onSubmit() {
    this.errorDates = false;
    if (this.form.get('initial_time').value && this.form.get('final_time').value) {
      if (moment(this.form.get('initial_time').value).isAfter(this.form.get('final_time').value)) {
        this.errorDates = true;
      }
    }

    if (!this.isFormValid(this.form) || this.errorDates) {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
      return;
    }

    this.showErrorMessage = false;
    if (this.offer) {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MISC.CHANGE_STATUS'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOnOk: () => this.submit()
      });
    } else {
      this.submit();
    }
  }

  submit(){
    this.isLoading = true;
    this.getSubmitData().subscribe(
      (data: OfferSubmitData) => {
        let action = this.offersService.create(data);
        let successMessage = 'SOCIAL_SUPPORT.OFFERS.FORM.CREATE_SUCCESS';
        if (this.offer) {
          action = this.offersService.edit(this.offer.id, data);
          successMessage = 'SOCIAL_SUPPORT.OFFERS.FORM.EDIT_SUCCESS';
        }

        action
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant(successMessage));
            this.goBack();
          });
      },
      () => {
        this.isLoading = false;
        this.uiService.showMessage(
          MessageType.error,
          this.translateService.instant('VOLUNTEERING.ACTIONS.FORM.LANG_ERROR')
        );
      }
    );
  }

  disableDate = (currentDate: Date): boolean => {
    return moment(currentDate).isBefore(moment(), 'day');
  };

  goBack() {
    this.router.navigateByUrl('social-support/offers-status-history');
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    if (this.form.get(inputName).errors.email) {
      return this.translateService.instant('MISC.EMAIL');
    }
    if (this.form.get(inputName).errors.min) {
      return this.translateService.instant('MISC.MIN_NUMBER', { number: this.form.get(inputName).errors.min.min || 0 })
    }
    if (this.form.get(inputName).errors.max) {
      return this.translateService.instant('MISC.MAX_NUMBER', { number: this.form.get(inputName).errors.max.max || 0 })
    }
    if (this.form.get(inputName).errors.emailDoesNotExist) {
      return this.translateService.instant('MISC.EMAIL_DOES_NOT_EXIST');
    }
    return null;
  }

  copyField(fieldName: string, langAcronym: string) {
    const fromFormControl = this.form.get(`translations.pt.${fieldName}`);
    const toFormControl = this.form.get(`translations.${langAcronym}.${fieldName}`);
    toFormControl.setValue(fromFormControl.value);
    toFormControl.markAsTouched();
    toFormControl.markAsDirty();
  }

  private getSubmitData(): Observable<OfferSubmitData> {
    return this.languages$.pipe(
      first(),
      map((languages: LanguageModel[]) => {
        let data = { translations: {} };
        languages.forEach((lang) => (data.translations[lang.acronym] = {}));
        const formValue = this.form.getRawValue();
        this.fields.forEach((field) => {
          if (field.translatable) {
            languages.forEach((lang) => {
              data.translations[lang.acronym][field.name] = this.form.value.translations[lang.acronym][field.name];
            });
          } else if (field.toExternalValue) {
            field.toExternalValue(this.form.value, data as OfferSubmitData);
          } else {
            data[field.name] = formValue[field.name];
          }
        });
        const translations = [];
        languages.forEach((lang) => translations.push({ ...data.translations[lang.acronym], language_id: lang.id }));
        data.translations = translations;
        return data as OfferSubmitData;
      })
    );
  }

  private getOfferForm(): FormGroup {
    const fg = {};
    const fgTranslations = {};
    this.translateService.langs.forEach((lang) => (fgTranslations[lang] = {}));
    this.fields.forEach((field) => {
      if (field.translatable) {
        this.translateService.langs.forEach(
          (lang) => (fgTranslations[lang][field.name] = this.getOfferFormControl(field))
        );
      } else {
        fg[field.name] = this.getOfferFormControl(field);
      }
    });
    this.translateService.langs.forEach((lang) => (fgTranslations[lang] = new FormGroup(fgTranslations[lang])));
    fg['translations'] = new FormGroup(fgTranslations);
    return new FormGroup(fg);
  }

  private getOfferFormControl(field: IFields): FormControl {
    const validators: ValidatorFn[] = [];
    const asyncValidators: AsyncValidatorFn[] = [];
    if (field.required) {
      validators.push(Validators.required);
    }
    (field.validators || []).forEach((validator) => validators.push(validator));
    (field.asyncValidators || []).forEach((validator) => asyncValidators.push(validator));
    return new FormControl({ value: field.value, disabled: field.disabled || false }, validators, asyncValidators);
  }

  private getOffer(id: number) {
    this.isLoadingOffer = true;
    this.offersService.getOffer(id)
      .pipe(first(), finalize(() => this.isLoadingOffer = false))
      .subscribe(result => {
        if(result.data.length){
          this.offer = result.data[0];
          this.fillOfferForm(this.offer);
        }
      });
  }

  private fillOfferForm(offer: ExperienceModel) {
    this.fields.forEach((field) => {
      if (field.translatable) {
        offer.translations.forEach((t) => {
          if (this.translateService.langs.includes(t.language.acronym)) {
            this.form.get(`translations.${t.language.acronym}.${field.name}`).setValue(t[field.name] || null);
          }
        });
      } else if (field.toInternalValue) {
        field.toInternalValue(offer, this.form);
      } else {
        this.form.get(field.name).setValue(offer[field.name]);
        if (field.name === 'email_search_advisor' && offer.experience_advisor_id !== null) {
          this.form.get(field.name).disable();
        }
      }
    });
  }

  getOrganicUnitOptions() {
    this.organicUnitsLoading = true;
    return this.organicUnitsService.organicUnits().pipe(
      first(),
      map((res) => res.data ),
      finalize(() => this.organicUnitsLoading = false)).subscribe((units => {
        this.organicUnits = units;
    }));
  }

  private handleNumericValueDependency(fromFieldName: string, toFieldName: string, from: number, to: number) {
    this.form
      .get(fromFieldName)
      .valueChanges.pipe(takeUntil(this.destroy$), debounceTime(100))
      .subscribe((value) => {
        if (!isNaN(value) && value >= from && value <= to) {
          this.form.get(toFieldName).setValue(to - value, { emitEvent: false });
        } else {
          this.form.get(fromFieldName).setValue(value < 0 ? 0 : 100);
        }
      });
  }

  private getLangages() {
    this.languagesService
      .listLanguages()
      .pipe(first())
      .subscribe((result) => this.languages$.next(result.data));
  }

  private getCanUseCurrentAccountConfig(): Observable<boolean> {
    return this.socialSupportService
      .getConfigs()
      .pipe(
        first(),
        map((result) => {
          const config = result.data.find((conf) => conf.key === 'HAS_CURRENT_ACCOUNT');
          if (!config) {
            return false;
          }
          return [true, 'true'].includes(config.value);
        })
      )
  }
}
