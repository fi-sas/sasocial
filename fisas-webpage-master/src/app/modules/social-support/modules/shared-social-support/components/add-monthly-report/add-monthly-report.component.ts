import { Component, Input } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { GeneralReportsApplicationService } from '../../../attendances/services/general-reports-application.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-add-monthly-report',
  templateUrl: './add-monthly-report.component.html',
  styleUrls: ['./add-monthly-report.component.less']
})
export class AddMonthlyReportComponent extends FormHandler {
  private _collaborationId: number;
  @Input() set collaborationId(id: number) {
    this._collaborationId = id;

    this.form = this.getForm();
  }

  get collaborationId() {
    return this._collaborationId;
  }

  readonly filterTypes = ['application/pdf'];

  form: FormGroup;

  isLoading: boolean;

  constructor(
    private generalReportsApplicationService: GeneralReportsApplicationService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,
    private uiService: UiService,
  ) {
    super();
  }

  checkValidityOfControls(controls: AbstractControl[]): boolean {
    for (const i in controls) {
      if (i) {
        controls[i].markAsDirty();
        controls[i].updateValueAndValidity();
      }
    }

    return !controls.find(c => c.status !== 'VALID');
  }

  onSubmit() {
    this.checkValidityOfControls([this.form.get('file'),this.form.get('report'),this.form.get('date')]);
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      const date = moment(this.form.value.date);
      const data = {
        user_interest_id: this.collaborationId,
        month: date.get('month') + 1,
        year: date.get('year'),
        report: this.form.value.report,
        file_id: this.form.value.file
      }
      this.generalReportsApplicationService.addMonthlyReports(data)
        .pipe(first(), finalize(() => this.isLoading = false))
        .subscribe(() => {
          this.modalRef.close(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.TABS.MONTHLY_REPORT.FORM.SUCCESS')
          );
        });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  onChangeRequired(){
    let isEmpty: boolean = false;    
    if(!this.form.value.file && !this.form.value.report){
      isEmpty = true;
    }

    if(!isEmpty){
      this.form.get("report").setValidators(null);
      this.form.get("file").setValidators(null);
    }else{
      this.form.get("report").setValidators([Validators.required,trimValidation]);
      this.form.get("file").setValidators([Validators.required]);
    }
  }

  private getForm() {
    return new FormGroup({
      file: new FormControl(null, Validators.required),
      report: new FormControl(null, [Validators.required,trimValidation]),
      date: new FormControl(null, Validators.required),
    });
  }
}
