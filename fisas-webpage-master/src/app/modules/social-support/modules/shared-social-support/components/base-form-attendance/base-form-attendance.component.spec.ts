import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFormAttendanceComponent } from './base-form-attendance.component';

describe('BaseFormAttendanceComponent', () => {
  let component: BaseFormAttendanceComponent;
  let fixture: ComponentFixture<BaseFormAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BaseFormAttendanceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFormAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
