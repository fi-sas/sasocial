import { Component, Input } from '@angular/core';
import { Resource } from '@fi-sas/core';

import { first } from 'rxjs/operators';
import * as moment from 'moment';

import { AttendanceModel } from '@fi-sas/webpage/modules/social-support/modules/attendances/models';
import { AttendancesService } from '@fi-sas/webpage/modules/social-support/modules/attendances/services/attendances.service';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { InterestModel } from '@fi-sas/webpage/modules/social-support/models/interest.model';

interface IAttendanceDate {
  start: moment.Moment;
  end: moment.Moment;
}

@Component({
  selector: 'app-base-form-attendance',
  template: '',
})
export class BaseFormAttendanceComponent extends FormHandler {
  _collaboration: InterestModel;
  @Input() set collaboration(collaboration: InterestModel) {
    if (collaboration) {
      this._collaboration = collaboration;
      this.experienceStartDate = moment(this._collaboration.experience.start_date);
      this.experienceEndDate = moment(this._collaboration.experience.end_date);
      this.getAttendances(collaboration.id);
    }
  }

  protected readonly range24 = this.range(24);
  protected readonly range60 = this.range(60);

  experienceStartDate: moment.Moment;
  experienceEndDate: moment.Moment;

  dates: IAttendanceDate[] = [];
  attendances: AttendanceModel[] = [];

  constructor(protected attendancesService: AttendancesService) {
    super();
  }

  disableDate = (currentDate: Date): boolean => {
    return this.overlapsCollaborationAttendances(moment(currentDate));
  };

  protected overlapsCollaborationAttendances(
    date: moment.Moment,
    granularity: moment.unitOfTime.StartOf = 'day'
  ): boolean {
    if (!date.isBetween(this.experienceStartDate, this.experienceEndDate, granularity, '[]')) {
      return true;
    }
    if (this.attendances.length === 0) {
      return false;
    }
    return this.dates.some((d: IAttendanceDate) => {
      if (date.isSame(d.start, granularity) || date.isSame(d.end, granularity)) {
        return false;
      }
      return date.isBetween(d.start, d.end, granularity);
    });
  }

  protected getAttendances(collaborationId: number): void {
    this.attendancesService
      .getAttendance(collaborationId)
      .pipe(first())
      .subscribe((attendances: Resource<AttendanceModel>) => {
        this.attendances = attendances.data;
        this.attendances.forEach((attendance: AttendanceModel) =>
          this.dates.push({ start: moment(attendance.initial_time), end: moment(attendance.final_time) })
        );
      });
  }

  /**
   * [from, to[
   *
   * @protected
   * @param {number} to
   * @param {number} [from=0]
   * @param {number} [step=1]
   * @return {*}  {number[]}
   * @memberof BaseFormAttendanceComponent
   */
  protected range(to: number, from: number = 0, step: number = 1): number[] {
    const r: number[] = [];
    for (let i = from; i < to; i += step) {
      r.push(i);
    }
    return r;
  }
}
