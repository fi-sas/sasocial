import { Component, Input, Type } from '@angular/core';

import { ModalOptionsForService, NzModalService } from 'ng-zorro-antd';

import { ExperienceEvaluationComponent } from '../experience-evaluation';
import { FormAbsencesComponent } from '../form-absences/form-absences.component';
import { FormAttendanceComponent } from '../form-attendance/form-attendance.component';
import { FormWithdrawalComponent } from '../../../shared-social-support/components/form-withdrawal/form-withdrawal.component';
import { InterestModel, INTEREST_STATUS } from '@fi-sas/webpage/modules/social-support/models/interest.model';

@Component({
  selector: 'app-buttons-panel',
  templateUrl: './buttons-panel.component.html',
  styleUrls: ['./buttons-panel.component.less'],
})
export class ButtonsPanelComponent {
  buttonStyle = {
    width: '100%',
    'text-overflow': 'ellipsis',
    'white-space': 'nowrap',
    overflow: 'hidden',
  };

  buttonStyle2 = {
    width: '100%',
    'text-overflow': 'ellipsis',
    'white-space': 'nowrap',
    overflow: 'hidden',
  };

  private _collaboration: InterestModel;
  @Input() set collaboration(collaboration: InterestModel) {
    if (collaboration) {
      this._collaboration = collaboration;

      this.disableActiveCollaborationActions = [
        INTEREST_STATUS.CLOSED.toString(),
        INTEREST_STATUS.CANCELLED.toString(),
        INTEREST_STATUS.WITHDRAWAL_ACCEPTED.toString(),
      ].includes(collaboration.status);

      this.collaborationStillGoing = [
        INTEREST_STATUS.COLABORATION.toString(),
        INTEREST_STATUS.WITHDRAWAL.toString(),
      ].includes(collaboration.status);

      this.disableWithdrawal = collaboration.status === INTEREST_STATUS.WITHDRAWAL.toString();
    }
  }

  get collaboration() {
    return this._collaboration;
  }

  loadingCertificateButton: boolean;

  disableActiveCollaborationActions: boolean;
  disableWithdrawal: boolean;
  collaborationStillGoing: boolean;

  constructor(private modalService: NzModalService) { }

  recordAttendance() {
    this.modalService.create(
      this.getModalOptions<FormAttendanceComponent>(
        FormAttendanceComponent,
        { collaboration: this.collaboration },
        { nzWidth: 750 }
      )
    );
  }

  absence() {
    this.modalService.create(
      this.getModalOptions<FormAbsencesComponent>(FormAbsencesComponent, { collaboration: this.collaboration })
    );
  }

  withdrawal() {
    this.modalService.create(
      this.getModalOptions<FormWithdrawalComponent>(FormWithdrawalComponent, {
        applicationID: this.collaboration.id,
        action: 'WITHDRAWAL',
      })
    );
  }

  experimentReport() {
    this.modalService.create(
      this.getModalOptions<ExperienceEvaluationComponent>(
        ExperienceEvaluationComponent,
        { collaboration: this.collaboration },
        { nzWidth: 700 }
      )
    );
  }

  getCertificate() {
    if (this.collaboration.certificate_file) {
      window.open(this.collaboration.certificate_file.url, '_blank');
    } else if (this._collaboration.certificate_generated && this._collaboration.certificate_generated.file) {
      window.open(this._collaboration.certificate_generated.file.url, '_blank');
    }
  }

  private getModalOptions<T>(
    component: Type<T>,
    params: Partial<T>,
    override: ModalOptionsForService<T> = {}
  ): ModalOptionsForService<T> {
    return Object.assign(
      {
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: component,
        nzComponentParams: params,
        nzFooter: null,
      },
      override
    );
  }
}
