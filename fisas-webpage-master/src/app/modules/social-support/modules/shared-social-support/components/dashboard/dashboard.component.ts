import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';

import { first } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';

import { AttendancesService } from '../../../attendances/services/attendances.service';
import { ChangeComponentService } from '@fi-sas/webpage/shared/components/bar-menu/services/change-component.service';
import { GeneralReportModel } from '@fi-sas/webpage/modules/social-support/modules/attendances/models/';
import { GeneralReportsApplicationService } from '@fi-sas/webpage/modules/social-support/modules/attendances/services/general-reports-application.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit, OnChanges, OnDestroy {
  @Input() applicationID = 0;
  loadingReports = false;

  generalReports: GeneralReportModel = null;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(
    private attendancesService: AttendancesService,
    private changeComponentService: ChangeComponentService,
    private generalReportsApplicationService: GeneralReportsApplicationService,
  ) {
    this.subscriptions = this.changeComponentService.currentIndexObservable().subscribe((index: number) => {
      if (index === 2) {
        this.getGeneralReports();
      }
    });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnChanges() {
    this.getGeneralReports();
  }

  ngOnInit() {
    this.subscriptions = merge(
      this.attendancesService.absencesObservable(),
      this.attendancesService.absencesReasonObservable(),
      this.attendancesService.attendanceObservable(),
    ).subscribe(() => this.getGeneralReports());
  }

  getGeneralReports() {
    this.loadingReports = true;
    this.generalReportsApplicationService
      .generalReports(this.applicationID)
      .pipe(first())
      .subscribe(
        (generalReports) => {
          this.generalReports = generalReports.data[0];
          this.loadingReports = false;
        },
        () => {
          this.loadingReports = false;
        }
      );
  }
}
