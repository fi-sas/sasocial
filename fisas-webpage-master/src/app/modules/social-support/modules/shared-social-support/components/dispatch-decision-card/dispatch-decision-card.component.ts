import { Component, EventEmitter, Input, Output } from '@angular/core';

import { DISPATCH_ACTIONS } from '@fi-sas/webpage/modules/social-support/models';

interface IAction {
  fn: () => void,
  label: string,
  permission?: string;
  title: string;
  btnType: 'danger' | 'primary';
  cancelText: string;
  okText: string;
}

@Component({
  selector: 'fi-sas-dispatch-decision-card',
  templateUrl: './dispatch-decision-card.component.html',
  styleUrls: ['./dispatch-decision-card.component.less']
})
export class DispatchDecisionCardComponent {
  private _decision: string;
  @Input() set decision(decision: string) {
    this._decision = decision;
  }
  get decision() {
    return DISPATCH_ACTIONS[this._decision] || 'SOCIAL_SUPPORT.DISPATCH.NO_INFORMATION';
  }

  @Output() onReject = new EventEmitter<boolean>();
  @Output() onApprove = new EventEmitter<boolean>();

  readonly actions: IAction[] = [
    {
      fn: () => this.reject(),
      label: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.ACCEPT.LABEL',
      permission: '',
      title: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.ACCEPT.TITLE',
      btnType: 'danger',
      cancelText: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.ACCEPT.CANCEL',
      okText: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.ACCEPT.OK',
    },
    {
      fn: () => this.approve(),
      label: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.REFUSE.LABEL',
      title: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.REFUSE.TITLE',
      btnType: 'primary',
      cancelText: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.REFUSE.CANCEL',
      okText: 'SOCIAL_SUPPORT.DISPATCH.ACTIONS.REFUSE.OK',
    }
  ];

  constructor() { }

  reject() {
    this.onReject.emit(true);
  }

  approve() {
    this.onApprove.emit(true);
  }
}
