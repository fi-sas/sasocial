import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs';

import { Resource } from '@fi-sas/core';
import { SendDispatchData } from '@fi-sas/webpage/modules/social-support/models';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fi-sas-dispatch-modal',
  templateUrl: './dispatch-modal.component.html',
  styleUrls: ['./dispatch-modal.component.less'],
})
export class DispatchModalComponent {
  @Input() serviceFn: (data: SendDispatchData) => Observable<Resource<unknown>>;
  @Input() actions: { key: string; label: string; }[];

  form: FormGroup;

  constructor() {
    this.form = new FormGroup({
      decision: new FormControl(null, Validators.required),
    });
  }

  onSubmit() {
    if (!this.isFormValid()) {
      return Promise.reject();
    }

    return this.serviceFn({ event: 'DISPATCH', decision: this.form.get('decision').value })
    .pipe(first())
    .toPromise();
  }

  private isFormValid() {
    for (const controlName in this.form.controls) {
      if (this.form.controls[controlName]) {
        this.form.controls[controlName].markAsDirty();
        this.form.controls[controlName].updateValueAndValidity();
      }
    }

    return this.form.valid;
  }
}
