import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ChangeStatusModel } from '@fi-sas/webpage/modules/social-support/models/change-status.model';
import { ChangeStatusService } from '@fi-sas/webpage/modules/social-support/services/change-status.service';
import { InterestService } from '@fi-sas/webpage/modules/social-support/services/interest.service';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { WithdrawalService } from '@fi-sas/webpage/modules/social-support/services/withdrawal.service';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-form-withdrawal',
  templateUrl: './form-withdrawal.component.html',
  styleUrls: ['./form-withdrawal.component.less'],
})
export class FormWithdrawalComponent extends FormHandler {
  @Input() applicationID = 0;
  @Input() action: string = null;

  loadingSubmit: boolean;

  withdrawalForm: FormGroup = new FormGroup({
    reason: new FormControl('', [Validators.required, trimValidation]),
  });

  constructor(
    private changeStatusService: ChangeStatusService,
    private interestService: InterestService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,
    private uiService: UiService,
    private withdrawalService: WithdrawalService,
  ) {
    super();
  }

  submitWithdrawal() {
    if (this.isFormValid(this.withdrawalForm)) {
      const withdrawal = new ChangeStatusModel();
      withdrawal.notes = this.withdrawalForm.value.reason;

      this.loadingSubmit = true;
      if (this.action === 'WITHDRAWAL') {
        this.interestService
          .widthrawFromCollaboration(this.applicationID, withdrawal)
          .pipe(
            first(),
            finalize(() => (this.loadingSubmit = false))
          )
          .subscribe(() => {
            this.withdrawalService.submittedWithdrawal(true);
            this.modalRef.close(true);
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('DASHBOARD.WIDGETS.MY_APPLICATIONS_SOCIAL_SUPPORT.MODAL_WITHDRAWAL.SUCCESS')
            );
            this.modalRef.close(true);
          });
      } else if (this.action === 'CANCEL') {
        this.changeStatusService
          .applicationCancel(this.applicationID, withdrawal)
          .pipe(
            first(),
            finalize(() => (this.loadingSubmit = false))
          )
          .subscribe(() => {
            this.withdrawalService.submittedCancel(true);
            this.modalRef.close(true);
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_SUCCESS')
            );
            this.modalRef.close(true);
          });
      }
    }
  }

  getInputError(inputName: string): string {
    return this.withdrawalForm.get(inputName).errors.required
    ? this.translateService.currentLang == 'pt'
      ? 'O campo é obrigatório'
      : 'Required field'
    : this.withdrawalForm.get(inputName).errors.requiredTrue
    ? this.translateService.currentLang == 'pt'
      ? 'O campo é obrigatório'
      : 'Required field'
    : null;
  }
}
