import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferStatusBadgeComponent } from './offer-status-badge.component';

describe('OfferStatusBadgeComponent', () => {
  let component: OfferStatusBadgeComponent;
  let fixture: ComponentFixture<OfferStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfferStatusBadgeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
