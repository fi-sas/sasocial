import { Component, Input } from '@angular/core';

import { NzModalService } from 'ng-zorro-antd';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { OfferStatusChangeHistoryComponent } from '../offer-status-change-history';

@Component({
  selector: 'app-offer-status-badge',
  templateUrl: './offer-status-badge.component.html',
  styleUrls: ['./offer-status-badge.component.less'],
})
export class OfferStatusBadgeComponent {
  @Input() offer: ExperienceModel = null;
  @Input() clickable: boolean = true;

  statusClassMapper = {
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    CLOSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CLOSED',
    DISPATCH: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DISPATCH',
    EXTERNAL_SYSTEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXTERNAL_SYSTEM',
    IN_COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.IN_COLABORATION',
    PUBLISHED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.PUBLISHED',
    REJECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.REJECTED',
    RETURNED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.RETURNED',
    SELECTION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SELECTION',
    SEND_SEEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SEND_SEEM',
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
  };

  protected isAction = true;

  constructor(private modalService: NzModalService) {}

  openOfferHistory($event:MouseEvent) {
    $event.stopPropagation();
    this.modalService.create({
      nzTitle: null,
      nzContent: OfferStatusChangeHistoryComponent,
      nzComponentParams: { offer: this.offer },
      nzFooter: null,
    });
  }
}
