import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferStatusChangeHistoryComponent } from './offer-status-change-history.component';

describe('OfferHistoryComponent', () => {
  let component: OfferStatusChangeHistoryComponent;
  let fixture: ComponentFixture<OfferStatusChangeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfferStatusChangeHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferStatusChangeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
