import { Component, Input } from '@angular/core';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';

@Component({
  selector: 'app-offer-status-change-history',
  templateUrl: './offer-status-change-history.component.html',
  styleUrls: ['./offer-status-change-history.component.less'],
})
export class OfferStatusChangeHistoryComponent {
  @Input() offer: ExperienceModel = null;

  constructor() {}
}
