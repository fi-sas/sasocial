import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';
import { OffersService } from '../../../../services/offers.services';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';

@Component({
  selector: 'app-offers-card',
  templateUrl: './offers-card.component.html',
  styleUrls: ['./offers-card.component.less'],
})
export class OffersCardComponent implements OnInit {
  private _offer : ExperienceModel = null;
  @Input() set offer(offer: ExperienceModel) {
    this._offer = offer;
    this.canGenerateReport = ['IN_COLABORATION', 'CLOSED'].includes(offer.status);
  }
  get offer() {
    return this._offer;
  }
  @Input() isAdvisor = false;
  @Input() showButtons = true;
  @Input() translations;
  @Output() isCancel = new EventEmitter();
  @Output() openList = new EventEmitter();
  modalDetail = false;
  successModalReport = false;
  buttonStyleDetail = {
    'font-size': '12px',
    height: 'auto',
    'padding-top': '2px',
    'padding-bottom': '2px',
  };

  status = {
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    RETURNED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.RETURNED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    PUBLISHED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.PUBLISHED',
    REJECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.REJECTED',
    SEND_SEEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SEND_SEEM',
    EXTERNAL_SYSTEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXTERNAL_SYSTEM',
    SELECTION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SELECTION',
    IN_COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.IN_COLABORATION',
  };

  loadingPrint: boolean;

  canGenerateReport: boolean;

  constructor(
    private router: Router,
    private offerService: OffersService,
    private uiService: UiService,
    private translateService: TranslateService,
    private modalService: NzModalService
  ) {}

  ngOnInit() {}

  checkTagStatusColor() {
    const css = {};
    css[this.offer.status] = true;
    return css;
  }

  manage() {
    this.openList.emit();
  }

  edit(idOffer: number) {
    this.router.navigateByUrl('social-support/offers/' + idOffer);
  }

  cancel(idInterest: number) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.OFFERS.CONFIRM_CANCEL'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelConfirm(idInterest),
    });
  }

  cancelConfirm(idInterest: number) {
    this.offerService
      .changeStatusExperienceCancel(idInterest)
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('SOCIAL_SUPPORT.ORDER_CANCEL'));
        this.isCancel.emit(true);
      });
  }

  reviewExperience() {
    this.router.navigateByUrl('social-support/experiences/' + this.offer.id);
  }

  manageCollaborations() {
    this.router.navigateByUrl('social-support/management-attendances/' + this.offer.id);
  }

  print() {
    this.loadingPrint = true;
    this.offerService.printOffer(this.offer.id)
      .pipe(first(), finalize(() => this.loadingPrint = false))
      .subscribe(() => {
        this.successModalReport = true
      });
  }
}
