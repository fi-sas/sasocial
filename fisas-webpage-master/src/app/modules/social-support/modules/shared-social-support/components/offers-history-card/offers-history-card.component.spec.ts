import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OffersHistoryCardComponent } from './offers-history-card.component';

describe('OffersHistoryCardComponent', () => {
  let component: OffersHistoryCardComponent;
  let fixture: ComponentFixture<OffersHistoryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OffersHistoryCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersHistoryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
