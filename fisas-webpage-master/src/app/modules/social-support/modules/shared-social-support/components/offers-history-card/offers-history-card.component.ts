import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ExperienceModel } from '@fi-sas/webpage/modules/social-support/models/experiences.model';

@Component({
  selector: 'app-offers-history-card',
  templateUrl: './offers-history-card.component.html',
  styleUrls: ['./offers-history-card.component.less'],
})
export class OffersHistoryCardComponent {
  @Input() offer: ExperienceModel = null;

  buttonStyle = {
    'font-size': '12px',
    color: 'var(--primary-color)',
    'border-color': 'var(--primary-color)',
    height: 'auto',
  };

  constructor(private router: Router) {}

  reviewExperience(idExperience: number) {
    this.router.navigateByUrl('social-support/experiences/' + idExperience);
  }

  goToOfferAttendances($event: MouseEvent,idExperience: number) {
    $event.stopPropagation();
    this.router.navigateByUrl('social-support/management-attendances/' + idExperience);
  }
}
