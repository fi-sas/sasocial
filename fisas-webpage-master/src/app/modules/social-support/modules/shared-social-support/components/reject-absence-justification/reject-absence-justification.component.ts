import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { AttendanceModel } from '../../../attendances/models';
import { AttendancesService } from '../../../attendances/services/attendances.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-reject-absence-justification',
  templateUrl: './reject-absence-justification.component.html',
  styleUrls: ['./reject-absence-justification.component.less'],
})
export class RejectAbsenceJustificationComponent extends FormHandler {
  private _attendance: AttendanceModel;

  @Input() set attendance(attendance: AttendanceModel) {
    this._attendance = attendance;

    this.form = this.getForm();
  }

  get attendance() {
    return this._attendance;
  }

  translations: TranslationModel[] = [];

  form: FormGroup;

  isLoading: boolean;

  constructor(
    private attendancesService: AttendancesService,
    private uiService: UiService,
    private translateService: TranslateService,
    private modalRef: NzModalRef,
    private configurationsService: ConfigurationsService
  ) {
    super();
    this.translationsTitle();
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
    this.translations = service.translations;
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      const data = {
        reject_reason: this.form.value.reject_reason,
      };
      this.attendancesService
        .rejectJustification(this.attendance.absence_reason[0].id, data)
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.SUCCESS')
          );
          this.modalRef.close(true);
        });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  close(): void {
    this.modalRef.close(false);
  }

  private getForm() {
    return new FormGroup({
      reject_reason: new FormControl('', [Validators.required, trimValidation]),
    });
  }
}
