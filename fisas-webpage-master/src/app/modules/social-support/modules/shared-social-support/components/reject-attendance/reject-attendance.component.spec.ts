import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectAttendanceComponent } from './reject-attendance.component';

describe('RejectAttendanceComponent', () => {
  let component: RejectAttendanceComponent;
  let fixture: ComponentFixture<RejectAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RejectAttendanceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
