import { Component, Input } from '@angular/core';

import {
  ApplicationFile,
  AuxDataApplication,
  SocialSupportApplicationModel,
} from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';

@Component({
  selector: 'app-review-application',
  templateUrl: './review-application.component.html',
  styleUrls: ['./review-application.component.less'],
})
export class ReviewApplicationComponent {
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application && (application.preferred_schedule || []).length && !(application.preferred_schedule_ids || []).length) {
      application.preferred_schedule_ids = application.preferred_schedule;
    }
    this._application = application;

    this.attachments = application.file_ids || application.files || [];

  }
  get application() {
    return this._application;
  }

  private _auxData: AuxDataApplication;
  @Input() set auxData(auxData: AuxDataApplication) {
    this._auxData = auxData;

    if ((auxData.organic_units || []).length) {
      this.organicUnits = auxData.organic_units;
    }

    if ((auxData.preferred_activities || []).length) {
      this.preferredActivities = auxData.preferred_activities;
    }
  }
  get auxData() {
    return this._auxData;
  }

  @Input() consultation = false;

  organicUnits = ['--'];
  preferredActivities = ['--'];

  attachments: ApplicationFile[] = [];

  readonly scheduleRows = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.MORNING',
      keys: [4, 7, 10, 13, 16, 19, 1],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.AFTERNOON',
      keys: [5, 8, 11, 14, 17, 20, 2],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.NIGHT',
      keys: [6, 9, 12, 15, 18, 21, 3],
    },
  ];

  constructor() { }
}
