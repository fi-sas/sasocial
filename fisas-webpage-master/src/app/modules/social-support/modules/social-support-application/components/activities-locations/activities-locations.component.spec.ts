import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiesLocationsComponent } from './activities-locations.component';

describe('ActivitiesLocationsComponent', () => {
  let component: ActivitiesLocationsComponent;
  let fixture: ComponentFixture<ActivitiesLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
