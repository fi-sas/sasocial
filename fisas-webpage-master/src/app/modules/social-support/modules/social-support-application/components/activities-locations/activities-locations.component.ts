import { Component, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { ActivitiesService } from '@fi-sas/webpage/modules/social-support/services/activities.service';
import { ActivityModel } from '@fi-sas/webpage/modules/social-support/models/activity.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';

interface IFormControlValue {
  [key: number]: boolean;
}

@Component({
  selector: 'app-activities-locations',
  templateUrl: './activities-locations.component.html',
  styleUrls: ['./activities-locations.component.less'],
})
export class ActivitiesLocationsComponent implements OnDestroy {
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this._application = application;
      if ((application.preferred_activities || []).length) {
        this._application.preferred_activities_ids = this._application.preferred_activities.map(item => item.activity_id);
      }
      if ((application.organic_units || []).length) {
        this._application.organic_units_ids = this._application.organic_units.map(item => item.organic_unit_id);
      }
      this.loadForm();
    }

    this.getOrganicUnits();
    this.getActivities();
  }

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  _application: SocialSupportApplicationModel = null;

  organicUnits: OrganicUnitModel[] = [];
  activities: ActivityModel[] = [];

  form: FormGroup;

  constructor(
    private activitiesService: ActivitiesService,
    private organicUnitsService: OrganicUnitsService,
    private translate: TranslateService
  ) {
    this.form = new FormGroup({
      organic_units: new FormControl(null, Validators.required),
      preferred_activities: new FormControl(null, Validators.required),
      locations: new FormGroup({}),
      activities: new FormGroup({}),
    });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.form.controls) {
      if (i) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
        if (this.form.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getFormValues() {
    return {
      organic_units_ids: this.form.get('organic_units').value,
      preferred_activities_ids: this.form.get('preferred_activities').value,
    };
  }

  getFormOptions() {
    return {
      organic_units: this._getSelected(this.organicUnits, this.form.get('locations').value, 'name') as string[],
      preferred_activities: this._getSelected(this.activities, this.form.get('activities').value, 'name') as string[],
    };
  }

  getInputError(inputName: string): string {
    return this.form.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required field'
      : this.form.get(inputName).errors.requiredTrue
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required field'
      : null;
  }

  private loadForm() {
    this.form.get('preferred_activities').setValue(this._application.preferred_activities_ids);
    this.form.get('organic_units').setValue(this._application.organic_units_ids);
  }

  private getActivities(): void {
    this.activitiesService
      .list()
      .pipe(first())
      .subscribe((activities) => {
        this.setActivities(activities.data);
        this.handleActivitiesValueChange();
      });
  }

  private setActivities(data: ActivityModel[]) {
    const formGroup: FormGroup = this.form.get('activities') as FormGroup;
    this.activities = data;
    this.activities.forEach((activity) =>
      formGroup.addControl(
        activity.id.toString(),
        new FormControl(
          this._application ? (this._application.preferred_activities_ids || []).includes(activity.id) : false
        )
      )
    );
  }

  private handleActivitiesValueChange() {
    this.subscriptions = this.form.get('activities').valueChanges.subscribe((value) => {
      const ids = this._getSelected(this.activities, value, 'id') as number[];
      this.form.get('preferred_activities').setValue((ids || []).length ? ids : null);
    });
  }

  private getOrganicUnits(): void {
    this.organicUnitsService
      .organicUnits()
      .pipe(first())
      .subscribe((organicUnits) => {
        this.setOrgnaicUnits(organicUnits.data);
        this.handleLocationsValueChange();
      });
  }

  private setOrgnaicUnits(data: OrganicUnitModel[]) {
    const formGroup: FormGroup = this.form.get('locations') as FormGroup;
    this.organicUnits = data;
    this.organicUnits.forEach((organicUnit) =>
      formGroup.addControl(
        organicUnit.id.toString(),
        new FormControl(
          this._application ? (this._application.organic_units_ids || []).includes(organicUnit.id) : false
        )
      )
    );
  }

  private handleLocationsValueChange() {
    this.subscriptions = this.form.get('locations').valueChanges.subscribe((value) => {
      const ids = this._getSelected(this.organicUnits, value, 'id') as number[];
      this.form.get('organic_units').setValue((ids || []).length ? ids : null);
    });
  }

  private _getSelected(
    objectList: (OrganicUnitModel | ActivityModel)[],
    controlValue: IFormControlValue,
    fieldToGet: 'id' | 'name' = 'id'
  ): (string | number)[] {
    const selected: (string | number)[] = [];
    objectList.forEach((obj) => {
      if (controlValue[obj.id]) {
        selected.push(obj[fieldToGet]);
      }
    });
    return selected;
  }
}
