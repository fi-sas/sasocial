import { Component, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { ApplicationFile, SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { FileUploadComponent } from '@fi-sas/webpage/shared/components';
import { NamedFileModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-attachments',
  templateUrl: './attachments.component.html',
  styleUrls: ['./attachments.component.less']
})
export class AttachmentsComponent extends FormHandler {
  @ViewChild('attachmentsTemplate') attachmentsTemplate: FileUploadComponent;

  @Input() set application(application: SocialSupportApplicationModel) {
    if (!application) {
      return;
    }

    this.form.get('attachments').setValue(application.files || []);
  }

  form = new FormGroup({ attachments: new FormControl(null) });

  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];

  constructor(private translateService: TranslateService) {
    super();
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  validForm() {
    return this.isFormValid(this.form);
  }

  getFormValues(): NamedFileModel[] {
    return this.form.get('attachments').value || [];
  }
}
