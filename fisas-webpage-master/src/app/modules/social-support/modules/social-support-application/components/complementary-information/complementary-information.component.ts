import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-complementary-information',
  templateUrl: './complementary-information.component.html',
  styleUrls: ['./complementary-information.component.less'],
})
export class ComplementaryInformationComponent {
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this._application = application;
      this.complementaryInformationForm.patchValue({
        observations: this.application.observations,
      });
    }
  }
  get application() {
    return this._application;
  }

  complementaryInformationForm: FormGroup = new FormGroup({
    observations: new FormControl('', [trimValidation]),
  });

  constructor() { }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.complementaryInformationForm.controls) {
      if (i) {
        this.complementaryInformationForm.controls[i].markAsDirty();
        this.complementaryInformationForm.controls[i].updateValueAndValidity();
        if (this.complementaryInformationForm.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getFormValues(): { observations?: string; } {
    return this.complementaryInformationForm.value;
  }

  clearForm() {
    this.complementaryInformationForm.reset();
  }
}
