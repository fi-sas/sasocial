import { Component, Input, OnInit } from '@angular/core';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.less'],
})
export class ContactsComponent implements OnInit {
  @Input() user: UserModel = null;

  emailRegex =
    "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|" +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  contactsForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex), trimValidation]),
    phone: new FormControl(''),
  });

  constructor( private translate: TranslateService) {}

  ngOnInit() {
    if (this.user) {
      this.contactsForm.patchValue({
        email: this.user.email,
        phone: this.user.phone,
      });
    }
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.contactsForm.controls) {
      if (i) {
        this.contactsForm.controls[i].markAsDirty();
        this.contactsForm.controls[i].updateValueAndValidity();
        if (this.contactsForm.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getInputError(inputName: string): string {
    return this.contactsForm.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : this.contactsForm.get(inputName).errors.requiredTrue
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : null;
  }

  getFormValues(): { email: string; phone?: string; } {
    return this.contactsForm.value;
  }

  clearForm() {
    this.contactsForm.reset();
  }
}
