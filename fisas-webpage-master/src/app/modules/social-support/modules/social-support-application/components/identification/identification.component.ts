import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AcademicYear } from '@fi-sas/webpage/shared/models';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { GenderOptions } from '@fi-sas/webpage/shared/data/gender';
import { nationalities } from '@fi-sas/webpage/shared/data/nationalities';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { Resource } from '@fi-sas/core';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.less'],
})
export class IdentificationComponent implements OnInit {

  private _user: UserModel = null;
  @Input() set user(user: UserModel) {
    this._user = user;
    if (user) {
      this.setFormValues();
    }
  }
  get user() {
    return this._user;
  }

  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    this._application = application;
    if (application) {
      this.setFormValues();
    }
  }
  get application() {
    return this._application;
  }

  nationalityOptions = nationalities;
  genderOptions = GenderOptions;

  loadingCourses = false;
  loadingSchools = false;
  loadingDegrees = false;

  degrees: CourseDegreeModel[];
  courses: CourseModel[] = [];
  organicUnits: OrganicUnitModel[] = [];

  educationInformation = {
    course_degree: '',
    course: '',
    school: '',
  };

  identificationForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    address: new FormControl('', [Validators.required, trimValidation]),
    postal_code: new FormControl('', Validators.required),
    city: new FormControl('', [Validators.required, trimValidation]),
    birth_date: new FormControl('', Validators.required),
    tin: new FormControl('', Validators.required),
    identification: new FormControl('', [Validators.required, trimValidation]),
    iban: new FormControl('', [Validators.required]),
    student_number: new FormControl('', Validators.required),
    academic_year: new FormControl({ value: '', disabled: true }, Validators.required),
    course_year: new FormControl('', Validators.required),
    course_degree_id: new FormControl('', Validators.required),
    school_id: new FormControl('', Validators.required),
    course_id: new FormControl('', Validators.required),
    nationality: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required),
  });

  private readonly formDescription: {
    formKey: string;
    userKey?: keyof UserModel;
    applicationKey: keyof SocialSupportApplicationModel;
  }[] = [
    { formKey: 'academic_year', applicationKey: 'academic_year' },
    { formKey: 'address', userKey: 'address', applicationKey: 'student_address' },
    { formKey: 'birth_date', userKey: 'birth_date', applicationKey: 'student_birthdate' },
    { formKey: 'city', userKey: 'city', applicationKey: 'student_location' },
    { formKey: 'course_year', userKey: 'course_year', applicationKey: 'course_year' },
    { formKey: 'gender', userKey: 'gender', applicationKey: 'genre' },
    { formKey: 'iban', applicationKey: 'iban' },
    { formKey: 'identification', userKey: 'identification', applicationKey: 'student_identification' },
    { formKey: 'name', userKey: 'name', applicationKey: 'student_name' },
    { formKey: 'nationality', userKey: 'nationality', applicationKey: 'student_nationality' },
    { formKey: 'postal_code', userKey: 'postal_code', applicationKey: 'student_code_postal' },
    { formKey: 'student_number', userKey: 'student_number', applicationKey: 'student_number' },
    { formKey: 'tin', userKey: 'tin', applicationKey: 'student_tin' },
    // THE FOLLOWING THREE FIELDS NEEDS TO FOLLOW THIS ORDER BECAUSE OF DEPENDENCIES BETWEEN FIELDS
    { formKey: 'school_id', userKey: 'organic_unit_id', applicationKey: 'school_id' },
    { formKey: 'course_degree_id', userKey: 'course_degree_id', applicationKey: 'course_degree_id' },
    { formKey: 'course_id', userKey: 'course_id', applicationKey: 'course_id' },
  ];

  constructor(
    private configurationsServices: ConfigurationsService,
    private organicUnitsService: OrganicUnitsService,
    public translate: TranslateService
  ) {
    this.getSchools();
    this.getDegrees();
    this.getCourses();
  }

  ngOnInit() {
    if (!this.application) {
      this.configurationsServices.getCurrentAcademicYear()
        .pipe(first())
        .subscribe(response => {
          if(response.data.length){
            this.identificationForm.get('academic_year').setValue(response.data[0].academic_year);
          }else{
            this.identificationForm.get('academic_year').enable()
          }
        });
    }
  }

  private setFormValues() {
    this.formDescription.forEach((item) => {
      let value: unknown = null;
      if (this.application) {
        value = this.application[item.applicationKey] || null;
      } else if (this.user && item.userKey) {
        value = this.user[item.userKey] || null;
      }
      if (value !== null) {
        this.identificationForm.get(item.formKey).setValue(value);
        if (item.userKey && this.user[item.userKey] !== null) {
          this.identificationForm.get(item.formKey).disable();
        }
      }
    });
  }

  getSchools() {
    this.loadingSchools = true;
    this.organicUnitsService
      .organicUnitsbySchools()
      .pipe(
        first(),
        finalize(() => (this.loadingSchools = false))
      )
      .subscribe((schools) => (this.organicUnits = schools.data));
  }

  getDegrees() {
    this.loadingDegrees = true;
    this.configurationsServices
      .courseDegrees()
      .pipe(
        first(),
        finalize(() => (this.loadingDegrees = false))
      )
      .subscribe((degrees) => (this.degrees = degrees.data));
  }

  getCourses() {
    this.loadingCourses = true;
    this.configurationsServices
      .courses(this.identificationForm.get('course_degree_id').value, this.identificationForm.get('school_id').value)
      .pipe(
        first(),
        finalize(() => (this.loadingCourses = false))
      )
      .subscribe((courses) => (this.courses = courses.data));
  }

  changeValue() {
    this.identificationForm.get('course_id').setValue(null);
    this.getCourses();
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.identificationForm.controls) {
      if (i) {
        this.identificationForm.controls[i].markAsDirty();
        this.identificationForm.controls[i].updateValueAndValidity();
        if (this.identificationForm.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getFormValues() {
    return this.identificationForm.getRawValue();
  }

  getEducationInformation(): any {
    this.educationInformation.course_degree = this.degrees.find(
      (degree) => degree.id === this.identificationForm.controls.course_degree_id.value
    ).name;
    this.educationInformation.course = this.courses.find(
      (school) => school.id === this.identificationForm.controls.course_id.value
    ).name;
    this.educationInformation.school = this.organicUnits.find(
      (course) => course.id === this.identificationForm.controls.school_id.value
    ).name;

    return this.educationInformation;
  }

  getInputError(inputName: string): string {
    return this.identificationForm.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : this.identificationForm.get(inputName).errors.requiredTrue
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : null;
  }

  clearForm() {
    this.identificationForm.reset();
  }

  disableEndDate = (current: Date): boolean => {
    return !moment(current).isBefore(new Date());
  };
}
