import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { languages } from '@fi-sas/webpage/modules/social-support/modules/social-support-application/components/skills-professional-experience/languages';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-linguistic-skills',
  templateUrl: './linguistic-skills.component.html',
  styleUrls: ['./linguistic-skills.component.less'],
})
export class LinguisticSkillsComponent {
  other_language = false;
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this.form.patchValue({
        foreign_languages: application.foreign_languages,
        language_skills: (application.language_skills || []).filter((language: string) =>
          this.languages.includes(language)
        ),
        other_language_skills: (application.language_skills || []).find(
          (language: string) => !this.languages.includes(language)
        ),
      });
      if(this.form.get('other_language_skills').value) {
        this.other_language = true;
        const aux = this.form.get('language_skills').value;
        aux.push('OTHER');

      }
    }
  }
  get application() {
    return this._application;
  }

  form = new FormGroup({
    foreign_languages: new FormControl(null, Validators.required),
    language_skills: new FormControl(null),
    other_language_skills: new FormControl(null, [trimValidation]),
  });

  languages = languages;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(private translate: TranslateService) { }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.form.controls) {
      if (i) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
        if (this.form.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getFormValues(): any {
    return this.form.value;
  }

  getInputError(inputName: string): string {
    return this.form.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : this.form.get(inputName).errors.requiredTrue
        ? this.translate.currentLang == 'pt'
          ? 'O campo é obrigatório'
          : 'Required Field'
        : null;
  }

  changeLanguage(event) {
    if (event) {
      this.form.controls.language_skills.setValidators([Validators.required]);
    } else {
      this.form.controls.language_skills.clearValidators();
    }
    this.form.controls.language_skills.updateValueAndValidity();
  }

  onChangeLanguage(event) {
    this.other_language = false;
    if (event.length > 0) {
      event.forEach(lang => {
        if (lang == "OTHER") {
          this.other_language = true;
          this.form.get('other_language_skills').enable();
        }
      });
      this.form.get('language_skills').setValue(event);
    } else {
      this.form.get('language_skills').setValue('');
    }

    if (this.other_language) {
      this.form.controls.other_language_skills.setValidators([Validators.required, trimValidation]);
    } else {
      this.form.controls.other_language_skills.clearValidators();
    }
    this.form.controls.other_language_skills.updateValueAndValidity();
  }


  check(lang) {
    if (this.form.get('language_skills').value) {
      return this.form.get('language_skills').value.includes(lang);
    } else {
      return false;
    }
  }

  changeOther() {
    this.form.get('other_language_skills').setValue('');
  }

}
