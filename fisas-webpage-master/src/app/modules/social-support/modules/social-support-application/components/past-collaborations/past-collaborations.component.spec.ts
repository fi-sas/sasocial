import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastCollaborationsComponent } from './past-collaborations.component';

describe('PastCollaborationsComponent', () => {
  let component: PastCollaborationsComponent;
  let fixture: ComponentFixture<PastCollaborationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastCollaborationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastCollaborationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
