import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { environment } from './../../../../../../../environments/environment';

@Component({
  selector: 'app-past-collaborations',
  templateUrl: './past-collaborations.component.html',
  styleUrls: ['./past-collaborations.component.less'],
})
export class PastCollaborationsComponent implements OnInit {
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this._application = application;
      this.pastCollaborationsForm.patchValue({
        has_collaborated_last_year: application.has_collaborated_last_year,
        previous_activity_description: application.previous_activity_description,
      });
    }
  }
  get application() {
    return this._application;
  }

  pastCollaborationsForm: FormGroup = new FormGroup({
    has_collaborated_last_year: new FormControl(null, Validators.required),
    previous_activity_description: new FormControl({value: '', disabled: true},[trimValidation]),
  });

  institute = environment.institute;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.handleHasCollaboratedLastYearChange();
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.pastCollaborationsForm.controls) {
      if (i) {
        this.pastCollaborationsForm.controls[i].markAsDirty();
        this.pastCollaborationsForm.controls[i].updateValueAndValidity();
        if (this.pastCollaborationsForm.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  setPreviousActivityDescriptionRequired(hasCollaboratedLastYear: boolean) {
    if (hasCollaboratedLastYear) {
      this.pastCollaborationsForm.get('previous_activity_description').setValidators([Validators.required,trimValidation]);
      this.pastCollaborationsForm.get('previous_activity_description').updateValueAndValidity();
    } else {
      this.pastCollaborationsForm.get('previous_activity_description').clearValidators();
      this.pastCollaborationsForm.get('previous_activity_description').updateValueAndValidity();
    }
  }

  getFormValues(): any {
    return this.pastCollaborationsForm.value;
  }

  getInputError(inputName: string): string {
    return this.pastCollaborationsForm.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : this.pastCollaborationsForm.get(inputName).errors.requiredTrue
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : null;
  }

  clearForm() {
    this.pastCollaborationsForm.reset();
  }

  private handleHasCollaboratedLastYearChange(): void {
    this.subscriptions = this.pastCollaborationsForm
      .get('has_collaborated_last_year')
      .valueChanges.subscribe((value) => this.toggleLanguageSkillsActiveMode(value));
  }

  private toggleLanguageSkillsActiveMode(value): void {
    const formControl = this.pastCollaborationsForm.get('previous_activity_description');
    if (value === true) {
      formControl.setValidators([Validators.required,trimValidation]);
      formControl.enable();
    } else {
      formControl.clearValidators();
      formControl.disable();
    }
    formControl.updateValueAndValidity();
  }
}
