import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { languages } from '@fi-sas/webpage/modules/social-support/modules/social-support-application/components/skills-professional-experience/languages';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { SocialSupportApplicationModel } from '../../../../models/social-support-application.model';

@Component({
  selector: 'app-skills-professional-experience',
  templateUrl: './skills-professional-experience.component.html',
  styleUrls: ['./skills-professional-experience.component.less'],
})
export class SkillsProfessionalExperienceComponent implements OnInit {
  private _application: SocialSupportApplicationModel = null;
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      this._application = application;
      this.skillsProfessionalExperienceForm.patchValue({
        has_profissional_experience: application.has_profissional_experience,
        foreign_languages: application.foreign_languages,
        language_skills: application.language_skills,
        type_of_company: application.type_of_company,
        job_at_company: application.job_at_company,
        job_description: application.job_description,
      });
    }
  }
  get application() {
    return this._application;
  }

  skillsProfessionalExperienceForm: FormGroup = new FormGroup({
    has_profissional_experience: new FormControl(null, Validators.required),
    type_of_company: new FormControl({ value: '', disabled: true },[trimValidation]),
    job_at_company: new FormControl({ value: '', disabled: true },[trimValidation]),
    job_description: new FormControl({ value: '', disabled: true },[trimValidation]),
  });

  languages = languages;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.handleHasProfissionalExperienceChange();
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  private handleHasProfissionalExperienceChange(): void {
    this.subscriptions = this.skillsProfessionalExperienceForm
      .get('has_profissional_experience')
      .valueChanges.subscribe((value) => this.toggleJobInfoActiveMode(value));
  }

  private toggleJobInfoActiveMode(value): void {
    [
      this.skillsProfessionalExperienceForm.get('type_of_company'),
      this.skillsProfessionalExperienceForm.get('job_at_company'),
      this.skillsProfessionalExperienceForm.get('job_description'),
    ].forEach((formControl: FormControl) => {
      if (value === true) {
        formControl.setValidators([Validators.required,trimValidation]);
        formControl.enable();
      } else {
        formControl.clearValidators();
        formControl.disable();
      }
      formControl.updateValueAndValidity();
    });
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.skillsProfessionalExperienceForm.controls) {
      if (i) {
        this.skillsProfessionalExperienceForm.controls[i].markAsDirty();
        this.skillsProfessionalExperienceForm.controls[i].updateValueAndValidity();
        if (this.skillsProfessionalExperienceForm.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  setProfissionalExperienceRequired(hasProfissionalExperience: boolean) {
    if (hasProfissionalExperience) {
      this.skillsProfessionalExperienceForm.get('type_of_company').setValidators([Validators.required,trimValidation]);
      this.skillsProfessionalExperienceForm.get('type_of_company').updateValueAndValidity();
      this.skillsProfessionalExperienceForm.get('job_at_company').setValidators([Validators.required,trimValidation]);
      this.skillsProfessionalExperienceForm.get('job_at_company').updateValueAndValidity();
      this.skillsProfessionalExperienceForm.get('job_description').setValidators([Validators.required,trimValidation]);
      this.skillsProfessionalExperienceForm.get('job_description').updateValueAndValidity();
    } else {
      this.skillsProfessionalExperienceForm.get('type_of_company').clearValidators();
      this.skillsProfessionalExperienceForm.get('type_of_company').updateValueAndValidity();
      this.skillsProfessionalExperienceForm.get('job_at_company').clearValidators();
      this.skillsProfessionalExperienceForm.get('job_at_company').updateValueAndValidity();
      this.skillsProfessionalExperienceForm.get('job_description').clearValidators();
      this.skillsProfessionalExperienceForm.get('job_description').updateValueAndValidity();
    }
  }

  setWhichLanguagesRequired(foreignLanguages: boolean) {
    if (foreignLanguages) {
      this.skillsProfessionalExperienceForm.get('language_skills').setValidators([Validators.required]);
      this.skillsProfessionalExperienceForm.get('language_skills').updateValueAndValidity();
    } else {
      this.skillsProfessionalExperienceForm.get('language_skills').clearValidators();
      this.skillsProfessionalExperienceForm.get('language_skills').updateValueAndValidity();
    }
  }

  getFormValues(): any {
    return this.skillsProfessionalExperienceForm.value;
  }

  getInputError(inputName: string): string {
    return this.skillsProfessionalExperienceForm.get(inputName).errors.required
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : this.skillsProfessionalExperienceForm.get(inputName).errors.requiredTrue
      ? this.translate.currentLang == 'pt'
        ? 'O campo é obrigatório'
        : 'Required Field'
      : null;
  }

  clearForm() {
    this.skillsProfessionalExperienceForm.reset();
  }
}
