import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialBenefitsComponent } from './social-benefits.component';

describe('SocialBenefitsComponent', () => {
  let component: SocialBenefitsComponent;
  let fixture: ComponentFixture<SocialBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SocialBenefitsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
