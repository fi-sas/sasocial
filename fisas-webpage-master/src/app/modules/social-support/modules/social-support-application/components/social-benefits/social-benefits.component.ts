import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { SocialSupportApplicationModel } from '../../../../models/social-support-application.model';

@Component({
  selector: 'app-social-benefits',
  templateUrl: './social-benefits.component.html',
  styleUrls: ['./social-benefits.component.less'],
})
export class SocialBenefitsComponent extends FormHandler implements OnInit {
  private _application: SocialSupportApplicationModel;
  @Input() set application(application: SocialSupportApplicationModel) {
    this._application = application;

    if (application) {
      this.form.patchValue({
        has_scholarship: application.has_scholarship,
        has_applied_scholarship: application.has_applied_scholarship,
        scholarship_monthly_value: application.scholarship_monthly_value,
        has_emergency_fund: application.has_emergency_fund,
        is_waiting_scholarship_response: application.is_waiting_scholarship_response,
        housed_residence: application.housed_residence,
        other_scholarship: application.other_scholarship,
        other_scholarship_values: application.other_scholarship_values,
      });
      this.handleFormValueChange();
    }
  }
  get application() {
    return this._application;
  }

  form: FormGroup = new FormGroup({
    has_scholarship: new FormControl(null, Validators.required),
    has_applied_scholarship: new FormControl(null, Validators.required),
    scholarship_monthly_value: new FormControl({ value: null, disabled: true }),
    has_emergency_fund: new FormControl(null, Validators.required),
    is_waiting_scholarship_response: new FormControl(false, Validators.required),
    housed_residence: new FormControl(null, Validators.required),
    other_scholarship: new FormControl(null, Validators.required),
    other_scholarship_values: new FormControl({ value: null, disabled: true }),
  });

  constructor(private translate: TranslateService) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.handleFormValueChange());
  }

  validForm(): boolean {
    let isValid = true;
    for (const i in this.form.controls) {
      if (i) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
        if (this.form.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getFormValues(): any {
    return this.form.value;
  }

  getInputError(inputName: string): string {
    if (
      this.form.get(inputName).errors.required ||
      this.form.get(inputName).errors.requiredTrue
    ) {
      return this.translate.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  clearForm() {
    this.form.reset();
  }

  private handleFormValueChange() {
    if (
      this.form.get('has_scholarship').value &&
      !this.form.get('is_waiting_scholarship_response').value
    ) {
      this.form.get('scholarship_monthly_value').enable({ emitEvent: false });
      this.form.get('scholarship_monthly_value').setValidators([Validators.required]);
    } else {
      this.form.get('scholarship_monthly_value').clearValidators();
      this.form.get('scholarship_monthly_value').disable({ emitEvent: false });
    }
    this.form.get('scholarship_monthly_value').updateValueAndValidity({ emitEvent: false });

    if (this.form.get('other_scholarship').value) {
      this.form.get('other_scholarship_values').enable({ emitEvent: false });
      this.form.get('other_scholarship_values').setValidators([Validators.required]);
    } else {
      this.form.get('other_scholarship_values').clearValidators();
      this.form.get('other_scholarship_values').disable({ emitEvent: false });
    }
    this.form.get('other_scholarship_values').updateValueAndValidity({ emitEvent: false });
  }
}
