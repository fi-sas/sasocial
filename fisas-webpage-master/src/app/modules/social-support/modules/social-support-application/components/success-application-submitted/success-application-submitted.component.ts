import { Component, Input } from '@angular/core';

import { NzModalRef } from 'ng-zorro-antd';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-success-application-submitted',
  templateUrl: './success-application-submitted.component.html',
  styleUrls: ['./success-application-submitted.component.less'],
})
export class SuccessApplicationSubmittedComponent {
  @Input() isEdit: boolean;

  translations: TranslationModel[] = [];

  constructor(private modal: NzModalRef, private configurationsService: ConfigurationsService) {
    this.getTranslations();
  }

  handleCancel(): void {
    this.modal.destroy();
  }

  getTranslations(){
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(SERVICE_IDS.SOCIAL_SUPPORT);
    this.translations = service.translations;
  }
}
