import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { SocialSupportApplicationModel } from '../../../../models/social-support-application.model';

interface IFormValues {
  preferred_schedule: number[];
  has_holydays_availability: boolean;
  schedule: { [key: number]: boolean };
}

@Component({
  selector: 'app-time-availability',
  templateUrl: './time-availability.component.html',
  styleUrls: ['./time-availability.component.less'],
})
export class TimeAvailabilityComponent extends FormHandler {
  @Input() set application(application: SocialSupportApplicationModel) {
    if (application) {
      if (
        (application.preferred_schedule || []).length &&
        (application.preferred_schedule || []).length !== (application.preferred_schedule_ids || []).length
      ) {
        application.preferred_schedule_ids = application.preferred_schedule.map((item) => item.schedule_id);
      }
      this.setFormData(application);
    }
  }

  readonly scheduleRows: { title: string; keys: string[] }[] = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.MORNING',
      keys: ['4', '7', '10', '13', '16', '19', '1'],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.AFTERNOON',
      keys: ['5', '8', '11', '14', '17', '20', '2'],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.NIGHT',
      keys: ['6', '9', '12', '15', '18', '21', '3'],
    },
  ];

  form: FormGroup;

  constructor(protected translateService: TranslateService) {
    super();
    this.form = this.getForm();
  }

  getFormValues(): IFormValues {
    const result: IFormValues = this.form.getRawValue();
    result.preferred_schedule = result.preferred_schedule.map((s) => (typeof s === 'number' ? s : parseInt(s, 10)));
    return result;
  }

  onScheduleChange(event: string[]) {
    this.form.get('preferred_schedule').setValue(event);
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  validForm(): boolean {
    return this.isFormValid(this.form);
  }

  clearForm() {
    this.form.reset();
  }

  protected disableFilledFields(fieldNames: string[]) {
    fieldNames.forEach((key: string) => {
      const formControl = this.form.get(key);
      if (formControl && formControl.value) {
        formControl.updateValueAndValidity();
        if (formControl.valid) {
          formControl.disable();
        }
      }
    });
  }

  private getForm(): FormGroup {
    const scheduleFG = {};
    this.scheduleRows.forEach((row) => row.keys.forEach((k) => (scheduleFG[k] = new FormControl(false))));

    return new FormGroup({
      preferred_schedule: new FormControl(null, Validators.required),
      has_holydays_availability: new FormControl(null, Validators.required),
      schedule: new FormGroup(scheduleFG),
    });
  }

  private setFormData(application: SocialSupportApplicationModel) {
    this.form.patchValue({ has_holydays_availability: application.has_holydays_availability });
    const schedule: string[] = application.preferred_schedule_ids.map((s) => s.toString());
    this.onScheduleChange(schedule);
    const preferredSchedule = {};
    this.scheduleRows.forEach((row) => row.keys.forEach((k) => (preferredSchedule[k] = schedule.includes(k))));
    this.form.controls.schedule.patchValue(preferredSchedule);
  }
}
