import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormApplicationReviewComponent } from './form-application-review.component';


const routes: Routes = [
  {
    path: '',
    component: FormApplicationReviewComponent,
    data: { breadcrumb: null, title: null , scope: ['social_scholarship:applications:read', 'sasocial:is_student'] }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormApplicationReviewRoutingModule { }
