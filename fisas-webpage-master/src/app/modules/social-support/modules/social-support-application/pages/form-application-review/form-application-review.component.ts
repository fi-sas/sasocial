import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NzModalService } from 'ng-zorro-antd';

import {
  AuxDataApplication,
  SocialSupportApplicationModel,
} from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { ChangeStatusService } from '@fi-sas/webpage/modules/social-support/services/change-status.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Resource } from '@fi-sas/core';
import { SocialSupportApplicationsService } from '../../services/social-support-applications.service';
import { TranslateService } from '@ngx-translate/core';
import { finalize, first } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-form-application-review',
  styleUrls: ['./form-application-review.component.less'],
  templateUrl: './form-application-review.component.html',
})
export class FormApplicationReviewComponent implements OnInit {
  buttonBackStyle = {
    width: 'auto',
    height: '30px',
    'font-size': '15px',
    'margin-top': '33px',
    'margin-bottom': '65.5px',
  };

  application: SocialSupportApplicationModel = null;
  auxData: AuxDataApplication = null;

  isLoaging: boolean;
  isCancelLoading: boolean;

  showCancelButton: boolean;

  canEditApplication: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private router: Router,
    private socialSupportApplicationsService: SocialSupportApplicationsService,
    private uiService: UiService,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private changeStatusService: ChangeStatusService
  ) {}

  ngOnInit() {
    const applicationId: number = this.activatedRoute.snapshot.params.id;
    this.getApplication(applicationId);
  }

  private getApplication(id: number) {
    this.isLoaging = true;
    this.socialSupportApplicationsService.getApplication(id)
      .pipe(first(), finalize(() => this.isLoaging = false))
      .subscribe(result => {
        if(result.data.length){
          this.application = result.data[0];
          this.application.preferred_schedule = this.application.preferred_schedule.map((preferred_schedule) => preferred_schedule.schedule.id);
          this.auxData = this.getAuxData();
          this.showCancelButton = ['SUBMITTED', 'ANALYSED', 'INTERVIEWED', 'ACCEPTED'].includes(this.application.status);
          this.canEditApplication = this.application.id && ['SUBMITTED', 'ANALYSED'].includes(this.application.status);
          this.changeDetector.markForCheck();
        }else{
          this.router.navigateByUrl('social-support/applications');
        }
      });
  }

  openCancelApplicationConfirmationModal() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_MODAL_TITLE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelApplication(),
    });
  }

  backToApplicationStatusHistory() {
    this.router.navigateByUrl('social-support/application-status-history');
  }

  private cancelApplication() {
    this.isCancelLoading = true;
    this.changeStatusService.applicationCancel(this.application.id)
      .pipe(first(), finalize(() => this.isCancelLoading = false))
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_SUCCESS')
        );
        this.router.navigateByUrl('social-support');
      });
  }

  private getAuxData() {
    return {
      course_degree: this.application.course_degree.name,
      course: this.application.course.name,
      school: this.application.school.name,
      organic_units: (this.application.organic_units || []).map((organicUnit) => organicUnit.organic_unit.name),
      organic_units_ids: (this.application.organic_units || []).map((organicUnit) => organicUnit.id),
      preferred_activities: (this.application.preferred_activities || []).map((a) => a.activity.name),
      preferred_activities_ids: (this.application.preferred_activities || []).map((a) => a.activity.id),
    };
  }
}
