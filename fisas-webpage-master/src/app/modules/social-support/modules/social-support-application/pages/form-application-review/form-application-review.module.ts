import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormApplicationReviewComponent } from './form-application-review.component';
import { FormApplicationReviewRoutingModule } from './form-application-review-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '@fi-sas/webpage/modules/social-support/modules/shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [FormApplicationReviewComponent],
  imports: [CommonModule, FormApplicationReviewRoutingModule, SharedModule, SharedSocialSupportModule],
})
export class FormApplicationReviewModule {}
