import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';

import { ApplicationSubmittedService } from './../../../application-status-history/services/application-submitted.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { environment } from './../../../../../../../environments/environment';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { SocialSupportApplicationsService } from '@fi-sas/webpage/modules/social-support/modules/social-support-application/services/social-support-applications.service';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import {
  ActivitiesLocationsComponent,
  AttachmentsComponent,
  ComplementaryInformationComponent,
  ContactsComponent,
  IdentificationComponent,
  LinguisticSkillsComponent,
  PastCollaborationsComponent,
  SkillsProfessionalExperienceComponent,
  SocialBenefitsComponent,
  SuccessApplicationSubmittedComponent,
  TimeAvailabilityComponent,
} from '../../components';

@Component({
  selector: 'app-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less'],
})
export class FormApplicationComponent implements OnInit, OnDestroy {
  @ViewChild('activitiesLocationsForm') activitiesLocationsForm: ActivitiesLocationsComponent;
  @ViewChild('attachmentsForm') attachmentsForm: AttachmentsComponent;
  @ViewChild('complementaryInformationForm') complementaryInformationForm: ComplementaryInformationComponent;
  @ViewChild('contactsForm') contactsForm: ContactsComponent;
  @ViewChild('identificationForm') identificationForm: IdentificationComponent;
  @ViewChild('linguisticSkillsForm') linguisticSkillsForm: LinguisticSkillsComponent;
  @ViewChild('pastCollaborationsForm') pastCollaborationsForm: PastCollaborationsComponent;
  @ViewChild('skillsProfessionalExperienceForm')
  skillsProfessionalExperienceForm: SkillsProfessionalExperienceComponent;
  @ViewChild('socialBenefitsForm') socialBenefitsForm: SocialBenefitsComponent;
  @ViewChild('timeAvailabilityForm') timeAvailabilityForm: TimeAvailabilityComponent;

  user: UserModel = null;
  institute = environment.institute;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  successFormModal: NzModalRef = null;

  buttonContinueStyle = {
    width: '100%',
    height: '50px',
    overflow: 'hidden',
    'text-overflow': 'ellipsis',
    'font-size': '17.5px',
    'margin-bottom': '31.5px',
  };

  buttonBackStyle = {
    width: 'auto',
    height: '30px',
    'font-size': '15px',
    'margin-top': '33px',
    'margin-bottom': '20.5px',
  };

  review = false;
  loadingSubmit = false;

  application: SocialSupportApplicationModel = null;
  showErrorMessage = false;
  auxData = {};

  translations: TranslationModel[] = [];

  isLoadingApplication: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private applicationSubmittedService: ApplicationSubmittedService,
    private authService: AuthService,
    private modalService: NzModalService,
    private router: Router,
    private socialSupportApplicationsService: SocialSupportApplicationsService
  ) {
    if (!isNaN(this.activatedRoute.snapshot.params.id)) {
      this.getApplication(this.activatedRoute.snapshot.params.id);
    }
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  validAllForms(): boolean {
    const activitiesLocationsFormValid = this.activitiesLocationsForm.validForm();
    const attachmentsFormValid = this.attachmentsForm.validForm();
    const complementaryInformationFormValid = this.complementaryInformationForm.validForm();
    const contactsFormValid = this.contactsForm.validForm();
    const identificationFormValid = this.identificationForm.validForm();
    const linguisticSkillsFormValid = this.linguisticSkillsForm.validForm();
    const pastCollaborationsFormValid = this.pastCollaborationsForm.validForm();
    const skillsProfessionalExperienceFormValid = this.skillsProfessionalExperienceForm.validForm();
    const socialBenefitsFormValid = this.socialBenefitsForm.validForm();
    const timeAvailabilityFormValid = this.timeAvailabilityForm.validForm();
    return (
      activitiesLocationsFormValid &&
      attachmentsFormValid &&
      complementaryInformationFormValid &&
      contactsFormValid &&
      identificationFormValid &&
      linguisticSkillsFormValid &&
      pastCollaborationsFormValid &&
      skillsProfessionalExperienceFormValid &&
      socialBenefitsFormValid &&
      timeAvailabilityFormValid
    );
  }

  goToReview() {
    if (this.validAllForms()) {
      if (!this.application || !this.application.id) {
        this.application = new SocialSupportApplicationModel();
      }

      this.showErrorMessage = false;
      const activitiesLocations = this.activitiesLocationsForm.getFormValues();
      const attachments_ids = this.attachmentsForm.getFormValues();
      const complementaryInformation = this.complementaryInformationForm.getFormValues();
      const contacts = this.contactsForm.getFormValues();
      const identification = this.identificationForm.getFormValues();
      const linguisticSkills = this.linguisticSkillsForm.getFormValues();
      const pastCollaborations = this.pastCollaborationsForm.getFormValues();
      const skillsProfessionalExperience = this.skillsProfessionalExperienceForm.getFormValues();
      const socialBenefits = this.socialBenefitsForm.getFormValues();
      const timeAvailability = this.timeAvailabilityForm.getFormValues();

      if (linguisticSkills.language_skills) {
        if (linguisticSkills.language_skills.includes('OTHER')) {
          const idx = linguisticSkills.language_skills.indexOf('OTHER');
          linguisticSkills.language_skills.splice(idx, 1);
        }
      }

      this.auxData = Object.assign(this.auxData, this.identificationForm.getEducationInformation());
      this.auxData = Object.assign(this.auxData, this.activitiesLocationsForm.getFormOptions());

      this.application.mentor_or_mentee = 'MENTEE';

      this.application.academic_year = identification.academic_year;
      this.application.course_id = identification.course_id;
      this.application.course_year = parseInt(identification.course_year, 10);
      this.application.iban = identification.iban;
      this.application.course_degree_id = identification.course_degree_id;
      this.application.school_id = identification.school_id;

      this.application.student_address = identification.address;
      this.application.student_birthdate = identification.birth_date;
      this.application.student_code_postal = identification.postal_code;
      this.application.student_identification = identification.identification;
      this.application.student_location = identification.city;
      this.application.student_name = identification.name;
      this.application.student_nationality = identification.nationality;
      this.application.student_number = identification.student_number;
      this.application.student_tin = identification.tin;
      this.application.genre = identification.gender;

      this.application.student_email = contacts.email;
      this.application.student_mobile_phone = contacts.phone.toString();

      this.application.has_scholarship = socialBenefits.has_scholarship;
      this.application.has_applied_scholarship = socialBenefits.has_applied_scholarship;
      this.application.scholarship_monthly_value = socialBenefits.has_scholarship
        ? socialBenefits.scholarship_monthly_value
        : 0;
      this.application.has_emergency_fund = socialBenefits.has_emergency_fund;
      this.application.is_waiting_scholarship_response = socialBenefits.is_waiting_scholarship_response;
      this.application.housed_residence = socialBenefits.housed_residence;
      this.application.other_scholarship = socialBenefits.other_scholarship;
      this.application.other_scholarship_values = socialBenefits.other_scholarship_values;

      this.application.preferred_schedule_ids = timeAvailability.preferred_schedule;
      this.application.has_holydays_availability = timeAvailability.has_holydays_availability;

      this.application.has_collaborated_last_year = pastCollaborations.has_collaborated_last_year;
      this.application.previous_activity_description = pastCollaborations.previous_activity_description;

      this.application.has_profissional_experience = skillsProfessionalExperience.has_profissional_experience;
      this.application.foreign_languages = skillsProfessionalExperience.foreign_languages;
      this.application.language_skills = skillsProfessionalExperience.foreign_languages
        ? skillsProfessionalExperience.language_skills
        : null;
      this.application.type_of_company = skillsProfessionalExperience.has_profissional_experience
        ? skillsProfessionalExperience.type_of_company
        : '';
      this.application.job_at_company = skillsProfessionalExperience.has_profissional_experience
        ? skillsProfessionalExperience.job_at_company
        : '';
      this.application.job_description = skillsProfessionalExperience.has_profissional_experience
        ? skillsProfessionalExperience.job_description
        : '';
      this.application.foreign_languages = linguisticSkills.foreign_languages;
      this.application.language_skills = linguisticSkills.language_skills;
      if (linguisticSkills.other_language_skills) {
        if (!this.application.language_skills) {
          this.application.language_skills = [];
        }
        this.application.language_skills.push(linguisticSkills.other_language_skills);
      }

      this.application.organic_units_ids = activitiesLocations.organic_units_ids;
      this.application.preferred_activities_ids = activitiesLocations.preferred_activities_ids;

      this.application.observations = complementaryInformation.observations;
      this.application.file_ids = attachments_ids;

      this.review = true;
      window.scrollTo(0, 0);
    } else {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
    }
  }

  submitApplication() {
    const applicationToSubmit: SocialSupportApplicationModel = Object.assign({}, this.application);
    this.loadingSubmit = true;
    if (applicationToSubmit.has_scholarship === false) {
      applicationToSubmit.scholarship_monthly_value = null;
    }
    if (applicationToSubmit.has_profissional_experience === false) {
      applicationToSubmit.type_of_company = null;
      applicationToSubmit.job_at_company = null;
      applicationToSubmit.job_description = null;
    }
    if (applicationToSubmit.foreign_languages) {
      applicationToSubmit.language_skills = applicationToSubmit.language_skills;
    }
    if (applicationToSubmit.has_collaborated_last_year === false) {
      applicationToSubmit.previous_activity_description = null;
    }
    if (applicationToSubmit.observations === '') {
      applicationToSubmit.observations = ' ';
    }

    const isEdit = !applicationToSubmit || !!applicationToSubmit.id;

    let action = this.socialSupportApplicationsService.createSocialSupportApplication(applicationToSubmit);
    if (isEdit) {
      action = this.socialSupportApplicationsService.editSocialSupportApplication(applicationToSubmit);
    }

    action.pipe(first()).subscribe(
      () => {
        this.loadingSubmit = false;
        this.applicationSubmittedService.saveSubmitted(true);
        this.successFormModal = this.modalService.create({
          nzWidth: 350,
          nzMaskClosable: false,
          nzClosable: true,
          nzContent: SuccessApplicationSubmittedComponent,
          nzComponentParams: { isEdit },
          nzFooter: null,
        });
        this.subscriptions = this.successFormModal.afterClose.subscribe(() =>
          this.router.navigateByUrl('social-support/application-status-history')
        );
      },
      () => {
        this.loadingSubmit = false;
      }
    );
  }

  backToForm() {
    this.review = false;
  }

  back(): void {
    /*this.changeComponentService.factoryComponent({
      index: this.changeComponentService.getCurrentIndex(),
      component: null,
    });*/
    this.router.navigateByUrl('social-support/experiences');
  }

  private getApplication(id: number) {
    this.isLoadingApplication = true;
    this.socialSupportApplicationsService
      .getApplication(id)
      .pipe(
        first(),
        finalize(() => (this.isLoadingApplication = false))
      )
      .subscribe((result) => {
        if (!['ANALYSED', 'SUBMITTED'].includes(result.data[0].status)) {
          return this.router.navigateByUrl('/unauthorized');
        }
        this.application = result.data[0];
        this.application.observations = this.application.observations === ' ' ? '' : this.application.observations;
        this.application.file_ids = this.application.files;
      });
  }
}
