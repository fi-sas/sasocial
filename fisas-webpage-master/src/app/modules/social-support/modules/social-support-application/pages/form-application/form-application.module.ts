import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormApplicationComponent } from './form-application.component';
import { FormSocialSupportRoutingModule } from './form-application-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedSocialSupportModule } from '../../../shared-social-support/shared-social-support.module';
import {
  ActivitiesLocationsComponent,
  AttachmentsComponent,
  ComplementaryInformationComponent,
  ContactsComponent,
  IdentificationComponent,
  LinguisticSkillsComponent,
  PastCollaborationsComponent,
  SkillsProfessionalExperienceComponent,
  SocialBenefitsComponent,
  SuccessApplicationSubmittedComponent,
  TimeAvailabilityComponent,
} from '../../components';

@NgModule({
  declarations: [
    ActivitiesLocationsComponent,
    AttachmentsComponent,
    ComplementaryInformationComponent,
    ContactsComponent,
    FormApplicationComponent,
    IdentificationComponent,
    LinguisticSkillsComponent,
    PastCollaborationsComponent,
    SkillsProfessionalExperienceComponent,
    SocialBenefitsComponent,
    SuccessApplicationSubmittedComponent,
    TimeAvailabilityComponent,
  ],
  imports: [CommonModule, FormSocialSupportRoutingModule, SharedModule, SharedSocialSupportModule],
  entryComponents: [SuccessApplicationSubmittedComponent],
})
export class FormSocialSupportModule {}
