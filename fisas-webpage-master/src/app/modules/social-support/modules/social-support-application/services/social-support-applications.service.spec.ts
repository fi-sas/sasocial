import { TestBed } from '@angular/core/testing';

import { SocialSupportApplicationsService } from './social-support-applications.service';

describe('SocialSupportApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialSupportApplicationsService = TestBed.get(SocialSupportApplicationsService);
    expect(service).toBeTruthy();
  });
});
