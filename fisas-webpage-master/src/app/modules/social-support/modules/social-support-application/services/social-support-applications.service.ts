import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable, Subject } from 'rxjs';
import { SocialSupportApplicationModel } from '@fi-sas/webpage/modules/social-support/models/social-support-application.model';
import { HttpParams } from '@angular/common/http';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SocialSupportApplicationsService {

  loading = false;

  public isApplicationActive: Subject<any> = new Subject<any>();

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private authService: AuthService
  ) {}

  createSocialSupportApplication(
    socialSupport: SocialSupportApplicationModel
  ): Observable<Resource<SocialSupportApplicationModel>> {
    delete socialSupport.user;
    return this.resourceService.create<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      socialSupport
    );
  }

  editSocialSupportApplication(
    socialSupport: SocialSupportApplicationModel
  ): Observable<Resource<SocialSupportApplicationModel>> {
    delete socialSupport.user;
    return this.resourceService.patch<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_ID', { id: socialSupport.id }),
      socialSupport
    );
  }

  applicationsUsers(): Observable<Resource<SocialSupportApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('user_id', this.authService.getUser().id.toString());
    return this.resourceService.list<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS'),
      { params }
    );
  }

  applicationActiveObservable() {
    return this.isApplicationActive.asObservable();
  }

  isInclude(status: string, arrayStatus: string[]): boolean {
    return arrayStatus.includes(status);
  }

  getApplication(id: number): Observable<Resource<SocialSupportApplicationModel>> {
    return this.resourceService.read<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATIONS_ID', { id })
    );
  }
}
