import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ActivityModel } from '@fi-sas/webpage/modules/social-support/models/activity.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  list():
    Observable<Resource<ActivityModel>> {

    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', 'name');
    params = params.set('query[active]', 'true');

    return this.resourceService.list<ActivityModel>(this.urlService.get('SOCIAL_SUPPORT.ACTIVITIES'), { params });
  }
}
