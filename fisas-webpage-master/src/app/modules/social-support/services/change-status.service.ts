import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ChangeStatusModel } from '../models/change-status.model';
import { SocialSupportApplicationModel } from '../models/social-support-application.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChangeStatusService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  applicationAccept(
    id: number
  ): Observable<Resource<SocialSupportApplicationModel>> {
    const status = new ChangeStatusModel();
    status.notes = 'Aceite e pelo utilizador';
    status.event = 'ACKNOWLEDGE';
    return this.resourceService.create<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATION_STATUS', { id }),
      status
    );
  }

  applicationCancel(
    id: number,
    notes?: ChangeStatusModel
  ): Observable<Resource<SocialSupportApplicationModel>> {
    return this.resourceService.create<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATION_CANCEL', { id }),
      notes
    );
  }

  applicationWithdraw(
    id: number,
    notes: ChangeStatusModel
  ): Observable<Resource<SocialSupportApplicationModel>> {
    return this.resourceService.create<SocialSupportApplicationModel>(
      this.urlService.get('SOCIAL_SUPPORT.APPLICATION_WITHDRAW', { id }),
      notes
    );
  }
}
