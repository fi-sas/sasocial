import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { ChangeStatusModel } from "../models/change-status.model";
import { ExperienceCandiate } from "../models/experience-candiate.model";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import { formContestModel } from "../models/form-contest.model";

import {
  IHasInterest,
  InterestModel,
  INTEREST_STATUS,
  InterviewModel,
} from "../models/interest.model";
import { SelfEvaluation } from "../models/self-evaluation.model";
import { SendDispatchData } from "../models";
import { UserInterestModel } from "../modules/experiences/models/user-interest.model";

@Injectable({
  providedIn: "root",
})
export class InterestService {
  constructor(
    private authService: AuthService,
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  userInterest(experienceId: number): Observable<Resource<UserInterestModel>> {
    const userInterest = new UserInterestModel();
    userInterest.experience_id = experienceId;
    return this.resourceService.create<UserInterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST", {}),
      userInterest
    );
  }

  userInterestColaboration(experience_id): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set("query[experience_id]", experience_id);
    params = params.set("query[status]", INTEREST_STATUS.COLABORATION);
    params = params.set("sort", "created_at");
    return this.resourceService.list<InterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCE_ID_USER_INTERESTS", {
        id: experience_id,
      }),
      { params }
    );
  }

  listInterest(): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set(
      "withRelated",
      "experience,interview,contract_file,history"
    );
    params = params.append("query[status]", INTEREST_STATUS.SUBMITTED);
    params = params.append("query[status]", INTEREST_STATUS.ANALYSED);
    params = params.append("query[status]", INTEREST_STATUS.INTERVIEWED);
    params = params.append("query[status]", INTEREST_STATUS.APPROVED);
    params = params.append("query[status]", INTEREST_STATUS.WAITING);
    params = params.append("query[status]", INTEREST_STATUS.NOT_SELECTED);
    params = params.append("query[status]", INTEREST_STATUS.ACCEPTED);
    params = params.append("query[status]", INTEREST_STATUS.COLABORATION);
    params = params.append("query[status]", INTEREST_STATUS.WITHDRAWAL);
    params = params.append("query[status]", INTEREST_STATUS.DECLINED);
    return this.resourceService.list<InterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST"),
      {
        params,
      }
    );
  }

  getListCandidates(
    id: number,
    pageIndex: number,
    pageSize: number
  ): Observable<Resource<ExperienceCandiate>> {
    let params = new HttpParams();
    params = params.set("offset", ((pageIndex - 1) * pageSize).toString());
    params = params.set("limit", pageSize.toString());
    params = params.set(
      "withRelated",
      "experience,interview,contract_file,history,application,historic_applications,historic_colaborations"
    );
    return this.resourceService.list<ExperienceCandiate>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCE_ID_USER_INTERESTS", {
        id,
      }),
      { params }
    );
  }

  getInterestById(id: number): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set(
      "withRelated",
      "experience,history,contract_file,report_avaliation_file,certificate_file,certificate_generated"
    );
    return this.resourceService.read<InterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST_ID", {
        id,
      }),
      { params }
    );
  }

  checkInterest(id: number): Observable<Resource<IHasInterest>> {
    return this.resourceService.read<IHasInterest>(
      this.urlService.get("SOCIAL_SUPPORT.CHECK_INTEREST", { id })
    );
  }

  canExpressInterest(id: number): Observable<{
    canExpresssInteres: boolean;
    hasApplication: boolean;
    applicationActive: boolean;
  }> {
    if (
      !this.authService.hasPermission(
        "social_scholarship:experience-user-interest:create"
      ) ||
      !this.authService.hasPermission("sasocial:is_student")
    ) {
      return of({
        canExpresssInteres: false,
        hasApplication: false,
        applicationActive: false,
      });
    }
    return this.checkInterest(id).pipe(
      map((result: Resource<IHasInterest>) => {
        return {
          canExpresssInteres:
            result.data[0].can_express_interest &&
            this.authService.hasPermission(
              "social_scholarship:experience-user-interest:create"
            ),
          hasApplication: result.data[0].has_application,
          applicationActive: result.data[0].has_application_accepted,
        };
      })
    );
  }

  changeStatusInterest(data: any, id: number) {
    return this.resourceService.create<any>(
      this.urlService.get("SOCIAL_SUPPORT.STATUS_CHANGE_INTEREST", { id }),
      data
    );
  }

  changeStatusInterestCancel(id: number) {
    return this.resourceService.create<any>(
      this.urlService.get("SOCIAL_SUPPORT.STATUS_CHANGE_INTEREST_CANCEL", {
        id,
      }),
      {}
    );
  }

  changeStatusInterestComplain(id: number, data: formContestModel) {
    return this.resourceService.create<formContestModel>(
      this.urlService.get("SOCIAL_SUPPORT.STATUS_CHANGE_INTEREST_COMPLAIN", {
        id,
      }),
      { data }
    );
  }

  changeStatusInterestScheduleInterview(data: any, id: number) {
    return this.resourceService.create<formContestModel>(
      this.urlService.get("SOCIAL_SUPPORT.STATUS_CHANGE_SCHEDULE_ENTERVIEW", {
        id,
      }),
      data
    );
  }

  listInterestClosedCancelled(): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set("withRelated", "experience,history");
    params = params.append("query[status]", INTEREST_STATUS.CLOSED);
    params = params.append("query[status]", INTEREST_STATUS.CANCELLED);
    params = params.append(
      "query[status]",
      INTEREST_STATUS.WITHDRAWAL_ACCEPTED
    );
    return this.resourceService.list<InterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST"),
      {
        params,
      }
    );
  }

  listInterestInColaboration(): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set("withRelated", "experience,history");
    params = params.append("status", INTEREST_STATUS.COLABORATION);
    return this.resourceService.list<InterestModel>(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST"),
      {
        params,
      }
    );
  }

  widthrawFromCollaboration(id: number, data: ChangeStatusModel) {
    return this.resourceService.create(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCE_WITHDRAW_ID", { id }),
      data
    );
  }

  collaborationEvaluation(id: number, data: SelfEvaluation) {
    return this.resourceService.create(
      this.urlService.get("SOCIAL_SUPPORT.EXPERIENCE_EVALUATION_ID", { id }),
      data
    );
  }

  addInterviewReport(
    id: number,
    data: InterviewModel
  ): Observable<Resource<InterviewModel>> {
    return this.resourceService.create(
      this.urlService.get(
        "SOCIAL_SUPPORT.EXPERIENCES_USER_INTEREST_INTERVIEW_REPORT_ID",
        { id }
      ),
      data
    );
  }

  sendDispatch(id: number, data: SendDispatchData) {
    return this.resourceService.create<InterviewModel>(
      this.urlService.get("SOCIAL_SUPPORT.STATUS_CHANGE_INTEREST", { id }),
      data
    );
  }
}
