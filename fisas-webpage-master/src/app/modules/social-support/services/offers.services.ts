import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';

import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ExperienceModel } from '../models/experiences.model';
import { OfferSubmitData } from '../modules/offers/models';

@Injectable({
  providedIn: 'root',
})
export class OffersService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }



  listOffersByResponsible(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.append('query[status]', 'SUBMITTED');
    params = params.append('query[status]', 'RETURNED');
    params = params.append('query[status]', 'ANALYSED');
    params = params.append('query[status]', 'APPROVED');
    params = params.append('query[status]', 'PUBLISHED');
    params = params.append('query[status]', 'REJECTED');
    params = params.append('query[status]', 'SEND_SEEM');
    params = params.append('query[status]', 'EXTERNAL_SYSTEM');
    params = params.append('query[status]', 'SELECTION');
    params = params.append('query[status]', 'IN_COLABORATION');
    params = params.append('query[status]', 'DISPATCH');
    params = params.append('withRelated', 'translations');
    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.GET_RESPONSIBLE'), { params }
    );
  }


  listOffersByResponsibleClosedCancelled(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.append('query[status]', 'CLOSED');
    params = params.append('query[status]', 'CANCELLED');
    params = params.append('withRelated', 'translations');
    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.GET_RESPONSIBLE'), { params }
    );
  }

  listOffersByAdvisor(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.append('query[status]', 'SUBMITTED');
    params = params.append('query[status]', 'RETURNED');
    params = params.append('query[status]', 'ANALYSED');
    params = params.append('query[status]', 'APPROVED');
    params = params.append('query[status]', 'PUBLISHED');
    params = params.append('query[status]', 'REJECTED');
    params = params.append('query[status]', 'SEND_SEEM');
    params = params.append('query[status]', 'EXTERNAL_SYSTEM');
    params = params.append('query[status]', 'SELECTION');
    params = params.append('query[status]', 'IN_COLABORATION');
    params = params.append('query[status]', 'DISPATCH');
    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.GET_ADVISOR'), { params }
    );
  }


  listOffersByAdvisorClosedCancelled(): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.append('query[status]', 'CLOSED');
    params = params.append('query[status]', 'CANCELLED');
    return this.resourceService.list<ExperienceModel>(
      this.urlService.get('SOCIAL_SUPPORT.GET_ADVISOR'), { params }
    );
  }

  changeStatusExperienceCancel(id: number) {
    return this.resourceService.create<any>(
      this.urlService.get('SOCIAL_SUPPORT.CANCEL_EXPERIENCE', {id}),
      { event: 'CANCEL' }
    );
  }

  getOffer(id: number): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.set(
      'withRelated',
      'attachment_file,current_account,experience_advisor,experience_responsible,organic_unit'
    );
    return this.resourceService.read<ExperienceModel>(this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_ID', {id}));
  }

  create(data: OfferSubmitData) {
    return this.resourceService.create<any>(this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES'), data);
  }

  edit(id: number, data: OfferSubmitData) {
    return this.resourceService.update<any>(this.urlService.get('SOCIAL_SUPPORT.EXPERIENCES_ID', { id }), data);
  }

  printOffer(id: number): Observable<Resource<any>> {
    return this.resourceService.list<any>(this.urlService.get('SOCIAL_SUPPORT.PRINT_EXPERIENCE_ID', { id }));
  }
}
