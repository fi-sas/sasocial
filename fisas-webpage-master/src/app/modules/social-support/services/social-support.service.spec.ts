import { TestBed } from '@angular/core/testing';

import { SocialSupportService } from './social-support.service';

describe('SocialSupportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialSupportService = TestBed.get(SocialSupportService);
    expect(service).toBeTruthy();
  });
});
