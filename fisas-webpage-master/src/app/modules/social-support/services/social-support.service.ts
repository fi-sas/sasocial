import { Injectable } from '@angular/core';

import { ConfigurationModel, ExternalEntity } from '@fi-sas/webpage/shared/models';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocialSupportService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) { }

  externalEntities() {
    return this.resourceService.list<ExternalEntity>(this.urlService.get('SOCIAL_SUPPORT.EXTERNAL_ENTITIES'));
  }

  getConfigs(): Observable<Resource<ConfigurationModel>> {
    return this.resourceService.list<ConfigurationModel>(this.urlService.get('SOCIAL_SUPPORT.CONFIGURATIONS'));
  }
}
