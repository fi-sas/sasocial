import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WithdrawalService {

  withdrawal = new BehaviorSubject(false);
  cancel = new BehaviorSubject(false);

  constructor() {}

  withdrawalObservable(): Observable<boolean> {
    return this.withdrawal.asObservable();
  }

  submittedWithdrawal(submitted: boolean) {
    this.withdrawal.next(submitted);
  }

  cancelObservable(): Observable<boolean> {
    return this.cancel.asObservable();
  }

  submittedCancel(submitted: boolean) {
    this.cancel.next(submitted);
  }

}
