import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SocialSupportComponent } from './social-support.component';

const routes: Routes = [
  {
    path: '',
    component: SocialSupportComponent,
    children: [
      { path: '', redirectTo: 'experiences', pathMatch: 'full' },
      {
        path: 'applications',
        loadChildren: 'src/app/modules/social-support/modules/social-support-application/pages/form-application/form-application.module#FormSocialSupportModule'
      },
      {
        path: 'applications/review/:id',
        loadChildren: 'src/app/modules/social-support/modules/social-support-application/pages/form-application-review/form-application-review.module#FormApplicationReviewModule'
      },
      {
        path: 'application-status-history',
        loadChildren: 'src/app/modules/social-support/modules/application-status-history/pages/application-status-history/application-status-history.module#ApplicationStatusHistorySocialSupportModule'
      },
      {
        path: 'express-interest-status-history',
        loadChildren: 'src/app/modules/social-support/modules/express-interest-status-history/pages/express-interest-status-history/express-interest-status-history.module#ExpressInterestStatusHistorySocialSupportModule'
      },
      {
        path: 'offers-status-history',
        loadChildren: 'src/app/modules/social-support/modules/offers-status-history/pages/offers-status-history/offers-status-history.module#OffersStatusHistorySocialSupportModule'
      },
      {
        path: 'record/:id',
        loadChildren: 'src/app/modules/social-support/modules/attendances/pages/record-attendance/record-attendance.module#RecordAttendanceModule'
      },
      {
        path: 'experiences',
        loadChildren: 'src/app/modules/social-support/modules/experiences/pages/experiences/experiences.module#SocialSupportExperiencesModule'
      },
      {
        path: 'experiences/:id',
        loadChildren: 'src/app/modules/social-support/modules/experiences/pages/experience-detail/experiences-detail.module#ExperiencesDetailSocialSupportModule'
      },
      {
        path: 'offers',
        loadChildren: 'src/app/modules/social-support/modules/offers/offers.module#OffersModule'
      },
      {
        path: 'management-attendances/:id',
        loadChildren: 'src/app/modules/social-support/modules/attendances/pages/management-attendances/management-attendances.module#ManagementAttendancesModule'
      },
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SocialSupportRoutingModule {}
