import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialSupportComponent } from './social-support.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SocialSupportComponent', () => {
  let component: SocialSupportComponent;
  let fixture: ComponentFixture<SocialSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SocialSupportComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        FiCoreModule,
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
