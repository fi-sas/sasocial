import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';

@Component({
  selector: 'app-social-support',
  templateUrl: './social-support.component.html',
  styleUrls: ['./social-support.component.less']
})

export class SocialSupportComponent implements OnInit {
  data: any[];
  barOptions: any[] = [];

  constructor(
    private configurationsService: ConfigurationsService, private authService: AuthService) {
  }

  ngOnInit() {
    this.data = this.configurationsService.getLocalStorageTotalConfiguration();
    this.barOptions = [
      {
        translate: this.validTitleTraductions(17),
        disable: true,
        routerLink: '/social-support/experiences',
        type: 'array',
        scope: this.authService.hasPermission('social_scholarship:experiences:list')
      },
      {
        translate: 'SOCIAL_SUPPORT_MENU.FIELD2',
        disable: false,
        routerLink: '/social-support/application-status-history',
        type: 'string',
        scope: this.authService.hasPermission('sasocial:is_student') && this.authService.hasPermission('social_scholarship:applications:list')
      },
      {
        translate: 'SOCIAL_SUPPORT_MENU.FIELD3',
        disable: false,
        routerLink: '/social-support/express-interest-status-history',
        type: 'string',
        scope: this.authService.hasPermission('sasocial:is_student') && this.authService.hasPermission('social_scholarship:experience-user-interest:create')
      },
      {
        translate: 'SOCIAL_SUPPORT_MENU.FIELD4',
        disable: false,
        routerLink: '/social-support/offers-status-history',
        type: 'string',
        scope: this.authService.hasPermission('social_scholarship:experiences:my_offers')
      },

    ];
  }

  validTitleTraductions(id: number) {
    return this.data.find(t => t.id === id) != null && this.data.find(t => t.id === id) != undefined ? this.data.find(t => t.id === id).translations : '';
  }



}
