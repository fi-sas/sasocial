import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocialSupportComponent } from '@fi-sas/webpage/modules/social-support/social-support.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SocialSupportRoutingModule } from '@fi-sas/webpage/modules/social-support/social-support-routing.module';
import { SharedSocialSupportModule } from './modules/shared-social-support/shared-social-support.module';

@NgModule({
  declarations: [SocialSupportComponent],
  imports: [CommonModule, SharedModule, SocialSupportRoutingModule, SharedSocialSupportModule],
  entryComponents: [],
})
export class SocialSupportModule {}
