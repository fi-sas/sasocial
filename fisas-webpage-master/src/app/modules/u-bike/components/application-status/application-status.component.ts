import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApplicationModel, Status } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { finalize, first } from 'rxjs/operators';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { hasOwnProperty } from 'tslint/lib/utils';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-application-status',
  templateUrl: './application-status.component.html',
  styleUrls: ['./application-status.component.less']
})
export class ApplicationStatusComponent implements OnInit {
  @Input() translations: TranslationModel[] = [];
  userApplications: ApplicationModel[] = [];
  @Input() userApplicationsHistory: ApplicationModel[] = [];
  @Input() applicantStartDate = '';
  @Input() applicantEndDate = '';
  @Output() applicationId = new EventEmitter();
  panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px'
      }
    }

  ];
  

  applicationIds = 0;
  applicationFuntion = '';
  loading = false;


  constructor(
    private applicationsService: ApplicationsService,
    private userService: AuthService,
    ) { }

  ngOnInit() {
    this.getUserApplication();
  }

  getUserApplication() {
    this.loading = true;
    this.applicationsService.readUserApplication().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(applications => {
      this.userApplications = applications.data.filter((application)=> {
        return application.status != 'withdrawal' && application.status != 'closed' && application.status != 'cancelled' && application.status != 'rejected'
      });
    });
  }



  /**
   * Is Date Before Today
   * @param endDate - Application end date
   */
  isDateBeforeToday(endDate) {
    return new Date(endDate) < new Date();
  }

  viewApplication(id: number) {
    if(id) {
      this.applicationId.emit(id);
    }
  }

  refresh(event) {
    if(event == true) {
      this.getUserApplication();
    }
  }
}
