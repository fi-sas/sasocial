import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { hasOwnProperty } from 'tslint/lib/utils';
import { ApplicationModel, Status } from '../../models/application.model';
import { first, finalize } from 'rxjs/operators';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.less']
})
export class ApplicationsComponent implements OnInit {

  userApplications: ApplicationModel[] = [];
  loading: boolean =  false;
  button = {
    'text-transform': 'uppercase'
  };
  status = Status;
  isVisible = false;

  applicationId: 0;
  applicationFuntion = '';

  constructor(
    private applicationsService: ApplicationsService,
  ) { }

  ngOnInit() {
    this.getUserApplication();
  }

  getUserApplication() {
    this.loading = true;
    this.applicationsService.readUserApplication().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(applications => {
      this.userApplications = applications.data.filter((application)=> {
        return application.status != 'withdrawal' && application.status != 'closed' && application.status != 'cancelled' && application.status != 'rejected'
      });
    });
  }

  refresh(event) {
    if(event == true) {
      this.getUserApplication();
    }
  }
}
