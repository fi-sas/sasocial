import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyFormButtonComponent } from './apply-form-button.component';

describe('ApplyFormButtonComponent', () => {
  let component: ApplyFormButtonComponent;
  let fixture: ComponentFixture<ApplyFormButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplyFormButtonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyFormButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
