import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-apply-form-button',
  templateUrl: './apply-form-button.component.html',
  styleUrls: ['./apply-form-button.component.less'],
})
export class ApplyFormButtonComponent {
  canApply: boolean;

  isLoading: boolean;

  constructor(
    private applicationsService: ApplicationsService,
    private authService: AuthService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService
  ) {
    this.checkCanApply();
  }

  private checkCanApply() {
    this.applicationsService
      .readApplicationCanAplay()
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((data) => {
        const can_apply = data.data[0];
        if (can_apply.can) {
          this.authService
            .isProfileDataCompleted(this.router, this.translateService, this.modalService)
            .then((result) => (this.canApply = result))
            .catch(() => (this.canApply = false));
        }
      });
  }
}
