import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { first, finalize } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ApplicationModel, Status } from '../../models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';

@Component({
  selector: 'app-applications-card-ubike',
  templateUrl: './card-application.component.html',
  styleUrls: ['./card-application.component.less']
})
export class ApplicationsCardComponent {
  @Input() view: boolean;
  @Input() history: boolean;
  @Input() applicantEndDate = '';
  @Input() userApplication: ApplicationModel;
  @Output() refresh = new EventEmitter();
  @Output() viewApplication = new EventEmitter();
  loading: boolean = false;
  status = Status;

  applicationId: 0;
  applicationFuntion = '';

  constructor(
    private applicationsService: ApplicationsService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService,
  ) { }

  checkTagStatusColor() {
    const css = {};
    css[this.userApplication.status] = true;
    return css;
  }


  rejectApplication() {
    this.loading = true;
    this.applicationsService.clientReject(this.applicationId)
      .pipe(
        first(),
        finalize(
          () => {
            this.loading = false;
          }
        )
      )
      .subscribe(
        (result) => {
          this.refresh.emit(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.ERROR')
          );
        }
      );
  }

  acceptApplication() {
    this.loading = true;
    this.applicationsService.clientAccept(this.applicationId)
      .pipe(
        first(),
        finalize(
          () => {
            this.loading = false;
          }
        )
      )
      .subscribe(
        (result) => {
          this.refresh.emit(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.ERROR')
          );
        }
      );
  }

  cancelApplication() {
    this.loading = true;
    this.applicationsService.cancel(this.applicationId)
      .pipe(
        first(),
        finalize(
          () => {
            this.loading = false;
          }
        )
      )
      .subscribe(
        (result) => {
          this.refresh.emit(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR')
          );
        }
      );
  }

  quitApplication() {
    this.loading = true;
    this.applicationsService.quit(this.applicationId)
      .pipe(
        first(),
        finalize(
          () => {
            this.loading = false;
          }
        )
      )
      .subscribe(
        (result) => {
          this.refresh.emit(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.QUIT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.QUIT.ERROR')
          );
        }
      );
  }

  withdrawQuitApplication() {
    this.loading = true;
    this.applicationsService.stay(this.applicationId)
      .pipe(
        first(),
        finalize(
          () => {
            this.loading = false;
          }
        )
      )
      .subscribe(
        (result) => {
          this.refresh.emit(true);
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAW_QUIT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAW_QUIT.ERROR')
          );
        }
      );
  }

  openModalConfirm(id, applicationFuntion) {
    this.applicationId = id;
    this.applicationFuntion = applicationFuntion;
    this.modalService.confirm({
      nzTitle: applicationFuntion == 'accept' ? this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.TITLE_ACCEPT') :
        applicationFuntion == 'reject' || applicationFuntion == 'quit' ? this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.TITLE_REJECT') :
          applicationFuntion == 'cancel' ? this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.TITLE_CANCEL') :
            applicationFuntion == 'withdraw_quit' ? this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.TITLE_WITHDRAWAL_CANCEL') : '',
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.executeFuntion()
    });
  }


  executeFuntion(): void {
    switch (this.applicationFuntion) {
      case 'accept':
        this.acceptApplication();
        break;
      case 'reject':
        this.rejectApplication();
        break;
      case 'cancel':
        this.cancelApplication();
        break;

      case 'quit':
        this.quitApplication();
        break;
      case 'withdraw_quit':
        this.withdrawQuitApplication();
        break;
      default:
        break;
    }
  }

  viewApplicationDetail(id: number) {
    if (this.view) {
      this.viewApplication.emit(id);
    } else {
      this.router.navigateByUrl('u-bike/application');
      localStorage.setItem('valueIdApplication', id.toString());
    }
  }

  continueApplication(id: number) {
    this.router.navigateByUrl(`u-bike/application/form/${id}`);
  }
}
