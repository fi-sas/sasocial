import { Component, Input, OnInit } from '@angular/core';

import { hasOwnProperty } from 'tslint/lib/utils';
import { finalize, first } from 'rxjs/operators';

import { TripModel } from '@fi-sas/webpage/modules/u-bike/models/trip.model';
import { ApplicationModel, Status } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { DashboardModel } from '@fi-sas/webpage/modules/u-bike/models/dashboard.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TripsService } from '@fi-sas/webpage/modules/u-bike/services/trips.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { DashboardOtherTypesModel } from '../../models/dashboard-other-types.model';


@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.less']
})

export class CounterComponent implements OnInit {

  @Input() userApplication: ApplicationModel = null;
  @Input() translations: TranslationModel[] = [];
  dashboard: DashboardModel = null;
  mode: string = '';
  month: Date;
  year: Date;
  date: Date;
  status = Status;
  hasContracted = false;
  otherDashboard: DashboardOtherTypesModel = null;
  buttonDetails = {
    width: '165px',
    height: '32px',
    'text-align': 'center',
    'font-size': '12px',
    'font-weight': 'bold',
    'line-height': '1.5'
  };

  buttonAddKm = {
    width: '270px',
    height: '48px',
    'text-align': 'center',
    'font-size': '17px',
    'line-height': '1.41',
    'font-weight': 'bold'
  };

  loadingDetails = false;
  loadingCounter = false;
  trips: TripModel[] = [];

  counter = true;
  details = false;
  manualRegistration = false;
  maintenance = false;
  ubikeId: number;
  userId: number;

  radioValue = 'ever';

  panelCo2 = {
    active: false,
    disabled: false,
    icon: '',
    customStyle: {
      background: 'transparent',
      border: '0px',
      'margin-bottom': '24px'
    }
  };

  constructor(
    private userService: AuthService,
    private tripsService: TripsService,
    private authService: AuthService,
    private uiService: UiService,
    private translateService: TranslateService,
    private applicationService: ApplicationsService
  ) { }

  ngOnInit() {
    this.getUserApplication();
  }

  changeRadio() {
    this.year = null;
    this.month = null;
    this.date = null;
    if(this.radioValue == 'year') {
      this.mode = 'year';
      this.year = new Date();
    }else if(this.radioValue == 'month') {
      this.mode = 'month';
      this.month = new Date();
    }else if(this.radioValue == 'day') {
      this.mode = 'day';
      this.date = new Date();
    }else {
      this.mode = '';
    }
    this.getUserDashboard();
  }

  getUserDashboard() {
    let year;
    let month;
    if(this.year) {
      year = this.year.getFullYear();
    }else if(this.month) {
      year = this.month.getFullYear();
      month = this.month.getMonth();
    }
    this.loadingCounter = true;
    this.applicationService.dashboard(this.radioValue, year, month, this.date)
      .pipe(
        first(),
        finalize(
          () => {
            this.loadingCounter = false;
          }
        )
      )
      .subscribe(
        (data) => {
          if (data.data.length) {
            this.dashboard = data.data[0];
          }
          this.getOtherTypes();
        }
      );
  }

  convertco2(value) {
    if(value<=1000) {
      return (value).toFixed(2) + ' g';
    }else {
      return (value/1000).toFixed(2) + ' kg';
    }
  }

  convertCal(value) {
    if(value<=1000) {
      return (value).toFixed(2) + ' cal';
    }else {
      return (value/1000).toFixed(2) + ' kcal';
    }
  }

  seeDetails() {
    this.loadingDetails = true;
    const user = this.authService.getUser();

    let firstDayMonth = new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString();
    let lastDayMonth = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString();
    let fisrtDayMonthFormat = moment(firstDayMonth).toISOString();
    let lastDayMonthFormat = moment(lastDayMonth).toISOString();
    if (hasOwnProperty(user, 'id')) {
      this.tripsService.readUserTrips(user.id, -1, fisrtDayMonthFormat, lastDayMonthFormat)
      .pipe(
        first()
      ).subscribe(
        (trips) => {
          this.trips = trips.data;
          this.loadingDetails = false;
          this.counter = false;
          this.details = true;
          this.scrollTop();
        }
      );
    }
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }

  numberToDecimalString(dist) {
    return Number(dist).toFixed(2);
  }

  addKm() {
      this.hasContracted = false;
      this.applicationService.readUserApplication()
        .pipe(
          first(),
          finalize(() => { })
        )
        .subscribe(
          (applications) => {
            for (const application of applications.data) {
              if (application.status === this.status.CONTRACTED) {
                this.hasContracted = true;
              }
            }

            if (this.hasContracted) {
              this.scrollTop();
              this.counter = false;
              this.manualRegistration = true;
            } else {
              this.uiService.showMessage(
                MessageType.warning,
                this.translateService.instant('U_BIKE.DASHBOARD.BUTTONS.ADD_KM_ERROR')
              );
            }
          }
        );
  }

  cancelAddKm(isCancel) {
    if (isCancel) {
      this.getUserDashboard();
      this.counter = true;
      this.manualRegistration = false;
    }
  }

  back(isBack) {
    if (isBack) {
      this.getUserDashboard();
      this.counter = true;
      this.details = false;
    }
  }

  maintenanceRequest() {
    this.counter = false;
    this.maintenance = true;
  }

  cancelMaintenance(isCancel) {
    if (isCancel) {
      this.counter = true;
      this.maintenance = false;
    }
  }



  getUserApplication() {
    //const user = this.userService.getUser();

    //if (hasOwnProperty(user, 'id')) {
      this.hasContracted = false;
      this.applicationService.readUserApplication()
        .pipe(
          first(),
          finalize(() => { })
        )
        .subscribe(
          (applications) => {
            for (const application of applications.data) {
              if (application.status == this.status.CONTRACTED) {
                this.hasContracted = true;
                this.ubikeId= application.bike_id;                
              }
            }
            this.getUserDashboard();
          }
        );
    //}
  }

  getOtherTypes() {
    if(this.ubikeId) {
      let year;
      let month;
      if(this.year) {
        year = this.year.getFullYear();
      }else if(this.month) {
        year = this.month.getFullYear();
        month = this.month.getMonth();
      }
      this.applicationService.getOtherTypes(this.ubikeId, this.radioValue, year, month, this.date).pipe(first()).subscribe((data)=> {
        this.otherDashboard = data.data[0];
      })
    }
    
  }
}
