import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SimpleFilesFormComponent } from './simple-files-form.component';



describe('SimpleFilesFormComponent', () => {
  let component: SimpleFilesFormComponent;
  let fixture: ComponentFixture<SimpleFilesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleFilesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleFilesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
