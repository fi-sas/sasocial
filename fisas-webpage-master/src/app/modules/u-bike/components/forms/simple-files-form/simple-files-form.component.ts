import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { ApplicationFormModel } from '@fi-sas/webpage/modules/u-bike/models/application-form.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { TranslateService } from '@ngx-translate/core';
import { hasOwnProperty } from 'tslint/lib/utils';
import { first, finalize } from 'rxjs/operators';
import { UploadFile } from 'ng-zorro-antd';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

@Component({
  selector: 'app-simple-files-form',
  templateUrl: './simple-files-form.component.html',
  styleUrls: ['./simple-files-form.component.less']
})
export class SimpleFilesFormComponent implements OnInit {

  @Output() cancelRequest = new EventEmitter();
  @Input() typeForm: string = '';
  @Input() application_id: number;
  @Input() formEdit = null;

  loading = false;
  formSimple: FormGroup;
  submitted: boolean = false;
  sendForm: ApplicationFormModel = new ApplicationFormModel();
  previewImage: string | undefined = '';
  previewVisible = false;
  file: Array<FileList> = [];
  dayCurrent: Date;

  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];


  constructor(
    private translateService: TranslateService,
    private uiService: UiService,
    private formBuilder: FormBuilder,
    private userService: AuthService,
    private router: Router,
    private applicationsService: ApplicationsService,
    private filesServices: FilesService
  ) { }

  ngOnInit() {
    this.formSimple = this.formBuilder.group({
      date: [''],
      description: ['', [Validators.required, trimValidation]],
      file_id: [null],
    });
    this.dayCurrent = new Date();

    if (this.formEdit) {
      this.formSimple.get('date').patchValue(this.formEdit.begin_date ? this.formEdit.begin_date : '');
      this.formSimple.get('description').patchValue(this.formEdit.request_description ? this.formEdit.request_description : '');
      this.formSimple.get('file_id').patchValue(this.formEdit.file_id ? this.formEdit.file_id : null);
    }
  }

  get f() { return this.formSimple.controls; }


  disableStartDate = (startDate: Date) => {
    if(this.typeForm == 'private_maintenance' || this.typeForm == 'breakdown' || this.typeForm == 'theft') {
      return;
    }
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(startDate).isBefore(currentDate) ;
  }

  cancel() {
    this.cancelRequest.emit(true);
    this.router.navigate(['u-bike', 'forms']);
  }

  async submitRequest() {
    this.submitted = true;
    if (this.formSimple.valid) {
      if (this.file[0]){
        if (this.file[0].length > 0){
          for (let i = 0; i < this.file[0].length; i++) {
            await this.uploadFile(this.file[0][i]);
          }
          this.createForms();
      }
     }else{
       this.createForms();
     }

    }
  }

  uploadFile(file: File) {
    this.loading = true;
    const formData = new FormData();
    formData.append('file', file);
    formData.append('name', file.name);

    this.filesServices.createFile(formData).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(() => {
    });

  }

  createForms() {
    if (!this.userService.hasPermission('u_bike:application-forms:create')) {
      return;
    }

    this.loading = true;
    this.sendForm.application_id = this.application_id;
    this.sendForm.begin_date = this.formSimple.get('date').value ? this.formSimple.get('date').value : this.dayCurrent;
    this.sendForm.request_description = this.formSimple.get('description').value;
    this.sendForm.subject = this.typeForm == 'theft' ? 'THEFT' :
                            this.typeForm == 'maintenance_request' ? 'MAINTENANCE_REQUEST' :
                            this.typeForm == 'private_maintenance' ? 'PRIVATE_MAINTENANCE' :
                            this.typeForm == 'breakdown' ? 'BREAKDOWN' : '';
    this.sendForm.file_id = this.formSimple.get('file_id').value;
    if (!this.formEdit) {
      
      this.applicationsService.createForms(this.sendForm).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('U_BIKE.FORMS.SUBMITTED')
        );
        this.cancelRequest.emit(true);

      });

    }else {
      this.sendForm.status = this.formEdit.status;
      this.applicationsService.updateForms(this.formEdit.id, this.sendForm).pipe(
        first(),
        finalize(() => this.loading = false)
      ).subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('U_BIKE.FORMS.SUBMITTED')
        );
        this.cancelRequest.emit(true);

      });
    }
  }

  handlePreview = async (file: UploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);

    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };


  listFiles(event) {
    this.file = [];
    if (event.fileList) {
      this.file.push(event.fileList);
    }

  }
}


