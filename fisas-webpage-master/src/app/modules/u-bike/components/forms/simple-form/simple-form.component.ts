import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { first, finalize } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { ApplicationFormModel } from '../../../models/application-form.model';
import { ApplicationsService } from '../../../services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.less'],
})
export class SimpleFormComponent {
  @Output() cancelRequest = new EventEmitter();
  @Input() application_id: number;
  @Input() formEdit = null;

  private _typeForm: string = '';
  @Input() set typeForm(typeForm: string) {
    this._typeForm = typeForm;
    const fields = {
      description: ['', [Validators.required, trimValidation]]
    };
    if (typeForm !== 'complaint') {
      fields['startDate'] = [''];
      fields['endDate'] = [''];
    }
    this.initForm(fields);
  }
  get typeForm() {
    return this._typeForm;
  }

  loading = false;
  formSimple: FormGroup;
  submitted: boolean = false;
  sendForm: ApplicationFormModel = new ApplicationFormModel();
  dayCurrent: Date;

  constructor(
    private applicationsService: ApplicationsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService,
    private userService: AuthService,
  ) {}

  initForm(fields: { [key: string]: any }) {
    this.formSimple = this.formBuilder.group(fields);

    this.dayCurrent = new Date();

    if (this.formEdit) {
      this.formSimple.get('startDate').patchValue(this.formEdit.begin_date ? this.formEdit.begin_date : '');
      this.formSimple.get('endDate').patchValue(this.formEdit.end_date ? this.formEdit.end_date : '');
      this.formSimple
        .get('description')
        .patchValue(this.formEdit.request_description ? this.formEdit.request_description : '');
    }
  }

  get f() {
    return this.formSimple.controls;
  }

  disableEndDate = (endDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(endDate).isBefore(currentDate) || moment(this.formSimple.get('startDate').value).isAfter(endDate);
  };

  disableStartDate = (startDate: Date) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    return moment(startDate).isBefore(currentDate) || moment(this.formSimple.get('endDate').value).isBefore(startDate);
  };

  cancel() {
    this.cancelRequest.emit(true);
    this.router.navigate(['u-bike', 'forms']);
  }

  submitRequest() {
    this.submitted = true;
    if (this.formSimple.valid) {
      this.createForms();
    }
  }

  createForms() {
    if (!this.userService.hasPermission('u_bike:application-forms:create')) {
      return;
    }

    this.loading = true;
    this.sendForm.application_id = this.application_id;
    if (this.typeForm === 'absence') {
      if (this.formSimple.contains('startDate')) {
        this.sendForm.begin_date = this.formSimple.get('startDate').value
        ? this.formSimple.get('startDate').value
        : this.dayCurrent;
      }
      if (this.formSimple.contains('endDate')) {
        this.sendForm.end_date = this.formSimple.get('endDate').value
          ? this.formSimple.get('endDate').value
          : this.dayCurrent;
      }
      this.sendForm.request_description = this.formSimple.get('description').value;
      this.sendForm.subject = 'ABSENCE';
    } else if (this.typeForm === 'complaint') {
      this.sendForm.request_description = this.formSimple.get('description').value;
      this.sendForm.subject = 'COMPLAINT';
    } else {
      if (this.formSimple.contains('startDate')) {
        this.sendForm.begin_date = this.formSimple.get('startDate').value
          ? this.formSimple.get('startDate').value
          : this.dayCurrent;
      }
      this.sendForm.request_description = this.formSimple.get('description').value;
      this.sendForm.subject =
        this.typeForm === 'withdrawal'
          ? 'WITHDRAWAL'
          : this.typeForm === 'renovation'
          ? 'RENOVATION'
          : this.typeForm === 'add_maintenance_kit'
          ? 'ADDITIONAL_MAINTENANCE_KIT'
          : '';
    }
    if (!this.formEdit) {
      this.applicationsService
        .createForms(this.sendForm)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(MessageType.success, this.translateService.instant('U_BIKE.FORMS.SUBMITTED'));
          this.cancelRequest.emit(true);
        });
    } else {
      this.sendForm.status = this.formEdit.status;
      this.applicationsService
        .updateForms(this.formEdit.id, this.sendForm)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(MessageType.success, this.translateService.instant('U_BIKE.FORMS.SUBMITTED'));
          this.cancelRequest.emit(true);
        });
    }
  }
}
