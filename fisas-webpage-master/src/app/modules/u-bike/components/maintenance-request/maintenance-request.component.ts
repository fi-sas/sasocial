import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { MaintenancesService } from '@fi-sas/webpage/modules/u-bike/services/maintenances.service';
import { MaintenanceModel } from '../../models/maintenance.model';
import { hasOwnProperty } from 'tslint/lib/utils';
import { first } from 'rxjs/operators';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-maintenance-request',
  templateUrl: './maintenance-request.component.html',
  styleUrls: ['./maintenance-request.component.less']
})
export class MaintenanceRequestComponent implements OnInit {

  @Input() userApplication: ApplicationModel = null;
  @Output() cancelMaintenanceRequest = new EventEmitter();

  formMaintenanceRequest: FormGroup;
  submitted: boolean = false;
  loading = false;

  constructor(private maintenancesService: MaintenancesService,
              private translateService: TranslateService,
              private uiService: UiService,
              private authService: AuthService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formMaintenanceRequest = this.formBuilder.group({
      date: ['', [Validators.required]],
      description: ['', [Validators.required, trimValidation]],
    });
  }

  get f() { return this.formMaintenanceRequest.controls; }


  submitMaintenanceRequest() {

    if (!this.authService.hasPermission('u_bike:maintenances:create')) {
      this.uiService.showMessage(
        MessageType.error,
        'Utilizador sem permissões para executar ação'
      );
      return;
    }

    this.submitted = true;
    this.loading = true;
    let maintenance: MaintenanceModel = new MaintenanceModel();
    if(this.formMaintenanceRequest.valid){
      if (this.userApplication !== null) {

        if (hasOwnProperty(this.userApplication, 'bike_id')) {

             maintenance.bike_id = this.userApplication.bike_id;
             maintenance.description = this.formMaintenanceRequest.get('description').value;
             maintenance.created_at = this.formMaintenanceRequest.get('date').value;
             this.maintenancesService.create(maintenance).pipe(first()).subscribe(() => {
               this.uiService.showMessage(
                 MessageType.success,
                 this.translateService.instant('U_BIKE.DASHBOARD.MAINTENANCE_REQUEST.SUBMIT.SUCCESS')
               );
               this.loading = false;
             }, () => {
               this.loading = false;
             });
        } else {
          this.loading = false;
        }
     } else {
       this.uiService.showMessage(
         MessageType.error,
         this.translateService.instant('U_BIKE.DASHBOARD.MAINTENANCE_REQUEST.SUBMIT.WITHOUT_APPLICATION')
       );
       this.loading = false;
     }
    }else{
      this.loading = false;
    }
    
    
  }

  cancel() {
    this.cancelMaintenanceRequest.emit(true);
  }

}
