import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputType, InputOptions } from '@fi-sas/webpage/shared/interfaces/input-options';
import { TripsService } from '@fi-sas/webpage/modules/u-bike/services/trips.service';
import { first } from 'rxjs/operators';
import { TripModel } from '@fi-sas/webpage/modules/u-bike/models/trip.model';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { hasOwnProperty } from 'tslint/lib/utils';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { ApplicationDataService } from '@fi-sas/webpage/shared/services/application-data.service';
import * as moment from 'moment';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-manual-registration',
  templateUrl: './manual-registration.component.html',
  styleUrls: ['./manual-registration.component.less']
})
export class ManualRegistrationComponent implements OnInit {

  @Input() userApplication: ApplicationModel = null;
  @Input() trip: TripModel = null;
  @Output() cancelAddKmButton = new EventEmitter();

  formUbike: FormGroup = this.formBuilder.group({
    distance: [null, [Validators.required, Validators.pattern('\\d+(\\.\\d{1,2})?')]],
    date: [null, Validators.required],
    dateRepeate: ['noRep'],
    time: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
    timeStart: [null],
    timeEnd: [null],
    description: ['', [Validators.required,trimValidation]]
  });
  submitted: boolean = false;
  get f() { return this.formUbike.controls; }

  timeErrors = false;

  inputOptions: InputOptions = { type: InputType.none };
  loading = false;

  constructor(private tripsService: TripsService, private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private uiService: UiService,
    private applicationDataService: ApplicationDataService) { }

  ngOnInit() {
    if (this.trip !== null) {
      this.formUbike.get('distance').setValue(this.trip.distance);
      this.formUbike.get('date').setValue(this.trip.trip_start);
      if (this.trip.trip_end) {
        this.formUbike.get('timeStart').setValue(new Date(this.trip.trip_start));
        this.formUbike.get('timeEnd').setValue(new Date(this.trip.trip_end));
        this.formUbike.get('time').setValue(null);
        this.timeTabChange(1);
      } else {
        this.formUbike.get('time').setValue(this.trip.time);
      }
      this.formUbike.get('description').setValue(this.trip.description);
      this.applicationDataService.createApplication(this.trip);
    }
  }

  submitManualRegistration(trip: TripModel) {

    this.loading = true;
    if (this.userApplication !== null) {

      if (this.trip === null) {
        if (hasOwnProperty(this.userApplication, 'bike_id')) {
          trip.height_diff = 0;
          trip.distance = +trip.distance;
          trip.time = trip.time ? +trip.time : null;
        this.tripsService.create(trip).pipe(first()).subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('U_BIKE.DASHBOARD.MANUAL_REGISTRATION.SUBMIT.SUCCESS')
            );
            this.loading = false;
            this.cancelAddKmButton.emit(true);
            this.cancel();
          }, () => {
            this.loading = false;
          });
        } else {
          this.loading = false;
        }
      } else {
        this.applicationDataService.clearApplication();

        const tripUpdate = new TripModel();

        tripUpdate.distance = +trip.distance;
        tripUpdate.description = trip.description;
        tripUpdate.trip_start = trip.trip_start;
        if (this.selectedTab == 0) {
          tripUpdate.time = +trip.time;
          tripUpdate.trip_end = null;
        } else {
          tripUpdate.trip_end = trip.trip_end;
        }
        tripUpdate.bike_id = this.trip.bike_id;
        tripUpdate.height_diff = this.trip.height_diff;


        this.tripsService.update(this.trip.id, tripUpdate).pipe(first()).subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('U_BIKE.DASHBOARD.MANUAL_REGISTRATION.UPDATE.SUCCESS')
          );
          this.cancelAddKmButton.emit(true);
          this.loading = false;
        });
      }
    } else {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('U_BIKE.DASHBOARD.MANUAL_REGISTRATION.SUBMIT.WITHOUT_APPLICATION')
      );
      this.loading = false;
    }
  }

  cancel() {
    // por o clear form!!
    this.cancelAddKmButton.emit(true);
  }

  selectedTab = 0;
  timeTabChange(event) {
    this.selectedTab = event;
    if (event === 0) {
      this.formUbike.controls.time.clearValidators();
      this.formUbike.controls.time.setValidators([Validators.required, Validators.pattern('^[0-9]*$')]);
      this.formUbike.controls.time.updateValueAndValidity();
      this.formUbike.controls.timeStart.clearValidators();
      this.formUbike.controls.timeStart.setErrors(null);
      this.formUbike.controls.timeStart.clearValidators();
      this.formUbike.controls.timeEnd.clearValidators();
      this.formUbike.controls.timeEnd.setErrors(null);
      this.formUbike.controls.timeEnd.updateValueAndValidity();
    } else {
      this.formUbike.controls.timeStart.clearValidators();
      this.formUbike.controls.timeStart.setValidators(Validators.required);
      this.formUbike.controls.timeStart.updateValueAndValidity();
      this.formUbike.controls.timeEnd.clearValidators();
      this.formUbike.controls.timeEnd.setValidators(Validators.required);
      this.formUbike.controls.timeEnd.updateValueAndValidity();
      this.formUbike.controls.time.clearValidators();
      this.formUbike.controls.time.setErrors(null);
    }

  }
  submit() {

    this.submitted = true;

    if (this.formUbike.invalid) { return; }

    let trip: TripModel = new TripModel();
    if (this.trip) {
      trip = this.trip;
    }


    trip.trip_start = new Date(this.formUbike.get('date').value);
    trip.distance = this.formUbike.get('distance').value;
    trip.description = this.formUbike.get('description').value;
    if (this.selectedTab == 0) {
      trip.time = this.formUbike.get('time').value;
      trip.trip_end = null;
      trip.trip_start.setHours(0, 0, 0, 0);
    } else {
      trip.trip_start.setHours(
        (this.formUbike.get('timeStart').value).getHours(),
        (this.formUbike.get('timeStart').value).getMinutes(),
        0,
        0
      );
      trip.trip_end = new Date(trip.trip_start);
      trip.trip_end.setHours(
        (this.formUbike.get('timeEnd').value).getHours(),
        (this.formUbike.get('timeEnd').value).getMinutes(),
        0,
        0
      );
      trip.time = null;
    }


    if (!this.trip) {
      let ex: Date = new Date(trip.trip_start);
      let ex2: Date = new Date(trip.trip_end);

      if (this.formUbike.get('dateRepeate').value === 'noRep') {
        this.submitManualRegistration(trip);
      } else if (this.formUbike.get('dateRepeate').value === 'WEveryD') {
        for (let i = ex.getDay(); i < 7; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'MEveryW') {
        for (let i = ex.getMonth(); ex.getMonth() === i; ex.setDate(ex.getDate() + 7), ex2.setDate(ex2.getDate() + 7)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'MEveryWeekDays') {
        for (let i = ex.getDay(); i < 5; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'MEveryWeekend') {
        for (let i = ex.getDay(); i < 7; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          if (i > 4) {
            trip.trip_start = ex;
            if (this.selectedTab == 1)
              trip.trip_end = ex;
            this.submitManualRegistration(trip);
          }
        }
      } else if (this.formUbike.get('dateRepeate').value === 'MEveryD') {
        for (let i = ex.getMonth(); ex.getMonth() === i; ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plusW') {
        for (let i = 0; i < 7; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus1') {
        for (let i = 0; i < 1; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus2') {
        for (let i = 0; i < 2; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus3') {
        for (let i = 0; i < 3; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus4') {
        for (let i = 0; i < 4; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus5') {
        for (let i = 0; i < 5; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      } else if (this.formUbike.get('dateRepeate').value === 'plus6') {
        for (let i = 0; i < 6; i++, ex.setDate(ex.getDate() + 1), ex2.setDate(ex2.getDate() + 1)) {
          trip.trip_start = ex;
          if (this.selectedTab == 1)
            trip.trip_end = ex;
          this.submitManualRegistration(trip);
        }
      }

    } else {
      this.submitManualRegistration(trip);
    }
  }

  disableDate = (date: Date)=> {
    var currentDate = new Date();
    return moment(date).isAfter(currentDate);
  }

}
