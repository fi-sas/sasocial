import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { ApplicationsService } from '../../services/applications.service';
import { ApplicationModel, Status } from '../../models/application.model';



@Component({
  selector: 'app-occurrences',
  templateUrl: './occurrences.component.html',
  styleUrls: ['./occurrences.component.less']
})
export class OccurrencesComponent implements OnInit {

  buttonDetails = {
    width: '165px',
    height: '32px',
    'text-align': 'center',
    'font-size': '12px',
    'font-weight': 'bold',
    'line-height': '1.5'
  };

  status = Status;
  loading: boolean = false;
  userApplications: ApplicationModel[] = [];

  constructor(
    private applicationsService: ApplicationsService
  ) { }

  ngOnInit() {
    this.getReadForms();
  }

  getReadForms() {
    this.loading = true;
    this.loading = true;
    this.applicationsService.list(0,-1,true).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(applications => {
      this.userApplications = applications.data;
    });
  }

  checkTagStatusColor(application) {
    const css = {};
    css[application.status] = true;
    return css;
  }

  isPar(val) {
    return (val % 2 == 0);
  }
}
