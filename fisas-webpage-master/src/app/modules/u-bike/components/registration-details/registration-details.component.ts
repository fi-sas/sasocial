import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TripModel } from '@fi-sas/webpage/modules/u-bike/models/trip.model';
import { TripsService } from '@fi-sas/webpage/modules/u-bike/services/trips.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { hasOwnProperty } from 'tslint/lib/utils';
import { first } from 'rxjs/operators';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Component({
  selector: 'app-registration-details',
  templateUrl: './registration-details.component.html',
  styleUrls: ['./registration-details.component.less']
})

export class RegistrationDetailsComponent implements OnInit {

  @Input() trips: TripModel [] = [];
  @Input() userApplication: ApplicationModel = null;
  @Output() backToCounter = new EventEmitter();

  loadingTrip = false;
  edit = false;
  trip: TripModel = null;
  presentMonth = new Date().getMonth();
  dateSelected;
  presentYear = new Date().getFullYear();

  constructor(private tripsService: TripsService,
              private authService: AuthService,
              private translateService: TranslateService,
              private uiService: UiService) { }

  ngOnInit() {}

  editTrip(trip: TripModel) {
     this.trip = trip;
     this.edit = true;
  }

  cancelEditTrip(isCancel: boolean) {
    if (isCancel) {
      this.getTripsByMonth({year: this.presentYear, month: this.presentMonth});
      this.edit = false;
    }
  }

  deleteTrip(id: number) {
    if (!this.authService.hasPermission('u_bike:trips:delete')) {
      this.uiService.showMessage(
        MessageType.error,
        'Utilizador sem permissões para executar ação'
      );
      return;
    }

    this.tripsService.delete(id).pipe(first()).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('U_BIKE.DASHBOARD.MANUAL_REGISTRATION.DELETE.SUCCESS')
      );
      this.getTripsByMonth({year: this.presentYear, month: this.presentMonth});
    });
  }

  getTripsByMonth(date: {month: number, year: number}) {
      this.presentMonth = date.month;
      this.presentYear = date.year;
      this.loadingTrip = true;
      const firstDayMonth = new Date(this.presentYear, this.presentMonth, 1).toISOString();
      const lastDayMonth = new Date(this.presentYear, this.presentMonth + 1 , 0).toISOString();
      let fisrtDayMonthFormat = moment(firstDayMonth).toISOString();
      let lastDayMonthFormat = moment(lastDayMonth).toISOString();
      const user = this.authService.getUser();
      this.dateSelected = date;
      if (hasOwnProperty(user, 'id')) {
        this.tripsService.readUserTrips(user.id,  -1, fisrtDayMonthFormat, lastDayMonthFormat).pipe(first()).subscribe( trips => {
          this.trips = trips.data;
          this.loadingTrip = false;
        });
      }
  }

  numberToDecimalString(dist) {
    return Number(dist).toFixed(2);
  }

  back() {
    this.backToCounter.emit(true);
  }

}
