import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegulamentationsComponent } from './regulamentations.component';

describe('RegulamentationsComponent', () => {
  let component: RegulamentationsComponent;
  let fixture: ComponentFixture<RegulamentationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegulamentationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegulamentationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
