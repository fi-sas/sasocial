import { Component, OnInit } from '@angular/core';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { first } from 'rxjs/operators';
import { RegulamentationsModel } from '@fi-sas/webpage/modules/u-bike/models/regulamentations.model';

@Component({
  selector: 'app-regulamentations',
  templateUrl: './regulamentations.component.html',
  styleUrls: ['./regulamentations.component.less']
})
export class RegulamentationsComponent implements OnInit {

  regulamentations: RegulamentationsModel = null;
  loading = false;

  constructor(private applicationsService: ApplicationsService) { }

  ngOnInit() {
    this.getRegulamentations();
  }

  getRegulamentations() {
    this.loading = true;
    this.applicationsService.regulamentations().pipe(first()).subscribe(regulamentations => {
      this.regulamentations = regulamentations.data[0];
      this.loading = false;
    });
  }

}
