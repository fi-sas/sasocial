export class ApplicationCanAplayModel {
    can: boolean;
    reason: string;
}