export class ApplicationFormModel {
    subject: string;
    request_description: string;
    begin_date: Date;
    end_date: Date;
    application_id: number;
    file_id: number;
    status: string;
  }
