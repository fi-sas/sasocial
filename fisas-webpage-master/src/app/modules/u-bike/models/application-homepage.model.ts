import { TermsAndPolicyModel } from "@fi-sas/webpage/pages/regulations/models/terms-and-policy.model";


export class ApplicationHomepageModel {
  terms_conditions?: TermsAndPolicyModel[]
}
