import { BikeModel } from '@fi-sas/webpage/modules/u-bike/models/bike.model';
import { ContractModel } from './contract.model';
import { TypologyModel } from '@fi-sas/webpage/modules/u-bike/models/typology.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';

export class ApplicationModel {
  subject;
  request_description;
  id?: number;
  full_name: string;
  birth_date: string;
  gender: Gender;
  weight: number;
  height: number;
  address: string;
  postal_code: string;
  email: string;
  phone_number: string;
  student_number: string;
  start_date: string;
  end_date?: string;
  typology_first_preference_id: number;
  typology_first_preference: TypologyModel;
  other_typology_preference?: boolean;
  daily_commute: DailyCommute;
  daily_consumption: DailyConsumption;
  daily_kms: number;
  number_of_times: number;
  avg_weekly_distance: number;
  total_weekly_distance: number;
  travel_time_school: number;
  travel_time_school_bike: number;
  manufacturer?: string;
  model?: string;
  year_manufactured?: number;
  engine_power?: number;
  avg_vehicle_consumption_100?: number;
  applicant_consent: boolean;
  consent_date?: string;
  bike_id?: number;
  user_id?: number;
  status?: string;
  pickup_location?: string;
  updated_at?: string;
  created_at?: string;
  decision?: string;
  daily_co2?: number;
  bike?: BikeModel;
  contract?: ContractModel;
  // history?: HistoryModel[];
  answer?: string;
  answered?: boolean;
  preferred_pickup_organict_unit_id: number;
  preferred_pickup_organict_unit?: OrganicUnitModel;
  delivery_reception_file_id: number;
  consent_terms_file_id: number;
}

export enum Gender {
  MALE = 'M',
  FEMALE = 'F',
  OTHER = 'U',
}

export enum DailyCommute {
  WALK = 'Walk',
  CAR = 'Car',
  MOTORCYCLE = 'Motorcycle',
  PUBLIC_TRANSPORT = 'Public Transport',
  BUS = 'Bus',
  TRAIN = 'Train',
  BIKE = 'Bike',
  OTHER = 'Other'
}

export enum DailyConsumption {
  ELECTRIC = 'Electric',
  GPL = 'GPL',
  DIESEL = 'Diesel',
  GASOLINE = 'Gasoline'
}

export enum Status {
  WAITING_APPROVAL = 'Waiting Approval',
  APPROVED      = 'Approved',
  WITHDRAW      = 'Withdraw',
  CONFIRM       = 'Confirm',

  ELABORATION   = 'elaboration',
  SUBMITTED     = 'submitted',
  ANALYSIS      = 'analysis',
  CANCELLED     = 'cancelled',
  DISPATCH      = 'dispatch',
  ASSIGNED      = 'assigned',
  ENQUEUED      = 'enqueued',
  UNASSIGNED    = 'unassigned',
  SUSPENDED     = 'suspended',
  COMPLAINT     = 'complaint',
  ACCEPTED      = 'accepted',
  CONTRACTED    = 'contracted',
  REJECTED      = 'rejected',
  QUITING       = 'quiting',
  WITHDRAWAL    = 'withdrawal',
  CLOSE         = 'closed'
}
