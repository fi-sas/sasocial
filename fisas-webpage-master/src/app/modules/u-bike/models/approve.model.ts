import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';

export class ApproveModel {
  status: string;
  notes: string;
  application?: ApplicationModel;
}

export class ClientApproveModel {
  action: string;
  notes: string;
  application?: ApplicationModel;
}
