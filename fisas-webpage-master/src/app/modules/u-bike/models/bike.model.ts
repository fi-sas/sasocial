export class BikeModel {
  id: number;
  identification: string;
  size: string;
  bike_model_id: number;
  asset_id: number;
  created_at?: string;
  updated_at?: string;
}


