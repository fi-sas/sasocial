
export class ContractModel {
  category: Category;
  created_at: Date;
  deleted: boolean;
  deleted_at: Date;
  file_category_id: number;
  file_deleted: boolean;
  file_deleted_at: Date;
  height: any;
  id: number;
  mime_type: string;
  path: string;
  type: string;
  updated_at: Date;
  url: string;
  weight: number;
  width: number;
  translations: [];
  public: boolean;
}

class Category {
  created_at: Date;
  description: string;
  file_category_id: any;
  id: number;
  name: string;
  updated_at: Date;
}
