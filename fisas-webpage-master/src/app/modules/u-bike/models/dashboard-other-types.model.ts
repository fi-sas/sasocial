export class DashboardOtherTypesModel {
    aggregate_data: DataOtherTypesModel;
}

export class DataOtherTypesModel {
    eletric_saved_co2: number;
    bus_saved_co2: number;
    train_saved_co2: number;
    gasoline_saved_co2: number;
    diesel_saved_co2: number;
    gpl_saved_co2: number;
}