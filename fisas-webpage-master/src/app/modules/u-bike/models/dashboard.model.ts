export class DashboardModel {
    total_trips: number;
    total_kilometers: number;
    total_saved_co2: number;
    total_burned_calories: number;
    gained_height: number;
    max_gained_height: number;
    avg_time: number;
    avg_speed: number;
    watts: number;
    application_id: number;
    total_points: number;
    period: string;
    other_types: any
}