export class MaintenanceModel {
  id?: number;
  bike_id: number;
  user_id: number;
  description: string;
  created_at?: string;
  updated_at?: string;
}


