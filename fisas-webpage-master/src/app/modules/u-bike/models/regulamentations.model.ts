export class RegulamentationsModel {
  key: string;
  value: Value;
}

class Value {
  language_id: number;
  regulamentation: string[];
}
