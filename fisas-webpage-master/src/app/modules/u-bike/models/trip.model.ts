import { BikeModel } from '@fi-sas/webpage/modules/u-bike/models/bike.model';

export class TripModel {
  id?: number;
  bike_id: number;
  bike_typology_id?: number;
  typology_name?: string;
  manufacturer?: string;
  model?: string;
  fuel_type?: string;
  year_manufactured?: number;
  engine_power?: number;
  application_id?: number;
  user_id?: number;
  user_profile_id?: number;
  profile_name?: string;
  user_age?: number;
  distance: number;
  height_diff: number;
  energy?: number;
  co2_emissions_saved?: number;
  time: number;
  trip_start: Date;
  trip_end: Date;
  description: string;
  is_weekend?: boolean;
  is_holiday?: boolean;
  bike?: BikeModel;
  created_at?: string;
  updated_at?: string;
}


