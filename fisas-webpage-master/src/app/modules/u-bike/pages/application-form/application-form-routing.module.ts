import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationFormUbikeComponent } from './application-form.component';
import { ApplyFormResolver } from './resolvers';

const routes: Routes = [
  {
    path: '',
    component: ApplicationFormUbikeComponent,
    data: { breadcrumb: null, title: null, scope: 'u_bike:applications' },
    resolve: { response: ApplyFormResolver },
  },
  {
    path: ':id',
    component: ApplicationFormUbikeComponent,
    data: { breadcrumb: null, title: null, scope: 'u_bike:applications' },
    resolve: { response: ApplyFormResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [ApplyFormResolver],
  exports: [RouterModule],
})
export class ApplicationFormRoutingModule {}
