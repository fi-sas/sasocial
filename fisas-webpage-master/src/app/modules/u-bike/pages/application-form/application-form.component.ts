import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

import { Subject } from 'rxjs';
import { finalize, first, takeUntil } from 'rxjs/operators';
import { ApplicationsService } from '../../services/applications.service';

import { IStep } from './models';
import { StepsService } from './services';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.less'],
})
export class ApplicationFormUbikeComponent implements OnDestroy {
  readonly steps: IStep[] = [
    {
      title: 'U_BIKE.APPLICATIONS.STEP_ONE.CURRENT_SHIPPING',
      description: 'U_BIKE.APPLICATIONS.STEP.STEP_ONE',
    },
    {
      title: 'U_BIKE.APPLICATIONS.STEP_TWO.BICYCLE_REQUIRED',
      description: 'U_BIKE.APPLICATIONS.STEP.STEP_TWO',
    },
    {
      title: 'U_BIKE.APPLICATIONS.STEP_THREE.PHYSICAL_STATURE',
      description: 'U_BIKE.APPLICATIONS.STEP.STEP_THREE',
    },
    {
      title: 'U_BIKE.APPLICATIONS.STEP_FOUR.TERMS_OF_USE',
      description: 'U_BIKE.APPLICATIONS.STEP.STEP_FOUR',
    },
  ];

  currentStep: number = 0;

  isLoading: boolean;
  isSubmiting: boolean;
  isLoadingApplication: boolean;
  showErrorMessage: boolean;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private applicationsService: ApplicationsService,
    private stepsService: StepsService
  ) {
    this.isLoading = true;

    this.stepsService.reset();
    this.stepsService.setMaxSteps(this.steps.length);

    this.activatedRoute.data.pipe(first()).subscribe(({ response }) => {
      if (!response.success) {
        this.isLoading = false;
        return this.router.navigateByUrl('/unauthorized');
      }

      this.applicationsService
        .readApplicationCanAplay()
        .pipe(
          first(),
          finalize(() => (this.isLoading = false))
        )
        .subscribe((data) => {
          const can_apply = data.data[0];
          if (!can_apply.can) {
            return this.router.navigateByUrl('/unauthorized');
          }

          this.stepsService.currentStep$.pipe(takeUntil(this.destroy$)).subscribe((step) => (this.currentStep = step));
          this.stepsService.showErrorMessage$
            .pipe(takeUntil(this.destroy$))
            .subscribe((show) => (this.showErrorMessage = show));

          const applicationId = response.id;
          if (applicationId) {
            this.getApplication(parseInt(applicationId));
          } else {
            this.stepsService.setApplication(null);
            this.stepsService.setDataSoFar(null);
          }
        });
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  submitCurrrentStep() {
    this.stepsService.markStepForSubmission(this.currentStep);
  }

  onStepChange(index: number): void {
    this.stepsService.setCurrentStep(index);
  }

  private getApplication(applicationId: number) {
    this.isLoadingApplication = true;
    this.applicationsService
      .read(applicationId)
      .pipe(
        first(),
        finalize(() => (this.isLoadingApplication = false))
      )
      .subscribe((result) => {
        if ((result.data || []).length > 0) {
          this.stepsService.setApplication(result.data[0]);
          this.stepsService.setDataSoFar(result.data[0]);
        }
      });
  }
}
