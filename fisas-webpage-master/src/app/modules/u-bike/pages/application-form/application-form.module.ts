import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationFormRoutingModule } from './application-form-routing.module';
import { ApplicationFormUbikeComponent } from './application-form.component';
import { BaseFormApplicationStepComponent } from './components/base-form-application-step';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import {
  FormApplicationStepOneComponent,
  FormApplicationStepTwoComponent,
  FormApplicationStepThreeComponent,
  FormApplicationStepFourComponent,
} from './components';

@NgModule({
  declarations: [
    ApplicationFormUbikeComponent,
    FormApplicationStepOneComponent,
    FormApplicationStepTwoComponent,
    FormApplicationStepThreeComponent,
    FormApplicationStepFourComponent,
    BaseFormApplicationStepComponent,
  ],
  imports: [ApplicationFormRoutingModule, CommonModule, SharedModule],
})
export class ApplicationFormUbikeModule {}
