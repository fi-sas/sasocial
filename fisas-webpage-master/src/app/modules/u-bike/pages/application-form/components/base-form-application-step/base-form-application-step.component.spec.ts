import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFormApplicationStepComponent } from './base-form-application-step.component';

describe('BaseFormApplicationStepComponent', () => {
  let component: BaseFormApplicationStepComponent;
  let fixture: ComponentFixture<BaseFormApplicationStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BaseFormApplicationStepComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFormApplicationStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
