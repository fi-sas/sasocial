import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { filter, finalize, first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { StepsService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base-form-application-step',
  template: '',
  styleUrls: [],
})
export class BaseFormApplicationStepComponent implements OnInit, OnDestroy {
  form: FormGroup;
  stepIndex: number;
  application: ApplicationModel;

  protected destroy$: Subject<void> = new Subject<void>();

  constructor(
    protected applicationsService: ApplicationsService,
    protected stepsService: StepsService,
    protected router: Router,
  ) {}

  ngOnInit(): void {
    this.form = this.getForm();

    this.stepsService.dataSoFar$.pipe(takeUntil(this.destroy$)).subscribe((application) => {
      this.onDataChange(application);
      if (application) {
        this.markFilledWithErrorsAsTouched();
      }
    });

    this.stepsService.application$
      .pipe(takeUntil(this.destroy$))
      .subscribe((application) => (this.application = application));

    this.stepsService.stepToSubmit$
      .pipe(
        takeUntil(this.destroy$),
        filter((stepIndex) => stepIndex === this.stepIndex)
      )
      .subscribe(() => this.onSubmitStep());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  get controls() {
    return this.form.controls;
  }

  hasErrors(controlName: string): boolean {
    const fc = this.form.get(controlName);
    if (!fc) {
      return false;
    }
    return fc.enabled && fc.touched && fc.dirty && Object.keys(fc.errors || {}).length > 0;
  }

  protected onSubmitStep(): void {
    if (!this.isFormValid()) {
      return this.afterSubmitError();
    }

    this.stepsService.markStepForSubmission(null);
    this.stepsService.setIsSubmiting(true);

    const data: any = Object.assign({}, this.application || {}, this.getData());

    let action = null;
    if(this.stepsService.isStepToSubmit(this.stepIndex)) {
        data.ready_to_submit = true;
        action = this.applicationsService.update(this.application.id, data)
    } else {
        action = this.application && this.application.id
          ? this.applicationsService.update(this.application.id, data)
          : this.applicationsService.create(data);
    }

    action
      .pipe(
        first(),
        finalize(() => this.stepsService.setIsSubmiting(false))
      )
      .subscribe(
        (result) => this.afterSubmitSuccess(result.data[0]),
        () => this.afterSubmitError()
      );
  }

  private afterSubmitSuccess(application: ApplicationModel): void {
    if(application.status === "submitted") {
      this.router.navigate(['/', 'u-bike', 'application']);

    } else {
      this.stepsService.setApplication(application);
      this.stepsService.setDataSoFar(application);
      this.stepsService.setShowErrorMessage(false);
      this.stepsService.setCurrentStep(this.stepIndex + 1);
      this.scrollTop();
    }

  }

  private afterSubmitError(): void {
    this.stepsService.setShowErrorMessage(true);
    this.scrollTop();
  }

  private scrollTop() {
    window.scrollTo(0, 0);
  }

  protected isFormValid(): boolean {
    let valid = true;
    Object.keys(this.form.controls).forEach((key) => {
      const control = this.form.get(key);
      if (control && control.enabled) {
        control.markAsTouched();
        control.markAsDirty();
        control.updateValueAndValidity();
        valid = valid && (control.valid || control.disabled);
      }
    });
    return valid;
  }

  protected formValueAsNumber(value: string | number): number {
    if (value == null || value === '') {
      return;
    }
    if (typeof value === 'number') {
      return value;
    }
    return Number(value.replace(',', '.'));
  }

  protected floatAsString(value: number): string {
    return value ? value.toString().replace('.', ',') : '';
  }

  protected markFilledWithErrorsAsTouched() {
    Object.keys(this.form.controls).forEach((key) => {
      const formControl = this.form.get(key);
      if (formControl.value == null || Object.keys(formControl.errors || {}).length === 0) {
        return;
      }
      formControl.markAsTouched();
      formControl.markAsDirty();
    });
  }

  protected getData(): ApplicationModel {
    throw Error('Not yet implemented');
  }

  protected getForm(): FormGroup {
    throw Error('Not yet implemented');
  }

  protected onDataChange(application: ApplicationModel): void {
    throw Error('Not yet implemented');
  }
}
