import { FormGroup } from '@angular/forms';

import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';

export interface BaseFormApplicationStep {
  stepIndex: number;

  /**
   * Transforms the form values into a partial Application with only the current step fields
   *
   * @return {*}  {ApplicationModel}
   * @memberof BaseFormApplicationStep
   */
  getData(): ApplicationModel;

  /**
   * Returns the FormGroup instance for that step
   *
   * @return {*}  {FormGroup}
   * @memberof BaseFormApplicationStep
   */
  getForm(): FormGroup;

  /**
   * Transforms Application to step form values
   *
   * @param {ApplicationModel} application
   * @memberof BaseFormApplicationStep
   */
  onDataChange(application: ApplicationModel): void;
}
