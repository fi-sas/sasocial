import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplicationStepFourComponent } from './form-application-step-four.component';

describe('FormApplicationStepFourComponent', () => {
  let component: FormApplicationStepFourComponent;
  let fixture: ComponentFixture<FormApplicationStepFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormApplicationStepFourComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplicationStepFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
