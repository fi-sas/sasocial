import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';

import { ApplicationHomepageModel } from '@fi-sas/webpage/modules/u-bike/models/application-homepage.model';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { BaseFormApplicationStep, BaseFormApplicationStepComponent } from '../base-form-application-step';
import { StepsService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-application-step-four',
  templateUrl: './form-application-step-four.component.html',
  styleUrls: [
    '../base-form-application-step/base-form-application-step.component.less',
    './form-application-step-four.component.less',
  ],
})
export class FormApplicationStepFourComponent
  extends BaseFormApplicationStepComponent
  implements BaseFormApplicationStep {
  stepIndex: number = 3;

  loadingTerms = false;
  modalTermsInfo = false;
  terms: ApplicationHomepageModel = null;

  constructor(
    protected applicationsService: ApplicationsService,
    protected stepsService: StepsService,
    protected router: Router,
  ) {
    super(applicationsService, stepsService, router);
  }

  getForm(): FormGroup {
    return new FormGroup({
      applicant_consent: new FormControl('', [Validators.requiredTrue]),
    });
  }

  getData(): ApplicationModel {
    const application = new ApplicationModel();
    application.applicant_consent = this.form.get('applicant_consent').value;
    return application;
  }

  onDataChange(application: ApplicationModel): void {
    if (!application) {
      this.form.reset();
      return;
    }
    this.form.patchValue({ applicant_consent: application ? application.applicant_consent : null });
  }

  getTerms (){
    this.loadingTerms = true;
    this.applicationsService.getTerms().pipe(first(), finalize(()=> this.loadingTerms = false)).subscribe(response => {
      this.terms = response.data[0];
      this.modalTermsInfo = true;
    })
  }
}
