import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplicationStepOneComponent } from './form-application-step-one.component';

describe('FormApplicationStepOneComponent', () => {
  let component: FormApplicationStepOneComponent;
  let fixture: ComponentFixture<FormApplicationStepOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormApplicationStepOneComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplicationStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
