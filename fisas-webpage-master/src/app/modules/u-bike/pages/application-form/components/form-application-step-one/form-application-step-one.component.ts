import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { BaseFormApplicationStep, BaseFormApplicationStepComponent } from '../base-form-application-step';
import { StepsService } from '../../services';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-application-step-one',
  templateUrl: './form-application-step-one.component.html',
  styleUrls: [
    '../base-form-application-step/base-form-application-step.component.less',
    './form-application-step-one.component.less',
  ],
})
export class FormApplicationStepOneComponent
  extends BaseFormApplicationStepComponent
  implements BaseFormApplicationStep, OnInit {
  stepIndex: number = 0;

  disableFutureYears = (current: Date) => moment(current).isAfter(new Date());
  defaultTimeDurationOpenValue: Date;

  readonly dailyCommuteOptions: { label: string; value: string }[] = [
    { label: 'U_BIKE.DAILY_COMMUTE.WALK', value: 'Walk' },
    { label: 'U_BIKE.DAILY_COMMUTE.CAR', value: 'Car' },
    { label: 'U_BIKE.DAILY_COMMUTE.MOTORCYCLE', value: 'Motorcycle' },
    { label: 'U_BIKE.DAILY_COMMUTE.BUS', value: 'Bus' },
    { label: 'U_BIKE.DAILY_COMMUTE.TRAIN', value: 'Train' },
    { label: 'U_BIKE.DAILY_COMMUTE.BIKE', value: 'Bike' },
    { label: 'U_BIKE.DAILY_COMMUTE.OTHER', value: 'Other' },
  ];

  private readonly carMotorcycleFields: { key: string; validators: ValidatorFn[] }[] = [
    { key: 'manufacturer', validators: [trimValidation, Validators.required] },
    { key: 'model', validators: [trimValidation, Validators.required] },
    { key: 'daily_consumption', validators: [Validators.required] },
    { key: 'year_manufactured', validators: [Validators.required] },
    { key: 'engine_power', validators: [Validators.min(1), Validators.max(1000)] },
    { key: 'avg_vehicle_consumption_100', validators: [Validators.min(1), Validators.max(1000)] },
  ];

  constructor(
    private authService: AuthService,
    protected applicationsService: ApplicationsService,
    protected stepsService: StepsService,
    protected router: Router,
    ) {
      super(applicationsService, stepsService, router);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.defaultTimeDurationOpenValue = new Date();
    this.defaultTimeDurationOpenValue.setHours(0, 1);
    this.handleDependentFields();

    this.form.get('daily_commute').valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value) => this.onDailyCommuteChange(value));
  }

  getForm(): FormGroup {
    const controls = {
      avg_weekly_distance: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(100000)]),
      daily_commute: new FormControl('', [Validators.required]),
      daily_kms: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(1000)]),
      number_of_times: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(50)]),
      total_weekly_distance: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(1000000)]),
      travel_time_school_bike: new FormControl(null, [Validators.required]),
      travel_time_school: new FormControl(null, [Validators.required]),
    };
    this.carMotorcycleFields.forEach(
      (field) => (controls[field.key] = new FormControl({ value: null, disabled: true }))
    );
    return new FormGroup(controls);
  }

  getData(): ApplicationModel {
    const application = new ApplicationModel();
    application.avg_weekly_distance = this.formValueAsNumber(this.controls.avg_weekly_distance.value);
    application.daily_commute = this.controls.daily_commute.value;
    if (['Car', 'Motorcycle'].includes(application.daily_commute)) {
      application.avg_vehicle_consumption_100 = this.formValueAsNumber(this.controls.avg_vehicle_consumption_100.value);
      application.engine_power = this.formValueAsNumber(this.controls.engine_power.value);
      application.year_manufactured = new Date(this.controls.year_manufactured.value).getFullYear();
      application.daily_consumption = this.controls.daily_consumption.value;
      application.manufacturer = this.controls.manufacturer.value;
      application.model = this.controls.model.value;
    }
    application.daily_kms = this.formValueAsNumber(this.form.get('daily_kms').value);
    application.number_of_times = this.formValueAsNumber(this.form.get('number_of_times').value);
    application.total_weekly_distance = this.formValueAsNumber(this.form.get('total_weekly_distance').value);
    application.travel_time_school = this.dateToMinutes(this.form.get('travel_time_school').value);
    application.travel_time_school_bike = this.dateToMinutes(this.form.get('travel_time_school_bike').value);
    application.user_id = this.authService.getUser().id;
    return application;
  }

  onDataChange(application: ApplicationModel): void {
    if (!application) {
      this.form.reset();
      return;
    }
    this.form.patchValue({
      daily_commute: application.daily_commute,
      avg_weekly_distance: application.avg_weekly_distance,
      daily_kms: application.daily_kms,
      number_of_times: application.number_of_times,
      total_weekly_distance: application.total_weekly_distance,
      travel_time_school: this.minutesToDate(application.travel_time_school),
      travel_time_school_bike: this.minutesToDate(application.travel_time_school_bike),
    });
    if (['Car', 'Motorcycle'].includes(application.daily_commute)) {
      this.form.patchValue({
        avg_vehicle_consumption_100: application.avg_vehicle_consumption_100,
        engine_power: application.engine_power,
        year_manufactured:  moment().set('year' , application.year_manufactured).toDate(),
        daily_consumption: application.daily_consumption,
        manufacturer: application.manufacturer,
        model: application.model
      })
    }
  }

  validTime(value: Date) {
    return value && moment(value).format('HH:mm') === '00:00';
  }

  disabledDurationMinutes(hour: number): number[] {
    if (!hour) {
      return [0];
    }
    return [];
  }

  private handleDependentFields(): void {
    this.onDailyCommuteChange( this.form.get('daily_commute').value);
  }

  private onDailyCommuteChange(dailyCommute: string): void {
    const isCarMotorcycle = ['Car', 'Motorcycle'].includes(dailyCommute);
    this.carMotorcycleFields.forEach((field) => {
      const fc = this.form.get(field.key);
      if (fc) {
        if (isCarMotorcycle) {
          fc.setValidators(field.validators);
          fc.enable();
        } else {
          fc.clearValidators();
          fc.disable();
        }
        fc.updateValueAndValidity();
      }
    });
  }

  private dateToMinutes(value: Date): number {
    let hourString = moment(value).format('HH:mm').toString();
    let hourSplitter = hourString.split(':');
    if (hourSplitter[0] === '00') {
      return Number(hourSplitter[1]);
    }
    return Number(hourSplitter[0]) * 60 + Number(hourSplitter[1]);
  }

  private minutesToDate(value: number): Date {
    if (!value) {
      return;
    }
    const hours = parseInt(`${value / 60}`);
    const minutes = value % 60;
    const d = new Date();
    d.setHours(isNaN(hours) ? 0 : Math.max(0, Math.min(24, hours)), minutes);
    return d;
  }
}
