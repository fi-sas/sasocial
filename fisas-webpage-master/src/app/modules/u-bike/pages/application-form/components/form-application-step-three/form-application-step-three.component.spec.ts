import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplicationStepThreeComponent } from './form-application-step-three.component';

describe('FormApplicationStepThreeComponent', () => {
  let component: FormApplicationStepThreeComponent;
  let fixture: ComponentFixture<FormApplicationStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormApplicationStepThreeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplicationStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
