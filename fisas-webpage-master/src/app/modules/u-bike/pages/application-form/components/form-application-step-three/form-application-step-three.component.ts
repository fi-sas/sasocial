import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';

import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { StepsService } from '../../services';
import { BaseFormApplicationStep, BaseFormApplicationStepComponent } from '../base-form-application-step';

@Component({
  selector: 'app-form-application-step-three',
  templateUrl: './form-application-step-three.component.html',
  styleUrls: [
    '../base-form-application-step/base-form-application-step.component.less',
    './form-application-step-three.component.less',
  ],
})
export class FormApplicationStepThreeComponent
  extends BaseFormApplicationStepComponent
  implements BaseFormApplicationStep {
  stepIndex: number = 2;

  constructor(protected applicationsService: ApplicationsService, protected stepsService: StepsService,protected router: Router,
    ) {
      super(applicationsService, stepsService, router);
  }

  getForm(): FormGroup {
    return new FormGroup({
      weight: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(400)]),
      height: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(4)]),
    });
  }

  getData(): ApplicationModel {
    const application = new ApplicationModel();
    application.weight = this.formValueAsNumber(this.controls.weight.value);
    application.height = this.formValueAsNumber(this.controls.height.value);
    return application;
  }

  onDataChange(application: ApplicationModel): void {
    if (!application) {
      this.form.reset();
      return;
    }
    this.form.patchValue({
      weight: application.weight ? this.floatAsString(application.weight): null,
      height: application.height ? this.floatAsString(application.height): null,
    });
  }
}
