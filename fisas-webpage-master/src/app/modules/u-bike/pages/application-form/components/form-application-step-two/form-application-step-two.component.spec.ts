import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplicationStepTwoComponent } from './form-application-step-two.component';

describe('FormApplicationStepTwoComponent', () => {
  let component: FormApplicationStepTwoComponent;
  let fixture: ComponentFixture<FormApplicationStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormApplicationStepTwoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplicationStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
