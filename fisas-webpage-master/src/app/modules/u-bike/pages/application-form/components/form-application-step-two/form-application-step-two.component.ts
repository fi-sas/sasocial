import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { BaseFormApplicationStep, BaseFormApplicationStepComponent } from '../base-form-application-step';
import { StepsService } from '../../services';
import { TypologiesService } from '@fi-sas/webpage/modules/u-bike/services/typologies.service';
import { TypologyModel } from '@fi-sas/webpage/modules/u-bike/models/typology.model';
import { Router } from '@angular/router';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';

@Component({
  selector: 'app-form-application-step-two',
  templateUrl: './form-application-step-two.component.html',
  styleUrls: [
    '../base-form-application-step/base-form-application-step.component.less',
    './form-application-step-two.component.less',
  ],
})
export class FormApplicationStepTwoComponent
  extends BaseFormApplicationStepComponent
  implements BaseFormApplicationStep, OnInit {
  stepIndex: number = 1;

  dataTypology: TypologyModel[] = [];
  organicUnits: OrganicUnitModel[] = [];
  organicUnitsLoading: boolean = false;

  disableEndDate = (endDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);

    return (
      moment(endDate).isBefore(currentDate) ||
      moment(this.form.get('start_date').value).isAfter(endDate) ||
      moment(endDate).isAfter(this.form.get('end_date').value) ||
      moment(endDate).isBefore(this.form.get('start_date').value)
    );
  };

  disableStartDate = (startDate: Date) => {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);

    return (
      moment(startDate).isBefore(currentDate) ||
      moment(startDate).isAfter(this.form.get('end_date').value) ||
      moment(startDate).isBefore(this.form.get('start_date').value)
    );
  };

  constructor(
    private typologiesService: TypologiesService,
    protected applicationsService: ApplicationsService,
    protected stepsService: StepsService,
    protected router: Router,
    private organicUnitsService : OrganicUnitsService
    ) {
      super(applicationsService, stepsService, router);
    this.getListTypologies();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.getOrganicUnits();
    this.form
      .get('start_date')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => this.controlOverlapDates());
  }

  getOrganicUnits(){
    this.organicUnitsLoading = true;
    this.organicUnitsService.organicUnits().pipe(first(), finalize(() => this.organicUnitsLoading = false)).subscribe(response => {
      this.organicUnits = response.data;
    })
  }

  getForm(): FormGroup {
    return new FormGroup({
      typology_first_preference_id: new FormControl('', [Validators.required]),
      other_typology_preference: new FormControl(null),
      preferred_pickup_organict_unit_id: new FormControl(null, [Validators.required]),
      start_date: new FormControl('', [Validators.required]),
      end_date: new FormControl('', [Validators.required]),
    });
  }

  getData(): ApplicationModel {
    const application = new ApplicationModel();
    application.typology_first_preference_id = this.controls.typology_first_preference_id.value;
    application.other_typology_preference = this.controls.other_typology_preference.value;
    application.preferred_pickup_organict_unit_id = this.controls.preferred_pickup_organict_unit_id.value;
    application.start_date = this.controls.start_date.value;
    application.end_date = this.controls.end_date.value;
    return application;
  }

  onDataChange(application: ApplicationModel): void {
    if (!application) {
      this.form.reset();
      return;
    }
    this.form.patchValue({
      typology_first_preference_id: application.typology_first_preference_id,
      other_typology_preference: application.other_typology_preference,
      preferred_pickup_organict_unit_id: application.preferred_pickup_organict_unit_id,
      start_date: this.stringAsDate(application.start_date),
      end_date: this.stringAsDate(application.end_date),
    });
  }

  private getListTypologies() {
    this.typologiesService
      .list()
      .pipe()
      .subscribe(typologies =>  this.dataTypology = typologies.data.filter(t =>  t.active));
  }

  private controlOverlapDates() {
    const start: Date = new Date(this.form.get('start_date').value);
    const end: Date = new Date(this.form.get('end_date').value);
    if (end && start > end) {
      this.form.get('end_date').setValue(null);
    }
  }

  private stringAsDate(dateStr: string): Date {
    const date = new Date(dateStr);
    if (moment(date).isSameOrAfter(moment(), 'day')) {
      return date;
    }
    return null;
  }
}
