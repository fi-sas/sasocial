import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { first, map } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ApplicationModel } from '../../../models/application.model';
import { ApplicationsService } from '../../../services/applications.service';
import { Resource } from '@fi-sas/core';

interface IResponse {
  success: boolean;
  id?: number;
}

@Injectable()
export class ApplyFormResolver implements Resolve<IResponse> {
  constructor(
    private applicationsService: ApplicationsService,
    private modalService: NzModalService,
    private translateService: TranslateService
  ) {}

  async resolve(route: ActivatedRouteSnapshot): Promise<IResponse> {
    const applicationId = route.paramMap.get('id');
    let id: number;
    if (applicationId) {
      id = parseInt(applicationId, 10);
    }

    let unfinishedApplications: Resource<ApplicationModel>;
    try {
      unfinishedApplications = await this.applicationsService.unfinishedApplication().pipe(first()).toPromise();
    } catch (_) {
      return { success: false };
    }

    if (id) {
      return { success: (unfinishedApplications.data || []).some((a) => a.id === id), id };
    }

    if ((unfinishedApplications.data || []).length) {
      id = unfinishedApplications.data[0].id;
      let responsePromise: Promise<IResponse> = new Promise((resolve: (response: IResponse) => void, _) => {
        this.modalService.confirm({
          nzTitle: this.translateService.instant('U_BIKE.APPLICATIONS.APPLY'),
          nzContent: this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.CONTINUE_PENDING_APPLICATION'),
          nzOkText: this.translateService.instant('true'),
          nzCancelText: this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.START_OVER'),
          nzOnOk: () => resolve({ success: true, id }),
          nzOnCancel: () =>
            this.applicationsService
              .cancel(id)
              .pipe(first())
              .subscribe(
                () => resolve({ success: true }),
                () => resolve({ success: false })
              ),
        });
      });
      return responsePromise;
    }

    return { success: true };
  }
}
