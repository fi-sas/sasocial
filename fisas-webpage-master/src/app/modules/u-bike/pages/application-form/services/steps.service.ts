import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';

@Injectable({
  providedIn: 'root',
})
export class StepsService {
  private dataSoFar: BehaviorSubject<ApplicationModel>;
  public dataSoFar$: Observable<ApplicationModel>;

  private application: BehaviorSubject<ApplicationModel>;
  public application$: Observable<ApplicationModel>;

  private currentStep: BehaviorSubject<number>;
  public currentStep$: Observable<number>;

  private showErrorMessage: BehaviorSubject<boolean>;
  public showErrorMessage$: Observable<boolean>;

  private isSubmiting: BehaviorSubject<boolean>;
  public isSubmiting$: Observable<boolean>;

  private stepToSubmit: BehaviorSubject<number>;
  public stepToSubmit$: Observable<number>;

  private maxSteps: number = 1;

  constructor() {
    this.dataSoFar = new BehaviorSubject(null);
    this.dataSoFar$ = this.dataSoFar.asObservable();

    this.application = new BehaviorSubject(null);
    this.application$ = this.application.asObservable();

    this.currentStep = new BehaviorSubject(0);
    this.currentStep$ = this.currentStep.asObservable();

    this.showErrorMessage = new BehaviorSubject(false);
    this.showErrorMessage$ = this.showErrorMessage.asObservable();

    this.isSubmiting = new BehaviorSubject(false);
    this.isSubmiting$ = this.isSubmiting.asObservable();

    this.stepToSubmit = new BehaviorSubject(null);
    this.stepToSubmit$ = this.stepToSubmit.asObservable();
  }

  reset() {
    this.dataSoFar.next(null);
    this.application.next(null);
    this.currentStep.next(0);
    this.showErrorMessage.next(false);
    this.isSubmiting.next(false);
    this.stepToSubmit.next(null);
  }

  setMaxSteps(maxSteps: number): void {
    this.maxSteps = maxSteps;
  }

  setCurrentStep(stepIndex: number): void {
    if (this.validStep(stepIndex)) {
      this.currentStep.next(stepIndex);
    }
  }

  setApplication(application: ApplicationModel): void {
    this.application.next(application);
  }

  setDataSoFar(data: ApplicationModel): void {
    this.dataSoFar.next(Object.assign({}, this.dataSoFar.getValue() || {}, data));
  }

  setShowErrorMessage(show: boolean = true): void {
    this.showErrorMessage.next(show);
  }

  markStepForSubmission(stepIndex: number): void {
    if (this.validStep(stepIndex)) {
      this.stepToSubmit.next(stepIndex);
    }
  }

  setIsSubmiting(isSubmiting: boolean): void {
    this.isSubmiting.next(isSubmiting);
  }

  private validStep(stepIndex: number): boolean {
    return stepIndex >= 0 && stepIndex < this.maxSteps;
  }

  public isStepToSubmit(stepIndex: number): boolean {
    return stepIndex === this.maxSteps - 1;
  }
}
