import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationInfoComponent } from './application-info.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationInfoComponent,
    data: { breadcrumb: null, title: null, scope: 'u_bike:applications:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationInfoRoutingModule {}
