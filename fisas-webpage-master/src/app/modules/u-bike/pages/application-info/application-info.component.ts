import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { ApplicationDataService } from '@fi-sas/webpage/shared/services/application-data.service';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { Status } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { NzModalService } from 'ng-zorro-antd';
import { ApplicationReportsModalComponent } from '../../components/application-reports-modal/application-reports-modal.component';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';

@Component({
  selector: 'app-application-info',
  templateUrl: './application-info.component.html',
  styleUrls: ['./application-info.component.less'],
})
export class ApplicationInfoComponent implements OnInit {
  @Input() applicationID = null;
  @Output() returnToApplications = new EventEmitter();

  isEdit = false;
  userApplication: ApplicationModel = null;

  status = Status;
  applicationId: 0;
  applicationFuntion = '';

  loading = false;
  loadingConsentFile: boolean = false;
  loadingDeliveryFile: boolean = false;
  
  consent_file = null;
  delivery_file = null;

  constructor(
    private applicationsService: ApplicationsService,
    private applicationDataService: ApplicationDataService,
    private translateService: TranslateService,
    private modalService: NzModalService,
    private uiService: UiService,
    private router: Router,
    private fileService: FilesService,
  ) {}

  ngOnInit() {
    this.getApplication();
  }

  withdraw(id: number) {
    this.applicationsService
      .withdraw(id)
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('MISC.OPERATION_SUCCESSFUL'));
        this.getApplication();
      });
  }

  isDateBeforeToday(endDate) {
    return new Date(endDate) < new Date();
  }

  getApplication() {
    this.loading = true;
    // const id = this.route.snapshot.paramMap.get('id');
    const id = this.applicationID;

    this.applicationsService
      .read(parseInt(id, 10))
      .pipe(first())
      .subscribe((application) => {
        this.applicationDataService.createApplication(application.data[0]);
        this.userApplication = application.data[0];
        if(this.userApplication.consent_terms_file_id){
          this.fileService.file(this.userApplication.consent_terms_file_id).pipe(first()).subscribe((file)=>{
            this.consent_file = file.data[0];
          });
        }
        if(this.userApplication.delivery_reception_file_id){
          this.fileService.file(this.userApplication.delivery_reception_file_id).pipe(first()).subscribe((file)=>{
            this.delivery_file = file.data[0];
          });
        }
        this.loading = false;

      });
  }

  backToApplications() {
    this.returnToApplications.emit(true);
  }

  download(url) {
    window.open(url);
  }

  formatMinutes(value: number) {
    if (!value) {
      return '-';
    }
    let hours = Math.floor(value / 60);
    let minutes = value % 60;
    return `${hours}h${minutes}m`;
  }

  rejectApplication() {
    this.loading = true;
    this.applicationsService
      .clientReject(this.applicationId)
      .pipe(
        first(),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (result) => {
          this.getApplication();
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.REJECT.ERROR')
          );
        }
      );
  }

  acceptApplication() {
    this.loading = true;
    this.applicationsService
      .clientAccept(this.applicationId)
      .pipe(
        first(),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (result) => {
          this.getApplication();
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CONFIRM.ERROR')
          );
        }
      );
  }

  cancelApplication() {
    this.loading = true;
    this.applicationsService
      .cancel(this.applicationId)
      .pipe(
        first(),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (result) => {
          this.getApplication();
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.CANCEL.ERROR')
          );
        }
      );
  }

  quitApplication() {
    this.loading = true;
    this.applicationsService
      .quit(this.applicationId)
      .pipe(
        first(),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (result) => {
          this.getApplication();
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.QUIT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.QUIT.ERROR')
          );
        }
      );
  }

  withdrawQuitApplication() {
    this.loading = true;
    this.applicationsService
      .stay(this.applicationId)
      .pipe(
        first(),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (result) => {
          this.getApplication();
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAW_QUIT.SUCCESS')
          );
        },
        (error) => {
          this.uiService.showMessage(
            MessageType.error,
            this.translateService.instant('VIEW_APPLICATION_STATUS.MESSAGE.WITHDRAW_QUIT.ERROR')
          );
        }
      );
  }

  complain() {
    this.router.navigate(['u-bike', 'forms']);
  }

  openModalConfirm(id, applicationFuntion) {
    this.applicationId = id;
    this.applicationFuntion = applicationFuntion;
    this.modalService.confirm({
      nzTitle: this.translateService.instant('U_BIKE.APPLICATIONS.MODAL.TITLE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.executeFuntion(),
    });
  }

  executeFuntion(): void {
    switch (this.applicationFuntion) {
      case 'accept':
        this.acceptApplication();
        break;
      case 'reject':
        this.rejectApplication();
        break;
      case 'cancel':
        this.cancelApplication();
        break;

      case 'quit':
        this.quitApplication();
        break;
      case 'withdraw_quit':
        this.withdrawQuitApplication();
        break;
      default:
        break;
    }
  }

  getConsentFile(id: number){
    this.loading = true;
    this.applicationsService.getConsentTermsFile(id).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('MISC.GENERATING_REPORT')
      );
    });
  }

  getDeliveryReceptionFile(id: number){
    this.loading = true;
    this.applicationsService.getDeliveryReceptionFile(id).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('MISC.GENERATING_REPORT')
      );
    });
  }
  
  openSubmitReportsModal(id: number){
    this.modalService
    .create({
      nzTitle: null,
      nzContent: ApplicationReportsModalComponent,
      nzFooter: null,
      nzComponentParams: {
        application_id: id
      },
      nzWidth: 650,
      nzWrapClassName: 'vertical-center-modal',
      nzOkText: "Submeter"
    }).afterClose.pipe(first())
    .subscribe((success: boolean) => {
        if(success){
          this.getApplication();
        }
    });
  }
}
