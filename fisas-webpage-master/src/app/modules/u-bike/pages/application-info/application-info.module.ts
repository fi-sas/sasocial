import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ApplicationInfoRoutingModule } from './application-info-routing.module';
import { ApplicationUbikeModule } from '../application/application.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, ApplicationInfoRoutingModule, ApplicationUbikeModule],
})
export class ApplicationInfoUbikeModule {}
