import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationComponent } from './application.component';

const applicationFormData = { breadcrumb: null, title: null, scope: 'u_bike:applications' };

const routes: Routes = [
  { path: '', component: ApplicationComponent, data: applicationFormData },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
