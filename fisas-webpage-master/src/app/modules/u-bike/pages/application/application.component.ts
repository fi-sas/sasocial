import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ApplicationCanAplayModel } from '../../models/application-can-aplay.model';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import * as moment from 'moment';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.less'],
})
export class ApplicationComponent implements OnInit {
  @ViewChild('homepage') homepage;
  translations: TranslationModel[] = [];
  applicationData = new ApplicationModel();
  userApplications: ApplicationModel[] = [];
  loadingApplication = true;
  userApplicationsHistory: ApplicationModel[] = [];
  applicationDetail = false;
  applicationID = null;
  loading = true;
  can_apply: ApplicationCanAplayModel;
  loadingButton = true;
  startDate: Date;
  endDate: Date;

  constructor(
    private applicationsService: ApplicationsService,
    private configurationsService: ConfigurationsService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private userService: AuthService,
  ) {}

  ngOnInit() {
    this.translationsTitle();

    this.getApplicationData();
    this.applicationData = new ApplicationModel();
    this.getApplicationCanAplay();
    if (localStorage.getItem('valueIdApplication')) {
      this.viewApplicationDetail(Number(localStorage.getItem('valueIdApplication')));
      localStorage.removeItem('valueIdApplication');
    }
    if (localStorage.getItem('formApplicationUbike')) {
      localStorage.removeItem('formApplicationUbike');
      return this.router.navigateByUrl('/u-bike/application/form');
    }
  }

  getApplicationCanAplay() {
    this.applicationsService
      .readApplicationCanAplay()
      .pipe(first())
      .subscribe((data) => {
        this.can_apply = data.data[0];
        if (this.can_apply.can) {
          this.userService.isProfileDataCompleted(this.router, this.translateService, this.modalService);
        }
        this.loadingButton = false;
      });
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(13);
    this.translations = service.translations;
  }

  getUserApplication() {
      this.applicationsService
        .readUserApplication()
        .pipe(
          first(),
          finalize(() => (this.loadingApplication = false))
        )
        .subscribe((applications) => {
          this.userApplications = applications.data;
          this.userApplicationsHistory = this.userApplications.filter((application) => {
            return (
              application.status == 'withdrawal' ||
              application.status == 'closed' ||
              application.status == 'cancelled' ||
              application.status == 'rejected'
            );
          });
          this.loading = false;
        });
  }

  getApplicationData() {
    this.applicationsService
      .startDate()
      .pipe(first())
      .subscribe((startDate) => {
        this.startDate = moment(startDate.data[0].value).isValid() ? moment(startDate.data[0].value).toDate() : null;
        this.applicationsService
          .endDate()
          .pipe(first())
          .subscribe((endDate) => {
            this.endDate = moment(endDate.data[0].value).isValid() ? moment(endDate.data[0].value).toDate(): null;
            this.getUserApplication();
          });
      });
  }

  viewApplicationDetail(id) {
    this.applicationID = id;
    this.applicationDetail = true;
  }

  exitViewApplicationDetail(event) {
    this.getUserApplication();
    this.applicationID = null;
    this.applicationDetail = false;
  }
}
