import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationComponent } from './application.component';
import { ApplicationInfoComponent } from '../application-info/application-info.component';
import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationStatusComponent } from '../../components/application-status/application-status.component';
import { DashboardUbikeModule } from '../dashboard/dashboard.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { ApplyFormButtonComponent } from '../../components/apply-form-button';

@NgModule({
  declarations: [
    ApplicationComponent,
    ApplicationInfoComponent,
    ApplicationStatusComponent,
    ApplyFormButtonComponent,
  ],
  imports: [ApplicationRoutingModule, CommonModule, DashboardUbikeModule, SharedModule],
  exports: [ApplicationInfoComponent],
})
export class ApplicationUbikeModule {}
