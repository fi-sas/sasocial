import { Component, OnInit } from '@angular/core';
import { hasOwnProperty } from 'tslint/lib/utils';
import { first } from 'rxjs/operators';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ApplicationModel, Status } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { Router } from '@angular/router';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {
  translations: TranslationModel[] = [];
  userApplications: ApplicationModel = null;
  applicationHistory: ApplicationModel[] = [];

  readonly serviceId = SERVICE_IDS.UBIKE;

  constructor(
      private applicationsService: ApplicationsService,
      private router:Router,
      private configurationsService: ConfigurationsService,
      ) { }

  ngOnInit() {
    this.translationsTitle();
    this.getUserApplication();
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(13);
    this.translations = service.translations;
  }

  getUserApplication() {
    this.applicationsService.readUserApplication().pipe(first())
    .subscribe(applications => {
        this.applicationHistory = applications.data;
        this.userApplications = applications.data.find( application => application.status === Status.CONTRACTED);
        if (this.userApplications === undefined) { this.userApplications = null; }
    });
  }

  goToApplication(){
    this.router.navigateByUrl('u-bike/application');
    localStorage.setItem('formApplicationUbike','true');
  }
}
