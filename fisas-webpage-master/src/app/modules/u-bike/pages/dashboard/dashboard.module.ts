import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CounterComponent } from '../../components/counter/counter.component';
import { OccurrencesComponent } from '../../components/occurrences/occurrences.component';
import { ApplicationsComponent } from '../../components/applications/applications.component';
import { RegistrationDetailsComponent } from '../../components/registration-details/registration-details.component';
import { ManualRegistrationComponent } from '../../components/manual-registration/manual-registration.component';
import { MaintenanceRequestComponent } from '../../components/maintenance-request/maintenance-request.component';
import { ApplicationsCardComponent } from '../../components/card-application/card-application.component';

@NgModule({
  declarations: [DashboardComponent, CounterComponent, OccurrencesComponent, ApplicationsComponent,
    RegistrationDetailsComponent,
    ManualRegistrationComponent,
    MaintenanceRequestComponent, ApplicationsCardComponent],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ],
  exports: [
    ApplicationsCardComponent
  ]
})
export class DashboardUbikeModule { }
