import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ApplicationsService } from '@fi-sas/webpage/modules/u-bike/services/applications.service';
import { ApplicationModel } from '../../models/application.model';

import { finalize, first } from 'rxjs/operators';
import { hasOwnProperty } from 'tslint/lib/utils';
import { ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.less']
})
export class FormsComponent implements OnInit {


  formEdit;
  typeForm: string = '';
  typeFormFiles: string = '';
  userApplications: ApplicationModel[] = [];
  validation: boolean = false;
  validationSigned: boolean = false;
  validationContracted: boolean = false;
  application_id: number;

  constructor(
    private userService: AuthService,
    private applicationsService: ApplicationsService,
    private router: ActivatedRoute
  ) {


    this.router.params.subscribe(
      (params: Params) => {
        const idParam = params['id'];
        if (idParam) {
          this.applicationsService.readForms(idParam).pipe(
            first(),
            finalize(() => {})
          ).subscribe(applications => {
            this.formEdit = applications.data[0];
            if (this.formEdit) {
              switch (this.formEdit.subject) {
                case 'WITHDRAWAL':
                  this.typeForm = 'withdrawal';
                  break;
                case 'ABSENCE':
                  this.typeForm = 'absence';
                  break;
                case 'COMPLAINT':
                  this.typeForm = 'complaint';
                  break;
                case 'RENOVATION':
                  this.typeForm = 'renovation';
                  break;
                case 'ADDITIONAL_MAINTENANCE_KIT':
                  this.typeForm = 'add_maintenance_kit';
                  break;
                case 'THEFT':
                  this.typeFormFiles = 'theft';
                  break;
                case 'MAINTENANCE_REQUEST':
                  this.typeFormFiles = 'maintenance_request';
                  break;
                case 'PRIVATE_MAINTENANCE':
                  this.typeFormFiles = 'private_maintenance';
                  break;
                case 'BREAKDOWN':
                  this.typeFormFiles = 'breakdown';
                  break;
                default:
                  break;
              }
            }
          });
        }
      }
    );
  }

  ngOnInit() {
    this.getUserApplication();
    this.typeForm = ''; //withdrawal or absence or complaint or renovation or add_maintenance_kit
    this.typeFormFiles = ''; //theft or maintenance_request or private_maintenance or breakdown

  }

  cancel(event) {
    if (event) {
      this.typeForm = '';
      this.typeFormFiles = '';
    }
  }

  getUserApplication() {
    this.applicationsService.readUserApplication()
      .pipe(
        first()
      )
      .subscribe(applications => {
        this.userApplications = applications.data;
        if (this.userApplications) {
          for (let i = 0; i < this.userApplications.length; i++) {

            if (this.userApplications[i].status === 'assigned' || this.userApplications[i].status === 'unassigned') {
              this.validationSigned = true;
              this.application_id = this.userApplications[i].id;
            }
            if (this.userApplications[i].status === 'contracted') {
              this.validationContracted = true;
              this.application_id = this.userApplications[i].id;
            }
        }
        }
      });
  }

  changeTypeForm(type: string) {
    this.typeForm = type;
  }

  changeTypeFormFiles(type: string) {
    this.typeFormFiles = type;
  }

}
