import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';
import { SimpleFilesFormComponent } from '../../components/forms/simple-files-form/simple-files-form.component';
import { SimpleFormComponent } from '../../components/forms/simple-form/simple-form.component';

@NgModule({
  declarations: [FormsComponent, SimpleFilesFormComponent, SimpleFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsRoutingModule
  ]
})
export class FormsUbikeModule { }
