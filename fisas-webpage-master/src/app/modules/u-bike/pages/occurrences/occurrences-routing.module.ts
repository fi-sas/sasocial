import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OccurrencesPageComponent } from './occurrences.component';

const routes: Routes = [
  {
    path: '',
    component: OccurrencesPageComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccurrencesRoutingModule { }
