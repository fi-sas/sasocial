import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccurrencesPageComponent } from './occurrences.component';

describe('OccurrencesPageComponent', () => {
  let component: OccurrencesPageComponent;
  let fixture: ComponentFixture<OccurrencesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccurrencesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccurrencesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
