import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first, finalize } from 'rxjs/operators';
import {  Status } from '../../models/application.model';
import { ApplicationsService } from '../../services/applications.service';

@Component({
  selector: 'app-occurrences.component',
  templateUrl: './occurrences.component.html',
  styleUrls: ['./occurrences.component.less']
})
export class OccurrencesPageComponent implements OnInit {
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  status = Status;
  loading: boolean = false;
  userApplications = [];

  mapOfExpandData: { [key: string]: boolean } = {};

  constructor(
    private router: Router,
    private applicationsService: ApplicationsService
  ) { }

  ngOnInit() {
    this.getReadForms();
  }

  checkTagStatusColor(application) {
    const css = {};
    css[application.status] = true;
    return css;
  }

  getReadForms() {
    this.loading = true;
    this.applicationsService.list(this.pageIndex, this.pageSize,false).pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(applications => {
      this.total = applications.link.total;
      this.userApplications = applications.data;
    });
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
        this.pageIndex = 1;
    }
    this.getReadForms();
}

  isPar(val) {
    return (val % 2 === 0);
  }

  goEdit(user) {
    this.router.navigate(['u-bike', 'forms', 'edit', user.id]);
  }
}
