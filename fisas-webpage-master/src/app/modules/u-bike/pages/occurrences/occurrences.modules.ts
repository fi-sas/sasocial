import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { OccurrencesPageComponent } from './occurrences.component';
import { OccurrencesRoutingModule } from './occurrences-routing.module';

@NgModule({
  declarations: [OccurrencesPageComponent],
  imports: [
    CommonModule,
    SharedModule,
    OccurrencesRoutingModule
  ]
})
export class OccurrencesModule { }
