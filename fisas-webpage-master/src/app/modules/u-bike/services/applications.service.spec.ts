import { TestBed } from '@angular/core/testing';

import { ApplicationsService } from './applications.service';
import { FiCoreModule } from '@fi-sas/core';

describe('ApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: ApplicationsService = TestBed.get(ApplicationsService);
    expect(service).toBeTruthy();
  });
});
