import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import * as moment from 'moment';

import { ApplicationCanAplayModel } from '../models/application-can-aplay.model';
import { ApplicationDateModel } from '@fi-sas/webpage/modules/u-bike/models/application-date.model';
import { ApplicationFormModel } from '../models/application-form.model';
import { ApplicationModel } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { ApproveModel, ClientApproveModel } from '@fi-sas/webpage/modules/u-bike/models/approve.model';
import { DashboardModel } from '@fi-sas/webpage/modules/u-bike/models/dashboard.model';
import { DashboardOtherTypesModel } from '../models/dashboard-other-types.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { RegulamentationsModel } from '@fi-sas/webpage/modules/u-bike/models/regulamentations.model';
import { RejectModel } from '@fi-sas/webpage/modules/u-bike/models/reject.model';
import { Status } from '@fi-sas/webpage/modules/u-bike/models/application.model';
import { CONFIGURATIONS } from '../utils/configuration.util';
import { ApplicationHomepageModel } from '../models/application-homepage.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  userCurrentApplication = new BehaviorSubject(null);

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  create(application: ApplicationModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS', {}), application);
  }

  update(id: number, application: ApplicationModel): Observable<Resource<ApplicationModel>> {
    return this.resourceService.patch<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID', { id }), application);
  }

  read(id: number): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'contract,bike,typology_first_preference,preferred_pickup_organict_unit');
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ID', { id }), {params});
  }

  createForms(application: ApplicationFormModel): Observable<Resource<ApplicationFormModel>> {
    return this.resourceService.create<ApplicationFormModel>(this.urlService.get('U_BIKE.APPLICATIONS_FORMS', {}), application);
  }

  updateForms(id, application: ApplicationFormModel): Observable<Resource<ApplicationFormModel>> {
    return this.resourceService.update<ApplicationFormModel>(this.urlService.get('U_BIKE.APPLICATIONS_FORMS_ID', {id}), application);
  }

  list(pageIndex?: number, pageSize?: number, limit?: boolean): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    if(limit) {
      params = params.set('limit', '5');
    }else {
      params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
      params = params.set('limit', pageSize.toString());
    }
    return this.resourceService.list<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_FORMS'), {params});
  }

  readForms(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_FORMS_ID', { id }), {});
  }

  dashboard(period: string, year, month, date): Observable<Resource<DashboardModel>> {
    let params = new HttpParams();
    params = params.set('period', period.toString());
    if(period == 'year') {
      params = params.set('start_date', moment([year, 1, 1]).startOf("year").toISOString() )
      params = params.set('end_date',moment([year, 1, 1]).endOf("year").toISOString() )
    }else if(period == 'month') {
      params = params.set('start_date', moment([year, month, 1]).startOf("month").toISOString() )
      params = params.set('end_date',moment([year, month, 1]).endOf("month").toISOString() )
    }else if(period == 'day') {
      params = params.set('start_date', moment(date).startOf("day").toISOString() )
      params = params.set('end_date',moment(date).endOf("day").toISOString() )
    }
    return this.resourceService.read<DashboardModel>(this.urlService.get('U_BIKE.APPLICATIONS_DASHBOARD'), {params});
  }

  startDate(): Observable<Resource<ApplicationDateModel>> {
    return this.resourceService.read<ApplicationDateModel>(this.urlService.get('U_BIKE.CONFIGURATIONS_BY_KEY', {key: CONFIGURATIONS.APPLICATION_START_DATE}), {});
  }

  endDate(): Observable<Resource<any>> {
    return this.resourceService.read<ApplicationDateModel>(this.urlService.get('U_BIKE.CONFIGURATIONS_BY_KEY', {key: CONFIGURATIONS.APPLICATION_END_DATE}), {});
  }

  readUserApplication(): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('sort', '-id');
    params = params.set('limit', '-1');
    params = params.set('withRelated', 'bike,typology_first_preference');

    return this.resourceService.list<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS'), { params });
  }

  readUserApplicationContracted(): Observable<Resource<ApplicationModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('sort', '-id');
    params = params.set('limit', '-1');
    params = params.set('withRelated', 'bike,typology_first_preference');
    params = params.set('query[status]', 'contracted');
    return this.resourceService.list<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS'), { params });
  }

  reject(id: number): Observable<Resource<ApplicationModel>> {
    const reject = new RejectModel();
    reject.notes = 'Candidatura rejeitada pelo utilizador';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_REJECT', { id }), reject);
  }

  quit(id: number): Observable<Resource<ApplicationModel>> {
    const quit = new ClientApproveModel();
    quit.notes = 'Candidatura rejeitada pelo utilizador';
    quit.action = 'quit';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_CLIENT', { id }), quit);
  }

  stay(id: number): Observable<Resource<ApplicationModel>> {
    const stay = new ClientApproveModel();
    stay.notes = 'Candidatura rejeitada pelo utilizador';
    stay.action = 'stay';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_CLIENT', { id }), stay);
  }

  approve(id: number): Observable<Resource<ApplicationModel>> {
    const approve = new ApproveModel();
    approve.notes = 'Candidatura aceite pelo utilizador';
    approve.status = Status.ACCEPTED;
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_APPROVE', { id }), approve);
  }

  clientReject(id: number): Observable<Resource<ApplicationModel>> {
    const cancel = new ClientApproveModel();
    cancel.notes = 'Candidatura aceite pelo utilizador';
    cancel.action = 'reject';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_CLIENT', { id }), cancel);
  }

  clientAccept(id: number): Observable<Resource<ApplicationModel>> {
    const approve = new ClientApproveModel();
    approve.notes = 'Candidatura aceite pelo utilizador';
    approve.action = 'confirm';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_CLIENT', { id }), approve);
  }

  cancel(id: number): Observable<Resource<ApplicationModel>> {
    const cancel = new ClientApproveModel();
    cancel.notes = 'Candidatura cancelada pelo utilizador';
    cancel.action = 'cancel';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_CLIENT', { id }), cancel);
  }

  withdraw(id: number): Observable<Resource<ApplicationModel>> {
    const withdraw = new RejectModel();
    withdraw.notes = 'Desistência da bicicleta pelo utilizador';
    return this.resourceService.create<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATION_WITHDRAW', { id }), withdraw);
  }

  regulamentations(): Observable<Resource<RegulamentationsModel>> {
    return this.resourceService.read<RegulamentationsModel>(this.urlService.get('U_BIKE.REGULAMENTATIONS', { }), {});
  }

  saveCurrentApplication(data: ApplicationModel) {
    this.userCurrentApplication.next(data);
  }

  getCurrentApplication(): ApplicationModel {
    return this.userCurrentApplication.getValue();
  }

  currentApplicationObservable(): Observable<ApplicationModel> {
    return this.userCurrentApplication.asObservable();
  }

  getCurrentApplicationProp(prop: string): any {

    const app = this.userCurrentApplication.getValue();

    if (app !== null) {

      if (app.hasOwnProperty(prop)) {
        return app[prop];
      } else {
        return null;
      }

    } else {
      return null;
    }
  }

  readApplicationCanAplay(): Observable<Resource<ApplicationCanAplayModel>> {
    return this.resourceService.read<ApplicationCanAplayModel>(this.urlService.get('U_BIKE.APPLICATION_CAN_APLAY'));
  }

  getOtherTypes(id_bike: number, period: string, year, month, date) {
    let params = new HttpParams();
    params = params.set('id_bike', id_bike.toString());
    params = params.set('period', period);
    if(period == 'year') {
      params = params.set('start_date', moment([year, 1, 1]).startOf("year").toISOString() )
      params = params.set('end_date',moment([year, 1, 1]).endOf("year").toISOString() )
    }else if(period == 'month') {
      params = params.set('start_date', moment([year, month, 1]).startOf("month").toISOString() )
      params = params.set('end_date',moment([year, month, 1]).endOf("month").toISOString() )
    }else if(period == 'day') {
      params = params.set('start_date', moment(date).startOf("day").toISOString() )
      params = params.set('end_date',moment(date).endOf("day").toISOString() )
    }
    return this.resourceService.read<DashboardOtherTypesModel>(this.urlService.get('U_BIKE.OTHER_TYPES'), {params});
  }

  unfinishedApplication(): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.APPLICATIONS_ELABORATION'));
  }

  getTerms(): Observable<Resource<ApplicationHomepageModel>> {
    return this.resourceService.read<ApplicationHomepageModel>(this.urlService.get('U_BIKE.TERMS'));
  }

  getConsentTermsFile(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.GET_CONSENT_TERMS_FILE',  { id }), {});
  }

  getDeliveryReceptionFile(id: number): Observable<Resource<ApplicationModel>> {
    return this.resourceService.read<ApplicationModel>(this.urlService.get('U_BIKE.GET_DELIVERY_RECEPTION_FILE',  { id }), {});
  }

  submitConsentDeliveryFile(id:number, data: any): Observable<Resource<any>> {
    return this.resourceService.create<any>(this.urlService.get('U_BIKE.SUBMIT_CONSENT_DELIVERY_REPORTS', { id }), {...data});
  }
}
