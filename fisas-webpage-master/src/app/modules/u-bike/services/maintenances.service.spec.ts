import { TestBed } from '@angular/core/testing';

import { MaintenancesService } from './maintenances.service';
import { FiCoreModule } from '@fi-sas/core';

describe('MaintenancesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: MaintenancesService = TestBed.get(MaintenancesService);
    expect(service).toBeTruthy();
  });
});
