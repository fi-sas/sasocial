import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { MaintenanceModel } from '@fi-sas/webpage/modules/u-bike/models/maintenance.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MaintenancesService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  create(maintenance: MaintenanceModel): Observable<Resource<MaintenanceModel>> {
    return this.resourceService.create<MaintenanceModel>(this.urlService.get('U_BIKE.MAINTENANCES', {}), maintenance);
  }

  read(id: number): Observable<Resource<MaintenanceModel>> {
    return this.resourceService.read<MaintenanceModel>(this.urlService.get('U_BIKE.MAINTENANCES_ID', { id }), {});
  }

  readUserMaintenances(user_id: number, limit: number): Observable<Resource<MaintenanceModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', limit.toString());
    params = params.set('user_id', user_id.toString());

    return this.resourceService.list<MaintenanceModel>(this.urlService.get('U_BIKE.MAINTENANCES'), { params });
  }
}
