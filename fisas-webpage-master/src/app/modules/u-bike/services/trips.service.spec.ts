import { TestBed } from '@angular/core/testing';

import { TripsService } from './trips.service';
import { FiCoreModule } from '@fi-sas/core';

describe('TripsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: TripsService = TestBed.get(TripsService);
    expect(service).toBeTruthy();
  });
});
