import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { TripModel } from '@fi-sas/webpage/modules/u-bike/models/trip.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TripsService {

  constructor(
        private resourceService: FiResourceService,
        private urlService: FiUrlService
    ){}
  create(trip: TripModel): Observable<Resource<TripModel>> {
    return this.resourceService.create<TripModel>(this.urlService.get('U_BIKE.TRIPS', {}), trip);
  }

  read(id: number): Observable<Resource<TripModel>> {
    return this.resourceService.read<TripModel>(this.urlService.get('U_BIKE.TRIPS_ID', { id }), {});
  }

  readUserTrips(userId: number, limit: number, tripStart?: string, tripEnd?: string): Observable<Resource<TripModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', limit.toString());
    params = params.set('sort', '-trip_start');
    if (tripStart) {
      params = params.set('query[trip_start][gte]', tripStart.toString());
    }
    if (tripEnd) {
      params = params.set('query[trip_start][lte]', tripEnd.toString());
    }
    params = params.set('query[user_id]', userId.toString());
    return this.resourceService.list<TripModel>(this.urlService.get('U_BIKE.TRIPS'), { params });
  }

  delete(id: number): Observable<any> {
    return this.resourceService.delete(this.urlService.get('U_BIKE.TRIPS_ID', { id } ), {});
  }

  update(id: number, trip: TripModel): Observable<Resource<TripModel>> {
    return this.resourceService.update<TripModel>(this.urlService.get('U_BIKE.TRIPS_ID', {id}), trip);
  }
}
