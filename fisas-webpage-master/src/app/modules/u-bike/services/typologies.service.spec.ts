import { TestBed } from '@angular/core/testing';

import { TypologiesService } from './typologies.service';
import { FiCoreModule } from '@fi-sas/core';

describe('TypologiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [FiCoreModule]
  }));

  it('should be created', () => {
    const service: TypologiesService = TestBed.get(TypologiesService);
    expect(service).toBeTruthy();
  });
});
