import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { TypologyModel } from '@fi-sas/webpage/modules/u-bike/models/typology.model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypologiesService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  list(
  ): Observable<Resource<TypologyModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');

    return this.resourceService.list<TypologyModel>(this.urlService.get('U_BIKE.TYPOLOGIES'), { params });
  }
}
