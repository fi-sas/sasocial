import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UBikeComponent } from '@fi-sas/webpage/modules/u-bike/u-bike.component';
import { RegulamentationsComponent } from '@fi-sas/webpage/modules/u-bike/components/regulamentations/regulamentations.component';



const routes: Routes = [
  {
    path: '',
    component: UBikeComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'application/form',
        loadChildren: './pages/application-form/application-form.module#ApplicationFormUbikeModule',
        data: { scope: 'u_bike:applications' }
      },
      {
        path: 'application',
        loadChildren: './pages/application/application.module#ApplicationUbikeModule',
        data: { scope: 'u_bike:applications' }
      },
      {
        path: 'forms',
        loadChildren: './pages/forms/forms.module#FormsUbikeModule',
        data: { scope: 'u_bike:application-forms' }
      },
      {
        path: 'occurrences',
        loadChildren: './pages/occurrences/occurrences.modules#OccurrencesModule'
      },
      {
        path: 'dashboard',
        loadChildren: './pages/dashboard/dashboard.module#DashboardUbikeModule',
        data: { scope: 'u_bike' }
      },
      /*{
        path: 'conditions/regulamentations',
        component: RegulamentationsComponent,
        data: { breadcrumb: 'U_BIKE.REGULAMENTATIONS.TERMS_AND_CONDITIONS', title: 'U_BIKE.REGULAMENTATIONS.TERMS_AND_CONDITIONS'}
      },*/
      {
        path: 'application/:id',
        loadChildren: './pages/application-info/application-info.module#ApplicationInfoUbikeModule',
        data: { breadcrumb: 'U_BIKE.APPLICATIONS.APPLICATION_NUMBER', title: 'U_BIKE.APPLICATIONS.APPLICATION_NUMBER', scope: 'u_bike:applications' }
      }
    ],
    data: { breadcrumb: null, title: null }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UBikeRoutingModule { }
