import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UBikeComponent } from './u-bike.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';

describe('UBikeComponent', () => {
  let component: UBikeComponent;
  let fixture: ComponentFixture<UBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UBikeComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        FiCoreModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
