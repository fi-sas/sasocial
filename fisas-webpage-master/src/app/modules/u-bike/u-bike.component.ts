import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';


@Component({
  selector: 'app-u-bike',
  templateUrl: './u-bike.component.html',
  styleUrls: ['./u-bike.component.less']
})
export class UBikeComponent implements OnInit {


  barOptions:any[] = [];

  constructor(
              private userService: AuthService) { }

  ngOnInit() {
    this.barOptions = [
      {
        translate: 'U_BIKE.BAR_MENU.DASHBOARD',
        disable: true,
        routerLink: '/u-bike/dashboard',
        scope: this.userService.hasPermission('u_bike')
      },
      {
        translate: 'U_BIKE.BAR_MENU.APPLICATIONS',
        disable: false,
        routerLink: '/u-bike/application',
        scope: this.userService.hasPermission('u_bike:applications:read')
      },
      {
        translate: 'U_BIKE.BAR_MENU.FORMS',
        disable: false,
        routerLink: '/u-bike/forms',
        scope: this.userService.hasPermission('u_bike:application-forms')
      },
      {
        translate: 'U_BIKE.BAR_MENU.OCCURRENCES',
        disable: false,
        routerLink: '/u-bike/occurrences',
        scope: true
      }
    ]
  }
}
