import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RegulamentationsComponent } from './components/regulamentations/regulamentations.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { UBikeComponent } from './u-bike.component';
import { UBikeRoutingModule } from '@fi-sas/webpage/modules/u-bike/u-bike-routing.module';
import { ApplicationReportsModalComponent } from './components/application-reports-modal/application-reports-modal.component';

@NgModule({
  declarations: [RegulamentationsComponent, UBikeComponent, ApplicationReportsModalComponent],
  imports: [CommonModule, SharedModule, UBikeRoutingModule],
  entryComponents: [ApplicationReportsModalComponent]
})
export class UBikeModule {}
