import { ExperienceModel } from './experiences.model';
import { FileModel } from '../../media/models/file.model';
import { InterviewModel } from './interest.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { VolunteeringModel } from './volunteering-application.model';

interface HistoricColaborations {
  academic_year: string;
  user_manifest_id: number;
  experience_translations: {
    language: any;
    id: number;
    title: string;
    proponent_service: string;
    applicant_profile: string;
    job: string;
    description: string;
    selection_criteria: string;
    language_id: number;
    experience_id: number;
    updated_at: string;
    created_at: string;
  }[];
}

interface HistoricApplications {
  academic_year: string;
  id: number;
}

export interface ActionCandiate {
  history: {
    user: UserModel;
    id: number;
    user_interest_id: number;
    status: string;
    user_id: number;
    notes: string;
    updated_at: string;
    created_at: string;
  }[];
  contract_file?: FileModel;
  experience: ExperienceModel;
  interview: InterviewModel[];
  historic_applications: HistoricApplications[];
  historic_colaborations: HistoricColaborations[];
  application: VolunteeringModel;
  id: number;
  experience_id: number;
  user_id: number;
  status: string;
  contract_file_id: number;
  student_avaliation: string;
  updated_at: string;
  created_at: string;
}
