import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { ScheduleExperienceModel } from './schedule-experience.model';

export class ExperienceModel {
  academic_year: string;
  address: string;
  applicant_profile: string;
  application_deadline_date: string;
  attachment_file_id: number;
  attachment_file?: FileModel;
  certificate_file_id: number;
  contract_file_id: number;
  created_at: string;
  current_account_id: number;
  description: string;
  end_date: string;
  experience_responsible_id: number;
  experience_responsible?: UserModel;
  finances_email: string;
  holydays_availability: boolean;
  id: number;
  job: string;
  number_candidates: number;
  number_simultaneous_candidates: number;
  number_weekly_hours: number;
  organic_unit_id: number;
  organic_unit?: OrganicUnitModel;
  payment_model: string;
  payment_value: number;
  perc_student_ca: number;
  perc_student_iban: number;
  proponent_service: string;
  publish_date: string;
  schedule?: ScheduleExperienceModel[];
  selection_criteria: string;
  start_date: string;
  status: string;
  subject: string;
  title: string;
  total_hours_estimation: number;
  updated_at: string;
  translations: [
    {
      applicant_profile: string;
      description: string;
      job: string;
      language_id: number;
      name: string;
      proponent_service: string;
      selection_criteria: string;
      title: string;
      language?: {
        acronym: string;
        id: number;
        name: string;
      };
    }
  ];
  number_interests: number;
}
