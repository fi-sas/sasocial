export class HistoryModel {
  id: number;
  application_id: number;
  status: string;
  user_id: number;
  notes: string;
  created_at: string;
  updated_at: string;
}
