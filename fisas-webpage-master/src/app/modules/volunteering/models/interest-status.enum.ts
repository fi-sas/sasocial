export enum INTEREST_STATUS {
  // Aceite
  ACCEPTED = 'ACCEPTED',
  // Em Análise
  ANALYSED = 'ANALYSED',
  // Aprovada
  APPROVED = 'APPROVED',
  // Cancelada
  CANCELLED = 'CANCELLED',
  // Fechada
  CLOSED = 'CLOSED',
  // Em Participação
  COLABORATION = 'COLABORATION',
  // Rejeitada
  DECLINED = 'DECLINED',
  // Não Selecionado
  NOT_SELECTED = 'NOT_SELECTED',
  // Submetida
  SUBMITTED = 'SUBMITTED',
  // Lista de Espera
  WAITING = 'WAITING',
  // Em Desistência
  WITHDRAWAL = 'WITHDRAWAL',
  // Desistência Aceite
  WITHDRAWAL_ACCEPTED = 'WITHDRAWAL_ACCEPTED',
  //Em Despacho
  DISPATCH = "DISPATCH",
  DISPATCH_ACCEPT = "DISPATCH_ACCEPT",
  DISPATCH_REJECT = "DISPATCH_REJECT",
}

export enum INTEREST_STATUS_EVENT {
  ACCEPT = 'ACCEPT',
  ANALYSE = 'ANALYSE',
  APPROVE = 'APPROVE',
  CANCEL = 'CANCEL',
  CLOSE = 'CLOSE',
  COLABORATION = 'COLABORATION',
  DECLINE = 'DECLINE',
  NOTSELECT = 'NOTSELECT',
  SUBMIT = 'SUBMIT',
  WAITING = 'WAITING',
  WITHDRAWAL = 'WITHDRAWAL',
  WITHDRAWALACCEPTED = 'WITHDRAWALACCEPTED',
  DISPATCH = "DISPATCH",
}

export const INTEREST_STATUS_STATE_MACHINE: { [status: string]: INTEREST_STATUS[] } = {
  ACCEPTED: [INTEREST_STATUS.CANCELLED, INTEREST_STATUS.COLABORATION],
  ANALYSED: [
    INTEREST_STATUS.CANCELLED,
  ],
  APPROVED: [INTEREST_STATUS.CANCELLED, INTEREST_STATUS.ACCEPTED],
  CANCELLED: [],
  CLOSED: [INTEREST_STATUS.COLABORATION],
  COLABORATION: [INTEREST_STATUS.CLOSED, INTEREST_STATUS.WITHDRAWAL],
  DECLINED: [],
  NOT_SELECTED: [INTEREST_STATUS.CLOSED, INTEREST_STATUS.ANALYSED],
  SUBMITTED: [INTEREST_STATUS.ANALYSED, INTEREST_STATUS.CANCELLED],
  WAITING: [
    INTEREST_STATUS.ANALYSED,
    INTEREST_STATUS.CLOSED,
    INTEREST_STATUS.CANCELLED,
  ],
  WITHDRAWAL: [INTEREST_STATUS.COLABORATION, INTEREST_STATUS.WITHDRAWAL_ACCEPTED],
  WITHDRAWAL_ACCEPTED: [],
  DISPATCH: [ 
    INTEREST_STATUS.ACCEPTED,
    INTEREST_STATUS.DECLINED,
    INTEREST_STATUS.WAITING,
    INTEREST_STATUS.NOT_SELECTED,
   
  ],
};

export enum ExperienceUserInterestDispatchDecision {
  REJECTED = 'REJECTED',
  ACCEPTED = 'ACCEPTED',
  NOT_SELECTED = 'NOT_SELECTED',
  WAITING = 'WAITING',
}

export class ExperienceUserInterestData {
  static eventToDispatchDecisionMapper: Partial<Record<INTEREST_STATUS, ExperienceUserInterestDispatchDecision>> = {
    ACCEPTED: ExperienceUserInterestDispatchDecision.ACCEPTED,
    DECLINED: ExperienceUserInterestDispatchDecision.REJECTED,
    NOT_SELECTED: ExperienceUserInterestDispatchDecision.NOT_SELECTED,
    WAITING: ExperienceUserInterestDispatchDecision.WAITING,
  }
}