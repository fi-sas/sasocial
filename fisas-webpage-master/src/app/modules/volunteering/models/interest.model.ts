import { ExperienceModel } from "./experiences.model";
import { FileModel, ReportFileModel } from "../../media/models/file.model";
import { HistoryModel } from "./history.model";
import { UserModel } from "@fi-sas/webpage/auth/models/user.model";

export interface InterestModel {
  certificate_file_id?: number;
  certificate_file?: FileModel;
  certificate_generated_id?: number;
  certificate_generated?: ReportFileModel;
  contract_file: FileModel;
  created_at: Date;
  experience_id: number;
  experience: ExperienceModel;
  history: HistoryModel[];
  id: number;
  interview: InterviewModel[];
  report_avaliation_file: FileModel;
  report_avaliation: string;
  status: string;
  student_avaliation: string;
  student_file_avaliation: string;
  updated_at: Date;
  user_id: number;
  user: UserModel;
  uuid: string;
}

export interface InterviewReportModel {
  file_id: number;
  notes: string;
}

export interface InterviewModel extends InterviewReportModel {
  created_at: Date;
  date: Date;
  file: FileModel;
  id: number;
  local: string;
  responsable_id: number;
  responsable: UserModel;
  updated_at: Date;
  user_interest_id: number;
}

export interface IHasInterest {
  can_express_interest: boolean;
  experience_available: boolean;
  has_application: boolean;
  has_interest: boolean;
  has_application_accepted: boolean;
}
