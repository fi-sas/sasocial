import { OrganicUnitModel } from './../../../shared/models/organic-unit.model';
export class OrganicUnitApplicationModel {
  organic_unit_id: number;
  created_at: string;
  updated_at: string;
  organic_unit: OrganicUnitModel;
}
