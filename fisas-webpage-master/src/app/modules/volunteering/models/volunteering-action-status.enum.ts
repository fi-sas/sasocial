export enum VOLUNTEERING_ACTION_STATUS {
  ANALYSED = 'ANALYSED',                // Em Análise
  APPROVED = 'APPROVED',                // Aprovada
  CANCELLED = 'CANCELED',               // Cancelada
  CLOSED = 'CLOSED',                    // Fechada
  EXTERNAL_SYSTEM = 'EXTERNAL_SYSTEM',  // Sistema externo
  IN_COLABORATION = 'IN_COLABORATION',  // Em Colaboração
  PUBLISHED = 'PUBLISHED',              // Publicada
  REJECTED = 'REJECTED',                // Rejeitada
  RETURNED = 'RETURNED',                // Devolvida
  SELECTION = 'SELECTION',              // Seleção
  SEND_SEEM = 'SEND_SEEM',              // Emitir Parecer
  SUBMITTED = 'SUBMITTED',              // Submetida
  DISPATCH = 'DISPATCH',              // Submetida
}

export enum VOLUNTEERING_ACTION_STATUS_EVENT {
  CANCEL = 'CANCEL',
}
