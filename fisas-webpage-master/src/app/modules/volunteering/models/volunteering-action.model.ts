export interface VolunteeringActionModel {
  academic_year: string;
  address: string;
  application_deadline_date: string;
  attachment_file_id?: number;
  end_date: string;
  experience_responsible_id: number;
  holydays_availability: boolean;
  id?: number;
  number_candidates?: number;
  number_simultaneous_candidates?: number;
  number_weekly_hours: number;
  organic_unit_id: number;
  publish_date: string;
  start_date: string;
  total_hours_estimation: number;
  schedule: {
    schedule_id: string;
    time_begin: string;
    time_end: string;
  }[];
  translations: {
    title: string;
    proponent_service: string;
    applicant_profile: string;
    job: string;
    description: string;
    selection_criteria: string;
    language_id: number;
  }[];
}
