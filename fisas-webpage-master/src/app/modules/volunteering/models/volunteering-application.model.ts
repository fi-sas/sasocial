import { ActivityModel } from './activity.model';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from './../../../shared/models/course.model';
import { ExperienceModel } from './experiences.model';
import { Gender } from '../../u-bike/models/application.model';
import { HistoryModel } from './history.model';
import { SchoolModel } from './../../../shared/models/school.model';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';

export class VolunteeringModel {
  id?: number;
  academic_year: string;
  subject: string;
  mentor_or_mentee: string;
  course_year: number;
  experience_id?: number;
  course_id: number;
  course_degree_id: number;
  school_id: number;
  iban: string;
  has_scholarship: boolean;
  has_applied_scholarship: boolean;
  scholarship_monthly_value: number;
  has_emergency_fund: boolean;
  has_profissional_experience: boolean;
  is_waiting_scholarship_response: boolean;
  type_of_company: string;
  job_at_company: string;
  job_description: string;
  preferred_activities: number[];
  preferred_schedule_ids: number[];
  has_collaborated_last_year: boolean;
  previous_activity_description: string;
  preferred_schedule: {
    application_id: number;
    id: number;
    schedule_id: number;
    schedule: {
      id: number;
      day_week: string;
      day_period: string;
    };
  }[];
  language_skills: string[];
  has_holydays_availability: boolean;
  foreign_languages: boolean;
  observations: string;
  created_at?: string;
  updated_at?: string;
  cv_file_id?: number;
  notification_guid?: string;
  status?: string;
  course?: CourseModel;
  course_degree?: CourseDegreeModel;
  school?: SchoolModel;
  history?: HistoryModel[];
  preferred_activity?: ActivityModel[];
  organic_units?: any[];
  organic_units_ids?: number[];
  experience?: ExperienceModel;
  user?: UserModel;
  student_number?: number;
  address: string;
  birthdate: string;
  code_postal: string;
  email: string;
  phone: number;
  identification_number: string;
  location: string;
  name: string;
  nationality: string;
  tin: number;
  mobile_phone: string;
  genre: Gender;
}

export class AuxDataApplication {
  course_degree: string;
  course: string;
  school: string;
  organic_units: string[];
}

export enum APPLICATION_STATUS {
  SUBMITTED = 'SUBMITTED',
  ANALYSED = 'ANALYSED',
  ACCEPTED = 'ACCEPTED',
  DECLINED = 'DECLINED',
  EXPIRED = 'EXPIRED',
  CANCELLED = 'CANCELLED',
  DISPATCH = 'DISPATCH',
}
