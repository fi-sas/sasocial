import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';

@Component({
  selector: 'app-application-card',
  templateUrl: './application-card.component.html',
  styleUrls: ['./application-card.component.less'],
})
export class ApplicationCardComponent {
  @Input() application: VolunteeringModel = null;
  @Input() translations: TranslationModel[] = [];

  buttonStyle = {
    'font-size': '12px',
    color: 'var(--primary-color)',
    'border-color': 'var(--primary-color)',
    height: 'auto',
  };

  constructor(private router: Router) {}

  goToApplicationReview() {
    this.router.navigateByUrl('volunteering/applications/review/' + this.application.id);
  }
}
