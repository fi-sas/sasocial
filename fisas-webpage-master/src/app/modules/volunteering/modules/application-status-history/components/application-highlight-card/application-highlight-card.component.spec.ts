import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationHighlightCardComponent } from './application-highlight-card.component';

describe('ApplicationHighlightCardComponent', () => {
  let component: ApplicationHighlightCardComponent;
  let fixture: ComponentFixture<ApplicationHighlightCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationHighlightCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationHighlightCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
