import { Component, Input, EventEmitter, Output } from '@angular/core';

import { first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ChangeStatusService } from '@fi-sas/webpage/modules/volunteering/services/change-status.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-application-highlight-card',
  templateUrl: './application-highlight-card.component.html',
  styleUrls: ['./application-highlight-card.component.less'],
})
export class ApplicationHighlightCardComponent {
  @Input() translations: TranslationModel[] = [];
  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this._application = application;
      this.canCancel = this.checkCanCancel();
      this.canEditApplication = application.id && ['SUBMITTED', 'ANALYSED', 'ACCEPTED'].includes(application.status);
    }
  }

  @Output() refresh = new EventEmitter();

  private _application: VolunteeringModel;
  get application() {
    return this._application;
  }

  canCancel: boolean;
  canEditApplication: boolean;

  constructor(
    private changeStatusService: ChangeStatusService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private uiService: UiService,
    private router:Router
  ) {}

  cancel() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('VOLUNTEERING.MY_APPLICATIONS.CANCEL_MODAL_TITLE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelConfirm()
    });
  }

  private cancelConfirm() {
    this.changeStatusService.applicationCancel(this.application.id).pipe(first()).subscribe(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('VOLUNTEERING.MY_APPLICATIONS.CANCEL_SUCCESS'));
        this.refresh.emit(true);
    })
  }

  private checkCanCancel(): boolean {
    return ['ANALYSED', 'ACCEPTED', 'SUBMITTED'].includes(this.application.status);
  }

  reviewApplication(idApplication: number) {
    this.router.navigateByUrl("volunteering/applications/review/" + idApplication);
  }

}
