import { Component, EventEmitter, Input, Output } from '@angular/core';

import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';

@Component({
  selector: 'app-list-applications',
  templateUrl: './list-applications.component.html',
  styleUrls: ['./list-applications.component.less'],
})
export class ListApplicationsComponent {
  @Input() applications: VolunteeringModel[] = [];
  @Input() translations: TranslationModel[] = [];
  @Input() isLoading = false;

  @Output() refresh = new EventEmitter();

  constructor() {}

  emitRefresh() {
    this.refresh.emit();
  }
}
