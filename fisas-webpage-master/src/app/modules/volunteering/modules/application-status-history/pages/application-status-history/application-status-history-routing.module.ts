import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationStatusHistoryComponent } from './application-status-history.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationStatusHistoryComponent,
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationStatusHistoryRoutingModule {}
