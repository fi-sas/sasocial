import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { NzModalService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { Resource } from '@fi-sas/core';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { VolunteeringApplicationsService } from './../../../volunteering-application/services/volunteering-applications.service';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-application-status-history',
  templateUrl: './application-status-history.component.html',
  styleUrls: ['./application-status-history.component.less'],
})
export class ApplicationStatusHistoryComponent implements OnInit, OnDestroy {
  readonly buttonStyle = {
    'font-size': '14px',
    'line-height': '24px',
    'margin-top': '20px',
    color: 'black',
    width: '400px',
  };

  readonly panels = [
    {
      active: false,
      disabled: false,
      name: 'SOCIAL_SUPPORT.HISTORY_APPLICATIONS.TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  translations: TranslationModel[] = [];

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  isLoadingSubmitApplication: boolean;
  activeApplicationButton: boolean;

  isLoadingCurrentApplications: boolean;
  currentApplications: VolunteeringModel[] = [];

  isLoadingPastApplications: boolean;
  pastApplications: VolunteeringModel[] = [];

  constructor(
    private configurationsService: ConfigurationsService,
    private router: Router,
    private volunteeringApplicationsService: VolunteeringApplicationsService
  ) {}

  ngOnInit() {
    this.getTranslations();

    this.getCurrentApplications();
    this.getPastApplications();
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  goToSubmitApplication() {
    this.router.navigateByUrl('volunteering/applications');
  }

  getCurrentApplications() {
    this.isLoadingCurrentApplications = true;
    this.volunteeringApplicationsService.currentApplications()
    .pipe(first(), finalize(() => this.isLoadingCurrentApplications = false))
    .subscribe(response => {
      this.currentApplications = response.data;
    });
  }

 getPastApplications() {
    this.isLoadingPastApplications = true;
    this.volunteeringApplicationsService.pastApplications()
    .pipe(first(), finalize(() => this.isLoadingPastApplications = false))
    .subscribe(response => {
      this.pastApplications = response.data;
    });
  }

  private getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }
}
