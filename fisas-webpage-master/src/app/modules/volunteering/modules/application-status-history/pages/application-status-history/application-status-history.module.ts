import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationStatusHistoryComponent } from './application-status-history.component';
import { ApplicationStatusHistoryRoutingModule } from './application-status-history-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import {
  ApplicationCardComponent,
  ApplicationHighlightCardComponent,
  ListApplicationsComponent,
} from '../../components';

@NgModule({
  declarations: [
    ApplicationCardComponent,
    ApplicationHighlightCardComponent,
    ApplicationStatusHistoryComponent,
    ListApplicationsComponent,
  ],
  imports: [CommonModule, SharedModule, ApplicationStatusHistoryRoutingModule, SharedVolunteeringModule],
})
export class ApplicationStatusHistoryVolunteeringModule {}
