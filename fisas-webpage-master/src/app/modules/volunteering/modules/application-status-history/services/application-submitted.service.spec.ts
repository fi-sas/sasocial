import { TestBed } from '@angular/core/testing';

import { ApplicationSubmittedService } from '../application-submitted.service;

describe('ApplicationSubmittedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationSubmittedService = TestBed.get(ApplicationSubmittedService);
    expect(service).toBeTruthy();
  });
});
