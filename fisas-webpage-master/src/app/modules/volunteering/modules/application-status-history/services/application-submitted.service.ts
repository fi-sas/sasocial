import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApplicationSubmittedService {
  submitted = new BehaviorSubject<boolean>(false);
  constructor() {}

  applicationSubmittedObservable(): Observable<boolean> {
    return this.submitted.asObservable();
  }

  saveSubmitted(submitted: boolean) {
    this.submitted.next(submitted);
  }
}
