import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { VolunteeringModel } from '../../../models/volunteering-application.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  applicationToReview = new BehaviorSubject<VolunteeringModel>(null);
  constructor() {}

  applicationToReviewObservable(): Observable<VolunteeringModel> {
    return this.applicationToReview.asObservable();
  }

  saveApplication(application: VolunteeringModel) {
    this.applicationToReview.next(application);
  }

  getApplication(): VolunteeringModel {
    return this.applicationToReview.getValue();
  }

  getApplicationProperty(property: string): any {

    const application = this.applicationToReview.getValue();

    if (application !== null) {

      if (application.hasOwnProperty(property)) {
        return application[property];
      } else {
        return null;
      }

    } else {
      return null;
    }
  }
}
