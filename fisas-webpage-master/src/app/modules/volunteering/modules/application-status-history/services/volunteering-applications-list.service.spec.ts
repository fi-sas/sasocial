import { TestBed } from '@angular/core/testing';

import { VolunteeringApplicationsListService } from './volunteering-applications-list.service';

describe('SocialApplicationsListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VolunteeringApplicationsListService = TestBed.get(VolunteeringApplicationsListService);
    expect(service).toBeTruthy();
  });
});
