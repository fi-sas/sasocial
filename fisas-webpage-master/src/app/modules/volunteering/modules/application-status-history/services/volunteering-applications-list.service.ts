import { AuthService } from '../../../../../auth/services/auth.service';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { VolunteeringModel } from '../../../models/volunteering-application.model';

@Injectable({
  providedIn: 'root',
})
export class VolunteeringApplicationsListService {
  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private authService: AuthService
  ) {}

  applicationsUsers(): Observable<Resource<VolunteeringModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('subject', 'V');
    params = params.set('user_id', this.authService.getUser().id.toString());
    params = params.set(
      'withRelated',
      'history,experience,course,course_degree,school,preferred_activity,organic_units,preferred_schedule'
    );

    return this.resourceService.list<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS'),
      { params }
    );
  }
}
