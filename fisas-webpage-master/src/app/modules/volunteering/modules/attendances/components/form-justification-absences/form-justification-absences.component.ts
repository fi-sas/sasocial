import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AbsenceReasonModel, AttendanceModel } from '../../models';
import { AttendancesService } from '../../services';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-form-justification-absences',
  templateUrl: './form-justification-absences.component.html',
  styleUrls: ['./form-justification-absences.component.less'],
})
export class FormJustificationAbsencesComponent implements OnInit {

  @Input() attendance: AttendanceModel = null;

  loadingSubmit = false;

  filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];

  justificationAbsencesForm: FormGroup = new FormGroup({
    reason: new FormControl('', [Validators.required, trimValidation]),
    attachment_file_id: new FormControl(null),
  });

  doc: any;

  constructor(
    private attendancesService: AttendancesService,
    private uiService: UiService,
    private filesService: FilesService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    if (this.attendance.absence_reason.length > 0) {
      if (this.attendance.absence_reason[0].attachment_file_id != null) {
        this.getFile(this.attendance.absence_reason[0].attachment_file_id);
      }
    }
  }

  submitJustification() {
    const absenceReason = new AbsenceReasonModel();

    for (const i in this.justificationAbsencesForm.controls) {
      if (i) {
        this.justificationAbsencesForm.controls[i].markAsDirty();
        this.justificationAbsencesForm.controls[i].updateValueAndValidity();
        if (this.justificationAbsencesForm.controls[i].errors !== null) {
          return false;
        }
      }
    }

    absenceReason.experience_attendance_id = this.attendance.id;
    absenceReason.attachment_file_id = this.justificationAbsencesForm.value.attachment_file_id;
    absenceReason.reason = this.justificationAbsencesForm.value.reason;
    absenceReason.accept = false;

    this.loadingSubmit = true;
    this.attendancesService
      .createAbsenceReason(absenceReason)
      .pipe(
        first(),
        finalize(() => (this.loadingSubmit = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translate.instant('SOCIAL_SUPPORT.ABSENCES.JUSTIFY_FORM.SUCCESS')
        );
        this.attendancesService.submittedAbsencesReason();
      });
  }

  getInputError(inputName: string): string {
    if (
      this.justificationAbsencesForm.get(inputName).errors.required ||
      this.justificationAbsencesForm.get(inputName).errors.requiredTrue
    ) {
      return this.translate.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  getFile(id: number) {
    this.filesService
      .file(id)
      .pipe(first())
      .subscribe((file) => (this.doc = file.data[0]));
  }
}
