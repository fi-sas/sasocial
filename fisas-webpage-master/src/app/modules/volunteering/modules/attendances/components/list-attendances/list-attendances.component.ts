import { Component, Input, Type } from '@angular/core';

import { first } from 'rxjs/operators';
import { ModalOptionsForService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AttendancesService } from '../../services';
import { FormJustificationAbsencesComponent } from '../form-justification-absences/form-justification-absences.component';
import { WithdrawalService } from '@fi-sas/webpage/modules/volunteering/services/withdrawal.service';
import {
  AbsenceReasonModel,
  AttendanceModel,
  ATTENDANCE_STATUS,
  IAttendancesTableRow,
  ITableRowIcon,
} from '../../models';
import { FormAttendanceComponent } from '../../../shared-volunteering/components';

@Component({
  selector: 'app-list-attendances',
  templateUrl: './list-attendances.component.html',
  styleUrls: ['./list-attendances.component.less'],
})
export class ListAttendancesComponent {
  @Input() collaborationId = 0;
  @Input() collaborationDateStart;
  @Input() collaborationDateEnd;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  pageIndex = 1;
  pageSize = 10;
  total = 0;

  loadingAttendances = false;

  attendances: AttendanceModel[] = [];
  justificationAbsencesFormModal: NzModalRef = null;

  attendancesTable: IAttendancesTableRow[] = [];

  constructor(
    private attendancesService: AttendancesService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private withdrawalService: WithdrawalService
  ) {
    this.subscriptions = this.attendancesService.attendanceObservable().subscribe((submitted) => {
      if (submitted) {
        this.pageIndex = 1;
        this.getAttendances();
      }
    });

    this.subscriptions = this.attendancesService.absencesObservable().subscribe((submitted) => {
      if (submitted) {
        this.pageIndex = 1;
        this.getAttendances();
      }
    });

    this.subscriptions = this.attendancesService.absencesReasonObservable().subscribe((submitted) => {
      if (submitted !== false) {
        this.justificationAbsencesFormModal.close();
        this.getAttendances();
      }
    });

    this.subscriptions = this.withdrawalService.withdrawalObservable().subscribe((withdrawal) => {
      if (withdrawal !== false) {
        this.getAttendances();
      }
    });

    this.subscriptions = this.translateService.onLangChange.subscribe(
      () => (this.attendancesTable = this.getAttendancesTable(this.attendances))
    );
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnChanges() {
    this.getAttendances();
  }

  justify(attendance: AttendanceModel): void {
   this.justificationAbsencesFormModal = this.modalService.create({
      nzWidth: 500,
      nzMaskClosable: false,
      nzClosable: true,
      nzContent: FormJustificationAbsencesComponent,
      nzFooter: null,
      nzComponentParams: { attendance: attendance },
    });
  }

  getAttendances(): void {
    this.loadingAttendances = true;
    this.attendancesService
      .list(this.pageIndex, this.pageSize, this.collaborationId)
      .pipe(first())
      .subscribe(
        (attendances) => {
          this.total = attendances.link.total;
          this.attendances = attendances.data;
          this.attendancesTable = this.getAttendancesTable(this.attendances);
          this.loadingAttendances = false;
        },
        () => {
          this.loadingAttendances = false;
        }
      );
  }

  private getAttendancesTable(attendances: AttendanceModel[]): IAttendancesTableRow[] {
    const result: IAttendancesTableRow[] = [];
    attendances.forEach((attendance: AttendanceModel) => {
      let showHasAttachments = false;
      let absenceReason: ITableRowIcon;
      if ((attendance.absence_reason || []).length) {
        showHasAttachments = attendance.absence_reason[0].attachment_file_id !== null;

        absenceReason = attendance.absence_reason[0].accept
          ? { icon: 'icons:paid', elemetClasses: 'paid-icon' }
          : {
              icon: 'icons:icons-close',
              elemetClasses: 'close-icon',
              reason: attendance.absence_reason[0].reject_reason || null,
            };
      }
      const data: IAttendancesTableRow = {
        hours: attendance.n_hours,
        submissionDate: attendance.created_at,
        justificationPeriod: this.getJustificationPeriod(attendance),
        justification: this.getJustification(attendance),
        attachments: {
          icon: showHasAttachments ? 'icons:icons-radiobutton-checked-2' : 'icons:icons-radiobutton-unchecked',
          elemetClasses: showHasAttachments ? 'checked-icon' : 'unchecked-icon',
          url: showHasAttachments ? attendance.absence_reason[0].attachment_file.url || null : null,
        },
        accepted: absenceReason,
        showJustifyButton: this.isEmpty(attendance.absence_reason) && !attendance.was_present,
        attendance,
        status: attendance.status
      };
      if ([ATTENDANCE_STATUS.ACCEPTED, ATTENDANCE_STATUS.REJECTED].includes(attendance.status)) {
        data.accepted =
          attendance.status === ATTENDANCE_STATUS.ACCEPTED
            ? { icon: 'icons:paid', elemetClasses: 'paid-icon' }
            : { icon: 'icons:icons-close', elemetClasses: 'close-icon' };
        data.showJustifyButton = false;
      }
      result.push(data);
    });
    return result;
  }

  openDetailEdit(row) {
    
    this.modalService.create(
      this.getModalOptions<FormAttendanceComponent>(
        FormAttendanceComponent,
        { attendance: row, 
          collaborationDateEnd: this.collaborationDateEnd,
          collaborationDateStart: this.collaborationDateStart,
          idCola: this.collaborationId },
        { nzWidth: 750 }
      )
    );
  }

  private getModalOptions<T>(
    component: Type<T>,
    params: Partial<T>,
    override: ModalOptionsForService<T> = {}
  ): ModalOptionsForService<T> {
    return Object.assign(
      {
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: component,
        nzComponentParams: params,
        nzFooter: null,
      },
      override
    );
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
      this.pageIndex = 1;
    }
    this.getAttendances();
  }

  isEmpty(object: object): boolean {
    if (object == null) {
      return true;
    }
    return Object.keys(object).length === 0;
  }

  private getJustification(attendance: AttendanceModel): string {
    if ((attendance.absence_reason || []).length) {
      return attendance.absence_reason[0].reason;
    }

    if (attendance.was_present) {
      return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.CONFIRMED_ATTENDANCE');
    }

    return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.NON_JUSTIFIED_ABSENCE');
  }

  private getJustificationPeriod(attendance: AttendanceModel): string[] {
    if ((attendance.absence_reason || []).length) {
      return attendance.absence_reason.map((reason: AbsenceReasonModel) => {
        return this.getJustificationPeriodLine(reason.start_date, reason.end_date);
      });
    }

    return [this.getJustificationPeriodLine(attendance.initial_time, attendance.final_time)];
  }

  private getJustificationPeriodLine(start: string, end: string): string {
    const startDate = moment(start);
    const endDate = moment(end);

    if (!startDate.isValid() || !endDate.isValid) {
      return;
    }

    const timeFormat = 'H:mm';
    const dateFormat = 'DD-MM-YYYY';

    if (startDate.format('L') === endDate.format('L')) {
      return `${startDate.format(dateFormat)}\n${startDate.format(timeFormat)} - ${endDate.format(timeFormat)}`;
    }

    return `${startDate.format(dateFormat)} ${this.translateService.instant(
      'SOCIAL_SUPPORT.ATTENDANCES.TABS.ATTENDANCES_ABSENCE.DATE_SEPARATOR'
    )} ${endDate.format(dateFormat)}`;
  }
}
