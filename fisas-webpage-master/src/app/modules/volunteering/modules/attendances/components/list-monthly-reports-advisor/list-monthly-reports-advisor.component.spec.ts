import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMonthlyReportsAdvisorComponent } from './list-monthly-reports-advisor.component';

describe('ListMonthlyReportsAdvisorComponent', () => {
  let component: ListMonthlyReportsAdvisorComponent;
  let fixture: ComponentFixture<ListMonthlyReportsAdvisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListMonthlyReportsAdvisorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMonthlyReportsAdvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
