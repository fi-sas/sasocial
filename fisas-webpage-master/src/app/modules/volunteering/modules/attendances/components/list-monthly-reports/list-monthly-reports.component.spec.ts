import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMonthlyReportsComponent } from './list-monthly-reports.component';

describe('ListMonthlyReportsComponent', () => {
  let component: ListMonthlyReportsComponent;
  let fixture: ComponentFixture<ListMonthlyReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListMonthlyReportsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMonthlyReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
