import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementAttendanceDocumentationComponent } from './management-attendance-documentation.component';

describe('ManagementAttendanceDocumentationComponent', () => {
  let component: ManagementAttendanceDocumentationComponent;
  let fixture: ComponentFixture<ManagementAttendanceDocumentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagementAttendanceDocumentationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementAttendanceDocumentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
