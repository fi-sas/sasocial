import { FileModel } from "@fi-sas/webpage/modules/media/models/file.model";

export class AbsenceReasonModel {
  accept: boolean;
  application_attendance_id?: number;
  application_id?: number;
  attachment_file?: FileModel;
  attachment_file_id: number;
  created_at?: string;
  end_date: string;
  experience_attendance_id?: number;
  id?: number;
  reason: string;
  start_date: string;
  updated_at?: string;
  reject_reason?: string;
}
