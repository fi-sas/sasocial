import { AbsenceReasonModel } from './absence-reason.model';
import { UserInterestModel } from '../../experiences/models/user-interest.model';
import { VolunteeringModel } from '../../../models';

export enum ATTENDANCE_STATUS {
  ACCEPTED = 'ACCEPTED',
  LACK = 'LACK',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
}

export class AttendanceModel {
  absence_reason?: AbsenceReasonModel[];
  application_id: number;
  application?: VolunteeringModel;
  created_at?: string;
  date: string;
  executed_service?: string;
  file_id: number;
  final_time?:any;
  id?: number;
  initial_time?: any;
  n_hours: string;
  notes: string;
  status: ATTENDANCE_STATUS;
  updated_at?: string;
  user_interest_id: number;
  user_interest?: UserInterestModel;
  was_present: boolean;
}
