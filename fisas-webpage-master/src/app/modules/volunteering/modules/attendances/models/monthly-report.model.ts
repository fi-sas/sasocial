import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';

export interface IMonthlyReport {
  created_at?: string;
  file?: FileModel;
  month: number;
  report: string;
  year: number;
}

export interface IMonthlyReportAlert {
  alert: boolean;
  total_hours: number;
}

export interface IMonthlyReportAlerts {
  current_month: IMonthlyReportAlert;
  last_month: IMonthlyReportAlert;
}
