import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagementAttendancesComponent } from './management-attendances.component';

const routes: Routes = [
  {
    path: '',
    component: ManagementAttendancesComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:experiences:my_offers' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagementAttendancesRoutingModule {}
