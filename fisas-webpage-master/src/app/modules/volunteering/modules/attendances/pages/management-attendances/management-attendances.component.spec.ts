import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementAttendancesComponent } from './management-attendances.component';

describe('ManagementAttendancesComponent', () => {
  let component: ManagementAttendancesComponent;
  let fixture: ComponentFixture<ManagementAttendancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManagementAttendancesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementAttendancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
