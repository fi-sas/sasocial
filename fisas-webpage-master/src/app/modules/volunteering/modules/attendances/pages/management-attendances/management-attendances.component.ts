import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';

import { first } from 'rxjs/operators';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { InterestModel, VolunteeringActionModel } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService, VolunteeringActionsService } from '@fi-sas/webpage/modules/volunteering/services';

@Component({
  selector: 'app-management-attendances',
  templateUrl: './management-attendances.component.html',
  styleUrls: ['./management-attendances.component.less'],
})
export class ManagementAttendancesComponent {
  isResponsable = false;

  action: VolunteeringActionModel;
  manifestations: InterestModel[] = [];
  selectedCollaborationId: number = null;

  isLoading: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private interestService: InterestService,
    private volunteeringActionsService: VolunteeringActionsService
  ) {
    const experience_id: number = this.activatedRoute.snapshot.params.id;
    this.getExperience(experience_id);
    this.getUserInterest(experience_id);
    this.isResponsable = this.authService.hasPermission('volunteering:experiences:responsable');
  }

  ngOnInit() {
   
  }

  selectUser(interest: number) {
    this.selectedCollaborationId = interest;
  }

  private getExperience(id: number) {
    this.volunteeringActionsService
      .getAction(id)
      .pipe(first())
      .subscribe((experience) => (this.action = experience.data[0]));
  }

  private getUserInterest(id: number) {
    this.interestService
      .userInterestColaboration(id)
      .pipe(first())
      .subscribe((users_interest) => {
        if ((users_interest.data || []).length) {
          this.manifestations = users_interest.data;
          this.selectUser(this.manifestations[0].id);
        }
      });
  }
}
