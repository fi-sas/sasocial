import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ManagementAttendancesComponent } from './management-attendances.component';
import { ManagementAttendancesRoutingModule } from './management-attendances-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import {
  ListAttendancesAdvisorComponent,
  ListMonthlyReportsAdvisorComponent,
  ManagementAttendanceDocumentationComponent,
} from '../../components';

@NgModule({
  declarations: [
    ListAttendancesAdvisorComponent,
    ListMonthlyReportsAdvisorComponent,
    ManagementAttendanceDocumentationComponent,
    ManagementAttendancesComponent,
  ],
  imports: [CommonModule, ManagementAttendancesRoutingModule, SharedVolunteeringModule, SharedModule],
})
export class ManagementAttendancesModule {}
