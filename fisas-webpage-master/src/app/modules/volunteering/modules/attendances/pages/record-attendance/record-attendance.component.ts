import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { InterestModel, INTEREST_STATUS } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService, WithdrawalService } from '@fi-sas/webpage/modules/volunteering/services';
import { Resource } from '@fi-sas/core';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-record-attendance',
  templateUrl: './record-attendance.component.html',
  styleUrls: ['./record-attendance.component.less'],
})
export class RecordAttendanceComponent {
  isLoading: boolean;

  currentColaboration: InterestModel;
  translations: TranslationModel[] = [];
  collaborationStillGoing: boolean;
  collaborationInWithdrawl : boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private configurationsService: ConfigurationsService,
    private interestService: InterestService,
    private withdrawalService: WithdrawalService,
  ) {
    this.withdrawalService.withdrawalObservable().subscribe((withdrawal) => {
      if (withdrawal !== false && this.currentColaboration) {
        this.getCollaboration(this.currentColaboration.id);
      }
    });

    this.withdrawalService.cancelWithdrawalObservable().subscribe((cancelWithdrawal) => {
      if (cancelWithdrawal !== false && this.currentColaboration) {
        this.getCollaboration(this.currentColaboration.id);
      }
    });
  }

  ngOnInit() {
    this.translationsTitle();
    const collaborationId: number = this.activatedRoute.snapshot.params.id;
    this.getCollaboration(collaborationId);
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }

  getCollaboration(id: number) {
    this.isLoading = true;
    this.interestService.getInterestById(id).pipe(first(), finalize(()=> this.isLoading = false)).subscribe(response => {
      this.currentColaboration = response.data[0];
      this.collaborationStillGoing = [ INTEREST_STATUS.COLABORATION.toString()].includes(this.currentColaboration.status);
      this.collaborationInWithdrawl = [ INTEREST_STATUS.WITHDRAWAL.toString()].includes(this.currentColaboration.status);

    })
  }
}
