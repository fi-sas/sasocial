import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RecordAttendanceComponent } from './record-attendance.component';
import { RecordAttendanceRoutingModule } from './record-attendance-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import {
  ExperienceNameComponent,
  FormJustificationAbsencesComponent,
  ListAttendancesComponent,
  ListMonthlyReportsComponent,
} from '../../components';

@NgModule({
  declarations: [
    ExperienceNameComponent,
    FormJustificationAbsencesComponent,
    ListAttendancesComponent,
    ListMonthlyReportsComponent,
    RecordAttendanceComponent,
  ],
  imports: [CommonModule, RecordAttendanceRoutingModule, SharedVolunteeringModule, SharedModule],
  entryComponents: [FormJustificationAbsencesComponent],
})
export class RecordAttendanceModule {}
