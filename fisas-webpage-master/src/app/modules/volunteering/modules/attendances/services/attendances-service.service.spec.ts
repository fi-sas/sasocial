import { TestBed } from '@angular/core/testing';

import { AttendancesServiceService } from './attendances-service.service';

describe('AttendancesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttendancesServiceService = TestBed.get(AttendancesServiceService);
    expect(service).toBeTruthy();
  });
});
