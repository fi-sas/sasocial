import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';

import { AbsenceReasonModel } from './../models/absence-reason.model';
import { AttendanceModel } from '../models/attendance.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class AttendancesService {
  SubmittedAbsences = new BehaviorSubject(false);
  SubmittedAbsencesReason = new BehaviorSubject(false);
  SubmittedAttendance = new BehaviorSubject(false);

  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  attendanceObservable(): Observable<boolean> {
    return this.SubmittedAttendance.asObservable();
  }

  absencesObservable(): Observable<boolean> {
    return this.SubmittedAbsences.asObservable();
  }

  absencesReasonObservable(): Observable<boolean> {
    return this.SubmittedAbsencesReason.asObservable();
  }

  submittedAttendance() {
    this.SubmittedAttendance.next(true);
  }

  submittedAbsences() {
    this.SubmittedAbsences.next(true);
  }

  submittedAbsencesReason() {
    this.SubmittedAbsencesReason.next(true);
  }

  list(pageIndex: number, pageSize: number, applicationId: number): Observable<Resource<AttendanceModel>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('query[user_interest_id]', applicationId.toString());
    params = params.set('sort', '-id');
    params = params.set('withRelated', 'user_interest,absence_reason');

    return this.resourceService.list<AttendanceModel>(this.urlService.get('VOLUNTEERING.ATTENDANCES'), { params });
  }

  getAttendance(applicationId: number): Observable<Resource<AttendanceModel>> {
    let params = new HttpParams();
    params = params.set('limit', '-1');
    params = params.set('query[user_interest_id]', applicationId.toString());

    return this.resourceService.list<AttendanceModel>(this.urlService.get('VOLUNTEERING.ATTENDANCES'), { params });
  }

  create(attendance: AttendanceModel): Observable<Resource<AttendanceModel>> {
    return this.resourceService.create<AttendanceModel>(
      this.urlService.get('VOLUNTEERING.ATTENDANCES', {}),
      attendance
    );
  }

  
  update(id, attendance: AttendanceModel): Observable<Resource<AttendanceModel>> {
    return this.resourceService.update<AttendanceModel>(
      this.urlService.get('VOLUNTEERING.ATTENDANCES', {id}),
      attendance
    );
  }


  createAbsenceReason(absenceReason: AbsenceReasonModel): Observable<Resource<AbsenceReasonModel>> {
    return this.resourceService.create<AbsenceReasonModel>(
      this.urlService.get('VOLUNTEERING.ABSENCE_REASON', {}),
      absenceReason
    );
  }

  changeStatus(id: number, data) {
    return this.resourceService.create<AttendanceModel>(
      this.urlService.get('VOLUNTEERING.ATTENDANCES_ID_CHAGE_STATUS', { id }),
      data
    );
  }

  validate(id: number) {
    return this.changeStatus(id, { status: 'ACCEPTED' });
  }

  reject(id: number, data) {
    return this.changeStatus(id, Object.assign({ status: 'REJECTED' }, data));
  }

  validateJustification(id: number) {
    return this.resourceService.create<any>(
      this.urlService.get('VOLUNTEERING.VALIDATE_ATTENDANCE_ID_JUSTIFICATION', { id }),
      { accept: true }
    );
  }

  rejectJustification(id: number, data) {
    return this.resourceService.create<any>(
      this.urlService.get('VOLUNTEERING.VALIDATE_ATTENDANCE_ID_JUSTIFICATION', { id }),
      Object.assign({ accept: false }, data)
    );
  }
}
