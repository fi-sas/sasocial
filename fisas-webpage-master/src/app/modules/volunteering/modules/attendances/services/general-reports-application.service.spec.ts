import { TestBed } from '@angular/core/testing';

import { GeneralReportsApplicationService } from './general-reports-application.service';

describe('GeneralReportsApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralReportsApplicationService = TestBed.get(GeneralReportsApplicationService);
    expect(service).toBeTruthy();
  });
});
