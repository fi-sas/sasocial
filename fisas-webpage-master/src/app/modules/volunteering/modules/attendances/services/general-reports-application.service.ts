import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { GeneralReportModel, IMonthlyReport, IMonthlyReportAlerts } from '../models';

@Injectable({
  providedIn: 'root',
})
export class GeneralReportsApplicationService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  /* id -> application_id */
  generalReports(id: number): Observable<Resource<GeneralReportModel>> {
    return this.resourceService.read<GeneralReportModel>(
      this.urlService.get('VOLUNTEERING.GENERAL_REPORTS_APPLICATIONS', { id })
    );
  }

  getMonthlyReports(
    pageIndex: number,
    pageSize: number,
    collaborationId: number
  ): Observable<Resource<IMonthlyReport>> {
    let params = new HttpParams();
    params = params.set('offset', ((pageIndex - 1) * pageSize).toString());
    params = params.set('limit', pageSize.toString());
    params = params.set('query[user_interest_id]', collaborationId.toString());
    params = params.set('sort', '-id');

    return this.resourceService.list<IMonthlyReport>(this.urlService.get('VOLUNTEERING.MONTHLY_REPORTS'), { params });
  }

  addMonthlyReports(data: object): Observable<Resource<IMonthlyReport>> {
    return this.resourceService.create<IMonthlyReport>(this.urlService.get('VOLUNTEERING.MONTHLY_REPORTS'), data);
  }

  addCollaborationEvaluationReport(id: number, avaliation_file: number): Observable<Resource<FileModel>> {
    return this.resourceService.create<FileModel>(this.urlService.get('VOLUNTEERING.ADD_REPORT_EVALUATION', { id }), {
      avaliation_file,
    });
  }

  getAlerts(id: number): Observable<Resource<IMonthlyReportAlerts>> {
    return this.resourceService.list<IMonthlyReportAlerts>(
      this.urlService.get('VOLUNTEERING.MONTHLY_REPORTS_ID_ALERTS', { id })
    );
  }
}
