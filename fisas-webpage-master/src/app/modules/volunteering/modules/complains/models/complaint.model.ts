export class ComplaintModel {
  id: number;
  complain: string;
  file_id: number;
  response: string;
  status: string;
  created_at: Date;
  updated_at: Date;

}

export enum ComplaintStatus {
  SUBMITTED = 'SUBMITTED',
  ANALYSED = 'ANALYSED',
  REPLYED = 'REPLYED'
}

export const ConfigStatusComplaint = [
    {
      name: ComplaintStatus.SUBMITTED,
      color: '#008452'
    },
    {
      name: ComplaintStatus.ANALYSED,
      color: '#0a5f38'
    },
    {
      name: ComplaintStatus.REPLYED,
      color: '#0a5f38'
    }
]