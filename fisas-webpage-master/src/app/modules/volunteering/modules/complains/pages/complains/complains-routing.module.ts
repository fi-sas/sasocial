import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComplainsComponent } from './complains.component';

const routes: Routes = [
  {
    path: '',
    component: ComplainsComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:complain-user-interests:list' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComplainsVolunteeringRoutingModule {}
