import { Component, OnInit } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { ConfigStatusComplaint } from '../../models/complaint.model';
import { ComplainsService } from '../../services/complains.service';

@Component({
  selector: 'app-complains',
  templateUrl: './complains.component.html',
  styleUrls: ['./complains.component.less']
})
export class ComplainsComponent implements OnInit {
  
  complaints = [];
  readonly configStatusComplaint = ConfigStatusComplaint;
  loading= false;
  pageIndex = 1;
  pageSize = 10;
  total = 0;

  constructor(
    private complaintsService: ComplainsService
  ) { }

  ngOnInit(){
    this.getComplaints();
  }

  paginationSearch(reset: boolean = false): void {
    if (reset) {
        this.pageIndex = 1;
    }
  }

  isPar(val) {
    return (val % 2 === 0);
  }

  getComplaints(){
    this.loading = true;
    this.complaintsService.getComplaintsByUser().pipe(first(), finalize(() => this.loading = false))
      .subscribe(response => {
        console.log(response.data)
        this.complaints = response.data;
        this.total = this.complaints.length;
      })
  }

}
