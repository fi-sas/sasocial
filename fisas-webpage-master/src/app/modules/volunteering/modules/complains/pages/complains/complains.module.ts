import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import { ComplainsVolunteeringRoutingModule } from './complains-routing.module';
import { ComplainsComponent } from './complains.component';


@NgModule({
  declarations: [
      ComplainsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SharedVolunteeringModule,
    ComplainsVolunteeringRoutingModule
  ],
  entryComponents: [],
})
export class ComplainsVolunteeringModule {}
