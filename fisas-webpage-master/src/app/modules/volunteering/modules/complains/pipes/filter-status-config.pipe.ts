import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterStatusConfig'
})
export class FilterStatusConfigPipe implements PipeTransform {

  transform(statusConfig: any[], status: string): any {
    return statusConfig.find( s => s.name === status);
  }


}
