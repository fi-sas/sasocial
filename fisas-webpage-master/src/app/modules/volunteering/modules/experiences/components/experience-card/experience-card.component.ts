import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models/experiences.model';
import { ExperiencesService } from './../../services/experiences.service';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { ManifestInterestComponent } from '../../../shared-volunteering/components';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-experience-card',
  templateUrl: './experience-card.component.html',
  styleUrls: ['./experience-card.component.less'],
})
export class ExperienceCardComponent {
  private _experience: ExperienceModel = null;
  @Input() set experience(experience: ExperienceModel) {
    this._experience = experience;

    this.interestService.canExpressInterest(experience.id).subscribe((result) => {
      this.canExpressInterest = result.canExpresssInteres;
      this.hasApplication = result.hasApplication;
      this.applicationActive = result.applicationActive;
    });
  }
  get experience() {
    return this._experience;
  }

  @Output() experienceChanged = new EventEmitter();

  translations: TranslationModel[] = [];

  canExpressInterest: boolean;
  hasApplication: boolean;
  applicationActive: boolean;
  user;
  constructor(
    private userService: AuthService,
    private configurationsService: ConfigurationsService,
    private experiencesService: ExperiencesService,
    private interestService: InterestService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService
  ) {
    this.user = this.userService.getUser();
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }

  experienceDetail() {
    this.experiencesService.saveExperience(this.experience);
  }

  expressInterest($event: MouseEvent) {
    // To prevent card click to being triggered
    $event.stopPropagation();

    if (!this.hasApplication) {
      return this.modalService.confirm({
        nzTitle: this.translateService.instant('VOLUNTEERING.NO_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('VOLUNTEERING.NO_APPLICATION.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: this.translateService.instant('MISC.APPLY'),
        nzOnOk: () => this.router.navigateByUrl('volunteering/applications'),
      });
    }

    if (!this.applicationActive) {
      return this.modalService.warning({
        nzTitle: this.translateService.instant('VOLUNTEERING.APPLICATION_NOT_ACTIVE.TITLE'),
        nzContent: this.translateService.instant('VOLUNTEERING.APPLICATION_NOT_ACTIVE.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: null,
      });
    }

    this.modalService
      .create({
        nzTitle: null,
        nzContent: ManifestInterestComponent,
        nzFooter: null,
        nzComponentParams: { experience: this.experience, translations: this.translations },
        nzWidth: 350,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.experienceChanged.emit(true);
        }
      });
  }
}
