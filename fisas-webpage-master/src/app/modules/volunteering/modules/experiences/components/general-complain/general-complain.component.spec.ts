import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralComplainComponent } from './general-complain.component';

describe('GeneralComplainComponent', () => {
  let component: GeneralComplainComponent;
  let fixture: ComponentFixture<GeneralComplainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GeneralComplainComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralComplainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
