import { Component, OnDestroy, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models/experiences.model';
import { ExperiencesService } from '../../services/experiences.service';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-list-experiences',
  templateUrl: './list-experiences.component.html',
  styleUrls: ['./list-experiences.component.less'],
})
export class ListExperiencesComponent implements OnInit, OnDestroy {
  isLoading = false;
  experiences: ExperienceModel[] = [];

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  readonly serviceId = SERVICE_IDS.VOLUNTEERING;

  constructor(private experiencesService: ExperiencesService) {}

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    this.getExperiences();
  }

  getExperiences() {
    this.isLoading = true;
    this.experiencesService
      .experiences(
        1,
        -1,
        'id',
        'DESC',
        'experience_responsible,organic_unit,attachment_file,translations',
        ['APPROVED', 'SELECTION', 'IN_COLABORATION'],
        true
      )
      .pipe(
        first(),
        finalize(() => (this.isLoading = false))
      )
      .subscribe((experiences) => (this.experiences = experiences.data));
  }
}
