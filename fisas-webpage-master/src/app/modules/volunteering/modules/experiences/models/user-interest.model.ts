import { UserModel } from "@fi-sas/webpage/auth/models/user.model";

export class UserInterestModel {
  experience_id: number;
  id?: number;
  is_interest?: true;
  user_id?: number;
  user: UserModel;
}
