import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel, IHasInterest } from './../../../../models';
import { ExperiencesService } from './../../services';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { ManifestInterestComponent } from '../../../shared-volunteering/components';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-experience-detail',
  templateUrl: './experience-detail.component.html',
  styleUrls: ['./experience-detail.component.less'],
})
export class ExperienceDetailComponent {
  readonly buttonStyle = {
    width: '100%',
    height: '50px',
    'font-size': '17.5px',
    'margin-top': '100px',
    'margin-bottom': '31.5px',
  };

  translations: TranslationModel[] = [];

  experienceId: number;
  experience: ExperienceModel;
  isLoading: boolean;
  user;
  interest: IHasInterest;
  isLoadingInterest: boolean;
  isLoadingExpressInterest: boolean;
  canExpressInterest: boolean;
  applicationActive: boolean;
  hasApplication: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private configurationsService: ConfigurationsService,
    private experiencesService: ExperiencesService,
    private interestService: InterestService,
    private modalService: NzModalService,
    private router: Router,
    private userService: AuthService,
    private translateService: TranslateService,
    private location: Location
  ) {
    this.user = this.userService.getUser();
    this.getTranslations();

    this.experienceId = this.activatedRoute.snapshot.params.id;
    if (!isNaN(this.experienceId)) {
      this.getExperience();
      this.checkInterest();
    }
  }

  back() {
    this.location.back();
    //this.router.navigateByUrl('volunteering/experiences');
  }

  getExperience() {
    this.experiencesService
      .experience(this.experienceId)
      .pipe((first(), finalize(() => (this.isLoading = false))))
      .subscribe((data) => (this.experience = data.data[0]));
  }

  checkInterest() {
    this.interestService.canExpressInterest(this.experienceId).subscribe((result) => {
      this.canExpressInterest = result.canExpresssInteres;
      this.hasApplication = result.hasApplication;
      this.applicationActive = result.applicationActive;
    });
  }

  expressInterest() {
    if (!this.hasApplication) {
      return this.modalService.confirm({
        nzTitle: this.translateService.instant('VOLUNTEERING.NO_APPLICATION.TITLE'),
        nzContent: this.translateService.instant('VOLUNTEERING.NO_APPLICATION.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: this.translateService.instant('MISC.APPLY'),
        nzOnOk: () => this.router.navigateByUrl('volunteering/applications'),
      });
    }

    if (!this.applicationActive) {
      return this.modalService.warning({
        nzTitle: this.translateService.instant('VOLUNTEERING.APPLICATION_NOT_ACTIVE.TITLE'),
        nzContent: this.translateService.instant('VOLUNTEERING.APPLICATION_NOT_ACTIVE.MESSAGE'),
        nzCancelText: this.translateService.instant('MISC.CLOSE'),
        nzOkText: null,
      });
    }

    this.modalService
      .create({
        nzTitle: null,
        nzContent: ManifestInterestComponent,
        nzFooter: null,
        nzComponentParams: { experience: this.experience, translations: this.translations },
        nzWidth: 350,
        nzWrapClassName: 'vertical-center-modal',
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        if (success) {
          this.checkInterest();
        }
      });
  }

  private getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }
}
