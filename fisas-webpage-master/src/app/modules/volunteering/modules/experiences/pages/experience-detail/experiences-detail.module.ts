import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ExperienceDetailComponent } from './experience-detail.component';
import { ExperiencesDetailVolunteeringRoutingModule } from './experiences-detail-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';

@NgModule({
  declarations: [ExperienceDetailComponent],
  imports: [CommonModule, SharedModule, ExperiencesDetailVolunteeringRoutingModule, SharedVolunteeringModule],
})
export class ExperiencesDetailVolunteeringModule {}
