import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { ApplicationsToAcceptService } from '../../services/applications-to-accept.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models/experiences.model';
import { ExperiencesService } from './../../services/experiences.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { NzModalService } from 'ng-zorro-antd';
import { GeneralComplainComponent } from '../../components/general-complain/general-complain.component';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.less'],
})
export class ExperiencesComponent implements OnDestroy {
  experience: ExperienceModel;
  showExperienceDetail = false;
  translations: TranslationModel[] = [];
  activeApplicationButton = false;
  idApplication: number;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(
    private applicationsToAcceptService: ApplicationsToAcceptService,
    private configurationsService: ConfigurationsService,
    private experiencesService: ExperiencesService,
    private modalService: NzModalService,
    private router: Router,
    private authService: AuthService,
  ) {

    this.getTranslations();
    if(this.authService.hasPermission('volunteering:applications:read')) {
      this.applicationUser();
    }

    this.subscriptions = this.experiencesService.experienceSelectedObservable().subscribe((experience) => {
      if (experience !== null) {
        this.experience = experience;
        this.showExperienceDetail = true;
        this.router.navigateByUrl('volunteering/experiences/' + this.experience.id);
      } else {
        this.showExperienceDetail = false;
        this.experience = null;
      }
    });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
    this.experiencesService.saveExperience(null);
  }

  complain() {
    this.modalService.create({
      nzTitle: null,
      nzContent: GeneralComplainComponent,
      nzFooter: null,
      nzWidth: 350,
      nzWrapClassName: 'vertical-center-modal',
      nzComponentParams: { translations: this.translations },
    });
  }

  getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }

  applicationUser() {
    this.applicationsToAcceptService
      .applicationUser()
      .pipe(first())
      .subscribe((data) => {
        this.activeApplicationButton = data.data[0].active;
        this.idApplication = data.data[0].id;
      });
    }

  goToApplication() {
    this.router.navigateByUrl('volunteering/applications');
  }

  goToOffer() {
    this.router.navigateByUrl('volunteering/actions');
  }

  goToReview() {
    this.router.navigateByUrl('volunteering/applications/review/' + this.idApplication);
  }
}
