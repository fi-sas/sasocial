import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExperienceCardComponent } from '../../components/experience-card/experience-card.component';
import { ExperiencesComponent } from './experiences.component';
import { ExperiencesVolunteeringRoutingModule } from './experiences-routing.module';
import { ListExperiencesComponent } from '../../components/list-experiences/list-experiences.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import { GeneralComplainComponent } from '../../components/general-complain/general-complain.component';

@NgModule({
  declarations: [ExperiencesComponent, GeneralComplainComponent, ListExperiencesComponent, ExperienceCardComponent],
  imports: [CommonModule, SharedModule, SharedVolunteeringModule, ExperiencesVolunteeringRoutingModule],
  entryComponents: [GeneralComplainComponent],
})
export class ExperiencesVolunteeringModule {}
