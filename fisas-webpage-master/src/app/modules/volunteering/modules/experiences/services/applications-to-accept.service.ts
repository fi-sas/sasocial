import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { VolunteeringModel } from '../../../models/volunteering-application.model';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsToAcceptService {
  accept = new BehaviorSubject(false);

  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  applicationsToAccept(): Observable<Resource<VolunteeringModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[status]', 'ACCEPTED');
    params = params.set('withRelated', 'experience');

    return this.resourceService.list<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS'),
      { params }
    );
  }

  acceptObservable(): Observable<boolean> {
    return this.accept.asObservable();
  }

  submittedAccept(submitted: boolean) {
    this.accept.next(submitted);
  }

  applicationUser(): Observable<Resource<any>> {
    return this.resourceService.read<any>(
      this.urlService.get('VOLUNTEERING.USER_APPLICATION')
    );
  }
}
