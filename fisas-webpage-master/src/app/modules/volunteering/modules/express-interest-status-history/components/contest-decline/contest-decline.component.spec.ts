import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContestDeclineComponent } from './contest-decline.component';

describe('ContestDeclineComponent', () => {
  let component: ContestDeclineComponent;
  let fixture: ComponentFixture<ContestDeclineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContestDeclineComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContestDeclineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
