import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';
import { ComplaintUserInterestModel } from '@fi-sas/webpage/modules/volunteering/models/complain-user-interests.model';

@Component({
  selector: 'app-contest-decline',
  templateUrl: './contest-decline.component.html',
  styleUrls: ['./contest-decline.component.less'],
})
export class ContestDeclineComponent extends FormHandler {
  @Input() set id(id: number) {
    this._id = id;
    this.form = this.getForm();
  }
  private _id: number;

  isLoading: boolean;
  form: FormGroup;
  filterTypes = ['application/pdf'];

  constructor(
    private interestService: InterestService,
    private modalRef: NzModalRef,
    private translateService: TranslateService
  ) {
    super();
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      const data: ComplaintUserInterestModel = new ComplaintUserInterestModel();
      data.complain =  this.form.value.complain;
      data.file_id = this.form.value.file_id;
      data.user_interest_id = this._id;

      this.interestService.contest(data).pipe(first(), finalize(()=> this.isLoading = false)).subscribe(() => {
        this.modalRef.close(true);
      });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  private getForm(): FormGroup {
    return new FormGroup({
      complain: new FormControl(null, [Validators.required, trimValidation]),
      file_id: new FormControl(null),
    });
  }
}
