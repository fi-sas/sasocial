import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestCardSmallComponent } from './interest-card-small.component';

describe('SmallInterestCardComponent', () => {
  let component: InterestCardSmallComponent;
  let fixture: ComponentFixture<InterestCardSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InterestCardSmallComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestCardSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
