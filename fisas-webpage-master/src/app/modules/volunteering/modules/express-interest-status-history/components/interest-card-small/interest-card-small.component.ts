import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { InterestModel } from '@fi-sas/webpage/modules/volunteering/models';

@Component({
  selector: 'app-interest-card-small',
  templateUrl: './interest-card-small.component.html',
  styleUrls: ['./interest-card-small.component.less'],
})
export class InterestCardSmallComponent {
  @Input() interest: InterestModel = null;

  constructor(private router: Router) {}

  goDetail($event: MouseEvent, id) {
    $event.stopPropagation();
    this.router.navigateByUrl('/volunteering/record/' + id)
  }
}
