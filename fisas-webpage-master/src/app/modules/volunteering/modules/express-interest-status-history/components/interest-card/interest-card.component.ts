import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { InterestModel, INTEREST_STATUS } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { ContestDeclineComponent } from '../contest-decline/contest-decline.component';
import { finalize, first } from 'rxjs/operators';
import { FormWithdrawalComponent } from '../../../shared-volunteering/components';

interface IActionButton {
  title: string;
  action: (i: number) => void;
  show: boolean;
  validStatus: INTEREST_STATUS[];
  loading: boolean;
  type: string;
}

@Component({
  selector: 'app-interest-card',
  templateUrl: './interest-card.component.html',
  styleUrls: ['./interest-card.component.less'],
})
export class InterestCardComponent {
  private _interest = null;
  get interest() {
    return this._interest;
  }
  @Input() set interest(interest: InterestModel) {
    this._interest = interest;
    this.approved = interest.status === INTEREST_STATUS.APPROVED;
    this.setButtons();
  }
  @Input() translations: TranslationModel[] = [];

  @Output() refresh = new EventEmitter();
  @Output() refreshAccepted = new EventEmitter();

  approved: boolean;
  buttons: IActionButton[] = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.MANAGE_ACTION',
      show: false,
      action: (_) => this.manage(),
      loading: false,
      validStatus: [INTEREST_STATUS.COLABORATION, INTEREST_STATUS.WITHDRAWAL],
      type: 'secundary'
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.WITHDRAWAL_ACTION',
      show: false,
      action: (i: number) => this.withdrawal(i),
      loading: false,
      validStatus: [INTEREST_STATUS.COLABORATION],
      type: 'red'
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.CONTEST',
      show: false,
      action: (_) => this.openContestModal(),
      loading: false,
      validStatus: [INTEREST_STATUS.NOT_SELECTED, INTEREST_STATUS.DECLINED],
      type: 'infoblue'
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.CANCEL',
      show: false,
      action: (i: number) => this.confirmAction('SOCIAL_SUPPORT.CONFIRM_CANCEL_INCRI', () => this.cancel(i)),
      loading: false,
      validStatus: [
        INTEREST_STATUS.WAITING,
        INTEREST_STATUS.APPROVED,
        INTEREST_STATUS.ACCEPTED,
        INTEREST_STATUS.SUBMITTED,
        INTEREST_STATUS.ANALYSED,
      ],
      type: 'red'
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.ACCEPT',
      show: false,
      action: (i: number) => this.accept(i),
      loading: false,
      validStatus: [INTEREST_STATUS.APPROVED],
      type: 'primary'
    },
  ];

  constructor(
    private interestService: InterestService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  private confirmAction(title: string, action: () => void) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant(title),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => action(),
    });
  }

  private withdrawal(actionIndex: number) {
    this.buttons[actionIndex].loading = true;
    this.modalService
      .create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: FormWithdrawalComponent,
        nzComponentParams: {
          applicationID: this.interest.id,
          action: 'WITHDRAWAL',
        },
        nzFooter: null,
      })
      .afterClose.pipe(first())
      .subscribe((success: boolean) => {
        this.buttons[actionIndex].loading = false;
        this.refresh.emit(success);
      });
  }

  private cancel(actionIndex: number) {
    this.buttons[actionIndex].loading = true;
    this.interestService.cancelInterest(this.interest.id).pipe(first(), finalize(() => this.buttons[actionIndex].loading = false))
    .subscribe(()=> {
      this.onActionSuccess('SOCIAL_SUPPORT.ORDER_CANCEL');
    });   
  }

  private manage() {
    this.router.navigateByUrl('volunteering/record/' + this.interest.id);
  }

  private openContestModal() {
    const modalRef = this.modalService.create({
      nzTitle: null,
      nzContent: ContestDeclineComponent,
      nzComponentParams: { id: this.interest.id },
      nzFooter: null,
    });
    modalRef.afterClose.pipe(first()).subscribe((result: boolean) => {
      if (result) {
        this.onActionSuccess('SOCIAL_SUPPORT.SUCCESS_COMPLAI');
      }
    });
  }

  private accept(actionIndex: number) {
    this.buttons[actionIndex].loading = true;
    this.interestService.accept(this.interest.id).pipe(first(), finalize(()=>  this.buttons[actionIndex].loading = false))
    .subscribe(() => {
      this.onActionSuccess('SOCIAL_SUPPORT.ORDER_ACCEPTED');
    });
  }

  private setButtons() {
    this.buttons.map((button: IActionButton) => {
      button.show = !!button.validStatus.find((s) => this.interest.status === s);
      return button;
    });
  }

  private onActionSuccess(message: string) {
    this.uiService.showMessage(MessageType.success, this.translateService.instant(message));
    this.refresh.emit(true);
  }
}
