import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExpressInterestStatusHistoryComponent } from './express-interest-status-history.component';

describe('ExpressInterestStatusHistoryComponent', () => {
  let component: ExpressInterestStatusHistoryComponent;
  let fixture: ComponentFixture<ExpressInterestStatusHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExpressInterestStatusHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressInterestStatusHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
