import { Component } from '@angular/core';

import { finalize, first } from 'rxjs/operators';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { InterestModel } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-express-interest-status-history',
  templateUrl: './express-interest-status-history.component.html',
  styleUrls: ['./express-interest-status-history.component.less'],
})
export class ExpressInterestStatusHistoryComponent {
  readonly panels = [
    {
      active: false,
      disabled: false,
      name: 'VOLUNTEERING.HISTORY_MANIFESTATIONS_TITLE',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  translations: TranslationModel[] = [];

  currentInterests: InterestModel[] = [];
  isLoadingCurrentInterests: boolean;

  pastInterests: InterestModel[] = [];
  isLoadingPastInterests: boolean;

  constructor(private interestService: InterestService, private configurationsService: ConfigurationsService) {}

  ngOnInit() {
    this.getTranslations();
    this.getPresentCollaborations();
    this.getPastCollaborations();
  }

  refresh(event) {
    if (event == true) {
      this.getPresentCollaborations();
    }
  }

  private getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }

  private getPresentCollaborations() {
    this.isLoadingCurrentInterests = true;
    this.interestService
      .listCurrentInterests()
      .pipe(
        first(),
        finalize(() => (this.isLoadingCurrentInterests = false))
      )
      .subscribe((result) => (this.currentInterests = result.data));
  }

  private getPastCollaborations() {
    this.isLoadingPastInterests = true;
    this.interestService
      .listPastInterests()
      .pipe(
        first(),
        finalize(() => (this.isLoadingPastInterests = false))
      )
      .subscribe((result) => (this.pastInterests = result.data));
  }
}
