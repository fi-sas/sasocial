import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContestDeclineComponent, InterestCardComponent, InterestCardSmallComponent } from '../../components';
import { ExpressInterestStatusHistoryComponent } from './express-interest-status-history.component';
import { ExpressInterestStatusHistoryVolunteeringRoutingModule } from './express-interest-status-history-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';

@NgModule({
  declarations: [
    ContestDeclineComponent,
    ExpressInterestStatusHistoryComponent,
    InterestCardComponent,
    InterestCardSmallComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ExpressInterestStatusHistoryVolunteeringRoutingModule,
    SharedVolunteeringModule,
  ],
  entryComponents: [ContestDeclineComponent],
})
export class ExpressInterestStatusHistoryVolunteeringModule {}
