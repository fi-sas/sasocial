import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionStatusBadgeComponent } from './action-status-badge.component';

describe('ActionStatusBadgeComponent', () => {
  let component: ActionStatusBadgeComponent;
  let fixture: ComponentFixture<ActionStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActionStatusBadgeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
