import { Component } from '@angular/core';

import { NzModalService } from 'ng-zorro-antd';

import { BaseStatusBadgeComponent } from '../base-status-badge/base-status-badge.component';

@Component({
  selector: 'app-action-status-badge',
  templateUrl: '../base-status-badge/base-status-badge.component.html',
  styleUrls: ['../base-status-badge/base-status-badge.component.less', './action-status-badge.component.less'],
})
export class ActionStatusBadgeComponent extends BaseStatusBadgeComponent {
  statusClassMapper = {
    ACCEPTED: 'VOLUNTEERING.ACTIONS.STATUS.ACCEPTED',
    ANALYSED: 'VOLUNTEERING.ACTIONS.STATUS.ANALYSED',
    APPROVED: 'VOLUNTEERING.ACTIONS.STATUS.APPROVED',
    // FIXME: MS must standardize
    CANCELED: 'VOLUNTEERING.ACTIONS.STATUS.CANCELLED',
    CANCELLED: 'VOLUNTEERING.ACTIONS.STATUS.CANCELLED',
    CLOSED: 'VOLUNTEERING.ACTIONS.STATUS.CLOSED',
    // FIXME: MS must standardize
    IN_COLABORATION: 'VOLUNTEERING.ACTIONS.STATUS.COLABORATION',
    COLABORATION: 'VOLUNTEERING.ACTIONS.STATUS.COLABORATION',
    DECLINED: 'VOLUNTEERING.ACTIONS.STATUS.DECLINED',
    NOT_SELECTED: 'VOLUNTEERING.ACTIONS.STATUS.NOT_SELECTED',
    SUBMITTED: 'VOLUNTEERING.ACTIONS.STATUS.SUBMITTED',
    WAITING: 'VOLUNTEERING.ACTIONS.STATUS.WAITING',
    WITHDRAWAL: 'VOLUNTEERING.ACTIONS.STATUS.WITHDRAWAL',
    SELECTION: 'VOLUNTEERING.ACTIONS.STATUS.SELECTION',
    WITHDRAWAL_ACCEPTED: 'VOLUNTEERING.ACTIONS.STATUS.WITHDRAWAL_ACCEPTED',
    DISPATCH: 'VOLUNTEERING.ACTIONS.STATUS.DISPATCH'
  };

  protected isAction = true;

  constructor(protected modalService: NzModalService) {
    super(modalService);
  }
}
