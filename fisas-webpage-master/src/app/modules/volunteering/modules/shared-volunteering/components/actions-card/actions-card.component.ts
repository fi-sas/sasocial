import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { finalize, first } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { VolunteeringActionsService } from '@fi-sas/webpage/modules/volunteering/services';

@Component({
  selector: 'app-actions-card',
  templateUrl: './actions-card.component.html',
  styleUrls: ['./actions-card.component.less'],
})
export class ActionsCardComponent {
  private _action : ExperienceModel = null;
  @Input() set action(action: ExperienceModel) {
    this._action = action;
    this.canGenerateReport = ['IN_COLABORATION', 'CLOSED'].includes(action.status);
  }
  get action() {
    return this._action;
  }
  successModalReport = false;
  @Input() showButtons = true;
  @Output() isCancel = new EventEmitter();
  @Output() openList = new EventEmitter();

  modalDetail = false;

  status = {
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    RETURNED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.RETURNED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    PUBLISHED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.PUBLISHED',
    REJECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.REJECTED',
    SEND_SEEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SEND_SEEM',
    EXTERNAL_SYSTEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXTERNAL_SYSTEM',
    SELECTION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SELECTION',
    IN_COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.IN_COLABORATION',
  };

  loadingGenerateReport: boolean;

  canGenerateReport: boolean;

  constructor(
    private actionsService: VolunteeringActionsService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  checkTagStatusColor() {
    const css = {};
    css[this.action.status] = true;
    return css;
  }

  manage() {
    this.openList.emit();
  }

  edit(idOffer: number) {
    this.router.navigateByUrl('volunteering/actions/' + idOffer);
  }

  cancel(idInterest: number) {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('SOCIAL_SUPPORT.OFFERS.CONFIRM_CANCEL_INCRI'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelConfirm(idInterest),
    });
  }

  cancelConfirm(idInterest: number) {
    this.actionsService
      .cancel(idInterest)
      .pipe(first())
      .subscribe(() => {
        this.uiService.showMessage(MessageType.success, this.translateService.instant('SOCIAL_SUPPORT.ORDER_CANCEL'));
        this.isCancel.emit(true);
      });
  }

  reviewExperience() {
    this.router.navigateByUrl('volunteering/experiences/' + this.action.id);
  }

  manageCollaborations() {
    this.router.navigateByUrl('volunteering/management-attendances/' + this.action.id);
  }

  generateReport() {
    this.loadingGenerateReport = true;
    this.actionsService.printOffer(this.action.id).pipe(first(), finalize(() => this.loadingGenerateReport = false))
      .subscribe(() => {
        this.successModalReport = true;
      });
  }
}
