import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsHistoryCardComponent } from './actions-history-card.component';

describe('ActionsHistoryCardComponent', () => {
  let component: ActionsHistoryCardComponent;
  let fixture: ComponentFixture<ActionsHistoryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActionsHistoryCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsHistoryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
