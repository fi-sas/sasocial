import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models';

@Component({
  selector: 'app-actions-history-card',
  templateUrl: './actions-history-card.component.html',
  styleUrls: ['./actions-history-card.component.less'],
})
export class ActionsHistoryCardComponent {
  @Input() action: ExperienceModel = null;

  constructor(private router: Router) {}

  reviewExperience(idExperience: number) {
    this.router.navigateByUrl('volunteering/experiences/' + idExperience);
  }

  goToOfferAttendances($event: MouseEvent,idExperience: number) {
    $event.stopPropagation();
    this.router.navigateByUrl('volunteering/management-attendances/' + idExperience);
  }
}
