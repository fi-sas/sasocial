import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationStatusBadgeComponent } from './application-status-badge.component';

describe('ApplicationStatusBadgeComponent', () => {
  let component: ApplicationStatusBadgeComponent;
  let fixture: ComponentFixture<ApplicationStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationStatusBadgeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
