import { Component } from '@angular/core';

import { NzModalService } from 'ng-zorro-antd';

import { BaseStatusBadgeComponent } from '../base-status-badge/base-status-badge.component';

@Component({
  selector: 'app-application-status-badge',
  templateUrl: '../base-status-badge/base-status-badge.component.html',
  styleUrls: ['../base-status-badge/base-status-badge.component.less', './application-status-badge.component.less'],
})
export class ApplicationStatusBadgeComponent extends BaseStatusBadgeComponent {
  statusClassMapper = {
    SUBMITTED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.SUBMITTED',
    ANALYSED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.ANALYSED',
    ACCEPTED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.ACCEPTED',
    DECLINED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.DECLINED',
    EXPIRED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.EXPIRED',
    CANCELLED: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.CANCELLED',
    COLABORATION: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.COLABORATION',
    DISPATCH: 'VOLUNTEERING.MY_APPLICATIONS.STATUS.DISPATCH',
  };

  protected isApplication = true;

  constructor(protected modalService: NzModalService) {
    super(modalService);
  }
}
