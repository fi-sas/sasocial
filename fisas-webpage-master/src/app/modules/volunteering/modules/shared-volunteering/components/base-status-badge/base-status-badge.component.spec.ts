import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseStatusBadgeComponent } from './base-status-badge.component';

describe('BaseStatusBadgeComponent', () => {
  let component: BaseStatusBadgeComponent;
  let fixture: ComponentFixture<BaseStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BaseStatusBadgeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
