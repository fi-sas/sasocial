import { Component, Input } from '@angular/core';

import { NzModalService } from 'ng-zorro-antd';

import { StatusHistoryComponent } from '../status-history/status-history.component';
import { IStatusBadgeData } from '../../models';

@Component({
  selector: 'app-base-status-badge',
  templateUrl: './base-status-badge.component.html',
  styleUrls: ['./base-status-badge.component.less'],
})
export class BaseStatusBadgeComponent {
  @Input() data: IStatusBadgeData = null;
  @Input() clickable: boolean = true;

  statusClassMapper = {};

  protected isApplication: boolean = false;
  protected isAction: boolean = false;

  constructor(protected modalService: NzModalService) {}

  openHistory($event: MouseEvent) {
    // To prevent card click to being triggered
    $event.stopPropagation();

    this.modalService.create({
      nzTitle: null,
      nzContent: StatusHistoryComponent,
      nzComponentParams: {
        data: this.data,
        statusMapper: this.statusClassMapper,
        isApplication: this.isApplication,
        isAction: this.isAction,
      },
      nzFooter: null,
    });
  }
}
