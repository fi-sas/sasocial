import { Component, Input, Type } from '@angular/core';

import { ModalOptionsForService, NzModalService } from 'ng-zorro-antd';
import { ExperienceEvaluationComponent } from '../experience-evaluation/experience-evaluation.component';
import { FormAbsencesComponent } from '../form-absences/form-absences.component';
import { FormAttendanceComponent } from '../form-attendance/form-attendance.component';
import { FormWithdrawalComponent } from '../form-withdrawal/form-withdrawal.component';
import { InterestModel, INTEREST_STATUS, INTEREST_STATUS_EVENT } from '@fi-sas/webpage/modules/volunteering/models';
import { TranslateService } from '@ngx-translate/core';
import { InterestService, WithdrawalService } from '@fi-sas/webpage/modules/volunteering/services';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-buttons-panel',
  templateUrl: './buttons-panel.component.html',
  styleUrls: ['./buttons-panel.component.less'],
})
export class ButtonsPanelComponent {
  buttonStyle = {
    width: '100%',
    'text-overflow': 'ellipsis',
    'white-space': 'nowrap',
    overflow: 'hidden',
  };

  buttonStyle2 = {
    width: '100%',
    color: 'black',
    'text-overflow': 'ellipsis',
    'white-space': 'nowrap',
    overflow: 'hidden',
  };

  private _collaboration: InterestModel;
  @Input() set collaboration(collaboration: InterestModel) {
    if (collaboration) {
      this._collaboration = collaboration;

      this.disableActiveCollaborationActions = [
        INTEREST_STATUS.CLOSED.toString(),
        INTEREST_STATUS.CANCELLED.toString(),
        INTEREST_STATUS.WITHDRAWAL_ACCEPTED.toString(),
      ].includes(collaboration.status);

      this.collaborationStillGoing = [
        INTEREST_STATUS.COLABORATION.toString(),
        INTEREST_STATUS.WITHDRAWAL.toString(),
      ].includes(collaboration.status);

      this.disableWithdrawal = collaboration.status === INTEREST_STATUS.WITHDRAWAL.toString();
    }
  }

  get collaboration() {
    return this._collaboration;
  }

  loadingCertificateButton: boolean;

  disableActiveCollaborationActions: boolean;
  disableWithdrawal: boolean;
  collaborationStillGoing: boolean;

  constructor(private modalService: NzModalService,
    private translateService: TranslateService,
    private interestService: InterestService,
    private withdrawalService: WithdrawalService,
    private uiService: UiService) {}

  recordAttendance() {
    this.modalService.create(
      this.getModalOptions<FormAttendanceComponent>(
        FormAttendanceComponent,
        { collaboration: this._collaboration },
        { nzWidth: 750 }
      )
    );
  }

  absence() {
    this.modalService.create(
      this.getModalOptions<FormAbsencesComponent>(FormAbsencesComponent, { collaboration: this._collaboration })
    );
  }

  withdrawal() {
    this.modalService.create(
      this.getModalOptions<FormWithdrawalComponent>(FormWithdrawalComponent, {
        applicationID: this._collaboration.id,
        action: 'WITHDRAWAL',
      })
    );
  }

  cancelWithdrawal(){
    this.modalService.confirm({
      nzTitle: this.translateService.instant('VOLUNTEERING.ATTENDANCES.CANCEL_WITHDRAWAL_MODAL_TITLE'),
      nzCancelText: this.translateService.instant('VOLUNTEERING.BUTTONS.NO'),
      nzOkText: this.translateService.instant('VOLUNTEERING.BUTTONS.YES'),
      nzOnOk: () => {
        this.interestService.changeStatus(this.collaboration.id, INTEREST_STATUS_EVENT.COLABORATION)
          .pipe(first())
          .subscribe(()=> {
            this.withdrawalService.submittedCancelWithdrawal(true);
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('VOLUNTEERING.ATTENDANCES.WITHDRAWAL_CANCEL_SUCCESSFUL')
            );
          })
      }
    });
  }

  experimentReport() {
    this.modalService.create(
      this.getModalOptions<ExperienceEvaluationComponent>(
        ExperienceEvaluationComponent,
        { collaboration: this.collaboration },
        { nzWidth: 700 }
      )
    );
  }

  getCertificate() {
    if (this._collaboration.certificate_file) {
      window.open(this._collaboration.certificate_file.url, '_blank');
    } else if (this._collaboration.certificate_generated && this._collaboration.certificate_generated.file) {
      window.open(this._collaboration.certificate_generated.file.url, '_blank');
    }
  }

  private getModalOptions<T>(
    component: Type<T>,
    params: Partial<T>,
    override: ModalOptionsForService<T> = {}
  ): ModalOptionsForService<T> {
    return Object.assign(
      {
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: component,
        nzComponentParams: params,
        nzFooter: null,
      },
      override
    );
  }
}
