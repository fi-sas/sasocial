import { Component, Input, OnInit } from '@angular/core';

import { first } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';

import { AttendancesService, GeneralReportsApplicationService } from '../../../attendances/services';
import { GeneralReportModel } from '../../../attendances/models';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {
  @Input() applicationID = 0;
  loadingReports = false;

  generalReports: GeneralReportModel = null;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(
    private generalReportsApplicationService: GeneralReportsApplicationService,
    private attendancesService: AttendancesService,
  ) {}

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnChanges() {
    this.getGeneralReports();
  }

  ngOnInit() {
    this.subscriptions = merge(
      this.attendancesService.absencesObservable(),
      this.attendancesService.absencesReasonObservable(),
      this.attendancesService.attendanceObservable(),
    ).subscribe(() => this.getGeneralReports());
  }

  getGeneralReports() {
    this.loadingReports = true;
    this.generalReportsApplicationService
      .generalReports(this.applicationID)
      .pipe(first())
      .subscribe(
        (generalReports) => {
          this.generalReports = generalReports.data[0];
          this.loadingReports = false;
        },
        () => {
          this.loadingReports = false;
        }
      );
  }
}
