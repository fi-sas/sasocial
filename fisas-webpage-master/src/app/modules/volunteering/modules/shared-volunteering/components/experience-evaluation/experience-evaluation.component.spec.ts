import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceEvaluationComponent } from './experience-evaluation.component';

describe('ExperienceEvaluationComponent', () => {
  let component: ExperienceEvaluationComponent;
  let fixture: ComponentFixture<ExperienceEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExperienceEvaluationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
