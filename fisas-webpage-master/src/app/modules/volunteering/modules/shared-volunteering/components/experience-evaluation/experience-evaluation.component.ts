import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { InterestModel } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-experience-evaluation',
  templateUrl: './experience-evaluation.component.html',
  styleUrls: ['./experience-evaluation.component.less'],
})
export class ExperienceEvaluationComponent extends FormHandler {
  private _collaboration: InterestModel = null;
  @Input() set collaboration(collaboration: InterestModel) {
    this._collaboration = collaboration;

    this.form = new FormGroup({ avaliation: new FormControl(null, [Validators.required,trimValidation]) });

    if (this.collaboration.student_avaliation) {
      this.form.get('avaliation').setValue(this.collaboration.student_avaliation);
      this.form.disable();
    }
  }

  get collaboration() {
    return this._collaboration;
  }

  form: FormGroup;

  isLoading: boolean;

  constructor(
    private interestService: InterestService,
    private uiService: UiService,
    private modalRef: NzModalRef,
    public translateService: TranslateService
  ) {
    super();
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.isLoading = true;
      const data = {
        avaliation: this.form.get('avaliation').value,
      };
      this.interestService.collaborationEvaluation(this.collaboration.id, data)
        .pipe(first(), finalize(() => this.isLoading = false))
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('SOCIAL_SUPPORT.EXPERIENCES.EVALUATION.SUCCESS_MESSAGE')
          );
          this.modalRef.close(true);
        });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }
}
