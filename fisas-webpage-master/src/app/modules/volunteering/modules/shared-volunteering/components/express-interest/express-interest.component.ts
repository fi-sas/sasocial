import { Component, Input } from '@angular/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-express-interest',
  templateUrl: './express-interest.component.html',
  styleUrls: ['./express-interest.component.less'],
})
export class ExpressInterestComponent {
  @Input() experience: ExperienceModel = null;
  @Input() translations: TranslationModel[] = [];

  loadingButton: boolean;

  constructor(
    private interestService: InterestService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  close(success: boolean = false) {
    this.modalRef.close(success);
  }

  confirmInterest() {
    this.loadingButton = true;
    this.interestService
      .createUserInterest(this.experience.id)
      .pipe(
        first(),
        finalize(() => (this.loadingButton = false))
      )
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.EXPERIENCES.INTEREST_MODAL.SUCCESS')
        );
        this.close(true);
      });
  }
}
