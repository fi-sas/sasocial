import { Component, Input } from '@angular/core';

import { FileModel } from '@fi-sas/webpage/modules/media/models/file.model';

@Component({
  selector: 'app-file-button',
  templateUrl: './file-button.component.html',
  styleUrls: ['./file-button.component.less'],
})
export class FileButtonComponent {
  @Input() file: FileModel;
  @Input() filename: string;
  @Input() pending: boolean = false;
  @Input() failed: boolean = false;

  constructor() {}
}
