import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAbsencesComponent } from './form-absences.component';

describe('FormAbsencesComponent', () => {
  let component: FormAbsencesComponent;
  let fixture: ComponentFixture<FormAbsencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormAbsencesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAbsencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
