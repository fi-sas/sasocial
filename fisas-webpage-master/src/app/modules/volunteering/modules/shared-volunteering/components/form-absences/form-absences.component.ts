import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AttendanceModel } from '../../../attendances/models';
import { AttendancesService } from '../../../attendances/services';
import { BaseFormAttendanceComponent } from '../base-form-attendance';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

interface ICache {
  [key: string]: number[];
}

@Component({
  selector: 'app-form-absences',
  templateUrl: './form-absences.component.html',
  styleUrls: ['./form-absences.component.less'],
})
export class FormAbsencesComponent extends BaseFormAttendanceComponent {
  errorDates = false;
  loadingSubmit: boolean;

  form: FormGroup;

  private disabledHoursCache: ICache = {};
  private disabledMinutesCache: ICache = {};
  start;
  end;
  
  constructor(
    private modal: NzModalRef,
    private translate: TranslateService,
    private uiService: UiService,
    protected attendancesService: AttendancesService
  ) {
    super(attendancesService);
    this.form = new FormGroup({
      final_time: new FormControl(null, [Validators.required]),
      initial_time: new FormControl(null, [Validators.required]),
      notes: new FormControl('', [Validators.required,trimValidation]),
    });
  }

  protected getAttendances(collaborationId: number){
    super.getAttendances(collaborationId);
    if(this._collaboration && this._collaboration.experience) {
      this.start = moment(this._collaboration.experience.start_date);
      this.end = moment(this._collaboration.experience.end_date);
    }
  
  }

  onSubmit() {
    this.errorDates = false;
    if(this.form.get('initial_time').value && this.form.get('final_time').value) {
      if(moment(this.form.get('initial_time').value).isAfter(this.form.get('final_time').value)){
        this.errorDates = true;
      }
    }
    if (this.isFormValid(this.form) && !this.errorDates) {
      this.loadingSubmit = true;
      this.attendancesService
        .create(this.getAttendanceInstance(this.form))
        .pipe(
          first(),
          finalize(() => (this.loadingSubmit = false))
        )
        .subscribe(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translate.currentLang == 'pt' ? 'Falta submetida com sucesso' : 'Absence sucessfully submitted'
          );
          this.attendancesService.submittedAbsences();
          this.modal.close(true);
        });
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translate.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  disabledTime = (currentDate: Date) => {
    return {
      nzDisabledHours: (): number[] => this.disabledHours(currentDate),
      nzDisabledMinutes: (hour: number): number[] => this.disabledMinutes(currentDate, hour),
      nzDisabledSeconds: (): number[] => [],
    };
  };

  private disabledHours(current: Date): number[] {
    return this.disableHoursMinutesHelper(
      current,
      this.disabledHoursCache,
      24,
      (currentDate: moment.Moment, i: number) => currentDate.set({ hour: i })
    );
  }

  private disabledMinutes(current: Date, hour: number): number[] {
    if (hour === undefined) {
      return this.range60;
    }
    return this.disableHoursMinutesHelper(
      current,
      this.disabledMinutesCache,
      60,
      (currentDate: moment.Moment, i: number) => currentDate.set({ minute: i })
    );
  }

  private disableHoursMinutesHelper(
    current: Date,
    cache: ICache,
    loopMax: number,
    getDateCallback: (d: moment.Moment, i: number) => moment.Moment
  ): number[] {
    const currentDate = moment(current);
    const result: number[] = [];
    const cacheKey = currentDate.unix();
    if (cache.hasOwnProperty(cacheKey)) {
      return cache[cacheKey];
    }
    for (let i = 0; i < loopMax; i++) {
      if (this.overlapsCollaborationAttendances(getDateCallback(currentDate, i))) {
        result.push(i);
      }
    }
    cache[cacheKey] = result;
    return result;
  }

  private getAttendanceInstance(form: FormGroup): AttendanceModel {
    const attendance = new AttendanceModel();

    attendance.user_interest_id = this._collaboration.id;
    attendance.notes = form.value.notes;
    attendance.was_present = false;
    attendance.initial_time = this.transformToIsoDate(form.value.initial_time as Date);
    attendance.final_time = this.transformToIsoDate(form.value.final_time as Date);

    return attendance;
  }

  private transformToIsoDate(date: Date): string {
    const momentDate = moment(date);
    momentDate.set({ second: 0, millisecond: 0 });
    return momentDate.toISOString();
  }
}
