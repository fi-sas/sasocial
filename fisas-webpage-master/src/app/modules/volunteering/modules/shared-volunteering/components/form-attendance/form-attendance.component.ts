import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AttendanceModel, IAttendancesTableRow, IMonthlyReportAlerts } from '../../../attendances/models';
import { AttendancesService } from '../../../attendances/services';
import { BaseFormAttendanceComponent } from '../base-form-attendance';
import { GeneralReportsApplicationService } from '../../../attendances/services/general-reports-application.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Resource } from '@fi-sas/core';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-form-attendance',
  templateUrl: './form-attendance.component.html',
  styleUrls: ['./form-attendance.component.less'],
})
export class FormAttendanceComponent extends BaseFormAttendanceComponent implements OnInit{
  edit = false;
  attendance: IAttendancesTableRow = null;
  collaborationDateEnd;
  idCola: number;
  collaborationDateStart;
  loadingSubmit: boolean;
  start;
  end;
  form: FormGroup;
  errorDates = false;
  readonly minuteStep: number = 1;
  private minuteRange = this.range(60, 0, this.minuteStep);

  constructor(
    protected attendancesService: AttendancesService,
    protected modal: NzModalRef,
    protected uiService: UiService,
    public translate: TranslateService,
    private generalReportsApplicationService: GeneralReportsApplicationService
  ) {
    super(attendancesService);
    this.form = new FormGroup({
      date: new FormControl(null, Validators.required),
      executed_service: new FormControl(null, [Validators.required, trimValidation]),
      final_time: new FormControl({ value: null, disabled: true }, Validators.required),
      initial_time: new FormControl({ value: null, disabled: true }, Validators.required),
      n_hours: new FormControl({ value: null, disabled: true }, Validators.required),
    });

    this.form
      .get('date')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value) => this.enableDisableTimeFields('initial_time', value));
    this.form
      .get('initial_time')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.enableDisableTimeFields('final_time', value);
        this.onTimeChanged();
      });

    this.form
      .get('final_time')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => this.onTimeChanged());
  }

  ngOnInit(){
    if(this.attendance){
      this.start = moment(this.collaborationDateStart);
      this.end = moment(this.collaborationDateEnd);
      this.edit = true;
      let difference = moment.duration();
      difference = moment.duration(moment(this.attendance.attendance.final_time).diff(moment(this.attendance.attendance.initial_time)));
      this.form.patchValue({
        executed_service: this.attendance.attendance.executed_service,
        date: new Date(this.attendance.attendance.date),
        final_time: new Date(this.attendance.attendance.final_time),
        initial_time:  new Date(this.attendance.attendance.initial_time),
        n_hours: difference.hours().toString().padStart(2, '0') + ':' + difference.minutes().toString().padStart(2, '0'),
      })
    }
  }

  getAttendances(collaborationId: number){
    super.getAttendances(collaborationId);
    if(this._collaboration && this._collaboration.experience) {
      this.start = moment(this._collaboration.experience.start_date);
      this.end = moment(this._collaboration.experience.end_date);
    }


  }

  onSubmit() {
    this.errorDates = false;
    if(this.form.get('initial_time').value && this.form.get('final_time').value) {
      if(moment(this.form.get('initial_time').value).isAfter(this.form.get('final_time').value)){
        this.errorDates = true;
      }
    }
    if (this.isFormValid(this.form) && !this.errorDates) {
      this.loadingSubmit = true;
      if(this.edit) {
        this.attendancesService.update(this.attendance.attendance.id, this.getAttendanceInstance(this.form))
          .pipe(first(), finalize(() => this.loadingSubmit = false))
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              this.translate.instant('VOLUNTEERING.ATTENDANCES.UPDATE_SUCCESS')
            );
            this.modal.close(true);
            this.attendancesService.submittedAttendance();
          })
      }else{
        this.attendancesService.create(this.getAttendanceInstance(this.form))
          .pipe(first(), finalize(() => this.loadingSubmit = false))
          .subscribe(() => {
            this.uiService.showMessage(
              MessageType.success,
              this.translate.instant('VOLUNTEERING.ATTENDANCES.CREATE_SUCCESS')
            );
            this.modal.close(true);
            this.attendancesService.submittedAttendance();
          })
      }
    
    }
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translate.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  disableDate = (currentDate: Date): boolean => {
    const d = moment(currentDate);
    if(this.edit) {
      return d.isAfter(moment(), 'day') || this.disabledEdit(d);
    }else{
      return d.isAfter(moment(), 'day') || this.overlapsCollaborationAttendances(d);
    }

  };


  disabledEdit(
    date: moment.Moment,
    granularity: moment.unitOfTime.StartOf = 'day'
  ): boolean {
    if (!date.isBetween(this.collaborationDateStart, this.collaborationDateEnd, granularity, '[]')) {
      return true;
    }
    if (this.attendances.length === 0) {
      return false;
    }
    return this.dates.some((d) => {
      if (date.isSame(d.start, granularity) || date.isSame(d.end, granularity)) {
        return false;
      }
      return date.isBetween(d.start, d.end, granularity);
    });
  }

  disabledHours = (): number[] => {
    const initialTime = moment(this.form.get('initial_time').value);
    const beginDate = moment(this.form.get('date').value);
    beginDate.set({ hour: initialTime.get('hour'), minute: 0, second: 0, millisecond: 0 });

    const disabledFutureHours = this.disabledFutureHours();
    return this.range24.filter(
      (hour: number) =>
        disabledFutureHours.includes(hour) ||
        this.hourHasAllMinutesDisabled(hour) ||
        moment(this.form.get('date').value)
          .set({ hour, minute: 0, second: 0, millisecond: 0 })
          .isBefore(beginDate, 'hour')
    );
  };

  disabledFutureHours = (): number[] => {
    if (this.isInputDateToday()) {
      const currentHour = moment().get('hour');
      return this.range24.filter((hour: number) => {
        if (currentHour === hour) {
          return this.hourHasAllMinutesDisabled(currentHour);
        }
        return hour > moment().get('hour');
      });
    }

    return [];
  };

  disabledMinutes = (hour?: number): number[] => {
    const invalidMinutes = this.minuteRange;
    if (isNaN(hour)) {
      return invalidMinutes;
    }

    const disabledFutureMinutes = this.disabledFutureMinutes(hour);
    const initialTime = moment(this.form.get('initial_time').value);
    if (hour > initialTime.get('hour')) {
      return disabledFutureMinutes;
    }
    const beginDate = moment(this.form.get('date').value);
    beginDate.set({ hour: initialTime.get('hour'), minute: initialTime.get('minute'), second: 0, millisecond: 0 });

    return invalidMinutes.filter(
      (minute: number) =>
        disabledFutureMinutes.includes(minute) ||
        moment(this.form.get('date').value)
          .set({ hour: initialTime.get('hour'), minute, second: 0, millisecond: 0 })
          .isSameOrBefore(beginDate, 'minute')
    );
  };

  disabledFutureMinutes = (hour?: number): number[] => {
    const invalidMinutes = this.minuteRange;
    if (isNaN(hour)) {
      return invalidMinutes;
    }

    if (this.isInputDateToday()) {
      return invalidMinutes.filter((minute: number) =>
        moment(this.form.get('date').value).set({ hour, minute, second: 0, millisecond: 0 }).isAfter(moment(), 'minute')
      );
    }

    return [];
  };

  private hourHasAllMinutesDisabled(hour: number) {
    return this.disabledMinutes(hour).length === this.minuteRange.length;
  }

  private isInputDateToday() {
    return moment(this.form.get('date').value).isSame(moment(), 'day');
  }

  private getAttendanceInstance(form: FormGroup): AttendanceModel {
    const attendance = new AttendanceModel();
    attendance.user_interest_id = this.edit ? this.idCola : this._collaboration.id;
    attendance.was_present = true;
    attendance.notes = 'Presença registrada pelo utilizador';
    attendance.executed_service = form.value.executed_service;
    attendance.initial_time = this.getDateTime(form.value.date, form.value.initial_time) ;
    attendance.final_time = this.getDateTime(form.value.date, form.value.final_time);
    attendance.date = form.value.date;
    return attendance;
  }

  private getDateTime(date: Date, time: Date) {
    date.setUTCHours(time.getUTCHours(), time.getUTCMinutes(), 0, 0);
    return moment(date).toISOString();
  }

  private enableDisableTimeFields(controlName: string, value: any) {
    const control = this.form.get(controlName);
    if (value) {
      control.enable();
    } else {
      control.disable();
    }
    control.updateValueAndValidity();
  }

  private onTimeChanged() {
    const initial_time = this.form.value.initial_time;
    const final_time = this.form.value.final_time;

    let difference = moment.duration();
    if (initial_time && final_time) {
      if (new Date(this.form.value.initial_time) >= new Date(this.form.value.final_time)) {
        this.form.controls.final_time.setErrors({ biggerThanInitial: true });
        return;
      }

      difference = moment.duration(moment(final_time).diff(moment(initial_time)));
    }
    this.form.controls.n_hours.setValue(
      difference.hours().toString().padStart(2, '0') + ':' + difference.minutes().toString().padStart(2, '0')
    );
  }

/*
  private async checkExceedsMaxHours() {
    let result: Resource<IMonthlyReportAlerts>;
    try {
      result = await this.generalReportsApplicationService.getAlerts(this._collaboration.id).pipe(first()).toPromise();
    } catch (_) {
      return Promise.reject();
    }

    if (result.data[0].current_month.alert) {
      this.uiService.showMessage(
        MessageType.success,
        this.translate.instant('SOCIAL_SUPPORT.ATTENDANCES.FORM.MAX_HOURS_EXCEEDED')
      );
    }

    return Promise.resolve(false);
  }*/
}
