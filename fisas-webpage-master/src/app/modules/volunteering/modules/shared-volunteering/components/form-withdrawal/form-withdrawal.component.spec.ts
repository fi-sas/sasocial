import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormWithdrawalComponent } from './form-withdrawal.component';

describe('FormWithdrawalComponent', () => {
  let component: FormWithdrawalComponent;
  let fixture: ComponentFixture<FormWithdrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormWithdrawalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
