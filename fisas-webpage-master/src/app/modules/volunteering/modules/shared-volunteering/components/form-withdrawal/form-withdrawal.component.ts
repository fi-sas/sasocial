import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { ChangeStatusModel } from '@fi-sas/webpage/modules/social-support/models/change-status.model';
import { ChangeStatusService, InterestService, WithdrawalService } from '@fi-sas/webpage/modules/volunteering/services';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { UiService, MessageType } from '@fi-sas/webpage/core/services/ui.service';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-form-withdrawal',
  templateUrl: './form-withdrawal.component.html',
  styleUrls: ['./form-withdrawal.component.less'],
})
export class FormWithdrawalComponent extends FormHandler {
  @Input() applicationID = 0;
  @Input() action: string = null;

  loadingSubmit = false;


  withdrawalForm: FormGroup = new FormGroup({
    reason: new FormControl('',[Validators.required, trimValidation]),
    attachment: new FormControl(null),
  });

  constructor(
    private changeStatusService: ChangeStatusService,
    private interestService: InterestService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,
    private uiService: UiService,
    private withdrawalService: WithdrawalService
  ) {
    super();
  }

  submitWithdrawal() {
    if (this.isFormValid(this.withdrawalForm)) {
      const withdrawal = new ChangeStatusModel();
      withdrawal.notes = this.withdrawalForm.value.reason;

      this.loadingSubmit = true;
      if (this.action === 'WITHDRAWAL') {
        this.interestService
          .withdrawInterest(this.applicationID, withdrawal)
          .pipe(
            first(),
            finalize(() => (this.loadingSubmit = false))
          )
          .subscribe(() => {
            this.withdrawalService.submittedWithdrawal(true);
            this.modalRef.close(true);
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('DASHBOARD.WIDGETS.MY_APPLICATIONS_SOCIAL_SUPPORT.MODAL_WITHDRAWAL.SUCCESS')
            );
          });
      } else if (this.action === 'CANCEL') {
        this.changeStatusService
          .applicationCancel(this.applicationID, withdrawal)
          .pipe(
            first(),
            finalize(() => (this.loadingSubmit = false))
          )
          .subscribe(() => {
            this.withdrawalService.submittedCancel(true);
            this.modalRef.close(true);
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('SOCIAL_SUPPORT.APPLICATIONS.CANCEL_SUCCESS')
            );
          });
      }
    }
  }

  getInputError(inputName: string): string {
    if (this.withdrawalForm.get(inputName).errors.required || this.withdrawalForm.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }
}
