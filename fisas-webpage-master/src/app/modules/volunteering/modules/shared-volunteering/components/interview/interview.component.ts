import { Component, Input } from '@angular/core';

import { InterviewModel } from '@fi-sas/webpage/modules/volunteering/models';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.less'],
})
export class InterviewComponent {
  @Input() interview: InterviewModel = null;

  constructor() {}
}
