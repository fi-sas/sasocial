import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestInterestComponent } from './manifest-interest.component';

describe('ManifestInterestComponent', () => {
  let component: ManifestInterestComponent;
  let fixture: ComponentFixture<ManifestInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManifestInterestComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
