import { Component, Input } from '@angular/core';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models';
import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { NzModalRef } from 'ng-zorro-antd';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-manifest-interest',
  templateUrl: './manifest-interest.component.html',
  styleUrls: ['./manifest-interest.component.less'],
})
export class ManifestInterestComponent {
  @Input() experience: ExperienceModel;
  @Input() translations: TranslationModel[] = [];

  isLoading: boolean;

  constructor(private interestService: InterestService, private modalRef: NzModalRef) {}

  confirmInterest() {
    this.isLoading = true;
    this.interestService.createUserInterest(this.experience.id).pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(() => {
        this.close(true);
      });
  }

  close(success: boolean = false) {
    this.modalRef.close(success);
  }
}
