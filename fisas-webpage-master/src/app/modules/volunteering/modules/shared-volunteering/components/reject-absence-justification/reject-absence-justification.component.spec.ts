import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectAbsenceJustificationComponent } from './reject-absence-justification.component';

describe('RejectAbsenceJustificationComponent', () => {
  let component: RejectAbsenceJustificationComponent;
  let fixture: ComponentFixture<RejectAbsenceJustificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RejectAbsenceJustificationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectAbsenceJustificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
