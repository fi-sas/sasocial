import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AttendanceModel } from '../../../attendances/models';
import { AttendancesService } from '../../../attendances/services';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { merge } from 'rxjs';
import { finalize, first, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-reject-attendance',
  templateUrl: './reject-attendance.component.html',
  styleUrls: ['./reject-attendance.component.less'],
})
export class RejectAttendanceComponent extends FormHandler {
  private _attendance: AttendanceModel;
  readonly MAX_TEXTAREA_CHARS = 1000;

  @Input() set attendance(attendance: AttendanceModel) {
    if (attendance) {
      this._attendance = attendance;

      this.form = this.getForm();
      merge(this.form.get('initial_time').valueChanges, this.form.get('final_time').valueChanges)
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => this.onTimeChanged());
    }
  }

  get attendance() {
    return this._attendance;
  }

  translations: TranslationModel[] = [];

  form: FormGroup;

  isLoading: boolean;

  readonly minuteStep: number = 1;
  errorDates = false;

  constructor(
    private attendancesService: AttendancesService,
    private uiService: UiService,
    private translateService: TranslateService,
    private modalRef: NzModalRef,
    private configurationsService: ConfigurationsService
  ) {
    super();
    this.translationsTitle();
  }

  translationsTitle() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }
  onSubmit() {
    if (!this.isFormValid(this.form)) {
      return;
    }
    this.isLoading = true;
    const data: {
      reject_reason: string;
      initial_time: string;
      final_time: string;
    } = {
      initial_time: moment(this.form.get('initial_time').value).toISOString(),
      final_time: moment(this.form.get('final_time').value).toISOString(),
      reject_reason: this.form.value.reject_reason,
    };
    this.attendancesService.reject(this.attendance.id, data).pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.SUCCESS')
        );
        this.modalRef.close(true);
      });
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    if (this.form.get(inputName).errors.aboveMaxHours) {
      return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.ERRORS.MAX_HOURS');
    }
    return null;
  }

  close(): void {
    this.modalRef.close(false);
  }

  getHours(total: number) {
    const res = this.getRealHoursMinutes(total.toString());
    if (res.minutes !== 0) {
      if (res.minutes !== 0) {
        return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.MINUTES', { minutes: res.minutes });
      }
      return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.HOURS_MINUTES', {
        hours: res.hours,
        minutes: res.minutes,
      });
    }
    return this.translateService.instant('SOCIAL_SUPPORT.ATTENDANCES.REJECT_FORM.HOURS', { hours: res.hours });
  }

  protected isFormValid(form: FormGroup): boolean {
    const initialTime = this.form.get('initial_time').value;
    const finalTime = this.form.get('final_time').value;
    this.errorDates = !initialTime || !finalTime || moment(initialTime).isAfter(finalTime);
    return super.isFormValid(form) && !this.errorDates;
  }

  private getRealHoursMinutes(total: string) {
    const [hours, minutes] = this.convertH2M(total);
    return { hours: hours, minutes: minutes};
  }

  convertH2M(timeInHour){
    var num = Math.floor(timeInHour * 60);;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return [rhours, rminutes];
  }

  private getForm() {
    const res = this.getRealHoursMinutes(this.attendance.n_hours);
    let realHours = `${res.hours.toString().padStart(2, '0')}:${res.minutes.toString().padStart(2, '0')}`;
    return new FormGroup({
      real_hours: new FormControl({ value: realHours, disabled: true }),
      reject_reason: new FormControl('', [
        Validators.required,
        trimValidation,
        Validators.maxLength(this.MAX_TEXTAREA_CHARS),
      ]),
      initial_time: new FormControl(this.stringToDate(this.attendance.initial_time), Validators.required),
      final_time: new FormControl(this.stringToDate(this.attendance.final_time), Validators.required),
    });
  }

  private stringToDate(dateStr: string): Date | null {
    return dateStr ? new Date(dateStr) : null;
  }

  private onTimeChanged() {
    const initial_time = this.form.value.initial_time;
    const final_time = this.form.value.final_time;

    let difference = moment.duration();
    if (initial_time && final_time) {
      if (new Date(this.form.value.initial_time) >= new Date(this.form.value.final_time)) {
        this.form.controls.final_time.setErrors({ biggerThanInitial: true });
        return;
      }
      difference = moment.duration(moment(final_time).diff(moment(initial_time)));
    }
    this.form.controls.real_hours.setValue(
      `${difference.hours().toString().padStart(2, '0')}:${difference.minutes().toString().padStart(2, '0')}`
    );
  }
 
}
