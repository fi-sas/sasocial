import { Component, Input } from '@angular/core';

import { environment } from 'src/environments/environment';
import {
  AuxDataApplication,
  VolunteeringModel,
} from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';

@Component({
  selector: 'app-review-application',
  templateUrl: './review-application.component.html',
  styleUrls: ['./review-application.component.less'],
})
export class ReviewApplicationComponent {
  private _application: VolunteeringModel = null;
  @Input() set application(application: VolunteeringModel) {
    this._application = application;
  }

  private _auxData: AuxDataApplication = null;
  @Input() set auxData(data: AuxDataApplication) {
    this._auxData = data;
    if ((this.organicUnits || []).length) {
      this.organicUnits = data ? data.organic_units : [];
    }
  }

  @Input() consultation = false;

  get application() {
    return this._application;
  }

  get auxData() {
    return this._auxData;
  }

  organicUnits = ['--'];

  institute = environment.institute;

  isStudent: boolean;
  isEmployee: boolean;

  readonly scheduleRows: { title: string; keys: number[] }[] = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.MORNING',
      keys: [4, 7, 10, 13, 16, 19, 1],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.AFTERNOON',
      keys: [5, 8, 11, 14, 17, 20, 2],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.NIGHT',
      keys: [6, 9, 12, 15, 18, 21, 3],
    },
  ];

  constructor() {}
}
