import { Component, Input } from '@angular/core';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { IStatusBadgeData } from '../../models';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';

interface IStatusMapper {
  [key: string]: string;
}

@Component({
  selector: 'app-status-history',
  templateUrl: './status-history.component.html',
  styleUrls: ['./status-history.component.less'],
})
export class StatusHistoryComponent {
  private _data: IStatusBadgeData = null;
  @Input() set data(data: IStatusBadgeData) {
    this._data = data;
    if (data.school) {
      this.location = `${data.school.name} - ${data.school.code}`;
    }
    if (data.experience && data.experience.organic_unit) {
      this.location = `${data.experience.organic_unit.name} - ${data.experience.organic_unit.code}`;
    }
  }
  get data() {
    return this._data;
  }

  @Input() set statusMapper(mapper: IStatusMapper) {
    this._statusMapper = Object.assign(this._statusMapper, mapper);
  }

  @Input() isApplication: boolean = false;
  @Input() isAction: boolean = false;

  get statusMapper() {
    return this._statusMapper;
  }

  private _statusMapper: IStatusMapper = {};

  location = '';

  translations: TranslationModel[] = [];

  constructor(private configurationsService: ConfigurationsService) {
    this.getTranslations();
  }

  private getTranslations() {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    this.translations = service.translations;
  }
}
