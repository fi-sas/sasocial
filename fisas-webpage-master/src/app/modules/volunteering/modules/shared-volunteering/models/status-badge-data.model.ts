import { ExperienceModel, HistoryModel } from '../../../models';
import { SchoolModel } from '@fi-sas/webpage/shared/models/school.model';

export interface IStatusBadgeData extends Partial<ExperienceModel> {
  school?: SchoolModel;
  history?: HistoryModel[];
  created_at?: string;
  status: string;
  experience?: ExperienceModel
}
