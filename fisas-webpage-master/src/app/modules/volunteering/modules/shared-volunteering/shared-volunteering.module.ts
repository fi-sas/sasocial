import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseFormAttendanceComponent } from './components/base-form-attendance';
import { SharedModule } from '../../../../shared/shared.module';
import {
  ActionsCardComponent,
  ActionsHistoryCardComponent,
  ActionStatusBadgeComponent,
  AddMonthlyReportComponent,
  ApplicationStatusBadgeComponent,
  BaseStatusBadgeComponent,
  ButtonsPanelComponent,
  DashboardComponent,
  ExperienceEvaluationComponent,
  ExpressInterestComponent,
  FileButtonComponent,
  FormAbsencesComponent,
  FormAttendanceComponent,
  FormWithdrawalComponent,
  InterviewComponent,
  ManifestInterestComponent,
  RejectAbsenceJustificationComponent,
  RejectAttendanceComponent,
  ReviewApplicationComponent,
  StatusHistoryComponent,
  DispatchModalComponent
} from './components';
import { FilterStatusConfigPipe } from '../complains/pipes/filter-status-config.pipe';

@NgModule({
  declarations: [
    ActionsCardComponent,
    ActionsHistoryCardComponent,
    ActionStatusBadgeComponent,
    AddMonthlyReportComponent,
    ApplicationStatusBadgeComponent,
    BaseFormAttendanceComponent,
    BaseStatusBadgeComponent,
    ButtonsPanelComponent,
    DashboardComponent,
    ExperienceEvaluationComponent,
    ExpressInterestComponent,
    FileButtonComponent,
    FormAbsencesComponent,
    FormAttendanceComponent,
    FormWithdrawalComponent,
    InterviewComponent,
    ManifestInterestComponent,
    RejectAbsenceJustificationComponent,
    RejectAttendanceComponent,
    ReviewApplicationComponent,
    StatusHistoryComponent,
    FilterStatusConfigPipe,
    DispatchModalComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: [
    ActionsCardComponent,
    ActionsHistoryCardComponent,
    ActionStatusBadgeComponent,
    ApplicationStatusBadgeComponent,
    ButtonsPanelComponent,
    DashboardComponent,
    FileButtonComponent,
    FormWithdrawalComponent,
    InterviewComponent,
    ReviewApplicationComponent,
    FilterStatusConfigPipe
  ],
  entryComponents: [
    AddMonthlyReportComponent,
    ExperienceEvaluationComponent,
    ExpressInterestComponent,
    FormAbsencesComponent,
    FormAttendanceComponent,
    FormWithdrawalComponent,
    ManifestInterestComponent,
    RejectAbsenceJustificationComponent,
    RejectAttendanceComponent,
    StatusHistoryComponent,
    DispatchModalComponent
  ],
})
export class SharedVolunteeringModule {}
