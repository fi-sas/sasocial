import { Component, EventEmitter, Input, Output } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { InterestService } from '@fi-sas/webpage/modules/volunteering/services';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import {
  ActionCandiate,
  INTEREST_STATUS_EVENT,
  INTEREST_STATUS_STATE_MACHINE,
  INTEREST_STATUS,
  ExperienceUserInterestData,
} from '@fi-sas/webpage/modules/volunteering/models';
import { DispatchModalComponent } from '../../../shared-volunteering/components/dispatch-modal/dispatch-modal.component';
import { SendDispatchData } from '@fi-sas/webpage/modules/volunteering/models/dispatch.model';

interface ChangeStatusAction {
  label: string;
  action: (candidate: ActionCandiate) => Promise<any>;
}

@Component({
  selector: 'app-selected-candidates',
  templateUrl: './selected-candidates.component.html',
  styleUrls: ['./selected-candidates.component.less'],
})
export class SelectedCandidatesComponent {
  private _actionId: number;
  @Input() set actionId(id: number) {
    this._actionId = id;
    this.getListCandidates();
  }

  @Output() candidatesChange = new EventEmitter();

  tableConfig = {
    pageIndex: 1,
    pageSize: 10,
    total: 0,
    showPagination: true,
    position: 'bottom',
    showSizeChanger: true,
    expandable: true,
    checkbox: true,
  };
  readonly INTEREST_STATUS = INTEREST_STATUS;

  expandedLines: { [key: string]: boolean } = {};

  private readonly actionsMapper: { [key: string]: ChangeStatusAction } = {
    ANALYSED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.ANALYZE',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.ANALYSE),
    },
    COLABORATION: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.COLABORATION',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.COLABORATION),
    },
    APPROVED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.APPROVE',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.APPROVE),
    },
    CANCELLED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.CANCEL),
    },
    CLOSED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.CLOSE),
    },
    DECLINED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.REJECT',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.DECLINE),
    },
    WAITING: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.WAITING',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WAITING),
    },
    NOT_SELECTED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.NOT_SELECTED',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.NOTSELECT),
    },
    ACCEPTED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.ACCEPTED',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.ACCEPT),
    },
    WITHDRAWAL: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.WITHDRAWAL',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WITHDRAWAL),
    },
    WITHDRAWAL_ACCEPTED: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.ACCEPT_WITHDRAWAL',
      action: (candidate: ActionCandiate) => this.changeStatus(candidate, INTEREST_STATUS_EVENT.WITHDRAWALACCEPTED),
    },
    DISPATCH_ACCEPT: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.DISPATCH_ACCEPT',
      action: (candidate: ActionCandiate) => this.dispatchChangeStatues(candidate, true),
    },
    DISPATCH_REJECT: {
      label: 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.DISPATCH_REJECT',
      action: (candidate: ActionCandiate) => this.dispatchChangeStatues(candidate, false),
    },
  };

  allowedActions: { [key: number]: ChangeStatusAction[] } = {};

  selectedCandidates: ActionCandiate[] = [];

  isLoading: boolean;

  multiActions = [];
  isAllDisplayDataChecked = false;
  isIndeterminate = false;
  mapOfCheckedId: { [key: string]: boolean } = {};

  constructor(
    private interestService: InterestService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  getListCandidates(resetPagination = false) {
    this.isLoading = true;
    if (resetPagination) {
      this.tableConfig.pageIndex = 1;
    }
    this.interestService.getListCandidates(this._actionId, this.tableConfig.pageIndex, this.tableConfig.pageSize)
      .pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(response => {
        this.selectedCandidates = response.data;
        //this.tableConfig.showPagination = response.data.length === this.tableConfig.pageSize;
        this.tableConfig.total = response.link.total;
        this.tableConfig.showSizeChanger = this.tableConfig.showPagination;
        this.allowedActions = this.getAllowedActions();
      });
  }

  checkAll(value: boolean): void {
    this.selectedCandidates.forEach((item) => (this.mapOfCheckedId[item.id] = value));
    this.refreshStatus();
  }

  refreshStatus(): void {
    this.isAllDisplayDataChecked = this.selectedCandidates.every((item) => this.mapOfCheckedId[item.id]);
    this.isIndeterminate =
      !this.isAllDisplayDataChecked && this.selectedCandidates.some((item) => this.mapOfCheckedId[item.id]);
    this.multiActions = this.getCommonAllowedActions();
  }

  performConfirmationAction(action: ChangeStatusAction, data: ActionCandiate) {
    let title = '';
    if(action.label == 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL') {
      title = 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CANCEL_TEXT';
    } else if (action.label == 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED') {
      title = 'VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.BUTTON.CLOSED_TEXT';
    }else{
      title = 'VOLUNTEERING.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM';
    }
    this.modalService.confirm({
      nzTitle: this.translateService.instant(title),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () =>
        action.action(data).then(() => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('VOLUNTEERING.OFFERS.LIST_SELECTED.CHANGE_STATUS.SUCCESS')
          );
          this.getListCandidates();
          this.candidatesChange.emit(true);
        }),
    });
  }

  private getCommonAllowedActions() {
    let actions: ChangeStatusAction[] = null;
    const candidates: ActionCandiate[] = [];
    this.selectedCandidates.forEach((candidate) => {
      if (this.mapOfCheckedId[candidate.id]) {
        candidates.push(candidate);
        if (actions === null) {
          actions = this.allowedActions[candidate.id];
        } else {
          actions = actions.filter((v) => this.allowedActions[candidate.id].some((x) => x.label === v.label));
        }
      }
    });

    return (actions || []).map((action) => {
      return {
        text: this.translateService.instant(action.label),
        onSelect: () => {
          this.modalService.confirm({
            nzTitle: this.translateService.instant(
              'VOLUNTEERING.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM_MULTIPLE'
            ),
            nzContent: this.translateService.instant(
              'VOLUNTEERING.OFFERS.LIST_SELECTED.CHANGE_STATUS.CONFIRM_MULTIPLE_BODY',
              { n_candidates: candidates.length }
            ),
            nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
            nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
            nzOnOk: () => {
              const promises: Promise<any>[] = [];
              candidates.forEach((candidate) => {
                promises.push(action.action(candidate));
              });
              Promise.all(promises).then(() => this.getListCandidates());
            },
          });
        },
      };
    });
  }

  private changeStatus(userInterest: ActionCandiate, newStatus: INTEREST_STATUS_EVENT){
    return this.interestService.changeStatus(userInterest.id, newStatus).pipe(first()).toPromise();
  }

  private getAllowedActions() {
    const allowedActions = {};
    this.selectedCandidates.forEach((candidate) => {
      const candidateActions = [];
      INTEREST_STATUS_STATE_MACHINE[candidate.status].forEach((status: INTEREST_STATUS) => {
        if (this.actionsMapper[status]) {
          candidateActions.push(this.actionsMapper[status]);
        }
      });
      allowedActions[candidate.id] = candidateActions;
    });
    return allowedActions;
  }

  dispatchModal(application: ActionCandiate) {
    const actions = [];
    INTEREST_STATUS_STATE_MACHINE.DISPATCH.forEach((status: INTEREST_STATUS) => {
      if (this.actionsMapper[status]) {
        actions.push({
          key: ExperienceUserInterestData.eventToDispatchDecisionMapper[status],
          label: this.actionsMapper[status].label
        });
      }
    });

    const modalRef: NzModalRef<DispatchModalComponent> = this.modalService.create({
      nzTitle: this.translateService.instant('VOLUNTEERING.OFFERS.LIST_SELECTED.TABLE.STATUS'),
      nzContent: DispatchModalComponent,
      nzComponentParams: {
        serviceFn: (data: SendDispatchData) => this.interestService.sendDispatch(application.id, data),
        actions: actions
      },
      nzFooter: [
        { label: this.translateService.instant('MISC.CLOSE'), onClick: (_) => modalRef.close() },
        {
          label: this.translateService.instant('DASHBOARD.SAVECHANGE'),
          type: 'primary',
          onClick: (componentInstance) => {
            componentInstance.onSubmit()
            .then(() => {
              this.getListCandidates();
              this.candidatesChange.emit(true);
              modalRef.close();
            })
            .catch(() => {});
          },
        },
      ],
    });
  }

  private dispatchChangeStatues(application: ActionCandiate, acceptDecision: boolean) {
    this.isLoading = true;
    return this.interestService
      .sendDispatch(application.id, { decision_dispatch: acceptDecision ? 'ACCEPT' : 'REJECT' })
      .pipe(
        first(),
        finalize(() => {
          this.isLoading = false;
        })
      )
      .toPromise();
  }

  sendDispatch(application: ActionCandiate, acceptDecision: boolean){
    this.dispatchChangeStatues(application, acceptDecision).then(() => {
      this.uiService.showMessage(
        MessageType.success,
        this.translateService.instant('VOLUNTEERING.OFFERS.LIST_SELECTED.CHANGE_STATUS.SUCCESS')
      );
      this.getListCandidates();
      this.candidatesChange.emit(true);
    });
  }

}
