import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { hasOwnProperty } from 'tslint/lib/utils';
import { TranslateService } from '@ngx-translate/core';

import { ActionCandiate, VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models';
import { ApplicationCertificateService } from '@fi-sas/webpage/modules/volunteering/services';
import { CertificateModel } from '@fi-sas/webpage/modules/social-support/modules/attendances/models';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Resource } from '@fi-sas/core';
import { finalize, first } from 'rxjs/operators';

interface ITab {
  title: string;
  tempalteRef: TemplateRef<any>;
  collapseActive: boolean;
  show?: () => boolean;
}

@Component({
  selector: 'app-view-candidates',
  templateUrl: './view-candidates.component.html',
  styleUrls: ['./view-candidates.component.less'],
})
export class ViewCandidatesComponent implements OnInit {
  @Input() listing: ActionCandiate = null;
  @Input() application: VolunteeringModel = null;

  @ViewChild('tabProfile') tabProfile: TemplateRef<any>;
  @ViewChild('tabApplication') tabApplication: TemplateRef<any>;
  @ViewChild('tabProfessionalExper') tabProfessionalExper: TemplateRef<any>;
  @ViewChild('tabCollaborationHistory') tabCollaborationHistory: TemplateRef<any>;
  @ViewChild('tabApplicationStatusChanges') tabApplicationStatusChanges: TemplateRef<any>;

  readonly scheduleRows = [
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.MORNING',
      keys: [4, 7, 10, 13, 16, 19, 1],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.AFTERNOON',
      keys: [5, 8, 11, 14, 17, 20, 2],
    },
    {
      title: 'SOCIAL_SUPPORT.APPLICATIONS.FORM_TIME_AVAILABILITY.SCHEDULE_DAY_PERIOD.NIGHT',
      keys: [6, 9, 12, 15, 18, 21, 3],
    },
  ];

  readonly applicationStatusLabelMapper = {
    ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
    ACKNOWLEDGED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACKNOWLEDGED',
    ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
    APPROVED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.APPROVED',
    CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
    CLOSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CLOSED',
    COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.COLABORATION',
    DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
    EXPIRED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXPIRED',
    EXTERNAL_SYSTEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXTERNAL_SYSTEM',
    IN_COLABORATION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.IN_COLABORATION',
    NOT_SELECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.NOT_SELECTED',
    PUBLISHED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.PUBLISHED',
    REJECTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.REJECTED',
    RETURNED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.RETURNED',
    SELECTION: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SELECTION',
    SEND_SEEM: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SEND_SEEM',
    SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
    WAITING: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WAITING',
    WITHDRAWAL: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.WITHDRAWAL',
  };

  tabs: ITab[] = [];
  selectedTabIndex: number = 0;

  loadingCertificateButton: boolean;

  constructor(
    private applicationCertificateService: ApplicationCertificateService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService
  ) {}

  ngOnInit() {
    this.application.preferred_schedule_ids = (this.application.preferred_schedule || []).map((s) => s.schedule_id);

    this.tabs = this.getTabs();
  }

  onTabIndexChange(selectedIndex: number) {
    this.selectedTabIndex = Math.min(selectedIndex, (this.tabs.length || 1) - 1);
  }

  goReviewApplication(id: number) {
    this.router.navigateByUrl('volunteering/applications/review/' + id);
  }

  getExperienceCertificate(user_interest_id: number) {
    this.loadingCertificateButton = true;
    this.applicationCertificateService.generateCertificate(user_interest_id).pipe(first(), finalize(() => this.loadingCertificateButton = false))
      .subscribe(response => {
        if (response.data[0].url) {
          window.open(response.data[0].url, '_blank');
        } else {
          this.uiService.showMessage(
            MessageType.info,
            this.translateService.instant('SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.NO_COLAB_CERTIFICATE')
          );
        }
      });
  }

  private getTabs(): ITab[] {
    return [
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.PROFILE',
        tempalteRef: this.tabProfile,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.APPLICATION',
        tempalteRef: this.tabApplication,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.PROFESSIONAL_EXPER',
        tempalteRef: this.tabProfessionalExper,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.HISTORY_COLLABORATION',
        tempalteRef: this.tabCollaborationHistory,
        collapseActive: false,
        show: () => true,
      },
      {
        title: 'SOCIAL_SUPPORT.OFFERS.LIST_SELECTED.TABLE.APPLICATION_STATUS_CHANGES',
        tempalteRef: this.tabApplicationStatusChanges,
        collapseActive: false,
        show: () => true,
      },
    ];
  }
}
