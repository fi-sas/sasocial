import { Component } from '@angular/core';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { ExperienceModel } from '@fi-sas/webpage/modules/volunteering/models';
import { Resource } from '@fi-sas/core';
import { ServiceConfigurationModel } from '@fi-sas/webpage/shared/models/service-configurations.model';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { VolunteeringActionsService } from '@fi-sas/webpage/modules/volunteering/services';
import { VOLUNTEERING_ACTION_STATUS } from '@fi-sas/webpage/modules/volunteering/models';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-status-history',
  templateUrl: './status-history.component.html',
  styleUrls: ['./status-history.component.less'],
})
export class StatusHistoryComponent {
  private _isResponsible = false;

  set isResponsible(isResponsible: boolean) {
    if (isResponsible && !this._isResponsible) {
      this.getResponsibleData();
    }

    this._isResponsible = isResponsible;
  }

  get isResponsible(): boolean {
    return this._isResponsible;
  }

  actions: ExperienceModel[] = [];
  actionsHistory: ExperienceModel[] = [];

  isLoading: boolean;
  isHistoryLoading: boolean;

  panels = [
    {
      active: false,
      disabled: false,
      name: 'VOLUNTEERING.ACTIONS_HISTORY',
      customStyle: {
        background: '#f0f2f5',
        padding: '0px',
        border: '0px',
        margin: '0px',
      },
    },
  ];

  actionToManage: number;

  translations: TranslationModel[] = [];

  private readonly offerStatusWeight = {
    RETURNED: 0,
    SEND_SEEM: 1,
    EXTERNAL_SYSTEM: 2,
    IN_COLABORATION: 3,
    SELECTION: 4,
    DISPATCH: 5,
    PUBLISHED: 6,
    APPROVED: 7,
    ANALYSED: 8,
    SUBMITTED: 9,
    REJECTED: 10,
    CLOSED: 11,
    CANCELLED: 12,
  };

  constructor(
    private authService: AuthService,
    private actionsService: VolunteeringActionsService,
    private configurationsService: ConfigurationsService
  ) {
    this.translations = this.getTranslations();

    this.isResponsible = this.authService.hasPermission('volunteering:experiences:responsable');
  }

  onManage(id: number) {
    this.actionToManage = id;
  }

  onCancel() {
    this.getResponsibleData();
  }

  onCandidatesChange() {
    if (this.isResponsible) {
      return this.getResponsibleData();
    }
  }

  private getResponsibleData() {
    this.getResponsibleActions();
    this.getResponsibleActionsHistory();
  }

  getResponsibleActions(){
    this.isLoading = true;
    this.actionsService.listByResponsible()
    .pipe(first(), finalize(() => this.isLoading = false))
    .subscribe(response => {
      this.actions = response.data.sort(this.offerSortFn.bind(this))
    })
  }

  getResponsibleActionsHistory(){
    this.isHistoryLoading = true;
    this.actionsService.listPastByResponsible()
    .pipe(first(), finalize(() => this.isHistoryLoading = false))
    .subscribe(response => {
      this.actionsHistory = response.data.sort(this.offerSortFn.bind(this))
    })
  }

  private getTranslations(): TranslationModel[] {
    let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(28);
    return service.translations;
  }

  private offerSortFn(a: ExperienceModel, b: ExperienceModel): number {
    const aWeight = this.normalizeWeight(this.offerStatusWeight[a.status]);
    const bWeight = this.normalizeWeight(this.offerStatusWeight[b.status]);
    return aWeight - bWeight;
  }

  private normalizeWeight(weight: number): number {
    if (weight == null || isNaN(weight)) {
      return Number.MAX_VALUE;
    }

    return weight;
  }
}
