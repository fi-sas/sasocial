import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatusHistoryComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: StatusHistoryComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:experiences:my_offers' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolunteeringActionsStatusHistoryRoutingModule {}
