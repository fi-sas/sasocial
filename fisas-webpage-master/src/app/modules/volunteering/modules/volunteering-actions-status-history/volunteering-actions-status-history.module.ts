import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../shared-volunteering/shared-volunteering.module';
import { StatusHistoryComponent } from './pages';
import { VolunteeringActionsStatusHistoryRoutingModule } from './volunteering-actions-status-history-routing.module';
import { SelectedCandidatesComponent, ViewCandidatesComponent } from './components';

@NgModule({
  declarations: [SelectedCandidatesComponent, StatusHistoryComponent, ViewCandidatesComponent],
  imports: [CommonModule, VolunteeringActionsStatusHistoryRoutingModule, SharedModule, SharedVolunteeringModule],
})
export class VolunteeringActionsStatusHistoryModule {}
