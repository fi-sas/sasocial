import { FormGroup, ValidatorFn } from '@angular/forms';

import { VolunteeringActionFormData, VolunteeringActionSubmitData } from './volunteering-action-data.model';
import { VolunteeringActionModel } from '@fi-sas/webpage/modules/volunteering/models';

export interface ISelectOption<T = any> {
  label: string;
  value: T;
}

export interface IFields {
  name: string;
  required?: boolean;
  disabled?: boolean;
  value?: any;
  translatable?: boolean;
  validators?: ValidatorFn[];
  toExternalValue?: (formData: Partial<VolunteeringActionFormData>, currentData: VolunteeringActionSubmitData) => void;
  toInternalValue?: (volunteeringAction: VolunteeringActionModel, form: FormGroup) => void;
}
