interface TranslatableFields {
  applicant_profile: string;
  description: string;
  job: string;
  proponent_service: string;
  selection_criteria: string;
  title: string;
}

interface CommonData {
  academic_year: string;
  address: string;
  attachment_file_id: number;
  experience_responsible_id: number;
  external_entity: string;
  holydays_availability: boolean;
  number_candidates: number;
  number_simultaneous_candidates: number;
  number_weekly_hours: number;
  organic_unit_id: number;
  total_hours_estimation: number;
}


export interface OfferFormData extends CommonData {
  application_deadline_date: Date;
  publish_date: Date;
  initial_time: Date;
  final_time: Date;
  desired_schedule: { [key: number]: boolean };
  translations: { [language: string]: TranslatableFields };
}

export interface VolunteeringActionFormData extends CommonData {
  application_deadline_date: Date;
  publish_date: Date;
  initial_time: Date;
  final_time: Date;
  desired_schedule: { [key: number]: boolean };
  translations: { [language: string]: TranslatableFields };
}

export interface VolunteeringActionSubmitData extends CommonData {
  application_deadline_date: string;
  publish_date: string;
  schedule: {
    schedule_id: number;
    time_begin: string;
    time_end: string;
  }[];
  start_date: any;
  end_date: any;
  translations: { [language: string]: TranslatableFields & { language_id: number } };
  uuid?: string;
}
