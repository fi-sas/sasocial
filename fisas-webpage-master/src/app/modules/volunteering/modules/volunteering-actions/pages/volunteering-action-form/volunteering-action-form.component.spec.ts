import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteeringActionFormComponent } from './volunteering-action-form.component';

describe('VolunteeringActionFormComponent', () => {
  let component: VolunteeringActionFormComponent;
  let fixture: ComponentFixture<VolunteeringActionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VolunteeringActionFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteeringActionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
