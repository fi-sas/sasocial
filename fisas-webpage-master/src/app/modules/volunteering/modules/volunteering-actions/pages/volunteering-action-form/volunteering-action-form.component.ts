import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { finalize, first, map, takeUntil } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AcademicYear } from '@fi-sas/webpage/shared/models';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';
import { IFields, ISelectOption, VolunteeringActionSubmitData } from '../../models';
import { LanguageModel } from '@fi-sas/webpage/modules/private-accommodation/models/language.model';
import { LanguagesService } from '@fi-sas/webpage/modules/private-accommodation/services/languages.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { Resource } from '@fi-sas/core';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { VolunteeringActionModel } from '@fi-sas/webpage/modules/volunteering/models';
import { VolunteeringActionsService } from '@fi-sas/webpage/modules/volunteering/services';
import { NzModalService } from 'ng-zorro-antd';

interface IScheduleRow {
  title: string;
  keys: string[];
  startTime: () => Date;
  endTime: () => Date;
}

@Component({
  selector: 'app-volunteering-action-form',
  templateUrl: './volunteering-action-form.component.html',
  styleUrls: ['./volunteering-action-form.component.less'],
})
export class VolunteeringActionFormComponent extends FormHandler implements OnInit {
  readonly filterTypes = ['application/pdf', 'image/png', 'image/jpeg'];
  errorDates = false;
  readonly fields: IFields[] = [
    { name: 'title', required: true, translatable: true, validators: [trimValidation], },
    { name: 'proponent_service', required: true, translatable: true, validators: [trimValidation], },
    { name: 'applicant_profile', required: true, translatable: true, validators: [trimValidation], },
    { name: 'job', required: true, translatable: true, validators: [trimValidation], },
    { name: 'description', required: true, translatable: true, validators: [trimValidation], },
    { name: 'selection_criteria', required: true, translatable: true, validators: [trimValidation], },
    { name: 'academic_year', required: true, disabled: true },
    { name: 'organic_unit_id', required: true },
    { name: 'address', required: true, validators: [trimValidation], },
    {
      name: 'initial_time',
      required: true,
      toInternalValue: (offer, form) =>
        form.get('initial_time').setValue(offer.start_date),
      toExternalValue: (formData, currentData) => {
          currentData.start_date = formData.initial_time;
      },
    },
    {
      name: 'final_time',
      required: true,
      toInternalValue: (offer, form) =>
        form.get('final_time').setValue(offer.end_date),
      toExternalValue: (formData, currentData) => {
          currentData.end_date = formData.final_time;
      },
    },
    { name: 'number_candidates', validators: [Validators.min(0), Validators.max(10000)] },
    { name: 'number_simultaneous_candidates', validators: [Validators.min(0), Validators.max(10000)] },
    { name: 'number_weekly_hours', required: true, validators: [Validators.min(0), Validators.max(168)] },
    { name: 'total_hours_estimation', required: true, validators: [Validators.min(0)] },
    { name: 'holydays_availability', value: null, required: true },
    { name: 'schedule', required: true },
    {
      name: 'publish_date',
      value: new Date(),
      required: true,
      toExternalValue: (formData, currentData) =>
        (currentData.publish_date = moment(formData.publish_date).toISOString()),
    },
    {
      name: 'application_deadline_date',
      required: true,
      toExternalValue: (formData, currentData) =>
        (currentData.application_deadline_date = moment(formData.application_deadline_date).toISOString()),
    },
    { name: 'attachment_file_id' },
  ];

  volunteeringAction: VolunteeringActionModel;
  form: FormGroup;
  isLoading: boolean;
  isLoadingAction: boolean;
  showErrorMessage: boolean;
  languages$: ReplaySubject<LanguageModel[]> = new ReplaySubject<LanguageModel[]>();


  organicUnitsLoading = false;
  organicUnits: OrganicUnitModel[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private configurationsService: ConfigurationsService,
    private languagesService: LanguagesService,
    private organicUnitsService: OrganicUnitsService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService,
    private modalService:NzModalService,
    private volunteeringActionsService: VolunteeringActionsService
  ) {
    super();
    this.getLangages();
  }

  ngOnInit() {
    this.getOrganicUnitOptions();
    this.form = this.getForm();

    if (isNaN(this.activatedRoute.snapshot.params.id)) {
      this.setCurrentAcademicYear();
    } else {
      this.getVolunteeringAction(this.activatedRoute.snapshot.params.id);
    }
  }

  onSubmit() {
    this.errorDates = false;
    if(this.form.get('initial_time').value && this.form.get('final_time').value) {
      if(moment(this.form.get('initial_time').value).isAfter(this.form.get('final_time').value)){
        this.errorDates = true;
      }
    }
    if (!this.isFormValid(this.form) || this.errorDates) {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
      return;
    }
    this.showErrorMessage = false;
    if(this.volunteeringAction) {
      this.modalService.confirm({
        nzTitle: this.translateService.instant('MISC.CHANGE_STATUS'),
        nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
        nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
        nzOnOk: () => this.submit()
    });
    }else{
      this.submit();
    }
  }

  submit(){
    this.isLoading = true;
    this.getSubmitData().subscribe(
      (data: VolunteeringActionSubmitData) => {
        let action = this.volunteeringActionsService.create(data);
        let successMessage = 'VOLUNTEERING.ACTIONS.FORM.CREATE_SUCCESS';
        if (this.volunteeringAction) {
          action = this.volunteeringActionsService.edit(this.volunteeringAction.id, data);
          successMessage = 'VOLUNTEERING.ACTIONS.FORM.EDIT_SUCCESS';
        }

        action
          .pipe(
            first(),
            finalize(() => (this.isLoading = false))
          )
          .subscribe(() => {
            this.uiService.showMessage(MessageType.success, this.translateService.instant(successMessage));
            this.goBack();
          });
      },
      () => {
        this.isLoading = false;
        this.uiService.showMessage(
          MessageType.error,
          this.translateService.instant('VOLUNTEERING.ACTIONS.FORM.LANG_ERROR')
        );
      }
    );
  }

  disableDate = (currentDate: Date): boolean => {
    return moment(currentDate).isBefore(moment(), 'day');
  };

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    if (this.form.get(inputName).errors.email) {
      return this.translateService.instant('MISC.EMAIL');
    }
    if (this.form.get(inputName).errors.min) {
      return this.translateService.instant('MISC.MIN_NUMBER', { number: this.form.get(inputName).errors.min.min || 0 })
    }
    if (this.form.get(inputName).errors.max) {
      return this.translateService.instant('MISC.MAX_NUMBER', { number: this.form.get(inputName).errors.max.max || 0 })
    }
    return null;
  }

  goBack() {
    this.router.navigateByUrl('volunteering/actions-status-history');
  }

  copyField(fieldName: string, langAcronym: string) {
    const fromFormControl = this.form.get(`translations.pt.${fieldName}`);
    const toFormControl = this.form.get(`translations.${langAcronym}.${fieldName}`);
    toFormControl.setValue(fromFormControl.value);
    toFormControl.markAsTouched();
    toFormControl.markAsDirty();
  }

  private getVolunteeringAction(id: number) {
    this.isLoadingAction = true;
    this.volunteeringActionsService.getAction(id).pipe(first(), finalize(() => this.isLoadingAction = false))
      .subscribe(response => {
        this.volunteeringAction = response.data[0];
        this.fillVolunteeringActionForm(this.volunteeringAction);
      });
  }

  private getSubmitData(): Observable<VolunteeringActionSubmitData> {
    return this.languages$.pipe(
      first(),
      map((languages: LanguageModel[]) => {
        let data = { translations: {} };
        languages.forEach((lang) => (data.translations[lang.acronym] = {}));
        const formValue = this.form.getRawValue();
        this.fields.forEach((field) => {
          if (field.translatable) {
            this.translateService.langs.forEach((lang) => {
              data.translations[lang][field.name] = this.form.value.translations[lang][field.name];
            });
          } else if (field.toExternalValue) {
            field.toExternalValue(this.form.value, data as VolunteeringActionSubmitData);
          } else {
            data[field.name] = formValue[field.name];
          }
        });
        const translations = [];
        languages.forEach((lang) => translations.push({ ...data.translations[lang.acronym], language_id: lang.id }));
        data.translations = translations;
        return data as VolunteeringActionSubmitData;
      })
    );
  }

  private getForm(): FormGroup {
    const fg = {};
    const fgTranslations = {};
    this.translateService.langs.forEach((lang) => (fgTranslations[lang] = {}));
    this.fields.forEach((field) => {
      if (field.translatable) {
        this.translateService.langs.forEach((lang) => (fgTranslations[lang][field.name] = this.getFormControl(field)));
      } else {
        fg[field.name] = this.getFormControl(field);
      }
    });
    this.translateService.langs.forEach((lang) => (fgTranslations[lang] = new FormGroup(fgTranslations[lang])));
    fg['translations'] = new FormGroup(fgTranslations);
    return new FormGroup(fg);
  }

  private getFormControl(field: IFields): FormControl {
    const validators: ValidatorFn[] = [];
    if (field.required) {
      validators.push(Validators.required);
    }
    (field.validators || []).forEach((validator) => validators.push(validator));
    return new FormControl({ value: field.value, disabled: field.disabled || false }, validators);
  }

  getOrganicUnitOptions() {
    this.organicUnitsLoading = true;
    return this.organicUnitsService.organicUnits().pipe(
      first(),
      map((res) => res.data ),
      finalize(() => this.organicUnitsLoading = false)).subscribe((units => {
        this.organicUnits = units;
    }));
  }


  private getLangages() {
    this.languagesService
      .listLanguages()
      .pipe(first())
      .subscribe((result) => this.languages$.next(result.data));
  }

  private fillVolunteeringActionForm(volunteeringAction: VolunteeringActionModel) {
    this.fields.forEach((field) => {
      if (field.translatable) {
        this.languages$.pipe(takeUntil(this.destroy$)).subscribe((languages) =>
          volunteeringAction.translations.forEach((t) => {
            const language = languages.find((lang) => lang.id === t.language_id);
            if (language && this.translateService.langs.includes(language.acronym)) {
              this.form.get(`translations.${language.acronym}.${field.name}`).setValue(t[field.name] || null);
            }
          })
        );
      } else if (field.toInternalValue) {
        field.toInternalValue(volunteeringAction, this.form);
      } else {
        this.form.get(field.name).setValue(volunteeringAction[field.name]);
      }
    });
  }

  private setCurrentAcademicYear() {
    this.configurationsService.getCurrentAcademicYear().pipe(first()).subscribe(res => {
      this.form.get('academic_year').setValue(res.data[0].academic_year)
    }, err => {
      this.form.get('academic_year').enable()
    });
  }
}
