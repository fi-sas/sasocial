import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VolunteeringActionFormComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: VolunteeringActionFormComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:activities:create' },
  },
  {
    path: ':id',
    component: VolunteeringActionFormComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:activities:update' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolunteeringActionsRoutingModule {}
