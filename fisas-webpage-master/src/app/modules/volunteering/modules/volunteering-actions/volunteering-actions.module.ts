import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { VolunteeringActionFormComponent } from './pages';
import { VolunteeringActionsRoutingModule } from './volunteering-actions-routing.module';

@NgModule({
  declarations: [VolunteeringActionFormComponent],
  imports: [CommonModule, VolunteeringActionsRoutingModule, SharedModule],
})
export class VolunteeringActionsModule {}
