import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { first, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { BaseFormComponent } from '../base-form';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';

interface IFormControlValue {
  [key: number]: boolean;
}

interface IFormValue {
  organic_units_ids: number[];
}

@Component({
  selector: 'app-activities-locations',
  templateUrl: './activities-locations.component.html',
  styleUrls: ['./activities-locations.component.less'],
})
export class ActivitiesLocationsComponent extends BaseFormComponent implements OnInit {
  private _application: VolunteeringModel = null;
  @Input() set application(application: VolunteeringModel) {
    this._application = application;
    if (application) {
      if ((application.organic_units || []).length) {
        this._application.organic_units_ids = this._application.organic_units.map(item => item.organic_unit_id);
      }
      this.loadForm();
    }
  }
  get application() {
    return this._application;
  }

  organicUnits: OrganicUnitModel[] = [];

  form: FormGroup = new FormGroup({
    organic_units: new FormControl(null, Validators.required),
    locations: new FormGroup({}),
  });

  constructor(private organicUnitsService: OrganicUnitsService, protected translateService: TranslateService) {
    super(translateService);
  }

  ngOnInit() {
    this.getOrganicUnits();
  }

  getFormValues(): IFormValue {
    return {
      organic_units_ids: this._getSelected(this.organicUnits, this.form.get('locations').value, 'id') as number[],
    };
  }

  getSelectedOrganicUnits() {
    return {
      organic_units: this._getSelected(this.organicUnits, this.form.get('locations').value, 'name') as string[],
    };
  }

  private loadForm() {
    if (this._application.organic_units_ids) {
      this.form.get('organic_units').setValue(this._application.organic_units_ids);
    } else if (this._application.organic_units) {
      this.form.get('organic_units').setValue((this._application.organic_units || []).map((o) => o.id));
    }
  }

  private getOrganicUnits(): void {
    this.organicUnitsService
      .organicUnits()
      .pipe(first())
      .subscribe((organicUnits) => {
        this.setOrgnaicUnits(organicUnits.data);
        this.handleLocationsValueChange();
      });
  }

  private setOrgnaicUnits(data: OrganicUnitModel[]) {
    const formGroup: FormGroup = this.form.get('locations') as FormGroup;
    this.organicUnits = data;
    this.organicUnits.forEach((organicUnit) =>
      formGroup.addControl(
        organicUnit.id.toString(),
        new FormControl(
          this._application ? (this._application.organic_units_ids || []).includes(organicUnit.id) : false
        )
      )
    );
  }

  private handleLocationsValueChange() {
    this.form
      .get('locations')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        const ids = this._getSelected(this.organicUnits, value, 'id') as number[];
        this.form.get('organic_units').setValue((ids || []).length ? ids : null);
      });
  }

  private _getSelected(
    objectList: OrganicUnitModel[],
    controlValue: IFormControlValue,
    fieldToGet: 'id' | 'name' = 'id'
  ): (string | number)[] {
    const selected: (string | number)[] = [];
    objectList.forEach((obj) => {
      if (controlValue[obj.id]) {
        selected.push(obj[fieldToGet]);
      }
    });
    return selected;
  }
}
