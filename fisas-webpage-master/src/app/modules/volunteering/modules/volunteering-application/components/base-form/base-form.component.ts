import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';

@Component({
  selector: 'app-base-form',
  template: '',
  styleUrls: [],
})
export class BaseFormComponent extends FormHandler {
  form: FormGroup;

  constructor(protected translateService: TranslateService) {
    super();
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }

  validForm(): boolean {
    return this.isFormValid(this.form);
  }

  getFormValues() {
    return this.form.getRawValue();
  }

  clearForm() {
    this.form.reset();
  }

  protected disableFilledFields(fieldNames: string[]) {
    fieldNames.forEach((key: string) => {
      const formControl = this.form.get(key);
      if (formControl && formControl.value) {
        formControl.updateValueAndValidity();
        if (formControl.valid) {
          formControl.disable();
        }
      }
    });
  }
}
