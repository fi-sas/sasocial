import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplementaryInformationComponent } from './complementary-information.component';

describe('ComplementaryInformationComponent', () => {
  let component: ComplementaryInformationComponent;
  let fixture: ComponentFixture<ComplementaryInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplementaryInformationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplementaryInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
