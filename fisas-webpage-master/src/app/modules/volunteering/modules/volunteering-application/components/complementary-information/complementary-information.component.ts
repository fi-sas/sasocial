import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { BaseFormComponent } from '../base-form';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

interface IFormValues {
  observations?: string;
}

@Component({
  selector: 'app-complementary-information',
  templateUrl: './complementary-information.component.html',
  styleUrls: ['./complementary-information.component.less'],
})
export class ComplementaryInformationComponent extends BaseFormComponent {
  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this.form.patchValue({ observations: application.observations });
    }
  }

  form: FormGroup = new FormGroup({
    observations: new FormControl('', trimValidation),
  });

  constructor(protected translateService: TranslateService) {
    super(translateService);
  }

  getFormValues(): IFormValues {
    return super.getFormValues();
  }
}
