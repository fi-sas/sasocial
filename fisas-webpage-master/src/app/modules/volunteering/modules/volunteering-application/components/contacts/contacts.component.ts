import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { TranslateService } from '@ngx-translate/core';
import { BaseFormComponent } from '../base-form';

interface IFormValues {
  email: string;
  phone?: number;
}

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.less'],
})
export class ContactsComponent extends BaseFormComponent {
  @Input() set user(user: UserModel) {
    if (user) {
      this.form.patchValue({
        phone: user.phone,
        email: user.email,
      });
    }
  }

  emailRegex =
    "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|" +
    '"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")' +
    '@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\' +
    '[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]' +
    '?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])';

  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex), trimValidation]),
    phone: new FormControl(''),
  });

  constructor(protected translateService: TranslateService) {
    super(translateService);
  }

  getFormValues(): IFormValues {
    return super.getFormValues();
  }
}
