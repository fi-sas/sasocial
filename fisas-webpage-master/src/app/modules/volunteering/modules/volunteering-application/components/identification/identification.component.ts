import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { finalize, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AcademicYear } from '@fi-sas/webpage/shared/models';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { BaseFormComponent } from '../base-form';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';
import { nationalities } from '@fi-sas/webpage/shared/data/nationalities';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';
import { OrganicUnitsService } from '@fi-sas/webpage/shared/services/organic-units.service';
import { Resource } from '@fi-sas/core';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';

interface IFormValue {
  academic_year: string;
  address: string;
  birth_date: string;
  city: string;
  identification_number: string;
  name: string;
  nationality: string;
  postal_code: string;
  tin: number;
  // Student
  course_degree_id?: number;
  course_id?: number;
  course_year?: number;
  // Student | Employee
  school_id?: number;
  student_number?: number;
}

interface IFormField {
  key: string;
  control: FormControl;
}

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.less'],
})
export class IdentificationComponent extends BaseFormComponent {
  @Input() set user(user: UserModel) {
    if (this.authService.hasPermission('sasocial:is_student')) {
      this.setAdditionalFields(this.studentFormFields);
      this.getDegrees();
      this.getCourses();
    }
    if (this.authService.hasPermission('sasocial:is_employee')) {
      this.setAdditionalFields(this.employeeFormFields);
    }
    this.getSchools();
    if (user) {
      this.fillFormFromUser(user);
    }
    this.getCurrentAcademicYear();
  }

  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this.fillFormFromApplication(application);
    }
    if (!application || !application.academic_year) {
      this.getCurrentAcademicYear();
    }
  }

  nationalityOptions = nationalities;

  loadingCourses = false;
  loadingSchools = false;
  loadingDegrees = false;

  disableCurses = true;

  degrees: CourseDegreeModel[];
  courses: CourseModel[] = [];
  organicUnits: OrganicUnitModel[] = [];

  educationInformation = {
    course_degree: '',
    course: '',
    school: '',
  };

  form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, trimValidation]),
    address: new FormControl('',[Validators.required, trimValidation]),
    postal_code: new FormControl('', Validators.required),
    city: new FormControl('', [Validators.required, trimValidation]),
    birth_date: new FormControl('', Validators.required),
    tin: new FormControl('', Validators.required),
    identification_number: new FormControl('', [Validators.required, trimValidation]),
    academic_year: new FormControl({ value: '', disabled: true }, Validators.required),
    nationality: new FormControl('', Validators.required),
    //school_id: new FormControl('', Validators.required),
  });

  private readonly studentFormFields: IFormField[] = [
    { key: 'course_degree_id', control: new FormControl('', [Validators.required]) },
    { key: 'course_id', control: new FormControl('', [Validators.required]) },
    { key: 'course_year', control: new FormControl('', [Validators.required]) },
    { key: 'school_id', control: new FormControl('',[Validators.required]) },
    { key: 'student_number', control: new FormControl('', [Validators.required]) },
  ];

  private readonly employeeFormFields: IFormField[] = [
    { key: 'student_number', control: new FormControl('', Validators.required) },
    //{ key: 'school_id', control: new FormControl('', Validators.required) },
  ];

  constructor(
    private authService: AuthService,
    private configurationsServices: ConfigurationsService,
    private organicUnitsService: OrganicUnitsService,
    public translateService: TranslateService
  ) {
    super(translateService);
  }

  getFormValues(): IFormValue {
    return super.getFormValues();
  }

  isFormValid(form: FormGroup): boolean {
    this.setFieldLoadingError(form, 'course_id', this.loadingCourses);
    this.setFieldLoadingError(form, 'course_degree_id', this.loadingDegrees);
    this.setFieldLoadingError(form, 'school_id', this.loadingSchools);
    let isValid = true;
    for (const i in form.controls) {
      if (i) {
        form.controls[i].markAsDirty();
        form.controls[i].updateValueAndValidity();
        if (form.controls[i].errors !== null) {
          isValid = false;
        }
      }
    }
    return isValid;
  }

  getSchools() {
    this.loadingSchools = true;
    this.organicUnitsService
      .organicUnitsbySchools()
      .pipe(
        first(),
        finalize(() => (this.loadingSchools = false))
      )
      .subscribe((schools) => this.organicUnits = schools.data);
  }

  getDegrees() {
    this.loadingDegrees = true;
    this.configurationsServices
      .courseDegrees()
      .pipe(
        first(),
        finalize(() => (this.loadingDegrees = false))
      )
      .subscribe((degrees) => this.degrees = degrees.data);
  }

  getCourses() {
    this.loadingCourses = true;
    this.configurationsServices
      .courses(this.form.get('course_degree_id').value, this.form.get('school_id').value)
      .pipe(
        first(),
        finalize(() => (this.loadingCourses = false))
      )
      .subscribe((courses) => this.courses = courses.data);
  }

  getEducationInformation(): any {
    if ((this.degrees || []).length && this.form.controls.course_degree_id) {
      this.educationInformation.course_degree = this.degrees.find(
        (degree) => degree.id === this.form.controls.course_degree_id.value
      ).name;
    }
    if ((this.courses || []).length && this.form.controls.course_id) {
      this.educationInformation.course = this.courses.find(
        (school) => school.id === this.form.controls.course_id.value
      ).name;
    }
    if ((this.organicUnits || []).length && this.form.controls.school_id) {
      this.educationInformation.school = this.organicUnits.find(
        (course) => course.id === this.form.controls.school_id.value
      ).name;
    }

    return this.educationInformation;
  }

  disableEndDate = (current: Date): boolean => {
    return !moment(current).isBefore(new Date());
  };

 getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.stillLoading) {
      return this.translateService.instant('MISC.LOADING_OPTIONS_ERROR');
    }
    return super.getInputError(inputName);
  }

  resetDegree() {
    this.form.get('course_degree_id').setValue(null);
    this.getDegrees();
    this.resetCourse();
  }

  resetCourse() {
    this.form.get('course_id').setValue(null);
    this.getCourses();
  }

  private setFieldLoadingError(form: FormGroup, fieldName: string, fieldLoading: boolean) {
    if (fieldLoading && form.controls[fieldName]) {
      form.get(fieldName).setErrors({ stillLoading: true });
    }
  }

  private getCurrentAcademicYear(){
    this.configurationsServices.getCurrentAcademicYear().pipe(first()).subscribe(response =>  {
      if(response.data.length){
        this.form.get('academic_year').setValue(response.data[0].academic_year)
      }else{
        this.form.get('academic_year').enable();
      }
  });
  }

  private fillFormFromApplication(application: VolunteeringModel) {
    this.form.patchValue({
      academic_year: application.academic_year,
      course_year: (application.course_year || '').toString(),
      iban: application.iban,
      address: application.address,
      birth_date: application.birthdate,
      postal_code: application.code_postal,
      identification_number: application.identification_number,
      city: application.location,
      name: application.name,
      nationality: application.nationality,
      student_number: (application.student_number || '').toString(),
      tin: (application.tin || '').toString(),
      // THE FOLLOWING THREE FIELDS NEEDS TO FOLLOW THIS ORDER BECAUSE OF DEPENDENCIES BETWEEN FIELDS
      school_id: application.school_id,
      course_degree_id: application.course_degree_id,
      course_id: application.course_id,
    });
  }

  private fillFormFromUser(user: UserModel) {
    this.form.patchValue({
      name: user.name,
      address: user.address,
      postal_code: user.postal_code,
      city: user.city,
      birth_date: user.birth_date,
      tin: user.tin,
      identification_number: user.identification,
      student_number: user.student_number,
      course_year: user.course_year,
      course_degree_id: user.course_degree_id,
      school_id: user.organic_unit_id,
      course_id: user.course_id,
      nationality: user.nationality,
      gender: user.gender,
    });

    this.setFieldLoadingError(this.form, 'nationality', false);
    this.disableFilledFields([
      'name',
      'address',
      'postal_code',
      'city',
      'birth_date',
      'tin',
      'identification_number',
      'student_number',
      'course_year',
      'course_degree_id',
      'school_id',
      'course_id',
      'nationality',
    ]);
  }

  private setAdditionalFields(fields: IFormField[]) {
    fields.forEach((field) => this.form.addControl(field.key, field.control));
  }
}
