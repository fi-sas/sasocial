import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinguisticSkillsComponent } from './linguistic-skills.component';

describe('LinguisticSkillsComponent', () => {
  let component: LinguisticSkillsComponent;
  let fixture: ComponentFixture<LinguisticSkillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinguisticSkillsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinguisticSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
