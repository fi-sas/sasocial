import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { languages } from '../skills-professional-experience/languages';
import { BaseFormComponent } from '../base-form';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

interface IFormValues {
  foreign_languages: boolean;
  language_skills: string[];
}

@Component({
  selector: 'app-linguistic-skills',
  templateUrl: './linguistic-skills.component.html',
  styleUrls: ['./linguistic-skills.component.less'],
})
export class LinguisticSkillsComponent extends BaseFormComponent {
  other_language = false;
  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this.form.patchValue({
        foreign_languages: application.foreign_languages,
        language_skills: (application.language_skills || []).filter((language: string) =>
          this.languages.includes(language)
        ),
        other_language_skills: (application.language_skills || []).find(
          (language: string) => !this.languages.includes(language)
        ),
      });
      if(this.form.get('other_language_skills').value) {
        this.other_language = true;
        const aux = this.form.get('language_skills').value;
        aux.push('OTHER');
        
      }
    }
  }
  
  form: FormGroup = new FormGroup({
    foreign_languages: new FormControl(null, Validators.required),
    language_skills: new FormControl(null),
    other_language_skills: new FormControl(null,[trimValidation]),
  });

  languages = languages;

  constructor(protected translateService: TranslateService) {
    super(translateService);

  }

  getFormValues(): IFormValues {
    const value: IFormValues & { other_language_skills: string } = super.getFormValues();
    if(value.language_skills) {
      if(value.language_skills.includes('OTHER')) {
        const idx = value.language_skills.indexOf('OTHER');
        value.language_skills.splice(idx,1);
      }
    }
    
    const languages = value.language_skills || [];
    if (value.other_language_skills) {
      value.other_language_skills
        .replace(/,\s+/gm, ',')
        .split(',')
        .forEach((language) => languages.push(language));
    }
    return {
      foreign_languages: value.foreign_languages,
      language_skills: value.foreign_languages ? languages : null,
    };
  }

  changeLanguage(event) {
   if(event) {
      this.form.controls.language_skills.setValidators([Validators.required]);
    }else {
      this.form.controls.language_skills.clearValidators();
    }
    this.form.controls.language_skills.updateValueAndValidity();
  }

  onChangeLanguage(event) {
    this.other_language = false;
    if (event.length > 0) {
      event.forEach(lang => {
        if (lang == "OTHER") {
          this.other_language = true;
          this.form.get('other_language_skills').enable();
        }
      });
      this.form.get('language_skills').setValue(event);
    } else {
      this.form.get('language_skills').setValue('');
    }

    if (this.other_language) {
      this.form.controls.other_language_skills.setValidators([Validators.required,trimValidation]);
    } else {
      this.form.controls.other_language_skills.clearValidators();
    }
    this.form.controls.other_language_skills.updateValueAndValidity();
  }


  check(lang) {
    if (this.form.get('language_skills').value) {
      return this.form.get('language_skills').value.includes(lang);
    } else {
      return false;
    }
  }

  changeOther() {
    this.form.get('other_language_skills').setValue('');
  }
}
