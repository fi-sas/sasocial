import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

import { BaseFormComponent } from '../base-form';
import { environment } from './../../../../../../../environments/environment';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { takeUntil } from 'rxjs/operators';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

interface IFormValue {
  has_collaborated_last_year: boolean;
  previous_activity_description?: string;
}

@Component({
  selector: 'app-past-collaborations',
  templateUrl: './past-collaborations.component.html',
  styleUrls: ['./past-collaborations.component.less'],
})
export class PastCollaborationsComponent extends BaseFormComponent {
  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this.form.patchValue({
        has_collaborated_last_year: application.has_collaborated_last_year,
        previous_activity_description: application.previous_activity_description,
      });
    }
  }

  form: FormGroup = new FormGroup({
    has_collaborated_last_year: new FormControl(null, Validators.required),
    previous_activity_description: new FormControl({ value: '', disabled: true }, [trimValidation]),
  });

  institute = environment.institute;

  constructor(protected translateService: TranslateService) {
    super(translateService);

    this.form
      .get('has_collaborated_last_year')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value: boolean) => this.setPreviousActivityDescriptionRequired(value));
  }

  getFormValues(): IFormValue {
    return super.getFormValues();
  }

  private setPreviousActivityDescriptionRequired(hasCollaboratedLastYear: boolean) {
    const formControl = this.form.get('previous_activity_description');
    if (hasCollaboratedLastYear) {
      formControl.enable();
      formControl.setValidators([Validators.required,trimValidation]);
    } else {
      formControl.disable();
      formControl.clearValidators();
      formControl.updateValueAndValidity();
    }
  }
}
