import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsProfessionalExperienceComponent } from './skills-professional-experience.component';

describe('SkillsProfessionalExperienceComponent', () => {
  let component: SkillsProfessionalExperienceComponent;
  let fixture: ComponentFixture<SkillsProfessionalExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SkillsProfessionalExperienceComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsProfessionalExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
