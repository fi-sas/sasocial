import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { BaseFormComponent } from '../base-form';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { trimValidation } from '@fi-sas/webpage/shared/validators/validators.validator';

interface IFormValues {
  has_profissional_experience: boolean;
  type_of_company: string;
  job_at_company: string;
  job_description: string;
}

@Component({
  selector: 'app-skills-professional-experience',
  templateUrl: './skills-professional-experience.component.html',
  styleUrls: ['./skills-professional-experience.component.less'],
})
export class SkillsProfessionalExperienceComponent extends BaseFormComponent {
  @Input() set application(application: VolunteeringModel) {
    if (application) {
      this.setFormData(application);
    }
  }

  form: FormGroup = new FormGroup({
    has_profissional_experience: new FormControl(null, Validators.required),
    type_of_company: new FormControl({ value: '', disabled: true },[trimValidation]),
    job_at_company: new FormControl({ value: '', disabled: true },[trimValidation]),
    job_description: new FormControl({ value: '', disabled: true },[trimValidation]),
  });

  constructor(protected translateService: TranslateService) {
    super(translateService);

    this.handleHasProfissionalExperienceChange();
  }

  getFormValues(): IFormValues {
    return super.getFormValues();
  }

  private setFormData(application: VolunteeringModel) {
    this.form.patchValue({
      has_profissional_experience: application.has_profissional_experience,
      type_of_company: application.type_of_company,
      job_at_company: application.job_at_company,
      job_description: application.job_description,
    });
  }

  private handleHasProfissionalExperienceChange(): void {
    this.form
      .get('has_profissional_experience')
      .valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe((value) => this.toggleJobInfoActiveMode(value));
  }

  private toggleJobInfoActiveMode(value): void {
    [this.form.get('type_of_company'), this.form.get('job_at_company'), this.form.get('job_description')].forEach(
      (formControl: FormControl) => {
        if (value === true) {
          formControl.setValidators([Validators.required,trimValidation]);
          formControl.enable();
        } else {
          formControl.clearValidators();
          formControl.disable();
        }
        formControl.updateValueAndValidity();
      }
    );
  }
}
