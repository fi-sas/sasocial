import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessApplicationSubmittedComponent } from './success-application-submitted.component';

describe('SuccessApplicationSubmittedComponent', () => {
  let component: SuccessApplicationSubmittedComponent;
  let fixture: ComponentFixture<SuccessApplicationSubmittedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SuccessApplicationSubmittedComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessApplicationSubmittedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
