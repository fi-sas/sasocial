import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormApplicationReviewComponent } from './form-application-review.component';

const routes: Routes = [
  {
    path: '',
    component: FormApplicationReviewComponent,
    data: { breadcrumb: null, title: null, scope: 'volunteering:applications:read' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormApplicationReviewRoutingModule {}
