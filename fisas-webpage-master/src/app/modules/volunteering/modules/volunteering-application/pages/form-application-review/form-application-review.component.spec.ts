import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormApplicationReviewComponent } from './form-application-review.component';

describe('FormApplicationReviewComponent', () => {
  let component: FormApplicationReviewComponent;
  let fixture: ComponentFixture<FormApplicationReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormApplicationReviewComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormApplicationReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
