import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import {
  APPLICATION_STATUS,
  AuxDataApplication,
  VolunteeringModel,
} from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { ChangeStatusService } from '@fi-sas/webpage/modules/volunteering/services/change-status.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Resource } from '@fi-sas/core';
import { VolunteeringApplicationsService } from '@fi-sas/webpage/modules/volunteering/modules/volunteering-application/services';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-form-application-review',
  templateUrl: './form-application-review.component.html',
  styleUrls: ['./form-application-review.component.less'],
})
export class FormApplicationReviewComponent implements OnInit {
  buttonBackStyle = {
    width: 'auto',
    height: '30px',
    'font-size': '15px',
    'margin-top': '33px',
    'margin-bottom': '65.5px',
  };

  application: VolunteeringModel;
  isLoading: boolean;
  isCancelLoading: boolean;
  showCancelButton: boolean;
  canEditApplication: boolean;

  auxData: AuxDataApplication = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private changeStatusService: ChangeStatusService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,
    private uiService: UiService,
    private volunteeringApplicationsService: VolunteeringApplicationsService
  ) {}

  ngOnInit() {
    const applicationId: number = this.activatedRoute.snapshot.params.id;
    this.getApplication(applicationId);
  }

  backToApplicationStatusHistory() {
    this.router.navigateByUrl('volunteering/application-status-history');
  }

  openCancelApplicationConfirmationModal() {
    this.modalService.confirm({
      nzTitle: this.translateService.instant('VOLUNTEERING.MY_APPLICATIONS.CANCEL_MODAL_TITLE'),
      nzCancelText: this.translateService.instant('VIEW_APPLICATION_STATUS.NO_CANCEL_TEXT'),
      nzOkText: this.translateService.instant('VIEW_APPLICATION_STATUS.YES_CANCEL_TEXT'),
      nzOnOk: () => this.cancelApplication(),
    });
  }

  private cancelApplication() {
    this.isCancelLoading = true;
    this.changeStatusService.applicationCancel(this.application.id).pipe(first(), finalize(() => this.isCancelLoading = false))
      .subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('VOLUNTEERING.MY_APPLICATIONS.CANCEL_SUCCESS')
        );
        this.router.navigateByUrl('volunteering');
      });  
  }

  private getApplication(id: number) {
    this.isLoading = true;
    this.volunteeringApplicationsService.getApplication(id).pipe(first(), finalize(() => this.isLoading = false))
      .subscribe(response => {
        this.application = response.data[0];
        this.application.preferred_schedule_ids = (this.application.preferred_schedule || []).map((s) => s.schedule_id);
        this.auxData = this.getAuxData();
        this.showCancelButton = [
          APPLICATION_STATUS.SUBMITTED,
          APPLICATION_STATUS.ANALYSED,
          APPLICATION_STATUS.ACCEPTED,
        ].includes(this.application.status as APPLICATION_STATUS);
        this.canEditApplication =
          this.application.id &&
          [APPLICATION_STATUS.SUBMITTED, APPLICATION_STATUS.ANALYSED].includes(
            this.application.status as APPLICATION_STATUS
          );
      });
  }

  private getAuxData() {
    return {
      course_degree: this.application.course_degree ? this.application.course_degree.name : '',
      course: this.application.course ? this.application.course.name : '',
      school: this.application.school ? this.application.school.name : '',
      organic_units: (this.application.organic_units || []).map((organicUnit) => organicUnit.organic_unit.name),
      organic_units_ids: (this.application.organic_units || []).map((organicUnit) => organicUnit.id),
    };
  }
}
