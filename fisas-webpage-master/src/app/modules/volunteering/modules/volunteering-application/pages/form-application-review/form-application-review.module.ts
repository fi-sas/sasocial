import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormApplicationReviewComponent } from './form-application-review.component';
import { FormApplicationReviewRoutingModule } from './form-application-review-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';

@NgModule({
  declarations: [FormApplicationReviewComponent],
  imports: [CommonModule, FormApplicationReviewRoutingModule, SharedModule, SharedVolunteeringModule],
})
export class FormApplicationReviewModule {}
