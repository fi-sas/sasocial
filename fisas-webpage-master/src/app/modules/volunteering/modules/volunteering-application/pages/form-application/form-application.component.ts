import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';

import { ApplicationSubmittedService } from '../../../application-status-history/services/application-submitted.service';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { environment } from './../../../../../../../environments/environment';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { VolunteeringApplicationsService } from './../../services/volunteering-applications.service';
import { VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import {
  ActivitiesLocationsComponent,
  ComplementaryInformationComponent,
  ContactsComponent,
  IdentificationComponent,
  LinguisticSkillsComponent,
  PastCollaborationsComponent,
  SkillsProfessionalExperienceComponent,
  SuccessApplicationSubmittedComponent,
  TimeAvailabilityComponent,
} from './../../components';

@Component({
  selector: 'app-form-application',
  templateUrl: './form-application.component.html',
  styleUrls: ['./form-application.component.less'],
})
export class FormApplicationComponent implements OnInit {
  @ViewChild('activitiesLocationsForm') activitiesLocationsForm: ActivitiesLocationsComponent;
  @ViewChild('complementaryInformationForm') complementaryInformationForm: ComplementaryInformationComponent;
  @ViewChild('contactsForm') contactsForm: ContactsComponent;
  @ViewChild('identificationForm') identificationForm: IdentificationComponent;
  @ViewChild('pastCollaborationsForm') pastCollaborationsForm: PastCollaborationsComponent;
  @ViewChild('skillsProfessionalExperienceForm')
  skillsProfessionalExperienceForm: SkillsProfessionalExperienceComponent;
  @ViewChild('timeAvailabilityForm') timeAvailabilityForm: TimeAvailabilityComponent;
  @ViewChild('linguisticSkillsForm') linguisticSkillsForm: LinguisticSkillsComponent;

  user: UserModel = null;
  institute = environment.institute;
  showErrorMessage = false;
  successFormModal: NzModalRef = null;

  review = false;
  loadingSubmit = false;

  application: VolunteeringModel = null;

  auxData = {};

  isLoadingApplication: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private applicationSubmittedService: ApplicationSubmittedService,
    private authService: AuthService,
    private modalService: NzModalService,
    private router: Router,
    private volunteeringApplicationsService: VolunteeringApplicationsService
  ) {
    if (!isNaN(this.activatedRoute.snapshot.params.id)) {
      this.getApplication(this.activatedRoute.snapshot.params.id);
    }
  }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  validAllForms(): boolean {
    const identificationValidity = this.identificationForm.validForm();
    const contactsValidity = this.contactsForm.validForm();
    const activitiesLocationsValidity = this.activitiesLocationsForm.validForm();
    const pastCollaborationsValidity = this.pastCollaborationsForm.validForm();
    const timeAvailabilityValidity = this.timeAvailabilityForm.validForm();
    const skillsProfessionalExperienceValidity = this.skillsProfessionalExperienceForm.validForm();
    const complementaryInformationValidity = this.complementaryInformationForm.validForm();
    const linguisticSkillsValidity = this.linguisticSkillsForm.validForm();
    return (
      identificationValidity &&
      contactsValidity &&
      activitiesLocationsValidity &&
      pastCollaborationsValidity &&
      timeAvailabilityValidity &&
      skillsProfessionalExperienceValidity &&
      complementaryInformationValidity &&
      linguisticSkillsValidity
    );
  }

  goToReview() {
    if (!this.validAllForms()) {
      this.showErrorMessage = true;
      window.scrollTo(0, 0);
      return;
    }
    this.showErrorMessage = false;
    if (!this.application || !this.application.id) {
      this.application = new VolunteeringModel();
    }

    const identification = this.identificationForm.getFormValues();
    const contacts = this.contactsForm.getFormValues();
    const pastCollaborations = this.pastCollaborationsForm.getFormValues();
    const timeAvailability = this.timeAvailabilityForm.getFormValues();
    const skillsProfessionalExperience = this.skillsProfessionalExperienceForm.getFormValues();
    const complementaryInformation = this.complementaryInformationForm.getFormValues();
    const linguisticSkills = this.linguisticSkillsForm.getFormValues();
    const activitiesLocations = this.activitiesLocationsForm.getFormValues();

    this.auxData = Object.assign(this.auxData, this.identificationForm.getEducationInformation());
    this.auxData = Object.assign(this.auxData, this.activitiesLocationsForm.getSelectedOrganicUnits());

    // identification
    this.application.name = identification.name;
    this.application.address = identification.address;
    this.application.nationality = identification.nationality;
    this.application.student_number = identification.student_number;
    this.application.course_year = identification.course_year;
    this.application.school_id = identification.school_id;
    this.application.course_degree_id = identification.course_degree_id;
    this.application.course_id = identification.course_id;
    this.application.academic_year = identification.academic_year;
    this.application.code_postal = identification.postal_code;
    this.application.location = identification.city;
    this.application.birthdate = identification.birth_date;
    this.application.tin = identification.tin;
    this.application.identification_number = identification.identification_number;
    this.application.genre = this.user.gender;

    // contacts
    this.application.email = contacts.email;
    this.application.phone = contacts.phone;

    // activitiesLocations
    this.application.organic_units_ids = activitiesLocations.organic_units_ids;

    // pastCollaborations
    this.application.has_collaborated_last_year = pastCollaborations.has_collaborated_last_year;
    this.application.previous_activity_description = pastCollaborations.previous_activity_description;

    // timeAvailability
    this.application.preferred_schedule_ids = timeAvailability.preferred_schedule;
    this.application.has_holydays_availability = timeAvailability.has_holydays_availability;

    // linguisticSkills
    this.application.foreign_languages = linguisticSkills.foreign_languages;
    this.application.language_skills = linguisticSkills.language_skills;

    // skillsProfessionalExperience
    this.application.has_profissional_experience = skillsProfessionalExperience.has_profissional_experience;
    if (this.application.has_profissional_experience) {
      this.application.type_of_company = skillsProfessionalExperience.type_of_company;
      this.application.job_at_company = skillsProfessionalExperience.job_at_company;
      this.application.job_description = skillsProfessionalExperience.job_description;
    }

    // complementaryInformation
    this.application.observations = complementaryInformation.observations;

    this.review = true;

    window.scrollTo(0, 0);
  }

  submitApplication() {
    const applicationToSubmit: VolunteeringModel = Object.assign({}, this.application);
    this.loadingSubmit = true;

    if (applicationToSubmit.has_profissional_experience === false) {
      applicationToSubmit.type_of_company = null;
      applicationToSubmit.job_at_company = null;
      applicationToSubmit.job_description = null;
    }
    if (applicationToSubmit.foreign_languages) {
      applicationToSubmit.language_skills = applicationToSubmit.language_skills;
    }
    if (applicationToSubmit.has_collaborated_last_year === false) {
      applicationToSubmit.previous_activity_description = null;
    }
    if (applicationToSubmit.observations === '') {
      applicationToSubmit.observations = ' ';
    }

    const isEdit = !!this.application.id;

    let action = this.volunteeringApplicationsService.createVolunteeringApplication(applicationToSubmit);
    if (isEdit) {
      action = this.volunteeringApplicationsService.editVolunteeringApplication(applicationToSubmit);
    }

    action.pipe(first()).subscribe(
      () => {
        this.loadingSubmit = false;
        this.applicationSubmittedService.saveSubmitted(true);
        this.successFormModal = this.modalService.create({
          nzWidth: 350,
          nzMaskClosable: false,
          nzClosable: true,
          nzContent: SuccessApplicationSubmittedComponent,
          nzComponentParams: { isEdit },
          nzFooter: null,
        });
        this.successFormModal.afterClose.pipe(first()).subscribe(() => this.back());
      },
      () => (this.loadingSubmit = false)
    );
  }

  backToForm() {
    this.review = false;
  }

  back(): void {
    this.router.navigateByUrl('volunteering/experiences');
  }

  private getApplication(id: number) {
    this.isLoadingApplication = true;
    this.volunteeringApplicationsService
      .getApplication(id)
      .pipe(
        first(),
        finalize(() => (this.isLoadingApplication = false))
      )
      .subscribe((result) => {
        if (!['ANALYSED', 'SUBMITTED', 'ACCEPTED'].includes(result.data[0].status)) {
          return this.router.navigateByUrl('/unauthorized');
        }
        this.application = result.data[0];
        this.application.observations = this.application.observations === ' ' ? '' : this.application.observations;
      });
  }
}
