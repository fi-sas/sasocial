import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormApplicationComponent } from './form-application.component';
import { FormVolunteeringRoutingModule } from './form-application-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from '../../../shared-volunteering/shared-volunteering.module';
import {
  ActivitiesLocationsComponent,
  ComplementaryInformationComponent,
  ContactsComponent,
  IdentificationComponent,
  LinguisticSkillsComponent,
  PastCollaborationsComponent,
  SkillsProfessionalExperienceComponent,
  SuccessApplicationSubmittedComponent,
  TimeAvailabilityComponent,
} from '../../components';

@NgModule({
  declarations: [
    ActivitiesLocationsComponent,
    ComplementaryInformationComponent,
    ContactsComponent,
    FormApplicationComponent,
    IdentificationComponent,
    LinguisticSkillsComponent,
    PastCollaborationsComponent,
    SkillsProfessionalExperienceComponent,
    SuccessApplicationSubmittedComponent,
    TimeAvailabilityComponent,
  ],
  imports: [CommonModule, SharedModule, FormVolunteeringRoutingModule, SharedVolunteeringModule],
  entryComponents: [SuccessApplicationSubmittedComponent],
})
export class FormVolunteeringModule {}
