import { TestBed } from '@angular/core/testing';

import { VolunteeringApplicationsService } from './volunteering-applications.service';

describe('SocialSupportApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VolunteeringApplicationsService = TestBed.get(VolunteeringApplicationsService);
    expect(service).toBeTruthy();
  });
});
