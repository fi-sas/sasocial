import { APPLICATION_STATUS, VolunteeringModel } from '@fi-sas/webpage/modules/volunteering/models/volunteering-application.model';
import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable, Subject } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class VolunteeringApplicationsService {
  loading = false;

  public isApplicationActive: Subject<any> = new Subject<any>();

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService,
    private authService: AuthService
  ) {}

  createVolunteeringApplication(application: VolunteeringModel): Observable<Resource<VolunteeringModel>> {
    delete application.user;
    return this.resourceService.create<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS', {}),
      application
    );
  }

  editVolunteeringApplication(application: VolunteeringModel): Observable<Resource<VolunteeringModel>> {
    delete application.user;
    return this.resourceService.patch<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_ID', { id: application.id }),
      application
    );
  }

  applicationsUsers(): Observable<Resource<VolunteeringModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('subject', 'V');
    params = params.set('user_id', this.authService.getUser().id.toString());
    return this.resourceService.list<VolunteeringModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS'), { params });
  }

  applicationActiveObservable() {
    return this.isApplicationActive.asObservable();
  }

  applicationActive() {
    const statusToApplicationActive = [
      'published',
      'withdrawal',
      'submitted',
      'analysed',
      'interviewed',
      'waiting',
      'accepted',
      'acknowledged',
    ];

    let userApplicationsActive: VolunteeringModel[] = [];

    if (this.loading) {
      return;
    }
    this.loading = true;


    this.applicationsUsers()
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((applications) => {
        userApplicationsActive = applications.data.filter((app) =>
          this.isInclude(app.status, statusToApplicationActive)
        );

        this.isApplicationActive.next(userApplicationsActive.length !== 0);
      });
  }

  isInclude(status: string, arrayStatus: string[]): boolean {
    return arrayStatus.includes(status);
  }

  currentApplications() {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', '-created_at');
    params = params.set(
      'withRelated',
      'history,experience,course,course_degree,school,organic_units,preferred_schedule,interviews,history'
    );
    params = params.append('query[status]', APPLICATION_STATUS.SUBMITTED);
    params = params.append('query[status]', APPLICATION_STATUS.ACCEPTED);
    params = params.append('query[status]', APPLICATION_STATUS.ANALYSED);
    params = params.append('query[status]', APPLICATION_STATUS.SUBMITTED);
    params = params.append('query[status]', APPLICATION_STATUS.DISPATCH);
    return this.resourceService.list<VolunteeringModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS'), { params });
  }

  pastApplications() {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('sort', '-created_at');
    params = params.append('query[status]', APPLICATION_STATUS.DECLINED);
    params = params.append('query[status]', APPLICATION_STATUS.EXPIRED);
    params = params.append('query[status]', APPLICATION_STATUS.CANCELLED);
    params = params.set(
      'withRelated',
      'history,experience,course,course_degree,school,organic_units,preferred_schedule'
    );
    return this.resourceService.list<VolunteeringModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS'), { params });
  }

  getApplication(id: number): Observable<Resource<VolunteeringModel>> {
    return this.resourceService.read<VolunteeringModel>(this.urlService.get('VOLUNTEERING.APPLICATIONS_ID', { id }));
  }
}
