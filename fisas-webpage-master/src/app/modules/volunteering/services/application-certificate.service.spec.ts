import { TestBed } from '@angular/core/testing';

import { ApplicationCertificateService } from './application-certificate.service';

describe('ApplicationCertificateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationCertificateService = TestBed.get(ApplicationCertificateService);
    expect(service).toBeTruthy();
  });
});
