import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { CertificateModel } from '@fi-sas/webpage/modules/social-support/modules/attendances/models/certificate.model';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class ApplicationCertificateService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  getCertificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.read<CertificateModel>(
      this.urlService.get('VOLUNTEERING.APPLICATIONS_CERTIFICATE', { id }),
      {}
    );
  }

  generateCertificate(id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(
      this.urlService.get('VOLUNTEERING.GENERATE_COLLABORATION_CERTIFICATE', { id }),
      {}
    );
  }

  addCertificate(id: number, certificate_file_id: number): Observable<Resource<CertificateModel>> {
    return this.resourceService.create<CertificateModel>(
      this.urlService.get('VOLUNTEERING.ADD_COLLABORATION_CERTIFICATE', { id }),
      { certificate_file_id }
    );
  }
}
