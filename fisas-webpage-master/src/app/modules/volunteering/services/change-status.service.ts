import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { ChangeStatusModel } from '../models/change-status.model';
import { Observable } from 'rxjs';
import { VolunteeringModel } from '../models/volunteering-application.model';

@Injectable({
  providedIn: 'root',
})
export class ChangeStatusService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  applicationAccept(id: number): Observable<Resource<VolunteeringModel>> {
    const status = new ChangeStatusModel();
    status.notes = 'Aceite e pelo utilizador';
    status.event = 'ACKNOWLEDGE';
    return this.resourceService.create<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATION_STATUS', { id }),
      status
    );
  }

  applicationCancel(id: number, notes?: ChangeStatusModel): Observable<Resource<VolunteeringModel>> {
    return this.resourceService.create<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATION_CANCEL', { id }),
      notes
    );
  }

  applicationWithdraw(id: number, notes: ChangeStatusModel): Observable<Resource<VolunteeringModel>> {
    return this.resourceService.create<VolunteeringModel>(
      this.urlService.get('VOLUNTEERING.APPLICATION_WITHDRAW', { id }),
      notes
    );
  }
}
