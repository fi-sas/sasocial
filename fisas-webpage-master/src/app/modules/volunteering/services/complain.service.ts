import { Injectable } from '@angular/core';

import { FiResourceService, FiUrlService } from '@fi-sas/core';
import { GeneralComplain } from '@fi-sas/webpage/shared/models';

@Injectable({
  providedIn: 'root'
})
export class ComplainService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  general(data: GeneralComplain) {
    return this.resourceService.create<any>(this.urlService.get('VOLUNTEERING.GENERAL_COMPLAINS'), data);
  }
}
