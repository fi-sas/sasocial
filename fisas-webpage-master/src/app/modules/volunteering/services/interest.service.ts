import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { map } from "rxjs/operators";
import { Observable, of } from "rxjs";

import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { FiResourceService, FiUrlService, Resource } from "@fi-sas/core";
import {
  ActionCandiate,
  ChangeStatusModel,
  IHasInterest,
  INTEREST_STATUS_EVENT,
  INTEREST_STATUS,
  InterestModel,
  InterviewModel,
  InterviewReportModel,
} from "../models";
import { ComplaintUserInterestModel } from "../models/complain-user-interests.model";
import { SendDispatchData } from "../models/dispatch.model";

@Injectable({
  providedIn: "root",
})
export class InterestService {
  constructor(
    private authService: AuthService,
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {}

  createUserInterest(
    experienceId: number
  ): Observable<Resource<InterestModel>> {
    return this.resourceService.create<InterestModel>(
      this.urlService.get("VOLUNTEERING.EXPERIENCES_USER_INTEREST"),
      {
        experience_id: experienceId,
      }
    );
  }

  listInterests(
    status: INTEREST_STATUS[] = [],
    related: string[] = [
      "experience",
      "interview",
      "contract_file",
      "history",
      "school",
    ]
  ): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    if ((related || []).length) {
      params = params.set("withRelated", related.join(","));
    }
    if ((status || []).length) {
      status.forEach((s) => (params = params.append("query[status]", s)));
    }
    return this.resourceService.list<InterestModel>(
      this.urlService.get("VOLUNTEERING.EXPERIENCES_USER_INTEREST"),
      {
        params,
      }
    );
  }

  listCurrentInterests(): Observable<Resource<InterestModel>> {
    return this.listInterests([
      INTEREST_STATUS.SUBMITTED,
      INTEREST_STATUS.ANALYSED,
      INTEREST_STATUS.APPROVED,
      INTEREST_STATUS.WAITING,
      INTEREST_STATUS.NOT_SELECTED,
      INTEREST_STATUS.ACCEPTED,
      INTEREST_STATUS.COLABORATION,
      INTEREST_STATUS.WITHDRAWAL,
      INTEREST_STATUS.DECLINED,
      INTEREST_STATUS.DISPATCH,
    ]);
  }

  listPastInterests(): Observable<Resource<InterestModel>> {
    return this.listInterests([
      INTEREST_STATUS.CLOSED,
      INTEREST_STATUS.CANCELLED,
      INTEREST_STATUS.WITHDRAWAL_ACCEPTED,
    ]);
  }

  changeStatus(
    id: number,
    status: INTEREST_STATUS_EVENT,
    data: object = {}
  ){
    return this.resourceService.create<any>(
      this.urlService.get("VOLUNTEERING.STATUS_CHANGE_INTEREST", { id }),
      Object.assign(data, { event: status })
    );
  }

  cancelInterest(id: number): Observable<Resource<any>> {
    return this.changeStatus(id, INTEREST_STATUS_EVENT.CANCEL);
  }

  withdrawInterest(
    id: number,
    data: ChangeStatusModel
  ): Observable<Resource<any>> {
    return this.resourceService.create(
      this.urlService.get("VOLUNTEERING.EXPERIENCE_WITHDRAW_ID", { id }),
      data
    );
  }

  accept(id: number): Observable<Resource<any>> {
    return this.changeStatus(id, INTEREST_STATUS_EVENT.ACCEPT);
  }

  contest( data: ComplaintUserInterestModel): Observable<Resource<ComplaintUserInterestModel>> {
    return this.resourceService.create<ComplaintUserInterestModel>(
      this.urlService.get("VOLUNTEERING.STATUS_CHANGE_INTEREST_COMPLAIN", {}),
      { ...data }
    );
  }

  checkInterest(id: number): Observable<Resource<IHasInterest>> {
    return this.resourceService.read<IHasInterest>(
      this.urlService.get("VOLUNTEERING.CHECK_INTEREST", { id })
    );
  }

  canExpressInterest(id: number): Observable<{
    canExpresssInteres: boolean;
    hasApplication: boolean;
    applicationActive: boolean;
  }> {
    if (
      this.authService.hasPermission(
        "volunteering:experience-user-interests:create"
      )
    ) {
      return this.checkInterest(id).pipe(
        map((result: Resource<IHasInterest>) => {
          return {
            canExpresssInteres:
              result.data[0].can_express_interest &&
              this.authService.hasPermission(
                "volunteering:experience-user-interests:create"
              ),
            hasApplication: result.data[0].has_application,
            applicationActive: result.data[0].has_application_accepted,
          };
        })
      );
    } else {
      return of({
        canExpresssInteres: false,
        hasApplication: false,
        applicationActive: false,
      });
    }
  }

  getListCandidates(
    id: number,
    pageIndex: number,
    pageSize: number
  ): Observable<Resource<ActionCandiate>> {
    let params = new HttpParams();
    params = params.set("offset", ((pageIndex - 1) * pageSize).toString());
    params = params.set("limit", pageSize.toString());
    params = params.set(
      "withRelated",
      "experience,interview,contract_file,history,application,historic_applications,historic_colaborations"
    );
    return this.resourceService.list<ActionCandiate>(
      this.urlService.get("VOLUNTEERING.EXPERIENCE_ID_USER_INTERESTS", { id }),
      { params }
    );
  }

  addInterviewReport(
    id: number,
    data: InterviewReportModel
  ): Observable<Resource<InterviewModel>> {
    return this.resourceService.create(
      this.urlService.get(
        "VOLUNTEERING.EXPERIENCES_USER_INTEREST_INTERVIEW_REPORT_ID",
        { id }
      ),
      data
    );
  }

  getInterestById(id: number): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set(
      "withRelated",
      "experience,history,contract_file,report_avaliation_file,certificate_file,certificate_generated"
    );
    return this.resourceService.read<InterestModel>(
      this.urlService.get("VOLUNTEERING.EXPERIENCES_USER_INTEREST_ID", { id }),
      { params }
    );
  }

  userInterestColaboration(experience_id): Observable<Resource<InterestModel>> {
    let params = new HttpParams();
    params = params.set("query[experience_id]", experience_id);
    params = params.set("query[status]", INTEREST_STATUS.COLABORATION);
    params = params.set("sort", "created_at");
    return this.resourceService.list<InterestModel>(
      this.urlService.get("VOLUNTEERING.EXPERIENCE_ID_USER_INTERESTS", {
        id: experience_id,
      }),
      { params }
    );
  }

  collaborationEvaluation(id: number, data: { avaliation: string }) {
    return this.resourceService.create(
      this.urlService.get("VOLUNTEERING.EXPERIENCE_EVALUATION_ID", { id }),
      data
    );
  }

  sendDispatch(id: number, data: SendDispatchData){
    return this.resourceService.create<any>(
      this.urlService.get("VOLUNTEERING.STATUS_CHANGE_INTEREST", { id }),
      data
    );
  }
}
