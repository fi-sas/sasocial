import { TestBed } from '@angular/core/testing';

import { VolunteeringActionsService } from './volunteering-actions.service';

describe('VolunteeringActionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VolunteeringActionsService = TestBed.get(VolunteeringActionsService);
    expect(service).toBeTruthy();
  });
});
