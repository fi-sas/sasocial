import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { VolunteeringActionSubmitData } from '../modules/volunteering-actions/models';
import {
  ExperienceModel,
  VolunteeringActionModel,
  VOLUNTEERING_ACTION_STATUS,
  VOLUNTEERING_ACTION_STATUS_EVENT,
} from '../models';

@Injectable({
  providedIn: 'root',
})
export class VolunteeringActionsService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  getAction(id: number): Observable<Resource<VolunteeringActionModel>> {
    let params = new HttpParams();
    params = params.set(
      'withRelated',
      'attachment_file,experience_responsible,organic_unit,translations,history'
    );
    return this.resourceService.read<VolunteeringActionModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCES_ID', { id }),
      { params }
    );
  }

  create(data: VolunteeringActionSubmitData): Observable<Resource<VolunteeringActionModel>> {
    return this.resourceService.create<VolunteeringActionModel>(this.urlService.get('VOLUNTEERING.EXPERIENCES'), data);
  }

  edit(id: number, data: VolunteeringActionSubmitData): Observable<Resource<VolunteeringActionModel>> {
    return this.resourceService.update<VolunteeringActionModel>(
      this.urlService.get('VOLUNTEERING.EXPERIENCES_ID', { id }),
      data
    );
  }

  cancel(id: number) {
    return this.resourceService.create<any>(this.urlService.get('VOLUNTEERING.CANCEL_EXPERIENCE_ID', { id }), {
      event: VOLUNTEERING_ACTION_STATUS_EVENT.CANCEL,
    });
  }

  printOffer(id: number): Observable<Resource<any>> {
    return this.resourceService.list<any>(this.urlService.get('VOLUNTEERING.PRINT_EXPERIENCE_ID', { id }));
  }

  listByResponsible(): Observable<Resource<ExperienceModel>> {
    return this.listCurrentActions('VOLUNTEERING.GET_RESPONSIBLE');
  }

  listPastByResponsible(): Observable<Resource<ExperienceModel>> {
    return this.listPastActions('VOLUNTEERING.GET_RESPONSIBLE');
  }

  private listActions(status: VOLUNTEERING_ACTION_STATUS[], endpoint: string): Observable<Resource<ExperienceModel>> {
    let params = new HttpParams();
    params = params.append('withRelated', 'translations');
    if ((status || []).length) {
      status.forEach((s) => (params = params.append('query[status]', s)));
    }
    return this.resourceService.list<ExperienceModel>(this.urlService.get(endpoint), { params });
  }

  private listCurrentActions(endpoint: string): Observable<Resource<ExperienceModel>> {
    return this.listActions(
      [
        VOLUNTEERING_ACTION_STATUS.SUBMITTED,
        VOLUNTEERING_ACTION_STATUS.RETURNED,
        VOLUNTEERING_ACTION_STATUS.ANALYSED,
        VOLUNTEERING_ACTION_STATUS.APPROVED,
        VOLUNTEERING_ACTION_STATUS.PUBLISHED,
        VOLUNTEERING_ACTION_STATUS.REJECTED,
        VOLUNTEERING_ACTION_STATUS.SEND_SEEM,
        VOLUNTEERING_ACTION_STATUS.EXTERNAL_SYSTEM,
        VOLUNTEERING_ACTION_STATUS.SELECTION,
        VOLUNTEERING_ACTION_STATUS.IN_COLABORATION,
        VOLUNTEERING_ACTION_STATUS.DISPATCH,
      ],
      endpoint
    );
  }

  private listPastActions(endpoint: string): Observable<Resource<ExperienceModel>> {
    return this.listActions([VOLUNTEERING_ACTION_STATUS.CLOSED, VOLUNTEERING_ACTION_STATUS.CANCELLED], endpoint);
  }
}
