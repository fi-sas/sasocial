import { Injectable } from '@angular/core';

import { ExternalEntity } from '@fi-sas/webpage/shared/models';
import { FiResourceService, FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root'
})
export class VolunteeringService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) { }

  externalEntities() {
    return this.resourceService.list<ExternalEntity>(this.urlService.get('VOLUNTEERING.EXTERNAL_ENTITIES'));
  }
}
