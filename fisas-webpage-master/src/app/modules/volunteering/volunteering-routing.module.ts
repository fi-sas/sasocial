import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VolunteeringComponent } from './volunteering.component';
const routes: Routes = [
  {
    path: '',
    component: VolunteeringComponent,
    children: [
      { path: '', redirectTo: 'experiences', pathMatch: 'full' },
      {
        path: 'applications',
        loadChildren: 'src/app/modules/volunteering/modules/volunteering-application/pages/form-application/form-application.module#FormVolunteeringModule',
      },
      {
        path: 'applications/review/:id',
        loadChildren: 'src/app/modules/volunteering/modules/volunteering-application/pages/form-application-review/form-application-review.module#FormApplicationReviewModule'
      },
      {
        path: 'application-status-history',
        loadChildren: 'src/app/modules/volunteering/modules/application-status-history/pages/application-status-history/application-status-history.module#ApplicationStatusHistoryVolunteeringModule',
      },
      {
        path: 'express-interest-status-history',
        loadChildren: 'src/app/modules/volunteering/modules/express-interest-status-history/pages/express-interest-status-history/express-interest-status-history.module#ExpressInterestStatusHistoryVolunteeringModule'
      },
      {
        path: 'actions-status-history',
        loadChildren: 'src/app/modules/volunteering/modules/volunteering-actions-status-history/volunteering-actions-status-history.module#VolunteeringActionsStatusHistoryModule'
      },
      {
        path: 'record/:id',
        loadChildren: 'src/app/modules/volunteering/modules/attendances/pages/record-attendance/record-attendance.module#RecordAttendanceModule'
      },
      {
        path: 'experiences',
        loadChildren: 'src/app/modules/volunteering/modules/experiences/pages/experiences/experiences.module#ExperiencesVolunteeringModule',
      },
      {
        path: 'experiences/:id',
        loadChildren: 'src/app/modules/volunteering/modules/experiences/pages/experience-detail/experiences-detail.module#ExperiencesDetailVolunteeringModule',
      },
      {
        path: 'actions',
        loadChildren: 'src/app/modules/volunteering/modules/volunteering-actions/volunteering-actions.module#VolunteeringActionsModule'
      },
      {
        path: 'management-attendances/:id',
        loadChildren: 'src/app/modules/volunteering/modules/attendances/pages/management-attendances/management-attendances.module#ManagementAttendancesModule'
      },
      {
        path: 'complains',
        loadChildren: 'src/app/modules/volunteering/modules/complains/pages/complains/complains.module#ComplainsVolunteeringModule'
      }
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolunteeringRoutingModule {}
