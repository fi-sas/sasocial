import { Component, OnInit } from '@angular/core';

import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ConfigurationsService } from '@fi-sas/webpage/shared/services/configurations.service';
import { VolunteeringApplicationsService } from './modules/volunteering-application/services';

@Component({
  selector: 'app-volunteering',
  templateUrl: './volunteering.component.html',
  styleUrls: ['./volunteering.component.less'],
})
export class VolunteeringComponent implements OnInit {
  data: any[];
  barOptions: any[] = [];

  constructor(
    private authService: AuthService,
    private configurationsService: ConfigurationsService,
    private volunteeringApplicationsService: VolunteeringApplicationsService,
  ) {
    if(authService.hasPermission('volunteering:applications:read')) {
      this.volunteeringApplicationsService.applicationActive();
    }

    this.data = this.configurationsService.getLocalStorageTotalConfiguration();
  }

  ngOnInit() {
    this.data = this.configurationsService.getLocalStorageTotalConfiguration();
    this.barOptions = [
      {
        translate: this.validTitleTraductions(28),
        disable: true,
        routerLink: '/volunteering/experiences',
        type: 'array',
        scope: this.authService.hasPermission('volunteering:experiences:list'),
      },
      {
        translate: 'VOLUNTEERING.MENU.MY_APPLICATIONS',
        disable: false,
        routerLink: '/volunteering/application-status-history',
        type: 'string',
        scope: this.authService.hasPermission('volunteering:applications:list'),
      },
      {
        translate: 'VOLUNTEERING.MENU.MY_INTERESTS',
        disable: true,
        routerLink: '/volunteering/express-interest-status-history',
        type: 'string',
        scope: this.authService.hasPermission('volunteering:experience-user-interests:list')
      },
      {
        translate: 'VOLUNTEERING.MENU.ACTIONS_HISTORY',
        disable: false,
        routerLink: '/volunteering/actions-status-history',
        type: 'string',
        scope: this.authService.hasPermission('volunteering:experiences:my_offers')
      },
      {
        translate: 'VOLUNTEERING.MENU.COMPLAINS',
        disable: false,
        routerLink: '/volunteering/complains',
        type: 'string',
        scope: this.authService.hasPermission('volunteering:complain-user-interests:list')
      },
    ];
  }

  validTitleTraductions(id: number) {
    return this.data.find((t) => t.id === id) != null && this.data.find((t) => t.id === id) != undefined
      ? this.data.find((t) => t.id === id).translations
      : '';
  }
}
