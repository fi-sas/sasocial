import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BaseFormComponent } from './modules/volunteering-application/components/base-form/base-form.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SharedVolunteeringModule } from './modules/shared-volunteering/shared-volunteering.module';
import { VolunteeringComponent } from '@fi-sas/webpage/modules/volunteering/volunteering.component';
import { VolunteeringRoutingModule } from './volunteering-routing.module';

@NgModule({
  declarations: [VolunteeringComponent, BaseFormComponent],
  imports: [CommonModule, SharedModule, VolunteeringRoutingModule, SharedVolunteeringModule],
})
export class VolunteeringModule {}
