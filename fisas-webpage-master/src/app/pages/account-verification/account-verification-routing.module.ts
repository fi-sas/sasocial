import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountVerificationComponent } from './account-verification.component';

const routes: Routes = [
  {
    path: ':verification_token',
    component: AccountVerificationComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountVerificationRoutingModule { }
