import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { passwordRegex } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-account-verification',
  templateUrl: './account-verification.component.html',
  styleUrls: ['./account-verification.component.less']
})
export class AccountVerificationComponent implements OnInit {

  loading = false;
  success = false;
  showForm = true;

  verificationForm = new FormGroup({
    new_password:  new FormControl(null, [
      Validators.pattern(passwordRegex),
      Validators.required]),
    repeat_password:  new FormControl(null, [
        Validators.required]),
  },
  this.passwordConfirming,
  );

  verification_token: string = null;
  error_type: string = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
  ) {

  }

  passwordConfirming(c: AbstractControl): { passwordUnmatch: boolean } {
    if (c.get('new_password').value !== c.get('repeat_password').value) {
        return { passwordUnmatch: true};
    }
}

  getMatchError() {
    return this.verificationForm.get("repeat_password").errors && this.verificationForm.get("repeat_password").errors.required
    ? `FORM.MESSAGE.REQUIRED_FIELD`
    : this.verificationForm.errors && this.verificationForm.errors.passwordUnmatch
      ? `FORM.MESSAGE.PASSWORD_MATCH` : null;
  }

  getInputError(field: string) {
    return this.verificationForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.verificationForm.get(field).errors.minlength
        ? 'FORM.MESSAGE.MIN_LENGTH'
        : this.verificationForm.get(field).errors.maxlength
          ? 'FORM.MESSAGE.MAX_LENGTH'
          : this.verificationForm.get(field).errors.min
            ? 'FORM.MESSAGE.VALUES'
            : this.verificationForm.get(field).errors.pattern
              ? 'FORM.MESSAGE.PASSWORD_PATTERN'
              : null;
  }

  ngOnInit() {
    this.verification_token = this.activatedRoute.snapshot.params.verification_token;

  }

  verifyAccount(value: any, valid: boolean) {

    if(!valid)
      return;

    if(this.verification_token) {

      this.loading = true;
      this.authService.verifyAccountToken(this.verification_token, value.new_password)
      .pipe(
        first(),
        finalize(() => this.loading = false),
      ).subscribe(() => {
        this.success = true;
        this.showForm = false;
      }, err => {
        this.showForm = false;
        if(err.error && err.error.errors.length > 0) {
          this.error_type = err.error.errors[0].type;
        }
      })
    }
  }

}
