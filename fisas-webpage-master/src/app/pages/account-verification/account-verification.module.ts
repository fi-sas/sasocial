import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountVerificationRoutingModule } from './account-verification-routing.module';
import { AccountVerificationComponent } from './account-verification.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [AccountVerificationComponent],
  imports: [
    CommonModule,
    SharedModule,
    AccountVerificationRoutingModule
  ]
})
export class AccountVerificationModule { }
