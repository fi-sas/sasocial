import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { passwordRegex } from '@fi-sas/webpage/auth/models/user.model';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { finalize, first } from 'rxjs/operators';
import { compareTwoFieldsValidation } from "@fi-sas/webpage/shared/validators/validators.validator";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit {

  actualPasswordVisible = false;
  newPasswordVisible = false;
  repearPasswordVisible = false;

  disableButtonChangePassword = false;

  loading = false;

  changePasswordForm = new FormGroup({
    actual_password:  new FormControl(null, [
      Validators.required]),
    new_password:  new FormControl(null, [
      Validators.pattern(passwordRegex),
      Validators.required]),
    repeat_password:  new FormControl(null, [
        Validators.required]),
      },
      this.passwordConfirming,
      );

  passwordConfirming(c: AbstractControl): { passwordUnmatch: boolean } {
    if (c.get('new_password').value !== c.get('repeat_password').value) {
        return { passwordUnmatch: true};
    }
}

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {}

  getMatchError() {
    return this.changePasswordForm.get("repeat_password").errors && this.changePasswordForm.get("repeat_password").errors.required
    ? `FORM.MESSAGE.REQUIRED_FIELD`
    : this.changePasswordForm.errors && this.changePasswordForm.errors.passwordUnmatch
      ? `FORM.MESSAGE.PASSWORD_MATCH` : null;
  }

  getInputError(field: string) {
    return this.changePasswordForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.changePasswordForm.get(field).errors.minlength
        ? 'FORM.MESSAGE.MIN_LENGTH'
        : this.changePasswordForm.get(field).errors.maxlength
          ? 'FORM.MESSAGE.MAX_LENGTH'
          : this.changePasswordForm.get(field).errors.min
            ? 'FORM.MESSAGE.VALUES'
            : this.changePasswordForm.get(field).errors.pattern
              ? 'FORM.MESSAGE.PASSWORD_PATTERN'
              : null;


  }

  submit(formValue, valid) {
    if (!valid) {
      return;
    }

    delete formValue.repeat_password;

    this.loading = true;
    this.authService
    .changePassword(formValue)
    .pipe(first(), finalize(() => (this.loading = false)))
    .subscribe(
      () => {
        this.changePasswordForm.disable();
        this.disableButtonChangePassword = true;


          this.authService.logout().pipe(first()).subscribe(() => {
            this.router.navigate(['/', 'auth', 'login']);
          });
        },
        () => {
          this.loading = false;
        }
      );
  }

  cancel() {
    this.changePasswordForm.reset();
    this.router.navigate(['profile']);
  }
}
