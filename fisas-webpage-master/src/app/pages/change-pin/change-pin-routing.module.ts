import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePinComponent } from './change-pin.component';

const routes: Routes = [
  {
    path: '',
    component: ChangePinComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangePinRoutingModule { }
