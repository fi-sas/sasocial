import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { Router } from '@angular/router';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { fieldsMustBeEquals } from '@fi-sas/webpage/shared/validators/validators.validator';

@Component({
  selector: 'app-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: ['./change-pin.component.less']
})
export class ChangePinComponent implements OnInit {

  newPinVisible = false;
  actual_pin_visible = false;
  confirm_pin_visible = false;
  loadingChangePin = false;
  disableButtonChangePin = false;
  hiddenSendEmailConfirm = true;

  loading = false;

  changePinForm =


    this.fb.group({
      actual_pin: new FormControl(null, [
        Validators.required,
        Validators.pattern('^([0-9]*)$'),
        Validators.minLength(4),
        Validators.maxLength(4)
      ]),
      new_pin: new FormControl(null, [
        Validators.required,
        Validators.pattern('^([0-9]*)$'),
        Validators.minLength(4),
        Validators.maxLength(4)
      ]),
      repeat_new_pin: new FormControl(null, [
        Validators.required,
        Validators.pattern('^([0-9]*)$'),
        Validators.minLength(4),
        Validators.maxLength(4)
      ]),
    }, {
      validator:
        [
          fieldsMustBeEquals('new_pin', 'repeat_new_pin')]
    });
  v
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private uiService: UiService,
    private translateService: TranslateService
  ) { }

  ngOnInit() { }

  getInputError(field: string) {
    return this.changePinForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.changePinForm.get(field).errors.minlength
        ? 'FORM.MESSAGE.MIN_LENGTH'
        : this.changePinForm.get(field).errors.maxlength
          ? 'FORM.MESSAGE.MAX_LENGTH'
          : this.changePinForm.get(field).errors.min
            ? 'FORM.MESSAGE.VALUES'
            : this.changePinForm.get(field).errors.pattern
              ? 'FORM.MESSAGE.NUMERIC_ONLY'
              : this.changePinForm.get(field).errors.different
                ? 'CHANGE_PIN.DO_NOT_MATCH_ERROR'
                : null;
  }

  submit(formValue, valid) {
    if (!valid) {
      return;
    }

    this.loading = true;
    this.authService
      .changePin(formValue)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe(
        () => {

          this.uiService.showMessage(MessageType.success,
            this.translateService.instant('CHANGE_PIN.SUCCESS_CHANGE'));
          this.router.navigate(['profile']);
        },
        () => {
          this.loading = false;
        }
      );
  }

  cancel() {
    this.changePinForm.reset();
    this.router.navigate(['profile']);
  }

  sendPin(){
    this.loadingChangePin = true;
    this.authService
    .randomPin()
    .pipe(finalize(() => (this.loadingChangePin = false)))
    .subscribe(
      () => {
        this.uiService.showMessage(MessageType.success,
          this.translateService.instant('CHANGE_PIN.SUCCESS_RANDON_PIN'));
        this.router.navigate(['profile']);
      },
      () => {
        this.loading = false;
      }
    );
  }
}
