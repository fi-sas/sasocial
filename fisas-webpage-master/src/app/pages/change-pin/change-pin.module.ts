import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePinRoutingModule } from './change-pin-routing.module';
import { ChangePinComponent } from './change-pin.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [
    ChangePinComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ChangePinRoutingModule
  ]
})
export class ChangePinModule { }
