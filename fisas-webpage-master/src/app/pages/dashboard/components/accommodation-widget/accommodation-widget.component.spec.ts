import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AccommodationWidgetComponent } from './accommodation-widget.component';


describe('AccommodationWidgetComponent', () => {
  let component: AccommodationWidgetComponent;
  let fixture: ComponentFixture<AccommodationWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccommodationWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccommodationWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
