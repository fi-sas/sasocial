import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { NzCarouselComponent } from 'ng-zorro-antd';
import { WidgetsModel } from '../../models/widgets.model';


@Component({
  selector: 'app-accommodation-widget',
  templateUrl: './accommodation-widget.component.html',
  styleUrls: ['./accommodation-widget.component.less']
})

export class AccommodationWidgetComponent implements OnInit {

  @Input() custumizeToggle;
  @Input() listProperties = new WidgetsModel;
  @Input() translations: TranslationModel[] = [];;

  constructor() {
  }

  ngOnInit() {
  }

  arrowClick($event: MouseEvent, type: string, carourel:NzCarouselComponent) {
    $event.stopPropagation();
    if(type == 'next') {
      carourel.next();
    }else {
      carourel.pre();
    }
  }
}
