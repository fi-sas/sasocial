import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsMenuWidgetComponent } from './buttons-menu-widget.component';

describe('ButtonsMenuWidgetComponent', () => {
  let component: ButtonsMenuWidgetComponent;
  let fixture: ComponentFixture<ButtonsMenuWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonsMenuWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsMenuWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
