
import { Router } from '@angular/router';
import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, Renderer2, AfterViewInit, AfterContentInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { SERVICE_IDS } from '@fi-sas/webpage/shared/models';
import { WidgetsModel } from '../../models/widgets.model';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';


export class DashboardModule {
  name: string;
  link?: string;
  icon?: string;
  active: boolean;
  external: boolean;

  constructor(
    name: string,
    link?: string,
    icon?: string,
    active = false,
    external = false
  ) {
    this.name = name;
    this.link = link;
    this.icon = icon;
    this.active = active;
    this.external = external;
  }
}

@Component({
  selector: 'app-buttons-menu-widget',
  templateUrl: './buttons-menu-widget.component.html',
  styleUrls: ['./buttons-menu-widget.component.less'],
  host: {
    '(window:resize)': 'onResize()'
  }
})

export class ButtonsMenuWidgetComponent implements OnInit, AfterViewInit  {
  @Input() btns = new WidgetsModel;
  @Input() custumizeToggle;
  @Input() active: boolean;
  @ViewChild('service_widgets') service_widgets: ElementRef;

  readonly serviceId = SERVICE_IDS.GERAL;
  externalServices = [];
  externalServicesConfig = [];
  modules: DashboardModule[] = [];
  dataConfiguration = [];
  languages: any;

  services = [];

  constructor(private router: Router, private authService: AuthService, public translate: TranslateService, private config: FiConfigurator, private renderer: Renderer2) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  ngAfterViewInit(): void {
    const widgetTimeout = setTimeout(() => {
      const elements = this.service_widgets ? this.service_widgets.nativeElement.children : null ;
      if(elements && elements.length){
        clearTimeout(widgetTimeout);
        this.onResize();
      }
    }, 50)
  }

  ngOnInit() {
    this.dataConfiguration = this.btns.data;
    this.externalServicesConfig = this.dataConfiguration.filter(
      (data) => data.is_external_service == true
    );
    this.services = [
      new DashboardModule(
        this.validTitleTraductions(1),
        "/accommodation",
        "icons:icons-accomodation",
        this.validActive(1, "accommodation")
      ),
      new DashboardModule(
        this.validTitleTraductions(15),
        "/privateaccommodation",
        "icons:icons-pin-a",
        this.validActive(15, "private_accommodation")
      ),
      new DashboardModule(
        this.validTitleTraductions(2),
        "/alimentation",
        "icons:icons-food",
        this.validActive(2, "alimentation")
      ),
      new DashboardModule(
        this.validTitleTraductions(11),
        "/current-account",
        "icons:walltet_v3",
        this.validActive(11, "current_account")
      ),
      new DashboardModule(
        this.validTitleTraductions(13),
        "/u-bike",
        "icons:icons-ubike",
        this.validActive(13, "u_bike")
      ),
      new DashboardModule(
        this.validTitleTraductions(17),
        "/social-support",
        "icons:icons-scholarship",
        this.validActive(17, "social_scholarship")
      ),
      new DashboardModule(
        this.validTitleTraductions(28),
        "/volunteering",
        "icons:icons-voluntariado_v3",
        this.validActive(28, "volunteering")
      ),
      new DashboardModule(
        this.validTitleTraductions(3),
        "/mobility",
        "icons:icons-mobility",
        this.validActive(3, "bus")
      ),
      new DashboardModule(
        this.validTitleTraductions(21),
        "/queue",
        "icons:icons-ticket",
        this.validActive(21, "queue")
      ),
      new DashboardModule(
        this.validTitleTraductions(7),
        "/health",
        "icons:icons-health",
        this.validActive(7, "health")
      ),
      new DashboardModule(
        this.validTitleTraductions(18),
        "/calendar",
        "icons:icons-calendar",
        this.validActive(18, "calendar")
      ),
    ];
    this.services.forEach((module) => {
      if (module.active) {
        this.modules.push(module);
      }
    });

    this.externalServicesConfig.forEach((external) =>
      this.externalServices.push({
        name: external.translations,
        link: external.external_link,
        icon: external.icon ? "icons:" + external.icon : "icons:icons-external",
        active: true,
        external: true,
      })
    );

    this.externalServices.sort((a, b) => {
      const nameA = a.name.find((nA) => nA.language_id === this.getCurrentLanguage()) ? a.name.find((nA) => nA.language_id === this.getCurrentLanguage()).description: null;
      const nameB = b.name.find((nB) => nB.language_id === this.getCurrentLanguage()) ? b.name.find((nB) => nB.language_id === this.getCurrentLanguage()).description: null;
      return nameA && nameB ? nameA > nameB ? 1 : -1 : -1;
    });
    this.externalServices.forEach((external) => {
      this.modules.push(external);
    });
  }

  onResize(){
    const elements = this.service_widgets ? this.service_widgets.nativeElement.children : null ;
    if(elements && elements.length){
      const arrayElements = Array.from(elements)
      arrayElements.map((e: HTMLElement) => this.renderer.removeStyle(e, 'height'));
      const maxHeigh = Math.max.apply(Math, arrayElements.map((e: HTMLElement) =>  e.offsetHeight ));
      arrayElements.map((e:HTMLElement) => this.renderer.setStyle(e, 'height', `${maxHeigh}px`));
    }

  }

  selectModule(module) {
    if (module.external === false) {
      this.router.navigate([module.link]);
    } else {
      window.open(
        module.link.includes("://") ? module.link : "http://" + module.link,
        "_blank"
      );
    }
  }

  validActive(id: number, scope: string) {
    let validScopt = this.authService.hasPermission(scope);
    if (this.dataConfiguration) {
      let validActiveConfig = this.dataConfiguration.find((t) => t.id === id)
        ? this.dataConfiguration.find((t) => t.id === id).active
        : false;
      return validScopt && validActiveConfig;
    }
    return false;
  }

  validTitleTraductions(id: number) {
    if (this.dataConfiguration) {
      return this.dataConfiguration.find((t) => t.id === id)
        ? this.dataConfiguration.find((t) => t.id === id).translations
        : [];
    }
    return [];
  }

  getCurrentLanguage() {
    const currentLang = this.languages.find(
      (lang) => lang.acronym === this.translate.currentLang
    );
    return currentLang.id;
  }
}
