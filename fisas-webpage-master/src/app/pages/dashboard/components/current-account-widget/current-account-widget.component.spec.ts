import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CurrentAccountWidgetComponent } from './current-account-widget.component';

describe('CurrentAccountWidgetComponent', () => {
  let component: CurrentAccountWidgetComponent;
  let fixture: ComponentFixture<CurrentAccountWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentAccountWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentAccountWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
