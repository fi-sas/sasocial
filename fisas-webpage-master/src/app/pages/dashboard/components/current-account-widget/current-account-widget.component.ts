import { Component, Input, OnInit } from "@angular/core";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { WidgetsModel } from "../../models/widgets.model";

@Component({
    selector: 'app-current-account-widget',
    templateUrl: './current-account-widget.component.html',
    styleUrls: ['./current-account-widget.component.less']
})
export class CurrentAccountWidgetComponent implements OnInit {
    @Input() custumizeToggle;
    @Input() movementsBalance = new WidgetsModel;
    @Input() translations: TranslationModel[] = [];
    constructor() { }

    ngOnInit() {
      
    }

}  