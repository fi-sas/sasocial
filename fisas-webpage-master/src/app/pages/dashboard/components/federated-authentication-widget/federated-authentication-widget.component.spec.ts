import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FederatedAuthenticationWidgetComponent } from './federated-authentication-widget.component';

describe('FederatedAuthenticationWidgetComponent', () => {
  let component: FederatedAuthenticationWidgetComponent;
  let fixture: ComponentFixture<FederatedAuthenticationWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FederatedAuthenticationWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FederatedAuthenticationWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
