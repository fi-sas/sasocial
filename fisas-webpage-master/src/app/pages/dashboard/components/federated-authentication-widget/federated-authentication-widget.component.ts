import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-federated-authentication-widget',
  templateUrl: './federated-authentication-widget.component.html',
  styleUrls: ['./federated-authentication-widget.component.less']
})
export class FederatedAuthenticationWidgetComponent implements OnInit {

  ssoEnabled = environment.hasSSO;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goAuthentication() {
    this.router.navigateByUrl('/auth/login');
  }

}
