import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthWidgetComponent } from './health-widget.component';

describe('HealthWidgetComponent', () => {
  let component: HealthWidgetComponent;
  let fixture: ComponentFixture<HealthWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
