import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MessageType, UiService } from "@fi-sas/webpage/core/services/ui.service";
import { ConfigStatus } from "@fi-sas/webpage/modules/health/models/appointment.model";
import { AppointmentsService } from "@fi-sas/webpage/modules/health/services/appointments.service";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { TranslateService } from "@ngx-translate/core";
import { WidgetsModel } from "../../models/widgets.model";
import * as moment from 'moment';

@Component({
  selector: 'app-health-widget',
  templateUrl: './health-widget.component.html',
  styleUrls: ['./health-widget.component.less']
})
export class HealthWidgetComponent implements OnInit {

  @Input() custumizeToggle;
  @Input() appointments = new WidgetsModel;
  @Input() translations: TranslationModel[] = [];
  readonly configStatus = ConfigStatus;
  
  constructor(
    private router: Router,
    private appointmentsService: AppointmentsService,
    private uiService: UiService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
  }

  goDetailAppoitment(id: number) {
      this.router.navigateByUrl('health/appointments/detail/' + id);
  }

  getCalendarExport(id: number){
    this.appointmentsService.getAppointmentById(id).subscribe(responseAppointment => {
      const appointment = responseAppointment.data[0];
      if(appointment){
        let filename = appointment.specialty.translations[0].name + "_" + appointment.name + "_" + moment(appointment.date).format("YYYY-MM-DD") + "_"+ moment(appointment.start_hour, "HH:mm:ss") +".ics";
        this.appointmentsService.generateCalendarExport(id, filename).subscribe(()=>{
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('HEALTH.APPOINTMENTS_TAB.CALENDAR.MESSAGE_OK')
        );
      })
    };
  });
}

}
