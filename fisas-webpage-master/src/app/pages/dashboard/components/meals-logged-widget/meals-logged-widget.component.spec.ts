import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MealsLoggedWidgetComponent } from './meals-logged-widget.component';


describe('MealsLoggedWidgetComponent', () => {
  let component: MealsLoggedWidgetComponent;
  let fixture: ComponentFixture<MealsLoggedWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealsLoggedWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsLoggedWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
