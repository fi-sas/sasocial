import { Component, Input, OnInit } from "@angular/core";
import * as moment from 'moment';
import { Router } from "@angular/router";
import { WidgetsModel } from "../../models/widgets.model";

@Component({
    selector: 'app-meals-logged-widget',
    templateUrl: './meals-logged-widget.component.html',
    styleUrls: ['./meals-logged-widget.component.less']
})

export class MealsLoggedWidgetComponent implements OnInit {

    @Input() custumizeToggle;
    @Input() meals = new WidgetsModel;
    date = moment(new Date()).format("YYYY-MM-DD");

    constructor(private router: Router) {

    }

    ngOnInit() {
    }

    goDetail(idReservation, idMenuDish, meal) {
        let date = moment(new Date()).format("YYYY-MM-DD");
        this.router.navigateByUrl('/alimentation/menu/' + idReservation + '/' + idMenuDish + '/' + meal + '/' + date);
    }


}  