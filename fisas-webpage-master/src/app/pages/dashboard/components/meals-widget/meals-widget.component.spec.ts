import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MealsWidgetComponent } from './meals-widget.component';


describe('MealsWidgetComponent', () => {
  let component: MealsWidgetComponent;
  let fixture: ComponentFixture<MealsWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealsWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
