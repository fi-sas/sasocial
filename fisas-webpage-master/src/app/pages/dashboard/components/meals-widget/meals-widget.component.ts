import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as moment from 'moment';
import { WidgetsModel } from "../../models/widgets.model";

@Component({
    selector: 'app-meals-widget',
    templateUrl: './meals-widget.component.html',
    styleUrls: ['./meals-widget.component.less']
})

export class MealsWidgetComponent implements OnInit {

    @Input() custumizeToggle;
    @Input() services = new WidgetsModel;
    meals = [];
    serviceSelected = 1;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.serviceSelected = this.services.data[0].id;
        this.meals = this.services.data[0].menus;
    }

    goDetail(idReservation, idMenuDish, meal) {
        let date = moment(new Date()).format("YYYY-MM-DD");
        this.router.navigateByUrl('/alimentation/menu/' + idReservation + '/' + idMenuDish + '/' + meal + '/' + date);
    }

    selectService(id, menus) {
        this.serviceSelected = id;
        this.meals = menus;
    }
}  