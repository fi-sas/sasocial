import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MobilityWidgetComponent } from './mobility-widget.component';

describe('MobilityWidgetComponent', () => {
  let component: MobilityWidgetComponent;
  let fixture: ComponentFixture<MobilityWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobilityWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilityWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
