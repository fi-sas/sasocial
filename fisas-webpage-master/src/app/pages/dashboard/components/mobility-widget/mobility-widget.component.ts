import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { WidgetsModel } from "../../models/widgets.model";

@Component({
    selector: 'app-mobility-widget',
    templateUrl: './mobility-widget.component.html',
    styleUrls: ['./mobility-widget.component.less']
})

export class MobilityWidgetComponent implements OnInit {

    @Input() custumizeToggle;
    @Input() mobility = new WidgetsModel;
    @Input() translations: TranslationModel[] = [];

    constructor(private router: Router) {
       
    }

    ngOnInit() {
    }
}  