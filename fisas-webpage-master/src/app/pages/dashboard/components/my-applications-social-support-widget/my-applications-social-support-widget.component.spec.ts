import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MyApplicationsSocialSupportWidgetComponent } from './my-applications-social-support-widget.component';

describe('MyApplicationsSocialSupportWidgetComponent', () => {
  let component: MyApplicationsSocialSupportWidgetComponent;
  let fixture: ComponentFixture<MyApplicationsSocialSupportWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyApplicationsSocialSupportWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyApplicationsSocialSupportWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
