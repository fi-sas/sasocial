import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { UiService, MessageType } from "@fi-sas/webpage/core/services/ui.service";
import { ChangeStatusModel } from "@fi-sas/webpage/modules/social-support/models/change-status.model";
import { ChangeStatusService } from "@fi-sas/webpage/modules/social-support/services/change-status.service";
import { ServiceConfigurationModel } from "@fi-sas/webpage/shared/models/service-configurations.model";
import { TranslateService } from "@ngx-translate/core";
import { first } from "rxjs/operators";
import { WidgetsModel } from "../../models/widgets.model";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services";
@Component({
    selector: 'app-my-applications-social-support-widget',
    templateUrl: './my-applications-social-support-widget.component.html',
    styleUrls: ['./my-applications-social-support-widget.component.less']
})

export class MyApplicationsSocialSupportWidgetComponent implements OnInit {
    @Input() custumizeToggle;
    @Input() myApplicationsSocialSupport = new WidgetsModel;
    @Output() successWithdrawal = new EventEmitter;
    translations: TranslationModel[] = [];
    idApplication;
    visibleModal = false;
    formWithdrawal: FormGroup;

    status = {
        SUBMITTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.SUBMITTED',
        ANALYSED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ANALYSED',
        INTERVIEWED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.INTERVIEWED',
        ACCEPTED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.ACCEPTED',
        DECLINED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.DECLINED',
        CANCELLED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.CANCELLED',
        EXPIRED: 'SOCIAL_SUPPORT.APPLICATIONS.STATUS.EXPIRED',
    };

    constructor(
        private changeStatusService: ChangeStatusService, private uiService: UiService,
        private translateService: TranslateService, private formBuilder: FormBuilder,
        private configurationsService:ConfigurationsService,
        private router: Router) { }

    ngOnInit() {
        this.formWithdrawal = this.formBuilder.group({
            reason: [],
        })
        this.getTranslationsTitle();
    }

    getTranslationsTitle() {
        let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
        this.translations = service.translations;
    }

    withdrawal(id): void {
        this.idApplication = id;
        this.visibleModal = true;
    }

    checkTagStatusColor(application) {
        const css = {};
        css[application.status] = true;
        return css;
    }

    submitWithdrawal() {
        const withdrawal = new ChangeStatusModel();
        withdrawal.notes = this.formWithdrawal.get('reason').value;
        this.changeStatusService
            .applicationWithdraw(this.idApplication, withdrawal)
            .pipe(first())
            .subscribe(
                () => {
                    this.visibleModal = false;
                    this.formWithdrawal.reset();
                    this.uiService.showMessage(
                        MessageType.success,
                        this.translateService.instant('DASHBOARD.WIDGETS.MY_APPLICATIONS_SOCIAL_SUPPORT.MODAL_WITHDRAWAL.SUCCESS'));
                    this.successWithdrawal.emit(true);
                });
    }

    reviewApplication(idApplication: number) {
        this.router.navigateByUrl("social-support/applications/review/" + idApplication);
    }
}