import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { WidgetsModel } from "../../models/widgets.model";


@Component({
    selector: 'app-posts-widget',
    templateUrl: './posts-widget.component.html',
    styleUrls: ['./posts-widget.component.less']
})

export class PostsWidgetComponent implements OnInit {

    @Input() custumizeToggle;
    @Input() posts = new WidgetsModel;

    constructor(private router: Router) {

    }

    ngOnInit() {

    }

    goDetail(id: number) {
        this.router.navigateByUrl('/news/detail/'+ id);
    }

    existVideo(data) {
        let aux = false;
        data.translations.map(control => {
            if ((control.file_id_1_1 && control.file_id_1_1.type == 'VIDEO') || (control.file_9_16 && control.file_9_16.type == 'VIDEO') ||
                (control.file_16_9 && control.file_16_9.type == 'VIDEO')) {
                aux = true;
            }
        });
        if (aux) {
           return true;
        } 
        return false;
    }
}  