import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QueueWidgetComponent } from './queue-widget.component';


describe('QueueWidgetComponent', () => {
  let component: QueueWidgetComponent;
  let fixture: ComponentFixture<QueueWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
