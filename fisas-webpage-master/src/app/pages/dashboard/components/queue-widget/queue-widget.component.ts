import { Component, Input, OnInit } from "@angular/core";
import { WidgetsModel } from "../../models/widgets.model";


@Component({
    selector: 'app-queue-widget',
    templateUrl: './queue-widget.component.html',
    styleUrls: ['./queue-widget.component.less']
})

export class QueueWidgetComponent implements OnInit {

    @Input() custumizeToggle;
    @Input() tickets = new WidgetsModel;

    constructor() {

    }

    ngOnInit() {

    }
}  