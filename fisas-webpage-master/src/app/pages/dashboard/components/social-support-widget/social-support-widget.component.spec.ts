import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SocialSupportWidgetComponent } from './social-support-widget.component';


describe('SocialSupportWidgetComponent', () => {
  let component: SocialSupportWidgetComponent;
  let fixture: ComponentFixture<SocialSupportWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialSupportWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialSupportWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
