import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceConfigurationModel } from "@fi-sas/webpage/shared/models/service-configurations.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { WidgetsModel } from "../../models/widgets.model";

@Component({
    selector: 'app-social-support-widget',
    templateUrl: './social-support-widget.component.html',
    styleUrls: ['./social-support-widget.component.less']
})

export class SocialSupportWidgetComponent implements OnInit {
    @Input() custumizeToggle;
    @Input() experienceSocialSupport = new WidgetsModel;
    translations: TranslationModel[] = [];

    constructor(private router: Router,
        private configurationsService: ConfigurationsService) {

    }

    ngOnInit() {
        this.getTranslationsTitle();
    }

    goDetailExperience(id: number) {
        this.router.navigateByUrl('social-support/experiences/' + id);
    }

    getTranslationsTitle() {
        let service: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(17);
        this.translations = service.translations;
    }
}  