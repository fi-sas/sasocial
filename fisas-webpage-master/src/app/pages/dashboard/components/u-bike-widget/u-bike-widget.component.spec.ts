import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UBikeWidgetComponent } from './u-bike-widget.component';

describe('UBikeWidgetComponent', () => {
  let component: UBikeWidgetComponent;
  let fixture: ComponentFixture<UBikeWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UBikeWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UBikeWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
