import { Component, Input, OnInit } from '@angular/core';
import { TranslationModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { WidgetsModel } from '../../models/widgets.model';

@Component({
  selector: 'app-u-bike-widget',
  templateUrl: './u-bike-widget.component.html',
  styleUrls: ['./u-bike-widget.component.less']
})
export class UBikeWidgetComponent implements OnInit {
  @Input() translations: TranslationModel[] = [];
  @Input() custumizeToggle;
  @Input() ubike = new WidgetsModel;

  constructor() {
   
  }

  ngOnInit() {
  
  }

}
