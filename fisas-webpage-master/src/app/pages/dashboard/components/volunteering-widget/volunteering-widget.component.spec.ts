import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VolunteeringWidgetComponent } from './volunteering-widget.component';


describe('VolunteeringWidgetComponent', () => {
  let component: VolunteeringWidgetComponent;
  let fixture: ComponentFixture<VolunteeringWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolunteeringWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteeringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
