import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { WidgetsModel } from "../../models/widgets.model";


@Component({
    selector: 'app-volunteering-widget',
    templateUrl: './volunteering-widget.component.html',
    styleUrls: ['./volunteering-widget.component.less']
})

export class VolunteeringWidgetComponent implements OnInit {
    @Input() custumizeToggle;
    @Input() experienceVolunteering = new WidgetsModel;

    constructor(
        private router: Router) { }

    ngOnInit() {
    }

    goDetailExperience(id: number) {
        this.router.navigateByUrl('volunteering/experiences/' + id);
    }
}  