import { Component, OnInit, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag } from '@angular/cdk/drag-drop';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { WidgetsModel } from './models/widgets.model';
import { WidgetsService } from './services/widgets.service';
import { finalize, first } from 'rxjs/operators';
import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";
import { ServiceConfigurationModel } from "@fi-sas/webpage/shared/models/service-configurations.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {


  logged: boolean;
  custumizeToggle = false;
  loadingPage = true;
  widgets: WidgetsModel[] = [];
  appArrayWidgets: WidgetsModel[] = [];
  translationsMobility: TranslationModel[] = [];
  translationsUbike: TranslationModel[] = [];
  translationsCA: TranslationModel[] = [];
  translationsAccommo: TranslationModel[] = [];
  translationsHealth: TranslationModel[] = [];
  //widgets
  accommodation = new WidgetsModel;
  buttons = new WidgetsModel;
  currentAccount = new WidgetsModel;
  ubike = new WidgetsModel;
  volunteering = new WidgetsModel;
  queue = new WidgetsModel;
  experienceSocialSupport = new WidgetsModel;
  applicationSocialSupport = new WidgetsModel;
  mobility = new WidgetsModel;
  meals = new WidgetsModel;
  posts = new WidgetsModel;
  btns = new WidgetsModel;
  health = new WidgetsModel;


  constructor(private authService: AuthService, private widgetsService: WidgetsService, private configurationsService:ConfigurationsService) {
    this.logged = this.authService.getIsLogged();
  }

  ngOnInit() {
    this.getTranslations();
    this.getWidgets();
  }

  getTranslations() {
    let serviceMobility: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(3);
    this.translationsMobility = serviceMobility.translations;
    let serviceUbike: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(13);
    this.translationsUbike = serviceUbike.translations;
    let serviceCA: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(11);
    this.translationsCA = serviceCA.translations;
    let serviceAccomo: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(15);
    this.translationsAccommo = serviceAccomo.translations;
    let serviceHealth: ServiceConfigurationModel = this.configurationsService.getLocalStorageService(7);
    this.translationsHealth = serviceHealth.translations;

}

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  getWidgets() {
    this.widgetsService.list().pipe(first(), finalize(() => this.loadingPage = false)).subscribe((data) => {
      this.widgets = data.data;
      this.appArrayWidgets = data.data.sort((a,b)=> a.position-b.position);
      this.accommodation = this.widgets.filter((fil) => { return fil.widget.type == 'accommodation-applications-widget'})[0];
      this.buttons = this.widgets.filter((fil) => { return fil.widget.type == 'buttons-widget'})[0];
      this.currentAccount = this.widgets.filter((fil) => { return fil.widget.type == 'current-account-widget'})[0];
      this.ubike = this.widgets.filter((fil) => { return fil.widget.type == 'ubike-widget'})[0];
      this.volunteering = this.widgets.filter((fil) => { return fil.widget.type == 'volunteering-widget'})[0];
      this.queue = this.widgets.filter((fil) => { return fil.widget.type == 'queue-widget'})[0];
      this.experienceSocialSupport = this.widgets.filter((fil) => { return fil.widget.type == 'experiences-social-scholarship-widget'})[0];
      this.applicationSocialSupport = this.widgets.filter((fil) => { return fil.widget.type == 'applications-social-scholarship-widget'})[0];
      this.mobility = this.widgets.filter((fil) => { return fil.widget.type == 'mobility-widget'})[0];
      this.meals = this.widgets.filter((fil) => { return fil.widget.type == 'meals-widget'})[0];
      this.posts = this.widgets.filter((fil) => { return fil.widget.type == 'posts-widget'})[0];
      this.btns = this.widgets.filter((fil) => { return fil.widget.type == 'buttons-widget'})[0];
      this.health = this.widgets.filter((fil) => { return fil.widget.type == 'health-widget'})[0];
    })
  }

  shownActiveCustomize(array): boolean {
    return array != undefined && array.active == false && this.custumizeToggle;
  }

  shownActive(array): boolean{
    return array != undefined && array.active == true;
  }

  successWithdrawal(event: boolean){
    if(event == true) {
      this.getWidgets();
    }
  }

  saveChanges() {
    this.loadingPage = true;
    for(let i=0; i<this.appArrayWidgets.length; i++) {
      this.appArrayWidgets[i].position = i + 1;
      this.appArrayWidgets[i].data = null;
    }
    let auxData: any = {};
    auxData.widgets = this.appArrayWidgets;
    this.widgetsService.patch(auxData).pipe(finalize(()=>this.loadingPage = false)).subscribe((data)=> {
      this.widgets = data.data;
      this.appArrayWidgets = data.data;
      this.accommodation = this.widgets.filter((fil) => { return fil.widget.type == 'accommodation-applications-widget'})[0];
      this.buttons = this.widgets.filter((fil) => { return fil.widget.type == 'buttons-widget'})[0];
      this.currentAccount = this.widgets.filter((fil) => { return fil.widget.type == 'current-account-widget'})[0];
      this.ubike = this.widgets.filter((fil) => { return fil.widget.type == 'ubike-widget'})[0];
      this.volunteering = this.widgets.filter((fil) => { return fil.widget.type == 'volunteering-widget'})[0];
      this.queue = this.widgets.filter((fil) => { return fil.widget.type == 'queue-widget'})[0];
      this.experienceSocialSupport = this.widgets.filter((fil) => { return fil.widget.type == 'experiences-social-scholarship-widget'})[0];
      this.applicationSocialSupport = this.widgets.filter((fil) => { return fil.widget.type == 'applications-social-scholarship-widget'})[0];
      this.mobility = this.widgets.filter((fil) => { return fil.widget.type == 'mobility-widget'})[0];
      this.meals = this.widgets.filter((fil) => { return fil.widget.type == 'meals-widget'})[0];
      this.posts = this.widgets.filter((fil) => { return fil.widget.type == 'posts-widget'})[0];
      this.health = this.widgets.filter((fil) => { return fil.widget.type == 'health-widget'})[0];
      this.custumizeToggle = false;
      this.scrollTop(); 

    })
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }

}


