import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ​​DragDropModule }​​ from '@angular/cdk/drag-drop';
import { ButtonsMenuWidgetComponent } from './components/buttons-menu-widget/buttons-menu-widget.component';
import { UBikeWidgetComponent } from './components/u-bike-widget/u-bike-widget.component';
import { FederatedAuthenticationWidgetComponent } from './components/federated-authentication-widget/federated-authentication-widget.component';
import { AccommodationWidgetComponent } from './components/accommodation-widget/accommodation-widget.component';
import { CurrentAccountWidgetComponent } from './components/current-account-widget/current-account-widget.component';
import { QueueWidgetComponent } from './components/queue-widget/queue-widget.component';
import { VolunteeringWidgetComponent } from './components/volunteering-widget/volunteering-widget.component';
import { SocialSupportWidgetComponent } from './components/social-support-widget/social-support-widget.component';
import { MealsLoggedWidgetComponent } from './components/meals-logged-widget/meals-logged-widget.component';
import { MyApplicationsSocialSupportWidgetComponent } from './components/my-applications-social-support-widget/my-applications-social-support-widget.component';
import { MealsWidgetComponent } from './components/meals-widget/meals-widget.component';
import { MobilityWidgetComponent } from './components/mobility-widget/mobility-widget.component';
import { PostsWidgetComponent } from './components/posts-widget/posts-widget.component';
import { MovementsModule } from '@fi-sas/webpage/modules/current-account/pages/movements/movements.module';
import { HealthWidgetComponent } from './components/health-widget/health-widget.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ButtonsMenuWidgetComponent,
    MobilityWidgetComponent,
    UBikeWidgetComponent,
    FederatedAuthenticationWidgetComponent,
    AccommodationWidgetComponent,
    CurrentAccountWidgetComponent,
    QueueWidgetComponent,
    VolunteeringWidgetComponent,
    SocialSupportWidgetComponent,
    MyApplicationsSocialSupportWidgetComponent,
    MealsLoggedWidgetComponent,
    MealsWidgetComponent,
    PostsWidgetComponent,
    HealthWidgetComponent
  ],
  imports: [
    ​​DragDropModule,
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    MovementsModule
  ]
})
export class DashboardModule { }
