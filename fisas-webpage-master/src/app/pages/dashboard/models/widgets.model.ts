export class WidgetsModel {
    widget_id: number;
    user_id: number;
    active: boolean;
    position: number;
    widget: WidgetModel;
    data: any[] = [];
}

export class WidgetModel {
    id: number;
    type: string;
    call_path: string;
    active: boolean;
    position: number;
    service_id: number;
    loginrequired: boolean;
}