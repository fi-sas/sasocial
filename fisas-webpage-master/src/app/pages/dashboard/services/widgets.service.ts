import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { WidgetsModel } from '../models/widgets.model';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WidgetsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  list():
    Observable<Resource<WidgetsModel>> {

    return this.resourceService.list<WidgetsModel>(this.urlService.get('CONFIGURATION.DASHBOARD'));
  }

  patch(data: any): Observable<Resource<WidgetsModel>> {
    return this.resourceService.patch<WidgetsModel>(this.urlService.get('CONFIGURATION.DASHBOARD'), data).pipe(first());
  }
}
