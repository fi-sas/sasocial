import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisableAccountComponent } from './disable-account.component';

const routes: Routes = [
  {
    path: '',
    component: DisableAccountComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisableAccountRoutingModule { }
