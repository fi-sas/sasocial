import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { finalize, first } from 'rxjs/operators';

@Component({
  selector: 'app-disable-account',
  templateUrl: './disable-account.component.html',
  styleUrls: ['./disable-account.component.less']
})
export class DisableAccountComponent implements OnInit {

  passwordVisible = false;

  disableAccountForm: FormGroup;
  isLoading = false;

  constructor(private authService: AuthService,
              private router: Router,
              private translateService: TranslateService,
              private uiService: UiService
  ) {
    this.disableAccountForm = new FormGroup({
      password: new FormControl('', [Validators.required,])
    });

  }

  ngOnInit() {}

  getInputError(field: string) {
    return this.disableAccountForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.disableAccountForm.get(field).errors.minlength
        ? 'FORM.MESSAGE.MIN_LENGTH'
        : this.disableAccountForm.get(field).errors.maxlength
          ? 'FORM.MESSAGE.MAX_LENGTH'
          : this.disableAccountForm.get(field).errors.min
            ? 'FORM.MESSAGE.VALUES'
            : this.disableAccountForm.get(field).errors.pattern
              ? 'FORM.MESSAGE.NUMERIC_ONLY'
              : null;
  }

  disableAccount(formValue, valid) {

    if (!valid) {
      return;
    }

    this.isLoading = true;
    this.authService
      .userDisableAccount(formValue.password)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        () => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('DISABLE_ACCOUNT.ACCOUNT_DISABLED')
          );
          this.authService.logout().pipe(first()).subscribe();
          this.router.navigate(['/', 'auth', 'login']);
        },
        () => {
          this.isLoading = false;
        }
      );
  }
}
