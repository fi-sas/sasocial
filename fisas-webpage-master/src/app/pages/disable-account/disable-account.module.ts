import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisableAccountRoutingModule } from './disable-account-routing.module';
import { DisableAccountComponent } from './disable-account.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [DisableAccountComponent],
  imports: [
    CommonModule,
    SharedModule,
    DisableAccountRoutingModule
  ]
})
export class DisableAccountModule { }
