import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../auth/services/auth.service';
import { UserModel } from '../../auth/models/user.model';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-first-login',
  templateUrl: './first-login.component.html',
  styleUrls: ['./first-login.component.less']
})
export class FirstLoginComponent implements OnInit {
  user: UserModel = null;

  passwordVisible = false;
  pinVisible = false;

  loading = false;

  firstLoginForm = new FormGroup({
    // password: new FormControl(null, [
    //   Validators.required,
    //   Validators.minLength(8)
    // ]),
    pin: new FormControl(null, [
      Validators.required,
      Validators.pattern('^([0-9][0-9]*)$'),
      Validators.minLength(4),
      Validators.maxLength(4)
    ])
  });

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  getInputError(field: string) {
    return this.firstLoginForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.firstLoginForm.get(field).errors.minlength
      ? 'FORM.MESSAGE.MIN_LENGTH'
      : this.firstLoginForm.get(field).errors.maxlength
      ? 'FORM.MESSAGE.MAX_LENGTH'
      : this.firstLoginForm.get(field).errors.min
      ? 'FORM.MESSAGE.VALUES'
      : this.firstLoginForm.get(field).errors.pattern
      ? 'FORM.MESSAGE.NUMERIC_ONLY'
      : null;
  }

  submit(formValue, valid) {
    if (!valid) {
      return;
    }

    this.loading = true;
    this.authService
      .firstLogin(formValue)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe(
        data => {
          this.authService.setUser(data.data[0]);
          this.router.navigate(['personal-data']);
        },
        () => {

        }
      );
  }
}
