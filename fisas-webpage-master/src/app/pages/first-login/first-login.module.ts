import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstLoginRoutingModule } from './first-login-routing.module';
import { FirstLoginComponent } from './first-login.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [
    FirstLoginComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FirstLoginRoutingModule
  ]
})
export class FirstLoginModule { }
