import { hasOwnProperty } from "tslint/lib/utils";
import { AuthService } from "./../../auth/services/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { environment } from "src/environments/environment";
import { first } from "rxjs/operators";
import { CCSamlRequest } from "@fi-sas/webpage/auth/models/cc-saml-request.model";

enum LoginStep {
  EMAIL,
  PASSWORD,
  SSO,
}

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.less"],
})
export class LoginComponent implements OnInit {
  ssoEnabled = environment.hasSSO;

  ccLoginEnabled = environment.hasCC;
  ccSamlRequest: CCSamlRequest = null;

  @ViewChild("passwordRef") passwordRef: ElementRef;

  LoginStep = LoginStep;

  currentLoginStep: LoginStep = LoginStep.EMAIL;

  passwordVisible = false;
  loginForm: FormGroup;
  isLoading = false;

  // EMAIL ERROR
  showEmailError = false;
  // PASSWORD ERROR
  showError = false;
  error: string;
  submit = false;
  returnUrl: string;
  attempts_remaining = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", Validators.required),
    });

    if (this.ssoEnabled) {
      this.currentLoginStep = LoginStep.SSO;
    }
  }

  get f() {
    return this.loginForm.controls;
  }

  getEmailInputError() {
    return this.loginForm.controls.email.errors.required
      ? "LOGIN.EMAIL_REQUIRED"
      : this.loginForm.controls.email.errors.email
      ? "LOGIN.EMAIL_INVALID"
      : "";
  }

  getPasswordInputError() {
    return this.loginForm.controls.password.errors.required
      ? "LOGIN.PASSWORD_REQUIRED"
      : "";
  }

  trimSpaceFromEmail() {
    this.loginForm
      .get("email")
      .patchValue(this.loginForm.get("email").value.trim());
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.hasOwnProperty("returnUrl")
      ? this.route.snapshot.queryParams.returnUrl
      : "/";
    if (this.route.snapshot.queryParams.hasOwnProperty("authType")) {
      if (this.route.snapshot.queryParams.authType.toLowerCase() === "email") {
        this.currentLoginStep = LoginStep.EMAIL;
      }
    }
  }

  changeEmail() {
    this.currentLoginStep = LoginStep.EMAIL;
  }

  changeSSO() {
    this.currentLoginStep = LoginStep.SSO;
  }

  validateEmailAddress(formValue: any, valid: boolean) {
    this.showEmailError = false;
    this.submit = true;
    if (this.f.email.valid) {
      this.submit = false;
      this.currentLoginStep = LoginStep.PASSWORD;
      setTimeout(() => {
        this.passwordRef.nativeElement.focus();
      }, 200);
    }
  }

  redirectSSOurl() {
    this.authService.ssoLoginUrl().subscribe((data) => {
      location.replace(data.data[0].context);
    });
  }

  redirectCC(form: HTMLFormElement) {
    this.authService
      .ssoCCLoginSAMLRequest()
      .pipe(first())
      .subscribe((result) => {
        this.ccSamlRequest = result.data[0];

        setTimeout(() => {
          form.submit();
        }, 2000);
      });
  }

  login(formValue: any, valid: boolean) {
    if (!valid) {
      this.loginForm.markAsDirty();
      return;
    }

    this.showError = false;
    this.isLoading = true;

    this.authService.login(formValue.email, formValue.password).subscribe(
      (value) => {
        if (value.user.first_login) {
          this.router.navigate(["firstLogin"]);
        } else {
          this.router.navigate([this.returnUrl]);
        }
        this.isLoading = false;
      },
      (error) => {
        if (error.status === 401) {
          if (error.error.errors.length > 0) {
            if (error.error.errors[0].data.remaining_attempts) {
              this.attempts_remaining =
                error.error.errors[0].data.remaining_attempts;
            } else {
              this.attempts_remaining = null;
            }

            switch (error.error.errors[0].type) {
              case "ERR_INVALID_CREDENTIALS":
                this.error = "LOGIN.ERR_INVALID_CREDENTIALS";
                break;
              case "ERR_TOO_MANY_LOGIN_ATTEMPTS":
                this.error = "LOGIN.ERR_TOO_MANY_LOGIN_ATTEMPTS";
                break;
              case "AUTH_DISABLED_ACCOUNT":
                this.error = "LOGIN.AUTH_DISABLED_ACCOUNT";
                break;
              case "AUTH_ACCOUNT_NOT_VERIFIED":
                this.error = "LOGIN.AUTH_ACCOUNT_NOT_VERIFIED";
                break;
              case "USER_PASSWORD_EXPIRED":
                this.error = "LOGIN.USER_PASSWORD_EXPIRED";
                break;
              default:
                this.error = "LOGIN.ERROR_LOGIN";
            }
          }
        } else {
          this.error = "LOGIN.ERROR_LOGIN";
        }

        this.showError = true;
        this.isLoading = false;
        throw error;
      }
    );
  }
}
