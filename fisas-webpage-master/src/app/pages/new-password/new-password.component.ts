import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.less']
})
export class NewPasswordComponent implements OnInit {

  recovery_token: string = null;

  newPasswordVisible = false;

  loading = false;

  newPasswordForm = new FormGroup({
    token: new FormControl(),
    password: new FormControl(null, [
      Validators.required
    ]),
    repeat_password: new FormControl(null, [
      Validators.required
    ])
  });

  constructor(private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private translateService: TranslateService,
              private uiService: UiService) { }

  ngOnInit() {
    this.recovery_token = this.activatedRoute.snapshot.params.recovery_token;
  }

  getInputError(field: string) {
    return this.newPasswordForm.get(field).errors.required
      ? `FORM.MESSAGE.REQUIRED_FIELD`
      : this.newPasswordForm.get(field).errors.minlength
        ? 'FORM.MESSAGE.MIN_LENGTH'
        : this.newPasswordForm.get(field).errors.maxlength
          ? 'FORM.MESSAGE.MAX_LENGTH'
          : this.newPasswordForm.get(field).errors.min
            ? 'FORM.MESSAGE.VALUES'
            : this.newPasswordForm.get(field).errors.pattern
              ? 'FORM.MESSAGE.NUMERIC_ONLY'
              : null;
  }

  submit(formValue, valid) {

    if (!valid) {
      return;
    }

    delete formValue.repeat_password;
    formValue.token = this.recovery_token;

    if (this.recovery_token) {

      this.loading = true;
      this.authService
        .newPassword(formValue)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe(
          () => {
            this.uiService.showMessage(
              MessageType.success,
              this.translateService.instant('CHANGE_PASSWORD.SUCCESS_CHANGE')
            );
            this.router.navigate(['/', 'auth', 'login']);
          },
          () => {
            this.loading = false;
          }
        );

    } else {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant('CHANGE_PASSWORD.WITHOUT_TOKEN')
      );
    }
  }

}
