import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewPasswordComponent } from './new-password.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { NewPasswordRoutingModule } from './new-password-routing.module';

@NgModule({
  declarations: [
    NewPasswordComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NewPasswordRoutingModule,
  ]
})
export class NewPasswordModule { }
