import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewVerificationCodeComponent } from './new-verification-code.component';

const routes: Routes = [
  {
    path: '',
    component: NewVerificationCodeComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewVerificationCodeRoutingModule { }
