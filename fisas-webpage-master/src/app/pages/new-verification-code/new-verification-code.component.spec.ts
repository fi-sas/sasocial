import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVerificationCodeComponent } from './new-verification-code.component';

describe('NewVerificationCodeComponent', () => {
  let component: NewVerificationCodeComponent;
  let fixture: ComponentFixture<NewVerificationCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVerificationCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVerificationCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
