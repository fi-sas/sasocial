import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-new-verification-code',
  templateUrl: './new-verification-code.component.html',
  styleUrls: ['./new-verification-code.component.less']
})
export class NewVerificationCodeComponent implements OnInit {


  newVerificationForm: FormGroup;
  isLoading = false;

  constructor(private authService: AuthService,
              private router: Router,
              private translateService: TranslateService,
              private uiService: UiService
  ) {
    this.newVerificationForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    });

  }

  ngOnInit() {}

  getEmailInputError() {
    return this.newVerificationForm.controls.email.errors.required
      ? 'RESET_PASSWORD.INSERT_EMAIL'
      : this.newVerificationForm.controls.email.errors.email
        ? 'LOGIN.EMAIL_INVALID'
        : '';
  }

  newVerificationCode(formValue, valid) {

    if (!valid) {
      return;
    }

    this.isLoading = true;
    this.authService
      .newVerificationCode(formValue.email)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        () => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('NEW_VERIFICATION.REQUEST_SEND')
          );
          this.router.navigate(['/', 'auth', 'login']);
        },
        () => {
          this.isLoading = false;
        }
      );
  }
}
