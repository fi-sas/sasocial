import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewVerificationCodeRoutingModule } from './new-verification-code-routing.module';
import { NewVerificationCodeComponent } from './new-verification-code.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [NewVerificationCodeComponent],
  imports: [
    CommonModule,
    SharedModule,
    NewVerificationCodeRoutingModule
  ]
})
export class NewVerificationCodeModule { }
