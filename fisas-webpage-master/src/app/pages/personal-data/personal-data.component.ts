import { Component, OnInit } from "@angular/core";
import { countries } from "../../shared/data/countries";
import { nationalities } from "../../shared/data/nationalities";
import { ApplicationDataService } from "@fi-sas/webpage/shared/services/application-data.service";
import { AuthService } from "@fi-sas/webpage/auth/services/auth.service";
import { first, finalize } from "rxjs/operators";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  UiService,
  MessageType,
} from "@fi-sas/webpage/core/services/ui.service";
import { TranslateService } from "@ngx-translate/core";
import { FilesService } from "@fi-sas/webpage/modules/media/services/files.service";
import { UserModel } from "@fi-sas/webpage/auth/models/user.model";
import { OrganicUnitsService } from "@fi-sas/webpage/shared/services/organic-units.service";
import { OrganicUnitModel } from "@fi-sas/webpage/shared/models/organic-unit.model";
import { ConfigurationsService } from "@fi-sas/webpage/shared/services/configurations.service";
import { CourseModel } from "@fi-sas/webpage/shared/models/course.model";
import { CourseDegreeModel } from "@fi-sas/webpage/shared/models/course-degree.model";
import { DocumentTypeModel } from "@fi-sas/webpage/auth/models/document-type.model";
import { trimValidation } from "@fi-sas/webpage/shared/validators/validators.validator";

@Component({
  selector: "app-personal-data",
  templateUrl: "./personal-data.component.html",
  styleUrls: ["./personal-data.component.less"],
})
export class PersonalDataComponent implements OnInit {
  loading = false;
  formPersonal: FormGroup;
  submitted: boolean = false;
  mask: string = "0000-000";
  countries = countries;
  nationalities = nationalities;
  fileSizeInfo: boolean = false;
  file64: string = "";
  loadingSchools = false;
  loadingCourses = false;
  organicUnits: OrganicUnitModel[] = [];
  loadingDegrees = false;
  degrees: CourseDegreeModel[] = [];
  disableCourses = true;
  courses: CourseModel[] = [];
  profileId: number = -1;
  blocked_fields = [];
  loadingDocumentTypes = false;
  document_types: DocumentTypeModel[] = [];

  constructor(
    private applicationDataService: ApplicationDataService,
    private router: Router,
    private uiService: UiService,
    public translateService: TranslateService,
    private authService: AuthService,
    private filesServices: FilesService,
    private formBuilder: FormBuilder,
    private organicUnitsService: OrganicUnitsService,
    private configurationsService: ConfigurationsService,
  ) {

    this.formPersonal = this.formBuilder.group({
      name: ["", [Validators.required, trimValidation]],
      birth_date: ["", [Validators.required]],
      gender: ["", [Validators.required]],
      phone: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(18),
        ],
      ],
      document_type_id: [null, [Validators.required]],
      identification: ["", [Validators.required, trimValidation]],
      tin: [
        "",
        [Validators.required, Validators.minLength(9), Validators.maxLength(9)],
      ],
      nationality: ["", [Validators.required]],
      address: ["", [Validators.required,trimValidation]],
      city: ["", [Validators.required,trimValidation]],
      postal_code: ["", [Validators.required, Validators.minLength(8)]],
      country: ["", [Validators.required]],
      organic_unit_id: [""],
      course_degree_id: [""],
      course_id: [""],
      course_year: [""],
    });

    this.authService
      .getUserInfoByToken()
      .pipe(first())
      .subscribe((res) => {
        this.setFormPersonal(res.data[0]);
        applicationDataService.createApplication(res.data[0]);

      });
  }

  ngOnInit() {
    this.translateService.onLangChange.subscribe(() => {
      this.validSortNati();
    });
    this.validSortNati();
  }

  validSortNati() {
    if (this.translateService.currentLang == 'en') {
      this.nationalities = this.nationalities.sort((a, b) => {
        if (a.translate > b.translate) {
          return 1;
        }
        if (a.translate < b.translate) {
          return -1;
        }
        return 0;

      });
    }else{
      this.nationalities = nationalities;
    }
  }

  get f() {
    return this.formPersonal.controls;
  }

  setFormPersonal(data: UserModel) {
    if (data) {
      this.blocked_fields = data.blocked_fields;
      this.profileId = data.profile_id;
      this.f.name.setValue(data.name);
      this.f.birth_date.setValue(data.birth_date);
      this.f.gender.setValue(data.gender);
      this.f.phone.setValue(data.phone);
      this.f.document_type_id.setValue(data.document_type_id);
      this.f.identification.setValue(data.identification);
      this.f.tin.setValue(data.tin);
      this.f.address.setValue(data.address);
      this.f.city.setValue(data.city);
      this.f.postal_code.setValue(data.postal_code);
      this.f.nationality.setValue(data.nationality);
      this.f.country.setValue(data.country);
      this.f.organic_unit_id.setValue(data.organic_unit_id);
      this.f.course_degree_id.setValue(data.course_degree_id);
      this.f.course_id.setValue(data.course_id);
      this.f.course_year.setValue(data.course_year);

      this.getDocumentTypes();

      if (this.authService.hasPermission('sasocial:is_student')) {
        this.getOrganicUnits();
        this.getDegrees();
        this.getCourses();
        this.formPersonal
          .get("organic_unit_id")
          .setValidators(Validators.required);
        this.formPersonal.get("organic_unit_id").updateValueAndValidity();
        this.formPersonal
          .get("course_degree_id")
          .setValidators(Validators.required);
        this.formPersonal.get("course_degree_id").updateValueAndValidity();
        this.formPersonal.get("course_id").setValidators(Validators.required);
        this.formPersonal.get("course_id").updateValueAndValidity();
        this.formPersonal.get("course_year").setValidators(Validators.required);
        this.formPersonal.get("course_year").updateValueAndValidity();
      }
    }
  }

  getDocumentTypes() {
    this.loadingDocumentTypes = true;
    this.authService
      .getDocumentTypes()
      .pipe(
        first(),
        finalize(() => (this.loadingDocumentTypes = false))
      )
      .subscribe((document_types) => {
        this.document_types = document_types.data;
      });
  }
  getOrganicUnits() {
    this.loadingSchools = true;
    this.organicUnitsService
      .organicUnitsbySchools()
      .pipe(
        first(),
        finalize(() => (this.loadingSchools = false))
      )
      .subscribe((schools) => {
        this.organicUnits = schools.data;
      });
  }

  validBlockedFields(field: string) {
    if(this.blocked_fields.find((data)=>data == field)){
      return true;
    }
    return false;

  }

  getDegrees() {
    this.loadingDegrees = true;
    this.configurationsService
      .courseDegrees()
      .pipe(
        first(),
        finalize(() => (this.loadingDegrees = false))
      )
      .subscribe((degrees) => {
        this.degrees = degrees.data;
      });
  }

  changeValue() {
    this.f.course_id.setValue(null);
    this.getCourses();
  }

  getCourses() {
    if (
      this.f.organic_unit_id.value != null &&
      this.f.organic_unit_id.value != undefined &&
      this.f.course_degree_id.value != null &&
      this.f.course_degree_id.value != undefined
    ) {
      this.disableCourses = true;
      this.loadingCourses = true;
      this.configurationsService
        .courses(this.f.course_degree_id.value, this.f.organic_unit_id.value)
        .pipe(
          first(),
          finalize(() => (this.loadingCourses = false))
        )
        .subscribe((courses) => {
          this.courses = courses.data;
          this.disableCourses = false;
        });
    }
  }

  submit() {
    this.loading = true;
    this.submitted = true;
    if (this.formPersonal.valid) {
      this.authService
        .updateMe(this.formPersonal.value)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe((res) => {
          this.router.navigate(["/profile"]);
        });
      this.submitted = false;
    } else {
      this.loading = false;
    }
  }

  cancel() {
    this.router.navigate(["/profile"]);
  }

  validCodPost(event) {
    if (
      event === "Portugal" ||
      event === "" ||
      event === undefined ||
      event === null
    ) {
      this.mask = "0000-000";
      this.f.postal_code.setValidators([
        Validators.required,
        Validators.minLength(8),
      ]);
    } else {
      this.mask = "A*";
      this.f.postal_code.setValidators(null);
      this.f.postal_code.setValidators([Validators.required]);
    }
    this.f.postal_code.updateValueAndValidity();
  }

  uploadImg(event) {
    const file = event.dataTransfer
      ? event.dataTransfer.files[0]
      : event.target.files[0];
    this.fileSizeInfo = file.size > 2000000;

    const formData = new FormData();
    formData.append("file", file);
    formData.append("name", file.name);
    if (this.fileSizeInfo) {
      this.uiService.showMessage(
        MessageType.error,
        this.translateService.instant("PERSONAL_DATA_PAGE.ERRORIMAGE")
      );
    } else {
      this.filesServices
        .createFile(formData)
        .pipe(
          first(),
          finalize(() => (this.loading = false))
        )
        .subscribe(() => {});
    }
  }
}
