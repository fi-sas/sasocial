import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalDataRoutingModule } from './personal-data-routing.module';
import { PersonalDataComponent } from './personal-data.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [
    PersonalDataComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PersonalDataRoutingModule
  ]
})
export class PersonalDataModule { }
