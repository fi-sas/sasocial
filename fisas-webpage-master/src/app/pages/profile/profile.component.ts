import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { first } from 'rxjs/operators';
import { MovementsService } from '@fi-sas/webpage/modules/current-account/services/movements.service';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { BalanceModel } from '@fi-sas/webpage/modules/current-account/models/balance.model';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { ConfigsService } from '@fi-sas/webpage/modules/private-accommodation/services/configs.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  user: UserModel = null;
  balances: BalanceModel[] = [];
  loading: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private movementsService: MovementsService,
    private configsService: ConfigsService
  ) { }

  ngOnInit() {

    this.authService.getUserInfoByToken().pipe(first()).subscribe( data => {
      this.user = data.data[0];
    });

    this.movementsService.balances().pipe(first()).subscribe(data => {
      this.balances = data.data;
    });
  }


  signout() {
    this.authService.logout().pipe(first()).subscribe();
    this.router.navigate(['auth', 'login']);
  }
}
