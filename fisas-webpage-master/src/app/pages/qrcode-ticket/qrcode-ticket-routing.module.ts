import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrcodeTicketComponent } from './qrcode-ticket.component';

const routes: Routes = [
  {
    path: '',
    component: QrcodeTicketComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrcodeTicketRoutingModule { }
