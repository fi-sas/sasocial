import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodeTicketComponent } from './qrcode-ticket.component';

describe('QrcodeTicketComponent', () => {
  let component: QrcodeTicketComponent;
  let fixture: ComponentFixture<QrcodeTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodeTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
