import { Component, OnInit } from '@angular/core';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import QRCode from 'qrcode'
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './qrcode-ticket.component.html',
  styleUrls: ['./qrcode-ticket.component.less']
})
export class QrcodeTicketComponent implements OnInit {

  ticket = '';
  ticket_url = null;
  institute = environment.institute;

  data = {
    name: '',
    email: '',
    student_number: '',
  }

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.data = {
      email: this.authService.getUser().email,
      name: this.authService.getUser().name,
      student_number: this.authService.getUser().student_number,
    }

    this.ticket = this.authService.getUser().email;
    this.drawQRCode();
  }

  drawQRCode() {
    QRCode.toDataURL(this.ticket)
    .then(url => {
      this.ticket_url = url;
    })
    .catch(err => {
      console.error(err)
    })
  }

}
