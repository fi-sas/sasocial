import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QrcodeTicketRoutingModule } from './qrcode-ticket-routing.module';
import { QrcodeTicketComponent } from './qrcode-ticket.component';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';

@NgModule({
  declarations: [QrcodeTicketComponent],
  imports: [
    CommonModule,
    QrcodeTicketRoutingModule,
    SharedModule,
  ]
})
export class QrcodeTicketModule { }
