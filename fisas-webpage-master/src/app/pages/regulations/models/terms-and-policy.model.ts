export class TermsAndPolicyModel {
  id: number;
  language_id: number;
  service_id: number;
  text: string;
  created_at: Date;
  updated_at: Date;
}
