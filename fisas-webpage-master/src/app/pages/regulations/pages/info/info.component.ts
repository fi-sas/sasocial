import { Component, OnInit } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { TranslateService } from '@ngx-translate/core';
import { first, finalize } from 'rxjs/operators';
import { InformationService } from '../../services/information.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.less']
})
export class InfoComponent implements OnInit {


  htmlToDisplay = [];
  languages: any;


  constructor(
    private informationService: InformationService,
    private translateService: TranslateService,
    config: FiConfigurator,
  ) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  ngOnInit() {

    this.informationService.getInformation(22)
    .pipe(
      first(),
      finalize(() => {})
    )
    .subscribe(
      (result) => {
        this.htmlToDisplay = result.data;
      }
    );
  }

  getCurrentLanguage() {
    const currentLang = this.languages.find(lang => lang.acronym === this.translateService.currentLang);
    return currentLang.id;
  }

}
