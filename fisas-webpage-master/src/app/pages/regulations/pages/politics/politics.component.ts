import { Component, OnInit } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { TranslateService } from '@ngx-translate/core';
import { first, finalize } from 'rxjs/operators';
import { PolicyService } from '../../services/policy.service';

@Component({
  selector: 'app-politics',
  templateUrl: './politics.component.html',
  styleUrls: ['./politics.component.less']
})
export class PoliticsComponent implements OnInit {

  htmlToDisplay = [];
  languages: any;


  constructor(
    private policyService: PolicyService,
    private translateService: TranslateService,
    config: FiConfigurator,
  ) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  ngOnInit() {

    this.policyService.getPolicy(22)
    .pipe(
      first(),
      finalize(() => {})
    )
    .subscribe(
      (result) => {
        this.htmlToDisplay = result.data;
      }
    );
  }

  getCurrentLanguage() {
    const currentLang = this.languages.find(lang => lang.acronym === this.translateService.currentLang);
    return currentLang.id;
  }
}
