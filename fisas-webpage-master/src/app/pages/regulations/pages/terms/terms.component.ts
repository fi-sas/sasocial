import { Component, OnInit } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { FiUrlService } from '@fi-sas/core';
import { TranslateService } from '@ngx-translate/core';
import { first, finalize } from 'rxjs/operators';
import { TermsService } from '../../services/terms.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.less']
})
export class TermsComponent implements OnInit {

  htmlToDisplay = [];
  languages: any;

  constructor(
    private termsService: TermsService,
    private urlService: FiUrlService,
    private translateService: TranslateService,
    config: FiConfigurator,
  ) {
    this.languages = Object.values(config.getOptionTree('LANGUAGES'))[0];
  }

  ngOnInit() {

    this.termsService.getTerms(22)
    .pipe(
      first(),
      finalize(() => {})
    )
    .subscribe(
      (result) => {
        this.htmlToDisplay = result.data;
      }
    );
  }

  getCurrentLanguage() {
    const currentLang = this.languages.find(lang => lang.acronym === this.translateService.currentLang);
    return currentLang.id;
  }

}
