import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoliticsComponent } from './pages/politics/politics.component';
import { RegulationsComponent } from './regulations.component';
import { TermsComponent } from './pages/terms/terms.component';
import { InfoComponent } from './pages/info/info.component';

const routes: Routes = [
  {
    path: '',
    component: RegulationsComponent,
    children: [
      {
        path: '',
        redirectTo: 'terms',
        pathMatch: 'full'
      },
      {
        path: 'informations',
        component: InfoComponent,
        data: { breadcrumb: 'FOOTER.INFORMATIONS' },
      },

      {
        path: 'terms',
        component: TermsComponent,
        data: { breadcrumb: 'REGULATIONS.TERMS' },
      },
      {
        path: 'privacy_politics',
        component: PoliticsComponent,
        data: { breadcrumb: 'REGULATIONS.PRIVACY_POLICY' },
      }
    ],
    data: { breadcrumb: null, title: null },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegulationsRoutingModule { }
