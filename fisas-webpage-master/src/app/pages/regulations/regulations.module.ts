
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { RegulationsRoutingModule } from './regulations-routing.module';
import { TermsComponent } from './pages/terms/terms.component';
import { PoliticsComponent } from './pages/politics/politics.component';
import { RegulationsComponent } from './regulations.component';
import { InfoComponent } from './pages/info/info.component';

@NgModule({
  declarations: [
  RegulationsComponent,
  TermsComponent,
  PoliticsComponent,
  InfoComponent],
  imports: [
    CommonModule,
    SharedModule,
    RegulationsRoutingModule
  ]
})
export class RegulationsModule { }
