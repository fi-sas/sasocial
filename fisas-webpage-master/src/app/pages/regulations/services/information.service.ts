import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { TermsAndPolicyModel } from '../models/terms-and-policy.model';

@Injectable({
  providedIn: 'root'
})
export class InformationService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  getInformation(id): Observable<Resource<TermsAndPolicyModel>> {
    const aux = this.urlService.get('CONFIGURATION.INFORMATIONS') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }
}
