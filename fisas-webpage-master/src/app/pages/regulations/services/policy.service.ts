import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { TermsAndPolicyModel } from '../models/terms-and-policy.model';

@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }

  getPolicy(id): Observable<Resource<TermsAndPolicyModel>> {
    const aux = this.urlService.get('CONFIGURATION.PRIVACY_POLICIES') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }
}
