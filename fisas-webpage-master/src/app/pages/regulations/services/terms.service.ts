import { Injectable } from '@angular/core';
import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { Observable } from 'rxjs';
import { TermsAndPolicyModel } from '../models/terms-and-policy.model';
@Injectable({
  providedIn: 'root'
})
export class TermsService {

  constructor(private resourceService: FiResourceService,
              private urlService: FiUrlService) { }



  getTerms(id): Observable<Resource<TermsAndPolicyModel>> {
    const aux = this.urlService.get('CONFIGURATION.TERMS') +
    '?query[service_id]=' + id;
    return this.resourceService
      .list(aux);
  }
}
