import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@fi-sas/webpage/auth/services/auth.service';
import { finalize } from 'rxjs/operators';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  isLoading = false;

  constructor(private authService: AuthService,
              private router: Router,
              private translateService: TranslateService,
              private uiService: UiService
  ) {
    this.resetPasswordForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    });

  }

  ngOnInit() {}

  getEmailInputError() {
    return this.resetPasswordForm.controls.email.errors.required
      ? 'RESET_PASSWORD.INSERT_EMAIL'
      : this.resetPasswordForm.controls.email.errors.email
        ? 'LOGIN.EMAIL_INVALID'
        : '';
  }

  resetPassword(formValue, valid) {

    if (!valid) {
      return;
    }

    this.isLoading = true;
    this.authService
      .resetPassword(formValue)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(
        () => {
          this.uiService.showMessage(
            MessageType.success,
            this.translateService.instant('RESET_PASSWORD.REQUEST_SEND')
          );
          this.router.navigate(['/', 'auth', 'login']);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

}
