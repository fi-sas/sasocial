import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SsoErrorComponent } from './sso-error.component';

const routes: Routes = [
  {
    path: '',
    component: SsoErrorComponent,
    data: { breadcrumb: null, title: null }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SsoErrorRoutingModule { }
