import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsoErrorComponent } from './sso-error.component';

describe('SsoErrorComponent', () => {
  let component: SsoErrorComponent;
  let fixture: ComponentFixture<SsoErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsoErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsoErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
