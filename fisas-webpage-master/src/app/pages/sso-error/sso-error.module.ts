import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SsoErrorRoutingModule } from './sso-error-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { SsoErrorComponent } from './sso-error.component';

@NgModule({
  declarations: [
    SsoErrorComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SsoErrorRoutingModule
  ]
})
export class SsoErrorModule { }
