import { Component, OnInit } from '@angular/core';
import { UiService } from '@fi-sas/webpage/core/services/ui.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.less']
})
export class UnauthorizedComponent implements OnInit {
  constructor(private location: Location) {

  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }
}
