import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnauthorizedRoutingModule } from './unauthorized-routing.module';
import { SharedModule } from '@fi-sas/webpage/shared/shared.module';
import { UnauthorizedComponent } from './unauthorized.component';

@NgModule({
  declarations: [
    UnauthorizedComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UnauthorizedRoutingModule
  ]
})
export class UnauthorizedModule { }
