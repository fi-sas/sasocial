import { Component, OnInit, Input } from '@angular/core';
import { BarOptions } from '@fi-sas/webpage/shared/interfaces/bar-options';
import { ChangeComponentService } from './services/change-component.service';
@Component({
  selector: 'app-bar-menu',
  templateUrl: './bar-menu.component.html',
  styleUrls: ['./bar-menu.component.less'],
})
export class BarMenuComponent implements OnInit {
  @Input() options: BarOptions[] = [];
  @Input() startIndex: number = null;

  constructor(private changeComponentService: ChangeComponentService) {}

  ngOnInit() {
    this.getOptionSelectedByLink();
  }

  saveIndex(i: number) {
    this.changeComponentService.saveCurrentIndex(i);
  }

  getOptionSelectedByLink() {
    let changedIndex = false;
    if (this.startIndex === null) {
      this.options.forEach((option, index) => {
        if (!option.disable && !changedIndex) {
          this.startIndex = index;
          changedIndex = true;
        }
      });
    }
  }
}
