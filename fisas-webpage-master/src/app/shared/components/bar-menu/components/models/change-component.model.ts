export class ChangeComponentModel {
  index: number;
  component: any;
  inputs?: object;
}
