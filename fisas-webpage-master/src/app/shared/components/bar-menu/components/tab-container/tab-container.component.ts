import { ChangeComponentService } from "./../../services/change-component.service";
import {
  Component,
  OnInit,
  Input,
  ComponentFactoryResolver,
  ViewContainerRef,
  ViewChild,
  OnDestroy,
} from "@angular/core";
import { BarOptions } from "@fi-sas/webpage/shared/interfaces/bar-options";
import { Subscription } from "rxjs";

@Component({
  selector: "app-tab-container",
  templateUrl: "./tab-container.component.html",
  styleUrls: ["./tab-container.component.less"],
})
export class TabContainerComponent implements OnInit, OnDestroy {
  @ViewChild("container", { read: ViewContainerRef })
  viewContainerRef: ViewContainerRef;
  @Input() option: BarOptions;
  @Input() index: number;

  _subscriptions: Subscription[] = [];
  set subscriptions(subscription: Subscription) {
    this._subscriptions.push(subscription);
  }

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private changeComponentService: ChangeComponentService
  ) {
    this.subscriptions = this.changeComponentService
      .alternativeComponentObservable()
      .subscribe((component) => {
        if (component !== null) {
          if (component.index === this.index) {
            this.componentFactory(component.component, component.inputs);
          }
        }
      });
  }

  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());
  }

  ngOnInit() {
    const factory = this.componentFactoryResolver.resolveComponentFactory(
      this.option.content
    );
    const ref = this.viewContainerRef.createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

  componentFactory(component: any, inputs: object = null) {
    this.viewContainerRef.remove();
    let componentToFactory: any;
    componentToFactory = component === null ? this.option.content : component;
    const factory = this.componentFactoryResolver.resolveComponentFactory(
      componentToFactory
    );
    const ref = this.viewContainerRef.createComponent(factory);

    if (inputs) {
      for (const [key, value] of Object.entries(inputs)) {
        ref.instance[key] = value;
      }
    }

    ref.changeDetectorRef.detectChanges();
  }
}
