import { TestBed } from '@angular/core/testing';

import { ChangeComponentService } from './change-component.service';

describe('ChangeComponentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChangeComponentService = TestBed.get(ChangeComponentService);
    expect(service).toBeTruthy();
  });
});
