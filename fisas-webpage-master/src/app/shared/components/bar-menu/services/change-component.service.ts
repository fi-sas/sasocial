import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ChangeComponentModel } from '../components/models/change-component.model';

@Injectable({
  providedIn: 'root',
})
export class ChangeComponentService {
  alternativeComponent = new BehaviorSubject<ChangeComponentModel>(null);
  currentIndex = new BehaviorSubject<number>(null);

  constructor() {}

  alternativeComponentObservable(): Observable<ChangeComponentModel> {
    return this.alternativeComponent.asObservable();
  }

  factoryComponent(component: ChangeComponentModel) {
    this.alternativeComponent.next(component);
  }

  getComponent(): ChangeComponentModel {
    return this.alternativeComponent.getValue();
  }

  currentIndexObservable(): Observable<number> {
    return this.currentIndex.asObservable();
  }

  saveCurrentIndex(index: number) {
    this.currentIndex.next(index);
  }

  getCurrentIndex(): number {
    return this.currentIndex.getValue();
  }
}
