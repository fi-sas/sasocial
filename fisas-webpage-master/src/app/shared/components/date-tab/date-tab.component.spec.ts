import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateTabComponent } from './date-tab.component';

describe('DateTabComponent', () => {
  let component: DateTabComponent;
  let fixture: ComponentFixture<DateTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
