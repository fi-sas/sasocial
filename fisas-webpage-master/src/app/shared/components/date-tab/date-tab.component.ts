import { options } from './../../shared.module';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Component({
  selector: 'app-date-tab',
  templateUrl: './date-tab.component.html',
  styleUrls: ['./date-tab.component.less'],
})
export class DateTabComponent implements OnInit {
  fifteenDays = [];
  fifteenMonth = [];

  startIndex: number;

  @Output() newDate = new EventEmitter();

  /*
   * Date Bar Types
   *  - weekday
   *  - months
   * */
  @Input() dateTabType = 'weekday';

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    this.setDays();
  }

  daySelected(day: any): void {
    const currentIndex = this.dateTabType === 'weekday' ? this.fifteenDays.indexOf(day) : this.fifteenMonth.indexOf(day);
    this.newDate.emit({ date: day, index: currentIndex, options: [], init: false });
  }

  setDays() {
    const currentDate = moment();
    let indexDate = currentDate;

    if (this.dateTabType === 'weekday') {
      const maxDate = moment().add(15, 'day');

      while (currentDate.isBefore(maxDate)) {
        this.fifteenDays.push(indexDate.toDate());
        indexDate = indexDate.add(1, 'day');
      }
      this.newDate.emit({ date: this.fifteenDays[0], index: 1, options: this.fifteenDays, init: true });
    } else if (this.dateTabType === 'months') {
      const maxMonth = moment().subtract(14, 'months');

      while (currentDate.isAfter(maxMonth)) {
        this.fifteenMonth.push(indexDate.endOf('month').toDate());
        indexDate = indexDate.subtract(1, 'months');
      }

      this.fifteenMonth.unshift(moment().add(1, 'months').toDate());

      this.startIndex = 1;
      this.newDate.emit({ date: this.fifteenMonth[1], index: 1, options: this.fifteenMonth, init: true });
    }
  }
}
