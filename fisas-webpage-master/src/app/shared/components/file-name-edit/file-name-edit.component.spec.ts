import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileNameEditComponent } from './file-name-edit.component';

describe('FileNameEditComponent', () => {
  let component: FileNameEditComponent;
  let fixture: ComponentFixture<FileNameEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileNameEditComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileNameEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
