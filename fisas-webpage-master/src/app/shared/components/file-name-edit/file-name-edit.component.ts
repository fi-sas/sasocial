import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';

import { NamedFileModel } from '@fi-sas/webpage/modules/media/models/file.model';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';

@Component({
  selector: 'app-file-name-edit',
  templateUrl: './file-name-edit.component.html',
  styleUrls: ['./file-name-edit.component.less'],
})
export class FileNameEditComponent extends FormHandler {
  @Input() set fileName(name: string) {
    this.form = this.getForm(name);
  }

  @Input() originalName: string = '';

  form: FormGroup;

  constructor(private modalRef: NzModalRef<FileNameEditComponent, string>) {
    super();
  }

  onSubmit() {
    if (!this.isFormValid(this.form)) {
      return;
    }

    this.modalRef.close(this.form.get('name').value);
  }

  private getForm(fileName: string): FormGroup {
    return new FormGroup({ name: new FormControl(fileName, Validators.required) });
  }
}
