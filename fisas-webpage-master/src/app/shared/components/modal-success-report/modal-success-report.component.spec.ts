import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSuccessReportComponent } from './modal-success-report.component';

describe('ModalSuccessReportComponent', () => {
  let component: ModalSuccessReportComponent;
  let fixture: ComponentFixture<ModalSuccessReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSuccessReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSuccessReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
