import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-month-selector',
  templateUrl: './month-selector.component.html',
  styleUrls: ['./month-selector.component.less']
})
export class MonthSelectorComponent implements OnInit {

  @Output() monthSelected = new EventEmitter();
  @Input() month;
  date = new Date();

  constructor(public translate: TranslateService) { }

  ngOnInit() {
    if(this.month) {
      this.date.setMonth(this.month.month);
      this.date.setFullYear(this.month.year);
    }
  }

  forwardYear() {
    this.date = new Date(this.date.setMonth(this.date.getMonth() + 1));
    this.monthSelected.emit({year: this.date.getFullYear(), month: this.date.getMonth()});
  }

  backYear() {
    this.date = new Date(this.date.setMonth(this.date.getMonth() - 1));
    this.monthSelected.emit({year: this.date.getFullYear(), month: this.date.getMonth()});
  }

}
