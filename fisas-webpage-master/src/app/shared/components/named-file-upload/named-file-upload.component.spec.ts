import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamedFileUploadComponent } from './named-file-upload.component';

describe('NamedFileUploadComponent', () => {
  let component: NamedFileUploadComponent;
  let fixture: ComponentFixture<NamedFileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NamedFileUploadComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamedFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
