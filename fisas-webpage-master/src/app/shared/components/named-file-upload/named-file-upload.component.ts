import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { first, map } from 'rxjs/operators';
import { NzModalService, UploadFile, UploadFilter, UploadListType, UploadXHRArgs } from 'ng-zorro-antd';
import { Observable, Observer, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { FiConfigurator } from '@fi-sas/configurator';
import { FileMessageModel } from '../../models/file-message.model';
import { FileNameEditComponent } from '../file-name-edit/file-name-edit.component';
import { FilesService } from '@fi-sas/webpage/modules/media/services/files.service';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { NamedFileModel } from '@fi-sas/webpage/modules/media/models/file.model';

type FileItem = NamedFileModel & { originalName: string; uid: string };

@Component({
  selector: 'app-named-file-upload',
  templateUrl: './named-file-upload.component.html',
  styleUrls: ['./named-file-upload.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NamedFileUploadComponent),
      multi: true,
    },
  ],
})
export class NamedFileUploadComponent implements ControlValueAccessor {
  // ControlValueAccessor
  onChange: any = () => {};
  onTouched: any = () => {};

  @Input() isPublic: string = 'true';
  @Input() weight: number = 1;
  @Input() multiUploads: boolean = false;
  @Input() listType: UploadListType = 'text';
  @Input() buttonName: string = 'FILE.BUTTON';
  @Input() limit: number = 0;
  @Input() filterTypes: string[] = [];
  // FIXME: DEFAULT MUST BE FALSE
  @Input() changeFileName: boolean = true;
  @Input() messages: FileMessageModel = {
    upload: {
      sucess: 'FILE.UPLOAD.SUCCESS',
      error: 'FILE.UPLOAD.ERROR',
    },
    delete: {
      sucess: 'FILE.DELETE.SUCCESS',
      error: 'FILE.DELETE.ERROR',
    },
    load: {
      error: 'FILE.LOAD.ERROR',
    },
  };

  filters: UploadFilter[] = [
    {
      name: 'type',
      fn: (fileList: UploadFile[]) => {
        if (this.filterTypes.length !== 0) {
          const filterFiles = fileList.filter((w) => ~this.filterTypes.indexOf(w.type));
          if (filterFiles.length !== fileList.length) {
            this.uiService.showMessage(MessageType.error, this.translateService.instant('FILE.TYPE.INVALID'));
            return filterFiles;
          }
        }
        return fileList;
      },
    },
    {
      name: 'async',
      fn: (fileList: UploadFile[]) =>
        new Observable((observer: Observer<UploadFile[]>) => {
          // doing
          observer.next(fileList);
          observer.complete();
        }),
    },
  ];

  languageId: number;
  categoryId: number;

  isLoadingFiles: boolean;

  fileList: FileItem[] = [];

  value: NamedFileModel[] = [];

  canUpload: boolean = true;

  constructor(
    private configurator: FiConfigurator,
    private filesServices: FilesService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private uiService: UiService
  ) {
    if (!this.languageId) {
      this.languageId = this.configurator.getOption('DEFAULT_LANG_ID');
    }

    if (!this.categoryId) {
      this.categoryId = this.configurator.getOption('DEFAULT_CATEGORY_ID');
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(files: NamedFileModel[]) {
    (files || []).forEach((file) => this.onFileAdded(file));
  }

  handleUploadFile: (item: UploadXHRArgs) => Subscription = (item) => {
    const formData = this.getFileFormData(item);
    return this.filesServices
      .createFile(formData)
      .pipe(first())
      .subscribe((result) => {
        item.onSuccess!({}, item.file!, event);
        const file = result.data[0];
        this.onFileAdded({ file, name: item.file.name, file_id: file.id }, item.file.uid);
        this.uiService.showMessage(MessageType.success, this.translateService.instant(this.messages.upload.sucess));
      });
  };

  handlePreview: (file: UploadFile) => void = (file) => {
    window.open(file.url);
  };

  handleFileRemove: (file: UploadFile) => boolean | Observable<boolean> = (file) => {
    let fileToRemove = this.fileList.find((f) => f.uid === file.uid);
    if (!fileToRemove) {
      return false;
    }
    return this.filesServices.removeFile(fileToRemove.file_id).pipe(
      first(),
      map(() => {
        this.fileList = this.fileList.filter((f) => f.file_id !== fileToRemove.file_id);
        this.value = this.value.filter((f) => f.file_id !== fileToRemove.file_id);
        return true;
      })
    );
  };

  removeFile(fileId: number) {
    this.filesServices.removeFile(fileId).pipe(first()).subscribe(() => {
      this.fileList = this.fileList.filter((f) => f.file_id !== fileId);
      this.value = this.value.filter((f) => f.file_id !== fileId);
      this.onChange(this.value);
    });
  }

  editName(fileIndex: number) {
    if (fileIndex < 0 || fileIndex >= this.fileList.length) {
      return;
    }
    this.modalService
      .create({
        nzWidth: 500,
        nzMaskClosable: false,
        nzClosable: true,
        nzContent: FileNameEditComponent,
        nzComponentParams: {
          fileName: this.fileList[fileIndex].name,
          originalName: this.fileList[fileIndex].originalName,
        },
        nzFooter: null,
      })
      .afterClose.pipe(first())
      .subscribe((result: string) => {
        if (result && typeof result === 'string') {
          this.fileList[fileIndex].name = result;
          this.value[fileIndex].name = result;
        }
      });
  }

  private getFileFormData(item: UploadXHRArgs): FormData {
    const formData = new FormData();
    formData.append('name', item.file.name);
    formData.append('weight', this.weight.toString());
    formData.append('public', this.isPublic);
    formData.append('file_category_id', this.categoryId.toString());
    formData.append('language_id', this.languageId.toString());
    formData.append('file', item.file as any, item.file.name);
    return formData;
  }

  private onFileAdded(file: NamedFileModel, uid: string = null): FileItem {
    this.fileList = this.fileList || [];
    this.value = this.value || [];
    const item = Object.assign({}, file, { originalName: file.name, uid: uid || file.file_id.toString() });
    this.value.push(file);
    this.fileList.push(item);
    this.onChange(this.value);
    if (this.limit > 0) {
      this.canUpload = this.value.length < this.limit;
    } else {
      this.canUpload = this.value.length > 0 ? this.multiUploads : true;
    }
    return item;
  }
}
