import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoConnectionComponent } from './no-connection.component';
import { NgZorroAntdModule, NzModalRef } from 'ng-zorro-antd';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('NoConnectionComponent', () => {
  let component: NoConnectionComponent;
  let fixture: ComponentFixture<NoConnectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoConnectionComponent ],
      imports: [
        NgZorroAntdModule,
        HttpClientTestingModule,
      ],
      providers : [
        NzModalRef
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
