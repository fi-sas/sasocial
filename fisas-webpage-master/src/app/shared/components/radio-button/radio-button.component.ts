import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OptionRadioButtonModel } from '@fi-sas/webpage/shared/models/option-radio-button.model';
import { hasOwnProperty } from 'tslint/lib/utils';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.less']
})
export class RadioButtonComponent implements OnInit {

  @Input() options: OptionRadioButtonModel[] = [{value: 'value', translation: 'DEFAULT.OPTION'}];
  @Input() optionsValue = 'value' ;
  @Input() disabled = false;
  @Output() optionSelected = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.optionsValue === null) {
      if (this.options.length !== 0) {
        if (hasOwnProperty(this.options[0], 'value')) {
          this.optionsValue = this.options[0].value;
        }
      }
    }
  }

  onChangeOption(value) {
    this.optionsValue = value;
    this.optionSelected.emit(value);
  }
}
