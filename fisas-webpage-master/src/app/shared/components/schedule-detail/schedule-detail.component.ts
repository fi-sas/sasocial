import { Component, Input } from '@angular/core';

import { scheduleTable, Schedule, IScheduleTable } from '../../data/schedule';

type FormatedScheduleRow = Schedule & IScheduleTable;
import * as moment from 'moment';
@Component({
  selector: 'app-schedule-detail',
  templateUrl: './schedule-detail.component.html',
  styleUrls: ['./schedule-detail.component.less'],
})
export class ScheduleDetailComponent {
  @Input() set schedule(schedule: Schedule[]) {
    if ((schedule || []).length) {
      this.formatedSchedule = this.getFormatedSchedule(schedule);
      this.getInfo();
     
    }
  }

  formatedSchedule: FormatedScheduleRow[] = [];
  schedules = {
    1: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    2: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    3: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    4: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    5: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    6: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    7: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    8: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    9: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    10: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    11: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    12: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    13: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    14: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    15: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    16: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    17: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    18: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
    19: { active: false, begin: moment().set('hour', 9).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 13).set('minutes', 0).set('second', 0).toDate() },
    20: { active: false, begin: moment().set('hour', 14).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate() },
    21: { active: false, begin: moment().set('hour', 18).set('minutes', 0).set('second', 0).toDate(), end: moment().set('hour', 22).set('minutes', 0).set('second', 0).toDate() },
  };

  constructor() {}

  private getFormatedSchedule(rawSchedule: Schedule[]) {
  
    const rows: FormatedScheduleRow[] = [];
    scheduleTable.forEach((item) => {
      const scheduleRow = rawSchedule.find((s) => s.schedule_id.toString() === item.id.toString());
      if (scheduleRow) {
        rows.push({ ...item, ...scheduleRow });
      }
    });
    return rows;
  }

  getInfo(){
    if(this.formatedSchedule.length>0) {
      this.formatedSchedule.map(s => {
        this.schedules[s.schedule_id].active = true;
        this.schedules[s.schedule_id].begin = s.time_begin;
        this.schedules[s.schedule_id].end = s.time_end;
      });
    }

  }
}
