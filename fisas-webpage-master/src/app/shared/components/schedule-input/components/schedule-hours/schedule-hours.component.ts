import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

import { dateAfter } from '@fi-sas/webpage/shared/validators/validators.validator';
import { FormHandler } from '@fi-sas/webpage/shared/helpers/form-handler';

@Component({
  selector: 'app-schedule-hours',
  templateUrl: './schedule-hours.component.html',
  styleUrls: ['./schedule-hours.component.less'],
})
export class ScheduleHoursComponent extends FormHandler {
  @Input() set dates(dates: { begin: string; end: string }) {
    this.form = new FormGroup(
      {
        time_begin: new FormControl(dates.begin, Validators.required),
        time_end: new FormControl(dates.end, Validators.required),
      },
      dateAfter('time_begin', 'time_end')
    );
  }

  readonly buttonStyle = {
    width: '100%',
    height: '40px',
    overflow: 'hidden',
    'text-overflow': 'ellipsis',
    'font-size': '15px',
    'margin-bottom': '10px',
    'margin-top': '10px',
    'padding-left': '40px',
    'padding-right': '40px',
  };

  form: FormGroup;

  constructor(private translateService: TranslateService, private modal: NzModalRef) {
    super();
  }

  onSubmit() {
    if (this.isFormValid(this.form)) {
      this.modal.close(this.form.value);
    }
  }

  close() {
    this.modal.close();
  }

  getInputError(inputName: string): string {
    if (this.form.get(inputName).errors.required || this.form.get(inputName).errors.requiredTrue) {
      return this.translateService.instant('MISC.REQUIRED_FIELD');
    }
    return null;
  }
}
