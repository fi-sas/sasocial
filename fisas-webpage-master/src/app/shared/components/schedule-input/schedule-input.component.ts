import { Component, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';

import { first, takeUntil } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { ScheduleHoursComponent } from './components';

interface IScheduleRow {
  title: string;
  keys: string[];
  startTime: () => Date;
  endTime: () => Date;
}

interface IScheduleItem {
  schedule_id: number;
  time_begin: string;
  time_end: string;
}

@Component({
  selector: 'app-schedule-input',
  templateUrl: './schedule-input.component.html',
  styleUrls: ['./schedule-input.component.less'],
  providers: [{ provide: NG_VALUE_ACCESSOR, multi: true, useExisting: ScheduleInputComponent }],
})
export class ScheduleInputComponent implements ControlValueAccessor, OnDestroy {
  readonly scheduleRows: IScheduleRow[] = [
    {
      title: 'SHARED.SCHEDULE.PERIOD.MORNING',
      keys: ['4', '7', '10', '13', '16', '19', '1'],
      startTime: () => moment().set({ hour: 9, minutes: 0, second: 0 }).toDate(),
      endTime: () => moment().set({ hour: 13, minutes: 0, second: 0 }).toDate(),
    },
    {
      title: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
      keys: ['5', '8', '11', '14', '17', '20', '2'],
      startTime: () => moment().set({ hour: 14, minutes: 0, second: 0 }).toDate(),
      endTime: () => moment().set({ hour: 18, minutes: 0, second: 0 }).toDate(),
    },
    {
      title: 'SHARED.SCHEDULE.PERIOD.NIGHT',
      keys: ['6', '9', '12', '15', '18', '21', '3'],
      startTime: () => moment().set({ hour: 18, minutes: 0, second: 0 }).toDate(),
      endTime: () => moment().set({ hour: 22, minutes: 0, second: 0 }).toDate(),
    },
  ];

  private destroy$: Subject<void> = new Subject<void>();

  form: FormGroup;

  onChange = (_: IScheduleItem[]) => {};
  onTouched = () => {};
  touched = false;
  disabled = false;

  constructor(private modalService: NzModalService, private translateService: TranslateService) {
    this.form = this.getForm();
    this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => this.onChange(this.toExternalValue()));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  writeValue(value?: IScheduleItem[]) {
    if (value) {
      this.form.patchValue(this.toInternalValue(value));
    }
  }

  registerOnChange(onChange: (value: IScheduleItem[]) => void) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  openScheduleHoursModal(scheduleId: number): void {
    const scheduleHoursForm = this.form.get(scheduleId.toString()) as FormGroup;
    const modalRef = this.modalService.create({
      nzTitle: this.translateService.instant('VOLUNTEERING.ACTIONS.SCHEDULE'),
      nzContent: ScheduleHoursComponent,
      nzComponentParams: {
        dates: {
          begin: scheduleHoursForm.get('time_begin').value,
          end: scheduleHoursForm.get('time_end').value,
        },
      },
      nzFooter: null,
      nzMaskClosable: false,
      nzClosable: false,
    });
    modalRef.afterClose.pipe(first()).subscribe((result) => {
      if (result) {
        this.form.get(scheduleId.toString()).patchValue({
          active: true,
          time_begin: result.time_begin,
          time_end: result.time_end,
        });
      }
    });
  }

  private toInternalValue(schedule: IScheduleItem[]) {
    const value = {};
    schedule.forEach((s) => {
      const beginDate = moment(s.time_begin);
      const endDate = moment(s.time_end);
      value[s.schedule_id.toString()] = {
        active: true,
        time_begin: moment()
          .set({ hour: beginDate.get('hour'), minute: beginDate.get('minute'), second: 0, millisecond: 0 })
          .toDate(),
        time_end: moment()
          .set({ hour: endDate.get('hour'), minute: endDate.get('minute'), second: 0, millisecond: 0 })
          .toDate(),
      };
    });
    return value;
  }

  private toExternalValue(): IScheduleItem[] {
    const result: IScheduleItem[] = [];
    this.scheduleRows.forEach((row) => {
      row.keys.forEach((key) => {
        const formControlValue = this.form.get(key).value;
        if (formControlValue && formControlValue.active) {
          result.push({
            schedule_id: parseInt(key, 10),
            time_begin: moment(formControlValue.time_begin).toISOString(),
            time_end: moment(formControlValue.time_end).toISOString(),
          });
        }
      });
    });
    return result;
  }

  private getForm(): FormGroup {
    const desiredScheduleControls = {};
    this.scheduleRows.forEach((row) =>
      row.keys.forEach(
        (value) =>
          (desiredScheduleControls[value] = new FormGroup({
            active: new FormControl(false),
            time_begin: new FormControl(row.startTime(), Validators.required),
            time_end: new FormControl(row.endTime(), Validators.required),
          }))
      )
    );
    return new FormGroup(desiredScheduleControls, Validators.required);
  }
}
