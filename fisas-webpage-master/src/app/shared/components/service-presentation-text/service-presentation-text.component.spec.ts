import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePresentationTextComponent } from './service-presentation-text.component';

describe('ServicePresentationTextComponent', () => {
  let component: ServicePresentationTextComponent;
  let fixture: ComponentFixture<ServicePresentationTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServicePresentationTextComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePresentationTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
