import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { first } from 'rxjs/operators';

import { ConfigurationsService } from '@fi-sas/webpage/shared/services';
import { TextApresentationsModel } from '@fi-sas/webpage/shared/models';

@Component({
  selector: 'app-service-presentation-text',
  templateUrl: './service-presentation-text.component.html',
  styleUrls: ['./service-presentation-text.component.less'],
})
export class ServicePresentationTextComponent {
  @Input() title: string[] = null;
  @Input() set serviceId(id: number) {
    this.id = id;
    if (!isNaN(id)) {
      this.getInfoApresentation(id);
    }
  }
  id: number = 0;
  info: TextApresentationsModel[] = [];

  constructor(private configurationsService: ConfigurationsService, private domSanitizer: DomSanitizer) {}

  getInfoApresentation(id: number) {
    this.configurationsService
      .listTextApresentations(id)
      .pipe(first())
      .subscribe((data) => (this.info = data.data));
  }

  getHTML(value: string) {
    if (value) {
      value = value.replace('line-height: 1.17', 'line-height: 0.17');
    }
    return this.domSanitizer.bypassSecurityTrustHtml(value);
  }
}
