import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, first, switchMap, tap } from 'rxjs/operators';

import { Resource } from '@fi-sas/core';
import { UserModel } from '@fi-sas/webpage/auth/models/user.model';
import { UsersService } from '../../services';

@Component({
  selector: 'app-user-select',
  templateUrl: './user-select.component.html',
  styleUrls: ['./user-select.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSelectComponent),
      multi: true,
    },
  ],
})
export class UserSelectComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @Input() returnID = true;

  _value: number = null;

  searchChange$ = new BehaviorSubject('');
  isDisabled = false;
  loading = false;
  users: UserModel[] = [];
  optionListSubscrition: Subscription = null;

  constructor(private usersService: UsersService, private changeDetectorRef: ChangeDetectorRef) {}

  onSearch(value: string): void {
    this.searchChange$.next(value);
  }

  onChange: any = () => {};
  onTouched: any = () => {};

  ngOnInit() {
    const optionList$: Observable<Resource<UserModel>> = this.getOptions();

    this.optionListSubscrition = optionList$.subscribe((results) => {
      this.users = results.data;
      this.loading = false;
    });
  }

  ngOnDestroy() {
    this.optionListSubscrition.unsubscribe();
  }

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(this._value);
    this.onTouched();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any) {
    if (value) {
      this.loadUser(value);
    }

    if (value !== undefined) {
      this.value = value;
      this.onChange(this.value);
    }
  }

  private getOptions(): Observable<Resource<UserModel>> {
    return this.searchChange$.asObservable().pipe(
      tap(() => (this.loading = true)),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((search) =>
        this.usersService.searchUsers(search).pipe(
          first(),
          finalize(() => this.changeDetectorRef.detectChanges())
        )
      )
    );
  }

  private loadUser(user_id: number) {
    this.loading = true;
    this.usersService
      .getUser(user_id)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((results) => {
        this.users = results.data;
        this.value = user_id;
        this.changeDetectorRef.detectChanges();
      });
  }
}
