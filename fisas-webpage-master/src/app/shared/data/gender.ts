import { Gender } from '../models';

export const GenderOptions: { value: Gender, label: string }[] = [
  { value: 'F', label: 'GENDER.FEMININE' },
  { value: 'M', label: 'GENDER.MALE' },
  { value: 'U', label: 'GENDER.OTHER' },
];
