export interface IScheduleTable {
  id: number;
  day_week: string;
  day_period: string;
  day_week_translation?: String;
  day_period_translation?: String;
}

export interface Schedule {
  schedule_id: number;
  time_begin: string;
  time_end: string;
}

export const scheduleTable: IScheduleTable[] = [
  {
    id: 1,
    day_week: 'domingo',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SUNDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 2,
    day_week: 'domingo',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SUNDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 3,
    day_week: 'domingo',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SUNDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 4,
    day_week: 'segunda',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.MONDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 5,
    day_week: 'segunda',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.MONDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 6,
    day_week: 'segunda',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.MONDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 7,
    day_week: 'terça',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.TUESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 8,
    day_week: 'terça',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.TUESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 9,
    day_week: 'terça',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.TUESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 10,
    day_week: 'quarta',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.WEDNESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 11,
    day_week: 'quarta',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.WEDNESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 12,
    day_week: 'quarta',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.WEDNESDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 13,
    day_week: 'quinta',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.THURSDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 14,
    day_week: 'quinta',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.THURSDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 15,
    day_week: 'quinta',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.THURSDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 16,
    day_week: 'sexta',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.FRIDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 17,
    day_week: 'sexta',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.FRIDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 18,
    day_week: 'sexta',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.FRIDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
  {
    id: 19,
    day_week: 'sábado',
    day_period: 'manhã',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SATURDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.MORNING',
  },
  {
    id: 20,
    day_week: 'sábado',
    day_period: 'tarde',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SATURDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.AFTERNOON',
  },
  {
    id: 21,
    day_week: 'sábado',
    day_period: 'noite',
    day_week_translation: 'SHARED.SCHEDULE.WEEK_DAYS.SATURDAY',
    day_period_translation: 'SHARED.SCHEDULE.PERIOD.NIGHT',
  },
];
