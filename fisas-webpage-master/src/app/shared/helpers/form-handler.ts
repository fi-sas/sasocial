import { OnDestroy } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms"

import { Subject } from "rxjs";

export abstract class FormHandler implements OnDestroy {
  protected destroy$: Subject<void> = new Subject<void>();

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  protected isFormValid(form: FormGroup): boolean {
    this.touchFields(form);
    form.markAsTouched();
    return form.valid;
  }

  private touchFields(formGroup: FormGroup) {
    for (const controlName in formGroup.controls) {
      const control = formGroup.get(controlName);
      if (control instanceof FormControl) {
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.touchFields(control);
      }
    }
  }
}
