import { TemplateRef } from '@angular/core';

export interface BarOptions {
  translate: string;
  content?: string | TemplateRef<{}> | any;
  disable: boolean;
}
