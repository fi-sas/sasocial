import { FormElement } from '@fi-sas/webpage/shared/interfaces/form-element';

export interface FormBody {
  title: string;
  formElements: FormElement[];
}
