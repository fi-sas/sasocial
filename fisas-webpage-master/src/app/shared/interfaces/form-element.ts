import { InputOptions } from '@fi-sas/webpage/shared/interfaces/input-options';

export interface FormElement {
  stepDividerTitle?: string;
  title?: string;
  subtitle?: string;
  required: boolean;
  formControlName: string;
  reviewProp?: string;
  validators?: any[];
  defaultValue?: any;
  hiddenTo?: string;
  hiddenConditions?: any[];
  discard?: boolean;
  inputOptions: InputOptions;
}
