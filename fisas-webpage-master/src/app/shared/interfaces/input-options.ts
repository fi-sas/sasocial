import { Values } from '@fi-sas/webpage/shared/interfaces/values';

export enum InputDataType {
  decimal = 'decimal',
  percentage = 'percentage',
  minutes = 'minutes',
}

export enum InputType {
  rgpd = 'rgpd',
  phone = 'phone',
  radio = 'radio',
  input = 'input',
  tabInput = 'tabInput',
  select = 'select',
  selectGroup = 'selectGroup',
  // number = 'number',
  checkbox = 'checkbox',
  switch = 'switch',
  date = 'date',
  year = 'year',
  textarea = 'textarea',
  daterange = 'daterange',
  time = 'time',
  // color = 'color',
  uploadFile = 'uploadfile',
  // uploadImage = 'uploadimage',
  // uploadImageGallery = 'uploadimagegallery',
  // multilanguage = 'multilanguage',
  // arrayfield = 'arrayfield',
  // monthPicker = 'monthPicker',
  // inputGroup = 'inputGroup',
  none = 'none'
}

export interface InputOptions {
  type: InputType;
  disabled?: boolean;
  values?: any[];
  valuesService?: any;
  valuesServiceSelect?: any;
  valuesServicePropName?: string;
  propValue?: string;
  placeholder?: string;
  more_option?: boolean;
  addOnBefore?: string;
  addOnAfter?: string;
  dateFormat?: string;
  checkedChildren?: string;
  unCheckedChildren?: string;
  label?: string;
  mask?: string;
  dropSpecialCharacters?: boolean;
  linkText?: string;
  linkUrl?: string;
  textareaRows?: number;
  inputDataType?: InputDataType;
  selectDataForInput?: string;
  selectMultiple?: boolean;
  disabledDate?: any;
  storeValueSelected?: boolean;
  storeData?: boolean;
  storeKey?: string;
  storeProp?: string;
  saveUrl?: boolean;
  filterFileTypes?: string[];
  disableOptions?: any;
}
