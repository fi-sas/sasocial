import { FormElement } from '@fi-sas/webpage/shared/interfaces/form-element';

export interface Steps {
  title: string;
  showStatus: boolean;
  formElements: FormElement[];
}
