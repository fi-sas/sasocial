export interface Values {
  value: any;
  translate?: string;
  translates?: any[];
  checked?: boolean;
  disabled?: boolean;
}
