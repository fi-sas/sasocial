export interface AcademicYear {
  academic_year: string;
  current: boolean;
  end_date: string;
  id: number;
  start_date: string;
}
