export const SERVICE_IDS = {
  ACCOMODATION: 1,
  CURRENT_ACCOUNT: 11,
  FOOD: 2,
  MOBILITY: 3,
  PRIVATE_ACCOMMODATION: 15,
  SOCIAL_SUPPORT: 17,
  UBIKE: 13,
  VOLUNTEERING: 28,
  QUEUE: 21,
  GERAL: 22
};

export interface ConfigurationModel {
  id: number;
  key: string;
  value?: string;
  created_at?: string;
  updated_at?: string;
}
