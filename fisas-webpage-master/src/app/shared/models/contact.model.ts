import { TranslationModel } from '@fi-sas/webpage/shared/models/translation.model';

export class ContactModel {
  id: number;
  email: string;
  url: string;
  translations: TranslationModel[];
  created_at: Date;
  updated_at: Date;
}
