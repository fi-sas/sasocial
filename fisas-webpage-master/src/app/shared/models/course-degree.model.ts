import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';

export class CourseDegreeModel {
  id: number;
  name: string;
  active: boolean;
  created_at: Date;
  updated_at: Date;
  courses: CourseModel[];
}
