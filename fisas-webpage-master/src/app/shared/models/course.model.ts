import { SchoolModel } from '@fi-sas/webpage/shared/models/school.model';
import { CourseDegreeModel } from '@fi-sas/webpage/shared/models/course-degree.model';
import { OrganicUnitModel } from './organic-unit.model';

export class CourseModel {
  id: number;
  name: string;
  active: boolean;
  acronym: string;
  organic_unit_id: number;
  course_degree_id: number;
  created_at: Date;
  updated_at: Date;
  organicUnit?: OrganicUnitModel;
  course_degree?: CourseDegreeModel;
  courseDegree?: CourseDegreeModel
}
