export interface ExternalEntity {
  user: {
    id: number;
    name: string;
  },
  id: number;
  name: string;
  email: string;
  gender: string;
  address: string;
  postal_code: string;
  city: string;
  country: string;
  phone: string;
  tin: string;
  status: string;
  active: true,
  user_id: number;
  file_id?: number;
  function_description: string;
  description: string;
}
