export class FileMessageModel {
  upload: Message;
  delete: Message;
  load: Message;
}

class Message {
  sucess?: string;
  error: string;
}
