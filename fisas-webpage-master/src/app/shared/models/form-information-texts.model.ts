
export class FormInformationTextsModel {
    id: number;
    name: string;
    key: string;
    visible: boolean;
    translations: TranslationsModelFormInformation[]=[];
}

export class TranslationsModelFormInformation {
    language_id: number;
    info: string;
    form_information_id: number;
}