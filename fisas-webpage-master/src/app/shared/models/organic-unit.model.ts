export class OrganicUnitModel {
  id: number;
  active: boolean;
  name: string;
  code: number;
  created_at: Date;
  updated_at: Date;
}
