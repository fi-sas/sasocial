import { CourseModel } from '@fi-sas/webpage/shared/models/course.model';

export class SchoolModel {
  id: number;
  name: string;
  active: boolean;
  acronym: string;
  created_at: Date;
  updated_at: Date;
  code: string;
  courses?: CourseModel[];
}
