import { TranslationModel } from "@fi-sas/webpage/modules/media/models/file.model";

export class ServiceConfigurationModel {
    active: boolean;
    created_at: Date;
    updated_at: Date;
    translations: TranslationModel[];
    id?: number;
  }
  