export class TaxModel {
  id: number;
  name: string;
  description: string;
  tax_value: number;
  active: boolean;
  created_at: Date;
  updated_at: Date;
}
