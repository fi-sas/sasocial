import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MessageType, UiService } from '@fi-sas/webpage/core/services/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@fi-sas/webpage/shared/interfaces/store';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class ApplicationDataService {

  applicationData = new BehaviorSubject({});
  applicationID = new BehaviorSubject(0);
  urlRedirect = new BehaviorSubject(null);
  submitAction = new BehaviorSubject(null);
  updateAction = new BehaviorSubject(null);
  propsToDelete =  new BehaviorSubject([]);
  submitSuccess = new BehaviorSubject(null);
  storage = new BehaviorSubject([]);
  isUpdate = new BehaviorSubject(false);
  formControlNames = new BehaviorSubject([]);

  loadingSubmit = new BehaviorSubject(false);

  constructor(private translateService: TranslateService,
              private uiService: UiService,
              private router: Router,
              private modalService: NzModalService) { }

  loadingSubmitObservable(): Observable<boolean> {
    return this.loadingSubmit.asObservable();
  }

  setLoadingSubmit(loading: boolean) {
    this.loadingSubmit.next(loading);
  }

  createApplication(data) {
    this.applicationData.next(data);
  }

  createFormControlNames(data) {
    this.formControlNames.next(data);
  }

  saveUrlRedirect(data) {
    this.urlRedirect.next(data);
  }

  createPropsToDelete(props) {
    this.propsToDelete.next(props);
  }

  changeToUpdate(data) {
    this.isUpdate.next(data);
  }

  clearApplication() {
    this.propsToDelete.next([]);
    this.applicationData.next({});
  }

  saveApplicationID(id: number) {
    this.applicationID.next(id);
  }

  resetApplicationID() {
    this.applicationID.next(0);
  }

  applicationObservable(): Observable<object> {
    return this.applicationData.asObservable();
  }

  saveSubmitSuccess(success) {
    this.submitSuccess.next(success);
  }

  submitSuccessObservable(): Observable<object> {
    return this.submitSuccess.asObservable();
  }

  getApplicationData(): object {
    return this.applicationData.getValue();
  }

  getApplicationProp(prop: string): any {

    const app = this.applicationData.getValue();

    if (app !== null) {

      if (app.hasOwnProperty(prop)) {
        return app[prop];
      } else {
        return null;
      }

    } else {
      return null;
    }
  }

  patchApplication(patch: {[key: string]: any}) {
    const app = this.applicationData.getValue();
    this.applicationData.next(Object.assign(app, patch));
  }


  setApplicationPropValue(prop: string, values: any): void {
    const app = this.applicationData.getValue();
    app[prop] = values[prop];
    this.applicationData.next(app);
  }

  setApplicationProps(props: object): void {
    const app = this.applicationData.getValue();
    const entries = Object.entries(props);
    for (const [prop, value] of entries) {
       app[prop] = value;
    }
    this.applicationData.next(app);
  }

  removeApplicationProps(): any {
      const app = this.applicationData.getValue();
      const props = this.propsToDelete.getValue();
      props.forEach(propName => {
        if (app.hasOwnProperty(propName)) {
          delete app[propName];
        }
      });
      return app;
  }

  removeProps(): any {
    const propID = 'id';
    const app = this.applicationData.getValue();
    const props = this.formControlNames.getValue();
    const newApp = {};
    this.saveApplicationID(app[propID]);
    props.forEach(propName => {
      if (app.hasOwnProperty(propName)) {
        newApp[propName] = app[propName];
      }
    });
    const propsDiscard = this.propsToDelete.getValue();
    propsDiscard.forEach(propName => {
      if (newApp.hasOwnProperty(propName)) {
        delete newApp[propName];
      }
    });
    return newApp;
  }

  saveSubmitAction(action) {
    this.submitAction.next(action);
  }

  saveUpdateAction(action) {
    this.updateAction.next(action);
  }

  callSubmitActionModal() {
      this.modalService.confirm({
        nzTitle: !this.isUpdate.getValue() ? this.translateService.instant('FORM_STEPS.SUBMIT_CONFIRM') :
                                             this.translateService.instant('FORM_STEPS.UPDATE_CONFIRM'),
        nzOnOk: () => this.callSubmitAction()
      });
  }

  callSubmitAction() {
    const submitAction = this.submitAction.getValue();
    const updateAction = this.updateAction.getValue();

    if (!this.isUpdate.getValue()) {

      this.setLoadingSubmit(true);
      submitAction(this.removeApplicationProps()).subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('FORM_STEPS.SUBMIT_SUCCESS')
        );
        this.clearApplication();
        this.clearStorage();
        this.saveSubmitSuccess(true);
        this.setLoadingSubmit(false);
        const url = this.urlRedirect.getValue();
        if (url !== null) {
          this.router.navigateByUrl(url);
        }
      }, () => {
        this.setLoadingSubmit(false);
        this.saveSubmitSuccess(false);
      });
    } else {
      this.setLoadingSubmit(true);
      updateAction(this.applicationID.getValue(), this.removeProps()).subscribe(() => {
        this.uiService.showMessage(
          MessageType.success,
          this.translateService.instant('FORM_STEPS.UPDATE_SUCCESS')
        );
        this.clearApplication();
        this.clearStorage();
        this.resetApplicationID();
        this.setLoadingSubmit(false);
        const url = this.urlRedirect.getValue();
        if (url !== null) {
          this.router.navigateByUrl(url);
        }
      }, () => {
        this.setLoadingSubmit(false);
      });
    }
  }


  storeData(data: Store) {
    const store = this.storage.getValue();
    store.push(data);
    this.storage.next(store);
  }

  removeStoreDatabyKey(key: string) {
    const store = this.storage.getValue();
    const index = store.findIndex(s => s.formControlName === key);
    if (index !== -1) {
      store.splice(index, 1);
      this.storage.next(store);
    }
  }

  getStoreDatabyKey(key: string): any {
    const store = this.storage.getValue();
    return store.find(s => s.formControlName === key);
  }

  clearStorage() {
    this.storage.next( []);
  }

  storageObservable(): Observable<Store[]> {
    return this.storage.asObservable();
  }

  getStorage(): Store[] {
    return this.storage.getValue();
  }
}
