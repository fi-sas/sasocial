import { TestBed } from '@angular/core/testing';

import { ConfigurationsService } from './configurations.service';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { OverlayModule } from '@angular/cdk/overlay';

describe('ConfigurationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      OverlayModule,
      NgZorroAntdModule,
      HttpClientTestingModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      })
    ]
  }));

  it('should be created', () => {
    const service: ConfigurationsService = TestBed.get(ConfigurationsService);
    expect(service).toBeTruthy();
  });
});
