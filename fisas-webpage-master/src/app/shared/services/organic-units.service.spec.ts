import { TestBed } from '@angular/core/testing';

import { OrganicUnitsService } from './organic-units.service';

describe('OrganicUnitsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrganicUnitsService = TestBed.get(OrganicUnitsService);
    expect(service).toBeTruthy();
  });
});
