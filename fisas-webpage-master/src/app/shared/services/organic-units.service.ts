import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { FiResourceService, FiUrlService, Resource } from '@fi-sas/core';
import { OrganicUnitModel } from '@fi-sas/webpage/shared/models/organic-unit.model';

@Injectable({
  providedIn: 'root',
})
export class OrganicUnitsService {
  constructor(private resourceService: FiResourceService, private urlService: FiUrlService) {}

  organicUnits(): Observable<Resource<OrganicUnitModel>> {
    let params = new HttpParams();
    params = params.set('offset', '0');
    params = params.set('limit', '-1');
    params = params.set('query[active]', 'true');
    params = params.set('sort', 'name');
    return this.resourceService.list<OrganicUnitModel>(this.urlService.get('INFRASTRUCTURE.ORGANIC_UNITS'), { params });
  }

  organicUnitsbySchools(idUnit?: number): Observable<Resource<OrganicUnitModel>> {
    let params = new HttpParams();
    params = params.set('query[is_school]', 'true');
    params = idUnit ? params.set('query[id]', idUnit.toString()) : params;
    params = params.set('sort', 'name');
    return this.resourceService.list<OrganicUnitModel>(this.urlService.get('INFRASTRUCTURE.ORGANIC_UNITS'), { params });
  }

  organicUnitsSchools(): Observable<Resource<OrganicUnitModel>> {
    let params = new HttpParams();
    params = params.set('query[is_school]', 'true');
    params = params.set('query[active]', 'true');
    params = params.set('sort', 'name');
    return this.resourceService.list<OrganicUnitModel>(this.urlService.get('INFRASTRUCTURE.ORGANIC_UNITS'), { params });
  }
}
