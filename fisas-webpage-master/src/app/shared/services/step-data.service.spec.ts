import { TestBed } from '@angular/core/testing';

import { StepDataService } from './step-data.service';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { OverlayModule } from '@angular/cdk/overlay';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@fi-sas/webpage/app.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StepDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
        OverlayModule,
        NgZorroAntdModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
      })
      ]
  }));

  it('should be created', () => {
    const service: StepDataService = TestBed.get(StepDataService);
    expect(service).toBeTruthy();
  });
});
