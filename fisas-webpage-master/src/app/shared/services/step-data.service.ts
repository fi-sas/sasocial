import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Steps } from '@fi-sas/webpage/shared/interfaces/steps';
import { hasOwnProperty } from 'tslint/lib/utils';
import { first } from 'rxjs/operators';
import { Values } from '@fi-sas/webpage/shared/interfaces/values';
import { ApplicationDataService } from '@fi-sas/webpage/shared/services/application-data.service';
import { FiUrlService } from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class StepDataService {
  steps: Steps[] = [];

  stepsData = new BehaviorSubject(this.steps);
  formControlsNames = [];
  propToRemove = [];
  urlMedia = this.urlService.get('MEDIA.UPLOAD');

  constructor(
    private applicationDaraService: ApplicationDataService,
    private urlService: FiUrlService
  ) {}

  private checkPropName(inputOptions): string {
    if (hasOwnProperty(inputOptions, 'valuesServicePropName')) {
      return inputOptions.valuesServicePropName;
    } else {
      return 'translations';
    }
  }

  createSteps(data: Steps[]) {
    data.forEach((step, i) => {
      if (hasOwnProperty(step, 'formElements')) {
        step.formElements.forEach((formElement, j) => {
          this.formControlsNames.push(formElement.formControlName);
          if (hasOwnProperty(formElement, 'discard')) {
            if (formElement.discard === true) {
              this.propToRemove.push(formElement.formControlName);
            }
          }
          if (hasOwnProperty(formElement, 'inputOptions')) {
            if (hasOwnProperty(formElement.inputOptions, 'valuesService')) {
              formElement.inputOptions
                .valuesService()
                .pipe(first())
                .subscribe((dataService) => {
                  const valuesArray: Values[] = [];
                  if (hasOwnProperty(formElement.inputOptions, 'storeData')) {
                    if (formElement.inputOptions.storeData) {
                      this.applicationDaraService.storeData({
                        formControlName: formElement.formControlName,
                        data: dataService.data,
                      });
                    }
                  }
                  const disableOptions =
                    formElement.inputOptions.disableOptions !== undefined
                      ? formElement.inputOptions.disableOptions
                      : this.disableOptionsDefault;
                  dataService.data.map((val) => {
                    const prop = this.checkPropName(formElement.inputOptions);
                    if (prop === 'translations') {
                      valuesArray.push({
                        value: val.id,
                        translates: val[prop],
                        disabled: disableOptions(val),
                      });
                    } else {
                      valuesArray.push({
                        value: val.id,
                        translate: val[prop],
                        disabled: disableOptions(val),
                      });
                    }
                  });
                  data[i].formElements[j].inputOptions.values = valuesArray;
                });
            }
          }
        });
      }
    });
    this.applicationDaraService.createFormControlNames(this.formControlsNames);
    this.applicationDaraService.createPropsToDelete(this.propToRemove);
    this.stepsData.next(data);
  }

  stepsObservable(): Observable<object> {
    return this.stepsData.asObservable();
  }

  getStepsData(): Steps[] {
    return this.stepsData.getValue();
  }

  setDataBySelect(dataToGo: string, valueSelected: any): void {
    const stepData = this.stepsData.getValue();
    stepData.forEach((step, i) => {
      if (hasOwnProperty(step, 'formElements')) {
        step.formElements.forEach((formElement, j) => {
          if (hasOwnProperty(formElement, 'inputOptions')) {
            if (formElement.formControlName === dataToGo) {
              if (
                hasOwnProperty(formElement.inputOptions, 'valuesServiceSelect')
              ) {
                formElement.inputOptions
                  .valuesServiceSelect(valueSelected)
                  .pipe(first())
                  .subscribe((dataService) => {
                    const valuesArray: Values[] = [];
                    if (hasOwnProperty(formElement.inputOptions, 'storeData')) {
                      if (formElement.inputOptions.storeData) {
                        this.applicationDaraService.storeData({
                          formControlName: formElement.formControlName,
                          data: dataService.data,
                        });
                      }
                    }
                    dataService.data.map((val) => {
                      const prop = this.checkPropName(formElement.inputOptions);
                      if (prop === 'translations') {
                        valuesArray.push({
                          value: val.id,
                          translates: val[prop],
                        });
                      } else {
                        valuesArray.push({
                          value: val.id,
                          translate: val[prop],
                        });
                      }
                    });
                    stepData[i].formElements[
                      j
                    ].inputOptions.values = valuesArray;
                  });
              }
            }
          }
        });
      }
    });
    this.stepsData.next(stepData);
  }

  setFileUrlBySelect(id): void {
    const stepData = this.stepsData.getValue();

    id = parseInt(id, 10);

    if (!isNaN(id)) {
      stepData.forEach((step, i) => {
        if (hasOwnProperty(step, 'formElements')) {
          step.formElements.forEach((formElement, j) => {
            if (hasOwnProperty(formElement, 'inputOptions')) {
              if (hasOwnProperty(formElement.inputOptions, 'storeKey')) {
                const store = this.applicationDaraService.getStorage();
                const dataFound = store.find(
                  (data) =>
                    data.formControlName === formElement.inputOptions.storeKey
                );
                if (dataFound !== undefined) {
                  const dtFound = dataFound.data.find((dt) => dt.id === id);

                  if (
                    hasOwnProperty(dtFound, formElement.inputOptions.storeProp)
                  ) {
                    stepData[i].formElements[j].inputOptions.linkUrl = dtFound[formElement.inputOptions.storeProp].url;
                  }
                }
              }
            }
          });
        }
      });
      this.stepsData.next(stepData);
    }
  }

  setLinkUrl(step: number, formElement: string, storeKey: string): void {

    const stepData = this.stepsData.getValue();
    const store = this.applicationDaraService.getStorage();

    const dataFound = store.find(
      (data) =>
        data.formControlName === storeKey
    );

    if (dataFound !== undefined) {

      if (hasOwnProperty(stepData[step], 'formElements')) {
        const indexFound = stepData[step].formElements.findIndex(
          (data) => data.formControlName === formElement
        );
        if (indexFound !== -1) {
          if (hasOwnProperty(stepData[step].formElements[indexFound], 'inputOptions')) {
            if (hasOwnProperty(dataFound, 'data')) {
              if (dataFound.data.length !== 0) {
                  stepData[step].formElements[indexFound].inputOptions.linkUrl =  dataFound.data[0]['regulationFile'].url;

                  this.stepsData.next(stepData);
              }
            }
          }
        }
      }
   }
  }

  disableOptionsDefault(option: any): boolean {
    if (option) {
      return false;
    }
  }
}
