import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IConfig, NgxMaskModule } from 'ngx-mask';
import { NgZorroAntdModule, NZ_I18N, pt_PT } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { LogoComponent } from './helpers/logo/logo.component';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { WP_CONFIGURATION } from './../app.config';
import {
  BarMenuComponent,
  ButtonComponent,
  DateTabComponent,
  FileUploadComponent,
  MonthSelectorComponent,
  NamedFileUploadComponent,
  NoConnectionComponent,
  RadioButtonComponent,
  ScheduleDetailComponent,
  ScheduleHoursComponent,
  ScheduleInputComponent,
  ServicePresentationTextComponent,
  SystemErrorComponent,
  TabContainerComponent,
  UserSelectComponent,
} from './components';
import {
  DataUnitsPipe,
  EuroCurrenyPipe,
  HasPermissionPipe,
  HourConvertPipe,
  LanguageChangePipe,
  LocalizedDatePipe,
  ReplacePipe,
  YesOrNoPipe,
} from './pipes';
import { ModalSuccessReportComponent } from './components/modal-success-report/modal-success-report.component';
import { GenderBeautifyPipe } from './pipes/gender-beautify.pipe';
import { GetFromObjectsArrayPipe } from './pipes/get-from-objects-array.pipe';
import { FileNameEditComponent } from './components/file-name-edit/file-name-edit.component';

export let options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    BarMenuComponent,
    ButtonComponent,
    DataUnitsPipe,
    DateTabComponent,
    EuroCurrenyPipe,
    FileUploadComponent,
    GenderBeautifyPipe,
    GetFromObjectsArrayPipe,
    HasPermissionPipe,
    HourConvertPipe,
    LanguageChangePipe,
    LocalizedDatePipe,
    LogoComponent,
    ModalSuccessReportComponent,
    MonthSelectorComponent,
    NamedFileUploadComponent,
    NoConnectionComponent,
    RadioButtonComponent,
    ReplacePipe,
    ScheduleDetailComponent,
    ScheduleHoursComponent,
    ScheduleInputComponent,
    ServicePresentationTextComponent,
    SystemErrorComponent,
    TabContainerComponent,
    UserSelectComponent,
    YesOrNoPipe,
    FileNameEditComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(options),
    NgZorroAntdModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-Pt' },
    { provide: NZ_I18N, useValue: pt_PT },
    { provide: OPTIONS_TOKEN, useValue: WP_CONFIGURATION },
  ],
  exports: [
    BarMenuComponent,
    ButtonComponent,
    DataUnitsPipe,
    DateTabComponent,
    EuroCurrenyPipe,
    FileUploadComponent,
    FormsModule,
    GenderBeautifyPipe,
    GetFromObjectsArrayPipe,
    HasPermissionPipe,
    HourConvertPipe,
    HttpClientModule,
    LanguageChangePipe,
    LocalizedDatePipe,
    LogoComponent,
    ModalSuccessReportComponent,
    MonthSelectorComponent,
    NamedFileUploadComponent,
    NgxMaskModule,
    NgZorroAntdModule,
    RadioButtonComponent,
    ReactiveFormsModule,
    ReplacePipe,
    ScheduleDetailComponent,
    ScheduleInputComponent,
    ServicePresentationTextComponent,
    TranslateModule,
    UserSelectComponent,
    YesOrNoPipe,
  ],
  entryComponents: [NoConnectionComponent, SystemErrorComponent, ScheduleHoursComponent, FileNameEditComponent],
})
export class SharedModule { }
