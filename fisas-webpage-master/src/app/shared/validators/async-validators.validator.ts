import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';

import { debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { UsersService } from '../services';

export function emailExistsValidator(userService: UsersService): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    if (!control.value) {
      return;
    }
    return userService
      .searchUserByEmail(control.value)
      .pipe(
        debounceTime(100),
        map((result) => (result.data || []).length ? null : { emailDoesNotExist: true }),
      );
  };
}
