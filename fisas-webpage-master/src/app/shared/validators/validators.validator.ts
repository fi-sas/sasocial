import { AbstractControl, FormGroup } from '@angular/forms';

import * as moment from 'moment';

export function compareTwoFieldsValidation(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.match) {
      // return if another validator has already found an error on the matchingControl
      return;
    }
    if (!matchingControl.value) return;
    if (control.value == matchingControl.value) {
      matchingControl.setErrors({ match: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function fieldsMustBeEquals(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.match) {
      // return if another validator has already found an error on the matchingControl
      return;
    }
    if (!matchingControl.value) return;
    if (control.value != matchingControl.value) {
      matchingControl.setErrors({ different: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function trimValidation(control: AbstractControl): { [key: string]: boolean } | null {
  if (control.value) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { required: true };
  }
}

export function dateAfter(startDateControlName: string, endDateControlName: string) {
  return (formGroup: FormGroup) => {
    const startDateControl = formGroup.controls[startDateControlName];
    const endDateControl = formGroup.controls[endDateControlName];

    if (endDateControl.errors && !endDateControl.errors.match) {
      // return if another validator has already found an error on the endDateControl
      return;
    }
    if (!endDateControl.value) return;

    if (moment(startDateControl.value).isBefore(endDateControl.value)) {
      return null;
    }

    return { dateAfter: true };
  };
}
