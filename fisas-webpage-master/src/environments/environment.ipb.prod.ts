const gateway_url = 'https://sas-social.ipb.pt';
export const environment = {
  production: false,
  version : require('../../package.json').version,
  institute: 'IPB',
  instituteFull: 'Instituto Politécnico de Bragança',
  institueDescription: '',
  hasSSO: false,
  hasCC: false,
  useLDAP: true,

  showQrCodeLink: false,

  mobility_regulation_url: '',


  version_check_url: gateway_url.concat('/assets/version.json'),
  auth_api_url: gateway_url + '/api/authorization',
  conf_api_url: gateway_url + '/api/configuration',
  media_api_url: gateway_url + '/api/media',
  media_url: gateway_url + '/media',
  communication_api_url: gateway_url + '/api/communication',
  alimentation_api_url: gateway_url + '/api/alimentation',
  bus_api_url: gateway_url + '/api/bus',
  private_accommodation_api_url: gateway_url + '/api/private_accommodation',
  accommodation_api_url: gateway_url + '/api/accommodation',
  infrastructure_api_url: gateway_url + '/api/infrastructure',
  u_bike_api_url: gateway_url + '/api/u_bike',
  sport_api_url: gateway_url + '/api/sport',
  calendar_api_url: gateway_url + '/api/calendar',
  current_account_api_url: gateway_url + '/api/current_account',
  payments_api_url: gateway_url + '/api/payments',
  notifications_api_url: gateway_url + '/api/notifications',
  queues_api_url: gateway_url + '/api/queue',
  gamification_api_url: gateway_url + '/api/gamification',
  social_support_api_url: gateway_url + '/api/social_scholarship',
  volunteering_api_url: gateway_url + '/api/volunteering',
  mentoring_api_url: gateway_url + '/api/mentoring',
  mobility_api_url: gateway_url + '/api/bus',
  reports_api_url: gateway_url + '/api/reports',
  health_api_url: gateway_url + '/api/health',
  emergency_fund_api_url: gateway_url + "/api/emergency_fund"
};
