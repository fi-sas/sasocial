## [1.45.7](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.6...v1.45.7) (2022-05-20)


### Bug Fixes

* **reservations:** revenue report query ([ce5180e](https://gitlab.com/fi-sas/fisas_alimentation/commit/ce5180e5a6b3e19fa7a95be707357b8d58a341fe))

## [1.45.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.5...v1.45.6) (2022-05-10)


### Bug Fixes

* **menu:** copy week fix undefined access ([f282bbb](https://gitlab.com/fi-sas/fisas_alimentation/commit/f282bbbd0b4f938d45d62be72c0d65060e98d38c))

## [1.45.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.4...v1.45.5) (2022-05-10)


### Bug Fixes

* **menus:** fix forEach misspelled ([8de113f](https://gitlab.com/fi-sas/fisas_alimentation/commit/8de113f9315a0132fc2d37086678a17e3b8686f1))

## [1.45.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.3...v1.45.4) (2022-05-10)


### Bug Fixes

* **menu:** calculate nutrients without show allergens ([939f3dc](https://gitlab.com/fi-sas/fisas_alimentation/commit/939f3dc1fbf13998567a80bcfad98652645cfdd5))

## [1.45.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.2...v1.45.3) (2022-05-10)


### Bug Fixes

* **menu:** fix configurations call ([cc06dcc](https://gitlab.com/fi-sas/fisas_alimentation/commit/cc06dccd1f83fa6435db9fca63cd29a75424583f))

## [1.45.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.1...v1.45.2) (2022-05-10)


### Bug Fixes

* **menu:** add show_allergens config to menus list ([dca28f1](https://gitlab.com/fi-sas/fisas_alimentation/commit/dca28f10232aef1d54677b404088df9ab0ea123d))

## [1.45.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.45.0...v1.45.1) (2022-05-10)


### Performance Improvements

* **menus:** change requests of copy week ([7b20968](https://gitlab.com/fi-sas/fisas_alimentation/commit/7b20968aa5c46b15bb5235a189dac15f3bd8dd60))

# [1.45.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.44.1...v1.45.0) (2022-05-06)


### Features

* **configurations:** add updated_by info ([1ef9b5d](https://gitlab.com/fi-sas/fisas_alimentation/commit/1ef9b5ddbb46e440fe48499a32d94ca3bd83ee2a))

## [1.44.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.44.0...v1.44.1) (2022-05-06)


### Performance Improvements

* **applications:** remove relateds from target notificaitons actions ([eb79181](https://gitlab.com/fi-sas/fisas_alimentation/commit/eb791819552d33f5e966b4f1d2c28cbc0fe72119))

# [1.44.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.43.0...v1.44.0) (2022-05-04)


### Features

* **food:** add alergen configuration ([39ffe85](https://gitlab.com/fi-sas/fisas_alimentation/commit/39ffe8586313a3a1578d1dac1daf09d9cc20c1fb))

# [1.43.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.42.3...v1.43.0) (2022-05-04)


### Features

* **dish-types:** add exclude user profile from menu ([2162992](https://gitlab.com/fi-sas/fisas_alimentation/commit/2162992a425f1e5e8cade770bb56300b97199219))

## [1.42.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.42.2...v1.42.3) (2022-04-07)


### Bug Fixes

* **entities:** filter active on entities users ([e317aa2](https://gitlab.com/fi-sas/fisas_alimentation/commit/e317aa26aa90fc837f356213bfa484ed29851016))

## [1.42.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.42.1...v1.42.2) (2022-04-07)


### Bug Fixes

* add active filter to users entities ([ba55106](https://gitlab.com/fi-sas/fisas_alimentation/commit/ba55106e7bbcff9de44457a1944f490bedb0eab1))

## [1.42.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.42.0...v1.42.1) (2022-04-01)


### Bug Fixes

* **menu:** change method of not logged dashboard ([28a7e89](https://gitlab.com/fi-sas/fisas_alimentation/commit/28a7e897e394fb3e5dd1325c479dbf1322629b7b))

# [1.42.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.41.3...v1.42.0) (2022-03-28)


### Bug Fixes

* **fiscal_entity:** return only user actives entities ([1af8179](https://gitlab.com/fi-sas/fisas_alimentation/commit/1af81795f142d0eface41b5c2f70e432838a7a0a))


### Features

* **sasocial:** add sasocial service ([7588962](https://gitlab.com/fi-sas/fisas_alimentation/commit/7588962d22e3dde4ecb48c29641860d341edc792))

## [1.41.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.41.2...v1.41.3) (2022-03-21)


### Bug Fixes

* **reservations:** add price to allowed fields ([f720db1](https://gitlab.com/fi-sas/fisas_alimentation/commit/f720db1790a8f67d259f4341bc15402ed0c2cfea))

## [1.41.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.41.1...v1.41.2) (2022-03-11)


### Bug Fixes

* **families:** produts are now optional ([a99ca78](https://gitlab.com/fi-sas/fisas_alimentation/commit/a99ca78c2e5788b5f7e07c0867b1d309b69b7330))

## [1.41.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.41.0...v1.41.1) (2022-02-21)


### Bug Fixes

* **reservations:** meal filter nullable ([b6fb0bd](https://gitlab.com/fi-sas/fisas_alimentation/commit/b6fb0bd27ddfe10908c3d951580fc29b7b619771))

# [1.41.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.40.1...v1.41.0) (2022-02-10)


### Features

* **products:** add families to product crud ([cb4cba7](https://gitlab.com/fi-sas/fisas_alimentation/commit/cb4cba7a1cd1fdaaa6789c8944dc7fd7d22b81d1))

## [1.40.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.40.0...v1.40.1) (2022-02-03)


### Bug Fixes

* **reports:** filter services by entity on reports ([78f1db2](https://gitlab.com/fi-sas/fisas_alimentation/commit/78f1db2aed4fba7c537f65312ff8c2eb8ddb1edb))

# [1.40.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.39.0...v1.40.0) (2022-02-02)


### Features

* **reservations:** add field price to reservations ([c4762f6](https://gitlab.com/fi-sas/fisas_alimentation/commit/c4762f6225c5663bf295d444cf655decdf18610b))

# [1.39.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.38.0...v1.39.0) (2022-02-01)


### Features

* **orders:** add new report revenue ([7ba059b](https://gitlab.com/fi-sas/fisas_alimentation/commit/7ba059bbac6f211a85c88801b2c331c7a321b84d))
* **reservations:** add new report counts ([388f62e](https://gitlab.com/fi-sas/fisas_alimentation/commit/388f62ebf614d5d5dbaa1f8c5d52652c3062838d))

# [1.38.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.37.1...v1.38.0) (2022-01-14)


### Features

* **menu:** remove unavailable dishs from menu ([3e907a8](https://gitlab.com/fi-sas/fisas_alimentation/commit/3e907a81d31c54b44da41affa86aeb9ff5aac087))

## [1.37.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.37.0...v1.37.1) (2022-01-06)


### Bug Fixes

* **menu:** product sanatize check stock composition ([ab3e790](https://gitlab.com/fi-sas/fisas_alimentation/commit/ab3e790124faef081ca8149475438380e9e67acb))

# [1.37.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.36.0...v1.37.0) (2021-12-06)


### Features

* address publications, new filters correction ([f133099](https://gitlab.com/fi-sas/fisas_alimentation/commit/f133099afc4f8b547bc51a184363e46b2df06c73))

# [1.36.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.5...v1.36.0) (2021-12-03)


### Features

* address publications, new filters ([ccf708d](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccf708d7530dd461591aef552159484a6b108613))

## [1.35.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.4...v1.35.5) (2021-12-02)


### Bug Fixes

* **entities:** add users withRelated ([3603c1e](https://gitlab.com/fi-sas/fisas_alimentation/commit/3603c1e35887b6c50ba786db20d5153e246c0e22))

## [1.35.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.3...v1.35.4) (2021-12-02)


### Bug Fixes

* **menu:** return only cantten services ([081fae7](https://gitlab.com/fi-sas/fisas_alimentation/commit/081fae788c03b73151cb588c46f7533755d6b5da))

## [1.35.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.2...v1.35.3) (2021-12-02)


### Bug Fixes

* **entities:** return all entities with scope ([5c0ba95](https://gitlab.com/fi-sas/fisas_alimentation/commit/5c0ba95f3a3330bc7b3aa4540fa4f3ce9ba7dd93))
* **menu:** remove schools without services ([31f1e12](https://gitlab.com/fi-sas/fisas_alimentation/commit/31f1e1245cbd4f3d36ce7f845c987ff338a3c4a2))

## [1.35.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.1...v1.35.2) (2021-11-23)


### Bug Fixes

* **stocks:** minimal_stock params convert ([34a3721](https://gitlab.com/fi-sas/fisas_alimentation/commit/34a3721af0f4ae6c6c04be94babc3f2c4459321e))

## [1.35.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.35.0...v1.35.1) (2021-11-23)


### Bug Fixes

* **stocks:** add minimal_stock to report ([5148bbc](https://gitlab.com/fi-sas/fisas_alimentation/commit/5148bbccd06629a2cf7bad893a637bdf34adfdc4))

# [1.35.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.34.1...v1.35.0) (2021-11-22)


### Features

* **stocks:** add report action ([61c5887](https://gitlab.com/fi-sas/fisas_alimentation/commit/61c5887a9f7ffabc9e3236b527dbd5dfa6033cb4))

## [1.34.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.34.0...v1.34.1) (2021-11-09)


### Bug Fixes

* **reservations:** add served_by_user withRelated ([0ebafd3](https://gitlab.com/fi-sas/fisas_alimentation/commit/0ebafd3f07eb0411242bb989eb08bcecd3df7d7d))

# [1.34.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.33.0...v1.34.0) (2021-11-09)


### Features

* **reservations:** add pack meal id to reservation ([61cf8e9](https://gitlab.com/fi-sas/fisas_alimentation/commit/61cf8e934fe6057fb6e4e85c203cea4ab4674195))

# [1.33.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.6...v1.33.0) (2021-10-20)


### Features

* **dish-types:** add pack_available field ([b46c8b4](https://gitlab.com/fi-sas/fisas_alimentation/commit/b46c8b4e5d19df714489cbf2f0ac616feb5983a8))

## [1.32.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.5...v1.32.6) (2021-10-19)


### Bug Fixes

* **services_families:** finds without query ([5e97f3e](https://gitlab.com/fi-sas/fisas_alimentation/commit/5e97f3ecd3c3bd12d9aa296d1eb06aa7b46d5cb4))

## [1.32.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.4...v1.32.5) (2021-10-19)


### Bug Fixes

* **menu:** remove map schoolws ([f8078ce](https://gitlab.com/fi-sas/fisas_alimentation/commit/f8078ce3b2f7e057bc7e5d8d2424a2392b1fc729))

## [1.32.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.3...v1.32.4) (2021-10-19)


### Bug Fixes

* add minor performance fixs ([b9c2703](https://gitlab.com/fi-sas/fisas_alimentation/commit/b9c2703a48511ba6d2093a6be8b1d951fb5f6e9f))

## [1.32.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.2...v1.32.3) (2021-10-14)


### Bug Fixes

* **reservations:** maximum hour for cancel ([14d25c6](https://gitlab.com/fi-sas/fisas_alimentation/commit/14d25c6a451f3d5927d6619206663574dd438834))

## [1.32.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.1...v1.32.2) (2021-10-13)


### Bug Fixes

* **stocks:** lower_stocks false ([dfb337c](https://gitlab.com/fi-sas/fisas_alimentation/commit/dfb337c79ae05276051d237b9802cf59b68689bd))

## [1.32.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.32.0...v1.32.1) (2021-10-13)


### Bug Fixes

* **stock:** lower_stock validate boolean ([d66261b](https://gitlab.com/fi-sas/fisas_alimentation/commit/d66261bf8b3cbb9ec8566bc5861fd8c71ded19bc))

# [1.32.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.5...v1.32.0) (2021-10-13)


### Features

* **stocks:** add filter lower stock ([c9dac5b](https://gitlab.com/fi-sas/fisas_alimentation/commit/c9dac5b306088833d1eb8556752d6c7cfe034172))

## [1.31.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.4...v1.31.5) (2021-10-11)


### Bug Fixes

* **menu:** fix price time calculation ([0676611](https://gitlab.com/fi-sas/fisas_alimentation/commit/067661196723346097300108319e4b40e7ceb12b))

## [1.31.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.3...v1.31.4) (2021-10-11)


### Bug Fixes

* **stocks:** validate fiscal entity on grouped endpoint ([bd1df83](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd1df8390836672a1a7005d1a979daba304e844c))

## [1.31.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.2...v1.31.3) (2021-10-11)


### Bug Fixes

* **stocks:** add validation of entity ([977ace5](https://gitlab.com/fi-sas/fisas_alimentation/commit/977ace530b9a3bcf3d454123e307806509435013))

## [1.31.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.1...v1.31.2) (2021-10-11)


### Bug Fixes

* menu lint ([d1887a6](https://gitlab.com/fi-sas/fisas_alimentation/commit/d1887a63cd6bbc2f13a2197d73169e0d9d5faad7))

## [1.31.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.31.0...v1.31.1) (2021-10-11)


### Bug Fixes

* **menu:** change dish prices calculation method ([ca59dc2](https://gitlab.com/fi-sas/fisas_alimentation/commit/ca59dc258438221793674bb3703f242ad2087296))
* **stocks:** add pagination info to sotks grouped ([dfe5433](https://gitlab.com/fi-sas/fisas_alimentation/commit/dfe543321565b3db12b425d286b0be7aab5badd5))

# [1.31.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.8...v1.31.0) (2021-10-08)


### Features

* **stocks:** add grouped endpoint ([b8311ca](https://gitlab.com/fi-sas/fisas_alimentation/commit/b8311cada5b8aad2e4cb3ec435b9745ecd183408))

## [1.30.8](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.7...v1.30.8) (2021-10-03)


### Bug Fixes

* **families:** withRealte products error ([6ccd800](https://gitlab.com/fi-sas/fisas_alimentation/commit/6ccd800f272668f4b1579804a897fe2154dd3cc5))

## [1.30.7](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.6...v1.30.7) (2021-10-01)


### Bug Fixes

* **families:** add unit to products for withRelated ([8c40956](https://gitlab.com/fi-sas/fisas_alimentation/commit/8c409563e5d1173ec07d396a3cf49af21ebea560))

## [1.30.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.5...v1.30.6) (2021-09-30)


### Bug Fixes

* **resservations:** count error 500 ([956985b](https://gitlab.com/fi-sas/fisas_alimentation/commit/956985bb9beadf23bce1a4bdfc7fcc98cb5faa9e))

## [1.30.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.4...v1.30.5) (2021-09-30)


### Bug Fixes

* **reservations:** add pos_operations to reservations ([e79be8c](https://gitlab.com/fi-sas/fisas_alimentation/commit/e79be8c3757419fa47dd48bae3302707de7be953))

## [1.30.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.3...v1.30.4) (2021-09-30)


### Bug Fixes

* **order_line:** fix remove stock ([7e70760](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e70760f1d5579833592246f9e0fb1e34e74d6ab))

## [1.30.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.2...v1.30.3) (2021-09-29)


### Bug Fixes

* **reservations:** missing first row ([64c82d9](https://gitlab.com/fi-sas/fisas_alimentation/commit/64c82d94272981bc9a3dbf6bdced7fdfe75160db))

## [1.30.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.1...v1.30.2) (2021-09-29)


### Bug Fixes

* **reservations:** cancel ignore maixmun hour in BO and POS ([9495964](https://gitlab.com/fi-sas/fisas_alimentation/commit/9495964db3d7e7115b2e754d23a815b3f2552669))

## [1.30.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.30.0...v1.30.1) (2021-09-29)


### Bug Fixes

* **reservations:** countAll remove canceled from reserved count ([7ed59a4](https://gitlab.com/fi-sas/fisas_alimentation/commit/7ed59a4154d6bc5fbef3457a6a4bc50a315b45fa))

# [1.30.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.29.2...v1.30.0) (2021-09-29)


### Features

* **menus:** add new action add menu_dish ([992e72a](https://gitlab.com/fi-sas/fisas_alimentation/commit/992e72aab3f41e542bba8092ed6aef91c4f192c6))

## [1.29.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.29.1...v1.29.2) (2021-09-29)


### Bug Fixes

* **menu:** fix products menu ([d89ef8e](https://gitlab.com/fi-sas/fisas_alimentation/commit/d89ef8eb8d7a7edd0ece9bcf3c6a1e63f3f408e0))

## [1.29.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.29.0...v1.29.1) (2021-09-29)


### Bug Fixes

* **menu:** products active and visible ([d5a5c95](https://gitlab.com/fi-sas/fisas_alimentation/commit/d5a5c95202eae798aa3cc1df945f5d998ee0844e))

# [1.29.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.28.4...v1.29.0) (2021-09-29)


### Bug Fixes

* **menu:** remove products non visible from menu ([e81cab8](https://gitlab.com/fi-sas/fisas_alimentation/commit/e81cab84cfb0a0ecf15c08c073b8ef3d0875c26b))


### Features

* **reservations:** add all reservations count ([0b19b1e](https://gitlab.com/fi-sas/fisas_alimentation/commit/0b19b1e79e3bbe7d7cde72b1e14ff5778de362ac))

## [1.28.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.28.3...v1.28.4) (2021-09-28)


### Bug Fixes

* **menu:** meal date info ([16d712e](https://gitlab.com/fi-sas/fisas_alimentation/commit/16d712ebb92b0481929f3c4041ca1c5dded1528a))

## [1.28.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.28.2...v1.28.3) (2021-09-28)


### Bug Fixes

* **stocks:** change quantity stock field ([1c86ee6](https://gitlab.com/fi-sas/fisas_alimentation/commit/1c86ee65cb97b73c4b821bb43ab88cd3a24d40e9))

## [1.28.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.28.1...v1.28.2) (2021-09-23)


### Bug Fixes

* reservation remove negative available menu dish ([30f3d66](https://gitlab.com/fi-sas/fisas_alimentation/commit/30f3d66aa1c5cd19b7380bfe72ebbbad63daa81d))

## [1.28.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.28.0...v1.28.1) (2021-09-23)


### Bug Fixes

* **orders:** cancel movement items ([22aff2a](https://gitlab.com/fi-sas/fisas_alimentation/commit/22aff2a490050f9cbf2cd6d304490643fc4e0141))

# [1.28.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.27.1...v1.28.0) (2021-09-22)


### Features

* **orders:** add unserve action ([c505701](https://gitlab.com/fi-sas/fisas_alimentation/commit/c505701ac73034742bd08dcf0fce75249a83557e))

## [1.27.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.27.0...v1.27.1) (2021-09-22)


### Bug Fixes

* **menu:** change location name of meals ([6759d19](https://gitlab.com/fi-sas/fisas_alimentation/commit/6759d19d42619a8f865178f18fd94a2cb7d4094b))

# [1.27.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.26.2...v1.27.0) (2021-09-20)


### Features

* **stocks:** add inital stock operations orders ([4b1f5de](https://gitlab.com/fi-sas/fisas_alimentation/commit/4b1f5def0522b7ae564645f6f96e933e70ac1069))

## [1.26.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.26.1...v1.26.2) (2021-09-15)


### Bug Fixes

* **wharehouses:** change user_access endpoint ([29f8647](https://gitlab.com/fi-sas/fisas_alimentation/commit/29f8647dfe154a820c79260eb2a3eac5ed00dc65))

## [1.26.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.26.0...v1.26.1) (2021-09-15)


### Bug Fixes

* **menu:** recents only return orders of sevice ([1da6275](https://gitlab.com/fi-sas/fisas_alimentation/commit/1da6275accb7d07484f08e57a03416b209fa1ccc))
* **wharehouses:** get services in user access ([1a2accd](https://gitlab.com/fi-sas/fisas_alimentation/commit/1a2accdf01f5432979cf16d53ed400be9e8b8dbc))

# [1.26.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.25.0...v1.26.0) (2021-09-14)


### Features

* **stock:** add product_id to stock operation ([30524bd](https://gitlab.com/fi-sas/fisas_alimentation/commit/30524bdb9d56c149de7dce51f8b1e21b166d0c00))

# [1.25.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.24.0...v1.25.0) (2021-09-14)


### Features

* **stock:** add new features to stock operations ([8fbf68e](https://gitlab.com/fi-sas/fisas_alimentation/commit/8fbf68e22c4ff977edb43bd3a92d6666d49e5d54))

# [1.24.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.23.0...v1.24.0) (2021-09-14)


### Features

* **stock:** add withRelated user ([bfd6c5e](https://gitlab.com/fi-sas/fisas_alimentation/commit/bfd6c5e9089735cc3c775ccc37b4002fb64f8c88))

# [1.23.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.22.1...v1.23.0) (2021-09-14)


### Features

* **stocks:** save transfer stock info ([28259a7](https://gitlab.com/fi-sas/fisas_alimentation/commit/28259a7ad487c0f4ac86e5bd79bad8fa5dacee40))

## [1.22.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.22.0...v1.22.1) (2021-09-14)


### Bug Fixes

* **stocks:** improve create movement action ([74d84b4](https://gitlab.com/fi-sas/fisas_alimentation/commit/74d84b469e07fce536cfe3c25dcaf3632f89983e))

# [1.22.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.21.0...v1.22.0) (2021-09-14)


### Features

* **stocks:** add new stock movement ([7e1f372](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e1f372a9b1e23d03f0dea8ed544480896c9fa4e))

# [1.21.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.20.2...v1.21.0) (2021-09-14)


### Features

* **stocks:** add action to return lotes of product ([ee1a28e](https://gitlab.com/fi-sas/fisas_alimentation/commit/ee1a28e70647d74c2c53507a48b9cb12292b03a9))

## [1.20.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.20.1...v1.20.2) (2021-09-13)


### Bug Fixes

* **stocks-operation:** change scopes to stocks ([b7bb500](https://gitlab.com/fi-sas/fisas_alimentation/commit/b7bb500d0425c1bffe642e1015bfc2661f6f7d30))

## [1.20.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.20.0...v1.20.1) (2021-09-10)


### Bug Fixes

* **menu:** refactor recents products bar ([36bcbe8](https://gitlab.com/fi-sas/fisas_alimentation/commit/36bcbe8a238424551d242e9ab1d5a322515e55c9))

# [1.20.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.19.1...v1.20.0) (2021-09-10)


### Features

* refactor menu canteen action ([91d683a](https://gitlab.com/fi-sas/fisas_alimentation/commit/91d683adfa1b7315db6e755e3a29214a6fc437c2))

## [1.19.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.19.0...v1.19.1) (2021-09-10)


### Bug Fixes

* **stocks:** fix acess stocks hook ([d3903bd](https://gitlab.com/fi-sas/fisas_alimentation/commit/d3903bdc2900fdba3c9585339614301945a1b886))

# [1.19.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.18.3...v1.19.0) (2021-09-09)


### Bug Fixes

* **menu:** price endpoint pos ([b650ceb](https://gitlab.com/fi-sas/fisas_alimentation/commit/b650ceb5db778bebe8e2e9ca64cf1b3397abd680))
* **stocks:** add nullable date expire ([f9baf70](https://gitlab.com/fi-sas/fisas_alimentation/commit/f9baf7023a0e51fedf6fa2bfbee186b29e9ed6e5))


### Features

* **migrations:** update stocks ([fed8d24](https://gitlab.com/fi-sas/fisas_alimentation/commit/fed8d241aa5ec9f5c3d2a29ce40c8cd23fa95ad0))
* **wharehouses:** add enpoint get wharehouses user access ([0038f2c](https://gitlab.com/fi-sas/fisas_alimentation/commit/0038f2c16293448ff6faf2bff34dbb2849f8807b))

## [1.18.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.18.2...v1.18.3) (2021-09-02)


### Bug Fixes

* **stocks:** filter stocks by wharehouse ([aba5890](https://gitlab.com/fi-sas/fisas_alimentation/commit/aba5890478a812213bc53c13789021f464231d76))

## [1.18.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.18.1...v1.18.2) (2021-08-30)


### Bug Fixes

* **menus_dishs:** remove condition ([87111b3](https://gitlab.com/fi-sas/fisas_alimentation/commit/87111b38c318d7a4b3759e508c67781bbbdeaed3))
* **menus_dishs:** save dish update action ([9386064](https://gitlab.com/fi-sas/fisas_alimentation/commit/93860647007de435f52d775e162a4643f2a07c84))
* **menus_dishs:** small fix ([362351f](https://gitlab.com/fi-sas/fisas_alimentation/commit/362351f7003af441af3dd1509c9e52ce72c68fd9))

## [1.18.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.18.0...v1.18.1) (2021-08-30)


### Bug Fixes

* **menu:** change verify dadte from menu ([6b03726](https://gitlab.com/fi-sas/fisas_alimentation/commit/6b0372621bde60a8e97218f8b77b145c6c696b99))

# [1.18.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.17.2...v1.18.0) (2021-08-30)


### Bug Fixes

* **families:** problem with cache ([421fc91](https://gitlab.com/fi-sas/fisas_alimentation/commit/421fc911c72fa00839079564d2140dc26bcf93ff))
* **menus_dishs:** problem with update menu ([7287e0d](https://gitlab.com/fi-sas/fisas_alimentation/commit/7287e0df55f446c5436d11559e800f209fa385ff))


### Features

* **reservations:** add has_canceled validation ([a14f99b](https://gitlab.com/fi-sas/fisas_alimentation/commit/a14f99b62c56a52058e2a5c5fc3690118c90eade))

## [1.17.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.17.1...v1.17.2) (2021-08-30)


### Bug Fixes

* **menu:** disponibility for menu dont check day of week ([6bfceee](https://gitlab.com/fi-sas/fisas_alimentation/commit/6bfceeef2fcbee4bc3c6c474cd7e51da27c28a03))

## [1.17.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.17.0...v1.17.1) (2021-08-30)


### Bug Fixes

* **menu:** remove week day validation on menus ([ca504b0](https://gitlab.com/fi-sas/fisas_alimentation/commit/ca504b02a9f6ed3ec8e2ddcf567b31576f3fdb18))

# [1.17.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.16.1...v1.17.0) (2021-08-30)


### Bug Fixes

* **menus:** add validation ([674c841](https://gitlab.com/fi-sas/fisas_alimentation/commit/674c841cacf1328f35123ea4138a32b80ba14fa4))
* **reservations:** add new field meal ([4dfa016](https://gitlab.com/fi-sas/fisas_alimentation/commit/4dfa0161422c344b95b335bea7b3d40204a8e99c))


### Features

* **migrations:** alter table reservation ([a3b9670](https://gitlab.com/fi-sas/fisas_alimentation/commit/a3b96708e2b27a994633b08007d39302b4643f9c))
* **stocks:** add query and validation on list ([26fc25e](https://gitlab.com/fi-sas/fisas_alimentation/commit/26fc25ea52c8fb986e488cac79be86c11ff35419))
* **wharehouses_users:** add action for get wharehouse users have access ([14ce16e](https://gitlab.com/fi-sas/fisas_alimentation/commit/14ce16eb3978d7fc49330116fb4a25ff7092030a))

## [1.16.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.16.0...v1.16.1) (2021-08-26)


### Bug Fixes

* **menu:** get schools only returns the services of the device ([29345f7](https://gitlab.com/fi-sas/fisas_alimentation/commit/29345f78a3c559cd93c6208c2cca4c137569b107))

# [1.16.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.15.2...v1.16.0) (2021-08-26)


### Features

* **services:** remove  devices from others bars ([dfba3d1](https://gitlab.com/fi-sas/fisas_alimentation/commit/dfba3d12a870a2dfdff396e1a83aa9407c6a9a77))

## [1.15.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.15.1...v1.15.2) (2021-08-26)


### Bug Fixes

* **menu:** check disponibility same day is a true flag ([81c27c1](https://gitlab.com/fi-sas/fisas_alimentation/commit/81c27c12362aea924b5fc434d6009fe2002a5440))

## [1.15.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.15.0...v1.15.1) (2021-08-25)


### Bug Fixes

* **reservations:** update validation ([f8c0fba](https://gitlab.com/fi-sas/fisas_alimentation/commit/f8c0fbaad5a000b7da36a55a31424906b188c478))

# [1.15.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.15...v1.15.0) (2021-08-24)


### Bug Fixes

* **reservations:** add served_by ([261873e](https://gitlab.com/fi-sas/fisas_alimentation/commit/261873e09c03936a31e6df5e756a940d897e41b9))
* **reservations:** update count reservations ([7034cc9](https://gitlab.com/fi-sas/fisas_alimentation/commit/7034cc958ec17c8fed230a27eb8c69072b55a3a8))


### Features

* **compounds:** add relation units ([fa8ddca](https://gitlab.com/fi-sas/fisas_alimentation/commit/fa8ddca7517aecedc758c2e94c03f6c3b027c68f))
* **migration:** update table product ([25bdc64](https://gitlab.com/fi-sas/fisas_alimentation/commit/25bdc6457e79c778327eaaf45a1a758488982aff))
* **migration:** update table products and reservations ([f68f76f](https://gitlab.com/fi-sas/fisas_alimentation/commit/f68f76fa550a9349d03d382b5aba2ce5c27b31d7))
* **products:** add validate code in update ([57e5c13](https://gitlab.com/fi-sas/fisas_alimentation/commit/57e5c130d58bfae5715a6bf7e69a53528a3cd918))
* **products:** add validation price and tax create/update product ([b96b9d9](https://gitlab.com/fi-sas/fisas_alimentation/commit/b96b9d98413903d6b1ee55fadc934d8ff3c2bf29))

## [1.14.15](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.14...v1.14.15) (2021-08-05)


### Bug Fixes

* **orders:** update the package MS Core ([030e8d7](https://gitlab.com/fi-sas/fisas_alimentation/commit/030e8d7cd9fbd0fe3758afd44afe834967979478))

## [1.14.14](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.13...v1.14.14) (2021-08-05)


### Bug Fixes

* **menu:** disable time expireation bar ([7e3e846](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e3e846e3d32cbb0f709bf559fb359b8474d707a))

## [1.14.13](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.12...v1.14.13) (2021-08-05)


### Bug Fixes

* **menu:** getPricesMethod on bar products ([9e85f92](https://gitlab.com/fi-sas/fisas_alimentation/commit/9e85f9255444bf9dc122d81130815245d7118a48))

## [1.14.12](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.11...v1.14.12) (2021-08-05)


### Bug Fixes

* **menu:** fix product price method ([8369ce2](https://gitlab.com/fi-sas/fisas_alimentation/commit/8369ce21d3a9e7cb9d9c106327d5cbda98011152))

## [1.14.11](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.10...v1.14.11) (2021-08-05)


### Bug Fixes

* **menu:** show the corret prices to user on BAR ([0e3b012](https://gitlab.com/fi-sas/fisas_alimentation/commit/0e3b01286ad8ba664c8c8a89775e54e4d1be8af2))

## [1.14.10](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.9...v1.14.10) (2021-08-05)


### Bug Fixes

* **menu:** change expires in ([67e8766](https://gitlab.com/fi-sas/fisas_alimentation/commit/67e8766b124bc5267392f5d153faac45b3495a5c))

## [1.14.9](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.8...v1.14.9) (2021-08-04)


### Bug Fixes

* **menu:** change addCart expires in to moment.js ([3dae13d](https://gitlab.com/fi-sas/fisas_alimentation/commit/3dae13deb4ab858a1a276bd9a52078293c686907))

## [1.14.8](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.7...v1.14.8) (2021-08-04)


### Bug Fixes

* pre production changes ([03a726c](https://gitlab.com/fi-sas/fisas_alimentation/commit/03a726c7df618ae08c48faf8ab62e687f23878cc))

## [1.14.7](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.6...v1.14.7) (2021-08-02)


### Bug Fixes

* **products:** validate product code per fiscal entity ([260ca3f](https://gitlab.com/fi-sas/fisas_alimentation/commit/260ca3f7fb3dc629dad27f9df1aa9d30455a3ebd))

## [1.14.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.5...v1.14.6) (2021-07-16)


### Bug Fixes

* **reservations:** reservations only return >= NOW ([52b144a](https://gitlab.com/fi-sas/fisas_alimentation/commit/52b144ad965cdd53436547762c2fa2328d4edc48))

## [1.14.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.4...v1.14.5) (2021-07-16)


### Bug Fixes

* **reservations:** add sort to getUserReservations ([6c15877](https://gitlab.com/fi-sas/fisas_alimentation/commit/6c15877a2f66906da4c0bb328257e213e7ae66bd))

## [1.14.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.3...v1.14.4) (2021-07-16)


### Bug Fixes

* **menu:** add sort to reservations of dashboard ([f9a17fd](https://gitlab.com/fi-sas/fisas_alimentation/commit/f9a17fd183a4f7818d53c0b29c0b5fc156a968dd))

## [1.14.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.2...v1.14.3) (2021-07-13)


### Bug Fixes

* **reservations:** add table name to query ([3b0db7f](https://gitlab.com/fi-sas/fisas_alimentation/commit/3b0db7fa15dbb8de0041513283daa5a3b4803b82))

## [1.14.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.1...v1.14.2) (2021-07-13)


### Bug Fixes

* **menu:** price calculation hour ([9813f5d](https://gitlab.com/fi-sas/fisas_alimentation/commit/9813f5de9c3f5aa9a900ed3416f6a81c202048fc))

## [1.14.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.14.0...v1.14.1) (2021-07-13)


### Bug Fixes

* **reservations:** add table name to raw query ([9522ba3](https://gitlab.com/fi-sas/fisas_alimentation/commit/9522ba3d6ae4579dd660c7dcf084180b3fe324f3))

# [1.14.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.13.0...v1.14.0) (2021-07-12)


### Bug Fixes

* **dish:** refactor calculatedAllergen action ([a812307](https://gitlab.com/fi-sas/fisas_alimentation/commit/a81230793b0711144068fe0d4d75ea9da2613dec))


### Features

* **reservations:** add query by meal ([9ee1921](https://gitlab.com/fi-sas/fisas_alimentation/commit/9ee19211b74a0cde82b6767f9e8421a53039d73a))

# [1.13.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.12.0...v1.13.0) (2021-06-29)


### Bug Fixes

* **fiscal_entities:** rename scope ([4b07282](https://gitlab.com/fi-sas/fisas_alimentation/commit/4b0728295ed65cb2e13e2820c2eeefc8fdb8a99f))
* **menus:** rename scopes ([3c09dc4](https://gitlab.com/fi-sas/fisas_alimentation/commit/3c09dc40ede5103db4b5c3185ddf8424a17578e2))
* **order:** rename scope ([a08ae28](https://gitlab.com/fi-sas/fisas_alimentation/commit/a08ae28f76d8433fb51b1a601249d82d86f87236))
* **order_line:** rename scope ([e01f68d](https://gitlab.com/fi-sas/fisas_alimentation/commit/e01f68dd16cc2e3a0f9239ce5291232f5af5441d))
* **reservations:** rename scopes ([682357b](https://gitlab.com/fi-sas/fisas_alimentation/commit/682357bde69c92f9530b32c285f54a6ba8d8e343))
* **wharehouses:** rename scopes ([d5c2777](https://gitlab.com/fi-sas/fisas_alimentation/commit/d5c27770c201b503c74697a2886941fe55859217))


### Features

* **dish_type:** add missing scopes ([11cf8b3](https://gitlab.com/fi-sas/fisas_alimentation/commit/11cf8b3a51843da4ca97f184bac9625eefe5ac3f))
* **families:** add missing scope ([5959a5a](https://gitlab.com/fi-sas/fisas_alimentation/commit/5959a5a77593ba2fdf746577e1848f87bfb6bf3d))
* **fiscal_entities:** add missing scopes ([9bcb16d](https://gitlab.com/fi-sas/fisas_alimentation/commit/9bcb16d4b1b11f9fff459afd80612cc40daeb328))
* **menus:** add missing scopes ([c12fba3](https://gitlab.com/fi-sas/fisas_alimentation/commit/c12fba33791c5ecbcae5a8e7352cae8bb3b9967c))
* **orders:** add missing scopes ([b48c905](https://gitlab.com/fi-sas/fisas_alimentation/commit/b48c90590290e41e6e7f6f42a25f9673756c325e))
* **orders_line:** add missing scopes ([a814f13](https://gitlab.com/fi-sas/fisas_alimentation/commit/a814f13a8daea21471614a7acdd2bfccaaaa31af))
* **products:** add missing scopes ([73e21eb](https://gitlab.com/fi-sas/fisas_alimentation/commit/73e21ebe9615bfc1ee729d2c25208f7b84b57483))
* **reservations:** add missing scopes ([7e3f564](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e3f564f43db96a95eb2413274418266fe51c90f))
* **scopes:** add missing scopes ([a37d6a3](https://gitlab.com/fi-sas/fisas_alimentation/commit/a37d6a3fb7996ffea2217a8699f6af68d0da1aed))
* **services:** add missing scope ([039563b](https://gitlab.com/fi-sas/fisas_alimentation/commit/039563bfb569738bbf34f6b169bc0f254680b28d))
* **stocks:** add missing scopes ([7c24e96](https://gitlab.com/fi-sas/fisas_alimentation/commit/7c24e965f373c7ca790c75abca0bbe63d8cfb306))

# [1.12.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.11.1...v1.12.0) (2021-06-29)


### Features

* **nutrients:** add translations search ([2cc5e4c](https://gitlab.com/fi-sas/fisas_alimentation/commit/2cc5e4c4bc7ef213ab59fa8b38cdcf94d98b34cc))

## [1.11.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.11.0...v1.11.1) (2021-06-28)


### Bug Fixes

* **menu:** checkDisponibility change hour to Lisbon ([14b7717](https://gitlab.com/fi-sas/fisas_alimentation/commit/14b7717e542b31563687fc123184e90308ee7cf3))

# [1.11.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.9...v1.11.0) (2021-06-24)


### Bug Fixes

* **menu:** hours disponibility update ([ef00078](https://gitlab.com/fi-sas/fisas_alimentation/commit/ef00078f2fc5f66c74b4de11b80fa0daa8cf5033))


### Features

* **complements:** add search by name, description ([834b60a](https://gitlab.com/fi-sas/fisas_alimentation/commit/834b60a3847dbbb9731954ca5ad51475dd8ae43a))
* **dishs:** add search by name and description ([3d032bf](https://gitlab.com/fi-sas/fisas_alimentation/commit/3d032bfa17722d16070566eb8964ecad6647c01d))
* **dishs_types:** search by name ([b59b8eb](https://gitlab.com/fi-sas/fisas_alimentation/commit/b59b8eb5e79ee571fcfe288c901e3775dc3bf37b))
* **families:** search by name ([d2f6452](https://gitlab.com/fi-sas/fisas_alimentation/commit/d2f6452807c84b8244c0f0a2ddcaec74aa0b662a))

## [1.10.9](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.8...v1.10.9) (2021-06-22)


### Bug Fixes

* **reservations:** count endpoint ([49dd9d9](https://gitlab.com/fi-sas/fisas_alimentation/commit/49dd9d94d409d012532801bdee643f111fd393c0))

## [1.10.8](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.7...v1.10.8) (2021-06-17)


### Bug Fixes

* **reservations:** create reservations by quantity ([2877c3e](https://gitlab.com/fi-sas/fisas_alimentation/commit/2877c3e93d8f629d862954ef11f563ac059e17a2))

## [1.10.7](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.6...v1.10.7) (2021-06-14)

### Bug Fixes

- **menu-dishs:** clear cache on price variation update ([b58d21a](https://gitlab.com/fi-sas/fisas_alimentation/commit/b58d21a2a0cdd215292ffdde2810dd2b4c18964a))

## [1.10.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.5...v1.10.6) (2021-06-11)

### Bug Fixes

- **menu:** add logs to getPrices ([b21494b](https://gitlab.com/fi-sas/fisas_alimentation/commit/b21494b4be89cb3ad7574d5430902b644c2dc6c4))

## [1.10.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.4...v1.10.5) (2021-06-11)

### Bug Fixes

- **menu_dish:** clear cache on dish-type update ([b61aaaf](https://gitlab.com/fi-sas/fisas_alimentation/commit/b61aaaff2fd992f7e99a459319872ed70b345b54))

## [1.10.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.3...v1.10.4) (2021-06-11)

### Bug Fixes

- **menu:** add logs for price calculation ([9fa0dca](https://gitlab.com/fi-sas/fisas_alimentation/commit/9fa0dca15eb614f02a362d05c095080e6c9c87ca))

## [1.10.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.2...v1.10.3) (2021-06-11)

### Bug Fixes

- **menus:** remove cache when dish-type is updated ([f5dab11](https://gitlab.com/fi-sas/fisas_alimentation/commit/f5dab116380e7e9aed22974abfbfab5815965c0b))

## [1.10.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.1...v1.10.2) (2021-06-07)

### Bug Fixes

- **reservaions:** fix conditions cancel reservation ([7a468bd](https://gitlab.com/fi-sas/fisas_alimentation/commit/7a468bd7af55beb8a4487c450728aaa08767c0f8))
- **reservations:** problem cancel reservation from POS ([6544155](https://gitlab.com/fi-sas/fisas_alimentation/commit/6544155267c1c95f7479a2fc85dc19a535328eb8))

## [1.10.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.10.0...v1.10.1) (2021-06-02)

### Bug Fixes

- **reservations:** date error ([249851a](https://gitlab.com/fi-sas/fisas_alimentation/commit/249851ab6ea5f3b40495d58cb7ad75eda1a53cae))

# [1.10.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.9.1...v1.10.0) (2021-05-31)

### Features

- **stocks:** add pagination in endpoint stocks_operations ([ffbf125](https://gitlab.com/fi-sas/fisas_alimentation/commit/ffbf125562b2f05ff8625d03ab7d483e70df081d))

## [1.9.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.9.0...v1.9.1) (2021-05-28)

### Bug Fixes

- **menu:** update count reservations and calculate max_stock ([8521b3c](https://gitlab.com/fi-sas/fisas_alimentation/commit/8521b3c050b03c22087ed42dce57ce27129f1243))
- **order_lines:** add revert stock_operation ([0bb7eaa](https://gitlab.com/fi-sas/fisas_alimentation/commit/0bb7eaab96a8cf7e419354abd9ca88846df3f1ed))
- **orders:** add remove stock quantity ([1acf3e7](https://gitlab.com/fi-sas/fisas_alimentation/commit/1acf3e7d5a2f18d07541c2979bff4e5ad30dbcfa))
- **reservations:** update field date ([2cf34ad](https://gitlab.com/fi-sas/fisas_alimentation/commit/2cf34ad728a548eb8073129372e9a0909ec68f56))
- **stocks:** endpoint remove mandatory out_stock_reason ([6aff328](https://gitlab.com/fi-sas/fisas_alimentation/commit/6aff3287f4f27299b6f561abed2a9436c05bd56d))

# [1.9.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.8.3...v1.9.0) (2021-05-27)

### Bug Fixes

- **menu:** fix problem calculate stock quantity product ([80e3fd6](https://gitlab.com/fi-sas/fisas_alimentation/commit/80e3fd65f693826a74620632f3ae4373e0125969))
- **services:** remover delete query ([4dbfb98](https://gitlab.com/fi-sas/fisas_alimentation/commit/4dbfb986a2c103ead4a13533f791291cd65e261b))

### Features

- **order:** add logger info ([ba1fcc7](https://gitlab.com/fi-sas/fisas_alimentation/commit/ba1fcc7d8cad34462d8db897baa9b386b5347144))

## [1.8.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.8.2...v1.8.3) (2021-05-26)

### Bug Fixes

- **order_line:** small fix in remove order ([4b5d17c](https://gitlab.com/fi-sas/fisas_alimentation/commit/4b5d17cefa60371a81d08434c6ddcb9907a2a4da))
- **orders_line:** alterar estado da order ao alterar order_line ([f2ee805](https://gitlab.com/fi-sas/fisas_alimentation/commit/f2ee8053a475c58a7a98b9cf828256097a37e906))
- **stocks:** update stocks quantity ([5314722](https://gitlab.com/fi-sas/fisas_alimentation/commit/531472247ae1e2a35664b44e76040e00e5b4e8f1))
- **stocks_operations:** fix quantitys ([9e96ef9](https://gitlab.com/fi-sas/fisas_alimentation/commit/9e96ef9064b12271ac3292f1e3d754b9dddba86c))

## [1.8.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.8.1...v1.8.2) (2021-05-25)

### Bug Fixes

- **orders:** add clear cache ([ee6f6ae](https://gitlab.com/fi-sas/fisas_alimentation/commit/ee6f6ae1317321f1952575d9d6462b2eb222fc35))

## [1.8.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.8.0...v1.8.1) (2021-05-25)

### Bug Fixes

- **families:** add clearcache and stocks filter ([f298c88](https://gitlab.com/fi-sas/fisas_alimentation/commit/f298c880cc203b41033e32ea0bf51cf8b8e2daf7))
- **order:** total_price problem ([6b54315](https://gitlab.com/fi-sas/fisas_alimentation/commit/6b5431564d106b88e7e7732131d680805b4a6473))
- **order:** update waiting endpoint ([521ee23](https://gitlab.com/fi-sas/fisas_alimentation/commit/521ee23234bb83d13db039554e108039947bacff))

# [1.8.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.7.3...v1.8.0) (2021-05-21)

### Bug Fixes

- **dishs_types:** add visibility ([fc7a2bc](https://gitlab.com/fi-sas/fisas_alimentation/commit/fc7a2bcd14f79cbe653dcc4b9f5e206e82cf7790))
- **fiscal_entities:** add published ([a4d2548](https://gitlab.com/fi-sas/fisas_alimentation/commit/a4d25487b23064d47ea71662e50865559131874a))
- **menus:** add visibility ([ecf2cff](https://gitlab.com/fi-sas/fisas_alimentation/commit/ecf2cff5933bc057b3555ef35245dbf78dc2d923))
- **menus:** add visibility ([6cf38c0](https://gitlab.com/fi-sas/fisas_alimentation/commit/6cf38c04ae641d884eff04b468b3b85a45df5ff3))
- **menus:** reduce problem ([e7b4678](https://gitlab.com/fi-sas/fisas_alimentation/commit/e7b4678f7ccc4e75621a6cf13888758e5d8bcbdb))
- **order:** add status ([cf415db](https://gitlab.com/fi-sas/fisas_alimentation/commit/cf415dbc8a827430699b4c6afed24bb7897673e8))
- **order_line:** update validations ([a47f249](https://gitlab.com/fi-sas/fisas_alimentation/commit/a47f24971d189afe075289e27d53f7274baf199d))
- **order_lines:** add validation order ([bd87172](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd87172eb07aa606c321bc2e71e7f31ab2f4638e))
- **orders:** remove get products and update messages ([cb28e50](https://gitlab.com/fi-sas/fisas_alimentation/commit/cb28e50de37c0240d764f9ee2984dde1b0c66708))
- **orders:** remove is_deleted from code ([04f96f0](https://gitlab.com/fi-sas/fisas_alimentation/commit/04f96f0a77569ff70beecaed87c6cb7a3f0e06bd))
- **produtos:** add visibility ([09c800a](https://gitlab.com/fi-sas/fisas_alimentation/commit/09c800a8f548dcc464526f58634de172397a7fd1))
- **recipes_steps:** add file relation ([542a9fc](https://gitlab.com/fi-sas/fisas_alimentation/commit/542a9fc0a7d3ff77ccd1460b7cadda234a3adb3d))
- **reservations:** add visibility ([dda643b](https://gitlab.com/fi-sas/fisas_alimentation/commit/dda643bb2a08f20a6d758b020224f023e5864ef1))
- **stocks:** add visibility ([8af73b0](https://gitlab.com/fi-sas/fisas_alimentation/commit/8af73b0ce6ee96c0e1f69d9747c958a17d4e1ac7))

### Features

- **families:** add stocks in product families ([d4025ed](https://gitlab.com/fi-sas/fisas_alimentation/commit/d4025ed1311d4a2e903415167caad34c2d736537))
- **menu:** add get service by device_id ([0b3840d](https://gitlab.com/fi-sas/fisas_alimentation/commit/0b3840d94951160b113c09cb92feacd3108b0845))
- **migrations:** remove fields and add status to order ([521edc2](https://gitlab.com/fi-sas/fisas_alimentation/commit/521edc235bc373a83f05fed8ab0308172fa6ef15))
- **migrations:** update order and stocks ([c7d3b45](https://gitlab.com/fi-sas/fisas_alimentation/commit/c7d3b45000d1186da8cd6c027eb38ebf530e6878))
- **order_line:** add order_line served, status, item_id, price ([ef13cc9](https://gitlab.com/fi-sas/fisas_alimentation/commit/ef13cc938a1578b84fc22487770e8c2cdcf5e937))
- **orders:** add serve, is_wait and total_price, add endpoints ([aeb5c4e](https://gitlab.com/fi-sas/fisas_alimentation/commit/aeb5c4ef6fe67637bea6c896a2049ce0ea6ee584))
- **stocks:** stock quantity, remove stocks ([494e957](https://gitlab.com/fi-sas/fisas_alimentation/commit/494e95719fe22f8028aadb0dd38827e85818eb4b))
- **stocks_operations:** add relation stocks and oi ([aedf6d2](https://gitlab.com/fi-sas/fisas_alimentation/commit/aedf6d26034b7eb4a0976905bc9eb3113fd3b437))

## [1.7.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.7.2...v1.7.3) (2021-05-18)

### Bug Fixes

- **fiscal_entities:** update addusers and remive users ([c3cc15a](https://gitlab.com/fi-sas/fisas_alimentation/commit/c3cc15accd998572ff115d7984857ec306953368))
- **menus:** copy week ([06b8b8a](https://gitlab.com/fi-sas/fisas_alimentation/commit/06b8b8aa8f69abb1f680d5db7ab14ab6902baf34))
- **menus_dish:** bulk save ([affeae1](https://gitlab.com/fi-sas/fisas_alimentation/commit/affeae10f2bb0e0f9c88c407e2869befa8774b37))
- **menus_dishs:** update menu_dish ([8dbc192](https://gitlab.com/fi-sas/fisas_alimentation/commit/8dbc19211c251a1216a6baad09ff8045f4f7a503))

## [1.7.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.7.1...v1.7.2) (2021-05-17)

### Bug Fixes

- **allergens:** add optional create_at and updated_at ([91a6f4f](https://gitlab.com/fi-sas/fisas_alimentation/commit/91a6f4f2f85e008555a2603e8e1066e17c0124e7))
- **fiscal_entities:** remove validation isManager ([012e57b](https://gitlab.com/fi-sas/fisas_alimentation/commit/012e57b62a2e23dc15924dffc32f8c5c47a078d2))
- **menu:** update copy week ([a75c6ab](https://gitlab.com/fi-sas/fisas_alimentation/commit/a75c6ab42430a65647d209a69de23afee368f6a3))
- **menus:** get allergens actives ([46a055a](https://gitlab.com/fi-sas/fisas_alimentation/commit/46a055a29a157c872fa2c3a08bd55e8e0cf263d4))

## [1.7.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.7.0...v1.7.1) (2021-05-07)

### Bug Fixes

- **families:** fix family search ([10fc516](https://gitlab.com/fi-sas/fisas_alimentation/commit/10fc516fa69f750c265da72a914213b8eb5c4ddc))

# [1.7.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.6.2...v1.7.0) (2021-05-07)

### Features

- **families:** add action to search by produtcs ([a6ec3d6](https://gitlab.com/fi-sas/fisas_alimentation/commit/a6ec3d618d88551f95454a7c00aa3fee82944172))

## [1.6.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.6.1...v1.6.2) (2021-05-07)

### Bug Fixes

- **products:** add translation filters ([c3214a7](https://gitlab.com/fi-sas/fisas_alimentation/commit/c3214a7f1ce3f5f22e7df8af59bb80f1dfc778ff))

## [1.6.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.6.0...v1.6.1) (2021-05-05)

### Bug Fixes

- **menu:** add cart ([2c519c2](https://gitlab.com/fi-sas/fisas_alimentation/commit/2c519c2b97fc12412e106fe2172befd8e5b2a6b5))
- **reservations:** correction in count reservations ([27c5cac](https://gitlab.com/fi-sas/fisas_alimentation/commit/27c5caca171cb36f0066391c272722f343311504))

# [1.6.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.5.0...v1.6.0) (2021-04-21)

### Bug Fixes

- **core:** update fisas core ([070a702](https://gitlab.com/fi-sas/fisas_alimentation/commit/070a7022ba3dd60081e3f20c55a54cb65fa16eb2))
- **menu:** add limit 3 ([09820fc](https://gitlab.com/fi-sas/fisas_alimentation/commit/09820fc062f835b0b49110fe603aea50e4f2a671))
- **menu:** add max quantity cart ([ca865c8](https://gitlab.com/fi-sas/fisas_alimentation/commit/ca865c80aab8bd7fc0422299f61fb389f15b1997))
- **reservations:** add limit to query ([05bdf37](https://gitlab.com/fi-sas/fisas_alimentation/commit/05bdf37a34c7889d2abec32da1bdb86f8f116843))

### Features

- **reservations:** add withrelated ([5bcb93e](https://gitlab.com/fi-sas/fisas_alimentation/commit/5bcb93ecb22d16a955863fc0f29b884b2f2c68dd))
- **services_devices:** add endpoint getservicesbarofdevice ([b17ef19](https://gitlab.com/fi-sas/fisas_alimentation/commit/b17ef1938d0457fd8ad4d2792c3b32ced9e3e53b))

# [1.5.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.4.2...v1.5.0) (2021-04-12)

### Bug Fixes

- **allergens:** translations filter ([83356da](https://gitlab.com/fi-sas/fisas_alimentation/commit/83356da572596e59ede84204083f2aa53ab85ba9))
- **complements_translations:** filter by language ([2c04b8d](https://gitlab.com/fi-sas/fisas_alimentation/commit/2c04b8db1c554674ec8b35d791eedb0c71baf1f9))
- **dishs_dishs_type:** update relations ([68d7070](https://gitlab.com/fi-sas/fisas_alimentation/commit/68d7070f304dd9ff264d2ebf28838bc80d795196))
- **dishs_translations:** filter by language ([06573b8](https://gitlab.com/fi-sas/fisas_alimentation/commit/06573b8b865e6f02cc6db5cbb9010d15da314e95))
- **dishs_type:** filter by language ([4dddca3](https://gitlab.com/fi-sas/fisas_alimentation/commit/4dddca3fcd8c52ef9a93ba1dd80068ee894a0be3))
- **families_translations:** filter by language ([282387b](https://gitlab.com/fi-sas/fisas_alimentation/commit/282387b9e8641129f57aaf26ea36942115938435))
- **menu:** problems in endpoints ([4be133d](https://gitlab.com/fi-sas/fisas_alimentation/commit/4be133d055ce28ab1d8aa9155198964e787d9a98))
- **menus:** remove relation ([18b9816](https://gitlab.com/fi-sas/fisas_alimentation/commit/18b9816bb723cd0b77233ce803c0c763ad13356f))
- **menus:** update menu dishs ([99046ac](https://gitlab.com/fi-sas/fisas_alimentation/commit/99046acd463e90aa62012a7ffd0abf843499bf85))
- **menus_dishs:** update endpoint ([835a02f](https://gitlab.com/fi-sas/fisas_alimentation/commit/835a02ffc62f7d43f8efc378a3da0545701dffad))
- **nutrients_translations:** filter by language ([8178e36](https://gitlab.com/fi-sas/fisas_alimentation/commit/8178e36015a4c5dbbae892bf221dba09258618b1))
- **products_translations:** filter by language ([7575d61](https://gitlab.com/fi-sas/fisas_alimentation/commit/7575d61dfa0d3c1d6a45daba0d85de8cced782a4))
- **units_translations:** filter by language ([8ef470f](https://gitlab.com/fi-sas/fisas_alimentation/commit/8ef470f52f84b4f8d032074161d8fd8c71eb1cab))

### Features

- **allergens:** add cache clear ([7cff55f](https://gitlab.com/fi-sas/fisas_alimentation/commit/7cff55f8ce835d5f24436040300a6962486ccc4a))
- **allergens:** add filter translations ([3c9d637](https://gitlab.com/fi-sas/fisas_alimentation/commit/3c9d6379315f0bfa52f15e65c3ec9e8e7af1ca0b))
- **complements:** add cache clear ([e1c043c](https://gitlab.com/fi-sas/fisas_alimentation/commit/e1c043c511f2166d30b3800b74c13d70259dd23a))
- **dishs:** add cache clear ([77a0bca](https://gitlab.com/fi-sas/fisas_alimentation/commit/77a0bcac051a619bf1f61466bb7f23875f7f934c))
- **dishs:** add query by dish_type_id ([7edd537](https://gitlab.com/fi-sas/fisas_alimentation/commit/7edd5372282d3b8ad041274fa299f8c523328725))
- **dishs_types:** add cache clear ([0de43b3](https://gitlab.com/fi-sas/fisas_alimentation/commit/0de43b3d44154887a3088fc1282a9d0c6984f74a))
- **families:** add cache clear ([bc5856b](https://gitlab.com/fi-sas/fisas_alimentation/commit/bc5856b96d7df6986d6772e8c8d3e543b52d40e9))
- **menu_dishs:** update relations ([24a808a](https://gitlab.com/fi-sas/fisas_alimentation/commit/24a808ac1643387e2b41e624adbac0e38fb51718))
- **nutrients:** add cache clear ([cb76c1f](https://gitlab.com/fi-sas/fisas_alimentation/commit/cb76c1f3b618703174a73f410c501591f8806d62))
- **products:** add cache clear ([5020eec](https://gitlab.com/fi-sas/fisas_alimentation/commit/5020eec2b6952c4737ee984a70331777185c832c))
- **units:** add cache clear ([372d87f](https://gitlab.com/fi-sas/fisas_alimentation/commit/372d87fb5a79f75cf53f8ec091a330c23c01d7ef))

## [1.4.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.4.1...v1.4.2) (2021-04-09)

### Bug Fixes

- **menu:** checkDisponibility change isAfter ([b408624](https://gitlab.com/fi-sas/fisas_alimentation/commit/b4086245463ba97e34eb525209c26c5a187fe003))

## [1.4.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.4.0...v1.4.1) (2021-04-09)

### Bug Fixes

- **menu:** checkDisponibility menu date after ([689f2dd](https://gitlab.com/fi-sas/fisas_alimentation/commit/689f2dd96795722117f9c11c37e38dabe950a79f))

# [1.4.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.3.0...v1.4.0) (2021-04-08)

### Features

- **menu:** add logger ([fb8f505](https://gitlab.com/fi-sas/fisas_alimentation/commit/fb8f5051cd48ebdaf4a7cc4c04646ca12d119bc2))

# [1.3.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.5...v1.3.0) (2021-04-06)

### Bug Fixes

- **menu:** endpoint for by product bar ([b08b374](https://gitlab.com/fi-sas/fisas_alimentation/commit/b08b3744607f594a75dbc2f4485fe72e4a14ffb4))
- **menu:** small fix ([29bee28](https://gitlab.com/fi-sas/fisas_alimentation/commit/29bee28e9ac7b0117a906a0e5c2fc3d6c97a013b))

### Features

- **migrations:** update orders ([d2eeebb](https://gitlab.com/fi-sas/fisas_alimentation/commit/d2eeebb482cfbb7b0e75d9843a8a00c965631595))
- **orders:** create confirm and cancel order ([7ee2f5f](https://gitlab.com/fi-sas/fisas_alimentation/commit/7ee2f5f38c7a2548a35e4f1dcd79e065c8a82029))

## [1.2.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.4...v1.2.5) (2021-03-24)

### Bug Fixes

- **menu:** sall fix ([c5af5a4](https://gitlab.com/fi-sas/fisas_alimentation/commit/c5af5a442ea7b489927aec25ba09a951f00587f5))

## [1.2.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.3...v1.2.4) (2021-03-24)

### Bug Fixes

- **menu:** checkDisponibility date to Date ([0b4282b](https://gitlab.com/fi-sas/fisas_alimentation/commit/0b4282b454b722749508a9d2092f6edd7fbf79f1))

## [1.2.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.2...v1.2.3) (2021-03-24)

### Bug Fixes

- **menu:** turn checkDisponibility sync function ([c433bca](https://gitlab.com/fi-sas/fisas_alimentation/commit/c433bca1f99fddb2cdd813671cc9e917a8ac82b6))

## [1.2.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.1...v1.2.2) (2021-03-24)

### Bug Fixes

- **menu:** add logs for debug porpose ([ecda602](https://gitlab.com/fi-sas/fisas_alimentation/commit/ecda602fa6d91c77f9cbfd7a967fa17d2080c153))

## [1.2.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.2.0...v1.2.1) (2021-03-24)

### Bug Fixes

- **menu:** minor fixs on checkDisponibility ([1a79d1c](https://gitlab.com/fi-sas/fisas_alimentation/commit/1a79d1c895f500d8c24e0dac9b1f3867736bc050))

# [1.2.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.1.1...v1.2.0) (2021-03-23)

### Features

- **units:** add inital search relateds feat ([7880cfe](https://gitlab.com/fi-sas/fisas_alimentation/commit/7880cfed671ceb3621ae7af931f895a4ba9b9a1b))

## [1.1.1](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.1.0...v1.1.1) (2021-03-18)

### Bug Fixes

- **wharehouse:** remove cicle withrelated ([e399351](https://gitlab.com/fi-sas/fisas_alimentation/commit/e399351ba2173956e60f56c95d224a8066e81461))

# [1.1.0](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0...v1.1.0) (2021-03-09)

### Bug Fixes

- **menu:** missing formatDate function ([0a36f38](https://gitlab.com/fi-sas/fisas_alimentation/commit/0a36f38bc5d051cdf6d4e0af608c848c4577222e))

### Features

- **menu:** add endpoint to WP widget ([50808a3](https://gitlab.com/fi-sas/fisas_alimentation/commit/50808a3151dc45675d32723078c0332d13585452))

# 1.0.0 (2021-03-09)

### Bug Fixes

- **core:** update module core ([3b60556](https://gitlab.com/fi-sas/fisas_alimentation/commit/3b6055689ffc2c42ecdb988f660aee799dd28038))
- **dish:** add clear cache ([ccaf0ff](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccaf0ff76a51ebe9745ea2199b13530fd4f83e96))
- **dish_recipes:** rename clearcache ([d188f7b](https://gitlab.com/fi-sas/fisas_alimentation/commit/d188f7b8446a253c259c9cf9495f346954b26a6e))
- **dish_types:** problem with null ([68d3d13](https://gitlab.com/fi-sas/fisas_alimentation/commit/68d3d13e73a476e7c7bd1e04cf85205b32af0240))
- **dishs:** withrelated in service dish ([6cdf331](https://gitlab.com/fi-sas/fisas_alimentation/commit/6cdf331e285c43e2f27aafeff365894d1513c8b2))
- **menu:** add clear cache ([388388e](https://gitlab.com/fi-sas/fisas_alimentation/commit/388388eb7dcc850742dcf1ca417a05e0e3144863))
- **menu:** add file to menuDish with related ([cb1da4e](https://gitlab.com/fi-sas/fisas_alimentation/commit/cb1da4ee80d18a9294357e4ecbf8aadefaba1d1e))
- **menu:** add menuDish with related file ([94571b8](https://gitlab.com/fi-sas/fisas_alimentation/commit/94571b814a120912319001a2a50527ba6197ed42))
- **menu:** add user_id to extra_info ([c9f5c20](https://gitlab.com/fi-sas/fisas_alimentation/commit/c9f5c20014492c989f1490b04aaa5471ce5212fb))
- **menu:** disponibility_dinner error ([1b08fb8](https://gitlab.com/fi-sas/fisas_alimentation/commit/1b08fb8eec6472dd5716a81c7ea2b66c0323fac0))
- **menu:** endpoint services ([580f729](https://gitlab.com/fi-sas/fisas_alimentation/commit/580f729107f929a4e449a77c343cef6803d48172))
- **menu:** fix problem with disponbility ([c550842](https://gitlab.com/fi-sas/fisas_alimentation/commit/c550842aee15a9f32326023626b98639f6771ff5))
- **menu:** not iterable problem ([abe9444](https://gitlab.com/fi-sas/fisas_alimentation/commit/abe94446024b79e2764ccb76f5a87ead59d46947))
- **menu:** remove schools info ([2537bf7](https://gitlab.com/fi-sas/fisas_alimentation/commit/2537bf749f6503f5a385d0483cb6df2640e24e64))
- **menu_dish:** update menu_dish problem ([5403ae6](https://gitlab.com/fi-sas/fisas_alimentation/commit/5403ae61ab740caea056f1f5164b89efde2e5c27))
- **menu_dishs:** error in withrelated field id ([e655063](https://gitlab.com/fi-sas/fisas_alimentation/commit/e655063cd0807881f5c1f3cf871e211036125cda))
- **menudish:** avaliable_doses entityvalidator ([cd3c22c](https://gitlab.com/fi-sas/fisas_alimentation/commit/cd3c22c3bd63b65637ac3e71dc2e65780fd2136e))
- **menus:** copy_week clear cacher ([ae8fcd1](https://gitlab.com/fi-sas/fisas_alimentation/commit/ae8fcd12018f0a844322b419618489e391c6d94f))
- **menus:** endpoint copy menu ([37c0821](https://gitlab.com/fi-sas/fisas_alimentation/commit/37c0821581c4c0e4169a7c5648f7dc1101e7c9fd))
- **menus:** fix endpoint copy_day ([c0a7e2f](https://gitlab.com/fi-sas/fisas_alimentation/commit/c0a7e2f622f60dcdae1e5c2cb5008ba0ff34578b))
- **menus:** fix endpoints of copy meals ([4c436f2](https://gitlab.com/fi-sas/fisas_alimentation/commit/4c436f270f2c445088b7db447a91a3eee3ba6b5d))
- **menus:** fix response and days of week ([25e4ab0](https://gitlab.com/fi-sas/fisas_alimentation/commit/25e4ab0543978dd358c28878ac8005ac2b810840))
- **menus:** return complet menu info ([d39b546](https://gitlab.com/fi-sas/fisas_alimentation/commit/d39b5463b6fd1dabcf53d9874a6642d0418dd736))
- **order-reservations:** fix problem with cache ([d1c6d75](https://gitlab.com/fi-sas/fisas_alimentation/commit/d1c6d756b3988464607c91bc9856f60827e7f467))
- **orders:** add field service_id ([c8e5fcc](https://gitlab.com/fi-sas/fisas_alimentation/commit/c8e5fcc3df216408e62ec846b8dd2d4aca3d2189))
- **orders:** add withRelated and date convert ([9dc5dc2](https://gitlab.com/fi-sas/fisas_alimentation/commit/9dc5dc254aee91da2ff3165351f6784d19f1e653))
- **orders:** create order ([152061f](https://gitlab.com/fi-sas/fisas_alimentation/commit/152061ff31c08f8c9d6f9260a67710b08498b56f))
- **orders:** name of withrelated ([b2b014a](https://gitlab.com/fi-sas/fisas_alimentation/commit/b2b014a198a545a69059cdb7893014c2cd5c02fb))
- **orders:** put user_id of extra_info ([d4de907](https://gitlab.com/fi-sas/fisas_alimentation/commit/d4de907b66b34c895ecc70f6556e909b98812e4e))
- **orders:** value of id ([312c75e](https://gitlab.com/fi-sas/fisas_alimentation/commit/312c75ed2fa89aba538237717daf84ee0c85b6d6))
- **orders-reservations:** entity_id query ([bba2850](https://gitlab.com/fi-sas/fisas_alimentation/commit/bba28504e2ecefb780c2afe88b2db63732b580f7))
- **products:** problem in update product ([9bc279f](https://gitlab.com/fi-sas/fisas_alimentation/commit/9bc279fc3a373f67ebe3d184ce9efb608e0b5cbe))
- **recipes:** add clear cache ([2ae8729](https://gitlab.com/fi-sas/fisas_alimentation/commit/2ae8729d2ec4defe36dc729ed95b8e98bdd2ed0a))
- **reservations:** add field service_id and fix services ([890ce0d](https://gitlab.com/fi-sas/fisas_alimentation/commit/890ce0d2b290332f970d61cf0c72b7b8079cb62b))
- **stocks:** add withrelated wharehouse ([b755307](https://gitlab.com/fi-sas/fisas_alimentation/commit/b7553077970c580786b9d51d11f3f2ef0b335181))
- **users_allergens:** remove condition ([47284c2](https://gitlab.com/fi-sas/fisas_alimentation/commit/47284c29f9c9ff8ec0ec75bf9d257e9168c32bce))
- add clear cache after remove all ([b9251ef](https://gitlab.com/fi-sas/fisas_alimentation/commit/b9251efa532cef9ddddf69db324cff44dae981d2))
- add items cart and confirms/cancel reservations ([c5fc2a0](https://gitlab.com/fi-sas/fisas_alimentation/commit/c5fc2a00c3571a9da01bcd76840b7c9ce670721e))
- add withrelated in dish_type_prices ([f2b0357](https://gitlab.com/fi-sas/fisas_alimentation/commit/f2b035714d1eae8604f9d006a1cbbd33c5370471))
- bar cart ([3733098](https://gitlab.com/fi-sas/fisas_alimentation/commit/3733098a12693c447d883c9b859b180add521cfe))
- bar cart add service_cancel_path ([b23fc8f](https://gitlab.com/fi-sas/fisas_alimentation/commit/b23fc8fa46121746fc5d3da96a79e806b19f45ee))
- bug and withrelated in service menu ([0a01e3b](https://gitlab.com/fi-sas/fisas_alimentation/commit/0a01e3bf8e98564146442bfdeaa396de738add2d))
- bug in menu endpoint ([232b886](https://gitlab.com/fi-sas/fisas_alimentation/commit/232b886b11d0654757e82fd4845e137fb41235a2))
- bug in relation of services ([49ff7ff](https://gitlab.com/fi-sas/fisas_alimentation/commit/49ff7ffdff97ba9b06356d4652190965276741b9))
- bugs in querys ([7e806b2](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e806b2c77000f131e414a084127fdadd8987eb3))
- cache problem in allergens and nutrients ([b7dc3ee](https://gitlab.com/fi-sas/fisas_alimentation/commit/b7dc3eed1cb5927143d4922aded488f3445cddc0))
- change things in complements ([17694ac](https://gitlab.com/fi-sas/fisas_alimentation/commit/17694ac23bb6a753b6994d20976474a4c7c1e61b))
- change things in units ([cf40b51](https://gitlab.com/fi-sas/fisas_alimentation/commit/cf40b5195bd0e51483761ea84213b6b5856bffb0))
- dishs types ([6bbc9c3](https://gitlab.com/fi-sas/fisas_alimentation/commit/6bbc9c30d52a46e47650e00be51255bceaf5471d))
- duplicate nutrients ([8d515bc](https://gitlab.com/fi-sas/fisas_alimentation/commit/8d515bc5e3150b36c5512e823b9c7d009050880a))
- endpoint of cart ([58144f6](https://gitlab.com/fi-sas/fisas_alimentation/commit/58144f66867bac2cc7635275426388179f535450))
- endpoint schools ([bd9d8a5](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd9d8a59ad15e2882bbec98c3b15e4d766e6c811))
- endpoint send to cart ([2d15683](https://gitlab.com/fi-sas/fisas_alimentation/commit/2d15683dcb8e2a6cc911b6d66270d120b2cda9e3))
- endpoints ([55f65c0](https://gitlab.com/fi-sas/fisas_alimentation/commit/55f65c0dc69772677c8b5e4175982d83a9127b81))
- endpoints and bugs in withrelateds ([725ae30](https://gitlab.com/fi-sas/fisas_alimentation/commit/725ae304609cac970adeb99d883133dcedf46bb1))
- entity_id in header ([bd4e017](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd4e0173efb51670873fea7d7dce9a4617b8eac9))
- field name description on recipe steps ([93ef7c6](https://gitlab.com/fi-sas/fisas_alimentation/commit/93ef7c6527075aa0a828e168b3f76e18f7b1efc0))
- fix default envs and packages ([a621726](https://gitlab.com/fi-sas/fisas_alimentation/commit/a621726fbfc0d0964144f495c2a791ca964239fc))
- migration file ([fe1e556](https://gitlab.com/fi-sas/fisas_alimentation/commit/fe1e556756f19c252b8b2539542b7a75ecf1c85e))
- nutrient quantity ([57e7948](https://gitlab.com/fi-sas/fisas_alimentation/commit/57e7948aec5dd37fd9e103ff4db434a46e3dada3))
- optional prices variations ([e654103](https://gitlab.com/fi-sas/fisas_alimentation/commit/e654103ea81f2e1ec3996ed069f6d73d192f1864))
- preferencial canteen ([ccf5688](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccf5688ed5ceead7a3596f5694e95f17a3d58a7d))
- prices variations ([3485327](https://gitlab.com/fi-sas/fisas_alimentation/commit/3485327fb58cc8d5345b3ae05cc96fa9cccba2c2))
- problem in reservations and products ([0435c1f](https://gitlab.com/fi-sas/fisas_alimentation/commit/0435c1f92673154e8b4075f19566e88d0e51750f))
- problem with cache and add more info in prices ([6fa1843](https://gitlab.com/fi-sas/fisas_alimentation/commit/6fa184349721df8065d7ae6cc104a2689add3502))
- recipes ([99375f7](https://gitlab.com/fi-sas/fisas_alimentation/commit/99375f72d488d0ddd2f11160eb0b31a14ccbfb0e))
- recipes_products service ([a93a646](https://gitlab.com/fi-sas/fisas_alimentation/commit/a93a6460479b6f3a19c82420677b396c4b6db281))
- remove array of disponibility ([4090a73](https://gitlab.com/fi-sas/fisas_alimentation/commit/4090a73b3425df24192243ef43c17fb81ae43968))
- remove dishs_prices from create and update ([367e5fe](https://gitlab.com/fi-sas/fisas_alimentation/commit/367e5feb29a84421eda27386b855e0ed897ab752))
- remove rest ([eb575e3](https://gitlab.com/fi-sas/fisas_alimentation/commit/eb575e3c02bafb87dd0e0044a5c6c26e6b61c86d))
- remove seed and add filter ([fddf3af](https://gitlab.com/fi-sas/fisas_alimentation/commit/fddf3afea4a233890abf1f324743afb0f98c2953))
- responses in families, menus, menus_dishs ([80aee7f](https://gitlab.com/fi-sas/fisas_alimentation/commit/80aee7fa5206703d9eb117f2c6c0d97cff0d613a))
- responses in withRelated ([efa4a42](https://gitlab.com/fi-sas/fisas_alimentation/commit/efa4a4280ad1c96f340e26e604c54f4fc546295f))
- responses of dishs, services and users-allergens ([80edf3a](https://gitlab.com/fi-sas/fisas_alimentation/commit/80edf3aabc6ccc642253597ed25e062d7515cd9d))
- school info for organic_unit ([7e34a05](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e34a05ff2627673cdc16be2baa75fe8a6e06fc0))
- test the commit ([c156a3e](https://gitlab.com/fi-sas/fisas_alimentation/commit/c156a3e6677100bf7ca3beaceb37006152a462eb))
- test the commit ([b1e0814](https://gitlab.com/fi-sas/fisas_alimentation/commit/b1e0814ebdac466c25485f3f75d5b37a129c5390))
- test the commit ([c67be52](https://gitlab.com/fi-sas/fisas_alimentation/commit/c67be528428eef4c1c3e1ee4ce956fb78f5370e8))
- visibility ([49a4d05](https://gitlab.com/fi-sas/fisas_alimentation/commit/49a4d05b65ae3d6e6edc073cad1d2ffcb58cb828))
- withrelated and add relations ([1c08a06](https://gitlab.com/fi-sas/fisas_alimentation/commit/1c08a060f113b20bd8efb344eea1ebc5f415d475))
- withrelated in princes of dish_types ([7da6f36](https://gitlab.com/fi-sas/fisas_alimentation/commit/7da6f360d85b0c88a11a8050edb2b44736031dd5))

### Features

- **service:** add endpoint return active devices ([ad71af8](https://gitlab.com/fi-sas/fisas_alimentation/commit/ad71af8cd38ad6a11ecfd932571115e6b9293be8))
- add all endpoints for menu ([2be7586](https://gitlab.com/fi-sas/fisas_alimentation/commit/2be75866350131f9c28266288b320819fa2bc570))
- add all endpoints to crud the stocks ([8f9c2cc](https://gitlab.com/fi-sas/fisas_alimentation/commit/8f9c2cc9ace7d934a9586bacc2a1204a14493b31))
- add all services ([328524c](https://gitlab.com/fi-sas/fisas_alimentation/commit/328524c019a928b074639297ff7aed0bc7853179))
- add altertable migration ([50c3a9f](https://gitlab.com/fi-sas/fisas_alimentation/commit/50c3a9f5d22deed2d6c8f6c53fb43091ed22d354))
- add clear cache ([8f61b17](https://gitlab.com/fi-sas/fisas_alimentation/commit/8f61b1750dac364e548c75c3a402474d1c34425e))
- add dish to cart, fix fiscal_entities, dish ([67e86ef](https://gitlab.com/fi-sas/fisas_alimentation/commit/67e86efed881a647a1f06864d251d178243567d5))
- add dish_id to endpoint ([40df98d](https://gitlab.com/fi-sas/fisas_alimentation/commit/40df98d327bb63527793c09990728d73acd59afd))
- add generic function for validate permitions ([8ace909](https://gitlab.com/fi-sas/fisas_alimentation/commit/8ace9091082010114a56935c11ced18fc3cdd4c2))
- add method removebyuser, allergensinfo, returnsconfig ([c99f9fd](https://gitlab.com/fi-sas/fisas_alimentation/commit/c99f9fd0c9ff2962c18d64ee4192f886ab179de2))
- add method return entity of user ([cf8b9b4](https://gitlab.com/fi-sas/fisas_alimentation/commit/cf8b9b438ec3538a56100fd5d9455f0fb3bf7a66))
- add permitions on dish and dish_type ([dd5b357](https://gitlab.com/fi-sas/fisas_alimentation/commit/dd5b357600753d0f424b7dd06c1bf633753e8a2b))
- add preferencial canteen ([9b0e8c8](https://gitlab.com/fi-sas/fisas_alimentation/commit/9b0e8c89c8a372870d8e7554772ac9b9a6b614da))
- all endpoints for crud products ([d0c3908](https://gitlab.com/fi-sas/fisas_alimentation/commit/d0c39089fb4bdcba2914789bb3d776a21f84bc09))
- all endpoints relationated of dishs ([11872f9](https://gitlab.com/fi-sas/fisas_alimentation/commit/11872f9a1906e312bf4c8f22577e308b62d5e882))
- all relationd for crud recipes ([7dd3682](https://gitlab.com/fi-sas/fisas_alimentation/commit/7dd36826468d76cb37a4b00eea155677f6664d08))
- all relations of crud wharehouse ([c966856](https://gitlab.com/fi-sas/fisas_alimentation/commit/c966856bb9caf096d25c8e46f7b61e084be8bb07))
- confirm reservation ([07010e8](https://gitlab.com/fi-sas/fisas_alimentation/commit/07010e87bb694c0dd04ad4bc9e2180072de3fb02))
- confirmation payment articletype bar ([b009aad](https://gitlab.com/fi-sas/fisas_alimentation/commit/b009aad096aa191084c031d6a97cd4dd4d92e441))
- crud all endpoints for products ([69d2370](https://gitlab.com/fi-sas/fisas_alimentation/commit/69d2370c549ad9944bcc3542e70ca0366ddb461e))
- crud and all relations for families ([c4de5d9](https://gitlab.com/fi-sas/fisas_alimentation/commit/c4de5d9992a2843edc9b8f4fa1d3fcd537689e54))
- crud complements with all relations ([ea3b238](https://gitlab.com/fi-sas/fisas_alimentation/commit/ea3b238c8da58a1320524a9c1dc7416a5b4ffa16))
- crud menus and all relations ([23164bc](https://gitlab.com/fi-sas/fisas_alimentation/commit/23164bc33e4777af79780b36c53e43286d00874b))
- crud reservations and all relations ([41cbc22](https://gitlab.com/fi-sas/fisas_alimentation/commit/41cbc2251db374d29cfa949eb3ad9a9fbb7cba1c))
- endpoint to get recent products of user ([50c4668](https://gitlab.com/fi-sas/fisas_alimentation/commit/50c4668e5593ebb17b96abe7df7d48ec72b84a3d))
- finalize crud units, allergens, nutrients ([191cccd](https://gitlab.com/fi-sas/fisas_alimentation/commit/191cccd4433e3926a0a1454c21eb1241fbbfa655))
- method calculcateallergens and method calculateallergennutrients ([12fa627](https://gitlab.com/fi-sas/fisas_alimentation/commit/12fa627d45cd30846a7bc347f7970b9c8d45fb23))
- ms at a new base boilerplate and new migrations file ([4202d46](https://gitlab.com/fi-sas/fisas_alimentation/commit/4202d465104c12232fcd29b8568b2c893a365b2c))
- relations of crud dishs_types ([adeae3f](https://gitlab.com/fi-sas/fisas_alimentation/commit/adeae3f8f07275a237c6972bd2618c4a67d4e407))
- relations of crud services ([7e9cda0](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e9cda0e529f44efe42e8d263609ea1b4f9e4608))
- relations of cruds in entity dishs and add helper ([ccab76e](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccab76e58b828bf0b5e8bc5171fcb48d1866c003))
- relations of cruds in entity fiscal_entities ([4b1ee53](https://gitlab.com/fi-sas/fisas_alimentation/commit/4b1ee534932ecfcecfa145523b5bebfc6d0b9f5c))
- teste commit ([b5a577e](https://gitlab.com/fi-sas/fisas_alimentation/commit/b5a577e2d6a66343cf13f3e9cecd8b51a80edb1a))
- teste commit ([efd7ee7](https://gitlab.com/fi-sas/fisas_alimentation/commit/efd7ee7b3a1be02f1db4e658cd1df6d2e583af22))
- verify permitions on fiscal_entity ([1c80df8](https://gitlab.com/fi-sas/fisas_alimentation/commit/1c80df8a8e8191af9fa5d7820bcd2607d7b969f9))

# [1.0.0-rc.64](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.63...v1.0.0-rc.64) (2021-03-03)

### Bug Fixes

- **dish_recipes:** rename clearcache ([d188f7b](https://gitlab.com/fi-sas/fisas_alimentation/commit/d188f7b8446a253c259c9cf9495f346954b26a6e))

# [1.0.0-rc.63](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.62...v1.0.0-rc.63) (2021-03-03)

### Bug Fixes

- **dish:** add clear cache ([ccaf0ff](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccaf0ff76a51ebe9745ea2199b13530fd4f83e96))
- **menu:** endpoint services ([580f729](https://gitlab.com/fi-sas/fisas_alimentation/commit/580f729107f929a4e449a77c343cef6803d48172))
- **recipes:** add clear cache ([2ae8729](https://gitlab.com/fi-sas/fisas_alimentation/commit/2ae8729d2ec4defe36dc729ed95b8e98bdd2ed0a))

# [1.0.0-rc.62](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.61...v1.0.0-rc.62) (2021-03-01)

### Bug Fixes

- **orders-reservations:** entity_id query ([bba2850](https://gitlab.com/fi-sas/fisas_alimentation/commit/bba28504e2ecefb780c2afe88b2db63732b580f7))

# [1.0.0-rc.61](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.60...v1.0.0-rc.61) (2021-03-01)

### Bug Fixes

- **order-reservations:** fix problem with cache ([d1c6d75](https://gitlab.com/fi-sas/fisas_alimentation/commit/d1c6d756b3988464607c91bc9856f60827e7f467))

# [1.0.0-rc.60](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.59...v1.0.0-rc.60) (2021-03-01)

### Bug Fixes

- **dish_types:** problem with null ([68d3d13](https://gitlab.com/fi-sas/fisas_alimentation/commit/68d3d13e73a476e7c7bd1e04cf85205b32af0240))
- **menu:** fix problem with disponbility ([c550842](https://gitlab.com/fi-sas/fisas_alimentation/commit/c550842aee15a9f32326023626b98639f6771ff5))
- **users_allergens:** remove condition ([47284c2](https://gitlab.com/fi-sas/fisas_alimentation/commit/47284c29f9c9ff8ec0ec75bf9d257e9168c32bce))

### Features

- **service:** add endpoint return active devices ([ad71af8](https://gitlab.com/fi-sas/fisas_alimentation/commit/ad71af8cd38ad6a11ecfd932571115e6b9293be8))

# [1.0.0-rc.59](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.58...v1.0.0-rc.59) (2021-02-26)

### Bug Fixes

- **products:** problem in update product ([9bc279f](https://gitlab.com/fi-sas/fisas_alimentation/commit/9bc279fc3a373f67ebe3d184ce9efb608e0b5cbe))

# [1.0.0-rc.58](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.57...v1.0.0-rc.58) (2021-02-26)

### Bug Fixes

- **orders:** create order ([152061f](https://gitlab.com/fi-sas/fisas_alimentation/commit/152061ff31c08f8c9d6f9260a67710b08498b56f))

# [1.0.0-rc.57](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.56...v1.0.0-rc.57) (2021-02-26)

### Bug Fixes

- **orders:** value of id ([312c75e](https://gitlab.com/fi-sas/fisas_alimentation/commit/312c75ed2fa89aba538237717daf84ee0c85b6d6))

# [1.0.0-rc.56](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.55...v1.0.0-rc.56) (2021-02-26)

### Bug Fixes

- **orders:** name of withrelated ([b2b014a](https://gitlab.com/fi-sas/fisas_alimentation/commit/b2b014a198a545a69059cdb7893014c2cd5c02fb))

# [1.0.0-rc.55](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.54...v1.0.0-rc.55) (2021-02-26)

### Bug Fixes

- **orders:** add withRelated and date convert ([9dc5dc2](https://gitlab.com/fi-sas/fisas_alimentation/commit/9dc5dc254aee91da2ff3165351f6784d19f1e653))

# [1.0.0-rc.54](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.53...v1.0.0-rc.54) (2021-02-26)

### Bug Fixes

- **menu:** add user_id to extra_info ([c9f5c20](https://gitlab.com/fi-sas/fisas_alimentation/commit/c9f5c20014492c989f1490b04aaa5471ce5212fb))
- **orders:** put user_id of extra_info ([d4de907](https://gitlab.com/fi-sas/fisas_alimentation/commit/d4de907b66b34c895ecc70f6556e909b98812e4e))

# [1.0.0-rc.53](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.52...v1.0.0-rc.53) (2021-02-26)

### Bug Fixes

- **menu_dishs:** error in withrelated field id ([e655063](https://gitlab.com/fi-sas/fisas_alimentation/commit/e655063cd0807881f5c1f3cf871e211036125cda))
- **orders:** add field service_id ([c8e5fcc](https://gitlab.com/fi-sas/fisas_alimentation/commit/c8e5fcc3df216408e62ec846b8dd2d4aca3d2189))
- **reservations:** add field service_id and fix services ([890ce0d](https://gitlab.com/fi-sas/fisas_alimentation/commit/890ce0d2b290332f970d61cf0c72b7b8079cb62b))
- **stocks:** add withrelated wharehouse ([b755307](https://gitlab.com/fi-sas/fisas_alimentation/commit/b7553077970c580786b9d51d11f3f2ef0b335181))

# [1.0.0-rc.52](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.51...v1.0.0-rc.52) (2021-02-25)

### Bug Fixes

- **menu:** disponibility_dinner error ([1b08fb8](https://gitlab.com/fi-sas/fisas_alimentation/commit/1b08fb8eec6472dd5716a81c7ea2b66c0323fac0))

# [1.0.0-rc.51](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.50...v1.0.0-rc.51) (2021-02-25)

### Bug Fixes

- **menu:** remove schools info ([2537bf7](https://gitlab.com/fi-sas/fisas_alimentation/commit/2537bf749f6503f5a385d0483cb6df2640e24e64))

# [1.0.0-rc.50](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.49...v1.0.0-rc.50) (2021-02-25)

### Bug Fixes

- **menu:** add clear cache ([388388e](https://gitlab.com/fi-sas/fisas_alimentation/commit/388388eb7dcc850742dcf1ca417a05e0e3144863))

# [1.0.0-rc.49](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.48...v1.0.0-rc.49) (2021-02-25)

### Bug Fixes

- **core:** update module core ([3b60556](https://gitlab.com/fi-sas/fisas_alimentation/commit/3b6055689ffc2c42ecdb988f660aee799dd28038))

# [1.0.0-rc.48](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.47...v1.0.0-rc.48) (2021-02-25)

### Bug Fixes

- **menus:** copy_week clear cacher ([ae8fcd1](https://gitlab.com/fi-sas/fisas_alimentation/commit/ae8fcd12018f0a844322b419618489e391c6d94f))

# [1.0.0-rc.47](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.46...v1.0.0-rc.47) (2021-02-25)

### Bug Fixes

- **menus:** fix endpoint copy_day ([c0a7e2f](https://gitlab.com/fi-sas/fisas_alimentation/commit/c0a7e2f622f60dcdae1e5c2cb5008ba0ff34578b))

# [1.0.0-rc.46](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.45...v1.0.0-rc.46) (2021-02-25)

### Bug Fixes

- **menu:** not iterable problem ([abe9444](https://gitlab.com/fi-sas/fisas_alimentation/commit/abe94446024b79e2764ccb76f5a87ead59d46947))

# [1.0.0-rc.45](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.44...v1.0.0-rc.45) (2021-02-25)

### Bug Fixes

- **menus:** endpoint copy menu ([37c0821](https://gitlab.com/fi-sas/fisas_alimentation/commit/37c0821581c4c0e4169a7c5648f7dc1101e7c9fd))

# [1.0.0-rc.44](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.43...v1.0.0-rc.44) (2021-02-25)

### Bug Fixes

- **menus:** fix response and days of week ([25e4ab0](https://gitlab.com/fi-sas/fisas_alimentation/commit/25e4ab0543978dd358c28878ac8005ac2b810840))

# [1.0.0-rc.43](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.42...v1.0.0-rc.43) (2021-02-25)

### Bug Fixes

- **menus:** return complet menu info ([d39b546](https://gitlab.com/fi-sas/fisas_alimentation/commit/d39b5463b6fd1dabcf53d9874a6642d0418dd736))

# [1.0.0-rc.42](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.41...v1.0.0-rc.42) (2021-02-25)

### Bug Fixes

- **menus:** fix endpoints of copy meals ([4c436f2](https://gitlab.com/fi-sas/fisas_alimentation/commit/4c436f270f2c445088b7db447a91a3eee3ba6b5d))

# [1.0.0-rc.41](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.40...v1.0.0-rc.41) (2021-02-24)

### Bug Fixes

- **menu_dish:** update menu_dish problem ([5403ae6](https://gitlab.com/fi-sas/fisas_alimentation/commit/5403ae61ab740caea056f1f5164b89efde2e5c27))

# [1.0.0-rc.40](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.39...v1.0.0-rc.40) (2021-02-24)

### Bug Fixes

- **menudish:** avaliable_doses entityvalidator ([cd3c22c](https://gitlab.com/fi-sas/fisas_alimentation/commit/cd3c22c3bd63b65637ac3e71dc2e65780fd2136e))

# [1.0.0-rc.39](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.38...v1.0.0-rc.39) (2021-02-23)

### Bug Fixes

- **dishs:** withrelated in service dish ([6cdf331](https://gitlab.com/fi-sas/fisas_alimentation/commit/6cdf331e285c43e2f27aafeff365894d1513c8b2))

# [1.0.0-rc.38](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.37...v1.0.0-rc.38) (2021-02-23)

### Bug Fixes

- field name description on recipe steps ([93ef7c6](https://gitlab.com/fi-sas/fisas_alimentation/commit/93ef7c6527075aa0a828e168b3f76e18f7b1efc0))

# [1.0.0-rc.37](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.36...v1.0.0-rc.37) (2021-02-23)

### Bug Fixes

- **menu:** add file to menuDish with related ([cb1da4e](https://gitlab.com/fi-sas/fisas_alimentation/commit/cb1da4ee80d18a9294357e4ecbf8aadefaba1d1e))

# [1.0.0-rc.36](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.35...v1.0.0-rc.36) (2021-02-23)

### Bug Fixes

- **menu:** add menuDish with related file ([94571b8](https://gitlab.com/fi-sas/fisas_alimentation/commit/94571b814a120912319001a2a50527ba6197ed42))

# [1.0.0-rc.35](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.34...v1.0.0-rc.35) (2021-02-22)

### Bug Fixes

- withrelated in princes of dish_types ([7da6f36](https://gitlab.com/fi-sas/fisas_alimentation/commit/7da6f360d85b0c88a11a8050edb2b44736031dd5))

# [1.0.0-rc.34](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.33...v1.0.0-rc.34) (2021-02-22)

### Bug Fixes

- add withrelated in dish_type_prices ([f2b0357](https://gitlab.com/fi-sas/fisas_alimentation/commit/f2b035714d1eae8604f9d006a1cbbd33c5370471))

# [1.0.0-rc.33](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.32...v1.0.0-rc.33) (2021-02-22)

### Bug Fixes

- add clear cache after remove all ([b9251ef](https://gitlab.com/fi-sas/fisas_alimentation/commit/b9251efa532cef9ddddf69db324cff44dae981d2))

# [1.0.0-rc.32](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-02-22)

### Bug Fixes

- remove dishs_prices from create and update ([367e5fe](https://gitlab.com/fi-sas/fisas_alimentation/commit/367e5feb29a84421eda27386b855e0ed897ab752))

# [1.0.0-rc.31](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-02-22)

### Bug Fixes

- endpoint of cart ([58144f6](https://gitlab.com/fi-sas/fisas_alimentation/commit/58144f66867bac2cc7635275426388179f535450))

# [1.0.0-rc.30](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-02-22)

### Bug Fixes

- endpoint send to cart ([2d15683](https://gitlab.com/fi-sas/fisas_alimentation/commit/2d15683dcb8e2a6cc911b6d66270d120b2cda9e3))

# [1.0.0-rc.29](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-02-22)

### Bug Fixes

- problem with cache and add more info in prices ([6fa1843](https://gitlab.com/fi-sas/fisas_alimentation/commit/6fa184349721df8065d7ae6cc104a2689add3502))

# [1.0.0-rc.28](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-02-19)

### Bug Fixes

- cache problem in allergens and nutrients ([b7dc3ee](https://gitlab.com/fi-sas/fisas_alimentation/commit/b7dc3eed1cb5927143d4922aded488f3445cddc0))

# [1.0.0-rc.27](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-02-19)

### Features

- add dish_id to endpoint ([40df98d](https://gitlab.com/fi-sas/fisas_alimentation/commit/40df98d327bb63527793c09990728d73acd59afd))

# [1.0.0-rc.26](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-02-19)

### Features

- add clear cache ([8f61b17](https://gitlab.com/fi-sas/fisas_alimentation/commit/8f61b1750dac364e548c75c3a402474d1c34425e))

# [1.0.0-rc.25](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-02-19)

### Bug Fixes

- prices variations ([3485327](https://gitlab.com/fi-sas/fisas_alimentation/commit/3485327fb58cc8d5345b3ae05cc96fa9cccba2c2))

# [1.0.0-rc.24](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-02-19)

### Bug Fixes

- optional prices variations ([e654103](https://gitlab.com/fi-sas/fisas_alimentation/commit/e654103ea81f2e1ec3996ed069f6d73d192f1864))

# [1.0.0-rc.23](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-02-18)

### Bug Fixes

- nutrient quantity ([57e7948](https://gitlab.com/fi-sas/fisas_alimentation/commit/57e7948aec5dd37fd9e103ff4db434a46e3dada3))

# [1.0.0-rc.22](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-02-18)

### Bug Fixes

- duplicate nutrients ([8d515bc](https://gitlab.com/fi-sas/fisas_alimentation/commit/8d515bc5e3150b36c5512e823b9c7d009050880a))
- endpoints and bugs in withrelateds ([725ae30](https://gitlab.com/fi-sas/fisas_alimentation/commit/725ae304609cac970adeb99d883133dcedf46bb1))

# [1.0.0-rc.21](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-02-12)

### Bug Fixes

- bug in menu endpoint ([232b886](https://gitlab.com/fi-sas/fisas_alimentation/commit/232b886b11d0654757e82fd4845e137fb41235a2))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-02-12)

### Bug Fixes

- remove array of disponibility ([4090a73](https://gitlab.com/fi-sas/fisas_alimentation/commit/4090a73b3425df24192243ef43c17fb81ae43968))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-02-12)

### Bug Fixes

- entity_id in header ([bd4e017](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd4e0173efb51670873fea7d7dce9a4617b8eac9))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-02-12)

### Bug Fixes

- dishs types ([6bbc9c3](https://gitlab.com/fi-sas/fisas_alimentation/commit/6bbc9c30d52a46e47650e00be51255bceaf5471d))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-02-12)

### Bug Fixes

- endpoints ([55f65c0](https://gitlab.com/fi-sas/fisas_alimentation/commit/55f65c0dc69772677c8b5e4175982d83a9127b81))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-02-12)

### Bug Fixes

- visibility ([49a4d05](https://gitlab.com/fi-sas/fisas_alimentation/commit/49a4d05b65ae3d6e6edc073cad1d2ffcb58cb828))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-02-12)

### Bug Fixes

- recipes ([99375f7](https://gitlab.com/fi-sas/fisas_alimentation/commit/99375f72d488d0ddd2f11160eb0b31a14ccbfb0e))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-02-08)

### Bug Fixes

- endpoint schools ([bd9d8a5](https://gitlab.com/fi-sas/fisas_alimentation/commit/bd9d8a59ad15e2882bbec98c3b15e4d766e6c811))
- school info for organic_unit ([7e34a05](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e34a05ff2627673cdc16be2baa75fe8a6e06fc0))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-02-05)

### Bug Fixes

- add items cart and confirms/cancel reservations ([c5fc2a0](https://gitlab.com/fi-sas/fisas_alimentation/commit/c5fc2a00c3571a9da01bcd76840b7c9ce670721e))
- bar cart ([3733098](https://gitlab.com/fi-sas/fisas_alimentation/commit/3733098a12693c447d883c9b859b180add521cfe))
- bar cart add service_cancel_path ([b23fc8f](https://gitlab.com/fi-sas/fisas_alimentation/commit/b23fc8fa46121746fc5d3da96a79e806b19f45ee))
- remove rest ([eb575e3](https://gitlab.com/fi-sas/fisas_alimentation/commit/eb575e3c02bafb87dd0e0044a5c6c26e6b61c86d))

### Features

- add altertable migration ([50c3a9f](https://gitlab.com/fi-sas/fisas_alimentation/commit/50c3a9f5d22deed2d6c8f6c53fb43091ed22d354))
- confirm reservation ([07010e8](https://gitlab.com/fi-sas/fisas_alimentation/commit/07010e87bb694c0dd04ad4bc9e2180072de3fb02))
- confirmation payment articletype bar ([b009aad](https://gitlab.com/fi-sas/fisas_alimentation/commit/b009aad096aa191084c031d6a97cd4dd4d92e441))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-02-02)

### Bug Fixes

- migration file ([fe1e556](https://gitlab.com/fi-sas/fisas_alimentation/commit/fe1e556756f19c252b8b2539542b7a75ecf1c85e))
- problem in reservations and products ([0435c1f](https://gitlab.com/fi-sas/fisas_alimentation/commit/0435c1f92673154e8b4075f19566e88d0e51750f))
- responses in families, menus, menus_dishs ([80aee7f](https://gitlab.com/fi-sas/fisas_alimentation/commit/80aee7fa5206703d9eb117f2c6c0d97cff0d613a))
- responses in withRelated ([efa4a42](https://gitlab.com/fi-sas/fisas_alimentation/commit/efa4a4280ad1c96f340e26e604c54f4fc546295f))
- responses of dishs, services and users-allergens ([80edf3a](https://gitlab.com/fi-sas/fisas_alimentation/commit/80edf3aabc6ccc642253597ed25e062d7515cd9d))
- withrelated and add relations ([1c08a06](https://gitlab.com/fi-sas/fisas_alimentation/commit/1c08a060f113b20bd8efb344eea1ebc5f415d475))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-01-27)

### Features

- add dish to cart, fix fiscal_entities, dish ([67e86ef](https://gitlab.com/fi-sas/fisas_alimentation/commit/67e86efed881a647a1f06864d251d178243567d5))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-01-26)

### Bug Fixes

- preferencial canteen ([ccf5688](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccf5688ed5ceead7a3596f5694e95f17a3d58a7d))

### Features

- add preferencial canteen ([9b0e8c8](https://gitlab.com/fi-sas/fisas_alimentation/commit/9b0e8c89c8a372870d8e7554772ac9b9a6b614da))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-05)

### Bug Fixes

- bugs in querys ([7e806b2](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e806b2c77000f131e414a084127fdadd8987eb3))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-05)

### Bug Fixes

- remove seed and add filter ([fddf3af](https://gitlab.com/fi-sas/fisas_alimentation/commit/fddf3afea4a233890abf1f324743afb0f98c2953))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-05)

### Bug Fixes

- fix default envs and packages ([a621726](https://gitlab.com/fi-sas/fisas_alimentation/commit/a621726fbfc0d0964144f495c2a791ca964239fc))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2020-12-18)

### Features

- endpoint to get recent products of user ([50c4668](https://gitlab.com/fi-sas/fisas_alimentation/commit/50c4668e5593ebb17b96abe7df7d48ec72b84a3d))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2020-12-16)

### Features

- add method return entity of user ([cf8b9b4](https://gitlab.com/fi-sas/fisas_alimentation/commit/cf8b9b438ec3538a56100fd5d9455f0fb3bf7a66))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-16)

### Bug Fixes

- bug and withrelated in service menu ([0a01e3b](https://gitlab.com/fi-sas/fisas_alimentation/commit/0a01e3bf8e98564146442bfdeaa396de738add2d))

### Features

- add generic function for validate permitions ([8ace909](https://gitlab.com/fi-sas/fisas_alimentation/commit/8ace9091082010114a56935c11ced18fc3cdd4c2))
- add permitions on dish and dish_type ([dd5b357](https://gitlab.com/fi-sas/fisas_alimentation/commit/dd5b357600753d0f424b7dd06c1bf633753e8a2b))
- verify permitions on fiscal_entity ([1c80df8](https://gitlab.com/fi-sas/fisas_alimentation/commit/1c80df8a8e8191af9fa5d7820bcd2607d7b969f9))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-15)

### Bug Fixes

- bug in relation of services ([49ff7ff](https://gitlab.com/fi-sas/fisas_alimentation/commit/49ff7ffdff97ba9b06356d4652190965276741b9))
- change things in complements ([17694ac](https://gitlab.com/fi-sas/fisas_alimentation/commit/17694ac23bb6a753b6994d20976474a4c7c1e61b))
- change things in units ([cf40b51](https://gitlab.com/fi-sas/fisas_alimentation/commit/cf40b5195bd0e51483761ea84213b6b5856bffb0))
- recipes_products service ([a93a646](https://gitlab.com/fi-sas/fisas_alimentation/commit/a93a6460479b6f3a19c82420677b396c4b6db281))

### Features

- add all endpoints for menu ([2be7586](https://gitlab.com/fi-sas/fisas_alimentation/commit/2be75866350131f9c28266288b320819fa2bc570))
- add all endpoints to crud the stocks ([8f9c2cc](https://gitlab.com/fi-sas/fisas_alimentation/commit/8f9c2cc9ace7d934a9586bacc2a1204a14493b31))
- add method removebyuser, allergensinfo, returnsconfig ([c99f9fd](https://gitlab.com/fi-sas/fisas_alimentation/commit/c99f9fd0c9ff2962c18d64ee4192f886ab179de2))
- all endpoints for crud products ([d0c3908](https://gitlab.com/fi-sas/fisas_alimentation/commit/d0c39089fb4bdcba2914789bb3d776a21f84bc09))
- all endpoints relationated of dishs ([11872f9](https://gitlab.com/fi-sas/fisas_alimentation/commit/11872f9a1906e312bf4c8f22577e308b62d5e882))
- all relationd for crud recipes ([7dd3682](https://gitlab.com/fi-sas/fisas_alimentation/commit/7dd36826468d76cb37a4b00eea155677f6664d08))
- all relations of crud wharehouse ([c966856](https://gitlab.com/fi-sas/fisas_alimentation/commit/c966856bb9caf096d25c8e46f7b61e084be8bb07))
- crud all endpoints for products ([69d2370](https://gitlab.com/fi-sas/fisas_alimentation/commit/69d2370c549ad9944bcc3542e70ca0366ddb461e))
- crud and all relations for families ([c4de5d9](https://gitlab.com/fi-sas/fisas_alimentation/commit/c4de5d9992a2843edc9b8f4fa1d3fcd537689e54))
- crud complements with all relations ([ea3b238](https://gitlab.com/fi-sas/fisas_alimentation/commit/ea3b238c8da58a1320524a9c1dc7416a5b4ffa16))
- crud menus and all relations ([23164bc](https://gitlab.com/fi-sas/fisas_alimentation/commit/23164bc33e4777af79780b36c53e43286d00874b))
- crud reservations and all relations ([41cbc22](https://gitlab.com/fi-sas/fisas_alimentation/commit/41cbc2251db374d29cfa949eb3ad9a9fbb7cba1c))
- finalize crud units, allergens, nutrients ([191cccd](https://gitlab.com/fi-sas/fisas_alimentation/commit/191cccd4433e3926a0a1454c21eb1241fbbfa655))
- method calculcateallergens and method calculateallergennutrients ([12fa627](https://gitlab.com/fi-sas/fisas_alimentation/commit/12fa627d45cd30846a7bc347f7970b9c8d45fb23))
- relations of crud dishs_types ([adeae3f](https://gitlab.com/fi-sas/fisas_alimentation/commit/adeae3f8f07275a237c6972bd2618c4a67d4e407))
- relations of crud services ([7e9cda0](https://gitlab.com/fi-sas/fisas_alimentation/commit/7e9cda0e529f44efe42e8d263609ea1b4f9e4608))
- relations of cruds in entity dishs and add helper ([ccab76e](https://gitlab.com/fi-sas/fisas_alimentation/commit/ccab76e58b828bf0b5e8bc5171fcb48d1866c003))
- relations of cruds in entity fiscal_entities ([4b1ee53](https://gitlab.com/fi-sas/fisas_alimentation/commit/4b1ee534932ecfcecfa145523b5bebfc6d0b9f5c))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas_alimentation/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-11-19)

### Bug Fixes

- test the commit ([c156a3e](https://gitlab.com/fi-sas/fisas_alimentation/commit/c156a3e6677100bf7ca3beaceb37006152a462eb))
- test the commit ([b1e0814](https://gitlab.com/fi-sas/fisas_alimentation/commit/b1e0814ebdac466c25485f3f75d5b37a129c5390))
- test the commit ([c67be52](https://gitlab.com/fi-sas/fisas_alimentation/commit/c67be528428eef4c1c3e1ee4ce956fb78f5370e8))

### Features

- add all services ([328524c](https://gitlab.com/fi-sas/fisas_alimentation/commit/328524c019a928b074639297ff7aed0bc7853179))
- teste commit ([b5a577e](https://gitlab.com/fi-sas/fisas_alimentation/commit/b5a577e2d6a66343cf13f3e9cecd8b51a80edb1a))
- teste commit ([efd7ee7](https://gitlab.com/fi-sas/fisas_alimentation/commit/efd7ee7b3a1be02f1db4e658cd1df6d2e583af22))

# 1.0.0-rc.1 (2020-11-18)

### Features

- ms at a new base boilerplate and new migrations file ([4202d46](https://gitlab.com/fi-sas/fisas_alimentation/commit/4202d465104c12232fcd29b8568b2c893a365b2c))
