module.exports.up = async (db) =>
	db.schema

		//Create a table unit
		.createTable("unit", (table) => {
			table.increments();
			table.string("acronym", 5).notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
		})
		// Create a table unit_translation
		.createTable("unit_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("unit_id").unsigned().notNullable();
			table.unique(["language_id", "unit_id"]);
			table.foreign("unit_id").references("id").inTable("unit");
		})
		// Create a table nutrients
		.createTable("nutrient", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.decimal("ddr", 6, 2).notNullable();
			table.integer("unit_id").unsigned().notNullable();
			table.foreign("unit_id").references("id").inTable("unit");
		})
		// Create a table nutrient_translation
		.createTable("nutrient_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("nutrient_id").unsigned().notNullable();
			table.unique(["language_id", "nutrient_id"]);
			table.foreign("nutrient_id").references("id").inTable("nutrient");
		})
		// Create a table allergen
		.createTable("allergen", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
		})
		// Create a table allergen_translation
		.createTable("allergen_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("description").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("allergen_id").unsigned().notNullable();
			table.unique(["language_id", "allergen_id"]);
			table.foreign("allergen_id").references("id").inTable("allergen");
		})
		// Create a table user_allergen
		.createTable("user_allergen", (table) => {
			table.increments();
			table.integer("user_id").unsigned().notNullable();
			table.integer("allergen_id").unsigned().notNullable();
			table.foreign("allergen_id").references("id").inTable("allergen");
			table.unique(["user_id", "allergen_id"]);
		})
		// Create a table fiscal_entity
		.createTable("fiscal_entity", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.string("name").notNullable();
			table.string("tin", 20).notNullable();
			table.string("street").nullable();
			table.integer("door").unsigned().nullable();
			table.string("city").nullable();
			table.string("cep", 10).nullable();
			table.string("phone", 20).nullable();
			table.string("email").nullable();
			table.string("website").nullable();
			table.integer("account_id").unsigned().nullable();
			table.integer("logo_file_id").unsigned().nullable();
		})
		// Create a table fiscal_entity_user
		.createTable("fiscal_entity_user", (table) => {
			table.increments();
			table.integer("user_id").unsigned().notNullable();
			table.integer("created_by_user_id").unsigned().notNullable();
			table.boolean("manager").defaultTo(false).notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.unique(["fentity_id", "user_id"]);
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table complement
		.createTable("complement", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		//Create a table complement_translation
		.createTable("complement_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("description").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("complement_id").unsigned().notNullable();
			table.unique(["language_id", "complement_id"]);
			table.foreign("complement_id").references("id").inTable("complement");
		})
		//Creata a table family
		.createTable("family", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("file_id").unsigned().nullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		//Create a table family_translation
		.createTable("family_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("family_id").unsigned().notNullable();
			table.unique(["language_id", "family_id"]);
			table.foreign("family_id").references("id").inTable("family");
		})
		// Create a table wharehouse
		.createTable("wharehouse", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.string("name").notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table wharehouse_user
		.createTable("wharehouse_user", (table) => {
			table.increments();
			table.integer("user_id").unsigned().notNullable();
			table.integer("created_by_user_id").unsigned().notNullable();
			table.boolean("manager").defaultTo(false).notNullable();
			table.integer("wharehouse_id").unsigned().notNullable();
			table.unique(["wharehouse_id", "user_id"]);
			table.foreign("wharehouse_id").references("id").inTable("wharehouse");
		})
		// Create a table service
		.createTable("service", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.string("name").notNullable();
			table.enum("type", ["bar", "canteen"]).notNullable().defaultTo("bar");
			table.integer("wharehouse_id").unsigned().notNullable();
			table.integer("service_id").unsigned().notNullable();
			table.integer("school_id").unsigned().notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
			table.foreign("wharehouse_id").references("id").inTable("wharehouse");
		})
		// Create a table service_family
		.createTable("service_family", (table) => {
			table.increments();
			table.integer("family_id").unsigned().notNullable();
			table.integer("service_id").unsigned().notNullable();
			table.foreign("family_id").references("id").inTable("family");
			table.foreign("service_id").references("id").inTable("service");
			table.unique(["service_id", "family_id"]);
		})
		// Create a table service_device
		.createTable("service_device", (table) => {
			table.increments();
			table.integer("device_id").unsigned().notNullable();
			table.integer("service_id").unsigned().notNullable();
			table.foreign("service_id").references("id").inTable("service");
			table.unique(["service_id", "device_id"]);
		})
		// Create a table product
		.createTable("product", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("file_id").unsigned().nullable();
			table.string("code", 55).notNullable();
			table.decimal("cost_price", 6, 2).notNullable();
			table.integer("tax_cost_price_id").unsigned().nullable();
			table.decimal("price", 6, 2).notNullable();
			table.integer("tax_price_id").unsigned().nullable();
			table.boolean("available").defaultTo(true).notNullable();
			table.boolean("visible").defaultTo(true).notNullable();
			table.boolean("show_derivatives").defaultTo(false).notNullable();
			table.decimal("minimal_stock", 6, 2).notNullable();
			table
				.enum("track_stock", ["none", "product", "composition", "product_composition"])
				.notNullable()
				.defaultTo("product");
			table.enum("type", ["bar", "canteen"]).notNullable().defaultTo("bar");
			table.integer("unit_id").unsigned().notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
			table.foreign("unit_id").references("id").inTable("unit");
			table.unique(["code"]);
		})
		// Create table product_translation
		.createTable("product_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("description").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("product_id").unsigned().notNullable();
			table.unique(["language_id", "product_id"]);
			table.foreign("product_id").references("id").inTable("product");
		})
		// Create table family_product
		.createTable("family_product", (table) => {
			table.increments();
			table.integer("product_id").unsigned().notNullable();
			table.integer("family_id").unsigned().notNullable();
			table.foreign("family_id").references("id").inTable("family");
			table.foreign("product_id").references("id").inTable("product");
			table.unique(["product_id", "family_id"]);
		})
		// Create table product_allergen
		.createTable("product_allergen", (table) => {
			table.increments();
			table.integer("product_id").unsigned().notNullable();
			table.integer("allergen_id").unsigned().notNullable();
			table.foreign("allergen_id").references("id").inTable("allergen");
			table.foreign("product_id").references("id").inTable("product");
			table.unique(["product_id", "allergen_id"]);
		})
		// Create table product_complement
		.createTable("product_complement", (table) => {
			table.increments();
			table.integer("product_id").unsigned().notNullable();
			table.integer("complement_id").unsigned().notNullable();
			table.foreign("complement_id").references("id").inTable("complement");
			table.foreign("product_id").references("id").inTable("product");
			table.unique(["product_id", "complement_id"]);
		})
		// Create table product_nutrient
		.createTable("product_nutrient", (table) => {
			table.increments();
			table.integer("product_id").unsigned().notNullable();
			table.integer("nutrient_id").unsigned().notNullable();
			table.decimal("quantity", 6, 2).notNullable();
			table.foreign("nutrient_id").references("id").inTable("nutrient");
			table.foreign("product_id").references("id").inTable("product");
			table.unique(["product_id", "nutrient_id"]);
		})
		// create a table compound
		.createTable("compound", (table) => {
			table.increments();
			table.integer("product_id").unsigned().notNullable();
			table.integer("compound_id").unsigned().notNullable();
			table.decimal("quantity", 6, 2).notNullable();
			table.foreign("compound_id").references("id").inTable("product");
			table.foreign("product_id").references("id").inTable("product");
			table.unique(["product_id", "compound_id"]);
		})
		// Create a table recipe
		.createTable("recipe", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.string("name").notNullable();
			table.string("description").notNullable();
			table.integer("number_doses").unsigned().notNullable().defaultTo(0);
			table.text("preparation").nullable();
			table.text("preparation_mode").nullable();
			table.text("microbiological_criteria").nullable();
			table.text("packaging").nullable();
			table.text("use").nullable();
			table.text("distribution").nullable();
			table.text("comment").nullable();
			table.text("associated_docs").nullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table recipe_product
		.createTable("recipe_product", (table) => {
			table.increments();
			table.integer("ingredient_id").unsigned().notNullable();
			table.integer("recipe_id").unsigned().notNullable();
			table.decimal("liquid_quantity", 6, 2).notNullable();
			table.decimal("gross_quantity", 6, 2).notNullable();
			table.foreign("recipe_id").references("id").inTable("recipe");
			table.foreign("ingredient_id").references("id").inTable("product");
			table.unique(["ingredient_id", "recipe_id"]);
		})
		// Create a table recipe_step
		.createTable("recipe_step", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("file_id").unsigned().nullable();
			table.integer("step").unsigned().notNullable().defaultTo(0);
			table.text("description").nullable();
			table.integer("recipe_id").unsigned().notNullable();
			table.foreign("recipe_id").references("id").inTable("recipe");
		})
		// Create a table dish_type
		.createTable("dish_type", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("tax_id").unsigned().notNullable();
			table.string("code").notNullable();
			table.decimal("price", 6, 2).notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table dish_type_translation
		.createTable("dish_type_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("dish_type_id").unsigned().notNullable();
			table.unique(["language_id", "dish_type_id"]);
			table.foreign("dish_type_id").references("id").inTable("dish_type");
		})
		// Create a table disponibility
		.createTable("disponibility", (table) => {
			table.increments();
			table.dateTime("begin_date").nullable();
			table.dateTime("end_date").nullable();
			table.boolean("monday").defaultTo(true).notNullable();
			table.boolean("tuesday").defaultTo(true).notNullable();
			table.boolean("wednesday").defaultTo(true).notNullable();
			table.boolean("thursday").defaultTo(true).notNullable();
			table.boolean("friday").defaultTo(true).notNullable();
			table.boolean("saturday").defaultTo(true).notNullable();
			table.boolean("sunday").defaultTo(true).notNullable();
			table.time("annulment_maximum_hour").nullable();
			table.time("minimum_hour").nullable();
			table.time("maximum_hour").nullable();
			table.integer("product_id").unsigned().nullable();
			table.integer("dish_type_lunch_id").unsigned().nullable();
			table.integer("dish_type_dinner_id").unsigned().nullable();
			table.foreign("dish_type_lunch_id").references("id").inTable("dish_type");
			table.foreign("dish_type_dinner_id").references("id").inTable("dish_type");
			table.foreign("product_id").references("id").inTable("product");
			table.unique("dish_type_lunch_id");
			table.unique("dish_type_dinner_id");
			table.unique("product_id");
		})
		// Create a table price_variation
		.createTable("price_variation", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.string("description").nullable();
			table.integer("tax_id").unsigned().notNullable();
			table.integer("profile_id").unsigned().notNullable();
			table.enum("meal", ["lunch", "dinner", "breakfast"]).nullable();
			table.decimal("price", 6, 2).notNullable();
			table.time("time").notNullable().defaultTo("00:00:00");
			table.integer("product_id").unsigned().nullable();
			table.integer("dish_type_id").unsigned().nullable();
			table.foreign("dish_type_id").references("id").inTable("dish_type");
			table.foreign("product_id").references("id").inTable("product");
		})
		// Create a table dish
		.createTable("dish", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("file_id").unsigned().nullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table dish_translation
		.createTable("dish_translation", (table) => {
			table.increments();
			table.string("name").notNullable();
			table.string("description").nullable();
			table.integer("language_id").unsigned().notNullable();
			table.integer("dish_id").unsigned().notNullable();
			table.unique(["language_id", "dish_id"]);
			table.foreign("dish_id").references("id").inTable("dish");
		})
		// Create a table dish_recipe
		.createTable("dish_recipe", (table) => {
			table.increments();
			table.integer("dish_id").unsigned().notNullable();
			table.integer("recipe_id").unsigned().notNullable();
			table.foreign("dish_id").references("id").inTable("dish");
			table.foreign("recipe_id").references("id").inTable("recipe");
			table.unique(["dish_id", "recipe_id"]);
		})
		// Create a table dish_dish_type
		.createTable("dish_dish_type", (table) => {
			table.increments();
			table.integer("dish_id").unsigned().notNullable();
			table.integer("dish_type_id").unsigned().notNullable();
			table.foreign("dish_id").references("id").inTable("dish");
			table.foreign("dish_type_id").references("id").inTable("dish_type");
			table.unique(["dish_id", "dish_type_id"]);
		})
		// Create a table menu
		.createTable("menu", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.enum("meal", ["lunch", "dinner", "breakfast"]).notNullable().defaultTo("lunch");
			table.date("date").notNullable();
			table.boolean("validated").defaultTo(false).notNullable();
			table.integer("service_id").unsigned().notNullable();
			table.foreign("service_id").references("id").inTable("service");
			table.unique(["service_id", "date", "meal"]);
		})
		// Create a table menu_dish
		.createTable("menu_dish", (table) => {
			table.increments();
			table.boolean("available").defaultTo(true).notNullable();
			table.integer("doses_available").notNullable();
			table.dateTime("available_until").nullable();
			table.dateTime("nullable_until").nullable();
			table.integer("menu_id").unsigned().notNullable();
			table.integer("dish_id").unsigned().notNullable();
			table.integer("type_id").unsigned().notNullable();
			table.foreign("menu_id").references("id").inTable("menu");
			table.foreign("dish_id").references("id").inTable("dish");
			table.foreign("type_id").references("id").inTable("dish_type");
			table.unique(["menu_id", "dish_id", "type_id"]);
		})
		// Create a table reservation
		.createTable("reservation", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.date("date").notNullable();
			table.string("location").nullable();
			table.integer("menu_dish_id").unsigned().notNullable();
			table.integer("movement_id").unsigned().notNullable();
			table.integer("item_id").unsigned().notNullable();
			table.integer("annulment_movement_id").unsigned().nullable();
			table.integer("device_id").unsigned().notNullable();
			table.integer("user_id").unsigned().notNullable();
			table.boolean("is_from_pack").notNullable().defaultTo(false);
			table.boolean("is_served").notNullable().defaultTo(false);
			table.boolean("is_available").notNullable().defaultTo(false);
			table.boolean("has_canceled").notNullable().defaultTo(false);
			table.dateTime("served_at").nullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("menu_dish_id").references("id").inTable("menu_dish");
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table order
		.createTable("order", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.boolean("served").defaultTo(false).notNullable();
			table.integer("user_id").unsigned().notNullable();
			table.dateTime("served_at").nullable();
			table.integer("served_by_user_id").unsigned().notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.foreign("fentity_id").references("id").inTable("fiscal_entity");
		})
		// Create a table order_line
		.createTable("order_line", (table) => {
			table.increments();
			table.decimal("quantity", 6, 2).notNullable();
			table.integer("order_id").unsigned().notNullable();
			table.integer("product_id").unsigned().notNullable();
			table.foreign("order_id").references("id").inTable("order");
			table.foreign("product_id").references("id").inTable("product");
		})
		// Create a table stock
		.createTable("stock", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.string("lote").nullable();
			table.dateTime("expired").notNullable();
			table.decimal("quantity", 6, 2).notNullable();
			table.integer("product_id").unsigned().notNullable();
			table.integer("wharehouse_id").unsigned().notNullable();
			table.foreign("product_id").references("id").inTable("product");
			table.foreign("wharehouse_id").references("id").inTable("wharehouse");
			table.unique(["product_id", "wharehouse_id", "lote"]);
		})
		// Create a table stock_operation
		.createTable("stock_operation", (table) => {
			table.increments();
			table.dateTime("created_at").notNullable();
			table.dateTime("updated_at").notNullable();
			table.integer("order_id").unsigned().nullable();
			table.integer("user_id").unsigned().nullable();
			table.integer("stock_id").unsigned().notNullable();
			table.enum("operation", ["in", "out", "discard"]).notNullable();
			table.decimal("quantity", 6, 2).notNullable();
			table.decimal("quantity_before", 6, 2).notNullable();
			table.decimal("quantity_after", 6, 2).notNullable();
			table.string("lote").notNullable();
			table.foreign("order_id").references("id").inTable("order");
			table.foreign("stock_id").references("id").inTable("stock");
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("stock_operation")
		.dropTable("stock")
		.dropTable("order_line")
		.dropTable("order")
		.dropTable("reservation")
		.dropTable("menu_dish")
		.dropTable("menu")
		.dropTable("dish_dish_type")
		.dropTable("dish_recipe")
		.dropTable("dish_translation")
		.dropTable("dish")
		.dropTable("price_variation")
		.dropTable("disponibility")
		.dropTable("dish_type_translation")
		.dropTable("dish_type")
		.dropTable("recipe_step")
		.dropTable("recipe_product")
		.dropTable("recipe")
		.dropTable("compound")
		.dropTable("product_nutrient")
		.dropTable("product_complement")
		.dropTable("user_allergen")
		.dropTable("family_product")
		.dropTable("product_translation")
		.dropTable("product")
		.dropTable("service_device")
		.dropTable("service_family")
		.dropTable("service")
		.dropTable("wharehouse_user")
		.dropTable("wharehouse")
		.dropTable("family_translation")
		.dropTable("family")
		.dropTable("complement_translation")
		.dropTable("complement")
		.dropTable("fiscal_entity_user")
		.dropTable("fiscal_entity")
		.dropTable("product_allergen")
		.dropTable("allergen_translation")
		.dropTable("allergen")
		.dropTable("nutrient_translation")
		.dropTable("nutrient")
		.dropTable("unit_translation")
		.dropTable("unit");
