module.exports.up = async (db) =>
	db.schema
		//Create a table Preferencial Canteen
		.createTable("preferencial_canteen", (table) => {
			table.increments();
			table.integer("user_id").unsigned().notNullable();
			table.integer("canteen_lunch_id").unsigned().nullable().references("id").inTable("service");
			table.integer("canteen_dinner_id").unsigned().nullable().references("id").inTable("service");
			table.unique(["user_id"]);
		});

module.exports.down = async (db) => db.schema.dropTable("preferencial_canteen");

module.exports.configuration = { transaction: true };
