module.exports.up = async (db) =>
	db.schema
		.alterTable("reservation", (table) => {
			table.string("movement_id").nullable().alter();
			table.string("annulment_movement_id").nullable().alter();
			table.string("item_id").nullable().alter();
			table.integer("device_id").nullable().alter();
		})
		.alterTable("order", (table) => {
			table.integer("served_by_user_id").unsigned().nullable().alter();
		});

module.exports.down = async (db) =>
	db.schema
		.alterTable("reservation", (table) => {
			table.integer("menu_dish_id").unsigned().notNullable().alter();
			table.integer("movement_id").unsigned().notNullable().alter();
			table.integer("device_id").unsigned().notNullable().alter();
			table.integer("annulment_movement_id").unsigned().nullable();
		})
		.alterTable("order", (table) => {
			table.integer("served_by_user_id").unsigned().notNullable().alter();
		});

module.exports.configuration = { transaction: true };
