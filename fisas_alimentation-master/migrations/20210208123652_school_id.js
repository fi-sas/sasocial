module.exports.up = async (db) =>
	db.schema.alterTable("service", (table) => {
		table.renameColumn("school_id", "organic_unit_id");
	});

module.exports.down = async (db) =>
	db.schema.alterTable("service", (table) => {
		table.renameColumn("organic_unit_id", "school_id");
	});

module.exports.configuration = { transaction: true };
