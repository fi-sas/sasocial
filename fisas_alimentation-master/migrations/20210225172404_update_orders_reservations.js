module.exports.up = async (db) =>
	db.schema
		.alterTable("reservation", (table) => {
			table.integer("service_id").nullable();
		})
		.alterTable("order", (table) => {
			table.integer("service_id").nullable();
		});

module.exports.down = async (db) =>
	db.schema
		.alterTable("reservation", (table) => {
			table.dropColumn("service_id");
		})
		.alterTable("order", (table) => {
			table.dropColumn("service_id");
		});

module.exports.configuration = { transaction: true };
