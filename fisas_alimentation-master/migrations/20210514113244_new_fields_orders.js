module.exports.up = async (db) =>
	db.schema
		.alterTable("order", (table) => {
			table.boolean("is_wait").nullable();
			table.double("total_price");
		})
		.alterTable("order_line", (table) => {
			table.enum("status", ["SERVED", "DELETED", "NOT_SERVED"]).nullable();
			table.string("item_id");
			table.double("price");
			table.integer("stock_id").references("id").inTable("stock").nullable();
		});

module.exports.down = async (db) =>
	db.schema
		.alterTable("order", (table) => {
			table.dropColumn("is_wait");
			table.dropColumn("total_price");
		})
		.alterTable("order_line", (table) => {
			table.dropColumn("status");
			table.dropColumn("item_id");
			table.dropColumn("price");
			table.dropColumn("stock_id");
		});

module.exports.configuration = { transaction: true };
