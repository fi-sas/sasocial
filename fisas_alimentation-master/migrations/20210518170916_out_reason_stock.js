module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.string("out_stock_reason").nullable();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("out_stock_reason");
	});

module.exports.configuration = { transaction: true };
