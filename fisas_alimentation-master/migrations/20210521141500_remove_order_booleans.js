module.exports.up = async (db) =>
	db.schema.alterTable("order", (table) => {
		table.dropColumn("is_wait");
		table.dropColumn("has_canceled");
		table.dropColumn("served");
		table.enum("status", ["CANCELED", "WAITING", "SERVED", "NOT_SERVED"]);
	});

module.exports.down = async (db) =>
	db.schema.alterTable("order", (table) => {
		table.boolean("is_wait").nullable();
		table.boolean("has_canceled");
		table.boolean("served");
	});

module.exports.configuration = { transaction: true };
