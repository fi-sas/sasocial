exports.up = (knex) => {
	return knex.schema.alterTable("disponibility", (table) => {
		table.specificType("annulment_maximum_hour", "timetz").nullable().alter();
		table.specificType("minimum_hour", "timetz").nullable().alter();
		table.specificType("maximum_hour", "timetz").nullable().alter();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("disponibility", (table) => {
		table.time("annulment_maximum_hour").nullable().alter();
		table.time("minimum_hour").nullable().alter();
		table.time("maximum_hour").nullable().alter();
	});
};
