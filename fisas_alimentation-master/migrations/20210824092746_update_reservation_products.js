
exports.up = (knex) => {
	return knex.schema.alterTable("product", (table) => {
		table.dropUnique("code");
		table.unique(["code", "fentity_id"]);
	})
		.alterTable("reservation", (table) => {
			table.integer("served_by").unsigned().nullable();
		});
};

exports.down = (knex) => {
	return knex.schema.alterTable("product", (table) => {
		table.dropUnique(["code", "fentity_id"]);
		table.unique("code");
	})
		.alterTable("reservation", (table) => {
			table.dropColumn("served_by");
		});
};