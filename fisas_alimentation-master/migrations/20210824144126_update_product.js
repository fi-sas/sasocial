
exports.up = (knex) => {
	return knex.schema.alterTable("product", (table) => {
		table.decimal("price", 6, 2).nullable().alter()
	})
};

exports.down = (knex) => {
	return knex.schema.alterTable("product", (table) => {
        table.decimal("price", 6, 2).norNullable().alter()
	})
};