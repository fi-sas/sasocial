
exports.up = (knex) => {
    return knex.
    schema.alterTable("reservation", (table) => {
        table.enum("meal", ["lunch", "dinner", "breakfast"])
	}).raw(`UPDATE reservation
    SET meal = menu.meal
    FROM (
        select  md.id as id, m.meal as meal
        FROM menu_dish as md inner join menu as m on m.id = md.menu_id ) AS menu
    WHERE
        menu.id = reservation.menu_dish_id`);
};

exports.down = (knex) => {
	return knex.schema.alterTable("reservation", (table) => {
        table.dropColumn("meal");
	})
};