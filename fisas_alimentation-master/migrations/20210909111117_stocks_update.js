

exports.up = (knex) => {
	return knex.schema.alterTable("stock", (table) => {
		table.dateTime("expired").nullable().alter()
	})
};

exports.down = (knex) => {
	return knex.schema.alterTable("product", (table) => {
        table.dateTime("expired").notNullable();
	})
};