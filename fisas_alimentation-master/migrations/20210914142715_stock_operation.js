exports.up = function (knex) {
	return knex.schema.alterTable("stock_operation", (table) => {
		table.integer("wharehouse_id").unsigned().nullable();
	}).raw(`UPDATE stock_operation
    SET wharehouse_id = stock.wharehouse_id
    FROM (
        select  s.id as id, s.wharehouse_id as wharehouse_id
        FROM stock as s) AS stock
    WHERE
    stock.id = stock_operation.stock_id`);
};

exports.down = function (knex) {
	return knex.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("wharehouse_id");
	});
};
