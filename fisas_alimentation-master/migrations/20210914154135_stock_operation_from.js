exports.up = function (knex) {
	return knex.schema.alterTable("stock_operation", (table) => {
		table.integer("wharehouse_id").unsigned().notNullable().alter();
		table.integer("transfer_wharehouse_id").unsigned().nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("transfer_wharehouse_id");
	});
};
