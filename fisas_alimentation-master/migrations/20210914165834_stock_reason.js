module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.string("stock_reason").nullable();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("stock_reason");
	});

module.exports.configuration = { transaction: true };
