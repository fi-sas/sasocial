module.exports.up = async (db) => {
	return db.raw("UPDATE stock_operation SET stock_reason = out_stock_reason").then(() => {
		return db.schema.alterTable("stock_operation", (table) => {
			table.dropColumn("out_stock_reason");
		});
	});
};

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.string("out_stock_reason").nullable();
	});

module.exports.configuration = { transaction: true };
