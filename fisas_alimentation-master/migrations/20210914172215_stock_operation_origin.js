module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.integer("device_id").unsigned().nullable();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("device_id");
	});

module.exports.configuration = { transaction: true };
