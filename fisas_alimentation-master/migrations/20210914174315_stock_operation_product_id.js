module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.integer("product_id").unsigned().nullable();
	}).raw(`UPDATE stock_operation
    SET product_id = stock.product_id
    FROM (
        select  s.id as id, s.product_id as product_id
        FROM stock as s) AS stock
    WHERE
    stock.id = stock_operation.stock_id`);

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("product_id");
	});

module.exports.configuration = { transaction: true };
