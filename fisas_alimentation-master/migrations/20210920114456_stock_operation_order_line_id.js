module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.integer("order_line_id").unsigned().nullable();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.dropColumn("order_line_id");
	});

module.exports.configuration = { transaction: true };
