module.exports.up = async (db) =>
	db.schema
		.alterTable("stock", (table) => {
			table.decimal("quantity", 12, 2).notNullable().alter();
		})
		.alterTable("stock_operation", (table) => {
			table.decimal("quantity", 12, 2).notNullable().alter();
		});

module.exports.down = async (db) =>
	db.schema
		.alterTable("stock", (table) => {
			table.decimal("quantity", 6, 2).notNullable().alter();
		})
		.alterTable("stock_operation", (table) => {
			table.decimal("quantity", 6, 2).notNullable().alter();
		});

module.exports.configuration = { transaction: true };
