exports.up = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.boolean("pos_operations").notNullable().defaultTo(false);
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.dropColumn("pos_operations");
	});
};
