module.exports.up = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.decimal("quantity_before", 12, 2).notNullable().alter();
		table.decimal("quantity_after", 12, 2).notNullable().alter();
	});

module.exports.down = async (db) =>
	db.schema.alterTable("stock_operation", (table) => {
		table.decimal("quantity_before", 6, 2).notNullable().alter();
		table.decimal("quantity_after", 6, 2).notNullable().alter();
	});

module.exports.configuration = { transaction: true };
