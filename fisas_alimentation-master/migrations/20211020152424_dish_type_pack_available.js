exports.up = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.boolean("pack_available").notNullable().defaultTo(false);
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.dropColumn("pack_available");
	});
};
