exports.up = (knex) => {
	return knex.schema.alterTable("reservation", (table) => {
		table.integer("user_meal_pack_id").unsigned().nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("reservation", (table) => {
		table.dropColumn("user_meal_pack_id");
	});
};
