exports.up = (knex) => {
	return knex.schema.alterTable("reservation", (table) => {
		table.decimal("price", 6, 2).notNullable().defaultTo(0);
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("reservation", (table) => {
		table.dropColumn("price");
	});
};
