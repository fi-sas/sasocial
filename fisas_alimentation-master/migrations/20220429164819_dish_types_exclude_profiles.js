exports.up = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.specificType("excluded_user_profile_ids", "int[]").unsigned().nullable().defaultTo("{}");
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("dish_type", (table) => {
		table.dropColumn("excluded_user_profile_ids");
	});
};
