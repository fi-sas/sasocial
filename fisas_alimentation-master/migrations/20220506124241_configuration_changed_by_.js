exports.up = (knex) => {
	return knex.schema.alterTable("configuration", (table) => {
		table.integer("updated_by_id").unsigned().nullable();
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("configuration", (table) => {
		table.dropColumn("updated_by_id");
	});
};
