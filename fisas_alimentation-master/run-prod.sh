#!/usr/bin/env bash
while true; do
    read -p "Detached mode: Run containers in the background (y/n)? " yn
    case $yn in
        [Yy]* ) sudo docker-compose -f docker-compose.prod.yml up --build -d; break;;
        [Nn]* ) sudo docker-compose -f docker-compose.prod.yml up --build; break;;
        * ) echo "Please answer yes or no.";;
    esac
done
exit