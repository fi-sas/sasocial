"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.allergens",
	table: "allergen",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "allergens")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "created_at", "updated_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.allergens_translations",
					"translations",
					"id",
					"allergen_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						name: { type: "string" },
						description: { type: "string", optional: true },
					},
				},
			},
			active: { type: "boolean" },
			created_at: { type: "date", convert: true, optional: true },
			updated_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
		before: {
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name", "description"],
						"alimentation.allergens_translations",
						"allergen_id",
					);
				},
			],
			create: [
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateAllergenId",
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			// CONFIRMAR O DELETE
			// remove: [
			// 	"remove_translations"
			// ]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateAllergenId(ctx) {
			const ids = this._find(ctx, {
				query: {
					id: ctx.params.id,
				},
			});
			if (ids.length === 0) {
				throw new Errors.EntityNotFoundError("Allergen not found", ctx.params.id);
			}
		},

		/**
		 * Method for validate languages id
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				const language = await ctx.call("configuration.languages.get", { id });
				if (language.length === 0) {
					throw new Errors.EntityNotFoundError("Language not found", ctx.params.id);
				}
			}
		},

		/**
		 * Method for save translations of allergens
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.allergens_translations.save_translations", {
				allergen_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.allergens_translations.find", {
				query: {
					allergen_id: res[0].id,
				},
			});

			return res;
		},

		/**
		 * Method for remove translations os allergens
		 */
		async remove_translations(ctx) {
			return await ctx.call("alimentation.allergens_translations.remove_translations", {
				allergen_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
