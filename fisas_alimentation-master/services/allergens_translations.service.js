"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.allergens_translations",
	table: "allergen_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "allergens_translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "description", "language_id", "allergen_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			description: { type: "string" },
			language_id: { type: "number", positive: true, integer: true },
			allergen_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			find: [
				async function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query.language_id = ctx.meta.language_id;
						const total = await this._count(ctx, ctx.params);
						if (total === 0) {
							delete ctx.params.query.language_id;
						}
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save translations
		 */
		save_translations: {
			params: {
				allergen_id: { type: "number", positive: true, integer: true },
				translations: {
					type: "array",
					item: {
						name: { type: "string" },
						description: { type: "string" },
						language_id: { type: "number", positive: true, integer: true },
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					allergen_id: ctx.params.allergen_id,
				});
				this.clearCache();
				for (const trans of ctx.params.translations) {
					const entities = {
						allergen_id: ctx.params.allergen_id,
						description: trans.description,
						language_id: trans.language_id,
						name: trans.name,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for remove translations
		 */
		remove_translations: {
			params: {
				allergen_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				const removes = await this.adapter.removeMany({
					allergen_id: ctx.params.allergen_id,
				});
				this.clearCache();
				return removes;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
