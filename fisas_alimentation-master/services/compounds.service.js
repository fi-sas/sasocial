"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.compounds",
	table: "compound",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "compounds")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "product_id", "compound_id", "quantity"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			product_id: { type: "number", positive: true, integer: true },
			compound_id: { type: "number", positive: true, integer: true },
			quantity: { type: "number", positive: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		saveCompoundsProduct: {
			params: {
				product_id: { type: "number", positive: true, integer: true },
				compounds: {
					type: "array",
					items: {
						type: "object",
						props: {
							compound_id: { type: "number", integer: true, positive: true },
							quantity: { type: "number" },
						},
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				for (let compound of ctx.params.compounds) {
					const compounds_info = await this._find(ctx, {
						query: {
							product_id: compound.compound_id,
						},
					});
					for (const compound_info of compounds_info) {
						const exist = ctx.params.compounds.find(
							(elem) => elem.compound_id === compound_info.compound_id,
						);
						if (exist) {
							throw new Errors.ValidationError(
								"In the list of compounds there are compounds of compounds",
								"LIST_COMPOUNDS_THERE_ARE_COMPOUNDS_OF_COMPOUNDS",
								{},
							);
						}
					}
					compound.product_id = ctx.params.product_id;
					const insert = await this._create(ctx, compound);
					resp.push(insert[0]);
				}
				return resp;
			},
		},
		getCompoundsByProduct: {
			async handler(ctx) {
				let resp = [];
				const listCompounds = await this._find(ctx, {
					query: {
						product_id: ctx.params.id,
					},
				});
				for (const compound of listCompounds) {
					const compound_info = await ctx.call("alimentation.products.get", {
						id: compound.compound_id,
						withRelated: "translations,unit",
					});
					const resp_object = {
						quantity: compound.quantity,
						compound: compound_info[0],
					};
					resp.push(resp_object);
				}
				return resp;
			},
		},
		getDerivativesByProduct: {
			async handler(ctx) {
				let resp = [];
				const listDerivatives = await this._find(ctx, {
					query: {
						compound_id: ctx.params.id,
					},
				});
				for (const derivative of listDerivatives) {
					const info = await ctx.call("alimentation.products.find", {
						query: {
							id: derivative.product_id,
						},
						withRelated: "translations",
					});
					resp.push(info[0]);
				}
				return resp;
			},
		},
		getProductCompoundDeep: {
			visibility: "published",
			authorization: false,
			authentication: false,
			rest: "GET /deep",
			cache: {
				keys: ["product_id"],
			},
			handler(ctx) {
				/*
				 * Return a arrray with compound_id for remove the stock on order
				 * [{"id":2,"product_id":3,"compound_id":2,"quantity":1}, ....]
				 *
				 */

				return this.adapter.raw(
					`WITH RECURSIVE composition AS (
					SELECT
						cc.id,
						cc.product_id,
						cc.compound_id,
						cc.quantity,
						p.track_stock
					FROM
						compound cc
					INNER JOIN product p on p.id = cc.compound_id
					WHERE
						cc.product_id = ?
					UNION
						SELECT
							c.id,
							c.product_id,
							c.compound_id,
							c.quantity,
							pp.track_stock
						FROM compound c
						INNER JOIN product pp on pp.id = c.compound_id
						INNER JOIN composition cs ON cs.compound_id = c.product_id AND (pp.track_stock != 'none')
				) SELECT
					*
				FROM
					composition;`,
					[ctx.params.product_id],
				);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
