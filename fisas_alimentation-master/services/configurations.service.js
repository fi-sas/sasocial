"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "updated_by_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: { type: "string" },
			value: { type: "string" },
			updated_by_id: { type: "number", positive: true, convert: true, optional: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [],
			update: [],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});

				const data = {};

				if (ctx.params.withRelated === "info") {
					const SHOW_ALLERGENS_ON_MENU = configs.find((c) => c.key === "SHOW_ALLERGENS_ON_MENU");
					if (SHOW_ALLERGENS_ON_MENU.updated_by_id) {
						SHOW_ALLERGENS_ON_MENU.updated_by = await ctx
							.call("authorization.users.find", {
								query: {
									id: SHOW_ALLERGENS_ON_MENU.updated_by_id,
								},
							})
							.then((users) => (users.length > 0 ? users[0] : null));

						data["SHOW_ALLERGENS_ON_MENU"] = {
							updated_by: SHOW_ALLERGENS_ON_MENU.updated_by,
							updated_by_id: SHOW_ALLERGENS_ON_MENU.updated_by_id,
							updated_at: SHOW_ALLERGENS_ON_MENU.updated_at,
						};
					}
				}

				return [result, data];
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			params: {
				SHOW_ALLERGENS_ON_MENU: { type: "boolean" },
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key: key } }).then((config) => {
						if (
							config.length > 0 &&
							ctx.params[config[0].key] != null &&
							ctx.params[config[0].key] !== config[0].value
						) {
							return this._update(
								ctx,
								{
									id: config[0].id,
									value: JSON.stringify(ctx.params[config[0].key]),
									updated_by_id: ctx.meta.user.id,
									updated_at: new Date(),
								},
								true,
							);
						}
					});
				});
				return Promise.all(promisses).then(() => ctx.call("alimentation.configurations.list", {}));
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
