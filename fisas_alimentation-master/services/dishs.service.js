"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.dishs",
	table: "dish",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "dishs")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "file_id", "fentity_id", "created_at", "updated_at"],
		defaultWithRelateds: ["translations", "type", "file"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.dishs-translations",
					"translations",
					"id",
					"dish_id",
				);
			},
			recipes(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.dishs-recipes.getRecipes", { dish_id: doc.id })
							.then((res) => (doc.recipes = res));
					}),
				);
			},
			type(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.dishs-dishs-types.getTypes", { dish_id: doc.id })
							.then((res) => (doc.type = res));
					}),
				);
			},
			entity(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.entities", "entity", "fentity_id");
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			active: { type: "boolean", default: true },
			file_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						name: { type: "string" },
						description: { type: "string", optional: true },
					},
				},
			},
			recipes_ids: {
				type: "array",
				items: {
					type: "number",
				},
			},
			types_ids: {
				type: "array",
				items: {
					type: "number",
				},
			},
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optionla: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"validateRecipes",
				"validateDishType",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateDishsEntity",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				addQueryFentity,
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name", "description"],
						"alimentation.dishs-translations",
						"dish_id",
					);
				},
				async function fix_query(ctx) {
					delete ctx.params.query.types;
					delete ctx.params.query.name;
					if (ctx.params.query.dish_type_id) {
						const dishs_types = await ctx.call("alimentation.dishs-dishs-types.find", {
							query: {
								dish_type_id: ctx.params.query.dish_type_id,
							},
						});
						delete ctx.params.query.dish_type_id;
						ctx.params.query.id = dishs_types.map((id) => id.dish_id);
					}
				},
			],
			get: ["validateDishsEntity"],
			// TODO: Falta saber o que fazer no delete
			// remove: [
			// 	"removeDishsTrasnlations",
			// 	"removeDishsType",
			// 	"removeDishsRecipes"
			// ]
		},
		after: {
			create: ["saveTranslations", "saveRecipes", "saveDishTypes"],
			update: ["saveTranslations", "saveRecipes", "saveDishTypes"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		calculatedAllergen: {
			cache: {
				keys: ["dish_id"],
			},
			params: {
				dish_id: { type: "number" },
			},
			async handler(ctx) {
				// GET THE DISH_RECIPES
				const recipes = await ctx.call("alimentation.dishs-recipes.find", {
					withRelated: false,
					query: {
						dish_id: ctx.params.dish_id,
					},
				});
				// GET THE RECIPES PRODUCTS
				const products = await ctx.call("alimentation.recipes-products.find", {
					withRelated: false,
					query: {
						recipe_id: recipes.map((r) => r.recipe_id),
					},
				});
				// GET THE PRODCUT ALLERGENS
				const prod_alerg = await ctx.call("alimentation.products-allergens.find", {
					withRelated: false,
					query: {
						product_id: products.map((p) => p.ingredient_id),
					},
				});

				return ctx.call("alimentation.allergens.find", {
					withRelated: "translations",
					query: {
						id: prod_alerg.map((pa) => pa.allergen_id),
					},
				});
			},
		},

		calculatedAllergenAndNutrientsComposition: {
			cache: {
				keys: ["dish_id"],
			},
			params: {
				dish_id: { type: "number" },
			},
			async handler(ctx) {
				const dish = await ctx.call("alimentation.dishs.get", {
					id: ctx.params.dish_id,
					withRelated: ["recipes"],
				});
				const nutrients = [];
				const allergens = [];
				const composition = [];

				for (const rec of dish[0].recipes) {
					for (const product of rec.products) {
						if (product.translations[0] !== undefined) {
							composition.push(product.translations[0]);
						}
						for (const nutri of product.nutrients) {
							const nutritemp = Object.assign(nutri, {});
							const temp_quantity =
								nutritemp.quantity * (product.recipes_quantitys.liquid_quantity / rec.number_doses);
							let findA = nutrients.find((nt) => nt.nutrient.id === nutritemp.nutrient.id);
							if (findA) {
								nutrients[nutrients.indexOf(findA)].quantity += temp_quantity;
							} else {
								nutri.quantity = temp_quantity;
								nutrients.push(nutri);
							}
							// const nutritemp = Object.assign(nutri, {});
							// const temp_quantity =
							// 	nutritemp.quantity * (product.recipes_quantitys.liquid_quantity / rec.number_doses);
							// if (nutrients.indexOf(nutritemp) !== -1) {
							// 	nutrients[nutrients.indexOf(nutritemp)].quantity += temp_quantity;
							// } else {
							// 	nutri.quantity = temp_quantity;
							// 	nutrients.push(nutri);
							// }
						}
						if (product.allergens.length !== 0) {
							for (const allergen of product.allergens) {
								if (allergens.indexOf(allergen) === -1) {
									allergens.push(allergen);
								}
							}
						}
					}
				}
				const resp = {
					nutrients: nutrients,
					allergens: allergens,
					composition: composition,
				};
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.products.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-nutrients.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-allergens.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-complements.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.recipes.*"(ctx) {
			this.logger.info("CAPTURED EVENT => recipes");
			this.clearCache();
		},
		"alimentation.prices-variations.*"(ctx) {
			this.logger.info("CAPTURED EVENT => alimentation.prices-variations");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validade recipes Ids
		 */
		async validateRecipes(ctx) {
			const list_recipes = ctx.params.recipes_ids;
			if (Array.isArray(list_recipes)) {
				for (const recipes of list_recipes) {
					const recipes_entity = await ctx.call("alimentation.recipes.find", {
						query: {
							id: recipes,
							fentity_id: ctx.meta.alimentation_entity_id,
						},
					});
					if (recipes_entity.length === 0) {
						throw new Errors.ValidationError(
							"The recipe id sent doesnt exist",
							"FOOD_RECIPE_DONT_EXIST",
							{},
						);
					}
				}
			}
		},

		/**
		 * Methods for validades Dishs Types
		 */
		async validateDishType(ctx) {
			const list_dishTypes = ctx.params.types_ids;
			if (Array.isArray(list_dishTypes)) {
				for (const dishTypes of list_dishTypes) {
					const dishsType_entity = await ctx.call("alimentation.dishs-types.find", {
						query: {
							id: dishTypes,
							fentity_id: ctx.meta.alimentation_entity_id,
						},
					});
					if (dishsType_entity.length === 0) {
						throw new Errors.ValidationError(
							"The dish type id sent doesnt exist",
							"FOOD_DISH_TYPE_NOT_EXIST",
							{},
						);
					}
				}
			}
		},

		/**
		 * Method for save translations of Dishs
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.dishs-translations.save_translations", {
				dish_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.dishs-translations.find", {
				query: {
					dish_id: res[0].id,
				},
			});
			return res;
		},

		/**
		 * Method for save Dishs Recipes
		 */
		async saveRecipes(ctx, res) {
			await ctx.call("alimentation.dishs-recipes.save_recipes", {
				dish_id: res[0].id,
				recipes_ids: ctx.params.recipes_ids,
			});
			res[0].recipes = await ctx.call("alimentation.dishs-recipes.find", {
				query: {
					dish_id: res[0].id,
				},
			});
			return res;
		},

		/**
		 * Method for save Dishs Dishs Types
		 */
		async saveDishTypes(ctx, res) {
			await ctx.call("alimentation.dishs-dishs-types.save_dishTypes", {
				dish_id: res[0].id,
				types_ids: ctx.params.types_ids,
			});
			await ctx.call("alimentation.dishs-dishs-types.find", {
				query: {
					dish_id: res[0].id,
				},
			});
			res[0].dishTypes = await ctx.call("alimentation.dishs-dishs-types.getTypes", {
				dish_id: res[0].id,
			});
			return res;
		},

		/**
		 * Method Remove Dishs Types
		 */
		async removeDishsType(ctx) {
			await ctx.call("alimentation.dishs-dishs-types.removeDishsTypes", {
				dish_id: parseInt(ctx.params.id),
			});
			return true;
		},

		/**
		 * Method Remove Dishs Recipes
		 */
		async removeDishsRecipes(ctx) {
			await ctx.call("alimentation.dishs-recipes.removeDishsRecipes", {
				dish_id: parseInt(ctx.params.id),
			});
			return true;
		},

		/**
		 * Method Remove Dishs Translations
		 */
		async removeDishsTrasnlations(ctx) {
			await ctx.call("alimentation.dishs-translations.removeDishsTrasnlations", {
				dish_id: parseInt(ctx.params.id),
			});
			return true;
		},
		/**
		 * Method for validate Dish Entity
		 */
		async validateDishsEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The Family not correspond the entity",
						"FAMILY_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError(
					"You need send fiscal entity Id",
					"NO_FISCAL_ENTITY_ID",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
