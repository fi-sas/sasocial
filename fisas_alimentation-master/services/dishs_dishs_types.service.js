"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.dishs-dishs-types",
	table: "dish_dish_type",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "dishs-dishs-types")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "dish_id", "dish_type_id"],
		defaultWithRelateds: [],
		withRelateds: {
			type(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.dishs-types", "type", "dish_type_id");
			},
		},
		entityValidator: {
			dish_id: { type: "number", positive: true, integer: true },
			dish_type_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Save Dish types
		 */
		save_dishTypes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
				types_ids: {
					type: "array",
					item: {
						type: "number",
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					dish_id: ctx.params.dish_id,
				});
				for (const types of ctx.params.types_ids) {
					const entities = {
						dish_id: ctx.params.dish_id,
						dish_type_id: types,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Get Dishs types
		 */
		getTypes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				let rest = [];
				const listAssociations = await this._find(ctx, {
					query: {
						dish_id: ctx.params.dish_id,
					},
					withRelated: ["type"],
				});
				for (const association of listAssociations) {
					rest.push(association.type);
				}
				return rest;
			},
		},

		/**
		 * Remove Dishs Types
		 */
		removeDishsTypes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					dish_id: ctx.params.dish_id,
				});
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
