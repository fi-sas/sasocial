"use strict";


const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.dishs-recipes",
	table: "dish_recipe",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "dishs-recipes")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"dish_id",
			"recipe_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			dish_id: { type: "number", positive: true, integer: true },
			recipe_id: { type: "number", positive: true, integer: true },
		}
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Actions for save Recipes
		 */
		save_recipes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
				recipes_ids: {
					type: "array",
					item: {
						type: "number"
					}
				}
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					dish_id: ctx.params.dish_id
				});
				for (const recipe of ctx.params.recipes_ids) {
					const entities = {
						dish_id: ctx.params.dish_id,
						recipe_id: recipe
					};
					const insert =  await this._insert(ctx, { entity: entities } );
					resp.push(insert[0]);
				}
				return resp;
			}
		},

		/**
		 * Get Recipes by dishId
		 */
		getRecipes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx){
				let rest = [];
				const listAssociations = await this._find(ctx,  {
					query: {
						dish_id: ctx.params.dish_id
					}
				});
				for (const association of listAssociations) {
					const recipe = await ctx.call("alimentation.recipes.get", { id: association.recipe_id });
					rest.push(recipe[0]);
				}
				return rest;
			}
		},

		/**
		 * Remove Dishs Recipes
		 */
		removeDishsRecipes: {
			params: {
				dish_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx){
				await this.adapter.removeMany({
					dish_id: ctx.params.dish_id
				});
				return true;
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
