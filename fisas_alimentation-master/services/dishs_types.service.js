"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.dishs-types",
	table: "dish_type",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "dishs-types")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"active",
			"tax_id",
			"price",
			"code",
			"pos_operations",
			"pack_available",
			"excluded_user_profile_ids",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"translations",
			"disponibility_lunch",
			"disponibility_dinner",
			"prices",
			"tax",
		],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.dishs-types-translations",
					"translations",
					"id",
					"dish_type_id",
				);
			},
			disponibility_lunch(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.disponibilities.find", {
								query: {
									dish_type_lunch_id: doc.id,
								},
							})
							.then((res) => (doc.disponibility_lunch = res[0]));
					}),
				);
			},
			disponibility_dinner(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.disponibilities.find", {
								query: {
									dish_type_dinner_id: doc.id,
								},
							})
							.then((res) => (doc.disponibility_dinner = res[0]));
					}),
				);
			},
			prices(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.prices-variations", "prices", "id", "dish_type_id");
			},
			tax(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "tax", "tax_id");
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						name: { type: "string" },
					},
				},
			},
			prices: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						description: { type: "string", optional: true, nullable: true },
						tax_id: { type: "number", positive: true, integer: false },
						profile_id: { type: "number", positive: true, integer: false },
						price: { type: "number", positive: true, integer: false },
						meal: { type: "string", optional: true, nullable: true },
						time: { type: "string", default: "00:00:00" },
					},
				},
			},
			disponibility_lunch: {
				type: "object",
				props: {
					begin_date: { type: "date", optional: true, nullable: true, convert: true },
					end_date: { type: "date", optional: true, nullable: true, convert: true },
					monday: { type: "boolean", default: true },
					tuesday: { type: "boolean", default: true },
					wednesday: { type: "boolean", default: true },
					thursday: { type: "boolean", default: true },
					friday: { type: "boolean", default: true },
					saturday: { type: "boolean", default: true },
					sunday: { type: "boolean", default: true },
					annulment_maximum_hour: { type: "string", optional: true, nullable: true, convert: true },
					minimum_hour: { type: "string", optional: true, nullable: true, convert: true },
					maximum_hour: { type: "string", optional: true, nullable: true },
				},
			},
			disponibility_dinner: {
				type: "object",
				props: {
					begin_date: { type: "date", optional: true, nullable: true, convert: true },
					end_date: { type: "date", optional: true, nullable: true, convert: true },
					monday: { type: "boolean", default: true },
					tuesday: { type: "boolean", default: true },
					wednesday: { type: "boolean", default: true },
					thursday: { type: "boolean", default: true },
					friday: { type: "boolean", default: true },
					saturday: { type: "boolean", default: true },
					sunday: { type: "boolean", default: true },
					annulment_maximum_hour: { type: "string", optional: true, nullable: true, convert: true },
					minimum_hour: { type: "string", optional: true, nullable: true, convert: true },
					maximum_hour: { type: "string", optional: true, nullable: true, convert: true },
				},
			},
			excluded_user_profile_ids: {
				type: "array",
				items: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
			},
			code: { type: "string", optional: true },
			pos_operations: { type: "boolean", default: false },
			pack_available: { type: "boolean", default: false },
			active: { type: "boolean", default: true },
			tax_id: { type: "number", positive: true, integer: true },
			price: { type: "number", positive: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"verifyCodeDishType",
				"validateLanguageIds",
				"validateTaxId",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateLanguageIds",
				"validateTaxId",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			// TODO: Falta saber o que fazer no delete
			// remove : [
			// 	Getfentety_id,
			// 	"verifyEntityDish",
			// 	"removeTranslations",
			// 	"removeDisponibilityDinner",
			// 	"removeDisponibilityLunch",
			// 	"removePriceVariation"
			// ],
			updatePrices: ["verifyEntityDish"],
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name"],
						"alimentation.dishs-types-translations",
						"dish_type_id",
					);
				},
				addQueryFentity,
			],
			get: ["verifyEntityDish"],
			getPrices: ["verifyEntityDish"],
		},
		after: {
			create: ["saveTranslations", "saveDisponibilityDinner", "saveDisponibilityLunch"],
			update: ["saveTranslations", "saveDisponibilityDinner", "saveDisponibilityLunch"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Update prices of Dishs
		 */
		updatePrices: {
			rest: "PATCH /:id/prices",
			params: {
				id: { type: "number", convert: true },
				prices: {
					type: "array",
					items: {
						type: "object",
						props: {
							description: { type: "string", optional: true, nullable: true },
							tax_id: { type: "number", positive: true, integer: false },
							profile_id: { type: "number", positive: true, integer: false },
							price: { type: "number", positive: true, integer: false },
							meal: { type: "string", optional: true, nullable: true },
							time: { type: "string", default: "00:00:00" },
						},
					},
				},
			},
			scope: "alimentation:dishs-types:update",
			visibility: "published",
			async handler(ctx) {
				await ctx.call("alimentation.prices-variations.savePriceVariation", {
					dish_type_id: parseInt(ctx.params.id),
					prices: ctx.params.prices,
				});
				//falta o clear da cache
				return this._get(ctx, { id: ctx.params.id });
			},
		},

		/**
		 * Get prices of Dishs
		 */
		getPrices: {
			rest: "GET /:id/prices",
			params: {
				id: { type: "number", convert: true },
			},
			visibility: "published",
			scope: "alimentation:dishs-types:read",
			async handler(ctx) {
				return await ctx.call("alimentation.prices-variations.find", {
					query: {
						dish_type_id: ctx.params.id,
					},
					withRelated: "tax,profile",
				});
			},
		},

		/**
		 * Get avaliable and nullableUntil
		 */
		availableNullableUntil: {
			params: {
				dish_type_id: { type: "number" },
				meal: { type: "string" },
				date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const dish_type_info = await this._get(ctx, {
					id: ctx.params.dish_type_id,
					withRelated: "disponibility_lunch,disponibility_dinner",
				});
				let disponibility;
				if (ctx.params.meal === "lunch") {
					disponibility = dish_type_info[0].disponibility_lunch;
				} else {
					disponibility = dish_type_info[0].disponibility_dinner;
				}
				if (disponibility) {
					let available_until = null;
					if (disponibility.maximum_hour) {
						const maximum_hour = moment(disponibility.maximum_hour, "HH:mm:ssZ");
						available_until = moment(ctx.params.date);
						available_until.hours(maximum_hour.hours());
						available_until.minutes(maximum_hour.minutes());
						available_until.seconds(maximum_hour.seconds());
					}

					let nullable_until = null;
					if (disponibility.annulment_maximum_hour) {
						const disponibility_anula = moment(disponibility.annulment_maximum_hour, "HH:mm:ssZ");
						nullable_until = moment(ctx.params.date);
						nullable_until.hours(disponibility_anula.hours());
						nullable_until.minutes(disponibility_anula.minutes());
						nullable_until.seconds(disponibility_anula.seconds());
					}

					return {
						available_until: available_until ? available_until.toDate() : null,
						nullable_until: nullable_until ? nullable_until.toDate() : null,
					};
				} else {
					throw new Errors.ValidationError(
						"The disponibility not exist for dish_type_id: " + ctx.params.dish_type_id,
						"THE_DISPONIBILITY_NOT_EXIST",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.prices-variations.deleted"(ctx) {
			this.logger.info("CAPTURED EVENT => alimentation.prices-variations.deleted");
			this.clearCache();
		},
		"alimentation.prices-variations.*"(ctx) {
			this.logger.info("CAPTURED EVENT => alimentation.prices-variations");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Dish Type code
		 */
		async verifyCodeDishType(ctx) {
			const dish_type = await this._find(ctx, {
				query: {
					code: ctx.params.code,
					fentity_id: ctx.meta.alimentation_entity_id,
				},
			});
			if (dish_type.length !== 0) {
				throw new Errors.ValidationError(
					"The dish type code already exists'",
					"DISH_TYPE_CODE_ALREADY_EXIST",
				);
			}
		},

		/**
		 * Method for validate languages id
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		/**
		 * Method for validate TaxId
		 */
		async validateTaxId(ctx) {
			await ctx.call("configuration.taxes.get", { id: ctx.params.tax_id });
		},

		/**
		 * Method for save translations of Dishs Types
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.dishs-types-translations.save_translations", {
				dish_type_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.dishs-types-translations.find", {
				query: {
					dish_type_id: res[0].id,
				},
			});

			return res;
		},

		/**
		 * Method for save DisponibilityDinner
		 */
		async saveDisponibilityDinner(ctx, res) {
			ctx.params.disponibility_dinner.dish_type_dinner_id = res[0].id;
			res[0].disponibility_dinner = await ctx.call(
				"alimentation.disponibilities.saveDisponibilityDinner",
				{ disponibility_dinner: ctx.params.disponibility_dinner },
			);
			return res;
		},

		/**
		 * Method for save DisponibilityLunch
		 */
		async saveDisponibilityLunch(ctx, res) {
			ctx.params.disponibility_lunch.dish_type_lunch_id = res[0].id;
			res[0].disponibility_lunch = await ctx.call(
				"alimentation.disponibilities.saveDisponibilityLunch",
				{ disponibility_lunch: ctx.params.disponibility_lunch },
			);
			return res;
		},

		/**
		 * Method for save Prices Variations
		 */
		async savePriceVariation(ctx, res) {
			await ctx.call("alimentation.prices-variations.savePriceVariation", {
				dish_type_id: res[0].id,
				prices: ctx.params.prices,
			});

			res[0].prices = await ctx.call("alimentation.prices-variations.find", {
				query: {
					dish_type_id: res[0].id,
				},
			});
			return res;
		},

		/**
		 * Method for verify entity DishType
		 */
		async verifyEntityDish(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The dish type not correspond the entity",
						"DISH_TYPE_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError(
					"You need send fiscal entity Id",
					"NO_FISCAL_ENTITY_ID",
					{},
				);
			}
		},

		/**
		 * Method for remove Translations of DishsType
		 */
		async removeTranslations(ctx) {
			return await ctx.call("alimentation.dishs-types-translations.removeTranslations", {
				dish_type_id: parseInt(ctx.params.id),
			});
		},

		/**
		 * Method for remove disponibility Dinner
		 */
		async removeDisponibilityDinner(ctx) {
			return await ctx.call("alimentation.disponibilities.removeDisponibilityDinner", {
				dish_type_id: parseInt(ctx.params.id),
			});
		},

		/**
		 * Method for remove Disponibility Lunch
		 */
		async removeDisponibilityLunch(ctx) {
			return await ctx.call("alimentation.disponibilities.removeDisponibilityLunch", {
				dish_type_id: parseInt(ctx.params.id),
			});
		},

		/**
		 * Method for remove Price Variations
		 */
		async removePriceVariation(ctx) {
			return await ctx.call("alimentation.prices-variations.removePriceVariation", {
				dish_type_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
