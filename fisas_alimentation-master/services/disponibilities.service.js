"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.disponibilities",
	table: "disponibility",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "disponibilities")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"begin_date",
			"end_date",
			"monday",
			"tuesday",
			"wednesday",
			"thursday",
			"friday",
			"saturday",
			"sunday",
			"annulment_maximum_hour",
			"minimum_hour",
			"maximum_hour",
			"product_id",
			"dish_type_lunch_id",
			"dish_type_dinner_id",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			begin_date: { type: "date", optional: true, nullable: true },
			end_date: { type: "date", optional: true, nullable: true },
			monday: { type: "boolean", default: true },
			tuesday: { type: "boolean", default: true },
			wednesday: { type: "boolean", default: true },
			thursday: { type: "boolean", default: true },
			friday: { type: "boolean", default: true },
			saturday: { type: "boolean", default: true },
			sunday: { type: "boolean", default: true },
			annulment_maximum_hour: { type: "string", optional: true, nullable: true },
			minimum_hour: { type: "string", optional: true, nullable: true },
			maximum_hour: { type: "string", optional: true, nullable: true },
			product_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			dish_type_lunch_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
			dish_type_dinner_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		saveDisponibilityDinner: {
			params: {
				disponibility_dinner: {
					type: "object",
					props: {
						dish_type_dinner_id: { type: "number" },
						begin_date: { type: "date", optional: true, nullable: true, convert: true },
						end_date: { type: "date", optional: true, nullable: true, convert: true },
						monday: { type: "boolean", default: true },
						tuesday: { type: "boolean", default: true },
						wednesday: { type: "boolean", default: true },
						thursday: { type: "boolean", default: true },
						friday: { type: "boolean", default: true },
						saturday: { type: "boolean", default: true },
						sunday: { type: "boolean", default: true },
						annulment_maximum_hour: { type: "string", optional: true, nullable: true },
						minimum_hour: { type: "string", optional: true, nullable: true },
						maximum_hour: { type: "string", optional: true, nullable: true },
					},
				},
			},
			async handler(ctx) {
				ctx.params.disponibility_dinner.annulment_maximum_hour = ctx.params.disponibility_dinner
					.annulment_maximum_hour
					? moment(ctx.params.disponibility_dinner.annulment_maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_dinner.annulment_maximum_hour;
				ctx.params.disponibility_dinner.minimum_hour = ctx.params.disponibility_dinner.minimum_hour
					? moment(ctx.params.disponibility_dinner.minimum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_dinner.minimum_hour;
				ctx.params.disponibility_dinner.maximum_hour = ctx.params.disponibility_dinner.maximum_hour
					? moment(ctx.params.disponibility_dinner.maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_dinner.maximum_hour;

				await this.adapter.removeMany({
					dish_type_dinner_id: ctx.params.disponibility_dinner.dish_type_dinner_id,
				});
				const insert = await this._insert(ctx, { entity: ctx.params.disponibility_dinner });
				return insert;
			},
		},
		saveDisponibilityLunch: {
			params: {
				disponibility_lunch: {
					type: "object",
					props: {
						dish_type_lunch_id: { type: "number" },
						begin_date: { type: "date", optional: true, nullable: true, convert: true },
						end_date: { type: "date", optional: true, nullable: true, convert: true },
						monday: { type: "boolean", default: true },
						tuesday: { type: "boolean", default: true },
						wednesday: { type: "boolean", default: true },
						thursday: { type: "boolean", default: true },
						friday: { type: "boolean", default: true },
						saturday: { type: "boolean", default: true },
						sunday: { type: "boolean", default: true },
						annulment_maximum_hour: { type: "date", optional: true, nullable: true, convert: true },
						minimum_hour: { type: "date", optional: true, nullable: true, convert: true },
						maximum_hour: { type: "date", optional: true, nullable: true, convert: true },
					},
				},
			},
			async handler(ctx) {
				ctx.params.disponibility_lunch.annulment_maximum_hour = ctx.params.disponibility_lunch
					.annulment_maximum_hour
					? moment(ctx.params.disponibility_lunch.annulment_maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_lunch.annulment_maximum_hour;
				ctx.params.disponibility_lunch.minimum_hour = ctx.params.disponibility_lunch.minimum_hour
					? moment(ctx.params.disponibility_lunch.minimum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_lunch.minimum_hour;
				ctx.params.disponibility_lunch.maximum_hour = ctx.params.disponibility_lunch.maximum_hour
					? moment(ctx.params.disponibility_lunch.maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_lunch.maximum_hour;

				await this.adapter.removeMany({
					dish_type_lunch_id: ctx.params.disponibility_lunch.dish_type_lunch_id,
				});
				const insert = await this._insert(ctx, { entity: ctx.params.disponibility_lunch });
				return insert;
			},
		},

		removeDisponibilityDinner: {
			params: {
				dish_type_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					dish_type_dinner_id: ctx.params.dish_type_id,
				});
				return true;
			},
		},
		removeDisponibilityLunch: {
			params: {
				dish_type_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					dish_type_lunch_id: ctx.params.dish_type_id,
				});
				return true;
			},
		},
		saveDisponibilityProduct: {
			params: {
				disponibility_product: {
					type: "object",
					props: {
						product_id: { type: "number" },
						begin_date: { type: "date", optional: true, nullable: true, convert: true },
						end_date: { type: "date", optional: true, nullable: true, convert: true },
						monday: { type: "boolean", default: true },
						tuesday: { type: "boolean", default: true },
						wednesday: { type: "boolean", default: true },
						thursday: { type: "boolean", default: true },
						friday: { type: "boolean", default: true },
						saturday: { type: "boolean", default: true },
						sunday: { type: "boolean", default: true },
						annulment_maximum_hour: { type: "string", optional: true, nullable: true },
						minimum_hour: { type: "string", optional: true, nullable: true },
						maximum_hour: { type: "string", optional: true, nullable: true },
					},
				},
			},
			async handler(ctx) {
				ctx.params.disponibility_product.annulment_maximum_hour = ctx.params.disponibility_product
					.annulment_maximum_hour
					? moment(ctx.params.disponibility_product.annulment_maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_product.annulment_maximum_hour;
				ctx.params.disponibility_product.minimum_hour = ctx.params.disponibility_product
					.minimum_hour
					? moment(ctx.params.disponibility_product.minimum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_product.minimum_hour;
				ctx.params.disponibility_product.maximum_hour = ctx.params.disponibility_product
					.maximum_hour
					? moment(ctx.params.disponibility_product.maximum_hour).format("HH:mm:ssZ")
					: ctx.params.disponibility_product.maximum_hour;

				await this.adapter.removeMany({
					product_id: ctx.params.disponibility_product.product_id,
				});
				const insert = await this._insert(ctx, { entity: ctx.params.disponibility_product });
				return insert;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
