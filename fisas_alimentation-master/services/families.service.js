"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.families",
	table: "family",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "families")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "file_id", "fentity_id", "created_at", "updated_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.families-translations",
					"translations",
					"id",
					"family_id",
				);
			},
			services(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.services-families.getInfoService", { family_id: doc.id })
							.then((listservices) => {
								doc.services = listservices;
							});
					}),
				);
			},
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
			products(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.families-products.getProductsInfo", { family_id: doc.id })
							.then((listproducts) => {
								doc.products = listproducts;
							});
					}),
				);
			},
		},
		entityValidator: {
			active: { type: "boolean", default: true },
			file_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						name: { type: "string" },
					},
				},
			},
			products: {
				type: "array",
				optional: true,
				items: { type: "number" },
			},
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"validateLanguageIds",
				"validateProducts",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateLanguageIds",
				"validateProducts",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			get: ["validateFamiliesEntity"],
			list: [
				addQueryFentity,
				function updateSearch(ctx) {
					delete ctx.params.query.name;
					return addSearchRelation(
						ctx,
						["name"],
						"alimentation.families-translations",
						"family_id",
					);
				},
			],
			remove: ["validateFamiliesEntity"],
		},
		after: {
			create: ["saveTranslations", "saveProducts"],
			update: ["saveTranslations", "saveProducts"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		list_products: {
			// REST: GET /
			visibility: "published",
			rest: "GET /:family_id/products/:service_id",
			scope: "alimentation:families:read",
			params: {
				family_id: { type: "number", positive: true, convert: true },
				service_id: { type: "number", positive: true, convert: true },
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"family_id",
					"service_id",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
			handler(ctx) {
				return ctx
					.call("alimentation.families-products.find", {
						query: { family_id: ctx.params.family_id },
					})
					.then((rd) => {
						return rd.map((fp) => fp.product_id);
					})
					.then((product_ids) => {
						delete ctx.params.family_id;
						ctx.params.query = ctx.params.query || {};
						ctx.params.query.id = product_ids;
						return ctx.call("alimentation.products.list", ctx.params);
					})
					.then(async (products) => {
						const stocks = await this.get_stock_by_service(
							ctx,
							products.rows.map((p) => p.id),
							ctx.params.service_id,
						);
						products.rows.forEach((product) => {
							product.stocks = stocks.filter((s) => s.product_id === product.id);
						});
						return products;
					});
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.stocks.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for save Families Translations
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.families-translations.save_translations", {
				family_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.families-translations.find", {
				query: {
					family_id: res[0].id,
				},
			});

			return res;
		},

		/**
		 * Method for save families Products
		 */
		async saveProducts(ctx, res) {
			if (ctx.params.products) {
				res[0].products = await ctx.call("alimentation.families-products.save_products", {
					family_id: res[0].id,
					products: ctx.params.products,
				});
			}
			return res;
		},

		/**
		 * Method for validate Languages
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		/**
		 * Method for validate Products
		 */
		async validateProducts(ctx) {
			if (ctx.params.products) {
				for (const product of ctx.params.products) {
					await ctx.call("alimentation.products.get", { id: product, withRelated: false });
				}
			}
		},

		/**
		 * Method for validate Families of Entity
		 */
		async validateFamiliesEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The Family not correspond the entity",
						"FAMILY_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
			}
		},

		get_stock_by_service(ctx, product_ids, service_id) {
			return ctx
				.call("alimentation.services.get", { id: service_id, withRelated: false })
				.then((services) => {
					return ctx.call("alimentation.stocks.find", {
						query: { product_id: product_ids, wharehouse_id: services[0].wharehouse_id },
						withRelated: false,
					});
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
