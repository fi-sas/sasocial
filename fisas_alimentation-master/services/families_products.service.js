"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.families-products",
	table: "family_product",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "families-products")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "product_id", "family_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			product_id: { type: "number", positive: true, integer: true },
			family_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Families Products
		 */
		save_products: {
			params: {
				family_id: { type: "number", positive: true, integer: true },
				products: {
					type: "array",
					item: { type: "number" },
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					family_id: ctx.params.family_id,
				});
				for (const id of ctx.params.products) {
					const entities = {
						family_id: ctx.params.family_id,
						product_id: id,
					};
					await this._insert(ctx, { entity: entities });
				}
				this.clearCache();
				return ctx.call("alimentation.products.find", {
					query: {
						id: ctx.params.products,
					},
					withRelated: "translations,unit",
				});
			},
		},

		save_families: {
			params: {
				product_id: { type: "number", positive: true, integer: true },
				families: {
					type: "array",
					item: { type: "number" },
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				for (const id of ctx.params.families) {
					const entities = {
						product_id: ctx.params.product_id,
						family_id: id,
					};
					await this._insert(ctx, { entity: entities });
				}
				this.clearCache();
				return ctx.call("alimentation.families.find", {
					query: {
						id: ctx.params.families,
					},
					withRelated: "translations",
				});
			},
		},
		getFamiliesByProduct: {
			params: {
				id: { type: "number" },
			},
			async handler(ctx) {
				const productFamilies = await this._find(ctx, {
					query: {
						product_id: ctx.params.id,
					},
				});
				const families_ids = productFamilies.map((pf) => pf.family_id);
				return ctx.call("alimentation.families.find", {
					query: {
						id: families_ids,
					},
					withRelated: "translations",
				});
			},
		},
		getProductsInfo: {
			params: {
				family_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				let resp = [];
				const families_products = await this._find(ctx, {
					query: {
						family_id: ctx.params.family_id,
					},
				});
				return ctx.call("alimentation.products.find", {
					query: {
						id: families_products.map((fp) => fp.product_id),
					},
					withRelated: "translations,unit",
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
