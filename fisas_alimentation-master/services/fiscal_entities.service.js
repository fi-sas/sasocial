"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.entities",
	table: "fiscal_entity",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "entities")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"tin",
			"street",
			"door",
			"city",
			"cep",
			"phone",
			"email",
			"website",
			"account_id",
			"logo_file_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "logo_file_id");
			},
			current_account(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "current_account.accounts", "current_account", "account_id");
			},
			users(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.entities.getUsers", { id: doc.id })
							.then((res) => (doc.users = res));
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean", default: true },
			tin: { type: "string", max: 20 },
			street: { type: "string", optional: true, nullable: true },
			door: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			city: { type: "string", optional: true, nullable: true },
			cep: { type: "string", max: 10, optional: true, nullable: true },
			phone: { type: "string", max: 20, optional: true, nullable: true },
			email: { type: "string", optional: true, nullable: true },
			website: { type: "string", optional: true, nullable: true },
			account_id: { type: "number", positive: true, integer: true, convert: true },
			logo_file_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
		},
	},
	hooks: {
		before: {
			create: [
				"verifyFile",
				"verifyCurrentAccount",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"verifyFile",
				"verifyCurrentAccount",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			// getUsers: [checkIsManager],
			addUser: ["check_is_manager"],
			removeUser: ["check_is_manager"],
		},
		after: {
			create: ["addUserManager"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Actions for get user by entity Id
		 */
		getUsers: {
			rest: "GET /:id/users",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:entities:read",
			visibility: "published",
			async handler(ctx) {
				let resp = [];
				const listUserEntity = await ctx.call("alimentation.fiscal-entities-users.find", {
					query: {
						fentity_id: parseInt(ctx.params.id),
					},
				});
				for (const association of listUserEntity) {
					const user_info = await ctx.call("authorization.users.get", { id: association.user_id });
					resp.push(user_info[0]);
				}
				return resp;
			},
		},

		/**
		 * Actions for associate users to entity
		 */
		addUser: {
			rest: "POST /:id/users",
			params: {
				user_id: { type: "number", positive: true, integer: true },
				manager: { type: "boolean" },
			},
			scope: "alimentation:entities:create",
			visibility: "published",
			async handler(ctx) {
				let user = await ctx.call("authorization.users.get", {
					id: ctx.params.user_id,
				});
				if (user.length !== 0) {
					if (user[0].active === false) {
						throw new Errors.ValidationError(
							"User with id " + ctx.params.user_id + " isn't active",
							"USER_NOT_ACTIVE",
						);
					}
				}
				await this._get(ctx, { id: ctx.params.id });
				const association = await ctx.call("alimentation.fiscal-entities-users.find", {
					query: {
						user_id: ctx.params.user_id,
						fentity_id: ctx.params.id,
					},
				});
				if (association.length === 0) {
					const entity = {
						user_id: ctx.params.user_id,
						manager: ctx.params.manager,
						fentity_id: parseInt(ctx.params.id),
						created_by_user_id: ctx.meta.user.id,
					};
					await ctx.call("alimentation.fiscal-entities-users.create", entity);
				}
				user[0].manager = ctx.params.manager;
				return user[0];
			},
		},

		/**
		 * Actions for remove user for entity
		 */
		removeUser: {
			rest: "DELETE /:id/users/:id_user",
			params: {
				id: { type: "number", convert: true },
				id_user: { type: "number", convert: true },
			},
			scope: "alimentation:entities:delete",
			visibility: "published",
			async handler(ctx) {
				await ctx.call("authorization.users.get", {
					id: ctx.params.id_user,
				});
				await this._get(ctx, { id: ctx.params.id });
				const association = await ctx.call("alimentation.fiscal-entities-users.find", {
					query: {
						user_id: ctx.params.id_user,
						fentity_id: ctx.params.id,
					},
				});
				if (association.length !== 0) {
					await ctx.call("alimentation.fiscal-entities-users.remove", { id: association[0].id });
					return true;
				}
			},
		},

		getEntityByUser: {
			rest: "GET /user",
			scope: "alimentation:entities",
			visibility: "published",
			async handler(ctx) {
				const hasPermission = await ctx.call("authorization.scopes.userHasScope", {
					scope: "alimentation:entities:create",
				});

				if (hasPermission) {
					return ctx.call("alimentation.entities.find", {
						query: {
							active: true,
						}
					});
				} else {
					const associations_list = await ctx.call("alimentation.fiscal-entities-users.find", {
						query: {
							user_id: ctx.meta.user.id,
						},
					});

					return this._find(ctx, {
						query: {
							id: associations_list.map((al) => al.fentity_id),
							active: true,
						},
					});
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async verifyFile(ctx) {
			if (ctx.params.logo_file_id) {
				await ctx.call("media.files.get", { id: ctx.params.logo_file_id });
			}
		},
		async verifyCurrentAccount(ctx) {
			await ctx.call("current_account.accounts.get", { id: ctx.params.account_id });
		},

		async addUserManager(ctx, res) {
			const user_entitie = {
				user_id: ctx.meta.user.id,
				created_by_user_id: ctx.meta.user.id,
				fentity_id: res[0].id,
				manager: true,
			};
			await ctx.call("alimentation.fiscal-entities-users.create", user_entitie);
			return res;
		},
		async check_is_manager(ctx) {
			await ctx.call("alimentation.fiscal-entities-users.isManager", {
				user_id: ctx.meta.user.id,
				fentity_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
