"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.fiscal-entities-users",
	table: "fiscal_entity_user",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "fiscal-entities-users")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "created_by_user_id", "manager", "fentity_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			user_id: { type: "number", integer: true, positive: true },
			created_by_user_id: { type: "number", integer: true, positive: true },
			manager: { type: "boolean", default: false },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		checkUserPermission: {
			params: {
				user_id: { type: "number" },
				fentity_id: { type: "number" },
			},
			async handler(ctx) {
				await ctx.call("alimentation.entities.get", { id: ctx.params.fentity_id });
				const userEntity = await this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
						fentity_id: ctx.params.fentity_id,
					},
				});
				if (userEntity.length === 0) {
					throw new Errors.ValidationError(
						"No permission to access this fiscal entity",
						"NO_PERMISSION_TO_ACCESS_FISCAL_ENTITY",
						{},
					);
				}
			},
		},

		isManager: {
			params: {
				user_id: { type: "number" },
				fentity_id: { type: "number" },
			},
			async handler(ctx) {
				await ctx.call("alimentation.entities.get", { id: ctx.params.fentity_id });
				const userManagerEntity = await this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
						fentity_id: ctx.params.fentity_id,
						manager: true,
					},
				});
				if (userManagerEntity.length === 0) {
					throw new Errors.ValidationError(
						"You not a manager this fiscal entity",
						"YOU_NOT_A_MANAGER_THIS_FISCAL_ENTITY",
						{},
					);
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
