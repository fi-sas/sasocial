"use strict";

const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.menu",

	mixins: [],

	/**
	 * Settings
	 */
	settings: {},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard_public: {
			cache: {
				enabled: true,
				ttl: 600, // 10 Minutes
			},
			visibility: "public",
			async handler(ctx) {
				const services = await ctx.call("alimentation.services.find", {
					query: { active: true },
					withRelated: false,
				});

				const servicesWithMenus = [];
				for (const service of services) {
					let today = new Date();
					let menus = [];
					// If after 22h, only appears next day menus
					if (today.getHours() > 22) {
						today.setDate(today.getDate() + 1);
						menus = await ctx.call("alimentation.menus.find", {
							query: {
								date: { gte: this.formatDate(today) },
								service_id: service.id,
								validated: true,
							},
							sort: "date",
							limit: 1,
							withRelated: false,
						});
					}
					// If after 14h and before 22h, appears todays dinner and  next day menus
					else if (today.getHours() >= 14 && today.getHours() < 22) {
						// get todays dinner
						menus = await ctx.call("alimentation.menus.find", {
							query: {
								meal: "dinner",
								date: { gte: this.formatDate(today) },
								service_id: service.id,
								validated: true,
							},
							sort: "date",
							limit: 1,
							withRelated: false,
						});

						if (menus.length === 0) {
							menus = await ctx.call("alimentation.menus.find", {
								query: {
									meal: "lunch",
									date: { gte: this.formatDate(today) },
									service_id: service.id,
									validated: true,
								},
								sort: "date",
								limit: 1,
								withRelated: false,
							});
						}
					} else {
						menus = await ctx.call("alimentation.menus.find", {
							query: {
								meal: "lunch",
								date: { gte: this.formatDate(today) },
								service_id: service.id,
								validated: true,
							},
							sort: "date",
							limit: 1,
							withRelated: false,
						});
					}

					if (menus.length > 0) {
						for (let menu of menus) {
							menu.dishs = [];

							menu.dishs = await ctx.call("alimentation.menus-dishs.find", {
								query: {
									menu_id: menu.id,
								},
								withRelated: "dish,type",
								limit: 3,
							});
						}

						service.menus = menus;
						servicesWithMenus.push(service);
					}
				}

				if (servicesWithMenus.length > 0) {
					return servicesWithMenus;
				} else {
					throw new Errors.ValidationError(
						"Services doesnt have any menu",
						"ALIMENTATION_SERVICES_DOESNT_HAVE_ANY_MENU",
						{},
					);
				}
			},
		},
		wp_dashboard: {
			visibility: "public",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				if (ctx.meta.isGuest) {
					return ctx.call("alimentation.menu.wp_dashboard_public");
				} else {
					const reservations = await ctx.call("alimentation.reservations.getUserReservations", {
						onlyAvailables: true,
						limit: 3,
						sort: "-id",
					});
					if (reservations.length > 0) {
						return reservations;
					}
					throw new Errors.ValidationError(
						"User don't have any reservation",
						"ALIMENTATION_USER_DONT_HAVE_RESERVATIONS",
						{},
					);
				}
			},
		},
		userAllergens: {
			rest: "PUT /user-config/allergens",
			params: {
				allergens: {
					type: "array",
					items: { type: "number" },
				},
			},
			visibility: "published",
			async handler(ctx) {
				await ctx.call("alimentation.users-allergens.removeByUser", { user_id: ctx.meta.user.id });

				for (const allergen of ctx.params.allergens) {
					const entities = {
						user_id: ctx.meta.user.id,
						allergen_id: allergen,
					};
					await ctx.call("alimentation.users-allergens.create", entities);
				}
				return await ctx.call("alimentation.users-allergens.allergensInfo", {
					user_id: ctx.meta.user.id,
				});
			},
		},

		getUserAllergens: {
			rest: "GET /user-config/allergens",
			visibility: "published",
			async handler(ctx) {
				return ctx
					.call("alimentation.allergens.find", { query: { active: true } })
					.then((allergens) => {
						return ctx
							.call("alimentation.users-allergens.find", {
								query: {
									user_id: ctx.meta.user.id,
								},
							})
							.then((user_allergens) => {
								return allergens.map((allergen) => {
									return {
										id: allergen.id,
										translations: allergen.translations,
										allergic: !!user_allergens.find((a) => a.allergen_id === allergen.id),
									};
								});
							});
					});
			},
		},

		postPreferencialCanteen: {
			rest: "POST /user-config/preferencial-canteen",
			params: {
				canteen_lunch_id: { type: "number", integer: true, convert: true, nullable: true },
				canteen_dinner_id: { type: "number", integer: true, convert: true, nullable: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				ctx.params.user_id = ctx.meta.user.id;
				return await ctx.call("alimentation.preferencial_canteens.save_cantine_user", ctx.params);
			},
		},

		getPreferencialCanteen: {
			rest: "GET /user-config/preferencial-canteen",
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				return await ctx.call("alimentation.preferencial_canteens.find", {
					query: {
						user_id: ctx.meta.user.id,
					},
				});
			},
		},

		countAvailableReservations: {
			rest: "GET /user/reservations/count",
			visibility: "published",
			async handler(ctx) {
				const reservations = await ctx.call("alimentation.reservations.find", {
					query: {
						user_id: ctx.meta.user.id,
						date: { gte: moment().format("YYYY-MM-DD") },
						is_available: true,
					},
				});
				return { total: reservations.length };
			},
		},

		getUserReservations: {
			rest: "GET /user/reservations",
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				return await ctx.call("alimentation.reservations.getUserReservations", ctx.params);
			},
		},

		cancelReservation: {
			rest: "POST /user/reservations/:id/cancel",
			visibility: "published",
			params: {
				id: { type: "number", convert: true },
			},
			async handler(ctx) {
				return await ctx.call("alimentation.reservations.cancelReservation", ctx.params);
			},
		},

		getSchools: {
			rest: "GET /schools",
			visibility: "published",
			async handler(ctx) {
				const schools = await ctx.call("infrastructure.organic-units.find");
				if (ctx.params.withRelated === "services") {
					for (const school of schools) {
						const services_list = await ctx.call("alimentation.menu.getServices", {
							organic_unit_id: school.id,
						});
						school.services = services_list;
					}
					return schools.filter((s) => s.services.length > 0);
				} else {
					const filteredSchools = [];
					for (const school of schools) {
						const services_count = await ctx.call("alimentation.services.count", {
							query: {
								organic_unit_id: school.id,
								type: "canteen",
							},
						});

						if (services_count > 0) filteredSchools.push(school);
					}

					return filteredSchools;
				}
			},
		},

		getServices: {
			rest: "GET /:organic_unit_id/services",
			params: {
				organic_unit_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				const service_devices = await ctx.call("alimentation.services-devices.find", {
					query: {
						device_id: ctx.meta.device.id,
					},
				});

				return ctx.call("alimentation.services.find", {
					query: {
						organic_unit_id: ctx.params.organic_unit_id,
						type: "canteen",
						id: service_devices.map((sd) => sd.service_id),
					},
					withRelated: false,
				});
			},
		},

		postProductCart: {
			rest: "POST /cart/bar/:product_id/add",
			params: {
				user_id: { type: "number", positive: true, optional: true },
				product_id: { type: "number", convert: true, integer: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const product_info = await this.getServiceFamilyProducts(ctx, ctx.params.product_id);

				const fiscal_entity = await ctx.call("alimentation.entities.get", {
					id: product_info.fentity_id,
				});
				const product_family = await ctx.call("alimentation.families-products.find", {
					query: {
						product_id: ctx.params.product_id,
					},
				});

				if (product_family.length === 0) {
					throw new Errors.ValidationError(
						"The product dont have a product_family",
						"PRODUCT_FAMILY_NOT_FOUND",
						{},
					);
				}
				const service = await ctx.call("alimentation.services-devices.getServiceBarOfDevice");

				let user = ctx.meta.user;
				if (ctx.params.user_id) {
					user = await ctx.call("authorization.users.get", {
						id: ctx.params.user_id,
					});
				}

				const prices = await this.getProductPrice([product_info], user, true);

				const productStock = await this.checkStockProduct(ctx, product_info, service[0]);

				const expires_in =
					prices.time === null
						? moment().add(30, "minutes").toDate() // TODO CHECK MAX DISPONIBILITY
						: moment().add(30, "minutes").toDate(); // TODO : moment(prices.time, "HH:mm:ss").toDate();

				const entity = {
					account_id: fiscal_entity[0].account_id,
					service_id: 2,
					product_code: product_info.code,
					article_type: "BAR",
					quantity: 1,
					unit_value: prices.price,
					vat_id: prices.tax_id,
					name: product_info.translations[0].name,
					description: product_info.translations[0].description,
					location: "Bar " + service ? service[0].name : "",
					max_quantity: productStock[1],
					service_confirm_path: "alimentation.orders.create_order",
					service_cancel_path: "alimentation.orders.cancel_order",
					extra_info: {
						product_id: product_info.id,
						fentity_id: fiscal_entity[0].id,
						service_id: service[0].id,
						user_id: user.id,
					},
					expires_in: expires_in,
				};
				return ctx.call("payments.cart.addCartItem", entity);
			},
		},

		postDishToCart: {
			rest: "POST /cart/refectory/:menu_dish_id/add",
			params: {
				user_id: { type: "number", convert: true, positive: true, optional: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;

				const menudish = await ctx.call("alimentation.menus-dishs.get", {
					id: ctx.params.menu_dish_id,
					withRelated: ["dish", "menu", "type"],
				});
				const dish_info = await ctx.call("alimentation.dishs.get", { id: menudish[0].dish.id });
				const fiscal_entity = await ctx.call("alimentation.entities.get", {
					id: dish_info[0].fentity_id,
				});
				let isToday = false;
				if (moment(menudish[0].menu.date).isSame(moment(), "d")) {
					isToday = true;
				}

				let user;
				if (ctx.meta.device.type === "POS") {
					if (ctx.params.user_id) {
						const info_user = await ctx.call("authorization.users.get", {
							id: ctx.params.user_id,
						});
						user = info_user[0];
					} else {
						throw new Errors.ValidationError(
							"Need send the user_id",
							"SEND_USER_ID_IS_REQUIRED",
							{},
						);
					}
				}
				if (ctx.meta.device.type !== "POS") {
					user = ctx.meta.user;
				}

				let date_available = null;
				if (menudish[0].menu.meal === "lunch") {
					if (menudish[0].type.disponibility_lunch.maximum_hour)
						date_available = moment(
							moment(menudish[0].menu.date).format("YYYY-MM-DD") +
								"T" +
								menudish[0].type.disponibility_lunch.maximum_hour,
						);
				} else {
					if (menudish[0].type.disponibility_dinner.maximum_hour)
						date_available = moment(
							moment(menudish[0].menu.date).format("YYYY-MM-DD") +
								"T" +
								menudish[0].type.disponibility_dinner.maximum_hour,
						);
				}

				const prices = await this.getDishPrice(ctx, menudish, user, isToday, menudish[0].menu.meal);

				const expires_in = prices.time === null ? date_available : prices.time.toDate();

				const quantity_dish = await this.checkDishStock(ctx, menudish);

				const entity = {
					user_id: user.id,
					account_id: fiscal_entity[0].account_id,
					service_id: 2,
					product_code: menudish[0].type.code,
					article_type: "REFECTORY",
					quantity: 1,
					unit_value: prices.price,
					vat_id: prices.tax_id,
					name: dish_info[0].translations[0].name,
					description: menudish[0].menu.service.name,
					location: menudish[0].menu.service.name,
					max_quantity: quantity_dish.stockQuantity,
					service_confirm_path: "alimentation.reservations.confirm_reservation",
					service_cancel_path: "alimentation.reservations.cancel_reservation",
					extra_info: {
						menu_dish_id: menudish[0].id,
						refectory_consume_at: menudish[0].menu.date,
						refectory_meal_category: menudish[0].menu.meal,
						refectory_meal_type: menudish[0].type.translations[0].name,
						fentity_id: fiscal_entity[0].id,
					},
					expires_in: expires_in,
				};

				return ctx.call("payments.cart.addCartItem", entity);
			},
		},

		getServiceById: {
			rest: "GET /service/:service_id",
			params: {
				service_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				return await ctx.call("alimentation.services.get", {
					id: ctx.params.service_id,
					withRelated: ["fentity", "devices"],
				});
			},
		},

		getServiceName: {
			rest: "GET /service",
			visibility: "published",
			handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				return ctx.call("alimentation.services-devices.getServiceBarOfDevice");
			},
		},

		getServiceFamilies: {
			rest: "GET /service/:service_id/families",
			params: {
				service_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const families_service = await ctx.call("alimentation.services-families.find", {
					query: {
						service_id: ctx.params.service_id,
					},
				});
				return ctx.call("alimentation.families.find", {
					query: {
						id: families_service.map((fs) => fs.family_id),
						active: true,
					},
					withRelated: ["translations", "file"],
				});
			},
		},

		getServiceFamilyProduct: {
			rest: "GET /service/:service_id/families/:family_id/products",
			params: {
				service_id: { type: "number", convert: true },
				family_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				// 1 - GET SERVICE
				const service = await ctx.call("alimentation.services.get", {
					id: ctx.params.service_id,
					withRelated: ["fentity"],
				});

				// 2 - VALIDATE FAMILY
				await ctx.call("alimentation.families.get", {
					id: ctx.params.family_id,
					withRelated: false,
				});

				// 3 - GET PRODUCTS IDS
				let product_family = await ctx.call("alimentation.families-products.find", {
					query: {
						family_id: ctx.params.family_id,
					},
					withRelated: ["translations", "prices", "composition"],
				});

				let list_products = [];
				for (const product of product_family) {
					const product_info = await this.getServiceFamilyProducts(ctx, product.product_id);
					list_products.push(product_info);
				}

				// 4 - Return only visibles and active
				list_products = list_products.filter((lp) => lp.visible === true && lp.active === true);

				let user = ctx.meta.user;

				let list_resp = [];
				for (const prod of list_products) {
					const isDisponible = this.checkDisponibility(prod.disponibility, new Date());
					let stockAvailable = false;
					let stockQuantity = 0;
					const stockInfo = await this.checkStockProduct(ctx, prod, service[0]);

					stockAvailable = stockInfo[0];
					stockQuantity = stockInfo[1];

					const prices = await this.getProductPrice([prod], user, true);

					let user_allergic = false;
					for (const allergens of prod.allergens) {
						if (allergens.allergic === true) {
							user_allergic = true;
							break;
						}
					}

					let resp = {
						id: prod.id,
						code: prod.code,
						price: prices.price,
						tax_id: prices.tax_id,
						prices: prod.prices,
						translations: prod.translations,
						allergens: prod.allergens,
						nutrients: prod.nutrients,
						complements: prod.complements,
						file_id: prod.file_id,
						service_id: service[0].id,
						account_id: service[0].fentity.account_id,
						isStockAvailable: stockAvailable,
						stockQuantity: stockQuantity,
						available: isDisponible && prod.available,
						show_derivatives: true,
						user_allergic: user_allergic,
						meal: null,
						date: null,
					};
					if (ctx.params.withRelated === "tax") {
						resp.tax = await ctx.call("configuration.taxes.get", { id: prod.tax_price_id });
					}
					if (ctx.params.withRelated === "file" && prod.file_id) {
						resp.file = await ctx.call("media.files.get", { id: prod.file_id });
					}
					list_resp.push(resp);
				}
				return list_resp;
			},
		},

		checkProductDisponibility: {
			rest: "GET /service/:service_id/products/:product_id/disponibility",
			params: {
				service_id: { type: "number", convert: true },
				product_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("alimentation.products.checkProductDisponibility", {
					product_id: parseInt(ctx.params.product_id),
				});
			},
		},

		getProductInfo: {
			rest: "GET /service/:service_id/products/:product_id",
			params: {
				service_id: { type: "number", convert: true },
				product_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				let resp = "";
				const service = await ctx.call("alimentation.services.get", {
					id: ctx.params.service_id,
					withRelated: "fentity",
				});
				const product = await this.getProductInfo(ctx, ctx.params.product_id);

				const allergens_nutrients = await this.calculateAllergensNutrientsValues(product);

				const user_allergens = await ctx.call("alimentation.users-allergens.returnsConfig", {
					user_id: ctx.meta.user ? ctx.meta.user.id : 0,
					allergens: allergens_nutrients.product_allergens,
				});
				const allergic = user_allergens.find((elm) => elm.allergic === true);
				let user_is_allergic = false;
				if (allergic) {
					user_is_allergic = true;
				}
				let price = product.price;
				let tax_id = product.tax_price_id;
				if (
					product.prices.length > 0 &&
					product.prices.filter((p) => p.profile_id === ctx.meta.user.profile_id).length > 0
				) {
					const maxPrice = product.prices
						.filter((p) => p.profile_id === ctx.meta.user.profile_id)
						.reduce((prev, current) => {
							const prevTime = new Date("2020-12-12T" + prev.time);
							const currentTime = new Date("2020-12-12T" + current.time);
							return prevTime > currentTime ? prev : current;
						});
					price = maxPrice ? maxPrice.price : product.price;
					tax_id = maxPrice ? maxPrice.tax_id : product.tax_price_id;
				}
				resp = {
					translations: product.translations,
					allergens: user_allergens,
					user_allergic: user_is_allergic,
					nutrients: allergens_nutrients.product_nutrients,
					prices: product.prices,
					composition: product.compound,
					file_id: product.file_id,
					price: price,
					tax_id: tax_id,
					service_id: service[0].id,
					account_id: service[0].fentity.account_id,
				};
				if (ctx.params.withRelated === "tax") {
					resp.tax = await ctx.call("configuration.taxes.get", { id: product.tax_price_id });
				}
				if (ctx.params.withRelated === "file") {
					resp.file = await ctx.call("media.files.get", { id: product.file_id });
				}
				return resp;
			},
		},

		getProductsRecents: {
			rest: "GET /service/:service_id/recents",
			params: {
				service_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;

				const service = await ctx.call("alimentation.services.get", {
					id: ctx.params.service_id,
					withRelated: ["fentity"],
				});

				const product_list = await ctx.call("alimentation.orders.getproductsLastMounth", {
					user_id: parseInt(ctx.meta.user.id),
					service_id: ctx.params.service_id,
					limit: 9,
				});

				let lista = [];
				for (const product of product_list) {
					const product_info = await this.getServiceFamilyProducts(ctx, product);
					lista.push(product_info);
				}
				let list_resp = [];
				for (const prod of lista) {
					const isDisponible = this.checkDisponibility(prod.disponibility, new Date());
					let stockAvailable = false;
					let stockQuantity = 0;
					const stockInfo = await this.checkStockProduct(ctx, prod, service[0]);
					stockAvailable = stockInfo[0];
					stockQuantity = stockInfo[1];
					let price = prod.price;
					let tax_id = prod.tax_price_id;
					if (
						prod.prices.length > 0 &&
						prod.prices.filter((p) => p.profile_id === ctx.meta.user.profile_id).length > 0
					) {
						const maxPrice = prod.prices
							.filter((p) => p.profile_id === ctx.meta.user.profile_id)
							.reduce((prev, current) => {
								const prevTime = new Date("2020-12-12T" + prev.time);
								const currentTime = new Date("2020-12-12T" + current.time);
								return prevTime > currentTime ? prev : current;
							});
						price = maxPrice ? maxPrice.price : prod.price;
						tax_id = maxPrice ? maxPrice.tax_id : prod.tax_price_id;
					}
					const user_allergens = await ctx.call("alimentation.users-allergens.returnsConfig", {
						user_id: ctx.meta.user ? ctx.meta.user.id : 0,
						allergens: prod.allergens,
					});
					const allergic = user_allergens.find((elm) => elm.allergic === true);
					let user_is_allergic = false;
					if (allergic) {
						user_is_allergic = true;
					}
					let resp = {
						id: prod.id,
						code: prod.code,
						price: price,
						tax_id: tax_id,
						prices: prod.prices,
						translations: prod.translations,
						allergens: prod.allergens,
						nutrients: prod.nutrients,
						complements: prod.complements,
						file_id: prod.file_id,
						isStockAvailable: stockAvailable,
						stockQuantity: stockQuantity,
						available: isDisponible && prod.available,
						user_allergic: user_is_allergic,
						show_derivatives: true,
						meal: null,
						date: null,
					};
					if (ctx.params.withRelated === "tax") {
						resp.tax = await ctx.call("configuration.taxes.get", { id: prod.tax_price_id });
					}
					if (ctx.params.withRelated === "file") {
						resp.file = await ctx.call("media.files.get", { id: prod.file_id });
					}
					list_resp.push(resp);
				}
				return list_resp;
			},
		},

		getServiceProducts: {
			rest: "GET /service/:service_id/products",
			params: {
				service_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const service = await ctx.call("alimentation.services.get", {
					id: ctx.params.service_id,
					withRelated: ["fentity"],
				});
				const familys = await ctx.call("alimentation.services-families.find", {
					query: {
						service_id: ctx.params.service_id,
					},
				});
				let list_products = [];
				for (const family of familys) {
					const product_family = await ctx.call("alimentation.families-products.find", {
						query: {
							family_id: family.family_id,
						},
					});
					for (const product of product_family) {
						list_products.push(product);
					}
				}
				let lista = [];
				for (const product of list_products) {
					const product_info = await this.getServiceFamilyProducts(ctx, product.product_id);
					lista.push(product_info);
				}
				let list_resp = [];
				for (const prod of lista) {
					const isDisponible = this.checkDisponibility(prod.disponibility, new Date());
					let stockAvailable = false;
					let stockQuantity = 0;
					const stockInfo = await this.checkStockProduct(ctx, prod, service[0]);
					stockAvailable = stockInfo[0];
					stockQuantity = stockInfo[1];
					let price = prod.price;
					let tax_id = prod.tax_price_id;
					if (
						prod.prices.length > 0 &&
						prod.prices.filter((p) => p.profile_id === ctx.meta.user.profile_id).length > 0
					) {
						const maxPrice = prod.prices
							.filter((p) => p.profile_id === ctx.meta.user.profile_id)
							.reduce((prev, current) => {
								const prevTime = new Date("2020-12-12T" + prev.time);
								const currentTime = new Date("2020-12-12T" + current.time);
								return prevTime > currentTime ? prev : current;
							});
						price = maxPrice ? maxPrice.price : prod.price;
						tax_id = maxPrice ? maxPrice.tax_id : prod.tax_price_id;
					}
					const user_allergens = await ctx.call("alimentation.users-allergens.returnsConfig", {
						user_id: ctx.meta.user ? ctx.meta.user.id : 0,
						allergens: prod.allergens,
					});
					const allergic = user_allergens.find((elm) => elm.allergic === true);
					let user_is_allergic = false;
					if (allergic) {
						user_is_allergic = true;
					}
					let resp = {
						id: prod.id,
						code: prod.code,
						price: price,
						tax_id: tax_id,
						prices: prod.prices,
						translations: prod.translations,
						allergens: prod.allergens,
						nutrients: prod.nutrients,
						complements: prod.complements,
						file_id: prod.file_id,
						service_id: service[0].id,
						account_id: service[0].fentity.account_id,
						isStockAvailable: stockAvailable,
						stockQuantity: stockQuantity,
						available: isDisponible && prod.available,
						user_allergic: user_is_allergic,
						show_derivatives: true,
						meal: null,
						date: null,
					};
					if (ctx.params.withRelated === "taxes") {
						resp.tax = await ctx.call("configuration.taxes.get", { id: prod.tax_price_id });
					}
					if (ctx.params.withRelated === "file") {
						resp.file = await ctx.call("media.files.get", { id: prod.file_id });
					}
					list_resp.push(resp);
				}
				return list_resp;
			},
		},
		getServiceMenu: {
			rest: "GET /service/:service_id/menus/:date/:meal",
			visibility: "published",
			params: {
				service_id: { type: "number", convert: true },
				date: { type: "string" },
				meal: { type: "string" },
			},
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const service = await ctx.call("alimentation.services.get", { id: ctx.params.service_id });
				if (service[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service is not type canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
				const menu = await ctx.call("alimentation.menus.find", {
					query: {
						meal: ctx.params.meal,
						date: ctx.params.date,
						service_id: ctx.params.service_id,
					},
					withRelated: false,
				});
				const products = [];
				if (menu.length !== 0) {
					if (menu[0].validated) {
						const configs = await ctx
							.call("alimentation.configurations.list", {})
							.then((result) => result[0]);

						let user;
						if (ctx.meta.device.type === "POS") {
							if (ctx.params.query.user_id) {
								const user_info = await ctx.call("authorization.users.get", {
									id: ctx.params.query.user_id,
								});
								user = user_info[0];
							} else {
								throw new Errors.ValidationError(
									"Need send the user_id",
									"SEND_USER_ID_IS_REQUIRED",
									{},
								);
							}
						} else {
							user = ctx.meta.user;
						}

						let menu_dishs = await ctx.call("alimentation.menus-dishs.find", {
							query: {
								menu_id: menu[0].id,
								available: true,
							},
							withRelated: ["dish", "menu", "type"],
						});

						menu_dishs = menu_dishs.filter(
							(md) => !md.type.excluded_user_profile_ids.includes(user.profile_id),
						);

						for (const menu_dish of menu_dishs) {
							let available = false;
							let isStockAvailable = false;
							let stockQuantity = 0;
							let user_allergic = false;
							if (menu_dish.menu.meal === "lunch") {
								this.logger.info("CHECK LUNCH DISPONIBILITY");
								available =
									this.checkDisponibility(
										menu_dish.type.disponibility_lunch,
										menu_dish.menu.date,
										true,
									) && menu_dish.available;
							}
							if (menu_dish.menu.meal === "dinner") {
								this.logger.info("CHECK DINNER DISPONIBILITY");
								available =
									this.checkDisponibility(
										menu_dish.type.disponibility_dinner,
										menu_dish.menu.date,
										true,
									) && menu_dish.available;
							}

							this.logger.info("FINAL DISPONIBILITY:", available);

							const stockResult = await this.checkDishStock(ctx, [menu_dish]);
							isStockAvailable = stockResult.isStockAvailable;
							stockQuantity = stockResult.stockQuantity;
							let isToday = false;
							if (moment(menu_dish.menu.date).isSame(moment(), "d")) {
								isToday = true;
							}

							const prices = await this.getDishPrice(
								ctx,
								[menu_dish],
								user,
								isToday,
								menu_dish.menu.meal,
							);

							const dishAllergenAndNutrients = await ctx.call(
								"alimentation.dishs.calculatedAllergenAndNutrientsComposition",
								{ dish_id: menu_dish.dish.id },
							);

							const user_allergens_composition = await ctx.call(
								"alimentation.users-allergens.returnsConfig",
								{
									user_id: ctx.meta.user ? ctx.meta.user.id : 0,
									allergens: dishAllergenAndNutrients.allergens,
								},
							);

							for (const dish_allergens of user_allergens_composition) {
								if (dish_allergens.allergic === true) {
									user_allergic = true;
								}
							}

							const entity = await ctx.call("alimentation.entities.get", {
								id: menu_dish.dish.fentity_id,
							});

							const resp = {
								meal: menu_dish.menu.meal,
								date: menu_dish.menu.date,
								id: menu_dish.id,
								code: menu_dish.type.code,
								price: prices.price,
								tax_id: prices.tax_id,
								prices: menu_dish.type.prices,
								file_id: menu_dish.file_id,
								dish_type_translation: menu_dish.type.translations,
								type: menu_dish.type,
								dish_type_id: menu_dish.type.id,
								show_allergens: configs.SHOW_ALLERGENS_ON_MENU,
								allergens: user_allergens_composition,
								nutrients: dishAllergenAndNutrients.nutrients,
								user_allergic: user_allergic,
								service_id: menu_dish.menu.service.id,
								account_id: entity[0].account_id,
								translations: menu_dish.dish.translations,
								location: menu_dish.menu.service.name,
								isStockAvailable,
								stockQuantity,
								dish_id: menu_dish.dish.id,
								available,
							};
							let withRelatedsArray = [];
							if (ctx.params.withRelated !== undefined) {
								withRelatedsArray = ctx.params.withRelated.split(",");
							}
							if (withRelatedsArray.includes("taxes")) {
								if (prices.tax_id !== null && prices.tax_id !== undefined) {
									resp.tax = await ctx.call("configuration.taxes.get", { id: prices.tax_id });
								}
							}
							if (withRelatedsArray.includes("file")) {
								resp.file = menu_dish.dish.file;
							}
							products.push(resp);
						}
					}
				}
				return products;
			},
		},

		checkDishDisponibility: {
			rest: "GET service/:service_id/dish/:menu_dish_id/disponibility",
			params: {
				service_id: { type: "number", convert: true },
				menu_dish_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				await ctx.call("alimentation.services.get", { id: ctx.params.service_id });

				const menuDish = await ctx.call("alimentation.menus-dishs.get", {
					id: ctx.params.menu_dish_id,
					withRelated: ["dish", "menu", "type"],
				});
				let available = false;
				let isStockAvailable = false;
				let stockQuantity = 0;

				if (menuDish[0].menu.meal === "lunch") {
					available =
						this.checkDisponibility(
							menuDish[0].type.disponibility_lunch,
							menuDish[0].menu.date,
							true,
						) && menuDish[0].available;
				}
				if (menuDish[0].menu.meal === "dinner") {
					available =
						this.checkDisponibility(
							menuDish[0].type.disponibility_dinner,
							menuDish[0].menu.date,
							true,
						) && menuDish[0].available;
				}
				const stockResult = await this.checkDishStock(ctx, menuDish);
				isStockAvailable = stockResult.isStockAvailable;
				stockQuantity = stockResult.stockQuantity;

				const resp = {
					available: available,
					isStockAvailable: isStockAvailable,
					stockQuantity: stockQuantity,
				};

				return resp;
			},
		},

		getDishInfo: {
			rest: "GET /service/:service_id/dish/:menu_dish_id",
			params: {
				service_id: { type: "number", convert: true },
				menu_dish_id: { type: "number", convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				await ctx.call("alimentation.services.get", { id: ctx.params.service_id });

				const configs = await ctx
					.call("alimentation.configurations.list", {})
					.then((result) => result[0]);

				const menuDish = await ctx.call("alimentation.menus-dishs.get", {
					id: ctx.params.menu_dish_id,
					withRelated: ["dish", "menu", "type"],
				});
				let available = false;
				let isStockAvailable = false;
				let stockQuantity = 0;
				let user_allergic = false;

				if (menuDish[0].menu.meal === "lunch") {
					available =
						this.checkDisponibility(
							menuDish[0].type.disponibility_lunch,
							menuDish[0].menu.date,
							true,
						) && menuDish[0].available;
				}
				if (menuDish[0].menu.meal === "dinner") {
					available =
						this.checkDisponibility(
							menuDish[0].type.disponibility_dinner,
							menuDish[0].menu.date,
							true,
						) && menuDish[0].available;
				}
				const entity = await ctx.call("alimentation.entities.get", {
					id: menuDish[0].dish.fentity_id,
				});

				const stockResult = await this.checkDishStock(ctx, menuDish);
				isStockAvailable = stockResult.isStockAvailable;
				stockQuantity = stockResult.stockQuantity;
				let isToday = false;
				if (moment(menuDish[0].menu.date).isSame(moment(), "d")) {
					isToday = true;
				}
				const prices = await this.getDishPrice(
					ctx,
					menuDish,
					ctx.meta.user,
					isToday,
					menuDish[0].menu.meal,
				);

				let all_allergens = [];
				let all_nutrients = [];

				const dishAllergenAndNutrients = await ctx.call(
					"alimentation.dishs.calculatedAllergenAndNutrientsComposition",
					{ dish_id: menuDish[0].dish.id },
				);
				all_nutrients = dishAllergenAndNutrients.nutrients;

				if (configs.SHOW_ALLERGENS_ON_MENU) {
					const allergens_dish = await ctx.call("alimentation.dishs.calculatedAllergen", {
						dish_id: menuDish[0].dish.id,
					});
					const user_allergens_dish = await ctx.call("alimentation.users-allergens.returnsConfig", {
						user_id: ctx.meta.user ? ctx.meta.user.id : 0,
						allergens: allergens_dish,
					});

					const user_allergens_composition = await ctx.call(
						"alimentation.users-allergens.returnsConfig",
						{
							user_id: ctx.meta.user ? ctx.meta.user.id : 0,
							allergens: dishAllergenAndNutrients.allergens,
						},
					);
					all_allergens = user_allergens_dish;
					for (const allergen_compositio of user_allergens_composition) {
						const findA = all_allergens.find((allergen) => allergen.id === allergen_compositio.id);
						if (!findA) {
							all_allergens.push(allergen_compositio);
						}
					}
					for (const dish_allergens of all_allergens) {
						if (dish_allergens.allergic === true) {
							user_allergic = true;
						}
					}
				}

				const resp = {
					meal: menuDish[0].menu.meal,
					date: menuDish[0].menu.date,
					id: menuDish[0].id,
					code: menuDish[0].type.code,
					price: prices.price,
					tax_id: prices.tax_id,
					prices: menuDish[0].type.prices,
					file_id: menuDish[0].file_id,
					dish_type_translation: menuDish[0].type.translations,
					dish_type_id: menuDish[0].type.id,
					type: menuDish[0].type,
					show_allergens: configs.SHOW_ALLERGENS_ON_MENU,
					allergens: all_allergens,
					nutrients: all_nutrients,
					user_allergic: user_allergic,
					service_id: menuDish[0].menu.service.id,
					account_id: entity[0].account_id,
					translations: menuDish[0].dish.translations,
					location: menuDish[0].menu.service.name,
					isStockAvailable: isStockAvailable,
					stockQuantity: stockQuantity,
					available: available,
					show_derivatives: false,
				};

				let withRelatedsArray = [];
				if (ctx.params.withRelated !== undefined) {
					withRelatedsArray = ctx.params.withRelated.split(",");
				}
				if (withRelatedsArray.includes("taxes")) {
					if (prices.tax_id !== null && prices.tax_id !== undefined) {
						resp.tax = await ctx.call("configuration.taxes.get", { id: prices.tax_id });
					}
				}
				if (withRelatedsArray.includes("file")) {
					resp.file = menuDish[0].dish.file;
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
		async verifyLanguage(ctx) {
			const language = await ctx.call("configuration.languages.get", {
				id: ctx.params.language_id,
			});
			if (language.length === 0) {
				throw new Errors.EntityNotFoundError("Language not found", ctx.params.language_id);
			}
		},

		async getProductInfo(ctx, product_id) {
			this.clearCache();
			let product = await ctx.call("alimentation.products.get", {
				id: product_id,
				withRelated: ["translations", "allergens", "nutrients", "prices", "composition"],
			});

			let compounds = [];
			for (const composition of product.composition) {
				const product_compund = await ctx.call("alimentation.products.get", {
					id: composition.compound_id,
					withRelated: ["translations", "allergens", "nutrients"],
				});
				product_compund[0].quantity = composition.quantity;
				compounds.push(product_compund[0]);
			}
			product[0].compound = compounds;
			for (const comp1 of product[0].compound) {
				let compositions1 = await ctx.call("alimentation.compounds.find", {
					query: {
						product_id: comp1.id,
					},
				});
				let compound1 = [];
				for (const composition1 of compositions1) {
					const product_compund1 = await ctx.call("alimentation.products.get", {
						id: composition1.compound_id,
						withRelated: ["translations", "allergens", "nutrients"],
					});
					product_compund1[0].quantity = composition1.quantity;
					compound1.push(product_compund1[0]);
				}
				comp1.compound1 = compound1;
			}
			return product[0];
		},

		async getServiceFamilyProducts(ctx, product_id) {
			let product = await ctx.call("alimentation.products.get", {
				id: product_id,
				withRelated: [
					"translations",
					"allergens",
					"prices",
					"disponibility",
					"stocks",
					"complements",
				],
			});
			let compositions = await ctx.call("alimentation.compounds.find", {
				query: {
					product_id: product_id,
				},
			});
			let compounds = [];
			for (const composition of compositions) {
				const product_compund = await ctx.call("alimentation.products.get", {
					id: composition.compound_id,
					withRelated: false,
				});
				product_compund[0].quantity = composition.quantity;
				compounds.push(product_compund[0]);
			}
			product[0].compound = compounds;
			for (const comp1 of product[0].compound) {
				let compositions1 = await ctx.call("alimentation.compounds.find", {
					query: {
						product_id: comp1.id,
					},
				});
				let compound1 = [];
				for (const composition1 of compositions1) {
					const product_compund1 = await ctx.call("alimentation.products.get", {
						id: composition1.compound_id,
						withRelated: ["translations", "allergens", "nutrients"],
					});
					product_compund1[0].quantity = composition1.quantity;
					compound1.push(product_compund1[0]);
				}
				comp1.compound1 = compound1;
			}
			const list_allergens = [];
			if (product[0].allergens) {
				if (product[0].allergens.length > 0) {
					for (const allergen of product[0].allergens) {
						const user_allergic = await ctx.call("alimentation.users-allergens.find", {
							query: {
								user_id: ctx.meta.user.id,
								allergen_id: allergen.id,
							},
						});

						if (user_allergic.length > 0) {
							allergen.allergic = true;
						} else {
							allergen.allergic = false;
						}
						list_allergens.push(allergen);
					}
				}
			}
			product[0].allergens = list_allergens;
			return product[0];
		},

		checkDisponibility(disponibility, date, isCanteen) {
			const now = moment();
			this.logger.info("NOW", now);

			this.logger.info("START: checkDisponibility");
			this.logger.info("DATE: ", date);

			const begin_date = disponibility.begin_date;
			const begin_date_after = disponibility.begin_date ? now.isSameOrAfter(begin_date) : true;
			this.logger.info("begin_date", begin_date);
			this.logger.info("begin_date_after", begin_date_after);

			const end_date = disponibility.end_date;
			const end_date_before = disponibility.end_date ? now.isSameOrBefore(end_date) : true;
			this.logger.info("end_date", end_date);
			this.logger.info("end_date_before", end_date_before);

			let maximum_hour_before = disponibility.maximum_hour ? false : true;
			let minimum_hour_after = disponibility.minimum_hour ? false : true;
			if (moment(date).isSame(moment(), "d")) {
				this.logger.info('moment(date).isSame(moment(), "d")');
				const maximum_hour = moment(disponibility.maximum_hour, "HH:mm:ssZ");
				const minimum_hour = moment(disponibility.minimum_hour, "HH:mm:ssZ");

				if (minimum_hour.isSameOrAfter(maximum_hour)) minimum_hour.subtract(1, "day");

				if (!maximum_hour_before) maximum_hour_before = now.isSameOrBefore(maximum_hour);

				if (!minimum_hour_after) minimum_hour_after = now.isSameOrAfter(minimum_hour);

				this.logger.info("maximum_hour", maximum_hour);
				this.logger.info("minimum_hour", minimum_hour);
				this.logger.info("maximum_hour_before", maximum_hour_before);
				this.logger.info("minimum_hour_after", minimum_hour_after);
			} else {
				this.logger.info('ELSE -> moment(date).isSame(moment(), "d")');
				this.logger.info("moment().isBefore(date)", now.isSameOrBefore(date));
				this.logger.info("moment().isAfter(date)", now.isSameOrAfter(date));

				if (moment().isSameOrBefore(date)) {
					maximum_hour_before = true;
					minimum_hour_after = true;
				} else {
					return false;
				}
			}
			let dayOfWeekDisponibility = false;

			if (!isCanteen) {
				switch (now.day()) {
					case 0:
						dayOfWeekDisponibility = disponibility.sunday;
						break;
					case 1:
						dayOfWeekDisponibility = disponibility.monday;
						break;
					case 2:
						dayOfWeekDisponibility = disponibility.tuesday;
						break;
					case 3:
						dayOfWeekDisponibility = disponibility.wednesday;
						break;
					case 4:
						dayOfWeekDisponibility = disponibility.thursday;
						break;
					case 5:
						dayOfWeekDisponibility = disponibility.friday;
						break;
					case 6:
						dayOfWeekDisponibility = disponibility.saturday;
						break;
				}
			} else {
				switch (moment(date).day()) {
					case 0:
						dayOfWeekDisponibility = disponibility.sunday;
						break;
					case 1:
						dayOfWeekDisponibility = disponibility.monday;
						break;
					case 2:
						dayOfWeekDisponibility = disponibility.tuesday;
						break;
					case 3:
						dayOfWeekDisponibility = disponibility.wednesday;
						break;
					case 4:
						dayOfWeekDisponibility = disponibility.thursday;
						break;
					case 5:
						dayOfWeekDisponibility = disponibility.friday;
						break;
					case 6:
						dayOfWeekDisponibility = disponibility.saturday;
						break;
				}
			}

			this.logger.info("begin_date_after", begin_date_after);
			this.logger.info("end_date_before", end_date_before);
			this.logger.info("minimum_hour_after", minimum_hour_after);
			this.logger.info("maximum_hour_before", maximum_hour_before);
			this.logger.info("dayOfWeekDisponibility", dayOfWeekDisponibility);
			return (
				begin_date_after &&
				end_date_before &&
				minimum_hour_after &&
				maximum_hour_before &&
				dayOfWeekDisponibility
			);
		},

		async checkDishStock(ctx, menuDish) {
			if (menuDish[0].doses_available === 0) {
				return { isStockAvailable: true, stockQuantity: 999 };
			}
			const reservationsResult = await ctx.call(
				"alimentation.reservations.getMenuDishReservationCount",
				{ menu_dish_id: menuDish[0].id },
			);
			return {
				isStockAvailable: menuDish[0].doses_available > reservationsResult,
				stockQuantity: menuDish[0].doses_available - reservationsResult,
			};
		},
		async getProductPrice(product, user, isToday) {
			if (product[0].prices && product[0].prices.length > 0) {
				const prices = product[0].prices.filter((p) => p.profile_id === user.profile_id);

				if (prices && prices.length > 0) {
					const now = moment().add(1, "hour");
					const existPriceTimeGraterCurrent = prices.find((p) =>
						moment(p.time, "HH:mm:ss").isBefore(now),
					);
					if (existPriceTimeGraterCurrent) {
						if (isToday) {
							const maxPrice = prices.reduce((prev, current) => {
								const prevTime = new Date("2018-12-12T" + prev.time);
								const currentTime = new Date("2018-12-12T" + current.time);
								return prevTime > currentTime ? prev : current;
							});
							return {
								price: maxPrice ? maxPrice.price : product[0].price,
								tax_id: maxPrice ? maxPrice.tax_id : product[0].tax_id,
								time: maxPrice ? maxPrice.time : null,
							};
						} else {
							const firstPrice = prices.reduce((prev, current) => {
								const prevTime = new Date("2018-12-12T" + prev.time);
								const currentTime = new Date("2018-12-12T" + current.time);
								return prevTime < currentTime ? prev : current;
							});
							return {
								price: firstPrice ? firstPrice.price : product[0].price,
								tax_id: firstPrice ? firstPrice.tax_id : product[0].tax_id,
								time: firstPrice ? firstPrice.time : null,
							};
						}
					}
				}
			}
			return {
				price: product[0].price,
				tax_id: product[0].tax_price_id,
				time: null,
			};
		},
		async getDishPrice(ctx, menuDish, user, isToday, meal) {
			if (menuDish[0].type.prices && menuDish[0].type.prices.length > 0) {
				const prices = menuDish[0].type.prices.filter(
					(p) => p.profile_id === user.profile_id && p.meal === meal,
				);

				if (prices && prices.length > 0) {
					if (isToday) {
						const now = moment().add(1, "hour");
						const pricesBeforeThanCurrent = prices.filter((p) => {
							return moment(p.time, "HH:mm:ss").isBefore(now);
						});
						const pricesAfterThanCurrent = prices.filter((p) => {
							return moment(p.time, "HH:mm:ss").isAfter(now);
						});

						if (pricesBeforeThanCurrent && pricesBeforeThanCurrent.length) {
							const maxPrice = pricesBeforeThanCurrent.reduce((prev, current) => {
								const prevTime = new Date("2018-12-12T" + prev.time);
								const currentTime = new Date("2018-12-12T" + current.time);
								return prevTime > currentTime ? prev : current;
							});

							let nextTime = null;
							if (pricesAfterThanCurrent.length > 0) {
								nextTime = pricesAfterThanCurrent.reduce((prev, current) => {
									const prevTime = new Date("2018-12-12T" + prev.time);
									const currentTime = new Date("2018-12-12T" + current.time);
									return prevTime < currentTime ? prev : current;
								});
							}

							return {
								price: maxPrice ? maxPrice.price : menuDish[0].type.price,
								tax_id: maxPrice ? maxPrice.tax_id : menuDish[0].type.tax_id,
								time: nextTime ? moment(nextTime.time, "HH:mm:ss") : null,
							};
						}

						return {
							price: menuDish[0].type.price,
							tax_id: menuDish[0].type.tax_id,
							time: null,
						};
					} else {
						const firstPrice = prices.reduce((prev, current) => {
							const prevTime = new Date("2018-12-12T" + prev.time);
							const currentTime = new Date("2018-12-12T" + current.time);
							return prevTime < currentTime ? prev : current;
						});

						return {
							price: firstPrice ? firstPrice.price : menuDish[0].type.price,
							tax_id: firstPrice ? firstPrice.tax_id : menuDish[0].type.tax_id,
							time: firstPrice
								? moment(moment(menuDish[0].menu.date).format("YYYY-MM-DD") + "T" + firstPrice.time)
								: null,
						};
					}
				}
			}
			return {
				price: menuDish[0].type.price,
				tax_id: menuDish[0].type.tax_id,
				time: null,
			};
		},

		async calculateAllergensNutrientsValues(product) {
			let product_allergens = [];
			let product_nutrients = [];
			for (const compound of product.compound) {
				for (const allergen of compound.allergens) {
					if (product_allergens.indexOf(allergen) === -1) {
						product_allergens.push(allergen);
					}
				}
				for (const nutrient of compound.nutrients) {
					const n_exist = product_nutrients.find((ele) => ele.nutrient.id === nutrient.nutrient.id);
					if (n_exist) {
						const temp_quantity = compound.quantity * nutrient.quantity;
						n_exist.quantity += temp_quantity;
					} else {
						const temp_quantity = compound.quantity * nutrient.quantity;
						const nutri = {
							quantity: temp_quantity,
							nutrient: nutrient.nutrient,
						};
						product_nutrients.push(nutri);
					}
				}
				for (const compound1 of compound.compound1) {
					for (const allergen1 of compound1.allergens) {
						if (product_allergens.indexOf(allergen1) === -1) {
							product_allergens.push(allergen1);
						}
						for (const nutr of compound1.nutrients) {
							let exist = product_nutrients.find((ele) => ele.nutrient.id === nutr.nutrient.id);
							if (exist) {
								const temp_quantity1 = compound.quantity * nutr.quantity;
								exist.quantity += temp_quantity1;
							} else {
								const temp_quantity1 = compound.quantity * nutr.quantity;
								const nutri1 = {
									quantity: temp_quantity1,
									nutrient: nutr.nutrient,
								};
								product_nutrients.push(nutri1);
							}
						}
					}
				}
				return {
					product_nutrients: product_nutrients,
					product_allergens: product_allergens,
				};
			}
			return {
				product_nutrients: product_nutrients,
				product_allergens: product_allergens,
			};
		},
		async checkStockProduct(ctx, product, service) {
			let stockAvailable = false;
			let stockQuantity = 0;

			if (product.track_stock === "product") {
				const stock = await ctx.call("alimentation.stocks.find", {
					query: {
						wharehouse_id: service.wharehouse_id,
						product_id: product.id,
						quantity: {
							gt: 0,
						},
					},
					withRelated: false,
				});
				product.stocks = stock;

				if (product.stocks.length === 0) {
					stockAvailable = false;
					stockQuantity = 0;
				}
				if (product.stocks.length !== 0) {
					stockQuantity = product.stocks.reduce((acc, curr) => acc + curr.quantity, 0);
					stockAvailable = stockQuantity > 0;
				} else {
					stockQuantity = 0;
					stockAvailable = false;
					return [stockAvailable, stockQuantity];
				}
			}

			if (product.track_stock === "composition" && product.compound) {
				for (let compound of product.compound) {
					compound.stock = await this.checkStockProduct(ctx, compound, service);
					compound.stock[1] = parseInt(compound.stock[1] / compound.quantity);
				}

				if (!product.compound.length) {
					return [stockAvailable, stockQuantity];
				}

				const isSomeUnavailable = product.compound.find((c) => c.stock[0] === false);
				if (isSomeUnavailable) {
					return isSomeUnavailable.stock;
				}

				product.compound.sort((a, b) => {
					return a.stock[1] - b.stock[1];
				});

				return product.compound[0].stock;
			}

			if (product.track_stock === "product_composition") {
				const stock = await ctx.call("alimentation.stocks.find", {
					query: {
						wharehouse_id: service.wharehouse_id,
						product_id: product.id,
						quantity: {
							gt: 0,
						},
					},
					withRelated: false,
				});
				product.stocks = stock;

				if (product.stocks.length === 0) {
					stockAvailable = false;
					stockQuantity = 0;
				}
				if (product.stocks.length !== 0) {
					stockQuantity = product.stocks.reduce((acc, curr) => acc + curr.quantity, 0);
					stockAvailable = stockQuantity > 0;
				} else {
					stockQuantity = 0;
					stockAvailable = false;
					return [stockAvailable, stockQuantity];
				}

				for (let compound of product.compound) {
					compound.stock = await this.checkStockProduct(ctx, compound, service);
					compound.stock[1] = compound.stock[1] / compound.quantity;
				}

				if (!product.compound.length) {
					return [stockAvailable, stockQuantity];
				}

				const isSomeUnavailable = product.compound.find((c) => c.stock[0] === false);
				if (isSomeUnavailable) {
					return isSomeUnavailable.stock;
				}

				product.compound.sort((a, b) => {
					return a.stock[1] - b.stock[1];
				});

				if (stockAvailable) {
					if (product.compound[0].stock[1] > stockQuantity) {
						return product.compound[0].stock;
					}
				}
			}

			if (product.track_stock === "none") {
				stockAvailable = true;
				stockQuantity = 999;
			}
			return [stockAvailable, stockQuantity];
		},
		formatDate(date) {
			return moment(date).format("yyyy-MM-DD");
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
