"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.menus",
	table: "menu",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "menus")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "meal", "date", "validated", "service_id", "created_at", "updated_at"],
		defaultWithRelateds: ["service"],
		withRelateds: {
			dishs(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((menu) => {
						return ctx
							.call("alimentation.menus-dishs.getDishInfo", { menu_id: menu.id })
							.then((res) => (menu.dishs = res));
					}),
				);
			},
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.services", "service", "service_id", "id");
			},
		},
		entityValidator: {
			meal: { type: "string", values: ["lunch", "dinner", "breakfast"], default: "lunch" },
			date: { type: "date", convert: true },
			validated: { type: "boolean", default: false },
			service_id: { type: "number", positive: true, integer: true },
			dishs: {
				type: "array",
				items: {
					type: "object",
					props: {
						dish_id: { type: "number", positive: true, integer: false },
						dishType_id: { type: "number", positive: true, integer: false },
						available: { type: "boolean" },
						doses_available: { type: "number", min: 0, integer: false, convert: true },
					},
				},
			},
		},
	},
	hooks: {
		before: {
			create: [
				"validateService",
				"validateMeals",
				"validateDishs",
				"validateDishsTypes",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.validated = true;
				},
			],
			update: [
				"validateService",
				"validateDishs",
				"validateDishsTypes",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
					ctx.params.validated = true;
				},
			],
		},
		after: {
			create: ["saveMenuDish"],
			update: ["updateMenuDish"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		acceptMenu: {
			rest: "POST /:id/accept",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:menus:validate",
			visibility: "published",
			async handler(ctx) {
				const menu = await this._get(ctx, { id: ctx.params.id });
				menu[0].dishs = [];
				menu[0].validated = true;
				await this._update(ctx, menu[0]);
				this.clearCache();
				return await this._get(ctx, { id: ctx.params.id });
			},
		},
		addMenuDish: {
			rest: "POST /addDish",
			params: {
				meal: { type: "enum", values: ["lunch", "dinner"] },
				date: { type: "date", convert: true },
				service_id: { type: "number", positive: true, integer: true },
				type_id: { type: "number", positive: true, integer: true },
				dish_id: { type: "number", positive: true, integer: true },
				doses_available: { type: "number", default: true, convert: true },
				available: { type: "boolean", default: true },
			},
			async handler(ctx) {
				const menus = await ctx.call("alimentation.menus.find", {
					query: {
						meal: ctx.params.meal,
						date: moment(ctx.params.date).format("YYYY-MM-DD"),
						service_id: ctx.params.service_id,
					},
				});

				if (menus.length === 0) {
					throw new Errors.ValidationError("Menu not found in this date", "MENU_NOT_FOUND", {});
				}

				return ctx.call("alimentation.menus-dishs.saveMenuDish", {
					menu_id: menus[0].id,
					meal: menus[0].meal,
					date: menus[0].date,
					dishs: [
						{
							dish_id: ctx.params.dish_id,
							dishType_id: ctx.params.type_id,
							available: ctx.params.available,
							doses_available: ctx.params.doses_available,
						},
					],
				});
			},
		},
		rejectMenu: {
			rest: "POST /:id/reject",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:menus:validate",
			visibility: "published",
			async handler(ctx) {
				const menu = await this._get(ctx, { id: ctx.params.id });
				menu[0].dishs = [];
				menu[0].validated = false;
				await this._update(ctx, menu[0]);
				this.clearCache();
				return await this._get(ctx, { id: ctx.params.id });
			},
		},
		getMenuDish: {
			rest: "GET /dish/:id",
			params: {
				id: { type: "number", convert: true },
			},
			visibility: "published",
			scope: "alimentation:menus:read",
			async handler(ctx) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["dish_id"] = ctx.params.id;
				return await ctx.call("alimentation.menus-dishs.find", ctx.params);
			},
		},
		createMenusBulk: {
			rest: "POST /bulk",
			params: {
				service_ids: {
					type: "array",
					items: { type: "number" },
				},
				meals: {
					type: "array",
					items: { type: "string" },
				},
				dates: {
					type: "array",
					items: { type: "string" },
				},
				dishs: {
					type: "array",
					items: {
						type: "object",
						props: {
							dish_id: { type: "number", positive: true, integer: false },
							dishType_id: { type: "number", positive: true, integer: false },
							available: { type: "boolean" },
							doses_available: { type: "number", min: 0, integer: false },
						},
					},
				},
			},
			scope: "alimentation:menus:create",
			visibility: "published",
			async handler(ctx) {
				let menus = [];
				for (const d of ctx.params.dates) {
					for (const m of ctx.params.meals) {
						for (const s of ctx.params.service_ids) {
							const existent = await this._find(ctx, {
								query: {
									date: d,
									meal: m,
									service_id: s,
								},
							});
							menus.push({
								id: existent.length !== 0 ? existent[0].id : null,
								date: d,
								meal: m,
								dishs: ctx.params.dishs,
								service_id: s,
							});
						}
					}
				}
				for (const m of menus) {
					if (m.id) {
						const me = await this._update(ctx, m);
						await ctx.call("alimentation.menus-dishs.saveMenuDish", {
							menu_id: me[0].id,
							meal: m.meal,
							dishs: m.dishs,
							date: new Date(m.date),
						});
					} else {
						const entity = {
							date: new Date(m.date),
							meal: m.meal,
							service_id: m.service_id,
							dishs: m.dishs,
							created_at: new Date(),
							updated_at: new Date(),
						};
						const me = await this._insert(ctx, { entity: entity });
						await ctx.call("alimentation.menus-dishs.saveMenuDish", {
							menu_id: me[0].id,
							meal: m.meal,
							dishs: m.dishs,
							date: new Date(m.date),
						});
					}
				}
			},
		},
		copyMenu: {
			rest: "POST /:id/copy",
			params: {
				id: { type: "number", convert: true },
				service_ids: {
					type: "array",
					items: { type: "number" },
				},
				meals: {
					type: "array",
					items: { type: "string" },
				},
				dates: {
					type: "array",
					items: { type: "string" },
				},
			},
			visibility: "published",
			scope: "alimentation:menus:create",
			async handler(ctx) {
				for (const d of ctx.params.dates) {
					for (const m of ctx.params.meals) {
						for (const s of ctx.params.service_ids) {
							const menu = await this._find(ctx, {
								query: {
									meal: m,
									date: d,
									service_id: s,
								},
							});
							if (menu.length !== 0) {
								throw new Errors.ValidationError(
									"Already exist a menu for this meal, service and date",
									"MEAL_ALREADY_EXIST",
									{},
								);
							}
						}
					}
				}
				const menu_dish = await ctx.call("alimentation.menus-dishs.find", {
					query: {
						menu_id: ctx.params.id,
					},
				});
				const menu = await await this._get(ctx, { id: ctx.params.id, withRelated: ["dishs"] });
				if (menu_dish.length !== 0) {
					let resp = [];
					for (const d of ctx.params.dates) {
						for (const m of ctx.params.meals) {
							for (const s of ctx.params.service_ids) {
								let dishs_array = [];
								for (const dish of menu[0].dishs) {
									const a = {
										dish_id: dish.dish.id,
										dishType_id: dish.type.id,
										available: dish.available,
										doses_available: dish.doses_available,
									};
									dishs_array.push(a);
								}
								const menu_copy = {
									date: new Date(d),
									meal: m,
									dishs: dishs_array,
									service_id: s,
									created_at: new Date(),
									updated_at: new Date(),
								};
								const me = await this._insert(ctx, { entity: menu_copy });
								const dish_me = await ctx.call("alimentation.menus-dishs.saveMenuDish", {
									menu_id: me[0].id,
									meal: m,
									dishs: dishs_array,
									date: new Date(d),
								});
								me[0].dishs = dish_me;
								resp.push(await this._get(ctx, { id: me[0].id, withRelated: ["dishs"] }));
							}
						}
					}
					return resp;
				}
			},
		},

		copyDay: {
			rest: "POST /copy/day",
			params: {
				from_day: { type: "string" },
				from_service_id: { type: "number" },
				service_ids: {
					type: "array",
					items: { type: "number" },
				},
				dates: {
					type: "array",
					items: { type: "string" },
				},
				meals: {
					type: "array",
					items: { type: "string" },
				},
			},
			scope: "alimentation:menus:create",
			visibility: "published",
			async handler(ctx) {
				const menu = await this._find(ctx, {
					query: {
						date: ctx.params.from_day,
						service_id: ctx.params.from_service_id,
					},
					withRelated: ["dishs"],
				});
				if (menu.length === 0) {
					throw new Errors.ValidationError("Menu not found in this date", "MENU_NOT_FOUND", {});
				}

				for (const s of ctx.params.service_ids) {
					for (const d of ctx.params.dates) {
						for (const m of ctx.params.meals) {
							const menu_exist = await this._find(ctx, {
								query: {
									service_id: s,
									date: d,
									meal: m,
								},
							});
							if (menu_exist.length !== 0) {
								throw new Errors.ValidationError(
									"Already exist menus in this dates",
									"MENU_ALREADY_EXIST_IN_THIS_DATES",
									{},
								);
							}
						}
					}
				}
				let resp = [];

				for (const s of ctx.params.service_ids) {
					for (const d of ctx.params.dates) {
						for (const m of menu) {
							let dishs_array = [];
							for (const dish of m.dishs) {
								const a = {
									dish_id: dish.dish.id,
									dishType_id: dish.type.id,
									available: dish.available,
									doses_available: dish.doses_available,
								};
								dishs_array.push(a);
							}
							const menu_copy = {
								date: new Date(d),
								meal: m.meal,
								dishs: dishs_array,
								service_id: s,
								created_at: new Date(),
								updated_at: new Date(),
							};

							const me = await this._insert(ctx, { entity: menu_copy });

							const dish_me = await ctx.call("alimentation.menus-dishs.saveMenuDish", {
								menu_id: me[0].id,
								meal: m.meal,
								dishs: dishs_array,
								date: new Date(d),
							});
							me[0].dishs = dish_me;
							resp.push(await this._get(ctx, { id: me[0].id, withRelated: ["dishs"] }));
						}
					}
				}
				return resp;
			},
		},

		copyWeek: {
			rest: "POST /copy/week",
			params: {
				from_day_of_week: { type: "string" },
				from_service_id: { type: "number" },
				service_ids: {
					type: "array",
					items: { type: "number" },
				},
				to_day_of_week: { type: "string" },
			},
			scope: "alimentation:menus:create",
			visibility: "published",
			async handler(ctx) {
				const first_day_week = moment(ctx.params.to_day_of_week).startOf("week");
				const toDaysOfWeek = [];
				for (let dow = 1; dow < 8; dow++) {
					toDaysOfWeek.push(moment(first_day_week).add(dow, "days").format("YYYY-MM-DD"));
				}
				for (const s of ctx.params.service_ids) {
					for (const days of toDaysOfWeek) {
						const formated_day = moment(days).format("YYYY-MM-DD");
						const menus = await this._find(ctx, {
							query: {
								date: formated_day,
								service_id: s,
							},
							withRelated: false,
						});
						if (menus.length !== 0) {
							throw new Errors.ValidationError(
								"Already exist menus in this dates",
								"MENU_ALREADY_EXIST_IN_THIS_DATES",
								{},
							);
						}
					}
				}
				let firstFromDayOfWeek = moment(ctx.params.from_day_of_week).startOf("week");
				let fromDaysOfWeek = [];
				for (let dow = 1; dow < 8; dow++) {
					fromDaysOfWeek.push(moment(firstFromDayOfWeek).add(dow, "days").format("YYYY-MM-DD"));
				}
				let persistedMenus = [];
				for (const days of fromDaysOfWeek) {
					const formated_day = moment(days).format("YYYY-MM-DD");
					const menu = await this._find(ctx, {
						query: {
							date: formated_day,
							service_id: ctx.params.from_service_id,
						},
						withRelated: false,
					});
					await ctx
						.call("alimentation.menus-dishs.find", {
							query: {
								menu_id: menu.map((m) => m.id),
							},
							withRelated: false,
						})
						.then((menu_dishs) => {
							menu.forEach((m) => (m.dishs = menu_dishs.filter((md) => md.menu_id === m.id)));
						});

					if (menu.length !== 0) {
						persistedMenus.push(...menu);
					}
				}
				let resp = [];
				for (const s of ctx.params.service_ids) {
					for (const m of persistedMenus) {
						let dishs_array = [];
						for (const dish of m.dishs) {
							const a = {
								dish_id: dish.dish_id,
								dishType_id: dish.type_id,
								available: dish.available,
								doses_available: dish.doses_available,
							};
							dishs_array.push(a);
						}
						const menu = {
							date: new Date(this.findEquivalentDayOfWeek(m.date, fromDaysOfWeek, toDaysOfWeek)),
							service_id: s,
							meal: m.meal,
							dishs: dishs_array,
							created_at: new Date(),
							updated_at: new Date(),
						};
						const me = await this._insert(ctx, { entity: menu });
						const dish_me = await ctx.call("alimentation.menus-dishs.saveMenuDish", {
							menu_id: me[0].id,
							meal: me[0].meal,
							dishs: dishs_array,
							date: new Date(this.findEquivalentDayOfWeek(m.date, fromDaysOfWeek, toDaysOfWeek)),
						});
						me[0].dishs = dish_me;
						resp.push(me[0]);
						//resp.push(await this._get(ctx, { id: me[0].id, withRelated: ["dishs"] }));
					}
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.menus-dishs.*"() {
			this.clearCache();
		},
		"alimentation.dishs-types.*"() {
			this.clearCache();
		},
		"alimentation.dishs.*"() {
			this.clearCache();
		},
		"alimentation.reservations.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateService(ctx) {
			await ctx.call("alimentation.services.get", { id: ctx.params.service_id });
		},

		async validateMeals(ctx) {
			const meal = await this._find(ctx, {
				query: {
					service_id: ctx.params.service_id,
					meal: ctx.params.meal,
					date: ctx.params.date,
				},
			});
			if (meal.length !== 0) {
				throw new Errors.ValidationError(
					"Already exist a menu for this meal, service and date",
					"MEAL_ALREADY_EXIST",
					{},
				);
			}
		},

		async validateDishs(ctx) {
			for (const dish of ctx.params.dishs) {
				await ctx.call("alimentation.dishs.get", { id: dish.dish_id });
			}
		},
		async validateDishsTypes(ctx) {
			for (const dish of ctx.params.dishs) {
				await ctx.call("alimentation.dishs-types.get", { id: dish.dishType_id });
			}
		},

		async saveMenuDish(ctx, res) {
			const r = await ctx.call("alimentation.menus-dishs.saveMenuDish", {
				menu_id: res[0].id,
				meal: ctx.params.meal,
				dishs: ctx.params.dishs,
				date: ctx.params.date,
			});
			res[0].dishs = r;
			return res;
		},

		async updateMenuDish(ctx, res) {
			const r = await ctx.call("alimentation.menus-dishs.updateMenuDish", {
				menu_id: res[0].id,
				meal: ctx.params.meal,
				dishs: ctx.params.dishs,
				date: ctx.params.date,
			});
			res[0].dishs = r;
			return res;
		},

		findEquivalentDayOfWeek(day, fromDates, toDates) {
			const dayStr = moment(day).format("YYYY-MM-DD");
			const indexOf = fromDates.indexOf(dayStr);
			return toDates[indexOf];
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
