"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.menus-dishs",
	table: "menu_dish",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "menus-dishs")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"available",
			"doses_available",
			"available_until",
			"nullable_until",
			"menu_id",
			"dish_id",
			"type_id",
		],
		defaultWithRelateds: [],
		withRelateds: {
			dish(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.dishs", "dish", "dish_id");
			},
			menu(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.menus", "menu", "menu_id");
			},
			type(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.dishs-types", "type", "type_id");
			},
			reservationCount(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.reservations.count", {
								query: {
									menu_dish_id: doc.id,
								},
							})
							.then((res) => (doc.reservationCount = res));
					}),
				);
			},
		},
		entityValidator: {
			available: { type: "boolean", default: true },
			doses_available: { type: "number", default: true, convert: true },
			available_until: { type: "date", optional: true, nullable: true },
			nullable_until: { type: "date", optional: true, nullable: true },
			menu_id: { type: "number", positive: true, integer: true },
			dish_id: { type: "number", positive: true, integer: true },
			type_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		// before: {
		// 	create: [
		// 		function sanatizeParams(ctx) {
		// 			ctx.params.created_at = new Date();
		// 			ctx.params.updated_at = new Date();
		// 		}
		// 	],
		// 	update: [
		// 		function sanatizeParams(ctx) {
		// 			ctx.params.updated_at = new Date();
		// 		}
		// 	]
		// }
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		changeQtdDosesAvailable: {
			rest: "PUT /:id/qtdChange",
			visibility: "published",
			params: {
				id: { type: "number", positive: true, convert: true },
				quantity: { type: "number", positive: true, convert: true, integer: true },
				operation: { type: "enum", values: ["in", "out"] },
			},
			async handler(ctx) {
				const menu_dish = await ctx.call("alimentation.menus-dishs.get", {
					id: ctx.params.id,
				});

				if (ctx.params.operation === "in") {
					return ctx.call("alimentation.menus-dishs.patch", {
						id: ctx.params.id,
						doses_available: menu_dish[0].doses_available + ctx.params.quantity,
					});
				} else {
					// GET NUMBER OF RESERVATIONS
					const reservations = await ctx.call("alimentation.reservations.count", {
						query: {
							menu_dish_id: ctx.params.id,
							has_canceled: false,
						},
					});

					// doses_available - ctx.params.quantity cant be inferior on reserved meals
					if (menu_dish[0].doses_available - ctx.params.quantity < reservations) {
						throw new Errors.ValidationError(
							"Cant decrease the number of the doses for a number lower than meals reserved",
							"FOOD_MENU_DISH_QTD_LOWER",
							{},
						);
					}

					return ctx.call("alimentation.menus-dishs.patch", {
						id: ctx.params.id,
						doses_available: menu_dish[0].doses_available - ctx.params.quantity,
					});
				}
			},
		},
		getDishInfo: {
			params: {
				menu_id: { type: "number" },
			},
			async handler(ctx) {
				let resp = [];
				const menu_dishs = await this._find(ctx, {
					query: {
						menu_id: ctx.params.menu_id,
					},
					withRelated: ["type", "reservationCount"],
				});
				for (let dish of menu_dishs) {
					const dish_info = await ctx.call("alimentation.dishs.get", { id: dish.dish_id });
					dish.dish = dish_info[0];
					resp.push(dish);
				}
				return resp;
			},
		},

		saveMenuDish: {
			params: {
				menu_id: { type: "number" },
				date: { type: "date", convert: true },
				meal: { type: "string" },
				dishs: {
					type: "array",
					items: {
						type: "object",
						props: {
							dish_id: { type: "number", positive: true, integer: false },
							dishType_id: { type: "number", positive: true, integer: false },
							available: { type: "boolean" },
							doses_available: { type: "number", convert: true, positive: false },
						},
					},
				},
			},
			scope: "alimentation:menus:create",
			async handler(ctx) {
				let resp = [];
				const menus_dishs = await this._find(ctx, { query: { menu_id: ctx.params.menu_id } });
				if (menus_dishs.length > 0) {
					for (const dish of ctx.params.dishs) {
						const exist = menus_dishs.find(
							(elem) => elem.dish_id === dish.dish_id && elem.type_id === dish.dishType_id,
						);
						if (exist) {
							let dish_index = ctx.params.dishs.indexOf(dish);
							ctx.params.dishs.slice(dish_index, 1);
							resp.push(dish);
						} else {
							const availableNullableUntil = await ctx.call(
								"alimentation.dishs-types.availableNullableUntil",
								{
									dish_type_id: dish.dishType_id,
									meal: ctx.params.meal,
									date: ctx.params.date,
								},
							);
							const entities = {
								available: dish.available,
								doses_available: dish.doses_available,
								dish_id: dish.dish_id,
								type_id: dish.dishType_id,
								menu_id: ctx.params.menu_id,
								available_until: availableNullableUntil.available_until,
								nullable_until: availableNullableUntil.nullable_until,
							};
							const menu_dish = await this._insert(ctx, { entity: entities });
							resp.push(menu_dish[0]);
						}
					}
				} else {
					for (const dish of ctx.params.dishs) {
						const availableNullableUntil = await ctx.call(
							"alimentation.dishs-types.availableNullableUntil",
							{
								dish_type_id: dish.dishType_id,
								meal: ctx.params.meal,
								date: ctx.params.date,
							},
						);
						const entities = {
							available: dish.available,
							doses_available: dish.doses_available,
							dish_id: dish.dish_id,
							type_id: dish.dishType_id,
							menu_id: ctx.params.menu_id,
							available_until: availableNullableUntil.available_until,
							nullable_until: availableNullableUntil.nullable_until,
						};

						const menu_dish = await this._insert(ctx, { entity: entities });
						resp.push(menu_dish[0]);
					}
				}

				return resp;
			},
		},
		updateMenuDish: {
			params: {
				menu_id: { type: "number" },
				date: { type: "date", convert: true },
				meal: { type: "string" },
				dishs: {
					type: "array",
					items: {
						type: "object",
						props: {
							dish_id: { type: "number", positive: true, integer: false },
							dishType_id: { type: "number", positive: true, integer: false },
							available: { type: "boolean" },
							doses_available: { type: "number", convert: true },
						},
					},
				},
			},
			async handler(ctx) {
				let resp = [];
				const menu_dishs = await this._find(ctx, {
					query: {
						menu_id: ctx.params.menu_id,
					},
				});
				for (const dish of ctx.params.dishs) {
					const menu_dish = await this._find(ctx, {
						query: {
							menu_id: ctx.params.menu_id,
							dish_id: dish.dish_id,
							type_id: dish.dishType_id,
						},
					});
					const availableNullableUntil = await ctx.call(
						"alimentation.dishs-types.availableNullableUntil",
						{
							dish_type_id: dish.dishType_id,
							meal: ctx.params.meal,
							date: ctx.params.date,
						},
					);
					if (menu_dish.length > 0) {
						menu_dish[0].available = dish.available;
						menu_dish[0].doses_available = dish.doses_available;
						menu_dish[0].dish_id = dish.dish_id;
						menu_dish[0].type_id = dish.dishType_id;
						menu_dish[0].available_until = availableNullableUntil.available_until;
						menu_dish[0].nullable_until = availableNullableUntil.nullable_until;
						const menu_dishs = await ctx.call("alimentation.menus-dishs.update", menu_dish[0]);
						resp.push(menu_dishs[0]);
					} else {
						const menu_dish_updated = {
							available: dish.available,
							doses_available: dish.doses_available,
							dish_id: dish.dish_id,
							type_id: dish.dishType_id,
							menu_id: ctx.params.menu_id,
							available_until: availableNullableUntil.available_until,
							nullable_until: availableNullableUntil.nullable_until,
						};
						const menu_dish = await this._insert(ctx, { entity: menu_dish_updated });
						resp.push(menu_dish[0]);
					}
				}
				await this.remove(ctx, menu_dishs, resp);
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.dishs.*"() {
			this.clearCache();
		},
		"alimentation.dishs-types.*"() {
			this.clearCache();
		},
		"alimentation.prices-variations.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async remove(ctx, menu_dishs, list_updated) {
			let menu_dishs_remove = [];
			for (const menu_dish of menu_dishs) {
				const exist = list_updated.filter(
					(ele) => ele.type_id === menu_dish.type_id && ele.dish_id === menu_dish.dish_id,
				);
				if (exist.length === 0) {
					menu_dishs_remove.push(menu_dish);
				}
			}
			for (const menu_dish_remove of menu_dishs_remove) {
				const reservations = await ctx.call("alimentation.reservations.count", {
					query: {
						menu_dish_id: menu_dish_remove.id,
					},
				});
				if (reservations > 0) {
					menu_dish_remove.available = false;
					await ctx.call("alimentation.menus-dishs.update", menu_dish_remove);
				} else {
					await ctx.call("alimentation.menus-dishs.remove", { id: menu_dish_remove.id });
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
