"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.nutrients",
	table: "nutrient",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "nutrients")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "active", "ddr", "unit_id", "created_at", "updated_at"],
		defaultWithRelateds: ["unit", "translations"],
		withRelateds: {
			unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.units", "unit", "unit_id");
			},
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.nutrients-translations",
					"translations",
					"id",
					"nutrient_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", integer: true, positive: true },
						name: { type: "string" },
					},
				},
			},
			active: { type: "boolean", default: true },
			ddr: { type: "number", convert: true },
			unit_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name"],
						"alimentation.nutrients-translations",
						"nutrient_id",
					);
				},
			],
			create: [
				"validateUnitId",
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateUnitId",
				"validateLanguageIds",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			//CONFIRMAR O QUE FAZER AQUI
			// remove: [
			// 	"remove_translations"
			// ]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Units
		 */
		async validateUnitId(ctx) {
			await ctx.call("alimentation.units.get", { id: ctx.params.unit_id });
		},

		/**
		 * Method for validate Language
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		/**
		 * Method for save Nutrients Translations
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.nutrients-translations.save_translations", {
				nutrient_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.nutrients-translations.find", {
				query: {
					nutrient_id: res[0].id,
				},
			});

			return res;
		},

		/**
		 * Method for remove Nutrients Translations
		 */
		async remove_translations(ctx) {
			return await ctx.call("alimentation.nutrients-translations.remove_translations", {
				nutrient_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
