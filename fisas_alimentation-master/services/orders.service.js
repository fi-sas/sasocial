"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addEntity, addQueryFentity } = require("./helpers/entity.guard.js");
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const Stream = require("stream");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.orders",
	table: "order",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "orders")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_id",
			"served_at",
			"served_by_user_id",
			"fentity_id",
			"movement_id",
			"created_at",
			"service_id",
			"status",
			"total_price",
			"updated_at",
		],
		defaultWithRelateds: ["order_lines"],
		withRelateds: {
			order_lines(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.orders-lines", "order_lines", "id", "order_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			user_served(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user_served", "served_by_user_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			served_at: { type: "date", convert: true, optional: true, nullable: true },
			served_by_user_id: { type: "number", optional: true, positive: true, integer: true },
			service_id: { type: "number", convert: true, optional: true, nullable: true },
			movement_id: { type: "string", nullable: true },
			total_price: { type: "number", convert: true },
			status: { type: "enum", values: ["SERVED", "CANCELED", "WAITING", "NOT_SERVED"] },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.is_wait = false;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [addQueryFentity],
			revenueReport: [addEntity],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},

		getproductsLastMounth: {
			params: {
				user_id: { type: "number" },
				service_id: { type: "number" },
				limit: { type: "number", optional: true, default: 10 },
			},
			async handler(ctx) {
				if (ctx.meta.isGuest) {
					return [];
				}

				const today = new Date();
				const date_last_mounth = new Date();
				date_last_mounth.setMonth(date_last_mounth.getMonth() - 1);

				const getLastMonthOrders = await this.adapter
					.db("order")
					.select("id")
					.whereBetween("created_at", [date_last_mounth, today])
					.andWhere("user_id", ctx.params.user_id)
					.andWhere("service_id", ctx.params.service_id);

				const getLastMonthOrdersLines = await this.adapter
					.db("order_line")
					.distinctOn("product_id")
					.andWhere(
						"order_id",
						"in",
						getLastMonthOrders.map((o) => o.id),
					)
					.limit(ctx.params.limit);

				return getLastMonthOrdersLines.length > 0
					? getLastMonthOrdersLines.map((ol) => ol.product_id)
					: [];
			},
		},

		create_order: {
			visibility: "published",
			async handler(ctx) {
				this.logger.info("entrada");
				this.logger.info(ctx.params);
				const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "BAR");

				let list_order_lines = [];
				if (filtered_items.length > 0) {
					const sum = this.calculate_order_price(filtered_items);
					const order_entity = {
						user_id: filtered_items[0].extra_info.user_id,
						fentity_id: filtered_items[0].extra_info.fentity_id,
						service_id: filtered_items[0].extra_info.service_id,
						movement_id: filtered_items[0].movement_id,
						total_price: sum,
						status: "NOT_SERVED",
						created_at: new Date(),
						updated_at: new Date(),
					};
					const order = await this._create(ctx, order_entity);

					ctx.meta.alimentationBypassEntity = true;
					const service = await ctx.call("alimentation.services.get", {
						id: order[0].service_id,
						withRelated: false,
					});

					for (const item of filtered_items) {
						const stock = await ctx.call("alimentation.stocks.find", {
							query: {
								wharehouse_id: service[0].wharehouse_id,
								product_id: item.extra_info.product_id,
							},
							withRelated: false,
							sort: "created_at",
						});

						const orders_lines_entity = {
							quantity: item.quantity,
							order_id: order[0].id,
							product_id: item.extra_info.product_id,
							price: item.total_value,
							status: "NOT_SERVED",
							item_id: item.id,
							stock_id: stock.length > 0 ? stock[0].id : null,
						};

						await ctx
							.call("alimentation.orders-lines.create", orders_lines_entity)
							.then((order_created) => {
								list_order_lines.push(order_created);

								this.remove_stock_order_line(
									ctx,
									item.extra_info.product_id,
									service[0].wharehouse_id,
									item.quantity,
									order[0].id,
									order_created[0].id,
								);
							});
					}
				}
				return list_order_lines.length === filtered_items.length ? true : false;
			},
		},

		cancel_order: {
			visibility: "published",
			async handler(ctx) {
				const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "BAR");
				const updated_orders = [];
				for (const item of filtered_items) {
					const orders = await this._find(ctx, {
						query: {
							movement_id: item.movement_id,
							status: "NOT_SERVED",
						},
					});
					if (orders) {
						if (orders.length === 0) {
							return false;
						}
						if (orders[0].status === "SERVED") {
							throw new Errors.ValidationError("The order has served", "FOOD_ORDER_HAS_SERVED", {});
						} else {
							orders[0].status = "CANCELED";
							for (const order_line of orders[0].order_lines) {
								await ctx.call("alimentation.orders-lines.in_stock", {
									order_id: order_line.order_id,
									order_line_id: order_line.id,
								});
							}
							await this._update(ctx, orders[0])
								.then((order_update) => {
									updated_orders.push(order_update);
								})
								.catch(() => {
									return false;
								});
						}
					}
				}
				return updated_orders.length === filtered_items.length ? true : false;
			},
		},

		order_serve_user: {
			rest: "POST /:order_id/serve",
			visibility: "published",
			params: {
				order_id: { type: "number", integer: true, convert: true },
			},
			scope: "alimentation:orders:status",
			async handler(ctx) {
				// set order_line served
				const order = await this._get(ctx, { id: ctx.params.order_id });
				if (order[0].status === "CANCELED") {
					throw new Errors.ValidationError("This order has canceled", "ORDERS_SERVE_ERROR", {});
				} else {
					await ctx.call("alimentation.orders-lines.set_all_served", {
						order_id: ctx.params.order_id,
					});
					// set order served
					return ctx.call("alimentation.orders.patch", {
						id: ctx.params.order_id,
						status: "SERVED",
						served_at: new Date(),
						served_by_user_id: ctx.meta.user.id,
					});
				}
			},
		},

		reverte_served: {
			rest: "POST /:order_id/not_served",
			visibility: "published",
			params: {
				order_id: { type: "number", integer: true, convert: true },
			},
			scope: "alimentation:orders:status",
			async handler(ctx) {
				// set order in waiting
				const order = await this._get(ctx, { id: ctx.params.order_id });
				if (order[0].status === "NOT_SERVED") {
					throw new Errors.ValidationError(
						"This order has not served",
						"ORDERS_HAS_NOT_SERVED_ERROR",
						{},
					);
				} else {
					return ctx
						.call("alimentation.orders.patch", {
							id: ctx.params.order_id,
							status: "NOT_SERVED",
						})
						.then(() => {
							return ctx.call("alimentation.orders-lines.set_all_not_served", {
								order_id: ctx.params.order_id,
							});
						});
				}
			},
		},

		order_waiting: {
			rest: "POST /:order_id/waiting",
			visibility: "published",
			params: {
				order_id: { type: "number", integer: true, convert: true },
			},
			scope: "alimentation:orders:status",
			async handler(ctx) {
				// set order in waiting
				const order = await this._get(ctx, { id: ctx.params.order_id });
				if (order[0].status === "CANCELED") {
					throw new Errors.ValidationError(
						"This order has canceled",
						"ORDERS_HAS_CANCELED_ERROR",
						{},
					);
				}
				if (order[0].status === "SERVED") {
					throw new Errors.ValidationError("This order has served", "ORDERS_HAS_SERVED_ERROR", {});
				} else {
					return ctx.call("alimentation.orders.patch", {
						id: ctx.params.order_id,
						status: "WAITING",
					});
				}
			},
		},

		cancel_order_user: {
			rest: "POST /:order_id/cancel",
			visibility: "published",
			params: {
				order_id: { type: "number", integer: true, convert: true },
			},
			scope: "alimentation:orders:status",
			async handler(ctx) {
				const order = await this._get(ctx, { id: ctx.params.order_id });
				if (order[0].status === "SERVED") {
					throw new Errors.ValidationError(
						"Unable to cancel the order has been served",
						"ALIMENTATION_ORDER_HAS_SERVED",
						{},
					);
				} else {
					const order_lines = order[0].order_lines.filter((ol) => ol.status === "NOT_SERVED");
					const order_line_ids = order_lines.map((ol) => ol.item_id);
					await ctx
						.call("current_account.movements.cancel", {
							movement_id: order[0].movement_id,
							items: order_line_ids,
						})
						.catch((err) => {
							this.logger.info("ALIMENTATION - ERROR - Cancel movement from order", err);
							throw new Errors.ValidationError(
								"Error in cancel order",
								"ORDERS_CANCEL_MOVEMENT_ERROR",
								{},
							);
						});

					order[0].status = "CANCELED";
					// set order to deleted
					const order_response = await this._update(ctx, order[0]);

					for (const order_line of order_lines) {
						await ctx.call("alimentation.orders-lines.patch", {
							id: order_line.id,
							status: "DELETED",
						});
						await ctx.call("alimentation.orders-lines.in_stock", {
							order_id: order_line.order_id,
							order_line_id: order_line.id,
						});
					}

					return order_response;
				}
			},
		},

		revenueReport: {
			rest: "GET /revenueReport",
			scope: "alimentation:orders:read",
			params: {
				service_id: {
					type: "array",
					items: { type: "number", convert: true, min: 0 },
					convert: true,
					optional: true,
				},
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const bindings = [];
				bindings.push(ctx.params.start_date);
				bindings.push(ctx.params.end_date);

				const fentityId = this.getFiscalEntity(ctx);
				if (ctx.params.service_id) {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								id: ctx.params.service_id,
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				} else {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				}
				bindings.push(...ctx.params.service_id);

				const rawResult = await this.adapter.raw(
					`SELECT s.name as service_name, ROUND(SUM(ol.price)::numeric, 2) as total FROM "order" as o
						INNER JOIN order_line as ol on ol.order_id = o.id
						INNER JOIN service as s on s.id = o.service_id
					WHERE
						ol.status != 'DELETED'
						and o.created_at::date >= ? and o.created_at::date <= ?
						${
							ctx.params.service_id
								? "and o.service_id in (" + ctx.params.service_id.map((_) => "?").join(",") + ") "
								: ""
						}
					GROUP BY s.name
					ORDER BY s.name`,
					bindings,
				);

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "FOOD_ORDERS_REVENUE_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							rows: rawResult.rows,
							start_date: moment(ctx.params.start_date).format("YYYY-MM-DD HH:mm:ss"),
							end_date: moment(ctx.params.end_date).format("YYYY-MM-DD HH:mm:ss"),
							created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
							user: {
								name: ctx.meta.user.name,
							},
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},

		revenueByProductReport: {
			rest: "GET /revenueByProductReport",
			scope: "alimentation:orders:read",
			params: {
				service_id: {
					type: "array",
					items: { type: "number", convert: true, min: 0 },
					convert: true,
					optional: true,
				},
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const bindings = [];
				bindings.push(ctx.params.start_date);
				bindings.push(ctx.params.end_date);

				const fentityId = this.getFiscalEntity(ctx);
				if (ctx.params.service_id) {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								id: ctx.params.service_id,
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				} else {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				}
				bindings.push(...ctx.params.service_id);

				const rawResult = await this.adapter.raw(
					`SELECT s.name as service_name, ol.product_id, pt.name, ROUND(SUM(ol.quantity), 2) as quantity, ROUND(SUM(ol.price)::numeric, 2) as total FROM "order" as o
				INNER JOIN service as s on o.service_id = s.id
				INNER JOIN order_line as ol on ol.order_id = o.id
				INNER JOIN product as p on p.id = ol.product_id
				INNER JOIN product_translation as pt on pt.product_id = p.id and pt.language_id = 3
				WHERE
					ol.status = 'SERVED'
					and o.created_at::date >= ? and o.created_at::date <= ?
					${
						ctx.params.service_id
							? "and o.service_id in (" + ctx.params.service_id.map((_) => "?").join(",") + ") "
							: ""
					}
				GROUP BY s.name, ol.product_id, pt.name
				ORDER BY pt.name
				`,
					bindings,
				);

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "FOOD_ORDERS_REVENUE_PRODUCT_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							products: rawResult.rows,
							start_date: moment(ctx.params.start_date).format("YYYY-MM-DD HH:mm:ss"),
							end_date: moment(ctx.params.end_date).format("YYYY-MM-DD HH:mm:ss"),
							created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
							service: null,
							user: {
								name: ctx.meta.user.name,
							},
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.orders-lines.*"() {
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/***
		 * getFiscalEntity
		 * Returns the fiscal entity id send in the header
		 * @params ctx moleculer context
		 * @returns number id of fiscal entity
		 */
		getFiscalEntity(ctx) {
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError(
					"Need send fiscal entity Id",
					"NEED_SEND_FISCAL_ENTITY_ID",
					{},
				);
			}

			return ctx.meta.alimentation_entity_id;
		},
		calculate_order_price(list) {
			let sum = 0;
			for (const product of list) {
				sum += product.total_value;
			}
			return sum;
		},

		// remove stock from product
		async remove_stock_order_line(
			ctx,
			product_id,
			wharehouse_id,
			quantity,
			order_id,
			order_line_id,
		) {
			const product = await ctx.call("alimentation.products.get", {
				id: product_id,
				withRelated: ["composition"],
			});

			// "none", "product", "composition", "product_composition"
			if (product[0].track_stock === "none") {
				return null;
			} else {
				if (product[0].track_stock === "product") {
					let stock = await ctx.call("alimentation.stocks.find", {
						query: {
							wharehouse_id: wharehouse_id,
							product_id: product_id,
							quantity: {
								gt: 0,
							},
						},
						withRelated: false,
						sort: "created_at",
					});

					if (stock.length > 0) {
						let quantity_remover = quantity;
						while (quantity_remover > 0) {
							let stock_remove = stock.find((element) => element.quantity > 0);
							if (quantity_remover <= stock_remove.quantity) {
								await ctx.call("alimentation.stocks.remove_stock", {
									id: stock_remove.id,
									quantity: quantity_remover,
									order_id: order_id,
									order_line_id,
								});
								return stock_remove.id;
							} else {
								quantity_remover = quantity_remover - stock_remove.quantity;
								await ctx.call("alimentation.stocks.remove_stock", {
									id: stock_remove.id,
									quantity: stock_remove.quantity,
									order_id: order_id,
									order_line_id,
								});
								stock = stock.filter((element) => element.id != stock_remove.id);
							}
						}
					} else {
						return null;
					}
				}

				if (product[0].track_stock === "composition") {
					for (let row of product[0].composition) {
						await this.remove_stock_order_line(
							ctx,
							row.compound.id,
							wharehouse_id,
							row.quantity * quantity,
							order_id,
							order_line_id,
						);
					}
				}

				if (product[0].track_stock === "product_composition") {
					let stock = await ctx.call("alimentation.stocks.find", {
						query: {
							wharehouse_id: wharehouse_id,
							product_id: product_id,
							quantity: {
								gt: 0,
							},
						},
						withRelated: false,
						sort: "created_at",
					});

					if (stock.length > 0) {
						let quantity_remover = quantity;
						while (quantity_remover > 0) {
							let stock_remove = stock.find((element) => element.quantity > 0);
							if (quantity_remover <= stock_remove.quantity) {
								await ctx.call("alimentation.stocks.remove_stock", {
									id: stock_remove.id,
									quantity: quantity_remover,
									order_id: order_id,
									order_line_id,
								});
								return stock_remove.id;
							} else {
								quantity_remover = quantity_remover - stock_remove.quantity;
								await ctx.call("alimentation.stocks.remove_stock", {
									id: stock_remove.id,
									quantity: stock_remove.quantity,
									order_id: order_id,
									order_line_id,
								});
								stock = stock.filter((element) => element.id != stock_remove.id);
							}
						}
					} else {
						return null;
					}

					for (let row of product[0].composition) {
						await this.remove_stock_order_line(
							ctx,
							row.compound.id,
							wharehouse_id,
							row.quantity * quantity,
							order_id,
							order_line_id,
						);
					}
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
