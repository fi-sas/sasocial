"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.orders-lines",
	table: "order_line",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "orders-lines")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "quantity", "order_id", "product_id", "status", "item_id", "price", "stock_id"],
		defaultWithRelateds: ["product", "stock"],
		withRelateds: {
			product(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.products", "product", "product_id");
			},
			order(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.orders", "order", "order_id");
			},
		},
		entityValidator: {
			quantity: { type: "number", positive: true },
			order_id: { type: "number", positive: true, integer: true },
			product_id: { type: "number", positive: true, integer: true },
			item_id: { type: "string" },
			status: { type: "enum", values: ["SERVED", "DELETED", "NOT_SERVED"] },
			price: { type: "number", convert: true },
			stock_id: { type: "number", convert: true, optional: true, nullable: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		order_line_serve: {
			rest: "POST /:order_line_id/serve",
			visibility: "published",
			scope: "alimentation:orders:status",
			params: { order_line_id: { type: "number", integer: true, convert: true } },
			async handler(ctx) {
				const order_line = await this._get(ctx, {
					id: ctx.params.order_line_id,
					withRelated: ["order"],
				});

				const order = await this._find(ctx, {
					query: {
						order_id: order_line[0].order_id,
						status: "NOT_SERVED",
					},
				});

				if (order_line[0].order.status === "CANCELED") {
					throw new Errors.ValidationError(
						"Error in remove item from order, order has canceled.",
						"ORDERS_LINE_REMOVE_ITEM_ERROR",
						{},
					);
				}
				if (order_line[0].status === "DELETED") {
					throw new Errors.ValidationError(
						"This order-line has removed",
						"ORDER_LINE_SERVE_ERROR",
						{},
					);
				}
				if (order_line[0].status === "SERVED") {
					throw new Errors.ValidationError(
						"This order-line has served",
						"ORDER_LINE_SERVE_ERROR",
						{},
					);
				} else {
					if (order_line[0].stock_id !== null) {
						await ctx.call("alimentation.stocks.remove_stock", {
							id: order_line[0].stock_id,
							quantity: order_line[0].quantity,
							order_id: order_line[0].order_id,
							order_line_id: ctx.params.order_line_id,
						});
					}
					if (order.length == 1) {
						ctx.call("alimentation.orders.patch", {
							id: order_line[0].order_id,
							status: "SERVED",
							served_at: new Date(),
							served_by_user_id: ctx.meta.user.id,
						});
					}
					return ctx.call("alimentation.orders-lines.patch", {
						id: ctx.params.order_line_id,
						status: "SERVED",
					});
				}
			},
		},

		order_line_remove: {
			rest: "POST /:order_line_id/remove",
			visibility: "published",
			params: { order_line_id: { type: "number", integer: true, convert: true } },
			scope: "alimentation:orders:delete",
			async handler(ctx) {
				const order_line = await this._get(ctx, {
					id: ctx.params.order_line_id,
					withRelated: ["order"],
				});
				const order_not_served = await this._find(ctx, {
					query: {
						order_id: order_line[0].order_id,
						status: "NOT_SERVED",
					},
				});

				const order_served = await this._find(ctx, {
					query: {
						order_id: order_line[0].order_id,
						status: "SERVED",
					},
				});

				if (order_line[0].order.status === "CANCELED") {
					throw new Errors.ValidationError(
						"Error in remove item from order, order has canceled.",
						"ORDERS_LINE_REMOVE_ITEM_ERROR",
						{},
					);
				}
				if (order_line[0].status === "SERVED") {
					throw new Errors.ValidationError(
						"Error in remove item from order, item has served.",
						"ORDERS_LINE_REMOVE_ITEM_ERROR",
						{},
					);
				}
				if (order_line[0].status === "DELETED") {
					throw new Errors.ValidationError(
						"Error in remove item from order, item has removed.",
						"ORDERS_LINE_REMOVE_ITEM_ERROR",
						{},
					);
				} else {
					return ctx
						.call("current_account.movements.cancel", {
							movement_id: order_line[0].order.movement_id,
							movement_item_id: order_line[0].item_id,
						})
						.then(async () => {
							order_line[0].status = "DELETED";
							if (order_not_served.length === 1) {
								if (order_served.length === 0) {
									ctx.call("alimentation.orders.patch", {
										id: order_line[0].order_id,
										status: "CANCELED",
									});
								} else {
									ctx.call("alimentation.orders.patch", {
										id: order_line[0].order_id,
										status: "SERVED",
									});
								}
							}
							await this.revert_stock_operation(ctx, order_line[0].order_id, order_line[0].id);
							return this._update(ctx, order_line[0]);
						})
						.catch((err) => {
							this.logger.info("ALIMENTATION - ERROR - Cancel movement from order line", err);
							throw new Errors.ValidationError(
								"Error in remove item from order, cancel movement error.",
								"ORDERS_LINE_CANCEL_MOVEMENT_ERROR",
								{},
							);
						});
				}
			},
		},

		// set order_lines served and update stocks
		set_all_served: {
			params: { order_id: { type: "number", integer: true, convert: true } },
			async handler(ctx) {
				const orders_line = await this._find(ctx, { query: { order_id: ctx.params.order_id } });
				for (const order of orders_line) {
					if (order.status === "NOT_SERVED") {
						order.status = "SERVED";
						await this._update(ctx, order);
					}
				}
			},
		},
		// set order_lines served and update stocks
		set_all_not_served: {
			params: { order_id: { type: "number", integer: true, convert: true } },
			async handler(ctx) {
				const orders_line = await this._find(ctx, { query: { order_id: ctx.params.order_id } });
				for (const order of orders_line) {
					order.status = "NOT_SERVED";
					await this._update(ctx, order);
				}
			},
		},
		//Return stock action
		in_stock: {
			params: {
				order_id: { type: "number", integer: true, convert: true },
				order_line_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				await this.revert_stock_operation(ctx, ctx.params.order_id, ctx.params.order_line_id);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		//Return stock action
		async revert_stock_operation(ctx, order_id, order_line_id) {
			const stock_operation = await ctx.call("alimentation.stocks-operations.find", {
				query: {
					order_id: order_id,
					order_line_id: order_line_id,
				},
				withRelated: ["stock"],
			});

			for (const operation of stock_operation) {
				await ctx.call("alimentation.stocks.updateQuantity", {
					id: operation.stock_id,
					operation: "in",
					quantity: operation.quantity,
					stock_reason: "Venda cancelada",
				});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
