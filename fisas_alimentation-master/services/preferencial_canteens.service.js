"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.preferencial_canteens",
	table: "preferencial_canteen",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "preferencial_canteens")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "canteen_lunch_id", "canteen_dinner_id"],
		defaultWithRelateds: ["lunch_canteen", "dinner_canteen"],
		withRelateds: {
			lunch_canteen(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.services", "lunch_canteen", "canteen_lunch_id");
			},
			dinner_canteen(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.services", "dinner_canteen", "canteen_dinner_id");
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true, convert: true },
			canteen_lunch_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
				convert: true,
			},
			canteen_dinner_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
				convert: true,
			},
		},
	},
	hooks: {
		before: {
			create: ["validations"],
			update: ["validations"],
			patch: ["validation_patch"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		save_cantine_user: {
			params: {
				user_id: { type: "number", integer: true, convert: true },
				canteen_lunch_id: { type: "number", integer: true, convert: true, optional: true },
				canteen_dinner_id: { type: "number", integer: true, convert: true, optional: true },
			},
			async handler(ctx) {
				await this.validations(ctx);
				await this.adapter.removeMany({
					user_id: ctx.params.user_id,
				});
				return await this._create(ctx, ctx.params);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validations(ctx) {
			const user = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
			if (user[0].active !== true) {
				throw new Errors.ValidationError("User not active", "USER_NOT_ACTIVE", {});
			}
			if (!ctx.params.canteen_lunch_id && !ctx.params.canteen_dinner_id) {
				throw new Errors.ValidationError(
					"Send canteen for lunch or canteen for dinner",
					"SEND_CANTEEN",
					{},
				);
			}
			if (ctx.params.canteen_lunch_id === null && !ctx.params.canteen_dinner_id === null) {
				throw new Errors.ValidationError(
					"Send canteen for lunch or canteen for dinner",
					"SEND_CANTEEN",
					{},
				);
			}
			if (ctx.params.canteen_lunch_id) {
				const canteen_lunch = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_lunch_id,
				});
				if (canteen_lunch[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
			if (ctx.params.canteen_dinner_id) {
				const canteen_dinnner = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_dinner_id,
				});
				if (canteen_dinnner[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
		},

		async validation_patch(ctx) {
			if (ctx.params.user_id) {
				const user = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
				if (user[0].active !== true) {
					throw new Errors.ValidationError("User not active", "USER_NOT_ACTIVE", {});
				}
			}
			if (ctx.params.canteen_lunch_id) {
				const canteen_lunch = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_lunch_id,
				});
				if (canteen_lunch[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
			if (ctx.params.canteen_dinner_id) {
				const canteen_dinnner = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_dinner_id,
				});
				if (canteen_dinnner[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
