"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.prices-variations",
	table: "price_variation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "prices-variations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"active",
			"description",
			"tax_id",
			"profile_id",
			"meal",
			"time",
			"price",
			"product_id",
			"dish_type_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			profile(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.profiles", "profile", "profile_id");
			},
			tax(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "tax", "tax_id");
			},
		},
		entityValidator: {
			active: { type: "boolean", default: true },
			description: { type: "string", optional: true, nullable: true },
			tax_id: { type: "number", positive: true, integer: false },
			price: { type: "number", positive: true, integer: false },
			profile_id: { type: "number", positive: true, integer: false },
			meal: { type: "string", optional: true, nullable: true },
			time: { type: "string", default: "00:00:00" },
			product_id: { type: "number", optional: true, nullable: true },
			dish_type_id: { type: "number", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		savePriceVariation: {
			params: {
				dish_type_id: { type: "number", positive: true, integer: true },
				prices: {
					type: "array",
					items: {
						type: "object",
						props: {
							description: { type: "string", optional: true, nullable: true },
							tax_id: { type: "number", positive: true, integer: false },
							profile_id: { type: "number", positive: true, integer: false },
							price: { type: "number", positive: true, integer: false },
							meal: {
								type: "string",
								values: ["lunch", "dinner", "breakfast"],
								optional: true,
								nullable: true,
							},
							time: { type: "string", default: "00:00:00" },
						},
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					dish_type_id: ctx.params.dish_type_id,
				});
				this.clearCache();
				ctx.emit("alimentation.prices-variations.deleted");

				for (let price of ctx.params.prices) {
					price.created_at = new Date();
					price.updated_at = new Date();
					price.dish_type_id = ctx.params.dish_type_id;
					const insert = await this._create(ctx, price);
					resp.push(insert[0]);
				}
				return resp;
			},
		},
		removePriceVariation: {
			params: {
				dish_type_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					dish_type_id: ctx.params.dish_type_id,
				});
				return true;
			},
		},

		savePriceVariationProducts: {
			params: {
				product_id: { type: "number", positive: true, integer: true },
				prices: {
					optional: true,
					type: "array",
					items: {
						type: "object",
						props: {
							description: { type: "string", optional: true, nullable: true },
							tax_id: { type: "number", positive: true, integer: false },
							profile_id: { type: "number", positive: true, integer: false },
							price: { type: "number", positive: true, integer: false },
							meal: { type: "string", optional: true, nullable: true },
							time: { type: "string", default: "00:00:00" },
						},
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				this.clearCache();
				if (ctx.params.prices) {
					for (let price of ctx.params.prices) {
						price.created_at = new Date();
						price.updated_at = new Date();
						price.product_id = ctx.params.product_id;
						const insert = await this._create(ctx, price);
						resp.push(insert[0]);
					}
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
