"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const Validator = require("fastest-validator");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.products",
	table: "product",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "products")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"active",
			"file_id",
			"code",
			"cost_price",
			"tax_cost_price_id",
			"price",
			"tax_price_id",
			"available",
			"visible",
			"show_derivatives",
			"minimal_stock",
			"track_stock",
			"type",
			"unit_id",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["translations", "unit"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.products-translations",
					"translations",
					"id",
					"product_id",
				);
			},
			complements(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.products-complements.getComplementsByProduct", { id: product.id })
							.then((res) => (product.complements = res));
					}),
				);
			},
			disponibility(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.disponibilities.find", {
								query: {
									product_id: doc.id,
								},
							})
							.then((res) => (doc.disponibility = res[0]));
					}),
				);
			},
			families(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.families-products.getFamiliesByProduct", { id: product.id })
							.then((res) => (product.families = res));
					}),
				);
			},
			allergens(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.products-allergens.getAllergensByProduct", { id: product.id })
							.then((res) => (product.allergens = res));
					}),
				);
			},
			nutrients(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.products-nutrients.getNutrientsByProduct", { id: product.id })
							.then((res) => (product.nutrients = res));
					}),
				);
			},
			prices(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.prices-variations", "prices", "id", "product_id");
			},
			unit(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.units", "unit", "unit_id");
			},
			stocks(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.stocks.find", {
								query: {
									product_id: product.id,
								},
								withRelated: false,
							})
							.then((res) => (product.stocks = res[0]));
					}),
				);
			},
			files(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx.call("media.files.get", { id: doc.file_id }).then((file) => {
							doc.file = file;
						});
					}),
				);
			},
			composition(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.compounds.getCompoundsByProduct", { id: product.id })
							.then((res) => (product.composition = res));
					}),
				);
			},
			derivatives(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((product) => {
						return ctx
							.call("alimentation.compounds.getDerivativesByProduct", { id: product.id })
							.then((res) => (product.derivatives = res));
					}),
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				items: {
					type: "object",
					props: {
						language_id: { type: "number", min: 1, integer: true, positive: true },
						name: { type: "string" },
						description: { type: "string", optional: true },
					},
				},
			},
			allergens: {
				optional: true,
				type: "array",
				items: { type: "number" },
			},
			complements: {
				optional: true,
				type: "array",
				items: { type: "number" },
			},
			composition: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						compound_id: { type: "number", integer: true, positive: true },
						quantity: { type: "number" },
					},
				},
			},
			nutrients: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						nutrient_id: { type: "number", integer: true, positive: true },
						quantity: { type: "number" },
					},
				},
			},
			prices: {
				optional: true,
				type: "array",
				items: {
					type: "object",
					props: {
						description: { type: "string", optional: true, nullable: true },
						tax_id: { type: "number", positive: true, integer: false },
						profile_id: { type: "number", positive: true, integer: false },
						price: { type: "number", positive: true, integer: false },
						meal: { type: "string", optional: true, nullable: true },
						time: { type: "string", default: "00:00:00" },
					},
				},
			},
			disponibility: {
				type: "object",
				props: {
					begin_date: { type: "date", optional: true, nullable: true, convert: true },
					end_date: { type: "date", optional: true, nullable: true, convert: true },
					monday: { type: "boolean", default: true },
					tuesday: { type: "boolean", default: true },
					wednesday: { type: "boolean", default: true },
					thursday: { type: "boolean", default: true },
					friday: { type: "boolean", default: true },
					saturday: { type: "boolean", default: true },
					sunday: { type: "boolean", default: true },
					annulment_maximum_hour: { type: "string", optional: true, nullable: true, convert: true },
					minimum_hour: { type: "string", optional: true, nullable: true, convert: true },
					maximum_hour: { type: "string", optional: true, nullable: true, convert: true },
				},
			},
			families: {
				optional: true,
				type: "array",
				items: {
					type: "number",
					positive: true,
					convert: true,
				},
			},
			active: { type: "boolean", default: true },
			file_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			code: { type: "string", max: 55 },
			cost_price: { type: "number", positive: true, integer: false },
			tax_cost_price_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
			price: { type: "number", positive: true, integer: false, nullable: true, optional: true },
			tax_price_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
			avaliable: { type: "boolean", default: true },
			visible: { type: "boolean", default: true },
			show_derivatives: { type: "boolean", default: false },
			minimal_stock: { type: "number", integer: false },
			track_stock: {
				type: "string",
				values: ["none", "product", "composition", "product_composition"],
				default: "product",
			},
			type: { type: "string", values: ["bar", "canteen"], default: "bar" },
			unit_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"validationCode",
				"validationsUnits",
				"validateCompounds",
				"validateNutrients",
				"validateAllergens",
				"validateComplements",
				"validateLanguageIds",
				"validatePrices",
				"validateFamilies",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateCodeUpdate",
				"validateProductEntity",
				"validationsUnits",
				"validateCompounds",
				"validateNutrients",
				"validateAllergens",
				"validateComplements",
				"validateLanguageIds",
				"validatePrices",
				"validateFamilies",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["validateProductEntity"],
			get: ["validateProductEntity"],
			list: [
				addQueryFentity,
				function updateSearch(ctx) {
					return addSearchRelation(
						ctx,
						["name", "description"],
						"alimentation.products-translations",
						"product_id",
					);
				},
			],
			updateDisponibility: ["validateProductEntity"],
			getDisponibility: ["validateProductEntity"],
			updateAllergens: ["validateProductEntity", "validateAllergens"],
			getAllergens: ["validateProductEntity"],
			updateNutrients: ["validateProductEntity"],
			getNutrients: ["validateProductEntity", "validateNutrients"],
			updateComplements: ["validateProductEntity", "validateComplements"],
			getComplements: ["validateProductEntity"],
			updateComposition: ["validateProductEntity"],
			getComposition: ["validateProductEntity"],
			updatePrices: ["validateProductEntity"],
			getPrices: ["validateProductEntity"],
			getStocks: ["validateProductEntity"],
			getDerivatives: ["validateProductEntity"],
		},
		after: {
			create: [
				"saveTranslations",
				"saveCompound",
				"saveFamilies",
				"saveDisponibility",
				"saveNutrientProduct",
				"saveAllergensProduct",
				"saveComplementsProducts",
				"savePriceVariations",
			],
			update: ["saveTranslations", "saveDisponibility", "saveFamilies"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for update disponibility of Product ID
		 */
		updateDisponibility: {
			rest: "PATCH /:id/disponibility",
			params: {
				id: { type: "number", convert: true },
			},
			visibility: "published",
			scope: "alimentation:products:update",
			async handler(ctx) {
				const entity = {
					dish_type_lunch_id: ctx.params.dish_type_lunch_id,
					begin_date: ctx.params.begin_date,
					end_date: ctx.params.end_date,
					monday: ctx.params.monday,
					tuesday: ctx.params.tuesday,
					wednesday: ctx.params.wednesday,
					thursday: ctx.params.thursday,
					friday: ctx.params.friday,
					saturday: ctx.params.saturday,
					sunday: ctx.params.sunday,
					annulment_maximum_hour: ctx.params.annulment_maximum_hour,
					minimum_hour: ctx.params.minimum_hour,
					maximum_hour: ctx.params.maximum_hour,
					product_id: parseInt(ctx.params.id),
				};
				await ctx.call("alimentation.disponibilities.saveDisponibilityProduct", {
					disponibility_product: entity,
				});
				return await this.getDisponibilityByProduct(ctx);
			},
		},

		/**
		 * Action for get disponibility of Product ID
		 */
		getDisponibility: {
			rest: "GET /:id/disponibility",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return this.getDisponibilityByProduct(ctx);
			},
		},

		/**
		 * Action for update allergens of Product ID
		 */
		updateAllergens: {
			rest: "PATCH /:id/allergens",
			visibility: "published",
			scope: "alimentation:products:update",
			params: {
				id: { type: "number", convert: true },
				allergens: {
					type: "array",
					items: { type: "number" },
				},
			},
			async handler(ctx) {
				await ctx.call("alimentation.products-allergens.saveAllergensProduct", {
					product_id: parseInt(ctx.params.id),
					allergens: ctx.params.allergens,
				});
				this.clearCache();
				return await this.getAllergensByProduct(ctx);
			},
		},

		/**
		 * Action for get Allergens of Product ID
		 */
		getAllergens: {
			rest: "GET /:id/allergens",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return await this.getAllergensByProduct(ctx);
			},
		},

		/**
		 * Action for update Nutrients of Product ID
		 */
		updateNutrients: {
			rest: "PATCH /:id/nutrients",
			visibility: "published",
			scope: "alimentation:products:update",
			params: {
				id: { type: "number", convert: true },
				nutrients: {
					type: "array",
					items: {
						type: "object",
						props: {
							nutrient_id: { type: "number", integer: true, positive: true },
							quantity: { type: "number" },
						},
					},
				},
			},
			async handler(ctx) {
				await ctx.call("alimentation.products-nutrients.saveNutrientsProduct", {
					product_id: parseInt(ctx.params.id),
					nutrients: ctx.params.nutrients,
				});
				return this.getNutrientsByProduct(ctx);
			},
		},

		/**
		 * Action for get Nutrients for Product ID
		 */
		getNutrients: {
			rest: "GET /:id/nutrients",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return await this.getNutrientsByProduct(ctx);
			},
		},

		/**
		 * Action for update Complements for Product ID
		 */
		updateComplements: {
			rest: "PATCH /:id/complements",
			visibility: "published",
			scope: "alimentation:products:update",
			params: {
				id: { type: "number", convert: true },
				complements: {
					type: "array",
					items: { type: "number" },
				},
			},
			async handler(ctx) {
				await ctx.call("alimentation.products-complements.saveComplementsProducts", {
					product_id: parseInt(ctx.params.id),
					complements: ctx.params.complements,
				});
				return this.getComplementsByProduct(ctx);
			},
		},

		/**
		 * Action for get Complements of Product ID
		 */
		getComplements: {
			rest: "GET /:id/complements",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return this.getComplementsByProduct(ctx);
			},
		},

		/**
		 * Action for update Composition of Product ID
		 */
		updateComposition: {
			rest: "PATCH /:id/composition",
			visibility: "published",
			scope: "alimentation:products:update",
			params: {
				id: { type: "number", convert: true },
				composition: {
					optional: true,
					type: "array",
					items: {
						type: "object",
						props: {
							compound_id: { type: "number", integer: true, positive: true },
							quantity: { type: "number" },
						},
					},
				},
			},
			async handler(ctx) {
				await ctx.call("alimentation.compounds.saveCompoundsProduct", {
					product_id: parseInt(ctx.params.id),
					compounds: ctx.params.composition,
				});
				return this.getCompositionsByProduct(ctx);
			},
		},

		/**
		 * Action for get Composition of Product ID
		 */
		getComposition: {
			rest: "GET /:id/composition",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return this.getCompositionsByProduct(ctx);
			},
		},

		/**
		 * Action for update Prices of Product ID
		 */
		updatePrices: {
			rest: "PATCH /:id/prices",
			visibility: "published",
			scope: "alimentation:products:update",
			params: {
				id: { type: "number", convert: true },
				prices: {
					type: "array",
					items: {
						type: "object",
						props: {
							description: { type: "string", optional: true, nullable: true },
							tax_id: { type: "number", positive: true, integer: false },
							profile_id: { type: "number", positive: true, integer: false },
							price: { type: "number", positive: true, integer: false },
							meal: { type: "string", optional: true, nullable: true },
							time: { type: "string", default: "00:00:00" },
						},
					},
				},
			},
			async handler(ctx) {
				await ctx.call("alimentation.prices-variations.savePriceVariationProducts", {
					product_id: parseInt(ctx.params.id),
					prices: ctx.params.prices,
				});
				return this.getPricesByProduct(ctx);
			},
		},

		/**
		 * Action for get Prices of Product ID
		 */
		getPrices: {
			rest: "GET /:id/prices",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return this.getPricesByProduct(ctx);
			},
		},

		/**
		 * Action for get Stocks of Product ID
		 */
		getStocks: {
			rest: "GET /:id/stocks",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("alimentation.stocks.find", {
					query: {
						product_id: ctx.params.id,
					},
					withRelated: false,
				});
			},
		},

		/**
		 * Action for get Derivatives by Product ID
		 */
		getDerivatives: {
			rest: "GET /:id/derivatives",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:products:read",
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("alimentation.compounds.getDerivativesByProduct", {
					id: ctx.params.id,
				});
			},
		},

		checkProductDisponibility: {
			params: {
				product_id: { type: "number" },
			},
			async handler(ctx) {
				const product = await this._get(ctx, {
					id: ctx.params.product_id,
					withRelated: ["disponibility"],
				});
				return await this.checkDisponibility(product[0].disponibility, new Date());
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validatePrices(ctx) {
			if (ctx.params.type == "bar") {
				const validation = new Validator();
				const schema = {
					price: { type: "number", integer: false, positive: true },
					tax_price_id: { type: "number", integer: true, convert: true },
				};
				const check = validation.compile(schema);
				const res = check(ctx.params);
				if (res !== true) {
					return Promise.reject(new Errors.ValidationError("Entity validation error!", null, res));
				}
			}
		},
		/**
		 * Method for validate Product Code
		 */
		async validationCode(ctx) {
			let product_code = await this._find(ctx, {
				query: {
					code: ctx.params.code,
					fentity_id: ctx.meta.alimentation_entity_id,
				},
			});
			if (product_code.length !== 0) {
				throw new Errors.ValidationError(
					"The product code already exist",
					"PRODUCT_CODE_ALREADY_EXIST",
					{},
				);
			}
		},

		/**
		 * Validate code in update product
		 * @param {*} ctx
		 */
		async validateCodeUpdate(ctx) {
			let product_code = await this._find(ctx, {
				query: {
					code: ctx.params.code,
					fentity_id: ctx.meta.alimentation_entity_id,
				},
			});
			if (product_code.length !== 0) {
				if (product_code[0].id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError(
						"The product code already exist",
						"PRODUCT_CODE_ALREADY_EXIST",
						{},
					);
				}
			}
		},

		/**
		 * Method for validate Languages
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		/**
		 * Method for validate Units
		 */
		async validationsUnits(ctx) {
			await ctx.call("alimentation.units.get", { id: ctx.params.unit_id });
		},

		/**
		 * Method for validate Compounds
		 */
		async validateCompounds(ctx) {
			if (ctx.params.composition) {
				for (const compound of ctx.params.composition) {
					await this._get(ctx, { id: compound.compound_id, withRelated: false });
				}
			}
		},

		/**
		 * Method for validate Nutrients
		 */
		async validateNutrients(ctx) {
			if (ctx.params.nutrients) {
				for (const nutrient of ctx.params.nutrients) {
					await ctx.call("alimentation.nutrients.get", {
						id: nutrient.nutrient_id,
						withRelated: false,
					});
				}
			}
		},

		/**
		 * Method for validate Allergens
		 */
		async validateAllergens(ctx) {
			if (ctx.params.allergens) {
				for (const allergenId of ctx.params.allergens) {
					await ctx.call("alimentation.allergens.get", { id: allergenId });
				}
			}
		},

		/**
		 * Method for validate Complements
		 */
		async validateComplements(ctx) {
			if (ctx.params.complements) {
				for (const complementId of ctx.params.complements) {
					await ctx.call("alimentation.complements.get", { id: complementId });
				}
			}
		},

		// Method to validate de families
		async validateFamilies(ctx) {
			if (ctx.params.families) {
				for (const complementId of ctx.params.families) {
					await ctx.call("alimentation.families.get", { id: complementId });
				}
			}
		},

		// Method to save de families
		async saveFamilies(ctx, res) {
			if (ctx.params.families) {
				res[0].families = await ctx.call("alimentation.families-products.save_families", {
					product_id: res[0].id,
					families: ctx.params.families,
				});
			}
			return res;
		},

		/**
		 * Method for save Products Translations
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.products-translations.save_translations", {
				product_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.products-translations.find", {
				query: {
					product_id: res[0].id,
				},
			});
			return res;
		},

		/**
		 * Method for save Products Compounds
		 */
		async saveCompound(ctx, res) {
			if (ctx.params.composition) {
				res[0].composition = await ctx.call("alimentation.compounds.saveCompoundsProduct", {
					product_id: res[0].id,
					compounds: ctx.params.composition,
				});
			}
			return res;
		},

		/**
		 * Method for save Product Disponibility
		 */
		async saveDisponibility(ctx, res) {
			ctx.params.disponibility.product_id = res[0].id;
			res[0].disponibility = await ctx.call(
				"alimentation.disponibilities.saveDisponibilityProduct",
				{ disponibility_product: ctx.params.disponibility },
			);
			return res;
		},

		/**
		 * Method for save Products Nutrients
		 */
		async saveNutrientProduct(ctx, res) {
			if (ctx.params.nutrients) {
				res[0].nutrients = await ctx.call("alimentation.products-nutrients.saveNutrientsProduct", {
					product_id: res[0].id,
					nutrients: ctx.params.nutrients,
				});
			}
			return res;
		},

		/**
		 * Method for save Products Allergens
		 */
		async saveAllergensProduct(ctx, res) {
			if (ctx.params.allergens) {
				res[0].allergens = await ctx.call("alimentation.products-allergens.saveAllergensProduct", {
					product_id: res[0].id,
					allergens: ctx.params.allergens,
				});
			}
			return res;
		},

		/**
		 * Method for save Products Complements
		 */
		async saveComplementsProducts(ctx, res) {
			if (ctx.params.complements) {
				res[0].complements = await ctx.call(
					"alimentation.products-complements.saveComplementsProducts",
					{
						product_id: res[0].id,
						complements: ctx.params.complements,
					},
				);
			}
			return res;
		},

		/**
		 * Method for save Product Prices
		 */
		async savePriceVariations(ctx, res) {
			res[0].prices = await ctx.call("alimentation.prices-variations.savePriceVariationProducts", {
				product_id: res[0].id,
				prices: ctx.params.prices,
			});
			return res;
		},

		/**
		 * Method for get Disponibility of Product ID
		 */
		async getDisponibilityByProduct(ctx) {
			return await ctx.call("alimentation.disponibilities.find", {
				query: {
					product_id: ctx.params.id,
				},
			});
		},

		/**
		 * Method for get Allergens of Product ID
		 */
		async getAllergensByProduct(ctx) {
			return await ctx.call("alimentation.products-allergens.getAllergensByProduct", {
				id: ctx.params.id,
			});
		},

		/**
		 * Method for get Nutrients of Product ID
		 */
		async getNutrientsByProduct(ctx) {
			return await ctx.call("alimentation.products-nutrients.getNutrientsByProduct", {
				id: ctx.params.id,
			});
		},

		/**
		 * Method for get Complements of Product ID
		 */
		async getComplementsByProduct(ctx) {
			return await ctx.call("alimentation.products-complements.getComplementsByProduct", {
				id: ctx.params.id,
			});
		},

		/**
		 * Method for get Composition of Product ID
		 */
		async getCompositionsByProduct(ctx) {
			return await ctx.call("alimentation.compounds.getCompoundsByProduct", { id: ctx.params.id });
		},

		/**
		 * Method for get Prices of Product ID
		 */
		async getPricesByProduct(ctx) {
			return await ctx.call("alimentation.prices-variations.find", {
				query: {
					product_id: ctx.params.id,
				},
				withRelated: "tax,profile",
			});
		},

		async checkDisponibility(disponibility, date) {
			const now = new Date();
			const begin_date = disponibility.begin_date;
			const begin_date_after = now > begin_date ? true : false;
			const end_date = disponibility.end_date;
			const end_date_before = now < end_date ? true : false;

			let maximum_hour_before = disponibility.maximum_hour ? false : true;
			let minimum_hour_after = disponibility.minimum_hour ? false : true;
			if (new Date().getDay() === date.getDay()) {
				const maximum_hour = new Date(disponibility.maximum_hour).getHours();
				const minimum_hour = new Date(disponibility.minimum_hour).getHours();
				maximum_hour_before = now < maximum_hour ? true : false;
				minimum_hour_after = now > minimum_hour ? true : false;
			} else {
				if (new Date().getDay() < date.getDay()) {
					maximum_hour_before = true;
				} else {
					return false;
				}
			}
			let dayOfWeekDisponibility = false;

			switch (now.getDay) {
				case 0:
					dayOfWeekDisponibility = disponibility.sunday;
					break;
				case 1:
					dayOfWeekDisponibility = disponibility.monday;
					break;
				case 2:
					dayOfWeekDisponibility = disponibility.tuesday;
					break;
				case 3:
					dayOfWeekDisponibility = disponibility.wednesday;
					break;
				case 4:
					dayOfWeekDisponibility = disponibility.thursday;
					break;
				case 5:
					dayOfWeekDisponibility = disponibility.friday;
					break;
				case 6:
					dayOfWeekDisponibility = disponibility.saturday;
					break;
			}
			return (
				begin_date_after &&
				end_date_before &&
				minimum_hour_after &&
				maximum_hour_before &&
				dayOfWeekDisponibility
			);
		},

		/**
		 * Method for validate Product by Entity
		 */
		async validateProductEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The product not corresponde the entity.",
						"PRODUCT_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
