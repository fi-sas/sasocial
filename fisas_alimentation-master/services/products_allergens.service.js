"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.products-allergens",
	table: "product_allergen",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "products-allergens")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "product_id", "allergen_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			product_id: { type: "number", positive: true, integer: true },
			allergen_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Product Allergens
		 */
		saveAllergensProduct: {
			params: {
				product_id: { type: "number" },
				allergens: {
					type: "array",
					items: { type: "number" },
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				this.clearCache();
				for (const allergen of ctx.params.allergens) {
					const entities = {
						allergen_id: allergen,
						product_id: ctx.params.product_id,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for get Allergens of Product ID
		 */
		getAllergensByProduct: {
			async handler(ctx) {
				let res = [];
				const list_associations = await ctx.call("alimentation.products-allergens.find", {
					query: {
						product_id: ctx.params.id,
					},
				});
				for (const association of list_associations) {
					const allergen = await ctx.call("alimentation.allergens.get", {
						id: association.allergen_id,
					});
					res.push(allergen[0]);
				}
				return res;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
