"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.products-complements",
	table: "product_complement",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "products-complements")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "product_id", "complement_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			product_id: { type: "number", positive: true, integer: true },
			complement_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Product Complements
		 */
		saveComplementsProducts: {
			params: {
				product_id: { type: "number" },
				complements: {
					type: "array",
					items: { type: "number" },
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				this.clearCache();
				for (const complementId of ctx.params.complements) {
					const entities = {
						product_id: ctx.params.product_id,
						complement_id: complementId,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for get Complements of Product ID
		 */
		getComplementsByProduct: {
			async handler(ctx) {
				let res = [];
				const list_associations = await ctx.call("alimentation.products-complements.find", {
					query: {
						product_id: ctx.params.id,
					},
				});
				for (const association of list_associations) {
					const complement = await ctx.call("alimentation.complements.get", {
						id: association.complement_id,
					});
					res.push(complement[0]);
				}
				return res;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
