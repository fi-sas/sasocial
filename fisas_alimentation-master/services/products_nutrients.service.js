"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.products-nutrients",
	table: "product_nutrient",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "products-nutrients")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "product_id", "nutrient_id", "quantity"],
		defaultWithRelateds: ["nutrient"],
		withRelateds: {
			nutrient(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.nutrients", "nutrient", "nutrient_id");
			},
		},
		entityValidator: {
			product_id: { type: "number", positive: true, integer: true },
			nutrient_id: { type: "number", positive: true, integer: true },
			quantity: { type: "number", positive: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Product Nutrients
		 */
		saveNutrientsProduct: {
			params: {
				product_id: { type: "number", positive: true, integer: true },
				nutrients: {
					type: "array",
					item: {
						nutrient_id: { type: "number", integer: true, positive: true },
						quantity: { type: "number" },
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				this.clearCache();
				for (const nutri of ctx.params.nutrients) {
					const entities = {
						product_id: ctx.params.product_id,
						nutrient_id: nutri.nutrient_id,
						quantity: nutri.quantity,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for get Nutrients of Product ID
		 */
		getNutrientsByProduct: {
			async handler(ctx) {
				let resp = [];
				const associations = await this._find(ctx, {
					query: {
						product_id: ctx.params.id,
					},
				});
				for (const nutrient of associations) {
					const nutri_info = await ctx.call("alimentation.nutrients.get", {
						id: nutrient.nutrient_id,
					});
					const resp_object = {
						quantity: nutrient.quantity,
						nutrient: nutri_info[0],
					};
					resp.push(resp_object);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
