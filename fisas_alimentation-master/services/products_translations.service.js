"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.products-translations",
	table: "product_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "products-translations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "description", "language_id", "product_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			description: { type: "string" },
			language_id: { type: "number", positive: true, integer: true },
			product_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			find: [
				async function sanatizeParams(ctx) {
					if (ctx.meta.language_id) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query.language_id = ctx.meta.language_id;
						const total = await this._count(ctx, ctx.params);
						if (total === 0) {
							delete ctx.params.query.language_id;
						}
					}
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Product Translations
		 */
		save_translations: {
			params: {
				product_id: { type: "number", positive: true, integer: true },
				translations: {
					type: "array",
					item: {
						name: { type: "string" },
						description: { type: "string" },
						language_id: { type: "number", positive: true, integer: true },
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					product_id: ctx.params.product_id,
				});
				this.clearCache();
				for (const trans of ctx.params.translations) {
					const entities = {
						product_id: ctx.params.product_id,
						description: trans.description,
						language_id: trans.language_id,
						name: trans.name,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
