"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.recipes",
	table: "recipe",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "recipes")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"description",
			"active",
			"number_doses",
			"preparation",
			"preparation_mode",
			"microbiological_criteria",
			"packaging",
			"use",
			"distribution",
			"comment",
			"associated_docs",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["products", "steps"],
		withRelateds: {
			products(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((doc) => {
						return ctx
							.call("alimentation.recipes-products.getProducts", { recipe_id: doc.id })
							.then((res) => (doc.products = res));
					}),
				);
			},
			steps(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.recipes-steps", "steps", "id", "recipe_id");
			},
		},
		entityValidator: {
			products: {
				type: "array",
				items: {
					type: "object",
					props: {
						ingredient_id: { type: "number", integer: true, positive: true },
						liquid_quantity: { type: "number", integer: false },
						gross_quantity: { type: "number", integer: false },
					},
				},
				optional: true,
			},
			preparation_steps: {
				type: "array",
				items: {
					type: "object",
					props: {
						file_id: { type: "number", integer: true, positive: true, optional: true },
						step: { type: "number", integer: true },
						description: { type: "string" },
					},
				},
				optional: true,
			},
			name: { type: "string" },
			description: { type: "string" },
			active: { type: "boolean", default: true },
			number_doses: { type: "number", integer: true, default: 0 },
			preparation: { type: "string", optional: true, nullable: true },
			preparation_mode: { type: "string", optional: true, nullable: true },
			microbiological_criteria: { type: "string", optional: true, nullable: true },
			packaging: { type: "string", optional: true, nullable: true },
			use: { type: "string", optional: true, nullable: true },
			distribution: { type: "string", optional: true, nullable: true },
			comment: { type: "string", optional: true, nullable: true },
			associated_docs: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"validateProducts",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateProducts",
				"verifyRecipesEntity",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			// FALTA CONFIRMAR O QUE FAZER AQUI
			// remove: [
			// 	"verifyRecipesEntity",
			// 	"removeProductRecipes",
			// 	"removeRecipeSteps"
			// ],
			list: [
				addQueryFentity,
				//need until correction
				function fix_query(ctx) {
					delete ctx.params.query.name;
				},
			],
			get: ["verifyRecipesEntity"],
		},
		after: {
			create: ["saveProductRecipes", "saveRecipeSteps"],
			update: ["saveProductRecipes", "saveRecipeSteps"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.products.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-nutrients.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-allergens.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.products-complements.*"(ctx) {
			this.logger.info("CAPTURED EVENT => products");
			this.clearCache();
		},

		"alimentation.dishs-types.*"(ctx) {
			this.logger.info("CAPTURED EVENT => dish_type");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Products
		 */
		async validateProducts(ctx) {
			if (ctx.params.products) {
				const valueArr = ctx.params.products.map((item) => {
					return item.ingredient_id;
				});
				const isDuplicate = valueArr.some((item, idx) => {
					return valueArr.indexOf(item) != idx;
				});
				if (isDuplicate) {
					throw new Errors.ValidationError("Exist product duplicated");
				}
				for (const ingredient of ctx.params.products) {
					await ctx.call("alimentation.products.get", { id: ingredient.ingredient_id });
				}
			}
		},

		/**
		 * Method for save Products in Recipes
		 */
		async saveProductRecipes(ctx, res) {
			if (ctx.params.products) {
				res[0].products = await ctx.call("alimentation.recipes-products.saveProductRecipes", {
					recipe_id: res[0].id,
					products: ctx.params.products,
				});
			}
			return res;
		},

		/**
		 * Method for save Recipes Steps
		 */
		async saveRecipeSteps(ctx, res) {
			if (ctx.params.preparation_steps) {
				res[0].steps = await ctx.call("alimentation.recipes-steps.saveRecipeSteps", {
					recipe_id: res[0].id,
					preparation_steps: ctx.params.preparation_steps,
				});
			}
			return res;
		},

		/**
		 * Method fot verify if Recipes corresponde the Entity
		 */
		async verifyRecipesEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError("The recipe not correspond to entity");
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ValidationError("You need send entityId");
			}
		},

		/**
		 * Method for remove Products Recipes
		 */
		async removeProductRecipes(ctx) {
			return await ctx.call("alimentation.recipes-products.removeProductRecipes", {
				recipe_id: parseInt(ctx.params.id),
			});
		},

		/**
		 * Method for remove Recipes Steps
		 */
		async removeRecipeSteps(ctx) {
			return await ctx.call("alimentation.recipes-steps.removeRecipeSteps", {
				recipe_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
