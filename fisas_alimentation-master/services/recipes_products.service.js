"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.recipes-products",
	table: "recipe_product",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "recipes-products")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "ingredient_id", "recipe_id", "liquid_quantity", "gross_quantity"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			ingredient_id: { type: "number", positive: true, integer: true },
			recipe_id: { type: "number", positive: true, integer: true },
			liquid_quantity: { type: "number", positive: true },
			gross_quantity: { type: "number", positive: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save products recipes
		 */
		saveProductRecipes: {
			params: {
				recipe_id: { type: "number", positive: true, integer: true },
				products: {
					type: "array",
					item: {
						ingredient_id: { type: "number", positive: true, integer: true },
						liquid_quantity: { type: "number", integer: false },
						gross_quantity: { type: "number", integer: false },
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					recipe_id: ctx.params.recipe_id,
				});
				for (const ingredient of ctx.params.products) {
					ingredient.recipe_id = ctx.params.recipe_id;
					const insert = await this._insert(ctx, { entity: ingredient });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for remove Products Recipes
		 */
		removeProductRecipes: {
			params: {
				recipe_id: { type: "number" },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					recipe_id: ctx.params.recipe_id,
				});
				return true;
			},
		},

		/**
		 * Action for get Products
		 */
		getProducts: {
			params: {
				recipe_id: { type: "number" },
			},
			async handler(ctx) {
				let res = [];
				const listAssociations = await this._find(ctx, {
					query: {
						recipe_id: ctx.params.recipe_id,
					},
				});
				for (const association of listAssociations) {
					const ingredient_info = await ctx.call("alimentation.products.get", {
						id: association.ingredient_id,
						withRelated: ["translations", "allergens", "nutrients", "unit"],
					});
					ingredient_info[0].recipes_quantitys = association;
					res.push(ingredient_info[0]);
				}
				return res;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
