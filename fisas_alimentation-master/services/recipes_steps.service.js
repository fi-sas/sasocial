"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.recipes-steps",
	table: "recipe_step",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "recipes-steps")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"active",
			"file_id",
			"step",
			"description",
			"recipe_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "media.files", "file", "file_id");
			},
		},
		entityValidator: {
			active: { type: "boolean", default: true },
			file_id: { type: "number", positive: true, integer: true, optional: true },
			step: { type: "number", integer: true, default: 0 },
			description: { type: "string", optional: true, nullable: true },
			recipe_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action for save Recipes Steps
		 */
		saveRecipeSteps: {
			params: {
				recipe_id: { type: "number", positive: true, integer: true },
				preparation_steps: {
					type: "array",
					item: {
						file_id: { type: "number", optional: true, nullable: true },
						step: { type: "number", optional: true },
						decription: { type: "string", optional: true, nullable: true },
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					recipe_id: ctx.params.recipe_id,
				});
				for (const step of ctx.params.preparation_steps) {
					step.recipe_id = ctx.params.recipe_id;
					step.created_at = new Date();
					step.updated_at = new Date();
					const insert = await this._insert(ctx, { entity: step });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		/**
		 * Action for remove Recipes Steps
		 */
		removeRecipeSteps: {
			params: {
				recipe_id: { type: "number" },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					recipe_id: ctx.params.recipe_id,
				});
				return true;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
