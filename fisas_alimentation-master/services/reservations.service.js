"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addEntity, addQueryFentity } = require("./helpers/entity.guard.js");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const moment = require("moment");
const Stream = require("stream");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.reservations",
	table: "reservation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "reservations")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"date",
			"location",
			"menu_dish_id",
			"movement_id",
			"item_id",
			"annulment_movement_id",
			"device_id",
			"user_id",
			"is_from_pack",
			"user_meal_pack_id",
			"is_served",
			"is_available",
			"has_canceled",
			"served_at",
			"served_by",
			"fentity_id",
			"service_id",
			"meal",
			"price",
			"created_at",
			"updated_at",
		],

		defaultWithRelateds: [],
		withRelateds: {
			menu_dish(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((reservation) => {
						return ctx
							.call("alimentation.menus-dishs.get", {
								id: reservation.menu_dish_id,
								withRelated: ["dish", "type", "menu"],
							})
							.then((res) => (reservation.menu_dish = res[0]));
					}),
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			served_by_user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "served_by_user", "served_by");
			},
		},
		entityValidator: {
			date: { type: "date", convert: true },
			location: { type: "string", optional: true, nullable: true },
			menu_dish_id: { type: "number", positive: true, integer: true, convert: true },
			movement_id: { type: "string", optional: true },
			item_id: { type: "string", optional: true },
			annulment_movement_id: {
				type: "string",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
			device_id: { type: "number", positive: true, integer: true, nullable: true },
			user_id: { type: "number", positive: true, integer: true },
			is_from_pack: { type: "boolean", default: false },
			user_meal_pack_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				convert: true,
			},
			is_served: { type: "boolean", default: false },
			is_available: { type: "boolean", default: false },
			has_canceled: { type: "boolean", default: false },
			meal: { type: "string", values: ["lunch", "dinner", "breakfast"] },
			service_id: { type: "number", convert: true, optional: true, nullable: true },
			price: { type: "number", convert: true, optional: true, nullable: true, default: 0 },
			served_at: { type: "date", optional: true, nullable: true },
			served_by: { type: "number", integer: true, nullable: true, optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [addQueryFentity],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},

		getUserReservations: {
			params: {
				limit: { type: "number", optional: true, convert: true },
				sort: { type: "string", optional: true },
			},
			async handler(ctx) {
				if (!ctx.meta.user.id || ctx.meta.isGuest) {
					return [];
				}
				return this._find(ctx, {
					withRelated: ["menu_dish"],
					query: (qb) => {
						qb.where("user_id", "=", ctx.meta.user.id);
						if (ctx.params.month) {
							qb.whereRaw("EXTRACT(MONTH FROM reservation.date) = ?", [ctx.params.month]);
						}
						if (ctx.params.year) {
							qb.whereRaw("EXTRACT(YEAR FROM reservation.date) = ?", [ctx.params.year]);
						}
						if (ctx.params.onlyAvailables) {
							qb.andWhere("reservation.is_available", "=", ctx.params.onlyAvailables);
							qb.andWhere("reservation.date", ">=", this.adapter.raw("NOW()::date"));
						}
					},
					limit: ctx.params.limit,
					sort: ctx.params.sort,
				});
			},
		},
		countAllReservations: {
			rest: "GET /countAll",
			visibility: "published",
			scope: "alimentation:reservations:read",
			params: {
				date: { type: "date", convert: true },
				meal: { type: "enum", values: ["lunch", "dinner"], optional: true },
			},
			handler(ctx) {
				if (!ctx.meta.alimentation_entity_id) {
					throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
				}

				return this.adapter
					.raw(
						`SELECT
							m.date as date,
							s.id as service_id,
							s.name as service_name,
							m.meal as meal,
							dtt.name as type,
							d_t.name as dish,
							(SELECT count(id) as reserved FROM reservation as r WHERE  r.menu_dish_id = md.id and r.has_canceled = false) as reserved
						  FROM menu as m
						  INNER JOIN service as s on s.id = m.service_id
						  INNER JOIN menu_dish as md on md.menu_id = m.id
						  INNER JOIN dish_type as dt on dt.id = md.type_id
						  INNER JOIN dish_type_translation as dtt on dtt.dish_type_id = md.type_id AND dtt.language_id = 3
						  INNER JOIN dish as d on d.id = md.dish_id
						  INNER JOIN dish_translation as d_t on d.id = d_t.dish_id AND d_t.language_id = 3
						  WHERE m.date  = ? and validated = true AND s.fentity_id = ?
						  ORDER BY m.service_id, m.meal, dtt.name`,
						[moment(ctx.params.date).format("YYYY-MM-DD"), ctx.meta.alimentation_entity_id],
					)
					.then((res) => {
						const services = [];
						res.rows.map((r) => {
							const service = services.find((s) => s.id === r.service_id);
							if (service) {
								service.meals.push({
									meal: r.meal,
									type: r.type,
									dish: r.dish,
									reserved: r.reserved,
								});
							} else {
								services.push({
									id: r.service_id,
									name: r.service_name,
									meals: [
										{
											meal: r.meal,
											type: r.type,
											dish: r.dish,
											reserved: r.reserved,
										},
									],
								});
							}
						});

						return services;
					});
			},
		},
		countReservations: {
			rest: "GET /count",
			visibility: "published",
			scope: "alimentation:reservations:read",
			params: {
				service_id: { type: "number", convert: true },
				meal: { type: "enum", values: ["lunch", "dinner"] },
				date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const menu = await ctx.call("alimentation.menus.find", {
					query: {
						service_id: ctx.params.service_id,
						meal: ctx.params.meal,
						date: moment(ctx.params.date).format("YYYY-MM-DD"),
					},
					withRelated: "dishs",
				});

				if (menu.length > 0) {
					const counts = [];

					for (let menu_dish of menu[0].dishs) {
						await this.adapter
							.raw(
								`SELECT (
							SELECT  count(is_served) as served
							FROM reservation WHERE menu_dish_id = ? AND is_served = true
							 ) as served,
							(
							SELECT count(is_served) as notServed
							FROM reservation WHERE menu_dish_id = ? AND is_served = false AND has_canceled = false
							) as notServed,
							(
							SELECT count(has_canceled) as cancelled
							FROM reservation WHERE menu_dish_id = ? AND has_canceled = true
							) as cancelled,
							(
							SELECT count(menu_dish_id) as total
							FROM reservation WHERE menu_dish_id = ?
							) as total,
							(
								SELECT count(menu_dish_id) as total
								FROM reservation WHERE menu_dish_id = ? AND has_canceled = false
								) as reserved`,
								[menu_dish.id, menu_dish.id, menu_dish.id, menu_dish.id, menu_dish.id],
							)
							.then((res) => {
								counts.push({
									type: menu_dish.type.translations[0].name,
									pos_operations: menu_dish.type.pos_operations,
									dish: menu_dish.dish.translations[0].name,
									doses_available: menu_dish.doses_available,
									available:
										menu_dish.doses_available - res.rows[0].total < 0
											? 0
											: menu_dish.doses_available - res.rows[0].total,
									cancelled: res.rows[0].cancelled,
									id: menu_dish.id,
									nonServed: res.rows[0].notserved,
									served: res.rows[0].served,
									total: res.rows[0].total,
								});
							});
					}

					return counts;
				} else {
					return [];
				}
			},
		},

		cancelReservation: {
			rest: "POST /:id/cancel",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:reservations:status",
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const reservation = await this._get(ctx, {
					id: ctx.params.id,
					withRelated: false,
				});
				const menu_dish = await ctx.call("alimentation.menus-dishs.get", {
					id: reservation[0].menu_dish_id,
					withRelated: false,
				});
				if (ctx.meta.device.type !== "POS" && ctx.meta.device.type !== "BO") {
					if (reservation[0].user_id !== ctx.meta.user.id) {
						throw new Errors.ValidationError(
							"This reservation is not associated to logged user",
							"RESERVATION_NOT_ASSOCIATED_LOGGED_USER",
							{},
						);
					}

					if (menu_dish[0].nullable_until) {
						if (moment().isAfter(moment(menu_dish[0].nullable_until))) {
							throw new Errors.ValidationError(
								"Maximum hour of consumption reached",
								"MAXIMUM_HOUR_CONSUPTION_REACHED",
								{},
							);
						}
					}
				}

				if (!reservation[0].is_available) {
					throw new Errors.ValidationError(
						"Is not available for annullament",
						"IS_NOT_AVAILABLE_FOR_ANNULLAMENT",
						{},
					);
				}

				if (reservation[0].has_canceled) {
					throw new Errors.ValidationError(
						"This reservation already has been cancelled",
						"RESERVATION_HAS_BEEN_CANCELLED",
						{},
					);
				}

				if (reservation[0].is_served) {
					throw new Errors.ValidationError(
						"Reservation already used",
						"RESERVATION_ALREADY_USED",
						{},
					);
				}

				if (reservation[0].is_from_pack) {
					throw new Errors.ValidationError(
						"Cant cancel reservations from pack",
						"CANT_CANCEL_RESERVATION_FROM_PACK",
						{},
					);
				}

				let reservation_update = await this._get(ctx, { id: ctx.params.id });

				const cancel_reservation = await ctx.call("current_account.movements.partialCancel", {
					movement_id: reservation_update[0].movement_id,
					items: [
						{
							id: reservation_update[0].item_id,
							quantity: 1,
						},
					],
				});

				reservation_update[0].is_available = false;
				reservation_update[0].has_canceled = true;
				reservation_update[0].annulment_movement_id = cancel_reservation[0].movement_cancel_id;
				return this._update(ctx, reservation_update[0]);
			},
		},

		unservedMeal: {
			rest: "DELETE /:id/unserved",
			params: {
				id: { type: "number", convert: true },
			},
			visibility: "published",
			scope: "alimentation:reservations:status",
			async handler(ctx) {
				const reservation = await this._get(ctx, { id: ctx.params.id });
				reservation[0].is_served = false;
				reservation[0].is_available = true;
				reservation[0].served_at = new Date();
				reservation[0].device_id = ctx.meta.device.id;
				reservation[0].served_by = ctx.meta.user.id;
				return this._update(ctx, reservation[0]);
			},
		},

		serveMeal: {
			rest: "POST /:id/serve",
			params: {
				id: { type: "number", convert: true },
			},
			visibility: "published",
			scope: "alimentation:reservations:status",
			async handler(ctx) {
				const reservation = await this._get(ctx, { id: ctx.params.id });
				if (reservation[0].has_canceled) {
					throw new Errors.ValidationError(
						"This reservation already has been cancelled",
						"RESERVATION_HAS_BEEN_CANCELLED",
						{},
					);
				}
				reservation[0].is_served = true;
				reservation[0].is_available = false;
				reservation[0].served_at = new Date();
				reservation[0].device_id = ctx.meta.device.id;
				reservation[0].served_by = ctx.meta.user.id;
				return this._update(ctx, reservation[0]);
			},
		},

		getMenuDishReservationCount: {
			params: {
				menu_dish_id: { type: "number" },
			},
			handler(ctx) {
				return this._count(ctx, {
					query: {
						menu_dish_id: ctx.params.menu_dish_id,
						has_canceled: false,
					},
				});
			},
		},

		confirm_reservation: {
			visibility: "public",
			async handler(ctx) {
				const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "REFECTORY");
				const created_list = [];
				for (const item of filtered_items) {
					const menudish = await ctx.call("alimentation.menus-dishs.get", {
						id: item.extra_info.menu_dish_id,
						withRelated: ["menu"],
					});
					for (let i = 1; i <= item.quantity; i++) {
						const entity = {
							date: new Date(menudish[0].menu.date),
							location: item.location,
							menu_dish_id: item.extra_info.menu_dish_id,
							movement_id: item.movement_id,
							item_id: item.id,
							device_id: null,
							user_id: ctx.params.user_id,
							is_from_pack: false,
							user_meal_pack_id: null,
							is_served: false,
							is_available: true,
							has_canceled: false,
							meal: menudish[0].menu.meal,
							fentity_id: item.extra_info.fentity_id,
							service_id: menudish[0].menu.service_id,
							price: item.unit_value,
							created_at: new Date(),
							updated_at: new Date(),
						};
						await this._create(ctx, entity).then((reservation) => {
							created_list.push(reservation);
						});
					}
				}
				return created_list.length === filtered_items.length ||
					created_list.length > filtered_items.length
					? true
					: false;
			},
		},

		cancel_reservation: {
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				const list_items = ctx.params.items;
				const filtered_items = list_items.filter((ele) => ele.article_type === "REFECTORY");
				const updated_reservations = [];
				for (const item of filtered_items) {
					const reservation = await this._find(ctx, {
						query: {
							movement_id: item.movement_id,
							item_id: item.id,
							menu_dish_id: item.extra_info.menu_dish_id,
							has_canceled: false,
						},
					});
					const menu_dish = await ctx.call("alimentation.menus-dishs.get", {
						id: item.extra_info.menu_dish_id,
						withRelated: false,
					});
					if (reservation) {
						if (reservation.length === 0) {
							return false;
						} else {
							if (reservation[0].has_canceled) {
								return false;
							}
							if (new Date() >= menu_dish[0].nullable_until) {
								return false;
							}
							if (reservation[0].is_served) {
								return false;
							}
							if (reservation[0].is_from_pack) {
								return false;
							}
							if (!reservation[0].is_available) {
								return false;
							}
							reservation[0].is_available = false;
							reservation[0].has_canceled = true;
							reservation[0].annulment_movement_id = ctx.params.movement_cancel_id;
							await this._update(ctx, reservation[0])
								.then((reservation_update) => {
									updated_reservations.push(reservation_update);
								})
								.catch(() => {
									return false;
								});
						}
					}
				}
				return updated_reservations.length === filtered_items.length ? true : false;
			},
		},

		get_users_with_purchased_tickets: {
			//rest: "GET /get-users-with-purchased-tickets",
			//visibility: "published",
			visibility: "public",
			params: {},
			async handler(ctx) {
				// Get current academic year start_date and end_date
				const academic_year = await ctx.call(
					"configuration.academic_years.get_current_academic_year",
					{},
				);

				this.adapter.find({
					query: (q) => {
						q.distinct("user_id as id");
						q.where("date", "<=", academic_year[0].end_date);
						q.where("date", ">=", academic_year[0].start_date);

						return q;
					},
					withRelated: false,
				});
			},
		},

		reservation_report: {
			rest: "GET /reservationsReport",
			scope: "alimentation:reservations:read",
			params: {
				service_id: {
					type: "array",
					items: { type: "number", convert: true, min: 0 },
					convert: true,
					optional: true,
				},
				dish_type_id: {
					type: "array",
					items: { type: "number", convert: true, min: 0 },
					convert: true,
					optional: true,
				},
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
				meal: { type: "enum", values: ["lunch", "dinner"], optional: true, nullable: true },
			},
			async handler(ctx) {
				const bindings = [];
				bindings.push(moment(ctx.params.start_date).format("YYYY-MM-DD"));
				bindings.push(moment(ctx.params.end_date).format("YYYY-MM-DD"));

				if (ctx.params.dish_type_id) {
					bindings.push(...ctx.params.dish_type_id);
				}

				if (ctx.params.meal) {
					bindings.push(ctx.params.meal);
				}

				const fentityId = this.getFiscalEntity(ctx);
				if (ctx.params.service_id) {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								id: ctx.params.service_id,
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				} else {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				}
				bindings.push(...ctx.params.service_id);

				const allReservations = await this.adapter.raw(
					`SELECT
					m.date as date,
					s.id as service_id,
					s.name as service_name,
					m.meal as meal,
					dtt.name as dish_type_name,
					(SELECT count(id) as reserved FROM reservation as r WHERE  r.menu_dish_id = md.id and r.has_canceled = false) as total
					FROM menu as m
					INNER JOIN service as s on s.id = m.service_id
					INNER JOIN menu_dish as md on md.menu_id = m.id
					INNER JOIN dish_type as dt on dt.id = md.type_id
					INNER JOIN dish_type_translation as dtt on dtt.dish_type_id = md.type_id AND dtt.language_id = 3
				where
					m.date >= ? and m.date <= ?
					and m.validated = true
					${
						ctx.params.dish_type_id
							? "and  dt.id in (" + ctx.params.dish_type_id.map((_) => "?").join(",") + ") "
							: ""
					}
					${ctx.params.meal ? "and m.meal = ? " : ""}
					${
						ctx.params.service_id
							? "and s.id in (" + ctx.params.service_id.map((_) => "?").join(",") + ") "
							: ""
					}
				order by m.date, s."name", m.meal, dtt."name"`,
					bindings,
				);
				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "FOOD_RESERVATIONS_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							reservations: allReservations.rows,
							start_date: moment(ctx.params.start_date).format("YYYY-MM-DD"),
							end_date: moment(ctx.params.end_date).format("YYYY-MM-DD"),
							created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
							user: {
								name: ctx.meta.user.name,
							},
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
		revenue_report: {
			rest: "GET /revenueReport",
			scope: "alimentation:reservations:read",
			params: {
				service_id: {
					type: "array",
					items: { type: "number", convert: true, min: 0 },
					convert: true,
					optional: true,
				},
				start_date: { type: "date", convert: true },
				end_date: { type: "date", convert: true },
			},
			async handler(ctx) {
				const bindings = [];
				bindings.push(ctx.params.start_date);
				bindings.push(ctx.params.end_date);

				const fentityId = this.getFiscalEntity(ctx);
				if (ctx.params.service_id) {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								id: ctx.params.service_id,
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				} else {
					ctx.params.service_id = await ctx
						.call("alimentation.services.find", {
							query: {
								fentity_id: fentityId,
							},
							withRelated: false,
							fields: "id",
						})
						.then((services) => services.map((service) => service.id));
				}
				bindings.push(...ctx.params.service_id);

				const allReservations = await this.adapter.raw(
					`SELECT s.id as service_id, s."name" as service_name, SUM(r.price) as total
				FROM reservation as r
				inner join menu_dish as md on md.id = r.menu_dish_id
				inner join menu as m on m.id = md.menu_id
				inner join service as s ON s.id = m.service_id
				where
					m.date >= ? and m.date <= ?
					and (r.has_canceled = false) and r.is_from_pack = false
					${
						ctx.params.service_id
							? "and s.id in (" + ctx.params.service_id.map((_) => "?").join(",") + ") "
							: ""
					}
				group by  s.id, s."name"
				order by s."name"`,
					bindings,
				);

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "FOOD_RESERVATIONS_REVENUE_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							rows: allReservations.rows,
							start_date: moment(ctx.params.start_date).format("YYYY-MM-DD HH:mm:ss"),
							end_date: moment(ctx.params.end_date).format("YYYY-MM-DD HH:mm:ss"),
							created_at: moment().format("YYYY-MM-DD HH:mm:ss"),
							user: {
								name: ctx.meta.user.name,
							},
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/***
		 * getFiscalEntity
		 * Returns the fiscal entity id send in the header
		 * @params ctx moleculer context
		 * @returns number id of fiscal entity
		 */
		getFiscalEntity(ctx) {
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError(
					"Need send fiscal entity Id",
					"NEED_SEND_FISCAL_ENTITY_ID",
					{},
				);
			}

			return ctx.meta.alimentation_entity_id;
		},
		async validateReservationEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The reservation not correspont the entity",
						"RESERVATION_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
			}
		},

		removeDuplicatedValues(array) {
			const array_not_duplicated = [];
			for (let menudish of array) {
				const exist = array_not_duplicated.find((ele) => ele.id === menudish.id);
				if (exist) {
					continue;
				} else {
					array_not_duplicated.push(menudish);
				}
			}
			return array_not_duplicated;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
