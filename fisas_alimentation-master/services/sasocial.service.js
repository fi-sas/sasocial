"use strict";

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "alimentation.sasocial",

	/**
	 * Settings
	 */
	settings: {
		backup_info: {
			SERVICE_NAME: "alimentation",
			DB_USER: process.env.POSTGRES_USER,
			DB_PASS: process.env.POSTGRES_PASSWORD,
			DATABASE: process.env.POSTGRES_DB,
			DB_HOST: process.env.POSTGRES_HOST,
			DB_PORT: process.env.POSTGRES_PORT,
		},
	},

	mixins: [],
	/**
	 * Dependencies
	 */
	//dependencies: ["sasocial.backups"],

	/**
	 * Actions
	 */
	actions: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
