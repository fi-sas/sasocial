"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.services",
	table: "service",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "services")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"type",
			"wharehouse_id",
			"device_id",
			"organic_unit_id",
			"fentity_id",
			"created_at",
			"service_id",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			fentity(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((service) => {
						return ctx
							.call("alimentation.entities.get", { id: service.fentity_id })
							.then((res) => (service.fentity = res[0]));
					}),
				);
			},
			wharehouse(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((service) => {
						return ctx
							.call("alimentation.wharehouses.get", {
								id: service.wharehouse_id,
								withRelated: false,
							})
							.then((res) => (service.wharehouse = res[0]));
					}),
				);
			},
			devices(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((service) => {
						return ctx
							.call("alimentation.services-devices.getInfoDivices", { service_id: service.id })
							.then((res) => (service.devices = res));
					}),
				);
			},
			school(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "infrastructure.organic-units", "school", "organic_unit_id");
			},
			families(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((service) => {
						return ctx
							.call("alimentation.services-families.getInfoFamilies", { service_id: service.id })
							.then((res) => (service.families = res));
					}),
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			type: { type: "string", values: ["bar", "canteen"], default: "bar" },
			wharehouse_id: { type: "number", positive: true, integer: true },
			organic_unit_id: { type: "number", positive: true, integer: true },
			active: { type: "boolean", default: true },
			service_id: { type: "number", positive: true, integer: true },
			families: {
				type: "array",
				items: { type: "number" },
				optional: true,
			},
			devices: {
				type: "array",
				items: { type: "number" },
				optional: true,
			},
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				"validateWharehouse",
				"validateOrganicUnit",
				"validateService",
				"validateFamilies",
				"validateDivices",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateServicesEntity",
				"validateWharehouse",
				"validateOrganicUnit",
				"validateService",
				"validateFamilies",
				"validateDivices",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [addQueryFentity],
			get: ["validateServicesEntity"],
			remove: ["validateServicesEntity"],
		},
		after: {
			create: ["saveServicesFamilies", "saveServicesDevices"],
			update: ["saveServicesFamilies", "saveServicesDevices"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		getActiveDevices: {
			rest: "GET /:id/active/devices",
			visibility: "published",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:services:read",
			async handler(ctx) {
				let resp = [];
				const devices = await ctx.call("alimentation.services-devices.find", {
					query: {
						service_id: ctx.params.id,
					},
					withRelated: ["device"],
				});
				for (const device of devices) {
					if (device.device !== null) {
						resp.push(device.device);
					}
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Wharehouse
		 */
		async validateWharehouse(ctx) {
			await ctx.call("alimentation.wharehouses.get", {
				id: ctx.params.wharehouse_id,
				withRelated: false,
			});
		},

		/**
		 * Method for validate Organic Unit
		 */
		async validateOrganicUnit(ctx) {
			await ctx.call("infrastructure.organic-units.get", { id: ctx.params.organic_unit_id });
		},

		/**
		 * Method for validate Services
		 */
		async validateService(ctx) {
			await ctx.call("configuration.services.get", { id: ctx.params.service_id });
		},

		/**
		 * Method for validate Families
		 */
		async validateFamilies(ctx) {
			if (Array.isArray(ctx.params.families)) {
				for (const family of ctx.params.families) {
					await ctx.call("alimentation.families.get", { id: family });
				}
			}
		},

		/**
		 * Method for validate Divices
		 */
		async validateDivices(ctx) {
			if (Array.isArray(ctx.params.devices)) {
				for (const divice of ctx.params.devices) {
					await ctx.call("configuration.devices.get", { id: divice });
				}
			}
		},

		/**
		 * Method for save Service Families
		 */
		async saveServicesFamilies(ctx, res) {
			if (ctx.params.families) {
				await ctx.call("alimentation.services-families.saveServiceFamilies", {
					service_id: res[0].id,
					families: ctx.params.families,
				});
				res[0].families = await ctx.call("alimentation.services-families.getInfoFamilies", {
					service_id: res[0].id,
				});
			}
			return res;
		},

		/**
		 * Method for save Service Divices
		 */
		async saveServicesDevices(ctx, res) {
			if (ctx.params.devices) {
				await ctx.call("alimentation.services-devices.saveServicesDevices", {
					service_id: res[0].id,
					type: ctx.params.type,
					devices: ctx.params.devices,
				});
				res[0].devices = await ctx.call("alimentation.services-devices.find", {
					query: {
						service_id: res[0].id,
					},
				});
			}
			return res;
		},

		/**
		 * Method for validate Service to Entity
		 */
		async validateServicesEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The services not correspont the entity",
						"SERVICE_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
