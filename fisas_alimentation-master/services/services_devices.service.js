"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.services-devices",
	table: "service_device",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "services-devices")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "device_id", "service_id"],
		defaultWithRelateds: [],
		withRelateds: {
			device(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.devices", "device", "device_id");
			},
		},
		entityValidator: {
			device_id: { type: "number", positive: true, integer: true },
			service_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		saveServicesDevices: {
			params: {
				service_id: { type: "number", positive: true, integer: true },
				type: { type: "string", values: ["bar", "canteen"], default: "bar" },
				devices: {
					type: "array",
					item: { type: "number" },
				},
			},
			async handler(ctx) {
				let resp = [];
				await this.adapter.removeMany({
					service_id: ctx.params.service_id,
				});
				if (ctx.params.type === "bar") {
					// SHOULD DELETE THE DEVICES FROM THE OTHERS SERVICES OF BAR
					const idsBarService = await ctx.call("alimentation.services.find", {
						query: {
							type: "bar",
						},
						fields: ["id"],
					});

					await this.adapter.raw(
						"delete from service_device where service_id in (" +
							idsBarService.map((_) => "?").join(",") +
							") and device_id in (" +
							ctx.params.devices.map((_) => "?").join(",") +
							");",
						[...idsBarService.map((s) => s.id), ...ctx.params.devices],
					);
				}

				this.clearCache();
				for (const device of ctx.params.devices) {
					const entities = {
						device_id: device,
						service_id: ctx.params.service_id,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},

		getInfoDivices: {
			params: {
				service_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				let resp = [];
				const associations = await this._find(ctx, {
					query: {
						service_id: ctx.params.service_id,
					},
				});
				for (const divices of associations) {
					const divice_info = await ctx.call("configuration.devices.get", {
						id: divices.device_id,
					});
					// divices.divice = divice_info[0];
					resp.push(divice_info[0]);
				}
				return resp;
			},
		},
		getServiceBarOfDevice: {
			async handler(ctx) {
				const device_id = ctx.meta.device.id;
				return this.adapter
					.raw(
						"select sd.service_id from service_device as sd inner join service as s on s.id = sd.service_id where s.type = 'bar' and sd.device_id = ?",
						[device_id],
					)
					.then((service) => {
						const services = service.rows.map((row) => row.service_id);
						if (services.length > 0) {
							return ctx.call("alimentation.services.get", { id: services[0] });
						} else {
							throw new Errors.ValidationError(
								"The device not have a service associated",
								"FOOD_DEVICE_WITHOUT_SERVICE",
								{},
							);
						}
					});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
