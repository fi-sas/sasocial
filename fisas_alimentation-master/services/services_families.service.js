"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.services-families",
	table: "service_family",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "services-families")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "family_id", "service_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			family_id: { type: "number", positive: true, integer: true },
			service_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		saveServiceFamilies: {
			params: {
				service_id: { type: "number", positive: true, integer: true },
				families: {
					type: "array",
					item: { type: "number" },
				},
			},
			async handler(ctx) {
				let resp = [];
				await this.adapter.removeMany({
					service_id: ctx.params.service_id,
				});
				this.clearCache();
				for (const family of ctx.params.families) {
					const entities = {
						family_id: family,
						service_id: ctx.params.service_id,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},
		getInfoFamilies: {
			params: {
				service_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				const associations = await this._find(ctx, {
					query: {
						service_id: ctx.params.service_id,
					},
				});
				return ctx.call("alimentation.families.find", {
					query: {
						id: associations.map((a) => a.family_id),
					},
					withRelated: ["translations", "files"],
				});
			},
		},
		getInfoService: {
			params: {
				family_id: { type: "number", integer: true, convert: true },
			},
			async handler(ctx) {
				const familiesServices = await this._find(ctx, {
					query: {
						family_id: ctx.params.family_id,
					},
				});
				return ctx.call("alimentation.services.find", {
					query: {
						id: familiesServices.map((fs) => fs.service_id),
					},
					withRelated: ["devices"],
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
