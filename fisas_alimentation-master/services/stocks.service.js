"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
const Knex = require("knex");
const Stream = require("stream");
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.stocks",
	table: "stock",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "stocks")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"lote",
			"quantity",
			"product_id",
			"wharehouse_id",
			"expired",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["product", "wharehouse"],
		withRelateds: {
			product(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.products", "product", "product_id");
			},
			wharehouse(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.wharehouses", "wharehouse", "wharehouse_id");
			},
		},
		entityValidator: {
			lote: { type: "string", optional: true, nullable: true },
			quantity: { type: "number", convert: true },
			product_id: { type: "number", positive: true, integer: true },
			wharehouse_id: { type: "number", positive: true, integer: true },
			expired: { type: "date", convert: true, optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				"validateWharehouse",
				"validateProduct",
				"validateLote",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateWharehouse",
				"validateProduct",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function sanatizeQuery(ctx) {
					if (!ctx.meta.alimentation_entity_id) {
						throw new Errors.ForbiddenError(
							"Need send fiscal entity Id",
							"NEED_SEND_FISCAL_ENTITY_ID",
							{},
						);
					}
					ctx.params.query = ctx.params.query || {};

					if (ctx.params.query.wharehouse_id) {
						const wharehouse_access = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
						if (Array.isArray(ctx.params.query.wharehouse_id)) {
							for (let wh_id of ctx.params.query.wharehouse_id) {
								if (!wharehouse_access.includes(Number.parseInt(wh_id)))
									throw new Errors.ValidationError(
										"No permission to access this wharehouse",
										"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
										{},
									);
							}
						} else {
							if (!wharehouse_access.includes(Number.parseInt(ctx.params.query.wharehouse_id)))
								throw new Errors.ValidationError(
									"No permission to access this wharehouse",
									"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
									{},
								);
						}
					} else
						ctx.params.query.wharehouse_id = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
				},
				function updateSearch(ctx) {
					ctx.params.query = ctx.params.query || {};

					if (ctx.params.query.name) {
						return ctx
							.call("alimentation.products-translations.find", {
								search: ctx.params.query.name,
								searchFields: "name",
								fields: "id,product_id",
								withRelated: false,
							})
							.then((res) => {
								delete ctx.params.query.name;
								ctx.params.query.product_id = res.map((pt) => pt.product_id);
							});
					}

					if (ctx.params.query.stock_lower === "true") {
						ctx.params.extraQuery = (eq) => {
							eq.innerJoin(Knex.raw("product as p"), "p.id", "stock.product_id");
							eq.where("quantity", "<", Knex.raw("p.minimal_stock"));
						};
					}
					delete ctx.params.query.stock_lower;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#user.id",
				],
			},
			visibility: "published",
		},
		grouped: {
			rest: "GET /grouped",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
					"#user.id",
				],
			},
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			visibility: "published",
			async handler(ctx) {
				if (!ctx.meta.alimentation_entity_id) {
					throw new Errors.ForbiddenError(
						"Need send fiscal entity Id",
						"NEED_SEND_FISCAL_ENTITY_ID",
						{},
					);
				}
				ctx.params.query = ctx.params.query || {};

				if (!ctx.params.limit) {
					ctx.params.limit = 10;
				}
				if (!ctx.params.offset) {
					ctx.params.offset = 0;
				}

				this.adapter.connect();
				const q = this.adapter
					.db(this.adapter.table)
					.select("product_id", "wharehouse_id", Knex.raw("sum(quantity) as quantity"))
					.offset(ctx.params.offset)
					.limit(ctx.params.limit)
					.groupBy("product_id", "wharehouse_id");

				const qCount = this.adapter
					.db(this.adapter.table)
					.as(this.adapter.table)
					.select("product_id", "wharehouse_id", Knex.raw("sum(quantity) as quantity"))
					.as("q1table")
					.groupBy("product_id", "wharehouse_id");

				if (ctx.params.query) {
					if (ctx.params.query.wharehouse_id) {
						const wharehouse_access = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
						if (Array.isArray(ctx.params.query.wharehouse_id)) {
							for (let wh_id of ctx.params.query.wharehouse_id) {
								if (!wharehouse_access.includes(Number.parseInt(wh_id)))
									throw new Errors.ValidationError(
										"No permission to access this wharehouse",
										"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
										{},
									);
							}
						} else {
							if (!wharehouse_access.includes(Number.parseInt(ctx.params.query.wharehouse_id)))
								throw new Errors.ValidationError(
									"No permission to access this wharehouse",
									"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
									{},
								);
						}
					} else {
						ctx.params.query.wharehouse_id = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
					}

					if (ctx.params.query.stock_lower === "true") {
						qCount.innerJoin(Knex.raw("product as p"), "p.id", "stock.product_id");
						qCount.where("quantity", "<", Knex.raw("p.minimal_stock"));
						q.innerJoin(Knex.raw("product as p"), "p.id", "stock.product_id");
						q.where("quantity", "<", Knex.raw("p.minimal_stock"));
					}
					delete ctx.params.query.stock_lower;

					if (ctx.params.query.wharehouse_id) {
						if (Array.isArray(ctx.params.query.wharehouse_id)) {
							qCount.whereIn("wharehouse_id", ctx.params.query.wharehouse_id);
							q.whereIn("wharehouse_id", ctx.params.query.wharehouse_id);
						} else {
							qCount.where("wharehouse_id", ctx.params.query.wharehouse_id);
							q.where("wharehouse_id", ctx.params.query.wharehouse_id);
						}
					}

					if (ctx.params.query.product_id) {
						qCount.where("product_id", ctx.params.query.product_id);
						q.where("product_id", ctx.params.query.product_id);
					}
				}

				return q
					.then(async (res) => {
						const product_ids = res.map((r) => r.product_id);
						return ctx
							.call("alimentation.products.find", {
								query: {
									id: product_ids,
								},
								withRelated: "translations,unit",
							})
							.then((products) => {
								return res.map((r) => {
									const product = products.find((p) => p.id === r.product_id);
									r.product = product;
									return r;
								});
							});
					})
					.then(async (res) => {
						const wharehouse_ids = res.map((r) => r.wharehouse_id);
						return ctx
							.call("alimentation.wharehouses.find", {
								query: {
									id: wharehouse_ids,
								},
								withRelated: "translation,unit",
							})
							.then((wharehouses) => {
								return res.map((r) => {
									const wharehouse = wharehouses.find((p) => p.id === r.wharehouse_id);
									r.wharehouse = wharehouse;
									return r;
								});
							});
					})
					.then((res) => {
						return this.adapter
							.db(this.adapter.table)
							.count()
							.from(qCount)
							.as("qtable")
							.then((total) => {
								return {
									total: total[0].count,
									// Page
									page: (ctx.params.offset / total[0].count) * ctx.params.limit,
									// Page size
									pageSize: ctx.params.limit,
									// Total pages
									totalPages: ctx.params.limit
										? Math.floor((total[0].count + ctx.params.limit - 1) / ctx.params.limit)
										: 1,
									rows: res,
								};
							});
					});
			},
		},
		/* DONT USE THIS ACTION USE THE `create_movement` */
		create: {
			visibility: "public", // TODO CHANGE TO PRIVATE WHEN SAFE
		},
		create_movement: {
			rest: "POST /",
			params: {
				operation: { type: "enum", values: ["in", "out", "transfer"] },
				wharehouse_id: { type: "number", positive: true, integer: true, convert: true },
				to_wharehouse_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true,
				},
				transfer_wharehouse_id: {
					type: "number",
					optional: true,
					positive: true,
					integer: true,
					convert: true,
				}, // JUST FOR RECURSIVE CALL
				product_id: { type: "number", positive: true, integer: true, convert: true },
				quantity: { type: "number", convert: true },
				lote: { type: "string", optional: false, nullable: true },
				stock_reason: { type: "string", optional: false, nullable: true },
				expired: { type: "date", convert: true, optional: true, nullable: true },
			},
			async handler(ctx) {
				const wharehouse_access = await ctx.call(
					"alimentation.wharehouses-users.user_wharehouse_access",
					{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
				);
				if (
					!wharehouse_access.includes(Number.parseInt(ctx.params.wharehouse_id)) ||
					(ctx.params.to_wharehouse_id &&
						!wharehouse_access.includes(Number.parseInt(ctx.params.to_wharehouse_id)))
				)
					throw new Errors.ValidationError(
						"No permission to access this wharehouse",
						"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
						{},
					);

				const existLote = await ctx.call("alimentation.stocks.find", {
					query: {
						product_id: ctx.params.product_id,
						wharehouse_id: ctx.params.wharehouse_id,
						lote: ctx.params.lote,
					},
				});
				let stock = null;
				if (existLote.length === 0) {
					stock = await ctx
						.call("alimentation.stocks.create", {
							lote: ctx.params.lote,
							quantity: 0,
							product_id: ctx.params.product_id,
							wharehouse_id: ctx.params.wharehouse_id,
							expired: ctx.params.expired,
						})
						.then((res) => {
							return res[0];
						});
				} else {
					stock = existLote[0];
				}

				if (ctx.params.operation === "in") {
					return ctx.call("alimentation.stocks.updateQuantity", {
						id: stock.id,
						operation: "in",
						quantity: ctx.params.quantity,
						stock_reason: ctx.params.stock_reason,
						transfer_wharehouse_id: ctx.params.transfer_wharehouse_id,
					});
				} else if (ctx.params.operation === "out") {
					if (existLote[0].quantity < ctx.params.quantity) {
						throw new Errors.ValidationError(
							"There is no amount available",
							"STOCK_NO_QUANTITY_AVAILABLE",
							{},
						);
					}

					return ctx.call("alimentation.stocks.updateQuantity", {
						id: stock.id,
						operation: "out",
						quantity: ctx.params.quantity,
						stock_reason: ctx.params.stock_reason,
						transfer_wharehouse_id: ctx.params.transfer_wharehouse_id,
					});
				} else if (ctx.params.operation === "transfer") {
					if (ctx.params.wharehouse_id === ctx.params.to_wharehouse_id)
						throw new Errors.ValidationError(
							"The wharehouse of destiny must be different of the origin",
							"INVALID_TRANSFER_WHAREHOUSE",
							{},
						);

					await ctx.call("alimentation.wharehouses.get", {
						id: ctx.params.to_wharehouse_id,
					});

					return ctx
						.call("alimentation.stocks.create_movement", {
							operation: "out",
							wharehouse_id: ctx.params.wharehouse_id,
							transfer_wharehouse_id: ctx.params.to_wharehouse_id,
							product_id: ctx.params.product_id,
							quantity: ctx.params.quantity,
							lote: ctx.params.lote,
							stock_reason: ctx.params.stock_reason,
							expired: ctx.params.expired,
						})
						.then((res) => {
							return ctx
								.call("alimentation.stocks.create_movement", {
									operation: "in",
									wharehouse_id: ctx.params.to_wharehouse_id,
									transfer_wharehouse_id: ctx.params.wharehouse_id,
									product_id: ctx.params.product_id,
									quantity: ctx.params.quantity,
									lote: ctx.params.lote,
									stock_reason: ctx.params.stock_reason,
									expired: ctx.params.expired,
								})
								.catch((err) => {
									return ctx
										.call("alimentation.stocks.create_movement", {
											operation: "out",
											wharehouse_id: ctx.params.to_wharehouse_id,
											product_id: ctx.params.product_id,
											quantity: ctx.params.quantity,
											lote: ctx.params.lote,
											stock_reason: ctx.params.stock_reason,
											expired: ctx.params.expired,
										})
										.then(() => {
											return ctx
												.call("alimentation.stocks.create_movement", {
													operation: "in",
													wharehouse_id: ctx.params.wharehouse_id,
													product_id: ctx.params.product_id,
													quantity: ctx.params.quantity,
													lote: ctx.params.lote,
													stock_reason: ctx.params.stock_reason,
													expired: ctx.params.expired,
												})
												.then(() => err);
										});
								});
						})
						.catch((err) => {
							return ctx
								.call("alimentation.stocks.create_movement", {
									operation: "in",
									wharehouse_id: ctx.params.wharehouse_id,
									product_id: ctx.params.product_id,
									quantity: ctx.params.quantity,
									lote: ctx.params.lote,
									stock_reason: ctx.params.stock_reason,
									expired: ctx.params.expired,
								})
								.then(() => err);
						});
				} else {
					throw new Errors.ValidationError(
						"The operation requested is not valid",
						"STOCK_INVALID_OPERATION",
						{},
					);
				}
			},
		},
		available_lotes: {
			rest: "GET /lotes",
			scope: "alimentation:stocks:read",
			cache: {
				keys: ["product_id", "wharehouse_id"],
			},
			params: {
				product_id: { type: "number", positive: true, integer: true, convert: true },
				wharehouse_id: { type: "number", positive: true, integer: true, convert: true },
			},
			handler(ctx) {
				return this.adapter
					.raw("SELECT lote, quantity FROM stock WHERE product_id = ? AND wharehouse_id = ?", [
						ctx.params.product_id,
						ctx.params.wharehouse_id,
					])
					.then((result) => {
						return result.rows.map((r) => ({ lote: r.lote, quantity: r.quantity }));
					});
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		getStockOperations: {
			params: {
				wharehouse_id: { type: "number", positive: true, integer: true, convert: true },
			},
			async handler(ctx) {
				let resp = [];
				const stocksWharehouse = await this._find(ctx, {
					query: {
						wharehouse_id: ctx.params.wharehouse_id,
					},
				});
				for (const stocks of stocksWharehouse) {
					const operationInfo = await ctx.call("alimentation.stocks-operations.find", {
						query: {
							stock_id: stocks.id,
						},
					});
					for (const operation of operationInfo) {
						operation.stock = stocks;
						resp.push(operation);
					}
				}
				return resp;
			},
		},

		getOperationsStocks: {
			rest: "GET /:id/operations",
			params: {
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			scope: "alimentation:stocks:read",
			visibility: "published",
			async handler(ctx) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["stock_id"] = ctx.params.id;
				ctx.params.withRelated = "stock";
				return await ctx.call("alimentation.stocks-operations.list", ctx.params);
			},
		},

		getStockLote: {
			rest: "GET /lote/:lote",
			params: {
				lote: { type: "string" },
			},
			scope: "alimentation:stocks:read",
			visibility: "published",
			async handler(ctx) {
				return await this._find(ctx, {
					query: {
						lote: ctx.params.lote,
					},
				});
			},
		},

		getStocksExpired: {
			rest: "GET /stocks/expired",
			params: {
				product_id: { type: "number", optional: true, convert: true },
				wharehouse_id: { type: "number", optional: true, convert: true },
			},
			scope: "alimentation:stocks:read",
			visibility: "published",
			async handler(ctx) {
				let resp = [];
				const actualDate = new Date();
				const stocks_list_expired = await this._find(ctx, {
					query: (qb) => {
						qb.where("expired", "<=", actualDate);
						if (ctx.params.product_id) {
							qb.andWhere("product_id", "=", ctx.params.product_id);
						}
						if (ctx.params.wharehouse_id) {
							qb.andWhere("wharehouse_id", "=", ctx.params.wharehouse_id);
						}
					},
				});
				for (const stock of stocks_list_expired) {
					const stock_info = await this._get(ctx, { id: stock.id });
					resp.push(stock_info[0]);
				}
				return resp;
			},
		},

		updateQuantity: {
			rest: "PUT /:id/quantity",
			params: {
				id: { type: "number", convert: true },
				operation: { type: "enum", values: ["in", "out", "transfer"] },
				quantity: { type: "number", convert: true },
				stock_reason: { type: "string", optional: true },
				transfer_wharehouse_id: { type: "number", convert: true, optional: true },
			},
			scope: "alimentation:stocks:update",
			visibility: "published",
			async handler(ctx) {
				const stock = await this._get(ctx, { id: ctx.params.id });
				const quantityBefore = parseFloat(stock[0].quantity);
				let quantity;
				if (ctx.params.operation === "out") {
					quantity = parseFloat(stock[0].quantity) - parseFloat(ctx.params.quantity);
				} else {
					quantity = parseFloat(stock[0].quantity) + parseFloat(ctx.params.quantity);
				}
				stock[0].quantity = quantity;
				await this._update(ctx, stock[0]);
				const entities = {
					user_id: ctx.meta.user.id,
					device_id: ctx.meta.device ? ctx.meta.device.id : null,
					stock_id: parseInt(ctx.params.id),
					operation: ctx.params.operation,
					quantity: ctx.params.quantity,
					quantity_before: quantityBefore,
					quantity_after: quantity,
					lote: stock[0].lote,
					product_id: stock[0].product_id,
					transfer_wharehouse_id: ctx.params.transfer_wharehouse_id,
					wharehouse_id: stock[0].wharehouse_id,
					stock_reason: ctx.params.stock_reason ? ctx.params.stock_reason : null,
					created_at: new Date(),
					updated_at: new Date(),
				};
				await ctx.call("alimentation.stocks-operations.insert", {
					entity: entities,
				});
				return await this._get(ctx, { id: ctx.params.id });
			},
		},

		remove_stock: {
			params: {
				id: { type: "number", integer: true, convert: true },
				quantity: { type: "number", convert: true },
				order_id: { type: "number", convert: true },
				order_line_id: { type: "number", convert: true },
			},
			async handler(ctx) {
				const stock = await this._get(ctx, { id: ctx.params.id, withRelated: false });

				const quantity_updated = stock[0].quantity - ctx.params.quantity;
				const operation = {
					quantity: ctx.params.quantity,
					user_id: ctx.meta.user.id,
					device_id: ctx.meta.device ? ctx.meta.device.id : null,
					order_id: ctx.params.order_id,
					order_line_id: ctx.params.order_line_id,
					stock_id: ctx.params.id,
					operation: "out",
					quantity_before: stock[0].quantity,
					quantity_after: quantity_updated,
					lote: stock[0].lote,
					stock_reason: "Venda",
					product_id: stock[0].product_id,
					wharehouse_id: stock[0].wharehouse_id,
					created_at: new Date(),
					updated_at: new Date(),
				};
				stock[0].quantity = quantity_updated;
				await ctx.call("alimentation.stocks-operations.create", operation);
				return await this._update(ctx, stock[0]);
			},
		},
		stock_report: {
			rest: "GET /inventoryReport",
			scope: "alimentation:stocks:read",
			params: {
				wharehouse_id: { type: "number", positive: true, integer: true, convert: true },
				minimal_stock: { type: "boolean", optional: true, default: false, convert: true },
			},
			async handler(ctx) {
				const wharehouse = await ctx
					.call("alimentation.wharehouses.get", {
						id: ctx.params.wharehouse_id,
					})
					.then((res) => res[0]);
				const stocks = await this.adapter
					.raw(
						`SELECT pt.name, s.lote, s.expired, s.quantity, u.acronym, p.minimal_stock
				FROM public.stock as s
				INNER JOIN product as p on p.id = s.product_id
				INNER JOIN product_translation as pt on pt.product_id = p.id and pt.language_id = 3
				INNER JOIN unit as u on u.id = p.unit_id
				WHERE s.quantity != 0 AND s.wharehouse_id = ? ${
					ctx.params.minimal_stock ? "AND s.quantity < p.minimal_stock" : ""
				}`,
						[ctx.params.wharehouse_id],
					)
					.then((res) => res.rows);

				const readable = new Stream.Readable();
				readable.push(
					JSON.stringify({
						key: "FOOD_WHAREHOUSE_STOCK_REPORT",
						options: {
							convertTo: "xlsx",
						},
						data: {
							stocks,
							wharehouse,
							date: moment().format("YYYY-MM-DD HH:mm:ss"),
							user: {
								name: ctx.meta.user.name,
							},
						},
					}),
				);
				// no more data
				readable.push(null);
				return ctx.call("reports.templates.printFromStream", readable);
			},
		},
	},

	/**
	 * Events
	 */
	events: {
		"alimentation.wharehouses-users.*"(ctx) {
			this.logger.info("CAPTURED EVENT => wharehouses-user");
			this.clearCache();
		},
		"alimentation.stocks.clearCache"(ctx) {
			this.logger.info("CAPTURED EVENT => alimentation.stocks.clearCache");
			this.clearCache();
		},
	},

	/**
	 * Methods
	 */
	methods: {
		async validateWharehouse(ctx) {
			await ctx.call("alimentation.wharehouses.get", {
				id: ctx.params.wharehouse_id,
			});

			const wharehouse_access = await ctx.call(
				"alimentation.wharehouses-users.user_wharehouse_access",
				{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
			);

			if (!wharehouse_access.includes(ctx.params.wharehouse_id))
				throw new Errors.ValidationError(
					"No permission to access this wharehouse",
					"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
					{},
				);
		},
		async validateProduct(ctx) {
			await ctx.call("alimentation.products.get", {
				id: ctx.params.product_id,
			});
		},
		async validateLote(ctx) {
			const stock = await this._find(ctx, {
				query: {
					product_id: ctx.params.product_id,
					wharehouse_id: ctx.params.wharehouse_id,
					lote: ctx.params.lote,
				},
			});
			if (stock.length !== 0) {
				throw new Errors.ValidationError(
					"Already exist stock for this lote",
					"STOCK_ALREADY_EXIST_FOR_THIS_LOTE",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
