"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.stocks-operations",
	table: "stock_operation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "stocks")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"order_id",
			"order_line_id",
			"user_id",
			"device_id",
			"stock_id",
			"operation",
			"quantity",
			"quantity_before",
			"quantity_after",
			"lote",
			"product_id",
			"wharehouse_id",
			"transfer_wharehouse_id",
			"stock_reason",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			stock(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.stocks", "stock", "stock_id");
			},
			product(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.products", "product", "product_id");
			},
			device(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"configuration.devices",
					"device",
					"device_id",
					"id",
					{},
					"name",
					false,
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id", "id", {}, "name", false);
			},
			wharehouse(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.wharehouses", "wharehouse", "wharehouse_id");
			},
			transferWharehouse(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"alimentation.wharehouses",
					"transferWharehouse",
					"transfer_wharehouse_id",
				);
			},
		},
		entityValidator: {
			order_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			order_line_id: {
				type: "number",
				positive: true,
				integer: true,
				optional: true,
				nullable: true,
			},
			user_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			device_id: { type: "number", positive: true, integer: true, optional: true, nullable: true },
			stock_id: { type: "number", positive: true, integer: true },
			operation: { type: "enum", values: ["in", "out", "transfer"] },
			quantity: { type: "number", convert: true },
			quantity_before: { type: "number", convert: true },
			quantity_after: { type: "number", convert: true },
			transfer_wharehouse_id: { type: "number", positive: true, integer: true, optional: true },
			wharehouse_id: { type: "number", positive: true, integer: true },
			lote: { type: "string" },
			product_id: { type: "number", positive: true, integer: true },
			stock_reason: { type: "string", optional: true, nullable: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					if (ctx.meta.device) ctx.params.device_id = ctx.meta.device.id;

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function sanatizeQuery(ctx) {
					if (!ctx.meta.alimentation_entity_id) {
						throw new Errors.ForbiddenError(
							"Need send fiscal entity Id",
							"NEED_SEND_FISCAL_ENTITY_ID",
							{},
						);
					}
					ctx.params.query = ctx.params.query || {};

					if (ctx.params.query.wharehouse_id) {
						const wharehouse_access = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
						if (Array.isArray(ctx.params.query.wharehouse_id)) {
							for (let wh_id of ctx.params.query.wharehouse_id) {
								if (!wharehouse_access.includes(Number.parseInt(wh_id)))
									throw new Errors.ValidationError(
										"No permission to access this wharehouse",
										"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
										{},
									);
							}
						} else {
							if (!wharehouse_access.includes(Number.parseInt(ctx.params.query.wharehouse_id)))
								throw new Errors.ValidationError(
									"No permission to access this wharehouse",
									"NO_PERMISSION_TO_ACCESS_WHAREHOUSE",
									{},
								);
						}
					} else
						ctx.params.query.wharehouse_id = await ctx.call(
							"alimentation.wharehouses-users.user_wharehouse_access",
							{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
						);
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "public",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "public",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "public",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "public",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
