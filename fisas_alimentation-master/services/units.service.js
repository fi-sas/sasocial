"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addSearchRelation } = require("@fisas/ms_core").Helpers.SearchRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.units",
	table: "unit",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "units")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "acronym", "active", "created_at", "updated_at"],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.units-translations",
					"translations",
					"id",
					"unit_id",
				);
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				item: {
					language_id: { type: "number", positive: true, integer: true, convert: true },
					name: { type: "string" },
				},
			},
			acronym: { type: "string", max: 5 },
			active: { type: "boolean" },
		},
	},
	hooks: {
		before: {
			list: [
				function updateSearch(ctx) {
					return addSearchRelation(ctx, ["name"], "alimentation.units-translations", "unit_id");
				},
			],
			create: [
				"validateLanguageIds",
				function sanatize(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				"validateLanguageIds",
				function sanatize(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			patch: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			// CONFIRMAR O QUE VAI SER FEITO AQUI
			// remove: [
			// 	"remove_translations"
			// ]
		},
		after: {
			create: ["saveTranslations"],
			update: ["saveTranslations"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"limit",
					"offset",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Languages
		 */
		async validateLanguageIds(ctx) {
			const ids = ctx.params.translations.map((translation) => translation.language_id);
			for (const id of ids) {
				await ctx.call("configuration.languages.get", { id });
			}
		},

		/**
		 * Method for save Unit Translations
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.units-translations.save_translations", {
				unit_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.units-translations.find", {
				query: {
					unit_id: res[0].id,
				},
			});

			return res;
		},

		/**
		 * Method for remove Unit Translations
		 */
		async remove_translations(ctx) {
			return await ctx.call("alimentation.units-translations.remove_translations", {
				unit_id: parseInt(ctx.params.id),
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
