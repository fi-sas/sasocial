"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.users-allergens",
	table: "user_allergen",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "users-allergens")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "allergen_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			allergen_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		removeByUser: {
			params: {
				user_id: { type: "number" },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					user_id: ctx.params.user_id,
				});
				this.clearCache();
			},
		},

		allergensInfo: {
			cache: {
				keys: ["user_id"],
			},
			params: {
				user_id: { type: "number" },
			},
			async handler(ctx) {
				const users_allergens = await this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
					},
				});
				return ctx.call("alimentation.allergens.get", {
					id: users_allergens.map((a) => a.allergen_id),
				});
			},
		},
		returnsConfig: {
			cache: {
				keys: ["user_id", "allergens"],
			},
			params: {
				user_id: { type: "number", positive: true },
				allergens: { type: "array" },
			},
			async handler(ctx) {
				let userAllergens = [];
				let resp = [];
				const AssociationUserAllergens = await this._find(ctx, {
					query: {
						user_id: ctx.params.user_id,
					},
				});
				userAllergens = await ctx.call("alimentation.allergens.get", {
					id: AssociationUserAllergens.map((a) => a.allergen_id),
				});

				if (!ctx.params.allergens) {
					ctx.params.allergens = await ctx.call("alimentation.allergens.find");
				}

				for (const allergen of ctx.params.allergens) {
					const userAlllergic = userAllergens.find((ua) => ua.id === allergen.id);

					const info = {
						id: allergen.id,
						translations: allergen.translations,
						allergic: !!userAlllergic,
					};
					resp.push(info);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
