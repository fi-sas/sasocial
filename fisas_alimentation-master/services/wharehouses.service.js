"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.wharehouses",
	table: "wharehouse",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "wharehouses")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "active", "fentity_id", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			stocks(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.stocks", "stocks", "id", "wharehouse_id");
			},
			services(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.services", "services", "id", "wharehouse_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean", default: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				addEntity,
				"validateProductEntity",
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			get: ["validateProductEntity"],
			list: [addQueryFentity],
			getOperations: ["validateProductEntity"],
			getUsers: ["validateProductEntity"],
			addUsers: ["validateProductEntity"],
			removeUsers: ["validateProductEntity"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#alimentation_entity_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#alimentation_entity_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		getOperations: {
			rest: "GET /:id/operations",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:wharehouses:read",
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("alimentation.stocks.getStockOperations", {
					wharehouse_id: parseInt(ctx.params.id),
				});
			},
		},

		getUsers: {
			rest: "GET /:id/users",
			params: {
				id: { type: "number", convert: true },
			},
			scope: "alimentation:wharehouses:read",
			visibility: "published",
			async handler(ctx) {
				return await ctx.call("alimentation.wharehouses-users.getUsersInfo", {
					wharehouse_id: parseInt(ctx.params.id),
				});
			},
		},

		addUsers: {
			rest: "POST /:id/users/:user_id",
			params: {
				id: { type: "number", convert: true },
				user_id: { type: "number", convert: true },
			},
			scope: "alimentation:wharehouses:create",
			visibility: "published",
			async handler(ctx) {
				const user_info = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
				if (user_info[0].active === false) {
					throw new Errors.ValidationError("The user is not active", "USER_NOT_ACTIVE", {});
				}
				const entities = {
					created_by_user_id: parseInt(ctx.meta.user.id),
					user_id: parseInt(ctx.params.user_id),
					wharehouse_id: parseInt(ctx.params.id),
				};
				await ctx.call("alimentation.wharehouses-users.insert", { entity: entities });
				ctx.emit("alimentation.stocks.clearCache");
				return user_info;
			},
		},

		removeUsers: {
			rest: "DELETE /:id/users/:user_id",
			params: {
				id: { type: "number", convert: true },
				user_id: { type: "number", convert: true },
			},
			scope: "alimentation:wharehouses:delete",
			visibility: "published",
			handler(ctx) {
				return ctx
					.call("alimentation.wharehouses-users.removeWharehouseUser", {
						wharehouse_id: parseInt(ctx.params.id),
						user_id: parseInt(ctx.params.user_id),
					})
					.then((res) => {
						ctx.emit("alimentation.stocks.clearCache");
						return res;
					});
			},
		},

		getWarehousesUserAcess: {
			rest: "GET /user_access",
			scope: "alimentation:wharehouses:read",
			visibility: "published",
			async handler(ctx) {
				if (!ctx.meta.alimentation_entity_id) {
					throw new Errors.ForbiddenError(
						"Need send fiscal entity Id",
						"NEED_SEND_FISCAL_ENTITY_ID",
						{},
					);
				}
				const wharehouse_user_access = await ctx.call(
					"alimentation.wharehouses-users.user_wharehouse_access",
					{ entity_id: ctx.meta.alimentation_entity_id, user_id: ctx.meta.user.id },
				);
				return ctx.call("alimentation.wharehouses.find", {
					query: { id: wharehouse_user_access },
					withRelated: "services",
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for validate Product by Entity
		 */
		async validateProductEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The wharehouse not corresponde the entity.",
						"WHAREHOUSE_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError("You need send Entity Id", "NO_ENTITY_ID", {});
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
