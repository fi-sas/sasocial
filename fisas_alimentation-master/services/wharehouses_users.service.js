"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.wharehouses-users",
	table: "wharehouse_user",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "wharehouses-users")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "user_id", "created_by_user_id", "manager", "wharehouse_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true },
			created_by_user_id: { type: "number", positive: true, integer: true },
			manager: { type: "boolean", default: false },
			wharehouse_id: { type: "number", positive: true, integer: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getUsersInfo: {
			visibility: "published",
			params: {
				wharehouse_id: { type: "number" },
			},
			async handler(ctx) {
				let resp = [];
				const associations = await this._find(ctx, {
					query: {
						wharehouse_id: ctx.params.wharehouse_id,
					},
				});
				for (const association of associations) {
					const user = await ctx.call("authorization.users.get", { id: association.user_id });
					resp.push(user[0]);
				}
				return resp;
			},
		},

		removeWharehouseUser: {
			visibility: "published",
			params: {
				wharehouse_id: { type: "number" },
				user_id: { type: "number" },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					wharehouse_id: ctx.params.wharehouse_id,
					user_id: ctx.params.user_id,
				});
				return true;
			},
		},
		user_wharehouse_access: {
			params:{
				user_id: { type: "number", integer: true, convert: true },
				entity_id: { type: "number", integer: true, convert: true },
			},
			handler(ctx) {
				return this.adapter.raw("Select W.id as id from wharehouse as W Inner Join wharehouse_user as WU on w.id = WU.wharehouse_id where W.fentity_id = ? And WU.user_id = ?", [ctx.params.entity_id, ctx.params.user_id]).then((res) =>
					res.rows.map((wh) => wh.id)
				);
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
