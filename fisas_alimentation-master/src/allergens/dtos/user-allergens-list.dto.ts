import { ApiModelProperty } from '@nestjs/swagger';
import {AllergenTranslation} from '../entities/allergen-translation.entity';
import {Allergen} from '../entities/allergen.entity';

export class UserAllergensListDto {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    translations: AllergenTranslation[];

    @ApiModelProperty()
    allergic: boolean;

    static fromAllergen(allergen: Allergen, active: boolean): UserAllergensListDto {
        const ual = new UserAllergensListDto();
        ual.id = allergen.id;
        ual.translations = allergen.translations;
        ual.allergic = active;

        return ual;
    }
}
