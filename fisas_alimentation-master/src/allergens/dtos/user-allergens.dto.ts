import { ApiModelProperty } from '@nestjs/swagger';
import {IsArray, IsNumber} from 'class-validator';

export class UserAllergensDto {
    @ApiModelProperty({ isArray: true, type: Number, default: '[ 1, 2, 3]', description: 'Array of allergen ids'})
    @IsArray({ message: 'allergens should be a array of ids'})
    @IsNumber({}, { each: true, message: 'allergens should be array of numbers'})
    allergens: number[];
}