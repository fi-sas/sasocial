import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn} from 'typeorm';
import {Allergen} from './allergen.entity';

@Entity()
export class UserAllergen {

  @ManyToOne(type => Allergen, a => a.id, { primary: true, nullable: false })
  allergen: Allergen;

  @PrimaryColumn()
  allergenId: number;

  @PrimaryColumn()
  user_id: number;
}