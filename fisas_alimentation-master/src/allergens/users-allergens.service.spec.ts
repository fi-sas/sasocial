import { Test, TestingModule } from '@nestjs/testing';
import { UsersAllergensService } from './users-allergens.service';

describe('UsersAllergensService', () => {
  let service: UsersAllergensService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersAllergensService],
    }).compile();

    service = module.get<UsersAllergensService>(UsersAllergensService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
