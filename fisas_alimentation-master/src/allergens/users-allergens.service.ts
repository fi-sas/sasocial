import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {UserAllergen} from './entities/user-allergen.entity';
import {UserAllergensDto} from './dtos/user-allergens.dto';
import {AllergensService} from './allergens.service';
import {NotFoundException} from '../common/exceptions/not-found.exception';
import {UserAllergensListDto} from './dtos/user-allergens-list.dto';
import {Allergen} from './entities/allergen.entity';

@Injectable()
export class UsersAllergensService {
    constructor(
        private readonly allergenService: AllergensService,
        @InjectRepository(UserAllergen) private readonly userAllergenRepo: Repository<UserAllergen>
    ) {
    }

    async saveConfig(user_id: number, configDto: UserAllergensDto, language_id: number): Promise<UserAllergensListDto[]> {
        const ids = [...new Set(configDto.allergens)];
        const persistedIds = await this.allergenService.existIDs(ids);
        if (ids.length > persistedIds) {
            throw new NotFoundException(404, 'allergen not found');
        }

        return new Promise<UserAllergensListDto[]>(async (resolve, reject) => {
            await this.userAllergenRepo.manager.transaction(async entityManger => {
                await entityManger.delete(UserAllergen, {user_id});
                const userAllergens = ids.map(id => ({user_id, allergenId: id}));
                if (userAllergens.length > 0) {
                    await entityManger.insert(UserAllergen, userAllergens);
                }
            }).then(async () => {
                    resolve(await this.returnsConfig(user_id, language_id));
            }).catch(reason => {
                    reject(reason);
            });
        });
    }

    async returnsConfig(user_id: number, language_id: number, allergens?: Allergen[]): Promise<UserAllergensListDto[]> {
        return new Promise<UserAllergensListDto[]>(async (resolve, reject) => {
            const userAllergens = await this.userAllergenRepo.find({ where: { user_id }});

            if (!allergens) {
                allergens = (await this.allergenService.findAll(0, -1, '-id', null, language_id))[0];
            }

            const result: UserAllergensListDto[] = [];
            for (const allergen of allergens) {
                if (allergen.translations.length > 0) {
                    const findA = userAllergens.find(ua => ua.allergenId === allergen.id);
                    if (findA) {
                        result.push(UserAllergensListDto.fromAllergen(allergen, true));
                    } else {
                        result.push(UserAllergensListDto.fromAllergen(allergen, false));
                    }
                }
            }
            resolve(result);
        });
    }
}
