import { Global, HttpModule, Module } from '@nestjs/common';
import { Logger } from './logger/logger.service';
import { DevicesService } from './services/devices.service';
import { LanguagesService } from './services/languages.service';
import { MediasService } from './services/medias.service';
import { UsersService } from './services/users.service';
import { JwtModule } from '@nestjs/jwt';
import { TaxesService } from './services/taxes.service';
import { MovementsService } from './services/movements.service';
import { CartService } from './services/cart/cart.service';
import { OrderService } from './services/order/order.service';

@Global()
@Module({
  imports: [
    JwtModule.register({}),
    HttpModule,
  ],
  providers: [
    Logger,
    DevicesService,
    LanguagesService,
    MediasService,
    UsersService,
    TaxesService,
    MovementsService,
    CartService,
    OrderService,
  ],
  exports: [
    Logger,
    DevicesService,
    LanguagesService,
    MediasService,
    UsersService,
    TaxesService,
    MovementsService,
    OrderService,
    CartService,
    HttpModule,
    JwtModule
  ],
})
export class CommomModule {}