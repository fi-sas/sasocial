import { createParamDecorator } from '@nestjs/common';

export const DeviceID = createParamDecorator((data, req) => {
  return req.device;
});