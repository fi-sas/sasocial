import { createParamDecorator } from '@nestjs/common';

export const isGuest = createParamDecorator((data, req) => {
  return req.hasOwnProperty('isGuest') ? req.isGuest : false;
});
