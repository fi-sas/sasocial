import { ReflectMetadata } from '@nestjs/common';

export const Scopes = (...scopes: string[]) => ReflectMetadata('scopes', scopes);