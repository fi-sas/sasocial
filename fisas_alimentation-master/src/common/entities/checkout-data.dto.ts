export class CheckoutDataItemDTO {
    id: number;
    order_id: number;
    service_id: number;
    product_code: string;
    article_type: string;
    name: string;
    description: string;
    location: string;
    extra_info: any;
    quantity: number;
    vat_id: number;
    vat: number;
    liquid_value: number;
    vat_value: number;
    unit_value: number;
    total_value: number;
    status: string;
    erp_budget: string;
    erp_sncap: string;
    erp_income_cost_center: string;
    erp_funding_source: string;
    erp_program: string;
    erp_measure: string;
    erp_project: string;
    erp_activity: string;
    erp_action: string;
    erp_functional_classifier: string;
    erp_organic: string;
    updated_at: string;
    created_at: string;
}

export class CheckoutDataDTO {
    id: number;
    account_id: number;
    user_id: number;
    payment_id: number;
    cancel_payment_id: number;
    value: number;
    status: string;
    items: CheckoutDataItemDTO[];
    updated_at: string;
    created_at: string;
}