import {Meal} from '../../menus/entities/menu.entity';

export class ReservationPackDto {
  id: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  date: string;
  service_id: number;
  dish_type_id: number;
  meal: Meal;
  menuDish_id: number;
  purchased_pack: any;
  purchasedPackId: number;
  can_cancel: boolean;
 }