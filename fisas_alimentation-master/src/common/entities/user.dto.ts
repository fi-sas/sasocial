export class UserDto {
  id: number;
  rfid: string;
  name: string;
  email: string;
  phone: string;
  user_name: string;
  institute: string;
  profile_id: number;
  student_number: number;
  external: boolean;
  active: boolean;
  updated_at: Date;
  created_at: Date;
  manager: boolean; // This is relative to the entity users
}
