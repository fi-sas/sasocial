export enum StockTrack {
  NONE = 'none',
  PRODUCT = 'product',
  COMPOSITION = 'composition',
  PRODUCT_COMPOSITION = 'product_composition'
}