import { HttpException, HttpStatus } from '@nestjs/common';
import { ResponseError } from '../response.model';

export class ForbiddenException extends HttpException {

  constructor(code: number, message: string) {
    super([new ResponseError(code, message)], HttpStatus.FORBIDDEN);
  }
}