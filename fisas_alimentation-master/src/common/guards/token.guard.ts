import { Injectable, CanActivate, ExecutionContext, HttpService, Inject } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CONFIGURATIONS } from '../../app.config';
import { RestURLBuilder } from 'rest-url-builder';
import { UnauthorizedException } from '../exceptions/unauthorized.exception';
import { JwtService } from '@nestjs/jwt';
import { ForbiddenException } from '../exceptions/forbidden.exception';
import { FiscalEntityService } from '../../entity/fiscal-entity.service';
import { hasOwnProperty } from 'tslint/lib/utils';

@Injectable()
export class TokenGuard implements CanActivate {
  private urlBuilder = new RestURLBuilder();

  constructor(private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
    private readonly httpService: HttpService,
    private readonly fentitiesService: FiscalEntityService) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    let entity_id: number = 0;
    let user = null;

    if (hasOwnProperty(context.switchToHttp().getRequest().params, 'entity_id')) {
      entity_id = context.switchToHttp().getRequest().params.entity_id;
    }

    if (context.getArgs().length > 0) {
      if (context.getArgByIndex(0).headers.hasOwnProperty('authorization')) {
        let token = context.getArgByIndex(0).headers.authorization;
        token = token.replace('Bearer ', '');
        // CHECK TOKEN VALIDITY
        const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.AUTHORIZATION);
        builder.setNamedParameter('token', token);
        let resultValidity = null;
        try {
          resultValidity = await this.httpService.get(builder.get()).toPromise();
        } catch (error) {
          throw new UnauthorizedException(901, 'Invalid token');
        }

        const jwtData: any = this.jwtService.decode(token, {});

        if (jwtData.hasOwnProperty('device')) {
          context.switchToHttp().getRequest().device = jwtData.device.id;
        }

        let grantedScopes = [];
        if (resultValidity.data.data.length > 0) {

          context.switchToHttp().getRequest().isGuest = jwtData.isGuest;

          if (resultValidity.data.data[0].hasOwnProperty('user')) {
            user = resultValidity.data.data[0].user;
            context.switchToHttp().getRequest().user = resultValidity.data.data[0].user;
          }

          if (resultValidity.data.data[0].hasOwnProperty('scopes')) {
            context.switchToHttp().getRequest().grantedScopes = resultValidity.data.data[0].scopes;
            grantedScopes = resultValidity.data.data[0].scopes;
          }

        }

        if (entity_id) {
          await this.fentitiesService.checkUserPermission(entity_id, user.id);
        }

        return true;

      } else {
        // WITHOUT AUTHORIZATION HEADER
        throw new UnauthorizedException(900, 'No token provided');
      }
    } else {
      // WITHOUT ARGS
      throw new UnauthorizedException(900, 'No token provided');
    }
  }

  convertMethod(methods: any) {
    const keys = Object.keys(methods);
    switch (keys[0]) {
      case 'POST':
        return 'create';
      case 'DELETE':
        return 'delete';
      case 'PUT':
      case 'PATCH':
        return 'update';
      default:
        return 'read';
    }
  }

}
