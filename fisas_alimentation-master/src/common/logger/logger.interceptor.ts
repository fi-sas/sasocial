import { ExecutionContext, Injectable, LoggerService, MiddlewareFunction, NestInterceptor, NestMiddleware } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {

  constructor(private logger: LoggerService) {}

  intercept(context: ExecutionContext, call$: Observable<any>): Observable<any> | Promise<Observable<any>> {
    const now = Date.now();
    return call$.pipe(
      tap(() => {
        const url = context.getArgByIndex(0).url;
        const method = context.getArgByIndex(0).method;
        const statusCode = context.getArgByIndex(0).statusCode;
        // this.logger.log(context.getArgByIndex(0).headers);
        this.logger.log(`[METHOD]: ${method} [URL]: ${url} [CODE]: ${statusCode} [EXECUTION TIME]: ${Date.now() - now}ms`);
      }),
    );
  }
}