import { LoggerService } from '@nestjs/common';
import { configure, getLogger } from 'log4js';

const categories = {
  default: {
    appenders: process.env.NODE_ENV === 'production' ? ['logstash', 'console'] : ['console'],
    level:  process.env.NODE_ENV === 'production' ? 'info' : 'all'
  },
  alimentation: {
    appenders: process.env.NODE_ENV === 'production' ? ['logstash', 'console'] : ['console'],
    level:  process.env.NODE_ENV === 'production' ? 'info' : 'all'
  }
};

configure({
  appenders: {
    logstash: {
      type: 'log4js-logstash-tcp',
      host: 'fisas_logs_logstash',
      port: 5000,
    },
    console: {
      type: 'console'
    }
  },
  categories
});
const logger = getLogger('alimentation');

export class Logger implements LoggerService {

  constructor() {
  }

  log(message: string) {
    logger.info(message);
  }
  error(message: string, trace: string) {
    console.error(message);
    console.error(trace);
    logger.error(message);
    logger.trace(trace);
  }
  warn(message: string) {
    logger.warn(message);
  }
}