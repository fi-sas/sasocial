import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus, HttpException } from '@nestjs/common';
import { ResponseError } from '../response.model';
import moment = require('moment');

@Injectable()
export class ParseDatePipe implements PipeTransform<string, string> {

  transform(value: string, metadata: ArgumentMetadata): string {
    const date = moment(value);
    if (!date.isValid()) {
      throw new HttpException(new ResponseError(123, 'Tha date sent is no valid '), HttpStatus.BAD_REQUEST);
    }
    return date.format();
  }
}