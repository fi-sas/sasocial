import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus, HttpException } from '@nestjs/common';
import { ResponseError } from '../response.model';
import { Meal } from '../../menus/entities/menu.entity';

@Injectable()
export class ParseMealPipe implements PipeTransform<string, Meal> {

  transform(value: string, metadata: ArgumentMetadata): Meal {
    value = value.toUpperCase();
    const meal: Meal = Meal[value];
    if (meal) {
      return meal;
    }
    throw new HttpException(new ResponseError(123, 'the type of meal is not valid'), HttpStatus.BAD_REQUEST);
  }
}