import { OrderDto } from './../order/dto/order.dto';
import { Response } from './../../response.model';
import { CONFIGURATIONS } from './../../../app.config';
import { RestURLBuilder } from 'rest-url-builder';
import { Reflector } from '@nestjs/core';
import { Injectable, HttpService } from '@nestjs/common';
import { first } from 'rxjs/operators';
import { CartDto, CartCheckoutDto } from './dto/cart.dto';

@Injectable()
export class CartService {

    private urlBuilder = new RestURLBuilder();

    constructor(private readonly httpService: HttpService,
      private readonly reflector: Reflector) {
    }
    createCartAndCheckout(cart: CartDto, cartCheckout: CartCheckoutDto, token: string): Promise<OrderDto> {
        const cartBuilder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.CART);
        return new Promise<OrderDto>((resolve, reject) => {
            this.httpService.post<Response<any>>(cartBuilder.get(),
            cart,
            { headers: { Authorization: token } })
            .pipe(first()).subscribe(cartData => {
                const id = cartData.data.data[0].id;
                const cartCheckoutBuilder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.CART_CHECKOUT);
                cartCheckoutBuilder.setNamedParameter('id', id.toString());

                this.httpService.post<Response<any>>(cartCheckoutBuilder.get(),
                cartCheckout,
                { headers: { Authorization: token } })
                .pipe(first()).subscribe(cartCheckoutData => {
                    resolve(cartCheckoutData.data.data[0]);
                }, err => {
                    reject(err);
                });
            }, err => {
                reject(err);
            });
        });
    }
}
