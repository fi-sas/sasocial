export interface CartCheckoutDto {
    tin?: string;
    email?: string;
    address?: string;
    postal_code?: string;
    city?: string;
    country?: string;
    payment_method_id: number;
    payment_method_data: {};
}

export interface CartItemDto {
    service_id: number;
    product_code: string;
    article_type: string;
    name: string;
    description: string;
    extra_info: {};
    quantity: number;
    liquid_value: number;
    vat_id: number;
    location: string;
    erp_budget: string;
    erp_sncap: string;
    erp_income_cost_center: string;
    erp_funding_source: string;
    erp_program: string;
    erp_measure: string;
    erp_project: string;
    erp_activity: string;
    erp_action: string;
    erp_functional_classifier: string;
    erp_organic: string;
}

export interface CartDto {
    account_id: number;
    user_id?: number;
    items: CartItemDto[];
}