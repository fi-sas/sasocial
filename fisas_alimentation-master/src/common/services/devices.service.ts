import { HttpService, Injectable } from '@nestjs/common';
import { DeviceDTO } from '../entities/device.dto';
import { CONFIGURATIONS } from '../../app.config';
import { RestURLBuilder } from 'rest-url-builder';
import { first } from 'rxjs/operators';
import { Reflector } from '@nestjs/core';

@Injectable()
export class DevicesService {
  private urlBuilder = new RestURLBuilder();
  constructor(
    private readonly httpService: HttpService,
    private readonly reflector: Reflector,
  ) {}

  public get(id: number, token: string): Promise<DeviceDTO> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.DEVICE_ID);
    builder.setNamedParameter('id', '' + id);

    return new Promise<DeviceDTO>((resolve, reject) => {
      this.httpService
        .get(builder.get(), { headers: { Authorization: token } })
        .pipe(first())
        .subscribe(
          value => resolve(value.data.data[0]),
          error => {
            if (error.response) {
             reject(error.response.data.errors);
           } else {
             reject(error);
           }
          });
    });
  }
}
