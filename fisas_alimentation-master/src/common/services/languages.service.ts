import { HttpService, Injectable } from '@nestjs/common';
import { RestURLBuilder } from 'rest-url-builder';
import { CONFIGURATIONS } from '../../app.config';
import { first } from 'rxjs/operators';
import { LanguageDTO } from '../entities/language.dto';
import { Response, ResponseError } from '../response.model';
import { isArray } from 'util';
import { AxiosResponse } from 'axios';
import { ValidationException } from '../exceptions/validation.exception';

@Injectable()
export class LanguagesService {

  private urlBuilder = new RestURLBuilder();

  constructor(private readonly httpService: HttpService) {
  }

  public async get(id: number, token: string): Promise<AxiosResponse<Response<LanguageDTO>>> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.LANGUAGE_ID);
    builder.setNamedParameter('id', '' + id);
    return this.httpService.get<Response<LanguageDTO>>(builder.get(),
      { headers: { Authorization: token } })
      .pipe(first()).toPromise();

  }

  public async getByAcronym(acronym: string, token: string): Promise<AxiosResponse<Response<LanguageDTO>>> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.LANGUAGES);
    return this.httpService.get<Response<LanguageDTO>>(builder.get() + '?acronym=' + acronym,
      { headers: { Authorization: token } })
      .pipe(first()).toPromise();

  }

  public async validateBody(body: any, token: string): Promise<boolean> {
    return new Promise<boolean>(async (resolve, reject) => {
      if (body.hasOwnProperty('translations')) {
        if (isArray(body.translations)) {
          for (const translation of body.translations) {

            if (body.translations.filter(trans => trans.language_id === translation.language_id).length > 1) {
              reject([{ message: 'can send repeat language id on translations', code: '400'}]);
            }

            const language = await this.get(translation.language_id, token).catch(reason => {
              reject(reason.response.data.errors);
            });
          }
          resolve(true);
        }
      }
      resolve(true);
    });
  }
}
