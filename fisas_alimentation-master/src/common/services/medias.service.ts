import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Response } from '../response.model';
import { CONFIGURATIONS } from '../../app.config';
import { first } from 'rxjs/operators';
import { MediaDto } from '../entities/media.dto';
import { RestURLBuilder } from 'rest-url-builder';

@Injectable()
export class MediasService {

  private urlBuilder = new RestURLBuilder();

  constructor(private readonly httpService: HttpService) {
  }

  public async get(id: number, token: string): Promise<AxiosResponse<Response<MediaDto>>> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.FILE_ID);
    builder.setNamedParameter('id', '' + id);
    return this.httpService.get<Response<MediaDto>>(builder.get(),
      { headers: { Authorization: token } })
      .pipe(first()).toPromise();

  }

  public async getIds(ids: number[], token: string): Promise<AxiosResponse<Response<MediaDto>>> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.FILE);
    let url: string = builder.get() + '?';
    for (const id of ids) {
      url = url + 'id=' + id + '&';
    }

    return this.httpService.get<Response<MediaDto>>(url,
      { headers: { Authorization: token } })
      .pipe(first()).toPromise();
  }

  public async loadFileRelateds(entities: any[], property_id: string, property_name: string, token: string): Promise<any[]> {
    return new Promise<any[]>(async (resolve, reject) => {
      const mediasIds: number[] = [];
      for (const ent of entities) {
        ent[property_id] ? mediasIds.push(ent[property_id]) : null;
      }

      try {
        const medias = await this.getIds(mediasIds, token);
        for (const ent of entities) {
          const tfile = medias.data.data.find(file => file.id === ent[property_id]);
          ent[property_name] = tfile ? tfile : null;
        }
        resolve(entities);
      }catch (e) {
        resolve(entities);
      }
    });
  }

  public async loadFileRelated(entity: any, property_id: string, property_name: string, token: string): Promise<any> {
    return new Promise<any[]>(async (resolve, reject) => {
      const mediaId: number = entity[property_id];

      if (entity[property_id]) {
        try {
          const medias = await this.get(mediaId, token);
          const tfile = medias.data.data.find(file => file.id === entity[property_id]);
          entity[property_name] = tfile ? tfile : null;
          resolve(entity);
        } catch (e) {
          resolve(entity);
        }
      } else {
          entity[property_name] = null;
          resolve(entity);
      }
    });
  }
}
