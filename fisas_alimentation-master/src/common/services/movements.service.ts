import { Response } from './../response.model';
import { HttpService, Injectable } from '@nestjs/common';
import { RestURLBuilder } from 'rest-url-builder';
import { Reflector } from '@nestjs/core';
import { CONFIGURATIONS } from '../../app.config';
import { first } from 'rxjs/operators';
import { AxiosResponse } from 'axios';

@Injectable()
export class MovementsService {

  private urlBuilder = new RestURLBuilder();

  constructor(private readonly httpService: HttpService,
    private readonly reflector: Reflector) {
  }

  public async createAnnulament(id: number, quantity: number, token: string): Promise<AxiosResponse<Response<any>>> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.MOVEMENT_ITEM_CANCEL);
    builder.setNamedParameter('id', '' + id);
    return this.httpService.post<Response<any>>(builder.get(),
      { quantity },
      { headers: { Authorization: token } })
      .pipe(first()).toPromise();

  }
}
