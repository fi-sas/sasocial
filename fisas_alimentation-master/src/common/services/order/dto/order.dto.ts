import { OrderItemDto } from './order.dto';
import { CartItemDto } from './../../cart/dto/cart.dto';
export enum OrderStatus {
    Pending = 'Pending',
    Paid = 'Paid',
    Cancelled = 'Cancelled',
    Delivered = 'Delivered',
};

export interface OrderItemDto {
    id: number;
    order_id: number;
    service_id: number;
    product_code: string;
    article_type: string;
    name: string;
    description: string;
    location: string;
    extra_info: {};
    quantity: number;
    vat_id: number;
    vat: number;
    liquid_value: number;
    vat_value: number;
    unit_value: number;
    total_value: number;
    status: OrderStatus;
    erp_budget: string;
    erp_sncap: string;
    erp_income_cost_center: string;
    erp_funding_source: string;
    erp_program: string;
    erp_measure: string;
    erp_project: string;
    erp_activity: string;
    erp_action: string;
    erp_functional_classifier: string;
    erp_organic: string;
    updated_at: string;
    created_at: string;
}

export interface OrderDto {
    id: number;
    account_id: number;
    user_id: number;
    payment_id: number;
    cancel_payment_id: number;
    value: number;
    status: OrderStatus;
    items: OrderItemDto[];
    updated_at: string;
    created_at: string;
}

export function orderItemToCartItem(orderItem: OrderItemDto, quantity?: number): CartItemDto {

    return {
        service_id: orderItem.service_id,
        product_code: orderItem.product_code,
        article_type: orderItem.article_type,
        name: orderItem.name,
        description: orderItem.description,
        extra_info: orderItem.extra_info,
        quantity: quantity ? quantity : orderItem.quantity,
        liquid_value: quantity ? ((orderItem.liquid_value / orderItem.quantity) * quantity) : orderItem.liquid_value,
        vat_id: orderItem.vat_id,
        location: orderItem.location,
        erp_budget: orderItem.erp_budget,
        erp_sncap: orderItem.erp_sncap,
        erp_income_cost_center: orderItem.erp_income_cost_center,
        erp_funding_source: orderItem.erp_funding_source,
        erp_program: orderItem.erp_program,
        erp_measure: orderItem.erp_measure,
        erp_project: orderItem.erp_project,
        erp_activity: orderItem.erp_activity,
        erp_action: orderItem.erp_action,
        erp_functional_classifier: orderItem.erp_functional_classifier,
        erp_organic: orderItem.erp_organic
    };
}