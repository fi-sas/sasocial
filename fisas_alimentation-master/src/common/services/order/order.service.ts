import { OrderDto, OrderItemDto } from './dto/order.dto';
import { AxiosResponse } from 'axios';
import { CONFIGURATIONS } from './../../../app.config';
import { RestURLBuilder } from 'rest-url-builder';
import { Injectable, HttpService } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { first } from 'rxjs/operators';
import { Response } from 'common/response.model';
import { Observable } from 'rxjs';

@Injectable()
export class OrderService {

    private urlBuilder = new RestURLBuilder();

    constructor(private readonly httpService: HttpService,
      private readonly reflector: Reflector) {
    }

    getOrder(id: number, token: string): Observable<AxiosResponse<Response<OrderDto>>> {
        const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.ORDER_ID);
        builder.setNamedParameter('id', id.toString());
        return this.httpService.get<Response<any>>(builder.get(),
            { headers: { Authorization: token } })
            .pipe(first());
    }

    getOrderItem(id: number, item_id: number, token: string): Observable<AxiosResponse<Response<OrderItemDto>>> {
        const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.ORDER_ITEM);
        builder.setNamedParameter('id', id.toString());
        builder.setNamedParameter('item_id', item_id.toString());
        return this.httpService.get<Response<any>>(builder.get(),
            { headers: { Authorization: token } })
            .pipe(first());
    }
}
