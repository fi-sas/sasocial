import { Test, TestingModule } from '@nestjs/testing';
import { TaxesService } from './taxes.service';

describe('TaxesService', () => {
  let service: TaxesService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaxesService],
    }).compile();
    service = module.get<TaxesService>(TaxesService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
