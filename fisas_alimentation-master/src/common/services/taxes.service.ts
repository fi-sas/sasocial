import { HttpService, Injectable } from '@nestjs/common';
import { RestURLBuilder } from 'rest-url-builder';
import { Reflector } from '@nestjs/core';
import { CONFIGURATIONS } from '../../app.config';
import { first, flatMap, map } from 'rxjs/operators';
import { TaxDto } from '../entities/tax.dto';
import { forkJoin, Observable, pipe } from 'rxjs';
import { AxiosResponse } from 'axios';

@Injectable()
export class TaxesService {

  private urlBuilder = new RestURLBuilder();

  constructor(private readonly httpService: HttpService,
              private readonly reflector: Reflector) {
  }

  public get(id: number, token: string): Promise<TaxDto> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.TAXE_ID);
    builder.setNamedParameter('id', '' + id);

    return new Promise<TaxDto>((resolve, reject) => {
      this.httpService.get(builder.get(),
        { headers: { Authorization: token } })
        .pipe(first()).subscribe(value => resolve(value.data.data[0]), error => reject(error.response.data.errors));
    });
  }

  public getALL(token: string): Promise<TaxDto> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.TAXES);

    return new Promise<TaxDto>((resolve, reject) => {
      this.httpService.get(builder.get(),
        { headers: { Authorization: token } })
        .pipe(first()).subscribe(value => resolve(value.data.data[0]), error => reject(error.response.data.errors));
    });
  }

  public getIds(ids: number[], token: string): Promise<TaxDto[]> {
    const uniqsIds = ids.filter((v, i, a) => a.indexOf(v) === i);
    const taxesObs: Observable<AxiosResponse<any>>[] = [];

    return new Promise<TaxDto[]>((resolve, reject) => {
      for (const id of uniqsIds) {
        const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.TAXE_ID);
        builder.setNamedParameter('id', '' + id);

        taxesObs.push(this.httpService.get(builder.get(),
          { headers: { Authorization: token } }));
      }

      forkJoin(taxesObs).pipe(first())
        .subscribe(value => { resolve(value.map(response => response.data.data[0])); }, error => reject(error.response.data.errors));

    });
  }

  public async loadTaxesRelateds(entities: any[], property_id: string, property_name: string, token: string): Promise<any[]> {
    return new Promise<any[]>(async (resolve, reject) => {
      const taxesIds: number[] = [];
      for (const ent of entities) {
        ent[property_id] ? taxesIds.push(ent[property_id]) : null;
      }

      try {
        const taxes = await this.getIds(taxesIds, token);
        for (const ent of entities) {
          const ttaxe = taxes.find(tax => tax.id === ent[property_id]);
          ent[property_name] = ttaxe ? ttaxe : null;
        }
        resolve(entities);
      }catch (e) {
        resolve(entities);
      }
    });
  }

  public async loadTaxesRelated(entity: any, property_id: string, property_name: string, token: string): Promise<any> {
    return new Promise<any[]>(async (resolve, reject) => {
      const taxId: number = entity[property_id];

      try {
        const tax = await this.get(taxId, token);
        entity[property_name] = tax ? tax : null;
        resolve(entity);
      } catch (e) {
        resolve(entity);
      }
    });
  }
}
