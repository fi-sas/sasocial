import { HttpService, Injectable } from '@nestjs/common';
import { RestURLBuilder } from 'rest-url-builder';
import { CONFIGURATIONS } from '../../app.config';
import { first } from 'rxjs/operators';
import { UserDto } from '../entities/user.dto';

@Injectable()
export class UsersService {
  private urlBuilder = new RestURLBuilder();
  constructor(private readonly httpService: HttpService) {}

  public get(id: number, token: string): Promise<UserDto> {
    const builder = this.urlBuilder.buildRestURL(CONFIGURATIONS.URLS.USERS_ID);
    builder.setNamedParameter('id', '' + id);

    return new Promise<UserDto>((resolve, reject) => {
      this.httpService.get(builder.get(),
        { headers: { Authorization: token}})
        .pipe(first()).subscribe(value => resolve(value.data.data[0]), error => reject(error.response.data.errors));
    });
  }
}
