import {registerDecorator, ValidationOptions, ValidationArguments} from 'class-validator';

export function IsCEP(property: string, validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      name: 'IsCEP',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const reg  = new RegExp('/^\d{4}-\d{3}?$/').compile();
          return reg.test(value);
        }
      }
    });
  };
}