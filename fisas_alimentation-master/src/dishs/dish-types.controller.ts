import { DishTypePricesPatchDto } from './dtos/dish-type-prices-patch.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Patch,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitParam,
  ApiProduces,
  ApiUseTags,
  ApiImplicitHeader,
  ApiImplicitQuery,
} from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import {
  Response,
  ResponseError,
  ResponseStatus,
} from '../common/response.model';
import { ValidationException } from '../common/exceptions/validation.exception';
import { DishTypesService } from './dish-types.service';
import { DishType } from './entities/dish-type.entity';
import { DishTypeDto } from './dtos/dish-type.dto';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { Token } from '../common/decorators/token.decorator';
import { LanguagesService } from '../common/services/languages.service';
import { PriceVariation } from 'products/entities/price-variation.entity';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Dishs Types')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/dish-types')
export class DishTypesController {
  constructor(
    private dishTypesService: DishTypesService,
    private readonly languageService: LanguagesService,
  ) {}

  // TODO SHOW BY FISCAL ENTITY

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "tax", "disponibility" and "prices" ',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'offset',
    description:
      'Offset from which the items should be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_OFFSET +
      '.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'limit',
    description:
      'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT +
      '. If no pagination is to be applied, use -1.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'sort',
    description:
      'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.',
    type: String,
  })
  @Scopes('dish-types')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @Get()
  public async findAll(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Query() query,
    @Headers('x-language-id') language_id,
  ) {
    if (!query.hasOwnProperty('offset'))
      query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit'))
      query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'tax' &&
          relation !== 'prices' &&
          relation !== 'disponibility',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    const entities = await this.dishTypesService.findAll(
      query.offset,
      query.limit,
      query.sort,
      relateds,
      language_id,
      entity_id,
    );
    return new Response<DishType[]>(ResponseStatus.OK, entities[0], {
      total: entities[1],
    });
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "tax", "disponibility" and "prices" ',
    type: String,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the dish type',
    name: 'id',
    type: Number,
  })
  @Scopes('dish-types')
  @Get(':id')
  public async findById(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Query() query,
    @Headers('x-language-id') language_id,
  ) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'tax' &&
          relation !== 'prices' &&
          relation !== 'disponibility',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    const entity = await this.dishTypesService.findByID(
      id,
      relateds,
      language_id,
      entity_id,
    );
    return new Response<DishType>(ResponseStatus.OK, entity);
  }

  @Scopes('dish-types')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @Post()
  public async create(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Body() entity: DishTypeDto,
    @Token() token,
  ) {
    if (entity.translations.length === 0) {
      throw new ValidationException([
        new ResponseError(2021, 'têm de existir uma tradução pelo menos'),
      ]);
    }
    await this.languageService.validateBody(entity, token).catch(reason => {
      throw new HttpException(reason[0], reason[0].code);
    });

    const createdEntity = await this.dishTypesService.create(entity, entity_id);
    return new Response<DishType>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the dish type',
    name: 'id',
    type: Number,
  })
  @Scopes('dish-types')
  @Put(':id')
  public async update(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() entity: DishTypeDto,
    @Token() token,
  ) {
    if (entity.translations.length === 0) {
      throw new ValidationException([
        new ResponseError(2021, 'têm de existir uma tradução pelo menos'),
      ]);
    }
    await this.languageService.validateBody(entity, token).catch(reason => {
      throw new HttpException(reason[0], reason[0].code);
    });

    const updatedEntity = await this.dishTypesService.update(
      id,
      entity,
      entity_id,
    );
    return new Response<DishType>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the dish type',
    name: 'id',
    type: Number,
  })
  @Scopes('dish-types')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
  ) {
    const result = await this.dishTypesService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the dish type',
    name: 'id',
    type: Number,
  })
  @Get(':id/prices')
  public async getPrices(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
  ) {
    return new Response<PriceVariation[]>(
      ResponseStatus.OK,
      await this.dishTypesService.getPrices(id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the dish type',
    name: 'id',
    type: Number,
  })
  @Patch(':id/prices')
  public async updatePrices(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() prices: DishTypePricesPatchDto,
  ) {
    return new Response<PriceVariation[]>(
      ResponseStatus.OK,
      await this.dishTypesService.updatePrices(id, prices, entity_id),
    );
  }
}
