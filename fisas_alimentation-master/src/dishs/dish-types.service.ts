import { Meal } from './../menus/entities/menu.entity';
import { DishTypePricesPatchDto } from './dtos/dish-type-prices-patch.dto';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { DishType } from './entities/dish-type.entity';
import { DishTypeDto } from './dtos/dish-type.dto';
import { DishTypeTranslation } from './entities/dish-type-translation.entity';
import { PriceVariation } from '../products/entities/price-variation.entity';
import { ResponseError } from '../common/response.model';
import { Disponibility } from '../products/entities/disponibility.entity';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { TaxesService } from '../common/services/taxes.service';
import {  Interval, NestSchedule } from 'nest-schedule';
import moment = require('moment');
import { CONFIGURATIONS } from '../app.config';
import { connect, NatsConnectionOptions, Payload } from 'ts-nats';

@Injectable()
export class DishTypesService extends NestSchedule {
  // NATS CONNECTION
  nc = null;

  constructor(
    @InjectRepository(DishType)
    private readonly dishTypesRepo: Repository<DishType>,
    @InjectRepository(PriceVariation)
    private readonly pricesRepo: Repository<PriceVariation>,
    private readonly taxesService: TaxesService,
  ) {
    super();

    this.natsConnection();
  }

  private async natsConnection() {
    try {
      this.nc = await connect({ maxReconnectAttempts: -1, reconnectTimeWait: 250, servers: [process.env.NATS_URL] });
    } catch (ex) {
      console.error('Nats connection has not succeful');
    }
  }

  public async create(
    entityDto: DishTypeDto,
    entity_id: number,
  ): Promise<DishType> {
    const codeCount = await this.dishTypesRepo.count({ code: entityDto.code });
    if (codeCount > 0) {
      throw new HttpException(
        new ResponseError(1001, 'The dish type code already exists'),
        HttpStatus.BAD_REQUEST,
      );
    }

    /*const taxPriceCount = await this.taxesService.existID(entityDto.tax_id, entity_id);
        if (taxPriceCount === 0) {
          throw new NotFoundException(404, 'the tax id sent doesnt exist');
        }

        const pricesTaxesIds = entityDto.prices
          .filter((value, index, array) =>
            index === array.findIndex((t) => (
              t.tax_id === value.tax_id
            )))
          .map(price => price.tax_id);
        const existsTax_ids = await this.taxesService.existIDs(pricesTaxesIds);
        if (existsTax_ids < pricesTaxesIds.length) {
          throw new NotFoundException(404, 'the tax id sent from prices array doesnt exist');
        }*/

    return new Promise<DishType>(async (resolve, reject) => {
      let id: number = 0;
      const tfe = new FiscalEntity();
      tfe.id = entity_id;

      await this.dishTypesRepo.manager
        .transaction(async entityManger => {
          const insertResult = await entityManger
            .createQueryBuilder()
            .insert()
            .into(DishType)
            .values({
              tax_id: entityDto.tax_id,
              code: entityDto.code,
              price: entityDto.price,
              active: entityDto.active,
              fentity: tfe,
            })
            .execute();

          id = insertResult.identifiers[0].id;
          const tdishType: DishType = new DishType();
          tdishType.id = id;

          for (const translation of entityDto.translations) {
            const tdtt: DishTypeTranslation = new DishTypeTranslation();
            tdtt.dish_type = tdishType;
            tdtt.language_id = translation.language_id;
            tdtt.name = translation.name;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(DishTypeTranslation)
              .values(tdtt)
              .execute();
          }

          // SAVE DISPONIBILITY
          const disponibilityResultLunch = await entityManger
            .createQueryBuilder()
            .insert()
            .into(Disponibility)
            .values({
              dishType_lunch: tdishType,
              begin_date: entityDto.disponibility_lunch.begin_date
                ? new Date(entityDto.disponibility_lunch.begin_date)
                : null,
              end_date: entityDto.disponibility_lunch.end_date
                ? new Date(entityDto.disponibility_lunch.end_date)
                : null,
              monday: entityDto.disponibility_lunch.monday,
              tuesday: entityDto.disponibility_lunch.tuesday,
              wednesday: entityDto.disponibility_lunch.wednesday,
              thursday: entityDto.disponibility_lunch.thursday,
              friday: entityDto.disponibility_lunch.friday,
              saturday: entityDto.disponibility_lunch.saturday,
              sunday: entityDto.disponibility_lunch.sunday,
              minimum_hour: entityDto.disponibility_lunch.minimum_hour
                ? new Date(entityDto.disponibility_lunch.minimum_hour)
                : null,
              maximum_hour: entityDto.disponibility_lunch.maximum_hour
                ? new Date(entityDto.disponibility_lunch.maximum_hour)
                : null,
              annulment_maximum_hour: entityDto.disponibility_dinner.annulment_maximum_hour
                ? new Date(entityDto.disponibility_dinner.annulment_maximum_hour)
                : null,
            })
            .execute();

          const disponibilityResultDinner = await entityManger
            .createQueryBuilder()
            .insert()
            .into(Disponibility)
            .values({
              dishType_dinner: tdishType,
              begin_date: entityDto.disponibility_dinner.begin_date
                ? new Date(entityDto.disponibility_dinner.begin_date)
                : null,
              end_date: entityDto.disponibility_dinner.end_date
                ? new Date(entityDto.disponibility_dinner.end_date)
                : null,
              monday: entityDto.disponibility_dinner.monday,
              tuesday: entityDto.disponibility_dinner.tuesday,
              wednesday: entityDto.disponibility_dinner.wednesday,
              thursday: entityDto.disponibility_dinner.thursday,
              friday: entityDto.disponibility_dinner.friday,
              saturday: entityDto.disponibility_dinner.saturday,
              sunday: entityDto.disponibility_dinner.sunday,
              minimum_hour: entityDto.disponibility_dinner.minimum_hour
                ? new Date(entityDto.disponibility_dinner.minimum_hour)
                : null,
              maximum_hour: entityDto.disponibility_dinner.maximum_hour
                ? new Date(entityDto.disponibility_dinner.maximum_hour)
                : null,
              annulment_maximum_hour: entityDto.disponibility_dinner.annulment_maximum_hour
                ? new Date(entityDto.disponibility_dinner.annulment_maximum_hour)
                : null,
            })
            .execute();

          if (entityDto.prices) {
            for (const price of entityDto.prices) {
              const tpv: PriceVariation = new PriceVariation();
              tpv.dishType = tdishType;
              tpv.profile_id = price.profile_id;
              tpv.time = new Date(price.time);
              tpv.price = price.price;
              tpv.description = price.description;
              tpv.tax_id = price.tax_id;
              tpv.meal = price.meal;
              await entityManger
                .createQueryBuilder()
                .insert()
                .into(PriceVariation)
                .values(tpv)
                .execute();
            }
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(
          await this.findByID(
            id,
            ['tax', 'prices', 'disponibility'],
            null,
            entity_id,
          ),
        );
      } catch (e) {
        reject(e);
      }
    });
  }

  public async findAll(
    offset: number,
    limit: number,
    sort: string,
    relateds: string[],
    language_id: number,
    entity_id: number,
  ): Promise<[DishType[], number]> {
    const dishTypesQB = this.dishTypesRepo.createQueryBuilder('dishType');

    if (limit > 0) {
      dishTypesQB.take(limit);
      dishTypesQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      dishTypesQB.orderBy(
        'dishType.' + sort_replaced,
        sort_replaced !== sort ? 'DESC' : 'ASC',
      );
    }

    dishTypesQB.leftJoinAndSelect(
      'dishType.translations',
      'dishType_translation',
      language_id ? 'dishType_translation.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'tax')) {
      // dishTypesQB.leftJoinAndSelect('dishType.tax', 'tax');
    }

    if (relateds.find(related => related === 'disponibility')) {
      dishTypesQB.leftJoinAndSelect(
        'dishType.disponibility_lunch',
        'disponibility_lunch',
      );
      dishTypesQB.leftJoinAndSelect(
        'dishType.disponibility_dinner',
        'disponibility_dinner',
      );
    }

    if (relateds.find(related => related === 'prices')) {
      dishTypesQB.leftJoinAndSelect('dishType.prices', 'price');
      // dishTypesQB.leftJoinAndSelect('price.tax', 'price_tax');
    }

    dishTypesQB.where('dishType.fentityId = :entity_id', { entity_id });

    return await dishTypesQB.getManyAndCount();
  }

  public async findByID(
    id: number,
    relateds: string[],
    language_id: number,
    entity_id: number,
  ): Promise<DishType> {
    const persistedEntity = await this.dishTypesRepo.count({
      id,
      fentity: { id: entity_id },
    });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Dish type not found');
    }

    const dishTypeQB = this.dishTypesRepo.createQueryBuilder('dishType');

    dishTypeQB.leftJoinAndSelect(
      'dishType.translations',
      'dishType_translation',
      language_id ? 'dishType_translation.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'tax')) {
      // dishTypeQB.leftJoinAndSelect('dishType.tax', 'tax');
    }

    if (relateds.find(related => related === 'disponibility')) {
      dishTypeQB.leftJoinAndSelect(
        'dishType.disponibility_lunch',
        'disponibility_lunch',
      );
      dishTypeQB.leftJoinAndSelect(
        'dishType.disponibility_dinner',
        'disponibility_dinner',
      );
    }

    if (relateds.find(related => related === 'prices')) {
      dishTypeQB.leftJoinAndSelect('dishType.prices', 'price');
      // dishTypeQB.leftJoinAndSelect('price.tax', 'price_tax');
    }

    dishTypeQB.where('dishType.id = :id', { id });
    return await dishTypeQB.getOne();
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.dishTypesRepo.count({ id, fentity: { id: entity_id } });
  }

  public async existIDs(ids: number[], entity_id: number): Promise<number> {
    if (ids.length) {
      return await this.dishTypesRepo.count({
        id: In(ids),
        fentity: { id: entity_id },
      });
    } else {
      return 0;
    }
  }

  public async update(
    id: number,
    entityDto: DishTypeDto,
    entity_id: number,
  ): Promise<DishType> {
    const persistedEntity = await this.dishTypesRepo.count({
      id,
      fentity: { id: entity_id },
    });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Dish type not found');
    }

    /*const taxPriceCount = await this.taxesService.existID(entityDto.tax_id, entity_id);
        if (taxPriceCount === 0) {
          throw new NotFoundException(404, 'the tax id sent doesnt exist');
        }

        const pricesTaxesIds = entityDto.prices
          .filter((value, index, array) =>
            index === array.findIndex((t) => (
              t.tax_id === value.tax_id
            )))
          .map(price => price.tax_id);
        const existsTax_ids = await this.taxesService.existIDs(pricesTaxesIds);
        if (existsTax_ids < pricesTaxesIds.length) {
          throw new NotFoundException(404, 'the tax id sent from prices array doesnt exist');
        }*/

    return new Promise<DishType>(async (resolve, reject) => {
      await this.dishTypesRepo.manager
        .transaction(async entityManger => {
          const updateResult = await entityManger
            .createQueryBuilder()
            .update(DishType)
            .set({
              tax_id: entityDto.tax_id,
              price: entityDto.price,
              active: entityDto.active,
            })
            .where(' id = :id ', { id })
            .execute();

          const tdishType: DishType = new DishType();
          tdishType.id = id;

          await entityManger
            .createQueryBuilder()
            .delete()
            .from(DishTypeTranslation)
            .where('dishTypeId = :id', { id })
            .execute();

          for (const translation of entityDto.translations) {
            const tdtt: DishTypeTranslation = new DishTypeTranslation();
            tdtt.dish_type = tdishType;
            tdtt.language_id = translation.language_id;
            tdtt.name = translation.name;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(DishTypeTranslation)
              .values(tdtt)
              .execute();
          }

          // UPDATE DISPONIBILITY
          const disponibilityResultLunch = await entityManger
            .createQueryBuilder()
            .update(Disponibility)
            .set({
              begin_date: entityDto.disponibility_lunch.begin_date
                ? new Date(entityDto.disponibility_lunch.begin_date)
                : null,
              end_date: entityDto.disponibility_lunch.end_date
                ? new Date(entityDto.disponibility_lunch.end_date)
                : null,
              monday: entityDto.disponibility_lunch.monday,
              tuesday: entityDto.disponibility_lunch.tuesday,
              wednesday: entityDto.disponibility_lunch.wednesday,
              thursday: entityDto.disponibility_lunch.thursday,
              friday: entityDto.disponibility_lunch.friday,
              saturday: entityDto.disponibility_lunch.saturday,
              sunday: entityDto.disponibility_lunch.sunday,
              minimum_hour: entityDto.disponibility_lunch.minimum_hour
                ? new Date(entityDto.disponibility_lunch.minimum_hour)
                : null,
              maximum_hour: entityDto.disponibility_lunch.maximum_hour
                ? new Date(entityDto.disponibility_lunch.maximum_hour)
                : null,
              annulment_maximum_hour: entityDto.disponibility_dinner.annulment_maximum_hour
                ? new Date(entityDto.disponibility_dinner.annulment_maximum_hour)
                : null,
            })
            .where('dishTypeLunchId = :id', { id })
            .execute();
          const disponibilityResultDinner = await entityManger
            .createQueryBuilder()
            .update(Disponibility)
            .set({
              begin_date: entityDto.disponibility_dinner.begin_date
                ? new Date(entityDto.disponibility_dinner.begin_date)
                : null,
              end_date: entityDto.disponibility_dinner.end_date
                ? new Date(entityDto.disponibility_dinner.end_date)
                : null,
              monday: entityDto.disponibility_dinner.monday,
              tuesday: entityDto.disponibility_dinner.tuesday,
              wednesday: entityDto.disponibility_dinner.wednesday,
              thursday: entityDto.disponibility_dinner.thursday,
              friday: entityDto.disponibility_dinner.friday,
              saturday: entityDto.disponibility_dinner.saturday,
              sunday: entityDto.disponibility_dinner.sunday,
              minimum_hour: entityDto.disponibility_dinner.minimum_hour
                ? new Date(entityDto.disponibility_dinner.minimum_hour)
                : null,
              maximum_hour: entityDto.disponibility_dinner.maximum_hour
                ? new Date(entityDto.disponibility_dinner.maximum_hour)
                : null,
              annulment_maximum_hour: entityDto.disponibility_dinner.annulment_maximum_hour
                ? new Date(entityDto.disponibility_dinner.annulment_maximum_hour)
                : null,
            })
            .where('dishTypeDinnerId = :id', { id })
            .execute();

          if (entityDto.prices) {
            await entityManger
              .createQueryBuilder()
              .delete()
              .from(PriceVariation)
              .where('dishTypeId = :id ', { id })
              .execute();
            for (const price of entityDto.prices) {
              const tpv: PriceVariation = new PriceVariation();
              tpv.dishType = tdishType;
              tpv.profile_id = price.profile_id;
              tpv.time = new Date(price.time);
              tpv.price = price.price;
              tpv.description = price.description;
              tpv.tax_id = price.tax_id;
              tpv.meal = price.meal;
              await entityManger
                .createQueryBuilder()
                .insert()
                .into(PriceVariation)
                .values(tpv)
                .execute();
            }
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(
          await this.findByID(
            id,
            ['tax', 'prices', 'disponibility'],
            null,
            entity_id,
          ),
        );
      } catch (e) {
        reject(e);
      }
    });
  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.dishTypesRepo.count({
      id,
      fentity: { id: entity_id },
    });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Dish type not found');
    }
    return await this.dishTypesRepo.delete(id);
  }

  @Interval(60000)
  private checkDishTypeMaximumHour() {
    console.log(
      'Check maximum hour of dishs at ' + moment().format('HH:mm:ss'),
    );

    this.dishTypesRepo
      .createQueryBuilder('dt')
      .leftJoin('dt.disponibility_lunch', 'dl')
      .leftJoin('dt.disponibility_dinner', 'dd')
      .where(
        ' HOUR(dl.maximum_hour) = :current_hour AND MINUTE(dl.maximum_hour) = :current_minute',
        {
          current_hour: moment().format('HH'),
          current_minute: moment().format('mm'),
        },
      )
      .getMany()
      .then(value => {
        if (value.length) {
          try {
            this.nc.publish(CONFIGURATIONS.QUEUES.DISH_TYPE_MAXIMUM_HOUR_LUNCH, JSON.stringify(value));
          } catch (e) {
            console.log('[Dish-Type] Error');
            console.log(e);
          }
        }
      });

    this.dishTypesRepo
      .createQueryBuilder('dt')
      .leftJoin('dt.disponibility_dinner', 'dd')
      .where(
        ' HOUR(dd.maximum_hour) = :current_hour AND MINUTE(dd.maximum_hour) = :current_minute',
        {
          current_hour: moment().format('HH'),
          current_minute: moment().format('mm'),
        },
      )
      .getMany()
      .then(value => {
        if (value.length) {
          try {
            this.nc.publish(CONFIGURATIONS.QUEUES.DISH_TYPE_MAXIMUM_HOUR_DINNER, JSON.stringify(value));
          } catch (e) {
            console.log('[Dish-Type] Error');
            console.log(e);
          }
        }
      });
  }

  async getPrices(id: any, entity_id: any): Promise<PriceVariation[]> {
    const persistedEntity = await this.dishTypesRepo.count({
      id,
      fentity: { id: entity_id },
    });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Dish type not found');
    }

    return await this.pricesRepo.find({ where: { dishType_id: id } });
  }

  async updatePrices(
    id: any,
    prices: DishTypePricesPatchDto,
    entity_id: any,
  ): Promise<PriceVariation[]> {
    const persistedEntity = await this.dishTypesRepo.count({
      id,
      fentity: { id: entity_id },
    });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Dish type not found');
    }

    return new Promise<PriceVariation[]>(async (resolve, reject) => {
      await this.pricesRepo.manager
        .transaction(async entityManger => {
          await entityManger
            .createQueryBuilder()
            .delete()
            .from(PriceVariation)
            .where('dishTypeId = :id ', { id })
            .execute();
          for (const price of prices.prices) {
            const tpv: PriceVariation = new PriceVariation();
            tpv.dishType_id = id;
            tpv.profile_id = price.profile_id;
            tpv.time = new Date(price.time);
            tpv.price = price.price;
            tpv.description = price.description;
            tpv.tax_id = price.tax_id;
            tpv.meal = price.meal;
            await entityManger
              .createQueryBuilder()
              .insert()
              .into(PriceVariation)
              .values(tpv)
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getPrices(id, entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Calculates the maximum date to buy or cancel a dish from this dish type
   * @param id id of the dish type
   * @param meal meal of the menu
   * @param date date of the menu
   * @returns { available_until: Date, nullable_until: Date }
   */
  async availableNullableUntil(id: number, meal: Meal, date?: Date|string): Promise<{ available_until: Date, nullable_until: Date }> {
    let disponibility: Disponibility = null;
    const type = await this.dishTypesRepo.findOneOrFail(id, { relations: [
      meal === Meal.LUNCH ? 'disponibility_lunch' : 'disponibility_dinner',
    ] });
    if (meal === Meal.LUNCH) {
      disponibility = type.disponibility_lunch;
    } else {
      disponibility = type.disponibility_dinner;
    }

    console.log(disponibility);
    let available_until: any = disponibility.maximum_hour ? moment('2020-01-01T' + disponibility.maximum_hour) : null;
    available_until = available_until ? moment(date)
    .set('hour', available_until.get('hour'))
    .set('minutes', available_until.get('minutes'))
    .set('seconds', 0)
    .toDate() : null;
    let nullable_until: any = disponibility.annulment_maximum_hour ? moment('2020-01-01T' + disponibility.annulment_maximum_hour): null;
    nullable_until = nullable_until ? moment(date)
    .set('hour', nullable_until.get('hour'))
    .set('minutes', nullable_until.get('minutes'))
    .set('seconds', 0)
    .toDate() : null;

    return Promise.resolve({
      available_until,
      nullable_until: (nullable_until ? nullable_until : available_until)
    });
  }

}
