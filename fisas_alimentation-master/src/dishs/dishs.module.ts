import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dish } from './entities/dish.entity';
import { DishTranslation } from './entities/dish-translation.entity';
import { DishType } from './entities/dish-type.entity';
import { DishTypeTranslation } from './entities/dish-type-translation.entity';
import { DishsController } from './dishs.controller';
import { DishsService } from './dishs.service';
import { DishTypesController } from './dish-types.controller';
import { DishTypesService } from './dish-types.service';
import { PriceVariation } from '../products/entities/price-variation.entity';
import { JwtModule } from '@nestjs/jwt';
import { RecipesModule } from '../recipes/recipes.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    RecipesModule,
    TypeOrmModule.forFeature([
      Dish,
      DishTranslation,
      DishType,
      DishTypeTranslation,
      PriceVariation
    ]),
    HttpModule,
    JwtModule.register({}),
  ],
  controllers: [
    DishsController,
    DishTypesController
  ],
  providers: [
    DishsService,
    DishTypesService,
  ],
  exports: [
    DishsService,
    DishTypesService
  ]
})
export class DishsModule {}