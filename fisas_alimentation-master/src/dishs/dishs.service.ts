import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Dish } from './entities/dish.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { DishDto } from './dtos/dish.dto';
import { DishTranslation } from './entities/dish-translation.entity';
import { NutrientProduct } from '../products/entities/nutrient-product.entity';
import { Allergen } from '../allergens/entities/allergen.entity';
import { RecipesService } from '../recipes/recipes.service';
import { MediasService } from '../common/services/medias.service';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { DishTypesService } from './dish-types.service';
import { ProductTranslation } from '../products/entities/product-translation.entity';

@Injectable()
export class DishsService {
  constructor(@InjectRepository(Dish) private readonly dishRepo: Repository<Dish>,
              private readonly recipesService: RecipesService,
              private readonly dishTypesService: DishTypesService,
              private readonly mediaService: MediasService,
  ) {
  }

  public async create(entityDto: DishDto, entity_id: number, token: string): Promise<Dish> {
    const recipes_id = await this.recipesService.existIDs(entityDto.recipes_ids, entity_id);
    if (recipes_id < entityDto.recipes_ids.length) {
      throw new NotFoundException(404, 'the recipe id sent doesnt exist');
    }

    const dishTypes_id = await this.dishTypesService.existIDs(entityDto.types_ids, entity_id);
    if (dishTypes_id < entityDto.types_ids.length) {
      throw new NotFoundException(404, 'the dish type id sent doesnt exist');
    }

    return new Promise<Dish>(async (resolve, reject) => {
      let id: number = 0;
      const tfe = new FiscalEntity();
      tfe.id = entity_id;

      await this.dishRepo.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(Dish)
          .values({
            file_id: entityDto.file_id,
            active: entityDto.active,
            fentity: tfe
          })
          .execute();

        id = insertResult.identifiers[0].id;
        const tdish: Dish = new Dish();
        tdish.id = id;

        for (const translation of entityDto.translations) {
          const tdt: DishTranslation = new DishTranslation();
          tdt.dish = tdish;
          tdt.language_id = translation.language_id;
          tdt.name = translation.name;
          tdt.description = translation.description;

          await entityManger.createQueryBuilder()
            .insert()
            .into(DishTranslation)
            .values(tdt)
            .execute();
        }

        for (const recipe of entityDto.recipes_ids) {
          await entityManger.createQueryBuilder()
            .insert()
            .into('dish_recipes_recipe')
            .values({
              recipeId: recipe,
              dishId: id,
            })
            .execute();
        }

        for (const type of entityDto.types_ids) {
          await entityManger.createQueryBuilder()
            .insert()
            .into('dish_types_dish_type')
            .values({
              dishTypeId: type,
              dishId: id,
            })
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['recipes', 'dishTypes', 'files'], null, entity_id, token));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async findAll(offset: number, limit: number, sort: string,
                       relateds: string[],
                       name: string, types: number[], language_id: number, entity_id: number, token: string) {
    const dishsQB = this.dishRepo.createQueryBuilder('dish');

    if (limit > 0) {
      dishsQB.take(limit);
      dishsQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      dishsQB.orderBy('dish.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    dishsQB.leftJoinAndSelect('dish.translations',
      'dish_translation',
      language_id ? 'dish_translation.language_id = :language_id' : '',
      { language_id });

    if (relateds.find(related => related === 'recipes')) {
      dishsQB.leftJoinAndSelect('dish.recipes', 'recipe');
    }

    if (relateds.find(related => related === 'dishTypes') || types.length > 0) {
      dishsQB.leftJoinAndSelect('dish.types', 'types');
      dishsQB.leftJoinAndSelect('types.translations', 'type_translations');
    }

    dishsQB.where(' dish.fentityId = :entity_id', { entity_id });

    if (name) {
      dishsQB.andWhere('dish_translation.name LIKE :name', { name: '%' + name + '%'});
    }

    if ( types.length > 0) {
      dishsQB.andWhere('types.id IN (:...types)', { types });
    }

    const entities = await dishsQB.getManyAndCount();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelateds(entities[0], 'file_id', 'file', token);
    }

    return entities;
  }

  public async findByID(id: number, relateds: string[], language_id: number, entity_id: number, token: string): Promise<Dish> {
    const persistedEntity = await this.dishRepo.count({ id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const dishQB = this.dishRepo.createQueryBuilder('dish');

    dishQB.leftJoinAndSelect('dish.translations',
      'dish_translation',
      language_id ? 'dish_translation.language_id = :language_id' : '',
      { language_id });

    if (relateds.find(related => related === 'recipes')) {
      dishQB.leftJoinAndSelect('dish.recipes', 'recipe');
    }

    if (relateds.find(related => related === 'dishTypes')) {
      dishQB.leftJoinAndSelect('dish.types', 'types');
      dishQB.leftJoinAndSelect('types.translations', 'type_translations');
    }

    dishQB.where('dish.id = :id', { id });
    const entity = await dishQB.getOne();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelated(entity, 'file_id', 'file', token);
    }

    return entity;
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.dishRepo.count({ id, fentity: { id: entity_id}  });
  }

  public async existIDs(ids: number[], entity_id: number): Promise<number> {
    if (ids.length) {
      return await this.dishRepo.count({ id: In(ids), fentity: { id: entity_id}  });
    } else {
      return 0;
    }
  }

  public async update(id: number, entityDto: DishDto, entity_id: number, token: string) {
    const persistedEntity = await this.dishRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }

    const recipes_id = await this.recipesService.existIDs(entityDto.recipes_ids, entity_id);
    if (recipes_id < entityDto.recipes_ids.length) {
      throw new NotFoundException(404, 'the recipe id sent doesnt exist');
    }

    const dishTypes_id = await this.dishTypesService.existIDs(entityDto.types_ids, entity_id);
    if (dishTypes_id < entityDto.types_ids.length) {
      throw new NotFoundException(404, 'the dish type id sent doesnt exist');
    }

    return new Promise<Dish>(async (resolve, reject) => {
      await this.dishRepo.manager.transaction(async entityManger => {
        await entityManger.createQueryBuilder()
          .update(Dish)
          .set({
            file_id: entityDto.file_id,
            active: entityDto.active,
          })
          .where(' id = :id ', { id })
          .execute();

        const tdish: Dish = new Dish();
        tdish.id = id;

        await entityManger.createQueryBuilder()
          .delete()
          .from(DishTranslation)
          .where('dishId = :id', { id })
          .execute();
        for (const translation of entityDto.translations) {
          const tdt: DishTranslation = new DishTranslation();
          tdt.dish = tdish;
          tdt.language_id = translation.language_id;
          tdt.name = translation.name;
          tdt.description = translation.description;

          await entityManger.createQueryBuilder()
            .insert()
            .into(DishTranslation)
            .values(tdt)
            .execute();
        }

        await entityManger.createQueryBuilder()
          .delete()
          .from('dish_recipes_recipe')
          .where('dishId = :id', { id })
          .execute();

        for (const recipe of entityDto.recipes_ids) {

          await entityManger.createQueryBuilder()
            .insert()
            .into('dish_recipes_recipe')
            .values({
              recipeId: recipe,
              dishId: id,
            })
            .execute();
        }

        await entityManger.createQueryBuilder()
          .delete()
          .from('dish_types_dish_type')
          .where('dishId = :id', { id })
          .execute();

        for (const type of entityDto.types_ids) {
          await entityManger.createQueryBuilder()
            .insert()
            .into('dish_types_dish_type')
            .values({
              dishTypeId: type,
              dishId: id,
            })
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['recipes', 'files', 'dishTypes'], null, entity_id, token));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.dishRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }
    return await this.dishRepo.delete(id);
  }

  public async calculatedAllergen(id: number, language_id: number): Promise<Allergen[]> {
    const persistedEntity = await this.dishRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }

    // TODO CHANGE THE PERFORMANCE OF THIS
    const dishQB = this.dishRepo.createQueryBuilder('dish');
    dishQB.leftJoinAndSelect('dish.recipes', 'recipe');
    dishQB.leftJoinAndSelect('recipe.ingredients', 'recipe_ingredient');
    dishQB.leftJoinAndSelect('recipe_ingredient.ingredient', 'ingredient');
    dishQB.leftJoinAndSelect('ingredient.allergens', 'allergen');
    dishQB.leftJoinAndSelect('allergen.translations', 'allergen_translation'
      , language_id ? 'allergen_translation.language_id = :language_id' : '', { language_id });

    dishQB.where('dish.id = :id', { id });
    const dish = await dishQB.getOne();

    const nutrients: NutrientProduct[] = [];
    const allergens: Allergen[] = [];
    dish.recipes.map(recipe => {
      recipe.ingredients.map(recipe_indients => {
        recipe_indients.ingredient.allergens.map(allergen => {
          allergens.indexOf(allergen) === -1 ? allergens.push(allergen) : null;
        });
      });
    });
    return allergens;
  }

  public async calculatedAllergenAndNutrientsComposition(id: number, language_id: number)
    : Promise<[NutrientProduct[], Allergen[], ProductTranslation[]]> {
    const persistedEntity = await this.dishRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }

    const dishQB = this.dishRepo.createQueryBuilder('dish');
    dishQB.leftJoinAndSelect('dish.recipes', 'recipe');
    dishQB.leftJoinAndSelect('recipe.ingredients', 'recipe_ingredient');
    dishQB.leftJoinAndSelect('recipe_ingredient.ingredient', 'ingredient');
    dishQB.leftJoinAndSelect('ingredient.translations', 'ingredient_translation'
      , language_id ? 'ingredient_translation.language_id = :language_id' : '', { language_id });
    dishQB.leftJoinAndSelect('ingredient.nutrients', 'ingredient_nutrient');
    dishQB.leftJoinAndSelect('ingredient_nutrient.nutrient', 'nutrient');
    dishQB.leftJoinAndSelect('nutrient.translations', 'nutrient_translation'
      , language_id ? 'nutrient_translation.language_id = :language_id' : '', { language_id });

    dishQB.leftJoinAndSelect('nutrient.unit', 'unit');
    dishQB.leftJoinAndSelect('unit.translations', 'unit_translation'
      , language_id ? 'unit_translation.language_id = :language_id' : '', { language_id });

    dishQB.leftJoinAndSelect('ingredient.allergens', 'allergen');
    dishQB.leftJoinAndSelect('allergen.translations', 'allergen_translation'
      , language_id ? 'allergen_translation.language_id = :language_id' : '', { language_id });

    dishQB.where('dish.id = :id', { id });

    const dish = await dishQB.getOne();

    const nutrients: NutrientProduct[] = [];
    const allergens: Allergen[] = [];
    const composition: ProductTranslation[] = [];
    const composition_ids: number[] = [];
    dish.recipes.map(recipe => {
      recipe.ingredients.map(recipe_indients => {

        if (!composition_ids.find(v => v === recipe_indients.ingredient.id)) {
          composition_ids.push(recipe_indients.ingredient.id);
          composition.push(recipe_indients.ingredient.translations[0]);
        }

        recipe_indients.ingredient.nutrients.map(nutrient => {
          const temp_quantity = nutrient.quantity * (recipe_indients.liquid_quantity / recipe.number_doses);
          const tnutri = nutrients.find(nutri => nutri.nutrient.id === nutrient.nutrient.id);
          if (tnutri) {
            tnutri.quantity = parseInt(tnutri.quantity.toString(), 10) + temp_quantity;
          } else {
            const tnp = new NutrientProduct();
            tnp.quantity = temp_quantity;
            tnp.nutrient = nutrient.nutrient;
            nutrients.push(tnp);
          }
        });

        recipe_indients.ingredient.allergens.map(allergen => {
          allergens.indexOf(allergen) === -1 ? allergens.push(allergen) : null;
        });
      });
    });

    nutrients.map(n => n.quantity = parseFloat(n.quantity.toFixed(2)));

    return [nutrients, allergens, composition];
  }
}