import { Type } from 'class-transformer';
import { PriceVariationDto } from './../../products/dtos/product.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { ValidateNested } from 'class-validator';
export class DishTypePricesPatchDto {
  @ApiModelProperty({ type: PriceVariationDto, isArray: true })
  @ValidateNested()
  @Type(() => PriceVariationDto)
  prices: PriceVariationDto[];
}
