import { ArrayMinSize, IsArray, IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, Min, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { PriceVariationDto } from '../../products/dtos/product.dto';
import { DisponibilityDto } from '../../products/dtos/disponibility.dto';

export class DishTypeTranslationDto {
  @ApiModelProperty()
  @IsNumber({}, { message: 'id da linguagem não esta correto' })
  language_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'name não pode ser nulo'})
  @IsString({ message: 'name têm de ser texto'})
  name: string;
}

export class DishTypeDto {
  id: number;

  @ApiModelProperty( {type: DishTypeTranslationDto, isArray: true, minItems: 1 })
  @IsArray({ message: 'translation têm de se um array'})
  @ArrayMinSize(1, { message: 'Têm de existir uma tradução no minimo'})
  @ValidateNested()
  @Type(() => DishTypeTranslationDto)
  translations: DishTypeTranslationDto[];

  @ApiModelProperty()
  @IsNumber({}, { message: 'the id tax should be a number'})
  tax_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'O code do produto não pode ser vazio' })
  @IsString({ message: 'o code tem de ser do tipo string'})
  @MaxLength(55, { message: 'O code não pode conter mais que 55 caracteres'})
  code: string;

  @ApiModelProperty()
  @Min(0, { message: 'o preço da variação de preço têm de ser maior que zero'})
  @IsNumber({}, { message: 'o preço da variação de preço têm de ser um numero'})
  price: number;

  @ApiModelPropertyOptional({ type: PriceVariationDto, isArray: true })
  @IsOptional()
  @ValidateNested()
  @Type(() => PriceVariationDto)
  prices: PriceVariationDto[];

  @ApiModelProperty({ type: DisponibilityDto})
  @IsNotEmpty({ message: 'the disponibility cant be null'})
  @ValidateNested()
  @Type(() => DisponibilityDto)
  disponibility_lunch: DisponibilityDto;

  @ApiModelProperty({ type: DisponibilityDto})
  @IsNotEmpty({ message: 'the disponibility cant be null'})
  @ValidateNested()
  @Type(() => DisponibilityDto)
  disponibility_dinner: DisponibilityDto;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}