import { ArrayMinSize, IsArray, IsBoolean, IsNotEmpty, IsNumber, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class DishTranslationDto {
  @ApiModelProperty()
  @IsNumber({}, { message: 'id da linguagem não esta correto' })
  language_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'name não pode ser nulo'})
  @IsString({ message: 'name têm de ser texto'})
  name: string;

  @ApiModelPropertyOptional()
  @IsString({ message: 'description têm de ser texto'})
  description: string;
}

export class DishDto {
  id: number;

  @ApiModelProperty( {type: DishTranslationDto, isArray: true, minItems: 1 })
  @IsArray({ message: 'translation têm de se um array'})
  @ArrayMinSize(1, { message: 'Têm de existir uma tradução no minimo'})
  @ValidateNested()
  @Type(() => DishTranslationDto)
  translations: DishTranslationDto[];

  @ApiModelProperty( {type: Number, isArray: true })
  @IsArray({ message: 'recipes têm de se um array'})
  recipes_ids: number[];

  @ApiModelProperty( {type: Number, isArray: true })
  @IsArray({ message: 'dish_types is a array'})
  types_ids: number[];

  @ApiModelProperty()
  @IsNumber({}, { message: 'file_id e um numero'})
  file_id: number;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}