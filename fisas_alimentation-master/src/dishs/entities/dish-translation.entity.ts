import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Dish } from './dish.entity';

@Entity()
export class DishTranslation {

  @PrimaryColumn()
  language_id: number;

  @ManyToOne(type => Dish, dish => dish.translations, { primary: true, cascade: true, onDelete: 'CASCADE'})
  dish: Dish;

  @Column({ nullable: false})
  name: string;

  @Column()
  description: string;

}