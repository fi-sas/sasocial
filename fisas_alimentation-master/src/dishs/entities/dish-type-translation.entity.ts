import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { DishType } from './dish-type.entity';

@Entity()
export class DishTypeTranslation {
  @PrimaryColumn()
  language_id: number;

  @ManyToOne(type => DishType, dishType => dishType.translations, {primary: true, cascade: true, onDelete: 'CASCADE'})
  dish_type: DishType;

  @Column({nullable: false})
  name: string;
}