import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { DishTypeTranslation } from './dish-type-translation.entity';
import { PriceVariation } from '../../products/entities/price-variation.entity';
import { Disponibility } from '../../products/entities/disponibility.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { Dish } from './dish.entity';

@Entity()
export class DishType extends BaseEntity {
  @OneToMany(type => DishTypeTranslation, translation => translation.dish_type)
  @JoinColumn()
  translations: DishTypeTranslation[];

  @Column({ nullable: false})
  tax_id: number;

  @Column({ unique: true, length: 55, nullable: false, readonly: true })
  code: string;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  price: number;

  @OneToOne(type1 => Disponibility, disponibilityL => disponibilityL.dishType_lunch, { nullable: false, cascade: true })
  disponibility_lunch: Disponibility;

  @OneToOne(type2 => Disponibility, disponibilityD => disponibilityD.dishType_dinner, { nullable: false, cascade: true })
  disponibility_dinner: Disponibility;

  @OneToMany(type1 => PriceVariation, price => price.dishType, { cascade: true })
  @JoinTable()
  prices: PriceVariation[];

  @ManyToMany(type => Dish, dsh => dsh.types)
  dishs: Dish[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}