import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, RelationId } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { DishTranslation } from './dish-translation.entity';
import { Recipe } from '../../recipes/entities/recipe.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { DishType } from './dish-type.entity';
import { MediaDto } from '../../common/entities/media.dto';

@Entity()
export class Dish extends BaseEntity {

  @OneToMany(type => DishTranslation, translation => translation.dish)
  @JoinColumn()
  translations: DishTranslation[];

  @Column()
  file_id: number;
  file: MediaDto;

  @ManyToMany(type => Recipe, recipe => recipe.dishs)
  @JoinTable()
  recipes: Recipe[];

  @RelationId( (dish: Dish) => dish.recipes)
  recipes_ids: number[];

  @ManyToMany(type => DishType, dt => dt.dishs)
  @JoinTable()
  types: DishType[];

  @RelationId( (dish: Dish) => dish.types)
  types_ids: number[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}