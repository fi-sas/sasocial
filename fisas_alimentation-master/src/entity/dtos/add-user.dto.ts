import { IsBoolean, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class AddUserDto {

  @ApiModelProperty()
  @IsNumber({}, { message: 'the user id should be a number'})
  user_id: number;

  @ApiModelProperty()
  @IsBoolean()
  manager: boolean;
}