import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsCEP } from '../../common/validators/IsCEP.validator';
import { IsBoolean, IsEmail, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class FiscalEntityDto {

  @ApiModelProperty()
  @IsNotEmpty({ message: 'the name cant be empty'})
  name: string;

  @ApiModelProperty()
  @IsNumber({}, { message: 'the nif should be a number'})
  nif: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'the street cant be empty'})
  street: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumber({}, { message: 'the door should be a number'})
  door: number;

  @ApiModelProperty()
  city: string;

  @IsCEP('props', { message: 'the cep number must have the format 1111-111'})
  @ApiModelProperty()
  cep: string;

  @ApiModelProperty()
  phone: string;

  @ApiModelProperty()
  @IsEmail({}, { message: 'the email is not valid' })
  email: string;

  @ApiModelProperty()
  website: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumber({}, { message: 'the logo file id should be a number'})
  logo_file_id: number;

  @ApiModelProperty()
  @IsNumber({}, { message: 'the account_id should be a number'})
  account_id: number;

  @ApiModelProperty()
  @IsBoolean()
  active: boolean;
}
