import { Column, Entity, JoinTable, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { UserFiscalEntity } from './user-fiscal-entity.entity';
import { MediaDto } from '../../common/entities/media.dto';

@Entity()
export class FiscalEntity extends BaseEntity {
  @Column()
  name: string;

  @Column()
  nif: number;

  @Column()
  street: string;

  @Column()
  door: number;

  @Column()
  city: string;

  @Column()
  cep: string;

  @Column()
  phone: string;

  @Column()
  email: string;

  @Column()
  website: string;

  @Column({ nullable: true})
  logo_file_id: number;
  logo_file: MediaDto;

  @OneToMany(type => UserFiscalEntity, ufe => ufe.entity)
  @JoinTable()
  users: UserFiscalEntity[];

  @Column({ nullable: false })
  account_id: number;
}
