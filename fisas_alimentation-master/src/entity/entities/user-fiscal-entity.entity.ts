import { Column, Entity, ManyToOne } from 'typeorm';
import { FiscalEntity } from './fiscal-entity.entity';

@Entity()
export class UserFiscalEntity {

  @ManyToOne(type => FiscalEntity, fe => fe.users,  { primary: true, cascade: true, onDelete: 'CASCADE' })
  entity: FiscalEntity;

  @Column('int', { primary: true, nullable: false })
  user_id: number;

  @Column('int', { nullable: false })
  created_by_user_id: number;

  @Column({ nullable: false, default: false })
  manager: boolean;
}