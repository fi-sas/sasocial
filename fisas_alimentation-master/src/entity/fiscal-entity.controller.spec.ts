import { Test, TestingModule } from '@nestjs/testing';
import { FiscalEntityController } from './fiscal-entity.controller';

describe('Fiscal Entity Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [FiscalEntityController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: FiscalEntityController = module.get<FiscalEntityController>(FiscalEntityController);
    expect(controller).toBeDefined();
  });
});
