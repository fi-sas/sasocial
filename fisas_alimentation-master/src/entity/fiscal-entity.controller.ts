import { Body, Controller, Delete, Get, HttpCode, HttpException, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { TokenGuard } from '../common/guards/token.guard';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiImplicitQuery, ApiProduces, ApiUseTags } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { Scopes } from '../common/decorators/scope.decorator';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { FiscalEntityService } from './fiscal-entity.service';
import { FiscalEntity } from './entities/fiscal-entity.entity';
import { FiscalEntityDto } from './dtos/fiscal-entity.dto';
import { UserDto } from '../common/entities/user.dto';
import { Token } from '../common/decorators/token.decorator';
import { User } from '../common/decorators/user.decorator';
import { AddUserDto } from './dtos/add-user.dto';
import { ForbiddenException } from '../common/exceptions/forbidden.exception';
import { MediasService } from '../common/services/medias.service';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Entities')
@Controller(CONFIGURATIONS.URL_PREFIX + '/entities')
export class FiscalEntityController {
  constructor(private readonly fiscalEntService: FiscalEntityService,
              private readonly mediaService: MediasService) {
  }

  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "files"', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @Scopes('entities')
  @Get()
  public async findAll(@Query() query, @Token() token) {

    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation => relation !== 'files');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.fiscalEntService.findAll(query.offset, query.limit, query.sort, relateds, token);

    return new Response<FiscalEntity[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
      },
    );
  }

  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "files"', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('entities')
  @Get(':id')
  public async findById(@Param('id', new ParseIntPipe()) id, @Query() query, @Token() token) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation => relation !== 'files');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.fiscalEntService.findByID(id, relateds, token);

    return new Response<FiscalEntity>(ResponseStatus.OK, entity);
  }

  @Scopes('entities')
  @Post()
  public async create(@Body() entity: FiscalEntityDto, @User() user, @Token() token) {
    await this.mediaService.get(entity.logo_file_id, token).catch(reason => {
      throw new HttpException(reason.response.data.errors, reason.response.status);
    });

    const createdEntity = await this.fiscalEntService.create(entity, token);
    await this.fiscalEntService.addUser(createdEntity.id, user.id, true, user.id, token);
    return new Response<FiscalEntity>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('entities')
  @Put(':id')
  public async update(@Param('id', new ParseIntPipe()) id, @Body() entity: FiscalEntityDto, @Token() token) {
    await this.mediaService.get(entity.logo_file_id, token).catch(reason => {
      throw new HttpException(reason.response.data.errors, reason.response.status);
    });

    const updatedEntity = await this.fiscalEntService.update(id, entity, token);
    return new Response<FiscalEntity>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('entities')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('id', new ParseIntPipe()) id) {
    const result = await this.fiscalEntService.delete(id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }

  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('entities')
  @Get(':id/users')
  public async getUsers(@Param('id', new ParseIntPipe()) id, @Query() query, @Token() token, @User() user) {
    const result = await this.fiscalEntService.isManager(id, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;

    const enteties = await this.fiscalEntService.getUsers(id, query.offset, query.limit, token);
    return new Response<UserDto[]>(ResponseStatus.OK, enteties[0],
      { total: enteties[1] },
    );
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id_entity', type: Number })
  @Scopes('entities')
  @Post(':id_entity/users')
  public async addUser(@Param('id_entity', new ParseIntPipe()) id_entity,
                       @Body() entity: AddUserDto, @User() user, @Token() token) {
    const result = await this.fiscalEntService.isManager(id_entity, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    return new Response<UserDto>(ResponseStatus.OK, await this.fiscalEntService.addUser(id_entity,
      entity.user_id, entity.manager, user.id, token));
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id_entity', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the user', name: 'id_user', type: Number })
  @Scopes('entities')
  @Delete(':id_entity/users/:id_user')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async removeUser(@Param('id_entity', new ParseIntPipe()) id_entity,
                          @Param('id_user', new ParseIntPipe()) id_user, @User() user, @Token() token) {
    const result = await this.fiscalEntService.isManager(id_entity, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    return new Response<any>(ResponseStatus.OK, await this.fiscalEntService.removeUser(id_entity, id_user));
  }
}
