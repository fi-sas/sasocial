import { HttpModule, Module } from '@nestjs/common';
import { FiscalEntityController } from './fiscal-entity.controller';
import { FiscalEntityService } from './fiscal-entity.service';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FiscalEntity } from './entities/fiscal-entity.entity';
import { UserFiscalEntity } from './entities/user-fiscal-entity.entity';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    JwtModule.register({}),
    HttpModule,
    TypeOrmModule.forFeature([
      FiscalEntity,
      UserFiscalEntity,
    ])
  ],
  controllers: [
    FiscalEntityController
  ],
  providers: [
    FiscalEntityService,
  ],
  exports: [
    FiscalEntityService
  ]
})
export class FiscalEntityModule {}
