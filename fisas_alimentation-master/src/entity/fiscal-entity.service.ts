import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FiscalEntity } from './entities/fiscal-entity.entity';
import { FiscalEntityDto } from './dtos/fiscal-entity.dto';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { UserFiscalEntity } from './entities/user-fiscal-entity.entity';
import { UserDto } from '../common/entities/user.dto';
import { UsersService } from '../common/services/users.service';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ResponseError } from '../common/response.model';
import { ForbiddenException } from '../common/exceptions/forbidden.exception';
import { MediasService } from '../common/services/medias.service';

@Injectable()
export class FiscalEntityService {

  constructor(@InjectRepository(FiscalEntity) private readonly entitiesRepo: Repository<FiscalEntity>,
              @InjectRepository(UserFiscalEntity) private readonly userEntitiesRepo: Repository<UserFiscalEntity>,
              private readonly usersService: UsersService,
              private readonly mediaService: MediasService) {
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[], token: string): Promise<[FiscalEntity[], number]> {
    const entitiesQB = this.entitiesRepo.createQueryBuilder('entity');

    if (limit > 0) {
      entitiesQB.take(limit);
      entitiesQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      entitiesQB.orderBy('entity.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    const entities = await entitiesQB.getManyAndCount();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelateds(entities[0], 'logo_file_id', 'logo_file', token);
    }

    return entities;
  }

  public async findByID(id: number, relateds: string[], token: string): Promise<FiscalEntity> {
    const persistedEntity = await this.entitiesRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const entityQB = this.entitiesRepo.createQueryBuilder('entity');
    entityQB.where(' id = :id', { id });
    const entity = await entityQB.getOne();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelated(entity, 'logo_file_id', 'logo_file', token);
    }

    return entity;
  }

  public async existID(id: number): Promise<number> {
    return await this.entitiesRepo.count({ id });
  }

  public async create(entityDto: FiscalEntityDto, token: string): Promise<FiscalEntity> {
    const insertResult = await this.entitiesRepo.createQueryBuilder()
      .insert()
      .into(FiscalEntity)
      .values({
        name: entityDto.name,
        cep: entityDto.cep,
        city: entityDto.city,
        door: entityDto.door,
        email: entityDto.email,
        logo_file_id: entityDto.logo_file_id,
        nif: entityDto.nif,
        phone: entityDto.phone,
        street: entityDto.street,
        website: entityDto.website,
        account_id: entityDto.account_id,
        active: entityDto.active,
      })
      .execute();
    return await this.findByID(insertResult.identifiers[0].id, ['files'], token);
  }

  public async update(id: number, entityDto: FiscalEntityDto, token: string): Promise<FiscalEntity> {
    const persistedEntity = await this.entitiesRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    await this.entitiesRepo.createQueryBuilder()
      .update(FiscalEntity)
      .set({
        name: entityDto.name,
        cep: entityDto.cep,
        city: entityDto.city,
        door: entityDto.door,
        email: entityDto.email,
        logo_file_id: entityDto.logo_file_id,
        nif: entityDto.nif,
        phone: entityDto.phone,
        street: entityDto.street,
        website: entityDto.website,
        account_id: entityDto.account_id,
        active: entityDto.active,
      })
      .where({ id })
      .execute();
    return await this.findByID(id, ['files'], token);
  }

  public async delete(id: number): Promise<any> {
    const persistedEntity = await this.entitiesRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    await this.entitiesRepo.delete({ id }).catch(err => {

      if (err.errno === 1451) {
        throw new HttpException(new ResponseError(20191, 'You can not delete an entity because of ' +
          'a dependency associated with it'), HttpStatus.CONFLICT);
      }
      throw new HttpException(err, 500);
    });
  }

  public async getUsers(entity_id: number, offset: number, limit: number, token: string): Promise<[UserDto[], number]> {
    const persistedEntity = await this.entitiesRepo.count({ id: entity_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity doesnt exist');
    }

    const ufeQB = this.userEntitiesRepo.createQueryBuilder('ufe');
    if (limit > 0) {
      ufeQB.take(limit);
      ufeQB.skip(offset);
    }

    ufeQB.where('entityId = :entity_id', { entity_id });

    const entities = await ufeQB.getManyAndCount();

    const tusers: UserDto[] = [];
    for (const entity of entities[0]) {
      try {
        const tuser = await this.usersService.get(entity.user_id, token);
        tuser.manager = entity.manager;
        tusers.push(tuser);
      } catch (errors) {
        if (errors[0].code === 404) {
          this.userEntitiesRepo.createQueryBuilder()
            .delete()
            .from(UserFiscalEntity)
            .where('user_id = :id', { id: entity.user_id })
            .execute();
          entities[1]--;
        }
      }
    }

    return [tusers, entities[1]];
  }

  public async addUser(entity_id: number, user_id: number, isManager: boolean, created_by_user_id: number, token: string): Promise<UserDto> {
    const entity = await this.findByID(entity_id, [], token);
    let user = null;
    try {
      user = await this.usersService.get(user_id, token);
    } catch (e) {
      throw new HttpException(e, e[0].code);
    }
    if (user.active === false) {
      throw new ValidationException([new ResponseError(400, 'the user is not active')]);
    }

    if (await this.userEntitiesRepo.count({ user_id, entity: { id: entity_id } }) === 0) {
      await this.userEntitiesRepo.manager.transaction(async entityManger => {
        await entityManger.createQueryBuilder()
          .insert()
          .into(UserFiscalEntity)
          .values({
            entity,
            user_id,
            manager: isManager,
            created_by_user_id,
          }).execute();
      }).catch(reason => {
        throw new HttpException(reason, 500);
      });
    }

    user.manager = isManager;
    return user;
  }

  public async removeUser(entity_id: number, user_id: number): Promise<any> {
    const persistedEntity = await this.entitiesRepo.count({ id: entity_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    await this.userEntitiesRepo.createQueryBuilder()
      .delete()
      .from(UserFiscalEntity)
      .where(' entityId = :entity_id AND user_id = :user_id', { entity_id, user_id }).execute();

    return true;
  }

  public async checkUserPermission(entity_id: number, user_id: number): Promise<boolean> {
    const persistedEntity = await this.entitiesRepo.count({ id: entity_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Fiscal Entity Not Found');
    }

    const result = await this.userEntitiesRepo.createQueryBuilder()
      .where(' entityId = :entity_id AND user_id = :user_id', { entity_id, user_id }).getCount();

    if (result === 0) {
      throw new ForbiddenException(910, 'No permission to access this fiscal entity');
    } else {
      return true;
    }
  }

  public async isManager(entity_id: number, user_id: number): Promise<boolean> {
    const persistedEntity = await this.entitiesRepo.count({ id: entity_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Fiscal Entity Not Found');
    }

    const result = await this.userEntitiesRepo.createQueryBuilder()
      .where(' entityId = :entity_id AND user_id = :user_id AND manager = TRUE', { entity_id, user_id }).getCount();

    return result !== 0;
  }
}
