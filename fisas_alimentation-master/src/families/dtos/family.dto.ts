import { ArrayMinSize, IsArray, IsBoolean, IsNotEmpty, IsNumber, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';

export class FamilyTranslationDto {
  @ApiModelProperty()
  @IsNumber({}, { message: 'id da linguagem não esta correto' })
  language_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'name não pode ser nulo'})
  @IsString({ message: 'name têm de ser texto'})
  name: string;
}

export class FamilyDto {
  id: number;

  @ApiModelProperty( {type: FamilyTranslationDto, isArray: true, minItems: 1 })
  @IsArray({ message: 'translation têm de se um array'})
  @ArrayMinSize(1, { message: 'Têm de existir uma tradução no minimo'})
  @ValidateNested()
  @Type(() => FamilyTranslationDto)
  translations: FamilyTranslationDto[];

  @ApiModelProperty()
  file_id: number;

  @ApiModelProperty( {type: Number, isArray: true})
  @IsArray({ message: 'products têm de se um array'})
  products: number[];

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}