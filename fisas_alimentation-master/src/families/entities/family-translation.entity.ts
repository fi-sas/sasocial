import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Family } from './family.entity';

@Entity()
export class FamilyTranslation {
  @PrimaryColumn()
  language_id: number;

  @ManyToOne(type => Family, family => family.translations, {primary: true, cascade: true, onDelete: 'CASCADE'})
  family: Family;

  @Column({ nullable: false})
  name: string;
}