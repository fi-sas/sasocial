import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { FamilyTranslation } from './family-translation.entity';
import { Product } from '../../products/entities/product.entity';
import { Service } from '../../service/entities/service.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { MediaDto } from '../../common/entities/media.dto';
@Entity()
export class Family extends BaseEntity{

  @OneToMany(type => FamilyTranslation, translation => translation.family)
  @JoinColumn()
  translations: FamilyTranslation[];

  @Column({ nullable: true })
  file_id: number;
  file: MediaDto;

  @ManyToMany(type => Product, product => product.families)
  @JoinTable()
  products: Product[];

  @ManyToMany(type => Service, service => service.families)
  @JoinTable()
  services: Service[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}