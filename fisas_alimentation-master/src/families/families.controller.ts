import { Body, Controller, Delete, Get, Headers, HttpCode, HttpException, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitQuery, ApiImplicitHeader } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { FamiliesService } from './families.service';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { Family } from './entities/family.entity';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { FamilyDto } from './dtos/family.dto';
import { ValidationException } from '../common/exceptions/validation.exception';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { LanguagesService } from '../common/services/languages.service';
import { Token } from '../common/decorators/token.decorator';
import { MediasService } from '../common/services/medias.service';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Families')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/families')
export class FamiliesController {

  constructor(private familiesService: FamiliesService,
              private readonly languageService: LanguagesService,
              private readonly mediaService: MediasService) {
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products", "files" and "services" ', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'name',
    description: 'Find families by partial name.', type: String,
  })
  @Scopes('families')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id, @Token() token) {

    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';
    if (!query.hasOwnProperty('name')) query.name = null;

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products' &&
        relation !== 'files' &&
        relation !== 'services');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.familiesService.findAll(query.offset, query.limit, query.sort, relateds, query.name, language_id, entity_id, token);

    return new Response<Family[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
      },
    );
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products", "files" and "services" ', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the family', name: 'id', type: Number })
  @Scopes('families')
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                        @Query() query, @Headers('x-language-id') language_id, @Token() token) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products' &&
        relation !== 'files' &&
        relation !== 'services');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.familiesService.findByID(id, relateds, language_id, entity_id, token);

    return new Response<Family>(ResponseStatus.OK, entity);
  }

  @Scopes('families')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: FamilyDto, @Token() token) {
    if (entity.translations.length === 0) {
      throw new ValidationException([new ResponseError(2021, 'têm de existir uma tradução pelo menos')]);
    }
    await this.languageService.validateBody(entity, token)
      .catch(reason => {
        throw new HttpException(reason[0], reason[0].code);
      });

    if (entity.file_id) {
      await this.mediaService.get(entity.file_id, token).catch(reason => {
        throw new HttpException(reason.response.data.errors, reason.response.status);
      });
    }

    const createdEntity = await this.familiesService.create(entity, entity_id, token);
    return new Response<Family>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the family', name: 'id', type: Number })
  @Scopes('families')
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id,
                      @Param('id', new ParseIntPipe()) id, @Body() entity: FamilyDto, @Token() token) {
    if (entity.translations.length === 0) {
      throw new ValidationException([new ResponseError(2021, 'têm de existir uma tradução pelo menos')]);
    }
    await this.languageService.validateBody(entity, token)
      .catch(reason => {
        throw new HttpException(reason[0], reason[0].code);
      });

    if (entity.file_id) {
      await this.mediaService.get(entity.file_id, token).catch(reason => {
        throw new HttpException(reason.response.data.errors, reason.response.status);
      });
    }

    const updatedEntity = await this.familiesService.update(id, entity, entity_id, token);
    return new Response<Family>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the family', name: 'id', type: Number })
  @Scopes('families')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.familiesService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}