import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Family } from './entities/family.entity';
import { FamilyTranslation } from './entities/family-translation.entity';
import { FamiliesController } from './families.controller';
import { FamiliesService } from './families.service';
import { ProductsModule } from '../products/products.module';
import { JwtModule } from '@nestjs/jwt';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    ProductsModule,
    TypeOrmModule.forFeature([
      Family,
      FamilyTranslation
    ]),
    HttpModule,
    JwtModule.register({})
  ],
  controllers: [
    FamiliesController
  ],
  providers: [
    FamiliesService
  ],
  exports: [
    FamiliesService
  ]
})
export class FamiliesModule {}