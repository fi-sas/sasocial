import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Family } from './entities/family.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { FamilyDto } from './dtos/family.dto';
import { FamilyTranslation } from './entities/family-translation.entity';
import { Product } from '../products/entities/product.entity';
import { ProductsService } from '../products/products.service';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { MediasService } from '../common/services/medias.service';

@Injectable()
export class FamiliesService {
  constructor(@InjectRepository(Family) private readonly familiesRepo: Repository<Family>,
              private readonly productsService: ProductsService,
              private readonly mediaService: MediasService
  ) {
  }

  public async create(entityDto: FamilyDto, entity_id: number, token: string): Promise<Family> {
    return new Promise<Family>(async (resolve, reject) => {
      let id: number = 0;
      const tfe = new FiscalEntity();
      tfe.id = entity_id;

      await this.familiesRepo.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(Family)
          .values({
            file_id: entityDto.file_id,
            active: entityDto.active,
            fentity: tfe
          })
          .execute();

        id = insertResult.identifiers[0].id;
        const tfamily = new Family();
        tfamily.id = id;

        for (const translation of entityDto.translations) {
          const ttranslation = new FamilyTranslation();
          ttranslation.name = translation.name;
          ttranslation.language_id = translation.language_id;
          ttranslation.family = tfamily;

          await entityManger.createQueryBuilder()
            .insert()
            .into(FamilyTranslation)
            .values(
              ttranslation,
            )
            .execute();
        }

        for (const product of entityDto.products) {
          const existProduct = await this.productsService.existID(product, entity_id);
          if (existProduct === 0) {
            throw new NotFoundException(404, 'the product with the id ' + product + ' was not found');
          }
          const tproduct = new Product();
          tproduct.id = product;

          await entityManger.createQueryBuilder()
            .relation(Family, 'products')
            .of(id)
            .add(tproduct);
        }
      }).catch(reason => {
        reject(reason);
      });
      resolve(await this.findByID(id, ['products', 'files'], null, entity_id, token));
    });
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[],
                       partial_name: string, language_id: number, entity_id: number, token: string) {
    const familyQB = this.familiesRepo.createQueryBuilder('family');

    if (limit > 0) {
      familyQB.take(limit);
      familyQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      familyQB.orderBy('family.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    familyQB.leftJoinAndSelect('family.translations',
      'family_translation',
      language_id ? 'family_translation.language_id = :language_id' : '',
      { language_id });

    if (relateds.find(related => related === 'products')) {
      familyQB.leftJoinAndSelect('family.products', 'products',
        'products.is_deleted = false');
      familyQB.leftJoinAndSelect('products.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'services')) {
      familyQB.leftJoinAndSelect('family.services', 'services');
    }

    familyQB.where(' family.fentityId = :entity_id ', { entity_id});

    if (partial_name) {
      familyQB.andWhere(' family_translation.name LIKE :partial_name ', { partial_name: '%' + partial_name + '%' });
    }

    const entities = await familyQB.getManyAndCount();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelateds(entities[0], 'file_id', 'file', token);
    }

    return entities;
  }

  public async findByID(id: number, relateds: string[], language_id: number, entity_id: number, token: string): Promise<Family> {
    const persistedEntity = await this.familiesRepo.count({ id, fentity: { id: entity_id } });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const familyQB = this.familiesRepo.createQueryBuilder('family');

    familyQB.leftJoinAndSelect('family.translations',
      'family_translation',
      language_id ? 'family_translation.language_id = :language_id' : '',
      { language_id });

    if (relateds.find(related => related === 'products')) {
      familyQB.leftJoinAndSelect('family.products', 'products',
      'products.is_deleted = false');
      familyQB.leftJoinAndSelect('products.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'services')) {
      familyQB.leftJoinAndSelect('family.services', 'services');
    }
    familyQB.where('family.id = :id', { id });

    const entity = await familyQB.getOne();

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelated(entity, 'file_id', 'file', token);
    }

    return entity;
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.familiesRepo.count({ id, fentity: { id: entity_id }  });
  }

  public async existIDs(ids: number[], entity_id: number): Promise<number> {
    if (ids.length > 0) {
      return await this.familiesRepo.count({ id: In(ids), fentity: { id: entity_id } });
    } else {
      return 0;
    }
  }

  public async update(id: number, entityDto: FamilyDto, entity_id: number, token: string) {
    const persistedEntity = await this.familiesRepo.count({ id, fentity: { id: entity_id }  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    return new Promise<Family>(async (resolve, reject) => {
      await this.familiesRepo.manager.transaction(async entityManger => {
        await entityManger.createQueryBuilder()
          .update(Family)
          .where('id = :id', { id })
          .set({
            file_id: entityDto.file_id,
            active: entityDto.active,
          })
          .execute();

        const tfamily = new Family();
        tfamily.id = id;

        await entityManger.createQueryBuilder()
          .delete()
          .from(FamilyTranslation)
          .where('familyId = :id', { id })
          .execute();

        for (const translation of entityDto.translations) {
          const ttranslation = new FamilyTranslation();
          ttranslation.name = translation.name;
          ttranslation.language_id = translation.language_id;
          ttranslation.family = tfamily;

          await entityManger.createQueryBuilder()
            .insert()
            .into(FamilyTranslation)
            .values(
              ttranslation,
            )
            .execute();
        }

        await entityManger.createQueryBuilder()
          .delete()
          .from('family_products_product')
          .where('familyId = :id', { id })
          .execute();

        for (const product of entityDto.products) {
          const existProduct = await this.productsService.existID(product, entity_id);
          if (existProduct === 0) {
            throw new NotFoundException(404, 'the product with the id ' + product + ' was not found');
          }
          const tproduct = new Product();
          tproduct.id = product;

          await entityManger.createQueryBuilder()
            .relation(Family, 'products')
            .of(id)
            .add(tproduct);
        }
      }).catch(reason => {
        reject(reason);
      });
      resolve(await this.findByID(id, ['products', 'files'], null, entity_id, token));
    });
  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.familiesRepo.count({ id, fentity: { id: entity_id }  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    return await this.familiesRepo.delete({ id });
  }
}