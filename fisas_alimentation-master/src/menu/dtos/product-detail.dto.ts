import { MediaDto } from '../../common/entities/media.dto';
import { NutrientProduct } from '../../products/entities/nutrient-product.entity';
import { Allergen } from '../../allergens/entities/allergen.entity';
import { PriceVariation } from '../../products/entities/price-variation.entity';
import { ApiModelProperty } from '@nestjs/swagger';
import { ProductTranslation } from '../../products/entities/product-translation.entity';
import { DishTypeTranslation } from '../../dishs/entities/dish-type-translation.entity';
import { DishTranslation } from '../../dishs/entities/dish-translation.entity';
import {UserAllergensListDto} from '../../allergens/dtos/user-allergens-list.dto';

export class ProductDetailDto {
  @ApiModelProperty()
  file_id: number;
  @ApiModelProperty()
  file: MediaDto;
  @ApiModelProperty( {type: DishTypeTranslation, isArray: true })
  dish_type_translation: DishTypeTranslation[];
  @ApiModelProperty( {type: ProductTranslation, isArray: true })
  translations: ProductTranslation[] | DishTranslation[];
  @ApiModelProperty()
  location: string;
  @ApiModelProperty({ isArray: true})
  composition: string[];
  @ApiModelProperty( {type: NutrientProduct, isArray: true})
  nutrients: NutrientProduct[];
  @ApiModelProperty( {type: UserAllergensListDto, isArray: true })
  allergens: UserAllergensListDto[];
  @ApiModelProperty()
  user_allergic: boolean = false;
  @ApiModelProperty( {type: PriceVariation, isArray: true })
  prices: PriceVariation[];
  @ApiModelProperty()
  price: number;
  @ApiModelProperty()
  tax_id: number;
  @ApiModelProperty()
  service_id: number;
  @ApiModelProperty()
  account_id: number;
}
