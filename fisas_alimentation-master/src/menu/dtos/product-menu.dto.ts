import { ProductTranslation } from '../../products/entities/product-translation.entity';
import { NutrientProduct } from '../../products/entities/nutrient-product.entity';
import { Complement } from '../../products/entities/complement.entity';
import { Product } from '../../products/entities/product.entity';
import { PriceVariation } from '../../products/entities/price-variation.entity';
import { DishTranslation } from '../../dishs/entities/dish-translation.entity';
import { MediaDto } from '../../common/entities/media.dto';
import { DishTypeTranslation } from '../../dishs/entities/dish-type-translation.entity';
import { UserAllergensListDto } from '../../allergens/dtos/user-allergens-list.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { Meal } from '../../menus/entities/menu.entity';

export class ProductMenuDto {
  @ApiModelProperty()
  id: number = null;
  @ApiModelProperty()
  code: string = '';
  @ApiModelProperty()
  price: number = 0;
  @ApiModelProperty()
  tax_id: number = null;
  @ApiModelProperty()
  prices: PriceVariation[] = [];
  @ApiModelProperty()
  available: boolean = false;
  @ApiModelProperty()
  show_derivatives: boolean = false;
  @ApiModelProperty()
  dish_type_translation: DishTypeTranslation[];
  @ApiModelProperty()
  dish_type_id: number;
  @ApiModelProperty()
  translations: ProductTranslation[] | DishTranslation[] = [];
  @ApiModelProperty()
  location: string;
  @ApiModelProperty({ type: UserAllergensListDto, isArray: true })
  allergens: UserAllergensListDto[];
  @ApiModelProperty()
  user_allergic: boolean = false;
  @ApiModelProperty()
  nutrients: NutrientProduct[] = [];
  @ApiModelProperty()
  complements: Complement[] = [];
  @ApiModelProperty()
  isStockAvailable: boolean = false;
  @ApiModelProperty()
  stockQuantity: number = 0;
  @ApiModelProperty()
  file: MediaDto = null;
  @ApiModelProperty()
  file_id: number = null;
  @ApiModelProperty()
  service_id: number = null;
  @ApiModelProperty()
  account_id: number = null;
  @ApiModelProperty()
  meal: Meal = null;
  @ApiModelProperty()
  date: Date = null;

  static fromProduct(product: Product): ProductMenuDto {
    const tpmd = new ProductMenuDto();
    tpmd.id = product.id;
    tpmd.code = product.code;
    tpmd.price = product.price;
    tpmd.tax_id = product.tax_price_id;
    tpmd.prices = product.prices;
    tpmd.available = product.available;
    tpmd.show_derivatives = product.show_derivatives;
    tpmd.translations = product.translations;
    tpmd.allergens = product.allergens.map(a => UserAllergensListDto.fromAllergen(a, false));
    tpmd.nutrients = product.nutrients;
    tpmd.complements = product.complements;
    tpmd.file_id = product.file_id;
    return tpmd;
  }
}
