import { School } from '../../schools/entities/school.entity';
import { Service } from 'service/entities/service.entity';

export class SchoolMenuDto {
  id: number;
  name: string;
  acronym: string;
  isDefault: boolean;
  active: boolean;
  services?: Service[];
  
  static fromSchool(school: School, isDefault: boolean): SchoolMenuDto {
    const tfs = new SchoolMenuDto();
    tfs.id = school.id;
    tfs.name = school.name;
    tfs.acronym = school.acronym;
    tfs.isDefault = isDefault;
    tfs.active = school.active;

    return tfs;
  }
}