export class MenuDisponibilityDto {
  disponible: boolean;
  quantity: number;
}
