import { hasOwnProperty } from 'tslint/lib/utils';
import { ReservationCancelDto } from './../reservations/dtos/reservation-cancel.dto';
import { UsersService } from './../common/services/users.service';
import { Body, Controller, Get, Headers, Param, Put, Query, UseGuards, HttpException, Delete, Post } from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiConsumes,
    ApiImplicitHeader,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiProduces,
    ApiResponse,
    ApiUseTags,
    ApiOperation
} from '@nestjs/swagger';
import {Response, ResponseError, ResponseStatus} from '../common/response.model';
import {ParseIntPipe} from '../common/pipes/parse-int.pipe';
import {MenuService} from './menu.service';
import {Meal, Menu} from '../menus/entities/menu.entity';
import {ParseMealPipe} from '../common/pipes/parse-meal.pipe';
import {ParseDatePipe} from '../common/pipes/parse-date.pipe';
import {TokenGuard} from '../common/guards/token.guard';
import {Service} from '../service/entities/service.entity';
import {Family} from '../families/entities/family.entity';
import {ProductMenuDto} from './dtos/product-menu.dto';
import {DeviceID} from '../common/decorators/deviceID.decorator';
import {Token} from '../common/decorators/token.decorator';
import {SchoolMenuDto} from './dtos/school-menu.dto';
import {ProductDetailDto} from './dtos/product-detail.dto';
import {LanguagesService} from '../common/services/languages.service';
import {NotFoundException} from '../common/exceptions/not-found.exception';
import {User} from '../common/decorators/user.decorator';
import {CONFIGURATIONS} from '../app.config';
import {UserAllergen} from '../allergens/entities/user-allergen.entity';
import {ValidationException} from '../common/exceptions/validation.exception';
import {UsersAllergensService} from '../allergens/users-allergens.service';
import {UserAllergensListDto} from '../allergens/dtos/user-allergens-list.dto';
import {UserAllergensDto} from '../allergens/dtos/user-allergens.dto';
import {isGuest} from '../common/decorators/isGuest.decorator';
import { Reservation } from '../reservations/entities/reservation.entity';
import { ReservationsService } from '../reservations/reservations.service';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Menu')
@Controller(CONFIGURATIONS.URL_PREFIX + '/menu')
export class MenuController {

    constructor(private menuService: MenuService,
                private reservationsService: ReservationsService,
                private readonly userAllergens: UsersAllergensService,
                private readonly userService: UsersService,
                private languagesService: LanguagesService) {
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @Put('user-config/allergens')
    public async updateUserConfigAllergens(@Body() allergens: UserAllergensDto, @User() user,  @isGuest() isGuest, @Token() token,
                                           @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym) {
        if (isGuest) {
            throw new ValidationException([new ResponseError(45632, 'the guest token is not permitted')]);
        }

        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        return new Response<UserAllergensListDto[]>(ResponseStatus.OK, await this.userAllergens.saveConfig(user.id, allergens, language_id));
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @Get('user-config/allergens')
    public async listUserConfigAllergens(@User() user, @isGuest() isGuest, @Token() token,
                                         @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym) {
        if (isGuest) {
            throw new ValidationException([new ResponseError(45632, 'the guest token is not permitted')]);
        }

        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        return new Response<UserAllergensListDto[]>(ResponseStatus.OK, await this.userAllergens.returnsConfig(user.id, language_id));
    }
    @ApiOperation({ title: 'Returns the number of available reservations', description: 'Returns the number of reservations available for comsuption'})
    @Get('user/reservations/count')
    public async getUserReservationsCount(@User() user,  @isGuest() isGuest, @Token() token) {
        if (isGuest) {
            throw new ValidationException([new ResponseError(45632, 'the guest token is not permitted')]);
        }

        const result = await this.reservationsService.getUserReservationsCount(user.id);
        return new Response<Reservation[]>(ResponseStatus.OK, result, {
            total: result.total
        });
    }


    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'onlyAvailables',
        description: 'Filter the reservations by served flag', type: Boolean,
      })
    @ApiImplicitQuery({
        required: false, name: 'month',
        description: 'Filter the reservations by the month sent, if you dont send the year it will be used the current year', type: Number,
      })
      @ApiImplicitQuery({
        required: false, name: 'year',
        description: 'Filter the reservations by the year sent', type: Number,
      })
      @ApiImplicitQuery({
        required: false, name: 'sort',
        description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
      })
    @ApiOperation({ title: 'Returns user reservations', description: 'Returns the reservations available and not available for the month and year' +
    ' sent'})
    @Get('user/reservations')
    public async getUserReservations(@User() user,  @isGuest() isGuest, @Token() token, @Query() query,
                                           @Headers('x-language-id') language_id,
                                            @Headers('x-language-acronym') language_acronym) {
        if (isGuest) {
            throw new ValidationException([new ResponseError(45632, 'the guest token is not permitted')]);
        }

        let onlyAvailables = query.hasOwnProperty('onlyAvailables') ? (query.onlyAvailables === 'true'? true : false ) : true;
        let month = query.hasOwnProperty('month') ? query.month : null;
        let year = query.hasOwnProperty('year') ? query.year : null;
        let sort = query.hasOwnProperty('sort') ? query.sort : '-date';

        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        const result = await this.reservationsService.getUserReservations(user.id, month, year, onlyAvailables, sort, language_id, token);
        return new Response<Reservation[]>(ResponseStatus.OK, result[0], {
            total: result[1]
        });
    }

    @ApiImplicitParam({
      required: true,
      description: 'id of the reservation',
      name: 'id',
      type: Number,
    })
    @ApiOperation({ title: 'Cancel reservation',
     description: 'Annullement of reservation and return the value to current_account'})
    @Post('user/reservations/:id/cancel')
    public async cancelReservation(
      @User() user,
      @isGuest() isGuest,
      @Param('id', new ParseIntPipe()) id,
      @Body() body: ReservationCancelDto,
      @Token() token,
    ) {
      if (isGuest) {
        throw new ValidationException([new ResponseError(45632, 'the guest token is not permitted')]);
      }

      return new Response<Reservation>(ResponseStatus.OK,
        await this.reservationsService.cancelReservation(id, body.payment_method_id ,token, user.id));
    }

    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
          'Possible values include "services"', type: String,
      })
    @Get('schools')
    public async menuSchools(@Query() query, @DeviceID() device_id, @Token() token) {

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
          relateds = query.withRelated.split(',');
          const relationErro = relateds.find(relation =>
            relation !== 'services');
            
          if (relationErro)
            
            throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entities = await this.menuService.getSchools(device_id, relateds, token);
        return new Response<SchoolMenuDto[]>(ResponseStatus.OK, entities[0],
            {
                total: entities[1],
            });
    }

    @ApiImplicitParam({required: true, description: 'id of the school', name: 'school_id', type: Number})
    @Get(':school_id/services') // DEVOLVE SERVIÇOS
    public async menuServices(@Param('school_id', new ParseIntPipe()) school_id, @Query() query,
                              @DeviceID() device_id) {
        const entities = await this.menuService.getServices(device_id, school_id);
        return new Response<Service[]>(ResponseStatus.OK, entities[0],
            {
                total: entities[1],
            });
    }

    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @Get('service/:service_id') // DEVOLVE SERVIÇOS INFO
    public async menuServiceId(@Param('service_id', new ParseIntPipe()) service_id, @Query() query) {
        const entities = await this.menuService.getServiceById(service_id);
        return new Response<Service>(ResponseStatus.OK, entities,
        );
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @Get('service/:service_id/families') // DEVOLVE FAMILIAS DE SERVIÇOS
    public async menu_families(@Param('service_id', new ParseIntPipe()) id, @Query() query,
                               @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym,
                               @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        const entities = await this.menuService.getServiceFamilies(id, device_id, language_id, token);
        return new Response<Family[]>(ResponseStatus.OK, entities);
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @ApiImplicitParam({required: true, description: 'id of the family', name: 'family_id', type: Number})
    @Get('service/:service_id/families/:family_id/products') // DEVOLVE PRODUTOS DE FAMILIA DE UM SERVIÇO
    public async menu_families_product(@Param('service_id', new ParseIntPipe()) service_id,
                                       @Param('family_id', new ParseIntPipe()) family_id,
                                       @Query() query, @Headers('x-language-id') language_id,
                                       @User() user,
                                       @Headers('x-language-acronym') language_acronym, @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entities = await this.menuService.getServiceFamilyProduct(service_id, family_id, device_id, language_id, user, relateds, token);
        return new Response<ProductMenuDto[]>(ResponseStatus.OK, entities);
    }

    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @ApiImplicitParam({required: true, description: 'id of the product', name: 'product_id', type: Number})
    @Get('service/:service_id/products/:product_id/disponibility')
    public async checkProductDisponibility(@Param('service_id', new ParseIntPipe()) service_id,
                                           @Param('product_id', new ParseIntPipe()) product_id) {
        return new Response(ResponseStatus.OK, await this.menuService.checkProductDisponibility(product_id));
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @ApiImplicitParam({required: true, description: 'id of the product', name: 'product_id', type: Number})
    @ApiResponse({status: 200, type: ProductDetailDto})
    @Get('service/:service_id/products/:product_id') // TODO DEVOLVE INFOS PRODUTOS
    public async menu_product(@Param('service_id', new ParseIntPipe()) service_id,
                              @Param('product_id', new ParseIntPipe()) product_id,
                              @Query() query, @Headers('x-language-id') language_id,
                              @User() user,
                              @Headers('x-language-acronym') language_acronym, @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entity = await this.menuService.getProductInfo(service_id, product_id, device_id, language_id, user, relateds, token);
        return new Response<ProductDetailDto>(ResponseStatus.OK, entity);
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @Get('service/:service_id/recents') // TODO DEVOLVE PRODUTOS RECENTES
    public async menu_products_recents(@Param('id', new ParseIntPipe()) id, @Query() query,
                                       @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym,
                                       @User() user, @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entities = await this.menuService.getServiceFamilyProduct(id, null, device_id, language_id, user, relateds, token);
        return new Response<ProductMenuDto[]>(ResponseStatus.OK, entities);
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @Get('service/:service_id/products') // DEVOLVE PRODUTOS DE SERVIÇO
    public async menu_products(@Param('id', new ParseIntPipe()) id, @Query() query,
                               @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym,
                               @User() user,
                               @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entities = await this.menuService.getServiceFamilyProduct(id, null, device_id, language_id, user, relateds, token);
        return new Response<ProductMenuDto[]>(ResponseStatus.OK, entities);
    }

    @ApiImplicitQuery({
        required: false, name: 'user_id', description: 'Returns the menu for the user id sent on this param',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'meal of the menu, possibles values "lunch" | "dinner"', name: 'meal', type: Meal})
    @ApiImplicitParam({required: true, description: 'date of the menu', name: 'date', type: String})
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @Get('service/:service_id/menus/:date/:meal') // DEVOLVE MENUS
    public async menu_menus(@Param('service_id', new ParseIntPipe()) id,
                            @Param('meal', new ParseMealPipe()) meal,
                            @Param('date', new ParseDatePipe()) date,
                            @Headers('x-language-id') language_id, @Headers('x-language-acronym') language_acronym,
                            @User() user,
                            @DeviceID() device_id, @Query() query, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        let userQuery = null;
        const user_id = query.hasOwnProperty('user_id') ? query.user_id : null;
        if (user_id) {
            userQuery = await this.userService.get(user_id, token);
        }

        const entities = await this.menuService.getServiceMenu(id, meal, date, device_id, language_id, userQuery ? userQuery : user, relateds, token);
        return new Response<ProductMenuDto[]>(ResponseStatus.OK, entities);
    }

    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @ApiImplicitParam({required: true, description: 'id of the menu dish', name: 'MenuDish_id', type: Number})
    @Get('service/:service_id/dish/:MenuDish_id/disponibility')
    public async checkDishdisponibility(@Param('service_id', new ParseIntPipe()) service_id,
                                        @Param('MenuDish_id', new ParseIntPipe()) MenuDish_id) {
        return new Response(ResponseStatus.OK, await this.menuService.checkDishDisponibility(MenuDish_id));
    }

    @ApiImplicitHeader({
        required: false, name: 'X-Language-Acronym', description: 'Language acronym of the request - not recommended.' +
            ' Used when no X-Language-ID header is sent. Serves the same purposes but requires a connection with ' +
            'MS Configurations to find the specified language ID.',
    })
    @ApiImplicitHeader({
        required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
            'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
            'returned data will be filtered by received ID and will contain translations for the specified language',
    })
    @ApiImplicitQuery({
        required: false, name: 'withRelated',
        description: 'Request related entities to be included on the data objects (comma-separet list),' +
            'Possible values include "taxes" ', type: String,
    })
    @ApiImplicitParam({required: true, description: 'id of the service', name: 'service_id', type: Number})
    @ApiImplicitParam({required: true, description: 'id of the menu dish', name: 'MenuDish_id', type: Number})
    @ApiResponse({status: 200, type: ProductDetailDto})
    @Get('service/:service_id/dish/:MenuDish_id') 
    public async menu_meals_detail(@Param('service_id', new ParseIntPipe()) service_id,
                                   @Param('MenuDish_id', new ParseIntPipe()) MenuDish_id,
                                   @Query() query, @Headers('x-language-id') language_id,
                                   @Headers('x-language-acronym') language_acronym,
                                   @User() user,
                                   @DeviceID() device_id, @Token() token) {
        if (language_acronym) {
            await this.languagesService.getByAcronym(language_acronym, token).then(val => {
                if (val.data.data.length > 0) {
                    language_id = val.data.data[0].id;
                } else {
                    throw new NotFoundException(404, 'Language Not Found');
                }
            }).catch(err => {
                throw new NotFoundException(404, 'Language Not Found');
            });
        }

        let relateds = [];
        if (query.hasOwnProperty('withRelated')) {
            relateds = query.withRelated.split(',');
            const relationErro = relateds.find(relation =>
                relation !== 'taxes');
            if (relationErro)
                throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
        }

        const entity = await this.menuService.getDishInfo(service_id, MenuDish_id, device_id, language_id, user, relateds, token);
        return new Response<ProductMenuDto>(ResponseStatus.OK, entity);
    }
}
