import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DevicesService } from '../common/services/devices.service';
import { WharehousesModule } from '../wharehouses/wharehouses.module';
import { MenuService } from './menu.service';
import { Service } from '../service/entities/service.entity';
import { ServiceDevice } from '../service/entities/service-device.entity';
import { MenuController } from './menu.controller';
import { Family } from '../families/entities/family.entity';
import { Menu } from '../menus/entities/menu.entity';
import { Product } from '../products/entities/product.entity';
import { MenusModule } from '../menus/menus.module';
import { DishsModule } from '../dishs/dishs.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { SchoolsModule } from '../schools/schools.module';
import { Dish } from '../dishs/entities/dish.entity';
import { DishType } from '../dishs/entities/dish-type.entity';
import { CommomModule } from '../common/commom.module';
import {AllergensModule} from '../allergens/allergens.module';
import { ReservationsModule } from '../reservations/reservations.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    WharehousesModule,
    MenusModule,
    DishsModule,
    SchoolsModule,
    AllergensModule,
    ReservationsModule,
    TypeOrmModule.forFeature([
      Service,
      Family,
      ServiceDevice,
      Menu,
      Product,
      Dish,
      DishType
    ])
  ],
  controllers: [
    MenuController
  ],
  providers: [
    MenuService,
  ],
  exports: [
  ]
})
export class MenuModule {}