import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DevicesService } from '../common/services/devices.service';
import { StockTrack } from '../common/enums/stock-track.enum';
import { Service } from '../service/entities/service.entity';
import { Family } from '../families/entities/family.entity';
import { Product } from '../products/entities/product.entity';
import { NutrientProduct } from '../products/entities/nutrient-product.entity';
import { ProductMenuDto } from './dtos/product-menu.dto';
import { Meal } from '../menus/entities/menu.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { MenusService } from '../menus/menus.service';
import { ServiceType } from '../common/enums/service-type.enum';
import { DishsService } from '../dishs/dishs.service';
import { Disponibility } from '../products/entities/disponibility.entity';
import { SchoolsService } from '../schools/schools.service';
import { School } from '../schools/entities/school.entity';
import { SchoolMenuDto } from './dtos/school-menu.dto';
import { MediasService } from '../common/services/medias.service';
import { ProductDetailDto } from './dtos/product-detail.dto';
import { Dish } from '../dishs/entities/dish.entity';
import { DishType } from '../dishs/entities/dish-type.entity';
import { UserDto } from '../common/entities/user.dto';
import { TaxesService } from '../common/services/taxes.service';
import * as moment from 'moment';
import { UsersAllergensService } from '../allergens/users-allergens.service';
import { MenuDish } from '../menus/entities/menu-dish.entity';
import { ReservationsService } from '../reservations/reservations.service';

@Injectable()
export class MenuService {
  constructor(@InjectRepository(Service) private readonly servicesRepo: Repository<Service>,
    @InjectRepository(Family) private readonly familiesRepo: Repository<Family>,
    @InjectRepository(Product) private readonly productRepo: Repository<Product>,
    @InjectRepository(Dish) private readonly dishRepo: Repository<Dish>,
    @InjectRepository(DishType) private readonly dishTypeRepo: Repository<DishType>,
    private readonly userAllergens: UsersAllergensService,
    private readonly mediaService: MediasService,
    private readonly taxesService: TaxesService,
    private readonly menusService: MenusService,
    private readonly schoolsService: SchoolsService,
    private readonly devicesService: DevicesService,
    private readonly dishsService: DishsService,
    private readonly reservationsService: ReservationsService,
  ) {
  }

  public async getSchools(device_id: number, relateds: string[], token: string): Promise<[SchoolMenuDto[], number]> {
    const schools: [School[], number] = await this.schoolsService.findAll(0, -1, 'id', ['devices'], token);
    const defaultSchool = await this.schoolsService.schoolWithDevice(device_id);

    const resultSchool: SchoolMenuDto[] = [];
    schools[0].map(school => {
      resultSchool.push(SchoolMenuDto.fromSchool(school, false));
    });

    if (defaultSchool) {
      resultSchool.find(school => school.id === defaultSchool.id).isDefault = true;
    } else {
      if (resultSchool.length > 0) {
        resultSchool[0].isDefault = true;
      }
    }

    if (relateds.find(related => related === 'services')) {
      for (const [index, school] of resultSchool.entries()) {
        const services = await this.getServices(device_id, resultSchool[index].id);
        resultSchool[index].services = services[0];
      }
    }

    return [resultSchool, schools[1]];
  }

  public async getServices(device_id: number, school_id: number): Promise<[Service[], number]> {

    return await this.servicesRepo.createQueryBuilder('service')
      .select('service')
      .leftJoin('service.devices', 'service_device')
      .where('service_device.device_id = :device_id and service.schoolId = :school_id', { device_id, school_id })
      .getManyAndCount();
  }

  public async getServiceById(service_id: number): Promise<Service> {
    const persistedEntity = await this.servicesRepo.count({ id: service_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }

    const serviceQB = this.servicesRepo.createQueryBuilder('service');
    serviceQB.leftJoinAndSelect('service.fentity', 'entity');
    serviceQB.where({ id: service_id });

    return await serviceQB.getOne();
  }

  public async getServiceFamilies(service_id: number, device_id: number, language_id: number, token: string): Promise<Family[]> {

    const persistedEntity = await this.servicesRepo.count({ id: service_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }

    const familiesQB = this.servicesRepo.createQueryBuilder('service');
    familiesQB.leftJoinAndSelect('service.families', 'family', 'family.active = true');
    familiesQB.leftJoinAndSelect('family.translations',
      'family_translation',
      language_id ? 'family_translation.language_id = :language_id' : '',
      { language_id });
    familiesQB.where('service.id = :service_id', { service_id });

    const service = await familiesQB.getOne();

    for (const [index, family] of service.families.entries()) {
      if (family.translations.length === 0) {
        delete service.families[index];
      }
    }

    await this.mediaService.loadFileRelateds(service.families, 'file_id', 'file', token);

    return service ? service.families : [];
  }

  public async getServiceFamilyProduct(
    service_id: number, family_id: number,
    device_id: number, language_id: number,
    user: UserDto, relateds: string[], token: string): Promise<ProductMenuDto[]> {
    const persistedEntity = await this.servicesRepo.count({ id: service_id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }

    const persistedFamily = await this.familiesRepo.count({ id: family_id });
    if (persistedFamily === 0) {
      throw new NotFoundException(404, 'Family not found');
    }

    const serviceQB = this.servicesRepo.createQueryBuilder('service');
    serviceQB.leftJoinAndSelect('service.fentity', 'entity');
    serviceQB.leftJoin('service.wharehouse', 'wharehouse');
    serviceQB.leftJoinAndSelect('service.families', 'family',
      family_id ? 'family.id = :family_id' : '', { family_id });

    serviceQB.leftJoinAndSelect('family.products', 'product',
      'product.visible = true AND product.is_deleted = false');
    serviceQB.leftJoinAndSelect('product.translations',
      'product_translation',
      language_id ? 'product_translation.language_id = :language_id' : '',
      { language_id });
    serviceQB.leftJoinAndSelect('product.disponibility', 'disponibility');
    serviceQB.leftJoinAndSelect('product.stocks', 'stock', 'stock.wharehouse = service.wharehouse');

    if (user) {
      serviceQB.leftJoinAndSelect('product.prices', 'prices',
        'prices.time <= CURRENT_TIME() AND prices.profile_id = :profile_id', { profile_id: user.profile_id });
    }

    serviceQB.leftJoinAndSelect('product.composition', 'composition');
    serviceQB.leftJoinAndSelect('composition.compound', 'compound');
    serviceQB.leftJoinAndSelect('compound.stocks', 'cstock', 'cstock.wharehouse = service.wharehouse');

    serviceQB.leftJoinAndSelect('compound.composition', 'composition1');
    serviceQB.leftJoinAndSelect('composition1.compound', 'compound1');
    serviceQB.leftJoinAndSelect('compound1.stocks', 'c1stock', 'c1stock.wharehouse = service.wharehouse');

    serviceQB.leftJoinAndSelect('product.allergens', 'allergens');
    serviceQB.leftJoinAndSelect('allergens.translations',
      'allergen_translation',
      language_id ? 'allergen_translation.language_id = :language_id' : '',
      { language_id });

    serviceQB.leftJoinAndSelect('compound.allergens', 'compound_allergens');
    serviceQB.leftJoinAndSelect('compound_allergens.translations',
      'compound_allergen_translation',
      language_id ? 'compound_allergen_translation.language_id = :language_id' : '',
      { language_id });

    serviceQB.leftJoinAndSelect('compound1.allergens', 'compound1_allergens');
    serviceQB.leftJoinAndSelect('compound1_allergens.translations',
      'compound1_allergen_translation',
      language_id ? 'compound1_allergen_translation.language_id = :language_id' : '',
      { language_id });

    serviceQB.leftJoinAndSelect('product.complements', 'complement');
    serviceQB.leftJoinAndSelect('complement.translations',
      'complement_translation',
      language_id ? 'complement_translation.language_id = :language_id' : '',
      { language_id });

    serviceQB.where('service.id = :service_id', { service_id });

    const service = await serviceQB.getOne();

    const products: ProductMenuDto[] = [];
    if (service.families) {
      service.families.map(family => {
        family.products.map(product => {

          // CHECK DISPONIBILITY
          const isDisponible = this.checkDisponibility(product.disponibility, moment());

          let stockAvailable = false;
          let stockQuantity = 0;
          const stockInfo = this.checkStockProduct(product);
          stockAvailable = stockInfo[0];
          stockQuantity = stockInfo[1];

          product.available = product.available && isDisponible;

          const tpd = ProductMenuDto.fromProduct(product);

          tpd.service_id = service.service_id;
          tpd.account_id = service.fentity.account_id;

          if (product.prices) {
            if (product.prices.length > 0) {
              const maxPrice = product.prices.filter(p => p.profile_id === user.profile_id).reduce((prev, current) => {
                const prevTime = new Date('2018-12-12T' + prev.time);
                const currentTime = new Date('2018-12-12T' + current.time);
                return (prevTime > currentTime) ? prev : current;
              });
              tpd.price = maxPrice ? maxPrice.price : product.price;
              tpd.tax_id = maxPrice ? maxPrice.tax_id : product.tax_price_id;
            }
          }

          tpd.isStockAvailable = stockAvailable;
          tpd.stockQuantity = stockQuantity;
          products.push(tpd);
        });
      });
    }

    await this.mediaService.loadFileRelateds(products, 'file_id', 'file', token);

    if (relateds.find(related => related === 'taxes')) {
      await this.taxesService.loadTaxesRelateds(products, 'tax_id', 'tax', token);
    }

    return products;
  }

  public async getServiceMenu(service_id: number, meal: Meal, date: string, device_id: number,
    language_id: number, user: UserDto, relateds: string[], token: string): Promise<ProductMenuDto[]> {
    const persistedEntity = await this.servicesRepo.count({ id: service_id, type: ServiceType.CANTEEN });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'The service is not type canteen');
    }

    const serviceQB = this.servicesRepo.createQueryBuilder('service');
    serviceQB.leftJoinAndSelect('service.fentity', 'entity');
    serviceQB.leftJoinAndSelect('service.school', 'school');
    serviceQB.where('service.id = :service_id', { service_id });
    const service = await serviceQB.getOne();

    const menu = await this.menusService.findMeal(['dishs', 'dishstype', 'prices', 'disponibility'],
      language_id, service.id, date, meal, user ? user.profile_id : null, service.fentity.id);

    if (menu) {
      if (menu.validated) {
        const products: ProductMenuDto[] = [];
        for (const menuDish of menu.dishs) {
          const pmd: ProductMenuDto = new ProductMenuDto();
          pmd.meal = menu.meal;
          pmd.date = menu.date;
          pmd.id = menuDish.id;
          pmd.code = menuDish.type.code;
          pmd.price = menuDish.type.price;
          pmd.tax_id = menuDish.type.tax_id;
          pmd.prices = menuDish.type.prices;
          pmd.file_id = menuDish.dish.file_id;
          pmd.dish_type_translation = menuDish.type.translations;
          pmd.dish_type_id = menuDish.type.id;
          pmd.allergens = await this.userAllergens.returnsConfig(
            user ? user.id : 0,
            language_id,
            await this.dishsService.calculatedAllergen(menuDish.dish.id, language_id)
          );
          pmd.user_allergic = !!pmd.allergens.find(a => a.allergic === true);
          pmd.service_id = service.service_id;
          pmd.account_id = service.fentity.account_id;

          pmd.translations = menuDish.dish.translations;
          pmd.location = service.school.acronym + ' - ' + service.name;

          const stockResult = await this.checkDishStock(menuDish);
          pmd.isStockAvailable = stockResult.isStockAvailable;
          pmd.stockQuantity = stockResult.stockQuantity;

          if (meal === Meal.LUNCH) {
            pmd.available = this.checkDisponibility(menuDish.type.disponibility_lunch, moment(date)) && menuDish.available;
          } else {
            pmd.available = this.checkDisponibility(menuDish.type.disponibility_dinner, moment(date)) && menuDish.available;
          }

          // IF THE DAY IS BEFORE GET THE  FIRST PRICE ON TIME RANGE
          const isToday = moment(menu.date).isSame(new Date(), 'day');
          const priceResult = await this.getDishPrice(menuDish, meal, user, isToday);
          pmd.price = priceResult.price;
          pmd.tax_id = priceResult.tax_id;

          products.push(pmd);
        }
        await this.mediaService.loadFileRelateds(products, 'file_id', 'file', token);

        if (relateds.find(related => related === 'taxes')) {
          await this.taxesService.loadTaxesRelateds(products, 'tax_id', 'tax', token);
        }

        return products;
      } else {
        return [];
      }
    } else {
      return [];
    }
  }

  private checkDisponibility(disponibility: Disponibility, date: moment.Moment): boolean {
    const now = moment();
    const begin_date = moment(disponibility.begin_date);
    const begin_date_after = begin_date.isValid() ? now.isAfter(begin_date) : true;
    const end_date = moment(disponibility.end_date);
    const end_date_before = end_date.isValid() ? now.isBefore(end_date) : true;

    let maximum_hour_before = disponibility.maximum_hour ? false : true;
    let minimum_hour_after = disponibility.minimum_hour ? false : true;

    if (moment().isSame(date, 'day')) {
      const maximum_hour = moment(disponibility.maximum_hour, 'HH:mm:ss');
      const minimum_hour = moment(disponibility.minimum_hour, 'HH:mm:ss');
      maximum_hour_before = maximum_hour.isValid() ? now.isBefore(maximum_hour) : true;
      minimum_hour_after = minimum_hour.isValid() ? now.isAfter(minimum_hour) : true;
    } else {
      if (moment().isBefore(date, 'day')) {
        maximum_hour_before = true;
      } else {
        return false;
      }
    }

    let dayOfWeekDisponibility = false;

    switch (now.weekday()) {
      case 0:
        dayOfWeekDisponibility = disponibility.sunday;
        break;
      case 1:
        dayOfWeekDisponibility = disponibility.monday;
        break;
      case 2:
        dayOfWeekDisponibility = disponibility.tuesday;
        break;
      case 3:
        dayOfWeekDisponibility = disponibility.wednesday;
        break;
      case 4:
        dayOfWeekDisponibility = disponibility.thursday;
        break;
      case 5:
        dayOfWeekDisponibility = disponibility.friday;
        break;
      case 6:
        dayOfWeekDisponibility = disponibility.saturday;
        break;
    }

    return begin_date_after && end_date_before && minimum_hour_after && maximum_hour_before && dayOfWeekDisponibility;
  }

  private checkStockProduct(product: Product): [boolean, number] {
    let stockAvailable = false;
    let stockQuantity: number = 0;

    switch (product.track_stock) {
      case StockTrack.PRODUCT:
        stockQuantity = product.stocks.reduce((a, b) => a + parseFloat(b.quantity.toString(10)), 0);
        stockAvailable = stockQuantity > 0;
        break;
      case StockTrack.COMPOSITION:
        product.composition.map(composition => {
          if (composition.compound !== null) {
            if (composition.compound.track_stock === StockTrack.COMPOSITION) {
              composition.compound.composition.map(composition1 => {
                const compoud_quantity = this.checkStockProduct(composition1.compound)[1];
                const max_quantity = compoud_quantity / composition1.quantity;
                stockQuantity = (stockQuantity < max_quantity) ? Math.floor(max_quantity) : stockQuantity;
              });
            } else {
              const compoud_quantity = this.checkStockProduct(composition.compound)[1];
              const max_quantity = compoud_quantity / composition.quantity;
              stockQuantity = (stockQuantity < max_quantity) ? Math.floor(max_quantity) : stockQuantity;
            }
          }
        });
        stockAvailable = stockQuantity > 0;
        break;
      case StockTrack.PRODUCT_COMPOSITION:
        stockAvailable = false;
        break;
      case StockTrack.NONE:
        stockAvailable = true;
        stockQuantity = 999;
        break;
      default:
        stockAvailable = false;
        stockQuantity = 0;
    }

    return [stockAvailable, stockQuantity];
  }

  private calculateAllergensNutrientsValues(product: Product): Product {
    if (product.composition.length > 0) {
      if (product.composition[0].compound === null) {
        product.composition = [];
      }
    }

    if (product.nutrients.length > 0) {
      if (product.nutrients[0].nutrient === null) {
        product.nutrients = [];
      }
    }

    product.composition.map(composition => {
      if (composition.compound) {
        composition.compound.allergens.map(allergen => {
          product.allergens.indexOf(allergen) === -1 ? product.allergens.push(allergen) : null;
        });

        composition.compound.nutrients.map(cnutrient => {
          if (cnutrient.nutrient) {
            const temp_quantity = cnutrient.quantity * composition.quantity;
            const tnutri = product.nutrients.find(nutri => nutri.nutrient.id === cnutrient.nutrient.id);
            if (tnutri) {
              tnutri.quantity = parseInt(tnutri.quantity.toString(), 10) + temp_quantity;
            } else {
              const tnp = new NutrientProduct();
              tnp.quantity = temp_quantity;
              tnp.nutrient = cnutrient.nutrient;
              product.nutrients.push(tnp);
            }
          }
        });

        composition.compound.composition.map(composition1 => {
          if (composition1.compound) {
            composition1.compound.allergens.map(allergen => {
              product.allergens.indexOf(allergen) === -1 ? product.allergens.push(allergen) : null;
            });

            composition1.compound.nutrients.map(cnutrient => {
              if (cnutrient.nutrient) {
                const temp_quantity = cnutrient.quantity * composition1.quantity;
                const tnutri = product.nutrients.find(nutri => nutri.nutrient.id === cnutrient.nutrient.id);
                if (tnutri) {
                  tnutri.quantity += temp_quantity;
                } else {
                  const tnp = new NutrientProduct();
                  tnp.quantity = temp_quantity;
                  tnp.nutrient = cnutrient.nutrient;
                  product.nutrients.push(tnp);
                }
              }
            });
          }
        });
      }
    });

    return product;
  }

  public async getProductInfo(service_id: any, product_id: any, device_id: any,
    language_id: any, user: UserDto, relateds: string[], token: string): Promise<ProductDetailDto> {

    const productCount = await this.productRepo.count({ id: product_id });
    if (productCount === 0) {
      throw new NotFoundException(404, 'Product not found');
    }

    const serviceQB = await this.servicesRepo.createQueryBuilder('service');
    serviceQB.leftJoinAndSelect('service.fentity', 'entity');
    serviceQB.where('service.id = :service_id', { service_id });
    const persistedService = await serviceQB.getOne();
    if (!persistedService) {
      throw new NotFoundException(404, 'Service not found');
    }

    const productQB = this.productRepo.createQueryBuilder('product');
    productQB.leftJoinAndSelect('product.translations',
      'product_translation',
      language_id ? 'product_translation.language_id = :language_id' : '',
      { language_id });
    productQB.leftJoinAndSelect('product.composition', 'composition');
    productQB.leftJoinAndSelect('composition.compound', 'compound');
    productQB.leftJoinAndSelect('compound.composition', 'composition1');
    productQB.leftJoinAndSelect('composition1.compound', 'compound1');

    productQB.leftJoinAndSelect('compound.nutrients', 'compound_nutrients');
    productQB.leftJoinAndSelect('compound_nutrients.nutrient', 'compound_nutrient');
    productQB.leftJoinAndSelect('compound_nutrient.translations',
      'compound_nutrient_translation',
      language_id ? 'compound_nutrient_translation.language_id = :language_id' : '',
      { language_id });

    productQB.leftJoinAndSelect('compound1.nutrients', 'compound1_nutrients');
    productQB.leftJoinAndSelect('compound1_nutrients.nutrient', 'compound1_nutrient');
    productQB.leftJoinAndSelect('compound1_nutrient.translations',
      'compound1_nutrient_translation',
      language_id ? 'compound_nutrient_translation.language_id = :language_id' : '',
      { language_id });
    productQB.leftJoinAndSelect('product.allergens', 'allergens');
    productQB.leftJoinAndSelect('allergens.translations',
      'allergen_translation',
      language_id ? 'allergen_translation.language_id = :language_id' : '',
      { language_id });

    productQB.leftJoinAndSelect('compound.allergens', 'compound_allergens');
    productQB.leftJoinAndSelect('compound_allergens.translations',
      'compound_allergen_translation',
      language_id ? 'compound_allergen_translation.language_id = :language_id' : '',
      { language_id });

    productQB.leftJoinAndSelect('compound1.allergens', 'compound1_allergens');
    productQB.leftJoinAndSelect('compound1_allergens.translations',
      'compound1_allergen_translation',
      language_id ? 'compound1_allergen_translation.language_id = :language_id' : '',
      { language_id });

    productQB.leftJoinAndSelect('product.nutrients', 'nutrients');
    productQB.leftJoinAndSelect('nutrients.nutrient', 'nutrient');
    productQB.leftJoinAndSelect('nutrient.translations',
      'nutrient_translation',
      language_id ? 'nutrient_translation.language_id = :language_id' : '',
      { language_id });

    if (user) {
      productQB.leftJoinAndSelect('product.prices', 'prices',
        'prices.time <= CURRENT_TIME() AND prices.profile_id = :profile_id', { profile_id: user.profile_id });
    }

    productQB.where('product.visible = true AND product.is_deleted = false AND product.id = :product_id', { product_id });

    let entity = await productQB.getOne();
    entity = this.calculateAllergensNutrientsValues(entity);

    const response = new ProductDetailDto();
    response.translations = entity.translations;
    response.allergens = await this.userAllergens.returnsConfig(
      user ? user.id : null,
      language_id,
      entity.allergens);
    response.user_allergic = !!response.allergens.find(a => a.allergic === true);
    response.nutrients = entity.nutrients;
    response.prices = entity.prices;
    response.file_id = entity.file_id;
    response.composition = entity.composition.length > 0 ? entity.composition.map(
      comp => comp.compound.translations ? comp.compound.translations[0].name : '') : [];

    await this.mediaService.loadFileRelated(response, 'file_id', 'file', token);

    response.price = entity.price;
    response.tax_id = entity.tax_price_id;

    response.service_id = persistedService.service_id;
    response.account_id = persistedService.fentity.account_id;

    if (entity.prices) {
      if (entity.prices.length > 0) {
        const maxPrice = entity.prices.filter(p => p.profile_id === user.profile_id).reduce((prev, current) => {
          const prevTime = new Date('2018-12-12T' + prev.time);
          const currentTime = new Date('2018-12-12T' + current.time);
          return (prevTime > currentTime) ? prev : current;
        });
        response.price = maxPrice ? maxPrice.price : entity.price;
        response.tax_id = maxPrice ? maxPrice.tax_id : entity.tax_price_id;
      }
    }

    if (relateds.find(related => related === 'taxes')) {
      await this.taxesService.loadTaxesRelated(response, 'tax_id', 'tax', token);
    }

    return response;
  }

  public async getDishInfo(service_id: any, menuDish_id: number,
    device_id: any, language_id: any,
    user: UserDto, relateds: string[],
    token: string): Promise<ProductMenuDto> {

    const serviceQB = await this.servicesRepo.createQueryBuilder('service');
    serviceQB.leftJoinAndSelect('service.fentity', 'entity');
    serviceQB.leftJoinAndSelect('service.school', 'school');
    serviceQB.where('service.id = :service_id', { service_id });
    const persistedService = await serviceQB.getOne();
    if (!persistedService) {
      throw new NotFoundException(404, 'Service not found');
    }

    const menuDish = await this.menusService.getMenuDish(menuDish_id, ['dishs', 'type', 'disponibilities', 'prices', 'services', 'menus'], language_id);
    const dish_result = await this.dishsService.calculatedAllergenAndNutrientsComposition(menuDish.dishId, language_id);

    const pmd: ProductMenuDto = new ProductMenuDto();
    pmd.meal = menuDish.menu.meal;
    pmd.date = menuDish.menu.date;
    pmd.id = menuDish.id;
    pmd.code = menuDish.type.code;
    pmd.price = menuDish.type.price;
    pmd.tax_id = menuDish.type.tax_id;
    pmd.prices = menuDish.type.prices;
    pmd.file_id = menuDish.dish.file_id;
    pmd.dish_type_translation = menuDish.type.translations;
    pmd.dish_type_id = menuDish.type.id;
    pmd.allergens = await this.userAllergens.returnsConfig(
      user ? user.id : 0,
      language_id,
      await this.dishsService.calculatedAllergen(menuDish.dish.id, language_id)
    );

    pmd.nutrients = dish_result[0];
    pmd.allergens = await this.userAllergens.returnsConfig(
      user.id,
      language_id,
      dish_result[1]
    );

    pmd.user_allergic = !!pmd.allergens.find(a => a.allergic === true);
    pmd.service_id = menuDish.menu.service.service_id;
    pmd.account_id = menuDish.menu.service.fentity.account_id;
    pmd.translations = menuDish.dish.translations;
    pmd.location = menuDish.menu.service.school.acronym + ' - ' + menuDish.menu.service.name;
    const stockResult = await this.checkDishStock(menuDish);
    pmd.isStockAvailable = stockResult.isStockAvailable;
    pmd.stockQuantity = stockResult.stockQuantity

    if (menuDish.menu.meal === Meal.LUNCH) {
      pmd.available = this.checkDisponibility(menuDish.type.disponibility_lunch, moment(menuDish.menu.date)) && menuDish.available;
    } else {
      pmd.available = this.checkDisponibility(menuDish.type.disponibility_dinner, moment(menuDish.menu.date)) && menuDish.available;
    }

    const isToday = moment(menuDish.menu.date).isSame(new Date(), 'day');
    const priceResult = await this.getDishPrice(menuDish, menuDish.menu.meal, user, isToday);
    pmd.price = priceResult.price;
    pmd.tax_id = priceResult.tax_id;

    await this.mediaService.loadFileRelated(pmd, 'file_id', 'file', token);

    if (relateds.find(related => related === 'taxes')) {
      await this.taxesService.loadTaxesRelated(pmd, 'tax_id', 'tax', token);
    }

    return pmd;
  }

  /*    const serviceQB = await this.servicesRepo.createQueryBuilder('service');
      serviceQB.leftJoinAndSelect('service.fentity', 'entity');
      serviceQB.leftJoinAndSelect('service.school', 'school');
      serviceQB.where('service.id = :service_id', { service_id });
      const persistedService = await serviceQB.getOne();
      if (!persistedService) {
        throw new NotFoundException(404, 'Service not found');
      }
  
      const menuDish = await this.menusService.getMenuDish(menuDish_id, []);
  
      const dish_result = await this.dishsService.calculatedAllergenAndNutrientsComposition(menuDish.dishId, language_id);
  
      const dishQB = this.dishRepo.createQueryBuilder('dish');
  
      dishQB.leftJoinAndSelect('dish.translations',
        'dish_translation',
        language_id ? 'dish_translation.language_id = :language_id' : '',
        { language_id });
      dishQB.where('dish.id = :id', { id: menuDish.dishId });
      const entity = await dishQB.getOne();
  
      const dishTypeQB = this.dishTypeRepo.createQueryBuilder('dishType');
      dishTypeQB.leftJoinAndSelect('dishType.translations',
        'dishType_translation',
        language_id ? 'dishType_translation.language_id = :language_id' : '',
        { language_id });
      dishTypeQB.leftJoinAndSelect('dishType.disponibility_lunch', 'disponibility_lunch');
      dishTypeQB.leftJoinAndSelect('dishType.disponibility_dinner', 'disponibility_dinner');
      if (user) {
        dishTypeQB.leftJoinAndSelect('dishType.prices', 'prices',
          'prices.time <= CURRENT_TIME() AND prices.profile_id = :profile_id', { profile_id: user.profile_id });
      }
      dishTypeQB.where('dishType.id = :id', { id: menuDish.typeId });
      const dishTypeEntity = await dishTypeQB.getOne();
  
      const response = new ProductDetailDto();
      response.translations = entity.translations;
      response.file_id = entity.file_id;
  
      await this.mediaService.loadFileRelated(response, 'file_id', 'file', token);
  
      response.nutrients = dish_result[0];
      response.allergens = await this.userAllergens.returnsConfig(
          user.id,
          language_id,
          dish_result[1]
      );
      response.user_allergic =  !!response.allergens.find(a => a.allergic === true);
      response.composition = dish_result[2].length > 0 ? dish_result[2].map(translation => translation.name) : [];
      response.dish_type_translation = dishTypeEntity.translations;
      response.prices = dishTypeEntity.prices;
      response.price = dishTypeEntity.price;
      response.tax_id = dishTypeEntity.tax_id;
      response.location = persistedService.school.acronym + ' - ' + persistedService.name;
      response.service_id = persistedService.service_id;
      response.account_id = persistedService.fentity.account_id;
  
      if (dishTypeEntity.prices) {
        if (dishTypeEntity.prices.length > 0) {
          const maxPrice = dishTypeEntity.prices.filter(p => p.profile_id === user.profile_id).reduce((prev, current) => {
            const prevTime = new Date('2018-12-12T' + prev.time);
            const currentTime = new Date('2018-12-12T' + current.time);
            return (prevTime > currentTime) ? prev : current;
          });
          response.price = maxPrice ? maxPrice.price : dishTypeEntity.price;
          response.tax_id = maxPrice ? maxPrice.tax_id : dishTypeEntity.tax_id;
        }
      }
  
      if (relateds.find(related => related === 'taxes')) {
        await this.taxesService.loadTaxesRelated(response, 'tax_id', 'tax', token);
      }
  
      return response;*/

  public async checkProductDisponibility(product_id: number): Promise<boolean> {

    const productCount = await this.productRepo.count({ id: product_id });
    if (productCount === 0) {
      throw new NotFoundException(404, 'Product not found');
    }

    const productQB = this.productRepo.createQueryBuilder('product');
    productQB.leftJoinAndSelect('product.disponibility', 'disponibility');
    productQB.where('product.id = :product_id', { product_id });
    const product = await productQB.getOne();
    return this.checkDisponibility(product.disponibility, moment());
  }

  public async checkDishDisponibility(menuDish_id: number): Promise<any> {

    const menuDish = await this.menusService.getMenuDish(menuDish_id, ['types', 'menus', 'disponibilities']);
    let available = false;
    let isStockAvailable = false;
    let stockQuantity = 0;

    if (menuDish.menu.meal === Meal.LUNCH) {
      available = this.checkDisponibility(menuDish.type.disponibility_lunch, moment(menuDish.menu.date)) && menuDish.available;
    } else {
      available = this.checkDisponibility(menuDish.type.disponibility_dinner, moment(menuDish.menu.date)) && menuDish.available;
    }

    const stockResult = await this.checkDishStock(menuDish);
    isStockAvailable = stockResult.isStockAvailable;
    stockQuantity = stockResult.stockQuantity

    return { available, isStockAvailable, stockQuantity };
  }

  public async checkDishStock(menuDish: MenuDish): Promise<any> {

    if (menuDish.doses_available === 0) {
      return { isStockAvailable: true, stockQuantity: 999 };
    }

    const reservationsResult = await this.reservationsService.getMenuDishReservationCount(menuDish.id);
    return {
      isStockAvailable: menuDish.doses_available > reservationsResult.total,
      stockQuantity: menuDish.doses_available - reservationsResult.total
    };
  }

  public async getDishPrice(menuDish: MenuDish, meal: Meal, user: UserDto, isToday: boolean) {
    if (menuDish.type.prices) {
      if (menuDish.type.prices.length > 0) {
        const prices = menuDish.type.prices.filter(p => p.profile_id === user.profile_id && p.meal === meal);
        if (prices.length > 0) {

          if (isToday) {
            const maxPrice = prices.reduce((prev, current) => {
              const prevTime = new Date('2018-12-12T' + prev.time);
              const currentTime = new Date('2018-12-12T' + current.time);
              return (prevTime > currentTime) ? prev : current;
            });
            return {
              price: maxPrice ? maxPrice.price : menuDish.type.price,
              tax_id: maxPrice ? maxPrice.tax_id : menuDish.type.tax_id
            };
          } else {
            const firstPrice = prices.reduce((prev, current) => {
              const prevTime = new Date('2018-12-12T' + prev.time);
              const currentTime = new Date('2018-12-12T' + current.time);
              return (prevTime < currentTime) ? prev : current;
            });
            return {
              price: firstPrice ? firstPrice.price : menuDish.type.price,
              tax_id: firstPrice ? firstPrice.tax_id : menuDish.type.tax_id
            };
          }
        } else {
          return {
            price: menuDish.type.price,
            tax_id: menuDish.type.tax_id
          };
        }
      }
      return {
        price: menuDish.type.price,
        tax_id: menuDish.type.tax_id
      };
    } else {
      return {
        price: menuDish.type.price,
        tax_id: menuDish.type.tax_id
      };
    }
  }
}
