import {
  IsArray,
  IsDateString,
  IsNotEmpty, IsNumber
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CopyDayDto {
  @ApiModelProperty({ })
  @IsNotEmpty({ each: true, message: 'date nao pode ser vazio'})
  @IsDateString({ each: true, message: 'the formate of the date should be 2018-09-11T14:34:08.700Z'})
  from_day: string;

  @ApiModelProperty({})
  @IsNotEmpty({ each: true, message: 'service_ids cant be empty'})
  @IsNumber({}, {  each: true, message: 'service_ids is type number'})
  from_service_id: number;

  @ApiModelProperty({ isArray: true })
  @IsArray({ message: 'services_ids must be a array'})
  @IsNotEmpty({ each: true, message: 'service_ids cant be empty'})
  @IsNumber({}, {  each: true, message: 'service_ids is type number'})
  service_ids: number[];

  @ApiModelProperty({ isArray: true })
  @IsArray({ message: 'dates must be a array'})
  @IsNotEmpty({ each: true, message: 'date nao pode ser vazio'})
  @IsDateString({ each: true, message: 'the formate of the date should be 2018-09-11T14:34:08.700Z'})
  dates: string[];
}
