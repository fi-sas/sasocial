import { MenuDishDto } from './menu.dto';
import {
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty, IsNumber
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Meal } from '../entities/menu.entity';

export class CopyMenuDto {

  @ApiModelProperty({ isArray: true })
  @IsArray({ message: 'services_ids must be a array'})
  @IsNotEmpty({ each: true, message: 'service_ids cant be empty'})
  @IsNumber({}, {  each: true, message: 'service_ids is type number'})
  service_ids: number[];

  @ApiModelProperty({ type: Meal, enum: Meal, isArray: true  })
  @IsArray({ message: 'meals must be a array'})
  @IsNotEmpty({ each: true, message: 'meal nao pode ser vazio'})
  @IsEnum(Meal, { each: true, message: 'meal e um enum'})
  meals: Meal[];

  @ApiModelProperty({ isArray: true })
  @IsArray({ message: 'dates must be a array'})
  @IsNotEmpty({ each: true, message: 'date nao pode ser vazio'})
  @IsDateString({ each: true, message: 'the formate of the date should be 2018-09-11T14:34:08.700Z'})
  dates: string[];
}
