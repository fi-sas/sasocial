import {
  IsArray,
  IsNotEmpty, IsNumber, IsPositive, IsDateString
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CopyWeekDto {

  @ApiModelProperty({ })
  @IsNotEmpty({ each: true, message: 'from_day_of_week cant be empty'})
  @IsDateString({ each: true, message: 'format of the from_day_of_week should be 2018-09-11T14:34:08.700Z'})
  from_day_of_week: string;

  @ApiModelProperty({})
  @IsNotEmpty({ each: true, message: 'service_ids cant be empty'})
  @IsNumber({}, {  each: true, message: 'service_ids is type number'})
  from_service_id: number;

  @ApiModelProperty({ isArray: true })
  @IsArray({ message: 'services_ids must be a array'})
  @IsNotEmpty({ each: true, message: 'service_ids cant be empty'})
  @IsNumber({}, {  each: true, message: 'service_ids is type number'})
  service_ids: number[];

  @ApiModelProperty({ isArray: true })
  @IsNotEmpty({ each: true, message: 'to_day_of_week cant be empty'})
  @IsDateString({ each: true, message: 'format of the to_day_of_week should be 2018-09-11T14:34:08.700Z'})
  to_day_of_week: string;
}
