import {
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty, IsNumber,
  ValidateNested, IsOptional, IsBoolean,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Meal } from '../entities/menu.entity';

export class MenuDishDto {

  static fromObj(data: {
    dish_id: number,
    dishType_id: number,
    available: boolean,
    doses_available: number
  }): MenuDishDto {
    const newMenuDish: MenuDishDto = new MenuDishDto();
    newMenuDish.dish_id = data.dish_id;
    newMenuDish.dishType_id = data.dishType_id;
    newMenuDish.available = data.available;
    newMenuDish.doses_available = data.doses_available;
    return newMenuDish;
  }

  // @ApiModelPropertyOptional({ description: 'the id of menu dish in case of update' })
  // @IsOptional()
  // @IsNumber({}, { message: 'menu_dish_id e do tipo numero'})
  // menu_dish_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'dish_id nao pode ser vazio'})
  @IsNumber({}, { message: 'dish_id e do tipo numero'})
  dish_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'dishType_id nao pode ser vazio'})
  @IsNumber({}, { message: 'dishType_id e do tipo numero'})
  dishType_id: number;

  @ApiModelProperty({ default: true})
  @IsBoolean({ message: 'available is type boolean'})
  @IsNotEmpty({ message: 'doses_available cant be empty'})
  available: boolean;

  @ApiModelProperty({ default: 0, description: 'Number of doses available to order; If 0 this validation is skiped'})
  @IsNotEmpty({ message: 'doses_available nao pode ser vazio'})
  @IsNumber({}, { message: 'doses_available e do tipo numero'})
  doses_available: number;
}

export class MenuDto {
  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'service_id nao pode ser vazio'})
  @IsNumber({}, { message: 'service_id e do tipo numero'})
  service_id: number;

  @ApiModelProperty({ type: Meal, enum: Meal })
  @IsNotEmpty({ message: 'meal nao pode ser vazio'})
  @IsEnum(Meal, { message: 'meal e um enum'})
  meal: Meal;

  @ApiModelProperty( {type: MenuDishDto, isArray: true })
  @IsArray({ message: 'os dishs e um array'})
  @ValidateNested()
  @Type(() => MenuDishDto)
  dishs: MenuDishDto[];

  @ApiModelProperty()
  @IsNotEmpty({ message: 'meal nao pode ser vazio'})
  @IsDateString({ message: 'the formate of the date should be 2018-09-11T14:34:08.700Z'})
  date: string;
}
