import {Column, Entity, ManyToOne, OneToMany, RelationId} from 'typeorm';
import { Dish } from '../../dishs/entities/dish.entity';
import { DishType } from '../../dishs/entities/dish-type.entity';
import { Menu } from './menu.entity';
import { BaseEntity } from '../../common/base.entity';
import {Reservation} from '../../reservations/entities/reservation.entity';

@Entity()
export class MenuDish extends BaseEntity {

  @ManyToOne(type1 => Menu, menu => menu.dishs, { cascade: true, onDelete: 'CASCADE' })
  menu: Menu;

  @Column()
  menuId: number;

  @ManyToOne(type => Dish, dish => dish.id, { cascade: true, onDelete: 'CASCADE' })
  dish: Dish;

  @Column()
  dishId: number;

  @ManyToOne(type => DishType, type => type.id, { cascade: true, onDelete: 'CASCADE' })
  type: DishType;

  @Column()
  typeId: number;

  @Column()
  doses_available: number;

  @Column({ nullable: true })
  available_until: Date;

  @Column({ nullable: true })
  nullable_until: Date;

  @OneToMany(type1 => Reservation, r => r.menu_dish)
  reservations: Reservation[];

  @Column()
  available: boolean;
}
