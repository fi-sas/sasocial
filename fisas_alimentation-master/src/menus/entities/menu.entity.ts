import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { MenuDish } from './menu-dish.entity';
import { Service } from '../../service/entities/service.entity';

export enum Meal {
  LUNCH = 'lunch',
  DINNER = 'dinner',
  BREAKFAST = 'breakfast'
}

@Entity()
export class Menu extends BaseEntity {
  @ManyToOne(type => Service, service => service.id, {  })
  service: Service;

  @Column({ name: 'serviceId'})
  service_id: number;

  @Column({ nullable: false, type: 'enum', enum: ['lunch', 'dinner', 'breakfast']})
  meal: Meal;

  @OneToMany(type => MenuDish, dish => dish.menu)
  @JoinColumn()
  dishs: MenuDish[];

  @Column({ nullable: false, type: 'date'})
  date: Date;

  @Column({ nullable: false, default: false })
  validated: boolean;
}
