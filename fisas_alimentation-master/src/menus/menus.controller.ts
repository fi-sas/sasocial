import { CopyWeekDto } from './dtos/ copy-week.dto';
import { CopyDayDto } from './dtos/ copy-day.dto';
import { CopyMenuDto } from './dtos/ copy-menu.dto';
import { BulkMenuDto } from './dtos/bulk-menu.dto';
import { Body, Controller, Delete, Get, Headers, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitHeader, ApiImplicitParam, ApiImplicitQuery, ApiProduces, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { MenusService } from './menus.service';
import { Menu } from './entities/menu.entity';
import { MenuDto } from './dtos/menu.dto';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { MenuDish } from './entities/menu-dish.entity';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Meals')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/meals')
export class MenusController {

  constructor(private menusService: MenusService) {
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "dishs" , "dishstype" and "service" ', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'service_id',
    description: 'Finds only the menus for the service of the id', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'date',
    description: 'Finds menus only for the date ', type: Date,
  })
  @ApiImplicitQuery({
    required: false, name: 'date_begin',
    description: 'Finds menus between the interval of date_begin and date_end', type: Date,
  })
  @ApiImplicitQuery({
    required: false, name: 'date_end',
    description: 'Finds menus between the interval of date_begin and date_ende', type: Date,
  })
  @Scopes('menus')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id) {
    let service_id = null;
    let date = null;
    let date_begin = null;
    let date_end = null;

    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-date';
    if (query.hasOwnProperty('service_id')) service_id = query.service_id;
    if (query.hasOwnProperty('date')) date = query.date;
    if (query.hasOwnProperty('date_begin')) date_begin = query.date_begin;
    if (query.hasOwnProperty('date_end')) date_end = query.date_end;

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation => relation !== 'dishs' && relation !== 'dishstype' && relation !== 'service');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.menusService.findAll(query.offset, query.limit, query.sort, relateds, language_id, service_id,
      date, date_begin, date_end, entity_id);
    return new Response<Menu[][]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
      },
    );
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "dishs" , "dishstype" and "service" ', type: String,
  })
  @Scopes('menus')
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                        @Query() query, @Headers('x-language-id') language_id) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation => relation !== 'dishs' && relation !== 'dishstype' && relation !== 'service');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.menusService.findByID(id, relateds, language_id, entity_id);
    return new Response<Menu>(ResponseStatus.OK, entity);
  }

  @Scopes('menus')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: MenuDto) {
    const createdEntity = await this.menusService.create(entity, entity_id);
    return new Response<Menu>(ResponseStatus.OK, createdEntity);
  }

  @Scopes('menus-validation')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Post(':id/accept')
  public async accept(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    return new Response<Menu>(ResponseStatus.OK, await this.menusService.validation(id, true, entity_id));
  }

  @Scopes('menus-validation')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Post(':id/reject')
  public async reject(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    return new Response<Menu>(ResponseStatus.OK, await this.menusService.validation(id, false, entity_id));
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Scopes('menus')
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id, @Body() entity: MenuDto) {
    const updatedEntity = await this.menusService.update(id, entity, entity_id);
    return new Response<Menu>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Scopes('menus')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.menusService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }

  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "menus", "types", "disponibilities" ', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Scopes('menus')
  @Get('dish/:id')
  public async findMenuDishById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                                @Query() query, @Headers('x-language-id') language_id) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'menus' &&
        relation !== 'types' &&
        relation !== 'disponibilities'
      );
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.menusService.getMenuDish(id, relateds);
    return new Response<MenuDish>(ResponseStatus.OK, entity);
  }

  @Scopes('menus')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiOperation({
    title: 'Create Menus in Bulk',
    description: 'Create menus for multiples mealsn and services days in one call'
  })
  @Post('bulk')
  public async createMenusBulk(
    @Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: BulkMenuDto
  ) {

    const menus: MenuDto[] = [];
    if (entity) {
      for (const d of entity.dates) {
        for (const m of entity.meals) {
          for (const s of entity.service_ids) {
              const existent = await this.menusService.checkIfMenuExist(new Date(d) , m , s);
              menus.push({
                id: existent ? existent.id : null,
                date: d,
                meal: m,
                dishs: entity.dishs,
                service_id: s
              });
          }
        }
      }

      for(const m of menus) {
        if (m.id) {
          this.menusService.update(m.id, m, entity_id, false);
        } else {
          this.menusService.create(m, entity_id);
        }
      }
    }

    return new Response<MenuDto[]>(ResponseStatus.OK, menus);
  }

  @Scopes('menus')
  @ApiOperation({
    title: 'Copy menu',
    description: 'Copy the menu for a meal of your choice'
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the menu', name: 'id', type: Number })
  @Post(':id/copy')
  async copyMenu(
    @Param('id', new ParseIntPipe()) id, @Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: CopyMenuDto
  ) {
    return new Response<Menu[]>(ResponseStatus.OK, await this.menusService.copyMenu(id, entity, entity_id));
  }

  @Scopes('menus')
  @ApiOperation({
    title: 'Copy day',
    description: 'Copy lunch and dinner from this day to another day the choice'
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post('copy/day')
  async copyDay(
    @Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: CopyDayDto
  ) {
    return new Response<Menu[]>(ResponseStatus.OK, await this.menusService.copyDay(entity, entity_id));
  }

  @Scopes('menus')
  @ApiOperation({
    title: 'Copy week',
    description: 'Copy lunch and dinner from all days of the week to other week of choice'
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post('copy/week')
  async copyWeek(
    @Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: CopyWeekDto
  ) {
    return new Response<Menu[]>(ResponseStatus.OK, await this.menusService.copyWeek(entity, entity_id));
  }

}
