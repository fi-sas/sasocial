import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Menu } from './entities/menu.entity';
import { MenusController } from './menus.controller';
import { MenusService } from './menus.service';
import { DishsModule } from '../dishs/dishs.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { ServicesModule } from '../service/services.module';
import { CommomModule } from '../common/commom.module';
import { MenuDish } from './entities/menu-dish.entity';

// TODO CRON TO GENERATE IMAGES TO COMMUNICATION
@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    DishsModule,
    ServicesModule,
    TypeOrmModule.forFeature([
      Menu,
      MenuDish
    ]),
  ],
  controllers: [
    MenusController
  ],
  providers: [
    MenusService
  ],
  exports: [
    MenusService
  ]
})
export class MenusModule {}