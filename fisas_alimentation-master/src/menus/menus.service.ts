import { CopyWeekDto } from './dtos/ copy-week.dto';
import { CopyDayDto } from './dtos/ copy-day.dto';
import { ValidationPipe } from './../common/pipes/validation.pipe';
import { CopyMenuDto } from './dtos/ copy-menu.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Meal, Menu } from './entities/menu.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { MenuDto, MenuDishDto } from './dtos/menu.dto';
import { MenuDish } from './entities/menu-dish.entity';
import { Dish } from '../dishs/entities/dish.entity';
import { DishType } from '../dishs/entities/dish-type.entity';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ResponseError } from '../common/response.model';
import { DishsService } from '../dishs/dishs.service';
import { DishTypesService } from '../dishs/dish-types.service';
import { Service } from '../service/entities/service.entity';
import { ServicesService } from '../service/services.service';
import { ServiceType } from '../common/enums/service-type.enum';
import moment = require('moment');

@Injectable()
export class MenusService {
  constructor(@InjectRepository(Menu) private readonly menuRepo: Repository<Menu>,
    @InjectRepository(MenuDish) public readonly menuDishRepo: Repository<MenuDish>,
    private dishsService: DishsService,
    private serviceService: ServicesService,
    private dishTypesService: DishTypesService,
  ) {
  }

  public async checkIfMenuExist(date: Date, meal: Meal, service_id: number): Promise<Menu | undefined> {
    return await this.menuRepo.findOne({ service_id, meal, date: moment(date).format('YYYY-MM-DD') });
  }

  public async create(entityDto: MenuDto, entity_id: number): Promise<Menu> {

    const resultService = await this.serviceService.existID(entityDto.service_id, entity_id, ServiceType.CANTEEN);
    if (resultService === 0) {
      throw new NotFoundException(404, 'the service id sent was not found');
    }
    const tservice = new Service();
    tservice.id = entityDto.service_id;

    const menuCount = await this.menuRepo.count({ service_id: entityDto.service_id, meal: entityDto.meal, date: new Date(entityDto.date) });
    if (menuCount > 0) {
      throw new ValidationException([new ResponseError(400, 'Already exist a menu for this meal, service and date')]);
    }

    const dishs_id = entityDto.dishs
      .filter((value, index, array) =>
        index === array.findIndex((t) => (
          t.dish_id === value.dish_id
        )))
      .map(dish => dish.dish_id);
    const dishTypes_id = entityDto.dishs
      .filter((value, index, array) =>
        index === array.findIndex((t) => (
          t.dishType_id === value.dishType_id
        )))
      .filter((value, index, array) => index === array.indexOf(value)).map(dish => dish.dishType_id);

    const dishsCount = await this.dishsService.existIDs(dishs_id, entity_id);
    if (dishsCount !== [...new Set(dishs_id)].length) {
      throw new NotFoundException(404, 'the dish id sent doesnt exist');
    }

    const dishTypesCount = await this.dishTypesService.existIDs(dishTypes_id, entity_id);
    if (dishTypesCount !== [...new Set(dishTypes_id)].length) {
      throw new NotFoundException(404, 'the dish type id sent doesnt exist');
    }

    return new Promise<Menu>(async (resolve, reject) => {
      this.menuRepo.manager.transaction(async entityManger => {

        const insertResult = await this.menuRepo.createQueryBuilder()
          .insert()
          .into(Menu)
          .values({
            service: tservice,
            meal: entityDto.meal,
            date: new Date(entityDto.date),
          })
          .execute();

        const id = insertResult.identifiers[0].id;
        for (const dish of entityDto.dishs) {
          const td = new Dish();
          td.id = dish.dish_id;
          const tdt = new DishType();
          tdt.id = dish.dishType_id;
          const tm = new Menu();
          tm.id = id;
          const tmd = new MenuDish();
          tmd.menu = tm;
          tmd.type = tdt;
          tmd.dish = td;
          tmd.doses_available = dish.doses_available;

          const anu = await this.dishTypesService.availableNullableUntil(
            dish.dishType_id,
            entityDto.meal,
            entityDto.date); // CALCULATE THE MAXIMUM HOUR TO BUY OR CANCEL
          tmd.available_until = anu.available_until;
          tmd.nullable_until = anu.nullable_until;

          tmd.available = dish.available;
          await this.menuRepo
            .createQueryBuilder()
            .insert()
            .into(MenuDish)
            .values(tmd)
            .execute();
        }

        resolve(await this.findByID(id, ['dishs', 'dishstype', 'service'], null, entity_id));
      }).catch(error => {
        reject(error);
      });
    });
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[],
    language_id: number, service_id: number,
    date: string, date_begin: string, date_end: string, entity_id: number): Promise<[Menu[][], number]> {
    const menusQB = this.menuRepo.createQueryBuilder('menu');

    if (service_id) {
      const resultService = await this.serviceService.existID(service_id, entity_id, ServiceType.CANTEEN);
      if (resultService === 0) {
        throw new NotFoundException(404, 'the service id sent was not found');
      }
    }

    if (limit > 0) {
      menusQB.take(limit);
      menusQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      menusQB.orderBy('menu.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'dishs')) {
      menusQB.leftJoinAndSelect('menu.dishs', 'dishs');
      menusQB.leftJoinAndSelect('dishs.dish', 'dish');
      menusQB.leftJoinAndSelect('dish.translations',
        'dish_translation',
        language_id ? 'dish_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'dishstype')) {
      menusQB.leftJoinAndSelect('dishs.type', 'type');
      menusQB.leftJoinAndSelect('type.translations',
        'type_translation',
        language_id ? 'type_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'service')) {
      menusQB.leftJoinAndSelect('menu.service', 'service');
    } else {
      menusQB.leftJoin('menu.service', 'service');
    }
    // TODO TEST THIS SORCERY
    menusQB.where('service.fentityId = :entity_id', { entity_id });

    if (service_id) {
      menusQB.andWhere(' serviceId = :service_id', { service_id });
    }

    if (date) {
      menusQB.andWhere(' date = :date', { date });
    } else {
      if (date_begin && date_end) {
        menusQB.andWhere(' date BETWEEN :date_begin AND :date_end', { date_begin, date_end });
      }
    }

    const entities: any = await menusQB.getManyAndCount();

    if (relateds.find(related => related === 'dishs')) {
      for (const entity of entities[0]) {
        if (entity.dishs.length > 0)
          entity.dishs = entity.dishs[0].dish === null ? [] : entity.dishs;
      }
    }

    return entities;
  }

  public async checkOfExistsMenuDish(menuId: number, typeId: number, dishId: number): Promise<MenuDish> {
    return await this.menuDishRepo.findOne(null, { where: { menuId, dishId, typeId }, select: ['id'] });
  }

  public async findMeal(relateds: string[], language_id: number, service_id: number,
    date: string, meal: Meal, profile_id: number, entity_id: number): Promise<Menu> {
    const menusQB = this.menuRepo.createQueryBuilder('menu');

    const resultService = await this.serviceService.existID(service_id, entity_id, ServiceType.CANTEEN);
    if (resultService === 0) {
      throw new NotFoundException(404, 'the service id sent was not found');
    }

    if (relateds.find(related => related === 'dishs')) {
      menusQB.leftJoinAndSelect('menu.dishs', 'dishs');
      menusQB.leftJoinAndSelect('dishs.dish', 'dish');
      menusQB.leftJoinAndSelect('dish.translations',
        'dish_translation',
        language_id ? 'dish_translation.language_id = :language_id' : '',
        { language_id });

      if (relateds.find(related => related === 'dishstype')) {
        menusQB.leftJoinAndSelect('dishs.type', 'type');
        menusQB.leftJoinAndSelect('type.translations',
          'type_translation',
          language_id ? 'type_translation.language_id = :language_id' : '',
          { language_id });

        if (relateds.find(related => related === 'disponibility')) {
          menusQB.leftJoinAndSelect('type.disponibility_lunch', 'disponibility_lunch');
          menusQB.leftJoinAndSelect('type.disponibility_dinner', 'disponibility_dinner');
        }

        if (relateds.find(related => related === 'prices') && profile_id) {
          menusQB.leftJoinAndSelect('type.prices', 'prices',
            'prices.time <= CURRENT_TIME() AND prices.profile_id = :profile_id', { profile_id });
        }
      }
    }

    if (relateds.find(related => related === 'service')) {
      menusQB.leftJoinAndSelect('menu.service', 'service');
    }

    if (service_id) {
      menusQB.where('menu.serviceId = :service_id', { service_id });
    }

    if (date) {
      menusQB.andWhere('menu.date = :date', { date });
    }

    if (meal) {
      menusQB.andWhere('menu.meal = :meal', { meal });
    }

    const entity: any = await menusQB.getOne();

    if (entity && relateds.find(related => related === 'dishs')) {
      entity.dishs = entity.dishs.length > 0 ? entity.dishs : [];
    }

    return entity;
  }

  public async findByID(id: number, relateds: string[], language_id: number, entity_id: number): Promise<Menu> {
    const persistedEntity = await this.menuRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const menuQB = this.menuRepo.createQueryBuilder('menu');

    if (relateds.find(related => related === 'dishs')) {
      menuQB.leftJoinAndSelect('menu.dishs', 'dishs');
      menuQB.leftJoinAndSelect('dishs.dish', 'dish');
      menuQB.leftJoinAndSelect('dish.translations',
        'dish_translation',
        language_id ? 'dish_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'dishstype')) {
      menuQB.leftJoinAndSelect('dishs.type', 'type');
      menuQB.leftJoinAndSelect('type.translations',
        'type_translation',
        language_id ? 'type_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'service')) {
      menuQB.leftJoinAndSelect('menu.service', 'service');
    }

    menuQB.where('menu.id = :id', { id });
    const entity: any = await menuQB.getOne();

    if (relateds.find(related => related === 'dishs')) {
      entity.dishs = entity.dishs[0].dish === null ? [] : entity.dishs;
    }
    return entity;
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.menuRepo.count({ id });
  }

  public async update(id: number, entityDto: MenuDto, entity_id: number, unvailableMenuDishs = true) {
    const persistedEntity = await this.menuRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }

    const dishs_id = entityDto.dishs.map(dish => dish.dish_id);
    const dishTypes_id = entityDto.dishs.map(dish => dish.dishType_id);
    const dishsCount = await this.dishsService.existIDs(dishs_id, entity_id);
    if (dishsCount < [...new Set(dishs_id)].length) {
      throw new NotFoundException(404, 'the dish id sent doesnt exist');
    }
    const dishTypesCount = await this.dishTypesService.existIDs(dishTypes_id, entity_id);
    if (dishTypesCount < [...new Set(dishTypes_id)].length) {
      throw new NotFoundException(404, 'the dish type id sent doesnt exist');
    }

    return new Promise<Menu>(async (resolve, reject) => {
      this.menuRepo.manager.transaction(async entityManger => {

        if (unvailableMenuDishs) {
        await this.menuDishRepo.createQueryBuilder()
          .update(MenuDish)
          .set({
            available: false
          })
          .where('available = 1 AND menuId = :id', { id })
          .execute();
        }

        for (const dish of entityDto.dishs) {
          const td = new Dish();
          td.id = dish.dish_id;
          const tdt = new DishType();
          tdt.id = dish.dishType_id;
          const tm = new Menu();
          tm.id = id;
          const tmd = new MenuDish();
          // tmd.id = dish.menu_dish_id ? dish.menu_dish_id : null;
          tmd.menu = tm;
          tmd.type = tdt;
          tmd.dish = td;
          tmd.doses_available = dish.doses_available;

          const anu = await this.dishTypesService.availableNullableUntil(
            dish.dishType_id,
            entityDto.meal,
            entityDto.date); // CALCULATE THE MAXIMUM HOUR TO BUY OR CANCEL
          tmd.available_until = anu.available_until;
          tmd.nullable_until = anu.nullable_until;

          tmd.available = dish.available;

          const existMD = await this.checkOfExistsMenuDish(id, dish.dishType_id, dish.dish_id);
          if (existMD) {
            tmd.id = existMD.id;
          }

          if (tmd.id) {
            await this.menuDishRepo
              .createQueryBuilder()
              .update(MenuDish)
              .set(tmd)
              .where('id = :id', { id: tmd.id })
              .execute();
          } else {
            await this.menuDishRepo
              .createQueryBuilder()
              .insert()
              .into(MenuDish)
              .values(tmd)
              .execute();
          }
        }

        resolve(await this.findByID(id, ['dishs', 'dishstype', 'service'], null, entity_id));
      }).catch(error => {
        reject(error);
      });
    });
  }

  public async validation(id: number, validated: boolean, entity_id: number): Promise<Menu> {
    const persistedEntity = await this.menuRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Menu not found');
    }

    const updated = await this.menuRepo.update(id, { validated });
    return await this.findByID(id, ['dishs', 'dishstype', 'service'], null, entity_id);
  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.menuRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }
    return await this.menuRepo.delete(id);
  }

  public async getMenuDish(id: number, relateds: string[], language_id?: number): Promise<MenuDish> {
    const persistedEntity = await this.menuDishRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Menu Dish not found');
    }

    const mdQB = this.menuDishRepo.createQueryBuilder('md');

    if (relateds.find(related => related === 'menus')
      || relateds.find(related => related === 'services')) {

      mdQB.leftJoinAndSelect('md.menu', 'menu');

      if (relateds.find(related => related === 'services')) {
        mdQB.leftJoinAndSelect('menu.service', 'service');
        mdQB.leftJoinAndSelect('service.fentity', 'fentity');
        mdQB.leftJoinAndSelect('service.school', 'school');
      }
    }

    if (relateds.find(related => related === 'dishs')) {
      mdQB.leftJoinAndSelect('md.dish', 'dish');
      mdQB.leftJoinAndSelect('dish.translations', 'translation', language_id ? 'translation.language_id = :language_id' : '', { language_id });
    }

    if (relateds.find(related => related === 'types')
      || relateds.find(related => related === 'disponibilities')
      || relateds.find(related => related === 'prices')) {
      mdQB.leftJoinAndSelect('md.type', 'type');
      mdQB.leftJoinAndSelect('type.translations', 'type_translation', language_id ? 'type_translation.language_id = :language_id' : '', { language_id });

      if (relateds.find(related => related === 'disponibilities')) {
        mdQB.leftJoinAndSelect('type.disponibility_lunch', 'dlunch');
        mdQB.leftJoinAndSelect('type.disponibility_dinner', 'ddinner');
      }

      if (relateds.find(related => related === 'prices')) {
        mdQB.leftJoinAndSelect('type.prices', 'prices');
      }
    }

    mdQB.where('md.id = :id', { id });

    return mdQB.getOne();
  }

  /***
   * This function is for the reservations of packs it returns a menudish of in the service sent of a type
   * @param service_id
   * @param dish_type_id
   */
  getMenuDishsToPackReserve(service_id: number, type_id: number): Promise<MenuDish> {
    return this.menuDishRepo.createQueryBuilder('md')
      .leftJoin('md.menu', 'm')
      .where('md.typeId = :type_id AND m.serviceId = :service_id', { type_id, service_id })
      .getOne();
  }

  /**
   * Copy menu
   */
  async copyMenu(id: number, data: CopyMenuDto, entity_id: number ): Promise<Menu[]> {
    const persistedEntity = await this.menuRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Menu not found');
    }

    // Check if is overlapping
    const existMenus = await this.menuRepo.count({
      where: {
        date: In(data.dates.map(d => moment(d).format('YYYY-MM-DD'))),
        service_id: In(data.service_ids),
        meal: In(data.meals)
      }
    });

    if (existMenus > 0) {
      const message = 'Already exist menus in this dates';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(message),
          message
        }
      ]);
    }

    const persistedMenu = await this.menuRepo.findOneOrFail(id, { relations: [
      'dishs'
    ]});

    const copiedMenus: Menu[] = [];
    for (const d of data.dates) {
      for (const m of data.meals) {
        for (const s of data.service_ids) {

            const newMenu: MenuDto = {
              id: null,
              date: d,
              service_id: s,
              meal: m,
              dishs: persistedMenu.dishs.map(md => MenuDishDto.fromObj({
                dish_id: md.dishId,
                dishType_id: md.typeId,
                available: md.available,
                doses_available: md.doses_available
              }))
            };
            copiedMenus.push(await this.create(newMenu, entity_id));
        }
      }
    }

    return Promise.resolve(copiedMenus);
  }

  async copyDay(data: CopyDayDto, entity_id: number): Promise<Menu[]>  {

    const persistedEntity = await this.menuRepo.count({
      date: moment(data.from_day).format('YYYY-MM-DD'),
      service_id: data.from_service_id,
     });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Menu not found in this date');
    }

    const existMenus = await this.menuRepo.count({
      where: {
        date: In(data.dates.map(d => moment(d).format('YYYY-MM-DD'))),
        service_id: In(data.service_ids),
      }
    });

    if (existMenus > 0) {
      const message = 'Already exist menus in this dates';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(message),
          message
        }
      ]);
    }

    const persistedMenus = await this.menuRepo.find({
      where: {
        date: moment(data.from_day).format('YYYY-MM-DD'),
        service_id: data.from_service_id,
      },
      relations: [
        'dishs'
      ]
    });

    const copiedMenus: Menu[] = [];
    for (const d of data.dates) {
      for (const s of data.service_ids) {
        for (const m of persistedMenus) {
          const newMenu: MenuDto = {
            id: null,
            date: d,
            service_id: s,
            meal: m.meal,
            dishs: m.dishs.map(md => MenuDishDto.fromObj({
              dish_id: md.dishId,
              dishType_id: md.typeId,
              available: md.available,
              doses_available: md.doses_available
            }))
          };
          copiedMenus.push(await this.create(newMenu, entity_id));
        }
      }
    }

    return Promise.resolve(copiedMenus);
  }

  async copyWeek(data: CopyWeekDto, entity_id: number): Promise<Menu[]>  {
    const firstToDayOfWeek = moment(data.to_day_of_week).startOf('week');
    const toDaysOfWeek = [];

    toDaysOfWeek.push(firstToDayOfWeek.format('YYYY-MM-DD'));
    for (let dow = 1; dow < 7; dow++ ) {
      toDaysOfWeek.push(firstToDayOfWeek.add(1, 'day').format('YYYY-MM-DD'));
    }

    const existMenus = await this.menuRepo.count({
      where: {
        date: In(toDaysOfWeek),
        service_id: In(data.service_ids),
      }
    });

    if (existMenus > 0) {
      const message = 'Already exist menus in this dates';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(message),
          message
        }
      ]);
    }

    const firstFromDayOfWeek = moment(data.from_day_of_week).startOf('week');
    const fromDaysOfWeek = [];

    fromDaysOfWeek.push(firstFromDayOfWeek.format('YYYY-MM-DD'));
    for (let dow = 1; dow < 7; dow++ ) {
      fromDaysOfWeek.push(firstFromDayOfWeek.add(1, 'day').format('YYYY-MM-DD'));
    }

    const persistedMenus = await this.menuRepo.find({
      where: {
        date: In(fromDaysOfWeek),
        service_id: data.from_service_id,
      },
      relations: [
        'dishs'
      ]
    });

    const copiedMenus: Menu[] = [];
    for (const s of data.service_ids) {
      for (const m of persistedMenus) {
        const newMenu: MenuDto = {
          id: null,
          date: this.findEquivalentDayOfWeek(m.date, fromDaysOfWeek, toDaysOfWeek),
          service_id: s,
          meal: m.meal,
          dishs: m.dishs.map(md => MenuDishDto.fromObj({
            dish_id: md.dishId,
            dishType_id: md.typeId,
            available: md.available,
            doses_available: md.doses_available
          }))
        };
        copiedMenus.push(await this.create(newMenu, entity_id));
      }
    }

    return Promise.resolve(copiedMenus);
  }

  private findEquivalentDayOfWeek(day: Date, fromDates: string[], toDates: string[]): string {
    const dayStr = moment(day).format('YYYY-MM-DD');
    const indexOf = fromDates.indexOf(dayStr);

    return toDates[indexOf];
  }
}
