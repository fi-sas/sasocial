import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1547564217322 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `menu_dish` ADD `doses_available` int NOT NULL");
        await queryRunner.query("ALTER TABLE `menu_dish` ADD `available` tinyint NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `menu_dish` DROP COLUMN `available`");
        await queryRunner.query("ALTER TABLE `menu_dish` DROP COLUMN `doses_available`");}

}
