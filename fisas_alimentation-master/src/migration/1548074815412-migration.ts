import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1548074815412 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `fiscal_entity` ADD `account_id` int NOT NULL");
        await queryRunner.query("ALTER TABLE `service` ADD `service_id` int NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `service` DROP COLUMN `service_id`");
        await queryRunner.query("ALTER TABLE `fiscal_entity` DROP COLUMN `account_id`");
    }

}
