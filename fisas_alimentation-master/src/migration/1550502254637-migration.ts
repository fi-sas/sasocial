import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1550502254637 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
    
        await queryRunner.query("ALTER TABLE `reservation` ADD `menuDishId` int NOT NULL");
        await queryRunner.query("ALTER TABLE `reservation` ADD CONSTRAINT `FK_5021f75a3edcf913484a0b0599f` FOREIGN KEY (`menuDishId`) REFERENCES `menu_dish`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP FOREIGN KEY `FK_5021f75a3edcf913484a0b0599f`");
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `menuDishId`");
    }

}
