import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1550591841925 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `user_allergen` (`allergenId` int NOT NULL, `user_id` int NOT NULL, PRIMARY KEY (`allergenId`, `user_id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user_allergen` ADD CONSTRAINT `FK_47c9474b744b96432fa7e464408` FOREIGN KEY (`allergenId`) REFERENCES `allergen`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_allergen` DROP FOREIGN KEY `FK_47c9474b744b96432fa7e464408`");
        await queryRunner.query("DROP TABLE `user_allergen`");
    }

}
