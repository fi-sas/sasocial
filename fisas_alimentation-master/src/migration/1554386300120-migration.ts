import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1554386300120 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `family` CHANGE `file_id` `file_id` int NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `family` CHANGE `file_id` `file_id` int NOT NULL");
    }

}
