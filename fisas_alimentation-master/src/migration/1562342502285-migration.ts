import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1562342502285 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` CHANGE `fentityId` `fentityId` int NULL");
        await queryRunner.query("ALTER TABLE `reservation` CHANGE `date` `date` date NULL");
    }
    
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` CHANGE `fentityId` `fentityId` INT NOT NULL");
        await queryRunner.query("ALTER TABLE `reservation` CHANGE `date` `date` date NOT NULL");
    }

}
