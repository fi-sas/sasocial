import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1563286693936 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `order_id` int NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `order_id`");
    }

}
