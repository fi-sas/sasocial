import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1563959793188 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `location` VARCHAR(50) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `location`");
    }

}
