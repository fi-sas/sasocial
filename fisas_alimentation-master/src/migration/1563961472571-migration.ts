import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1563961472571 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `served_at` datetime(6) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `served_at`");
    }

}
