import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1575472126938 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `item_id` int NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `item_id`");
    }

}
