import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1575889528375 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `device_id` int NULL DEFAULT null");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `device_id`");
        }

}
