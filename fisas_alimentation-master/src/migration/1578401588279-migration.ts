import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1578401588279 implements MigrationInterface {
    name = 'migration1578401588279'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `disponibility` ADD `annulment_maximum_hour` time NULL", undefined);
        await queryRunner.query("ALTER TABLE `menu_dish` ADD `available_until` datetime NULL", undefined);
        await queryRunner.query("ALTER TABLE `menu_dish` ADD `nullable_until` datetime NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `menu_dish` DROP COLUMN `nullable_until`", undefined);
        await queryRunner.query("ALTER TABLE `menu_dish` DROP COLUMN `available_until`", undefined);
        await queryRunner.query("ALTER TABLE `disponibility` DROP COLUMN `annulment_maximum_hour`", undefined);
    }

}
