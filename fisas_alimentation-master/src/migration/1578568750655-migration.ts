import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1578568750655 implements MigrationInterface {
    name = 'migration1578568750655'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` ADD `has_canceled` tinyint NOT NULL DEFAULT 0", undefined);
        await queryRunner.query("ALTER TABLE `reservation` ADD `annulment_movement_id` int NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `annulment_movement_id`", undefined);
        await queryRunner.query("ALTER TABLE `reservation` DROP COLUMN `has_canceled`", undefined);
    }

}
