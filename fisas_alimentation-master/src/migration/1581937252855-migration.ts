import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1581937252855 implements MigrationInterface {
    name = 'migration1581937252855'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `disponibility` ADD `minimum_hour` time NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `disponibility` DROP COLUMN `minimum_hour`", undefined);
        }

}
