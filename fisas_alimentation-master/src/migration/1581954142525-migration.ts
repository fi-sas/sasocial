import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1581954142525 implements MigrationInterface {
    name = 'migration1581954142525'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `price_variation` ADD `meal` enum ('lunch', 'dinner', 'breakfast') NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `price_variation` DROP COLUMN `meal`", undefined);
    }

}
