import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1582195240726 implements MigrationInterface {
    name = 'migration1582195240726'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `product` CHANGE `file_id` `file_id` int NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `product` CHANGE `file_id` `file_id` int NOT NULL", undefined);
    }

}
