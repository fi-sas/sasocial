import { MigrationInterface, QueryRunner } from "typeorm";

export class migration1590163890851 implements MigrationInterface {
    name = 'migration1590163890851'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `description` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `preparation` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `preparation_mode` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `microbiological_criteria` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `packaging` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `use` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `distribution` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `comment` text", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `associated_docs` text", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `associated_docs` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `comment` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `distribution` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `use` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `packaging` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `microbiological_criteria` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `preparation_mode` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `preparation` varchar(255) NOT NULL", undefined);
        await queryRunner.query("ALTER TABLE `recipe` MODIFY `description` varchar(255) NOT NULL", undefined);
    }

}
