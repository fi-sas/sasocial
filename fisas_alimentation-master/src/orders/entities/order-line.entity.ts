import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class OrderLine {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Order, order => order.lines, { nullable: false })
  order: Order;

  @Column()
  quantity: number;

  @Column()
  price: number;

  // TODO SAVE THE STOCK ID TO TRACK LOTE
}