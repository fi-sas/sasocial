import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { OrderLine } from './order-line.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';

@Entity()
export class Order extends BaseEntity {

  @OneToMany(type => OrderLine, orderline => orderline.order)
  lines: OrderLine[];

  @Column()
  user_id: number;

  @Column({ nullable: false, default: false })
  served: boolean;

  @Column({ type: 'datetime'})
  serveDateTime: Date;

  @Column()
  servedByUser_id: number;

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}
