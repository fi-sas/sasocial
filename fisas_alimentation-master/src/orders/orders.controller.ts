import { Controller, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { TokenGuard } from '../common/guards/token.guard';
import { ApiProduces, ApiConsumes, ApiBearerAuth, ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { Scopes } from '../common/decorators/scope.decorator';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { CONFIGURATIONS } from '../app.config';
import { OrdersService } from './orders.service';
import { FiscalEntityService } from '../entity/fiscal-entity.service';
import { User } from '../common/decorators/user.decorator';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards( TokenGuard)
@ApiUseTags('orders')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/orders')
export class OrdersController {

  constructor(private readonly ordersService: OrdersService,
              private readonly fentitiesService: FiscalEntityService) {
  }

  @Scopes('orders')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @User() user) {
    await this.fentitiesService.checkUserPermission(entity_id, user.id);
    return 'Ola Mundo';
  }

  @Scopes('orders')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the order', name: 'id', type: Number })
  @Get(':id')
  public async findByID(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {}

  @Scopes('orders')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the order', name: 'id', type: Number })
  @Put(':id')
  public async updateOrder(@Param('entity_id', new ParseIntPipe()) entity_id) {}

  @Scopes('orders')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the order', name: 'id', type: Number })
  @Post(':id')
  public async confirmOrder(@Param('entity_id', new ParseIntPipe()) entity_id) {}

  @Scopes('orders')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the order', name: 'id', type: Number })
  @Post(':id')
  public async rejectOrder(@Param('entity_id', new ParseIntPipe()) entity_id) {}
}
