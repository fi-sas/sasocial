import { Module } from '@nestjs/common';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderLine } from './entities/order-line.entity';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    TypeOrmModule.forFeature([
      Order,
      OrderLine
    ]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService]
})
export class OrdersModule {}
