import { Body, Controller, Delete, Get, Headers, HttpCode, HttpException, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ComplementsService } from './complements.service';
import { CONFIGURATIONS } from '../app.config';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { Complement } from './entities/complement.entity';
import { ComplementCreateDto } from './dtos/complement-create.dto';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitHeader, ApiImplicitQuery } from '@nestjs/swagger';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { ValidationException } from '../common/exceptions/validation.exception';
import { LanguagesService } from '../common/services/languages.service';
import { Token } from '../common/decorators/token.decorator';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards( TokenGuard)
@ApiUseTags('complements')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/complements')
export class ComplementsController {

  constructor( private readonly complementsService: ComplementsService,
               private readonly languageService: LanguagesService) {}

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitQuery({ required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number})
  @ApiImplicitQuery({ required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number})
  @ApiImplicitQuery({ required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String})
  @ApiImplicitQuery({
    required: false, name: 'name',
    description: 'Find complements by partial name.', type: String,
  })
  @Scopes('complements')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id) {
    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (! query.hasOwnProperty('sort')) query.sort = '-id';
    if (! query.hasOwnProperty('name')) query.name = null;
    const complements = await this.complementsService.findAll(query.offset, query.limit, query.sort, query.name, language_id, entity_id);
    return new Response<Complement[]>(ResponseStatus.OK, complements[0], {
      total: complements[1]
    });
  }

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of complement', name: 'id', type: Number })
  @Scopes('complements')
  @Get(':id')
  async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id, @Headers('x-language-id') language_id) {
    const complement = await this.complementsService.findById(id, language_id, entity_id);

    return new Response<Complement>(ResponseStatus.OK, complement);
  }

  @Scopes('complements')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() complement: ComplementCreateDto, @Token() token) {
    if (complement.translations.length === 0) {
      throw new ValidationException([new ResponseError(2021, 'têm de existir uma tradução pelo menos')]);
    }
    await this.languageService.validateBody(complement, token)
      .catch(reason => {
        throw new HttpException(reason[0], reason[0].code);
      });

    const createdComplement = await this.complementsService.create(complement, entity_id);
    return new Response<Complement>(ResponseStatus.OK, createdComplement);
  }

  @ApiImplicitParam({ required: true, description: 'id of complement', name: 'id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Scopes('complements')
  @Put(':id')
  async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
               @Body() complement: ComplementCreateDto, @Token() token) {
    if (complement.translations.length === 0) {
      throw new ValidationException([new ResponseError(2021, 'têm de existir uma tradução pelo menos')]);
    }
    await this.languageService.validateBody(complement, token)
      .catch(reason => {
        throw new HttpException(reason[0], reason[0].code);
      });

    const updatedComplement = await this.complementsService.update(id, complement, entity_id);
    return new Response<Complement>(ResponseStatus.OK, updatedComplement);
  }

  @ApiImplicitParam({ required: true, description: 'id of complement', name: 'id', type: Number })
  @Scopes('complements')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.complementsService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}