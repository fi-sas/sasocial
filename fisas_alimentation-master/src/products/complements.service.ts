import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { Complement } from './entities/complement.entity';
import { ComplementCreateDto } from './dtos/complement-create.dto';
import { ComplementTranslation } from './entities/complement-translation.entity';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';

@Injectable()
export class ComplementsService {

  constructor(@InjectRepository(Complement) private readonly complementRepo: Repository<Complement>
  ) {}

  public async findAll(offset: number, limit: number, sort: string,
                       name: string, language_id: number, entity_id: number): Promise<[Complement[], number]> {
    const complementsQB = this.complementRepo.createQueryBuilder('complement');

    if (limit > 0) {
      complementsQB.take(limit);
      complementsQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      complementsQB.orderBy('complement.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    complementsQB.leftJoinAndSelect('complement.translations',
      'complement_translation',
      language_id ? 'complement_translation.language_id = :language_id' : '',
      { language_id });

    complementsQB.where('complement.fentityId = :entity_id', { entity_id });

    if (name) {
      complementsQB.andWhere('complement_translation.name LIKE :name', { name: '%' + name + '%'});
    }

    return await complementsQB.getManyAndCount();
  }

  public async findById(id: number, language_id: number, entity_id: number): Promise<Complement> {
    const persistedComplement = await this.complementRepo.count({ id, fentity: { id: entity_id} });
    if (persistedComplement === 0) {
      throw new NotFoundException(404, 'Complemento não existe');
    }

    const complementQB = this.complementRepo.createQueryBuilder('complement');

    complementQB.leftJoinAndSelect('complement.translations',
      'complement_translation',
      language_id ? 'complement_translation.language_id = :language_id' : '',
      { language_id });

    complementQB.where('complement.id = :id ', { id });

    return await complementQB.getOne();
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.complementRepo.count({ id, fentity: { id : entity_id } });
  }

  public async create(complementDto: ComplementCreateDto, entity_id: number): Promise<Complement> {
    return new Promise<Complement>(async (resolve, reject) => {
      let id: number = 0;
      const tfe = new FiscalEntity();
      tfe.id = entity_id;

      await this.complementRepo.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(Complement)
          .values({
            price: complementDto.price,
            fentity: tfe
          })
          .execute();

        id = insertResult.identifiers[0].id;
        const tcomplement = new Complement();
        tcomplement.id = id;

        for (const translation of complementDto.translations) {
          const tct = new ComplementTranslation();
          tct.name = translation.name;
          tct.description = translation.description;
          tct.language_id = translation.language_id;
          tct.complement = tcomplement;

          await entityManger.createQueryBuilder()
            .insert()
            .into(ComplementTranslation)
            .values(
              tct
            )
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findById(id, null, entity_id));
      } catch ( reason) {
        reject(reason);
      }
    });
  }

  public async update(id: number, complementDto: ComplementCreateDto, entity_id: number): Promise<Complement> {
    const persistedComplement = await this.complementRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedComplement === 0) {
      throw new NotFoundException(404, 'Complemento não existe');
    }

    return new Promise<Complement>(async (resolve, reject) => {
      await this.complementRepo.manager.transaction(async entityManger => {
        await entityManger.createQueryBuilder()
          .update(Complement)
          .set({
            price: complementDto.price
          })
          .execute();

        const tcomplement = new Complement();
        tcomplement.id = id;

        await entityManger.createQueryBuilder()
          .delete()
          .from(ComplementTranslation)
          .where('complementId = :id ', { id })
          .execute();

        for (const translation of complementDto.translations) {
          const tct = new ComplementTranslation();
          tct.name = translation.name;
          tct.description = translation.description;
          tct.language_id = translation.language_id;
          tct.complement = tcomplement;

          await entityManger.createQueryBuilder()
            .insert()
            .into(ComplementTranslation)
            .values(
              tct
            )
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findById(id, null, entity_id));
      } catch ( reason) {
        reject(reason);
      }
    });
  }

  public async delete(id: number, entity_id: number): Promise<DeleteResult> {
    const persistedComplement = await this.complementRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedComplement === 0) {
      throw new NotFoundException(404, 'Complemento não existe');
    }

    return await this.complementRepo.delete(id);
  }
}