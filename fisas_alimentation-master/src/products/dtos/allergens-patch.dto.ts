import { IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class AllergensPatchDto {

  @ApiModelProperty({ type: Number, isArray: true})
  @IsArray({ message: 'allergens têm de se um array'})
  allergens: number[];
}