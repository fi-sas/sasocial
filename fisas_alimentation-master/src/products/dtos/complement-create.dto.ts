import { ArrayMinSize, IsArray, IsNotEmpty, IsNumber, IsString, Min, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class ComplementTranslationCreateDto {
  complement: number;

  @ApiModelProperty()
  @IsNumber({}, { message: 'id da linguagem não esta correto' })
  language_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'name não pode ser nulo'})
  @IsString({ message: 'name têm de ser texto'})
  name: string;

  @ApiModelPropertyOptional()
  @IsString({ message: 'description têm de ser texto'})
  description: string;
}

export class ComplementCreateDto {
  id: number;

  @ApiModelProperty({ type: ComplementTranslationCreateDto, isArray: true, minItems: 1})
  @IsArray({ message: 'translation têm de se um array'})
  @ArrayMinSize(1, { message: 'Têm de existir uma tradução no minimo'})
  @ValidateNested()
  @Type(() => ComplementTranslationCreateDto)
  translations: ComplementTranslationCreateDto[];

  @ApiModelProperty()
  @Min(0, { message: 'A preço têm de ser maior que zero'})
  @IsNumber({}, { message: 'preço têm de ser um numero'})
  price: number;
}
