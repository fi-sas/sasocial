import { IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ComplementsPatchDto {
  @ApiModelProperty({ type: Number, isArray: true})
  @IsArray({ message: 'complements têm de se um array'})
  complements: number[];
}