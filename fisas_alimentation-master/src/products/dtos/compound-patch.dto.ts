import { IsArray, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CompoundDto } from './product.dto';

export class CompoundPatchDto {

  @ApiModelProperty({ type: CompoundDto, isArray: true})
  @IsArray({ message: 'composition têm de se um array'})
  @ValidateNested()
  @Type(() => CompoundDto)
  composition: CompoundDto[];
}