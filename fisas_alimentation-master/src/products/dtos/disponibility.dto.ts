import { IsBoolean, IsDateString, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
export class DisponibilityDto {
  id: number;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'begin_date is type string'})
  begin_date: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'end_date is type string'})
  end_date: string;

  @ApiModelProperty()
  @IsBoolean({message: 'monday is type boolean'})
  monday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'tuesday is type boolean'})
  tuesday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'wednesday is type boolean'})
  wednesday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'thursday is type boolean'})
  thursday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'friday is type boolean'})
  friday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'saturday is type boolean'})
  saturday: boolean;

  @ApiModelProperty()
  @IsBoolean({message: 'sunday is type boolean'})
  sunday: boolean;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'minimum_hour is type string format YYYY-MM-ddTHH:mm:ss.mmm'})
  minimum_hour: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'maximum_hour is type string format YYYY-MM-ddTHH:mm:ss.mmm'})
  maximum_hour: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'annulment_maximum_hour is type string format YYYY-MM-ddTHH:mm:ss.mmm'})
  annulment_maximum_hour: string;
}