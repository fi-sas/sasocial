import { IsArray, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { NutrientProductDto } from './product.dto';

export class NutrientsPatchDto {

  @ApiModelProperty({ type: NutrientProductDto, isArray: true})
  @IsArray({ message: 'nutrients têm de se um array'})
  @ValidateNested()
  @Type(() => NutrientProductDto)
  nutrients: NutrientProductDto[];
}