import { ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { PriceVariationDto } from './product.dto';

export class PricesPatchDto {

  @ApiModelProperty({ type: PriceVariationDto, isArray: true })
  @ValidateNested()
  @Type(() => PriceVariationDto)
  prices: PriceVariationDto[];
}