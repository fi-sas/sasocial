import { Meal } from './../../menus/entities/menu.entity';
import {
  ArrayMinSize,
  IsArray,
  IsBoolean, IsDateString,
  IsEnum, IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  Min, ValidateIf,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { DisponibilityDto } from './disponibility.dto';
import { ServiceType } from '../../common/enums/service-type.enum';
import { StockTrack } from '../../common/enums/stock-track.enum';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class NutrientProductDto {
  @ApiModelProperty()
  @Min(0, { message: 'A quantidade têm de ser maior que zero'})
  @IsNumber({}, { message: 'quantidade têm de ser um numero'})
  quantity: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o nutriente não pode ser vazio'})
  @IsNumber({}, { message: 'id do nutrient não esta correcto'})
  nutrient_id: number;
}

export class ProductTranslationDto {
  @ApiModelProperty()
  @IsNumber({}, { message: 'id da linguagem não esta correto' })
  language_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'name não pode ser nulo'})
  @IsString({ message: 'name têm de ser texto'})
  name: string;

  @ApiModelProperty()
  @IsString({ message: 'description têm de ser texto'})
  description: string;
}

export class CompoundDto {
  @ApiModelProperty()
  @IsNumber({}, { message: 'id do compound não esta correto' })
  compound_id: number;

  @ApiModelProperty()
  @Min(0, { message: 'A quantidade têm de ser maior que zero'})
  @IsNumber({}, { message: 'quantidade têm de ser um numero'})
  quantity: number;
}

export class PriceVariationDto {
  @ApiModelPropertyOptional()
  @IsOptional()
  @IsString({ message: 'description da variação de têm de ser texto'})
  description: string;

  @ApiModelProperty()
  @IsNumber({}, { message: 'id da taxa iva na variação de preço não esta correcta'})
  tax_id: number;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumber({}, { message: 'id do user group na variação de preço não esta correcta'})
  profile_id: number;

  @ApiModelProperty()
  @Min(0, { message: 'o preço da variação de preço têm de ser maior que zero'})
  @IsNumber({}, { message: 'o preço da variação de preço têm de ser um numero'})
  price: number;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsDateString({ message: 'time e do tipo string YYYY-MM-ddTHH:mm:ss.mmm'})
  time: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsEnum(Meal, { message: 'meal is a enum of lunch,dinner,breakfast'})
  meal: Meal;
}

export class ProductDto {

  @ApiModelProperty({ type: ProductTranslationDto, isArray: true, minItems: 1 })
  @IsArray({ message: 'translation têm de se um array'})
  @ArrayMinSize(1, { message: 'Têm de existir uma tradução no minimo'})
  @ValidateNested()
  @Type(() => ProductTranslationDto)
  translations: ProductTranslationDto[];

  @ApiModelProperty()
  @ValidateIf(object => object.type !== ServiceType.CANTEEN)
  @IsNumber({}, { message: 'file_id e um numero'})
  file_id: number;

  @ApiModelPropertyOptional({ type: Number, isArray: true})
  @IsOptional()
  @IsArray({ message: 'allergens têm de se um array'})
  allergens: number[];

  @ApiModelPropertyOptional({ type: Number, isArray: true})
  @IsOptional()
  @IsArray({ message: 'complements têm de se um array'})
  complements: number[];

  @ApiModelPropertyOptional({ type: CompoundDto, isArray: true})
  @IsOptional()
  @IsArray({ message: 'composition têm de se um array'})
  @ValidateNested()
  @Type(() => CompoundDto)
  composition: CompoundDto[];

  @ApiModelPropertyOptional({ type: NutrientProductDto, isArray: true})
  @IsOptional()
  @IsArray({ message: 'nutrients têm de se um array'})
  @ValidateNested()
  @Type(() => NutrientProductDto)
  nutrients: NutrientProductDto[];

  @ApiModelProperty()
  @IsNumber({}, { message: 'O id da unidade não esta correto' })
  unit_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'O code do produto não pode ser vazio' })
  @IsString({ message: 'o code tem de ser do tipo string'})
  @MaxLength(55, { message: 'O code não pode conter mais que 55 caracteres'})
  code: string;

  @ApiModelProperty()
  @Min(0, { message: 'A preco custo têm de ser maior que zero'})
  @IsNumber({}, { message: 'preco custo têm de ser um numero'})
  cost_price: number;

  @ApiModelProperty()
  @IsNumber({}, { message: 'O id da taxa id do preço de custo não esta correto' })
  tax_cost_price_id: number;

  @ApiModelProperty()
  @Min(0, { message: 'A preço têm de ser maior que zero'})
  @IsNumber({}, { message: 'preço têm de ser um numero'})
  price: number;

  @ApiModelProperty()
  @IsNumber({}, { message: 'O id da taxa id do preço não esta correto' })
  tax_price_id: number;

  @ApiModelPropertyOptional({ type: PriceVariationDto, isArray: true })
  @IsOptional()
  @ValidateNested()
  @Type(() => PriceVariationDto)
  prices: PriceVariationDto[];

  @ApiModelProperty({ type: DisponibilityDto})
  @IsNotEmpty({ message: 'disponibility tem de estar preenchido'})
  @ValidateNested()
  @Type(() => DisponibilityDto)
  disponibility: DisponibilityDto;

  @ApiModelProperty()
  @IsBoolean({ message: 'available têm de ser tipo true ou false'})
  available: boolean;

  @ApiModelProperty()
  @IsBoolean({ message: 'visible têm de ser tipo true ou false'})
  visible: boolean;

  @ApiModelProperty()
  @IsBoolean({ message: 'show derivatives têm de ser tipo true ou false'})
  show_derivatives: boolean;

  @ApiModelProperty()
  @Min(0, { message: 'minimal stock têm de ser maior que zero'})
  @IsNumber({}, { message: 'minimal stock têm de ser um numero'})
  minimal_stock: number;

  @ApiModelProperty({ type: StockTrack, enum: ['product', 'composition', 'none', 'product_composition'] })
  @IsEnum(StockTrack, { message: 'Stock track e um enum '})
  track_stock: StockTrack;

  @ApiModelPropertyOptional({ type: ServiceType, enum: ['bar', 'canteen']})
  @IsOptional()
  @IsEnum(ServiceType, { message: 'prodcut type e um enum '})
  type: ServiceType;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}
