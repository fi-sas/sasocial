import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Complement } from './complement.entity';

@Entity()
export class ComplementTranslation {

  @PrimaryColumn()
  language_id: number;

  @ManyToOne(type => Complement, complement => complement.id, { primary: true, onDelete: 'CASCADE' })
  complement: Complement;

  @Column({ nullable : false})
  name: string;

  @Column({ nullable : false})
  description: string;
}