import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { ComplementTranslation } from './complement-translation.entity';
import { Product } from './product.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';

@Entity()
export class Complement extends BaseEntity {

  @OneToMany(type1 => ComplementTranslation, translation => translation.complement, { cascade: true })
  @JoinColumn()
  translations: ComplementTranslation[];

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  price: number;

  @ManyToMany(type1 => Product, product => product.complements, { lazy: true, onDelete: 'CASCADE'})
  products: Product[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}