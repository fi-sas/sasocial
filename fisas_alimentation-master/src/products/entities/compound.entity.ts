import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Product } from './product.entity';

@Entity()
export class Compound {
  @ManyToOne(type => Product, product => product.id, { primary: true, onDelete: 'CASCADE' })
  @JoinColumn()
  product: Product;

  @ManyToOne(type => Product, product => product.id, { primary: true, cascade: false, onDelete: 'CASCADE'})
  @JoinColumn()
  compound: Product;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 3})
  quantity: number;
}