import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { Product } from './product.entity';
import { DishType } from '../../dishs/entities/dish-type.entity';
import { BaseEntity } from '../../common/base.entity';
import { Meal } from '../../menus/entities/menu.entity';

@Entity()
export class Disponibility extends BaseEntity {

  @OneToOne(type => DishType, dishType => dishType.disponibility_lunch, { nullable: true, onDelete: 'CASCADE'})
  @JoinColumn()
  dishType_lunch: DishType;

  @OneToOne(type => DishType, dishType => dishType.disponibility_dinner, { nullable: true, onDelete: 'CASCADE'})
  @JoinColumn()
  dishType_dinner: DishType;

  @OneToOne(type => Product, product => product.disponibility, { nullable: true, onDelete: 'CASCADE'})
  @JoinColumn()
  product: Product;

  @Column({ type: 'date', nullable: true})
  begin_date: Date;

  @Column({ type: 'date', nullable: true})
  end_date: Date;

  @Column({ nullable: false, default: true})
  monday: boolean;

  @Column({ nullable: false, default: true})
  tuesday: boolean;

  @Column({ nullable: false, default: true})
  wednesday: boolean;

  @Column({ nullable: false, default: true})
  thursday: boolean;

  @Column({ nullable: false, default: true})
  friday: boolean;

  @Column({ nullable: false, default: true})
  saturday: boolean;

  @Column({ nullable: false, default: true})
  sunday: boolean;

  @Column({ type: 'time', nullable: true})
  minimum_hour: Date;

  @Column({ type: 'time', nullable: true})
  maximum_hour: Date;

  @Column({ type: 'time', nullable: true})
  annulment_maximum_hour: Date;

}