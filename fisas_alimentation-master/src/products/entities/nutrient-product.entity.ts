import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Product } from './product.entity';
import { Nutrient } from '../../nutrients/entities/nutrient.entity';

@Entity()
export class NutrientProduct {

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  quantity: number;

  @ManyToOne(type => Product, product => product.id, { nullable: false, primary: true, onDelete: 'CASCADE', lazy: true})
  @JoinColumn()
  product: Product;

  @ManyToOne(type => Nutrient, nutrient => nutrient.id, { cascade: true, onDelete: 'CASCADE', nullable: false, primary: true})
  @JoinColumn()
  nutrient: Nutrient;
}