import { Meal } from './../../menus/entities/menu.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { Product } from './product.entity';
import { DishType } from '../../dishs/entities/dish-type.entity';

@Entity()
export class PriceVariation extends BaseEntity{

  @Column({ nullable: true})
  description: string;

  @ManyToOne(type => Product, product => product.id, { nullable: true, onDelete: 'CASCADE', lazy: true })
  @JoinColumn()
  product: Product;

  @Column({ name: 'productId', nullable: true})
  product_id: number;

  @ManyToOne(type => DishType, dishType => dishType.id, { nullable: true, onDelete: 'CASCADE', lazy: true })
  @JoinColumn()
  dishType: DishType;

  @Column({ name: 'dishTypeId', nullable: true})
  dishType_id: number;

  @Column({ nullable: false, type: 'enum', enum: ['lunch', 'dinner', 'breakfast']})
  meal: Meal;

  @Column({ nullable: false})
  tax_id: number;

  @Column({ nullable: true })
  profile_id: number;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  price: number;

  @Column({ type: 'time', default: '00:00:00', nullable: false })
  time: Date;
}