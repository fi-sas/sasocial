import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Product } from './product.entity';

@Entity()
export class ProductTranslation {

  @PrimaryColumn()
  language_id: number;

  @ManyToOne(type => Product, product => product.id, { primary: true, onDelete: 'CASCADE' })
  @JoinColumn()
  product: Product;

  @Column({ nullable : false})
  name: string;

  @Column({ nullable : false})
  description: string;
}