import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { Unit } from '../../units/entities/unit.entity';
import { Allergen } from '../../allergens/entities/allergen.entity';
import { NutrientProduct } from './nutrient-product.entity';
import { ProductTranslation } from './product-translation.entity';
import { PriceVariation } from './price-variation.entity';
import { Complement } from './complement.entity';
import { Disponibility } from './disponibility.entity';
import { Compound } from './compound.entity';
import { Family } from '../../families/entities/family.entity';
import { Stock } from '../../stocks/entities/stock.entity';
import { ServiceType } from '../../common/enums/service-type.enum';
import { StockTrack } from '../../common/enums/stock-track.enum';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { MediaDto } from '../../common/entities/media.dto';

@Entity()
export class Product extends BaseEntity {

  @OneToMany(type1 => ProductTranslation, translation => translation.product, { cascade: true })
  translations: ProductTranslation[];

  @Column({ nullable: true })
  file_id: number;
  file: MediaDto;

  @ManyToMany(type => Allergen, allergen => allergen.id)
  @JoinTable()
  allergens: Allergen[];

  @OneToMany(type => NutrientProduct, nutrientProduct => nutrientProduct.product, { cascade: true } )
  @JoinColumn()
  nutrients: NutrientProduct[];

  @ManyToOne(type => Unit, unit => unit.id, { nullable: false })
  @JoinColumn()
  unit: Unit;

  @Column({name: 'unitId'})
  unit_id: number;

  @Column({ unique: true, length: 55, nullable: false, readonly: true })
  code: string;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  cost_price: number;

  @Column({ nullable: false})
  tax_cost_price_id: number;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  price: number;

  @Column({ nullable: false})
  tax_price_id: number;

  @OneToMany(type1 => PriceVariation, price => price.product, { cascade: true })
  @JoinTable()
  prices: PriceVariation[];

  @OneToMany(type1 => Compound, compound => compound.product, { cascade: true})
  composition: Compound[];

  @ManyToMany(type1 => Complement, complement => complement.products)
  @JoinTable()
  complements: Complement[];

  @OneToOne(type1 => Disponibility, disponibility => disponibility.product, { nullable: false, cascade: true })
  disponibility: Disponibility;

  @Column({ default: true, nullable: false })
  available: boolean;

  @Column({ default: true, nullable: false })
  visible: boolean;

  @Column({ default: false, nullable: false })
  show_derivatives: boolean;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  minimal_stock: number;

  @Column({ type: 'enum', enum: ['none', 'product', 'composition', 'product_composition'], nullable: false, default: StockTrack.PRODUCT })
  track_stock: StockTrack;

  @Column({ type: 'enum',  enum: ['bar', 'canteen'], nullable: true })
  type: ServiceType;

  @ManyToMany(type1 => Family, family => family.products)
  families: Family[];

  @OneToMany(type1 => Stock, sotck => sotck.product)
  stocks: Stock[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;

  @Column({ default: false, nullable: false, select: false})
  is_deleted: boolean;

  @Column({ type: 'datetime', nullable: true, select: false})
  delete_at: Date;

  @Column({ nullable: true, select: false})
  delete_by_user_id: number;
}
