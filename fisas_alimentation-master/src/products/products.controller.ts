import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CONFIGURATIONS } from '../app.config';
import {
  Response,
  ResponseError,
  ResponseStatus,
} from '../common/response.model';
import { Product } from './entities/product.entity';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ProductDto } from './dtos/product.dto';
import { DisponibilityDto } from './dtos/disponibility.dto';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitHeader,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiProduces,
  ApiUseTags,
} from '@nestjs/swagger';
import { Disponibility } from './entities/disponibility.entity';
import { Allergen } from '../allergens/entities/allergen.entity';
import { AllergensPatchDto } from './dtos/allergens-patch.dto';
import { NutrientProduct } from './entities/nutrient-product.entity';
import { NutrientsPatchDto } from './dtos/nutrients-patch.dto';
import { Complement } from './entities/complement.entity';
import { ComplementsPatchDto } from './dtos/complements-patch.dto';
import { Compound } from './entities/compound.entity';
import { CompoundPatchDto } from './dtos/compound-patch.dto';
import { PriceVariation } from './entities/price-variation.entity';
import { Stock } from '../stocks/entities/stock.entity';
import { PricesPatchDto } from './dtos/prices-patch.dto';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { LanguagesService } from '../common/services/languages.service';
import { Token } from '../common/decorators/token.decorator';
import { MediasService } from '../common/services/medias.service';
import { User } from '../common/decorators/user.decorator';
import { ServiceType } from '../common/enums/service-type.enum';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('products')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/products')
export class ProductsController {
  constructor(
    private readonly productService: ProductsService,
    private readonly languageService: LanguagesService,
    private readonly mediaService: MediasService,
  ) {}

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "complements", "taxes", "composition", "disponibility", "nutrients", "allergens", "prices",' +
      ' "unit", "files", "derivatives"  and "stocks"',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'offset',
    description:
      'Offset from which the items should be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_OFFSET +
      '.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'limit',
    description:
      'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT +
      '. If no pagination is to be applied, use -1.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'sort',
    description:
      'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'name',
    description: 'Find products by partial name.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'type',
    description:
      'Find products by type. Possible values bar, canteen, or empty for all',
    type: String,
  })
  @Scopes('products')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @Get()
  async findAll(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Query() query,
    @Headers('x-language-id') language_id,
    @Token() token,
  ) {
    if (!query.hasOwnProperty('offset'))
      query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit'))
      query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';
    if (!query.hasOwnProperty('name')) query.name = null;
    if (!query.hasOwnProperty('type')) query.type = null;
    if (query.type) {
      query.type = query.type.toUpperCase();
      query.type = ServiceType[query.type];
    }

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'complements' &&
          relation !== 'composition' &&
          relation !== 'disponibility' &&
          relation !== 'nutrients' &&
          relation !== 'files' &&
          relation !== 'allergens' &&
          relation !== 'prices' &&
          relation !== 'unit' &&
          relation !== 'derivatives' &&
          relation !== 'taxes' &&
          relation !== 'stocks',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    const products = await this.productService.findAll(
      query.offset,
      query.limit,
      query.sort,
      relateds,
      query.name,
      query.type,
      language_id,
      entity_id,
      token,
    );
    return new Response<Product[]>(ResponseStatus.OK, products[0], {
      total: products[1],
    });
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "complements", "taxes", "composition", "disponibility", "nutrients", "allergens", "prices",' +
      ' "unit", "files", "derivatives"  and "stocks"',
    type: String,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id')
  async getById(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Query() query,
    @Headers('x-language-id') language_id,
    @Token() token,
  ) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'complements' &&
          relation !== 'composition' &&
          relation !== 'disponibility' &&
          relation !== 'nutrients' &&
          relation !== 'allergens' &&
          relation !== 'files' &&
          relation !== 'prices' &&
          relation !== 'unit' &&
          relation !== 'derivatives' &&
          relation !== 'taxes' &&
          relation !== 'stocks',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    const product = await this.productService.findById(
      id,
      relateds,
      language_id,
      entity_id,
      token,
    );
    return new Response<Product>(ResponseStatus.OK, product);
  }

  @Scopes('products')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @Post()
  async create(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Body() productDto: ProductDto,
    @Token() token,
  ) {
    if (productDto.translations.length === 0) {
      throw new ValidationException([
        new ResponseError(2021, 'têm de existir uma tradução pelo menos'),
      ]);
    }
    await this.languageService.validateBody(productDto, token).catch(reason => {
      throw new HttpException(reason[0], reason[0].code);
    });

    if (productDto.file_id) {
      await this.mediaService.get(productDto.file_id, token).catch(reason => {
        throw new HttpException(
          reason.response.data.errors,
          reason.response.status,
        );
      });
    }

    return new Response<Product>(
      ResponseStatus.OK,
      await this.productService.create(productDto, entity_id, token),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Put(':id')
  async update(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() productDto: ProductDto,
    @Token() token,
  ) {
    if (productDto.translations.length === 0) {
      throw new ValidationException([
        new ResponseError(2021, 'têm de existir uma tradução pelo menos'),
      ]);
    }
    await this.languageService.validateBody(productDto, token).catch(reason => {
      throw new HttpException(reason[0], reason[0].code);
    });

    if (productDto.file_id) {
      await this.mediaService.get(productDto.file_id, token).catch(reason => {
        throw new HttpException(
          reason.response.data.errors,
          reason.response.status,
        );
      });
    }

    return new Response<Product>(
      ResponseStatus.OK,
      await this.productService.update(id, productDto, entity_id, token),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  async delete(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Token() token,
    @User() user,
  ) {
    return new Response<any>(
      ResponseStatus.OK,
      await this.productService.delete(id, entity_id, user.id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/disponibility')
  async getDisponibility(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
  ) {
    return new Response<Disponibility>(
      ResponseStatus.OK,
      await this.productService.getDisponibility(id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/disponibility')
  async patchDisponibility(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() disponibilityDto: DisponibilityDto,
  ) {
    return new Response<Disponibility>(
      ResponseStatus.OK,
      await this.productService.patchDisponibility(
        id,
        disponibilityDto,
        entity_id,
      ),
    );
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/allergens')
  async getAllergens(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Headers('x-language-id') language_id,
  ) {
    return new Response<Allergen[]>(
      ResponseStatus.OK,
      await this.productService.getAllergens(id, language_id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/allergens')
  async patchAllergens(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() allergens: AllergensPatchDto,
  ) {
    return new Response<Allergen[]>(
      ResponseStatus.OK,
      await this.productService.patchAllergens(
        id,
        allergens.allergens,
        entity_id,
      ),
    );
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "units"',
    type: String,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/nutrients')
  async getNutrients(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Headers('x-language-id') language_id,
    @Query() query,
  ) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'units',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    return new Response<NutrientProduct[]>(
      ResponseStatus.OK,
      await this.productService.getNutrients(id, language_id, relateds, entity_id),
    );
  }
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/nutrients')
  async patchNutrients(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() nutrientsDto: NutrientsPatchDto,
  ) {
    return new Response<NutrientProduct[]>(
      ResponseStatus.OK,
      await this.productService.patchNutrients(
        id,
        nutrientsDto.nutrients,
        entity_id,
      ),
    );
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/complements')
  async getComplements(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Headers('x-language-id') language_id,
  ) {
    return new Response<Complement[]>(
      ResponseStatus.OK,
      await this.productService.getComplements(id, language_id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/complements')
  async patchComplements(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() complementsDto: ComplementsPatchDto,
  ) {
    return new Response<Complement[]>(
      ResponseStatus.OK,
      await this.productService.patchComplements(
        id,
        complementsDto.complements,
        entity_id,
      ),
    );
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/composition')
  async getComposition(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Headers('x-language-id') language_id,
  ) {
    return new Response<Compound[]>(
      ResponseStatus.OK,
      await this.productService.getComposition(id, language_id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/composition')
  async patchComposition(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() compositionDto: CompoundPatchDto,
  ) {
    return new Response<Compound[]>(
      ResponseStatus.OK,
      await this.productService.patchComposition(
        id,
        compositionDto.composition,
        entity_id,
      ),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/prices')
  async getPrices(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
  ) {
    return new Response<PriceVariation[]>(
      ResponseStatus.OK,
      await this.productService.getPrices(id, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Patch(':id/prices')
  async patchPrices(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Body() pricesDto: PricesPatchDto,
  ) {
    return new Response<PriceVariation[]>(
      ResponseStatus.OK,
      await this.productService.patchPrices(id, pricesDto.prices, entity_id),
    );
  }

  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/stocks')
  async getStocks(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
  ) {
    return new Response<Stock[]>(
      ResponseStatus.OK,
      await this.productService.getStocks(id, entity_id),
    );
  }

  @ApiImplicitHeader({
    required: false,
    name: 'X-Language-ID',
    description:
      'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "complements", "composition", "disponibility", "nutrients", "allergens", "prices",' +
      ' "unit"  and "stocks"',
    type: String,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the product',
    name: 'id',
    type: Number,
  })
  @Scopes('products')
  @Get(':id/derivatives')
  async getDerivatives(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Param('id', new ParseIntPipe()) id,
    @Query() query,
    @Headers('x-language-id') language_id,
  ) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(
        relation =>
          relation !== 'complements' &&
          relation !== 'composition' &&
          relation !== 'disponibility' &&
          relation !== 'nutrients' &&
          relation !== 'allergens' &&
          relation !== 'prices' &&
          relation !== 'unit' &&
          relation !== 'stocks',
      );
      if (relationErro)
        throw new ValidationException([
          new ResponseError(
            112,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    return new Response<Product[]>(
      ResponseStatus.OK,
      await this.productService.getDerivatives(
        id,
        relateds,
        language_id,
        entity_id,
      ),
    );
  }
}
