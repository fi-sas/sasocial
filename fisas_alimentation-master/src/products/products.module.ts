import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Allergen } from '../allergens/entities/allergen.entity';
import { Product } from './entities/product.entity';
import { NutrientProduct } from './entities/nutrient-product.entity';
import { ProductTranslation } from './entities/product-translation.entity';
import { PriceVariation } from './entities/price-variation.entity';
import { Complement } from './entities/complement.entity';
import { Disponibility } from './entities/disponibility.entity';
import { Compound } from './entities/compound.entity';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { ComplementTranslation } from './entities/complement-translation.entity';
import { ComplementsController } from './complements.controller';
import { ComplementsService } from './complements.service';
import { AllergensModule } from '../allergens/allergens.module';
import { NutrientsModule } from '../nutrients/nutrients.module';
import { UnitsModule } from '../units/units.module';
import { Stock } from '../stocks/entities/stock.entity';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    TypeOrmModule.forFeature([
      ProductTranslation,
      Product,
      PriceVariation,
      Complement,
      ComplementTranslation,
      Disponibility,
      Compound,
      NutrientProduct,
      Allergen,
      Stock
    ]),
    AllergensModule,
    NutrientsModule,
    UnitsModule,
  ],
  controllers: [
    ProductsController,
    ComplementsController,
  ],
  providers: [
    ProductsService,
    ComplementsService,
  ],
  exports: [
    ProductsService,
    ComplementsService
  ]
})
export class ProductsModule {}