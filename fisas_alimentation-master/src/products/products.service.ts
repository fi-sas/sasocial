import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Product } from './entities/product.entity';
import {
  CompoundDto,
  NutrientProductDto,
  PriceVariationDto,
  ProductDto,
} from './dtos/product.dto';
import { Disponibility } from './entities/disponibility.entity';
import { ProductTranslation } from './entities/product-translation.entity';
import { NutrientProduct } from './entities/nutrient-product.entity';
import { Compound } from './entities/compound.entity';
import { PriceVariation } from './entities/price-variation.entity';
import { ResponseError } from '../common/response.model';
import { Nutrient } from '../nutrients/entities/nutrient.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { Unit } from '../units/entities/unit.entity';
import { Allergen } from '../allergens/entities/allergen.entity';
import { Complement } from './entities/complement.entity';
import { Stock } from '../stocks/entities/stock.entity';
import { DisponibilityDto } from './dtos/disponibility.dto';
import { UnitsService } from '../units/units.service';
import { NutrientsService } from '../nutrients/nutrients.service';
import { AllergensService } from '../allergens/allergens.service';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { MediasService } from '../common/services/medias.service';
import { ServiceType } from '../common/enums/service-type.enum';
import { TaxesService } from '../common/services/taxes.service';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepo: Repository<Product>,
    @InjectRepository(Disponibility)
    private readonly disponibilityRepo: Repository<Disponibility>,
    @InjectRepository(ProductTranslation)
    private readonly productTranslationRepo: Repository<ProductTranslation>,
    @InjectRepository(NutrientProduct)
    private readonly productNutrientRepo: Repository<NutrientProduct>,
    @InjectRepository(Compound)
    private readonly productCompoundRepo: Repository<Compound>,
    @InjectRepository(PriceVariation)
    private readonly productPriceVariationRepo: Repository<PriceVariation>,
    @InjectRepository(Complement)
    private readonly complementRepo: Repository<Complement>,
    @InjectRepository(Allergen)
    private readonly allergenRepo: Repository<Allergen>,
    @InjectRepository(Stock) private readonly stockRepo: Repository<Stock>,
    private readonly unitsService: UnitsService,
    private readonly nutrientsService: NutrientsService,
    private readonly allergensService: AllergensService,
    private readonly taxesService: TaxesService,
    private readonly mediaService: MediasService,
  ) {}

  public async findAll(
    offset: number,
    limit: number,
    sort: string,
    relateds: string[],
    name: string,
    type: ServiceType,
    language_id: number,
    entity_id: number,
    token: string,
  ): Promise<[Product[], number]> {
    const productsQB = this.productRepo.createQueryBuilder('product');

    if (limit > 0) {
      productsQB.take(limit);
      productsQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      productsQB.orderBy(
        'product.' + sort_replaced,
        sort_replaced !== sort ? 'DESC' : 'ASC',
      );
    }

    productsQB.leftJoinAndSelect(
      'product.translations',
      'product_translation',
      language_id ? 'product_translation.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'complements')) {
      productsQB.leftJoinAndSelect('product.complements', 'complements');
      productsQB.leftJoinAndSelect(
        'complements.translations',
        'complement_translation',
        language_id ? 'complement_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'composition')) {
      productsQB.leftJoinAndSelect('product.composition', 'composition');
      productsQB.leftJoinAndSelect('composition.compound', 'compound');
      productsQB.leftJoinAndSelect(
        'compound.translations',
        'compound_translation',
        language_id ? 'compound_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'disponibility')) {
      productsQB.leftJoinAndSelect('product.disponibility', 'disponibility');
    }

    if (relateds.find(related => related === 'allergens')) {
      productsQB.leftJoinAndSelect('product.allergens', 'allergens');
      productsQB.leftJoinAndSelect(
        'allergens.translations',
        'allergen_translation',
        language_id ? 'allergen_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'nutrients')) {
      productsQB.leftJoinAndSelect('product.nutrients', 'nutrients');
      productsQB.leftJoinAndSelect('nutrients.nutrient', 'nutrient');
      productsQB.leftJoinAndSelect(
        'nutrient.translations',
        'nutrient_translation',
        language_id ? 'nutrient_translation.language_id = :language_id' : '',
        { language_id },
      );

      if (relateds.find(related => related === 'unit')) {
        productsQB.leftJoinAndSelect('nutrient.unit', 'nutrient_unit');
        productsQB.leftJoinAndSelect(
          'nutrient_unit.translations',
          'nutrient_unit_translation',
          language_id
            ? 'nutrient_unit_translation.language_id = :language_id'
            : '',
          { language_id },
        );
      }
    }

    /*if (relateds.find(related => related === 'taxes')) {
      productsQB.leftJoinAndSelect('product.tax_cost_price', 'tax_cost_price');
      productsQB.leftJoinAndSelect('product.tax_price', 'tax_price');
    }*/

    if (relateds.find(related => related === 'prices')) {
      productsQB.leftJoinAndSelect('product.prices', 'prices');
    }

    if (relateds.find(related => related === 'unit')) {
      productsQB.leftJoinAndSelect('product.unit', 'unit');
      productsQB.leftJoinAndSelect(
        'unit.translations',
        'unit_translation',
        language_id ? 'unit_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'stocks')) {
      productsQB.leftJoinAndSelect('product.stocks', 'stocks');
    }

    productsQB.where(
      'product.fentityId = :entity_id AND product.is_deleted = false',
      { entity_id },
    );

    if (name) {
      productsQB.andWhere('product_translation.name LIKE :name', {
        name: '%' + name + '%',
      });
    }

    if (type) {
      productsQB.andWhere("product.type = :type OR product.type = ''", {
        type,
      });
    }

    const entities: any = await productsQB.getManyAndCount();

    if (relateds.find(related => related === 'derivatives')) {
      for (const entity of entities[0]) {
        if (entity.show_derivatives === true)
          entity.derivatives = await this.getDerivatives(
            entity.id,
            relateds,
            language_id,
            entity_id,
          );
      }
    }

    if (relateds.find(related => related === 'composition')) {
      for (const entity of entities[0]) {
        if (entity.composition !== undefined)
          entity.composition =
            entity.composition[0].compound === null ? [] : entity.composition;
      }
    }

    if (relateds.find(related => related === 'files')) {
      await this.mediaService.loadFileRelateds(
        entities[0],
        'file_id',
        'file',
        token,
      );
    }

    return entities;
  }

  public async findById(
    id: number,
    relateds: string[],
    language_id: number,
    entity_id: number,
    token: string,
  ): Promise<Product> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Product not found');
    }

    const productQB = this.productRepo.createQueryBuilder('product');

    productQB.leftJoinAndSelect(
      'product.translations',
      'product_translation',
      language_id ? 'product_translation.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'complements')) {
      productQB.leftJoinAndSelect('product.complements', 'complements');
      productQB.leftJoinAndSelect(
        'complements.translations',
        'complement_translation',
        language_id ? 'complement_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'composition')) {
      productQB.leftJoinAndSelect('product.composition', 'composition');
      productQB.leftJoinAndSelect('composition.compound', 'compound');
      productQB.leftJoinAndSelect(
        'compound.translations',
        'compound_translation',
        language_id ? 'compound_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'disponibility')) {
      productQB.leftJoinAndSelect('product.disponibility', 'disponibility');
    }

    if (relateds.find(related => related === 'allergens')) {
      productQB.leftJoinAndSelect('product.allergens', 'allergens');
      productQB.leftJoinAndSelect(
        'allergens.translations',
        'allergen_translation',
        language_id ? 'allergen_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'nutrients')) {
      productQB.leftJoinAndSelect('product.nutrients', 'nutrients');
      productQB.leftJoinAndSelect('nutrients.nutrient', 'nutrient');
      productQB.leftJoinAndSelect(
        'nutrient.translations',
        'nutrient_translation',
        language_id ? 'nutrient_translation.language_id = :language_id' : '',
        { language_id },
      );

      if (relateds.find(related => related === 'unit')) {
        productQB.leftJoinAndSelect('nutrient.unit', 'nutrient_unit');
        productQB.leftJoinAndSelect(
          'nutrient_unit.translations',
          'nutrient_unit_translation',
          language_id
            ? 'nutrient_unit_translation.language_id = :language_id'
            : '',
          { language_id },
        );
      }
    }

    /*if (relateds.find(related => related === 'taxes')) {
      productQB.leftJoinAndSelect('product.tax_cost_price', 'tax_cost_price');
      productQB.leftJoinAndSelect('product.tax_price', 'tax_price');
    }*/

    if (relateds.find(related => related === 'prices')) {
      productQB.leftJoinAndSelect('product.prices', 'prices');
      /*if (relateds.find(related => related === 'taxes')) {
        productQB.leftJoinAndSelect('prices.tax', 'prices_tax');
      }*/
    }

    if (relateds.find(related => related === 'unit')) {
      productQB.leftJoinAndSelect('product.unit', 'unit');
      productQB.leftJoinAndSelect(
        'unit.translations',
        'unit_translation',
        language_id ? 'unit_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'stocks')) {
      productQB.leftJoinAndSelect('product.stocks', 'stocks');
    }

    productQB.where('product.id = :id', { id });

    const entity: any = await productQB.getOne();

    if (relateds.find(related => related === 'derivatives')) {
      if (entity.show_derivatives === true)
        entity.derivatives = await this.getDerivatives(
          entity.id,
          relateds,
          language_id,
          entity_id,
        );
    }

    if (relateds.find(related => related === 'composition')) {
      if (entity.composition !== undefined)
        entity.composition =
          entity.composition[0].compound === null ? [] : entity.composition;
    }

    if (relateds.find(related => related === 'files') && entity.file_id !== null) {
      await this.mediaService.loadFileRelated(entity, 'file_id', 'file', token);
    }

    return entity;
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
  }

  public async existIDs(ids: number[], entity_id: number): Promise<number> {
    if (ids.length) {
      return await this.productRepo.count({
        id: In(ids),
        fentity: { id: entity_id },
        is_deleted: false,
      });
    } else {
      return 0;
    }
  }

  public async create(
    productDto: ProductDto,
    entity_id: number,
    token: string,
  ): Promise<Product> {
    const unitCount = await this.unitsService.existID(productDto.unit_id);
    if (unitCount === 0) {
      throw new NotFoundException(404, 'the unit id sent doesnt exist');
    }

    /*const taxPriceCount = await this.taxesService.existID(productDto.tax_price_id, entity_id);
    if (taxPriceCount === 0) {
      throw new NotFoundException(404, 'the tax price id sent doesnt exist');
    }
    const taxPriceCostCount = await this.taxesService.existID(productDto.tax_cost_price_id, entity_id);
    if (taxPriceCostCount === 0) {
      throw new NotFoundException(404, 'the tax cost price id sent doesnt exist');
    }*/

    if (productDto.composition) {
      const compounds_id = productDto.composition.map(cmp => cmp.compound_id);
      const existsCompounds = await this.existIDs(compounds_id, entity_id);
      if (existsCompounds < compounds_id.length) {
        throw new NotFoundException(404, 'the compound id sent doesnt exist');
      }
    }

    if (productDto.nutrients) {
      const nutrients = productDto.nutrients.map(nutr => nutr.nutrient_id);
      const existsNutrients = await this.nutrientsService.existIDs(nutrients);
      if (existsNutrients < nutrients.length) {
        throw new NotFoundException(404, 'the nutrient id sent doesnt exist');
      }
    }

    if (productDto.allergens) {
      const existsAllergens = await this.allergensService.existIDs(
        productDto.allergens,
      );
      if (existsAllergens < productDto.allergens.length) {
        throw new NotFoundException(404, 'the allergen id sent doesnt exist');
      }
    }

    if (productDto.complements) {
      if (productDto.complements.length) {
        const existsComplements = await this.complementRepo.count({
          id: In(productDto.complements),
        });
        if (existsComplements < productDto.complements.length) {
          throw new NotFoundException(
            404,
            'the complement id sent doesnt exist',
          );
        }
      }
    }

    /*const pricesTaxesIds = productDto.prices.map(price => price.tax_id);
    const existsTax_ids = await this.taxesService.existIDs(pricesTaxesIds);
    if (existsTax_ids < pricesTaxesIds.length) {
      throw new NotFoundException(404, 'the tax id sent from prices array doesnt exist');
    }*/

    const codeCount = await this.productRepo.count({ code: productDto.code });
    if (codeCount > 0) {
      throw new HttpException(
        new ResponseError(1001, 'The product code already exists'),
        HttpStatus.BAD_REQUEST,
      );
    }

    const tfe = new FiscalEntity();
    tfe.id = entity_id;

    const tunit = new Unit();
    tunit.id = productDto.unit_id;

    return new Promise<Product>(async (resolve, reject) => {
      let id: number = 0;
      await this.productRepo.manager
        .transaction(async entityManger => {
          // SAVE PRODUCT
          const insertResult = await entityManger
            .createQueryBuilder()
            .insert()
            .into(Product)
            .values({
              unit: tunit,
              code: productDto.code,
              tax_cost_price_id: productDto.tax_cost_price_id,
              tax_price_id: productDto.tax_price_id,
              file_id: productDto.file_id,
              price: productDto.price,
              available: productDto.available,
              visible: productDto.visible,
              show_derivatives: productDto.show_derivatives,
              minimal_stock: productDto.minimal_stock,
              track_stock: productDto.track_stock,
              type: productDto.type,
              fentity: tfe,
              active: productDto.active,
            })
            .execute();

          id = insertResult.identifiers[0].id;
          const tproduct: Product = new Product();
          tproduct.id = id;

          // SAVE DISPONIBILITY
          await entityManger
            .createQueryBuilder()
            .insert()
            .into(Disponibility)
            .values({
              product: tproduct,
              begin_date: productDto.disponibility.begin_date
                ? new Date(productDto.disponibility.begin_date)
                : null,
              end_date: productDto.disponibility.end_date
                ? new Date(productDto.disponibility.end_date)
                : null,
              monday: productDto.disponibility.monday,
              tuesday: productDto.disponibility.tuesday,
              wednesday: productDto.disponibility.wednesday,
              thursday: productDto.disponibility.thursday,
              friday: productDto.disponibility.friday,
              saturday: productDto.disponibility.saturday,
              sunday: productDto.disponibility.sunday,
              minimum_hour: productDto.disponibility.minimum_hour
                ? new Date(productDto.disponibility.minimum_hour)
                : null,
              maximum_hour: productDto.disponibility.maximum_hour
                ? new Date(productDto.disponibility.maximum_hour)
                : null,
              annulment_maximum_hour: productDto.disponibility.annulment_maximum_hour
                ? new Date(productDto.disponibility.annulment_maximum_hour)
                : null,
            })
            .execute();

          // CREATE TRANSLATIONS
          for (const translation of productDto.translations) {
            const tpt: ProductTranslation = new ProductTranslation();
            tpt.language_id = translation.language_id;
            tpt.product = tproduct;
            tpt.name = translation.name;
            tpt.description = translation.description;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(ProductTranslation)
              .values(tpt)
              .execute();
          }

          if (productDto.composition) {
            // CREATE COMPOUNDS
            for (const compound of productDto.composition) {
              const tcp: Product = new Product();
              tcp.id = compound.compound_id;

              const tc: Compound = new Compound();
              tc.product = tproduct;
              tc.quantity = compound.quantity;
              tc.compound = tcp;

              await entityManger
                .createQueryBuilder()
                .insert()
                .into(Compound)
                .values(tc)
                .execute();
            }
          }

          if (productDto.nutrients) {
            // CREATE NUTRIENTS
            for (const nutrient of productDto.nutrients) {
              const tn: Nutrient = new Nutrient();
              tn.id = nutrient.nutrient_id;

              const tnp: NutrientProduct = new NutrientProduct();
              tnp.product = tproduct;
              tnp.nutrient = tn;
              tnp.quantity = nutrient.quantity;

              await entityManger
                .createQueryBuilder()
                .insert()
                .into(NutrientProduct)
                .values(tnp)
                .execute();
            }
          }

          if (productDto.allergens) {
            // CREATE ALLERGENS
            for (const allergen of productDto.allergens) {
              await entityManger
                .createQueryBuilder()
                .insert()
                .into('product_allergens_allergen')
                .values({
                  productId: id,
                  allergenId: allergen,
                })
                .execute();
            }
          }

          if (productDto.complements) {
            // CREATE COMPLEMENTS
            for (const complement of productDto.complements) {
              await entityManger
                .createQueryBuilder()
                .insert()
                .into('product_complements_complement')
                .values({
                  productId: id,
                  complementId: complement,
                })
                .execute();
            }
          }

          if (productDto.prices) {
            // CREATE PRICE VARIATIONS
            for (const price of productDto.prices) {
              const tpv: PriceVariation = new PriceVariation();
              tpv.product = tproduct;
              tpv.profile_id = price.profile_id;
              tpv.time = price.time ? new Date(price.time) : null;
              tpv.price = price.price;
              tpv.description = price.description;
              tpv.tax_id = price.tax_id;
              tpv.meal = null;
              await entityManger
                .createQueryBuilder()
                .insert()
                .into(PriceVariation)
                .values(tpv)
                .execute();
            }
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(
          await this.findById(
            id,
            [
              'complements',
              'composition',
              'disponibility',
              'nutrients',
              'allergens',
              'files',
              'prices',
              'unit',
              'taxes',
              'derivatives',
              'stocks',
            ],
            null,
            entity_id,
            token,
          ),
        );
      } catch (e) {
        reject(e);
      }
    });
  }

  public async update(
    id: number,
    productDto: ProductDto,
    entity_id: number,
    token: string,
  ): Promise<Product> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const unitCount = await this.unitsService.existID(productDto.unit_id);
    if (unitCount === 0) {
      throw new NotFoundException(404, 'the unit id sent doesnt exist');
    }

    /*const taxPriceCount = await this.taxesService.existID(productDto.tax_price_id, entity_id);
    if (taxPriceCount === 0) {
      throw new NotFoundException(404, 'the tax price id sent doesnt exist');
    }
    const taxPriceCostCount = await this.taxesService.existID(productDto.tax_cost_price_id, entity_id);
    if (taxPriceCostCount === 0) {
      throw new NotFoundException(404, 'the tax cost price id sent doesnt exist');
    }*/

    if (productDto.composition) {
      const compounds_id = productDto.composition.map(cmp => cmp.compound_id);
      const existsCompounds = await this.existIDs(compounds_id, entity_id);
      if (existsCompounds < compounds_id.length) {
        throw new NotFoundException(404, 'the compound id sent doesnt exist');
      }
    }

    if (productDto.nutrients) {
      const nutrients = productDto.nutrients.map(nutr => nutr.nutrient_id);
      const existsNutrients = await this.nutrientsService.existIDs(nutrients);
      if (existsNutrients < nutrients.length) {
        throw new NotFoundException(404, 'the nutrient id sent doesnt exist');
      }
    }

    if (productDto.allergens) {
      const existsAllergens = await this.allergensService.existIDs(
        productDto.allergens,
      );
      if (existsAllergens < productDto.allergens.length) {
        throw new NotFoundException(404, 'the allergen id sent doesnt exist');
      }
    }

    if (productDto.complements) {
      if (productDto.complements.length) {
        const existsComplements = await this.complementRepo.count({
          id: In(productDto.complements),
        });
        if (existsComplements < productDto.complements.length) {
          throw new NotFoundException(
            404,
            'the complement id sent doesnt exist',
          );
        }
      }
    }

    /*const pricesTaxesIds = productDto.prices.map(price => price.tax_id);
    const existsTax_ids = await this.taxesService.existIDs(pricesTaxesIds);
    if (existsTax_ids < pricesTaxesIds.length) {
      throw new NotFoundException(404, 'the tax id sent from prices array doesnt exist');
    }*/

    const tunit = new Unit();
    tunit.id = productDto.unit_id;

    return new Promise<Product>(async (resolve, reject) => {
      await this.productRepo.manager
        .transaction(async entityManger => {
          // SAVE PRODUCT
          await entityManger
            .createQueryBuilder()
            .update(Product)
            .set({
              unit: tunit,
              tax_cost_price_id: productDto.tax_price_id,
              tax_price_id: productDto.tax_price_id,
              price: productDto.price,
              file_id: productDto.file_id,
              available: productDto.available,
              visible: productDto.visible,
              show_derivatives: productDto.show_derivatives,
              minimal_stock: productDto.minimal_stock,
              track_stock: productDto.track_stock,
              type: productDto.type,
              active: productDto.active,
            })
            .where(' id = :id ', { id })
            .execute();

          const tproduct: Product = new Product();
          tproduct.id = id;

          // UPDATE DISPONIBILITY
          await entityManger
            .createQueryBuilder()
            .update(Disponibility)
            .set({
              begin_date: productDto.disponibility.begin_date
                ? new Date(productDto.disponibility.begin_date)
                : null,
              end_date: productDto.disponibility.end_date
                ? new Date(productDto.disponibility.end_date)
                : null,
              monday: productDto.disponibility.monday,
              tuesday: productDto.disponibility.tuesday,
              wednesday: productDto.disponibility.wednesday,
              thursday: productDto.disponibility.thursday,
              friday: productDto.disponibility.friday,
              saturday: productDto.disponibility.saturday,
              sunday: productDto.disponibility.sunday,
              minimum_hour: productDto.disponibility.minimum_hour
                ? new Date(productDto.disponibility.minimum_hour)
                : null,
              maximum_hour: productDto.disponibility.maximum_hour
                ? new Date(productDto.disponibility.maximum_hour)
                : null,
              annulment_maximum_hour: productDto.disponibility.annulment_maximum_hour
                ? new Date(productDto.disponibility.annulment_maximum_hour)
                : null,
            })
            .where('productId = :id', { id })
            .execute();

          // CREATE TRANSLATIONS
          await entityManger
            .createQueryBuilder()
            .delete()
            .from(ProductTranslation)
            .where('productId = :id ', { id })
            .execute();
          for (const translation of productDto.translations) {
            const tpt: ProductTranslation = new ProductTranslation();
            tpt.language_id = translation.language_id;
            tpt.product = tproduct;
            tpt.name = translation.name;
            tpt.description = translation.description;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(ProductTranslation)
              .values(tpt)
              .execute();
          }

          // CREATE COMPOUNDS
          if (productDto.composition) {
            await entityManger
              .createQueryBuilder()
              .delete()
              .from(Compound)
              .where('productId = :id ', { id })
              .execute();
            for (const compound of productDto.composition) {
              const tcp: Product = new Product();
              tcp.id = compound.compound_id;

              const tc: Compound = new Compound();
              tc.product = tproduct;
              tc.quantity = compound.quantity;
              tc.compound = tcp;

              await entityManger
                .createQueryBuilder()
                .insert()
                .into(Compound)
                .values(tc)
                .execute();
            }
          }

          if (productDto.nutrients) {
            // CREATE NUTRIENTS
            await entityManger
              .createQueryBuilder()
              .delete()
              .from(NutrientProduct)
              .where('productId = :id ', { id })
              .execute();
            for (const nutrient of productDto.nutrients) {
              const tn: Nutrient = new Nutrient();
              tn.id = nutrient.nutrient_id;

              const tnp: NutrientProduct = new NutrientProduct();
              tnp.product = tproduct;
              tnp.nutrient = tn;
              tnp.quantity = nutrient.quantity;

              await entityManger
                .createQueryBuilder()
                .insert()
                .into(NutrientProduct)
                .values(tnp)
                .execute();
            }
          }
          if (productDto.allergens) {
            // CREATE ALLERGENS
            await entityManger
              .createQueryBuilder()
              .delete()
              .from('product_allergens_allergen')
              .where('productId = :id ', { id })
              .execute();
            for (const allergen of productDto.allergens) {
              await entityManger
                .createQueryBuilder()
                .insert()
                .into('product_allergens_allergen')
                .values({
                  productId: id,
                  allergenId: allergen,
                })
                .execute();
            }
          }

          if (productDto.complements) {
            // CREATE COMPLEMENTS
            await entityManger
              .createQueryBuilder()
              .delete()
              .from('product_complements_complement')
              .where('productId = :id ', { id })
              .execute();
            for (const complement of productDto.complements) {
              await entityManger
                .createQueryBuilder()
                .insert()
                .into('product_complements_complement')
                .values({
                  productId: id,
                  complementId: complement,
                })
                .execute();
            }
          }

          if (productDto.prices) {
            // CREATE PRICE VARIATIONS
            await entityManger
              .createQueryBuilder()
              .delete()
              .from(PriceVariation)
              .where('productId = :id ', { id })
              .execute();
            for (const price of productDto.prices) {
              const tpv: PriceVariation = new PriceVariation();
              tpv.product = tproduct;
              tpv.profile_id = price.profile_id;
              tpv.time = price.time ? new Date(price.time) : null;
              tpv.price = price.price;
              tpv.description = price.description;
              tpv.tax_id = price.tax_id;
              tpv.meal = null;
              await entityManger
                .createQueryBuilder()
                .insert()
                .into(PriceVariation)
                .values(tpv)
                .execute();
            }
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(
          await this.findById(
            id,
            [
              'complements',
              'composition',
              'disponibility',
              'nutrients',
              'allergens',
              'files',
              'prices',
              'unit',
              'taxes',
              'derivatives',
              'stocks',
            ],
            null,
            entity_id,
            token,
          ),
        );
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getDisponibility(
    id: number,
    entity_id: number,
  ): Promise<Disponibility> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.disponibilityRepo.findOne(id);
  }

  public async patchDisponibility(
    id: number,
    disponibilityDto: DisponibilityDto,
    entity_id: number,
  ): Promise<Disponibility> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }
    const tproduct: Product = new Product();
    tproduct.id = id;

    await this.disponibilityRepo
      .createQueryBuilder()
      .update()
      .set({
        begin_date: new Date(disponibilityDto.begin_date),
        end_date: new Date(disponibilityDto.end_date),
        monday: disponibilityDto.monday,
        tuesday: disponibilityDto.tuesday,
        wednesday: disponibilityDto.wednesday,
        thursday: disponibilityDto.thursday,
        friday: disponibilityDto.friday,
        saturday: disponibilityDto.saturday,
        sunday: disponibilityDto.sunday,
        minimum_hour: disponibilityDto.minimum_hour
                ? new Date(disponibilityDto.minimum_hour)
                : null,
        maximum_hour: disponibilityDto.maximum_hour
                ? new Date(disponibilityDto.maximum_hour)
                : null,
        annulment_maximum_hour: disponibilityDto.annulment_maximum_hour
          ? new Date(disponibilityDto.annulment_maximum_hour)
          : null,
      })
      .where('productId = :id', { id })
      .execute();

    return this.getDisponibility(id, entity_id);
  }

  public async getAllergens(
    id: number,
    language_id: number,
    entity_id: number,
  ): Promise<Allergen[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.allergenRepo
      .createQueryBuilder('allergen')
      .select()
      .leftJoinAndSelect(
        'allergen.translations',
        'allergen_translation',
        language_id ? 'allergen_translation.language_id = :language_id' : '',
        { language_id },
      )
      .leftJoin(
        'product_allergens_allergen',
        'pa',
        'pa.allergenId = allergen.id',
      )
      .where('pa.productId = :id', { id })
      .getMany();
  }

  public async patchAllergens(
    id: number,
    allergens: number[],
    entity_id: number,
  ): Promise<Allergen[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const existsAllergens = await this.allergensService.existIDs(allergens);
    if (existsAllergens < allergens.length) {
      throw new NotFoundException(404, 'the allergen id sent doesnt exist');
    }

    return new Promise<Allergen[]>(async (resolve, reject) => {
      await this.allergenRepo.manager
        .transaction(async entityManger => {
          await entityManger
            .createQueryBuilder()
            .delete()
            .from('product_allergens_allergen')
            .where('productId = :id ', { id })
            .execute();
          for (const allergen of allergens) {
            await entityManger
              .createQueryBuilder()
              .insert()
              .into('product_allergens_allergen')
              .values({
                productId: id,
                allergenId: allergen,
              })
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getAllergens(id, null, entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getNutrients(
    id: number,
    language_id: number,
    relateds: string[],
    entity_id: number,
  ): Promise<NutrientProduct[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const nutrientsQB = this.productNutrientRepo
    .createQueryBuilder('nutrientProduct')
    .select()
    .leftJoinAndSelect('nutrientProduct.nutrient', 'nutrient')
    .leftJoinAndSelect(
      'nutrient.translations',
      'nutrient_translations',
      language_id ? 'nutrient_translations.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'units')) {
      nutrientsQB.leftJoinAndSelect('nutrient.unit', 'unit');
    }

    nutrientsQB.where('nutrientProduct.product.id = :id', { id });
    return await nutrientsQB.getMany();
  }

  public async patchNutrients(
    id: number,
    nutrients: NutrientProductDto[],
    entity_id: number,
  ): Promise<NutrientProduct[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const nutrients_ids = nutrients.map(nutr => nutr.nutrient_id);
    const existsNutrients = await this.nutrientsService.existIDs(nutrients_ids);
    if (existsNutrients < nutrients_ids.length) {
      throw new NotFoundException(404, 'the nutrient id sent doesnt exist');
    }

    const tproduct: Product = new Product();
    tproduct.id = id;

    return new Promise<NutrientProduct[]>(async (resolve, reject) => {
      await this.productNutrientRepo.manager
        .transaction(async entityManger => {
          await entityManger
            .createQueryBuilder()
            .delete()
            .from(NutrientProduct)
            .where('productId = :id ', { id })
            .execute();

          for (const nutrient of nutrients) {
            const tn: Nutrient = new Nutrient();
            tn.id = nutrient.nutrient_id;

            const tnp: NutrientProduct = new NutrientProduct();
            tnp.product = tproduct;
            tnp.nutrient = tn;
            tnp.quantity = nutrient.quantity;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(NutrientProduct)
              .values(tnp)
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getNutrients(id, null, ['units'], entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getComplements(
    id: number,
    language_id: number,
    entity_id: number,
  ): Promise<Complement[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.complementRepo
      .createQueryBuilder('complement')
      .select()
      .leftJoinAndSelect(
        'complement.translations',
        'complement_translations',
        language_id ? 'complement_translations.language_id = :language_id' : '',
        { language_id },
      )
      .leftJoin('complement.products', 'product')
      .where('product.id = :id', { id })
      .getMany();
  }

  public async patchComplements(
    id: number,
    complements: number[],
    entity_id: number,
  ): Promise<Complement[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    if (complements.length) {
      const existsComplements = await this.complementRepo.count({
        id: In(complements),
        fentity: { id: entity_id },
      });
      if (existsComplements < complements.length) {
        throw new NotFoundException(404, 'the complement id sent doesnt exist');
      }
    }

    return new Promise<Complement[]>(async (resolve, reject) => {
      await this.complementRepo.manager
        .transaction(async entityManger => {
          await entityManger
            .createQueryBuilder()
            .delete()
            .from('product_complements_complement')
            .where('productId = :id ', { id })
            .execute();
          for (const complement of complements) {
            await entityManger
              .createQueryBuilder()
              .insert()
              .into('product_complements_complement')
              .values({
                productId: id,
                complementId: complement,
              })
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getComplements(id, null, entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getComposition(
    id: number,
    language_id: number,
    entity_id: number,
  ): Promise<Compound[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.productCompoundRepo
      .createQueryBuilder('composition')
      .select()
      .leftJoinAndSelect('composition.compound', 'compound')
      .leftJoinAndSelect(
        'compound.translations',
        'compound_translation',
        language_id ? 'compound_translation.language_id = :language_id' : '',
        { language_id },
      )
      .where('composition.productId = :id', { id })
      .getMany();
  }

  public async patchComposition(
    id: number,
    composition: CompoundDto[],
    entity_id: number,
  ): Promise<Compound[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const compounds_id = composition.map(cmp => cmp.compound_id);
    const existsCompounds = await this.existIDs(compounds_id, entity_id);
    if (existsCompounds < compounds_id.length) {
      throw new NotFoundException(404, 'the compound id sent doesnt exist');
    }

    const tproduct: Product = new Product();
    tproduct.id = id;

    return new Promise<Compound[]>(async (resolve, reject) => {
      await this.productCompoundRepo.manager
        .transaction(async entityManger => {
          await entityManger
            .createQueryBuilder()
            .delete()
            .from(Compound)
            .where('productId = :id ', { id })
            .execute();
          for (const compound of composition) {
            const tcp: Product = new Product();
            tcp.id = compound.compound_id;

            const tc: Compound = new Compound();
            tc.product = tproduct;
            tc.quantity = compound.quantity;
            tc.compound = tcp;

            await entityManger
              .createQueryBuilder()
              .insert()
              .into(Compound)
              .values(tc)
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getComposition(id, null, entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getPrices(
    id: number,
    entity_id: number,
  ): Promise<PriceVariation[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.productPriceVariationRepo
      .createQueryBuilder('price')
      .select()
      .where('price.productId = :id', { id })
      .getMany();
  }

  public async patchPrices(
    id: number,
    prices: PriceVariationDto[],
    entity_id: number,
  ): Promise<PriceVariation[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    /*const pricesTaxesIds = prices.map(price => price.tax_id);
    const existsTax_ids = await this.taxesService.existIDs(pricesTaxesIds);
    if (existsTax_ids < pricesTaxesIds.length) {
      throw new NotFoundException(404, 'the tax id sent from prices array doesnt exist');
    }*/

    const tproduct: Product = new Product();
    tproduct.id = id;

    return new Promise<PriceVariation[]>(async (resolve, reject) => {
      await this.productPriceVariationRepo.manager
        .transaction(async entityManger => {
          // CREATE PRICE VARIATIONS
          await entityManger
            .createQueryBuilder()
            .delete()
            .from(PriceVariation)
            .where('productId = :id ', { id })
            .execute();
          for (const price of prices) {
            const tpv: PriceVariation = new PriceVariation();
            tpv.product = tproduct;
            tpv.profile_id = price.profile_id;
            tpv.time = price.time ? new Date(price.time) : null;
            tpv.price = price.price;
            tpv.description = price.description;
            tpv.tax_id = price.tax_id;
            await entityManger
              .createQueryBuilder()
              .insert()
              .into(PriceVariation)
              .values(tpv)
              .execute();
          }
        })
        .catch(reason => {
          reject(reason);
        });
      try {
        resolve(await this.getPrices(id, entity_id));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async getStocks(id: number, entity_id: number): Promise<Stock[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    return await this.stockRepo
      .createQueryBuilder('stock')
      .select()
      .where('stock.productId = :id', { id })
      .getMany();
  }

  public async getDerivatives(
    id: number,
    relateds: string[],
    language_id: number,
    entity_id: number,
  ): Promise<Product[]> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    const derivativesQB = this.productCompoundRepo.createQueryBuilder(
      'composition',
    );
    derivativesQB.leftJoinAndSelect('composition.product', 'product');
    derivativesQB.leftJoinAndSelect(
      'product.translations',
      'product_translation',
      language_id ? 'product_translation.language_id = :language_id' : '',
      { language_id },
    );

    if (relateds.find(related => related === 'complements')) {
      derivativesQB.leftJoinAndSelect('product.complements', 'complements');
      derivativesQB.leftJoinAndSelect(
        'complements.translations',
        'complement_translation',
        language_id ? 'complement_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'composition')) {
      derivativesQB.leftJoinAndSelect('product.composition', 'composition1');
      derivativesQB.leftJoinAndSelect('composition1.compound', 'compound1');
      derivativesQB.leftJoinAndSelect(
        'compound1.translations',
        'compound_translation',
        language_id ? 'compound_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'disponibility')) {
      derivativesQB.leftJoinAndSelect('product.disponibility', 'disponibility');
    }

    if (relateds.find(related => related === 'allergens')) {
      derivativesQB.leftJoinAndSelect('product.allergens', 'allergens');
      derivativesQB.leftJoinAndSelect(
        'allergens.translations',
        'allergen_translation',
        language_id ? 'allergen_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'nutrients')) {
      derivativesQB.leftJoinAndSelect('product.nutrients', 'nutrients');
      derivativesQB.leftJoinAndSelect('nutrients.nutrient', 'nutrient');
      derivativesQB.leftJoinAndSelect(
        'nutrient.translations',
        'nutrient_translation',
        language_id ? 'nutrient_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'prices')) {
      derivativesQB.leftJoinAndSelect('product.prices', 'prices');
    }

    if (relateds.find(related => related === 'unit')) {
      derivativesQB.leftJoinAndSelect('product.unit', 'unit');
      derivativesQB.leftJoinAndSelect(
        'unit.translations',
        'unit_translation',
        language_id ? 'unit_translation.language_id = :language_id' : '',
        { language_id },
      );
    }

    if (relateds.find(related => related === 'stocks')) {
      derivativesQB.leftJoinAndSelect('product.stocks', 'stocks');
    }

    derivativesQB.where('composition.compoundId = :id', { id });

    const prods = await derivativesQB.getMany();
    return prods.map(ent => ent.product);
  }

  async delete(id: any, entity_id: any, user_id: number): Promise<any> {
    const persistedProducts = await this.productRepo.count({
      id,
      fentity: { id: entity_id },
      is_deleted: false,
    });
    if (persistedProducts === 0) {
      throw new NotFoundException(404, 'Produto não encontrado');
    }

    await this.productRepo.update(
      { id },
      { is_deleted: true, delete_at: Date(), delete_by_user_id: user_id },
    );
  }
}
