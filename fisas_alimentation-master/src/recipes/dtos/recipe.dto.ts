import {
  IsArray,
  IsBoolean,
  IsNotEmpty, IsNumber, IsOptional,
  IsString, Min,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class RecipeStepDto {
  @ApiModelProperty()
  @IsNotEmpty({ message: 'file_id nao pode ser vazio'})
  @IsNumber({}, { message: 'file_id e do tipo numero'})
  file_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'step nao pode ser vazio'})
  @IsNumber({}, { message: 'step e do tipo numero'})
  @Min(0, { message: 'o numero minimo de step é 0'})
  step: number;

  @ApiModelProperty()
  @IsOptional()
  @IsString({ message: ' o description e do tipo texto'})
  description: string;
}

export class ProductRecipeDto {

  @ApiModelProperty()
  @IsNotEmpty({ message: 'product_id nao pode ser vazio'})
  @IsNumber({}, { message: 'product_id e do tipo numero'})
  ingredient_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'liquid_quantity nao pode ser vazio'})
  @IsNumber({}, { message: 'liquid_quantity e do tipo numero'})
  @Min(0, { message: 'o minimo de liquid_quantity é 0'})
  liquid_quantity: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'gross_quantity nao pode ser vazio'})
  @IsNumber({}, { message: 'gross_quantity e do tipo numero'})
  @Min(0, { message: 'o minimo de gross_quantity é 0'})
  gross_quantity: number;
}

export class RecipeDto {
  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o nome não pode ser vazio'})
  @IsString({ message: ' o nmeo e do tipo texto'})
  name: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsString({ message: ' a descrição e do tipo texto'})
  description: string;

  @ApiModelProperty( {type: ProductRecipeDto, isArray: true })
  @IsArray({ message: 'os ingrdients e um array'})
  @ValidateNested()
  @Type(() => ProductRecipeDto)
  ingredients: ProductRecipeDto[];

  @ApiModelProperty( {type: RecipeStepDto, isArray: true })
  @IsArray({ message: 'os steps da receita e um array'})
  @ValidateNested()
  @Type(() => RecipeStepDto)
  preparation_steps: RecipeStepDto[];

  @ApiModelProperty()
  @IsNotEmpty({ message: 'number_doses nao pode ser vazio'})
  @IsNumber({}, { message: 'number_doses e do tipo numero'})
  @Min(0, { message: 'o minimo de doses é 0'})
  number_doses: number;

  @ApiModelProperty()
  @IsString({ message: 'preparation é do tipo string'})
  preparation: string;

  @ApiModelProperty()
  @IsString({ message: 'preparation_mode é do tipo string'})
  preparation_mode: string;

  @ApiModelProperty()
  @IsString({ message: 'microbiological_criteria é do tipo string'})
  microbiological_criteria: string;

  @ApiModelProperty()
  @IsString({ message: 'packaging é do tipo string'})
  packaging: string;

  @ApiModelProperty()
  @IsString({ message: 'use é do tipo string'})
  use: string;

  @ApiModelProperty()
  @IsString({ message: 'distribution é do tipo string'})
  distribution: string;

  @ApiModelProperty()
  @IsString({ message: 'comment é do tipo string'})
  comment: string;

  @ApiModelProperty()
  @IsString({ message: 'associated_docs é do tipo string'})
  associated_docs: string;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}