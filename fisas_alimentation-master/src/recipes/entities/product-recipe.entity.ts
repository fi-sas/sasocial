import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Product } from '../../products/entities/product.entity';
import { Recipe } from './recipe.entity';

@Entity()
export class ProductRecipe {
  @ManyToOne(type => Product, product => product.id, { primary: true, cascade: true, onDelete: 'CASCADE'})
  ingredient: Product;

  @PrimaryColumn({ name: 'ingredientId'})
  ingredient_id: number;

  @ManyToOne(type => Recipe, recipe => recipe.ingredients, { primary: true, cascade: true, onDelete: 'CASCADE'})
  recipe: Recipe;

  @Column({ default: 0, nullable: true, type: 'decimal', precision: 10, scale: 2})
  liquid_quantity: number;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  gross_quantity: number;
}