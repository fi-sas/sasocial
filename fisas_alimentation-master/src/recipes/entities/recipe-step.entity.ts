import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { Recipe } from './recipe.entity';
import { MediaDto } from '../../common/entities/media.dto';

@Entity()
export class RecipeStep extends BaseEntity{
  @ManyToOne(type => Recipe, recipe => recipe.preparation_steps, { primary: true, cascade: true, onDelete: 'CASCADE'})
  recipe: Recipe;

  @PrimaryColumn()
  file_id: number;
  file: MediaDto;

  @Column({ nullable: false})
  step: number;

  @Column()
  description: string;
}