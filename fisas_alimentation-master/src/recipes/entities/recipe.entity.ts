import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { ProductRecipe } from './product-recipe.entity';
import { RecipeStep } from './recipe-step.entity';
import { Dish } from '../../dishs/entities/dish.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';

@Entity()
export class Recipe extends BaseEntity {
  @Column({ nullable: false })
  name: string;

  @Column({ type: 'text', nullable: true  })
  description: string;

  @OneToMany(type => ProductRecipe, ingredient => ingredient.recipe)
  @JoinColumn()
  ingredients: ProductRecipe[];

  @Column({ nullable: false, default: 0 })
  number_doses: number;

  @Column({ type: 'text', nullable: true  })
  preparation: string;

  @OneToMany(type => RecipeStep, step => step.recipe)
  @JoinColumn()
  preparation_steps: RecipeStep[];

  @Column({ type: 'text', nullable: true })
  preparation_mode: string;

  @Column({ type: 'text', nullable: true  })
  microbiological_criteria: string;

  @Column({ type: 'text', nullable: true  })
  packaging: string;

  @Column({ type: 'text', nullable: true  })
  use: string;

  @Column({ type: 'text', nullable: true  })
  distribution: string;

  @Column({ type: 'text', nullable: true  })
  comment: string;

  @Column({ type: 'text', nullable: true  })
  associated_docs: string;

  @ManyToMany(type => Dish, dish => dish.recipes)
  dishs: Dish[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}