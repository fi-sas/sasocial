import { Body, Controller, Delete, Get, Headers, HttpCode, HttpException, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitHeader, ApiImplicitQuery } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { RecipesService } from './recipes.service';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { Recipe } from './entities/recipe.entity';
import { RecipeDto } from './dtos/recipe.dto';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { Token } from '../common/decorators/token.decorator';
import { MediasService } from '../common/services/medias.service';
import { ValidationError } from 'class-validator';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Recipes')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/recipes')
export class RecipesController {

  constructor(private recipesService: RecipesService,
              private mediaService: MediasService) {}

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitQuery({ required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number})
  @ApiImplicitQuery({ required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number})
  @ApiImplicitQuery({ required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String})
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "ingredients", "steps", "files"', type: String,
  })
  @Scopes('recipes')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query,
                       @Headers('x-language-id') language_id, @Token() token) {

    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (! query.hasOwnProperty('sort')) query.sort = '-id';
    if (! query.hasOwnProperty('name')) query.name = null;

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'ingredients' &&
        relation !== 'files' &&
        relation !== 'steps');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.recipesService.findAll(query.offset, query.limit, query.sort, query.name, relateds, language_id, entity_id, token);
    return new Response<Recipe[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1]
      }
    );
  }

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "ingredients", "steps", "files"', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the recipe', name: 'id', type: Number })
  @Scopes('recipes')
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                        @Query() query, @Headers('x-language-id') language_id, @Token() token) {
    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'ingredients' &&
        relation !== 'files' &&
        relation !== 'steps');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.recipesService.findByID(id, relateds, language_id, entity_id, token);
    return new Response<Recipe>(ResponseStatus.OK, entity);
  }

  @Scopes('recipes')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: RecipeDto, @Token() token ) {

    for (const step of entity.preparation_steps) {
      await this.mediaService.get(step.file_id, token).catch( reason => {
        throw new HttpException(reason.response.data.errors, reason.response.status);
      });
    }

    const createdEntity = await this.recipesService.create(entity, entity_id, token);
    return new Response<Recipe>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the recipe', name: 'id', type: Number })
  @Scopes('recipes')
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                      @Body() entity: RecipeDto, @Token() token) {

    for (const step of entity.preparation_steps) {
      await this.mediaService.get(step.file_id, token).catch( reason => {
        throw new HttpException(reason.response.data.errors, reason.response.status);
      });
    }

    const updatedEntity = await this.recipesService.update(id, entity, entity_id, token);
    return new Response<Recipe>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the recipe', name: 'id', type: Number })
  @Scopes('recipes')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.recipesService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}