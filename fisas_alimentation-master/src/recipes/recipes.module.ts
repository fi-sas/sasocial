import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Recipe } from './entities/recipe.entity';
import { RecipeStep } from './entities/recipe-step.entity';
import { ProductRecipe } from './entities/product-recipe.entity';
import { Product } from '../products/entities/product.entity';
import { RecipesController } from './recipes.controller';
import { RecipesService } from './recipes.service';
import { ProductsModule } from '../products/products.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    ProductsModule,
    TypeOrmModule.forFeature([
      Recipe,
      RecipeStep,
      ProductRecipe,
      Product
    ])
  ],
  controllers: [
    RecipesController
  ],
  providers: [
    RecipesService
  ],
  exports: [
    RecipesService
  ]
})
export class RecipesModule {}