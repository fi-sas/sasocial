import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Recipe } from './entities/recipe.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { RecipeDto } from './dtos/recipe.dto';
import { Product } from '../products/entities/product.entity';
import { ProductRecipe } from './entities/product-recipe.entity';
import { RecipeStep } from './entities/recipe-step.entity';
import { ProductsService } from '../products/products.service';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { MediasService } from '../common/services/medias.service';
import { ResponseError } from '../common/response.model';
import { ValidationException } from '../common/exceptions/validation.exception';

@Injectable()
export class RecipesService {
  constructor(@InjectRepository(Recipe) private readonly recipeRepo: Repository<Recipe>,
              @InjectRepository(ProductRecipe) private readonly productRecipeRepo: Repository<ProductRecipe>,
              private readonly productsService: ProductsService,
              private readonly mediaService: MediasService
  ) {}

  public async create(entityDto: RecipeDto, entity_id: number, token: string): Promise<Recipe> {
    const products_id = [...new Set(entityDto.ingredients.map( idt => idt.ingredient_id))];

    if (products_id.length < entityDto.ingredients.length) {
      throw new ValidationException([new ResponseError(400, 'Existem ingradientes repetidos')]);
    }


    const existsProduct = await this.productsService.existIDs(products_id, entity_id);
    if (existsProduct < products_id.length) {
      throw new NotFoundException(404, 'the ingredient id sent doesnt exist');
    }

    let id: number = 0;
    const tfe = new FiscalEntity();
    tfe.id = entity_id;

    return new Promise<Recipe>(async (resolve, reject) => {
      await this.recipeRepo.manager.transaction(async entityManger => {
        const resultInsert = await entityManger.createQueryBuilder()
          .insert()
          .into(Recipe)
          .values({
            associated_docs: entityDto.associated_docs,
            comment: entityDto.comment,
            description: entityDto.description,
            distribution: entityDto.distribution,
            microbiological_criteria: entityDto.microbiological_criteria,
            name: entityDto.name,
            number_doses: entityDto.number_doses,
            packaging: entityDto.packaging,
            preparation: entityDto.preparation,
            preparation_mode: entityDto.preparation_mode,
            use: entityDto.use,
            active: entityDto.active,
            fentity: tfe
          })
          .execute();

        id = resultInsert.identifiers[0].id;
        const trecipe: Recipe = new Recipe();
        trecipe.id = id;

        for (const ingredient of entityDto.ingredients) {
          const tproduct: Product = new Product();
          tproduct.id = ingredient.ingredient_id;

          const tpr: ProductRecipe = new ProductRecipe();
          tpr.recipe = trecipe;
          tpr.ingredient = tproduct;
          tpr.gross_quantity = ingredient.gross_quantity;
          tpr.liquid_quantity = ingredient.liquid_quantity;

          await entityManger.createQueryBuilder()
            .insert()
            .into(ProductRecipe)
            .values(tpr)
            .execute();
        }

        for (const step of entityDto.preparation_steps) {
          const tstep: RecipeStep = new RecipeStep();
          tstep.recipe = trecipe;
          tstep.file_id = step.file_id;
          tstep.step = step.step;
          tstep.description = step.description;

          await entityManger.createQueryBuilder()
            .insert()
            .into(RecipeStep)
            .values(tstep)
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['ingredients', 'steps', 'files'], null, entity_id, token));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async findAll(offset: number, limit: number, sort: string, name: string, relateds: string[],
                       language_id: number, entity_id: number, token: string) {
    const recipesQB = this.recipeRepo.createQueryBuilder('recipe');

    if (limit > 0) {
      recipesQB.take(limit);
      recipesQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      recipesQB.orderBy('recipe.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'steps')) {
      recipesQB.leftJoinAndSelect('recipe.preparation_steps', 'step');
    }

    recipesQB.where(' recipe.fentityId = :entity_id', { entity_id });

    if(name) {
      recipesQB.andWhere(`recipe.name LIKE :name`,{ name: '%' + name + '%'});
    }

    const entities = await recipesQB.getManyAndCount();

    if (relateds.find(related => related === 'ingredients')) {
      for (const ent of entities[0]) {
        ent.ingredients = await this.getIngredients(ent.id, language_id);
      }
    }

    if (relateds.find(related => related === 'steps') && relateds.find(related => related === 'steps')) {
      for (const entity of entities[0]) {
        await this.mediaService.loadFileRelateds(entity.preparation_steps, 'file_id', 'file', token);
      }
    }

    return entities;
  }

  public async findByID(id: number, relateds: string[], language_id: number, entity_id: number, token: string): Promise<Recipe> {
    const persistedEntity = await this.recipeRepo.count({ id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const recipeQB = this.recipeRepo.createQueryBuilder('recipe');

    if (relateds.find(related => related === 'steps')) {
      recipeQB.leftJoinAndSelect('recipe.preparation_steps', 'step');
    }

    recipeQB.where('recipe.id = :id', { id });

    const entity = await recipeQB.getOne();

    if (relateds.find(related => related === 'ingredients')) {
      entity.ingredients = await this.getIngredients(entity.id, language_id);
    }

    if (relateds.find(related => related === 'steps') && relateds.find(related => related === 'steps')) {
        await this.mediaService.loadFileRelateds(entity.preparation_steps, 'file_id', 'file', token);
    }

    return entity;
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.recipeRepo.count({ id, fentity: { id: entity_id}  });
  }

  public async existIDs(ids: number[], entity_id: number): Promise<number> {
    if (ids.length) {
      return await this.recipeRepo.count({ id: In(ids), fentity: { id: entity_id}  });
    } else {
      return 0;
    }
  }

  public async update(id: number, entityDto: RecipeDto, entity_id: number, token: string) {
    const persistedEntity = await this.recipeRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }

    const products_id = [...new Set(entityDto.ingredients.map( idt => idt.ingredient_id))];

    if (products_id.length < entityDto.ingredients.length) {
      throw new ValidationException([new ResponseError(400, 'Existem ingradientes repetidos')]);
    }

    const existsProduct = await this.productsService.existIDs(products_id, entity_id);
    if (existsProduct < products_id.length) {
      throw new NotFoundException(404, 'the ingredient id sent doesnt exist');
    }

    return new Promise<Recipe>(async (resolve, reject) => {
      await this.recipeRepo.manager.transaction(async entityManger => {
        const resultUpdate = await entityManger.createQueryBuilder()
          .update(Recipe)
          .set({
            associated_docs: entityDto.associated_docs,
            comment: entityDto.comment,
            description: entityDto.description,
            distribution: entityDto.distribution,
            microbiological_criteria: entityDto.microbiological_criteria,
            name: entityDto.name,
            number_doses: entityDto.number_doses,
            packaging: entityDto.packaging,
            preparation: entityDto.preparation,
            preparation_mode: entityDto.preparation_mode,
            use: entityDto.use,
            active: entityDto.active
          })
          .where('id = :id', { id })
          .execute();

        const trecipe: Recipe = new Recipe();
        trecipe.id = id;

        await entityManger.createQueryBuilder()
          .delete()
          .from(ProductRecipe)
          .where('recipeId = :id', { id })
          .execute();

        for (const ingredient of entityDto.ingredients) {
          const tproduct: Product = new Product();
          tproduct.id = ingredient.ingredient_id;

          const tpr: ProductRecipe = new ProductRecipe();
          tpr.recipe = trecipe;
          tpr.ingredient = tproduct;
          tpr.gross_quantity = ingredient.gross_quantity;
          tpr.liquid_quantity = ingredient.liquid_quantity;

          await entityManger.createQueryBuilder()
            .insert()
            .into(ProductRecipe)
            .values(tpr)
            .execute();
        }

        await entityManger.createQueryBuilder()
          .delete()
          .from(RecipeStep)
          .where('recipeId = :id', { id })
          .execute();

        for (const step of entityDto.preparation_steps) {
          const tstep: RecipeStep = new RecipeStep();
          tstep.recipe = trecipe;
          tstep.file_id = step.file_id;
          tstep.step = step.step;
          tstep.description = step.description;

          await entityManger.createQueryBuilder()
            .insert()
            .into(RecipeStep)
            .values(tstep)
            .execute();
        }
      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['ingredients', 'steps', 'files'], null, entity_id, token));
      } catch (e) {
        reject(e);
      }
    });
  }

  async getIngredients(recipe_id: number, language_id: number) {
    const ingrQB = this.productRecipeRepo.createQueryBuilder('ingredientRecipe');
    ingrQB.leftJoinAndSelect('ingredientRecipe.ingredient', 'ingredient');
    ingrQB.leftJoinAndSelect('ingredient.translations',
      'ingredient_translation',
      language_id ? 'ingredient_translation.language_id = :language_id' : '',
      { language_id });
    ingrQB.leftJoinAndSelect('ingredient.unit', 'unit');
    ingrQB.where('ingredientRecipe.recipeId = :recipe_id', { recipe_id });

    const ents = await ingrQB.getMany();
    return ents;

  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.recipeRepo.count({ id, fentity: { id: entity_id}  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }
    return await this.recipeRepo.delete(id);
  }
}