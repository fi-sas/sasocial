import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class ReservationCancelDto {
    @ApiModelProperty()
    @IsNumber({}, { message: 'payment_method_id must be a number' })
    payment_method_id: number;

}