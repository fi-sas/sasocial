import { UserDto } from './../../common/entities/user.dto';
import { Column, Entity, ManyToOne } from 'typeorm';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { BaseEntity } from '../../common/base.entity';
import {MenuDish} from '../../menus/entities/menu-dish.entity';

@Entity()
export class Reservation extends BaseEntity {

  @Column({ type: 'date', nullable: true})
  date: Date;

  @Column()
  user_id: number;

  @Column({ nullable: true})
  order_id: number;

  @Column({ nullable: true})
  item_id: number;

  order?: any;

  user?: UserDto;

  @Column({ nullable: true, length: 50})
  location: string;

  @Column({ nullable: false, default: false })
  isFromPack: boolean;

  @Column({ nullable: false, default: false })
  has_canceled: boolean;

  @Column({ nullable: true })
  annulment_movement_id: number;

  @Column({ nullable: false, default: true })
  isAvailable: boolean;

  @Column({ nullable: true, default: null })
  device_id: number;

  @Column({ nullable: false, default: false })
  isServed: boolean;

  @Column({ nullable: true})
  served_at: Date;

  @ManyToOne(type => MenuDish, md => md.reservations)
  menu_dish: MenuDish;

  @Column({ name: 'menuDishId'})
  menuDishId: number;

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: true, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;

  @Column()
  fentityId: number;
}