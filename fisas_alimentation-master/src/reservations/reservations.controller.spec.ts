import { Test, TestingModule } from '@nestjs/testing';
import { ReservationsController } from './reservations.controller';

describe('Reservations Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ReservationsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ReservationsController = module.get<ReservationsController>(ReservationsController);
    expect(controller).toBeDefined();
  });
});
