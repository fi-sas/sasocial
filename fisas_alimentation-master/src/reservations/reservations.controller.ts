import { ReservationCancelDto } from './dtos/reservation-cancel.dto';
import {
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete,
  Query,
  UseGuards,
  Body,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitParam,
  ApiImplicitQuery,
  ApiProduces,
  ApiUseTags,
} from '@nestjs/swagger';
import { User } from '../common/decorators/user.decorator';
import { Scopes } from '../common/decorators/scope.decorator';
import { CONFIGURATIONS } from '../app.config';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { ReservationsService } from './reservations.service';
import { Reservation } from './entities/reservation.entity';
import {
  Response,
  ResponseError,
  ResponseStatus,
} from '../common/response.model';
import moment = require('moment');
import { TokenGuard } from '../common/guards/token.guard';
import { Token } from '../common/decorators/token.decorator';
import { Meal } from '../menus/entities/menu.entity';
import { DeviceID } from '../common/decorators/deviceID.decorator';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Reservations')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/reservations')
export class ReservationsController {
  constructor(private readonly reservationsService: ReservationsService) {}

  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    description: 'meal of the menu, possibles values "lunch" | "dinner"',
    name: 'meal',
    type: Meal})
  @ApiImplicitQuery({
    required: false,
    name: 'date',
    description: 'Return reservations for the date sent.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'service_id',
    description: 'Limits the reservation to a service.',
    type: String,
  })
  @Get('count')
  public async countReservations(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Query() query,
  ) {
    return new Response<any>(ResponseStatus.OK, await this.reservationsService.countReservations(
      query.service_id,
      query.date,
      query.meal,
      entity_id
    ));
  }

  @Scopes('reservations')
  @ApiImplicitQuery({
    required: false,
    name: 'withRelated',
    description:
      'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "dishs", "types" ',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'offset',
    description:
      'Offset from which the items should be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_OFFSET +
      '.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'limit',
    description:
      'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT +
      '. If no pagination is to be applied, use -1.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'sort',
    description:
      'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'has_canceled',
    description: 'Returns only reservations has_canceled if sended true.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'isServed',
    description: 'Returns only reservations served if sended true.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    description: 'meal of the menu, possibles values "lunch" | "dinner"',
    name: 'meal',
    type: Meal})
  @ApiImplicitQuery({
    required: false,
    name: 'user_id',
    description: 'Returns only reservations from the user id sent.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'menuDishId',
    description:
      'Returns only the reservations for id of the dish of the menu.',
    type: Number,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'date',
    description: 'Return reservations for the date sent.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'begin_date',
    description: 'Return reservations equal and after the date sent.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'end_date',
    description: 'Return reservations equal and before the date sent.',
    type: String,
  })
  @ApiImplicitQuery({
    required: false,
    name: 'service_id',
    description: 'Limits the reservation to a service.',
    type: String,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @Get()
  public async findAll(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @Query() query,
    @User() user,
    @Token() token,
  ) {
    if (!query.hasOwnProperty('offset'))
      query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit'))
      query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';
    if (!query.hasOwnProperty('user_id')) query.user_id = null;
    if (!query.hasOwnProperty('menuDishId')) query.menuDishId = null;
    if (!query.hasOwnProperty('date')) query.date = null;
    if (!query.hasOwnProperty('meal')) query.meal = null;
    if (!query.hasOwnProperty('device_id')) query.device_id = null;
    if (!query.hasOwnProperty('begin_date')) query.begin_date = null;
    if (!query.hasOwnProperty('has_canceled')) query.has_canceled = false;
    if (!query.hasOwnProperty('isServed')) query.isServed = null;
    else
      query.begin_date = moment(query.begin_date).isValid()
        ? query.begin_date
        : null;
    if (!query.hasOwnProperty('end_date')) query.end_date = null;
    else
      query.end_date = moment(query.end_date).isValid() ? query.end_date : null;
    if (!query.hasOwnProperty('service_id')) query.service_id = null;

    if (query.isServed) {
      try {
        query.isServed = JSON.parse(query.isServed);
      } catch (r) {
        query.isServed = null;
      }
    }

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation => 
        relation !== 'dishs' &&
        relation !== 'types' &&
        relation !== 'menus'
        );
      if (relationErro)
        return new Response(ResponseStatus.ERROR, [], {}, [
          new ResponseError(
            500,
            relationErro + ' is not defined on the model.',
          ),
        ]);
    }

    const reservations = await this.reservationsService.findAll(
      query.offset,
      query.limit,
      query.sort,
      relateds,
      query.user_id,
      query.device_id,
      query.menuDishId,
      query.meal,
      query.date,
      query.begin_date,
      query.end_date,
      query.has_canceled,
      query.isServed,
      query.service_id,
      token,
    );
    return new Response<Reservation[]>(ResponseStatus.OK, reservations[0], {
      total: reservations[1],
    });
  }

  @Scopes('reservations')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the reservation',
    name: 'id',
    type: Number,
  })
  @Get(':id')
  public async findByID(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @User() user,
    @Param('id', new ParseIntPipe()) id,
    @Token() token,
  ) {
    return new Response<Reservation>(
      ResponseStatus.OK,
      await this.reservationsService.findById(id, token),
    );
  }

  @Scopes('reservations')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the reservation',
    name: 'id',
    type: Number,
  })
  @Post(':id/cancel')
  public async cancelReservation(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @User() user,
    @Param('id', new ParseIntPipe()) id,
    @Body() body: ReservationCancelDto,
    @Token() token,
  ) {
    return new Response<Reservation>(ResponseStatus.OK, await this.reservationsService.cancelReservation(id, body.payment_method_id, token));
  }

  @Scopes('reservations')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the reservation',
    name: 'id',
    type: Number,
  })
  @Delete(':id/unserve')
  public async rejectReservation(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @User() user,
    @DeviceID() device_id,
    @Param('id', new ParseIntPipe()) id,
    @Token() token,
  ) {
    return new Response<Reservation>(ResponseStatus.OK, await this.reservationsService.unserveMeal(id, device_id, token));
  }

  @Scopes('reservations')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the reservation',
    name: 'id',
    type: Number,
  })
  @Put(':id')
  public async update(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @User() user,
    @Param('id', new ParseIntPipe()) id,
  ) {
    // IS ONLY POSSIBLE TO UPDATE IN SOME CIRCUSTANCIES
    return user;
  }

  @Scopes('reservations')
  @ApiImplicitParam({
    required: true,
    description: 'id of the fiscal entity',
    name: 'entity_id',
    type: Number,
  })
  @ApiImplicitParam({
    required: true,
    description: 'id of the reservation',
    name: 'id',
    type: Number,
  })
  @Post(':id/serve')
  public async confirmReservation(
    @Param('entity_id', new ParseIntPipe()) entity_id,
    @User() user,
    @Param('id', new ParseIntPipe()) id,
    @DeviceID() device_id,
    @Token() token,
  ) {
    // return user;
    return new Response<Reservation>(ResponseStatus.OK, await this.reservationsService.serveMeal(id, device_id, token));
  }
}
