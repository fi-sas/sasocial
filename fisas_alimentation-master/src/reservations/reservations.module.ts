import { Module } from '@nestjs/common';
import { ReservationsController } from './reservations.controller';
import { ReservationsService } from './reservations.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from './entities/reservation.entity';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';
import {MenusModule} from '../menus/menus.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    MenusModule,
    TypeOrmModule.forFeature([
      Reservation
    ])
  ],
  controllers: [
    ReservationsController
  ],
  providers: [
    ReservationsService
  ],
  exports: [
    ReservationsService
  ]
})
export class ReservationsModule {}
