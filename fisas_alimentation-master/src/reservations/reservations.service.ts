import { OrderService } from './../common/services/order/order.service';
import { MovementsService } from './../common/services/movements.service';
import { NotFoundException } from './../common/exceptions/not-found.exception';
import { CheckoutDataDTO } from './../common/entities/checkout-data.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Reservation } from './entities/reservation.entity';
import { CONFIGURATIONS } from '../app.config';
import { ReservationPackDto } from '../common/entities/reservation-pack.dto';
import { MenusService } from '../menus/menus.service';
import { UsersService } from '../common/services/users.service';
import * as moment from 'moment';
import { connect } from 'ts-nats';
import { MediasService } from '../common/services/medias.service';
import { NestSchedule, Cron } from 'nest-schedule';
import { Meal } from 'menus/entities/menu.entity';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ValidationPipe } from '../common/pipes/validation.pipe';
import { CartService } from '../common/services/cart/cart.service';
import { CartDto, CartCheckoutDto } from '../common/services/cart/dto/cart.dto';
import { orderItemToCartItem } from '../common/services/order/dto/order.dto';

@Injectable()
export class ReservationsService extends NestSchedule {
  // Nats connection
  nc = null;

  constructor(
    @InjectRepository(Reservation)
    private readonly reservationRepo: Repository<Reservation>,
    private menusService: MenusService,
    private usersService: UsersService,
    private mediasService: MediasService,
    private cartService: CartService,
    private orderService: OrderService,
  ) {
    super();

    this.natsConnection();
  }

  private async natsConnection() {
    try {
      this.nc = await connect({ maxReconnectAttempts: -1, reconnectTimeWait: 250, servers: [process.env.NATS_URL] });

      const sub = await this.nc.subscribe(
        CONFIGURATIONS.QUEUES.CREATE_RESERVATION_PACK,
        (err, msg) => {
          if (err) {
            throw new Error(err);
          } else {
            console.log('[Reservation] - New reservation message from pack');
            this.createPackReservations(JSON.parse(msg.data));
          }
        },
      );

      const sub1 = await this.nc.subscribe(
        CONFIGURATIONS.QUEUES.PAYMENT_V1_ORDER_CREATED,
        (err, msg) => {
          if (err) {
            throw new Error(err);
          } else {
            console.log('[Reservation] - New reservation message from payment');
            try {
              const data: CheckoutDataDTO = JSON.parse(msg.data) as CheckoutDataDTO;
              this.createReservation(data);
            } catch (e) {
              throw new Error(e);
            }
          }
        },
      );
    } catch (ex) {
      console.error('Nats connection has got a error');
    }


  }

  async createReservation(data: CheckoutDataDTO) {
    /* 
     {"user_id":2,"account_id":1,
     "status":"Pending Payment","value":3.41,"updated_at":"2019-07-16T11:36:35.000Z",
     "created_at":"2019-07-16T11:36:35.000Z","id":209,"payment_id":null,
     "cancel_payment_id":null,
     "items":[{"id":300,"order_id":209,"service_id":21,
     "product_code":"ALMOCOCARNE","article_type":"REFECTORY","movement_id":null,"status":"Pending","description":"Refeições [CA - Cantina]","extra_info":{"id":424},"quantity":1,"unit_value":3.41,"liquid_value":2.772,"vat_value":0.63756,"total_value":3.41,"vat_id":1,"vat":0.23,"erp_budget":null,"erp_sncap":null,"erp_income_cost_center":null,"erp_funding_source":null,"erp_program":null,"erp_measure":null,"erp_project":null,"erp_activity":null,"erp_action":null,"erp_functional_classifier":null,"erp_organic":null,"updated_at":"2019-07-16T11:36:35.000Z","created_at":"2019-07-16T11:36:35.000Z","name":"Arroz de Pato [Carne] [lunch] [Tue Jul 16 2019 12:36:26 GMT+0100 (Hora de verão da Europa Ocidental)]"}]}
 */
    if (data) {
      if (data.items) {
        data.items = data.items.filter(i => i.article_type === 'REFECTORY');
        if (data.items) {
          for (const item of data.items) {
            if (item.liquid_value >= 0) {
              try {
                const reservationT = new Reservation();
                reservationT.user_id = data.user_id;
                reservationT.menuDishId = item.extra_info.refectory_id;
                reservationT.isServed = false;
                reservationT.location = item.location;
                reservationT.isFromPack = false;
                reservationT.order_id = data.id;
                reservationT.item_id = item.id;

                await this.reservationRepo.insert(Array(item.quantity).fill(reservationT));
              } catch (ex) {
                console.log(ex);
              }
            }
          }
        }
      }
    }
  }

  async createPackReservations(
    reservations: ReservationPackDto[],
  ): Promise<any> {
    const ids_reserved = [];

    for (const r of reservations) {
      if (r.menuDish_id) {
        try {
          const result = await this.reservationRepo.insert({
            menuDishId: r.menuDish_id,
            isFromPack: true,
            user_id: r.purchased_pack.user_id,
          });
          ids_reserved.push({ id: r.id, menuDishId: r.menuDish_id });
        } catch (e) {
          console.error(
            '[RESERVAIONS] Error on reservations creation from pack',
          );
          console.error(e);
        }
      } else {
        const md = await this.menusService.getMenuDishsToPackReserve(
          r.service_id,
          r.dish_type_id,
        );
        if (md) {
          try {
            this.reservationRepo.insert({
              menuDishId: md.id,
              isFromPack: true,
              user_id: r.purchased_pack.user_id,
            });
            ids_reserved.push({ id: r.id, menuDishId: md.id });
          } catch (e) {
            console.error(
              '[RESERVAIONS] Error on reservations creation from pack',
            );
            console.error(e);
          }
        }
      }
    }

    if (ids_reserved) {
      try {
        this.nc.publish(
          CONFIGURATIONS.QUEUES.RESERVED_MEALS_PACK,
          JSON.stringify(ids_reserved),
        );
      } catch (e) {
        console.log('[Reservations] Error');
        console.log(e);
      }
    }
  }

  public async findAll(
    offset: number,
    limit: number,
    sort: string,
    relateds: string[],
    user_id: number,
    device_id: number,
    menuDishId: number,
    meal: Meal,
    date: string,
    begin_date: string,
    end_date: string,
    has_canceled: boolean,
    isServed: boolean,
    service_id: number,
    token: string,
  ): Promise<[Reservation[], number]> {
    const rQB = this.reservationRepo.createQueryBuilder('r');

    if (limit > 0) {
      rQB.take(limit);
      rQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      rQB.orderBy(
        'r.' + sort_replaced,
        sort_replaced !== sort ? 'DESC' : 'ASC',
      );
    }

    if (relateds.find(related => related === 'dishs') ||
      relateds.find(related => related === 'types') ||
      relateds.find(related => related === 'menus')) {
      rQB.leftJoinAndSelect('r.menu_dish', 'md');
      rQB.leftJoinAndSelect('md.dish', 'd');
      rQB.leftJoinAndSelect('d.translations', 'dt');

      if (relateds.find(related => related === 'types')) {
        rQB.leftJoinAndSelect('md.type', 't');
        rQB.leftJoinAndSelect('t.translations', 'tt');
      }

      if (relateds.find(related => related === 'menus')) {
        rQB.leftJoinAndSelect('md.menu', 'm');
      } else {
        rQB.leftJoin('md.menu', 'm');
      }

      if (service_id) {
        rQB.andWhere('m.serviceId = :service_id', { service_id });
      }
    } else {
      rQB.leftJoin('r.menu_dish', 'md');
      rQB.leftJoin('md.menu', 'm');
      if (service_id) {
        rQB.andWhere('m.serviceId = :service_id', { service_id });
      }
    }

    if (user_id) {
      rQB.andWhere('r.user_id = :user_id', { user_id });
    }

    if (device_id) {
      rQB.andWhere('r.device_id = :device_id', { device_id });
    }

    if (menuDishId) {
      rQB.andWhere('r.menuDishId = :menuDishId', { menuDishId });
    }

    if (meal) {
      rQB.andWhere('m.meal = :meal', { meal });
    }

    if (date) {
      /*rQB.andWhere('r.date = :date', {
        date,
      });*/

      rQB.andWhere('m.date = :date', { date: moment(date).format('YYYY-MM-D') });
    } else {
      if (begin_date && end_date) {
        rQB.andWhere('m.date BETWEEN :begin_date and :end_date', {
          begin_date,
          end_date,
        });
      } else {
        if (begin_date) {
          rQB.andWhere('m.date >= :begin_date', { begin_date });
        }
        if (end_date) {
          rQB.andWhere('m.date <= :end_date', { end_date });
        }
      }
    }

    if (isServed === true || isServed === false) {
      rQB.andWhere('r.isServed = :isServed', { isServed });
    }

    if (has_canceled === false) {
      rQB.andWhere('r.has_canceled = false');
    } else {
      rQB.andWhere('r.has_canceled = true');
    }

    const results = await rQB.getManyAndCount();

    for (const reservation of results[0]) {
      if (reservation.user_id) {
        try {
          reservation.user = await this.usersService.get(
            reservation.user_id,
            token,
          );
        } catch (e) { }
      }
    }

    return results;
  }

  async findById(id: number, token: string): Promise<Reservation> {
    const rQB = this.reservationRepo.createQueryBuilder('r');
    rQB.where('r.id = :id', { id });
    const result = await rQB.getOne();

    if (result.user_id) {
      try {
        result.user = await this.usersService.get(result.user_id, token);
      } catch (e) { }
    }

    return result;
  }

  async countReservations(serviceId: number, date: string, meal: string, entity_id: number) {
    const qb = this.menusService
      .menuDishRepo
      .createQueryBuilder('md')
      .select(`tt.name as type,
       dt.name as dish, 
       md.doses_available AS available,
       COALESCE(SUM(r.isServed = true),0) AS served,
       COALESCE(SUM(r.isServed = false), 0) as nonServed,
        COUNT(r.id) as total`)
      .leftJoin('md.reservations', 'r')
      .leftJoin('md.dish', 'd')
      .leftJoin('d.translations', 'dt', 'dt.language_id = 3')
      .leftJoin('md.type', 't')
      .leftJoin('t.translations', 'tt', 'tt.language_id = 3')
      .leftJoin('md.menu', 'm')
      .leftJoin('m.service', 's')
      .groupBy('md.id');

    qb.where('s.fentityId = :entity_id', { entity_id });
    qb.andWhere('r.has_canceled = false');

    if (date) {
      qb.andWhere('m.date = :date', { date });
    }

    if (meal) {
      qb.andWhere('m.meal = :meal', { meal });
    }

    if (serviceId) {
      qb.andWhere('m.serviceId = :serviceId', { serviceId });
    }

    return await qb.getRawMany();
  }

  async serveMeal(id: number, device_id: number, token: string) {
    await this.reservationRepo.update(id, { isServed: true, served_at: new Date(), isAvailable: false, device_id });
    return await this.findById(id, token);
  }

  async unserveMeal(id: number, device_id: number, token: string) {
    await this.reservationRepo.update(id, { isServed: false, served_at: new Date(), isAvailable: true, device_id });
    return await this.findById(id, token);
  }

  async getUserReservations(user_id: number, month?: number, year?: number, onlyAvailables?: boolean, sort?: string, language_id?: number, token?: string) {
    const qb = this.reservationRepo
      .createQueryBuilder('r')
      .leftJoinAndSelect('r.menu_dish', 'md')
      .leftJoinAndSelect('md.dish', 'd')
      .leftJoinAndSelect('d.translations', 'dt', language_id ? 'dt.language_id = :language_id' : '', { language_id })
      .leftJoinAndSelect('md.type', 't')
      .leftJoinAndSelect('t.translations', 'tt', language_id ? 'tt.language_id = :language_id' : '', { language_id })
      .leftJoinAndSelect('md.menu', 'm')
      .leftJoinAndSelect('m.service', 's')
      .where('r.user_id = :user_id', { user_id });

    if (month || year) {
      if (month) {
        qb.andWhere('MONTH(m.date) = :month', { month });
      }

      if (year) {
        qb.andWhere('YEAR(m.date) = :year', { year });
      }
    }

    if (onlyAvailables) {
      qb.andWhere('r.isAvailable = 1');
    }

    if (sort) {
      sort.split(',').map(ss => {
        const sort_replaced = ss.replace('-', '');
        qb.orderBy('m.' + sort_replaced, sort_replaced !== ss ? 'DESC' : 'ASC');
      });
    }

    const reservations = await qb.getManyAndCount();

    for (const reservation of reservations[0]) {
      await this.mediasService.loadFileRelated(reservation.menu_dish.dish, 'file_id', 'file', token);
    }

    return reservations;
  }

  async getUserReservationsCount(user_id: number) {
    const qb = this.reservationRepo
      .createQueryBuilder('r')
      .select('count(r.id) as total')
      .leftJoin('r.menu_dish', 'md')
      .leftJoin('md.menu', 'm')
      .where('r.user_id = :user_id', { user_id })
      .andWhere('m.date >= CURDATE()')
      .andWhere('r.isAvailable = 1')
      .andWhere('r.isServed = 0')
      .andWhere('r.has_canceled = 0');

    return await qb.getRawOne();
  }

  async getMenuDishReservationCount(menu_dish_id: number) {
    const qb = this.reservationRepo
      .createQueryBuilder('r')
      .select('count(r.id) as total')
      .where('r.menuDishId = :menu_dish_id', { menu_dish_id })

    return await qb.getRawOne();
  }

  @Cron('0 16 * * *')
  async updateLunchAvailables() {
    console.log('[Reservations] UPDATE LUNCH AVAILABLE')
    this.reservationRepo.query(`
      UPDATE reservation as r
      LEFT JOIN menu_dish as md ON r.menuDishId = md.id
      LEFT JOIN menu as m ON md.menuId = m.id
      SET isAvailable = 0
      WHERE m.meal = 'lunch' AND m.date <= NOW() AND isServed = 0
    `)
  }

  @Cron('0 23 * * *')
  async updateDinnerAvailables() {
    console.log('[Reservations] UPDATE DINNER AVAILABLE')
    this.reservationRepo.query(`
      UPDATE reservation as r
      LEFT JOIN menu_dish as md ON r.menuDishId = md.id
      LEFT JOIN menu as m ON md.menuId = m.id
      SET isAvailable = 0
      WHERE m.meal = 'dinner' AND m.date <= NOW() AND isServed = 0
    `)
  }

  /**
   * 
   * @param id 
   * @param token 
   */
  async cancelReservation(id: number, payment_method_id: number, token: string, user_id?: number): Promise<Reservation> {
    const persistedReservation = await this.reservationRepo.count({ id });
    if (persistedReservation === 0) {
      throw new NotFoundException(404, 'Reservation not found');
    }

    // check the nullable_until and !served available ?
    const reservation = await this.reservationRepo.findOne({ id }, {
      relations: [
        'menu_dish'
      ]
    });

    if (user_id && reservation.user_id !== user_id) {
      const msg = 'this reservation is not associated to logged user';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(msg),
          message: msg
        }
      ]);
    }

    if (reservation.has_canceled) {
      const msg = 'this reservation already has been cancelled';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(msg),
          message: msg
        }
      ]);
    }

    if (reservation.menu_dish.nullable_until) {
      if (moment().isAfter(reservation.menu_dish.nullable_until)) {
        const msg = 'maximum hour of consumption reached';
        throw new ValidationException([
          {
            code: ValidationPipe.generateHash(msg),
            message: msg
          }
        ]);
      }
    }

    if (reservation.isServed) {
      const msg = 'reservation already used';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(msg),
          message: msg
        }
      ]);
    }

    if (reservation.isFromPack) {
      const msg = 'cant cancel reservations from pack';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(msg),
          message: msg
        }
      ]);
    }

    if (!reservation.isAvailable) {
      const msg = 'is not available for annullament';
      throw new ValidationException([
        {
          code: ValidationPipe.generateHash(msg),
          message: msg
        }
      ]);
    }

    return new Promise<Reservation>((resolve, reject) => {
      this.orderService.getOrder(reservation.order_id, token).subscribe(orderData => {
        this.orderService.getOrderItem(reservation.order_id, reservation.item_id, token)
        .subscribe(orderItemData => {
            const cartDto: CartDto = {
              account_id: orderData.data.data[0].account_id,
              // user_id: orderData.data.data[0].user_id,
              items: [
                orderItemToCartItem(orderItemData.data.data[0], 1)
              ]
            };
            cartDto.items.map(i => i.liquid_value = -i.liquid_value);

            const cartCheckoutDto: CartCheckoutDto = {
              payment_method_id,
              payment_method_data: {},
            };

            this.cartService.createCartAndCheckout(cartDto, cartCheckoutDto, token).then(cartData => {
              const update: any = {};
              update.isAvailable = false;
              update.has_canceled = true;
              update.annulment_movement_id = cartData.id;
              this.reservationRepo.update(id, update).then(() => {
                resolve(this.reservationRepo.findOne(id));
              });
            }).catch(err => {
              console.log('cart');
              console.log(err.response.data);
              reject(err);
            });
          }, err => {
            console.log('order iten');
            console.log(err.response.data);
            reject(err);
          });
      }, err => {
        console.log('order');
        console.log(err.response.data);
        reject(err);
      });
    });
  }
}
