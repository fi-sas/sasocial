import {
  IsArray,
  IsBoolean,
  IsNotEmpty, IsString, MaxLength,
} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { MenuDishDto } from '../../menus/dtos/menu.dto';

export class SchoolDto {
  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o nome não pode ser vazio'})
  @IsString({ message: ' o nmeo e do tipo texto'})
  name: string;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o acroniumo não pode ser vazio'})
  @IsString({ message: ' o acronimo e do tipo texto'})
  @MaxLength(5, { message: 'acronym as the max length of 5'})
  acronym: string;

  @ApiModelProperty({type: Number, isArray: true })
  @IsArray({ message: 'defaultDevices têm de se um array'})
  devices: number[];

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}