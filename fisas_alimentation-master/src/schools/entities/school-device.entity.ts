import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { School } from './school.entity';

@Entity()
export class SchoolDevice {

  @ManyToOne(type => School, sc => sc.devices, { primary: true })
  school: School;

  @PrimaryColumn()
  device_id: number;
}