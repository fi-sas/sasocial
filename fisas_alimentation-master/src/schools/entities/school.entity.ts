import { Column, Entity, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { SchoolDevice } from './school-device.entity';
import { Service } from '../../service/entities/service.entity';

@Entity()
export class School extends BaseEntity{
  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false, length: 5 })
  acronym: string;

  @OneToMany(type => Service, sv => sv.school)
  services : Service[];

  @OneToMany(type => SchoolDevice, sd => sd.school)
  @JoinColumn()
  devices: SchoolDevice[];

}