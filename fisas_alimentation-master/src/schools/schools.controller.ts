import { Body, Controller, Delete, Get, Headers, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitHeader, ApiImplicitQuery } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { SchoolsService } from './schools.service';
import { SchoolDto } from './dtos/school.dto';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { School } from './entities/school.entity';
import { Token } from '../common/decorators/token.decorator';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Schools')
@Controller(CONFIGURATIONS.URL_PREFIX + '/schools')
export class SchoolsController {

  constructor(private schoolsService: SchoolsService) {}

  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "devices"', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @Scopes('schools')
  @Get()
  public async findAll(@Query() query, @Token() token) {

    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'devices');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.schoolsService.findAll(query.offset, query.limit, query.sort, relateds, token);
    return new Response<School[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1]
      }
    );
  }

  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include and "devices"', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the school', name: 'id', type: Number })
  @Scopes('schools')
  @Get(':id')
  public async findById(@Param('id', new ParseIntPipe()) id,
                        @Query() query, @Token() token) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'devices');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.schoolsService.findByID(id, relateds, token);
    return new Response<School>(ResponseStatus.OK, entity);
  }

  @Scopes('schools')
  @Post()
  public async create(@Body() entity: SchoolDto, @Token() token) {
    const createdEntity = await this.schoolsService.create(entity, token);
    return new Response<School>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the school', name: 'id', type: Number })
  @Scopes('schools')
  @Put(':id')
  public async update(@Param('id', new ParseIntPipe()) id, @Body() entity: SchoolDto, @Token() token) {
    const updatedEntity = await this.schoolsService.update(id, entity, token);
    return new Response<School>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the school', name: 'id', type: Number })
  @Scopes('schools')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('id', new ParseIntPipe()) id) {
    const result = await this.schoolsService.delete(id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}