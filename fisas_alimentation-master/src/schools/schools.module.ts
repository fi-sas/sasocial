import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SchoolsController } from './schools.controller';
import { SchoolsService } from './schools.service';
import { School } from './entities/school.entity';
import { SchoolDevice } from './entities/school-device.entity';
import { ServicesModule } from '../service/services.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    forwardRef(() => ServicesModule),
    TypeOrmModule.forFeature([
      School,
      SchoolDevice
    ])
  ],
  controllers: [
    SchoolsController
  ],
  providers: [
    SchoolsService
  ],
  exports: [
    SchoolsService
  ]
})
export class SchoolsModule {}