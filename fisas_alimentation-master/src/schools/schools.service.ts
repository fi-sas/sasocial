import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { School } from './entities/school.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { SchoolDto } from './dtos/school.dto';
import { ServicesService } from '../service/services.service';
import { SchoolDevice } from './entities/school-device.entity';
import { DevicesService } from '../common/services/devices.service';
import { DeviceDTO } from '../common/entities/device.dto';
import { ServiceDevice } from '../service/entities/service-device.entity';

@Injectable()
export class SchoolsService {
  constructor(@InjectRepository(School) private readonly schoolRepo: Repository<School>,
              @InjectRepository(SchoolDevice) private readonly schoolDeviceRepo: Repository<SchoolDevice>,
              @Inject(forwardRef(() => ServicesService)) private readonly servicesService: ServicesService,
              private readonly devicesService: DevicesService
  ) {}

  public async create(entityDto: SchoolDto, token: string): Promise<School> {

    return new Promise<School>(async (resolve, reject) => {
      let id: number = 0;

      await this.schoolRepo.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(School)
          .values({
            name: entityDto.name,
            acronym: entityDto.acronym,
            active: entityDto.active,
          })
          .execute();

        id = insertResult.identifiers[0].id;
        const tschool: School = new School();
        tschool.id = id;

        if (entityDto.devices.length > 0) {
          await entityManger.createQueryBuilder()
            .delete().from(SchoolDevice)
            .where('device_id IN (:devices)', { id, devices: entityDto.devices })
            .execute();
        }

        for (const devices of entityDto.devices) {
          const tsd: SchoolDevice = new SchoolDevice();
          tsd.school = tschool;
          tsd.device_id = devices;

          await entityManger.createQueryBuilder()
            .insert()
            .into(SchoolDevice)
            .values(tsd)
            .execute();
        }

      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['devices'], token));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[], token: string) {
    const schoolsQB = this.schoolRepo.createQueryBuilder('school');

    if (limit > 0) {
      schoolsQB.take(limit);
      schoolsQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      schoolsQB.orderBy('school.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'devices')) {
      schoolsQB.leftJoinAndSelect('school.devices', 'device');
    }

    const entities = await schoolsQB.getManyAndCount();

    if (relateds.find(related => related === 'devices')) {
      for (const entity of entities[0]) {
        const tdevs: DeviceDTO[] = [];
        entity.devices = entity.devices.length > 0 ? entity.devices : [];
        for (const device of entity.devices) {
          try {
            const tdev = await this.devicesService.get(device.device_id, token);
            tdevs.push(tdev);
          } catch (errors) {
            if (errors[0].code === 404) {
              this.schoolDeviceRepo.createQueryBuilder()
                .delete()
                .from(ServiceDevice)
                .where('deviceId = :id', { id: device.device_id })
                .execute();
            }
          }
        }
        // @ts-ignore
        entity.devices = tdevs;
      }
    }
    return entities;
  }

  public async findByID(id: number, relateds: string[], token: string): Promise<School> {
    const persistedEntity = await this.schoolRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'School not found');
    }

    const schoolQB = this.schoolRepo.createQueryBuilder('school');

    if (relateds.find(related => related === 'devices')) {
      schoolQB.leftJoinAndSelect('school.devices', 'devices');
    }

    schoolQB.where('school.id = :id', { id });
    const entity = await schoolQB.getOne();

    if (relateds.find(related => related === 'devices')) {
      const tdevs: DeviceDTO[] = [];
      for (const device of entity.devices) {
        try {
          const tdev = await this.devicesService.get(device.device_id, token);
          tdevs.push(tdev);
        } catch (errors) {
          if (errors[0].code === 404) {
            this.schoolDeviceRepo.createQueryBuilder()
              .delete()
              .from(ServiceDevice)
              .where('deviceId = :id', { id: device.device_id })
              .execute();
          }
        }
      }
      // @ts-ignore
      entity.devices = tdevs;
    }

    return entity;
  }

  public async existID(id: number): Promise<number> {
    return await this.schoolRepo.count({ id });
  }

  public async update(id: number, entityDto: SchoolDto, token: string): Promise<School> {

    const persistedEntity = await this.schoolRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'School not found');
    }
    entityDto.id = id;

    return new Promise<School>(async (resolve, reject) => {
      await this.schoolRepo.manager.transaction(async entityManger => {
        await entityManger.createQueryBuilder()
          .update(School)
          .set({
            name: entityDto.name,
            acronym: entityDto.acronym,
            active: entityDto.active,
          })
          .where('id = :id', { id })
          .execute();

        const tschool: School = new School();
        tschool.id = id;

        const qbDeleteDevices = entityManger.createQueryBuilder()
          .delete().from(SchoolDevice);
        qbDeleteDevices.where('schoolId = :id ', { id });
        if (entityDto.devices.length > 0) {
          qbDeleteDevices.andWhere('device_id IN (:devices)', { devices: entityDto.devices });
        }
        await qbDeleteDevices.execute();

        for (const devices of entityDto.devices) {
          const tsd: SchoolDevice = new SchoolDevice();
          tsd.school = tschool;
          tsd.device_id = devices;

          await entityManger.createQueryBuilder()
            .insert()
            .into(SchoolDevice)
            .values(tsd)
            .execute();
        }

      }).catch(reason => {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['devices'], token));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async schoolWithDevice(device_id: number): Promise<School> {
    const schoolDevice = await this.schoolDeviceRepo.createQueryBuilder('sd')
      .select()
      .leftJoinAndSelect('sd.school', 'sc')
      .where('sd.device_id = :device_id', { device_id })
      .getOne();

    return schoolDevice ? schoolDevice.school : null;
  }

  public async delete(id: number) {
    const persistedEntity = await this.schoolRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'School not found');
    }
    return await this.schoolRepo.delete(id);
  }
}