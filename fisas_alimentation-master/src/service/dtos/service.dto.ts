import { IsArray, IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateIf } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { ServiceType } from '../../common/enums/service-type.enum';
import { Column } from 'typeorm';

export class ServiceDto {
  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o id do aramzem não pode ser vazio'})
  @IsNumber({}, { message: 'o id do aramzem e um numero'})
  wharehouse_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o nome não pode ser vazio'})
  @IsString({ message: ' o nmeo e do tipo texto'})
  name: string;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' maintenance não pode ser vazio'})
  @IsBoolean({ message: ' maintenance é do tipo boolean'})
  maintenance: boolean;

  @ApiModelProperty({ type: Number, isArray: true})
  @IsNotEmpty({ message: 'devices não pode ser vazio'})
  @IsArray({ message: 'devices e um array de inteiros'})
  devices: number[];

  @ApiModelProperty()
  @IsNumber({}, { message: 'the school id must be a number'})
  school_id: number;

  @ApiModelProperty({ type: Number, isArray: true})
  @IsNotEmpty({ message: 'families cant by empty'})
  @IsArray({ message: 'families is a array of integers'})
  families: number[];

  @ApiModelProperty({ type: ServiceType, enum: ['bar', 'canteen']})
  @IsEnum(ServiceType, { message: 'prodcut type is type enum, "bar" or "canteen"'})
  type: ServiceType;

  @ApiModelProperty({ description: 'Service ID from configuration microservice'})
  @IsNumber({}, { message: 'service_id should be a number'})
  service_id: number;

  @ApiModelProperty()
  @IsBoolean({ message: 'active is boolean type'})
  active: boolean;
}
