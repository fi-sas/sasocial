import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Service } from './service.entity';

@Entity()
export class ServiceDevice {
  @PrimaryColumn()
  device_id: number;

  @ManyToOne(type => Service, service => service.devices, { primary : true, onDelete: 'CASCADE'})
  service: Service;
}