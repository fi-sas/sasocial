import { Column, Entity, ManyToMany, ManyToOne, OneToMany, RelationId } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { Wharehouse } from '../../wharehouses/entities/wharehouse.entity';
import { ServiceDevice } from './service-device.entity';
import { Family } from '../../families/entities/family.entity';
import { ServiceType } from '../../common/enums/service-type.enum';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';
import { School } from '../../schools/entities/school.entity';

@Entity()
export class Service extends BaseEntity {

  @ManyToOne(type => Wharehouse, wharehouse => wharehouse.services, { nullable: false })
  wharehouse: Wharehouse;

  @Column({ name: 'wharehouseId'})
  wharehouse_id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false, default: true })
  maintenance: boolean;

  @Column({ nullable: false, type: 'enum', enum: ['bar', 'canteen'], default: ServiceType.BAR })
  type: ServiceType;

  @ManyToOne(type => School, sc => sc.services, { onDelete: 'RESTRICT' })
  school: School;

  @Column({ name: 'schoolId'})
  school_id: number;

  @OneToMany(type => ServiceDevice, serviceDevice => serviceDevice.service, { cascade: true, onDelete: 'CASCADE'})
  devices: ServiceDevice[];

  @RelationId((service: Service) => service.devices)
  device_ids: number[];

  @ManyToMany(type => Family, family => family.services)
  families: Family[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;

  @Column({ nullable: false })
  service_id: number;
}
