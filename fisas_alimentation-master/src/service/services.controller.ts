import { Body, Controller, Delete, Get, Headers, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitHeader, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitQuery } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { ServicesService } from './services.service';
import { Service } from './entities/service.entity';
import { ServiceDto } from './dtos/service.dto';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { Token } from '../common/decorators/token.decorator';
import { ServiceType } from '../common/enums/service-type.enum';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Services')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/services')
export class ServicesController {

  constructor(private servicesService: ServicesService) {}

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitQuery({ required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "families" , "devices", "school" and "wharehouse" ', type: String})
  @ApiImplicitQuery({ required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number})
  @ApiImplicitQuery({ required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number})
  @ApiImplicitQuery({ required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String})
  @ApiImplicitQuery({ required: false, name: 'type',
    description: 'Finds the services of type sent in this query. Possibilities is BAR, CANTEEN', type: ServiceType})
  @Scopes('services')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id, @Token() token) {

    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (! query.hasOwnProperty('sort')) query.sort = '-id';
    if (! query.hasOwnProperty('type')) {
      query.type = null;
    } else {
      query.type = query.type as ServiceType;
    }
    let relateds = [];
    if ( query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'families' &&
        relation !== 'wharehouse' &&
        relation !== 'devices' &&
        relation !== 'school'
      );
      if (relationErro)
        return new Response(ResponseStatus.ERROR, [], {}, [ new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.servicesService.findAll(query.offset, query.limit,  query.sort, relateds, query.type, language_id, token, entity_id);
    return new Response<Service[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1]
      }
    );
  }

  @ApiImplicitHeader({ required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language'})
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the service', name: 'id', type: Number })
  @ApiImplicitQuery({ required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "families", "devices", "school" and "wharehouse" ', type: String})
  @Scopes('services')
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id, @Query('withRelated') withRelated,
                        @Headers('x-language-id') language_id, @Token() token) {

    let relateds = [];
    if ( withRelated) {
      relateds = withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'families' &&
        relation !== 'wharehouse' &&
        relation !== 'devices' &&
        relation !== 'school'
      );
      if (relationErro)
        return new Response(ResponseStatus.ERROR, [], {}, [ new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.servicesService.findByID(id, relateds, language_id, token, entity_id);
    return new Response<Service>(ResponseStatus.OK, entity, { total: 1});
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Scopes('services')
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: ServiceDto, @Token() token) {
    const createdEntity = await this.servicesService.create(entity, token, entity_id);
    return new Response<Service>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the service', name: 'id', type: Number })
  @Scopes('services')
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                      @Body() entity: ServiceDto, @Token() token) {
    const updatedEntity = await this.servicesService.update(id, entity, token, entity_id);
    return new Response<Service>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the service', name: 'id', type: Number })
  @Scopes('services')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.servicesService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}