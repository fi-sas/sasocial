import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from './entities/service.entity';
import { ServiceDevice } from './entities/service-device.entity';
import { ServicesController } from './services.controller';
import { ServicesService } from './services.service';
import { WharehousesModule } from '../wharehouses/wharehouses.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { SchoolsModule } from '../schools/schools.module';
import { FamiliesModule } from '../families/families.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    forwardRef(() => SchoolsModule),
    FiscalEntityModule,
    WharehousesModule,
    FamiliesModule,
    TypeOrmModule.forFeature([
      Service,
      ServiceDevice
    ])
  ],
  controllers: [
    ServicesController
  ],
  providers: [
    ServicesService
  ],
  exports: [
    ServicesService
  ]
})
export class ServicesModule {}