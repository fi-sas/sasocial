import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Service } from './entities/service.entity';
import { ServiceDto } from './dtos/service.dto';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { Wharehouse } from '../wharehouses/entities/wharehouse.entity';
import { ServiceDevice } from './entities/service-device.entity';
import { Family } from '../families/entities/family.entity';
import { DevicesService } from '../common/services/devices.service';
import { DeviceDTO } from '../common/entities/device.dto';
import { WharehousesService } from '../wharehouses/wharehouses.service';
import { ServiceType } from '../common/enums/service-type.enum';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';
import { SchoolsService } from '../schools/schools.service';
import { School } from '../schools/entities/school.entity';
import { FamiliesService } from '../families/families.service';

@Injectable()
export class ServicesService {
  constructor(@InjectRepository(Service) private readonly servicesRepo: Repository<Service>,
              private readonly devicesService: DevicesService,
              private readonly wharehouseService: WharehousesService,
              private readonly familiesService: FamiliesService,
              @Inject(forwardRef(() => SchoolsService)) private readonly schoolsService: SchoolsService
  ) {
  }

  public async create(entityDto: ServiceDto, token: string, entity_id: number): Promise<Service> {
    const wharehouseExist = await this.wharehouseService.existID(entityDto.wharehouse_id, entity_id);
    if (wharehouseExist === 0 ) {
      throw new NotFoundException(404, 'the wharehouse id sent doesnt exist');
    }

    const schoolExist = await this.schoolsService.existID(entityDto.school_id);
    if (schoolExist === 0 ) {
      throw new NotFoundException(404, 'the school id sent doesnt exist');
    }

    const familiesExists = await this.familiesService.existIDs(entityDto.families, entity_id);
    if (familiesExists !== entityDto.families.length ) {
      throw new NotFoundException(404, 'the family id sent doesnt exist');
    }

    return new Promise<Service>(async (resolve, reject) => {

      this.servicesRepo.manager.transaction(async entityManger => {
        const twahrehouse = new Wharehouse();
        twahrehouse.id = entityDto.wharehouse_id;
        const tfe = new FiscalEntity();
        tfe.id = entity_id;
        const tsc = new School();
        tsc.id = entityDto.school_id;

        const updateResult = await this.servicesRepo.createQueryBuilder()
          .insert()
          .into(Service)
          .values({
            active: entityDto.active,
            name: entityDto.name,
            maintenance: entityDto.maintenance,
            school: tsc,
            wharehouse: twahrehouse,
            type: entityDto.type,
            fentity: tfe,
            service_id: entityDto.service_id
          })
          .execute();

        const id = updateResult.identifiers[0].id;
        await this.servicesRepo.createQueryBuilder()
          .delete()
          .from(ServiceDevice)
          .where(' serviceId = :id', { id })
          .execute();
        for (const device of entityDto.devices) {
          const tserve = new Service();
          tserve.id = id;
          const updateDevices = await this.servicesRepo.createQueryBuilder()
            .insert()
            .into(ServiceDevice)
            .values({ device_id: device, service: tserve })
            .execute();
        }

        await this.servicesRepo.createQueryBuilder()
          .delete()
          .from('family_services_service')
          .where({ serviceId: id })
          .execute();

        for (const family of entityDto.families) {
          const tfamily = new Family();
          tfamily.id = family;
          const updatFamily = await this.servicesRepo.createQueryBuilder()
            .relation(Service, 'families')
            .of(id)
            .add(tfamily);
        }

        resolve(await this.findByID(id, ['devices', 'families', 'wharehouse', 'school'], null, token, entity_id));
      }).catch(reason => {
        reject(reason);
      });
    });
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[],
                       type: ServiceType,
                       language_id: number, token: string, entity_id: number) {
    const servicesQB = this.servicesRepo.createQueryBuilder('service');

    if (limit > 0) {
      servicesQB.take(limit);
      servicesQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      servicesQB.orderBy('service.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'wharehouse')) {
      servicesQB.leftJoinAndSelect('service.wharehouse', 'wharehouse');
    }

    if (relateds.find(related => related === 'devices')) {
      servicesQB.leftJoinAndSelect('service.devices', 'service_device');
    }

    if (relateds.find(related => related === 'school')) {
      servicesQB.leftJoinAndSelect('service.school', 'school');
    }

    if (relateds.find(related => related === 'families')) {
      servicesQB.leftJoinAndSelect('service.families', 'families');
      servicesQB.leftJoinAndSelect('families.translations',
        'family_translation',
        language_id ? 'family_translation.language_id = :language_id' : '',
        { language_id });
    }

    servicesQB.where('service.fentityId = :entity_id', { entity_id });

    if (type) {
      servicesQB.andWhere('service.type = :type', { type });
    }

    const entities: any = await servicesQB.getManyAndCount();

    if (relateds.find(related => related === 'devices')) {
      for (const entity of entities[0]) {
        const tdevs: DeviceDTO[] = [];
        entity.devices = entity.devices.length > 0 ? entity.devices : [];
        for (const device of entity.devices) {
          try {
            const tdev = await this.devicesService.get(device.device_id, token);
            tdevs.push(tdev);
          } catch (errors) {
            if (errors[0].code === 404) {
              this.servicesRepo.createQueryBuilder()
                .delete()
                .from(ServiceDevice)
                .where('deviceId = :id', { id: device.device_id })
                .execute();
            }
          }
        }
        entity.devices = tdevs;
      }
    }

    return entities;
  }

  public async findByID(id: number, relateds: string[], language_id: number, token: string, entity_id: number): Promise<Service> {
    const persistedEntity = await this.servicesRepo.count({ id, fentity: { id: entity_id } });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }

    const serviceQB = this.servicesRepo.createQueryBuilder('service');

    if (relateds.find(related => related === 'wharehouse')) {
      serviceQB.leftJoinAndSelect('service.wharehouse', 'wharehouse');
    }

    if (relateds.find(related => related === 'devices')) {
      serviceQB.leftJoinAndSelect('service.devices', 'service_device');
    }

    if (relateds.find(related => related === 'school')) {
      serviceQB.leftJoinAndSelect('service.school', 'school');
    }

    if (relateds.find(related => related === 'families')) {
      serviceQB.leftJoinAndSelect('service.families', 'families');
      serviceQB.leftJoinAndSelect('families.translations',
        'family_translation',
        language_id ? 'family_translation.language_id = :language_id' : '',
        { language_id });
    }
    serviceQB.where({ id });
    const entity: any = await serviceQB.getOne();

    if (relateds.find(related => related === 'devices')) {
      const tdevs: DeviceDTO[] = [];
      for (const device of entity.devices) {
        try {
          const tdev = await this.devicesService.get(device.device_id, token);
          tdevs.push(tdev);
        } catch (errors) {
          if (errors[0].code === 404) {
            this.servicesRepo.createQueryBuilder()
              .delete()
              .from(ServiceDevice)
              .where('deviceId = :id', { id: device.device_id })
              .execute();
          }
        }
      }
      entity.devices = tdevs;
    }
    return entity;
  }

  public async existID(id: number, entity_id: number, type?: ServiceType): Promise<number> {
    return await this.servicesRepo.count({ id, fentity: { id: entity_id }, type });
  }

  public async update(id: number, entityDto: ServiceDto, token: string, entity_id: number): Promise<Service> {
    const persistedEntity = await this.servicesRepo.count({ id, fentity: { id: entity_id }  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }

    const schoolExist = await this.schoolsService.existID(entityDto.school_id);
    if (schoolExist === 0 ) {
      throw new NotFoundException(404, 'the school id sent doesnt exist');
    }

    const wharehouseExist = await this.wharehouseService.existID(entityDto.wharehouse_id, entity_id);
    if (wharehouseExist === 0 ) {
      throw new NotFoundException(404, 'the wharehouse id sent doesnt exist');
    }

    const familiesExists = await this.familiesService.existIDs(entityDto.families, entity_id);
    if (familiesExists !== entityDto.families.length ) {
      throw new NotFoundException(404, 'the family id sent doesnt exist');
    }

    return new Promise<Service>(async (resolve, reject) => {
      await this.servicesRepo.manager.transaction(async entityManger => {
        const updateResult = await this.servicesRepo.createQueryBuilder()
          .update(Service)
          .set({
            school: { id: entityDto.school_id },
            active: entityDto.active,
            name: entityDto.name,
            maintenance: entityDto.maintenance,
            wharehouse: { id: entityDto.wharehouse_id },
            type: entityDto.type,
            service_id: entityDto.service_id
          })
          .where('id = :id', { id })
          .execute();

        await this.servicesRepo.createQueryBuilder()
          .delete()
          .from(ServiceDevice)
          .where(' serviceId = :id', { id })
          .execute();
        for (const device of entityDto.devices) {
          const tserve = new Service();
          tserve.id = id;
          const updateDevices = await this.servicesRepo.createQueryBuilder()
            .insert()
            .into(ServiceDevice)
            .values({ device_id: device, service: tserve })
            .execute();
        }

        await this.servicesRepo.createQueryBuilder()
          .delete()
          .from('family_services_service')
          .where({ serviceId: id })
          .execute();

        for (const family of entityDto.families) {
          const tfamily = new Family();
          tfamily.id = family;
          const updatFamily = await this.servicesRepo.createQueryBuilder()
            .relation(Service, 'families')
            .of(id)
            .add(tfamily);
        }

        resolve(await this.findByID(id, ['devices', 'families', 'wharehouse', 'school'], null, token, entity_id));
      }).catch(reason => {
        reject(reason);
      });
    });

  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.servicesRepo.count({ id, fentity: { id: entity_id }  });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Service not found');
    }
    return await this.servicesRepo.delete(id);
  }

}
