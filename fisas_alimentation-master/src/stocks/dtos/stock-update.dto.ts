import { IsBoolean, IsDateString, IsNotEmpty } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class StockUpdateDto {

  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o lote nao esta preenchido' })
  lote: string;

  @ApiModelPropertyOptional()
  @IsDateString({message: 'expire é do tipo date'})
  expire: string;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}