import { IsBoolean, IsDateString, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class StockDto {

  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: ' o armazem nao pode ser vazio'})
  @IsNumber({}, { message: 'o id do armazem e um numero' })
  wharehouse_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o id do producto nao pode ser vazio'})
  @IsNumber({}, { message: 'o id do produto e um numero '})
  product_id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o quantity nao pode ser vazio'})
  @IsNumber({}, { message: 'o quantity e um numero '})
  quantity: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o lote nao esta preenchido' })
  lote: string;

  @ApiModelPropertyOptional()
  @IsDateString({message: 'expire é do tipo date'})
  expire: string;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}