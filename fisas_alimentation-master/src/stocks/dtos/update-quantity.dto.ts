import { IsEnum, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Operation } from '../entities/stock-operation.entity';

export class UpdateQuantityDto {

  id: number;

  @ApiModelProperty({ type: Operation, enum: Operation })
  @IsNotEmpty({ message: 'operation nao pode ser vazio'})
  @IsEnum(Operation, { message: 'operation e um enum'})
  operation: Operation;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o quantity nao pode ser vazio'})
  @IsNumber({}, { message: 'o quantity e um numero '})
  quantity: number;
}