import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Stock } from './stock.entity';

export enum Operation {
  IN = 'in',
  OUT = 'out',
  DISCARD = 'discard'
}

@Entity()
export class StockOperation  {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Stock, stock => stock.stock_operations, { nullable: false})
  stock: Stock;

  @Column({ nullable: true })
  order_id: number;

  @Column({ nullable: false, type: 'enum', enum: ['in', 'out', 'discard']})
  operation: Operation;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  quantity: number;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  quantity_before: number;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  quantity_after: number;

  @Column({ nullable: false, type: 'timestamp' })
  date: Date;

  @Column({ nullable: false})
  lote: string;

  @Column({ nullable: true})
  user_id: number;
}