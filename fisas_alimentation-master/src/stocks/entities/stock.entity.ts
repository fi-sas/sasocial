import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { Wharehouse } from '../../wharehouses/entities/wharehouse.entity';
import { Product } from '../../products/entities/product.entity';
import { BaseEntity } from '../../common/base.entity';
import { StockOperation } from './stock-operation.entity';

@Entity()
export class Stock extends BaseEntity {
  @ManyToOne(type => Wharehouse, wharehouse => wharehouse.stocks, { nullable: false})
  wharehouse: Wharehouse;

  @ManyToOne(type => Product, product => product.stocks, { nullable: false})
  product: Product;

  @Column({ nullable: false, unique: true})
  lote: string;

  @Column({ type: 'date'})
  expire: Date;

  @Column({ default: 0, nullable: false, type: 'decimal', precision: 10, scale: 2})
  quantity: number;

  @OneToMany(type => StockOperation, stockOps => stockOps.stock)
  stock_operations: Stock[];
}