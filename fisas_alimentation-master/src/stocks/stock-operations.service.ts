import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Operation, StockOperation } from './entities/stock-operation.entity';
import { Stock } from './entities/stock.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { Service } from '../service/entities/service.entity';
import { ModuleRef } from '@nestjs/core';

@Injectable()
export class StockOperationsService {

  constructor(@InjectRepository(StockOperation) private readonly stocksOpsRepo: Repository<StockOperation>,
              @InjectRepository(Stock) private readonly stocksRepo: Repository<Stock>
  ) {}


  public async create(stock_id: number, lote: string, operation: Operation,
                      quantity: number, quantity_after: number, quantity_before: number,
                      order_id?: number, user_id?: number): Promise<StockOperation> {
    let entity: StockOperation = new StockOperation();
    const tstock = new Stock();
    tstock.id = stock_id;

    entity.stock = tstock;
    entity.lote = lote;
    entity.operation = operation;
    entity.quantity = quantity;
    entity.quantity_after = quantity_after;
    entity.quantity_before = quantity_before;
    entity.order_id = order_id;
    entity.user_id = user_id;

    entity = this.stocksOpsRepo.create(entity);
    return await this.stocksOpsRepo.save(entity);
  }

  public async findAll(offset: number, limit: number) {
    return await this.stocksOpsRepo.findAndCount({ take: limit, skip: offset});
  }

  public async  findByStock(stock_id: number, limit: number, offset: number, sort: string): Promise<[StockOperation[], number]> {
    const countResult = await this.stocksRepo.count( { id: stock_id});
    if ( countResult === 0 ) {
      throw new NotFoundException(404, 'the stock_id ' + stock_id + ' was not found');
    }

    const opsQB = this.stocksOpsRepo.createQueryBuilder('op');

    if (limit > 0) {
      opsQB.take(limit);
      opsQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      opsQB.orderBy('op.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    return await opsQB
      .select()
      .where('stockId = :stock_id', { stock_id })
      .getManyAndCount();

  }

  public async  findByWharehouse(wharehouse_id: number, offset: number, limit: number): Promise<[StockOperation[], number]> {
    const ops = this.stocksOpsRepo.createQueryBuilder('ops');
    ops.leftJoinAndSelect('ops.stock', 'stock',
      'stock.wharehouseId = :wharehouse_id', { wharehouse_id });
    ops.leftJoinAndSelect('stock.product', 'product');
    ops.leftJoinAndSelect('product.translations', 'product_translation');

    if (limit > 0) {
      ops.take(limit);
      ops.skip(offset);
    }

    return await ops.getManyAndCount();
  }
}
