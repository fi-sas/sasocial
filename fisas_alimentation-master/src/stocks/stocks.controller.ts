import { Body, Controller, Delete, Get, Headers, HttpCode, HttpStatus, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiImplicitQuery,
  ApiImplicitHeader, ApiProduces, ApiUseTags, ApiOperation } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { StocksService } from './stocks.service';
import { Stock } from './entities/stock.entity';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { StockDto } from './dtos/stock.dto';
import { StockUpdateDto } from './dtos/stock-update.dto';
import { UpdateQuantityDto } from './dtos/update-quantity.dto';
import { StockOperationsService } from './stock-operations.service';
import { StockOperation } from './entities/stock-operation.entity';
import { Scopes } from '../common/decorators/scope.decorator';
import { TokenGuard } from '../common/guards/token.guard';
import { User } from '../common/decorators/user.decorator';
import { ForbiddenException } from '../common/exceptions/forbidden.exception';
import { WharehousesService } from '../wharehouses/wharehouses.service';
import { Token } from '../common/decorators/token.decorator';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Stocks')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/stocks')
export class StocksController {

  constructor(private stocksService: StocksService,
              private stocksOpsService: StockOperationsService,
              private readonly wharehousesService: WharehousesService) {}

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products"', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'wharehouse_id',
    description: 'Finds only stocks on the wharehouse of the id sended', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @User() user, @Query() query, @Headers('x-language-id') language_id) {

    const result = await this.wharehousesService.hasAccess(query.wharehouse_id, user.id);
    if (!result) {
      throw new ForbiddenException(912, 'You dont have permission to access to this wharhouse');
    }
// TODO METER ISTO NO WHAREHOUSE CONTROLLER OU FAZER SEGUIMNENTO DELE

    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.stocksService.findAll(query.wharehouse_id, query.offset, query.limit, query.sort, relateds, language_id);
    return new Response<Stock[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
        pages: Math.round(entities[1] / query.limit),
      }
    );
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products"', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the stock', name: 'id', type: Number })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                        @Query() query, @Headers('x-language-id') language_id) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.stocksService.findByID(id, relateds, language_id);
    return new Response<Stock>(ResponseStatus.OK, entity);
  }

  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'id of the stock', name: 'id', type: Number })
  @ApiOperation({ title: 'Stock Operations',
    description: 'Returns all operations like inn out and discards of the stock with the id sended in parameter' })
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Scopes('stocks')
  @Get(':id/operations')
  public async findStockOperations(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                                   @Query() query): Promise<Response<StockOperation[]>> {
    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    const entities = await this.stocksOpsService.findByStock(id,
      query.offset, query.limit,
      query.sort);
    return new Response<StockOperation[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
        pages: Math.round(entities[1] / query.limit),
      }
    );
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products"', type: String,
  })
  @ApiImplicitParam({ required: true, description: 'lote of the stock', name: 'lote', type: String })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get('lote/:lote')
  public async findByLote(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('lote') lote,
                          @Query() query, @Headers('x-language-id') language_id) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.stocksService.findByLote(lote, relateds, language_id);
    return new Response<Stock>(ResponseStatus.OK, entity);
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "products"', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @ApiImplicitQuery({
    required: false, name: 'wharehouse_id',
    description: 'Restricts the search to a wharehouse', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'product_id',
    description: 'Restricts the search to a product', type: Number,
  })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get('stocks/expired')
  public async findExpired(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id) {
    if (! query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (! query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';
    if (!query.hasOwnProperty('wharehouse_id')) query.wharehouse_id = 0;
    if (!query.hasOwnProperty('product_id')) query.product_id = 0;

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'products');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entities = await this.stocksService.findExpired(query.product_id, query.wharehouse_id,
      query.offset, query.limit,
      query.sort, relateds, language_id, entity_id);
    return new Response<Stock[]>(ResponseStatus.OK, entities[0],
      {
        total: entities[1],
        pages: Math.round(entities[1] / query.limit),
      }
    );
  }

  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @User() user,
                      @Body() entity: StockDto, @Token() token) {
    const result = await this.wharehousesService.hasAccess(entity.wharehouse_id, user.id);
    if (!result) {
      throw new ForbiddenException(912, 'You dont have permission to access to this wharehouse');
    }

    const createdEntity = await this.stocksService.create(entity, user.id, entity_id, token);
    return new Response<Stock>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the stock', name: 'id', type: Number })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id, @Body() entity: StockUpdateDto) {

    const updatedEntity = await this.stocksService.update(id, entity);
    return new Response<Stock>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the stock', name: 'id', type: Number })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Put(':id/quantity')
  public async updateQuantity(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                              @User() user, @Body() entity: UpdateQuantityDto) {
    const updatedEntity = await this.stocksService.changeQuantity(id, entity, user.id);
    return new Response<Stock>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the stock', name: 'id', type: Number })
  @Scopes('stocks')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.stocksService.delete(id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }
}
