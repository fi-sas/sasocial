import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { StockOperation } from './entities/stock-operation.entity';
import { StocksController } from './stocks.controller';
import { StocksService } from './stocks.service';
import { StockOperationsService } from './stock-operations.service';
import { ProductsModule } from '../products/products.module';
import { WharehousesModule } from '../wharehouses/wharehouses.module';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    TypeOrmModule.forFeature([
      Stock,
      StockOperation
    ]),
    ProductsModule,
    forwardRef(() => WharehousesModule)
  ],
  controllers: [
    StocksController
  ],
  providers: [
    StocksService,
    StockOperationsService
  ],
  exports: [
    StocksService,
    StockOperationsService
  ]
})
export class StocksModule {}