import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Stock } from './entities/stock.entity';
import { NotFoundException } from '../common/exceptions/not-found.exception';
import { StockDto } from './dtos/stock.dto';
import { StockOperationsService } from './stock-operations.service';
import { ProductsService } from '../products/products.service';
import { WharehousesService } from '../wharehouses/wharehouses.service';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ResponseError } from '../common/response.model';
import { Operation } from './entities/stock-operation.entity';
import { StockUpdateDto } from './dtos/stock-update.dto';
import { UpdateQuantityDto } from './dtos/update-quantity.dto';

@Injectable()
export class StocksService {
  constructor(@InjectRepository(Stock) private readonly stocksRepo: Repository<Stock>,
              private produtsService: ProductsService,
              private wharehousesService: WharehousesService,
              private stockOpeService: StockOperationsService,
  ) {
  }

  public async create(entityDto: StockDto, user_id, entity_id: number, token: string): Promise<Stock> {
    const tproduct = await this.produtsService.findById(entityDto.product_id, [], null, entity_id, token);
    const twharehouse = await this.wharehousesService.findByID(entityDto.wharehouse_id, [], null, entity_id);

    const persistedStockWithLote = await this.stocksRepo.count({ lote: entityDto.lote });
    if (persistedStockWithLote > 0) {
      throw new ValidationException([new ResponseError(4545, 'already exists a stock with the same lote')]);
    }

    return new Promise<Stock>(async (resolve, reject) => {
      let id: number = 0;
      await this.stocksRepo.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(Stock)
          .values({
            product: tproduct,
            wharehouse: twharehouse,
            quantity: entityDto.quantity,
            lote: entityDto.lote,
            expire: new Date(entityDto.expire),
            active: entityDto.active,
          })
          .execute();
        id = insertResult.identifiers[0].id;

      }).catch(reason => {
        reject(reason);
      });
      this.stockOpeService.create(id, entityDto.lote, Operation.IN, entityDto.quantity, entityDto.quantity, 0, null, user_id);
      try {
        resolve(await this.findByID(id, ['products'], null));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async findAll(wharehouse_id: number, offset: number, limit: number, sort: string, relateds: string[],
                       language_id: number): Promise<[Stock[], number]> {
    const stocksQB = this.stocksRepo.createQueryBuilder('stock');

    if (limit > 0) {
      stocksQB.take(limit);
      stocksQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      stocksQB.orderBy('stock.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'products')) {
      stocksQB.leftJoinAndSelect('stock.product', 'product');
      stocksQB.leftJoinAndSelect('product.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (!isNaN(wharehouse_id)) {
      stocksQB.where('stock.wharehouseId = :wharehouse_id', { wharehouse_id });
    }

    return await stocksQB.getManyAndCount();
  }

  public async findByID(id: number, relateds: string[], language_id: number): Promise<Stock> {
    const persistedEntity = await this.stocksRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const stockQB = this.stocksRepo.createQueryBuilder('stock');

    if (relateds.find(related => related === 'products')) {
      stockQB.leftJoinAndSelect('stock.product', 'product');
      stockQB.leftJoinAndSelect('product.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    stockQB.where('stock.id = :id', { id });
    return await stockQB.getOne();
  }

  public async findByLote(lote: string, relateds: string[], language_id: number): Promise<Stock> {
    const persistedEntity = await this.stocksRepo.count({ lote });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }

    const stockQB = this.stocksRepo.createQueryBuilder('stock');

    if (relateds.find(related => related === 'products')) {
      stockQB.leftJoinAndSelect('stock.product', 'product');
      stockQB.leftJoinAndSelect('product.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    stockQB.where('stock.lote = :lote', { lote });
    return await stockQB.getOne();
  }

  public async findExpired(product_id: number, wharehouse_id: number,
                           offset: number, limit: number, sort: string,
                           relateds: string[], language_id: number, entity_id: number): Promise<[Stock[], number]> {
    if (wharehouse_id) {
      const countWharehouse = await this.wharehousesService.existID(wharehouse_id, entity_id);
      if (countWharehouse === 0 ) {
        throw new NotFoundException(404, 'is not possible to find a wahrehouse with the id ' + wharehouse_id);
      }
    }

    if (product_id) {
      const countProduct = await this.produtsService.existID(product_id, entity_id);
      if (countProduct === 0 ) {
        throw new NotFoundException(404, 'is not possible to find a product with the id ' + product_id);
      }
    }

    const stocksQB = this.stocksRepo.createQueryBuilder('stock');

    if (limit > 0) {
      stocksQB.take(limit);
      stocksQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      stocksQB.orderBy('stock.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'products')) {
      stocksQB.leftJoinAndSelect('stock.product', 'product');
      stocksQB.leftJoinAndSelect('product.translations',
        'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    stocksQB.where('stock.expire <= NOW()');

    if (wharehouse_id) {
      stocksQB.andWhere('stock.wharehouse_id = :wharehouse_id', { wharehouse_id });
    }

    if (product_id) {
      stocksQB.andWhere('stock.product_id = :product_id', { product_id });
    }

    return await stocksQB.getManyAndCount();
  }

  public async existID(id: number): Promise<number> {
    return await this.stocksRepo.count({ id });
  }

  public async update(id: number, entityDto: StockUpdateDto) {
    const persistedEntity = await this.stocksRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'ENtity não existe');
    }
    entityDto.id = id;

    return new Promise<Stock>(async (resolve, reject) => {
      await this.stocksRepo.manager.transaction(async entityManger => {
        const updateResult = await entityManger.createQueryBuilder()
          .update(Stock)
          .set({
            lote: entityDto.lote,
            expire: new Date(entityDto.expire),
            active: entityDto.active,
          })
          .where('stock.id = :id', { id })
          .execute();

      }).catch(reason => {
        reject(reason);
      });

      try {
        resolve(await this.findByID(id, ['products'], null));
      } catch (e) {
        reject(e);
      }
    });
  }

  public async changeQuantity(id: number, entityDto: UpdateQuantityDto, user_id: number): Promise<Stock> {
    const tstock = await this.findByID(id, ['products'], null);

    let quantity: number = 0;
    if (entityDto.operation !== Operation.IN) {
      quantity = Number(tstock.quantity) - Number(entityDto.quantity);
    } else {
      quantity = Number(tstock.quantity) + Number(entityDto.quantity);
    }

    return new Promise<Stock>(async (resolve, reject) => {
      await this.stocksRepo.manager.transaction(async entityManger => {
        const updateResult = await entityManger.createQueryBuilder()
          .update(Stock)
          .set({
            quantity
          })
          .where('stock.id = :id', { id })
          .execute();

      }).catch(reason => {
        reject(reason);
      });
      const after_stock = await this.findByID(id, ['products'], null);
      this.stockOpeService.create(tstock.product.id, tstock.lote, entityDto.operation, entityDto.quantity,
        after_stock.quantity, tstock.quantity, null, user_id);
      try {
        resolve(after_stock);
      } catch (e) {
        reject(e);
      }
    });
  }

  public async delete(id: number) {
    const persistedEntity = await this.stocksRepo.count({ id });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Entity não existe');
    }
    return await this.stocksRepo.delete(id);
  }
}