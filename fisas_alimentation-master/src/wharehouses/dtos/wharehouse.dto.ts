import {  IsBoolean, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class WharehouseDto {
  id: number;

  @ApiModelProperty()
  @IsNotEmpty({ message: 'o nome não pode estar vazio'})
  name: string;

  @ApiModelProperty()
  @IsBoolean({ message: 'active não esta corretamente preenchido'})
  active: boolean;
}