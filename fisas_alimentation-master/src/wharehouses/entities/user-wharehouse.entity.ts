import { Column, Entity, ManyToOne } from 'typeorm';
import { Wharehouse } from './wharehouse.entity';

@Entity()
export class UserWharehouse {

  @ManyToOne(type => Wharehouse, wh => wh.users,  { primary: true, cascade: true, onDelete: 'CASCADE' })
  wharehouse: Wharehouse;

  @Column('int', { primary: true, nullable: false })
  user_id: number;

  @Column('int', { nullable: false })
  created_by_user_id: number;

  @Column({ nullable: false, default: false })
  manager: boolean;
}