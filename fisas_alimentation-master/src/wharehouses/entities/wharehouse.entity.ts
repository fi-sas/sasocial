import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../common/base.entity';
import { Stock } from '../../stocks/entities/stock.entity';
import { Service } from '../../service/entities/service.entity';
import { UserWharehouse } from './user-wharehouse.entity';
import { FiscalEntity } from '../../entity/entities/fiscal-entity.entity';

@Entity()
export class Wharehouse extends BaseEntity {
  @Column({ nullable: false })
  name: string;

  @OneToMany(type => Stock, stock => stock.wharehouse)
  stocks: Stock[];

  @OneToMany(type => Service, service => service.wharehouse)
  services: Service[];

  @OneToMany(type => UserWharehouse, uwh => uwh.wharehouse)
  users: UserWharehouse[];

  @ManyToOne(type => FiscalEntity, fentity => fentity.id, { nullable: false, onDelete: 'RESTRICT' })
  fentity: FiscalEntity;
}