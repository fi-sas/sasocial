import { Body, Controller, Delete, forwardRef, Get, Headers, HttpCode, HttpStatus, Inject, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiImplicitParam, ApiProduces, ApiUseTags, ApiImplicitQuery, ApiImplicitHeader } from '@nestjs/swagger';
import { CONFIGURATIONS } from '../app.config';
import { WharehousesService } from './wharehouses.service';
import { Wharehouse } from './entities/wharehouse.entity';
import { WharehouseDto } from './dtos/wharehouse.dto';
import { ParseIntPipe } from '../common/pipes/parse-int.pipe';
import { Response, ResponseError, ResponseStatus } from '../common/response.model';
import { TokenGuard } from '../common/guards/token.guard';
import { Scopes } from '../common/decorators/scope.decorator';
import { StockOperationsService } from '../stocks/stock-operations.service';
import { StockOperation } from '../stocks/entities/stock-operation.entity';
import { Token } from '../common/decorators/token.decorator';
import { UserDto } from '../common/entities/user.dto';
import { User } from '../common/decorators/user.decorator';
import { FiscalEntityService } from '../entity/fiscal-entity.service';
import { ForbiddenException } from '../common/exceptions/forbidden.exception';
import { ValidationException } from '../common/exceptions/validation.exception';

@ApiProduces('application/json')
@ApiConsumes('application/json')
@ApiBearerAuth()
@UseGuards(TokenGuard)
@ApiUseTags('Wharehouses')
@Controller(CONFIGURATIONS.URL_PREFIX + '/:entity_id/wharehouses')
export class WharehousesController {

  constructor(private wharehousesService: WharehousesService,
              @Inject(forwardRef(() => StockOperationsService))private stocksOpsService: StockOperationsService,
              private readonly fiscalEntService: FiscalEntityService) {}

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "stocks", "products", "services" ', type: String
  })
  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'sort',
    description: 'A comma-separated list of sorting parameters of main object, with "-" prepared for DESC order.', type: String,
  })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get()
  public async findAll(@Param('entity_id', new ParseIntPipe()) entity_id, @Query() query, @Headers('x-language-id') language_id) {
    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'stocks' &&
        relation !== 'products' &&
        relation !== 'services');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const products = await this.wharehousesService.findAll(query.offset, query.limit, query.sort, relateds, language_id, entity_id);
    return new Response<Wharehouse[]>(ResponseStatus.OK, products[0], {
      total: products[1],
    });
  }

  @ApiImplicitHeader({
    required: false, name: 'X-Language-ID', description: 'Language ID of the request - will impact on the translations content. ' +
      'If no ID is sent, returned data will contain translations for all languages. Otherwise, ' +
      'returned data will be filtered by received ID and will contain translations for the specified language',
  })
  @ApiImplicitQuery({
    required: false, name: 'withRelated',
    description: 'Request related entities to be included on the data objects (comma-separet list),' +
      'Possible values include "stocks", "products", "services" ', type: String
  })
  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get(':id')
  public async findById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                        @Query() query, @Headers('x-language-id') language_id) {

    let relateds = [];
    if (query.hasOwnProperty('withRelated')) {
      relateds = query.withRelated.split(',');
      const relationErro = relateds.find(relation =>
        relation !== 'stocks' &&
        relation !== 'products' &&
        relation !== 'services');
      if (relationErro)
        throw new ValidationException([new ResponseError(112, relationErro + ' is not defined on the model.')]);
    }

    const entity = await this.wharehousesService.findByID(id, relateds, language_id, entity_id);
    return new Response<Wharehouse>(ResponseStatus.OK, entity);
  }

  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get(':id/operations')
  public async OperationById(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id,
                             @Query() query, @Headers('x-language-id') language_id) {
    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;
    if (!query.hasOwnProperty('sort')) query.sort = '-id';

    const ops = await this.stocksOpsService.findByWharehouse(id, query.offset, query.limit);
    return new Response<StockOperation[]>(ResponseStatus.OK, ops[0], {
      total: ops[1],
    });
  }

  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post()
  public async create(@Param('entity_id', new ParseIntPipe()) entity_id, @Body() entity: WharehouseDto) {
    const createdEntity = await this.wharehousesService.create(entity, entity_id);
    return new Response<Wharehouse>(ResponseStatus.OK, createdEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Put(':id')
  public async update(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id, @Body() entity: WharehouseDto) {
    const updatedEntity = await this.wharehousesService.update(id, entity, entity_id);
    return new Response<Wharehouse>(ResponseStatus.OK, updatedEntity);
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async delete(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id', new ParseIntPipe()) id) {
    const result = await this.wharehousesService.delete(id, entity_id);
    if (result) {
      return new Response(ResponseStatus.OK);
    }

    return new Response(ResponseStatus.FAIL);
  }

  @ApiImplicitQuery({
    required: false, name: 'offset',
    description: 'Offset from which the items should be returned - defaults to ' + CONFIGURATIONS.DEFAULT_OFFSET + '.'
    , type: Number,
  })
  @ApiImplicitQuery({
    required: false, name: 'limit',
    description: 'Limits the number of items to be returned - defaults to ' +
      CONFIGURATIONS.DEFAULT_LIMIT + '. If no pagination is to be applied, use -1.', type: Number,
  })
  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Get(':id/users')
  public async getUsers(@Param('entity_id', new ParseIntPipe()) entity_id,
                        @User() user, @Param('id', new ParseIntPipe()) id, @Query() query, @Token() token) {
    const result = await this.fiscalEntService.isManager(entity_id, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    if (!query.hasOwnProperty('offset')) query.offset = CONFIGURATIONS.DEFAULT_OFFSET;
    if (!query.hasOwnProperty('limit')) query.limit = CONFIGURATIONS.DEFAULT_LIMIT;

    const enteties = await this.wharehousesService.getUsers(id, query.offset, query.limit, token, entity_id);
    return new Response<UserDto[]>(ResponseStatus.OK, enteties[0],
      { total: enteties[1] }
    );
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id_wharehouse', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the user', name: 'id_user', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Post(':id_wharehouse/users/:id_user')
  public async addUser(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id_wharehouse', new ParseIntPipe()) id_wharehouse,
                       @Param('id_user', new ParseIntPipe()) id_user, @User() user, @Token() token) {
    const result = await this.fiscalEntService.isManager(entity_id, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    return new Response<UserDto>(ResponseStatus.OK, await this.wharehousesService.addUser(id_wharehouse, id_user, user.id, token, entity_id));
  }

  @ApiImplicitParam({ required: true, description: 'id of the entity', name: 'id_wharehouse', type: Number })
  @ApiImplicitParam({ required: true, description: 'id of the user', name: 'id_user', type: Number })
  @Scopes('wharehouses')
  @ApiImplicitParam({ required: true, description: 'id of the fiscal entity', name: 'entity_id', type: Number })
  @Delete(':id_wharehouse/users/:id_user')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async removeUser(@Param('entity_id', new ParseIntPipe()) entity_id, @Param('id_wharehouse', new ParseIntPipe()) id_wharehouse,
                          @Param('id_user', new ParseIntPipe()) id_user, @User() user, @Token() token) {
    const result = await this.fiscalEntService.isManager(entity_id, user.id);
    if (!result) {
      throw new ForbiddenException(911, 'You have to be the manager of this entity to access this resource');
    }

    return new Response<any>(ResponseStatus.OK, await this.wharehousesService.removeUser(id_wharehouse, id_user, entity_id));
  }
}