import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wharehouse } from './entities/wharehouse.entity';
import { Service } from '../service/entities/service.entity';
import { WharehousesService } from './wharehouses.service';
import { WharehousesController } from './wharehouses.controller';
import { StocksModule } from '../stocks/stocks.module';
import { UserWharehouse } from './entities/user-wharehouse.entity';
import { FiscalEntityModule } from '../entity/fiscal-entity.module';
import { CommomModule } from '../common/commom.module';

@Module({
  imports: [
    CommomModule,
    FiscalEntityModule,
    forwardRef(() => StocksModule),
    TypeOrmModule.forFeature([
      Wharehouse,
      UserWharehouse,
      Service
    ])
  ],
  controllers: [
    WharehousesController
  ],
  providers: [
    WharehousesService
  ],
  exports: [
    WharehousesService
  ]
})
export class WharehousesModule {}