import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Wharehouse } from './entities/wharehouse.entity';
import { WharehouseDto } from './dtos/wharehouse.dto';
import { UserDto } from '../common/entities/user.dto';
import { ValidationException } from '../common/exceptions/validation.exception';
import { ResponseError } from '../common/response.model';
import { UserWharehouse } from './entities/user-wharehouse.entity';
import { UsersService } from '../common/services/users.service';
import { FiscalEntity } from '../entity/entities/fiscal-entity.entity';

@Injectable()
export class WharehousesService {
  constructor(@InjectRepository(Wharehouse) private readonly wharehouseRepository: Repository<Wharehouse>,
              @InjectRepository(UserWharehouse) private readonly userWharehouseRepository: Repository<UserWharehouse>,
              private readonly userService: UsersService
  ) {}

  public async create(entityDto: WharehouseDto, entity_id: number): Promise<Wharehouse> {
    let id: number = 0;
    const tfe = new FiscalEntity();
    tfe.id = entity_id;

    return new Promise<Wharehouse>(async (resolve, reject) => {
      await this.wharehouseRepository.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(Wharehouse)
          .values({
            active: entityDto.active,
            name: entityDto.name,
            fentity: tfe
          })
          .execute();

        id = insertResult.identifiers[0].id;
      }).catch(reason =>  {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['stocks', 'products', 'services'], null, entity_id));
      } catch ( e ) {
        reject(e);
      }
    });
  }

  public async findAll(offset: number, limit: number, sort: string, relateds: string[], language_id: number, entity_id: number) {
    const wharehousesQB = this.wharehouseRepository.createQueryBuilder('wh');

    if (limit > 0) {
      wharehousesQB.take(limit);
      wharehousesQB.skip(offset);
    }

    if (sort) {
      const sort_replaced = sort.replace('-', '');
      wharehousesQB.orderBy('wh.' + sort_replaced, sort_replaced !== sort ? 'DESC' : 'ASC');
    }

    if (relateds.find(related => related === 'stocks')) {
      wharehousesQB.leftJoinAndSelect('wh.stocks', 'stock');
    }

    if (relateds.find(related => related === 'products')) {
      wharehousesQB.leftJoinAndSelect('stock.product', 'product');
      wharehousesQB.leftJoinAndSelect('product.translations', 'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'services')) {
      wharehousesQB.leftJoinAndSelect('wh.services', 'service');

    }

    wharehousesQB.where('wh.fentityId = :entity_id', { entity_id });

    return await wharehousesQB.getManyAndCount();
  }

  public async findByID(id: number, relateds: string[], language_id: number, entity_id: number): Promise<Wharehouse> {
    const persistedEntity = await this.wharehouseRepository.count({ id , fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Armazem não existe');
    }

    const wharehouseQB = this.wharehouseRepository.createQueryBuilder('wh');

    if (relateds.find(related => related === 'stocks')) {
      wharehouseQB.leftJoinAndSelect('wh.stocks', 'stock');
    }

    if (relateds.find(related => related === 'products')) {
      wharehouseQB.leftJoinAndSelect('stock.product', 'product');
      wharehouseQB.leftJoinAndSelect('product.translations', 'product_translation',
        language_id ? 'product_translation.language_id = :language_id' : '',
        { language_id });
    }

    if (relateds.find(related => related === 'services')) {
      wharehouseQB.leftJoinAndSelect('wh.services', 'service');

    }

    wharehouseQB.where('wh.id = :id', { id });
    return await wharehouseQB.getOne();
  }

  public async existID(id: number, entity_id: number): Promise<number> {
    return await this.wharehouseRepository.count({ id, fentity: { id: entity_id} });
  }

  public async update(id: number, entityDto: WharehouseDto, entity_id: number) {
    const persistedEntity = await this.wharehouseRepository.count({ id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Armazem não existe');
    }
    entityDto.id = id;

    return new Promise<Wharehouse>(async (resolve, reject) => {
      await this.wharehouseRepository.manager.transaction(async entityManger => {
        const updateResult = await entityManger.createQueryBuilder()
          .update(Wharehouse)
          .set({
            active: entityDto.active,
            name: entityDto.name
          })
          .where(' id = :id ', { id })
          .execute();

      }).catch(reason =>  {
        reject(reason);
      });
      try {
        resolve(await this.findByID(id, ['stocks', 'products', 'services'], null, entity_id));
      } catch ( e ) {
        reject(e);
      }
    });
  }

  public async delete(id: number, entity_id: number) {
    const persistedEntity = await this.wharehouseRepository.count({ id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Armazem não existe');
    }
    return await this.wharehouseRepository.delete(id);
  }

  public async getUsers(id: number, offset: number, limit: number, token: string, entity_id: number): Promise<[UserDto[], number]> {
    const persistedEntity = await this.wharehouseRepository.count({ id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Armazem não existe');
    }

    const uwhQB = this.userWharehouseRepository.createQueryBuilder('uwh');
    if (limit > 0) {
      uwhQB.take(limit);
      uwhQB.skip(offset);
    }

    uwhQB.where('wharehouseId = :id', { id });

    const entities = await uwhQB.getManyAndCount();

    const tusers: UserDto[] = [];
    for (const entity of entities[0]) {
      try {
        const tuser = await this.userService.get(entity.user_id, token);
        tusers.push(tuser);
      } catch (errors) {
        if (errors[0].code === 404) {
          this.userWharehouseRepository.createQueryBuilder()
            .delete()
            .from(UserWharehouse)
            .where('user_id = :id', { id: entity.user_id })
            .execute();
          entities[1]--;
        }
      }
    }

    return [tusers, entities[1]];
  }

  public async addUser(wharehouse_id: number, user_id: number, created_by_user_id: number, token: string, entity_id: number): Promise<UserDto> {
    const wharehouse = await this.findByID(wharehouse_id, [], null, entity_id);
    let user = null;
    try {
      user = await this.userService.get(user_id, token);
    } catch (e) {
      throw new HttpException(e, e[0].code );
    }
    if (user.active === false) {
      throw new ValidationException([new ResponseError(400, 'the user is not active')]);
    }

    if (await this.userWharehouseRepository.count({ user_id, wharehouse: { id: wharehouse_id }}) === 0) {
      await this.userWharehouseRepository.manager.transaction(async entityManger => {
        const insertResult = await entityManger.createQueryBuilder()
          .insert()
          .into(UserWharehouse)
          .values({
            wharehouse,
            user_id,
            created_by_user_id,
          }).execute();
      }).catch(reason => {
        throw new HttpException(reason, 500);
      });
    }

    return user;
  }

  public async removeUser(wharehouse_id: number, user_id: number, entity_id: number): Promise<any> {
    const persistedEntity = await this.wharehouseRepository.count({ id: wharehouse_id, fentity: { id: entity_id} });
    if (persistedEntity === 0) {
      throw new NotFoundException(404, 'Armazem não existe');
    }

    await this.userWharehouseRepository.createQueryBuilder()
      .delete()
      .from(UserWharehouse)
      .where(' wharehouseId = :wharehouse_id AND user_id = :user_id', { wharehouse_id, user_id }).execute();

    return true;
  }

  public  async hasAccess(wharehouse_id: number, user_id: number): Promise<boolean> {
    const result = await this.userWharehouseRepository.createQueryBuilder()
      .where('user_id = :user_id AND wharehouseId = :wharehouse_id', { wharehouse_id, user_id }).getCount();

    if (result) {
      return true;
    } else {
      return false;
    }
  }
}