# [1.5.0](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.8...v1.5.0) (2022-04-29)


### Features

* **users_defaults:** add bnew endpoint to change users configs ([2ccf232](https://gitlab.com/fi-sas/fisas_packs/commit/2ccf23295f48225859b193969fc79bcbb442a9e2))

## [1.4.8](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.7...v1.4.8) (2022-03-07)


### Bug Fixes

* **users_packs:** remove default user_meals withRelated ([c81ce1a](https://gitlab.com/fi-sas/fisas_packs/commit/c81ce1a2e939ed12b3008c9fb5f15d30809fa774))

## [1.4.7](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.6...v1.4.7) (2022-01-17)


### Bug Fixes

* new version ([889f1a6](https://gitlab.com/fi-sas/fisas_packs/commit/889f1a6f6179cf43ebfb383a0f5d0949e786fe58))

## [1.4.6](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.5...v1.4.6) (2022-01-14)


### Bug Fixes

* **user_meals:** fix the reservation withou propoer meal ([7f3549a](https://gitlab.com/fi-sas/fisas_packs/commit/7f3549a4f5decf57d0f9e8ab3fcf2206ff80f364))


### Reverts

* **user_packs:** revert allow create user_pack validation ([7f30d01](https://gitlab.com/fi-sas/fisas_packs/commit/7f30d01ac6e6754b3fa5195a6254b504fa28dbc5))

## [1.4.5](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.4...v1.4.5) (2022-01-12)


### Bug Fixes

* **user_pack:** on creation validate only the current entity ([c5dcee8](https://gitlab.com/fi-sas/fisas_packs/commit/c5dcee8265cc343a09815a0fa610bbcca2fce032))

## [1.4.4](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.3...v1.4.4) (2022-01-12)


### Bug Fixes

* **user_packs:** rearrange order of clearcache user defaults ([5aec51f](https://gitlab.com/fi-sas/fisas_packs/commit/5aec51fc83848609cbf7061157622142260d3da2))

## [1.4.3](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.2...v1.4.3) (2022-01-12)


### Bug Fixes

* **user_menu:** return new user defaults after save ([4457e33](https://gitlab.com/fi-sas/fisas_packs/commit/4457e3343a8f2596e4c70068c8a80c66aaa3b5ec))

## [1.4.2](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.1...v1.4.2) (2022-01-12)


### Bug Fixes

* **user_defaults:** remove cache after update ([c69868e](https://gitlab.com/fi-sas/fisas_packs/commit/c69868ec74d96293e6d7ef6be3749a84dbc35d6b))

## [1.4.1](https://gitlab.com/fi-sas/fisas_packs/compare/v1.4.0...v1.4.1) (2021-11-22)


### Bug Fixes

* **user_defaults:** update reservations for new defaults ([01006d7](https://gitlab.com/fi-sas/fisas_packs/commit/01006d79093113fbe5902be662115e66a3fb9b23))

# [1.4.0](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.5...v1.4.0) (2021-11-17)


### Features

* **user_packs:** bulk create ([f54887e](https://gitlab.com/fi-sas/fisas_packs/commit/f54887e38cfc44016786d16e9edc1273747d1130))

## [1.3.5](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.4...v1.3.5) (2021-11-12)


### Bug Fixes

* **user_meals:**  flat promisse all ([9fa2b69](https://gitlab.com/fi-sas/fisas_packs/commit/9fa2b69469e9629e7a386275ae8a70d620ff845c))
* **user_meals:** packs debug ([8ebdf2c](https://gitlab.com/fi-sas/fisas_packs/commit/8ebdf2c61d221cb970cc59eeff4b254dc5fb4465))

## [1.3.4](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.3...v1.3.4) (2021-11-10)


### Bug Fixes

* **menu:** pre confirm on reserved ([a04e23a](https://gitlab.com/fi-sas/fisas_packs/commit/a04e23a802a91b07ec04afbbb32e26a17614eaa4))
* **user-meals:** only reserve meals from active packs ([63de2a4](https://gitlab.com/fi-sas/fisas_packs/commit/63de2a4250cc691e18132a97af8785abd05b9364))

## [1.3.3](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.2...v1.3.3) (2021-11-10)


### Bug Fixes

* **packs:** multiple preconfirm on menu ([d8b23cc](https://gitlab.com/fi-sas/fisas_packs/commit/d8b23cc849480242dbbdd451c9473995a9d1980a))

## [1.3.2](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.1...v1.3.2) (2021-11-10)


### Bug Fixes

* **menu:** save service_id on meal change ([4962904](https://gitlab.com/fi-sas/fisas_packs/commit/49629043465e38aa7adbe265037144de569974fe))
* **pakcs-meals:** send service id of menu_dish ([4d33b38](https://gitlab.com/fi-sas/fisas_packs/commit/4d33b3815d8f73614d4302cb8bda4a61fd60c38f))

## [1.3.1](https://gitlab.com/fi-sas/fisas_packs/compare/v1.3.0...v1.3.1) (2021-11-10)


### Bug Fixes

* **user_meals:** clear cache after reservations ([717ba0a](https://gitlab.com/fi-sas/fisas_packs/commit/717ba0a96aee8677ed34c94c5d8ebe8e51924726))

# [1.3.0](https://gitlab.com/fi-sas/fisas_packs/compare/v1.2.0...v1.3.0) (2021-11-09)


### Features

* **packs:** add sell to public option ([75cecca](https://gitlab.com/fi-sas/fisas_packs/commit/75cecca2c40ab97e514f93180d03923221311860))

# [1.2.0](https://gitlab.com/fi-sas/fisas_packs/compare/v1.1.3...v1.2.0) (2021-11-09)


### Features

* **uesr_meals:** add reservation cron ([6db1ce7](https://gitlab.com/fi-sas/fisas_packs/commit/6db1ce7ed7ee514415bbd07d45e7f4e141546bca))

## [1.1.3](https://gitlab.com/fi-sas/fisas_packs/compare/v1.1.2...v1.1.3) (2021-10-21)


### Bug Fixes

* **menu:** postUserDefaults validations ([79363b4](https://gitlab.com/fi-sas/fisas_packs/commit/79363b40f1bfe00eb461785275234bfe973138e0))

## [1.1.2](https://gitlab.com/fi-sas/fisas_packs/compare/v1.1.1...v1.1.2) (2021-10-21)


### Bug Fixes

* change throw message ([d219dc8](https://gitlab.com/fi-sas/fisas_packs/commit/d219dc8e6c0086c5f4924174f0b73c2c1e869e7d))

## [1.1.1](https://gitlab.com/fi-sas/fisas_packs/compare/v1.1.0...v1.1.1) (2021-10-21)


### Bug Fixes

* change endpoints ([ad1f004](https://gitlab.com/fi-sas/fisas_packs/commit/ad1f0046c230c6c2d5e9fff33dfbd8d28cd8e029))

# [1.1.0](https://gitlab.com/fi-sas/fisas_packs/compare/v1.0.0...v1.1.0) (2021-10-21)


### Features

* **users_defaults:** add user_defaults by entity ([288f7f5](https://gitlab.com/fi-sas/fisas_packs/commit/288f7f52adc3b46067395dfc2155d242868b68c2))

# 1.0.0 (2021-10-20)


### Bug Fixes

* **configs:** change for urls to internal requests ([14f7b35](https://gitlab.com/fi-sas/fisas_packs/commit/14f7b35fe1b72447fdd7cd9e28b35bea51b763d5))
* **docker-compose:** add always restart in container ([4076840](https://gitlab.com/fi-sas/fisas_packs/commit/40768406d9596c4f04c1d7445042960c0ff1ca05))
* **readme:** add new commands ([7bbb7be](https://gitlab.com/fi-sas/fisas_packs/commit/7bbb7bef214b03d4da1c4708c2e204c2d4a84cf8))
* **services:** fix minor bugs ([9c04aab](https://gitlab.com/fi-sas/fisas_packs/commit/9c04aab47a6e11820d8f1dc89987717b34bd5c5f))


### Features

* **api:**  creation of main modules ([5e8f93b](https://gitlab.com/fi-sas/fisas_packs/commit/5e8f93b3bf7a57c1e9a77a861fee5a9c6e2f6b23))
* **api:** creation of configs ([f7e2537](https://gitlab.com/fi-sas/fisas_packs/commit/f7e25379f1eb5f3e4d3bc06babed0af59ed57a94))
* **app:** conversion rabbitmq to nats ([36b2780](https://gitlab.com/fi-sas/fisas_packs/commit/36b27800f3e2c7e4e4bba8683aea2f22de50ab51))
* initial commit ([879d062](https://gitlab.com/fi-sas/fisas_packs/commit/879d0625a1529a977df06b72e9453ec8deaa4267))
* **menu:** add menu packs list for sell on devices ([9a38875](https://gitlab.com/fi-sas/fisas_packs/commit/9a3887556e20b9c740d4f59b5b9891cad59b1442))
* **menu:** add translations to menu ([ef5b70a](https://gitlab.com/fi-sas/fisas_packs/commit/ef5b70a7e63fdd9bb6066d385b3e90c6d395f3f5))
* **packs:** add translation entity to pack entity ([f2b6bd7](https://gitlab.com/fi-sas/fisas_packs/commit/f2b6bd77a57938d04847d1bd9e826236c6985e40))
* **packs:** add translation to packs controller ([badfd9a](https://gitlab.com/fi-sas/fisas_packs/commit/badfd9af36ce5fef2fe8eac5144929de2ba67507))
