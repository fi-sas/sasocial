exports.up = (knex) => {
	return knex.schema
		.createTable("pack", (table) => {
			table.increments();
			table.decimal("price", 6, 2).notNullable();
			table.integer("vat_id").unsigned().notNullable();
			table.integer("number_lunchs").unsigned().notNullable();
			table.integer("number_dinners").unsigned().notNullable();
			table.enum("period", ["week", "month"]).notNullable();
			table.specificType("blocking_lunch_at", "timetz").notNullable();
			table.specificType("blocking_dinner_at", "timetz").notNullable();
			table.boolean("active").defaultTo(true).notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.timestamps();
		})
		.createTable("pack_translation", (table) => {
			table.increments();
			table.integer("language_id").unsigned().notNullable();
			table.string("name").notNullable();
			table.string("description").nullable();
			table
				.integer("pack_id")
				.unsigned()
				.notNullable()
				.references("id")
				.inTable("pack")
				.onDelete("cascade");
		})
		.createTable("pack_price", (table) => {
			table.increments();
			table
				.integer("pack_id")
				.unsigned()
				.notNullable()
				.references("id")
				.inTable("pack")
				.onDelete("cascade");
			table.decimal("price", 6, 2).notNullable();
			table.integer("vat_id").unsigned().notNullable();
			table.integer("profile_id").unsigned().notNullable();
		})
		.createTable("user_pack", (table) => {
			table.increments();
			table.integer("accommodation_application_id").unsigned().nullable();
			table.uuid("movement_id").nullable();
			table.uuid("movement_item_id").nullable();
			table.integer("user_id").unsigned().notNullable();
			table.integer("pack_id").unsigned().notNullable().references("id").inTable("pack");
			table.date("begin_at").notNullable();
			table.date("end_at").notNullable();
			table.integer("number_lunchs").unsigned().notNullable();
			table.integer("number_dinners").unsigned().notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.timestamps();
		})
		.createTable("user_meal", (table) => {
			table.increments();
			table.integer("user_pack_id").unsigned().notNullable().references("id").inTable("user_pack");
			table.integer("user_id").unsigned().nullable();
			table.integer("service_id").unsigned().nullable();
			table.integer("dish_type_id").unsigned().nullable();
			table.integer("menu_dish_id").unsigned().nullable();
			table.date("date").notNullable();
			table.enum("meal", ["lunch", "dinner"]).notNullable();
			table.boolean("reserved").defaultTo(false).notNullable();
			table.integer("reservation_id").unsigned().nullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.timestamps();
			table.unique(["user_id", "date", "meal"]);
		})
		.createTable("user_default", (table) => {
			table.increments();
			table.integer("user_id").unsigned().notNullable();
			table.integer("fentity_id").unsigned().notNullable();
			table.integer("canteen_lunch_id").unsigned().nullable();
			table.integer("dish_type_lunch_id").unsigned().nullable();
			table.integer("canteen_dinner_id").unsigned().nullable();
			table.integer("dish_type_dinner_id").unsigned().nullable();
			table.unique(["user_id", "fentity_id"]);
		});
};

exports.down = (knex) => {
	return knex.schema
		.dropTable("pack")
		.dropTable("pack_translation")
		.dropTable("pack_price")
		.dropTable("user_pack")
		.dropTable("user_meal")
		.dropTable("user_default");
};
