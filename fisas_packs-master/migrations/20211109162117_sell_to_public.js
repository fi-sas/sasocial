exports.up = (knex) => {
	return knex.schema.alterTable("pack", (table) => {
		table.boolean("sell_to_public").notNullable().defaultTo(false);
	});
};

exports.down = (knex) => {
	return knex.schema.alterTable("pack", (table) => {
		table.dropColumn("sell_to_public");
	});
};
