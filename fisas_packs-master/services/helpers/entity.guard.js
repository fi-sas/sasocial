const { Errors } = require("@fisas/ms_core").Helpers;

function addEntity(ctx) {
	if (ctx.meta.alimentation_entity_id) {
		ctx.params.fentity_id = ctx.meta.alimentation_entity_id;
	} else {
		throw new Errors.ForbiddenError("Need send fiscal entity Id", "NEED_SEND_FISCAL_ENTITY_ID", {});
	}
}

function addQueryFentity(ctx) {
	if (ctx.meta.alimentation_entity_id) {
		ctx.params.query = ctx.params.query || {};
		ctx.params.query["fentity_id"] = ctx.meta.alimentation_entity_id;
	} else {
		throw new Errors.ForbiddenError("Need send fiscal entity Id", "NEED_SEND_FISCAL_ENTITY_ID", {});
	}
}
async function checkUserPermission(ctx) {
	if (!ctx.meta.user.id) {
		throw new Errors.ValidationError("Need send user id", "NEED_SEND_USER_ID", {});
	}
	if (!ctx.meta.alimentation_entity_id) {
		throw new Errors.ForbiddenError("Need send fiscal entity Id", "NEED_SEND_FISCAL_ENTITY_ID", {});
	}
	await ctx.call("alimentation.fiscal-entities-users.checkUserPermission", {
		user_id: parseInt(ctx.meta.user.id),
		fentity_id: parseInt(ctx.meta.alimentation_entity_id),
	});
}
async function checkIsManager(ctx) {
	if (!ctx.meta.user.id) {
		throw new Errors.ValidationError("Need send user id", "NEED_SEND_USER_ID", {});
	}
	if (!ctx.meta.alimentation_entity_id) {
		throw new Errors.ForbiddenError("Need send fiscal entity Id", "NEED_SEND_FISCAL_ENTITY_ID", {});
	}
	await ctx.call("alimentation.fiscal-entities-users.isManager", {
		user_id: parseInt(ctx.meta.user.id),
		fentity_id: parseInt(ctx.meta.alimentation_entity_id),
	});
}
module.exports = {
	addQueryFentity,
	checkUserPermission,
	checkIsManager,
	addEntity,
};
