"use strict";
const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.menu_packs",
	/**
	 * Settings
	 */
	settings: {},
	hooks: {
		before: {},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getEntityCanteens: {
			rest: "GET /user-config/entities",
			cache: {
				keys: ["#user.id"],
			},
			handler(ctx) {
				return ctx
					.call("alimentation.entities.find", {
						query: {
							active: true,
						},
						fields: ["name", "id"],
						withRelated: false,
					})
					.then(async (entities) => {
						const user_defaults = await ctx.call("alimentation.users_defaults.find", {
							query: {
								user_id: ctx.meta.user.id,
							},
						});

						for (let entity of entities) {
							entity.user_defaults =
								user_defaults.find((ud) => ud.fentity_id === entity.id) || null;

							entity.canteens = await ctx.call("alimentation.services.find", {
								query: {
									type: "canteen",
									fentity_id: entity.id,
								},
								fields: ["name", "id"],
								withRelated: false,
							});
							entity.dishs_types = await ctx.call("alimentation.dishs-types.find", {
								query: {
									pack_available: true,
									fentity_id: entity.id,
								},
								fields: ["id"],
								withRelated: ["translations"],
							});
						}

						return entities;
					});
			},
		},
		postUserDefaults: {
			rest: "POST /user-config/defaults",
			params: {
				canteen_lunch_id: { type: "number", integer: true, convert: true, nullable: true },
				dish_type_lunch_id: { type: "number", integer: true, convert: true, nullable: true },
				canteen_dinner_id: { type: "number", integer: true, convert: true, nullable: true },
				dish_type_dinner_id: { type: "number", integer: true, convert: true, nullable: true },
				entity_id: { type: "number", positive: true, convert: true },
			},
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;

				const entity = await ctx.call("alimentation.entities.get", {
					id: ctx.params.entity_id,
				});

				const services = await ctx.call("alimentation.services.find", {
					query: {
						id: [ctx.params.canteen_lunch_id, ctx.params.canteen_dinner_id],
						type: "canteen",
						fentity_id: ctx.params.entity_id,
					},
				});

				const dishs_types = await ctx.call("alimentation.dishs-types.find", {
					query: {
						id: [ctx.params.dish_type_lunch_id, ctx.params.dish_type_dinner_id],
						pack_available: true,
						fentity_id: ctx.params.entity_id,
					},
				});

				if (!services.find((s) => s.id === ctx.params.canteen_lunch_id)) {
					throw new Errors.EntityNotFoundError("canteen_lunch_id", ctx.params.canteen_lunch_id);
				}

				if (!dishs_types.find((dt) => dt.id === ctx.params.dish_type_lunch_id)) {
					throw new Errors.EntityNotFoundError("dish_type_lunch_id", ctx.params.dish_type_lunch_id);
				}
				if (!services.find((s) => s.id === ctx.params.canteen_dinner_id)) {
					throw new Errors.EntityNotFoundError("canteen_dinner_id", ctx.params.canteen_dinner_id);
				}
				if (!dishs_types.find((dt) => dt.id === ctx.params.dish_type_dinner_id)) {
					throw new Errors.EntityNotFoundError(
						"dish_type_dinner_id",
						ctx.params.dish_type_dinner_id,
					);
				}

				ctx.params.user_id = ctx.meta.user.id;
				return ctx
					.call("alimentation.users_defaults.save_defaults_user", ctx.params)
					.then((res) => {
						return Promise.all([
							this.clearCache(),
							this.broker.cacher.clean("alimentation.menu_packs.*"),
						]);
					})
					.then(() => {
						return ctx.call("alimentation.menu_packs.getEntityCanteens");
					});
			},
		},

		getPreferencialDishType: {
			rest: "GET /user-config/defaults",
			visibility: "published",
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;
				return await ctx.call("alimentation.preferencial_canteens.find", {
					query: {
						user_id: ctx.meta.user.id,
					},
				});
			},
		},
		changeReserve: {
			rest: "POST /changeReserve",
			visibility: "published",
			params: {
				user_meal_id: { type: "number", positive: true, convert: true },
				menu_dish_id: { type: "number", positive: true, convert: true, optional: true },
				dish_type_id: { type: "number", positive: true, convert: true, optional: true },
			},
			async handler(ctx) {
				const user_meal = await ctx.call("alimentation.users_meals.get", {
					id: ctx.params.user_meal_id,
				});

				if (user_meal[0].reserved) {
					// THROW
					throw new Errors.ValidationError(
						"Cant change this meal beacause is already reserved in the canteen",
						"FOOD_PACK_MEAL_RESERVED",
					);
				}

				if (!ctx.params.menu_dish_id && ctx.params.dish_type_id) {
					return ctx.call("alimentation.users_meals.patch", {
						id: ctx.params.user_meal_id,
						dish_type_id: ctx.params.dish_type_id,
					});
				}

				if (ctx.params.menu_dish_id) {
					const menu_dish = await ctx.call("alimentation.menus-dishs.get", {
						id: ctx.params.menu_dish_id,
						withRelated: "menu",
					});

					ctx.params.dish_type_id = menu_dish[0].type_id;

					return ctx.call("alimentation.users_meals.patch", {
						id: ctx.params.user_meal_id,
						dish_type_id: ctx.params.dish_type_id,
						service_id: menu_dish[0].menu.service_id,
						menu_dish_id: ctx.params.menu_dish_id,
					});
				}

				if (!ctx.params.menu_dish_id && !ctx.params.dish_type_id) {
					return user_meal;
				}
			},
		},
		getServiceMenu: {
			rest: "GET /service/:service_id/menus/:date/:meal",
			visibility: "published",
			params: {
				service_id: { type: "number", convert: true },
				date: { type: "string" },
				meal: { type: "string" },
			},
			async handler(ctx) {
				ctx.meta.alimentationBypassEntity = true;

				const service = await ctx.call("alimentation.services.get", { id: ctx.params.service_id });
				if (service[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service is not type canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}

				const user_meals = await ctx.call("alimentation.users_meals.find", {
					query: {
						date: moment(ctx.params.date).format("YYYY-MM-DD"),
						meal: ctx.params.meal,
						user_id: ctx.meta.user.id,
					},
				});

				let pack_available = false;
				let user_meal_id = null;
				let reserved = false;
				if (user_meals.length > 0) {
					pack_available = true;
					user_meal_id = user_meals[0].id;
					reserved = user_meals[0].reserved;
				}

				const menu = await ctx.call("alimentation.menus.find", {
					query: {
						meal: ctx.params.meal,
						date: ctx.params.date,
						service_id: ctx.params.service_id,
					},
					withRelated: "dishs",
				});

				const products = [];
				if (menu.length !== 0) {
					if (menu[0].validated) {
						const menu_dishs = await ctx.call("alimentation.menus-dishs.find", {
							query: {
								menu_id: menu[0].id,
							},
							withRelated: ["dish", "menu", "type"],
						});

						let already_confirmed = false;

						for (const menu_dish of menu_dishs) {
							if (menu_dish.type.pack_available) {
								let user_allergic = false;

								const dishAllergenAndNutrients = await ctx.call(
									"alimentation.dishs.calculatedAllergenAndNutrientsComposition",
									{ dish_id: menu_dish.dish.id },
								);

								const user_allergens_composition = await ctx.call(
									"alimentation.users-allergens.returnsConfig",
									{
										user_id: ctx.meta.user ? ctx.meta.user.id : 0,
										allergens: dishAllergenAndNutrients.allergens,
									},
								);

								for (const dish_allergens of user_allergens_composition) {
									if (dish_allergens.allergic === true) {
										user_allergic = true;
									}
								}

								let pre_confirmed = false;
								let confirmed = false;
								if (pack_available) {
									confirmed = user_meals[0].menu_dish_id === menu_dish.id;

									if (confirmed) {
										already_confirmed = true;
									}

									if (!user_meals[0].menu_dish_id && !confirmed && !already_confirmed) {
										pre_confirmed =
											user_meals[0].dish_type_id === menu_dish.type_id &&
											user_meals[0].service_id === menu_dish.menu.service_id;

										if (pre_confirmed) already_confirmed = true;
									}
								}

								const resp = {
									meal: menu_dish.menu.meal,
									date: menu_dish.menu.date,
									id: menu_dish.id,
									file_id: menu_dish.file_id,
									dish_type_translation: menu_dish.type.translations,
									type: menu_dish.type,
									dish_type_id: menu_dish.type.id,
									allergens: user_allergens_composition,
									nutrients: dishAllergenAndNutrients.nutrients,
									user_allergic: user_allergic,
									service_id: menu_dish.menu.service.id,
									translations: menu_dish.dish.translations,
									location: menu_dish.menu.service.name,
									dish_id: menu_dish.dish.id,
									available: menu_dish.available,
									pack_available,
									user_meal_id,
									pre_confirmed,
									confirmed,
									reserved,
								};
								let withRelatedsArray = [];
								if (ctx.params.withRelated !== undefined) {
									withRelatedsArray = ctx.params.withRelated.split(",");
								}

								if (withRelatedsArray.includes("file")) {
									resp.file = menu_dish.dish.file;
								}
								products.push(resp);
							}
						}
					}
				}

				return products;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		clearCache() {
			this.broker.broadcast(`cache.clean.alimentation.menu_packs`);
			if (this.broker.cacher) return this.broker.cacher.clean(`alimentation.menu_packs.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
