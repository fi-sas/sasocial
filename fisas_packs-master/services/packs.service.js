"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const Validator = require("fastest-validator");
const { addQueryFentity, addEntity } = require("./helpers/entity.guard.js");
const { Errors } = require("@fisas/ms_core").Helpers;
const moment = require("moment");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.packs",
	table: "pack",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"price",
			"vat_id",
			"active",
			"sell_to_public",
			"period",
			"number_lunchs",
			"number_dinners",
			"blocking_lunch_at",
			"blocking_dinner_at",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["translations", "prices", "dishTypes"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"alimentation.packs_translations",
					"translations",
					"id",
					"pack_id",
				);
			},
			vat(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "configuration.taxes", "vat", "vat_id");
			},
			prices(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.packs_prices", "prices", "id", "pack_id");
			},
		},
		entityValidator: {
			translations: {
				type: "array",
				min: 1,
				items: {
					type: "object",
					props: {
						name: { type: "string" },
						description: { type: "string", optional: true },
						language_id: { type: "number", positive: true },
					},
				},
			},
			prices: {
				type: "array",
				min: 0,
				items: {
					type: "object",
					props: {
						price: { type: "number", positive: true },
						vat_id: { type: "number", positive: true },
						profile_id: { type: "number", positive: true },
					},
				},
				optional: true,
			},
			period: { type: "enum", values: ["week", "month"] },
			price: { type: "number", min: 0, convert: true },
			vat_id: { type: "number", positive: true, convert: true },
			number_lunchs: { type: "number", min: 0, convert: true },
			number_dinners: { type: "number", min: 0, convert: true },
			blocking_lunch_at: { type: "string" },
			blocking_dinner_at: { type: "string" },
			sell_to_public: { type: "boolean" },
			active: { type: "boolean" },
			fentity_id: { type: "number", positive: true, convert: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();

					const v = new Validator();
					const schema = {
						blocking_lunch_at: { type: "date", convert: true },
						blocking_dinner_at: { type: "date", convert: true },
					};
					const check = v.compile(schema);
					const res = check({ ...ctx.params });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);

					ctx.params.blocking_lunch_at = moment(ctx.params.blocking_lunch_at).format("HH:mm:ssZ");
					ctx.params.blocking_dinner_at = moment(ctx.params.blocking_dinner_at).format("HH:mm:ssZ");
				},
			],
			update: [
				addEntity,
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();

					const v = new Validator();
					const schema = {
						blocking_lunch_at: { type: "date", convert: true },
						blocking_dinner_at: { type: "date", convert: true },
					};
					const check = v.compile(schema);
					const res = check({ ...ctx.params });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);

					ctx.params.blocking_lunch_at = moment(ctx.params.blocking_lunch_at).format("HH:mm:ssZ");
					ctx.params.blocking_dinner_at = moment(ctx.params.blocking_dinner_at).format("HH:mm:ssZ");
				},
			],
			list: [addQueryFentity],
			get: ["validateDishsEntity"],
		},
		after: {
			create: ["saveTranslations", "savePrices", "saveDishTypes"],
			update: ["saveTranslations", "savePrices", "saveDishTypes"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Method for save translations of packs
		 */
		async saveTranslations(ctx, res) {
			await ctx.call("alimentation.packs_translations.save_translations", {
				pack_id: res[0].id,
				translations: ctx.params.translations,
			});

			res[0].translations = await ctx.call("alimentation.packs_translations.find", {
				query: {
					pack_id: res[0].id,
				},
			});

			return res;
		},
		/**
		 * Method for save prices of packs
		 */
		async savePrices(ctx, res) {
			if (ctx.params.prices) {
				await ctx.call("alimentation.packs_prices.save_prices", {
					pack_id: res[0].id,
					prices: ctx.params.prices,
				});

				res[0].prices = await ctx.call("alimentation.packs_prices.find", {
					query: {
						pack_id: res[0].id,
					},
				});
			}

			return res;
		},

		/**
		 * Method for validate Dish Entity
		 */
		async validateDishsEntity(ctx) {
			if (ctx.meta.alimentationBypassEntity === true) {
				return true;
			}
			const verify = await this._get(ctx, ctx.params);
			if (ctx.meta.alimentation_entity_id) {
				if (verify[0].fentity_id !== parseInt(ctx.meta.alimentation_entity_id)) {
					throw new Errors.ValidationError(
						"The pack not correspond the entity",
						"PACK_NOT_CORRESPOND_THE_ENTITY",
						{},
					);
				}
			}
			if (!ctx.meta.alimentation_entity_id) {
				throw new Errors.ForbiddenError(
					"You need send fiscal entity Id",
					"NO_FISCAL_ENTITY_ID",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
