"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.packs_prices",
	table: "pack_price",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "pack_id", "price", "vat_id", "profile_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			pack_id: { type: "number", positive: true },
			price: { type: "number", positive: true },
			vat_id: { type: "number", positive: true },
			profile_id: { type: "number", positive: true },
		},
	},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) {}],
			update: [function sanatizeParams(ctx) {}],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Action for save prices
		 */
		save_prices: {
			params: {
				pack_id: { type: "number", positive: true, integer: true },
				prices: {
					type: "array",
					min: 0,
					items: {
						type: "object",
						props: {
							price: { type: "number", positive: true },
							vat_id: { type: "number", positive: true },
							profile_id: { type: "number", positive: true },
						},
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					pack_id: ctx.params.pack_id,
				});
				this.clearCache();
				for (const price of ctx.params.prices) {
					const entities = {
						pack_id: ctx.params.pack_id,
						price: price.price,
						vat_id: price.vat_id,
						profile_id: price.profile_id,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
