"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.packs_translations",
	table: "pack_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "name", "pack_id", "description", "language_id"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			pack_id: { type: "number", positive: true },
			description: { type: "string", optional: true, default: "" },
			language_id: { type: "number", positive: true },
		},
	},
	hooks: {
		before: {
			create: [function sanatizeParams(ctx) {}],
			update: [function sanatizeParams(ctx) {}],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
		 * Action for save translations
		 */
		save_translations: {
			params: {
				pack_id: { type: "number", positive: true, integer: true },
				translations: {
					type: "array",
					min: 1,
					items: {
						type: "object",
						props: {
							name: { type: "string" },
							description: { type: "string", optional: true },
							language_id: { type: "number", positive: true },
						},
					},
				},
			},
			async handler(ctx) {
				const resp = [];
				await this.adapter.removeMany({
					pack_id: ctx.params.pack_id,
				});
				this.clearCache();
				for (const trans of ctx.params.translations) {
					const entities = {
						pack_id: ctx.params.pack_id,
						description: trans.description,
						language_id: trans.language_id,
						name: trans.name,
					};
					const insert = await this._insert(ctx, { entity: entities });
					resp.push(insert[0]);
				}
				return resp;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
