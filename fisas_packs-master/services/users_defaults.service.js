"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addEntity } = require("./helpers/entity.guard.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.users_defaults",
	table: "user_default",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_id",
			"canteen_lunch_id",
			"dish_type_lunch_id",
			"canteen_dinner_id",
			"dish_type_dinner_id",
			"fentity_id",
		],
		defaultWithRelateds: ["lunch_canteen", "dinner_canteen", "lunch_dish_type", "dinner_dish_type"],
		withRelateds: {
			lunch_canteen(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"alimentation.services",
					"lunch_canteen",
					"canteen_lunch_id",
					null,
					null,
					false,
				);
			},
			dinner_canteen(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"alimentation.services",
					"dinner_canteen",
					"canteen_dinner_id",
					null,
					null,
					false,
				);
			},
			lunch_dish_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"alimentation.dishs-types",
					"lunch_dish_type",
					"dish_type_lunch_id",
					null,
					null,
					null,
					["translations"],
				);
			},
			dinner_dish_type(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"alimentation.dishs-types",
					"dinner_dish_type",
					"dish_type_dinner_id",
					null,
					null,
					null,
					["translations"],
				);
			},
			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id",
					null,
					null,
					"id,name,email,student_number",
					[],
				);
			},
		},
		entityValidator: {
			user_id: { type: "number", positive: true, integer: true, convert: true },
			canteen_lunch_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			dish_type_lunch_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			canteen_dinner_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			dish_type_dinner_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			fentity_id: { type: "number", positive: true, integer: true, convert: true },
		},
	},
	hooks: {
		before: {
			list: [
				async function search(ctx) {
					if(ctx.params.search) {
						const users_ids = await ctx.call("authorization.users.find", {
							search: ctx.params.search,
							searchFields: "name,email,tin,user_name,student_number",
							fields: "id",
							withRelated: false,
						});

						ctx.params.query = ctx.params.query ||  {};
						ctx.params.query["user_id"] = users_ids.map(u => u.id);
					}
					delete ctx.params.search;
					delete ctx.params.searchFields;
				}
			],
			create: ["validations"],
			update: ["validations"],
			patch: ["validation_patch"],
			bo_save_defaults_user: [
				addEntity,
			]
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		bo_save_defaults_user: {
			rest: "PUT /:user_id/defaults",
			scope: "alimentation:packs:update",
			params: {
				user_id: { type: "number", min: 0, convert: true },
				canteen_lunch_id: { type: "number", min: 0, convert: true },
				dish_type_lunch_id: { type: "number", min: 0, convert: true },
				canteen_dinner_id: { type: "number", min: 0, convert: true },
				dish_type_dinner_id: { type: "number", min: 0, convert: true },
				fentity_id: { type: "number", min: 0, convert: true, optional: true },
			},
			handler(ctx) {
				return ctx.call("alimentation.users_defaults.save_defaults_user", {
					...ctx.params,
					entity_id: ctx.params.fentity_id,
				});
			},
		},
		save_defaults_user: {
			params: {
				user_id: { type: "number", integer: true, convert: true },
				canteen_lunch_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				dish_type_lunch_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				canteen_dinner_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				dish_type_dinner_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
				},
				entity_id: { type: "number", positive: true, convert: true },
			},
			async handler(ctx) {
				await this.validations(ctx);
				await this.adapter.removeMany({
					user_id: ctx.params.user_id,
					fentity_id: ctx.params.entity_id,
				});
				ctx.params.fentity_id = ctx.params.entity_id;
				delete ctx.params.entity_id;

				return await this._create(ctx, ctx.params).then((res) => {
					return Promise.all([
						this.adapter.raw(
							"update user_meal set dish_type_id = ?, service_id = ? where user_id = ? and fentity_id = ? and meal = 'lunch' and reserved = false and menu_dish_id is NULL",
							[
								ctx.params.dish_type_lunch_id,
								ctx.params.canteen_lunch_id,
								ctx.params.user_id,
								ctx.params.fentity_id,
							],
						),
						this.adapter.raw(
							"update user_meal set dish_type_id = ?, service_id = ? where user_id = ? and  fentity_id = ? and meal = 'dinner' and reserved = false and menu_dish_id is NULL",
							[
								ctx.params.dish_type_dinner_id,
								ctx.params.canteen_dinner_id,
								ctx.params.user_id,
								ctx.params.fentity_id,
							],
						),
						ctx.call("alimentation.users_meals.cacheClear"),
					]).then(() => {
						this.clearCache();
						return res;
					});
				});
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validations(ctx) {
			if (ctx.params.dish_type_lunch_id) {
				await ctx.call("alimentation.dishs-types.get", {
					id: ctx.params.dish_type_lunch_id,
				});
			}
			if (ctx.params.dish_type_dinner_id) {
				await ctx.call("alimentation.dishs-types.get", {
					id: ctx.params.dish_type_dinner_id,
				});
			}

			if (ctx.params.canteen_lunch_id) {
				const canteen_lunch = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_lunch_id,
				});
				if (canteen_lunch[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
			if (ctx.params.canteen_dinner_id) {
				const canteen_dinnner = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_dinner_id,
				});
				if (canteen_dinnner[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
		},

		async validation_patch(ctx) {
			if (ctx.params.user_id) {
				const user = await ctx.call("authorization.users.get", { id: ctx.params.user_id });
				if (user[0].active !== true) {
					throw new Errors.ValidationError("User not active", "USER_NOT_ACTIVE", {});
				}
			}
			if (ctx.params.canteen_lunch_id) {
				const canteen_lunch = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_lunch_id,
				});
				if (canteen_lunch[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
			if (ctx.params.canteen_dinner_id) {
				const canteen_dinnner = await ctx.call("alimentation.services.get", {
					id: ctx.params.canteen_dinner_id,
				});
				if (canteen_dinnner[0].type !== "canteen") {
					throw new Errors.ValidationError(
						"The service not correspond the canteen",
						"SERVICE_NOT_CANTEEN",
						{},
					);
				}
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
