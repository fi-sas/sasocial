"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Cron = require("moleculer-cron");
const moment = require("moment");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.users_meals",
	table: "user_meal",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs"), Cron],

	crons: [
		{
			name: "reserveUserMeals",
			cronTime: "* * * * *",
			onTick: function () {
				this.getLocalService("alimentation.users_meals")
					.actions.reservationJob()
					.then(() => {});
			},
			runOnInit: function () {},
		},
	],

	// CRON 1 MIN
	// GET TIME OF BLOCK PACK LUNCH AND DINNER
	// CHECK IF IS TIME OF SOME PACK
	// GET USER_MEAL NOT RESERVED
	// TRY TO SET MENU_DISH_ID
	// SEND ALL TO RESERVATION SERVICE

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"user_pack_id",
			"user_id",
			"service_id",
			"dish_type_id",
			"menu_dish_id",
			"date",
			"meal",
			"reserved",
			"reservation_id",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["service", "dish_type"],
		withRelateds: {
			service(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.services", "service", "service_id");
			},
			dish_type(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.dishs-types", "dish_type", "dish_type_id");
			},
		},
		entityValidator: {
			user_pack_id: { type: "number", positive: true },
			user_id: { type: "number", positive: true },
			service_id: { type: "number", positive: true },
			dish_type_id: { type: "number", positive: true },
			menu_dish_id: { type: "number", positive: true, optional: true },
			date: { type: "date" },
			meal: { type: "enum", values: ["lunch", "dinner"] },
			reserved: { type: "boolean" },
			reservation_id: { type: "number", positive: true, optional: true },
			fentity_id: { type: "number", positive: true, convert: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		reservationJob: {
			handler(ctx) {
				let packs_ids_to_block_lunch = [];
				let packs_ids_to_block_dinner = [];

				ctx
					.call("alimentation.packs.find", {
						query: {
							active: true,
						},
					})
					.then((packs) => {
						packs.map((p) => {
							this.logger.info("HORA SERVIDOR ? ", moment().format("HH:mm"));
							this.logger.info("PACK ID: ", p.id);
							this.logger.info(
								"BLOQUEIO ALMOÇO:",
								moment(p.blocking_lunch_at, "HH:mm:ssZ").format("HH:mm"),
							);
							this.logger.info(
								"BLOQUEIO JANTAR: ",
								moment(p.blocking_dinner_at, "HH:mm:ssZ").format("HH:mm"),
							);
						});

						//GET packs
						packs_ids_to_block_lunch = packs
							.filter((p) => moment(p.blocking_lunch_at, "HH:mm:ssZ").isSameOrBefore(moment()))
							.map((p) => p.id);
						packs_ids_to_block_dinner = packs
							.filter((p) => moment(p.blocking_dinner_at, "HH:mm:ssZ").isSameOrBefore(moment()))
							.map((p) => p.id);
						return [packs_ids_to_block_lunch, packs_ids_to_block_dinner];
					})
					.then(async (result) => {
						//GET users_meals FROM pack_id, meal, not reserved and date today !!
						const promises = [];
						if (result[0].length) {
							promises.push(
								this.adapter
									.raw(
										`select um.* from user_meal as um
					inner join user_pack as up on up.id = um.user_pack_id
					where um.reserved = false and um.date = ? and um.meal = ? and up.pack_id in (${result[0]
						.map((id) => parseInt(id))
						.join(",")})`,
										[moment().format("YYYY-MM-DD"), "lunch"],
									)
									.then((res) => res.rows),
							);
						}
						if (result[1].length) {
							promises.push(
								this.adapter
									.raw(
										`select um.* from user_meal as um
						inner join user_pack as up on up.id = um.user_pack_id
						where um.reserved = false and um.date = ? and um.meal = ? and up.pack_id in (${result[1]
							.map((id) => parseInt(id))
							.join(",")})`,
										[moment().format("YYYY-MM-DD"), "dinner"],
									)
									.then((res) => res.rows),
							);
						}
						return Promise.all(promises);
					})
					.then((user_meals) => {
						user_meals = user_meals.flat();
						this.logger.info("Getting User Meals...");
						this.logger.info(user_meals);

						return ctx
							.call(
								"alimentation.menus.find",
								{
									query: {
										date: moment().format("YYYY-MM-DD"),
									},
									withRelated: "dishs",
								},
								{
									meta: {
										alimentationBypassEntity: true,
									},
								},
							)
							.then((menus) => {
								this.logger.info("Menus Fetched...");

								user_meals
									.filter((um) => !um.menu_dish_id)
									.map((um) => {
										const menu = menus.find(
											(m) => m.service_id === um.service_id && um.meal === m.meal,
										);
										if (menu) {
											const menu_dish = menu.dishs.find((md) => md.type_id === um.dish_type_id);

											if (menu_dish) {
												um.service_id = menu.service_id;
												um.menu_dish_id = menu_dish.id;
											} else {
												if (menu.dishs.length > 0) {
													um.service_id = menu.service_id;
													um.menu_dish_id = menu.dishs[0].id;
												}
											}
										}
									});

								this.logger.info("Sending reservations...");

								return ctx
									.call("alimentation.reservations.insert", {
										entities: user_meals
											.filter((um) => um.menu_dish_id)
											.map((um) => ({
												date: new Date(um.date),
												location: null,
												menu_dish_id: um.menu_dish_id,
												movement_id: null,
												item_id: null,
												device_id: null,
												user_id: um.user_id,
												is_from_pack: true,
												user_meal_pack_id: um.id,
												is_served: false,
												is_available: true,
												has_canceled: false,
												meal: um.meal,
												fentity_id: um.fentity_id,
												service_id: um.service_id,
												created_at: new Date(),
												updated_at: new Date(),
											})),
									})
									.then((reservations) => {
										this.logger.info("Reservations Done...");
										return this.blockAndUpdateReservationIDUserMeals(
											reservations.map((res) => {
												return {
													id: res.user_meal_pack_id,
													service_id: res.service_id,
													menu_dish_id: res.menu_dish_id,
													reservation_id: res.id,
												};
											}),
										);
									})
									.catch((err) => {
										this.logger.error("Error!");
										this.logger.error(err);
									});
							});
					});
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		changeType: {
			params: {},
			handler(ctx) {
				return null;
			},
		},
		cacheClear: {
			visibility: "public",
			handler(ctx) {
				this.clearCache();
			},
		},
		createMeals: {
			params: {
				dates: { type: "array", items: "date" },
				number_lunchs: { type: "number", min: 0 },
				number_dinners: { type: "number", min: 0 },
				user_pack_id: { type: "number", positive: true },
				fentity_id: { type: "number", positive: true },
				user_id: { type: "number", positive: true },
			},
			async handler(ctx) {
				let default_lunch_service_id = null;
				let default_lunch_dish_type_id = null;
				let default_dinner_service_id = null;
				let default_dinner_dish_type_id = null;

				const user_settings = await ctx.call("alimentation.users_defaults.find", {
					query: {
						user_id: ctx.params.user_id,
						fentity_id: ctx.params.fentity_id,
					},
				});

				if (user_settings.length > 0) {
					default_lunch_service_id = user_settings[0].canteen_lunch_id;
					default_lunch_dish_type_id = user_settings[0].dish_type_lunch_id;
					default_dinner_service_id = user_settings[0].canteen_dinner_id;
					default_dinner_dish_type_id = user_settings[0].dish_type_dinner_id;
					// ???
				} else {
					// AND NOW ??
					default_lunch_service_id = 2;
					default_lunch_dish_type_id = 2;
					default_dinner_service_id = 2;
					default_dinner_dish_type_id = 2;
				}

				const meals = [];
				let lunchs_created = 0;
				let dinners_created = 0;
				for (const date of ctx.params.dates) {
					if (lunchs_created < ctx.params.number_lunchs) {
						lunchs_created++;
						meals.push({
							user_pack_id: ctx.params.user_pack_id,
							user_id: ctx.params.user_id,
							service_id: default_lunch_service_id,
							dish_type_id: default_lunch_dish_type_id,
							menu_dish_id: null,
							date: date,
							meal: "lunch",
							reserved: false,
							reservation_id: null,
							fentity_id: ctx.params.fentity_id,
							created_at: new Date(),
							updated_at: new Date(),
						});
					}

					if (dinners_created < ctx.params.number_dinners) {
						dinners_created++;
						meals.push({
							user_pack_id: ctx.params.user_pack_id,
							user_id: ctx.params.user_id,
							service_id: default_dinner_service_id,
							dish_type_id: default_dinner_dish_type_id,
							menu_dish_id: null,
							date: date,
							meal: "dinner",
							reserved: false,
							reservation_id: null,
							fentity_id: ctx.params.fentity_id,
							created_at: new Date(),
							updated_at: new Date(),
						});
					}
				}

				return this._insert(ctx, { entity: meals });
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 *
		 * @param {*} ids array of number
		 * @returns Promise
		 */
		blockUserMeals(ids) {
			return this.adapter.updateMany(
				(qb) => {
					qb.where("id", "in", ids);
				},
				{
					reserved: true,
				},
			);
		},
		/**
		 *
		 * @param { id: number, reservation_id: number}[] array of reserations
		 * @returns Promise
		 */
		async blockAndUpdateReservationIDUserMeals(user_meals) {
			for (let um of user_meals) {
				await this.adapter.updateById(um.id, {
					reservation_id: um.reservation_id,
					service_id: um.service_id,
					menu_dish_id: um.menu_dish_id,
					reserved: true,
				});
			}
			this.clearCache();
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
