"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const Validator = require("fastest-validator");
const moment = require("moment");
const moment_bussiness = require("moment-business-days");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { addEntity } = require("./helpers/entity.guard.js");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "alimentation.users_packs",
	table: "user_pack",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("alimentation", "packs")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"accommodation_application_id",
			"movement_id",
			"movement_item_id",
			"user_id",
			"pack_id",
			"begin_at",
			"end_at",
			"number_lunchs",
			"number_dinners",
			"fentity_id",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["user", "pack"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			pack(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "alimentation.packs", "pack", "pack_id");
			},
			user_meals(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "alimentation.users_meals", "user_meals", "id", "user_pack_id");
			},
		},
		entityValidator: {
			accommodation_application_id: { type: "number", positive: true, optional: true },
			movement_id: { type: "number", positive: true, optional: true },
			movement_item_id: { type: "number", positive: true, optional: true },
			user_id: { type: "number", positive: true },
			pack_id: { type: "number", positive: true },
			begin_at: { type: "date", convert: true },
			end_at: { type: "date", convert: true },
			number_lunchs: { type: "number", min: 0 },
			number_dinners: { type: "number", min: 0 },
			fentity_id: { type: "number", positive: true, convert: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function checkDates(ctx) {
					const v = new Validator();
					const schema = {
						pack_id: { type: "number", positive: true },
						begin_at: { type: "date", convert: true },
						end_at: { type: "date", convert: true, optional: true },
					};
					const check = v.compile(schema);
					const res = check({ ...ctx.params });
					if (res !== true)
						return Promise.reject(
							new Errors.ValidationError("Entity validation error!", null, res),
						);

					if (ctx.params.end_at) {
						if (moment(ctx.params.begin_at).isSameOrAfter(ctx.params.end_at, "day")) {
							throw new Errors.ValidationError(
								"The end date must be after the begin date",
								"PACKS_END_AT_BEFORE_BEGIN",
								{},
							);
						}
					}

					const packs = await ctx.call("alimentation.packs.get", { id: ctx.params.pack_id });

					if (packs[0].period === "month") {
						ctx.params.begin_at = moment(ctx.params.begin_at).startOf("month").toDate();
						ctx.params.end_at = moment(ctx.params.begin_at).endOf("month").toDate();
					}

					if (packs[0].period === "week") {
						ctx.params.begin_at = moment(ctx.params.begin_at).startOf("week").toDate();
						ctx.params.end_at = moment(ctx.params.begin_at).endOf("week").toDate();
					}

					ctx.params.fentity_id = packs[0].fentity_id;
					ctx.params.number_lunchs = packs[0].number_lunchs;
					ctx.params.number_dinners = packs[0].number_dinners;

					if (!ctx.meta.isBackoffice & moment(ctx.params.begin_at).isBefore()) {
						throw new Errors.ValidationError(
							"The begin date must be in the next period",
							"PACKS_BEGIN_IS_BEFORE_TODAY",
							{},
						);
					}
				},
				"validateOverlap",
				function sanatizeParams(ctx) {
					if (!ctx.meta.alimentation_entity_id) {
						throw new Errors.ForbiddenError(
							"Need send fiscal entity Id",
							"NEED_SEND_FISCAL_ENTITY_ID",
							{},
						);
					}

					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					if (!ctx.meta.alimentation_entity_id) {
						throw new Errors.ForbiddenError(
							"Need send fiscal entity Id",
							"NEED_SEND_FISCAL_ENTITY_ID",
							{},
						);
					}

					ctx.params.updated_at = new Date();
				},
			],
			list: [
				async function sanatizeFEntityId(ctx) {
					if (ctx.meta.alimentation_entity_id) {
						ctx.params.query = ctx.params.query || {};
						ctx.params.query["pack_id"] = await ctx
							.call("alimentation.packs.find", {
								query: {
									fentity_id: ctx.meta.alimentation_entity_id,
								},
								fields: ["id"],
								withRelated: false,
							})
							.then((packs) => packs.map((p) => p.id));
					} else {
						throw new Errors.ForbiddenError(
							"Need send fiscal entity Id",
							"NEED_SEND_FISCAL_ENTITY_ID",
							{},
						);
					}
				},
			],
			create_bulk: [
				addEntity,
			]
		},
		after: {
			create: [
				async function createUserMeals(ctx, res) {
					const dates = [];
					let curr_date = moment_bussiness(ctx.params.begin_at);
					while (curr_date.isBefore(ctx.params.end_at)) {
						if (curr_date.isBusinessDay()) dates.push(curr_date.toDate());

						curr_date = curr_date.add(1, "day");
					}

					return ctx
						.call("alimentation.users_meals.createMeals", {
							dates,
							user_pack_id: res[0].id,
							fentity_id: res[0].fentity_id,
							user_id: res[0].user_id,
							number_lunchs: res[0].number_lunchs,
							number_dinners: res[0].number_dinners,
						})
						.then((user_meals) => {
							res[0].user_meals = user_meals;
							return res;
						});
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		find: {
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
					"#language_id",
				],
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		create_bulk: {
			// REST: POST /bulk
			rest: "POST /bulk",
			visibility: "published",
			params: {
				user_id: { type: "number", positive: true },
				pack_id: { type: "number", positive: true },
				begin_at: { type: "date", convert: true },
				end_at: { type: "date", convert: true },

				canteen_lunch_id: { type: "number", positive: true, optional: true },
				dish_type_lunch_id: { type: "number", positive: true, optional: true },
				canteen_dinner_id: { type: "number", positive: true, optional: true },
				dish_type_dinner_id: { type: "number", positive: true, optional: true },
				fentity_id: { type: "number", positive: true, optional: true },
			},
			async handler(ctx) {
				await this.validateOverlap(ctx);


				if(ctx.params.canteen_lunch_id && ctx.params.dish_type_lunch_id && ctx.params.canteen_dinner_id && ctx.params.dish_type_dinner_id) {
					await ctx.call("alimentation.users_defaults.save_defaults_user", {
						user_id: ctx.params.user_id,
						canteen_lunch_id: ctx.params.canteen_lunch_id,
						dish_type_lunch_id: ctx.params.dish_type_lunch_id,
						canteen_dinner_id: ctx.params.canteen_dinner_id,
						dish_type_dinner_id: ctx.params.dish_type_dinner_id,
						entity_id: ctx.params.fentity_id,
					});
				}

				const packs = await ctx.call("alimentation.packs.get", { id: ctx.params.pack_id });

				let periods = [];
				let isSamePeriod = false;
				if (packs[0].period === "week") {
					ctx.params.begin_at = moment(ctx.params.begin_at).startOf("week").toDate();
					isSamePeriod = moment(ctx.params.end_at).isSame(moment(ctx.params.begin_at), "week");
					periods.push(ctx.params.begin_at);

					let temp_currentDate = moment(ctx.params.begin_at);
					while (!moment(ctx.params.end_at).isSame(temp_currentDate, "week")) {
						temp_currentDate = temp_currentDate.add(1, "week");
						periods.push(temp_currentDate.startOf("week").toDate());
					}
				}

				if (packs[0].period === "month") {
					ctx.params.begin_at = moment(ctx.params.begin_at).startOf("month").toDate();
					isSamePeriod = moment(ctx.params.end_at).isSame(moment(ctx.params.begin_at), "month");
					periods.push(ctx.params.begin_at);

					let temp_currentDate = moment(ctx.params.begin_at);
					while (!moment(ctx.params.end_at).isSame(temp_currentDate, "month")) {
						temp_currentDate = temp_currentDate.add(1, "month");
						periods.push(temp_currentDate.startOf("month").toDate());
					}
				}

				if (!isSamePeriod) {
					let response = [];
					for (let begin_at of periods) {
						response = await ctx.call("alimentation.users_packs.create", {
							user_id: ctx.params.user_id,
							pack_id: ctx.params.pack_id,
							begin_at,
						});
					}
					return response;
				} else {
					return ctx.call("alimentation.users_packs.create", {
						user_id: ctx.params.user_id,
						pack_id: ctx.params.pack_id,
						begin_at: ctx.params.begin_at,
					});
				}
			},
		},
		get: {
			// REST: GET /:id
			visibility: "published",
			cache: {
				keys: ["id", "withRelated", "fields", "mapping", "#language_id"],
			},
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		async validateOverlap(ctx) {
			const users_packs = await this._count(ctx, {
				query: (qb) => {
					qb.where("user_id", "=", ctx.params.user_id);
					qb.whereRaw("tstzrange(begin_at,end_at, '[]') && tstzrange(?, ?, '[]')", [
						ctx.params.begin_at,
						ctx.params.end_at,
					]);
				},
			});

			if (users_packs > 0) {
				throw new Errors.ValidationError(
					"The begin date and end date overlap with other active pack",
					"PACKS_USER_PACK_OVERLAP",
					{},
				);
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
