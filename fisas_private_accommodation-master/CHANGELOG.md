## [1.8.3](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.8.2...v1.8.3) (2022-03-09)


### Bug Fixes

* **private accommodation:** fix key syntax ([dd72ab3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/dd72ab3fbc547dad0d7db485f43f5bf22b48fb3c))
* **private accommodation:** listing widget ([fa862a3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/fa862a36300228eb892ba021acbbb67b0f195ecd))

## [1.8.2](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.8.1...v1.8.2) (2021-12-20)


### Bug Fixes

* **listings:** include rooms to getMinMaxPrice action ([d203d63](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d203d635cfaa87f9eaf81dcfdee0e40c9473e36b))

## [1.8.1](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.8.0...v1.8.1) (2021-12-20)


### Bug Fixes

* **listings:** wrong relation between listings and rooms ([0c10b64](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/0c10b645afd45999c4e312736746ff445701af18))

# [1.8.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.7.0...v1.8.0) (2021-12-17)


### Features

* **listings:** include rooms prices into search by price ([a5d9de7](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a5d9de773f9141ff4765b6cb71038f385a49a474))

# [1.7.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.6.0...v1.7.0) (2021-12-10)


### Features

* change mandatory fields ([9fa5b6c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/9fa5b6c5c5634bf0c3185a2001abbc1f8a79e80e))
* change mandatory fields ([a808b30](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a808b3051659d3884f1875c85ae40ba4bd2ff598))

# [1.6.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.5.0...v1.6.0) (2021-12-10)


### Features

* change mandatory fields, check properties ([614c25e](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/614c25e86d582285b0f44b753e86cb755ce8b028))


### Performance Improvements

* **listings:** change call to count ([89278e9](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/89278e9f79ec556eed407cf9842445c28cd83e27))

# [1.5.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.4.3...v1.5.0) (2021-10-25)


### Features

* property title in english ([111c731](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/111c731c94c3acc4dde921fbd3da9ab7a0071eaa))

## [1.4.3](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.4.2...v1.4.3) (2021-10-01)


### Bug Fixes

* **listings:** fix 500 error ([4f0be24](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4f0be24b93c38141fffd77d85478e04b25fc176d))

## [1.4.2](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.4.1...v1.4.2) (2021-08-05)


### Bug Fixes

* **owner:** send email verification account active ([f048e93](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f048e9309e66a4d4b80f8e58956b77626f7dd271))

## [1.4.1](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.4.0...v1.4.1) (2021-07-28)


### Bug Fixes

* **complaints:** validate if complain has one response ([4dfd9d8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4dfd9d80f73ac6ae0cd85bb966e1dd9358822dd3))

# [1.4.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.3.2...v1.4.0) (2021-07-23)


### Features

* **owner:** adicionar nacionalidade e tipo de documento e user_name igual email ([169ec07](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/169ec07543272ba0a6de01d408e2c530754962d9))

## [1.3.2](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.3.1...v1.3.2) (2021-07-16)


### Bug Fixes

* **owners:** return erros of users creation ([e1fe8a5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e1fe8a500dfeefe4f25c3ea7ead98ec56de78ef5))

## [1.3.1](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.3.0...v1.3.1) (2021-07-12)


### Bug Fixes

* **migration:** add notnullable ([a6b500e](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a6b500e07a9957877849441e49bf5fdeaa54077f))
* **migration:** update migration default value ([28d76e4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/28d76e4531abdc257955a5cfaeb44ea9d1af466f))

# [1.3.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.2.1...v1.3.0) (2021-07-12)


### Bug Fixes

* **listing:** add has_deleted field ([98e2a6b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/98e2a6bbe0fb660d6506a34a755186fd8a9fb669))
* **listing:** query endpoint dasboard ([c5c69ef](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/c5c69ef12aea0cf7b2ac85558ba1ab20347989f8))


### Features

* **migrations:** update table listing ([305f4e0](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/305f4e054adb70153b2d4f2d65cdd353909a2849))

## [1.2.1](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.2.0...v1.2.1) (2021-07-05)


### Bug Fixes

* **configurations:** problems with json ([7a82718](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/7a82718688f33ac9033c14392b76375794a2626c))

# [1.2.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.10...v1.2.0) (2021-06-18)


### Bug Fixes

* **listings:** add scope endpoint owner ([f5b29d1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f5b29d142b8959a4504567307dc78d59c684cb17))
* **listings:** lint problems ([577177e](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/577177eb56b53b8cafec2b9dbedf3f58bc29f83f))
* **listings:** min_price ([d6bbad6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d6bbad65c372fc257b55ac512134d8029e732959))


### Features

* **listing:** add endpoint max_min_ price and list owner listings ([392095a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/392095a265e0893a0bdd69e96bd06b320192f343))

## [1.1.10](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.9...v1.1.10) (2021-06-17)


### Bug Fixes

* **listings:** error in search listings ([56de1a8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/56de1a8339f9f2205441386307d5b958035ba545))
* **owners:** remove fields pin and password ([50cd6db](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/50cd6db272ba0e9c5f4b41aee0ade48816d6f0b7))

## [1.1.9](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.8...v1.1.9) (2021-06-15)


### Bug Fixes

* **listsings:** add sanatize params ([90b9a7b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/90b9a7b7fa0fa6cfcbc61358e3770222fd6cb058))

## [1.1.8](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.7...v1.1.8) (2021-05-14)


### Bug Fixes

* **listings:** scopes names ([727868a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/727868af28655cf66a4a107f713d0afc1eae797f))

## [1.1.7](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.6...v1.1.7) (2021-05-04)


### Bug Fixes

* **complaints:** status ([df78fc3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/df78fc377e91e428ec9dd868cf8df16c497a4cd7))
* **env_files:** update name of postgres container ([3f250ed](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3f250edccfb3578f436e8394299aff0c70e15a60))
* **owners:** update create owner ([a2419a1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a2419a17fa330287353cac75c04bcc4b57a03c1c))

## [1.1.6](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.5...v1.1.6) (2021-04-26)


### Bug Fixes

* **configurations:** fix configurations service ([e8a21cd](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e8a21cd785f5e8e323ddb7beb19fe341b02c12d7))

## [1.1.5](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.4...v1.1.5) (2021-04-13)


### Bug Fixes

* **owners:** add source by and source name info to user ([bc5e644](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/bc5e6449f2b3e2ea0ca528a2c897e4891ab09c26))

## [1.1.4](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.3...v1.1.4) (2021-04-01)


### Bug Fixes

* **listing:** fix search filters and optimize get locations ([98a3e7b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/98a3e7b561dd563a665ece79078ec1f676e4fd01))

## [1.1.3](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.2...v1.1.3) (2021-03-22)


### Bug Fixes

* **owners:** remove user_groups from request ([31d2217](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/31d22173ece05608a6171c05d71d79bceaf43c66))

## [1.1.2](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.1...v1.1.2) (2021-03-10)


### Bug Fixes

* **listings:** add publish_date to listings ([8911ded](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8911dede1cea85e17516657e7ee1d84f7d4c7e13))

## [1.1.1](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.1.0...v1.1.1) (2021-03-10)


### Bug Fixes

* **listings:** add missing withrelateds ([92dbb27](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/92dbb2796a2a6186a7a6646ddff6020f04e8fa56))

# [1.1.0](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0...v1.1.0) (2021-03-09)


### Features

* **listings:** add endpoint to WP widget ([4f247a5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4f247a5f29929be68b411146b4a324af4baccdb5))

# 1.0.0 (2021-03-09)


### Bug Fixes

* address_no, and add withRelated typology ([764f856](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/764f85657bbb1995e4711958ffca8924371f7904))
* fix validations on listings ([13fbd28](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/13fbd2874f12682bcab6e1f62e7357eb795ef18b))
* listings service ([6349f14](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6349f1418f4c5311c8dfa1218e4ce5fe517fa7bc))
* optimize code ([a07f6a4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a07f6a4ac50e917f621284002787064756b82ed7))
* type of latitude e longitude ([6174db7](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6174db77a769306260b8300cb71f948756871c79))
* **complaint:** add get user method ([a1ce340](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a1ce340d4a43c12968af3f73811b78e9d717adab))
* **complaint:** fix withRelated ([1a3581c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1a3581c972f590487e91931219f1c935c550ed25))
* **complaints:** change insert for create ([4899c16](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4899c16d8dede6134313dd95d824dd83702d095e))
* **configuration:** parse owner regulation ([e081b51](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e081b51f80e98a505dc9bef1a025121ec9ba20f8))
* **configurations:** add set endpoints ([aecb5c9](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/aecb5c91ce775d03c78fc7cd082ff94039caf98d))
* **configurations:** data conversion ([da4a8a3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/da4a8a303639a10106914dcbe22ee8c36979b7a2))
* **configurations:** fix save settings ([6da3cbf](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6da3cbf0b830a4823612ce86b8090f71db822a97))
* **lint:** fix lint errors ([8fc8492](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8fc8492882be9826e32962b44390decf13e12986))
* **listing:** fix notifications ([ed7f54b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ed7f54b3db184a36763b7664c8ad704f03febfa6))
* **listing:** remove convert date ([c9276ca](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/c9276caa18f950ca6790b80691597d1e2f9455cf))
* **listing:** validate medias, translations, amenities and expenses ([cbf242b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/cbf242bcd50a7fa0bb08f799af0c1d72521f7d10))
* **migrations:** add drop table for rooms ([5d36b1c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5d36b1c9694e4f7855b453222b639f37beae959f))
* **migrations:** change status ([d93ca74](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d93ca743191748786502120d6e5a3e2d3f7788fe))
* add user id on listings creation ([bdfb8e4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/bdfb8e4155363b8ea26a5cfc6652a69515f4caef))
* fix error on knex timeout ([714c5d2](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/714c5d207f15b8a9ba825e01f111169838f70fbf))
* listings withRelated ([8c51db6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8c51db6ae063a52253c746911944bb165702ea81))
* randon onwer user_name ([6bcdb8d](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6bcdb8d31c3c329b752a898557a930843648e2db))
* **listings:** add allow pets ([010de50](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/010de50e99f2c984ea2465f83637169ef77ef0a8))
* **listings:** add withRelateds and validations ([c651c4c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/c651c4c493832767505e2cbc3502b8d47741e30b))
* **migrations:** add enums ([4175d35](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4175d356b6011582766caac86397cbde79cc57cf))
* **owners:** fix list owners ([930afc1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/930afc1374f8fe610784fe2e905c65f6854b3bf2))
* **owners:** fix remove ([1228b4c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1228b4c8de58af925c9bd54b632cdffd37e4e7c8))
* **seed:** set seeds for configurations ([f608099](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f60809955b9b4b2713ec0f262c1443c79c12504a))
* **typologies:** add endpoint for listings and wanted-ads ([77f4a1b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/77f4a1b0a9e6991c9b32ba0de652d844f593205f))
* **typologies:** fix update ([058fd1f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/058fd1fd789187f65907a9a0017f011c8786340d))
* **wanted_adds:** fix withRelated user ([df53448](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/df53448b9429b089189d6d9c5682413b4603bc81))
* deploy QA ([3456b72](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3456b721a7b949339def527caec91f04fbf36ca6))
* start.sh helper script ([a25f1f3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a25f1f388de3232a771702cf8826e1a294b60f9f))
* **docker-compose:** create docker-compose to prod and dev ([e30ca0f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e30ca0f24417ee072b0307e9e5bf14cd28177f6d))
* **listing:** add house_typology and usable_space ([a0792b7](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a0792b7040776a5ff1ca965ddb22c6e3630a3e17))
* **listing:** add new load media in listing search and listing by id ([1194be6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1194be64db567111613b9b039f43534d7f6ba16c))
* **listing:** allow null in area and floor ([ad008e0](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ad008e02a396e5d5f8dcc2ec0b8f66e192d161b3))
* **listing:** change size in description ([86abcfc](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/86abcfcacfb2723447c37059333b58d0f020243d))
* **listing:** fix migration ([271140d](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/271140def4cbcbd7f540c5326c7491d9e08c2fe0))
* **listing:** owner data ([6e88056](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6e88056d5c46ee59a527967e9943ba259b34df78))
* **listing:** set area and floor optional ([2971d27](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/2971d27e2c287647de68d94beffe95fc0c57215c))
* **listing:** set owner id when device type is web ([6990540](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6990540d70e8b2d6c0a2a666e64be3e53e2f2d24))
* **listings:** add house_typology and usable_space ([898d0fa](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/898d0fa6ea593838fdfb43d19ddd798fe19373bb))
* **listings:** add medias file in listings ([520a2f8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/520a2f83c59826cbfaece8e318a3b484726089b9))
* **listings:** remove country ([01a654c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/01a654c5856d32785160336208c2ed652f7601a6))
* **listings:** remove country in schema ([a308d67](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a308d67214bb69bc6b570c3622d0fecdca49c65f))
* **migrations:** delete duplicate code ([5f3b15b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5f3b15bed0c952b01417bb7e4661729f02350333))
* **owner:** add issues receipt in owner ([d3c539a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d3c539a58bc4a4486ff4cddd28080d7c9c84a9bf))
* **owner:** add parce to invalid owner ([441e397](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/441e397b7fadc6fafe69e731c6b336ea60b05198))
* **owner:** fix  issues receipt ([31fed4a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/31fed4a2d248e740b49e1bfd046e54f78887dcc4))
* **owner:** parse issues receipt to boolean ([ac255b1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ac255b15e9d38e7102c768043d7677abdbdc17f1))
* **owner_applications:** remove validation for notes in change status ([f9adf7c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f9adf7c0f84ae157d0e03981e9b1b39659f2fe62))
* **owner_applications:** remove validation for notes when approve ([3e07f03](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3e07f0358e788c41919b2e80dc6074c4d88dfda0))
* **owner-application:** fix issues receipt ([ea858cf](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ea858cf66acf3fc23259e32d482eea654505c890))
* allow no email posting owner @ owner ([10d06b4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/10d06b4086937ba7867bc7479ce35f92603aa878))
* allow null latitude and longitude @ listing ([4c15996](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4c159960d10c3703022d0be3b0db9987859d32b2))
* backoffice middleware missing @ PATCH listing ([a46a457](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a46a4571181f58d3d804377903140a507afb5df1))
* filter by owner_id @ owners ([487047f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/487047fbfa0a266fac0f2b9c35085c0fe00e5154))
* fixing check owner validation @ PATCH listing ([21ff947](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/21ff947c24c7be5b7c1e31bfb0b834e13593fc83))
* testing related entities method - loadRelated with limit -1 ([bd30b2a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/bd30b2a1abd93be09158b3490fe78717862c9154))
* **owner:** delete owner in cascade ([3856a81](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3856a812b50892fba54db8213f6f51e56acfad48))
* **owner-applications:** add tin ([6724689](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/672468948b2f89216b44be98fdc034265549ceed))
* **owners:** create owner,activate and deactivate ([5b313da](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5b313da4889a4343ff97e7a0ad963ef77b2f1126))
* **user data:** add user data in responses ([67d4a73](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/67d4a7382fdd9c5cef6424e42b787011557a1e57))


### Features

* add endpoint to return all locations actives ([05a6709](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/05a6709364462971d966f42d6cf55c1126d5a10b))
* fix and add rooms translations ([9c24e5c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/9c24e5c5cc1cae332bc8cd72649a0b26834c3a00))
* **complaint:** add remove transition for responses ([1ed1eb5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1ed1eb5b9b2c0422a203bb4959b2a946225b47ca))
* **configuration:** add endpoint for ckeck configurations ([50bacb6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/50bacb60ebd5c4bf92bee570df6b0c31cd27b9b2))
* **notificatrions:** add notifications for complaints, listings, wanted ads and owners ([2acffd8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/2acffd848dc52b949de5453f8a554ae60723afc8))
* add apresentation title and informations of rooms ([004abb9](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/004abb94c8e4433e724484c86aac4f7da1cee70a))
* **complaints:** completion of the complaints service ([ac67dab](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ac67dabff9237444f1051d53b641dd834b279fb9))
* **configurations:** add owner regulation ([a025c01](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a025c01f2555a3e44d10aebe9d7aff05e818776f))
* **listings:** add update ([53a3be5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/53a3be551d68d885a7273f4858aa6520c79a1351))
* **owners:** create a owner and change status ([014bed6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/014bed63040758698b177f3cb8de5cd582595dbd))
* **removes:** add transitions for removes ([8067ca0](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8067ca0978e0ab295ccf85492c02418267685008))
* **wanted-ads:** add approve and reject ([b8fd76b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/b8fd76bbf01ca8bb35b9788a9614e4ef4efcdbed))
* removing required notes @ listing approve/reject ([cb284fc](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/cb284fc2b5e09fbf49c47a42a033fac9f3af093f))


### Performance Improvements

* **listings:** optimization of media request for listings ([6cd9b35](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6cd9b352019f5ff6804438c009bf746e4068d9a4))

# [1.0.0-rc.20](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2021-02-08)


### Bug Fixes

* listings service ([6349f14](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6349f1418f4c5311c8dfa1218e4ce5fe517fa7bc))
* type of latitude e longitude ([6174db7](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6174db77a769306260b8300cb71f948756871c79))

# [1.0.0-rc.19](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2021-02-05)


### Bug Fixes

* optimize code ([a07f6a4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a07f6a4ac50e917f621284002787064756b82ed7))

# [1.0.0-rc.18](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2021-02-05)


### Features

* add endpoint to return all locations actives ([05a6709](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/05a6709364462971d966f42d6cf55c1126d5a10b))

# [1.0.0-rc.17](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2021-01-26)


### Bug Fixes

* address_no, and add withRelated typology ([764f856](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/764f85657bbb1995e4711958ffca8924371f7904))

# [1.0.0-rc.16](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2021-01-25)


### Bug Fixes

* fix validations on listings ([13fbd28](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/13fbd2874f12682bcab6e1f62e7357eb795ef18b))

# [1.0.0-rc.15](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-01-25)


### Features

* fix and add rooms translations ([9c24e5c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/9c24e5c5cc1cae332bc8cd72649a0b26834c3a00))

# [1.0.0-rc.14](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-01-20)


### Bug Fixes

* **complaint:** add get user method ([a1ce340](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a1ce340d4a43c12968af3f73811b78e9d717adab))
* **configuration:** parse owner regulation ([e081b51](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e081b51f80e98a505dc9bef1a025121ec9ba20f8))
* **listing:** validate medias, translations, amenities and expenses ([cbf242b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/cbf242bcd50a7fa0bb08f799af0c1d72521f7d10))

# [1.0.0-rc.13](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-01-19)


### Bug Fixes

* **listing:** remove convert date ([c9276ca](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/c9276caa18f950ca6790b80691597d1e2f9455cf))

# [1.0.0-rc.12](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-01-19)


### Bug Fixes

* **complaints:** change insert for create ([4899c16](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4899c16d8dede6134313dd95d824dd83702d095e))
* **listing:** fix notifications ([ed7f54b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ed7f54b3db184a36763b7664c8ad704f03febfa6))
* **migrations:** change status ([d93ca74](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d93ca743191748786502120d6e5a3e2d3f7788fe))

# [1.0.0-rc.11](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-01-19)


### Bug Fixes

* add user id on listings creation ([bdfb8e4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/bdfb8e4155363b8ea26a5cfc6652a69515f4caef))
* randon onwer user_name ([6bcdb8d](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6bcdb8d31c3c329b752a898557a930843648e2db))

# [1.0.0-rc.10](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-01-18)


### Bug Fixes

* fix error on knex timeout ([714c5d2](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/714c5d207f15b8a9ba825e01f111169838f70fbf))

# [1.0.0-rc.9](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-01-18)


### Bug Fixes

* **lint:** fix lint errors ([8fc8492](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8fc8492882be9826e32962b44390decf13e12986))
* **migrations:** add drop table for rooms ([5d36b1c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5d36b1c9694e4f7855b453222b639f37beae959f))


### Features

* add apresentation title and informations of rooms ([004abb9](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/004abb94c8e4433e724484c86aac4f7da1cee70a))

# [1.0.0-rc.8](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-01-18)


### Bug Fixes

* **seed:** set seeds for configurations ([f608099](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f60809955b9b4b2713ec0f262c1443c79c12504a))


### Features

* **configuration:** add endpoint for ckeck configurations ([50bacb6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/50bacb60ebd5c4bf92bee570df6b0c31cd27b9b2))
* **notificatrions:** add notifications for complaints, listings, wanted ads and owners ([2acffd8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/2acffd848dc52b949de5453f8a554ae60723afc8))

# [1.0.0-rc.7](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-01-08)


### Bug Fixes

* **configurations:** data conversion ([da4a8a3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/da4a8a303639a10106914dcbe22ee8c36979b7a2))
* **owners:** fix list owners ([930afc1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/930afc1374f8fe610784fe2e905c65f6854b3bf2))
* **owners:** fix remove ([1228b4c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1228b4c8de58af925c9bd54b632cdffd37e4e7c8))


### Features

* **complaint:** add remove transition for responses ([1ed1eb5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1ed1eb5b9b2c0422a203bb4959b2a946225b47ca))
* **listings:** add update ([53a3be5](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/53a3be551d68d885a7273f4858aa6520c79a1351))
* **removes:** add transitions for removes ([8067ca0](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8067ca0978e0ab295ccf85492c02418267685008))
* **wanted-ads:** add approve and reject ([b8fd76b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/b8fd76bbf01ca8bb35b9788a9614e4ef4efcdbed))

# [1.0.0-rc.6](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-01-07)


### Bug Fixes

* **complaint:** fix withRelated ([1a3581c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1a3581c972f590487e91931219f1c935c550ed25))
* **configurations:** add set endpoints ([aecb5c9](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/aecb5c91ce775d03c78fc7cd082ff94039caf98d))
* **configurations:** fix save settings ([6da3cbf](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6da3cbf0b830a4823612ce86b8090f71db822a97))
* **listings:** add allow pets ([010de50](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/010de50e99f2c984ea2465f83637169ef77ef0a8))
* **listings:** add withRelateds and validations ([c651c4c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/c651c4c493832767505e2cbc3502b8d47741e30b))
* **typologies:** add endpoint for listings and wanted-ads ([77f4a1b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/77f4a1b0a9e6991c9b32ba0de652d844f593205f))
* **typologies:** fix update ([058fd1f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/058fd1fd789187f65907a9a0017f011c8786340d))
* **wanted_adds:** fix withRelated user ([df53448](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/df53448b9429b089189d6d9c5682413b4603bc81))


### Features

* **owners:** create a owner and change status ([014bed6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/014bed63040758698b177f3cb8de5cd582595dbd))

# [1.0.0-rc.5](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-01-05)


### Bug Fixes

* listings withRelated ([8c51db6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/8c51db6ae063a52253c746911944bb165702ea81))

# [1.0.0-rc.4](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2020-12-23)


### Bug Fixes

* **migrations:** add enums ([4175d35](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4175d356b6011582766caac86397cbde79cc57cf))


### Features

* **complaints:** completion of the complaints service ([ac67dab](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ac67dabff9237444f1051d53b641dd834b279fb9))

# [1.0.0-rc.3](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2020-12-11)


### Bug Fixes

* deploy QA ([3456b72](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3456b721a7b949339def527caec91f04fbf36ca6))

# [1.0.0-rc.2](https://gitlab.com/fi-sas/fisas_private_accommodation/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2020-12-11)


### Bug Fixes

* start.sh helper script ([a25f1f3](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a25f1f388de3232a771702cf8826e1a294b60f9f))

# 1.0.0-rc.1 (2020-10-22)


### Bug Fixes

* **docker-compose:** create docker-compose to prod and dev ([e30ca0f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/e30ca0f24417ee072b0307e9e5bf14cd28177f6d))
* **listing:** add house_typology and usable_space ([a0792b7](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a0792b7040776a5ff1ca965ddb22c6e3630a3e17))
* **listing:** add new load media in listing search and listing by id ([1194be6](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/1194be64db567111613b9b039f43534d7f6ba16c))
* **listing:** allow null in area and floor ([ad008e0](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ad008e02a396e5d5f8dcc2ec0b8f66e192d161b3))
* **listing:** change size in description ([86abcfc](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/86abcfcacfb2723447c37059333b58d0f020243d))
* **listing:** fix migration ([271140d](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/271140def4cbcbd7f540c5326c7491d9e08c2fe0))
* **listing:** owner data ([6e88056](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6e88056d5c46ee59a527967e9943ba259b34df78))
* **listing:** set area and floor optional ([2971d27](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/2971d27e2c287647de68d94beffe95fc0c57215c))
* **listing:** set owner id when device type is web ([6990540](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6990540d70e8b2d6c0a2a666e64be3e53e2f2d24))
* **listings:** add house_typology and usable_space ([898d0fa](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/898d0fa6ea593838fdfb43d19ddd798fe19373bb))
* **listings:** add medias file in listings ([520a2f8](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/520a2f83c59826cbfaece8e318a3b484726089b9))
* **listings:** remove country ([01a654c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/01a654c5856d32785160336208c2ed652f7601a6))
* **listings:** remove country in schema ([a308d67](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a308d67214bb69bc6b570c3622d0fecdca49c65f))
* **migrations:** delete duplicate code ([5f3b15b](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5f3b15bed0c952b01417bb7e4661729f02350333))
* **owner:** add issues receipt in owner ([d3c539a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/d3c539a58bc4a4486ff4cddd28080d7c9c84a9bf))
* **owner:** add parce to invalid owner ([441e397](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/441e397b7fadc6fafe69e731c6b336ea60b05198))
* **owner:** fix  issues receipt ([31fed4a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/31fed4a2d248e740b49e1bfd046e54f78887dcc4))
* **owner:** parse issues receipt to boolean ([ac255b1](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ac255b15e9d38e7102c768043d7677abdbdc17f1))
* **owner_applications:** remove validation for notes in change status ([f9adf7c](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/f9adf7c0f84ae157d0e03981e9b1b39659f2fe62))
* **owner_applications:** remove validation for notes when approve ([3e07f03](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3e07f0358e788c41919b2e80dc6074c4d88dfda0))
* **owner-application:** fix issues receipt ([ea858cf](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/ea858cf66acf3fc23259e32d482eea654505c890))
* allow no email posting owner @ owner ([10d06b4](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/10d06b4086937ba7867bc7479ce35f92603aa878))
* allow null latitude and longitude @ listing ([4c15996](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/4c159960d10c3703022d0be3b0db9987859d32b2))
* backoffice middleware missing @ PATCH listing ([a46a457](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a46a4571181f58d3d804377903140a507afb5df1))
* filter by owner_id @ owners ([487047f](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/487047fbfa0a266fac0f2b9c35085c0fe00e5154))
* fixing check owner validation @ PATCH listing ([21ff947](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/21ff947c24c7be5b7c1e31bfb0b834e13593fc83))
* testing related entities method - loadRelated with limit -1 ([bd30b2a](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/bd30b2a1abd93be09158b3490fe78717862c9154))
* **owner:** delete owner in cascade ([3856a81](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/3856a812b50892fba54db8213f6f51e56acfad48))
* **owner-applications:** add tin ([6724689](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/672468948b2f89216b44be98fdc034265549ceed))
* **owners:** create owner,activate and deactivate ([5b313da](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/5b313da4889a4343ff97e7a0ad963ef77b2f1126))
* **user data:** add user data in responses ([67d4a73](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/67d4a7382fdd9c5cef6424e42b787011557a1e57))


### Features

* **configurations:** add owner regulation ([a025c01](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/a025c01f2555a3e44d10aebe9d7aff05e818776f))
* removing required notes @ listing approve/reject ([cb284fc](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/cb284fc2b5e09fbf49c47a42a033fac9f3af093f))


### Performance Improvements

* **listings:** optimization of media request for listings ([6cd9b35](https://gitlab.com/fi-sas/fisas_private_accommodation/commit/6cd9b352019f5ff6804438c009bf746e4068d9a4))
