"use strict";
const _ = require("lodash");

/*****************************************************************************
 *             Checks for ids that do not exist in the database              *
 *****************************************************************************/

/**
 *
 * @param {object} ctx Molecular Context
 * @param {Array} ids IDS to be searched
 * @param {String} micro_service Micro service name
 * @param {String} service Service name
 */
async function getData(ctx, ids, micro_service, service) {
	return Promise.all(
		ids.map(async id => {
			const query = {};
			query["id"] = id;
			let data = await ctx.call(`${micro_service}.${service}.find`, {
				query
			});
			return data[0];
		})
	);
}

/**
 *
 * @param {object} ctx Molecular Context
 * @param {Array} ids IDS to be searched
 * @param {String} micro_service  Micro service name
 * @param {String} service Service name
 */
async function checkIdsIsNotExist(ctx, ids, micro_service, service) {
	let results = await getData(ctx, ids, micro_service, service);
	let result_ids = [];
	for (let result_response of results) {
		if (result_response !== undefined) {
			result_ids.push(result_response.id);
		}
	}
	return _.difference(ids, result_ids);
}

module.exports = {
	checkIdsIsNotExist,
};
