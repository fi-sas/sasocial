module.exports.up = async (db) =>
	db.schema
		.createTable("owner", (table) => {
			table.increments();
			table.text("description");
			table.bool("active").default(false).notNullable();
			table.bool("applicant_consent").notNullable();
			table.enum("status", ["Pending", "Approved", "Rejected"]).notNullable();
			table.integer("user_id").notNullable();
			table.bool("issues_receipt").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("typology", (table) => {
			table.increments();
			table.string("name", 255).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("configuration", (table) => {
			table.increments();
			table.string("key", 120).notNullable();
			table.string("value", 250).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
			table.unique("key");
		})

		.createTable("listing", (table) => {
			table.increments();
			table.string("apresentation_title").notNullable();
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology")
				.notNullable();
			table.enum("house_typology", ["HOUSE", "APARTMENT"]);
			table.enum("usable_space", ["WHOLE_HOUSE", "PRIVATE_ROOM", "SHARED_ROOM"]);
			table.integer("area").unsigned();
			table.integer("floor").unsigned();
			table.float("price").unsigned().notNullable();
			table.float("longitude", 10, 6);
			table.float("latitude", 10, 6);
			table.string("address", 250).notNullable();
			table.string("address_no", 25).nullable();
			table.string("postal_code", 10).notNullable();
			table.string("city", 250).notNullable();
			table.bool("allows_smokers").default(false).notNullable();
			table.bool("allows_pets").default(false).notNullable();
			table.enum("gender", ["M", "F", "I"]);
			table.enum("occupation", ["N", "F", "I"]);
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.integer("visualizations").default(0).notNullable();
			table.integer("owner_id").unsigned().references("id").inTable("owner");
			table.enum("status", ["Pending", "Approved", "Rejected"]);
			table.integer("user_id").unsigned();
			table.bool("active").default(true).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_amenities", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing")
				.notNullable()
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.bool("kitchen_access").default(false).notNullable();
			table.bool("microwave").default(false).notNullable();
			table.bool("stove").default(false).notNullable();
			table.bool("fridge").default(false).notNullable();
			table.bool("dishwasher").default(false).notNullable();
			table.bool("washing_machine").default(false).notNullable();
			table.bool("elevator").default(false).notNullable();
			table.bool("heating_system").default(false).notNullable();
			table.bool("cooling_system").default(false).notNullable();
			table.bool("tv").default(false).notNullable();
			table.bool("cable_tv").default(false).notNullable();
			table.bool("internet").default(false).notNullable();
			table.bool("parking_garage").default(false).notNullable();
			table.bool("private_wc").default(false).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_expenses", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing")
				.notNullable()
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.bool("water").default(false).notNullable();
			table.bool("gas").default(false).notNullable();
			table.bool("condo").default(false).notNullable();
			table.bool("electricity").default(false).notNullable();
			table.bool("cable_tv").default(false).notNullable();
			table.bool("internet").default(false).notNullable();
			table.bool("cleaning").default(false).notNullable();
			table.bool("furniture").default(false).notNullable();
			table.bool("bedding").default(false).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_media", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing")
				.notNullable()
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.integer("file_id");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_translation", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing")
				.notNullable()
				.onUpdate("CASCADE")
				.onDelete("CASCADE");
			table.integer("language_id").notNullable().unsigned();
			table.string("description", 1024);
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_history", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing");
			table.string("status").notNullable();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("rooms", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing").notNullable();
			table.integer("price").unsigned().notNullable();
			table.integer("number_rooms").unsigned().notNullable();
			table.enum("room_type", ["PRIVATE_ROOM", "SHARED_ROOM"]).notNullable();
			table.bool("shared_wc").notNullable();
			table.string("description").nullable();
			table.bool("active").notNullable().default(true);
		})

		.createTable("wanted_ad", (table) => {
			table.increments();
			table.string("author_name", 250).notNullable();
			table.string("author_nationality", 250).notNullable();
			table.string("author_phone", 25).notNullable();
			table.string("author_email", 120).notNullable();
			table
				.integer("typology_id")
				.unsigned()
				.references("id")
				.inTable("typology")
				.notNullable();
			table.float("max_price").unsigned().notNullable();
			table.enum("furnished", ["Y", "N", "I"]);
			table.string("zone", 250).notNullable();
			table.string("description", 250).notNullable();
			table.bool("author_consent").notNullable();
			table.datetime("start_date").notNullable();
			table.datetime("end_date").nullable();
			table.enum("status", ["Pending", "Approved", "Rejected"]);
			table.integer("user_id").unsigned();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("wanted_ad_history", (table) => {
			table.increments();
			table
				.integer("wanted_ad_id")
				.unsigned()
				.references("id")
				.inTable("wanted_ad");
			table.string("status", 16).notNullable();
			table.integer("user_id").unsigned();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("complaint", (table) => {
			table.increments();
			table
				.integer("listing_id")
				.unsigned()
				.references("id")
				.inTable("listing")
				.notNullable();
			table.text("description").notNullable();
			table.integer("user_id").notNullable();
			table.enum("status", ["New", "Closed"]).notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("complaint_response", (table) => {
			table.increments();
			table
				.integer("complaint_id")
				.unsigned()
				.references("id")
				.inTable("complaint")
				.notNullable();
			table.text("description").notNullable();
			table.integer("user_id").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("owner_history", (table) => {
			table.increments();
			table.integer("owner_id").unsigned().references("id").inTable("owner");
			table.enum("status", ["Pending", "Approved", "Rejected"]);
			table.integer("user_id").unsigned();
			table.bool("active").default(false).notNullable();
			table.string("notes");
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		})

		.createTable("listing_search", (table) => {
			table.increments();
			table.integer("typology_id");
			table.enum("house_typology", ["HOUSE", "APARTMENT"]);
			table.enum("usable_space", ["WHOLE_HOUSE", "PRIVATE_ROOM", "SHARED_ROOM"]);
			table.float("min_price", 8, 2).unsigned();
			table.float("max_price", 8, 2).unsigned();
			table.string("city", 250);
			table.string("address", 250);
			table.integer("min_area", 10).unsigned();
			table.integer("max_area", 10).unsigned();
			table.enum("gender", ["M", "F", "I"]);
			table.enum("occupation", ["N", "F", "I"]);
			table.bool("included_expenses").default(false).notNullable();
			table.bool("allows_pets").default(false).notNullable();
			table.bool("kitchen_access").default(false).notNullable();
			table.bool("tv").default(false).notNullable();
			table.bool("parking_garage").default(false).notNullable();
			table.bool("private_wc").default(false).notNullable();
			table.bool("furniture").default(false).notNullable();
			table.bool("bedding").default(false).notNullable();
			table.integer("user_id").unsigned();
			table.datetime("search_date").notNullable();
		});

module.exports.down = async (db) =>
	db.schema
		.dropTable("complaint_response")
		.dropTable("complaint")
		.dropTable("configuration")
		.dropTable("listing_search")
		.dropTable("listing_amenities")
		.dropTable("listing_expenses")
		.dropTable("listing_media")
		.dropTable("listing_translation")
		.dropTable("listing_history")
		.dropTable("rooms")
		.dropTable("listing")
		.dropTable("wanted_ad_history")
		.dropTable("wanted_ad")
		.dropTable("owner_history")
		.dropTable("owner_application_history")
		.dropTable("owner_application")
		.dropTable("owner")
		.dropTable("typology");

module.exports.configuration = { transaction: true };
