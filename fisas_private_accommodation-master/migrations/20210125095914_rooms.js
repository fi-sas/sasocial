module.exports.up = async (db) =>
	db.schema
		.alterTable("rooms", (table) => {
			table.double("area").unsigned();
			table.dropColumn("number_rooms");
			table.dropColumn("description");
			table.double("price").unsigned().notNullable().alter();
		})
		.alterTable("listing", (table) => {
			table.integer("rooms_number");
			table.dropColumn("usable_space");
			table.double("price").unsigned().alter();
			table.double("area").unsigned().alter();
		})
		.createTable("room_translations", (table) => {
			table.increments();
			table.integer("room_id").unsigned().notNullable().references("id").inTable("rooms");
			table.integer("language_id").unsigned().notNullable();
			table.string("description").notNullable();
			table.datetime("updated_at").notNullable();
			table.datetime("created_at").notNullable();
		});

module.exports.down = async db => db.schema
	.alterTable("rooms", (table) => {
		table.dropColumn("area");
		table.integer("number_rooms").unsigned().notNullable();
		table.string("description").nullable();
		table.integer("price").unsigned().notNullable().alter();
	})
	.alterTable("listing", (table) => {
		table.enum("usable_space", ["WHOLE_HOUSE", "PRIVATE_ROOM", "SHARED_ROOM"]);
		table.float("price").unsigned().notNullable().alter();
		table.integer("area").unsigned().alter();
		table.dropColumn("rooms_number");
	})
	.dropTable("room_translations");

module.exports.configuration = { transaction: true };
