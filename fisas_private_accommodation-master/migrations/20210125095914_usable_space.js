module.exports.up = async (db) =>
	db.schema
		.alterTable("listing", (table) => {
			table.enum("usable_space", ["WHOLE_HOUSE", "SHARED_ROOM"]);
		});
module.exports.down = async db => db.schema
	.alterTable("listing", (table) => {
		table.enum("usable_space", ["WHOLE_HOUSE", "PRIVATE_ROOM", "SHARED_ROOM"]).alter();
	});
module.exports.configuration = { transaction: true };