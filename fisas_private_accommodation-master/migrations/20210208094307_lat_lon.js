module.exports.up = async (db) =>
    db.schema
        .alterTable("listing", (table) => {
            table.double("latitude").alter();
            table.double("longitude").alter();
        });
module.exports.down = async db => db.schema
    .alterTable("listing", (table) => {
        table.float("longitude", 10, 6).alter();
        table.float("latitude", 10, 6).alter();
    });
module.exports.configuration = { transaction: true };