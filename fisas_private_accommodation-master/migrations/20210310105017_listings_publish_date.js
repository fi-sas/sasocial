

module.exports.up = async (db) =>
	db.schema
		.alterTable("listing", (table) => {
			table.datetime("publish_date");
		});
module.exports.down = async db => db.schema
	.alterTable("listing", (table) => {
		table.dropColumn("publish_date");
	});
module.exports.configuration = { transaction: true };