
module.exports.up = async (db) =>
db.schema
    .alterTable("listing", (table) => {
        table.boolean("has_deleted").notNullable().defaultTo(false);
    });
module.exports.down = async db => db.schema
.alterTable("listing", (table) => {
    table.dropColumn("has_deleted");
});
module.exports.configuration = { transaction: true };