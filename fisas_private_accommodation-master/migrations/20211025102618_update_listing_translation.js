module.exports.up = async (db) =>
db.schema
    .alterTable("listing_translation", (table) => {
        table.string("name", 1024);
    })
    .alterTable("listing", (table) => {
        table.dropColumn("apresentation_title");
    });

module.exports.down = async db => db.schema
    .alterTable("listing_translation", (table) => {
        table.dropColumn("name");
    })
    .alterTable("listing", (table) => {
        table.string("apresentation_title").notNullable();
    });

module.exports.configuration = { transaction: true };
