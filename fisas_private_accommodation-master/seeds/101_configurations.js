exports.seed = (knex) => {
    return Promise.all([
		knex("configuration")
			.select()
			.where("key", "admin_notification_email")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "admin_notification_email",
						value: "",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "owner_user_group_ids")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "owner_user_group_ids",
						value: "",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "owner_profile_id")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "owner_profile_id",
						value: "2",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "max_wantedads_publish_days")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "max_wantedads_publish_days",
						value: "5",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "max_listing_publish_days")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "max_listing_publish_days",
						value: "5",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "max_medias_per_listing")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "max_medias_per_listing",
						value: "5",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			}),

			knex("configuration")
			.select()
			.where("key", "owner_regulation_file_id")
			.then(async (rows) => {
				if (rows.length === 0) {
					return knex("configuration").insert({
						key: "owner_regulation_file_id",
						value: "",
						created_at: new Date(),
						updated_at: new Date(),
					});
				}
				return true;
			})
			.catch((err) => {
				// eslint-disable-next-line no-console
				console.error(err);
			})
	]);
}