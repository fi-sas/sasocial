exports.seed = (knex) =>
  knex("owner")
    .select()
    .where("user_id", 2)
    .then(async (rows) => {
      if (rows.length === 0) {
        return knex("owner").insert({
          id: 1,
          description: "",
          active: true,
          applicant_consent: true,
          status: "Approved",
          user_id: 2,
          issues_receipt: true,
          created_at: new Date(),
          updated_at: new Date(),
        });
      }
      return true;
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.error(err);
    });
