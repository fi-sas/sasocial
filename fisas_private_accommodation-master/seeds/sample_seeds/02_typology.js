exports.seed = (knex) =>
  knex("typology")
    .select()
    .where("name", "Quarto")
    .then(async (rows) => {
      if (rows.length === 0) {
        return knex("typology").insert({
          name: "Quarto",
          created_at: new Date(),
          updated_at: new Date(),
        });
      }
      return true;
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.error(err);
    });
