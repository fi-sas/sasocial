"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { COMPLAINT_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.complaints",
	table: "complaint",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "complaints")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"listing_id",
			"description",
			"user_id",
			"status",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			listing(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"private_accommodation.listings",
					"listing",
					"listing_id"
				);
			},
			response(ids, docs, rule, ctx) {
				if (docs.length !== 0)
					return ctx.call("private_accommodation.complaint_response.find", {
						query: {
							complaint_id: docs[0].id,
						}
					}).then(result => {
						return Promise.resolve(docs.map(d => d.response = result.find(c => c ? (c.complaint_id === d.id) : false) || {}));
					});
			},
			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id"
				);
			},
		},
		entityValidator: {
			listing_id: { type: "number", integer: true,
				positive: true,
				convert: true, },
			description: { type: "string" },
			user_id: { type: "number", integer: true, min: 1 },
			status: { type: "enum", values: _.values(COMPLAINT_STATUS) },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				async function isExistListing(ctx) {
					if (ctx.params.listing_id) {
						await ctx.call("private_accommodation.listings.get", { id: ctx.params.listing_id });
					} else {
						throw new Errors.ValidationError(
							"'listing_id' is required!",
							"LISTING_ID_REQUIRED",
							{}
						);
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.status = "New";
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			list: [
				function setUserIfNotIsBackoffice(ctx) {
					if (!ctx.meta.isBackoffice)
						ctx.params.query = { user_id: ctx.meta.user.id };
				}
			],
			remove: [
				"deleteRelatedResponses",
			]
		},
		after : {
			create: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx,"NEW_COMPLAINT",_.first(res).user_id, _.first(res));
					return res;
				},
				async function ReturnComplaint(ctx, res) {
					return res;
				}
			],
			sendResponse: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx,"COMPLAINT_RESPONSE",_.first(res).user_id, _.first(res));
				},
				async function ReturnComplaint(ctx, res) {
					return res;
				}
			]
		}
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		create: {
			// REST: POST /
			visibility: "published",
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		getResponse: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/:id/response",
			},
			scope: "private_accommodation:complaints:get-response",
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
			},
			async handler(ctx) {
				if (ctx.meta.isBackoffice)
					return ctx.call("private_accommodation.complaint_response.find",
						{ query: { complaint_id: ctx.params.id } });
				else
					return ctx.call("private_accommodation.complaint_response.find",
						{ query: { complaint_id: ctx.params.id, user_id: ctx.meta.user.id } });
			},
		},
		sendResponse: {
			visibility: "published",
			rest: {
				method: "POST",
				path: "/:id/response",
			},
			scope: "private_accommodation:complaints:send-response",
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
				description: {
					type: "string",
				}
			},
			async handler(ctx) {

				await ctx.call("private_accommodation.complaints.get", { id: ctx.params.id });
				ctx.params.complaint_id = ctx.params.id;
				const response = await ctx.call("private_accommodation.complaint_response.find", { query: {
					complaint_id : ctx.params.complaint_id
				} });
				if(response.length > 0 ){
					throw new Errors.ValidationError(
						"Already exist response for this complain",
						"PRIVATE_ACCOMMODATION_RESPONSE_ALREADY_EXIST",
						{}
					);
				}
				return ctx.call("private_accommodation.complaint_response.create",
					ctx.params);
			},
		},
		getListing: {
			visibility: "published",
			rest: {
				method: "GET",
				path: "/:id/listing",
			},
			scope: "private_accommodation:listings:read",
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
				withRelated: {
					type: "string"
				}
			},
			async handler(ctx) {
				let complaint;
				if (ctx.meta.isBackoffice)
					complaint = await ctx.call("private_accommodation.complaints.get", { id: ctx.params.id });
				else
					complaint = await ctx.call("private_accommodation.complaints.find",
						{ query: { id: ctx.params.id, user_id: ctx.meta.user.id } });


				return ctx.call("private_accommodation.listings.find",
					{ query: { id: complaint[0].listing_id }, withRelated: ctx.params.withRelated });
			},
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {
		async deleteRelatedResponses(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.complaint_response.find", {
					query: {
						complaint_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (complaintResponseOderRel) =>
							await ctx.call("private_accommodation.complaint_response.remove", { id: complaintResponseOderRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Complaint Response relation!", err));
		},
		/**
		 * Send a notification for user
		 * @param {object} ctx
		 * @param {string} type
		 * @param {integer} user_id
		 * @param {string} name
		 * @param {string} complaint
		 */
		async sendNotification(ctx, type, user_id, complaint) {
			let user = await this.getUser(ctx, user_id);
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "PRIVATE_ACCOMMODATION_" + type.toUpperCase(),
				user_id: user_id,
				user_data: { email: _.first(user).email, phone: _.first(user).phone },
				data: { complaint: complaint , user: _.first(user) },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
		/**
		 * Get user data
		 * @param {object} ctx
		 * @param {integer} user_id
		 */
		async getUser(ctx, id) {
			return await ctx.call("authorization.users.get", { id } );
		},

	},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
