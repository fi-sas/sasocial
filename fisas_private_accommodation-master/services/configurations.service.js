"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const _ = require("lodash");
const { checkIdsIsNotExist } = require("../helpers/checkers");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.configurations",
	table: "configuration",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "configurations")],

	/**
	 * Settings
	 */
	settings: {
		fields: ["id", "key", "value", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			key: "string|max:120",
			value: "string|max:250",
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			setOwnerRegulation: ["validateMedia"],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			// REST: GET /
			scope: "private_accommodation:configurations:configure",
			visibility: "published",
			async handler(ctx) {
				const configs = await this._find(ctx, {});
				const result = {};
				configs.forEach((conf) => {
					result[conf.key] = conf.value;
				});

				result.owner_profile_id = result.owner_profile_id
					? parseInt(result.owner_profile_id)
					: null;
				result.max_wantedads_publish_days = result.max_wantedads_publish_days
					? parseInt(result.max_wantedads_publish_days)
					: null;
				result.max_medias_per_listing = result.max_medias_per_listing
					? parseInt(result.max_medias_per_listing)
					: null;
				result.max_listing_publish_days = result.max_listing_publish_days
					? parseInt(result.max_listing_publish_days)
					: null;
				result.owner_regulation_file_id = result.owner_regulation_file_id
					? parseInt(result.owner_regulation_file_id)
					: null;

				if (result.owner_regulation_file_id) {
					let owner_regulation_file = null;
					try {
						owner_regulation_file = await ctx.call("media.files.get", {
							id: result.owner_regulation_file_id,
						});
						result.owner_regulation_file = owner_regulation_file[0];
					} catch (ex) {
						result.owner_regulation_file_id = null;
					}
				}

				return result;
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			scope: "private_accommodation:configurations:configure",
			params: {
				admin_notification_email: { type: "email" },
				max_listing_publish_days: { type: "number", positive: true, convert: true },
				max_medias_per_listing: { type: "number", positive: true, convert: true },
				max_wantedads_publish_days: { type: "number", positive: true, convert: true },
				owner_profile_id: { type: "number", positive: true, convert: true },
				owner_regulation_file_id: { type: "number", positive: true, convert: true },
				owner_user_group_ids: {
					type: "array",
					items: { type: "number", positive: true, convert: true },
				},
				$$strict: "remove",
			},
			handler(ctx) {
				const keys = Object.keys(ctx.params);
				const promisses = keys.map((key) => {
					return this._find(ctx, { query: { key } }).then((config) => {
						if (config.length > 0 && ctx.params[config[0].key]) {
							return this._update(
								ctx,
								{ id: config[0].id, value: JSON.stringify(ctx.params[config[0].key]) },
								true,
							);
						}
					});
				});
				return Promise.all(promisses).then(() =>
					ctx.call("private_accommodation.configurations.list", {}),
				);
			},
		},

		getMaxMediasPerListing: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-max_medias_per_listing",
			rest: {
				method: "GET",
				path: "/max_medias_per_listing",
			},
			params: {},
			async handler(ctx) {
				let maxMediasPerListing = await this._find(ctx, {
					query: {
						key: "max_medias_per_listing",
					},
				});

				try {
					return parseInt(maxMediasPerListing[0].value);
				} catch (ex) {
					return null;
				}
			},
		},

		getMaxListingPublishDays: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-max_listing_publish_days",
			rest: {
				method: "GET",
				path: "/max_listing_publish_days",
			},
			params: {},
			async handler(ctx) {
				let maxListingPublishDays = await this._find(ctx, {
					query: {
						key: "max_listing_publish_days",
					},
				});

				try {
					return parseInt(maxListingPublishDays[0].value);
				} catch (ex) {
					return null;
				}
			},
		},

		getMaxWantedadsPublishDays: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-max_wantedads_publish_days",
			rest: {
				method: "GET",
				path: "/max_wantedads_publish_days",
			},
			params: {},
			async handler(ctx) {
				let maxWantedadsPublishDays = await this._find(ctx, {
					query: {
						key: "max_wantedads_publish_days",
					},
				});

				try {
					return parseInt(maxWantedadsPublishDays[0].value);
				} catch (ex) {
					return null;
				}
			},
		},

		getAdminNotificationEmail: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-admin_notification_email",
			rest: {
				method: "GET",
				path: "/admin_notification_email",
			},
			params: {},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						key: "admin_notification_email",
					},
				}).then((result) => {
					try {
						return JSON.parse(result[0]);
					} catch (ex) {
						return null;
					}
				});
			},
		},

		getOwnerProfileId: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-owner_profile_id",
			rest: {
				method: "GET",
				path: "/owner_profile_id",
			},
			params: {},
			async handler(ctx) {
				let ownerProfileId = await this._find(ctx, {
					query: {
						key: "owner_profile_id",
					},
				});

				try {
					return parseInt(ownerProfileId[0].value, 10);
				} catch (ex) {
					return null;
				}
			},
		},

		getOwnerUserGroupIds: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-owner_user_group_ids",
			rest: {
				method: "GET",
				path: "/owner_user_group_ids",
			},
			params: {},
			async handler(ctx) {
				let ownerUserGroupIds = await this._find(ctx, {
					query: {
						key: "owner_user_group_ids",
					},
				});

				try {
					return JSON.parse(ownerUserGroupIds[0].value);
				} catch (ex) {
					return [];
				}
			},
		},

		getOwnerRegulation: {
			visibility: "published",
			scope: "private_accommodation:configurations:get-owner_regulation",
			rest: {
				method: "GET",
				path: "/owner_regulation",
			},
			params: {},
			async handler(ctx) {
				let ownerRegulation = await this._find(ctx, {
					query: {
						key: "owner_regulation_file_id",
					},
				});
				try {
					return parseInt(ownerRegulation[0].value);
				} catch (ex) {
					return null;
				}
			},
		},

		isConfigured: {
			visibility: "published",
			scope: "private_accommodation:configurations:is_configured",
			rest: {
				method: "GET",
				path: "/is_configured",
			},
			params: {},
			async handler(ctx) {
				let keysConfigured = [];
				let listing = {
					key: "LISTINGS",
					configured: true,
					keys: [],
				};
				let owner = {
					key: "OWNERS",
					configured: true,
					keys: [],
				};
				let wantedAds = {
					key: "WANTED_ADS",
					configured: true,
					keys: [],
				};
				let ownerNotification = {
					key: "OWNER_NOTIFICATION",
					configured: true,
					keys: [],
				};

				let configurations = await this._list(ctx, {});
				if (configurations.length !== 0) {
					let maxMediasPerListing = _.find(configurations.rows, ["key", "max_medias_per_listing"]);

					if (maxMediasPerListing && parseInt(maxMediasPerListing.value, 10)) {
						listing.keys.push({ key: "max_medias_per_listing", configured: true });
					} else {
						listing.configured = false;
						listing.keys.push({ key: "max_medias_per_listing", configured: false });
					}

					let maxListingPublishDays = _.find(configurations.rows, [
						"key",
						"max_listing_publish_days",
					]);

					if (maxListingPublishDays && parseInt(maxListingPublishDays.value, 10)) {
						listing.keys.push({ key: "max_listing_publish_days", configured: true });
					} else {
						listing.configured = false;
						listing.keys.push({ key: "max_listing_publish_days", configured: false });
					}

					let maxWantedadsPublishDays = _.find(configurations.rows, [
						"key",
						"max_wantedads_publish_days",
					]);

					if (maxWantedadsPublishDays && parseInt(maxWantedadsPublishDays.value, 10)) {
						wantedAds.keys.push({ key: "max_wantedads_publish_days", configured: true });
					} else {
						wantedAds.configured = false;
						wantedAds.keys.push({ key: "max_wantedads_publish_days", configured: false });
					}

					let adminNotificationEmail = _.find(configurations.rows, [
						"key",
						"admin_notification_email",
					]);

					if (adminNotificationEmail && adminNotificationEmail.value) {
						ownerNotification.keys.push({ key: "adminNotificationEmail", configured: true });
					} else {
						ownerNotification.configured = false;
						ownerNotification.keys.push({ key: "adminNotificationEmail", configured: false });
					}

					let ownerProfileId = _.find(configurations.rows, ["key", "owner_profile_id"]);

					if (ownerProfileId && parseInt(ownerProfileId.value, 10)) {
						owner.keys.push({ key: "owner_profile_id", configured: true });
					} else {
						owner.configured = false;
						owner.keys.push({ key: "owner_profile_id", configured: false });
					}

					let ownerUserGroupIds = _.find(configurations.rows, ["key", "owner_user_group_ids"]);

					if (
						ownerUserGroupIds &&
						ownerUserGroupIds.value.split(",").map((id) => parseInt(id, 10))
					) {
						owner.keys.push({ key: "owner_user_group_ids", configured: true });
					} else {
						owner.configured = false;
						owner.keys.push({ key: "owner_user_group_ids", configured: false });
					}

					let owner_regulation_file_id = _.find(configurations.rows, [
						"key",
						"owner_regulation_file_id",
					]);

					if (owner_regulation_file_id && owner_regulation_file_id.value) {
						let file = owner_regulation_file_id.value;
						if (file.file_id !== null) {
							owner.keys.push({ key: "owner_regulation_file_id", configured: true });
						} else {
							owner.configured = false;
							owner.keys.push({ key: "owner_regulation_file_id", configured: false });
						}
					} else {
						owner.configured = false;
						owner.keys.push({ key: "owner_regulation_file_id", configured: false });
					}

					keysConfigured = [listing, owner, wantedAds, ownerNotification];

					return keysConfigured;
				}
				return false;
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Update configuration value
		 * @param {object} ctx
		 * @param {string} key
		 */
		async updateConfiguration(ctx, key) {
			let isUpdate = await this.adapter.updateMany(
				{
					key: key,
				},
				{
					value: ctx.params.value,
				},
			);
			if (!isUpdate) {
				await this.adapter.insert({
					key: key,
					value: ctx.params.value,
					created_at: new Date(),
					updated_at: new Date(),
				});
			}
		},
		/**
		 * Validate if file is exist
		 * @param {object} ctx
		 */
		async validateMedia(ctx) {
			if (ctx.params.value.file_id) {
				await ctx.call("media.files.get", { id: ctx.params.value.file_id });
			} else {
				throw new Errors.ValidationError("'file_id' is required!", "FILE_ID_REQUIRED", {});
			}
		},
		/**
		 * Validate if user group id exist
		 * @param {object} ctx
		 * @param {array} userGroupsIds
		 */
		async validateGroupIDS(ctx, userGroupsIds) {
			let ids = await checkIdsIsNotExist(
				ctx,
				userGroupsIds.map((user_group_id) => user_group_id),
				"authorization",
				"user-groups",
			);
			if (ids.length !== 0)
				throw new Errors.ValidationError(
					`There are no user groups with these ids: ${ids.toString()}`,
					"NOT_FOUND_USER_GROUPS",
					{
						user_groups_ids: ids,
					},
				);
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
