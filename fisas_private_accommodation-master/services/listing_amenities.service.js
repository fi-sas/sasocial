"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.listing_amenities",
	table: "listing_amenities",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "listing_amenities")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"listing_id",
			"kitchen_access",
			"microwave",
			"stove",
			"fridge",
			"dishwasher",
			"washing_machine",
			"elevator",
			"heating_system",
			"cooling_system",
			"tv",
			"cable_tv",
			"internet",
			"parking_garage",
			"private_wc",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			listing_id: { type: "number", integer: true, min: 1, optional: false },
			kitchen_access: { type: "boolean", optional: true },
			microwave: { type: "boolean", optional: true },
			stove: { type: "boolean", optional: true },
			fridge: { type: "boolean", optional: true },
			dishwasher: { type: "boolean", optional: true },
			washing_machine: { type: "boolean", optional: true },
			elevator: { type: "boolean", optional: true },
			heating_system: { type: "boolean", optional: true },
			cooling_system: { type: "boolean", optional: true },
			tv: { type: "boolean", optional: true },
			cable_tv: { type: "boolean", optional: true },
			internet: { type: "boolean", optional: true },
			parking_garage: { type: "boolean", optional: true },
			private_wc: { type: "boolean", optional: true },
			created_at: { type: "date", optional: false },
			updated_at: { type: "date", optional: false },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		// insert: {
		//   // REST: POST /
		//   visibility: "published",
		// },
		// // list: {
		// //   // REST: GET /
		// //   visibility: "published",
		// // },
		// get: {
		//   // REST: GET /:id
		//   visibility: "published",
		// }
		// update: {
		//   // REST: PUT /:id
		//   visibility: "published",
		// },
		// patch: {
		//   // REST: PATCH /:id
		//   visibility: "published",
		// },
		// remove: {
		//   // REST: DELETE /:id
		//   visibility: "published",
		// },
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
