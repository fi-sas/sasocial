"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.listing_expenses",
	table: "listing_expenses",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "listing_expenses")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"listing_id",
			"water",
			"gas",
			"condo",
			"electricity",
			"cable_tv",
			"internet",
			"cleaning",
			"furniture",
			"bedding",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			listing_id: { type: "number", integer: true, min: 1, optional: false },
			water: { type: "boolean", optional: true },
			gas: { type: "boolean", optional: true },
			condo: { type: "boolean", optional: true },
			electricity: { type: "boolean", optional: true },
			cable_tv: { type: "boolean", optional: true },
			internet: { type: "boolean", optional: true },
			cleaning: { type: "boolean", optional: true },
			furniture: { type: "boolean", optional: true },
			bedding: { type: "boolean", optional: true },
			created_at: { type: "date", optional: false },
			updated_at: { type: "date", optional: false },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		// insert: {
		//   // REST: POST /
		//   visibility: "published",
		// },
		// list: {
		//   // REST: GET /
		//   visibility: "published",
		// },
		// get: {
		//   // REST: GET /:id
		//   visibility: "published",
		// },
		// update: {
		//   // REST: PUT /:id
		//   visibility: "published",
		// },
		// patch: {
		//   // REST: PATCH /:id
		//   visibility: "published",
		// },
		// remove: {
		//   // REST: DELETE /:id
		//   visibility: "published",
		// },
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
