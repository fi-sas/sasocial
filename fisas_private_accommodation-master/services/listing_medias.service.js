"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.listing_medias",
	table: "listing_media",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "listing_medias")],

	/**
   * Settings
   */
	settings: {
		fields: ["id", "listing_id", "file_id", "created_at", "updated_at"],
		defaultWithRelateds: ["file"],
		withRelateds: {
			file(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"media.files",
					"file",
					"file_id",
					"id"
				);
			},
		},
		entityValidator: {
			listing_id: { type: "number", integer: true, min: 1, optional: false },
			file_id: { type: "number", integer: true, min: 1, optional: false },
			created_at: { type: "date", optional: false },
			updated_at: { type: "date", optional: false },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		// insert: {
		//   // REST: POST /
		//   visibility: "published",
		// },
		// list: {
		//   // REST: GET /
		//   visibility: "published",
		// },
		// get: {
		//   // REST: GET /:id
		//   visibility: "published",
		// },
		// update: {
		//   // REST: PUT /:id
		//   visibility: "published",
		// },
		// patch: {
		//   // REST: PATCH /:id
		//   visibility: "published",
		// },
		// remove: {
		//   // REST: DELETE /:id
		//   visibility: "published",
		// },
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
