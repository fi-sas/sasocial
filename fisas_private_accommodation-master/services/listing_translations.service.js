"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.listing_translations",
	table: "listing_translation",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "listing_translations")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"listing_id",
			"language_id",
			"name",
			"description",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			listing_id: { type: "number", integer: true, min: 1, optional: false },
			language_id: { type: "number", integer: true, min: 1, optional: false },
			name: { type: "string", max: 1024, optional: false },
			description: { type: "string", max: 1024, optional: false },
			created_at: { type: "date", optional: false },
			updated_at: { type: "date", optional: false },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		// insert: {
		//   // REST: POST /
		//   visibility: "published",
		// },
		// list: {
		//   // REST: GET /
		//   visibility: "published",
		// },
		// get: {
		//   // REST: GET /:id
		//   visibility: "published",
		// },
		// update: {
		//   // REST: PUT /:id
		//   visibility: "published",
		// },
		// patch: {
		//   // REST: PATCH /:id
		//   visibility: "published",
		// },
		// remove: {
		//   // REST: DELETE /:id
		//   visibility: "published",
		// },
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
