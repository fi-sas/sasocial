"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { Errors } = require("@fisas/ms_core").Helpers;
const {
	LISTING_HOUSE_TYPOLOGY,
	LISTING_USABLE_SPACE,
	LISTING_GENDER,
	LISTING_OCCUPATION,
	LISTING_STATUS,
} = require("../utils/constants");
const _ = require("lodash");
const { checkIdsIsNotExist } = require("../helpers/checkers");
const moment = require("moment");
moment.locale("pt");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.listings",
	table: "listing",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "listings")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			//"apresentation_title",
			"amenities",
			"typology_id",
			"house_typology",
			"usable_space",
			"area",
			"floor",
			"price",
			"longitude",
			"latitude",
			"address",
			"address_no",
			"postal_code",
			"city",
			"allows_smokers",
			"allows_pets",
			"gender",
			"occupation",
			"start_date",
			"end_date",
			"visualizations",
			"owner_id",
			"status",
			"user_id",
			"rooms_number",
			"active",
			"has_deleted",
			"created_at",
			"updated_at",
			"publish_date",
		],
		defaultWithRelateds: ["amenities", "expenses", "translations", "rooms", "medias"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
			owner(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "private_accommodation.owners", "owner", "owner_id");
			},
			amenities(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((listing) => {
						return ctx
							.call("private_accommodation.listing_amenities.find", {
								query: {
									listing_id: listing.id,
								},
							})
							.then((res) => (listing.amenities = res[0]));
					}),
				);
			},
			expenses(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map((listing) => {
						return ctx
							.call("private_accommodation.listing_expenses.find", {
								query: {
									listing_id: listing.id,
								},
							})
							.then((res) => (listing.expenses = res[0]));
					}),
				);
			},
			medias(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.listing_medias",
					"medias",
					"id",
					"listing_id",
				);
			},
			translations(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.listing_translations",
					"translations",
					"id",
					"listing_id",
				);
			},
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.listing_history",
					"history",
					"id",
					"listing_id",
				);
			},
			rooms(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "private_accommodation.rooms", "rooms", "id", "listing_id");
			},
			typology(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "private_accommodation.typologies", "typology", "typology_id");
			},
		},
		entityValidator: {
			//apresentation_title: { type: "string" },
			typology_id: { type: "number", integer: true, min: 1, optional: false },
			house_typology: {
				type: "enum",
				values: _.values(LISTING_HOUSE_TYPOLOGY),
				optional: false,
			},
			usable_space: {
				type: "enum",
				values: _.values(LISTING_USABLE_SPACE),
				optional: false,
			},
			area: {
				type: "number",
				positive: true,
				optional: true,
			},
			floor: {
				type: "number",
				integer: true,
				positive: true,
				min: 0,
				optional: true,
			},
			price: { type: "number", min: 0, optional: false },
			longitude: { type: "number", optional: true },
			latitude: { type: "number", optional: true },
			address: { type: "string", max: 250, optional: false },
			address_no: { type: "string", max: 25, optional: false },
			postal_code: { type: "string", max: 10, optional: false },
			city: { type: "string", max: 250, optional: false },
			allows_smokers: { type: "boolean", optional: true },
			allows_pets: { type: "boolean", optional: true },
			gender: {
				type: "enum",
				values: _.values(LISTING_GENDER),
				optional: false,
			},
			occupation: {
				type: "enum",
				values: _.values(LISTING_OCCUPATION),
				optional: false,
			},
			rooms: {
				type: "array",
				item: {
					type: "object",
					props: {
						price: { type: "number", positive: true },
						room_type: { type: "enum", values: ["PRIVATE_ROOM", "SHARED_ROOM"] },
						shared_wc: { type: "boolean" },
						area: { type: "number", convert: true },
						active: { type: "boolean", default: true },
						translations: {
							type: "array",
							items: {
								type: "object",
								props: {
									language_id: { type: "number", convert: true },
									description: { type: "string" },
								},
							},
						},
					},
				},
				optional: true,
			},
			rooms_number: { type: "number", integer: true, convert: true },
			start_date: { type: "date", optional: false, convert: true },
			end_date: { type: "date", optional: false, convert: true },
			visualizations: { type: "number", integer: true, min: 0, optional: true },
			owner_id: { type: "number", integer: true, min: 1, optional: false },
			status: { type: "enum", values: _.values(LISTING_STATUS) },
			user_id: { type: "number", integer: true, min: 1, optional: false },
			active: { type: "boolean", optional: true },
			has_deleted: { type: "boolean", optional: true, nullable: true },
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				async function checkAmenities(ctx) {
					if (!ctx.params.amenities && ctx.params.amenities.length === 0) {
						throw new Errors.ValidationError("'amenities' is empty", "EMPTY_AMENITIES", {});
					}
				},
				async function checkExpenses(ctx) {
					if (!ctx.params.expenses && ctx.params.expenses.length === 0) {
						throw new Errors.ValidationError("'expenses' is empty", "EMPTY_EXPENSES", {});
					}
				},
				async function checkMedias(ctx) {
					if (ctx.params.medias && ctx.params.medias.length !== 0) {
						let ids = await checkIdsIsNotExist(
							ctx,
							ctx.params.medias.map((media) => media["file_id"]),
							"media",
							"files",
						);
						if (ids.length !== 0)
							throw new Errors.ValidationError(
								`There are no medias with these ids: ${ids.toString()}`,
								"NOT_FOUND_MEDIAS",
								{
									medias_ids: ids,
								},
							);
					} else {
						throw new Errors.ValidationError("'medias' is empty", "EMPTY_MEDIAS", {});
					}
				},
				async function checkTranslations(ctx) {
					if (ctx.params.usable_space === LISTING_USABLE_SPACE.WHOLE_HOUSE) {
						if (ctx.params.translations.length !== 0) {
							let ids = await checkIdsIsNotExist(
								ctx,
								ctx.params.translations.map((translation) => translation["language_id"]),
								"configuration",
								"languages",
							);
							if (ids.length !== 0)
								throw new Errors.ValidationError(
									`There are no languages with these ids: ${ids.toString()}`,
									"NOT_FOUND_LANGUAGES",
									{
										languages_ids: ids,
									},
								);
						} else {
							throw new Errors.ValidationError("'translations' is empty", "EMPTY_TRANSLATIONS", {});
						}
					}
				},
				async function validateTypology(ctx) {
					if (ctx.params.typology_id) {
						await ctx.call("private_accommodation.typologies.get", { id: ctx.params.typology_id });
					} else {
						throw new Errors.ValidationError(
							"'typology_id' is required!",
							"TYPOLOGY_ID_REQUIRED",
							{},
						);
					}
				},
				async function validateOwner(ctx) {
					if (ctx.meta.isBackoffice) {
						if (ctx.params.owner_id) {
							await ctx.call("private_accommodation.owners.get", { id: ctx.params.owner_id });
						} else {
							throw new Errors.ValidationError("'owner_id' is required!", "OWNER_ID_REQUIRED", {});
						}
					} else {
						let owner = await ctx.call("private_accommodation.owners.find", {
							query: { user_id: ctx.meta.user.id },
						});
						if (owner.length > 0) {
							ctx.params.owner_id = _.first(owner).id;
						} else {
							throw new Errors.ValidationError(
								"This user not registed by a owner",
								"USER_NOT_HAS_OWNER",
								{},
							);
						}
					}
				},
				function sanatizeParams(ctx) {
					ctx.params.status = _.has(ctx, "params.status")
						? ctx.params.status
						: LISTING_STATUS.PENDING;
					ctx.params.start_date = new Date(ctx.params.start_date);
					ctx.params.end_date = new Date(ctx.params.end_date);
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ctx.params.has_deleted = false;
					ctx.params.user_id = ctx.meta.user.id;
				},
				async function checkTranslationsRooms(ctx) {
					if (ctx.params.rooms) {
						for (const room of ctx.params.rooms) {
							if (!room.translations || room.translations.length === 0) {
								throw new Errors.ValidationError(
									"Rooms translations is required",
									"ROOMS_TRANSLATIONS_IS_REQUIRED",
									{},
								);
							} else {
								for (const translation of room.translations) {
									await ctx.call("configuration.languages.get", { id: translation.language_id });
								}
							}
						}
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
				async function checkTranslationsRooms(ctx) {
					if (ctx.params.rooms) {
						for (const room of ctx.params.rooms) {
							if (!room.translations || room.translations.length === 0) {
								throw new Errors.ValidationError(
									"Rooms translations is required",
									"ROOMS_TRANSLATIONS_IS_REQUIRED",
									{},
								);
							} else {
								for (const translation of room.translations) {
									await ctx.call("configuration.languages.get", { id: translation.language_id });
								}
							}
						}
					}
				},
			],
			list: [
				function sanatizeQuery(ctx) {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query.has_deleted = false;

					let params = ctx.params;

					if (params.query.min_price || params.query.max_price) {
						params.max_price = params.query.max_price || 100000;
						params.min_price = params.query.min_price || 0;
						delete params.query.max_price;
						delete params.query.min_price;

						params.extraQuery = (qb) => {
							qb.leftJoin("rooms", "rooms.listing_id", "listing.id");
							qb.andWhere((qb1) => {
								qb1
									.andWhere((qb11) => {
										qb11
											.where("listing.price", ">=", params.min_price)
											.andWhere("listing.price", "<=", params.max_price);
									})
									.orWhere((qb2) => {
										qb2
											.where("rooms.price", ">=", params.min_price)
											.andWhere("rooms.price", "<=", params.max_price);
									});
							});
						};
					}

				},
			],
		},
		after: {
			create: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx, "APPROVAL_NEEDED", res.user_id, res, true);
					return res;
				},
			],
			approve: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx, "APPROVED", _.first(res).user_id, res, ctx.params.notes);
				},
			],
			reject: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx, "REJECTED", _.first(res).user_id, res, ctx.params.notes);
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		wp_dashboard: {
			visibility: "public",
			cache: {
				keys: [
					"#user.id",
					"#language_id",
				],
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						status: "Approved",
						active: true,
						has_deleted: false,
						start_date: { lte: moment(new Date()).format("YYYY-MM-DD") },
						end_date: { gte: moment(new Date()).format("YYYY-MM-DD") },
					},
					withRelated: ["translations", "rooms", "medias"],
					sort: "-publish_date",
					limit: 3,
				});
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
			async handler(ctx) {
				let params = ctx.params;

				// Fetch user from token
				if (!_.has(params, "user_id")) {
					params.user_id = ctx.meta.user.id;
				}

				const listing_params = _.omit(params, ["amenities", "expenses"]);

				let rooms = [];
				let listing;
				if (ctx.params.usable_space === LISTING_USABLE_SPACE.SHARED_ROOM) {
					if (!ctx.params.rooms || ctx.params.rooms.length === 0) {
						throw new Errors.ValidationError(
							"For 'SHARED_ROOM' the rooms info is required",
							"ROOMS_INFO_IS_REQUIRED",
							{},
						);
					} else {
						listing_params.price = 0;
						listing = await this._create(ctx, listing_params);
						rooms = await this.saveRooms(ctx, listing[0].id, params.rooms);
					}
				} else {
					listing = await this._create(ctx, listing_params);
				}

				const amenities = await this.handleAmenities(ctx, listing[0].id, params.amenities);

				const expenses = await this.handleExpenses(ctx, listing[0].id, params.expenses);

				const medias = await this.handleMedias(ctx, listing[0].id, params.medias);

				const translations = await this.handleTranslations(ctx, listing[0].id, params.translations);

				return _.assign(
					{},
					listing[0],
					{ amenities: _.first(amenities) },
					{ expenses: _.first(expenses) },
					{ medias },
					{ translations },
					{ rooms },
				);
			},
		},

		update: {
			// REST: PUT /
			visibility: "published",
			async handler(ctx) {
				let params = ctx.params;

				// Fetch user from token
				if (!_.has(params, "user_id")) {
					params.user_id = ctx.meta.user.id;
				}

				const listing_params = _.omit(params, ["amenities", "expenses"]);

				let rooms = [];
				let listing;
				if (ctx.params.usable_space === LISTING_USABLE_SPACE.SHARED_ROOM) {
					if (!ctx.params.rooms || ctx.params.rooms.length === 0) {
						throw new Errors.ValidationError(
							"For 'PRIVATE_ROOM' or 'SHARED_ROOM' the rooms info is required",
							"ROOMS_INFO_IS_REQUIRED",
							{},
						);
					} else {
						listing_params.price = 0;
						listing = await this._update(ctx, listing_params);
						rooms = await this.saveRooms(ctx, listing[0].id, params.rooms);
					}
				} else {
					await ctx.call("private_accommodation.rooms.remove_listining_rooms", {
						listing_id: ctx.params.id,
					});
					listing = await this._update(ctx, listing_params);
				}

				const amenities = await this.handleAmenities(ctx, listing[0].id, params.amenities);

				const expenses = await this.handleExpenses(ctx, listing[0].id, params.expenses);

				const medias = await this.handleMedias(ctx, listing[0].id, params.medias);

				const translations = await this.handleTranslations(ctx, listing[0].id, params.translations);

				return _.assign(
					{},
					listing[0],
					{ amenities: _.first(amenities) },
					{ expenses: _.first(expenses) },
					{ medias },
					{ translations },
					{ rooms },
				);
			},
		},
		search: {
			visibility: "published",
			cache: {
				keys: [
					"withRelated",
					"fields",
					"page",
					"pageSize",
					"offset",
					"limit",
					"sort",
					"search",
					"searchFields",
					"query",
				],
			},
			rest: "GET /search",
			scope: "private_accommodation:listings:read",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			async handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				params.query = params.query || {};
				let ids = null;
				if (params.query.amenities) {
					const amenities = await ctx.call("private_accommodation.listing_amenities.find", {
						query: params.query.amenities,
						withRelated: false,
					});
					ids = amenities.map((am) => am.listing_id);
					delete params.query.amenities;
				}
				if (params.query.expenses) {
					const expenses = await ctx.call("private_accommodation.listing_expenses.find", {
						query: params.query.expenses,
						withRelated: false,
					});
					ids = ids
						? _.intersection(ids, await expenses.map((am) => am.listing_id))
						: expenses.map((am) => am.listing_id);
					delete params.query.expenses;
				}

				if (params.query.min_price || params.query.max_price) {
					params.max_price = params.query.max_price || 100000;
					params.min_price = params.query.min_price || 0;
					delete params.query.max_price;
					delete params.query.min_price;

					params.extraQuery = (qb) => {
						qb.leftJoin("rooms", "rooms.listing_id", "listing.id");
						qb.andWhere((qb1) => {
							qb1
								.andWhere((qb11) => {
									qb11
										.where("listing.price", ">=", params.min_price)
										.andWhere("listing.price", "<=", params.max_price);
								})
								.orWhere((qb2) => {
									qb2
										.where("rooms.price", ">=", params.min_price)
										.andWhere("rooms.price", "<=", params.max_price);
								});
						});
					};
				}

				if (ids) params.searchIds = ids;

				params.query.has_deleted = false;
				params.query.active = true;
				params.query.status = "Approved";

				return this._list(ctx, params);
			},
		},
		list: {
			visibility: "published",
			// rest: "GET /",
		},
		get: {
			visibility: "published",
			// rest: "GET /:id",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
			async handler(ctx) {
				let params = ctx.params;

				// Fetch user from token
				if (!_.has(params, "user_id")) {
					params.user_id = ctx.meta.user.id;
				}

				const listing_params = _.omit(params, ["amenities", "expenses"]);

				let rooms = [];
				let listing;
				if (ctx.params.usable_space === LISTING_USABLE_SPACE.SHARED_ROOM) {
					if (!ctx.params.rooms) {
						throw new Errors.ValidationError(
							"For 'PRIVATE_ROOM' or 'SHARED_ROOM' the rooms info is required",
							"ROOMS_INFO_IS_REQUIRED",
							{},
						);
					} else {
						listing_params.price = 0;
						listing = await this._update(ctx, listing_params);
						rooms = await this.saveRooms(ctx, listing[0].id, params.rooms);
					}
				} else {
					await ctx.call("private_accommodation.rooms.remove_listining_rooms", {
						listing_id: ctx.params.id,
					});
					listing = await this._update(ctx, listing_params);
				}

				const amenities = await this.handleAmenities(ctx, listing[0].id, params.amenities);

				const expenses = await this.handleExpenses(ctx, listing[0].id, params.expenses);

				const medias = await this.handleMedias(ctx, listing[0].id, params.medias);

				const translations = await this.handleTranslations(ctx, listing[0].id, params.translations);

				return _.assign(
					{},
					listing[0],
					{ amenities: _.first(amenities) },
					{ expenses: _.first(expenses) },
					{ medias },
					{ translations },
					{ rooms },
				);
			},
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
			async handler(ctx) {
				return await this._update(ctx, { id: ctx.params.id, has_deleted: true }, true);
			},
		},

		increment: {
			visibility: "published",
			cache: false,
			rest: "POST /:id/increment",
			scope: "private_accommodation:listings:read",
			async handler(ctx) {
				const listing = await this._get(ctx, { id: ctx.params.id });
				return this._update(
					ctx,
					{ id: listing[0].id, visualizations: listing[0].visualizations + 1 },
					true,
				);
			},
		},
		approve: {
			visibility: "published",
			cache: false,
			rest: "POST /:id/approve",
			scope: "private_accommodation:listings:status",
			async handler(ctx) {
				const status = LISTING_STATUS.APPROVED;
				const listing = await this._get(ctx, { id: ctx.params.id });
				return this.handleStatusChange(ctx, _.first(listing), status);
			},
		},
		reject: {
			visibility: "published",
			cache: false,
			rest: "POST /:id/reject",
			scope: "private_accommodation:listings:status",
			async handler(ctx) {
				const status = LISTING_STATUS.REJECTED;
				const listing = await this._get(ctx, { id: ctx.params.id });
				return this.handleStatusChange(ctx, _.first(listing), status);
			},
		},
		changeActiveInListings: {
			cache: false,
			params: {
				owner_id: { type: "number", integer: true, min: 1, convert: true, optional: false },
				active: { type: "boolean" },
			},
			async handler(ctx) {
				this.changeActiveInListings(ctx);
			},
		},
		getActiveLocations: {
			rest: "GET /locations",
			cache: true,
			visibility: "published",
			handler() {
				return this.adapter
					.getDB("listing")
					.distinct("city")
					.where("status", "Approved")
					.orderBy("city", "asc")
					.then((result) => {
						return result.map((l) => l.city);
					});
			},
		},

		getMaxMinPrice: {
			rest: "GET /price-min-max",
			visibility: "published",
			async handler() {

				const dbMinMaxPrices = await this.adapter.db.raw(
					`select min(x."price") as min_price, max(x."price") as max_price from "listing" l join (
						SELECT id, price FROM "listing" l2
						  UNION ALL
						  SELECT listing_id id, price FROM "rooms" r
					) x ON l.id=x.id;`
				);

				let minMaxPrices = {
					min_price: 0,
					max_price:0
				};

				if(dbMinMaxPrices.rows && dbMinMaxPrices.rows.length) {
					Object.assign(minMaxPrices,
						{
							min_price: dbMinMaxPrices.rows[0].min_price,
							max_price: dbMinMaxPrices.rows[0].max_price
						});
				}

				return minMaxPrices;
			},
		},

		getlistingByOwner: {
			rest: "GET /owner",
			params: {
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				fields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				page: { type: "number", integer: true, min: 1, optional: true, convert: true },
				pageSize: { type: "number", integer: true, min: 0, optional: true, convert: true },
				limit: { type: "number", integer: true, min: -1, optional: true, convert: true },
				offset: { type: "number", integer: true, min: 0, optional: true, convert: true },
				sort: { type: "string", optional: true },
				search: { type: "string", optional: true },
				searchFields: [
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
				query: [
					{ type: "object", optional: true },
					{ type: "string", optional: true },
				],
			},
			visibility: "published",
			scope: "private_accommodation:listings:owner",
			async handler(ctx) {
				const onwer = await ctx.call("private_accommodation.owners.find", {
					query: { user_id: ctx.meta.user.id },
				});
				if (onwer.length <= 0) {
					throw new Errors.ValidationError(
						"This user not registed by a owner",
						"USER_NOT_HAS_OWNER",
						{},
					);
				} else {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["owner_id"] = onwer[0].id;
					ctx.params.query["has_deleted"] = false;
					const listings = await ctx.call("private_accommodation.listings.list", ctx.params);
					const currentDate = new Date();
					for (let list of listings.rows) {
						if (list.status === "Approved") {
							if (
								moment(currentDate, "day").isSameOrAfter(list.start_date, "day") &&
								moment(currentDate, "day").isSameOrBefore(list.end_date, "day")
							) {
								list.status = "Published";
							}
							if (moment(currentDate).isAfter(list.end_date)) {
								list.status = "Expired";
							}
						}
					}
					return listings;
				}
			},
		},

		getCheckProperties: {
			rest: "GET /check_properties",
			params: {},
			visibility: "published",
			scope: "private_accommodation:listings:read",
			async handler(ctx) {
				const onwer = await ctx.call("private_accommodation.owners.count", {
					query: { user_id: ctx.meta.user.id },
				});

				if (onwer <= 0) {
					return false;
				} else {
					ctx.params.query = ctx.params.query || {};
					ctx.params.query["user_id"] = ctx.meta.user.id;
					ctx.params.query["has_deleted"] = false;

					const listings = await ctx.call("private_accommodation.listings.count", ctx.params);

					return listings === 0 ? false : true;
				}
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Handles status change.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Object} status
		 * @returns {Object}
		 */
		async handleStatusChange(ctx, listing, status) {
			if (listing.status !== LISTING_STATUS.PENDING) {
				throw new Errors.ValidationError("Listing not in a Pending status", "LISTING_NOT_PENDING", {
					listing_id: ctx.params.id,
					status: listing.status,
				});
			}
			let result = null;
			if (status == LISTING_STATUS.APPROVED) {
				result = await this._update(
					ctx,
					{ id: listing.id, status, publish_date: new Date() },
					true,
				);
			} else {
				result = await this._update(ctx, { id: listing.id, status }, true);
			}

			// Save status change history
			if (result && !_.isEmpty(result)) {
				const history_params = _.assign(
					{},
					{ user_id: ctx.meta.user.id },
					{ status: result[0].status },
					{ listing_id: result[0].id },
					{ start_date: result[0].start_date },
					{ end_date: result[0].end_date },
					{ notes: _.has(ctx, "params.notes") ? ctx.params.notes : "" },
				);
				await ctx.call("private_accommodation.listing_history.create", history_params);
			}

			return result;
		},

		/**
		 * Handles 1-n relation with Listing Amenities.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Object} params
		 * @returns {Object}
		 */
		async handleAmenities(ctx, listing_id, params) {
			if (!listing_id) {
				return null;
			}

			// Find if entity already exists
			let amenities = await ctx.call("private_accommodation.listing_amenities.find", {
				query: { listing_id },
			});

			if (_.isEmpty(amenities)) {
				// Create if doesnt exist
				amenities = await ctx.call(
					"private_accommodation.listing_amenities.create",
					_.assign({}, { listing_id }, params),
				);
			} else {
				// Update otherwise
				amenities = await ctx.call(
					"private_accommodation.listing_amenities.patch",
					_.assign({}, { id: amenities[0].id }, params),
				);
			}

			return amenities;
		},

		/**
		 * Handles 1-n relation with Listing Expenses.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Object} params
		 * @returns {Object}
		 */
		async handleExpenses(ctx, listing_id, params) {
			if (!listing_id) {
				return null;
			}

			// Find if entity already exists
			let expenses = await ctx.call("private_accommodation.listing_expenses.find", {
				query: { listing_id },
			});

			if (_.isEmpty(expenses)) {
				// Create if doesnt exist
				expenses = await ctx.call(
					"private_accommodation.listing_expenses.create",
					_.assign({}, { listing_id }, params),
				);
			} else {
				// Update otherwise
				expenses = await ctx.call(
					"private_accommodation.listing_expenses.patch",
					_.assign({}, { id: expenses[0].id }, params),
				);
			}

			return expenses;
		},

		/**
		 * Handles 1-n relation with Listing Medias.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Array} medias
		 * @returns {Array}
		 */
		async handleMedias(ctx, listing_id, medias) {
			if (!listing_id || _.isEmpty(medias)) {
				return null;
			}

			const result = [];

			await medias.reduce(async (promise, media) => {
				await promise;
				const m = await this.handleSingleMedia(ctx, listing_id, media);
				result.push(m);
			}, Promise.resolve());

			return result;
		},

		/**
		 * Handles a single Listing Media.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Object} params
		 * @returns {Object}
		 */
		async handleSingleMedia(ctx, listing_id, params) {
			if (!listing_id || !params.file_id) {
				return null;
			}

			// Find if entity already exists
			let media = await ctx.call("private_accommodation.listing_medias.find", {
				query: { listing_id, file_id: params.file_id },
			});

			if (_.isEmpty(media)) {
				// Create if doesnt exist
				media = await ctx.call(
					"private_accommodation.listing_medias.create",
					_.assign({}, { listing_id }, params),
				);
			} else {
				// Update otherwise
				media = await ctx.call(
					"private_accommodation.listing_medias.patch",
					_.assign({}, { id: media[0].id }, params),
				);
			}

			return _.first(media);
		},

		/**
		 * Handles 1-n relation with Listing Translations.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Array} translations
		 * @returns {Array}
		 */
		async handleTranslations(ctx, listing_id, translations) {
			if (!listing_id || _.isEmpty(translations)) {
				return null;
			}

			const result = [];

			await translations.reduce(async (promise, translation) => {
				await promise;
				const t = await this.handleSingleTranslation(ctx, listing_id, translation);
				result.push(t);
			}, Promise.resolve());

			return result;
		},

		/**
		 * Handles a single Listing Translation.
		 *
		 * @methods
		 *
		 * @param {Context} ctx
		 * @param {Number} listing_id
		 * @param {Object} params
		 * @returns {Object}
		 */
		async handleSingleTranslation(ctx, listing_id, params) {
			if (!listing_id || !params.language_id) {
				return null;
			}

			// Find if entity already exists
			let translation = await ctx.call("private_accommodation.listing_translations.find", {
				query: { listing_id, language_id: params.language_id },
			});

			if (_.isEmpty(translation)) {
				// Create if doesnt exist
				translation = await ctx.call(
					"private_accommodation.listing_translations.create",
					_.assign({}, { listing_id }, params),
				);
			} else {
				// Update otherwise
				translation = await ctx.call(
					"private_accommodation.listing_translations.patch",
					_.assign({}, { id: translation[0].id }, params),
				);
			}

			return _.first(translation);
		},
		async changeActiveInListings(ctx) {
			this.adapter.updateMany(
				{
					owner_id: ctx.params.owner_id,
				},
				{ active: ctx.params.active },
			);
		},
		async saveRooms(ctx, listing_id, rooms) {
			return await ctx.call("private_accommodation.rooms.save_listings_rooms", {
				listing_id: listing_id,
				rooms: rooms,
			});
		},
		async deleteRelatedAmenities(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listing_amenities.find", {
					query: {
						listing_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (listingAmenitiesOderRel) =>
							await ctx.call("private_accommodation.listing_amenities.remove", {
								id: listingAmenitiesOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Listing Amenities relation!", err));
		},
		async deleteRelatedExpenses(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listing_expenses.find", {
					query: {
						listing_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (listingExpensesOderRel) =>
							await ctx.call("private_accommodation.listing_expenses.remove", {
								id: listingExpensesOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Listing Expenses relation!", err));
		},
		async deleteRelatedHistory(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listing_history.find", {
					query: {
						listing_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (listingHistoryOderRel) =>
							await ctx.call("private_accommodation.listing_history.remove", {
								id: listingHistoryOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Listing History relation!", err));
		},
		async deleteRelatedTranslations(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listing_translations.find", {
					query: {
						listing_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (listingTranslationsOderRel) =>
							await ctx.call("private_accommodation.listing_translations.remove", {
								id: listingTranslationsOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Listing Translations relation!", err));
		},
		async deleteRelatedMedias(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listing_medias.find", {
					query: {
						listing_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(async (listingMediasOderRel) => {
						await ctx.call("media.files.remove", { id: listingMediasOderRel.file_id });
						await ctx.call("private_accommodation.listing_medias.remove", {
							id: listingMediasOderRel.id,
						});
					});
				})
				.catch((err) => this.logger.error("Unable to delete Listing Medias relation!", err));
		},
		async getUser(ctx, id) {
			return await ctx.call("authorization.users.get", { id });
		},
		async sendNotification(ctx, status, user_id, listing, notes = "", sendFromAdmin = false) {
			let user_data = {};
			let user = await this.getUser(ctx, user_id);

			if (sendFromAdmin) {
				let adminEmail = await this.getAdminEmail(ctx);
				user_data = {
					email: adminEmail,
				};
			}

			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "PRIVATE_ACCOMMODATION_LISTING_" + status.toUpperCase(),
				user_id: user_id,
				user_data: user_data,
				data: { listing: listing, user: _.first(user), notes: notes },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
		async getAdminEmail(ctx) {
			let adminNotificationEmail = await ctx.call(
				"private_accomodation.configurations.getAdminNotificationEmail",
				{},
			);
			if (adminNotificationEmail.length === 0 || _.first(adminNotificationEmail).value === null)
				throw new Errors.ValidationError(
					"Missing configuration: admin_notification_email. Please set configuration to enable the creation of a new User.",
					"MISSING_ADMIN_NOTIFICATION_EMAIL",
					{},
				);
			else return _.first(adminNotificationEmail).value;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
