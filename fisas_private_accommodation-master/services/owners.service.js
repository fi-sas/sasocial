"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;
const { OWNER_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { hasMany, hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const { OWNER_APPLICATION_STATUS } = require("../utils/constants");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.owners",
	table: "owner",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "owners")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"description",
			"active",
			"applicant_consent",
			"status",
			"user_id",
			"issues_receipt",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			listings(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "private_accommodation.listings", "listings", "id", "owner_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			description: { type: "string" },
			active: { type: "boolean" },
			applicant_consent: { type: "boolean" },
			status: { type: "enum", values: _.values(OWNER_STATUS) },
			user_id: { type: "number", integer: true, min: 1, optional: false },
			issues_receipt: { type: "boolean" },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			ownerApplication: [
				async function sanatizeParams(ctx) {
					let ownerProfileID, userGroupIDS;
					ctx.params.status = _.has(ctx, "params.status")
						? ctx.params.status
						: OWNER_APPLICATION_STATUS.PENDING;
					ctx.params.external = true;
					ctx.params.active = false;
					ctx.params.can_access_BO = false;
					ctx.params.can_change_password = true;
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
					ownerProfileID = await this.getOwnerProfile(ctx);
					userGroupIDS = await this.getOwnerUserGroups(ctx);

					if (ownerProfileID === null)
						throw new Errors.ValidationError(
							"Missing configuration: owner_profile_id. Please set configuration to enable the creation of a new User.",
							"MISSING_OWNER_PROFILE_ID",
							{},
						);
					else ctx.params.profile_id = ownerProfileID;
					// if (userGroupIDS.length === 0 || _.first(userGroupIDS).value === null)
					// 	throw new Errors.ValidationError(
					// 		"Missing configuration: owner_user_group_ids. Please set configuration to enable the creation of a new User.",
					// 		"MISSING_OWNER_USER_GROUP_IDS",
					// 		{},
					// 	);
					// else ctx.params.user_group_ids = _.first(userGroupIDS).value;
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["deleteRelatedHistory", "deleteRelatedListings"],
		},
		after: {
			ownerApplication: [
				async function notification(ctx, res) {
					this.sendNotification(ctx, "Pending", _.first(res).user_id, _.first(res), "");
					return res;
				},
				async function createHistory(ctx, res) {
					this.handleHistory(ctx, _.first(res));
					return res;
				},
				async function retrunResponse(ctx, res) {
					return res;
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		list: {
			visibility: "published",
			// rest: "GET /",
		},

		get: {
			visibility: "published",
			// rest: "GET /:id",
			async handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				const owner = await this._get(ctx, params);
				const user = await ctx.call("authorization.users.find", {
					query: { id: owner[0].user_id },
				});
				owner[0].user = _.first(user);
				return owner;
			},
		},

		remove: {
			visibility: "published",
			// rest: "DELETE /:id",
		},

		changeActive: {
			visibility: "published",
			rest: "POST /:id/change-active",
			scope: "private_accommodation:owners:update",
			async handler(ctx) {
				let params = ctx.params;
				if (!_.has(params, "active")) {
					throw new Errors.ValidationError(
						"Active status (boolean) must be provided",
						"CHANGE_ACTIVE_MISSING_ACTIVE_FIELD",
						{
							id: ctx.params.id,
							active: null,
						},
					);
				}
				return this.handleActiveChange(ctx, params);
			},
		},

		ownerApplication: {
			visibility: "published",
			rest: "POST /application",
			scope: "private_accommodation:owners:create",
			params: {
				name: { type: "string", max: 250, optional: false },
				identification: { type: "string", max: 255, optional: true },
				tin: { type: "string" },
				email: { type: "email", optional: false },
				birth_date: { type: "date", convert: true, optional: true },
				gender: { type: "enum", values: ["M", "F", "U"], optional: true },
				address: { type: "string", max: 255, optional: false },
				postal_code: { type: "string", max: 10, optional: false },
				city: { type: "string", max: 250, optional: false },
				country: { type: "string", max: 250, optional: true },
				phone: { type: "string", max: 250, optional: false },
				description: { type: "string", max: 500, optional: false },
				applicant_consent: { type: "boolean" },
				issues_receipt: { type: "boolean" },
				document_type_id: {
					type: "number",
					positive: true,
					integer: true,
					convert: true,
					optional: true
				},
				nationality: { type: "string", optional: true },
			},
			async handler(ctx) {
				let user = _.omit(ctx.params, [
					"description",
					"applicant_consent",
					"issues_receipt",
					"status",
					"created_at",
					"updated_at",
				]);

				let owner = _.omit(ctx.params, [
					"name",
					"identification",
					"tin",
					"email",
					"birth_date",
					"gender",
					"address",
					"postal_code",
					"city",
					"country",
					"phone",
					"external",
					"can_access_BO",
					"can_change_password",
					"profile_id",
					"user_group_ids",
					"document_type_id",
					"nationality"
				]);

				if (ctx.meta.isBackoffice) {
					user.user_name = "owner_".concat(new Date().getTime());
					user.birth_date = new Date();
				} else if (ctx.params.birth_date === undefined || ctx.params.birth_date === null) {
					user.birth_date = new Date(0);
				}

				if (ctx.params.gender === undefined || ctx.params.gender === null) {
					user.gender = "U";
				}

				user.user_name = ctx.params.email;
				user.sourced_by = "INTERNAL";
				user.source_name = "private_accommodation.owners";

				return this.createUser(ctx, user).then(user => {
					owner.user_id = _.first(user).id;
					return this._create(ctx, owner);
				});
			},
		},

		ownerApplicationApprove: {
			visibility: "published",
			rest: "POST /application/:id/approve",
			scope: "private_accommodation:owners:approve",
			params: {
				id: { type: "number", integer: true, min: 1, convert: true, optional: false },
				notes: { type: "string", max: 500, optional: true },
			},
			async handler(ctx) {
				return this.handleStatusChange(ctx, "Approved");
			},
		},

		ownerApplicationRejected: {
			visibility: "published",
			rest: "POST /application/:id/reject",
			scope: "private_accommodation:owners:reject",
			params: {
				id: { type: "number", integer: true, min: 1, convert: true, optional: false },
				notes: { type: "string", max: 500, optional: true },
			},
			async handler(ctx) {
				return this.handleStatusChange(ctx, "Rejected");
			},
		},

		listing: {
			visibility: "published",
			rest: "GET /:id/listings",
			scope: "private_accommodation:owners:read",
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
			},
			async handler(ctx) {
				return await ctx.call("private_accommodation.listings.list", {
					query: { owner_id: ctx.params.id },
					withRelated: ctx.params.withRelated,
				});
			},
		},
	},
	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Handles active change.
		 *
		 * @methods
		 *
		 * @param {object} ctx
		 * @param {object} params
		 * @returns {object}
		 */
		async handleActiveChange(ctx, params) {
			const owner = await this._get(ctx, { id: params.id });
			await this.updateUser(ctx, { user_id: _.first(owner).user_id, active: ctx.params.active });
			await this.updateListing(ctx);

			if (owner[0].status != OWNER_STATUS.APPROVED) {
				throw new Errors.ValidationError(
					"Owner must be approved status before changing active status",
					"OWNER_NOT_APPROVED",
					{
						owner_id: params.id,
						status: owner[0].status,
					},
				);
			}

			const result = await this._update(
				ctx,
				{ id: _.first(owner).id, active: params.active },
				true,
			);

			// Save status change history
			await this.handleHistory(ctx, _.first(result));

			return result;
		},
		/**
		 * Creates the history of changes in the state of the owner
		 * @param {object} ctx
		 * @param {object} owner
		 */
		async handleHistory(ctx, owner) {
			let history;
			// Save status change history
			if (owner && !_.isEmpty(owner)) {
				const history_params = _.assign(
					{},
					{ user_id: ctx.meta.user.id },
					{ status: owner.status },
					{ owner_id: owner.id },
					{ active: owner.active },
					{ notes: _.has(ctx, "params.notes") ? ctx.params.notes : "" },
				);
				history = await ctx.call("private_accommodation.owners_history.create", history_params);
				return history;
			}
		},
		/**
		 * Change owner status
		 * @param {object} ctx
		 * @param {string} status
		 */
		async handleStatusChange(ctx, status) {
			const owner = await this._get(ctx, { id: ctx.params.id });

			if (status === "Approved")
				await this.updateUser(ctx, { user_id: _.first(owner).user_id, active: true });

			if (owner[0].status != OWNER_APPLICATION_STATUS.PENDING) {
				throw new Errors.ValidationError("Owner not in a Pending status", "OWNER_NOT_PENDING", {
					owner: ctx.params.id,
					status: _.first(owner).status,
				});
			}
			const result = await this._update(
				ctx,
				{
					id: _.first(owner).id,
					status,
					active: status === "Approved" ? true : _.first(owner).active,
				},
				true,
			);

			// Save status change history
			await this.handleHistory(ctx, _.first(result));
			await this.sendNotification(
				ctx,
				status,
				_.first(result).user_id,
				_.first(result),
				ctx.params.notes,
			);

			return result;
		},
		/**
		 * Get owner profile id define in configurations
		 * @param {object} ctx
		 */
		async getOwnerProfile(ctx) {
			return await ctx.call("private_accommodation.configurations.getOwnerProfileId", {});
		},
		/**
		 * Get owner user groups ids define in configurations
		 * @param {object} ctx
		 */
		async getOwnerUserGroups(ctx) {
			return await ctx.call("private_accommodation.configurations.getOwnerUserGroupIds", {});
		},
		/**
		 * Create user for the owner in MS authorization users
		 * @param {object} ctx
		 * @param {object} user
		 */
		async createUser(ctx, user) {
			return await ctx.call("authorization.users.create", user);
		},
		/**
		 * Update owner user data on MS authorization users
		 * @param {object} ctx
		 * @param {object} user
		 */
		async updateUser(ctx, user) {
			return await ctx.call("authorization.users.change_status", user);
		},
		/**
		 * Get user data of owner
		 * @param {object} ctx
		 * @param {integer} user_id
		 */
		async getUser(ctx, id) {
			return await ctx.call("authorization.users.get", { id });
		},
		/**
		 * Disable or enable listing of an owner
		 * @param {object} ctx
		 */
		async updateListing(ctx) {
			return await ctx.call("private_accommodation.listings.changeActiveInListings", {
				owner_id: ctx.params.id,
				active: ctx.params.active,
			});
		},
		/**
		 * Remove owner history
		 * @param {object} ctx
		 * @param {object} res
		 */
		async deleteRelatedHistory(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.owners_history.find", {
					query: {
						owner_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (ownerHistoryOderRel) =>
							await ctx.call("private_accommodation.owners_history.remove", {
								id: ownerHistoryOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Owner History OderRel relation!", err));
		},
		/**
		 * Remove owner listings
		 * @param {object} ctx
		 * @param {object} res
		 */
		async deleteRelatedListings(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.listings.find", {
					query: {
						owner_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (listingOderRel) =>
							await ctx.call("private_accommodation.listings.remove", { id: listingOderRel.id }),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Listing OderRel relation!", err));
		},
		/**
		 * Send a notification for owner
		 * @param {object} ctx
		 * @param {string} status
		 * @param {integer} user_id
		 * @param {string} name
		 * @param {object} application
		 * @param {string} notes
		 */
		async sendNotification(ctx, status, user_id, application, notes = "") {
			let user = await this.getUser(ctx, user_id);
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "PRIVATE_ACCOMMODATION_OWNER_APPLICATION_" + status.toUpperCase(),
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { application: application, user: _.first(user), notes: notes },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
		/**
		 *
		 * @param {object} ctx
		 */
		async getAdminEmail(ctx) {
			let adminNotificationEmail = await ctx.call(
				"private_accomodation.configurations.getAdminNotificationEmail",
				{},
			);
			if (adminNotificationEmail.length === 0 || _.first(adminNotificationEmail).value === null)
				throw new Errors.ValidationError(
					"Missing configuration: admin_notification_email. Please set configuration to enable the creation of a new User.",
					"MISSING_ADMIN_NOTIFICATION_EMAIL",
					{},
				);
			else return _.first(adminNotificationEmail).value;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() { },

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() { },
};
