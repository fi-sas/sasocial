"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { OWNER_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.owners_history",
	table: "owner_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "owners_history")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"owner_id",
			"status",
			"user_id",
			"active",
			"notes",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: ["user"],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"authorization.users",
					"user",
					"user_id"
				);
			},
			owner(ids, docs, rule, ctx) {
				return hasOne(
					docs,
					ctx,
					"private_accommodation.owners",
					"owner",
					"owner_id"
				);
			},
		},
		entityValidator: {
			owner_id: { type: "number", integer: true, min: 1, optional: false },
			status: { type: "enum", values: _.values(OWNER_STATUS) },
			user_id: { type: "number", integer: true, min: 1, optional: false },
			active: { type: "boolean" },
			notes: { type: "string", max: 255, optional: true },
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
