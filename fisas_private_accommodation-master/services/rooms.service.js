"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.rooms",
	table: "rooms",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "rooms")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"listing_id",
			"price",
			"room_type",
			"shared_wc",
			"area",
			"active",
		],
		defaultWithRelateds: ["translations"],
		withRelateds: {
			translations(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "private_accommodation.rooms_translations", "translations", "id", "room_id");
			}
		},
		entityValidator: {
			listing_id: { type: "number", positive: true, integer: true },
			price: { type: "number", positive: true },
			room_type: { type: "enum", values: ["PRIVATE_ROOM", "SHARED_ROOM"] },
			shared_wc: { type: "boolean" },
			area: { type: "number", convert: true },
			active: { type: "boolean", default: true },
		},
	},
	hooks: {},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_listings_rooms: {
			params: {
				listing_id: { type: "number", positive: true, integer: true },
				rooms: {
					type: "array",
					item: {
						type: "object", props: {
							listing_id: { type: "number", positive: true, integer: true },
							price: { type: "number", positive: true },
							room_type: { type: "enum", values: ["PRIVATE_ROOM", "SHARED_ROOM"] },
							shared_wc: { type: "boolean" },
							area: { type: "number", optional: true },
							active: { type: "boolean", default: true },
							translations: {
								type: "array",
								items: {
									type:"object", props: {
										language_id: { type: "number", convert: true },
										description: { type: "string" },
									}
								}
							}
						}
					}
				}
			},
			async handler(ctx) {
				let resp = [];
				const list_rooms =  await this._find(ctx, {
					query: {
						listing_id : ctx.params.listing_id
					}
				});
				for(const room of list_rooms){
					await ctx.call("private_accommodation.rooms_translations.remove_rooms_translations", {
						room_id: room.id
					});
				}
				await this.adapter.removeMany({
					listing_id: ctx.params.listing_id
				});
				for (const room of ctx.params.rooms) {
					room.listing_id = ctx.params.listing_id;
					const room_created = await this._create(ctx, room);
					const translation = await ctx.call("private_accommodation.rooms_translations.save_rooms_translations", {
						room_id: room_created[0].id,
						translations: room.translations
					});
					room_created[0].translations = translation;
					resp.push(room_created[0]);
				}
				return resp;
			}
		},

		remove_listining_rooms: {
			param: {
				listing_id: { type: "number", positive: true, integer: true },
			},
			async handler(ctx) {
				const list_rooms =  await this._find(ctx, {
					query: {
						listing_id : ctx.params.listing_id
					}
				});
				for(const room of list_rooms){
					await ctx.call("private_accommodation.rooms_translations.remove_rooms_translations", {
						room_id: room.id
					});
				}
				await this.adapter.removeMany({
					listing_id: ctx.params.listing_id
				});
				this.clearCache();
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
