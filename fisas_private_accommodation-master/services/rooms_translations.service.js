"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.rooms_translations",
	table: "room_translations",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "rooms")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"room_id",
			"language_id",
			"description",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			room_id: { type: "number", positive: true, integer: true, convert: true },
			language_id: { type: "number", positive: true, integer: true, convert: true },
			description: { type: "string" },
			updated_at: { type: "date", convert: true, optional: true },
			created_at: { type: "date", convert: true, optional: true },
		},
	},
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		/**
         * Action for save rooms translations
         */
		save_rooms_translations:{
			param:{
				room_id : { type: "number", conver: true },
				translations: {
					type: "array",
					item:{
						type: "object", props: {
							language_id : { type: "number", convert: true },
							description: { type: "string" }
						}
					}
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					room_id: ctx.params.room_id
				});
				let resp = [];
				for(const translation of ctx.params.translations) {
					const entity = {
						language_id: translation.language_id,
						room_id: ctx.params.room_id,
						description: translation.description,
						created_at: new Date(),
						updated_at: new Date()
					};
					const room_translation =  await this._create(ctx, entity);
					resp.push(room_translation[0]);
				}
				return resp;
			}
		},

		/**
         * Action for remove all rooms translations
         */
		remove_rooms_translations: {
			param: {
				room_id: { type: "number", convert: true },
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					room_id: ctx.params.room_id
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
