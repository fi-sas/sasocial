"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.typologies",
	table: "typology",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "typologies")],

	/**
   * Settings
   */
	settings: {
		fields: ["id", "name", "created_at", "updated_at"],
		defaultWithRelateds: [],
		withRelateds: {
			listings(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.listings",
					"listings",
					"id",
					"typology_id"
				);
			},
			wanted_ads(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.wanted_ads",
					"wanted_ads",
					"id",
					"typology_id"
				);
			},
		},
		entityValidator: {
			name: "string|max:255",
			created_at: { type: "date", optional: true },
			updated_at: { type: "date", optional: true },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			getListingsByTipologyId: [
				async function checkTypology(ctx) {
					await ctx.call("private_accommodation.typologies.get", {
						id: ctx.params.id,
					});
				},
			],
			getWantedAdsByTipologyId: [
				async function checkTypology(ctx) {
					await ctx.call("private_accommodation.typologies.get", {
						id: ctx.params.id,
					});
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			scope: "private_accommodation:typologies:list",
		},
		clearServiceCache: {
			// REST: GET /
			visibility: "public",
			async handler() {
				return this.clearCache();
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getListingsByTipologyId: {
			visibility: "published",
			scope: "private_accommodation:typologies:get-listings",
			rest: {
				method: "GET",
				path: "/:id/listings",
			},
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
			},
			async handler(ctx) {
				return ctx.call("private_accommodation.listings.find", {
					query: {
						typology_id: ctx.params.id,
					},
					withRelated: ctx.params.withRelated,
				});
			},
		},
		getWantedAdsByTipologyId: {
			visibility: "published",
			scope: "private_accommodation:typologies:get-wanted_ads",
			rest: {
				method: "GET",
				path: "/:id/wanted_ads",
			},
			params: {
				id: {
					type: "number",
					integer: true,
					positive: true,
					convert: true,
				},
				withRelated: [
					{ type: "boolean", optional: true },
					{ type: "string", optional: true },
					{ type: "array", optional: true, items: "string" },
				],
			},
			async handler(ctx) {
				return ctx.call("private_accommodation.wanted_ad.find", {
					query: {
						typology_id: ctx.params.id,
					},
					withRelated: ctx.params.withRelated,
				});
			},
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
