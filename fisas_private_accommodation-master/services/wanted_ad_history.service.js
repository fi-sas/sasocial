"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne } = require("@fisas/ms_core").Helpers.WithRelateds;
const {
	WANTED_ADS_STATUS
} = require("../utils/constants");
const _ = require("lodash");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.wanted_ad_history",
	table: "wanted_ad_history",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "wanted_ad_history")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"wanted_ad_id",
			"user_id",
			"status",
			"notes",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [
			"user"
		],
		withRelateds: {
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			wanted_ad_id: { type: "number", integer: true, min: 1, optional: false },
			notes: { type: "string", max: 255, optional: true },
			user_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			status: {
				type: "enum",
				values: _.values(WANTED_ADS_STATUS),
				optional: true,
			},
			created_at: { type: "date" },
			updated_at: { type: "date" },
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
			scope: "private_accommodation:wanted_ad_history:list",
		},
		clearServiceCache: {
			// REST: GET /
			visibility: "public",
			async handler() {
				return this.clearCache();
			},
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() {},

	/**
   * Service started lifecycle event handler
   */
	async started() {},

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() {},
};
