"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasOne, hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;
const { MoleculerError } = require("moleculer").Errors;
const { WANTED_ADS_STATUS } = require("../utils/constants");
const _ = require("lodash");
const { Errors } = require("@fisas/ms_core").Helpers;
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "private_accommodation.wanted_ads",
	table: "wanted_ad",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("private_accommodation", "wanted_ads")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"author_name",
			"author_nationality",
			"author_phone",
			"author_email",
			"typology_id",
			"max_price",
			"furnished",
			"zone",
			"description",
			"author_consent",
			"start_date",
			"end_date",
			"user_id",
			"status",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {
			history(ids, docs, rule, ctx) {
				return hasMany(
					docs,
					ctx,
					"private_accommodation.wanted_ad_history",
					"history",
					"id",
					"wanted_ad_id",
				);
			},
			typology(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "private_accommodation.typologies", "typology", "typology_id");
			},
			user(ids, docs, rule, ctx) {
				return hasOne(docs, ctx, "authorization.users", "user", "user_id");
			},
		},
		entityValidator: {
			author_name: { type: "string", max: 255, optional: true },
			author_nationality: { type: "string", max: 255 },
			author_phone: { type: "string", max: 25 },
			author_email: { type: "email" },
			typology_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
			},
			max_price: { type: "number", convert: true },
			furnished: { type: "enum", values: ["Y", "N", "I"], optional: true },
			zone: { type: "string", max: 250 },
			description: { type: "string", max: 255, optional: true },
			author_consent: { type: "boolean" },
			start_date: { type: "date" },
			end_date: { type: "date", optional: true },
			user_id: {
				type: "number",
				positive: true,
				integer: true,
				convert: true,
				optional: true,
			},
			status: {
				type: "enum",
				values: _.values(WANTED_ADS_STATUS),
				optional: true,
			},
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.status = WANTED_ADS_STATUS.PENDING;
					ctx.params.user_id = ctx.meta.user.id;
					ctx.params.start_date = new Date(ctx.params.start_date);
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
				async function validateTypology(ctx) {
					if (ctx.params.typology_id) {
						await ctx.call("private_accommodation.typologies.get", { id: ctx.params.typology_id });
					} else {
						throw new MoleculerError("'typology_id' is required!", 400);
					}
				},
				async function validateUser(ctx) {
					if (ctx.params.user_id) {
						await ctx.call("authorization.users.get", { id: ctx.params.user_id });
					} else {
						throw new MoleculerError("'user_id' is required!", 400);
					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
			remove: ["deleteRelatedHistory"],
		},
		after: {
			create: [
				async function createHistory(ctx, res) {
					await this.handleHistory(ctx, _.first(res));
				},
			],
			approve: [
				async function notification(ctx, res) {
					await this.sendNotification(ctx, "APPROVED", _.first(res).user_id, ctx.params.notes);
				},
			],
			reject: [
				async function notification(ctx, res) {
					await this.sendNotification(
						ctx,
						"REJECTED",
						_.first(res).user_id,
						_.first(res),
						ctx.params.notes,
					);
				},
			],
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		clearServiceCache: {
			// REST: GET /
			visibility: "public",
			async handler() {
				return this.clearCache();
			},
		},
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		getSearch: {
			visibility: "published",
			cache: false,
			scope: "private_accommodation:wanted_ads:read",
			rest: {
				method: "GET",
				path: "/search",
			},
			async handler(ctx) {
				let params = this.sanitizeParams(ctx, ctx.params);
				params.query = params.query || {};
				/* params.query.active = true; */
				params.query.status = "Approved";
				return this._list(ctx, params);
			},
		},
		approve: {
			visibility: "published",
			cache: false,
			rest: "POST /:id/approve",
			scope: "private_accommodation:wanted_ads:approve",
			async handler(ctx) {
				const status = WANTED_ADS_STATUS.APPROVED;
				return this.handleStatusChange(ctx, status);
			},
		},
		reject: {
			visibility: "published",
			cache: false,
			rest: "POST /:id/reject",
			scope: "private_accommodation:wanted_ads:reject",
			async handler(ctx) {
				const status = WANTED_ADS_STATUS.REJECTED;
				return this.handleStatusChange(ctx, status);
			},
		},
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Creates the history of changes in the state of the wanted ad
		 * @param {object} ctx
		 * @param {object} wanted_ad
		 */
		async handleHistory(ctx, wanted_ad) {
			let history;
			// Save status change history
			if (wanted_ad && !_.isEmpty(wanted_ad)) {
				const history_params = _.assign(
					{},
					{ user_id: ctx.meta.user.id },
					{ status: wanted_ad.status },
					{ wanted_ad_id: wanted_ad.id },
					{ notes: _.has(ctx, "params.notes") ? ctx.params.notes : "" },
				);
				history = await ctx.call("private_accommodation.wanted_ad_history.create", history_params);
				return history;
			}
		},
		/**
		 * Change wanted ad status
		 * @param {object} ctx
		 * @param {string} status
		 */
		async handleStatusChange(ctx, status) {
			const wanted_ad = await this._get(ctx, { id: ctx.params.id });

			if (wanted_ad[0].status != WANTED_ADS_STATUS.PENDING) {
				throw new Errors.ValidationError(
					"Wanted ad not in a Pending status",
					"WANTED_AD_NOT_PENDING",
					{
						wanted_ad_id: ctx.params.id,
						status: _.first(wanted_ad).status,
					},
				);
			}
			const result = await this._update(
				ctx,
				{
					id: _.first(wanted_ad).id,
					status,
				},
				true,
			);

			// Save status change history
			await this.handleHistory(ctx, _.first(result));

			return result;
		},
		async deleteRelatedHistory(ctx, res) {
			typeof res;
			return ctx
				.call("private_accommodation.wanted_ad_history.find", {
					query: {
						wanted_ad_id: ctx.params.id,
					},
				})
				.then((res) => {
					res.map(
						async (wantedAdHistoryOderRel) =>
							await ctx.call("private_accommodation.wanted_ad_history.remove", {
								id: wantedAdHistoryOderRel.id,
							}),
					);
				})
				.catch((err) => this.logger.error("Unable to delete Wanted Ads History relation!", err));
		},
		async sendNotification(ctx, status, user_id, wanted_ad, notes = "") {
			await ctx.call("notifications.alerts.create_alert", {
				alert_type_key: "PRIVATE_ACCOMMODATION_WANTED_AD_" + status.toUpperCase(),
				user_id: user_id,
				user_data: { email: null, phone: null },
				data: { wanted_ad: wanted_ad, notes: notes },
				variables: {},
				external_uuid: null,
				medias: [],
			});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {},
};
