/* eslint-disable no-unused-vars */
module.exports = {
	COMPLAINT_STATUS: {
		NEW: "New",
		CLOSED: "Closed",
	},

	LISTING_HOUSE_TYPOLOGY: {
		HOUSE: "HOUSE",
		APARTMENT: "APARTMENT",
	},

	LISTING_USABLE_SPACE: {
		WHOLE_HOUSE: "WHOLE_HOUSE",
		SHARED_ROOM: "SHARED_ROOM",
	},

	LISTING_GENDER: {
		MALE: "M",
		FEMALE: "F",
		OTHER: "I",
	},

	LISTING_OCCUPATION: {
		NATIONAL_STUDENTS: "N",
		INTERNATIONAL_STUDENTS: "F",
		OTHER: "I",
	},

	LISTING_STATUS: {
		PENDING: "Pending",
		APPROVED: "Approved",
		REJECTED: "Rejected",
	},

	WANTED_ADS_STATUS: {
		PENDING: "Pending",
		APPROVED: "Approved",
		REJECTED: "Rejected",
	},

	OWNER_STATUS: {
		PENDING: "Pending",
		APPROVED: "Approved",
		REJECTED: "Rejected",
	},

	OWNER_APPLICATION_STATUS: {
		PENDING: "Pending",
		APPROVED: "Approved",
		REJECTED: "Rejected",
	},
};
