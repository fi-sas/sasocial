/**
 * @license
 * Copyright Fisas All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://www.fisas.com/license
 */
export * from './lib/share.module';
export {
  FiResponsiveBreakpointDirective,
  RESPONSIVE_BREAKPOINTS_TOKEN
} from './lib/responsive.directive';
export { FiPriorityService } from './lib/priority.service';
