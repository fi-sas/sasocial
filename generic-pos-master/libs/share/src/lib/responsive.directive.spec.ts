import { FiShareModule } from './share.module';
import { By } from '@angular/platform-browser';
import { Component, DebugElement } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { FiResponsiveBreakpointDirective } from './responsive.directive';

@Component({
  selector: 'fi-dummy',
  template: `
  <div fi-responsive>
    <h1>Dummy Component</h1>
  </div>
  `
})
class DummyComponent {}

describe('> Responsive directive', () => {
  let fixture: ComponentFixture<DummyComponent>;
  let component: DummyComponent;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiShareModule],
        declarations: [DummyComponent]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(DummyComponent);
          component = fixture.componentInstance;
        });
    })
  );

  it('# Should listen when trigger window resize', (done: DoneFn) => {
    const debugElement: DebugElement = fixture.debugElement.query(
      By.directive(FiResponsiveBreakpointDirective)
    );
    const responsiveDirective = debugElement.injector.get(
      FiResponsiveBreakpointDirective
    );

    responsiveDirective.responsiveChange.subscribe(size => {
      expect(size.hasOwnProperty('key')).toBeTruthy();
      expect(size.hasOwnProperty('width')).toBeTruthy();
      done();
    });

    window.dispatchEvent(new Event('resize'));
  });
});
