import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';
import { TemplateComponent } from './core/components/template/template.component';
import { UnavailableComponent } from './scenes/unavailable/unavailable.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateComponent,
    children: [
      {
        path: 'install',
        loadChildren: () => import('./scenes/install/install.module').then(m => m.FiInstallModule)
      },
      {
        path: 'queue',
        loadChildren: () => import('./scenes/queue/queue.module').then(m => m.QueueModule)
        
      },
      {
        path: 'unavailable',
        component: UnavailableComponent
      },
      { path: '', redirectTo: 'queue', pathMatch: 'full' },
    
      {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
