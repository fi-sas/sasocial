import { FiConfigurator } from "@fi-sas/configurator";

import {
  ComponentRef
} from '@angular/core';
import { FiElectronService } from "./shared/services/electron.service";
import { CONFIGURATION } from "./app.config";
import { merge } from 'lodash';


export function bootstrapListenerFactory(configurator: FiConfigurator, electronService: FiElectronService) {

  return (component: ComponentRef<any>) => {
    let data = merge(CONFIGURATION, electronService.preferences.store);
    let root = document.querySelector(':root') as any;
    let theme;
    let colors;

    if (!electronService.isReady()) {
      theme = configurator.getOption('INSTITUTE.THEME');
      colors =
        (configurator.getOptionTree(
          `INSTITUTE.COLORS.${theme}`,
          false
        ) as string[]) || [];
      
    } else {
      theme = data.INSTITUTE.THEME;
      colors = data.INSTITUTE.COLORS[theme];
    }
    colors.forEach((color, index) => {
      const num = index + 1;
      root.style.setProperty(`--fisas-${num > 9 ? num : '0' + num}`, color);
    });
  };
}
