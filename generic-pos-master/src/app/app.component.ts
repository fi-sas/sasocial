import { Component,HostBinding } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  @HostBinding('class') theme = 'page theme-ipvc';

  constructor(
    private translate: TranslateService,
    private configurator: FiConfigurator,
  ) {
    // SETUP TRANSLATIONS
    translate.addLangs(["pt", "en"]);
    translate.setDefaultLang("pt");

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/pt|en/) ? browserLang : "pt");

    this.theme =
      'page theme-' +
      this.configurator
        .getOption<string>('INSTITUTE.THEME', 'IPVC')
        .toLowerCase();

  }
}
