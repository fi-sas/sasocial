import { environment } from '../environments/environment';

export const CONFIGURATION = {
  DEFAULT_LANG: 'pt',
  LANGUAGES: [
    { acronym: 'pt', id: 3, name: 'Português' },
    { acronym: 'en', id: 4, name: 'English' }
  ],
  DOMAINS_API: [
    { HOST: environment.api_gateway, KEY: '@api_gateway' },
    { HOST: '', KEY: '@local' }
  ],
  ENDPOINTS: {
    AUTH: {
      LOGIN: '@api_gateway:/api/authorization/authorize/device/:id',
      LOGIN_USER: '@api_gateway:/api/authorization/authorize/user',
      REFRESH_TOKEN: '@api_gateway:/api/authorization/authorize/refresh-token/:type',
    },
    QUEUE: {
      SERVICES: '@api_gateway:/api/queue/services/device',
      ISSUETICKET: '@api_gateway:/api/queue/tickets/sequential'
    },
  },
  SCREEN_SAVER: {
    TIME_OUT: 120 * 1000
  },
  INSTITUTE: {
    THEME: 'IPVC',
    COLORS: {
      IPVC: [
        '#69c9d9',
        '#3faec0',
        '#1791a5',
        '#107e90',
        '#0f6776',
        '#0b5a67',
        '#084d59',
        '#06353c',
        '#041e22',
        '#031416',
        '#02090b'
      ],
      IPCA: [
        '#1ba974',
        '#0f8156',
        '#076743',
        '#005133',
        '#02482e',
        '#013c26',
        '#013320',
        '#012819',
        '#011d12',
        '#04130d',
        '#010b07'
      ],
      IPB: [
        '#d72d9a',
        '#a5006a',
        '#870157',
        '#79024e',
        '#6e0347',
        '#680143',
        '#61003e',
        '#520035',
        '#330021',
        '#210417',
        '#15020e'
      ],
      IPViseu: [
        '#EEEEEE',
        '#D5D5D5',
        '#BABBBB',
        '#A3A4A4',
        '#8A8A8A',
        '#737474',
        '#5C5E5F',
        '#4B4E4E',
        '#2E3233',
        '#161C1E',
        '#161C1E'
      ],
      IPP: [
        '#F7F3E6',
        '#EBE2C1',
        '#E0D09B',
        '#D5C079',
        '#C9AF54',
        '#BF9F33',
        '#99802B',
        '#7C6A24',
        '#4B431A',
        '#232412',
        '#0f0f08'
      ],
      IPL: [
        '#F2E3E5',
        '#DEBABE',
        '#CA8E95',
        '#B76871',
        '#A43E4A',
        '#921928',
        '#741621',
        '#5E141C',
        '#3A0F15',
        '#1B0C10',
        '#010b07'
      ],
      IPCoimbra: [
        '#EEEEEE',
        '#D5D5D5',
        '#BABBBB',
        '#A3A4A4',
        '#8A8A8A',
        '#737474',
        '#5C5E5F',
        '#4B4E4E',
        '#2E3233',
        '#161C1E',
        '#161C1E'
      ]
    }
  },
  DEVICE_ID: 8
};
