
import { NgModule, APP_INITIALIZER, APP_BOOTSTRAP_LISTENER } from '@angular/core';
import { FiConfigurator } from "@fi-sas/configurator";
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { FiCoreModule } from '@fi-sas/core';
import { AppRoutingModule } from './app-routing.module';
import { CONFIGURATION } from './app.config';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { bootstrapListenerFactory } from './app.bootstrap';
// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenInterceptor } from './shared/interceptor/token.interceptor';
import { AuthService } from './auth/services/auth.service';
import { ApplicationInitializer } from './app.initializer';
import { FiElectronService } from './shared/services/electron.service';
import { Socket, SocketIoConfig, SocketIoModule } from 'ngx-socket-io';

import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { UnavailableComponent } from './scenes/unavailable/unavailable.component';
import { FiMiddlewareListenerService } from './shared/services/middleware.service';
import { FiPrintService } from './shared/services/print.service';

const config: SocketIoConfig = {
  url: "http://localhost:8080",
  options: {
    transports:  ['websocket']
  }
};

export function initializeApp(applicationInitializer: ApplicationInitializer, authService: AuthService) {
  return (): Promise<any> => {
    return applicationInitializer.initialize().then(()=> {
      return authService.refreshToken();
    }
    );
  };
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent,UnavailableComponent],
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FiCoreModule,
    CoreModule,
    SocketIoModule.forRoot(config),
    SharedModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ApplicationInitializer,
    AuthService,
    {
      provide: FiMiddlewareListenerService,
      useClass: FiMiddlewareListenerService,
      deps: [Router, Socket]
    },
    {
      provide: FiPrintService,
      useClass: FiPrintService,
      deps: [Socket]
    },
    { provide: APP_INITIALIZER,useFactory: initializeApp, deps: [ApplicationInitializer, AuthService], multi: true},
    {
      provide: APP_BOOTSTRAP_LISTENER,
      useFactory: bootstrapListenerFactory,
      multi: true,
      deps: [FiConfigurator,FiElectronService ]
    },

    { provide: OPTIONS_TOKEN, useValue: CONFIGURATION },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }

  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(
    private router: Router,
  ) {
    this.networkListeners();
  }

  private networkListeners() {
    fromEvent(window, 'offline')
      .pipe(map(e => this.router.navigate(['unavailable'])))
      .subscribe(() => console.dir('offline'));

    fromEvent(window, 'online')
      .pipe(map(e => this.router.navigate(['/'])))
      .subscribe(() => console.dir('online'));
  }
}
