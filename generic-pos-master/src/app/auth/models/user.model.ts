
export class UserModel {
  id: number;
  rfid: string;
  name: string;
  email: string;
  phone: string;
  user_name: string;
  institute: string;
  student_number: string;
  gender: Gender;
  profile_id: number;
  profile: any;
  external: boolean;
  active: boolean;
  first_login: boolean;
  can_access_BO: boolean;
  address: string;
  city: string;
  country: string;
  postal_code: string;
  tin: string;
  identification: string;
  birth_date: string;
  scopes: any[];
  user_groups: any[];
  updated_at: Date;
  created_at: Date;
  organic_unit_id: number;
  course_degree_id: number;
  course_id: number;
  course_year: number;
}


export enum Gender {
  MALE = 'M',
  FEMALE = 'F',
  OTHER = 'U',
}