import { Observable, BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { FiResourceService, FiUrlService } from "@fi-sas/core";
import { first } from "rxjs/operators";
import { HttpHeaders } from "@angular/common/http";
import { UserModel } from "./../models/user.model";
import { LoginSuccessModel } from "./../models/login-success.model";
import { CONFIGURATION } from '../../app.config';

export class AuthObject {
  user: UserModel;
  scopes: AuthScope[];
  token: string;
  expires_in: Date;
  authDate: Date;
}

export class AuthScope {
  microservice: string;
  entity: string;
  permission: string;
  all: boolean;
}

@Injectable({
  providedIn: "root",
})
export class AuthService {

  userSubject: BehaviorSubject<any> = new BehaviorSubject<any>(new UserModel());

  private _authObj: AuthObject;
  private _token: string;
  private _refreshTimeout = null;
  isGuest = false;
  isGuestSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  constructor(
    private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) {
    this.changeIsGuest(false);

  }

  
  /***
   * Creates a token guest
   */
  loginGuest(): Promise<AuthObject> {
    return new Promise<AuthObject>((resolve, reject) => {
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.LOGIN", { id: CONFIGURATION.DEVICE_ID }),
          {},
          {
            headers: new HttpHeaders().append("no-error", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {
            this.createAuthObject(value.data[0]).subscribe(
              (res) => {

                resolve(res);
              },
              (err) => {
                reject(err);
              }
            );
          },
          (error) => {
            reject(error);
          }
        );
    });
  }


  /***
   * Creates the auth objects after authentication
   */
  createAuthObject(result: LoginSuccessModel): Observable<AuthObject> {
    return new Observable<AuthObject>((observer) => {
      const authObj = new AuthObject();

      authObj.authDate = new Date();
      authObj.token = result.token;
      authObj.expires_in = new Date(result.expires_in);

      this.resourceService
        .read<UserModel>(this.urlService.get("AUTH.LOGIN_USER"), {
          headers: { Authorization: "Bearer " + authObj.token },
        })
        .subscribe(
          (value1) => {
            const isGuest = value1.data[0].user_name === 'guest';

            if (!isGuest) {
              authObj.user = value1.data[0];
              this.userSubject.next(authObj.user);
            }
            this.saveAuthObj(authObj);

            if (isGuest) {
              this.changeIsGuest(true);
            } else {
              this.changeIsGuest(false);
            }

            // CREATES THE TIMEOUT TO UPDATE THE TOKEN
            this.createTimeoutToUpdateToken();

            observer.next(authObj);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            observer.complete();
          }
        );
    });
  }


  /***
   * Save the auth obj to local storage
   */
  private saveAuthObj(authObj: AuthObject) {
    this._authObj = authObj;
  }

  /***
 * Return the user data of the login data
 */
  getAuth(): AuthObject {
    return this._authObj;
  }


  createTimeoutToUpdateToken() {
    var now = new Date().getTime();
    var diff = this._authObj.expires_in.getTime() - now - (1000 * 60);
    this._refreshTimeout = setTimeout(() => {
      this.refreshToken();
      clearTimeout(this._refreshTimeout);
    }, diff);
  }


  private changeIsGuest(isGuest: boolean) {
    this.isGuest = isGuest;
    this.isGuestSubject.next(this.isGuest);
  }


  refreshToken(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.resourceService
        .create<LoginSuccessModel>(
          this.urlService.get("AUTH.REFRESH_TOKEN", { type: 'GENERIC' }),
          {},
          {
            headers: new HttpHeaders().append("no-error", "true"),
            withCredentials: true,
          }
        )
        .pipe(first())
        .subscribe(
          (value) => {
            this._token = value.data[0].token;

            this.createAuthObject(value.data[0]).subscribe(
              (res) => {
                resolve(res);
              },
              (err) => {
                reject(err);
              }
            );
          },
          (err) => {
           
            this.loginGuest()
          
              .then((res) => {
                resolve(res);
              })
              .catch((err1) => {
                reject();
              });
          }
        );
    });
  }

}
