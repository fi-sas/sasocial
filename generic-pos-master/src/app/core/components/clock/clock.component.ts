import { Component, OnInit, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'fi-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.less']
})
export class ClockComponent implements OnInit, OnDestroy {
  @Input() seconds = false;

  interval: NodeJS.Timer;
  todayDate = Date.now();

  public today = new Date(this.todayDate);
  public lastDayOfMarch = new Date(new Date().getFullYear() + '-03-31');
  public lastDayOfOctober = new Date(new Date().getFullYear() + '-10-31');
  public lastSundayOfMarch = new Date(
    this.lastDayOfMarch.setDate(
      this.lastDayOfMarch.getDate() - this.lastDayOfMarch.getDay()
    )
  );
  public lastSundayOfOctober = new Date(
    this.lastDayOfOctober.setDate(
      this.lastDayOfOctober.getDate() - this.lastDayOfOctober.getDay()
    )
  );
  public timezone = 'UTC+0';

  ngOnInit() {
    this.setTimezone();

    this.interval = setInterval(() => {
      this.setTimezone();
      this.todayDate = Date.now();
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  setTimezone() {
    this.today = new Date(this.todayDate);
    this.timezone =
      this.today >= this.lastSundayOfMarch &&
      this.today < this.lastSundayOfOctober
        ? 'UTC+1'
        : 'UTC+0';
  }
}
