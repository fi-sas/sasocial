import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

    public institute: string;
    public language: string;

    constructor(
        public translate: TranslateService,
        private configurator: FiConfigurator
    ) { }

    ngOnInit(): void {
        this.institute = this.configurator
            .getOption<string>('ORGANIZATION.THEME', 'IPVC')
            .toUpperCase();
    }

    langChange(lang) {
        this.translate.use(lang);
    }
}
