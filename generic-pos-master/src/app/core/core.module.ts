import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SharedModule } from '../shared/shared.module';
import { DateComponent } from './components/date/date.component';
import { ClockComponent } from './components/clock/clock.component';
import { TemplateComponent } from './components/template/template.component';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt-PT');

@NgModule({
  declarations: [
    HeaderComponent,
    DateComponent,
    ClockComponent,
    TemplateComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    HeaderComponent,
    TemplateComponent,
  ],
  
})
export class CoreModule { }
