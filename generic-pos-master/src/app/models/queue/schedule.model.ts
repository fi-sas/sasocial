export class ScheduleModelQueue {
    id: number;
    description: string;
    week_day: number
    schedules: SchedulesModelQueue[] = []
  }
    
  export class SchedulesModelQueue {
    opening_hour: string;
    closing_hour: string;
    id: number;
  }