import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiInstallComponent } from './install.component';

const routes: Routes = [
    {
        path: '',
        component: FiInstallComponent
    },
    { path: '', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FiInstallRoutingModule { }
