import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FiConfigurator } from "@fi-sas/configurator";
import { AuthService } from "../../auth/services/auth.service"
import { FiElectronService } from "../../shared/services/electron.service"
import { merge } from 'lodash';
import { concatMap, tap } from 'rxjs/operators';
import { Router } from "@angular/router";


interface FormInstallValue {
    device: number;
    institute: string;
    domain: string;

}

@Component({
    selector: 'fi-install',
    templateUrl: './install.component.html',
    styleUrls: ['./install.component.less']
})


export class FiInstallComponent implements OnInit {
    formInstall: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private configurator: FiConfigurator,
        private authService: AuthService,
        private router: Router,
        private electronService: FiElectronService) {

    }

    ngOnInit() {
        this.formInstall = this.formBuilder.group({
            institute: ['', [Validators.required]],
            domain: ['', [Validators.required]],
            device: ['', [Validators.required]]
        })
    }

    get f() { return this.formInstall.controls; }

    saveConfigurations() {
        const model: FormInstallValue = this.formInstall.value;
        if (this.formInstall.valid) {
            const config = {
                DEVICE_ID: model.device,
                DOMAINS_API: [
                    { HOST: model.domain, KEY: '@api_gateway' },
                    { HOST: '', KEY: '@local' }
                ],
                INSTITUTE: {
                    THEME: model.institute
                },
            };


            this.electronService.changes
                .pipe(
                    tap(() => {
                        this.configurator.options = merge(
                            this.configurator.options,
                            config
                        );
                    }),
                    concatMap(() => this.authService.loginGuest())
                )
                .subscribe();

            this.electronService.storeConfig(config);
            this.electronService.send('ELECTRON_RELAUCH');
            this.router.navigate(['/']);
        }
    }
}