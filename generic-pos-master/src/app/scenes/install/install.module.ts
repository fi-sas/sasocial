import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';


import { FiInstallRoutingModule } from './install-routing.module';
import { FiInstallComponent } from './install.component';

@NgModule({
  imports: [FiInstallRoutingModule, SharedModule],
  declarations: [FiInstallComponent]
})

export class FiInstallModule {}
