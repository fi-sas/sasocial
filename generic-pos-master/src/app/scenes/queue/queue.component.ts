import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { issueTicketModel } from '@fi-sas/kiosk/models/queue/issue-ticket.model';
import { ServiceModelQueue } from '@fi-sas/kiosk/models/queue/service.model';
import { SubjectModel } from '@fi-sas/kiosk/models/queue/subject.model';
import { TicketsModel } from '@fi-sas/kiosk/models/queue/tickets.model';
import { TranslationModel } from '@fi-sas/kiosk/shared/models/translation.model';
import { FiPrintService } from '@fi-sas/kiosk/shared/services/print.service';
import { finalize, first } from 'rxjs/operators';
import { QueueService } from './queue.service';
const dayjs = require('dayjs');

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.less']
})
export class QueueComponent implements OnInit {

  public subjectSelected: SubjectModel = new SubjectModel();
  public loading = true;
  public services: ServiceModelQueue[] = [];
  public modalSuccess = false;
  public responseDataTicket: TicketsModel = new TicketsModel();
  public translation: TranslationModel[] = [];

  constructor(
    public queueService: QueueService,
    private printerService: FiPrintService,
  ) { }

  ngOnInit(): void {
    this.getServices();
  }

  getServices() {
    this.queueService.listServices().pipe(first(), finalize(() => (this.loading = false))).subscribe((data) => {
      this.services = data.data;
    })
  }

  transformSecontToHour(value: number) {
    let h = Math.floor(value / 3600);
    let m = Math.floor(value % 3600 / 60);
    let hDisplay;
    let mDisplay;
    if (h == 0) {
      hDisplay = '00';
    } else if (h < 10) {
      hDisplay = '0' + h;
    } else {
      hDisplay = h;
    }
    if (m == 0) {
      mDisplay = '00';
    } else if (m < 10) {
      mDisplay = m + '0';
    } else {
      mDisplay = m;
    }
    return hDisplay + 'h' + mDisplay + 'm';
  }

  selected(subject: SubjectModel) {

    if (this.subjectSelected.id != undefined && this.subjectSelected.id == subject.id) {
      this.subjectSelected = new SubjectModel();
    } else {
      this.subjectSelected = subject;
    }
  }

  issueTicket() {
    let sendValues: issueTicketModel = new issueTicketModel();
    sendValues.priority = false;
    sendValues.subject_id = this.subjectSelected.id;

    this.queueService.issueTicket(sendValues).pipe(first()).subscribe((data) => {
      this.subjectSelected = new SubjectModel();
      this.responseDataTicket = data.data[0].ticket;

      this.printerService.printTicket({
        date: dayjs(this.responseDataTicket.created_at).format("YYYY-MM-DD"),
        hour: dayjs(this.responseDataTicket.created_at).format("HH:mm"),
        ticket: this.responseDataTicket.ticket_code,
        current_ticket: "---",
        people_after_you: data.data[0].ntickets.toString(),
        estimated_hour: dayjs(this.responseDataTicket.estimated_time).format("HH:mm"),
      });

      this.modalSuccess = true;
      setTimeout(() => this.modalSuccess = false, 4000);

    });
  }

}

