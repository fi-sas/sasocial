import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QueueRoutingModule } from './queue-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { QueueComponent } from './queue.component';
import { CoreModule } from '../../core/core.module';
import { QueueService } from './queue.service';

@NgModule({
  declarations: [QueueComponent],
  imports: [
    CommonModule, 
    SharedModule, 
    QueueRoutingModule,
    CoreModule
  ],
  providers: [
    QueueService
  ]
})
export class QueueModule {}
