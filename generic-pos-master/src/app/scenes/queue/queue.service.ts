
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { issueTicketModel } from '../../models/queue/issue-ticket.model';
import { ServiceModelQueue } from '../../models/queue/service.model';
import { SubjectModel } from '../../models/queue/subject.model';
import { ResponseTicketModel } from '../../models/queue/response-isseu-ticket.model';
import {
  FiResourceService,
  FiUrlService,
  Resource
} from '@fi-sas/core';

@Injectable({
  providedIn: 'root',
})
export class QueueService {

  loading = false;

  constructor(private resourceService: FiResourceService,
    private urlService: FiUrlService
  ) { }

  listServices(): Observable<Resource<ServiceModelQueue>> {
    let params = new HttpParams();
    params = params.set('withRelated', 'subjects,translations,groups');
    params = params.set('query[active]', 'true');
    return this.resourceService.list<ServiceModelQueue>(this.urlService.get('QUEUE.SERVICES'), {params});
  }

  issueTicket(sendValue: issueTicketModel){
    return this.resourceService.create<ResponseTicketModel>(
      this.urlService.get("QUEUE.ISSUETICKET"),
      sendValue
    );
  }

}
