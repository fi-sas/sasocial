import { AuthService, AuthObject } from '../../auth/services/auth.service';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const authObj: AuthObject = this.authService.getAuth();
    if (authObj && authObj.token && !request.headers.has('Authorization')) {
      const cloned = request.clone({
        headers: request.headers.append('Authorization', `Bearer ${authObj.token}`)
      });
      return next.handle(cloned);
    }
    return next.handle(request);
  }
}
