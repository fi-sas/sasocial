export class TranslationModel {
    id: number;
    language_id: number;
    message: string;
    type: string;
    language: LanguageModel;
}

export class LanguageModel {
    acronym: string;
    active: boolean;
    id: number;
    name: string;
    order: number;
}

export class TranslationGenericModel {
    language_id: number;
    name: string;
    description?: string;
}