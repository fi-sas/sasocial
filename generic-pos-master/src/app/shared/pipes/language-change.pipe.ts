import { ChangeDetectorRef, Pipe, PipeTransform} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { TranslationGenericModel } from '../models/translation.model'

@Pipe({
  name: 'languageChange',
  pure: false
})
export class LanguageChangePipe implements PipeTransform {

  value: string;
  currentLang;
  languagesIDS: any = [];

  constructor(private translate: TranslateService,
              private ref: ChangeDetectorRef,
              private config: FiConfigurator) {

    if (config !== undefined) {
      this.languagesIDS = Object.values(config.getOptionTree('LANGUAGES'))[0];
    } else {
      this.languagesIDS = undefined;
    }

    translate.onLangChange.subscribe(() => {
      if (this.languagesIDS !== undefined) {
      this.currentLang = this.languagesIDS.find(lang => lang.acronym === this.translate.currentLang);
      } else {
        this.currentLang = undefined;
      }
      this.currentLang = this.currentLang !== undefined ? this.currentLang.id : 0;
      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
      }
    });
  }

  transform( translations: TranslationGenericModel[] ): any {
      let translateObject;

      if (this.languagesIDS !== undefined && this.languagesIDS != null) {
        this.currentLang = this.languagesIDS.find(lang => lang.acronym === this.translate.currentLang);
      } else {
        this.currentLang = undefined;
      }

      this.currentLang = this.currentLang !== undefined ? this.currentLang.id : 0;

      if (translations !== null && translations != undefined) {
        translateObject = translations.find(trans => trans.language_id === this.currentLang);
        if (translateObject !== undefined) {
          return translateObject;
        } else {
          return translations[0];
        }
      } else {
        return {acronym: 'none', id: 0};
      }
  }

}
