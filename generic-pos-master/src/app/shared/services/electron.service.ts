import { Injectable } from '@angular/core';
import * as childProcess from 'child_process';
import { ipcRenderer, IpcRendererEvent, Menu } from 'electron';
import { Subject } from 'rxjs';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
@Injectable()
export class FiElectronService {
  private _ipc: typeof ipcRenderer;
  private childProcess: typeof childProcess;

  public changes = new Subject();

  /**
   * Typing for Conf preferences.
   */
  public preferences: {
    set: (key: string, value: any) => void;
    get: (key: string) => any;
    has: (key: string) => boolean;
    delete: (key: string) => void;
    clear: () => void;
    onDidChange: (
      key: string,
      callback: (newValue: any, oldValue: any) => {}
    ) => void;
    size: number;
    store: Object;
  };

  constructor() {
    // Conditional imports
    if (this.isElectron()) {
      this._ipc = window.require('electron').ipcRenderer;
      this.childProcess = window.require('child_process');

      this._initPreferencesStore();
    }
  }

  private _initPreferencesStore() {
    const Conf = window.require('conf');
    const electron = window.require('electron');

    const userDir = electron.remote.app.getPath('userData');    
    electron.webFrame.setVisualZoomLevelLimits(1, 1);

    this.preferences = new Conf({
      cwd: userDir,
      configName: 'preferences'
    });
  }

  public storeConfig(config: any) {
    this.preferences.store = config;
    this.changes.next(config);
  }

  public isElectron(): boolean {
    return !!window && !!window.process && !!window.process.type;
  }

  public isReady(): boolean {
    return this.isElectron() && this.preferences.size > 0;
  }

  public on(channel: string, listener: (event: IpcRendererEvent, ...args: any[]) => void): void {
    if (!this._ipc) {
      return;
    }

    this._ipc.on(channel, listener);
  }

  public send(channel: string, ...args): void {
    if (!this._ipc) {
      return;
    }

    this._ipc.send(channel, ...args);
  }
}
