import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class FiMiddlewareListenerService {

  isSocketOn = false;

  constructor(
    private _router: Router,
    _socket: Socket) {

      alert("AQUI AUQI");
    const timeout = setTimeout(() => {
      if (!this.isSocketOn && this._router.url !== '/install')
        this.navigateToUnavailable();

      clearTimeout(timeout);
    }, 5000);

    console.log("AQUI");
    console.log(_socket);
    _socket.on('connect', () => {
      alert("Connected");

      this.isSocketOn = true;

      if (this._router.url !== '/install') {
        this.gotoScreenSaver();
      }
    });
    _socket.on('disconnect', () => {
      this.isSocketOn = false;
      if (this._router.url !== '/install') {
        this.navigateToUnavailable();
      }
    });

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean> {
    if (this.isSocketOn || state.url === '/install') {
      return of(true);
    } else {
      this.navigateToUnavailable();
      return of(false);
    }
  }

  gotoScreenSaver(): void {
    this._router.navigate(['/']);
  }

  navigateToUnavailable() {
    this._router.navigate(['unavailable']);
  }

}

