import { Injectable } from "@angular/core";
import { Socket } from "ngx-socket-io";

export enum PrintCommandEnum {
  LOGO = "LOGO",
  CTLS = "CTLS",
  //SET = "set",
  NEW_LINE = "NEW_LINE",
  TEXTLN = "textLn",
  TEXT = "text",
  DIVIDER = "DIVIDER",
  STYLE_DOUBLE_HEIGHT = "STYLE_DOUBLE_HEIGHT",
  STYLE_DOUBLE_WIDTH = "STYLE_DOUBLE_WIDTH",
  STYLE_DOUBLE_HEIGHT_WIDTH = "STYLE_DOUBLE_HEIGHT_WIDTH",
  STYLE_DOUBLE_HEIGHT_WIDTH_BOLD = "STYLE_DOUBLE_HEIGHT_WIDTH_BOLD",
  STYLE_BOLD = "STYLE_BOLD",
  STYLE_NONE = "STYLE_NONE",
  //LEFTRIGHT = "leftRight",
  //TABLE = "table",
  //NOW = "now",
  CUT = "cut",
}

interface CmdLeftRight {
  left: string;
  right: string;
}

interface CmdTable {
  data: string[];
  widths: number[];
}

export interface PrintCommand {
  cmd: PrintCommandEnum;
  value: string | CmdTable | CmdLeftRight;
}

@Injectable()
export class FiPrintService {
  private commands: PrintCommand[] = [];

  /**
   * Constructor
   *
   */
  constructor(private socket: Socket) {}

  printTicket(ticket: {
    date: string;
    hour: string;
    ticket: string;
    current_ticket: string;
    people_after_you: string;
    estimated_hour: string;
  }): void {
    this.commands.push({ cmd: PrintCommandEnum.LOGO, value: "" });
    this.addDivider();
    this.setStyleDoubleHeight();
    this.addTextLn("SERVIÇOS DE AÇÃO SOCIAL");
    this.setStyleNone();
    this.addDivider();
    this.addTextLn(`Data: ${ticket.date}               Hora: ${ticket.hour}`);
    this.setStyleDoubleHeightwidthBold();
    this.addTextLn(ticket.ticket);
    this.setStyleNone();
    this.addNewline();
    this.addDivider();
    this.addNewline();
    this.addTextLn("Senha em atendimento");
    this.setStyleBold();
    this.addTextLn(ticket.current_ticket);
    this.setStyleNone();
    this.addText("Estão ");
    this.setStyleBold();
    this.addText(ticket.people_after_you);
    this.setStyleNone();
    this.addTextLn(" a sua frente");
    this.addTextLn("Hora estimada para o atendimento");
    this.setStyleBold();
    this.addTextLn(ticket.estimated_hour);
    this.setStyleNone();
    this.addNewline();
    this.addCut();
    this.flush();
  }

  private flush(): void {

    this.socket.emit("SEND_TO_PRINTER", this.commands);
    this.commands = [];
  }

  private setStyleDoubleHeightwidthBold() {
    this.commands.push({
      cmd: PrintCommandEnum.STYLE_DOUBLE_HEIGHT_WIDTH_BOLD,
      value: "",
    });
  }
  private setStyleDoubleHeight() {
    this.commands.push({
      cmd: PrintCommandEnum.STYLE_DOUBLE_HEIGHT,
      value: "",
    });
  }
  private setStyleBold() {
    this.commands.push({ cmd: PrintCommandEnum.STYLE_BOLD, value: "" });
  }
  private setStyleNone() {
    this.commands.push({ cmd: PrintCommandEnum.STYLE_NONE, value: "" });
  }

  private addTextLn(value: string) {
    this.commands.push({ cmd: PrintCommandEnum.TEXTLN, value });
  }
  private addText(value: string) {
    this.commands.push({ cmd: PrintCommandEnum.TEXT, value });
  }

  private addDivider() {
    this.commands.push({ cmd: PrintCommandEnum.DIVIDER, value: "" });
  }

  private addNewline() {
    this.commands.push({ cmd: PrintCommandEnum.NEW_LINE, value: "" });
  }

  private addCut() {
    this.commands.push({ cmd: PrintCommandEnum.CUT, value: "" });
  }
}
