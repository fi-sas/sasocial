import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NzSelectModule } from "ng-zorro-antd/select";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { NzDropDownModule } from "ng-zorro-antd/dropdown";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzEmptyModule } from "ng-zorro-antd/empty";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzAlertModule } from "ng-zorro-antd/alert";
import { NzModalModule } from "ng-zorro-antd/modal";

import { TranslateModule } from "@ngx-translate/core";
import { PageNotFoundComponent } from "./components/";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FiElectronService } from "./services/electron.service";
import { FiMiddlewareListenerService } from "./services/middleware.service";
import { FiPrintService } from "./services/print.service";
import { ErrorTranslationPipe } from "./pipes/error-translation.pipe";
import { LanguageChangePipe } from "./pipes/language-change.pipe";

@NgModule({
  declarations: [
    PageNotFoundComponent,
    ErrorTranslationPipe,
    LanguageChangePipe,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    NzButtonModule,
    NzGridModule,
    NzMenuModule,
    NzDropDownModule,
    NzLayoutModule,
    NzIconModule,
    NzCardModule,
    NzFormModule,
    NzSelectModule,
    NzEmptyModule,
    NzInputModule,
    NzAlertModule,
    NzModalModule,
    ReactiveFormsModule,
  ],
  exports: [
    TranslateModule,
    FormsModule,
    NzButtonModule,
    NzGridModule,
    NzMenuModule,
    NzDropDownModule,
    NzLayoutModule,
    NzIconModule,
    NzCardModule,
    NzFormModule,
    NzSelectModule,
    NzEmptyModule,
    NzInputModule,
    NzAlertModule,
    NzModalModule,
    ReactiveFormsModule,
    LanguageChangePipe,
    ErrorTranslationPipe,
  ],
  providers: [
    FiElectronService,
    FiMiddlewareListenerService
  ],
})
export class SharedModule {}
