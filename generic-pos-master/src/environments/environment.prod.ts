export const gateway_url = 'https://sasocialdev.sas.ipvc.pt';

export const environment = {
  production: true,
  environment: 'PROD',
  api_gateway: 'https://sasocialdev.sas.ipvc.pt'
};
