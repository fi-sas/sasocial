export const gateway_url = 'https://sasocialdev.sas.ipvc.pt';

export const environment = {
  production: false,
  environment: 'DEV',
  api_gateway: 'https://sasocialdev.sas.ipvc.pt'
};
