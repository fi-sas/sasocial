# [0.23.0-rc.80](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.79...v0.23.0-rc.80) (2022-05-20)


### Bug Fixes

* **keyboard:** add dot button ([11a0ae8](https://gitlab.com/fi-sas/kiosk-frontend/commit/11a0ae84918999efd9363def57c63242dfb55bed))

# [0.23.0-rc.79](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.78...v0.23.0-rc.79) (2022-05-20)


### Bug Fixes

* **home:** no content text ([f127fe8](https://gitlab.com/fi-sas/kiosk-frontend/commit/f127fe83edd57e1b569dab03803853b84dc780c0))

# [0.23.0-rc.78](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.77...v0.23.0-rc.78) (2022-05-10)


### Features

* **canteen:** show generic allergen message ([475ae68](https://gitlab.com/fi-sas/kiosk-frontend/commit/475ae689073044b15cac34706ccd151067f8dd7c))

# [0.23.0-rc.77](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.76...v0.23.0-rc.77) (2022-02-10)


### Features

* send receipts on payment callback ([dc1fdbe](https://gitlab.com/fi-sas/kiosk-frontend/commit/dc1fdbe0649a32ce868edad554e67a25846d905e))

# [0.23.0-rc.76](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.75...v0.23.0-rc.76) (2022-02-03)


### Bug Fixes

* minor ux changes ([da0d3ea](https://gitlab.com/fi-sas/kiosk-frontend/commit/da0d3ea1eac687a6ccc8de60fd51bd22f28cc051))

# [0.23.0-rc.75](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.74...v0.23.0-rc.75) (2022-01-27)


### Bug Fixes

* **geral:** add new keyboard ([9ea02a2](https://gitlab.com/fi-sas/kiosk-frontend/commit/9ea02a2e3cf875b7cf6c77b8bada383f67907d61))
* **geral:** new keyboard ([84d1179](https://gitlab.com/fi-sas/kiosk-frontend/commit/84d11799f00a3c80380b3ba6ca0a0b424013ef9f))
* **geral:** new keyboard ([68f7e85](https://gitlab.com/fi-sas/kiosk-frontend/commit/68f7e85a66555ebf509334525f946719f333d327))
* **keyboard:** add animation on numpad ([364261b](https://gitlab.com/fi-sas/kiosk-frontend/commit/364261b3173b13b6732f1d59e74155389d28cfed))
* **keyboards:** resolve imports ([fb4d08f](https://gitlab.com/fi-sas/kiosk-frontend/commit/fb4d08f64d4497eaaff77402a055f7164d0d702e))


### Features

* **keyboard:** add special keys ([7fe79ce](https://gitlab.com/fi-sas/kiosk-frontend/commit/7fe79ce45273e95958a984891ba20cfb5031b548))
* **keyboard:** change visual of keyboard ([8510d34](https://gitlab.com/fi-sas/kiosk-frontend/commit/8510d34ccc8169ec282e2bc2ce605e0eaa24bf23))
* **keyboard:** initial version of native kiosk keyboard ([06392e4](https://gitlab.com/fi-sas/kiosk-frontend/commit/06392e46028b7557995f0ee8b8aa1fc91a154be1))

# [0.23.0-rc.74](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.73...v0.23.0-rc.74) (2022-01-10)


### Features

* **mobility:** initial purchase system ([96edf31](https://gitlab.com/fi-sas/kiosk-frontend/commit/96edf31c20c33545ae6084c4c58c34dc2b622076))

# [0.23.0-rc.73](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.72...v0.23.0-rc.73) (2021-12-21)


### Bug Fixes

* **dashboard:** add icon ([15dd1fe](https://gitlab.com/fi-sas/kiosk-frontend/commit/15dd1fe19156b9454a5a9c99c5c33f72529d9131))

# [0.23.0-rc.72](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.71...v0.23.0-rc.72) (2021-12-14)


### Bug Fixes

* **configurations:** icons external services ([17a0c0b](https://gitlab.com/fi-sas/kiosk-frontend/commit/17a0c0b52199faa6ef03959d840d8879a46d7b4e))

# [0.23.0-rc.71](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.70...v0.23.0-rc.71) (2021-11-25)


### Bug Fixes

* **main:** open chrome in mode app ([3a6950c](https://gitlab.com/fi-sas/kiosk-frontend/commit/3a6950cf1a2ce821d0e5039734167cb4f24b010e))

# [0.23.0-rc.70](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.69...v0.23.0-rc.70) (2021-11-25)


### Features

* **dashboard:** open new window dashboard ([72e4aab](https://gitlab.com/fi-sas/kiosk-frontend/commit/72e4aab3755d9ac6d41e02bece265e92ba43d7e7))

# [0.23.0-rc.69](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.68...v0.23.0-rc.69) (2021-11-24)


### Bug Fixes

* **movements:** add meal ([3a954e7](https://gitlab.com/fi-sas/kiosk-frontend/commit/3a954e752149eb56bb6d9f997bd5302774c1b0d3))

# [0.23.0-rc.68](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.67...v0.23.0-rc.68) (2021-11-24)


### Bug Fixes

* **charging:** delete text ([edc92bd](https://gitlab.com/fi-sas/kiosk-frontend/commit/edc92bde090dc451508a7087b52e4008bef53554))

# [0.23.0-rc.67](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.66...v0.23.0-rc.67) (2021-11-24)


### Bug Fixes

* **pay:** btn style ([505a445](https://gitlab.com/fi-sas/kiosk-frontend/commit/505a445cd87ebc858e117eac8312c812705a440d))

# [0.23.0-rc.66](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.65...v0.23.0-rc.66) (2021-11-17)


### Bug Fixes

* **geral:** changes requested team ([0bc425c](https://gitlab.com/fi-sas/kiosk-frontend/commit/0bc425c4150313d021583a346d0ff7895cda1fc8))

# [0.23.0-rc.65](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.64...v0.23.0-rc.65) (2021-11-16)


### Bug Fixes

* **canteen:** adding border to box when selected ([01e07cf](https://gitlab.com/fi-sas/kiosk-frontend/commit/01e07cff31e1f503cf5d7fb8fe4af7a78817cad8))
* **cart:** valid product modal ([8ee29e5](https://gitlab.com/fi-sas/kiosk-frontend/commit/8ee29e5de54f4ad3538be55f223eb6f1cb609885))
* **dashboard:** add external services ([20336eb](https://gitlab.com/fi-sas/kiosk-frontend/commit/20336ebe85755edb098ef20f363026460a2d46c3))
* **geral:** change modal confirm ([ecb4ff2](https://gitlab.com/fi-sas/kiosk-frontend/commit/ecb4ff21c7f28aafaa1557b0bf4218c0f97bdd89))
* **menu:** add right column ([7aa0ff5](https://gitlab.com/fi-sas/kiosk-frontend/commit/7aa0ff57075610c3a84d3abc48cfbfffefa7c589))

# [0.23.0-rc.64](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.63...v0.23.0-rc.64) (2021-11-15)


### Bug Fixes

* **authentication:** color input ([8f4e28e](https://gitlab.com/fi-sas/kiosk-frontend/commit/8f4e28e84984b3153ecdaa3b6c68858393e80ee8))
* **bar:** border bottom color ([9238f37](https://gitlab.com/fi-sas/kiosk-frontend/commit/9238f375a0d4525ce7cf344da17fb9abcc3216d2))
* **cart:** delete button ([a5ec1fc](https://gitlab.com/fi-sas/kiosk-frontend/commit/a5ec1fca1a400c1d0f6d2b4cdb0dbf633c7b0178))
* **charge:** change text ([38c788c](https://gitlab.com/fi-sas/kiosk-frontend/commit/38c788c9412620a768ef9e7e6a1709abc5deccc0))
* **charge:** separate error message ([5b68904](https://gitlab.com/fi-sas/kiosk-frontend/commit/5b689045222a0ff49639376d95c64abab5f0fb44))
* **current_account:** date refectory_consume_at ([7d09473](https://gitlab.com/fi-sas/kiosk-frontend/commit/7d09473593d56b6a5bb06c83c0a6d2cfdc816747))
* **geral:** initial text change ([b854cdf](https://gitlab.com/fi-sas/kiosk-frontend/commit/b854cdfec024bd2030e51520a77fe45d0a35eba0))
* **queue:** card closed ([cb418fc](https://gitlab.com/fi-sas/kiosk-frontend/commit/cb418fc51d0b1ddf4dd73025f82b13a50f924ed4))
* **queue:** change message modal ([adf89ad](https://gitlab.com/fi-sas/kiosk-frontend/commit/adf89ad31920d38695a25ea8b0dccc2f027af766))

# [0.23.0-rc.63](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.62...v0.23.0-rc.63) (2021-11-11)


### Bug Fixes

* **geral:** change modal confirm payment ([72eb5c3](https://gitlab.com/fi-sas/kiosk-frontend/commit/72eb5c3b69cf826fdd508fb2e8f10eae26be28c7))

# [0.23.0-rc.62](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.61...v0.23.0-rc.62) (2021-11-10)


### Bug Fixes

* **news:** news validation ([74b4c82](https://gitlab.com/fi-sas/kiosk-frontend/commit/74b4c82c292ac98ffcb095ccec56b55df824d2df))

# [0.23.0-rc.61](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.60...v0.23.0-rc.61) (2021-11-10)


### Bug Fixes

* **bar:** style button ([5a876f1](https://gitlab.com/fi-sas/kiosk-frontend/commit/5a876f1a12eaf9a490ada9d5982f32d67ec2fd45))

# [0.23.0-rc.60](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.59...v0.23.0-rc.60) (2021-11-03)


### Bug Fixes

* **canteen:** change date ([26515ff](https://gitlab.com/fi-sas/kiosk-frontend/commit/26515ff8e9888339a070dcd3658761e8d75190e3))

# [0.23.0-rc.59](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.58...v0.23.0-rc.59) (2021-11-03)


### Bug Fixes

* **cart:** text change modal ([c5d66f9](https://gitlab.com/fi-sas/kiosk-frontend/commit/c5d66f97ac5479959d7d52de469c99d3db3e0bac))

# [0.23.0-rc.58](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.57...v0.23.0-rc.58) (2021-11-02)


### Bug Fixes

* **geral:** text change ([5e1899a](https://gitlab.com/fi-sas/kiosk-frontend/commit/5e1899a9f2a211c7a0755b4adf145b38227af84a))

# [0.23.0-rc.57](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.56...v0.23.0-rc.57) (2021-11-02)


### Bug Fixes

* **cart:** change layout purchase + modal confirm ([a653948](https://gitlab.com/fi-sas/kiosk-frontend/commit/a6539484d1e8acee343de0cdc63f8491695b3c9e))

# [0.23.0-rc.56](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.55...v0.23.0-rc.56) (2021-10-27)


### Bug Fixes

* **mobility:** correction error search ([1c98716](https://gitlab.com/fi-sas/kiosk-frontend/commit/1c98716e6dc87989b8ae2b9506dbd747f0fb846a))

# [0.23.0-rc.55](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.54...v0.23.0-rc.55) (2021-10-27)


### Bug Fixes

* **mobility:** format date ([2fae9a4](https://gitlab.com/fi-sas/kiosk-frontend/commit/2fae9a4d530c97bebdfe582067c1b84c5e96a10e))

# [0.23.0-rc.54](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.53...v0.23.0-rc.54) (2021-10-26)


### Bug Fixes

* **mobility:** correction error ([c75efde](https://gitlab.com/fi-sas/kiosk-frontend/commit/c75efdeee9e513e38a60caa1c171e5e62cde37d6))

# [0.23.0-rc.53](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.52...v0.23.0-rc.53) (2021-10-26)


### Bug Fixes

* **mobility:** validation duration ([3f1a2b8](https://gitlab.com/fi-sas/kiosk-frontend/commit/3f1a2b8a11eefae2fb80b54f680ba44273456dcc))

# [0.23.0-rc.52](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.51...v0.23.0-rc.52) (2021-10-25)


### Bug Fixes

* **geral:** change buy text ([a3ed33f](https://gitlab.com/fi-sas/kiosk-frontend/commit/a3ed33fa8430986ff895935d595e4550d6b3dd0b))

# [0.23.0-rc.51](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.50...v0.23.0-rc.51) (2021-10-15)


### Bug Fixes

* **charging:** change btn back, text alert email ([fc77e41](https://gitlab.com/fi-sas/kiosk-frontend/commit/fc77e4150f8f0a1bee28478a35be84bbecabf0b4))
* **dashboard:** modal not logged in to services ([b1d03cf](https://gitlab.com/fi-sas/kiosk-frontend/commit/b1d03cf703b000f6c662b244d79f582ef0da22e8))
* **menu:** delete button information ([4625494](https://gitlab.com/fi-sas/kiosk-frontend/commit/4625494c5c5eebcb018abe9cd7f98241a7d49090))
* **mobility:** change button ([0b5a1aa](https://gitlab.com/fi-sas/kiosk-frontend/commit/0b5a1aa6eb8b222a8d978ad981ffb450229c1330))
* **mobility:** correction of errors in fields ([b8a3084](https://gitlab.com/fi-sas/kiosk-frontend/commit/b8a308460868ea362a04aa7a0c3a50bf74a340b5))
* **mobility:** local search ([437aeda](https://gitlab.com/fi-sas/kiosk-frontend/commit/437aeda0ea3c144c968eeaff647a4032ea152ece))
* **mobility:** remove search ([69153ef](https://gitlab.com/fi-sas/kiosk-frontend/commit/69153eff29c4e4783776f3718de4f305155512af))
* **mobility:** scroll, change button save info ([072d1a2](https://gitlab.com/fi-sas/kiosk-frontend/commit/072d1a2a4ce95653e0c285e817b22cb4632a480c))

# [0.23.0-rc.50](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.49...v0.23.0-rc.50) (2021-10-14)


### Features

* **auth:** auto login after 4 digits on pin modal ([f0f5a33](https://gitlab.com/fi-sas/kiosk-frontend/commit/f0f5a3376b5ca9fb621d516277f22004d56852d2))

# [0.23.0-rc.49](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.48...v0.23.0-rc.49) (2021-10-13)


### Bug Fixes

* **canteen:** increase letter selected period ([5cc1b9b](https://gitlab.com/fi-sas/kiosk-frontend/commit/5cc1b9b5182170bd8b9c805a597bf1dadd330f7a))

# [0.23.0-rc.48](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.47...v0.23.0-rc.48) (2021-10-13)


### Bug Fixes

* **cart:** modal payment ([0c4d3c6](https://gitlab.com/fi-sas/kiosk-frontend/commit/0c4d3c600f1bbb0f036cf1ed075e7dbff8f2d1ef))

# [0.23.0-rc.47](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.46...v0.23.0-rc.47) (2021-10-01)


### Bug Fixes

* **payments:** valid number fixed 2 ([5a48d63](https://gitlab.com/fi-sas/kiosk-frontend/commit/5a48d639bd6912d1302db07b8a218b729b01b2f7))

# [0.23.0-rc.46](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.45...v0.23.0-rc.46) (2021-10-01)


### Bug Fixes

* **alimentation:** message when buying no login ([57374f1](https://gitlab.com/fi-sas/kiosk-frontend/commit/57374f1074f0118dd163f3cbd05bec07bb3a288a))

# [0.23.0-rc.45](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.44...v0.23.0-rc.45) (2021-09-30)


### Bug Fixes

* **shopping-cart:** alert message refectory ([13a9dbc](https://gitlab.com/fi-sas/kiosk-frontend/commit/13a9dbca90f268ece0484307724524b6a11c0e4c))

# [0.23.0-rc.44](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.43...v0.23.0-rc.44) (2021-09-30)


### Bug Fixes

* **shopping-cart:** loading page payment ([bcf1c5c](https://gitlab.com/fi-sas/kiosk-frontend/commit/bcf1c5c45320d5d5af8ac4b4248372edb891a27a))

# [0.23.0-rc.43](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.42...v0.23.0-rc.43) (2021-09-29)


### Bug Fixes

* **payment:** correction error payments cart ([8d33f8e](https://gitlab.com/fi-sas/kiosk-frontend/commit/8d33f8e3bd1423ed98d20b16e05666a13df0bffe))

# [0.23.0-rc.42](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.41...v0.23.0-rc.42) (2021-09-25)


### Bug Fixes

* **bar:** families translations ([0c7a682](https://gitlab.com/fi-sas/kiosk-frontend/commit/0c7a682edbf4598260596ccc37eab7b7955f7a24))

# [0.23.0-rc.41](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.40...v0.23.0-rc.41) (2021-09-20)


### Bug Fixes

* **keyboard:** add  action to disable keyboard after lose focus ([ee25da1](https://gitlab.com/fi-sas/kiosk-frontend/commit/ee25da1af3771385f6f685ecc832731b09af12cc))


### Features

* **pospayment:** add fail screen on tpa error ([9970026](https://gitlab.com/fi-sas/kiosk-frontend/commit/9970026e4c7a9909386e605923803ebe26df5e90))

# [0.23.0-rc.40](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.39...v0.23.0-rc.40) (2021-09-17)


### Bug Fixes

* **alimentation:** stock style ([09f4027](https://gitlab.com/fi-sas/kiosk-frontend/commit/09f40277dbc6abce91c4bb887df527ff8943801d))

# [0.23.0-rc.39](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.38...v0.23.0-rc.39) (2021-09-17)


### Bug Fixes

* **geral:** sound videos ([99273fb](https://gitlab.com/fi-sas/kiosk-frontend/commit/99273fbcdd80e1449c9bb9c28868776f0025c8c4))

# [0.23.0-rc.38](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.37...v0.23.0-rc.38) (2021-09-17)


### Bug Fixes

* **geral:** x keyboard, config time, downtime prob ([03744f2](https://gitlab.com/fi-sas/kiosk-frontend/commit/03744f20deed4d5ab04099df55d41517fadc7226))

# [0.23.0-rc.37](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.36...v0.23.0-rc.37) (2021-09-16)


### Bug Fixes

* **install:** remove consoles ([73fb6ff](https://gitlab.com/fi-sas/kiosk-frontend/commit/73fb6ff4911c252a990b8199526a8e192b6fae12))

# [0.23.0-rc.36](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.35...v0.23.0-rc.36) (2021-09-15)


### Bug Fixes

* **install:** change title ([e54d5f3](https://gitlab.com/fi-sas/kiosk-frontend/commit/e54d5f310594ec350cc77a0d7e29d1fdd2245166))

# [0.23.0-rc.35](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.34...v0.23.0-rc.35) (2021-09-13)


### Bug Fixes

* **install:** news configuration ([b7898ab](https://gitlab.com/fi-sas/kiosk-frontend/commit/b7898ab5261843b10e599f055a74174febe30ac9))

# [0.23.0-rc.34](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.33...v0.23.0-rc.34) (2021-09-13)


### Bug Fixes

* **auth:** fox token refresh ([833a79a](https://gitlab.com/fi-sas/kiosk-frontend/commit/833a79a3b60bed299df6ccd9efdad8894679848d))

# [0.23.0-rc.33](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.32...v0.23.0-rc.33) (2021-09-10)


### Features

* **bar:** add recents products ([8094274](https://gitlab.com/fi-sas/kiosk-frontend/commit/80942743a612d97d886ff710646725bffde3abb6))

# [0.23.0-rc.32](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.31...v0.23.0-rc.32) (2021-09-09)


### Features

* **cart:** add cancel payment page ([f22f827](https://gitlab.com/fi-sas/kiosk-frontend/commit/f22f827c47627a02ca9275b8fa6a20bfba3fe0db))

# [0.23.0-rc.31](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.30...v0.23.0-rc.31) (2021-09-08)


### Bug Fixes

* **cart:** block pay button when paying ongoing ([25635fc](https://gitlab.com/fi-sas/kiosk-frontend/commit/25635fc2194c2f20624d28c322ec06f870b25ec4))
* **pos:** remove alert ([6456436](https://gitlab.com/fi-sas/kiosk-frontend/commit/6456436f3584fca057d29ae16f69806015f8f5b1))
* **rfid:** remove debug rfid read ([3143d36](https://gitlab.com/fi-sas/kiosk-frontend/commit/3143d360cfefb5822e5115fa3a3ef99e1ddc675c))


### Features

* **charging:** check if tpa is available ([b8885aa](https://gitlab.com/fi-sas/kiosk-frontend/commit/b8885aadc1648ce83579d1a97d5ec913542a736a))

# [0.23.0-rc.30](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.29...v0.23.0-rc.30) (2021-09-08)


### Bug Fixes

* **install:** alphabetical order list ([29a2efb](https://gitlab.com/fi-sas/kiosk-frontend/commit/29a2efbdc37f4f44c37973dec6dcf4062ca9f1cf))

# [0.23.0-rc.29](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.28...v0.23.0-rc.29) (2021-09-06)


### Bug Fixes

* **rfid:** change modal rfid login ([2f0f1a4](https://gitlab.com/fi-sas/kiosk-frontend/commit/2f0f1a4af8809762f1167caaef7d37a15db8da10))


### Features

* change pin modal layout ([62478a0](https://gitlab.com/fi-sas/kiosk-frontend/commit/62478a0fd1f9d46469777d27b5f07682d115a52f))

# [0.23.0-rc.28](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.27...v0.23.0-rc.28) (2021-09-06)


### Bug Fixes

* **geral:** themas ([ad76470](https://gitlab.com/fi-sas/kiosk-frontend/commit/ad7647049b8c91188b8a100b5643b2875e90d0b9))

# [0.23.0-rc.27](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.26...v0.23.0-rc.27) (2021-08-30)


### Bug Fixes

* **current_account:** item value correction ([8135668](https://gitlab.com/fi-sas/kiosk-frontend/commit/81356683387ec00b1a1c2df7aa625adee4ef3b9b))

# [0.23.0-rc.26](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.25...v0.23.0-rc.26) (2021-08-27)


### Bug Fixes

* **geral:** fix test errors ([16b2a8f](https://gitlab.com/fi-sas/kiosk-frontend/commit/16b2a8f56247369eeb0769d9a348e6d36dbab0c0))

# [0.23.0-rc.25](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.24...v0.23.0-rc.25) (2021-08-27)


### Bug Fixes

* **pospayment:** change confirmation to payment page ([7dc65af](https://gitlab.com/fi-sas/kiosk-frontend/commit/7dc65af01b60a5e3102143eea4bd5f9ac8879d8f))

# [0.23.0-rc.24](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.23...v0.23.0-rc.24) (2021-08-25)


### Features

* **rfid:** add send random pin option ([e4a70e8](https://gitlab.com/fi-sas/kiosk-frontend/commit/e4a70e830c31b52ee8d70f0106fd1661eb8ebe66))

# [0.23.0-rc.23](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.22...v0.23.0-rc.23) (2021-08-24)


### Bug Fixes

* **bar:** add product validation befora change quantity ([7084b7c](https://gitlab.com/fi-sas/kiosk-frontend/commit/7084b7c285e8d46a4d2fff0e5ae1e5efb920848b))

# [0.23.0-rc.22](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.21...v0.23.0-rc.22) (2021-08-23)


### Bug Fixes

* **current_account:** hour moviment ([fd0e6dc](https://gitlab.com/fi-sas/kiosk-frontend/commit/fd0e6dcbffc8089a3c27ee5ef7ca6ecf3d65df52))
* **current_account:** listing aspect correction ([4e7c330](https://gitlab.com/fi-sas/kiosk-frontend/commit/4e7c33058856b9234f9abe5e97e7349e575ccab2))
* minor fixs ([959a4ca](https://gitlab.com/fi-sas/kiosk-frontend/commit/959a4ca39a636a83f2fe94df4d35ef8ea24e998b))
* **bar:** fix cart ([1d7b3f7](https://gitlab.com/fi-sas/kiosk-frontend/commit/1d7b3f7839169bfe0e1e1ae37744b2590877a8c9))
* **bar:** redirect to dashboard in fail ([94d858d](https://gitlab.com/fi-sas/kiosk-frontend/commit/94d858db9c9edfade5616ff3153ed02e9eb975db))
* **charging:** add behavior by tag ([7a7fd4e](https://gitlab.com/fi-sas/kiosk-frontend/commit/7a7fd4e47440df3101866ec664c8f123139a0f40))
* **charging:** add translations and RefMB Text ([c8d7580](https://gitlab.com/fi-sas/kiosk-frontend/commit/c8d758062e5581c36c92d0f4c42c892095c67439))
* translation file ([c96084c](https://gitlab.com/fi-sas/kiosk-frontend/commit/c96084cc16b11d473486028864d5fa4f8165e60b))


### Features

* show refMB info ([0e868de](https://gitlab.com/fi-sas/kiosk-frontend/commit/0e868de4cc1af39dd83a55250c2712bce2efaaa3))
* **charging:** add charging features ([e9406de](https://gitlab.com/fi-sas/kiosk-frontend/commit/e9406de79201f613aefdba8fdb41dc40f09e4ecf))
* inital confirm payment ([b30c0c1](https://gitlab.com/fi-sas/kiosk-frontend/commit/b30c0c1d05a992c75e9455dd06d1a63d722e9cd9))
* **charging:** add charging page ([eea3cf6](https://gitlab.com/fi-sas/kiosk-frontend/commit/eea3cf622c95795ce6fcac5943afb638961b2c56))
* **install:** set cpuid on install ([13ea328](https://gitlab.com/fi-sas/kiosk-frontend/commit/13ea328e635e1ef2a67965ad30b924871cd9ef4f))
* **pospayment:** add status from TPa ([095f543](https://gitlab.com/fi-sas/kiosk-frontend/commit/095f543192f94568f609f253d6980c6ed1275fa7))
* **queue:** add printer not available translation ([ccdf52f](https://gitlab.com/fi-sas/kiosk-frontend/commit/ccdf52ffa077e2c31153a431cd88536c678c47fc))
* inital version of tpa charge ([f9f3466](https://gitlab.com/fi-sas/kiosk-frontend/commit/f9f3466d16533486243ccbc0c8fcf95f7573f6d9))

# [0.23.0-rc.21](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.20...v0.23.0-rc.21) (2021-07-26)


### Features

* **queue:** add printer available validation ([155115a](https://gitlab.com/fi-sas/kiosk-frontend/commit/155115a2df0e75da576c179466a28dc3323ca5e2))

# [0.23.0-rc.20](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.19...v0.23.0-rc.20) (2021-07-16)


### Bug Fixes

* **movements:** add order by to moviments ([bc3749f](https://gitlab.com/fi-sas/kiosk-frontend/commit/bc3749fc5403234b4906727cc2ff862cd910d939))


### Features

* **current-account:** view movements by account ([7915e80](https://gitlab.com/fi-sas/kiosk-frontend/commit/7915e805770db9230dd1918885855f8f558d7780))

# [0.23.0-rc.19](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.18...v0.23.0-rc.19) (2021-07-06)


### Features

* **queue:** send ticket to printer ([829cfdd](https://gitlab.com/fi-sas/kiosk-frontend/commit/829cfddd78d863230d4e4381e063428ff4ac467c))

# [0.23.0-rc.18](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.17...v0.23.0-rc.18) (2021-06-04)


### Bug Fixes

* change window creation proprieties ([bc150b6](https://gitlab.com/fi-sas/kiosk-frontend/commit/bc150b6d2d2f66eb5261956bbed6a15cc2921e02))


### Features

* **rfid:** RFID auth no ask for PIn of user ([8a277ad](https://gitlab.com/fi-sas/kiosk-frontend/commit/8a277adf3b0d4f430287bd57325902b27cd0862e))

# [0.23.0-rc.17](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.16...v0.23.0-rc.17) (2021-06-01)


### Bug Fixes

* **geral:** bar validation ([320dee6](https://gitlab.com/fi-sas/kiosk-frontend/commit/320dee661529a1331c08e1306a7120c28f8e8113))

# [0.23.0-rc.16](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.15...v0.23.0-rc.16) (2021-05-26)


### Bug Fixes

* **geral:** correction cart ([381f174](https://gitlab.com/fi-sas/kiosk-frontend/commit/381f174d6a434994259f319bd987caab2fa276aa))

# [0.23.0-rc.15](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.14...v0.23.0-rc.15) (2021-05-26)


### Bug Fixes

* **geral:** loading cart ([1768f3a](https://gitlab.com/fi-sas/kiosk-frontend/commit/1768f3a2b81570acb7ab0e510ae66ff0abc8f5cc))

# [0.23.0-rc.14](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.13...v0.23.0-rc.14) (2021-05-24)


### Bug Fixes

* **geral:** correction of document errors ([ddcdbf8](https://gitlab.com/fi-sas/kiosk-frontend/commit/ddcdbf8dc686d0f470e6a02e450894a867b398dd))

# [0.23.0-rc.13](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.12...v0.23.0-rc.13) (2021-04-15)


### Bug Fixes

* **bar, canteen:** shopping cart quantities ([18cbfd7](https://gitlab.com/fi-sas/kiosk-frontend/commit/18cbfd76fb4f9b31c96479eb78b2f80bc607620f))

# [0.23.0-rc.12](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.11...v0.23.0-rc.12) (2021-04-09)


### Bug Fixes

* **payments:** pay total ([ebc8705](https://gitlab.com/fi-sas/kiosk-frontend/commit/ebc8705728ff8d6f7b16664e938045a395aa025d))

# [0.23.0-rc.11](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.10...v0.23.0-rc.11) (2021-04-08)


### Bug Fixes

* **shopping-cart:** shopping cart implementation ([f1bd1f6](https://gitlab.com/fi-sas/kiosk-frontend/commit/f1bd1f6311e9ec3bf925dbb8bdffc0652b87a9ff))

# [0.23.0-rc.10](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.9...v0.23.0-rc.10) (2021-04-05)


### Bug Fixes

* **queue:** allow queuing configuration by device ([575f853](https://gitlab.com/fi-sas/kiosk-frontend/commit/575f853d40f79621c3beaa2d5e3192676e2f8179))

# [0.23.0-rc.9](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.8...v0.23.0-rc.9) (2021-03-16)


### Bug Fixes

* **geral:** correction error ([55349ed](https://gitlab.com/fi-sas/kiosk-frontend/commit/55349edc32290c2db4e12e535a14d0f3f5ac7688))

# [0.23.0-rc.8](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.7...v0.23.0-rc.8) (2021-03-16)


### Bug Fixes

* **queue:** change install, tradutions queue ([100637e](https://gitlab.com/fi-sas/kiosk-frontend/commit/100637e8ce849e06c6aeabf74b1373c6502ab1cc))

# [0.23.0-rc.7](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.6...v0.23.0-rc.7) (2021-03-11)


### Bug Fixes

* **alimentation:** correction error field MS, img ([c883d12](https://gitlab.com/fi-sas/kiosk-frontend/commit/c883d12e9704ade0325491fc72d4b74c42d4e462))

# [0.23.0-rc.6](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.5...v0.23.0-rc.6) (2021-03-03)


### Bug Fixes

* **refectory:** fix refectory build errors ([88375bf](https://gitlab.com/fi-sas/kiosk-frontend/commit/88375bf8a93dcd76a1ff846d9d2ab2b6f49aea96))

# [0.23.0-rc.5](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.4...v0.23.0-rc.5) (2021-03-03)


### Bug Fixes

* **auth:** fix refresh token process ([a0e8200](https://gitlab.com/fi-sas/kiosk-frontend/commit/a0e82005c820fae329f2b26e65577e6e7784e22e))

# [0.23.0-rc.4](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.3...v0.23.0-rc.4) (2021-03-02)


### Bug Fixes

* **media:** fix translation validation ([7ea579b](https://gitlab.com/fi-sas/kiosk-frontend/commit/7ea579b16001bf72590da95966560f3a3cd5a7d3))

# [0.23.0-rc.3](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.2...v0.23.0-rc.3) (2021-03-02)


### Bug Fixes

* remove unused files ([3a48d25](https://gitlab.com/fi-sas/kiosk-frontend/commit/3a48d255f33ed878406bd98c8c1d249ad2f2f550))

# [0.23.0-rc.2](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.23.0-rc.1...v0.23.0-rc.2) (2021-01-26)


### Bug Fixes

* **mobility:** change variables names ([93a3cdc](https://gitlab.com/fi-sas/kiosk-frontend/commit/93a3cdc1f449a75328629a528311860f31a9c7b4))

# [0.23.0-rc.1](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.22.18...v0.23.0-rc.1) (2020-12-30)


### Features

* update to new boilerplate ([baa1b2c](https://gitlab.com/fi-sas/kiosk-frontend/commit/baa1b2c72591907eba8ff819444793376fce2895))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.22.18](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.22.17...v0.22.18) (2020-11-25)


### Bug Fixes

* **socketio:** fix fisas-hub address ([aa7dae0](https://gitlab.com/fi-sas/kiosk-frontend/commit/aa7dae076329203a9fcc244ab438cd239f36285b))

### [0.22.17](https://gitlab.com/fi-sas/kiosk-frontend/compare/v0.22.16...v0.22.17) (2020-11-25)

### 0.22.16 (2020-09-15)


### Features

* **core:** unavailable in case of middleware off ([abba860](https://gitlab.com/fi-sas/kiosk-frontend/commit/abba86076aae7c56e52742ceb72293aa86f3cf96))
* **hub:** add initial hub config ([dc0b33a](https://gitlab.com/fi-sas/kiosk-frontend/commit/dc0b33ac89bac873febc1237b767de93d75459fa))


### Bug Fixes

* **directives:** replace the scroll directive ([16657a7](https://gitlab.com/fi-sas/kiosk-frontend/commit/16657a77ff64976c4728de7db47c0df7622d8617))

<a name="0.22.15"></a>
## [0.22.15](https://gitlab.com/fi-sas/frontend/compare/v0.22.14...v0.22.15) (2019-02-11)


### Bug Fixes

* **install:** environment variables ([ca6b378](https://gitlab.com/fi-sas/frontend/commit/ca6b378))



<a name="0.22.14"></a>
## [0.22.14](https://gitlab.com/fi-sas/frontend/compare/v0.22.13...v0.22.14) (2019-02-11)


### Bug Fixes

* **install:** implement lifecycle hook interface on destroy ([d1b0cef](https://gitlab.com/fi-sas/frontend/commit/d1b0cef))



<a name="0.22.13"></a>
## [0.22.13](https://gitlab.com/fi-sas/frontend/compare/v0.22.12...v0.22.13) (2019-02-11)


### Bug Fixes

* **menu:** disable menu in installation ([31ae6a4](https://gitlab.com/fi-sas/frontend/commit/31ae6a4))



<a name="0.22.12"></a>
## [0.22.12](https://gitlab.com/fi-sas/frontend/compare/v0.21.12...v0.22.12) (2019-02-11)



<a name="0.21.12"></a>
## [0.21.12](https://gitlab.com/fi-sas/frontend/compare/v0.20.12...v0.21.12) (2019-02-08)



<a name="0.20.12"></a>
## [0.20.12](https://gitlab.com/fi-sas/frontend/compare/v0.19.12...v0.20.12) (2019-02-08)


### Bug Fixes

* **release:** operator '===' cannot be applied to types ([52d9dd7](https://gitlab.com/fi-sas/frontend/commit/52d9dd7))



<a name="0.19.12"></a>
## [0.19.12](https://gitlab.com/fi-sas/frontend/compare/v0.18.12...v0.19.12) (2019-02-08)


### Bug Fixes

* **movement:** operator '===' cannot be applied to types ([61c7102](https://gitlab.com/fi-sas/frontend/commit/61c7102))



<a name="0.18.12"></a>
## [0.18.12](https://gitlab.com/fi-sas/frontend/compare/v0.17.12...v0.18.12) (2019-02-08)



<a name="0.17.12"></a>
## [0.17.12](https://gitlab.com/fi-sas/frontend/compare/v0.16.12...v0.17.12) (2019-02-08)


### Bug Fixes

* **movement:** type number trivially inferred from a number literal ([8f8ba16](https://gitlab.com/fi-sas/frontend/commit/8f8ba16))
* **movementd:** shadowed name data ([c6c6a61](https://gitlab.com/fi-sas/frontend/commit/c6c6a61))


### Features

* **current-account:** empty module ([a8249ad](https://gitlab.com/fi-sas/frontend/commit/a8249ad))
* **dashboard:** added current account to dashboard ([43a2d00](https://gitlab.com/fi-sas/frontend/commit/43a2d00))
* **meals:** product id must be the code field ([360c2e6](https://gitlab.com/fi-sas/frontend/commit/360c2e6))
* **movements:** current account activity without pagination and icons ([7e7ede0](https://gitlab.com/fi-sas/frontend/commit/7e7ede0))
* **movements:** with pagination, icons and colors in prices ([815d66c](https://gitlab.com/fi-sas/frontend/commit/815d66c))



<a name="0.16.12"></a>
## [0.16.12](https://gitlab.com/fi-sas/frontend/compare/v0.15.12...v0.16.12) (2019-02-01)


### Bug Fixes

* **bundle:** windows bundle ([32defd5](https://gitlab.com/fi-sas/frontend/commit/32defd5))



<a name="0.15.12"></a>
## [0.15.12](https://gitlab.com/fi-sas/frontend/compare/v0.14.12...v0.15.12) (2019-02-01)


### Features

* **install:** add new micro service in install ([b919bee](https://gitlab.com/fi-sas/frontend/commit/b919bee))



<a name="0.14.12"></a>
## [0.14.12](https://gitlab.com/fi-sas/frontend/compare/v0.13.11...v0.14.12) (2019-02-01)


### Bug Fixes

* **electron:** slice can not be used in ngFor directives of templates ([e8a6f19](https://gitlab.com/fi-sas/frontend/commit/e8a6f19))



<a name="0.13.11"></a>
## [0.13.11](https://gitlab.com/fi-sas/frontend/compare/v0.12.11...v0.13.11) (2019-02-01)


### Bug Fixes

* **backoffice:** angular.json and nx.json are out of sync ([8a87e63](https://gitlab.com/fi-sas/frontend/commit/8a87e63))
* **balances:** adjustments for overflow ([92b6e56](https://gitlab.com/fi-sas/frontend/commit/92b6e56))
* **balances:** no data in balances ([b8c425f](https://gitlab.com/fi-sas/frontend/commit/b8c425f))
* **balances:** shadowed name res ([643f3ef](https://gitlab.com/fi-sas/frontend/commit/643f3ef))
* **cart:** issufficient balance and clear cart ([dce2822](https://gitlab.com/fi-sas/frontend/commit/dce2822))
* **food:** disable for now the bar ([569c778](https://gitlab.com/fi-sas/frontend/commit/569c778))
* **food:** type string trivially inferred from a string literal ([84b8fd2](https://gitlab.com/fi-sas/frontend/commit/84b8fd2))
* **mobility:** shadowed name ([dcaf6a9](https://gitlab.com/fi-sas/frontend/commit/dcaf6a9))
* **product:** product detail interface ([e6f6a08](https://gitlab.com/fi-sas/frontend/commit/e6f6a08))
* **route:** add destroy ([537a8aa](https://gitlab.com/fi-sas/frontend/commit/537a8aa))
* **route:** missing implements ([d641e73](https://gitlab.com/fi-sas/frontend/commit/d641e73))


### Features

* **backoffice:** removed backoffice from frontend repository ([eea4931](https://gitlab.com/fi-sas/frontend/commit/eea4931))
* **balance:** current account balance name ([1f8aca7](https://gitlab.com/fi-sas/frontend/commit/1f8aca7))
* **balance:** update balance when cart checkout finishes ([d0f10de](https://gitlab.com/fi-sas/frontend/commit/d0f10de))
* **balances:** balances at logon without name ([50481a3](https://gitlab.com/fi-sas/frontend/commit/50481a3))
* **cart:** adjust mobility module cart list ([2093fa3](https://gitlab.com/fi-sas/frontend/commit/2093fa3))
* **cart:** cart list pay button balance ([73c3f2e](https://gitlab.com/fi-sas/frontend/commit/73c3f2e))
* **cart:** cart meals list ([ee82f9d](https://gitlab.com/fi-sas/frontend/commit/ee82f9d))
* **cart:** checkout multiple blocks and accounts ([2238f2a](https://gitlab.com/fi-sas/frontend/commit/2238f2a))
* **cart:** checkout one module ([a69fc30](https://gitlab.com/fi-sas/frontend/commit/a69fc30))
* **cart:** clear cart meals products ([d13bcf1](https://gitlab.com/fi-sas/frontend/commit/d13bcf1))
* **cart:** disable module pay button ([aa933ab](https://gitlab.com/fi-sas/frontend/commit/aa933ab))
* **cart:** empty cart and tax total ([db92b60](https://gitlab.com/fi-sas/frontend/commit/db92b60))
* **cart:** list all products by module ([74fd984](https://gitlab.com/fi-sas/frontend/commit/74fd984))
* **cart:** list cart by current account ([2222253](https://gitlab.com/fi-sas/frontend/commit/2222253))
* **cart:** mobility params ([85b1b66](https://gitlab.com/fi-sas/frontend/commit/85b1b66))
* **cart:** passing correct parameters for title and description ([c09e9bf](https://gitlab.com/fi-sas/frontend/commit/c09e9bf))
* **cart:** pay buttons added for refectory ([2b63906](https://gitlab.com/fi-sas/frontend/commit/2b63906))
* **cart:** save cart by account module ([ba511ff](https://gitlab.com/fi-sas/frontend/commit/ba511ff))
* **cart:** sequential checkout ([1e1a6ca](https://gitlab.com/fi-sas/frontend/commit/1e1a6ca))
* **cart:** warning empty cart on authentication ([b8a8950](https://gitlab.com/fi-sas/frontend/commit/b8a8950))
* **cart-summary:** cart summary in services and account in cart ([12b467e](https://gitlab.com/fi-sas/frontend/commit/12b467e))
* **environment:** public api access environment applied ([d391eef](https://gitlab.com/fi-sas/frontend/commit/d391eef))
* **funds:** controlling insufficient user funds ([6eb14ec](https://gitlab.com/fi-sas/frontend/commit/6eb14ec))
* **loader:** waiting loader for cart checkout ([c4a3b75](https://gitlab.com/fi-sas/frontend/commit/c4a3b75))
* **mobility:** bind mobility product missing fields with cart ([8fe7d72](https://gitlab.com/fi-sas/frontend/commit/8fe7d72))
* **modal:** modal text translations ([7fbefe8](https://gitlab.com/fi-sas/frontend/commit/7fbefe8))
* **notifications:** notifications for unavailable functionality ([5fb3efd](https://gitlab.com/fi-sas/frontend/commit/5fb3efd))



<a name="0.12.11"></a>
## [0.12.11](https://gitlab.com/fi-sas/frontend/compare/v0.11.11...v0.12.11) (2019-01-03)


### Bug Fixes

* **alimentation:** integration of taxes on alimentation ([5170b48](https://gitlab.com/fi-sas/frontend/commit/5170b48))
* **app_config:** add new endpoints ([25caebb](https://gitlab.com/fi-sas/frontend/commit/25caebb))
* **bus_config:** add new routes ([a76103c](https://gitlab.com/fi-sas/frontend/commit/a76103c))
* **communication:** add categories to feeds form ([38e5d5b](https://gitlab.com/fi-sas/frontend/commit/38e5d5b))
* **communication:** fix list posts relatoins ([20d4e7a](https://gitlab.com/fi-sas/frontend/commit/20d4e7a))
* **config_files:** update ([5519b93](https://gitlab.com/fi-sas/frontend/commit/5519b93))
* **configurations:** add messages on forms submit ([004815a](https://gitlab.com/fi-sas/frontend/commit/004815a))
* **envoirmento:** change links of microservices to gateway ([ae2dec5](https://gitlab.com/fi-sas/frontend/commit/ae2dec5))
* **form_routes:** add external input ([2610fea](https://gitlab.com/fi-sas/frontend/commit/2610fea))
* **form_zone:** fix label name ([2ca3683](https://gitlab.com/fi-sas/frontend/commit/2ca3683))
* **links:** fix input size ([da7b90e](https://gitlab.com/fi-sas/frontend/commit/da7b90e))
* **list_prices_tables:** add page for empty ([c032cef](https://gitlab.com/fi-sas/frontend/commit/c032cef))
* **list_routes:** add option in menu ([145f9d1](https://gitlab.com/fi-sas/frontend/commit/145f9d1))
* **list_timetables:** update hour ([a98dc85](https://gitlab.com/fi-sas/frontend/commit/a98dc85))
* **list_zones:** fix html tags ([e66fa33](https://gitlab.com/fi-sas/frontend/commit/e66fa33))
* **meals:** prefix for routes and fixed day 2018-11-26 for testing ([0ea29a3](https://gitlab.com/fi-sas/frontend/commit/0ea29a3))
* **models:** fix models ([a5d211e](https://gitlab.com/fi-sas/frontend/commit/a5d211e))
* **package_json:** fix package-lock ([e13cff2](https://gitlab.com/fi-sas/frontend/commit/e13cff2))
* **routes:** add new menu options ([040e994](https://gitlab.com/fi-sas/frontend/commit/040e994))
* **share:** fix error handlers and api error interceptor ([4754ac2](https://gitlab.com/fi-sas/frontend/commit/4754ac2))
* **shared:** fix permission directive ([da0d2f8](https://gitlab.com/fi-sas/frontend/commit/da0d2f8))
* **shared:** fix truncate pipe error without args ([576971e](https://gitlab.com/fi-sas/frontend/commit/576971e))
* **style:** fix side menu style ([bab3774](https://gitlab.com/fi-sas/frontend/commit/bab3774))
* **timetable_service:** fix service ([fcbd8ab](https://gitlab.com/fi-sas/frontend/commit/fcbd8ab))
* **types_days:** change name the type day ([098a90c](https://gitlab.com/fi-sas/frontend/commit/098a90c))
* **typos:** remove typos from messages ([e55d755](https://gitlab.com/fi-sas/frontend/commit/e55d755))
* **users:** add profiles to user form ([11703da](https://gitlab.com/fi-sas/frontend/commit/11703da))
* **users:** fix users groups and scopes loading ([95f9ebb](https://gitlab.com/fi-sas/frontend/commit/95f9ebb))


### Features

* **alimentation:** add dish list and dish form pages ([2685314](https://gitlab.com/fi-sas/frontend/commit/2685314))
* **alimentation:** add list recipes page ([22dfd36](https://gitlab.com/fi-sas/frontend/commit/22dfd36))
* **alimentation:** creation of dish types form and list pages ([5fefe94](https://gitlab.com/fi-sas/frontend/commit/5fefe94))
* **backoffice:** add loader for slow networks ([48071b1](https://gitlab.com/fi-sas/frontend/commit/48071b1))
* **backoffice:** add unauthorized page ([ce401a1](https://gitlab.com/fi-sas/frontend/commit/ce401a1))
* **configuration:** creation of page list and form for taxes ([a9d85c3](https://gitlab.com/fi-sas/frontend/commit/a9d85c3))
* **list_links:** list links of route ([7a294d1](https://gitlab.com/fi-sas/frontend/commit/7a294d1))
* **share:** add global error interceptor ([e727f40](https://gitlab.com/fi-sas/frontend/commit/e727f40))
* **share:** add no connection modal ([8ae8f21](https://gitlab.com/fi-sas/frontend/commit/8ae8f21))
* **ticket_config:** create ticket config ([35a1461](https://gitlab.com/fi-sas/frontend/commit/35a1461))
* **tickets_boughts:**  list tickets ([b641cec](https://gitlab.com/fi-sas/frontend/commit/b641cec))
* **tickets_configs:** add service ([6a0cf80](https://gitlab.com/fi-sas/frontend/commit/6a0cf80))
* **users:** add list profiles and form profiles ([879e74b](https://gitlab.com/fi-sas/frontend/commit/879e74b))



<a name="0.11.11"></a>
## [0.11.11](https://gitlab.com/fi-sas/frontend/compare/v0.11.10...v0.11.11) (2018-12-14)


### Bug Fixes

* **mobility:** more timeout ([efdda67](https://gitlab.com/fi-sas/frontend/commit/efdda67))



<a name="0.11.10"></a>
## [0.11.10](https://gitlab.com/fi-sas/frontend/compare/v0.10.10...v0.11.10) (2018-12-14)


### Bug Fixes

* **mobility:** change fields names ([dfc39ce](https://gitlab.com/fi-sas/frontend/commit/dfc39ce))



<a name="0.10.10"></a>
## [0.10.10](https://gitlab.com/fi-sas/frontend/compare/v0.10.9...v0.10.10) (2018-12-14)


### Bug Fixes

* **mobility:** remove replce from template ([7116702](https://gitlab.com/fi-sas/frontend/commit/7116702))



<a name="0.10.9"></a>
## [0.10.9](https://gitlab.com/fi-sas/frontend/compare/v0.9.9...v0.10.9) (2018-12-14)


### Bug Fixes

* **interceptor:** wrong return value ([eb6d842](https://gitlab.com/fi-sas/frontend/commit/eb6d842))
* **meals:** dish type is no more necessary ([fd8a625](https://gitlab.com/fi-sas/frontend/commit/fd8a625))
* **mobility:** changed model ([fb1d1cd](https://gitlab.com/fi-sas/frontend/commit/fb1d1cd))
* **mobility:** fix some bugs after testing ([8c8accf](https://gitlab.com/fi-sas/frontend/commit/8c8accf))
* **mobility:** wrong field name ([4d97349](https://gitlab.com/fi-sas/frontend/commit/4d97349))
* **pipe:** on init missing ([0dc0301](https://gitlab.com/fi-sas/frontend/commit/0dc0301))
* **pipeline:** fix pipeline fail ([e4d978e](https://gitlab.com/fi-sas/frontend/commit/e4d978e))


### Features

* **cart-list:** cart list total and translations ([968b006](https://gitlab.com/fi-sas/frontend/commit/968b006))
* **cart-list:** translations for cart scene ([5475070](https://gitlab.com/fi-sas/frontend/commit/5475070))
* **mobility:** add ticket to cart has a product ([df70abd](https://gitlab.com/fi-sas/frontend/commit/df70abd))
* **mobility:** mobility detail scene with steps ([f5bbb45](https://gitlab.com/fi-sas/frontend/commit/f5bbb45))
* **mobility:** navigate to route detail ([35dc835](https://gitlab.com/fi-sas/frontend/commit/35dc835))
* **mobility:** route detail summary and information ([4358d03](https://gitlab.com/fi-sas/frontend/commit/4358d03))
* **route-detail:** add title to route detail ([a18494f](https://gitlab.com/fi-sas/frontend/commit/a18494f))



<a name="0.9.9"></a>
## [0.9.9](https://gitlab.com/fi-sas/frontend/compare/v0.8.9...v0.9.9) (2018-12-10)


### Bug Fixes

* **alimentation:** fix product update errors ([669c497](https://gitlab.com/fi-sas/frontend/commit/669c497))
* **alimentation:** remove list and create menus ([e950a78](https://gitlab.com/fi-sas/frontend/commit/e950a78))
* **css:** css uniformisation ([11c2b35](https://gitlab.com/fi-sas/frontend/commit/11c2b35))
* **css:** remove theming files from authentication ([8b9ce6c](https://gitlab.com/fi-sas/frontend/commit/8b9ce6c))
* **css:** remove theming files from dashboard and pipe problems ([4008b10](https://gitlab.com/fi-sas/frontend/commit/4008b10))
* **css:** remove theming files from header ([af68be5](https://gitlab.com/fi-sas/frontend/commit/af68be5))
* **css:** remove theming files from keyboard ([63d1f09](https://gitlab.com/fi-sas/frontend/commit/63d1f09))
* **css:** remove theming files from main menu ([8d5e6e2](https://gitlab.com/fi-sas/frontend/commit/8d5e6e2))
* **css:** remove theming files from ticker ([d20afce](https://gitlab.com/fi-sas/frontend/commit/d20afce))
* **css:** remove theming files from unavailable ([df8ee2a](https://gitlab.com/fi-sas/frontend/commit/df8ee2a))
* **css:** removed theming files from footer ([79c8e66](https://gitlab.com/fi-sas/frontend/commit/79c8e66))
* **fifteen-day:** type declaration ([33eaef7](https://gitlab.com/fi-sas/frontend/commit/33eaef7))
* **progress-bar:** progress bar position ([6385270](https://gitlab.com/fi-sas/frontend/commit/6385270))
* **service:** pipe error ([2a0319a](https://gitlab.com/fi-sas/frontend/commit/2a0319a))
* **services:** no default school ([87543e3](https://gitlab.com/fi-sas/frontend/commit/87543e3))
* **shared:** fix token interceptor overlap headers ([f809116](https://gitlab.com/fi-sas/frontend/commit/f809116))


### Features

* **alimentation:** creation of components and routing ([8f4f5b0](https://gitlab.com/fi-sas/frontend/commit/8f4f5b0))
* **alimentation:** creation of menus list and form ([944504c](https://gitlab.com/fi-sas/frontend/commit/944504c))
* **mobility:** add ticket to cart ([c2a2bd7](https://gitlab.com/fi-sas/frontend/commit/c2a2bd7))
* **mobility:** list routes screen new design ([4e06c5f](https://gitlab.com/fi-sas/frontend/commit/4e06c5f))
* **mobility:** places design ([e9559db](https://gitlab.com/fi-sas/frontend/commit/e9559db))
* **mobility:** search place freely ([edaf1c3](https://gitlab.com/fi-sas/frontend/commit/edaf1c3))
* **timer:** refresh videos and media after 30 min ([bc42eb5](https://gitlab.com/fi-sas/frontend/commit/bc42eb5))



<a name="0.8.9"></a>
## [0.8.9](https://gitlab.com/fi-sas/frontend/compare/v0.8.8...v0.8.9) (2018-11-30)


### Bug Fixes

* **timer:** enable timer for screen saver ([28c8758](https://gitlab.com/fi-sas/frontend/commit/28c8758))



<a name="0.8.8"></a>
## [0.8.8](https://gitlab.com/fi-sas/frontend/compare/v0.8.7...v0.8.8) (2018-11-30)


### Bug Fixes

* **conflicts:** resolve conflicts ([35c6b56](https://gitlab.com/fi-sas/frontend/commit/35c6b56))
* **install:** add missing services address ([e750bac](https://gitlab.com/fi-sas/frontend/commit/e750bac))



<a name="0.8.6"></a>
## [0.8.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.6) (2018-11-30)


### Bug Fixes

* **meals:** added fixed date to have data ([c34bef7](https://gitlab.com/fi-sas/frontend/commit/c34bef7))



<a name="0.8.7"></a>
## [0.8.7](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.7) (2018-11-30)

### Bug Fixes

* **environment:** missing entry for mobility configuration ([079d51f](https://gitlab.com/fi-sas/frontend/commit/079d51f))

<a name="0.8.6"></a>
## [0.8.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.6...v0.8.6) (2018-11-30)


### Bug Fixes

* **car:** product interface ([b0514c2](https://gitlab.com/fi-sas/frontend/commit/b0514c2))
* **communication:** fix ration of 9:16 image ([158f49e](https://gitlab.com/fi-sas/frontend/commit/158f49e))
* **meals:** added fixed date to have data ([c34bef7](https://gitlab.com/fi-sas/frontend/commit/c34bef7))
* **meals:** fix declaration ([ca6236d](https://gitlab.com/fi-sas/frontend/commit/ca6236d))
* **meals:** removed fixed date ([d39394e](https://gitlab.com/fi-sas/frontend/commit/d39394e))
* **merge:** config var ([1fd08e5](https://gitlab.com/fi-sas/frontend/commit/1fd08e5))
* **merge:** resolve conflicts ([8ded2c4](https://gitlab.com/fi-sas/frontend/commit/8ded2c4))
* **merge:** resolve conflicts ([527995f](https://gitlab.com/fi-sas/frontend/commit/527995f))
* **title:** title input ([087fdc1](https://gitlab.com/fi-sas/frontend/commit/087fdc1))


### Features

* **alimentation:** creation of complement form and list page ([6deab50](https://gitlab.com/fi-sas/frontend/commit/6deab50))
* **alimentation:** form products updated ([46376cb](https://gitlab.com/fi-sas/frontend/commit/46376cb))
* **alimentation:** partial creation of products form ([80186cc](https://gitlab.com/fi-sas/frontend/commit/80186cc))
* **cart:** add products to the top of the stack ([24bc66c](https://gitlab.com/fi-sas/frontend/commit/24bc66c))
* **cart:** add to cart in meal detail ([ae088d1](https://gitlab.com/fi-sas/frontend/commit/ae088d1))
* **cart:** add to cart in meals product ([0023e76](https://gitlab.com/fi-sas/frontend/commit/0023e76))
* **interceptor:** implemented http interceptor ([49e12d2](https://gitlab.com/fi-sas/frontend/commit/49e12d2))
* **loading:** loader in navigation ([51ebcbc](https://gitlab.com/fi-sas/frontend/commit/51ebcbc))
* **meal:** detail meal ([41a2fa6](https://gitlab.com/fi-sas/frontend/commit/41a2fa6))
* **meal:** meal detail ([436c00f](https://gitlab.com/fi-sas/frontend/commit/436c00f))
* **meals:** change categories language, set default school and service ([4f1b368](https://gitlab.com/fi-sas/frontend/commit/4f1b368))
* **meals:** connect with families service ([578942d](https://gitlab.com/fi-sas/frontend/commit/578942d))
* **meals:** list available meals ([18c10c6](https://gitlab.com/fi-sas/frontend/commit/18c10c6))
* **meals:** set meals component variables for service call ([93c4a14](https://gitlab.com/fi-sas/frontend/commit/93c4a14))
* **meals:** transform meals data ([c2fc275](https://gitlab.com/fi-sas/frontend/commit/c2fc275))
* **resolver:** food detail service and resolver ([6c87c78](https://gitlab.com/fi-sas/frontend/commit/6c87c78))
* **route:** scene detail init ([2fab06a](https://gitlab.com/fi-sas/frontend/commit/2fab06a))
* **scene:** route page for mobility ([2c312e8](https://gitlab.com/fi-sas/frontend/commit/2c312e8))
* **services:** save selected school and service in alimentation service ([d6ced1f](https://gitlab.com/fi-sas/frontend/commit/d6ced1f))



<a name="0.7.6"></a>
## [0.7.6](https://gitlab.com/fi-sas/frontend/compare/v0.7.5...v0.7.6) (2018-11-26)


### Bug Fixes

* **environment:** missing options on production ([d3666b8](https://gitlab.com/fi-sas/frontend/commit/d3666b8))



<a name="0.7.5"></a>
## [0.7.5](https://gitlab.com/fi-sas/frontend/compare/v0.6.5...v0.7.5) (2018-11-23)


### Bug Fixes

* **alimenation:** add form family products tab ([772a3ad](https://gitlab.com/fi-sas/frontend/commit/772a3ad))
* **alimentation:** fix wharhouse service list operations ([dc96a66](https://gitlab.com/fi-sas/frontend/commit/dc96a66))
* **backoffice:** change page index of the resolvers ([e85ebf3](https://gitlab.com/fi-sas/frontend/commit/e85ebf3))
* **build:** fix problems for build ([8231d21](https://gitlab.com/fi-sas/frontend/commit/8231d21))
* **cart:** review problems ([2b539b6](https://gitlab.com/fi-sas/frontend/commit/2b539b6))
* **cart-summary:** link to cart list ([00df95a](https://gitlab.com/fi-sas/frontend/commit/00df95a))
* **communication:** add post state on every step of posts history ([0aa88b0](https://gitlab.com/fi-sas/frontend/commit/0aa88b0))
* **communication:** change text on Login form as posts form ([53a20fb](https://gitlab.com/fi-sas/frontend/commit/53a20fb))
* **communication:** minor bugs ([02c091f](https://gitlab.com/fi-sas/frontend/commit/02c091f))
* **communication:** remove active from posts lists ([fdccb57](https://gitlab.com/fi-sas/frontend/commit/fdccb57))
* **config:** remove unused configurations ([18238bd](https://gitlab.com/fi-sas/frontend/commit/18238bd))
* **dashboard:** enable mobility module ([5a47e4a](https://gitlab.com/fi-sas/frontend/commit/5a47e4a))
* **dashboard:** fix module bus routing ([06d5398](https://gitlab.com/fi-sas/frontend/commit/06d5398))
* **days:** use const instead of let ([37b9cd7](https://gitlab.com/fi-sas/frontend/commit/37b9cd7))
* **history:** prevent infinity back hsitory ([c6e24d7](https://gitlab.com/fi-sas/frontend/commit/c6e24d7))
* **icons:** svg icons were not showing ([bae8144](https://gitlab.com/fi-sas/frontend/commit/bae8144))
* **media:** validate missing properties ([40e0fef](https://gitlab.com/fi-sas/frontend/commit/40e0fef))
* **merge:** conflicts ([ae12f99](https://gitlab.com/fi-sas/frontend/commit/ae12f99))
* **merge:** fix merge conflicts ([fd1ca7f](https://gitlab.com/fi-sas/frontend/commit/fd1ca7f))
* **merge:** resolve conflicts ([ff468d6](https://gitlab.com/fi-sas/frontend/commit/ff468d6))
* **merge:** resolving conflicts ([d93c9cc](https://gitlab.com/fi-sas/frontend/commit/d93c9cc))
* **navigation:** add navigation flow ([ddf9a6b](https://gitlab.com/fi-sas/frontend/commit/ddf9a6b))
* **navigation:** better navigation flow ([098fef3](https://gitlab.com/fi-sas/frontend/commit/098fef3))
* **pipe:** broken pipe ([eda2e31](https://gitlab.com/fi-sas/frontend/commit/eda2e31))
* **service:** no data problems ([a80be3e](https://gitlab.com/fi-sas/frontend/commit/a80be3e))
* **service:** no data problems ([f0b89f3](https://gitlab.com/fi-sas/frontend/commit/f0b89f3))
* **services:** import missing ([fd4d886](https://gitlab.com/fi-sas/frontend/commit/fd4d886))
* **shared:** fix translator no message ([7a998d8](https://gitlab.com/fi-sas/frontend/commit/7a998d8))
* **users:** fix users requireds feilds ([e1ff883](https://gitlab.com/fi-sas/frontend/commit/e1ff883))
* **weather:** weather service config default value ([97fe16e](https://gitlab.com/fi-sas/frontend/commit/97fe16e))


### Features

* **alimenation:** creation of entity form ([756a820](https://gitlab.com/fi-sas/frontend/commit/756a820))
* **alimentation:** add allergens to routing ([c959279](https://gitlab.com/fi-sas/frontend/commit/c959279))
* **alimentation:** add units to routing ([400cf0d](https://gitlab.com/fi-sas/frontend/commit/400cf0d))
* **alimentation:** add users to the entities form ([bd336bc](https://gitlab.com/fi-sas/frontend/commit/bd336bc))
* **alimentation:** add wharehouses form and list components ([933a22a](https://gitlab.com/fi-sas/frontend/commit/933a22a))
* **alimentation:** adding schools to routing ([f569961](https://gitlab.com/fi-sas/frontend/commit/f569961))
* **alimentation:** cration of form and list families ([9afdd4f](https://gitlab.com/fi-sas/frontend/commit/9afdd4f))
* **alimentation:** creation of alimentation service ([937ebba](https://gitlab.com/fi-sas/frontend/commit/937ebba))
* **alimentation:** creation of allergen form ([20f258a](https://gitlab.com/fi-sas/frontend/commit/20f258a))
* **alimentation:** creation of allergens components ([e72b42d](https://gitlab.com/fi-sas/frontend/commit/e72b42d))
* **alimentation:** creation of allergens service ([8b2c5ab](https://gitlab.com/fi-sas/frontend/commit/8b2c5ab))
* **alimentation:** creation of entity guard ([27204ad](https://gitlab.com/fi-sas/frontend/commit/27204ad))
* **alimentation:** creation of fiscal entity service ([140b41f](https://gitlab.com/fi-sas/frontend/commit/140b41f))
* **alimentation:** creation of list allergens ([967fffc](https://gitlab.com/fi-sas/frontend/commit/967fffc))
* **alimentation:** creation of list and form nutrients component ([f5ebbb7](https://gitlab.com/fi-sas/frontend/commit/f5ebbb7))
* **alimentation:** creation of nutrients components and service ([b40b998](https://gitlab.com/fi-sas/frontend/commit/b40b998))
* **alimentation:** creation of school form ([4342ed2](https://gitlab.com/fi-sas/frontend/commit/4342ed2))
* **alimentation:** creation of schools components ([d7864ce](https://gitlab.com/fi-sas/frontend/commit/d7864ce))
* **alimentation:** creation of schools list ([50fc438](https://gitlab.com/fi-sas/frontend/commit/50fc438))
* **alimentation:** creation of schools service ([055a454](https://gitlab.com/fi-sas/frontend/commit/055a454))
* **alimentation:** creation of services form and list component ([85a4b13](https://gitlab.com/fi-sas/frontend/commit/85a4b13))
* **alimentation:** creation of units service ([0f26996](https://gitlab.com/fi-sas/frontend/commit/0f26996))
* **alimentation:** units list and form dev ([a0f256a](https://gitlab.com/fi-sas/frontend/commit/a0f256a))
* **app_config_bus:** endpoints for bus api ([0a95a49](https://gitlab.com/fi-sas/frontend/commit/0a95a49))
* **balance:** user menu account balance ([fe2d9cc](https://gitlab.com/fi-sas/frontend/commit/fe2d9cc))
* **bus api url:** bus link ([c7ceaa8](https://gitlab.com/fi-sas/frontend/commit/c7ceaa8))
* **bus button:** enable button on dashboard ([67d50d3](https://gitlab.com/fi-sas/frontend/commit/67d50d3))
* **bus_module_files:** module configs ([458e4b1](https://gitlab.com/fi-sas/frontend/commit/458e4b1))
* **bus_routing:** bus module route ([7b4e6c5](https://gitlab.com/fi-sas/frontend/commit/7b4e6c5))
* **cart:** add user decision menus ([b1b6c58](https://gitlab.com/fi-sas/frontend/commit/b1b6c58))
* **cart:** cart service ([53acdda](https://gitlab.com/fi-sas/frontend/commit/53acdda))
* **cart:** cart service ([4ee5cfe](https://gitlab.com/fi-sas/frontend/commit/4ee5cfe))
* **cart:** cart service ([0754543](https://gitlab.com/fi-sas/frontend/commit/0754543))
* **cart:** cart service ([3db1141](https://gitlab.com/fi-sas/frontend/commit/3db1141))
* **cart:** cart service ([35c09b0](https://gitlab.com/fi-sas/frontend/commit/35c09b0))
* **cart:** cart service ([a6ea085](https://gitlab.com/fi-sas/frontend/commit/a6ea085))
* **cart:** cart service ([f5b1ec4](https://gitlab.com/fi-sas/frontend/commit/f5b1ec4))
* **cart:** cart service ([468626a](https://gitlab.com/fi-sas/frontend/commit/468626a))
* **cart:** init cart scene ([71651a2](https://gitlab.com/fi-sas/frontend/commit/71651a2))
* **cart:** wip cart summary ([f4273a9](https://gitlab.com/fi-sas/frontend/commit/f4273a9))
* **cart-summary:** static html and css for cart summary preview ([6629ba1](https://gitlab.com/fi-sas/frontend/commit/6629ba1))
* **contacts:** creation of contacts ([18a948c](https://gitlab.com/fi-sas/frontend/commit/18a948c))
* **currencys:** creation of currencys ([38f18ff](https://gitlab.com/fi-sas/frontend/commit/38f18ff))
* **day-fifteen:** show disabled weekend days ([801b45a](https://gitlab.com/fi-sas/frontend/commit/801b45a))
* **day-fifteen:** show disabled weekend days ([25fc804](https://gitlab.com/fi-sas/frontend/commit/25fc804))
* **days:** automatic list next fifteen days ([04023a0](https://gitlab.com/fi-sas/frontend/commit/04023a0))
* **detail:** food route detail ([9fd9a57](https://gitlab.com/fi-sas/frontend/commit/9fd9a57))
* **detail:** product big shop styling ([013dfda](https://gitlab.com/fi-sas/frontend/commit/013dfda))
* **detail:** static page styled and implemented ([bb9e55a](https://gitlab.com/fi-sas/frontend/commit/bb9e55a))
* **detail:** template and initial styling ([2d6df2c](https://gitlab.com/fi-sas/frontend/commit/2d6df2c))
* **fifteen-day:** automatic fifteen days ([d3db54b](https://gitlab.com/fi-sas/frontend/commit/d3db54b))
* **item:** car item description ([b2c16ac](https://gitlab.com/fi-sas/frontend/commit/b2c16ac))
* **locals:** creation of locals and list ([caedb4c](https://gitlab.com/fi-sas/frontend/commit/caedb4c))
* **mobility:** init scene ([e6c7629](https://gitlab.com/fi-sas/frontend/commit/e6c7629))
* **mobility:** scene for list places ([3ccaeb2](https://gitlab.com/fi-sas/frontend/commit/3ccaeb2))
* **mobility:** translate scene mobility ([08f2731](https://gitlab.com/fi-sas/frontend/commit/08f2731))
* **mobility-list:** mobility route list ([94d3fbd](https://gitlab.com/fi-sas/frontend/commit/94d3fbd))
* **periods:** creation of periods and list ([2962983](https://gitlab.com/fi-sas/frontend/commit/2962983))
* **places:** origin and destiny with init swap ([a899a51](https://gitlab.com/fi-sas/frontend/commit/a899a51))
* **prices_tables:** creation of prices tables and list ([3b7c4f6](https://gitlab.com/fi-sas/frontend/commit/3b7c4f6))
* **routes:** creation of routes and list. Creation of links ([83aa02b](https://gitlab.com/fi-sas/frontend/commit/83aa02b))
* **seasons:** creation of seasons and list ([9b80740](https://gitlab.com/fi-sas/frontend/commit/9b80740))
* **services:** dynamic data for services screen ([03c892b](https://gitlab.com/fi-sas/frontend/commit/03c892b))
* **services:** in alimentation select school and get services ([48d834f](https://gitlab.com/fi-sas/frontend/commit/48d834f))
* **shop-big:** meal detail shop button ([23ac3d6](https://gitlab.com/fi-sas/frontend/commit/23ac3d6))
* **tickets_types:** creation of tickets types and list ([6ba5f70](https://gitlab.com/fi-sas/frontend/commit/6ba5f70))
* **timetables:** creation of timetables and list ([c9a131b](https://gitlab.com/fi-sas/frontend/commit/c9a131b))
* **types_days:** creation of types days and list ([1c0ccb7](https://gitlab.com/fi-sas/frontend/commit/1c0ccb7))
* **types_users:** creation of types users and list ([327d602](https://gitlab.com/fi-sas/frontend/commit/327d602))
* **weather:** weather for different places ([9c0c40f](https://gitlab.com/fi-sas/frontend/commit/9c0c40f))
* **zones:** creation of zones and list ([04dbe1e](https://gitlab.com/fi-sas/frontend/commit/04dbe1e))



<a name="0.6.5"></a>
## [0.6.5](https://gitlab.com/fi-sas/frontend/compare/v0.6.4...v0.6.5) (2018-11-09)


### Bug Fixes

* **menu:** menu condition ([88392e7](https://gitlab.com/fi-sas/frontend/commit/88392e7))



<a name="0.6.4"></a>
## [0.6.4](https://gitlab.com/fi-sas/frontend/compare/v0.5.4...v0.6.4) (2018-11-09)


### Bug Fixes

* **backoffice:** fix resolvers empty response ([7722592](https://gitlab.com/fi-sas/frontend/commit/7722592))
* **colors:** change the name of grey and red colors ([63732fe](https://gitlab.com/fi-sas/frontend/commit/63732fe))
* **communication:** add delete button to feed list and categories list ([b995f4d](https://gitlab.com/fi-sas/frontend/commit/b995f4d))
* **communication:** add video upload to posts ([695f490](https://gitlab.com/fi-sas/frontend/commit/695f490))
* **communication:** change languages conditions ([a5d4ab1](https://gitlab.com/fi-sas/frontend/commit/a5d4ab1))
* **communication:** fix error when posts dont have any translation title ([40e6914](https://gitlab.com/fi-sas/frontend/commit/40e6914))
* **config:** add feeds urls ([0bf1175](https://gitlab.com/fi-sas/frontend/commit/0bf1175))
* **configs:** delete buttons confirmation ([b91dcf3](https://gitlab.com/fi-sas/frontend/commit/b91dcf3))
* **css:** font family and 2px ([51f9318](https://gitlab.com/fi-sas/frontend/commit/51f9318))
* **date-day:** interval and theming ([5590f25](https://gitlab.com/fi-sas/frontend/commit/5590f25))
* **food:** removing unnecessary theming ([3fc6258](https://gitlab.com/fi-sas/frontend/commit/3fc6258))
* **icons:** current color and names ([8cf4952](https://gitlab.com/fi-sas/frontend/commit/8cf4952))
* **media:** change upload image component ([76e3185](https://gitlab.com/fi-sas/frontend/commit/76e3185))
* **merge:** conflicts ([db169bc](https://gitlab.com/fi-sas/frontend/commit/db169bc))
* **merge:** resolve conflicts ([a2d631b](https://gitlab.com/fi-sas/frontend/commit/a2d631b))
* **merge:** resolve conflicts ([b2a413e](https://gitlab.com/fi-sas/frontend/commit/b2a413e))
* **rfid:** fix read data ([82c08ee](https://gitlab.com/fi-sas/frontend/commit/82c08ee))
* **serial:** change from usb to serial ([2f8dc23](https://gitlab.com/fi-sas/frontend/commit/2f8dc23))
* **serial:** refactor class serial ([e521ca0](https://gitlab.com/fi-sas/frontend/commit/e521ca0))
* **service:** when initializing with invalid token ([4fd069a](https://gitlab.com/fi-sas/frontend/commit/4fd069a))
* **share:** change declaration on image componenet uploader ([2dc2bc3](https://gitlab.com/fi-sas/frontend/commit/2dc2bc3))
* **share:** fix upload componenets ([69423b6](https://gitlab.com/fi-sas/frontend/commit/69423b6))
* **share:** upload image component is now a form control ([4ebdbf8](https://gitlab.com/fi-sas/frontend/commit/4ebdbf8))
* **shared:** fix upload components test error ([8f2d735](https://gitlab.com/fi-sas/frontend/commit/8f2d735))
* **shared:** fix upload file missing language_id ([04d4a01](https://gitlab.com/fi-sas/frontend/commit/04d4a01))
* **space:** scroll space ([0b3e8d1](https://gitlab.com/fi-sas/frontend/commit/0b3e8d1))
* **svg:** svg fill color and theming ([7cbdb12](https://gitlab.com/fi-sas/frontend/commit/7cbdb12))
* **two-col:** theming ([7b731f4](https://gitlab.com/fi-sas/frontend/commit/7b731f4))
* **typings:** typescript lint errors ([03abedf](https://gitlab.com/fi-sas/frontend/commit/03abedf))
* **users:** change style from css to less ([184ee2f](https://gitlab.com/fi-sas/frontend/commit/184ee2f))
* **users:** update scope model, form and list ([c992764](https://gitlab.com/fi-sas/frontend/commit/c992764))
* **vertical-item:** interface typo ([169f549](https://gitlab.com/fi-sas/frontend/commit/169f549))


### Features

* **button:** styling antd button ([abd7221](https://gitlab.com/fi-sas/frontend/commit/abd7221))
* **button:** theme button ([0b26fc3](https://gitlab.com/fi-sas/frontend/commit/0b26fc3))
* **communication:** add categories service ([fd25249](https://gitlab.com/fi-sas/frontend/commit/fd25249))
* **communication:** add feed's service ([7fe4f5d](https://gitlab.com/fi-sas/frontend/commit/7fe4f5d))
* **communication:** add posts validation status ([c33308b](https://gitlab.com/fi-sas/frontend/commit/c33308b))
* **communication:** creation of feed's form ([d067020](https://gitlab.com/fi-sas/frontend/commit/d067020))
* **communication:** creation of form category ([1d52b9f](https://gitlab.com/fi-sas/frontend/commit/1d52b9f))
* **communication:** creation of list of feeds ([7dbb161](https://gitlab.com/fi-sas/frontend/commit/7dbb161))
* **communication:** creation of list of posts ([c173313](https://gitlab.com/fi-sas/frontend/commit/c173313))
* **communication:** creation of lists categories ([303f864](https://gitlab.com/fi-sas/frontend/commit/303f864))
* **communication:** creation of module comunication ([5a437d9](https://gitlab.com/fi-sas/frontend/commit/5a437d9))
* **communication:** creation of posts form ([1a1bdae](https://gitlab.com/fi-sas/frontend/commit/1a1bdae))
* **communication:** creation of posts service ([e56993a](https://gitlab.com/fi-sas/frontend/commit/e56993a))
* **communication:** creation of resolvers categories and channels ([958c49a](https://gitlab.com/fi-sas/frontend/commit/958c49a))
* **communication:** show history of posts status ([f9cdbd6](https://gitlab.com/fi-sas/frontend/commit/f9cdbd6))
* **configs:** add devices service ([d1b12ae](https://gitlab.com/fi-sas/frontend/commit/d1b12ae))
* **configs:** add footers service ([7754c5e](https://gitlab.com/fi-sas/frontend/commit/7754c5e))
* **configs:** add groups service ([434ad01](https://gitlab.com/fi-sas/frontend/commit/434ad01))
* **configs:** creation of languages service ([5c39a04](https://gitlab.com/fi-sas/frontend/commit/5c39a04))
* **context:** component new structure ([82c9990](https://gitlab.com/fi-sas/frontend/commit/82c9990))
* **context-large-block:** theming ([a75085e](https://gitlab.com/fi-sas/frontend/commit/a75085e))
* **core:** add confirm modal to ui service ([045edf3](https://gitlab.com/fi-sas/frontend/commit/045edf3))
* **core:** add return modal function ([f65b1d9](https://gitlab.com/fi-sas/frontend/commit/f65b1d9))
* **core:** set side menu always open when activated ([9793ade](https://gitlab.com/fi-sas/frontend/commit/9793ade))
* **electron:** init preferences ([d6d1057](https://gitlab.com/fi-sas/frontend/commit/d6d1057))
* **fifteen-days:** new component structure ([a50918e](https://gitlab.com/fi-sas/frontend/commit/a50918e))
* **food:** product list detail icons ([e3bf5dc](https://gitlab.com/fi-sas/frontend/commit/e3bf5dc))
* **food:** removing theming ([86c8b55](https://gitlab.com/fi-sas/frontend/commit/86c8b55))
* **icons:** icons reviewed ([af143c2](https://gitlab.com/fi-sas/frontend/commit/af143c2))
* **install:** add serial port to install config ([265edf5](https://gitlab.com/fi-sas/frontend/commit/265edf5))
* **large-block:** passing content ([249568b](https://gitlab.com/fi-sas/frontend/commit/249568b))
* **main-menu:** new component structure ([d57a18e](https://gitlab.com/fi-sas/frontend/commit/d57a18e))
* **medias:** add category service ([6e80f9f](https://gitlab.com/fi-sas/frontend/commit/6e80f9f))
* **medias:** add files service ([7e051c4](https://gitlab.com/fi-sas/frontend/commit/7e051c4))
* **menu-vertical-item:** new component structure ([5708a26](https://gitlab.com/fi-sas/frontend/commit/5708a26))
* **preferences:** init serial preferences ([9f4e08f](https://gitlab.com/fi-sas/frontend/commit/9f4e08f))
* **product:** new component structure ([c78dac3](https://gitlab.com/fi-sas/frontend/commit/c78dac3))
* **rfid:** implement rfid authentication ([87ac004](https://gitlab.com/fi-sas/frontend/commit/87ac004))
* **rfid:** read serial port ([4d6cc90](https://gitlab.com/fi-sas/frontend/commit/4d6cc90))
* **serial:** async serial resolver ([8c48fb7](https://gitlab.com/fi-sas/frontend/commit/8c48fb7))
* **services:** services list design ([02d5416](https://gitlab.com/fi-sas/frontend/commit/02d5416))
* **services:** services screen design ([c995203](https://gitlab.com/fi-sas/frontend/commit/c995203))
* **services:** translations ([bfc3020](https://gitlab.com/fi-sas/frontend/commit/bfc3020))
* **share:** add http error interceptor ([14f0c3d](https://gitlab.com/fi-sas/frontend/commit/14f0c3d))
* **share:** creation of resolvers all groups ([ff9ecf0](https://gitlab.com/fi-sas/frontend/commit/ff9ecf0))
* **timezone:** daylight saving time ([3f122e0](https://gitlab.com/fi-sas/frontend/commit/3f122e0))
* **users:** add scopes service ([27dfc7b](https://gitlab.com/fi-sas/frontend/commit/27dfc7b))
* **users:** add users groups service ([89097b8](https://gitlab.com/fi-sas/frontend/commit/89097b8))
* **users:** add users service ([01e7ac4](https://gitlab.com/fi-sas/frontend/commit/01e7ac4))
* **vertical-item:** vertical menu item component with data ([9752607](https://gitlab.com/fi-sas/frontend/commit/9752607))



<a name="0.5.4"></a>
## [0.5.4](https://gitlab.com/fi-sas/frontend/compare/v0.5.3...v0.5.4) (2018-10-29)


### Bug Fixes

* **video:** trying to avoid error cache operation not supported ([8601c27](https://gitlab.com/fi-sas/frontend/commit/8601c27))


### Features

* **config:** connect config with service ([6dedf49](https://gitlab.com/fi-sas/frontend/commit/6dedf49))
* **ticker:** change tickers language ([7280456](https://gitlab.com/fi-sas/frontend/commit/7280456))



<a name="0.5.3"></a>
## [0.5.3](https://gitlab.com/fi-sas/frontend/compare/v0.5.2...v0.5.3) (2018-10-26)


### Bug Fixes

* **electron:** environment was true always ([efe33ff](https://gitlab.com/fi-sas/frontend/commit/efe33ff))



<a name="0.5.2"></a>
## [0.5.2](https://gitlab.com/fi-sas/frontend/compare/v0.5.1...v0.5.2) (2018-10-26)


### Bug Fixes

* **options:** options for electron missing ([fd37c76](https://gitlab.com/fi-sas/frontend/commit/fd37c76))



<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/fi-sas/frontend/compare/v0.5.0...v0.5.1) (2018-10-26)


### Bug Fixes

* **zip:** deploy zip empty was fixed ([5546a1e](https://gitlab.com/fi-sas/frontend/commit/5546a1e))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/fi-sas/frontend/compare/v0.4.0...v0.5.0) (2018-10-26)


### Bug Fixes

* **backoffice:** add messages from ui service ([84332b7](https://gitlab.com/fi-sas/frontend/commit/84332b7))
* **backoffice:** add resolvers to all features of backoffice ([f540884](https://gitlab.com/fi-sas/frontend/commit/f540884))
* **backoffice:** change controls error detection ([a1bd5c8](https://gitlab.com/fi-sas/frontend/commit/a1bd5c8))
* **core:** create interface for the api errors on resource interface ([848002d](https://gitlab.com/fi-sas/frontend/commit/848002d))
* **core:** export resource error interface ([9740ee9](https://gitlab.com/fi-sas/frontend/commit/9740ee9))
* **css:** days position ([b016340](https://gitlab.com/fi-sas/frontend/commit/b016340))
* **css:** design adjustments ([263cd72](https://gitlab.com/fi-sas/frontend/commit/263cd72))
* **css:** remove text selection and elements outline ([dac292f](https://gitlab.com/fi-sas/frontend/commit/dac292f))
* **font:** add missing fonts and load them locally ([44f6d62](https://gitlab.com/fi-sas/frontend/commit/44f6d62))
* **import:** routing imports clean ([c5c4e3d](https://gitlab.com/fi-sas/frontend/commit/c5c4e3d))
* **keyboard:** add output to changes on keyboard values ([8dc6fff](https://gitlab.com/fi-sas/frontend/commit/8dc6fff))
* **keyboard:** get form control injection ([eff9223](https://gitlab.com/fi-sas/frontend/commit/eff9223))
* **load:** remove loading text ([b19a464](https://gitlab.com/fi-sas/frontend/commit/b19a464))
* **loading:** loading page removed ([b3f5f0a](https://gitlab.com/fi-sas/frontend/commit/b3f5f0a))
* **loading:** removed loader ([195e349](https://gitlab.com/fi-sas/frontend/commit/195e349))
* **media:** add header to media service ([74c8bb5](https://gitlab.com/fi-sas/frontend/commit/74c8bb5))
* **media:** empty media ([a3501af](https://gitlab.com/fi-sas/frontend/commit/a3501af))
* **media:** muted audio ([3ce6daf](https://gitlab.com/fi-sas/frontend/commit/3ce6daf))
* **media:** public file could be false ([98d3efd](https://gitlab.com/fi-sas/frontend/commit/98d3efd))
* **media:** remove media with empty path file ([2bd931c](https://gitlab.com/fi-sas/frontend/commit/2bd931c))
* **media:** wrong name for key ([772c644](https://gitlab.com/fi-sas/frontend/commit/772c644))
* **medias:** medias and tickers empty arrays ([6052c88](https://gitlab.com/fi-sas/frontend/commit/6052c88))
* **merge:** conflicts ([0479012](https://gitlab.com/fi-sas/frontend/commit/0479012))
* **merge:** resolve conflicts ([a630dd8](https://gitlab.com/fi-sas/frontend/commit/a630dd8))
* **merge:** resolve conflicts ([810926a](https://gitlab.com/fi-sas/frontend/commit/810926a))
* **merge:** resolve merge conflicts ([6e7b339](https://gitlab.com/fi-sas/frontend/commit/6e7b339))
* **mock:** remove mock tokens ([c3ecb2c](https://gitlab.com/fi-sas/frontend/commit/c3ecb2c))
* **nx:** upgrade nx to version 6.1.1 ([eff9e1f](https://gitlab.com/fi-sas/frontend/commit/eff9e1f))
* **provider:** app base ref token provider ([4eead2c](https://gitlab.com/fi-sas/frontend/commit/4eead2c))
* **release:** only for tags ([ccdd372](https://gitlab.com/fi-sas/frontend/commit/ccdd372))
* **style:** icons style ([868ee70](https://gitlab.com/fi-sas/frontend/commit/868ee70))
* **test:** add ticker service to providers ([ef18a9e](https://gitlab.com/fi-sas/frontend/commit/ef18a9e))
* **test:** failed pipeline ([c61650a](https://gitlab.com/fi-sas/frontend/commit/c61650a))
* **test:** play component test ([8eeda69](https://gitlab.com/fi-sas/frontend/commit/8eeda69))
* **ticker:** text color opacity ([c6f498f](https://gitlab.com/fi-sas/frontend/commit/c6f498f))
* **users:** delete repeat_password from object send on post ([e567feb](https://gitlab.com/fi-sas/frontend/commit/e567feb))
* **video:** autoplay false ([9ceee9a](https://gitlab.com/fi-sas/frontend/commit/9ceee9a))


### Features

* **account:** menu account styling ([acc7797](https://gitlab.com/fi-sas/frontend/commit/acc7797))
* **auth:** add permissions verification and token validation ([e0e11da](https://gitlab.com/fi-sas/frontend/commit/e0e11da))
* **auth:** auth menu when user logged in ([8457624](https://gitlab.com/fi-sas/frontend/commit/8457624))
* **auth:** device auth ([912a0f5](https://gitlab.com/fi-sas/frontend/commit/912a0f5))
* **auth:** init separation of concepts ([a6085f8](https://gitlab.com/fi-sas/frontend/commit/a6085f8))
* **auth:** theming ([0530d4b](https://gitlab.com/fi-sas/frontend/commit/0530d4b))
* **cart:** empty cart ([ef74bfa](https://gitlab.com/fi-sas/frontend/commit/ef74bfa))
* **context:** new context component ([9803866](https://gitlab.com/fi-sas/frontend/commit/9803866))
* **dashboard:** creating applications grid ([73eb70a](https://gitlab.com/fi-sas/frontend/commit/73eb70a))
* **dashboard:** implementing icons and style ([3e4d123](https://gitlab.com/fi-sas/frontend/commit/3e4d123))
* **device:** save token device authorize ([13e001c](https://gitlab.com/fi-sas/frontend/commit/13e001c))
* **food:** food and meal component init ([ba713c7](https://gitlab.com/fi-sas/frontend/commit/ba713c7))
* **food:** meals design ([65292bd](https://gitlab.com/fi-sas/frontend/commit/65292bd))
* **food:** meals design ([806aeba](https://gitlab.com/fi-sas/frontend/commit/806aeba))
* **food:** meals overflows ([c3f947d](https://gitlab.com/fi-sas/frontend/commit/c3f947d))
* **food:** meals static design ([ef9f961](https://gitlab.com/fi-sas/frontend/commit/ef9f961))
* **food:** static design for meals ([81da9a7](https://gitlab.com/fi-sas/frontend/commit/81da9a7))
* **history:** routing history and guards ([e434431](https://gitlab.com/fi-sas/frontend/commit/e434431))
* **intializer:** hook angular initialization ([fea5d16](https://gitlab.com/fi-sas/frontend/commit/fea5d16))
* **login:** authenticate user in service ([72d3d96](https://gitlab.com/fi-sas/frontend/commit/72d3d96))
* **meal:** simple bar horizontal ([6acfa83](https://gitlab.com/fi-sas/frontend/commit/6acfa83))
* **meals:** theming ([e72eefc](https://gitlab.com/fi-sas/frontend/commit/e72eefc))
* **media:** add category form page ([0d94a6f](https://gitlab.com/fi-sas/frontend/commit/0d94a6f))
* **media:** add form file page to upload content ([7b25721](https://gitlab.com/fi-sas/frontend/commit/7b25721))
* **media:** add list categories page ([fafc511](https://gitlab.com/fi-sas/frontend/commit/fafc511))
* **media:** add list files page ([552ed7c](https://gitlab.com/fi-sas/frontend/commit/552ed7c))
* **media:** add media module to backoffice ([6afdc10](https://gitlab.com/fi-sas/frontend/commit/6afdc10))
* **media:** join component with microservice of communication ([b9d5151](https://gitlab.com/fi-sas/frontend/commit/b9d5151))
* **media:** media with priority applied ([0a1d717](https://gitlab.com/fi-sas/frontend/commit/0a1d717))
* **media:** use token that is in storage for media service ([c55482d](https://gitlab.com/fi-sas/frontend/commit/c55482d))
* **pwa:** implementing capability to work offline ([3a2346f](https://gitlab.com/fi-sas/frontend/commit/3a2346f))
* **scroll:** horizontal drag content ([ee23c93](https://gitlab.com/fi-sas/frontend/commit/ee23c93))
* **share:** add file upload component ([bfbb6f9](https://gitlab.com/fi-sas/frontend/commit/bfbb6f9))
* **share:** add image upload component with cropperjs ([2d546c3](https://gitlab.com/fi-sas/frontend/commit/2d546c3))
* **share:** add messages to ui service ([7fca565](https://gitlab.com/fi-sas/frontend/commit/7fca565))
* **share:** error translator service for error validation from api ([785d16b](https://gitlab.com/fi-sas/frontend/commit/785d16b))
* **storage:** implementing service for storage ([6fc2fac](https://gitlab.com/fi-sas/frontend/commit/6fc2fac))
* **ticker:** connect ticker to service ([a74bbfd](https://gitlab.com/fi-sas/frontend/commit/a74bbfd))
* **tickers:** added tickers coming from service on authentication ([bf8315e](https://gitlab.com/fi-sas/frontend/commit/bf8315e))
* **timer:** tickers timer caching ([f96adc1](https://gitlab.com/fi-sas/frontend/commit/f96adc1))
* **vertical-menu:** design of vertical menu of items ([16c8ab3](https://gitlab.com/fi-sas/frontend/commit/16c8ab3))
* **weather:** connect to open weather map ([aa2c999](https://gitlab.com/fi-sas/frontend/commit/aa2c999))
* **week-day:** theming ([fa5fd16](https://gitlab.com/fi-sas/frontend/commit/fa5fd16))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/fi-sas/frontend/compare/v0.3.0...v0.4.0) (2018-08-31)


### Bug Fixes

* **auth:** change to work with new resource service ([2633921](https://gitlab.com/fi-sas/frontend/commit/2633921))
* **auth:** correct user mode load after session closed ([21e20bb](https://gitlab.com/fi-sas/frontend/commit/21e20bb))
* **auth:** fix user is send null to observable ([acf8c70](https://gitlab.com/fi-sas/frontend/commit/acf8c70))
* **config:** change config to work with new resource service ([c02e582](https://gitlab.com/fi-sas/frontend/commit/c02e582))
* **core:** change http data signature ([42191fd](https://gitlab.com/fi-sas/frontend/commit/42191fd))
* **dashboard:** add alimentation link to dashboard ([1b06242](https://gitlab.com/fi-sas/frontend/commit/1b06242))
* **header-user:** add observable of user object ([078db84](https://gitlab.com/fi-sas/frontend/commit/078db84))
* **header-user:** add the data of user ([18ac73e](https://gitlab.com/fi-sas/frontend/commit/18ac73e))
* **json:** wrong limit in footer mock ([3410b59](https://gitlab.com/fi-sas/frontend/commit/3410b59))
* **keyboard:** domain dot suffix cursor position ([cb257d8](https://gitlab.com/fi-sas/frontend/commit/cb257d8))
* **keyboard:** set font size ([0cb9720](https://gitlab.com/fi-sas/frontend/commit/0cb9720))
* **login:** fix no message in case of exception ([b71c1be](https://gitlab.com/fi-sas/frontend/commit/b71c1be))
* **login:** show different error messages ([abe8c99](https://gitlab.com/fi-sas/frontend/commit/abe8c99))
* **resource:** fix create and update methods ([e1d5cfb](https://gitlab.com/fi-sas/frontend/commit/e1d5cfb))
* **resource:** resource service now return all resource data ([21dbb2a](https://gitlab.com/fi-sas/frontend/commit/21dbb2a))
* **service:** fix when config endpoints object is empty ([53fb8f2](https://gitlab.com/fi-sas/frontend/commit/53fb8f2))
* **shared:** move page not fount and test component to shared module ([58b09de](https://gitlab.com/fi-sas/frontend/commit/58b09de))
* **test:** install tests dependencies ([7d05671](https://gitlab.com/fi-sas/frontend/commit/7d05671))
* **theme:** include paths missing for tests ([ba2abae](https://gitlab.com/fi-sas/frontend/commit/ba2abae))
* **tickers:** change to work with new resource service ([851a46d](https://gitlab.com/fi-sas/frontend/commit/851a46d))
* **users:** change the url to get scope by id ([fa0ed07](https://gitlab.com/fi-sas/frontend/commit/fa0ed07))
* **users:** fix to work with new resource service ([feba891](https://gitlab.com/fi-sas/frontend/commit/feba891))


### Features

* **alimentation:** creation of alimentation module ([452d2d7](https://gitlab.com/fi-sas/frontend/commit/452d2d7))
* **auth:** added authentication layout to the backoffice ([99c26f4](https://gitlab.com/fi-sas/frontend/commit/99c26f4))
* **auth:** creation of authentication interceptor ([dd5fe65](https://gitlab.com/fi-sas/frontend/commit/dd5fe65))
* **auth:** creation of authentication service ([c000e89](https://gitlab.com/fi-sas/frontend/commit/c000e89))
* **auth-models:** creation of user module models ([1e92fab](https://gitlab.com/fi-sas/frontend/commit/1e92fab))
* **authentication:** authentication route ([2692973](https://gitlab.com/fi-sas/frontend/commit/2692973))
* **authentication:** english translation ([a25cae1](https://gitlab.com/fi-sas/frontend/commit/a25cae1))
* **authentication:** initial theming ([2727c62](https://gitlab.com/fi-sas/frontend/commit/2727c62))
* **authentication:** reactive form ([5fc57ad](https://gitlab.com/fi-sas/frontend/commit/5fc57ad))
* **authentication:** reajust response signature ([8dc22e4](https://gitlab.com/fi-sas/frontend/commit/8dc22e4))
* **authentication:** validator on form ([dc87c1a](https://gitlab.com/fi-sas/frontend/commit/dc87c1a))
* **config:** add configuration module ([2b61f3d](https://gitlab.com/fi-sas/frontend/commit/2b61f3d))
* **config:** add footers list and form component ([76e88d2](https://gitlab.com/fi-sas/frontend/commit/76e88d2))
* **config:** add group form ([7f938bf](https://gitlab.com/fi-sas/frontend/commit/7f938bf))
* **config:** add language form ([3df6ff2](https://gitlab.com/fi-sas/frontend/commit/3df6ff2))
* **config:** add libraries core to the backoffice ([fa28506](https://gitlab.com/fi-sas/frontend/commit/fa28506))
* **config:** add list of devices ([046a4a1](https://gitlab.com/fi-sas/frontend/commit/046a4a1))
* **config:** add list of groups ([ee41269](https://gitlab.com/fi-sas/frontend/commit/ee41269))
* **config:** add list of language ([ee3b79b](https://gitlab.com/fi-sas/frontend/commit/ee3b79b))
* **config:** added form device ([b833219](https://gitlab.com/fi-sas/frontend/commit/b833219))
* **docker:** adjust release for backoffice ([71dc9fc](https://gitlab.com/fi-sas/frontend/commit/71dc9fc))
* **docker:** build only on master ([b257262](https://gitlab.com/fi-sas/frontend/commit/b257262))
* **docker:** initial test for automated deploys ([98c2751](https://gitlab.com/fi-sas/frontend/commit/98c2751))
* **guards:** add public and private guard to backoffice ([72f881b](https://gitlab.com/fi-sas/frontend/commit/72f881b))
* **guards:** added public and private guard ([8069ca5](https://gitlab.com/fi-sas/frontend/commit/8069ca5))
* **install:** initial screen for config ([6621b2a](https://gitlab.com/fi-sas/frontend/commit/6621b2a))
* **install:** save local config ([62ecd9c](https://gitlab.com/fi-sas/frontend/commit/62ecd9c))
* **keyboard:** write in the middle, accents and hide/show ([a107498](https://gitlab.com/fi-sas/frontend/commit/a107498))
* **login:** added login page to backoffice ([994e8ec](https://gitlab.com/fi-sas/frontend/commit/994e8ec))
* **scopes:** add scope form and list of scopes ([f72b67e](https://gitlab.com/fi-sas/frontend/commit/f72b67e))
* **theme:** color for ticker ([ff8f535](https://gitlab.com/fi-sas/frontend/commit/ff8f535))
* **theme:** footer and header components ([13e1344](https://gitlab.com/fi-sas/frontend/commit/13e1344))
* **theme:** initial theme aproach ([0985894](https://gitlab.com/fi-sas/frontend/commit/0985894))
* **theme:** menu theming process ([7707793](https://gitlab.com/fi-sas/frontend/commit/7707793))
* **theme:** reajust definitions ([e0c19c1](https://gitlab.com/fi-sas/frontend/commit/e0c19c1))
* **theme:** theme colors for all available components ([1c67a71](https://gitlab.com/fi-sas/frontend/commit/1c67a71))
* **user-groups:** add user-group form and list of user-groups ([7701f9e](https://gitlab.com/fi-sas/frontend/commit/7701f9e))
* **users:** add user form and list of users ([e56300f](https://gitlab.com/fi-sas/frontend/commit/e56300f))
* **users:** add users module to backoffice ([2b2a63c](https://gitlab.com/fi-sas/frontend/commit/2b2a63c))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/fi-sas/frontend/compare/v0.2.0...v0.3.0) (2018-07-30)


### Bug Fixes

* **antd:** antd does not support module split ([f53a6eb](https://gitlab.com/fi-sas/frontend/commit/f53a6eb))
* **bugs:** minor bugs ([1f0ad40](https://gitlab.com/fi-sas/frontend/commit/1f0ad40))
* **clock:** clock with date pipe ([f25686f](https://gitlab.com/fi-sas/frontend/commit/f25686f))
* **dashboard:** change declaration of DashboardModule class ([32d0b04](https://gitlab.com/fi-sas/frontend/commit/32d0b04))
* **dashboard:** set the type of variable ([85d8569](https://gitlab.com/fi-sas/frontend/commit/85d8569))
* **imports:** remove unwanted imports ([a358eaa](https://gitlab.com/fi-sas/frontend/commit/a358eaa))
* **media:** change equal comparison ([0fdd34c](https://gitlab.com/fi-sas/frontend/commit/0fdd34c))
* **media:** fix to run in electron ([8c793ed](https://gitlab.com/fi-sas/frontend/commit/8c793ed))
* **media:** media width strange behaviour ([1ebfa7e](https://gitlab.com/fi-sas/frontend/commit/1ebfa7e))
* **media:** scroll-y influence on media content ([31c4397](https://gitlab.com/fi-sas/frontend/commit/31c4397))
* **merge:** fix merge changes ([8dd4539](https://gitlab.com/fi-sas/frontend/commit/8dd4539))
* **merge:** merge branch 'develop' into FE-Translations ([1c23101](https://gitlab.com/fi-sas/frontend/commit/1c23101))
* **sidenav:** fix sub menu items link's ([5f5fcd7](https://gitlab.com/fi-sas/frontend/commit/5f5fcd7))
* **styling:** move layout css to main style file ([558f316](https://gitlab.com/fi-sas/frontend/commit/558f316))
* **test:** app component state provider ([63f2544](https://gitlab.com/fi-sas/frontend/commit/63f2544))
* **test:** refactor some tests ([9fd75fe](https://gitlab.com/fi-sas/frontend/commit/9fd75fe))


### Features

* **breadcrumb:** add breadcrumb component ([6621533](https://gitlab.com/fi-sas/frontend/commit/6621533))
* **dashboard:** add dashboard component ([8ecd8e6](https://gitlab.com/fi-sas/frontend/commit/8ecd8e6))
* **footer:** add footer component ([8a6ee72](https://gitlab.com/fi-sas/frontend/commit/8a6ee72))
* **header:** add component header to full layout ([983ae0d](https://gitlab.com/fi-sas/frontend/commit/983ae0d))
* **keyboard:** component initial for keyboard ([4534daa](https://gitlab.com/fi-sas/frontend/commit/4534daa))
* **layout:** add full layout component ([5300954](https://gitlab.com/fi-sas/frontend/commit/5300954))
* **layout:** add ui-service to components  ([d546ccd](https://gitlab.com/fi-sas/frontend/commit/d546ccd))
* **layout:** create core module ([8c68255](https://gitlab.com/fi-sas/frontend/commit/8c68255))
* **layout:** creation of ui-service for controlling the layout ([c7c68f2](https://gitlab.com/fi-sas/frontend/commit/c7c68f2))
* **media:** media slide with videos ([3a87421](https://gitlab.com/fi-sas/frontend/commit/3a87421))
* **not-found:** add page not found component ([6f78419](https://gitlab.com/fi-sas/frontend/commit/6f78419))
* **shared:** shared module ([62dd689](https://gitlab.com/fi-sas/frontend/commit/62dd689))
* **sidenav:** add side nav component ([08a3ae1](https://gitlab.com/fi-sas/frontend/commit/08a3ae1))
* **state:** redux state ([97593a3](https://gitlab.com/fi-sas/frontend/commit/97593a3))
* **structure:** create structure of backoffice ([9944462](https://gitlab.com/fi-sas/frontend/commit/9944462))
* **ticker:** async ticker data ([446ef61](https://gitlab.com/fi-sas/frontend/commit/446ef61))
* **timer:** deactivate app to screen saver ([b104181](https://gitlab.com/fi-sas/frontend/commit/b104181))
* **translation:** translations in home scene ([f4bfd09](https://gitlab.com/fi-sas/frontend/commit/f4bfd09))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/fi-sas/frontend/compare/v0.0.1...v0.2.0) (2018-07-20)


### Bug Fixes

* **clock:** added import component to spec ([4e30247](https://gitlab.com/fi-sas/frontend/commit/4e30247))
* **clock:** invalid character ([d41ff7b](https://gitlab.com/fi-sas/frontend/commit/d41ff7b))
* **core:** wrong import in spec module ([9528893](https://gitlab.com/fi-sas/frontend/commit/9528893))
* **import:** fix test import class ([0412b89](https://gitlab.com/fi-sas/frontend/commit/0412b89))
* **media:** component media name ([69caba5](https://gitlab.com/fi-sas/frontend/commit/69caba5))
* **media:** import antd module in spec ([c5ae6c8](https://gitlab.com/fi-sas/frontend/commit/c5ae6c8))
* **merge:** fix merge conflicts ([dab1bfa](https://gitlab.com/fi-sas/frontend/commit/dab1bfa))
* **merge:** merge branch 'develop' into FE-Ticker ([ab12f7b](https://gitlab.com/fi-sas/frontend/commit/ab12f7b))
* **nx:** fix nx.json with backoffice ([d7c8da6](https://gitlab.com/fi-sas/frontend/commit/d7c8da6))
* **selector:** fix prefix selectors ([c131929](https://gitlab.com/fi-sas/frontend/commit/c131929))
* **test:** fix home spec ([00ebc4a](https://gitlab.com/fi-sas/frontend/commit/00ebc4a))
* **test:** fix wrong imports ([21a5889](https://gitlab.com/fi-sas/frontend/commit/21a5889))
* **test:** forgotten import on component date ([3569e8c](https://gitlab.com/fi-sas/frontend/commit/3569e8c))
* **test:** test fixed for initial release ([9f7d344](https://gitlab.com/fi-sas/frontend/commit/9f7d344))
* **test:** test problem in ticker interval ([80dc1dc](https://gitlab.com/fi-sas/frontend/commit/80dc1dc))
* **theme:** refactor directories for theming ([41d2fca](https://gitlab.com/fi-sas/frontend/commit/41d2fca))
* **ticker:** lint problems ([8b2bc56](https://gitlab.com/fi-sas/frontend/commit/8b2bc56))


### Features

* **backoffice:** add app backoffice ([18e5751](https://gitlab.com/fi-sas/frontend/commit/18e5751))
* **clock:** clock component for header ([75e67b3](https://gitlab.com/fi-sas/frontend/commit/75e67b3))
* **configurator:** add lib configurator ([87c0857](https://gitlab.com/fi-sas/frontend/commit/87c0857))
* **language:** lazy load of languages ([080c870](https://gitlab.com/fi-sas/frontend/commit/080c870))
* **libs:** add lib utils and share ([b23bd7c](https://gitlab.com/fi-sas/frontend/commit/b23bd7c))
* **media:** media dummy ([fb47205](https://gitlab.com/fi-sas/frontend/commit/fb47205))
* **media:** media slide show example ([1a66998](https://gitlab.com/fi-sas/frontend/commit/1a66998))
* **menu:** dummy menu component ([dba3970](https://gitlab.com/fi-sas/frontend/commit/dba3970))
* **services:** resource service and related core services ([2823ad8](https://gitlab.com/fi-sas/frontend/commit/2823ad8))
* **theme:** initial theming rules ([6c15219](https://gitlab.com/fi-sas/frontend/commit/6c15219))
* **ticker:** applying design ([2340702](https://gitlab.com/fi-sas/frontend/commit/2340702))
* **ticker:** ticker version with out design ([f29b061](https://gitlab.com/fi-sas/frontend/commit/f29b061))
* **weather:** added weather component ([872f8f0](https://gitlab.com/fi-sas/frontend/commit/872f8f0))



<a name="0.0.1"></a>
## 0.0.1 (2018-07-13)


### Bug Fixes

* **merge:** merge conflict changes ([478005d](https://gitlab.com/fi-sas/frontend/commit/478005d))
* **mock:** only a record should be on footer ([ebd6ad1](https://gitlab.com/fi-sas/frontend/commit/ebd6ad1))


### Features

* **antd:** add antd ui project ([93c8b9c](https://gitlab.com/fi-sas/frontend/commit/93c8b9c))
* **bundle:** Compile and generate electron executable bundle. ([002f804](https://gitlab.com/fi-sas/frontend/commit/002f804))
* **commitlint:** add lint on commits ([a1afaf3](https://gitlab.com/fi-sas/frontend/commit/a1afaf3))
* **mock:** json rest data mock ([4befcae](https://gitlab.com/fi-sas/frontend/commit/4befcae))
* **schematic:** Added schemati ti generate files and scripts for electron. ([bba8a26](https://gitlab.com/fi-sas/frontend/commit/bba8a26))
* **translate:** add ngx-translate lib ([f0b426c](https://gitlab.com/fi-sas/frontend/commit/f0b426c))
