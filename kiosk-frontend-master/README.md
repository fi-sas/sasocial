# @FIS-SASOCIAL

Monorepo de aplicações para kiosk, tv e web.

## Iniciação

Estas instruções servem para obter uma cópia do projecto de forma a correr localmente na máquina para desenvolvimento e testes.

### Pré-Requisitos

* Nodejs 8.16
* Git
* Angular Cli
* Gulp
* Windows 10
* ATEN USB UC232A
* EPSON TM-T88V

Por defeito a aplicação gerada pela pipeline do Gitlab é para windows.<br>
Pré-requisito do sistema operativo é Windows 10. <br>
Para o **RFID** funcionar é necessário utilizar o cabo `ATEN USB UC232A` (conversor de usb para porta de série) e fazer download dos drivers do mesmo que se encontram [aqui](https://www.aten.com/global/en/products/usb-&-thunderbolt/usb-converters/uc232a/#.W-Vo4nr7Rlc).<br>
Para a **Impressora** é necessário utilizar a impressora `EPSON TM-T88V` e fazer download dos drivers da mesma que se encontram [aqui](https://download.epson-biz.com/modules/pos/index.php?page=single_soft&cid=5736&pcat=3&pid=36) é também necessário fazer download do software para a criação de uma **Porta COM Virtual** (visto que esta impressora funciona por usb) que se encontra [aqui](https://download.epson-biz.com/modules/pos/index.php?page=single_soft&cid=6175&pcat=3&pid=36).

Este projecto usa como estrutura base [Nrwl](https://github.com/nrwl/nx) e [Angular Cli](https://github.com/angular/angular-cli).

## Instalação

Inicie por clonar o projecto para a sua máquina local.

```
git clone git@gitlab.com:fi-sas/frontend.git fis-social
```

De seguida proceda à instalação de todas as dependências.

```
cd fis-social
npm i
```

Depois da instalação estará pronto para iniciar aplicações do repositório.

## Indice de aplicações e comandos

Para ambiente de desenvolvimento de aplicações electron é necessário que corra sobre dois terminais os seguintes comandos:

```
Terminal1@: npm run gulp.kiosk.electron.watch
Terminal2@: npm run gulp.kiosk.watch
```

| Aplicação | Comando                           | Descritivo                                                                            | Ambiente        |
| --------- | --------------------------------- | ------------------------------------------------------------------------------------- | --------------- |
| Kiosk     | npm run gulp.kiosk.electron.watch | Iniciar electron para desenvolvimento com watch mode.                                 | Desenvolvimento |
|           | npm run gulp.kiosk.watch          | Iniciar angular compile em watch mode.                                                | Desenvolvimento |
|           | npm run gulp.kiosk.electron.aot   | Compilção do angular com header AOT e colocação de files necessários para o electron. | Productivo      |
|           | npm run kiosk.bundle              | Criação e compilação da aplicação electron para utilização em dispositvo desktop.     | Produtivo       |

**TODO**: Colocar comandos para cada aplicação quando forem criadas.

## Substituição de Portas COM

Para substituir as portas COM e definir novas é necessário aceder ao seguinte diretório:<br>

```
C:\Users\%username%\AppData\Roaming\@fi-sas\kiosk
```

Editar o ficheiro **preferences.json** substituindo a Porta COM que pretende.
Ou então pode simplesmente apagar o ficheiro **preferences.json** e voltar a executar a app.
<br><br>
Exemplo:

```
"SERIAL_PORT": "COM1", //RFID
"SERIAL_PORT_PRINTER": "COM3",  //IMPRESSORA
```

## Activar ou Desativar Carregamentos

Para ativar ou desativar os carregamentos é necessário aceder ao seguinte diretório:<br>

```
C:\Users\%username%\AppData\Roaming\@fi-sas\kiosk
```

Editar o ficheiro **preferences.json** alterando os campos "ACTIVATE_CHARGING","ACTIVATE_MOBILITY" e "ACTIVATE_REFECTORY" para true(caso pretenda ativar estas funcionalidades) ou false (caso as pretenda desativar.
Ou então pode simplesmente apagar o ficheiro **preferences.json** e voltar a executar a app.
<br><br>
Exemplo:

```
"ACTIVATE_CHARGING": true      //CARREGAMENTOS ATIVADO
"ACTIVATE_MOBILITY": false      //MOBILIDADE DESATIVADO
"ACTIVATE_REFECTORY": true      //REFEIÇÕES ATIVADA
```

## Passos para criação de aplicação Electron

Para criação de uma aplicação baseada em electron existem determinadas regras e configurações a cumprir para que o tooling desenvolvido funcione na perfeição.

Imagine que queremos criar uma aplicação para dispositivo ecrâ. Os passos a efectuar são:

Criar inicialmente uma nova aplicação angular.

```
ng generate app screen --routing --tags=screen
```

Após a criação da aplicação angular vamos adicionar a funcionalidade de electron. Para isso execute o seguinte comando com o argumento do nome da aplicação anteriormente criada:

```
npm run workspace-schematic electron-app screen
```

Agora a sua aplicação está pronta para ser desenvolvida. Este processo adicionou novos ficheiros e configuração á aplicação na qual de seguida explicamos a sua finalidade.

O processo adicionou no ficheiro package.json a configuração necessária para que o tooling baseado em gulp tenha as informações necessárias para o desenvolvimento.

```
{
"name": "screen",
"bundleName": "MY SCREEN",
"bundleVersion": "1.0.0",
"root": "apps/screen",
"type": "electron",
"electronDir": "src/electron",
"electronFile": "electron-app.js"
}
```

Legenda:

| Chave         | Descritivo                                                                       |
| ------------- | -------------------------------------------------------------------------------- |
| name          | O nome da aplicação é mandatório e tem de reflectir o nome da aplicação criada.  |
| bundleName    | Nome a usar aquando a criação do bundle para utilização de desktop.              |
| bundleVersion | Versão a usar para bundle produzido.                                             |
| root          | Path do directório criado pelo comando de criação da aplicação.                  |
| type          | Tipo de aplicação: 'electron'                                                    |
| electronDir   | Path do directório onde irá conter o o script de arranque da aplicação electron. |
| electronFile  | Nome do ficheiro responsável pelo arranque da aplicação.                         |

Foram também adcionado npm scripts para execução/bundle da aplicação.

```
"gulp.screen.electron.watch": "gulp screen:electron:watch",
"gulp.screen.watch": "gulp screen:compile:watch",
"gulp.screen.electron.aot": "gulp screen:compile:aot",
"screen.bundle": "NODE_ENV=production gulp screen:bundle",
"screen:test:ci": "NODE_ENV=test ng test screen --watch=false --browsers=ChromeHeadless"
```

Após todos estes passos completados estará apto para correr a aplicação em modo de desenvolvimento usando os comandos acima descritos em dois terminais diferentes. Apenas necessita ajustar os nomes dos scripts para os configurados:

```
Terminal1@: npm run gulp.screen.electron.watch
Terminal2@: npm run gulp.screen.watch
```

## Processo de desenvolvimento e regras

Para contribuir para o projecto é necessário ter em conta determinadas regras a cumprir. O processo utilizado é Scrum. Para cada novo device é estipulado uma sprint design na qual se fazem interações de como a feature vai ser implementada de forma a atingir o objectivo colocado pelo owner do projecto. Nesta sprint design são reunidas ideias, discussão aberta sobre elas definindo as aproachs a tomar bem como produção rápida de mocks e wireframes. No final é apresentado ao owner do projecto a solução e dá-se inicio a nivel de design a concepção final do wireframe.

No diário de uma sprint é convenção é sprints de uma semana com as cerimónias de apresentação e reunião da equipa no ultimo dia da sprint para que se discuta e apresente-se o produzido, o que correu bem ou mal, definição da próxima sprint com a recolha de tarefas do backlog e definição das mesmas para a area de sprint.

Como tools é usado uma board do [trello](https://trello.com/b/dHMJufeG/fisas) onde se encontra todas as tarefas e processo de scrum.

### Design

**TODO**: @alves

### Desenvolvimento

As nomenclaturas usadas a nivel de git são: [github flow](https://guides.github.com/introduction/flow/). Documento ilustrativo e resumido [aqui](./docs/githubflow-online.pdf).

![github flow](./docs/git-flow.jpg)

A nomenclatura apenas tem uma pequena derivação sendo o ramo para desenvolvimento `develop`. A partir deste ramo é criado uma nova branch que irá reflectir uma feature/fix etc... Sobre esta nova branch são comitadas todas as acções de desenvolvimento. As acções devem reflectir a seguinte _Definiton of Done_ :

* Mensagens de commits mediante [nomenclatura angular](https://gist.github.com/stephenparish/9941e89d80e2bc58a153)
* Código formatado pelo prettier
* Nomenclatura de nomes regidas pelo tslint
* Criação de unit tests
* Code review

Estando já em conclusão, o developer abre um _pull request_ da branch em causa para a branch `develop`. Nesta fase inicia-se o code review e discussão/partilha de conhecimento entre os intervenientes. Também aqui de uma forma automatizada o repositório irá proceder com o automatismo de execução de testes verificando se a branch tem a possibilidade de ser _merged_ no pedido efectuado.

No final, e depois de concordância entre os intervenientes o developer responsável/assignado deverá proceder ao merge do código para a branch de desenvolvimento (develop).

#### Exemplo

Vamos supor que existe a necessidade de se criar um slider de imagens. O processo inicia-se por definir as tarefas no trello mediante os requisitos apresentados pelo owner do projecto. Dito isto imaginesmo que o card da task é o numero: 25. Por convenção de boa prática ao criares um branch deves adicionar um prefixo, como estamos sobre frontend vamos usar FE como prefixo, isto é uma ideia, poderiamos usar por device, feature etc.

Antes de criar a branch é importante actualizar o nosso branch base de desenvolvimento, no caso `develop` para evitarmos conflitos na altura do merge.

```
git pull
```

De seguida vamos criar a branch:

```
git checkout -b FE-25-Slider
```

Depois disto estamos aptos para inciar todo o desenvolvimento do nosso slider na respectiva branch. Seguindo a [convenção de mensagens](https://conventionalcommits.org/) de commit do angular, imaginemos que estamos a finalizar o slider
e commitamos da seguinte forma:

```
git commit -m "feat(slider): Slider implementation and functionality."
```

Pós isto é necessário abrir o [pull request no gitlab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) para aprecissão/discussão/partilha de conhecimento. Não esquecer nos passos apenas de referenciar e assignar as pessoas para code review. Para mantermos o repo o mais limpo possivel selecione a opção de _Remove source branch when merge request is accepted_.

Após devidamente validade e aceite o developer assignado deve proceder ao merge e pós esta acção voltamos ao nosso branch de `develop` e voltamos a actualizar para recebermos todas as alterações que foram mesclados no acto o pull request. E com este ciclo terminado voltamos ao ponto inicial de criar uma nova branch para a feture seguinte.

### Nomenclatura de commits

A nomenclatura de commits obedece a regras, o commit segue o seguinte formato:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Tipos definidos:

* feat(feature)
* fix(bug fix)
* style(formatting)
* refactor(code change)
* test(add tests)
* build(build)
* ci(continuos integration)
* perf(performance)
* revert(revert code/feature)

`<subject>`: Texto a aplicar na mensagem.

* usar presente imperativo: "adição de feature ...."
* sem maisculas ao iniciar a frase
* não finalizar frase com ponto

`<body>`: Corpo da mensage

`<footer>`: Alterações drásticas que implicam quebrar a aplicação.

```
BREAKING CHANGES: Foi retirado a funcionalidade de input....
```

Referenciar card/issues (trello/gitlab):

```
Closes #25, #26
```

# Micro Serviços

https://docs.google.com/spreadsheets/d/1ZD31dVj4WIPMiMHUlSXsnOTaMfTsbl7uM_iR81cowJk/edit#gid=0

Acesso aos micro serviços é feito via VPN. Utilizador: leandrosa e pass: 14177317

* Micro serviço de autorização: 172.31.221.97:8080 (/api-docs)
* Micro serviço de configuração: 172.31.221.97:8090 (/api-docs)
* Micro serviço de media: 172.31.221.97:8070 (/api-docs)
* Micro serviço de comunicação: 172.31.221.97:8050 (/api-docs)
* Micro serviço de alojamento: 172.31.221.97:8040 (/api-docs)
* Micro serviço de pagamento: 172.31.221.97:8030 (/api-docs)
* Micro serviço de conta corrente: 172.31.221.97:8020 (/api-docs)
* Micro serviço de conta mobilidade: 172.31.221.97:7090 (/api-docs)
* Micro serviço de conta carregamentos: 172.31.221.97:8010 (/api-docs)

# Deploy

Para release é usado uma tooling que trata de incrementar a verãos mediante a definição SemVer, cria um chancelou com a nomenclatura usada no standard de mensagens e adiciona uma label ao ramo (master) para definir pontos de release na nossa linha produtiva.

Para proceder deve correr o seguinte script:

```
npm run release
```

Para mais informação sobre a tooling siga o [link](https://github.com/conventional-changelog/standard-version).

# Licenças

Todos os direitos de autoria estão reservados a...
