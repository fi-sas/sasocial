export * from './lib/configurator.module';
export {
  FiConfigurator,
  OPTIONS_TOKEN,
  Options
} from './lib/configurator.service';
