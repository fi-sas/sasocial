import { async, TestBed } from '@angular/core/testing';
import { FiUtilsModule } from './utils.module';

describe('UtilsModule', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiUtilsModule]
      }).compileComponents();
    })
  );

  it('should create', () => {
    expect(FiUtilsModule).toBeDefined();
  });
});
