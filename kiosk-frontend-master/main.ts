import { app, BrowserWindow, ipcMain, screen } from "electron";
import * as  child_process from 'child_process';
import * as path from "path";
import * as url from "url";
import * as updater from "electron-updater";
let win: BrowserWindow = null;
const autoUpdater = updater.autoUpdater;

const args = process.argv.slice(1),
  serve = args.some((val) => val === "--serve");

function createWindow(): BrowserWindow {
  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().bounds;

  // Create the browser window.
  win = new BrowserWindow({
    //x: 0,
    //y: 0,
    width: size.width,
    height: size.height,
    //kiosk: (serve) ? false: true,
    fullscreen: serve ? false : true,
    fullscreenable: true,
    frame: serve ? true : false,
    alwaysOnTop: serve ? false : true,
    resizable: serve ? true : false,
    webPreferences: {
      webSecurity: false,
      nodeIntegration: true,
      enableRemoteModule: true,
      allowRunningInsecureContent: serve ? true : false,
    },
  });

  app.commandLine.appendSwitch("disable-features", "OutOfBlinkCors");

  if (serve) {
    win.webContents.openDevTools();

    require("electron-reload")(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`),
    });
    win.loadURL("http://localhost:4203");
  } else {
    win.fullScreen = true;
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, "dist/index.html"),
        protocol: "file:",
        slashes: true,
      })
    );
  }

  // Emitted when the window is closed.
  win.on("closed", () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  win.webContents.on("before-input-event", (event, input) => {
    if (
      input.type === "keyUp" &&
      input.code === "KeyC" &&
      input.control === true &&
      input.alt === true
    ) {
      console.log("Event -> OPEN_CONFIG_PAGE");
      win.webContents.send("OPEN_CONFIG_PAGE");
    }

    if (
      input.type === "keyUp" &&
      input.code === "KeyI" &&
      input.control === true &&
      input.alt === true
    ) {
      win.webContents.openDevTools();
    }
  });

  initMainListener();

  return win;
}

/**
 * Hooks for electron main process
 */
function initMainListener() {
  ipcMain.on("ELECTRON_RELAUCH", () => {
    app.relaunch({ args: process.argv.slice(1).concat(["--relaunch"]) });
    app.exit(0);
  });

  ipcMain.on("OPEN_EXTERNAL_SERVICE", (event, arg) => {

    //var child = require("child_process").execFile;
    var executablePath = `start `;
     //start chrome -ArgumentList "--start-fullscreen --app=https://youtube.com"
    var parameters = ["chrome",  "--start-fullscreen", `--app=${arg}`];

    child_process.spawn(executablePath, parameters, {
      shell: true
    });


    /*child(executablePath, parameters, function (err, data) {
      console.log(err);
      console.log(data.toString());
    });*/
  });
}

try {
  app.allowRendererProcessReuse = true;

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
  app.on("ready", () => {
    /*const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python', ["C:\\Users\\leand\\Documents\\SASocial\\fisas-hub\\fi_hub_main.py"]);

    pythonProcess.stdout.on('data', function (data) {
      console.info(data.toString());
    });

    pythonProcess.stderr.on('data', (data) => {
      console.error(data.toString());
    });*/

    setTimeout(createWindow, 400);
  });

  // Quit when all windows are closed.
  app.on("window-all-closed", () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== "darwin") {
      app.quit();
    }
  });

  app.on("activate", () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });
} catch (e) {
  // Catch Error
  // throw e;
}

/**
 * AUTO UPDATER
 */

///////////////////
// Auto upadater //
///////////////////
// autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "Personal access Token" };

/*
autoUpdater.autoDownload = true;

autoUpdater.setFeedURL({
    provider: "generic",
    url: "https://gitlab.com/fi-sas/kiosk-frontend/-/jobs/artifacts/master/download?job=build"
});

autoUpdater.on('checking-for-update', function () {
    sendStatusToWindow('Checking for update...');
});

autoUpdater.on('update-available', function (info) {
    sendStatusToWindow('Update available.');
});

autoUpdater.on('update-not-available', function (info) {
    sendStatusToWindow('Update not available.');
});

autoUpdater.on('error', function (err) {
    sendStatusToWindow('Error in auto-updater.');
});

autoUpdater.on('download-progress', function (progressObj) {
    let log_message = "Download speed: " + progressObj.bytesPerSecond;
    log_message = log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%';
    log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
    sendStatusToWindow(log_message);
});

autoUpdater.on('update-downloaded', function (info) {
    sendStatusToWindow('Update downloaded; will install in 1 seconds');
});

autoUpdater.on('update-downloaded', function (info) {
    setTimeout(function () {
        autoUpdater.quitAndInstall();
    }, 1000);
});

autoUpdater.checkForUpdates();

function sendStatusToWindow(message) {
    console.log(message);
}`
*/
