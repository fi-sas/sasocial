import { environment } from '../environments/environment';

export const CONFIGURATION = {
  DEFAULT_LANG: 'pt',
  FEED: true,
  LONGITUDE: -8.83287,
  LATITUDE: 41.69323,
  LANGUAGES: [
    { acronym: 'pt', id: 3, name: 'Português' },
    { acronym: 'en', id: 4, name: 'English' }
  ],
  DOMAINS_API: [
    { HOST: environment.api_gateway, KEY: '@api_gateway' },
    { HOST: '', KEY: '@local' }
  ],
  ENDPOINTS: {
    AUTH: {
      AUTHORIZE_TOKEN: '@api_gateway:/api/authorization/authorize/device/:id',
      VALIDATE_TOKEN: '@api_gateway:/api/authorization/authorize/validate-token',
      LOGOUT: '@api_gateway:/api/authorization/authorize/logout/:type',
      REFRESH_TOKEN: '@api_gateway:/api/authorization/authorize/refresh-token/:type',
      GET_USER: '@api_gateway:/api/authorization/authorize/user',
      RANDOM_PIN: '@api_gateway:/api/authorization/users/randomPin'

    },
    CONFIG: {
      GET_CONFIG: '@api_gateway:/api/configuration/configuration',
      SET_CPUID: '@api_gateway:/api/configuration/devices/cpuid',
      SERVICES_KIOSK: '@api_gateway:/api/configuration/services/external_kiosk'
    },
    MEDIA: {
      UPLOADS: '@api_gateway:/media/',
      RESIZE: '@api_gateway:/media/files/:id/image/:width'
    },
    COMMUNICATION: {
      MEDIA: '@api_gateway:/api/communication/posts/medias-kiosk',
      TICKERS: '@api_gateway:/api/communication/posts/tickers'
    },
    ALIMENTATION: {
      SCHOOLS: '@api_gateway:/api/alimentation/menu/schools',
      SERVICES: '@api_gateway:/api/alimentation/menu/:school_id/services',
      CATEGORIES: '@api_gateway:/api/alimentation/menu/service/:service_id/families',
      MEALS:
        '@api_gateway:/api/alimentation/menu/service/:service_id/menus/:day/:type?withRelated=taxes,file',
      MEALDETAIL:
        '@api_gateway:/api/alimentation/menu/service/:service_id/dish/:dish_id?withRelated=taxes,file',
      CART_ADD_ALIMENTATION: '@api_gateway:/api/alimentation/menu/cart/refectory/:id/add'
    },
    BAR: {
      PRODUCTS: '@api_gateway:/api/alimentation/menu/service/:service_id/families/:family_id/products?withRelated=file',
      PRODUCTS_RECENT: '@api_gateway:/api/alimentation/menu/service/:service_id/recents?withRelated=file',
      CATEGORIES: '@api_gateway:/api/alimentation/menu/service/:service_id/families',
      CARD_ADD_BAR: '@api_gateway:/api/alimentation/menu/cart/bar/:id/add',
      GET_BAR: '@api_gateway:/api/alimentation/menu/service'
    },
    MOBILITY: {
      LOCALS: '@api_gateway:/api/bus/locals',
      ROUTE: '@api_gateway:/api/bus/route_search',
      CART_ADD: '@api_gateway:/api/bus/route_search/addCart',
    },
    CURRENT_ACCOUNT: {
      BALANCES: '@api_gateway:/api/current_account/movements/balances',
      MOVEMENTS: '@api_gateway:/api/current_account/movements',
      MOVEMENTS_CHARGE_ACCOUNT: '@api_gateway:/api/current_account/movements/chargeAccount',
    },
    PAYMENTS: {
      CART: {
        CREATE: '@api_gateway:/api/payments/cart',
        CART_CHECKOUT: '@api_gateway:/api/payments/cart/checkout',
        CLEAR: '@api_gateway:/api/payments/cart/clearcart',
        REMOVE_ITEM: '@api_gateway:/api/payments/cart/removeItem',
        CHANGE_QTD_ITEM: '@api_gateway:/api/payments/cart/changeQtdItem'
      },
      CHARGES: '@api_gateway:/api/payments/charges/chargeAccount',
      PAYMENT_ID: '@api_gateway:/api/payments/payments/:id',
      CONFIRM_TPA_PAYMENT: '@api_gateway:/api/payments/payment-method-tpa-kiosk/callback',
      TPA_PERIOD_EVENT: '@api_gateway:/api/payments/tpa_period'
    },
    QUEUE: {
      SERVICES: '@api_gateway:/api/queue/services/device?withRelated=subjects,translations,groups',
      ISSUETICKET: '@api_gateway:/api/queue/tickets/sequential'
    }
  },
  SCREEN_SAVER: {
    TIME_OUT: 120 * 1000
  },
  ORGANIZATION: {
    /* themes: IPVC, IPCA or IPB */
    THEME: 'IPVC',
    COLORS: {
      IPVC: [
        '#3faec0',
        '#1791a5',
        '#107e90',
        '#0f6776',
        '#0b5a67',
        '#084d59',
        '#06353c',
        '#041e22',
        '#031416',
        '#02090b'
      ],
      IPCA: [
        '#0f8156',
        '#076743',
        '#005133',
        '#02482e',
        '#013c26',
        '#013320',
        '#012819',
        '#011d12',
        '#04130d',
        '#010b07'
      ],
      IPB: [
        '#a5006a',
        '#870157',
        '#79024e',
        '#6e0347',
        '#680143',
        '#61003e',
        '#520035',
        '#330021',
        '#210417',
        '#15020e'
      ],
      IPCOIMBRA: [
        '#737474',
        '#666868',
        '#5A5C5D',
        '#4D5051',
        '#414445',
        '#34393A',
        '#282D2E',
        '#1B2122',
        '#0F1517',
        '#02090B'
      ],
      IPLEIRIA: [
        '#737474',
        '#666868',
        '#5A5C5D',
        '#4D5051',
        '#414445',
        '#34393A',
        '#282D2E',
        '#1B2122',
        '#0F1517',
        '#02090B'
      ],
      IPPORTALEGRE: [
        '#BF9F33',
        '#AA8E2F',
        '#957E2A',
        '#806D26',
        '#6B5C21',
        '#564C1D',
        '#413B18',
        '#2C2A14',
        '#171A0F',
        '#02090B'
      ],
      IPSANTAREM: [
        '#0F8156',
        '#076743',
        '#005133',
        '#02482E',
        '#013C26',
        '#013320',
        '#012819',
        '#011D12',
        '#04130D',
        '#010B07'
      ],
      IPVISEU: [
        '#B84E86',
        '#A44678',
        '#903F6B',
        '#7B375D',
        '#672F4F',
        '#532842',
        '#3F2034',
        '#2A1826',
        '#161119',
        '#02090B'
      ]
    }
  },
  DEVICE_ID: 7
};
