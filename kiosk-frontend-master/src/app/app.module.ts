import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FiAppComponent } from './app.component';
import { ApplicationRoutingModule } from './app.routing';
import { ApplicationProvisionModule } from './app.provision';

import { FiMediaModule } from '@fi-sas/kiosk/components/media/media.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiMenuModule } from '@fi-sas/kiosk/components/menu/main/menu.module';
import { FiHeaderModule } from '@fi-sas/kiosk/components/header/header.module';
import { FiFooterModule } from '@fi-sas/kiosk/components/footer/footer.module';
import { FiKeyboardModule } from './components/keyboard/keyboard.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';


import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiConditionService } from './services/condition.service';

export const COMPONENTS_MODULES = [
  FiMediaModule,
  FiTickerModule,
  FiMenuModule,
  FiFooterModule,
  FiHeaderModule,
  FiKeyboardModule,
];

const config: SocketIoConfig = { url: 'http://localhost:8080', options: {
  transports: ['websocket']
} };

@NgModule({
  declarations: [FiAppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApplicationProvisionModule,
    ...COMPONENTS_MODULES,
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    }),
    SocketIoModule.forRoot(config),
    AngularSvgIconModule.forRoot(),
    ApplicationRoutingModule,
  ],
  bootstrap: [FiAppComponent],
  providers: [FiConditionService],
  exports: [ApplicationProvisionModule, COMPONENTS_MODULES]
})
export class AppModule {
  constructor(
    private _appRef: ApplicationRef,
    private _translateLazyService: FiTranslateLazyService
  ) {
    console.group('APPLICATION REFERENCE');
    console.groupEnd();

    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
