/**
 * Application provision module.
 * Imports ShareModule, TranslateModule, NgZorroAntdModule and ConfiguratorModule.
 * Provides TranslateLazyService to define language in TranslateModule and NZ_I18N
 * on NgZorroAntdModule.
 */
import { Router } from '@angular/router';
import {
  NgModule,
  APP_INITIALIZER,
  Injector,
  APP_BOOTSTRAP_LISTENER,
  ComponentRef
} from '@angular/core';
import { DOCUMENT, registerLocaleData } from '@angular/common';
import { merge } from 'lodash';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import { CONFIGURATION } from './app.config';
import { FiShareModule } from '@fi-sas/share';
import {
  FiCoreModule,
  StateToken,
  ApplicationState,
  FiUrlService,
  FiResourceService,
  FiStorageService
} from '@fi-sas/core';
import { FiBalanceService } from '@fi-sas/kiosk/components/account/balance.service';
import { FiElectronService } from '@fi-sas/kiosk/services/electron.service';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiIdentifierGuardService } from '@fi-sas/kiosk/services/identifier-guard.service';
import {
  FiConfiguratorModule,
  OPTIONS_TOKEN,
  FiConfigurator
} from '@fi-sas/configurator';

import { INITIAL_APLICATION_STATE } from './app.state';
import { ApplicationInitializer } from './app.initializer';

import { TranslateModule, TranslateService } from '@ngx-translate/core';
import {
  NgZorroAntdModule,
  NZ_I18N,
  pt_PT,
  NzI18nService,
  NzModalService
} from 'ng-zorro-antd';

import localePt from '@angular/common/locales/pt';
import localeEn from '@angular/common/locales/en';
import { FiAuthService } from './services/auth.service';
import { FiScreenGuardService } from './services/screen-guard.service';
import { FiAuthPresenter } from './scenes/authentication/authentication.presenter';
import { FormBuilder } from '@angular/forms';
import { FiRoutingHistoryService } from '@fi-sas/kiosk/services/routing.service';
import { FiAuthenticationGuardService } from '@fi-sas/kiosk/services/auth-guard.service';
import { FiConfigGuardService } from '@fi-sas/kiosk/services/config-guard.service';
import { FiMenuPaymentService } from '@fi-sas/kiosk/services/menu-payment.service';
import { HttpsRequestInterceptor } from './services/http-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FiRfidService } from './services/rfid.service';
import { FiTpaService } from './services/tpa.service';
import { Socket } from 'ngx-socket-io';
import { FiMiddlewareListenerService } from './services/middleware.service';
import { FiPrintService } from '@fi-sas/kiosk/services/print.service';
import { FiShoppingCartService } from './components/cart/shopping-cart.service';
import { AppResolver } from './app.resolver';
import { FiKeyboardService } from './components/keyboard/keyboard.service';

registerLocaleData(localePt, 'pt-PT');
registerLocaleData(localeEn, 'en-US');

export function configFactory(electronService: FiElectronService) {
  return electronService.isReady()
    ? merge(CONFIGURATION, electronService.preferences.store)
    : CONFIGURATION;
}

export function initializerFactory(appInitializer: ApplicationInitializer) {
  return () => appInitializer.initialize();
}

export function bootstrapListenerFactory(configurator: FiConfigurator) {
  return (component: ComponentRef<any>) => {
    const theme = configurator.getOption('ORGANIZATION.THEME');
    const root = document.querySelector(':root') as any;
    const colors =
      (configurator.getOptionTree(
        `ORGANIZATION.COLORS.${theme}`,
        false
      ) as string[]) || [];

    colors.forEach((color, index) => {
      const num = index + 1;
      root.style.setProperty(`--fisas-${num > 9 ? num : '0' + num}`, color);
    });

    console.group('APPLICATION BOOTSTRAP LISTENER');
    console.dir(component);
    console.groupEnd();
  };
}

@NgModule({
  imports: [
    FiShareModule,
    NgZorroAntdModule,
    TranslateModule.forRoot(),
    FiConfiguratorModule,
    FiCoreModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: pt_PT },
    { provide: StateToken, useValue: INITIAL_APLICATION_STATE },
    {
      provide: FiTranslateLazyService,
      useClass: FiTranslateLazyService,
      deps: [TranslateService, NzI18nService]
    },
    {
      provide: FiAuthService,
      useClass: FiAuthService,
      deps: [FiUrlService, FiConfigurator, FiStorageService, FiResourceService]
    },
    {
      provide: FiAuthPresenter,
      useClass: FiAuthPresenter,
      deps: [FormBuilder, FiAuthService, FiRoutingHistoryService]
    },
    {
      provide: FiConditionService,
      useClass: FiConditionService,
      deps: [Router, FiConfigurator, DOCUMENT]
    },
    { provide: FiElectronService, useClass: FiElectronService },
    { provide: FiRfidService, useClass: FiRfidService },
    { provide: FiTpaService, useClass: FiTpaService },
    {
      provide: FiIdentifierGuardService,
      useClass: FiIdentifierGuardService,
      deps: [Router, FiAuthService]
    },
    {
      provide: FiMiddlewareListenerService,
      useClass: FiMiddlewareListenerService,
      deps: [Router, Socket]
    },
    {
      provide: FiPrintService,
      useClass: FiPrintService,
      deps: [Socket]
    },
    {
      provide: FiScreenGuardService,
      useClass: FiScreenGuardService,
      deps: [Router, ApplicationState]
    },
    {
      provide: FiAuthenticationGuardService,
      useClass: FiAuthenticationGuardService,
      deps: [Router, ApplicationState]
    },
    {
      provide: OPTIONS_TOKEN,
      useFactory: configFactory,
      deps: [FiElectronService]
    },
    {
      provide: ApplicationInitializer,
      useClass: ApplicationInitializer,
      deps: [FiElectronService, Injector]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializerFactory,
      multi: true,
      deps: [ApplicationInitializer]
    },
    {
      provide: APP_BOOTSTRAP_LISTENER,
      useFactory: bootstrapListenerFactory,
      multi: true,
      deps: [FiConfigurator]
    },
    {
      provide: FiRoutingHistoryService,
      useClass: FiRoutingHistoryService,
      deps: [Router]
    },
    {
      provide: FiConfigGuardService,
      useClass: FiConfigGuardService,
      deps: [FiUrlService, FiConfigurator, FiStorageService, FiResourceService]
    },
    {
      provide: FiShoppingCartService,
      useClass: FiShoppingCartService,
     // deps: [FiResourceService, FiStorageService, FiUrlService, FiTranslateLazyService]
    },
    {
      provide: FiBalanceService,
      useClass: FiBalanceService,
      deps: [FiConfigurator, FiResourceService]
    },
    {
      provide: FiMenuPaymentService,
      useClass: FiMenuPaymentService
    },
    {
      provide: HttpsRequestInterceptor,
      useClass: HttpsRequestInterceptor,
      deps: [NzModalService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsRequestInterceptor,
      multi: true
    },
    {
      provide: FiKeyboardService,
      useClass: FiKeyboardService,
      deps: [ApplicationState]
    },
    AppResolver,
  ],
  exports: [NgZorroAntdModule, FiShareModule, TranslateModule, FiCoreModule]
})
export class ApplicationProvisionModule {
  /**
   * Create instance of the module.
   *
   * @param {TranslateLazyService} _translateLazyService
   * @param {FisConfigurator} _configurator
   * @param {Router} _router
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService,
    private _configurator: FiConfigurator,
    private _router: Router,
    private _routerHistoryService: FiRoutingHistoryService
  ) {
    this.init();
    this.networkListeners();
    this.registerRoutingHistory();
  }

  /**
   * Set language in TranslateService and NzI18nService.
   */
  private init(): void {
    const langAcronym: string = this._configurator.getOption(
      'DEFAULT_LANG',
      'pt'
    );

    this._translateLazyService.setLanguage(langAcronym);
  }

  private networkListeners() {
    fromEvent(window, 'offline')
      .pipe(map(e => this._router.navigate(['unavailable'])))
      .subscribe(() => console.dir('OFFLINE'));

    fromEvent(window, 'online')
      .pipe(map(e => this._router.navigate(['/'])))
      .subscribe(() => console.dir('ONLINE'));
  }

  private registerRoutingHistory() {
    this._routerHistoryService.loadRouting();
  }
}
