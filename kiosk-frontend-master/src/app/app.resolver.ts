import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Resource } from '@fi-sas/core';

import { first, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ConfigurationsService } from './services/configurations.service';

@Injectable()
export class AppResolver
  implements Resolve<Observable<any>> {
  constructor(private configurationGeralService: ConfigurationsService) {}

  resolve() {
    localStorage.removeItem("configuraction_active_menu_kiosk");
    return this.configurationGeralService.geral().pipe(
      first(), 
      tap(results => {
        localStorage.setItem("configuraction_active_menu_kiosk", JSON.stringify(results[0].services));
        })
  );
  }
}
