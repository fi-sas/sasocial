/**
 * Application Routing Module
 *
 * All routes are defined in this module. Lazy load of
 * routing is configured to use.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FiIdentifierGuardService } from './services/identifier-guard.service';
import { FiScreenGuardService } from './services/screen-guard.service';
import { FiAuthenticationGuardService } from '@fi-sas/kiosk/services/auth-guard.service';
import { FiConfigGuardService } from '@fi-sas/kiosk/services/config-guard.service';
import { FiMiddlewareListenerService } from './services/middleware.service';
import { AppResolver } from './app.resolver';

const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: '@fi-sas/kiosk/scenes/home/home.module#FiHomeModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService,
      FiScreenGuardService,
      FiAuthenticationGuardService,
      FiConfigGuardService
    ]
  },
  {
    path: 'dashboard',
    resolve: [AppResolver],
    pathMatch: 'full',
    loadChildren:
      '@fi-sas/kiosk/scenes/dashboard/dashboard.module#FiDashboardModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService]
  },
  {
    path: 'authentication',
    pathMatch: 'full',
    loadChildren:
      '@fi-sas/kiosk/scenes/authentication/authentication.module#FiAuthenticationModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService,
      FiAuthenticationGuardService]
  },
  {
    path: 'install',
    pathMatch: 'full',
    loadChildren: '@fi-sas/kiosk/scenes/install/install.module#FiInstallModule',
    canActivate: [
      FiMiddlewareListenerService
    ]
  },
  {
    path: 'unavailable',
    pathMatch: 'full',
    loadChildren:
      '@fi-sas/kiosk/scenes/unavailable/unavailable.module#FiUnavailableModule'
  },
  {
    path: 'refectory',
    resolve: [AppResolver],
    loadChildren:
      '@fi-sas/kiosk/scenes/refectory/refectory.module#FiFoodModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService]
  },
  {
    path: 'bar',
    resolve: [AppResolver],
    loadChildren:
      '@fi-sas/kiosk/scenes/bar/bar.module#FiBarModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  {
    path: 'queue',
    resolve: [AppResolver],
    pathMatch: 'full',
    loadChildren:
      '@fi-sas/kiosk/scenes/queue/queue.module#FiQueueModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService]
  },
  {
    path: 'cart',
    resolve: [AppResolver],
    loadChildren: '@fi-sas/kiosk/scenes/cart/cart.module#FiCartSceneModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  {
    path: 'pospayment',
    resolve: [AppResolver],
    loadChildren: '@fi-sas/kiosk/scenes/pospayment/pospayment.module#FiPosPaymentSceneModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  {
    path: 'charging',
    resolve: [AppResolver],
    loadChildren:
      '@fi-sas/kiosk/scenes/charging/charging.module#FiChargingModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  {
    path: 'mobility',
    resolve: [AppResolver],
    loadChildren:
      '@fi-sas/kiosk/scenes/mobility/mobility.module#FiMobilityModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  {
    path: 'current-account', 
    resolve: [AppResolver],
    loadChildren:
      '@fi-sas/kiosk/scenes/current-account/current-account.module#FiCurrentAccountModule',
    canActivate: [
      FiMiddlewareListenerService,
      FiIdentifierGuardService
    ]
  },
  /* for testing purposes */
  {
    path: 'play',
    loadChildren: '@fi-sas/kiosk/scenes/play/play.module#FiPlayModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, {
      enableTracing: false,
      initialNavigation: 'enabled',
      useHash: false,
    })
  ],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
