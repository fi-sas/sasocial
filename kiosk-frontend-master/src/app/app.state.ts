import { FormControlName } from '@angular/forms';

export interface KioskState {
  ui: {
    keyboard: {
      active: string;
      element?: HTMLElement;
      control?: FormControlName;
    };
    header: {
      active: boolean;
      ticker: boolean;
    };
    footer: {
      active: boolean;
    };
    menu: {
      logged?: boolean;
      back?: boolean;
      authentication?: boolean;
      active?: boolean;
    };
    screensaver: boolean;
    loading: boolean;
  };
  user: {};
}

export const INITIAL_APLICATION_STATE: KioskState = {
  ui: {
    keyboard: {
      active: 'off',
      element: null,
      control: null
    },
    header: {
      active: true,
      ticker: true
    },
    footer: {
      active: true
    },
    menu: {
      logged: false,
      back: false,
      authentication: false,
      active: true
    },
    screensaver: true,
    loading: false
  },
  user: null
};
