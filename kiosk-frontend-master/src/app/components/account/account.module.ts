import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiBalanceSmallModule } from './balance-small/balance.module';
import { FiMovementModule } from './movement/item.module';

import PT_TRANSLATION_BSMALL from './balance-small/pt.translation.json';
import EN_TRANSLATION_BSMALL from './balance-small/en.translation.json';

@NgModule({
  imports: [
  	NgZorroAntdModule,
  	TranslateModule,
  	FiShareModule,
  	FiDirectivesModule,
    FiBalanceSmallModule,
    FiMovementModule
  ],
  exports: [
    FiBalanceSmallModule,
    FiMovementModule
  ]
})
export class FiAccountModule {
  constructor(
  	private _translateLazyService: FiTranslateLazyService
  ) {
    this._translateLazyService.load(PT_TRANSLATION_BSMALL, 'pt');
    this._translateLazyService.load(EN_TRANSLATION_BSMALL, 'en');
  }
}
