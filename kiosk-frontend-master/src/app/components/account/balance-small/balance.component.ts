import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FiBalanceService, Balance } from '../balance.service';
import { get, has } from 'lodash';

@Component({
  selector: 'fi-balance-small',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.less']
})
export class FiBalanceSmallComponent implements OnInit, OnDestroy, OnChanges {
  /* Public propreties */
  public language: string;
  public balances: Balance[];

  @Input() public reload = false;

  /* Private propreties */
  private _subject = new Subject<boolean>();
  private listener;

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   * @param {FiBalanceService}       private _balanceService$
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService,
    private _balanceService$: FiBalanceService
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    this.balances = [];

    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }

    this.getBalance();

    this.listener = window.addEventListener(
      'RELOAD_BALANCE',
      this.getBalance.bind(this)
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (has(changes, 'reload.currentValue')) {
      const reload = get(changes, 'reload.currentValue', null);

      if (reload) {
        this.getBalance();
      }
    }
  }

  getBalance() {
    this._balanceService$
      .getBalances()
      .pipe(takeUntil(this._subject))
      .subscribe(balances => {
        this.balances = balances;
      });

    this._balanceService$.pipe(takeUntil(this._subject)).subscribe(data => {
      this.balances = data;
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if (this.listener) {
      this.listener.removeListener('RELOAD_BALANCE', this.getBalance);
    }
    //this._subject.next(true);
    //this._subject.unsubscribe();
  }
}
