import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs'
import { map, timeout, catchError } from 'rxjs/operators';
import { sortBy } from 'lodash';
import { FiResourceService, FiUrlService, FiStorageService } from '@fi-sas/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { HttpParams } from '@angular/common/http';

/* interfaces */
export interface Balance {
  account_id: number,
  account_name?:string,
  current_balance: number,
  in_debt: number,
  available_methods: any[]
}

@Injectable()
export class FiBalanceService extends BehaviorSubject<any> {

  /* public properties */
  public balances:Balance[] = [{
    account_id: 0,
    account_name: 'Guest',
    current_balance: 0,
    in_debt: 0,
    available_methods: []
  }];

  /* private properties */
  private _lang = this._translateService.getLanguage();
  private _authData = null;

  /**
   * Constructor
   * @param {FiConfigurator} private _configurator
   * @param {FiResourceService} private _resourceService
   */
  constructor(
  private _resourceService: FiResourceService,
  private _translateService: FiTranslateLazyService,
  private _storageService: FiStorageService,
  private _urlService: FiUrlService
  ) {
    super(null);
  }

  /**
   * Get balances
   */
  getBalances() {

    const url = this._urlService.get('CURRENT_ACCOUNT.BALANCES');
    this._authData = this._storageService.get<{ token: string }>('DEVICE_TOKEN');
    this._lang = this._translateService.getLanguage();
    
    return this._resourceService
    .list<any>(url, {
        params: new HttpParams().append('withRelated', 'available_methods'),
        headers: {
          Authorization: 'Bearer ' + this._authData.token,
          'X-Language-Acronym': this._lang
        }
      })
      .pipe(
        map(res => {
          /* truncate name */
          /*res.data.map((item) => {
            const index = item.account_name.substr(0, 20).lastIndexOf(' ');
            if (index !== -1) {
              item.account_name = item.account_name.substr(0, index);
            }
          });*/

          this.balances = res.data;
          this.balances = sortBy(this.balances, item => item.account_id)
          this.next(this.balances);

          return this.balances;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

}