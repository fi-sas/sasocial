import { Component, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';

import { coerceNumberProperty } from '@fi-sas/utils';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

export enum QuantityAction {
  INCREMENT = 'INCREMENT',
  DECREMENT = 'DECREMENT',
  REMOVE = 'REMOVE'
}

@Component({
  selector: 'fi-current-account-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less']
})
export class FiMovementComponent {
  /**
   * Output
   */
  @Output()
  quantityChange = new Subject<{ id: string; action: QuantityAction; }>();

  /**
   * Input: id, price, vat, quantity
   */
  @Input()
  set id(id: string) {
    this._id = id;
  }
  get id() {
    return this._id;
  }

  @Input()
  set total_value(value: number) {
    this._total_value = value;
  }
  get total_value() {
    return this._total_value;
  }

  @Input()
  set price(price: number) {
    this._price = coerceNumberProperty(price, 0);
  }
  get price() {
    return this._price;
  }

  @Input()
  set vat(vat: number) {
    this._vat = coerceNumberProperty(vat, 0);
  }
  get vat() {
    return this._vat;
  }

  @Input()
  set quantity(qt: number) {
    this._quantity = coerceNumberProperty(qt, 0);
  }
  get quantity() {
    return this._quantity;
  }

  @Input()
  set balance(balance: number) {
    this._balance = coerceNumberProperty(balance, 0);
  }
  get balance() {
    return this._balance;
  }

  @Input()
  set status(status: number) {
    this._status = Number(status);
  }
  get status() {
    return this._status;
  }

  @Input()
  set operation(operation: number) {
    this._operation = Number(operation);
  }
  get operation() {
    return this._operation;
  }

  @Input()
  set account(account:string) {
    this._account = account;
  }
  get account() {
    return this._account;
  }
  
  @Input()
  set transaction_value(transaction_value:number) {
    this._transaction_value = transaction_value;
  }
  get transaction_value() {
    return this._transaction_value;
  }

  /**
   * Properties
   */
  private _id = null;
  private _price = 0;
  private _vat = 0;
  private _quantity = 0;
  private _balance = 0;
  private _status = 0;
  private _operation = 0;
  private _account = '';
  private _transaction_value = 0;
  private _total_value = 0;

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService
  ) {}

  /**
   * Calculate VAT
   */
  vatCalc() {
    return ((this.price - (this.price / (1 + this.vat))) * this.quantity);
  }

  /**
   * Handler quantity increase
   * @param {UIEvent} ev
   */
  handlerQtIncrease(ev: UIEvent) {

    ev.preventDefault();

    if (this.quantity < 99) {
      this.quantity++;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.INCREMENT
      });
    }

  }

  /**
   * Handler quantity decrease
   * @param {UIEvent} ev
   */
  handlerQtDecrease(ev: UIEvent) {

    ev.preventDefault();

    if (this.quantity > 0) {
      this.quantity--;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.DECREMENT
      });
    }

  }

  /**
   * Handler remove product
   * @param {UIEvent} ev
   */
  handlerRemoveProduct(ev: UIEvent) {
      this.quantity = 0;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.REMOVE
      });
  }

}
