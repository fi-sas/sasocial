import { NgModule } from '@angular/core';
import { FiShopBigModule } from './shop-big/shop-big.module';

@NgModule({
  imports: [FiShopBigModule],
  exports: [FiShopBigModule]
})
export class FiButtonsModule {}
