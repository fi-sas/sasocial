import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'fi-shop-big',
  templateUrl: 'shop-big.component.html',
  styleUrls: ['shop-big.component.less']
})
export class FiShopBigComponent implements OnInit {

  @Input('loading') public loading: string;
  @Input('type') public type: string;

  @Input('quantity')
  set quantity(qt: any) {
    this._quantity = Number(qt);
  }
  get quantity() {
    return this._quantity;
  }

  @Input('isUserLogged') isUserLogged: boolean = false;

  @Input('price') public price: string;
  @Input('quantProd') public quantProd: number;

  @Output() OnQtChange = new EventEmitter<{ action: 'add' | 'remove', quantity: 0 }>();

  private _quantity = 0;
  modalNoLogged = false;

  /**
   * Constructor
   */
  constructor() { }

  /**
   * On init
   */
  ngOnInit() {

  }

  /**
   * Handler quantity change
   * @param {UIEvent} event
   */
  handlerQtCHange(event: UIEvent): void {

    event.preventDefault();

    if (this._quantity > 0) {
      this._quantity = this._quantity - 1;
      this.OnQtChange.emit({ action: 'remove', quantity: this.quantity });
    }

  }

  /**
   * Handler price change
   * @param {UIEvent} event
   */
  handlerPrice(event: UIEvent) {
    event.preventDefault();

    if (this.quantity < this.quantProd) {
      this._quantity += 1;
      this.OnQtChange.emit({ action: 'add', quantity: this.quantity });
    }


  }

  modalOpen() {
    this.modalNoLogged = true;
    setTimeout(() => this.modalNoLogged = false, 4000);
  }
}
