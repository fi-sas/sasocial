import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { TranslateModule } from '@ngx-translate/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiShopBigComponent } from './shop-big.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    AngularSvgIconModule,
    TranslateModule,
    FiShareModule
  ],
  declarations: [
    FiShopBigComponent
  ],
  exports: [
    FiShopBigComponent
  ]
})
export class FiShopBigModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
