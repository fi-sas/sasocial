import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { FiCartItemModule } from './item/item.module';
import { FiCartSummaryModule } from './summary/summary.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION_SUMMARY from './summary/pt.translation.json';
import EN_TRANSLATION_SUMMARY from './summary/en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiDirectivesModule,
    FiCartSummaryModule,
    FiCartItemModule
  ],
  exports: [FiCartSummaryModule, FiCartItemModule]
})
export class FiCartModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION_SUMMARY, 'pt');
    this._translateLazyService.load(EN_TRANSLATION_SUMMARY, 'en');
  }
}
