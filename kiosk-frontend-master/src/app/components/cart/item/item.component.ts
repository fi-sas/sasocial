import { Component, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { coerceNumberProperty } from '@fi-sas/utils';

export enum QuantityAction {
  INCREMENT = 'INCREMENT',
  DECREMENT = 'DECREMENT',
  REMOVE = 'REMOVE'
}

@Component({
  selector: 'fi-cart-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.less']
})
export class FiCartItemComponent {
  /**
   * Output
   */
  @Output()
  quantityChange = new Subject<{ id: string; action: QuantityAction; quantity: number }>();


  
  /**
   * Input: id, price, vat, quantity
   */
  @Input()
  set id(id: string) {
    this._id = id;
  }
  get id() {
    return this._id;
  }

  @Input()
  set price(price: number) {
    this._price = coerceNumberProperty(price, 0);
  }
  get price() {
    return this._price;
  }
  @Input()
  set vat(vat: number) {
    this._vat = coerceNumberProperty(vat, 0);
  }
  get vat() {
    return this._vat;
  }

  @Input()
  set quantity(qt: number) {
    this._quantity = coerceNumberProperty(qt, 0);
  }
  get quantity() {
    return this._quantity;
  }
  @Input('totalValue') public totalValue: number;
  @Input('vatValue') public vatValue: number;
  @Input('stock') public stock: number;


  /**
   * Properties
   */
  private _id = null;
  private _price = 0;
  private _vat = 0;
  private _quantity = 0;

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   */
  constructor(
  ) {}


  /**
   * Handler quantity increase
   * @param {UIEvent} ev
   */
  handlerQtIncrease(ev: UIEvent) {
    ev.preventDefault();
    
    if (this.quantity < this.stock) {
      this.quantity++;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.INCREMENT,
        quantity: this.quantity
      });
    }

  }

  /**
   * Handler quantity decrease
   * @param {UIEvent} ev
   */
  handlerQtDecrease(ev: UIEvent) {

    ev.preventDefault();

    if (this.quantity > 0) {
      this.quantity--;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.DECREMENT,
        quantity: this.quantity
      });
    }

  }

  /**
   * Handler remove product
   * @param {UIEvent} ev
   */
  handlerRemoveProduct(ev: UIEvent) {
      this.quantity = 0;
      this.quantityChange.next({
        id: this.id,
        action: QuantityAction.REMOVE,
        quantity: this.quantity
      });
  }

}
