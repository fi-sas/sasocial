import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiCartItemComponent } from './item.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    RouterModule,
    FiShareModule
  ],
  declarations: [FiCartItemComponent],
  exports: [FiCartItemComponent]
})
export class FiCartItemModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
