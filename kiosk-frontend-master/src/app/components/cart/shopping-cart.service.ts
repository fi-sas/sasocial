import { Inject, Injectable } from "@angular/core";
import { FiTranslateLazyService } from "@fi-sas/kiosk/services/translate.lazy.service";
import { BehaviorSubject, of, Subject } from "rxjs";
import { catchError, first, map, timeout, takeUntil } from "rxjs/operators";
import { FiResourceService } from "../../../../libs/core/src/lib/resource.service";
import { FiStorageService } from "../../../../libs/core/src/lib/storage.service";
import { FiUrlService } from "../../../../libs/core/src/lib/url.service";
import { KioskState } from "@fi-sas/kiosk/app.state";
import { ApplicationState, Store } from "@fi-sas/core";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class FiShoppingCartService {
  private authData = null;
  public isUserLogged = false;
  cartTotalPrice$ = new BehaviorSubject<number>(0);
  cartTotal$ = new BehaviorSubject<number>(0);
  cartData$ = new BehaviorSubject([]);
  private _subject = new Subject<boolean>();

  constructor(
    private resourceService: FiResourceService,
    private storageService: FiStorageService,
    private urlService: FiUrlService,
    private _translateService: FiTranslateLazyService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    const storeUser = this._appState.createSlice("user");
    storeUser
      .watch()
      .pipe(takeUntil(this._subject))
      .subscribe((user) => {
        this.isUserLogged = !!user;
        if (!this.isUserLogged) {
          this.cartData$.next([]);
          this.cartTotal$.next(0);
          this.cartTotalPrice$.next(0);
        } else {
          this.getDataNotification();
        }
      });
  }

  addToCartAlimentation(id: number) {
    const url = this.urlService.get("ALIMENTATION.CART_ADD_ALIMENTATION", {
      id,
    });
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.create<any>(
      url,
      {},
      {
        headers: {
          Authorization: "Bearer ".concat(this.authData.token),
        },
      }
    );
  }

  /**
   * removes all items from an account's cart
   * @param id - account id
   */
  removeItemsCart(id: number) {
    let body: any = {};
    body.account_id = id;
    const url = this.urlService.get("PAYMENTS.CART.CLEAR");
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.create<any>(url, body, {
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  /**
   * removes an item from the cart
   * @param id - item id
   */
  removeItemCard(id) {
    const params = new HttpParams().set("item_id", id.toString());
    const url = this.urlService.get("PAYMENTS.CART.REMOVE_ITEM");
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.delete(url, {
      params: params,
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  getAllProducts() {
    const params = new HttpParams()
      .set("sort", "-account_id")
      .set("limit", "-1");

    const url = this.urlService.get("PAYMENTS.CART.CREATE");

    return this.resourceService
      .list<any>(url, {
        params,
        ...this.storageToken(),
      })
      .pipe(
        map((response) => response.data),
        timeout(30000),
        catchError(() => of([]))
      );
  }

  getDataNotification(): void {
    this.getAllProducts()
      .pipe(first())
      .subscribe(
        (result) => {
          let total = 0;
          let listData = [];
          let finalPrice = 0;
          for (const cart of result) {
            total += cart.items.length;
            for (const item of cart.items) {
              listData.push(item);
              finalPrice += item.total_value;
            }
          }
          this.cartTotalPrice$.next(finalPrice);
          this.cartData$.next(result);
          this.cartTotal$.next(total);
        },
        (error) => {}
      );
  }

  addToCartBar(id: number) {
    let body: any = {};
    body.product_quantity = 1;
    const url = this.urlService.get("BAR.CARD_ADD_BAR", { id });
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.create<any>(url, body, {
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  addToCartMobility(hash: string) {
    const  body = {
      hash
    };
    const url = this.urlService.get("MOBILITY.CART_ADD", { });
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.create<any>(url, body, {
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  checkout(id) {
    let body: any = {};
    body.account_id = id;
    const url = this.urlService.get("PAYMENTS.CART.CART_CHECKOUT");
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.update<any>(url, body, {
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  totalValueCart(cart): number {
    let finalPrice = 0;
    for (const item of cart.items) {
      finalPrice += Number(item.total_value);
    }
    return finalPrice;
  }

  changeQtdItem(item_id: string, quantity: number) {
    let body: any = {};
    body.item_id = item_id;
    body.quantity = quantity;
    const url = this.urlService.get("PAYMENTS.CART.CHANGE_QTD_ITEM");
    this.authData = this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this.resourceService.update<any>(url, body, {
      headers: {
        Authorization: "Bearer ".concat(this.authData.token),
      },
    });
  }

  /**
   * Storage token
   */
  private storageToken() {
    let _lang = this._translateService.getLanguage();
    const storageToken =
      this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return {
      headers: {
        Authorization: "Bearer " + storageToken.token,
        "X-Language-Acronym": _lang,
      },
    };
  }
}
