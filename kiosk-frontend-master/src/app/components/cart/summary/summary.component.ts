import { Component, Inject, OnInit } from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiShoppingCartService } from '../shopping-cart.service';
import { KioskState } from '@fi-sas/kiosk/app.state';
import { ApplicationState, Store } from '@fi-sas/core';
import { finalize, first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Balance, FiBalanceService } from '../../account/balance.service';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';
import { FiAuthPresenter } from '@fi-sas/kiosk/scenes/authentication/authentication.presenter';
import { ScreenAction } from '@fi-sas/kiosk/services/screen.reducer';
import { UserAction } from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '../../menu/main/menu.reducer';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';

@Component({
  selector: 'fi-cart-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.less']
})
export class FiCartSummaryComponent implements OnInit {
  public language: string;
  private _subject = new Subject<boolean>();
  public isUserLogged = false;
  loadingCheckout = false;
  total = 0;
  cartObjects;
  cartTotalPrice = 0;
  existRefectory = false;
  public accounts: Balance[];

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   */
  constructor(
    private _conditionService: FiConditionService,
    private _translateLazyService: FiTranslateLazyService,
    public shoppingCartService: FiShoppingCartService,
    private balanceService: FiBalanceService,
    public authPresenter: FiAuthPresenter,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    const storeUser = this._appState.createSlice('user');
    storeUser
      .watch()
      .pipe(takeUntil(this._subject))
      .subscribe(user => (this.isUserLogged = !!user));
    if (this.isUserLogged) {
      this.shoppingCartService.cartTotal$.subscribe(
        (total) => {
          this.total = total;
        }
      );
      this.shoppingCartService.cartData$.subscribe(
        (cartObjects) => {
          this.cartObjects = cartObjects;
        }
      );
      this.shoppingCartService.cartTotalPrice$.subscribe(
        (cartTotalPrice) => {
          this.cartTotalPrice = cartTotalPrice;
        }
      );

      this.shoppingCartService.getDataNotification();
      this.balanceService.getBalances().pipe(first()).subscribe((data)=> {
        this.accounts = data;
      });
    }
  }

  /**
   * On init
   */
  ngOnInit() {
    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }
  }

  translateMoney(numb: number) {
    return (Number(numb.toString()).toFixed(2)).split('.').join(',') + '€';
  }


  getCurrentBalance(id): number {
    if (this.accounts) {
      for (const account of this.accounts) {
        if (id === account.account_id) {
          return account.current_balance;
        }
      }
    }
    return 0;
  }

  countTotalValue(cart): number {
    return this.shoppingCartService.totalValueCart(cart);
  }

}
