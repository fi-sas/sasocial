import {
  Component,
  Input,
  AfterViewInit,
  ContentChild,
  ElementRef
} from '@angular/core';

interface ContextInterface {
  title: string;
  subtitle: string;
}

@Component({
  selector: 'fi-context-large-block',
  templateUrl: './large-block.component.html',
  styleUrls: ['./large-block.component.less']
})
export class FiContextLargeBlockComponent implements AfterViewInit {
  @Input('data') data: ContextInterface = { title: null, subtitle: null };
  @ContentChild('largeBlock', { read: ElementRef })
  public largeBlockText;

  ngAfterViewInit() {}
}
