import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiContextLargeBlockComponent } from './large-block.component';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [NgZorroAntdModule, FiShareModule, FiDirectivesModule,TranslateModule],
  declarations: [FiContextLargeBlockComponent],
  exports: [FiContextLargeBlockComponent]
})
export class FiContextLargeBlockModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
