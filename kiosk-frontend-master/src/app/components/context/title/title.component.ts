import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'fi-context-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.less']
})
export class FiContextTitleComponent implements OnInit {

	/* Inputs */
	@Input('title')
	title:string;

	@Input('bottomBorder')
	bottomBorder:boolean;

	/* Public propreties */
	public showBottomBorder = true;

	/**
	 * On init
	 */
	ngOnInit() {
		this.showBottomBorder = this.bottomBorder;
	}

}
