import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

import { FiContextTitleComponent } from './title.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    RouterModule,
    NgZorroAntdModule,
    FiShareModule,
    TranslateModule,
    FiDirectivesModule
  ],
  declarations: [
  	FiContextTitleComponent
  ],
  exports: [
  	FiContextTitleComponent
  ]
})
export class FiContextTitleModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
