import { Component, Input, AfterViewInit } from '@angular/core';

interface TitleInterface {
  mainTitle: string,
  secondaryTitle: string,
  secondarySubtitle: string,
  secondaryLabel: string
  type: string;
}

@Component({
  selector: 'fi-context-two-col',
  templateUrl: './two-col.component.html',
  styleUrls: ['./two-col.component.less']
})
export class FiContextTwoColComponent implements AfterViewInit {

  @Input()
  data: TitleInterface = null;

  ngAfterViewInit() {

  }

  getName(name): string {
    return name.length>30 ? (name.substring(0,20) + '...') : name;
  }

}

