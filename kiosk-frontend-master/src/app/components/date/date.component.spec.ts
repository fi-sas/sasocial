import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FiDateComponent } from './date.component';

describe('> Date Component', () => {
  let component: FiDateComponent;
  let fixture: ComponentFixture<FiDateComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [TranslateModule.forRoot({})],
        declarations: [FiDateComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('# Should create date component instance.', () => {
    expect(component).toBeTruthy();
  });
});
