import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fi-date, [fi-date]',
  templateUrl: 'date.component.html',
  styleUrls: ['./date.component.less']
})
export class FiDateComponent implements OnInit, OnDestroy {
  public currentDate: number = Date.now();

  public locale: string;
  public dateSubscribe: Subscription;

  public today = new Date(this.currentDate);
  public lastDayOfMarch = new Date(new Date().getFullYear() + '-03-31');
  public lastDayOfOctober = new Date(new Date().getFullYear() + '-10-31');
  public lastSundayOfMarch = new Date(
    this.lastDayOfMarch.setDate(
      this.lastDayOfMarch.getDate() - this.lastDayOfMarch.getDay()
    )
  );
  public lastSundayOfOctober = new Date(
    this.lastDayOfOctober.setDate(
      this.lastDayOfOctober.getDate() - this.lastDayOfOctober.getDay()
    )
  );

  public timezone = 'UTC+0';

  private _interval: NodeJS.Timer;

  constructor(
    @Inject(TranslateService) private _translateService: TranslateService
  ) {}

  ngOnInit() {
    const currentLang = this._translateService.currentLang || 'pt';

    this.setTimezone();

    this.dateSubscribe = this._translateService.onLangChange.subscribe(
      (event: LangChangeEvent) => {
        this.locale = event.lang + '-' + event.lang.toUpperCase();
      }
    );

    this.locale = currentLang + '-' + currentLang.toUpperCase();

    this._interval = setInterval(() => {
      this.currentDate = Date.now();
      this.setTimezone();
    }, 5000);
  }

  ngOnDestroy() {
    if (this.dateSubscribe) {
      this.dateSubscribe.unsubscribe();
    }

    clearInterval(this._interval);
  }

  setTimezone() {
    this.today = new Date(this.currentDate);
    this.timezone =
      this.today >= this.lastSundayOfMarch &&
      this.today < this.lastSundayOfOctober
        ? 'UTC+1'
        : 'UTC+0';
  }
}
