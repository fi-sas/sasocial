import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiDateComponent } from './date.component';

@NgModule({
  imports: [FiShareModule, TranslateModule],
  declarations: [FiDateComponent],
  exports: [FiDateComponent]
})
export class FiDateModule {}
