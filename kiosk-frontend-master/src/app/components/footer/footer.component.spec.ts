import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StateToken, FiCoreModule } from '@fi-sas/core';
import { INITIAL_APLICATION_STATE } from '@fi-sas/kiosk/app.state';

import { FiFooterComponent } from './footer.component';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

describe('FooterComponent', () => {
  let component: FiFooterComponent;
  let fixture: ComponentFixture<FiFooterComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          FiCoreModule,
          TranslateModule.forRoot(),
          BrowserAnimationsModule
        ],
        declarations: [FiFooterComponent],
        providers: [
          { provide: StateToken, useValue: INITIAL_APLICATION_STATE },
          {
            provide: FiTranslateLazyService,
            useClass: FiTranslateLazyService,
            deps: [TranslateService]
          }
        ]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(FiFooterComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
        });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
