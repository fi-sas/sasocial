import { Component, OnInit, Input, Inject, OnChanges } from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/kiosk/app.state';
import {
  FooterActionDisable,
  FooterActionEnable,
  FooterDisableReduce,
  FooterEnableReduce
} from './footer.reducer';


import { FiElectronService } from '@fi-sas/kiosk/services/electron.service';
import { IpcRendererEvent } from 'electron';
@Component({
  selector: 'fi-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[class.hidden]': '!enable'
  }
})
export class FiFooterComponent implements OnInit, OnChanges {
  @Input() public language: string;

  public enable: boolean;

  public isPrinterActive = false;
  public isCashActive = false;

  constructor(
    private _translateLazyService: FiTranslateLazyService,
    private _electronService: FiElectronService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    const store = this._appState.createSlice('ui').createSlice('footer');

    store.addReducer(FooterActionEnable, FooterEnableReduce);
    store.addReducer(FooterActionDisable, FooterDisableReduce);
  }

  ngOnInit() {
    const store = this._appState.createSlice('ui').createSlice('header');

    store.watch().subscribe(state => {
      this.enable = state.active;
    });

    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }

    this._electronService.on(
      'ELECTRON_PRINTER',
      (ev: IpcRendererEvent, connected: boolean) => {
        this.isPrinterActive = connected;
      }
    );

    this._electronService.on(
      'ELECTRON_CASH',
      (ev: IpcRendererEvent, state: boolean) => {
        this.isCashActive = state;
      }
    );
  }

  changeLanguage(event: MouseEvent) {
    event.preventDefault();

    this.language = this.language === 'pt' ? 'en' : 'pt';
    this._translateLazyService.setLanguage(this.language);
  }


  ngOnChanges(){
    this._translateLazyService.getService().onLangChange.subscribe((data) => {
     this.language=data.lang;
    })
  }

  handlerPrinter(ev: UIEvent) {
    ev.preventDefault();

    this._electronService.send('ELECTRON_PRINT', {
      type: 'Almoço',
      food: 'Carne',
      date: '26-jun-2019',
      price: '2.85'
    });
  }

  /*  activateCasher() {
    this._electronService.send(
      'ELECTRON_CASH_COMMAND',
      'CASH_TOTALBLOCKING 0 \n\r'
    );
    this.isCashActive = true;
  }

  deactivateCasher() {
    this._electronService.send(
      'ELECTRON_CASH_COMMAND',
      'CASH_TOTALBLOCKING 1 \n\r'
    );
    this.isCashActive = false;
  } */
}
