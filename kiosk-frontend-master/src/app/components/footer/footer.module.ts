import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { FiShareModule } from '@fi-sas/share';
import { FiFooterComponent } from './footer.component';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    FiShareModule,
    NgZorroAntdModule,
    TranslateModule
  ],
  declarations: [FiFooterComponent],
  exports: [FiFooterComponent]
})
export class FiFooterModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
