import { Action, Reducer } from '@fi-sas/core';

export const FooterActionEnable = new Action<{ active: boolean }>(
  'FOOTER_ENABLE'
);
export const FooterActionDisable = new Action<{ active: boolean }>(
  'FOOTER_DISABLE'
);

export const FooterEnableReduce: Reducer<
  {
    active: boolean;
  },
  {
    active: boolean;
  }
> = (state: { active: boolean }, payload: { active: boolean }) =>
  (state = payload);

export const FooterDisableReduce: Reducer<
  { active: boolean },
  { active: boolean }
> = (state: { active: boolean }, payload: { active: boolean }) =>
  (state = payload);
