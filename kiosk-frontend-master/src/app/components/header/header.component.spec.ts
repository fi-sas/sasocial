import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiHeaderComponent } from './header.component';

import {
  StateToken,
  FiCoreModule,
  FiUrlService,
  FiResourceService,
  FiStorageService
} from '@fi-sas/core';
import { INITIAL_APLICATION_STATE } from '@fi-sas/kiosk/app.state';
import { FiDateModule } from '@fi-sas/kiosk/components/date/date.module';
import { FiWeatherModule } from '@fi-sas/kiosk/components/weather/weather.module';
import { FiClockModule } from '@fi-sas/kiosk/components/clock/clock.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiTickerService } from '@fi-sas/kiosk/components/ticker/ticker.service';

xdescribe('> Header Component', () => {
  let component: FiHeaderComponent;
  let fixture: ComponentFixture<FiHeaderComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          FiDateModule,
          FiWeatherModule,
          FiClockModule,
          FiCoreModule,
          RouterModule,
          FiTickerModule,
          TranslateModule.forRoot()
        ],
        declarations: [FiHeaderComponent],
        providers: [
          { provide: StateToken, useValue: INITIAL_APLICATION_STATE },
          {
            provide: FiTranslateLazyService,
            useClass: FiTranslateLazyService,
            deps: [TranslateService]
          },
          {
            provide: FiTickerService,
            useClass: FiTickerService,
            deps: [
              FiUrlService,
              FiResourceService,
              FiTranslateLazyService,
              FiStorageService
            ]
          }
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('# Should create header component instance.', () => {
    expect(component).toBeTruthy();
  });
});
