import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenAction } from '@fi-sas/kiosk/services/screen.reducer';
import { UserAction } from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { ApplicationState, Store } from '@fi-sas/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { KioskState } from '@fi-sas/kiosk/app.state';
import {
  HeaderActionEnable,
  HeaderEnableReduce,
  HeaderActionDisable,
  HeaderDisableReduce
} from './header.reducer';
import { Observable } from 'rxjs';
import {
  TickerInterface,
  FiTickerService
} from '@fi-sas/kiosk/components/ticker/ticker.service';
import { take, map } from 'rxjs/operators';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

@Component({
  selector: 'fi-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[class.hidden]': '!enable'
  }
})
export class FiHeaderComponent implements OnInit {
  public tickers$: Observable<TickerInterface[]>;

  public enable: boolean;
  public enableTicker = true;
  viewFeed: boolean;
  title = null;

  /**
   * Constructor
   * @param {Router} private _router
   * @param {FiTickerService} private _tickerService
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiConfigurator}    private _configurator
   * @param {Store<KioskState>} @Inject(ApplicationState) private _appState
   */
  constructor(
    private _router: Router,
    private _tickerService: FiTickerService,
    private _translateService: FiTranslateLazyService,
    private _configurator: FiConfigurator,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    const store = this._appState.createSlice('ui').createSlice('header');

    store.addReducer(HeaderActionEnable, HeaderEnableReduce);
    store.addReducer(HeaderActionDisable, HeaderDisableReduce);
  }

  /**
   * On init
   */
  ngOnInit() {
    const store = this._appState.createSlice('ui').createSlice('header');

    this.title = this._configurator
      .getOption<string>('ORGANIZATION.THEME', 'IPVC')
      .toUpperCase();
    this.viewFeed = this._configurator
      .getOption<boolean>('FEED', true);

    store.watch().subscribe(state => {
      this.enable = state.active;
      this.enableTicker = state.ticker;
    });

    this.tickers$ = this.requestTickers();

    this._translateService.getService().onLangChange.subscribe(data => {
      this.enableTicker = true;
      this._tickerService.forceReload();

      this.tickers$ = this.requestTickers();
    });
  }

  /**
   * Request tickers
   */
  requestTickers() {
    return this._tickerService.getTickers().pipe(
      map(tickers => {
        tickers && tickers.length > 0 
          ? (this.enableTicker = true)
          : (this.enableTicker = false);

        return tickers;
      }),
      map(tickers => {
        return tickers.filter(data => data.translations && data.translations.length>0)
      }),
      take(1)
    );
  }

  /**
   * Go to screen saver
   */
  goToScreenSaver() {
    ScreenAction.next(true);
    UserAction.next(null);
    MenuAction.next({ authentication: false, logged: false });

    this._router.navigate(['/']);
  }
}
