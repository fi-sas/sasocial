import { Action, Reducer } from '@fi-sas/core';

export const HeaderActionEnable = new Action<{ active: boolean, ticker: boolean }>(
  'HEADER_ENABLE'
);
export const HeaderActionDisable = new Action<{ active: boolean, ticker: boolean }>(
  'HEADER_DISABLE'
);

export const HeaderEnableReduce: Reducer<
  {
    active: boolean,
    ticker: boolean
  },
  {
    active: boolean,
    ticker: boolean
  }
> = (state: { active: boolean, ticker: boolean }, payload: { active: boolean, ticker: boolean }) =>
  (state = payload);

export const HeaderDisableReduce: Reducer<
  { active: boolean, ticker: boolean },
  { active: boolean, ticker: boolean }
> = (state: { active: boolean, ticker: boolean }, payload: { active: boolean, ticker: boolean }) =>
  (state = payload);
