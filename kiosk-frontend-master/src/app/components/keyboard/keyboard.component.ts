import {
  Component,
  Directive,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { FiKeyboardService } from "./keyboard.service";

export enum KeyboardType {
  All = "all",
  Keyboard = "keyboard",
  Numpad = "numpad",
}


@Directive({
  selector: "fi-keyboard-input, [fi-keyboard-input]",
})
export class FiKeyboardInputDirective {
  constructor(
    private _elRef: ElementRef,
    private _keyboardService: FiKeyboardService
  ) {
    this._elRef.nativeElement.addEventListener("focus", (ev) => {
      this._keyboardService.status.next({
        visible: true,
        input: this._elRef.nativeElement as HTMLInputElement,
        keyboardType: KeyboardType.Keyboard,
      });
    });
  }
}

@Directive({
  selector: "fi-keyboard-input-all, [fi-keyboard-input-all]",
})
export class FiKeyboardInputAllDirective {
  constructor(
    private _elRef: ElementRef,
    private _keyboardService: FiKeyboardService
  ) {
    this._elRef.nativeElement.addEventListener("focus", (ev) => {
      this._keyboardService.status.next({
        visible: true,
        input: this._elRef.nativeElement as HTMLInputElement,
        keyboardType: KeyboardType.All,
      });
    });
  }
}

@Directive({
  selector: "fi-keyboard-input-numpad, [fi-keyboard-input-numpad]",
})
export class FiKeyboardInputNumpadDirective {
  constructor(
    private _elRef: ElementRef,
    private _keyboardService: FiKeyboardService
  ) {
    this._elRef.nativeElement.addEventListener("focus", (ev) => {
      this._keyboardService.status.next({
        visible: true,
        input: this._elRef.nativeElement as HTMLInputElement,
        keyboardType: KeyboardType.Numpad,
      });
    });
  }
}


@Component({
  selector: "fi-sas-keyboard",
  templateUrl: "./keyboard.component.html",
  styleUrls: ["./keyboard.component.less"],
})
export class FiKeyboardComponent implements OnInit {
  @ViewChild("keyboardContainer") keyboardContainer: ElementRef = null;

  keyboardStatus: { visible: boolean, input: HTMLInputElement, keyboardType: KeyboardType} = null;

  KeyboardType = KeyboardType;
  currentType: KeyboardType = KeyboardType.All;
  isUpperCase = false;
  isVisible = false;
  showSpecials = false;
  clickListenerActive = false;

  currentInput: HTMLInputElement = null;

  specialsRows = [
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
    ["@", "#", "€", "_", "&", "-", "+", "(", ")", "/"],
    ["*", '"', "'", ":", ";", "!", "?", "$", "%"],
    ["{", "}", "\\", "ç", "=", "[", "]", "."],
  ];

  keyboardRows = [
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
    ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
    ["a", "s", "d", "f", "g", "h", "j", "k", "l"],
    [ "z", "x", "c", "v", "b", "n", "m",],
  ];
  upperKeyboardRows = [
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
    ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
    ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
    ["Z", "X", "C", "V", "B", "N", "M"],
  ];

  constructor(
    private keyboarService: FiKeyboardService,
    private renderer: Renderer2
  ) {
    this.closeWhenClickOutside();

    keyboarService.status.subscribe((status) => {
      this.keyboardStatus = status;

      this.isVisible = status.visible;
      this.currentInput = status.input,
      this.currentType = status.keyboardType;
      this.showSpecials = false;

      setTimeout(() => {
        this.clickListenerActive = true;
      }, 200);
    });
  }

  ngOnInit(): void {}

  isSomeChild(element: HTMLElement, children: HTMLCollection): boolean {
    for(const child of Array.from(children)) {
      if(child === element) {
        console.log("isSomeChild 1")
        console.log(child)
        return true;
      }
    }

    for(const child of Array.from(children)) {
      if(this.isSomeChild(element, child.children)) {
        console.log("isSomeChild 2")
        return true;
      }
    }

    return false;
  }

  closeWhenClickOutside() {
    this.renderer.listen("window", "click", (e: Event) => {

      if (this.keyboardContainer && this.clickListenerActive === true) {
        if (
          (
            e.target as HTMLElement !== this.keyboardContainer.nativeElement &&
            e.target as HTMLElement !== this.currentInput &&
            !this.isSomeChild(e.target as HTMLElement, this.keyboardContainer.nativeElement.children)
          )
        ) {
          this.keyboarService.status.next({ visible: false, input: null, keyboardType: KeyboardType.All });

        } else {
          this.currentInput.focus();
        }
      }
    });
  }

  toggleShift(e: Event,) {
    if(e) {
      e.preventDefault();
      e.stopPropagation();
      e.returnValue = false;
      e.cancelBubble = true;
    }

    this.isUpperCase = !this.isUpperCase;
  }

  toggleSpecials(e: Event,) {
    if(e) {
      e.preventDefault();
      e.stopPropagation();
      e.returnValue = false;
      e.cancelBubble = true;
    }

    this.showSpecials = !this.showSpecials;
  }


  clickKey(e: Event, key: string) {
    if(e) {
      e.preventDefault();
      e.stopPropagation();
      e.returnValue = false;
      e.cancelBubble = true;
    }

    if(this.currentInput) {
      const start = this.currentInput.selectionStart;
      const end = this.currentInput.selectionEnd;
      this.currentInput.value = this.currentInput.value.slice(0, start).concat(key).concat(this.currentInput.value.slice(end));
      this.currentInput.selectionStart = this.currentInput.selectionEnd = start + key.length;
      this.currentInput.focus();
    }
  }

  backspace(e: Event,) {
    if(e) {
      e.preventDefault();
      e.stopPropagation();
      e.returnValue = false;
      e.cancelBubble = true;
    }

    if(this.currentInput && this.currentInput.value) {
      const start = this.currentInput.selectionStart;
      const end = this.currentInput.selectionEnd;
      this.currentInput.value = this.currentInput.value.slice(0, start - 1).concat(this.currentInput.value.slice(end));
      this.currentInput.selectionStart = this.currentInput.selectionEnd = start - 1;

      this.currentInput.focus();
    }
  }

  space(e: Event) {
    this.clickKey(e, ' ');
  }

  clickKeyNumpad(e: Event, num: number) {
    this.clickKey(e, num.toString());
  }
}
