import { FiKeyboardModule } from './keyboard.module';

describe('KeyboardModule', () => {
  let keyboardModule: FiKeyboardModule;

  beforeEach(() => {
    keyboardModule = new FiKeyboardModule();
  });

  it('should create an instance', () => {
    expect(keyboardModule).toBeTruthy();
  });
});
