import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import {
  FiKeyboardComponent,
  FiKeyboardInputAllDirective,
  FiKeyboardInputDirective,
  FiKeyboardInputNumpadDirective,
} from './keyboard.component';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule
  ],
  declarations: [
    FiKeyboardInputDirective,
    FiKeyboardInputAllDirective,
    FiKeyboardInputNumpadDirective,
    FiKeyboardComponent
  ],
  exports: [
    FiKeyboardComponent,
    FiKeyboardInputDirective,
    FiKeyboardInputAllDirective,
    FiKeyboardInputNumpadDirective
  ]
})
export class FiKeyboardModule {}
