import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiMediaComponent } from './media.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import {
  FiCoreModule,
  StateToken,
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FiShareModule } from '@fi-sas/share';
import { INITIAL_APLICATION_STATE } from '@fi-sas/kiosk/app.state';
import { FiMediaService } from './media.service';
import { of, defer } from 'rxjs';

export function fakeAsyncResponse<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

xdescribe('MediaComponent', () => {
  let component: FiMediaComponent;
  let fixture: ComponentFixture<FiMediaComponent>;

  const mediaServiceStub = {
    get() {
      return fakeAsyncResponse({ data: [] });
    }
  };

  beforeEach(
    async(() => {
      // const mediaService = jasmine.createSpyObj('FiMediaService', ['getMedia']);
      // mediaService.getMedia.and.returnValue(of({ data: [] }));

      TestBed.configureTestingModule({
        imports: [
          FiShareModule,
          NgZorroAntdModule,
          FiCoreModule,
          TranslateModule.forRoot()
        ],
        declarations: [FiMediaComponent],
        providers: [
          { provide: StateToken, useValue: INITIAL_APLICATION_STATE },
          {
            provide: FiTranslateLazyService,
            useClass: FiTranslateLazyService,
            deps: [TranslateService]
          },
          {
            provide: FiMediaService,
            useValue: mediaServiceStub
          }
        ]
      }).compileComponents();
    })
  );

  beforeEach(async () => {
    fixture = TestBed.createComponent(FiMediaComponent);
    component = fixture.componentInstance;
    await fixture.whenStable();
    fixture.detectChanges();
  });

  it(
    'should create',
    async(() => {
      expect(component).toBeTruthy();
    })
  );
});
