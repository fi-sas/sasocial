import { FiMediaModule } from './media.module';

describe('MediaModule', () => {
  let mediaModule: FiMediaModule;

  beforeEach(() => {
    mediaModule = new FiMediaModule();
  });

  it('should create an instance', () => {
    expect(mediaModule).toBeTruthy();
  });
});
