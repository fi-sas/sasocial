import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiMediaComponent } from './media.component';
import { FiMediaService } from './media.service';
import {
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

@NgModule({
  imports: [FiShareModule, NgZorroAntdModule],
  declarations: [FiMediaComponent],
  exports: [FiMediaComponent],
  providers: [
    {
      provide: FiMediaService,
      useClass: FiMediaService,
      deps: [
        FiResourceService,
        FiUrlService,
        FiTranslateLazyService,
        FiStorageService
      ]
    }
  ]
})
export class FiMediaModule {}
