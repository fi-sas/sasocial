import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { FiPrintService } from '@fi-sas/kiosk/services/print.service';
import { FiRoutingHistoryService } from '@fi-sas/kiosk/services/routing.service';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { ActivatedRoute } from '@angular/router';
import { get } from 'lodash';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';

@Component({
  selector: 'fi-invoice-menu',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.less']
})
export class FiMenuInvoiceComponent implements OnInit {
  /* public */
  public accountUser = 1;
  @Input() public value = 0;
  public redirectTo: string | null = null;
  public count;

  /* private */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {FiChargingService}         private    _chargingService
   * @param {FiRoutingHistoryService}   private    _routeHistoryService
   * @param {FiConditionService}        private    _conditionService
   * @param {ActivatedRoute}            private    _activeRouter
   */
  constructor(
    private _printService: FiPrintService,
    private _routeHistoryService: FiRoutingHistoryService,
    private _conditionService: FiConditionService,
    private _activeRouter: ActivatedRoute
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this._activeRouter.queryParams.subscribe(params => {
      this.redirectTo = get(params, 'redirect', null);
      this.accountUser = get(params, 'accountUser', 1);
    });

  }

  /**
   *  Charge Account with money
   */
  proceedPaymentWithoutTIN() {
    LoadingAction.next(true);
 
  }
  
  /**
   *  Charge Account with money
   */
  proceedPaymentWithTIN() {
    LoadingAction.next(true);
 
  }

  /**
   * Back
   * @param {UIEvent} event
   */
  back(event: UIEvent) {
    event.preventDefault();

    let previous = this._routeHistoryService.getPreviousUrl();
    previous = previous.substring(1);

    if (previous !== '') {
      this._conditionService.goto(previous);
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy() { }
}
