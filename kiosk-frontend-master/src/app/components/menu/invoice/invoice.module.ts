import { NgModule } from '@angular/core';
import { FiMenuInvoiceComponent } from './invoice.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { TranslateModule } from '@ngx-translate/core';
import { FiShareModule } from '../../../../../libs/share/src/lib/share.module';
import { FiAccountModule } from '../../account/account.module';
import { FiBalanceSmallModule } from '../../account/balance-small/balance.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    AngularSvgIconModule,
    TranslateModule,
    FiShareModule,
    FiAccountModule,
    FiBalanceSmallModule
  ],
  declarations: [
    FiMenuInvoiceComponent
  ],
  exports: [
    FiMenuInvoiceComponent
  ],
})
export class FiMenuInvoiceModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}