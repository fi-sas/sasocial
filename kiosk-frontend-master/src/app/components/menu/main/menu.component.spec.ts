import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FiMenuComponent } from './menu.component';

xdescribe('MenuComponent', () => {
  let component: FiMenuComponent;
  let fixture: ComponentFixture<FiMenuComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [AngularSvgIconModule],
        declarations: [FiMenuComponent]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(FiMenuComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
        });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
