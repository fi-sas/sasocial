import {
  Component,
  Directive,
  HostBinding,
  ViewChild,
  TemplateRef,
  Inject,
  OnInit,
  AfterViewInit,
  ChangeDetectorRef,
  Input,
} from '@angular/core';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';

import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/kiosk/app.state';

import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { FiRoutingHistoryService } from '@fi-sas/kiosk/services/routing.service';

import { FiAuthPresenter } from '@fi-sas/kiosk/scenes/authentication/authentication.presenter';

import {
  ScreenAction,
  screenReduce,
} from '@fi-sas/kiosk/services/screen.reducer';
import {
  UserAction,
  userAuthReduce,
} from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction, menuReduce } from './menu.reducer';
import { FiRfidService } from '@fi-sas/kiosk/services/rfid.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { PinModalComponent } from '../../pin-modal/pin-modal.component';
import { TranslateService } from '@ngx-translate/core';

@Directive({
  selector: 'fi-menu-item, [fi-menu-item]',
})
export class FiMenuItemDirective {
  @HostBinding('class.menu__item') isMenuItem = true;
}

@Component({
  selector: 'fi-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[class.component-hide]': '!enable',
  },
})
export class FiMenuComponent implements OnInit, AfterViewInit {
  @ViewChild('defaultMenu') private defaultTemplateRef: TemplateRef<any>;

  @ViewChild('authTemplate') private authTemplateRef: TemplateRef<any>;

  @ViewChild('loggedTemplate') private loggedTemplateRef: TemplateRef<any>;

  @Input() enable = true;

  /* public */
  activeTemplate: TemplateRef<any>;

  user$: any;
  menu$: any;

  _pinModalRef: NzModalRef;

  /**
   * Constructor
   * @param {FiAuthPresenter}         public                    authPresenter
   * @param {ChangeDetectorRef}       private                   _cdRef
   * @param {FiConditionService}      private                   _conditionService
   * @param {FiRoutingHistoryService} private                   _routeHistoryService
   * @param {FiRfidService}       private                   _rfidService
   * @param {Store<KioskState>}       @Inject(ApplicationState) private              _appState
   */
  constructor(
    public authPresenter: FiAuthPresenter,
    private _cdRef: ChangeDetectorRef,
    private _conditionService: FiConditionService,
    private _routeHistoryService: FiRoutingHistoryService,
    private _rfidService: FiRfidService,
    private _modalService: NzModalService,
    private _translate: TranslateService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    const storeUser = this._appState.createSlice('user');
    const storeMenu = this._appState.createSlice('ui').createSlice('menu');
    const storeScreen = this._appState
      .createSlice('ui')
      .createSlice('screensaver');

    storeUser.addReducer(UserAction, userAuthReduce);
    storeMenu.addReducer(MenuAction, menuReduce);
    storeScreen.addReducer(ScreenAction, screenReduce);

    this.user$ = storeUser.watch();
    this.menu$ = storeMenu.watch();

    storeMenu.watch().subscribe((state) => {
      this.enable = state.active;

      if (state.authentication && !state.logged) {
        this.activeTemplate = this.authTemplateRef;
      } else if (state.logged) {
        this.activeTemplate = this.loggedTemplateRef;
      } else {
        this.activeTemplate = this.defaultTemplateRef;
      }

      this._cdRef.detectChanges();
    });

    this._rfidService.getRfidObservable().subscribe((rfid) => {
      if (this._pinModalRef) {
        this._pinModalRef.close();
      }

      this._pinModalRef = this._modalService.create({
        nzTitle: this._translate.instant('PIN_MODAL.TITLE'),
        nzContent: PinModalComponent,
        nzMask: true,
        nzMaskClosable: true,
        nzClosable: false,
        nzWidth: 260,
        nzComponentParams: {
          rfid
        },
        nzFooter: null,
        nzWrapClassName: 'vertical-center-modal',
      });
    });
  }

  /**
   * After view init
   */
  ngAfterViewInit() {
    this.activeTemplate = this.defaultTemplateRef;
    this._cdRef.detectChanges();
  }

  /**
   * Go to authentication
   * @param {UIEvent} event
   */
  goToAuthentication(event: UIEvent) {
    event.stopPropagation();
    this._conditionService.goto('authentication');
  }

  /**
   * Go to dashboard
   * @param {UIEvent} event
   */
  goToDashboard(event: UIEvent) {
    event.stopPropagation();
    this._conditionService.goto('dashboard');
  }

  /**
   * Handler authenticate
   * @param {UIEvent} event
   */
  handlerAuthenticate(event: UIEvent) {
    event.preventDefault();
    this.authPresenter
      .authenticateByPin()
      .pipe(
        concatMap((logged) =>
          logged ? this.authPresenter.currentUser() : of({ data: null })
        )
      )
      .subscribe((response) => {
        if (response && response.data) {
          UserAction.next({ user: response.data[0] });
          MenuAction.next({ authentication: false, logged: true });

          this.authPresenter.authenticatePinForm.reset();

          this._conditionService.goto('dashboard');
        }
      });
  }

  /**
   * Handler unauthenticate
   * @param {UIEvent} event [description]
   */
  handlerUnauthenticate(event: UIEvent) {
    event.preventDefault();

    this.authPresenter.destroySession().subscribe((rs: any) => {
      ScreenAction.next(true);
      UserAction.next(null);
      MenuAction.next({ authentication: false, logged: false });

      this._conditionService.gotoScreenSaver();
    });
  }

  /**
   * Back
   * @param {UIEvent} event
   */
  back(event: UIEvent) {
    event.preventDefault();

    let previous = this._routeHistoryService.getPreviousUrl();
    previous = previous.substring(1);

    if (previous !== '') {
      this._conditionService.goto(previous);
    }
  }


}
