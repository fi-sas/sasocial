import { NgModule } from '@angular/core';

import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiAccountModule } from '@fi-sas/kiosk/components/account/account.module';
import { FiBalanceSmallModule } from '@fi-sas/kiosk/components/account/balance-small/balance.module';
import { FiMenuComponent, FiMenuItemDirective } from './menu.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { PinModalComponent } from '../../pin-modal/pin-modal.component';

@NgModule({
  imports: [
    NgZorroAntdModule,
    AngularSvgIconModule,
    TranslateModule,
    FiShareModule,
    FiAccountModule,
    FiBalanceSmallModule,
  ],
  declarations: [
    FiMenuComponent,
    FiMenuItemDirective,
    PinModalComponent,
  ],
  exports: [
    FiMenuComponent,
    FiMenuItemDirective,
  ]
})
export class FiMenuModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
