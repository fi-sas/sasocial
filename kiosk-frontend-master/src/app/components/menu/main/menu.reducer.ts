import { Action, Reducer } from '@fi-sas/core';

export const MenuAction = new Action<{
  logged?: boolean;
  authentication?: boolean;
  back?: boolean;
  active?: boolean;
}>('MENU_ACTION');

export const menuReduce: Reducer<
  {
    logged?: boolean;
    authentication?: boolean;
    back?: boolean;
    active?: boolean;
  },
  {
    logged?: boolean;
    authentication?: boolean;
    back?: boolean;
    active?: boolean;
  }
> = (state, payload) => {
  return { ...state, ...payload };
};
