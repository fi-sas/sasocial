import { Component, OnInit } from '@angular/core';
import { FiRoutingHistoryService } from '@fi-sas/kiosk/services/routing.service';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import {
  FiBalanceService,
  Balance
} from '@fi-sas/kiosk/components/account/balance.service';
import { FiShoppingCartService } from '../../cart/shopping-cart.service';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';
import { FiAuthPresenter } from '@fi-sas/kiosk/scenes/authentication/authentication.presenter';
import {
  ScreenAction,
} from '@fi-sas/kiosk/services/screen.reducer';
import {
  UserAction
} from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '../main/menu.reducer';
/* interfaces */
interface PaymentsCart {
  account_id: number;
  user_id: number;
  id: string;
  payment_method_id: string;
  items: CartProductItem[];
}

interface CartProductItem {
  service_id: number;
  product_code: string;
  article_type: string;
  description: string;
  quantity: number;
  vat_id: number;
  cart_id: string;
  expired: boolean;
  id: string;
  liquid_unit_value: number;
  liquid_value: number;
  location: string;
  name: string;
  total_value: number;
  unit_value: number;
  vat: number;
  extra_info: ExtraInfo[];
}

interface ExtraInfo {
  refectory_meal_type: string;
  refectory_meal_category: string;
  menu_dish_id: number;
  product_id: number;
}

@Component({
  selector: 'fi-payment-menu',
  templateUrl: 'payment-menu.component.html',
  styleUrls: ['payment-menu.less']
})
export class FiPaymentMenuComponent implements OnInit {
  existRefectory = false;
  public accounts: Balance[];
  cartData: PaymentsCart[] = [];
  modalSuccess = false;
  modalError = false;
  loadingCheckout = false;
  /**
   * Constructor
   */
  constructor(
    private _conditionService: FiConditionService,
    private _routeHistoryService: FiRoutingHistoryService,
    private _balanceService$: FiBalanceService,
    private shoppingCartService: FiShoppingCartService,
    private router: Router,
    public authPresenter: FiAuthPresenter,
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this.getItemsCart();
    this.accounts = this._balanceService$.balances;

  }

  getItemsCart() {
    this.shoppingCartService.cartData$.subscribe(
      (cartData) => {
        this.cartData = cartData;
      }
    );
  }

  /**
   * Back
   * @param {UIEvent} event
   */
  back(event: UIEvent) {
    event.preventDefault();

    let previous = this._routeHistoryService.getPreviousUrl();
    previous = previous.substring(1);

    if (previous !== '') {
      this._conditionService.goto(previous);
    }
  }

  clickCharging() {
    this.router.navigateByUrl('/charging');
  }

  payTotalCart(exit) {
    this.existRefectory = false;
    let count = 0;
    if (this.cartData.length == 0) {
      return;
    } else {
      this.cartData.forEach(cart => {
        if (cart.items.length == 0) {
          return;
        } else {
          if (cart.items.find((car) => car.article_type == "REFECTORY")) {
            this.existRefectory = true;
          }
          if (Number((this.getCurrentBalance(cart.account_id)).toFixed(2)) - Number(this.countTotalValue(cart).toFixed(2)) < 0) {
            count += 1;
          }
        }
      });
      if (count > 0) {
        this.modalError = true;
        setTimeout(() => this.modalError = false, 4000);
      } else {
        this.cartData.forEach(cart => {
          if (cart.items.length == 0) {
            return;
          } else {
            this.goToCheckout(cart.account_id, exit);
          }
        });
      }

    }
  }

  getCurrentBalance(id): number {
    if (this.accounts) {
      for (const account of this.accounts) {
        if (id === account.account_id) {
          return account.current_balance;
        }
      }
    }
    return 0;
  }

  countTotalValue(cart): number {
    return this.shoppingCartService.totalValueCart(cart);
  }

  goToCheckout(accountId: number, exit) {
    this.loadingCheckout = true;
    LoadingAction.next(true);
    this.shoppingCartService.checkout(accountId)
      .pipe(
        first(),
        finalize(() => {
          this.loadingCheckout = false;
          LoadingAction.next(false)
        })
      )
      .subscribe(
        (result) => {
          this.modalSuccess = true;
          setTimeout(() => {
            this.modalSuccess = false;
            if (exit) {
              this.authPresenter.destroySession().subscribe((rs: any) => {
                ScreenAction.next(true);
                UserAction.next(null);
                MenuAction.next({ authentication: false, logged: false });
  
                this._conditionService.gotoScreenSaver();
              });
            }
          }, 5000);
        },
        (error) => { }
      );


  }
}
