import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { TranslateModule } from '@ngx-translate/core';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiPaymentMenuComponent } from './payment-menu.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    FiShareModule,
    NgZorroAntdModule,
    AngularSvgIconModule,
    TranslateModule
  ],
  declarations: [FiPaymentMenuComponent],
  exports: [FiPaymentMenuComponent]
})
export class FiPaymentMenutModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
