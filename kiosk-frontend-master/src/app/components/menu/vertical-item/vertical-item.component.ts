import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

interface MenuInterface {
  subtitle: string;
  title: string;
  categories: CategoryInterface[];
}

interface CategoryInterface {
  id: number;
  name: string;
  description: string;
  active: boolean;
}

@Component({
  selector: 'fi-menu-vertical-item',
  templateUrl: './vertical-item.component.html',
  styleUrls: ['./vertical-item.component.less']
})
export class FiMenuVerticalItemComponent implements OnInit {

  @Input() public height = '1008px';

  @Input() public language: string;

  @Input('data')
  data: MenuInterface = { subtitle: null, title: null, categories: [] };

  @Input('type')
  type = 'all';

  @Output() OnOptionChange = new EventEmitter<number>();

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService
  ) {}

  /**
   * On init
   */
  ngOnInit() {

    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }

  }

  /**
   * Handler option change
   */
  handlerOptionChange(id) {

    this.data.categories = this.data.categories.map(item => ({...item, active: (item.id === id ? true : false)}));
    this.OnOptionChange.emit(id);

  }
}
