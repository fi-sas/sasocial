import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { TranslateModule } from '@ngx-translate/core';
import { FiMenuVerticalItemComponent } from './vertical-item.component';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [FiShareModule, FiDirectivesModule, TranslateModule],
  declarations: [FiMenuVerticalItemComponent],
  exports: [FiMenuVerticalItemComponent]
})
export class FiMenuVerticalItemModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
