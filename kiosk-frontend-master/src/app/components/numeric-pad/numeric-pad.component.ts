import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'fi-sas-numeric-pad',
  templateUrl: './numeric-pad.component.html',
  styleUrls: ['./numeric-pad.component.less']
})
export class FiNumericPadComponent implements OnInit {

  @Input() number: string | number = 0.00;
  @Output() numberChange = new EventEmitter<number|string>();

  constructor() { }

  ngOnInit(): void {
    this.numberChange.emit(this.number);
  }

  addValue(value: number) {
    let num = ((Number(this.number) * 10) + (value / 100)).toFixed(2);
    if(Number(num) > 999999.99) {
      return;
    }

    this.number = num;
    this.numberChange.emit(this.number);
  }

  clearValue() {
    this.number = 0.00;
    this.numberChange.emit(this.number);
  }
}
