import { Component, Output } from '@angular/core';
import { Subject } from 'rxjs';

export type TripeType = 'ARRIVAL' | 'DEPARTURE';

@Component({
  selector: 'fi-options-trip',
  templateUrl: './options-trip.component.html',
  styleUrls: ['./options-trip.component.less']
})
export class FiOptionsTripComponent {
  @Output() tripChange = new Subject<TripeType>();

  tripeType: TripeType = 'DEPARTURE';

  handlerTripChange($event: UIEvent, type: TripeType) {
    $event.preventDefault();

    this.tripeType = type;

    this.tripChange.next(type);
  }

  isDeparture() {
    return this.tripeType === 'DEPARTURE';
  }
}
