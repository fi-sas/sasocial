import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FiAuthPresenter } from "@fi-sas/kiosk/scenes/authentication/authentication.presenter";
import { UserAction } from "@fi-sas/kiosk/services/auth.reducer";
import { FiConditionService } from "@fi-sas/kiosk/services/condition.service";
import { TranslateService } from "@ngx-translate/core";
import { NzModalRef } from "ng-zorro-antd";
import { of } from "rxjs";
import { concatMap, finalize, first } from "rxjs/operators";
import { MenuAction } from "../menu/main/menu.reducer";

@Component({
  selector: "fi-sas-pin-modal",
  templateUrl: "./pin-modal.component.html",
  styleUrls: ["./pin-modal.component.less"],
})
export class PinModalComponent implements OnInit {
  rfid = "";
  pin = "";
  ERROR_MESSAGE = null;
  _timeout;
  modalSuccess = false;

  constructor(
    private modal: NzModalRef,
    public authPresenter: FiAuthPresenter,
    private _conditionService: FiConditionService,
    private _translate: TranslateService
  ) {
    this._timeout = setTimeout(() => {
      this.modal.destroy();
    }, 30000);
  }

  ngOnInit(): void { }

  pinLenght(): number {
    return this.pin.length;
  }

  getInputPinText(): string {
    if (this.pin.length === 0) {
      return "____";
    } else {
      let text = "";
      for (let index = 0; index < this.pinLenght(); index++) {
        text = text + "*";
      }
      for (let index = 0; index < 4 - this.pinLenght(); index++) {
        text = text + "_";
      }
      return text;
    }
  }

  clickButton(option: string) {
    if (option === "EMPTY") {
      this.pin = "";
      return;
    }

    if (option === "CLEAR") {
      if (this.pin.length > 0) {
        this.pin = this.pin.substring(0, this.pin.length - 1);
      }
      return;
    }

    if (this.pin.length === 4) {
      return;
    }

    this.pin = this.pin + option;

    if (this.pin.length === 4) {
      this.login();
    }
  }

  setError(ERROR_MESSAGE: string) {
    this.ERROR_MESSAGE = ERROR_MESSAGE;
  }

  sendPin() {

    this.authPresenter
      .sendRandomPin(this.rfid)
      .pipe(
        first()
      )
      .subscribe(() => {
        this.modalSuccess = true;
        setTimeout(() => {
          this.modalSuccess = false;
          this.modal.close();
        }, 4000);
      });
  }

  login() {
    if (this.pin.length !== 4) return;

    this.authPresenter
      .authenticateByRfid(this.rfid, this.pin)
      .pipe(
        first(),
        concatMap((logged) =>
          logged.authorized
            ? this.authPresenter.currentUser()
            : of(logged.response)
        )
      )
      .subscribe(
        (response) => {
          if (response instanceof HttpErrorResponse) {
            this.clickButton("EMPTY");
            const err = response as HttpErrorResponse;
            if (err.error.errors[0]) {
              const err_trans = err.error.errors[0].translations;
              const current_lang = this._translate.currentLang;

              const final_error = err_trans.find(
                (fe) => fe.language.acronym === current_lang
              );

              this.setError(final_error.message);
            }
          }

          if (response && response.data) {
            UserAction.next({ user: response.data[0] });
            MenuAction.next({
              authentication: false,
              logged: true,
            });

            this._conditionService.goto("dashboard");
            this.modal.close();
          } else {
            UserAction.next(null);
            MenuAction.next({
              authentication: false,
              logged: false,
            });

            this.authPresenter.destroySession();
            this._conditionService.goto("dashboard");
          }
        },
        (error) => {
          this.clickButton("EMPTY");

          UserAction.next(null);
          MenuAction.next({ authentication: false, logged: false });

          this.authPresenter.destroySession();
          this._conditionService.goto("dashboard");
        },
        () => { }
      );
  }

  cancel() {
    this.modal.close();
  }
}
