import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FiShoppingCartService } from '@fi-sas/kiosk/components/cart/shopping-cart.service';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';


@Component({
  selector: 'fi-sas-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.less']
})
export class FiItemListComponent implements OnInit, OnDestroy {
  /* Inputs */
  @Input('service_id') service_id: number = null;
  @Input('isUserLogged') isUserLogged: boolean = false;
  @Input('data')
  set data(value: any) {
    if (value) {
      this.product = value;
      //this.defaulProductValues();
    }
  }
  loading = false;
  public product = null;
  public cartQuantity = 0;
  validProduct = '';
  stock = 999;
  modalNoLogged = false;
  public _subscription: Subscription;

  /**
   * Constructor
   */
  constructor(private shoppingCartService: FiShoppingCartService
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this.existProduct();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  /**
   * Default product values
   */
  defaulProductValues() {
    if (this.product && this.isUserLogged) {
      this.shoppingCartService.getAllProducts()
        .pipe(first())
        .subscribe(
          (result) => {
            if (result.length > 0) {
              result.forEach(prod => {
                prod.items.forEach(item => {
                  if (item.product_code == this.product.code) {
                    this.cartQuantity = item.quantity;
                  }
                });
              })
            } else {
              this.cartQuantity = 0;
            }

          }
        );
    }
  }

    existProduct(): void {
      this.validProduct = '';
      this._subscription = this.shoppingCartService.cartData$.subscribe(
        (cartObjects) => {
          if(cartObjects.length == 0) {
            this.stock = 999;
            this.cartQuantity = 0;
          }else{
            cartObjects.forEach(cart => {
              if (cart.items.length && this.product) {
                const finded = cart.items.find(i => i.product_code == this.product.code);
                if (finded) {
                  this.validProduct = finded.id;
                  this.stock = finded.max_quantity;
                  this.cartQuantity = finded.quantity;
                } else {
                  this.cartQuantity = 0;
                }
              }
            });
          }

        }
      );
    }

  /**
   * Handler quantity change
   * @param {object} item object
   */
  handlerQtChange(item: { action: 'add' | 'remove'; quantity: number }) {
    if (this.isUserLogged) {
      this.loading = true;
      this.existProduct();


      if (item.action == 'add') {
        this.cartQuantity = this.cartQuantity + 1;
        if (this.validProduct == '') {
          this.shoppingCartService.addToCartBar(this.product.id).pipe(
            first()
          ).subscribe(
            () => {
              this.shoppingCartService.getAllProducts()
                .pipe(first(), finalize(() => this.loading = false))
                .subscribe(
                  (result) => {
                    this.shoppingCartService.getDataNotification();
                  }
                );
            },
            (error) => {
              this.cartQuantity = 0
            }
          );
        } else {
          this.shoppingCartService.changeQtdItem(this.validProduct, this.cartQuantity).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
            this.shoppingCartService.getDataNotification();
          })
        }

      } else {
        this.cartQuantity = this.cartQuantity - 1;
        if (this.cartQuantity == 0) {
          this.shoppingCartService.removeItemCard(this.validProduct).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
            this.shoppingCartService.getDataNotification();
          })
        } else {
          this.shoppingCartService.changeQtdItem(this.validProduct, this.cartQuantity).pipe(first(), finalize(() => this.loading = false)).subscribe(() => {
            this.shoppingCartService.getDataNotification();
          })
        }
      }
    } else {
      this.modalNoLogged = true;
      setTimeout(() => this.modalNoLogged = false, 4000);
    }

  }

}
