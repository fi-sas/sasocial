import { NgModule } from '@angular/core';
import { FiItemListComponent } from './item-list.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiShareModule } from '../../../../../../libs/share/src/lib/share.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

@NgModule({
  imports: [
    RouterModule,
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule
  ],
  declarations: [FiItemListComponent],
  exports: [FiItemListComponent]
})
export class FiItemListModule { 
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
