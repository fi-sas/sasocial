import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiShareModule } from '@fi-sas/share';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiProductBasicDetailComponent } from './basic-detail.component';

@NgModule({
  imports: [
    NgZorroAntdModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule
  ],
  declarations: [FiProductBasicDetailComponent],
  exports: [FiProductBasicDetailComponent]
})
export class FiProductBasicDetailModule {}
