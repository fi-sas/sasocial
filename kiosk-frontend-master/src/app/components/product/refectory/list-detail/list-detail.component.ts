import { Component, Input, OnInit } from '@angular/core';
import { FiShoppingCartService } from '@fi-sas/kiosk/components/cart/shopping-cart.service';
import { finalize, first } from 'rxjs/operators';


/**
 * Component
 */
@Component({
  selector: 'fi-product-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.less']
})
export class FiProductListDetailComponent implements OnInit {
  /* Inputs */
  @Input('service_id') service_id: number = null;
  @Input ('isLogged') isLogged: boolean = false;
  @Input() selectedDishType: string = '';
  @Input('data')
  set data(value: any) {
    if (value) {
      this.product = value;
      this.defaulProductValues();
    }
  }

  public product = null;
  public cartQuantity = 0;
  validProduct = '';
  stock = 999;
  loading = false;
  modalNoLogged = false;

  /**
   * Constructor
   */
  constructor(
    private shoppingCartService: FiShoppingCartService
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    this.existProduct();
  }

  /**
   * Default product values
   */
  defaulProductValues() {
    if(this.product && this.isLogged) {
      this.shoppingCartService.getAllProducts()
        .pipe(first())
        .subscribe(
            (result) => {
              if(result.length > 0) {
                result.forEach(prod => {
                  prod.items.forEach(item => {
                    if(item.extra_info.menu_dish_id == this.product.id) {
                      this.cartQuantity = item.quantity;
                    }
                  });
                })
              }else {
                this.cartQuantity = 0;
              }
              
            }
          );
    }
  }

  existProduct() {
    this.validProduct = '';
    this.shoppingCartService.cartData$.subscribe(
      (cartObjects) => {
        if(cartObjects.length == 0) {
          this.stock = 999;
          this.cartQuantity = 0;
        }else{ 
          cartObjects.forEach(cart => {
            if (cart.items.length>0 && this.product) {
  
              const finded = cart.items.find(i => i.extra_info && i.extra_info.menu_dish_id === this.product.id);
              if (finded) {
                this.validProduct = finded.id;
                this.stock = finded.max_quantity;
                this.cartQuantity = finded.quantity;
              } else {
                this.cartQuantity = 0;
              }
            }
          });
        }
        
      }
    );
  }

  /**
   * Handler quantity change
   * @param {object} item object
   */
  handlerQtChange(item: { action: 'add' | 'remove' }) {
    if(this.isLogged) {
      this.loading = true;
      this.existProduct();
      if(item.action == 'add') {
        this.cartQuantity = this.cartQuantity + 1;
        if(this.validProduct == '') {
          this.shoppingCartService.addToCartAlimentation(this.product.id).pipe(
            first()
          ).subscribe(
            () => {
              this.shoppingCartService.getAllProducts()
                .pipe(first(),finalize(()=>this.loading=false))
                .subscribe(
                  (result) => {
                    this.shoppingCartService.getDataNotification();
                  }
                );
            },
            (error) => {
              this.cartQuantity = 0
            }
          );
        }else {
          this.shoppingCartService.changeQtdItem(this.validProduct, this.cartQuantity).pipe(first(),finalize(()=>this.loading=false)).subscribe(()=> {
            this.shoppingCartService.getDataNotification();
          })
        }
      }else {
        this.cartQuantity = this.cartQuantity - 1;
        if(this.cartQuantity == 0) {
          this.shoppingCartService.removeItemCard(this.validProduct).pipe(first(),finalize(()=>this.loading=false)).subscribe(()=> {
            this.shoppingCartService.getDataNotification();
          })
        }else {
          this.shoppingCartService.changeQtdItem(this.validProduct, this.cartQuantity).pipe(first(), finalize(()=>this.loading=false)).subscribe(()=> {
            this.shoppingCartService.getDataNotification();
          })
        }
      }
    }else {
      this.modalNoLogged = true;
      setTimeout(() => this.modalNoLogged = false, 4000);
    }
    
  }
}
