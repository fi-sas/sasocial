import { Component, AfterViewInit, Input } from '@angular/core';

@Component({
  selector: 'fi-product-list-options',
  templateUrl: './list-options.component.html',
  styleUrls: ['./list-options.component.less']
})
export class FiProductListOptionsComponent implements AfterViewInit {
  @Input() selectedDishType: string = '';
  ngAfterViewInit() {}
}
