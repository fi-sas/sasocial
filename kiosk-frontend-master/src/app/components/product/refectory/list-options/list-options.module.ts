import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FiShareModule } from '@fi-sas/share';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiProductListOptionsComponent } from './list-options.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule
  ],
  declarations: [FiProductListOptionsComponent],
  exports: [FiProductListOptionsComponent]
})
export class FiProductListOptionsModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
