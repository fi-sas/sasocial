import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Subject, Subscription } from "rxjs";
import { FiMobilityService } from "@fi-sas/kiosk/scenes/mobility/mobility.service";
import { FiShoppingCartService } from "../../cart/shopping-cart.service";
import { finalize, first, takeUntil } from "rxjs/operators";

/**
 * Component
 */
@Component({
  selector: "fi-route-item",
  templateUrl: "./route-item.component.html",
  styleUrls: ["./route-item.component.less"],
})
export class FiRouteItemComponent implements OnDestroy, OnInit {
  /* Input: model, index */
  @Input() stops = 0;
  @Input() index = 0;
  @Input() model: any;
  @Input() duration = 0;

  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Router}                 private _router
   * @param {FiMobilityService}      private _serviceMobility
   */
  constructor(
    private _router: Router,
    private _serviceMobility: FiMobilityService,
    private _shoppingCart: FiShoppingCartService
  ) {}

ngOnInit() {
  this.existProduct();
}
/**
 * On destroy
 */
ngOnDestroy() {
  this._subject.next(true);
  this._subject.unsubscribe();
  }

  /**
   * View route detail
   */
  viewRouteDetail(event: UIEvent) {
    event.preventDefault();
    this._serviceMobility.selectedRouteIndex = this.index - 1;
    this._router.navigate(["/mobility/detail"]);
  }

  convertDuration(value): string {
    const hours = Math.floor(value / 60);
    const minutes = value % 60;
    let end =
      hours != 0
        ? hours.toString().split(".")[0] +
          " h " +
          minutes.toString().split(".")[0] +
          " m"
        : minutes.toString().split(".")[0] + " m";
    return end;
  }

  isUserLogged = true;
  modalNoLogged=false;
  loading = false;
  stock = 999;
  public cartQuantity = 0;
  validProduct = "";
  public _subscription: Subscription;
  /**
   * Handler quantity change
   * @param {object} item object
   */
  handlerQtChange(item: { action: "add" | "remove"; quantity: number }) {
    if (this.isUserLogged) {
      this.loading = true;
      this.existProduct();

      if (item.action == "add") {
        this.cartQuantity = this.cartQuantity + 1;
        if (this.validProduct == "") {
          this._shoppingCart
            .addToCartMobility(this.getTicketHash())
            .pipe(first())
            .subscribe(
              () => {
                this._shoppingCart
                  .getAllProducts()
                  .pipe(
                    first(),
                    finalize(() => (this.loading = false))
                  )
                  .subscribe((result) => {
                    this._shoppingCart.getDataNotification();
                  });
              },
              (error) => {
                this.cartQuantity = 0;
              }
            );
        } else {
          this._shoppingCart
            .changeQtdItem(this.validProduct, this.cartQuantity)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe(() => {
              this._shoppingCart.getDataNotification();
            });
        }
      } else {
        this.cartQuantity = this.cartQuantity - 1;
        if (this.cartQuantity == 0) {
          this._shoppingCart
            .removeItemCard(this.validProduct)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe(() => {
              this._shoppingCart.getDataNotification();
            });
        } else {
          this._shoppingCart
            .changeQtdItem(this.validProduct, this.cartQuantity)
            .pipe(
              first(),
              finalize(() => (this.loading = false))
            )
            .subscribe(() => {
              this._shoppingCart.getDataNotification();
            });
        }
      }
    } else {
      this.modalNoLogged = true;
      setTimeout(() => (this.modalNoLogged = false), 4000);
    }
  }


  existProduct(): void {
    this.validProduct = '';
    this._subscription = this._shoppingCart.cartData$.pipe(takeUntil(this._subject)).subscribe(
      (cartObjects) => {
        if(cartObjects.length == 0) {
          this.stock = 999;
          this.cartQuantity = 0;
        }else{
          cartObjects.forEach(cart => {
            if (cart.items.length && this.model) {
              const finded = cart.items.find(i => i.product_code == this.getTicketProductCode());
              if (finded) {
                this.validProduct = finded.id;
                this.stock = finded.max_quantity;
                this.cartQuantity = finded.quantity;
              } else {
                this.cartQuantity = 0;
              }
            }
          });
        }

      }
    );
  }


  /**
   * Add group of products
   */
  addProduct(event: UIEvent) {
    event.preventDefault();
    event.stopPropagation();

    this.handlerQtChange({action: 'add', quantity: 1});
  }

  /**
   * Add group of products
   */
  removeProduct(event: UIEvent) {
    event.preventDefault();
    event.stopPropagation();

    this.handlerQtChange({action: 'remove', quantity: 1});
  }

  haveTicketPrice() {
    if (
      this.model &&
      this.model.prices &&
      Array.isArray(this.model.prices) &&
      this.model.prices.length
    ) {
      const ticket_price = this.model.prices.find(
        (p) => p.ticket_type === "TICKET"
      );
      return !!ticket_price;
    }
    return false;
  }

  getTicketPrice() {
    if (
      this.model &&
      this.model.prices &&
      Array.isArray(this.model.prices) &&
      this.model.prices.length
    ) {
      const ticket_price = this.model.prices.find(
        (p) => p.ticket_type === "TICKET"
      );
      return ticket_price.value;
    }
    return null;
  }

  getTicketHash() {
    if (
      this.model &&
      this.model.prices &&
      Array.isArray(this.model.prices) &&
      this.model.prices.length
    ) {
      const ticket_price = this.model.prices.find(
        (p) => p.ticket_type === "TICKET"
      );
      return ticket_price.hash;
    }
    return null;
  }

  getTicketProductCode() {
    if (
      this.model &&
      this.model.prices &&
      Array.isArray(this.model.prices) &&
      this.model.prices.length
    ) {
      const ticket_price = this.model.prices.find(
        (p) => p.ticket_type === "TICKET"
      );
      return ticket_price.product_code;
    }
    return null;
  }

  /**
   * Handler quantity change
   * @param {object} item object
   */
  /* handlerQtChange(item:{action: 'add' | 'remove'; quantity: number }) {

    forkJoin(
      this._translate.get('MOBILITY.ROUTES.LIST.ITEM.MOBILITY'),
      this._translate.get('MOBILITY.ROUTES.LIST.ITEM.TRIP'),
    ).pipe(
      takeUntil(this._subject),
      map(res => {

        const mobilityLabel = res[0];
        const tripLabel = res[1];

        const id = this.productIdPrefix + this._model.date + this._model.departure_place + this._model.departure_hour + this._model.arrival_place + this._model.arrival_hour;

        const langAcronym = this._translateService.getLanguage();
        const datePipe = new DatePipe(langAcronym + '-' + langAcronym.toUpperCase());
        const day = datePipe.transform(this._model.date, 'dd MMM yyyy');
        const departure =  this._model.departure_hour.slice(0, 5);
        const arrival = this._model.arrival_hour.slice(0, 5);

        const organization = this._configurator.getOptionTree('ORGANIZATION')["ORGANIZATION"];

        let accountId = null;
        let servicetId = null;
        let taxId = null;
        let tax = 0;
        let reference = '';
        if (has(this._model, 'route')) {
          this._model.route.forEach(route => {
            if (has(route, 'prices') && has(route.prices, 'price')) {

              const ticketsPrices = filter(route.prices.price, (price) => (price.purchase_type === "TICKET"))

              if (ticketsPrices.length) {
                accountId = ticketsPrices[0].account_id;
                servicetId = ticketsPrices[0].service_id;
                taxId = ticketsPrices[0].tax_id;
                tax = has(ticketsPrices[0], 'tax') ? ticketsPrices[0].tax : 0;
                reference = JSON.stringify(ticketsPrices.map(ticket => ticket.prod_cod));
              }

            }
          });
        }

        const product = {
          id: id,
          account_id: accountId,
          service_id: servicetId,
          tax_id: taxId,
          ref: reference,
          module: productModules.MOBILITY,
          name: tripLabel + ' ' + this._model.departure_place + '-' + this._model.arrival_place + ' [' + day + ']' + ' [' + departure + ' - ' + arrival + ']',
          description: mobilityLabel + ' [' + organization.THEME + ']',
          price: this._model.tickets_prices_total,
          vat: tax,
          quantity: 1,
          module_required_values: {
            prod_compound_cod: this._model.prod_compound_cod,
            tickets_prices_total: this._model.tickets_prices_total,
            date: this._model.date,
            departure_place: this._model.departure_place,
            departure_hour: this._model.departure_hour,
            arrival_place: this._model.arrival_place,
            arrival_hour: this._model.arrival_hour
          }
        };

        switch (item.action) {
          case 'add':
            this.cartService$.addProduct(product);
            break;

          case 'remove':
            this.cartService$.changeProductQt(product, 'remove', 1);
            break;
        }

        const cartProduct = this.cartService$.getProduct({id: id});
        this._model["cartQuantity"] = (cartProduct) ? cartProduct.quantity : 0;

      })
    ).subscribe();

  }*/
}
