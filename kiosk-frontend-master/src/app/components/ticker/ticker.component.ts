import { OnDestroy, ChangeDetectorRef, Attribute, Component, ViewChild, ViewChildren, ElementRef, Renderer2, Directive, AfterViewInit, ContentChildren, QueryList, Input, OnChanges, EventEmitter } from '@angular/core';

@Directive({
  selector: 'fi-ticker-wrap, [fi-ticker-wrap]'
})
export class FiTickerWrapperDirective {
  constructor(public el: ElementRef) {}
}

@Directive({
  selector: 'fi-ticker-item, [fi-ticker-item]',
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    class: 'ticker__item'
  }
})
export class FiTickerItemDirective implements OnChanges {
  @Input() scope: string;

  output = new EventEmitter();

  constructor(public el: ElementRef) {}

  ngOnChanges(changes: any) {
    this.output.emit(changes);
  }
}

@Component({
  selector: 'fi-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.less']
})
export class FiTickerComponent implements AfterViewInit, OnDestroy {

  @ViewChild(FiTickerWrapperDirective)
  tickerContainer: FiTickerWrapperDirective;

  @ContentChildren(FiTickerItemDirective)
  tickerItemsNodes: QueryList<FiTickerItemDirective>;

  public tickerNodes: {
    totalWidth: number;
    nodeWidth: number;
    tickerNode: FiTickerItemDirective;
  }[] = [];
  public tickerFirstNode: FiTickerItemDirective;
  public tickerScopeNode: string;

  public tickerIndex = 0;
  public tickerMargin = 0;
  public tickerTotalWidth = 0;
  public tickerPaddingRight = 200;

  public interval: NodeJS.Timer;

  public scopeSubscribe: any;

  constructor(
    private r: Renderer2,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngAfterViewInit() {
    this.tickerItemsNodes.forEach(item =>
      item.output.subscribe(i => {
        this.tickerScopeNode = this.tickerNodes[
          this.tickerIndex
        ].tickerNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();
      })
    );

    const totalTicker = this.tickerItemsNodes.length;

    if (totalTicker) {
      /* set ticker first node */
      this.tickerFirstNode = this.tickerItemsNodes.first;

      /* set ticker scope */
      this.tickerScopeNode = this.tickerFirstNode.scope;
      this.changeDetector.markForCheck();
      this.changeDetector.detectChanges();

      /* calculate and set widths */
      let totalWidth = 0;
      this.tickerItemsNodes.map(tickerNode => {
        const nodeWidth = tickerNode.el.nativeElement.offsetWidth;
        totalWidth = totalWidth + nodeWidth;
        this.tickerNodes.push({
          totalWidth: totalWidth,
          nodeWidth: nodeWidth,
          tickerNode: tickerNode
        });
      });

      /* set ticker total width */
      this.tickerTotalWidth = this.tickerContainer.el.nativeElement.offsetWidth;

      this.moveLeft();
    }
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  moveLeft(): void {
    this.tickerMargin = this.tickerTotalWidth;

    this.interval = setInterval(() => {
      /* last node reached */
      if (
        this.tickerIndex === this.tickerNodes.length - 1 &&
        this.tickerMargin <
          -(
            this.tickerNodes[this.tickerNodes.length - 1].totalWidth -
            this.tickerPaddingRight
          )
      ) {
        this.tickerMargin = this.tickerTotalWidth;
        this.tickerIndex = 0;

        this.tickerScopeNode = this.tickerNodes[
          this.tickerIndex
        ].tickerNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();

        clearInterval(this.interval);
        this.moveLeft();
      }

      /* next node reached */
      if (
        this.tickerMargin + this.tickerNodes[this.tickerIndex].totalWidth <
          this.tickerTotalWidth &&
        this.tickerIndex !== this.tickerNodes.length - 1
      ) {
        this.tickerIndex = this.tickerIndex + 1;

        this.tickerScopeNode = this.tickerNodes[
          this.tickerIndex
        ].tickerNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();
      }

      /* move first node */
      this.r.setStyle(
        this.tickerFirstNode.el.nativeElement,
        'margin-left',
        this.tickerMargin-- + 'px'
      );
    }, 10);
  }
}
