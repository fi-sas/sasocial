import { FiTickerModule } from './ticker.module';

describe('TickerModule', () => {
  let tickerModule: FiTickerModule;

  beforeEach(() => {
    tickerModule = new FiTickerModule();
  });

  it('should create an instance', () => {
    expect(tickerModule).toBeTruthy();
  });
});
