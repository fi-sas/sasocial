import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import {
  FiTickerComponent,
  FiTickerItemDirective,
  FiTickerWrapperDirective
} from './ticker.component';

@NgModule({
  imports: [FiShareModule],
  declarations: [
    FiTickerComponent,
    FiTickerItemDirective,
    FiTickerWrapperDirective
  ],
  exports: [FiTickerComponent, FiTickerItemDirective, FiTickerWrapperDirective]
})
export class FiTickerModule {}
