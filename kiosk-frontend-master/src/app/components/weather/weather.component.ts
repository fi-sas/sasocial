import { Component, OnInit } from '@angular/core';
import { FiWeatherService } from './weather.service';

import { timer } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';

@Component({
  selector: 'fi-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.less']
})
export class FiWeatherComponent implements OnInit {
  public iconsPath = 'http://openweathermap.org/img/w/';
  public localWeather = null;

  public weatherTemp = null;
  public weatherIcon = null;

  constructor(private _weatherService: FiWeatherService) {}

  ngOnInit() {
    timer(0, 3600000)
      .pipe(
        concatMap(() => this._weatherService.getWeather()),
        map(data => {
          this.localWeather = data;
          this.weatherTemp = this.localWeather.main.temp;
          this.weatherIcon =
            this.iconsPath + this.localWeather.weather[0].icon + '.png';
        })
      )
      .subscribe();
  }
}
