import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import { FiConfigurator } from '@fi-sas/configurator';
import { FiResourceService } from '@fi-sas/core';

import { FiWeatherComponent } from './weather.component';
import { FiWeatherService } from './weather.service';

@NgModule({
  providers: [
    {
      provide: FiWeatherService,
      useClass: FiWeatherService,
      deps: [FiConfigurator, FiResourceService]
    }
  ],
  imports: [FiShareModule],
  declarations: [FiWeatherComponent],
  exports: [FiWeatherComponent]
})
export class FiWeatherModule {}
