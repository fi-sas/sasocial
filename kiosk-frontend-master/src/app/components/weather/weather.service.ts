import { Injectable } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { FiResourceService } from '@fi-sas/core';

interface Coordinates {
    place: string,
    latitude: string,
    longitude: string
}

@Injectable()
export class FiWeatherService {

  /*private _places: Coordinates[] = [
    {place: 'ipvc', latitude: '41.69323', longitude: '-8.83287'},
    {place: 'ipca', latitude: '41.53183', longitude: '-8.61233'},
    {place: 'ipb', latitude: '41.756081', longitude: '-6.75535'},
    {place: 'ipcoimbra', latitude: '40.204915', longitude: '-8.453806'},
    {place: 'ipleiria', latitude: '39.737683', longitude: '-8.811241'},
    {place: 'ipportalegre', latitude: '39.291483', longitude: '-7.433100'},
    {place: 'ipsantarem', latitude: '39.221921', longitude: '-8.687795'},
    {place: 'ipviseu', latitude: '40.6468692', longitude: '-7.919621'},
  ];*/
  private _weatherServiceKey = '29f6e81090987c4f3d1ffd5dd6338c8f';
  private _weatherService = 'http://api.openweathermap.org/data/2.5/weather?lat={LAT}&lon={LON}&units=metric&appid=' + this._weatherServiceKey;

  private _placeCoords = null;

  /**
   * Constructor
   * @param {FiConfigurator}    private _configurator
   * @param {FiResourceService} private _resourceService
   */
  constructor(
    private _configurator: FiConfigurator,
    private _resourceService: FiResourceService
  ) {
    const lat = this._configurator.getOption<number>('LATITUDE');
    const lon = this._configurator.getOption<number>('LONGITUDE');
    this._weatherService = this._weatherService.replace('{LAT}', lat.toString()).replace('{LON}', lon.toString());
  }

  /**
   * Get weather
   */
  public getWeather() {
    return this._resourceService.request('GET', this._weatherService);
  }
}
