import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import { PerfectScrollbarModule, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
 
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
 
@NgModule({
  imports: [
    FiShareModule,
    PerfectScrollbarModule
  ],
  declarations: [

  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  exports: [
    PerfectScrollbarComponent
  ]
})
export class FiDirectivesModule {}
