import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiAuthenticationComponent } from './authentication.component';

const routes: Routes = [
  {
    path: '',
    component: FiAuthenticationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiAuthenticationRoutingModule {}
