import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiAuthenticationComponent } from './authentication.component';

xdescribe('> Authentication Component', () => {
  let component: FiAuthenticationComponent;
  let fixture: ComponentFixture<FiAuthenticationComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FiAuthenticationComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
