import { Component, OnInit, Renderer2, Inject, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { forkJoin, of, Subject } from 'rxjs';
import { takeUntil, flatMap, concatMap, first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { NzModalService } from 'ng-zorro-antd';
import {
  UserAction
} from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { FiAuthPresenter } from './authentication.presenter';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';

export type AUTHENTICATION_MODE = 'PIN' | 'RFID';

/* enums */
enum AuthenticationMode {
  PIN = 'PIN',
  RFID = 'RFID'
}

@Component({
  selector: 'fi-sas-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.less']
})
export class FiAuthenticationComponent implements OnInit, OnDestroy {

  /* public */
  public mode: AUTHENTICATION_MODE = AuthenticationMode.PIN;
  public authenticateForm: FormGroup = this._authPresenter.authenticatePinForm;
  modalSuccess = false;
  /* private */
  private _subject = new Subject<boolean>();

  /**
   * Creates an instance of FiAuthenticationComponent
   * @param {Renderer2}             private _render
   * @param {FiAuthPresenter}       private _authPresenter
   * @param {TranslateService}      private _translate
   * @param {NzModalService}        private _modal
   */
  constructor(
    private _render: Renderer2,
    private _authPresenter: FiAuthPresenter,
    private _translate: TranslateService,
    private _modal: NzModalService,
    private _conditionService: FiConditionService,
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({ authentication: true });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.authenticateForm.reset();
    MenuAction.next({ authentication: false });
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * On input focus.
   *
   * @param {FocusEvent} ev
   * @memberof FiAuthenticationComponent
   */
  inputOnFocus(ev: FocusEvent) {

    const el = (ev.target as HTMLInputElement).parentElement;

    this.setElementShadow(true, el);

  }

  handlerAuthenticate(event: UIEvent) {
    this.authenticateForm.get('username').setValue((<HTMLInputElement>document.getElementById("usernameAuth")).value);
    this.authenticateForm.get('pin').setValue((<HTMLInputElement>document.getElementById("pinAuth")).value);
    event.preventDefault();
    this._authPresenter
      .authenticateByPin()
      .pipe(
        concatMap((logged) =>
          logged ? this._authPresenter.currentUser() : of({ data: null })
        )
      )
      .subscribe((response) => {
        if (response && response.data) {
          UserAction.next({ user: response.data[0] });
          MenuAction.next({ authentication: false, logged: true });

          this._authPresenter.authenticatePinForm.reset();

          this._conditionService.goto('dashboard');
        }
      });
  }

  /**
   * On input blur.
   *
   * @param {FocusEvent} ev
   * @memberof FiAuthenticationComponent
   */
  inputOnBlur(ev: FocusEvent) {

    const el = (ev.target as HTMLInputElement).parentElement;

    this.setElementShadow(false, el);

  }

  sendPin() {

    if(this.authenticateForm.get('username').value) {
      this._authPresenter
      .sendRandomPinUsername(this.authenticateForm.get('username').value)
      .pipe(
        first()
      )
      .subscribe(() => {
        this.modalSuccess = true;
        setTimeout(() => {
          this.modalSuccess = false;
        }, 4000);
      });
    }else {
      this.authenticateForm.get('username').markAsDirty();
    }
    
  }

  /**
   * Set element shadow.
   *
   * @param {boolean} active
   * @param {HTMLElement} el
   * @memberof FiAuthenticationComponent
   */
  setElementShadow(active: boolean, el: HTMLElement) {

    active
      ? this._render.addClass(el, 'focus')
      : this._render.removeClass(el, 'focus');

  }

}
