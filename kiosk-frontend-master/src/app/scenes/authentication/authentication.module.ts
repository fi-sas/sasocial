import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiAuthenticationComponent } from './authentication.component';
import { FiAuthenticationRoutingModule } from './authentication-routing.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiKeyboardModule } from '@fi-sas/kiosk/components/keyboard/keyboard.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    FiAuthenticationRoutingModule,
    FiTickerModule,
    TranslateModule,
    FiKeyboardModule
  ],
  declarations: [FiAuthenticationComponent]
})
export class FiAuthenticationModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
