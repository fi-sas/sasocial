import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FiAuthService } from '@fi-sas/kiosk/services/auth.service';
import { tap, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Resource } from '@fi-sas/core';
import { FiRoutingHistoryService } from '@fi-sas/kiosk/services/routing.service';

@Injectable()
export class FiAuthPresenter {
  isLoading = false;

  public authenticatePinForm: FormGroup = this._fb.group({
    username: ['', [Validators.required]],
    pin: ['', [Validators.required]]
  });

  constructor(
    private _fb: FormBuilder,
    private _authService: FiAuthService,
    private _routingService: FiRoutingHistoryService,
  ) {}

  public sendRandomPin(rfid: string): Observable<Resource<{ changed: boolean }>> {
    return this._authService.sendRandomPin(rfid);
  }

  public sendRandomPinUsername(username: string): Observable<Resource<{ changed: boolean }>> {
    return this._authService.sendRandomPinUsername(username);
  }

  public authenticateByPin() {
    this.validatePinForm();

    if (this.authenticatePinForm.valid) {
      return this.makeAuthenticationByPin().pipe(
        map(loggedin => {
          if (!loggedin) {
            this.authenticatePinForm.controls['username'].setErrors({
              required: true
            });
            this.authenticatePinForm.controls['pin'].setErrors({
              required: true
            });

            this.authenticatePinForm.controls['username'].markAsTouched();
            this.authenticatePinForm.controls['pin'].markAsTouched();
          }

          return loggedin;
        })
      );
    } else {
      return of(false);
    }
  }

  public authenticateByRfid(rfid: string, pin: string) {
    return this.makeAuthenticationByRfid(rfid, pin);
  }

  public validatePinForm() {
    // tslint:disable-next-line:forin
    for (const key in this.authenticatePinForm.controls) {
      this.authenticatePinForm.controls[key].markAsDirty();
      // this.authenticatePinForm.controls[key].markAsPristine();
      this.authenticatePinForm.controls[key].updateValueAndValidity();
    }
  }

  public makeAuthenticationByPin() {
    this.isLoading = true;

    const user = this.authenticatePinForm.value.username;
    const pin = this.authenticatePinForm.value.pin;

    return this._authService
      .loginByPin({ user: user, pin: pin })
      .pipe(tap(() => (this.isLoading = false)));
  }

  public makeAuthenticationByRfid(rfid: string, pin :string) {
    this.isLoading = true;

    return this._authService
      .loginByRfid(rfid, pin)
      .pipe(tap(() => (this.isLoading = false)));
  }

  public currentUser() {
    return this._authService.getCurrentUser();
  }

  public destroySession() {
    this._authService.logout();
    this._routingService.resetHistory();
    return of(true);
  }
}
