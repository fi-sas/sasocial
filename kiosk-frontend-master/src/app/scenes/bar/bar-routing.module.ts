import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiBarComponent } from './bar.component';


const routes: Routes = [
  {
    path: 'menu',
    component: FiBarComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/bar/menu/menu.module#FiBarMenuModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiBarRoutingModule { }
