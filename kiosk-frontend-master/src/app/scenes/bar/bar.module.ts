import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiBarRoutingModule } from './bar-routing.module';
import { FiBarComponent } from './bar.component';
import { FiServicesService } from '../refectory/service/services.service';
import { FiBarMenuService } from './menu/menu.service';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';
import { FiMenuModule } from '@fi-sas/kiosk/components/menu/main/menu.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiShareModule } from '../../../../libs/share/src/lib/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';


@NgModule({
  declarations: [FiBarComponent],
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiTickerModule,
    FiMenuModule,
    FiCartModule,
    FiBarRoutingModule
  ],
  providers: [
    {
      provide: FiServicesService,
      useClass: FiServicesService
    },
    {
      provide: FiBarMenuService,
      useClass: FiBarMenuService
    }
  ]
})
export class FiBarModule { }
