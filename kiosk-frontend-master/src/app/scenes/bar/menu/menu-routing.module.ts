import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiBarMenuComponent } from './menu.component';
import { FiBarMenuResolver } from './menu.resolver';

const routes: Routes = [
  {
    path: 'list',
    pathMatch: 'full',
    component: FiBarMenuComponent,
    resolve: {
      data: FiBarMenuResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiBarMenuRoutingModule { }
