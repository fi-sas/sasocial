import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FiBarMenuService } from "./menu.service";
import { ActivatedRoute, Router } from "@angular/router";
import { takeUntil, map, flatMap, first } from "rxjs/operators";
import { Subject, forkJoin, of } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { FiTranslateLazyService } from "@fi-sas/kiosk/services/translate.lazy.service";
import { ApplicationState, Store } from "@fi-sas/core";
import { KioskState } from "@fi-sas/kiosk/app.state";

/**
 * Interfaces
 */
interface TittleInterface {
  mainTitle: string;
  secondaryTitle: string;
  secondarySubtitle: string;
  secondaryLabel: string;
  type: string;
}

@Component({
  selector: "fi-sas-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.less"],
})
export class FiBarMenuComponent implements OnInit, OnDestroy {
  public loadingProducts = false;
  public isUserLogged = false;
  public menuCategories = null;
  public selectedCategory = null;

  public titleData: TittleInterface = null;
  service = { id: null };

  public products = [];

  /* private properties */
  private _subject = new Subject<boolean>();

  constructor(
    private _menuService: FiBarMenuService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _translate: TranslateService,
    private _translateService: FiTranslateLazyService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    if (localStorage.getItem("idBar"))
      this.service.id = Number(localStorage.getItem("idBar"));
    // TODO REDIRECT DASHBOARD
    else _router.navigate(["/", "dashboard"]);
  }

  /**
   * On init
   */
  ngOnInit(): void {
    const storeUser = this._appState.createSlice("user");
    storeUser
      .watch()
      .pipe(takeUntil(this._subject))
      .subscribe((user) => (this.isUserLogged = !!user));

    this.setTitle();
    this.getCategories(true);
    this.getProducts();

    this._translateService
      .getService()
      .onLangChange.pipe(takeUntil(this._subject))
      .subscribe(() => {
        this.setTitle();
        this.getCategories(false);
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Set scene title
   */
  setTitle() {
    /* title */
    forkJoin(
      this._translate.get("BAR.MENU.TITLE"),
      this._translate.get("BAR.MENU.CHANGE")
    )
      .pipe(
        takeUntil(this._subject),
        flatMap((res) => {
          this.titleData = {
            mainTitle: res[0],
            secondaryTitle: "",
            secondarySubtitle: localStorage.getItem("nameBar"),
            secondaryLabel: res[1],
            type: "BAR",
          };

          return of(res);
        })
      )
      .subscribe();
  }

  /**
   * Set categories
   */
  getCategories(init: boolean = false) {
    /* categories */
    if (init) {
      this._route.data.pipe(takeUntil(this._subject)).subscribe(({ data }) => {
        let categories = [];
        categories = data.map((item, index) => ({
          id: item.id,
          name: item.translations[0].name,
          description: null,
          type: "bar",
          active: false,
        }));

        // SORT BY NAME
        categories.sort((a, b) => {
          if (a.name < b.name) {
            return -1;
          }
          if (a.name > b.name) {
            return 1;
          }
          return 0;
        });

        this._translate
          .get("BAR.MENU.MOST_RECENT")
          .pipe(first())
          .subscribe((res) => {
            categories.unshift({
              id: 0,
              name: res,
              description: null,
              type: "bar",
              active: true,
            });
          });

        this.menuCategories = { categories };
        this.selectedCategory = this.menuCategories.categories[0];
      });
    } else {
      if (this.service) {
        this._menuService
          .getCategories(this.service.id)
          .pipe(
            takeUntil(this._subject),
            map((res) => {
              let categories = [];
              categories = res.map((item, index) => ({
                id: item.id,
                name: item.translations[0].name,
                description: null,
                type: "bar",
                active: false,
              }));

              // SORT BY NAME
              categories.sort((a, b) => {
                if (a.name < b.name) {
                  return -1;
                }
                if (a.name > b.name) {
                  return 1;
                }
                return 0;
              });

              this._translate
                .get("BAR.MENU.MOST_RECENT")
                .pipe(first())
                .subscribe((res) => {
                  categories.unshift({
                    id: 0,
                    name: res,
                    description: null,
                    type: "bar",
                    active: true,
                  });
                });

              this.menuCategories = { categories: categories };
              this.selectedCategory = this.menuCategories.categories[0];
              this.getProducts();
            })
          )
          .subscribe();
      }
    }
  }

  /**
   * Get products from service
   */
  getProducts(): void {
    this.loadingProducts = true;

    if (this.selectedCategory.id > 0) {
      this._menuService
        .getProducts(this.service.id, this.selectedCategory.id)
        .pipe(takeUntil(this._subject))
        .subscribe((res) => {
          this.products = this._menuService.productsSignatureForListDetails(
            { id: this.service.id },
            res
          );

          // SORT BY NAME
          this.products.sort((a, b) => {
            if (a.title < b.title) {
              return -1;
            }
            if (a.title > b.title) {
              return 1;
            }
            return 0;
          });

          this.loadingProducts = false;
        });
    } else {
      // GET RECENTS
      this._menuService
        .getRecents(this.service.id)
        .pipe(takeUntil(this._subject))
        .subscribe((res) => {
          this.products = this._menuService.productsSignatureForListDetails(
            { id: this.service.id },
            res
          );

          // SORT BY NAME
          this.products.sort((a, b) => {
            if (a.title < b.title) {
              return -1;
            }
            if (a.title > b.title) {
              return 1;
            }
            return 0;
          });

          this.loadingProducts = false;
        });
    }
  }

  /**
   * Handler category change
   * @param {number} $event
   */
  changeCategory($event: number): void {
    const categoryID = $event;
    const selectedCategory = this.menuCategories.categories.filter(
      (item) => item.id === categoryID
    );

    if (
      selectedCategory &&
      selectedCategory.length >= 0 &&
      selectedCategory[0].type === "bar"
    ) {
      this.selectedCategory = selectedCategory[0];

      this.getProducts();
    }
  }
}
