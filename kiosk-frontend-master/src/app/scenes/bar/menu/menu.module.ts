import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiBarMenuComponent } from './menu.component';
import { FiBarMenuResolver } from './menu.resolver';
import { FiServicesService } from '../../refectory/service/services.service';
import { FiBarMenuRoutingModule } from './menu-routing.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { FiShareModule } from '../../../../../libs/share/src/lib/share.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiContextTwoColModule } from '@fi-sas/kiosk/components/context/two-col/two-col.module';
import { FiMenuVerticalItemModule } from '@fi-sas/kiosk/components/menu/vertical-item/vertical-item.module';
import { FiBarMenuService } from './menu.service';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiItemListModule } from '@fi-sas/kiosk/components/product/bar/item-list/item-list.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';


@NgModule({
  declarations: [FiBarMenuComponent],
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiTickerModule,
    FiContextTwoColModule,
    FiMenuVerticalItemModule,
    FiItemListModule,
    CommonModule,
    FiBarMenuRoutingModule,
    FiDirectivesModule
  ],
  providers: [
    {
      provide: FiBarMenuResolver,
      useClass: FiBarMenuResolver,
      deps: [FiBarMenuService, FiServicesService]
    }
  ]
})
export class FiBarMenuModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
