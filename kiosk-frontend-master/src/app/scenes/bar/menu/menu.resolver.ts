import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve
} from '@angular/router';
import { FiBarMenuService } from './menu.service';

export class FiBarMenuResolver implements Resolve<any> {
  constructor(
    private _menusService: FiBarMenuService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = Number(localStorage.getItem('idBar'));
    return this._menusService.getCategories(id);
  }
}
