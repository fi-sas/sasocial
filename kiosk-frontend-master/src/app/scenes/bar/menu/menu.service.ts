import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '../../../../../libs/configurator/src/lib/configurator.service';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiUrlService } from '../../../../../libs/core/src/lib/url.service';
import { FiResourceService } from '../../../../../libs/core/src/lib/resource.service';
import { FiStorageService } from '../../../../../libs/core/src/lib/storage.service';
import { map, catchError, timeout } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { has } from 'lodash';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FiBarMenuService {
  /* private properties */
  private _lang = this._translateService.getLanguage();
  private _authData = null;

  /**
   * Creates an instance of FiMealsService.
   * @param {TranslateService}       private _translate
   * @param {FiConfigurator}         private _configurator
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiUrlService}           private _urlService
   * @param {FiResourceService}      private _resourceService
   * @param {FiStorageService}       private _storageService
   */
  constructor(
    private _translate: TranslateService,
    private _configurator: FiConfigurator,
    private _translateService: FiTranslateLazyService,
    private _urlService: FiUrlService,
    private _resourceService: FiResourceService,
    private _storageService: FiStorageService
  ) { }

  /**
   * Get categories
   * @param {number} serviceID service id
   */
  getCategories(serviceID: number): Observable<any> {
    const params = new HttpParams()
    .append('limit', '-1')
    .append('offset', '0');
    const url = this._urlService.get('BAR.CATEGORIES', {
      service_id: serviceID,

    });

    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );
    this._lang = this._translateService.getLanguage();

    const languages = this._configurator.getOptionTree('LANGUAGES')[
      'LANGUAGES'
    ];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list<any>(url, {
      params,
      headers: {
        Authorization: 'Bearer '.concat(this._authData.token),
        'X-Language-Acronym': this._lang
      }
    })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Get meals
   * @param {number} serviceID service id
   * @param {number} categoryID service id
   */
  getProducts(serviceID: number, categoryID: number) {
    const url = this._urlService.get('BAR.PRODUCTS', {
      service_id: serviceID,
      family_id: categoryID
    });
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );
    this._lang = this._translateService.getLanguage();

    const languages = this._configurator.getOptionTree('LANGUAGES')[
      'LANGUAGES'
    ];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list<any>(url, {
      headers: {
        Authorization: 'Bearer '.concat(this._authData.token),
        'X-Language-Acronym': this._lang
      }
    })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  getRecents(serviceID: number) {
    const url = this._urlService.get('BAR.PRODUCTS_RECENT', {
      service_id: serviceID,
    });
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );
    this._lang = this._translateService.getLanguage();

    const languages = this._configurator.getOptionTree('LANGUAGES')[
      'LANGUAGES'
    ];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list<any>(url, {
      headers: {
        Authorization: 'Bearer '.concat(this._authData.token),
        'X-Language-Acronym': this._lang
      }
    })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Transform meals
   * @param {any} meals
   */
  productsSignatureForListDetails(service: any, products: any) {

    const serviceTypeTranslation = 'Bar';

    const langAcronym = this._translateService.getLanguage();
    const datePipe = new DatePipe(
      langAcronym + '-' + langAcronym.toUpperCase());

    return products.map(item => {

      const produtName = item.translations.length
        ? item.translations[0].name
        : null;
      return {
        id: item.id,
        available: item.available,
        isStockAvailable: item.isStockAvailable,
        code: item.code,
        tax_id: item.tax_id,
        account_id: item.account_id,
        service_id: item.service_id,
        image: item.file != null ? item.file[0].url : '',
        title: produtName,
        description: item.translations.length ? item.translations[0].description : null,
        allergens: item.allergens,
        price: item.price,
        mealType: null,
        namePayment: produtName,
        cartTitle: produtName,
        cartQuantity: null,
        cartPrice: null,
        cartConsumeAt: null,
        cartMealCategory: null,
        cartMealType: null,
        stockQuantity: item.stockQuantity,
      };
    });
  }

  getBar() {
    const url = this._urlService.get('BAR.GET_BAR');
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );
    return this._resourceService
      .read<any>(url, {
        headers: {
          Authorization: 'Bearer '.concat(this._authData.token),
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );

  }
}
