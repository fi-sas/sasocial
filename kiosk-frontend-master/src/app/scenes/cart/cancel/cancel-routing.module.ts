import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartCancelComponent } from './cancel.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartCancelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartCancelRoutingModule {}
