import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartSceneComponent } from './cart.component';

const routes: Routes = [
  {
    path: '',
    //pathMatch: 'full',
    component: FiCartSceneComponent,
    children: [
      {
        path: 'list',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/list/list.module#FiCartListModule'
      },
      {
        path: 'clear',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/clear/clear.module#FiCartClearModule'
      },
      {
        path: 'success',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/success/success.module#FiCartSuccessModule'
      },
      {
        path: 'charge',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/charge/charge.module#FiCartChargeModule'
      },
      {
        path: 'error',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/error/error.module#FiCartErrorModule'
      },
      {
        path: 'cancel',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/cancel/cancel.module#FiCartCancelModule'
      },
      {
        path: 'tin',
        loadChildren:
          '@fi-sas/kiosk/scenes/cart/tin/tin.module#FiCartTinModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartRoutingModule {}
