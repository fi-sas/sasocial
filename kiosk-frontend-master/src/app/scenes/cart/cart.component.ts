import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

@Component({
  selector: 'fi-cart-scene',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class FiCartSceneComponent implements OnInit, OnDestroy {
  ngOnInit() {
    MenuAction.next({ active: false });
  }

  ngOnDestroy() {
    MenuAction.next({ active: true });
  }
}
