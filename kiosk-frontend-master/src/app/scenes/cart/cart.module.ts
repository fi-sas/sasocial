import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';

import { FiPaymentMenutModule } from '@fi-sas/kiosk/components/menu/payment/payment-menu.module';
import { FiCartSceneComponent } from './cart.component';
import { FiCartRoutingModule } from './cart-routing.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    FiCartRoutingModule,
    TranslateModule,
    FiPaymentMenutModule
  ],
  declarations: [FiCartSceneComponent],

})
export class FiCartSceneModule {
  constructor() {}
}
