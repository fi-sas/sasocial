import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartChargeComponent } from './charge.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartChargeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartChargeRoutingModule {}
