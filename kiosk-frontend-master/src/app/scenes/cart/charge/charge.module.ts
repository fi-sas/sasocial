import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiCartChargeComponent } from './charge.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiCartChargeRoutingModule } from './charge-routing.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiCartItemModule } from '@fi-sas/kiosk/components/cart/item/item.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    TranslateModule,
    FiCartChargeRoutingModule,
    FiDirectivesModule,
    FiCartItemModule,
    AngularSvgIconModule
  ],
  declarations: [FiCartChargeComponent]
})
export class FiCartChargeModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
