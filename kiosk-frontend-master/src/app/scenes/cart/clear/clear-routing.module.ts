import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartClearComponent } from './clear.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartClearComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartClearRoutingModule {}
