import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { FiMenuPaymentService } from '@fi-sas/kiosk/services/menu-payment.service';

@Component({
  selector: 'fi-cart-clear',
  templateUrl: './clear.component.html',
  styleUrls: ['./clear.component.less']
})
export class FiCartClearComponent implements OnInit, OnDestroy {
  /**
   * Constructor
   * @param {Router}                 private                  _router
   * @param {FiTranslateLazyService} private                  _translateService
   */
  constructor(
    private _router: Router,
    private _menuPaymentService: FiMenuPaymentService
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    this._menuPaymentService.setMenu(false);
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._menuPaymentService.setMenu(true);
  }

  /**
   * Reset cart
   */
  resetCart() {
    this._router.navigate(['/dashboard']);
  }
}
