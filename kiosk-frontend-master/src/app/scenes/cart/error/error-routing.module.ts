import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartErrorComponent } from './error.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartErrorRoutingModule {}
