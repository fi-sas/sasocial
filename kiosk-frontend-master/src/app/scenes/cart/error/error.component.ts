import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { FiMenuPaymentService } from '@fi-sas/kiosk/services/menu-payment.service';

@Component({
  selector: 'fi-cart-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.less']
})
export class FiCartErrorComponent implements OnInit, OnDestroy {
  /* public proprities */
  public seconds = 10;
  _interval = null;

  /**
   * Constructor
   * @param {Router}                 private                  _router
   * @param {FiTranslateLazyService} private                  _translateService
   */
  constructor(
    private _router: Router,
    private _menuPaymentService: FiMenuPaymentService
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({ active: true });
    this._menuPaymentService.setMenu(false);

    this._interval = setInterval(() => {
      if (this.seconds === 1) {
        clearInterval(this._interval);
        this._router.navigate(['/dashboard']);
      } else {
        this.seconds = this.seconds - 1;
      }
    }, 1000);
  }

  ngOnDestroy() {
    if(this._interval) {
      clearInterval(this._interval);
    }
  }
}
