import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartListComponent } from './list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartListRoutingModule {}
