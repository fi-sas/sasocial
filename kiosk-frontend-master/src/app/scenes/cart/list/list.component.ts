import { Component, OnInit } from '@angular/core';
import { first, finalize } from 'rxjs/operators';
import { FiBalanceService, Balance } from '@fi-sas/kiosk/components/account/balance.service';
import { FiShoppingCartService } from '@fi-sas/kiosk/components/cart/shopping-cart.service';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { FiAuthPresenter } from '../../authentication/authentication.presenter';
import { ScreenAction } from '@fi-sas/kiosk/services/screen.reducer';
import { UserAction } from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

/* interfaces */
interface PaymentsCart {
  account_id: number;
  user_id: number;
  id: string;
  payment_method_id: string;
  items: CartProductItem[];
}

interface CartProductItem {
  service_id: number;
  product_code: string;
  article_type: string;
  description: string;
  quantity: number;
  vat_id: number;
  cart_id: string;
  expired: boolean;
  id: string;
  liquid_unit_value: number;
  liquid_value: number;
  location: string;
  name: string;
  total_value: number;
  unit_value: number;
  vat: number;
  extra_info: ExtraInfo[];
}

interface ExtraInfo {
  refectory_meal_type: string;
  refectory_meal_category: string;
  menu_dish_id: number;
  product_id: number;
}

@Component({
  selector: 'fi-cart-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class FiCartListComponent implements OnInit {
  existRefectory = false;
  cartData: PaymentsCart[] = [];
  accounts: Balance[] = [];
  total = 0;
  modalSuccess = false;
  checkoutLoading = false;
  idAccountPayment;
  /**
   * Constructor
   */
  constructor(
    private _balanceService$: FiBalanceService,
    private shoppingCartService: FiShoppingCartService,
    private _conditionService: FiConditionService,
    public authPresenter: FiAuthPresenter,
  ) {
   
  }

  /**
   * On init
   */
  ngOnInit() {
    this.getItemsCart();
    this.getCartsAccount();
  }

  getItemsCart() {
    this.existRefectory = false;
    this.shoppingCartService.cartData$.subscribe(
      (cartData) => {
        this.cartData = cartData;
        this.cartData.forEach(cart => {
          if (cart.items.length > 0) {
            if (cart.items.find((car) => car.article_type == "REFECTORY")) {
              this.existRefectory = true;
            }
          }
        });
      })
  }

  getCartsAccount() {
    this.accounts = this._balanceService$.balances;
  }

  getAccountName(id) {
    if (this.accounts) {
      for (const account of this.accounts) {
        if (id === account.account_id) {
          return account.account_name;
        }
      }
    }
  }

  payConfirm(accountId: number){
    this.idAccountPayment = accountId;
    this.goToCheckout(this.idAccountPayment);
  }

  close(){
    this.authPresenter.destroySession().subscribe((rs: any) => {
      ScreenAction.next(true);
      UserAction.next(null);
      MenuAction.next({ authentication: false, logged: false });

      this._conditionService.gotoScreenSaver();
    });
  }

  getCurrentBalance(id) {
    if (this.accounts) {
      for (const account of this.accounts) {
        if (id === account.account_id) {
          return account.current_balance;
        }
      }
    }
  }

  removeItemsCart(account_id) {
    this.shoppingCartService.removeItemsCart(account_id).pipe(
      first()
    ).subscribe(() => {
      this.shoppingCartService.getDataNotification();
      this.getItemsCart();
    })
  }

  /**
   * Quantity Change
   * @param {UIEvent} e
   */

  quantityChange(e) {
    const action = e.action;
    const id = e.id;
    const quantity = e.quantity;

    switch (action) {
      case 'INCREMENT':
        this.addToCartQtd(id, quantity);
        break;

      case 'DECREMENT':
        if (quantity == 0) {
          this.removeItemCard(id);
        } else {
          this.addToCartQtd(id, quantity);
        }
        break;

      case 'REMOVE':
        this.removeItemCard(id);
        break;
    }
  }

  removeItemCard(idItem: number) {
    this.shoppingCartService.removeItemCard(idItem).pipe(first()).subscribe(() => {
      this.shoppingCartService.getDataNotification();
      this.getItemsCart();
    });
  }

  addToCartQtd(id: string, quantity: number) {
    this.shoppingCartService.changeQtdItem(id, quantity).pipe(first()).subscribe(() => {
      this.shoppingCartService.getDataNotification();
      this.getItemsCart();
    })
  }

  countTotalValue(cart): number {
    return this.shoppingCartService.totalValueCart(cart);
  }

  countTotalValueVat(cart): number {
    let finalPrice = 0;
    for (const item of cart.items) {
      finalPrice += Number(item.vat_value);
    }
    return finalPrice;
  }

  goToCheckout(accountId: number) {
    this.checkoutLoading = true;
    LoadingAction.next(true);
    this.shoppingCartService.checkout(accountId)
      .pipe(
        first(),
        finalize(() => {
          this.checkoutLoading = false;
          LoadingAction.next(false);
        })
      )
      .subscribe(
        (result) => {
          this.modalSuccess = true;
          setTimeout(() => {
            this.modalSuccess = false;
          }, 5000);
          this.shoppingCartService.getDataNotification();
          this.getItemsCart();
        },
        (error) => { }
      );


  }

  validTotal(account, total) {

    if(Number(account.toFixed(2)) - Number(total.toFixed(2))<0) {
      return true;
    } 
    return false;
  }

}
