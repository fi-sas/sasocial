import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartSuccessComponent } from './success.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartSuccessRoutingModule {}
