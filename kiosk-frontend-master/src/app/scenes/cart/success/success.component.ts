import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { FiMenuPaymentService } from '@fi-sas/kiosk/services/menu-payment.service';

import { get } from 'lodash';

@Component({
  selector: 'fi-cart-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.less']
})
export class FiCartSuccessComponent implements OnInit, OnDestroy {
  /* public proprities */
  public seconds = 5;
  public redirectTo: string | null = null;
  _interval = null;

  /**
   * Constructor
   * @param {Router}                 private       _router
   * @param {FiMenuPaymentService}   private       _menuPaymentService
   * @param {ActivatedRoute}         private       _activeRouter
   */
  constructor(
    private _router: Router,
    private _menuPaymentService: FiMenuPaymentService,
    private _activeRouter: ActivatedRoute
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({ active: true });
    this._menuPaymentService.setMenu(false);

    this._activeRouter.queryParams.subscribe(params => {
      this.redirectTo = get(params, 'redirect', null);
    });

    this._interval = setInterval(() => {
      if (this.seconds === 1) {
        clearInterval(this._interval);
        this.redirectTo
          ? this._router.navigate([this.redirectTo])
          : this._router.navigate(['/dashboard']);
      } else {
        this.seconds = this.seconds - 1;
      }
    }, 1000);
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    if(this._interval) {
      clearInterval(this._interval);
    }

    if (this.redirectTo) {
      MenuAction.next({ active: false });
      this._menuPaymentService.setMenu(true);
    }
  }

  /**
   * Back button
   */
  back() {
    this.redirectTo
      ? this._router.navigate([this.redirectTo])
      : this._router.navigate(['/dashboard']);
  }
}
