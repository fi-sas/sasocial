import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCartTinComponent } from './tin.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCartTinComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCartTinRoutingModule { }
