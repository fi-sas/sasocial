import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FiMenuPaymentService } from '@fi-sas/kiosk/services/menu-payment.service';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

import { get } from 'lodash';

export enum InvoiceDestinyEnum {
  EMAIL='email',
  BALCONY='balcony'
}

@Component({
  selector: 'fi-sas-tin',
  templateUrl: './tin.component.html',
  styleUrls: ['./tin.component.less']
})
export class FiCartTinComponent implements OnInit {
  InvoiceDestinyEnum = InvoiceDestinyEnum
  public invoiceDestiny: InvoiceDestinyEnum = InvoiceDestinyEnum.EMAIL;

  redirectTo = '';

  /**
   * Constructor
   * @param {Router}                 private       _router
   * @param {FiMenuPaymentService}   private       _menuPaymentService
   * @param {ActivatedRoute}         private       _activeRouter
   */
  constructor(
    private _router: Router,
    private _menuPaymentService: FiMenuPaymentService,
    private _activeRouter: ActivatedRoute
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({ active: false });
    this._menuPaymentService.setMenu(false);
    this._activeRouter.queryParams.subscribe(params => {
      this.redirectTo = get(params, 'redirect', null);
    });
  }

  /*
  /**
   * On destroy
   */
  ngOnDestroy() {
    MenuAction.next({ authentication: false });
    if (this.redirectTo) {
      MenuAction.next({ active: false });
      this._menuPaymentService.setMenu(true);
    }
  }

  selectDestiny(destiny: InvoiceDestinyEnum) {
    this.invoiceDestiny = destiny;
  }

  /**
   * Back button
   */
  back() {
    this.redirectTo
      ? this._router.navigate([this.redirectTo])
      : this._router.navigate(['/dashboard']);
  }
}
