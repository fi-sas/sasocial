import { NgModule } from '@angular/core';

import { FiCartTinRoutingModule } from './tin-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiShareModule } from '../../../../../libs/share/src/lib/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiCartItemModule } from '@fi-sas/kiosk/components/cart/item/item.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiCartTinComponent } from './tin.component';
import { FiMenuInvoiceModule } from '../../../components/menu/invoice/invoice.module';
@NgModule({
  declarations: [FiCartTinComponent],
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    TranslateModule,
    FiDirectivesModule,
    FiCartItemModule,
    AngularSvgIconModule,
    FiCartTinRoutingModule,
    FiMenuInvoiceModule
  ]
})
export class FiCartTinModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
