import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiChargingComponent } from './charging.component';

const routes: Routes = [
  {
    path: '',
    component: FiChargingComponent,
    data: {
      name: 'charging'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiChargingRoutingModule {}
