import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  FiAuthService,
  UserInterface,
} from "@fi-sas/kiosk/services/auth.service";
import { FiChargingService } from "./charging.service";
import { MenuAction } from "@fi-sas/kiosk/components/menu/main/menu.reducer";

import { first, catchError, finalize, takeUntil } from "rxjs/operators";
import {
  Balance,
  FiBalanceService,
} from "@fi-sas/kiosk/components/account/balance.service";
import { Subject, Subscription } from "rxjs";
import { LoadingAction } from "@fi-sas/kiosk/services/loading.reducer";
import { FiConditionService } from "@fi-sas/kiosk/services/condition.service";
import { FiRoutingHistoryService } from "@fi-sas/kiosk/services/routing.service";
import { FiTranslateLazyService } from "@fi-sas/kiosk/services/translate.lazy.service";
import { LangChangeEvent, TranslateService } from "@ngx-translate/core";
import { FiTpaService} from '@fi-sas/kiosk/services/tpa.service';

interface RefMBData {
  Amount: number;
  EntityNumber: string;
  ExpirationDate: Date;
  Reference: string;
}

@Component({
  selector: "fi-charging",
  templateUrl: "./charging.component.html",
  styleUrls: ["./charging.component.less"],
  providers: [FiChargingService],
})
export class FiChargingComponent implements OnInit, OnDestroy {
  /* private propreties */
  private user: UserInterface;
  public balances: Balance[] = [];
  isLoading = false;
  selected_account_id: number = null;
  selected_payment_method_id = null;
  errorType = null;
  errorData = null;

  amount_to_charge = 0;
  balance_after_transaction = '0';

  public locale: string;

  show_refMBResult = false;
  refMBResult: RefMBData = null;/*{
    Amount: 5.5,
    EntityNumber: "77006",
    ExpirationDate: new Date("2021-08-14T16:53:25.972Z"),
    Reference: "000004048",
  };*/

  isTpaAvailable = false;

  /* private propreties */
  private _subject = new Subject<boolean>();
  private subscription: Subscription;

  /**
   * Constructor
   */
  constructor(
    private _balanceService$: FiBalanceService,
    private _chargingService: FiChargingService,
    private _authService: FiAuthService,
    private _tpaService: FiTpaService,
    private _conditionService: FiConditionService,
    private _routeHistoryService: FiRoutingHistoryService,
    private _translateService: TranslateService
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({ active: false });

    this._tpaService.getTpaAvailableObservable()
      .pipe(first())
      .subscribe(available => {
        this.isTpaAvailable = available;

        // SET THIS PAYMENT METHOD SELECTED BY DEFAULT IF AVAILABLE
        if(available)
          this.selected_payment_method_id = 'e9d4ad01-c1c8-4ac7-82f3-a373b08d030c';
        else {
          const other = this.balances[0].available_methods.find(am => am.id !== 'e9d4ad01-c1c8-4ac7-82f3-a373b08d030c');
          if(other)
            this.selected_payment_method_id = other.id;
        }
      });

    this._authService
      .getCurrentUser()
      .subscribe((user) => (this.user = user.data[0]));

    this.isLoading = true;

    /* balances */
    this._balanceService$
      .getBalances()
      .pipe(takeUntil(this._subject))
      .subscribe((data) => {
        this.balances = data;

        if (this.balances.length > 0) {
          this.selected_account_id = this.balances[0].account_id;

          if (this.balances[0].available_methods.length > 0) {

            if(this.balances[0].available_methods.find(am => am.id === 'e9d4ad01-c1c8-4ac7-82f3-a373b08d030c')) {
                this._tpaService.requestAvailable();
            } else {
              this.selected_payment_method_id =
                this.balances[0].available_methods[0].id;
            }
          }


          this.updateBalanceAfterTransaction();
        }
        this.isLoading = false;
      });

    this.subscription = this._translateService.onLangChange.subscribe(
      (event: LangChangeEvent) => {
        this.locale = event.lang + "-" + event.lang.toUpperCase();
      }
    );
  }
  /**
   * On destroy
   */
  ngOnDestroy() {
    MenuAction.next({ active: true });

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getSelectedAccountAvailableMethods() {
    const cc = this.balances.find(
      (a) => a.account_id === this.selected_account_id
    );
    return cc ? cc.available_methods : [];
  }

  getSelectedAccountName(): string {
    const cc = this.balances.find(
      (a) => a.account_id === this.selected_account_id
    );
    return cc ? cc.account_name : "--";
  }

  updateBalanceAfterTransaction(): void {
    const cc = this.balances.find(
      (a) => a.account_id === this.selected_account_id
    );
    if(cc) {
      this.balance_after_transaction = Number(Number(this.amount_to_charge) + Number(cc.current_balance)).toFixed(2);
    }

  }

  changeSelectedAccount(account_id: number): void {
    if (this.show_refMBResult) return;

    this.selected_account_id = account_id;
    this.selected_payment_method_id = null;
    this.updateBalanceAfterTransaction();
  }

  goToDashboard(event: UIEvent) {
    event.stopPropagation();
    this._conditionService.goto('dashboard');
  }

  changeSelectedPaymentMethod(payment_method_id: string): void {

    if(payment_method_id === 'e9d4ad01-c1c8-4ac7-82f3-a373b08d030c' && !this.isTpaAvailable) {
      return;
    }
    this.selected_payment_method_id = payment_method_id;
  }

  getSelectedPaymentMethod(): any {
    const cc = this.balances.find(
      (a) => a.account_id === this.selected_account_id
    );
    return cc
      ? cc.available_methods
        ? cc.available_methods.find(
            (pm) => pm.id === this.selected_payment_method_id
          )
        : null
      : null;
  }

  /**
   *  Charge Account with money
   */
  chargeAccount(): void {

    if(!this.selected_account_id){
      this.errorType = "ERROR_FIELDS_1"; 
    }else if(!this.selected_payment_method_id){
      this.errorType = "ERROR_FIELDS_2";
    }else if(this.amount_to_charge == 0) {
      this.errorType = "ERROR_FIELDS_3";
    }else {
      LoadingAction.next(true);
      this._chargingService
      .chargeAccount(
        this.amount_to_charge.toString(),
        this.selected_account_id,
        this.selected_payment_method_id
      )
      .pipe(
        takeUntil(this._subject),
        finalize(() => LoadingAction.next(false))
      )
      .subscribe(
        (res) => {
          if (this.getSelectedPaymentMethod().tag === "MB_KIOSK") {
            this._conditionService.goto(
              `/pospayment?paymentID=${res[0].payment_id}&transaction_value=${res[0].transaction_value}`
            );
          }

          if (this.getSelectedPaymentMethod().tag === "REFMB_AMA") {
            // TODO SHOW THE DATA TO PAY
            this.show_refMBResult = true;
            this.refMBResult = res[0].payment.payment_method_data;
          }

          //this._tpaService.createPayment(res.payment_id, res.transaction_value);
          /*this.redirectTo
          ? this._conditionService.goto(this.redirectTo)
          : this._conditionService.goto("/dashboard");*/
        },
        (resultErr) => {
          if (
            resultErr.error.errors[0].type ===
            "CHARGE_AMOUNT_LOWER_THAN_ALLOWED"
          ) {
            this.errorType = resultErr.error.errors[0].type;
            this.errorData = resultErr.error.errors[0].data;
            //max_charge_amount: 50
            //min_charge_amount: 1
          }

          if (
            resultErr.error.errors[0].type ===
            "CHARGE_AMOUNT_GREATER_THAN_ALLOWED"
          ) {
            this.errorType = resultErr.error.errors[0].type;
            this.errorData = resultErr.error.errors[0].data;
          }
        }
      );
    }

  }

  /**
   * Back
   * @param {UIEvent} event
   */
  back(event: UIEvent): void {
    event.preventDefault();

    let previous = this._routeHistoryService.getPreviousUrl();
    previous = previous.substring(1);

    if (previous !== "") {
      this._conditionService.goto(previous);
    }
  }
}
