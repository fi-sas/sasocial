import { Injectable } from '@angular/core';
import { of, BehaviorSubject, Subject, Observable } from 'rxjs';
import { map, timeout, catchError, takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { FiConfigurator } from '@fi-sas/configurator';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import {
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { get } from 'lodash';

export interface Movement {
  payment_id: string,
  transaction_value: number;
  payment: any;
}

/**
 * Injectable
 */
@Injectable()
export class FiChargingService {
  /* public properties */
  public accountUser = 1;
  public amountChange = new BehaviorSubject(0);
  public counterChange = new BehaviorSubject(10);
  public interval;
  public redirectTo: string | null = null;

  /* private properties */
  private _lang = this._translateService.getLanguage();
  private _authData = null;
  private amount = 0;
  private counter = 0;
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {FiConditionService}     private _conditionService
   * @param {FiConfigurator}         private _configurator
   * @param {FiUrlService}           private _urlService
   * @param {FiResourceService}      private _resourceService
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiStorageService}       private _storageService
   * @param {ActivatedRoute}         private _activeRouter
   */

  constructor(
    private _conditionService: FiConditionService,
    private _configurator: FiConfigurator,
    private _urlService: FiUrlService,
    private _resourceService: FiResourceService,
    private _translateService: FiTranslateLazyService,
    private _storageService: FiStorageService,
    private _activeRouter: ActivatedRoute
  ) { }

  setAmount(amount: number) {
    this.amount = amount + this.amount;
    this.amountChange.next(this.amount);
  }

  accountBalance() {
    const url = this._urlService.get('CURRENT_ACCOUNT.BALANCES');
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    const languages = this._configurator.getOptionTree('LANGUAGES')[
      'LANGUAGES'
    ];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list(url, {
        headers: {
          Authorization: 'Bearer ' + this._authData.token,
          'X-Language-Acronym': this._lang
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Charge Account
   */
  chargeAccount(value: string, account: any, payment_method_id: string): Observable<Movement[]> {
    const url = this._urlService.get('CURRENT_ACCOUNT.MOVEMENTS_CHARGE_ACCOUNT');
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    const languages = this._configurator.getOptionTree('LANGUAGES')[
      'LANGUAGES'
    ];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;
    const convertedValue = value.replace(',', '.');

    const data = {
      transaction_value: Number(convertedValue),
      account_id: parseInt(account, 10),
      payment_method_id
    };

    return this._resourceService
      .create<Movement>(url, data, {
      headers: {
        Authorization: 'Bearer '.concat(this._authData.token),
        'X-Language-Acronym': this._lang
      }
    })
      .pipe(
        map(resp => {
          return resp.data;
        }),
      );
  }
}
