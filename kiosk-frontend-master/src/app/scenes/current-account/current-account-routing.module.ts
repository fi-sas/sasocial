import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCurrentAccountComponent } from './current-account.component';

const routes: Routes = [
  {
    path: 'movements',
    component: FiCurrentAccountComponent,
    loadChildren: '@fi-sas/kiosk/scenes/current-account/movement/movement.module#FiCurrentAccountListModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCurrentAccountRoutingModule {}
