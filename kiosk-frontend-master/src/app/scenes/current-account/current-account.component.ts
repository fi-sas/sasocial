import { Component } from '@angular/core';

@Component({
  selector: 'fi-current-account',
  templateUrl: './current-account.component.html',
  styleUrls: ['./current-account.component.less']
})
export class FiCurrentAccountComponent {}
