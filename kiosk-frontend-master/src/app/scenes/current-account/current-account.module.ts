import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiCurrentAccountService } from './current-account.service';
import { FiMovementService } from './movement/movement.service';
import { FiCurrentAccountComponent } from './current-account.component';
import { FiCurrentAccountRoutingModule } from './current-account-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiCartModule,
    FiCurrentAccountRoutingModule
  ],
  declarations: [
    FiCurrentAccountComponent
  ],
  providers: [
    FiCurrentAccountService,
    FiMovementService
  ]
})
export class FiCurrentAccountModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
