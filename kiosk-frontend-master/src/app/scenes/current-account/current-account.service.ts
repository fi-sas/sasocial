import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { timeout, catchError, map } from 'rxjs/operators';

import { FiUrlService, FiResourceService, FiStorageService } from '@fi-sas/core';

/**
 * Injectable
 */
@Injectable()
export class FiCurrentAccountService {

  /* public properties */

  /**
   * Constructor
   */
  constructor() {}

}