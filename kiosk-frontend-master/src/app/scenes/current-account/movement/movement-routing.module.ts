import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiCurrentAccountListComponent } from './movement.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiCurrentAccountListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiCurrentAccountListRoutingModule {}
