import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { union, find, has } from 'lodash';

import { FiBalanceService, Balance } from '@fi-sas/kiosk/components/account/balance.service';
import { FiMovementService } from '@fi-sas/kiosk/scenes/current-account/movement/movement.service';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';

@Component({
  selector: 'fi-movements-list',
  templateUrl: './movement.component.html',
  styleUrls: ['./movement.component.less']
})
export class FiCurrentAccountListComponent implements OnInit, OnDestroy {

  /* view child */
  @ViewChildren('accountMovement')
  accountMovement: QueryList<ElementRef<HTMLDivElement>>;

  /* public properties */
  public isLoading = false;
  public isUserLogged = false;
  public balances:Balance[] = [];
  public movements:any = [];

  public total = 0;
  public maxTotalLimit = 200;
  public limit = 10;
  public offset = 0;

  public selected_account_id: number = null;

  /* private propreties */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   */
  constructor(
    private _balanceService$: FiBalanceService,
    private _movementService: FiMovementService
  ) {}

  /**
   * On init
   */
  ngOnInit() {

    this.isLoading = true;

    /* balances */
    this._balanceService$.getBalances().pipe(takeUntil(this._subject)).subscribe((data) => {
      this.balances = data;
      if(this.balances.length > 0) {
        this.selected_account_id = this.balances[0].account_id;
        this.getMovements();
      }

    });

  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    this._subject.next(true);
    this._subject.unsubscribe();

  }

  changeSelectedAccount(account_id: number) {
    this.selected_account_id = account_id;
    this.getMovements(true);
  }

  /**
   * Get more movements
   */
  getMovements(reset?: boolean): void {

    this.isLoading = true;
    LoadingAction.next(true);

    if(reset) {
      this.offset = 0;
      this.movements = [];
    } else {
      if (((this.offset + this.limit) < this.total) && ((this.offset + this.limit) < this. maxTotalLimit)) {
        this.offset = this.offset + this.limit;
      }
    }


    /* movements */
    this._movementService.getMovements(this.selected_account_id, this.limit, this.offset).pipe(takeUntil(this._subject)).subscribe((res:any) => {

      const moreMovements = res.data;

      /*
      moreMovements.map((item) => {
        const account = find(this.balances, function(o) { return o.account_id === item.account_id; });
        item["account_name"] = has(account, 'account_name') ? account.account_name : '';
      });*/

      let lastElOffset = null;
      if (this.offset !== 0) {

        this.accountMovement.map(el => {
          const lastEl = el.nativeElement;
          const lastName = lastEl.getAttribute('id');

          if (lastName) {
            lastElOffset = lastEl;
          }
          return true;
        });
      }

      this.movements = this.movements.concat(moreMovements);
      this.total = res.link.total;

      this.isLoading = false;
      LoadingAction.next(false);

      if (this.offset !== 0) {
        const scrollIntoView = setTimeout(() => {
          lastElOffset.scrollIntoView({block: 'start', behavior: 'smooth'});
          clearTimeout(scrollIntoView);
        });
      }

    });

  }

  getAccount(idAcocunt: number) {
    return this.balances.find(bal => bal.account_id == idAcocunt) ? this.balances.find(bal => bal.account_id == idAcocunt).account_name : ''
  }

}
