import { Injectable } from '@angular/core';
import { Subject, of, forkJoin } from 'rxjs';
import { map, timeout, catchError, takeUntil, flatMap } from 'rxjs/operators';

import { TranslateService } from '@ngx-translate/core';
import { FiConfigurator } from '@fi-sas/configurator';
import {
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

const dayjs = require('dayjs');
const pt = require('dayjs/locale/pt');
const en = require('dayjs/locale/en');

@Injectable()
export class FiMovementService {
  /* public properties */
  public operations: any = {
    Adiantamento: 1,
    'Anulação de Adiantamento': 2,
    Carregamento: 3,
    'Fatura/Recibo': 4,
    Recibo: 5,
    Fatura: 6,
    Devolução: 7,
    Anulação: 8
  };
  public status: any = {
    Confirmed: 1,
    Pending: 2,
    Paid: 3,
    Cancelled: 4,
    'Cancelled due to Lack of Payment': 5
  };
  public lunch;
  public dinner;

  /* private properties */
  private _lang = this._translateService.getLanguage();
  private _authData = null;
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {FiConfigurator}    private _configurator
   * @param {FiResourceService} private _resourceService
   * @param {TranslateService}  private  _translate
   */
  constructor(
    private _configurator: FiConfigurator,
    private _resourceService: FiResourceService,
    private _translateService: FiTranslateLazyService,
    private _storageService: FiStorageService,
    private _translate: TranslateService,
    private _urlService: FiUrlService
  ) {}

  /**
   * Get balances
   */
  getMovements(account_id: number, limit: number, offset: number) {
    const url = this._urlService.get('CURRENT_ACCOUNT.MOVEMENTS');
    this._authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );
    this._lang = this._translateService.getLanguage();

    const serviceOptions =
      '?limit=' + limit.toString() + '&offset=' + offset.toString() + '&sort=-seq_doc_num&query[account_id]=' + account_id.toString() + '&withRelated=items';

    return this._resourceService
      .list<any>(url + serviceOptions, {
        headers: {
          Authorization: 'Bearer ' + this._authData.token,
          'X-Language-Acronym': this._lang
        }
      })
      .pipe(
        map(res => {
          /* transform status and operations */
          res.data.map(item => {
            item['status_id'] = this.status[item.status];
            item['operation_id'] = this.operations[item.operation];
            item['firstTitle'] = item.items[0].name;
            item['totalName'] = '';

            if (
              (item.items[0].article_type =
                'REFECTORY' && item.items[0].extra_info)
            ) {
              /* Category */
              forkJoin(
                this._translate.get('CURRENT_ACCOUNT.LUNCH'),
                this._translate.get('CURRENT_ACCOUNT.DINNER')
              )
                .pipe(
                  takeUntil(this._subject),
                  flatMap((response: any) => {
                    this.lunch = response[0];
                    this.dinner = response[1];

                    return of(response);
                  })
                )
                .subscribe();

              dayjs.locale(this._translate.getDefaultLang());
              const d = dayjs(
                item.items[0].extra_info.refectory_consume_at
              ).format('DD MMM YYYY');
              const meal =
                item.items[0].extra_info.refectory_meal_category === 'LUNCH'
                  ? this.lunch
                  : this.dinner;

              item['firstTitle'] = item.items[0].extra_info.refectory_meal_type;
              item['totalName'] = ' - ' + meal + ' - ' + d;
            }
          });

          return res;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }
}
