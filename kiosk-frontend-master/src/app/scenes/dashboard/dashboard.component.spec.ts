import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiDashboardComponent } from './dashboard.component';

xdescribe('DashboardComponent', () => {
  let component: FiDashboardComponent;
  let fixture: ComponentFixture<FiDashboardComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FiDashboardComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
