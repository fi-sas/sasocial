import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { KioskState } from '@fi-sas/kiosk/app.state';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

import { ApplicationState, Store } from '@fi-sas/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { FiElectronService } from '@fi-sas/kiosk/services/electron.service';
import { ConfigurationsService } from '@fi-sas/kiosk/services/configurations.service';
import { FiBarMenuService } from '../bar/menu/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fi-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class FiDashboardComponent implements OnInit, OnDestroy {
  /* public proprities */
  public isUserLogged = false;
  dataConfiguration;
  public isChargingActive = false;
  public isMobilityActive = false;
  public isRefectoryActive = false;
  public isBarActive = true;
  public isQueueActive = false;
  public validBar = true;
  modalNoLogged = false;
  dataExternal: any[] = [];

  /* private propreties */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Store<KioskState>} @Inject(ApplicationState) private _appState
   */
  constructor(
    @Inject(ApplicationState) private _appState: Store<KioskState>,
    @Inject(FiConfigurator) private configurator: FiConfigurator,
    @Inject(FiElectronService) private _electronService: FiElectronService,
    private configurationsService: ConfigurationsService,
    private router: Router,
    private _menuService: FiBarMenuService,
    private configurationGeralService: ConfigurationsService,
    private translateService: FiTranslateLazyService
  ) {
    this.dataConfiguration = this.configurationsService.getLocalStorageTotalConfiguration();
    this.getBar();
    this.translateService.getService().onLangChange.subscribe(() => {
      this.getServicesKiosk();
    })
    this.getServicesKiosk();
  }

  /**
   * On init
   */
  ngOnInit() {

  }

  getServicesKiosk() {
    this.configurationGeralService.geralServicesKiosk().pipe(first()).subscribe((data) => {
      this.dataExternal = data;
    })

  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Handler service
   * @param {MouseEvent} event
   */
  public handlerService(event: MouseEvent) {
    event.preventDefault();
  }

  relauch() {
    this._electronService.send('ELECTRON_RELAUCH');
  }

  getBar() {
    localStorage.removeItem('nameBar');
    this._menuService.getBar().pipe(first()).subscribe((bar) => {
      if (bar.length > 0) {
        localStorage.setItem('idBar', bar[0].id);
        localStorage.setItem('nameBar', bar[0].name);
        this.validBar = true;
      } else {
        this.validBar = false;
      }
      MenuAction.next({ back: true });
      this.isChargingActive = this.configurator.getOption('ACTIVATE_CHARGING', true) && this.validActive(11);
      this.isMobilityActive = this.configurator.getOption('ACTIVATE_MOBILITY', true) && this.validActive(3);
      this.isRefectoryActive = this.configurator.getOption('ACTIVATE_REFECTORY', true) && this.validActive(2);
      this.isBarActive = this.configurator.getOption('ACTIVATE_BAR', true) && this.validActive(2) && this.validBar;
      this.isQueueActive = this.configurator.getOption('ACTIVATE_QUEUE', true) && this.validActive(21);

      const storeUser = this._appState.createSlice('user');

      storeUser
        .watch()
        .pipe(takeUntil(this._subject))
        .subscribe(user => {
          return (this.isUserLogged = !!user);
        });
    })
  }

  validActive(id: number) {
    let validActiveConfig = this.dataConfiguration.find(t => t.id === id) != null && this.dataConfiguration.find(t => t.id === id) != undefined ? this.dataConfiguration.find(t => t.id === id).active : false;
    return validActiveConfig;
  }

  goLink(link: string, valid: boolean) {
    if (!valid) {
      this.router.navigateByUrl(link);
    } else {
      if (this.isUserLogged) {
        this.router.navigateByUrl(link);
      } else {
        this.modalNoLogged = true;
        setTimeout(() => this.modalNoLogged = false, 4000);
      }
    }

  }

  linkExternal(link) {
    this._electronService.send('OPEN_EXTERNAL_SERVICE', link);
  }

  getSrc(icon) {
    if (icon) {
      return 'assets/icons/' + icon + '.svg';
    }
    return 'assets/icons/icons-external.svg';
  }
}
