import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { TranslateModule } from '@ngx-translate/core';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiDashboardComponent } from './dashboard.component';
import { FiDashboardRoutingModule } from './dashboard-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    FiShareModule,
    TranslateModule,
    NgZorroAntdModule,
    AngularSvgIconModule,
    FiTickerModule,
    FiCartModule,
    FiDashboardRoutingModule
  ],
  declarations: [FiDashboardComponent]
})
export class FiDashboardModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
