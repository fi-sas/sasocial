import { Component, HostListener, HostBinding, OnInit, OnDestroy } from '@angular/core';

import { ScreenAction } from '@fi-sas/kiosk/services/screen.reducer';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { FiKeyboardService } from '@fi-sas/kiosk/components/keyboard/keyboard.service';
import { KeyboardType } from '@fi-sas/kiosk/components/keyboard/keyboard.component';


@Component({
  selector: 'fi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class FiHomeComponent implements OnInit, OnDestroy {
  @HostBinding('class.page__body') isPageBody = true;

  @HostListener('document:click', ['$event'])
  kioskActivation(event: MouseEvent) {
    event.preventDefault();

    this._conditionService.goto('dashboard');
  }

  /**
   * Constructor
   * @param {FiConditionService} private _conditionService
   */
  constructor(
    private _conditionService: FiConditionService,
    private _fiKeyboardService: FiKeyboardService,
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    this._fiKeyboardService.status.next({ visible: false, input: null, keyboardType: KeyboardType.All });
    MenuAction.next({ back: false });

  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    ScreenAction.next(false);

  }
}
