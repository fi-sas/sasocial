import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiInstallComponent } from './install.component';

const routes: Routes = [
  {
    path: '',
    component: FiInstallComponent,
    data: {
      name: 'install'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiInstallRoutingModule {}
