import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiShareModule } from '@fi-sas/share';
import { FiInstallComponent } from './install.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FiElectronService } from '@fi-sas/kiosk/services/electron.service';

xdescribe('InstallComponent', () => {
  let component: FiInstallComponent;
  let fixture: ComponentFixture<FiInstallComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          NgZorroAntdModule,
          FiShareModule,
          RouterTestingModule.withRoutes([]),
          BrowserAnimationsModule
        ],
        declarations: [FiInstallComponent],
        providers: [{ provide: FiElectronService, useClass: FiElectronService }]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiInstallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
