import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { merge } from "lodash";

import {
  HeaderActionDisable,
  HeaderActionEnable,
} from "@fi-sas/kiosk/components/header/header.reducer";
import {
  FooterActionDisable,
  FooterActionEnable,
} from "@fi-sas/kiosk/components/footer/footer.reducer";
import { MenuAction } from "@fi-sas/kiosk/components/menu/main/menu.reducer";
import { FiElectronService } from "@fi-sas/kiosk/services/electron.service";
import { FiStorageService } from "@fi-sas/core";
import { FiConfigurator } from "@fi-sas/configurator";
import { FiAuthService } from "@fi-sas/kiosk/services/auth.service";
import { concatMap, first, map, tap } from "rxjs/operators";
import { FiMiddlewareListenerService } from "@fi-sas/kiosk/services/middleware.service";
import { ConfigurationsService } from "@fi-sas/kiosk/services/configurations.service";

interface FormInstallValue {
  general: {
    lang: string;
    timeout: number;
    organization: string;
    port: string;
    port_printer: string;
    port_money: string;
    activate_charge: boolean;
    activate_mobility: boolean;
    activate_refectory: boolean;
    activate_bar: boolean;
    activate_queue: boolean;
    feed: boolean;
    latitude: number;
    longitude: number;
  };
  network: {
    device: number;
    domain: string;
  };
}

@Component({
  selector: "fi-install",
  templateUrl: "./install.component.html",
  styleUrls: ["./install.component.less"],
})
export class FiInstallComponent implements OnInit, OnDestroy {
  configTabIndex = 0;

  formInstall: FormGroup;

  showMidlewareNotPresentError = false;

  constructor(
    private _router: Router,
    private _electronService: FiElectronService,
    private _configurator: FiConfigurator,
    private _storage: FiStorageService,
    private _authService: FiAuthService,
    private _middlewareService: FiMiddlewareListenerService,
    private _configurationService: ConfigurationsService
  ) { }

  ngOnInit() {
    this._storage.reset();
    HeaderActionDisable.next({ active: false, ticker: false });
    FooterActionDisable.next({ active: false });
    MenuAction.next({ active: false });

    const urlPattern = /^(?:(?:https?):\/\/).*/i;

    this.formInstall = new FormGroup({
      general: new FormGroup({
        lang: new FormControl("pt", [Validators.required]),
        timeout: new FormControl(60, [Validators.required]),
        organization: new FormControl("IPVC", [Validators.required]),
        activate_charge: new FormControl(false),
        activate_mobility: new FormControl(false),
        activate_refectory: new FormControl(false),
        activate_bar: new FormControl(false),
        activate_queue: new FormControl(false),
        feed: new FormControl(true),
        longitude: new FormControl(null, [Validators.required]),
        latitude: new FormControl(null, [Validators.required]),
      }),
      network: new FormGroup({
        device: new FormControl(1, [Validators.required]),
        domain: new FormControl("http://62.28.241.42/", [
          Validators.required,
          Validators.pattern(urlPattern),
        ]),
      }),
    });

    this.formInstall.patchValue({
      general: {
        lang: this._electronService.preferences.has("DEFAULT_LANG")
          ? this._electronService.preferences.get("DEFAULT_LANG")
          : "pt",
        organization: this._electronService.preferences.has("ORGANIZATION")
          ? this._electronService.preferences.get("ORGANIZATION").THEME
          : "IPVC",
        timeout: this._electronService.preferences.has("SCREEN_SAVER")
          ? this._electronService.preferences.get("SCREEN_SAVER").TIME_OUT /
          1000
          : 30,
        activate_charge: this._electronService.preferences.has(
          "ACTIVATE_CHARGING"
        )
          ? this._electronService.preferences.get("ACTIVATE_CHARGING")
          : true,
        activate_mobility: this._electronService.preferences.has(
          "ACTIVATE_MOBILITY"
        )
          ? this._electronService.preferences.get("ACTIVATE_MOBILITY")
          : true,
        activate_refectory: this._electronService.preferences.has(
          "ACTIVATE_REFECTORY"
        )
          ? this._electronService.preferences.get("ACTIVATE_REFECTORY")
          : true,
        activate_bar: this._electronService.preferences.has("ACTIVATE_BAR")
          ? this._electronService.preferences.get("ACTIVATE_BAR")
          : true,
        activate_queue: this._electronService.preferences.has("ACTIVATE_QUEUE")
          ? this._electronService.preferences.get("ACTIVATE_QUEUE")
          : true,
        feed: this._electronService.preferences.has("FEED")
          ? this._electronService.preferences.get("FEED")
          : true,
        latitude: this._electronService.preferences.has("LATITUDE")
          ? this._electronService.preferences.get("LATITUDE")
          : 41.69323,
        longitude: this._electronService.preferences.has("LONGITUDE")
          ? this._electronService.preferences.get("LONGITUDE")
          : -8.83287,
      },
      network: {
        device: this._electronService.preferences.has("DEVICE_ID")
          ? this._electronService.preferences.get("DEVICE_ID")
          : "",
        domain: this._electronService.preferences.has("DOMAINS_API")
          ? this._electronService.preferences.get("DOMAINS_API")[0].HOST
          : "https://SASOCOCIAL.pt/",
      },
    });
  }

  ngOnDestroy() {
    MenuAction.next({ active: true });
  }

  onSubmit($event: Event) {
    if (!this._middlewareService.isMiddlewarePresent()) {
      this.showMidlewareNotPresentError = true;
      alert("O Kiosk não consegue estabelecer conexão com o middleware.");
      return;
    }
    this.showMidlewareNotPresentError = false;

    $event.preventDefault();

    const model: FormInstallValue = this.formInstall.value;

    const config = {
      DEFAULT_LANG: model.general.lang,
      DEVICE_ID: model.network.device,
      DOMAINS_API: [
        { HOST: model.network.domain, KEY: "@api_gateway" },
        { HOST: "", KEY: "@local" },
      ],
      SCREEN_SAVER: {
        TIME_OUT: model.general.timeout * 1000,
      },
      ORGANIZATION: {
        THEME: model.general.organization,
      },
      SERIAL_PORT: model.general.port,
      SERIAL_PORT_PRINTER: model.general.port_printer,
      SERIAL_PORT_MONEY: model.general.port_money,
      ACTIVATE_CHARGING: model.general.activate_charge,
      ACTIVATE_MOBILITY: model.general.activate_mobility,
      ACTIVATE_REFECTORY: model.general.activate_refectory,
      ACTIVATE_BAR: model.general.activate_bar,
      ACTIVATE_QUEUE: model.general.activate_queue,
      FEED: model.general.feed,
      LATITUDE: model.general.latitude,
      LONGITUDE: model.general.longitude
    };

    console.dir(this);
    console.dir(config);

    this._electronService.changes
      .pipe(
        first(),
        tap(() => {
          this._configurator.options = merge(
            this._configurator.options,
            config
          );
          return true;
        }),
        map(() => {
          return this._authService.setDeviceToken().pipe(first()).subscribe();
        })
      )
      .subscribe(() => {
        this._middlewareService
          .getCPUIDObservable()
          .pipe(first())
          .subscribe((cpuid) => {
            setTimeout(() => {
              this._configurationService
                .setCPUID(cpuid)
                .pipe(first())
                .subscribe(() => {
                  // TODO Reset the machine
                  //this._storage.reset();
                  //this._electronService.send('ELECTRON_RELAUCH');
                });

              //this._router.navigate(['/']);
              HeaderActionDisable.next({ active: true, ticker: true });
              FooterActionDisable.next({ active: true });
              MenuAction.next({ active: true });
            }, 1000);
          });
        this._middlewareService.requestCPUID();
      });
    this._electronService.storeConfig(config);


  }
}
