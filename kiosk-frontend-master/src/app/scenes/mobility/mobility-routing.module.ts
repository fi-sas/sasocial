import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiMobilityComponent } from './mobility.component';

const routes: Routes = [
  {
    path: 'places',
    component: FiMobilityComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/mobility/route-places/places.module#FiMobilityPlacesModule'
  },
  {
    path: 'routes',
    component: FiMobilityComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/mobility/route-list/route-list.module#FiMobilityRoutesListModule'
  },
  {
    path: 'detail',
    component: FiMobilityComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/mobility/route-detail/route-detail.module#FiMobilityRoutesDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiMobilityRoutingModule {}
