import { Component } from '@angular/core';

@Component({
  selector: 'fi-mobility',
  templateUrl: './mobility.component.html',
  styleUrls: ['./mobility.component.less']
})
export class FiMobilityComponent {}
