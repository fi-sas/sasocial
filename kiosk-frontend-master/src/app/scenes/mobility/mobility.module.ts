import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';

import { FiMobilityService } from './mobility.service';
import { FiMobilityComponent } from './mobility.component';
import { FiMobilityRoutingModule } from './mobility-routing.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiCartModule,
    FiMobilityRoutingModule
  ],
  declarations: [
    FiMobilityComponent
  ],
  providers: [
    {
      provide: FiMobilityService,
      useClass: FiMobilityService
    }
  ]
})
export class FiMobilityModule {
  constructor() {}
}
