import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { timeout, catchError, map } from 'rxjs/operators';

import { FiUrlService, FiResourceService, FiStorageService } from '@fi-sas/core';

/* interfaces */
export interface MobilityPlaceInterface {
  id: number;
  local: string;
  region?: string;
  institute?: boolean;
  address?: string;
  latitude?: string;
  longitude?: string;
}

/**
 * Injectable
 */
@Injectable()
export class FiMobilityService {

  /* public properties */
  public currentQueryParamOrigin: string;
  public currentQueryParamDestiny: string;
  public currentDateSelection: Date;

  public selectedOrigin:MobilityPlaceInterface = null;
  public selectedDestiny:MobilityPlaceInterface = null;

  public selectedRouteIndex = 0;

  public routes:any = null;

  /**
   * Constructor
   * @param {FiUrlService}      private _urlService
   * @param {FiStorageService}  private _storageService
   * @param {FiResourceService} private _resourceService
   */
  constructor(
    private _urlService: FiUrlService,
    private _storageService: FiStorageService,
    private _resourceService: FiResourceService
  ) {}

  /**
   * Get predefined places
   */
  getPredefinedPlaces(search) {

    const url = this._urlService.get('MOBILITY.LOCALS');

    const params = new HttpParams()
      .set('limit', '-1')
      .set('offset', '0')
      .set('sort', 'local')
      .set('searchFields','local,region')
      .set('search',search);


    return this._resourceService
      .list<MobilityPlaceInterface>(url, {
          params: params,
          ...this._storageToken()
      })
      .pipe(
        map(response => response.data),
        timeout(30000),
        catchError(() => of([]))
      );

  }

  /**
   * Get origin places
   */
  getOriginPlaces() {
    return this.getPredefinedPlaces('');
  }

  /**
   * Search route
   * @param {string} origin
   * @param {string} destiny
   * @param {date} date
   */
  searchRoute(origin: string, destiny: string, date:Date = new Date()) {

    this.currentQueryParamOrigin = origin;
    this.currentQueryParamDestiny = destiny;
    this.currentDateSelection = date;

    const params = new HttpParams()
      .set('origin', origin)
      .set('destination', destiny)
      .set(
        'date',
        `${date.getFullYear()}/${date.getMonth() +
          1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
      );

    const url = this._urlService.get('MOBILITY.ROUTE');
    const httpOptions = { ...this._storageToken(), ...{ params: params } };

    return this._resourceService
      .list(url, httpOptions)
      .pipe(
        map(response => {
          this.routes = response.data;
          return response.data;
        }),
        timeout(30000),
        catchError(() => of([]))
      );

  }

  /**
   * Storage token
   */
  private _storageToken() {

    const storageToken = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    return {
      headers: {
        Authorization: 'Bearer ' + storageToken.token
      }
    };

  }

}