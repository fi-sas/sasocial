import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiRouteDetailComponent } from './route-detail.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiRouteDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiRouteDetailRoutingModule {}
