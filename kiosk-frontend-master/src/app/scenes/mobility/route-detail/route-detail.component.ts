import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Location } from '@angular/common';
import { FiMobilityService } from '../mobility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fi-route-detail',
  templateUrl: './route-detail.component.html',
  styleUrls: ['./route-detail.component.less']
})
export class FiRouteDetailComponent implements OnInit, OnDestroy {

  /* Public properties */
  public loading = false;

  public origin = null;
  public destiny = null;
  public locale = 'pt-PT';

  public route = null;
  public routeInit = null;
  public contacts: any = null;
  public index = 0;
  /* Private properties */
  private _subject: Subject<boolean> = new Subject<boolean>();

  /**
   * Constructor
   * @param {TranslateService}  private _translateService
   * @param {FiMobilityService} private _serviceMobility
   */
  constructor(
    private _translateService: TranslateService,
    private _serviceMobility: FiMobilityService,
    private location: Location, private router: Router
  ) { }

  /**
   * On init
   */
  ngOnInit() {

    /* route selected */
    const routes = this._serviceMobility.routes;
    this.routeInit = routes[0];
    if (routes && this.routeInit.route.length > 0) {
      const index = this._serviceMobility.selectedRouteIndex;
      // const otherRoutes = steps.filter(item => (!item.prices && item.vehicle !== 'CAMINHADA') || (item.prices && !item.prices.purchase_active));

      if (this.routeInit.route[index].contacts) {
        this.contacts = this.routeInit.route[index].contacts;
      }

      this.route = this.routeInit.route[index];
      this.origin = this._serviceMobility.selectedOrigin;
      this.destiny = this._serviceMobility.selectedDestiny;
      this.index = index;

    }

    /* change language */
    this._translateService.onLangChange.pipe(takeUntil(this._subject)).subscribe(
      (event: LangChangeEvent) => {
        this.locale = event.lang + '-' + event.lang.toUpperCase();
      }
    );

  }

  convertDuration(value): string {
    const hours = Math.floor(value / 60);
    const minutes = value % 60;
    let end = hours != 0 ? ((hours.toString()).split('.')[0] + ' h ' + (minutes.toString()).split('.')[0] + ' m') : ((minutes.toString()).split('.')[0] + ' m');
    return end;
  }

  goPlaces() {
    localStorage.setItem('Origin', JSON.stringify(this.origin));
    localStorage.setItem('Destiny', JSON.stringify(this.destiny));
    this.router.navigateByUrl('/mobility/places');
  }

  goBack() {
    this.location.back();
  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    this._subject.next(true);
    this._subject.unsubscribe();

  }

}
