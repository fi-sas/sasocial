import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

import { FiRouteItemModule } from '@fi-sas/kiosk/components/route/item/route-item.module';
import { FiContextTitleModule } from '@fi-sas/kiosk/components/context/title/title.module';


import { FiMobilityService } from '../mobility.service';
import { FiRouteDetailComponent } from './route-detail.component';
import { FiRouteDetailRoutingModule } from './route-detail-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    RouterModule,
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule,
    FiContextTitleModule,
    FiRouteItemModule,
    FiRouteDetailRoutingModule
  ],
  declarations: [FiRouteDetailComponent]
})
export class FiMobilityRoutesDetailModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
