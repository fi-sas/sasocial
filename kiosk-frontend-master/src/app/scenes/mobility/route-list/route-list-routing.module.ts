import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiRouteListResolver } from './route-list.resolver';
import { FiRouteListComponent } from './route-list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiRouteListComponent,
    resolve: {
      routes: FiRouteListResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiRouteListRoutingModule {}
