import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, first, tap } from 'rxjs/operators';

import { FiMobilityService } from '../mobility.service';

/**
 * Component
 */
@Component({
  selector: 'fi-route-list',
  templateUrl: './route-list.component.html',
  styleUrls: ['./route-list.component.less']
})
export class FiRouteListComponent implements OnInit, OnDestroy {

  /* Public properties */
  public routing = [];
  public loading = false;

  public origin = null;
  public destiny = null;

  /* Private properties */
  private _subject: Subject<boolean> = new Subject<boolean>();

  /**
   * Constructor
   * @param {ActivatedRoute}    private _activatedRoute
   * @param {FiMobilityService} private _serviceMobility
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _serviceMobility: FiMobilityService,
    private router: Router
  ) {}

  /**
   * On init
   */
  ngOnInit() {

    /* resolver */
    this._activatedRoute.data.pipe(takeUntil(this._subject)).subscribe(({ routes }) => {
      this.routing = routes;
    });

    this.origin = this._serviceMobility.selectedOrigin;
    this.destiny = this._serviceMobility.selectedDestiny;

  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    this._subject.next(true);
    this._subject.unsubscribe();

  }

  /**
   * Handler day change
   * @param {number} timestamp
   */
  handlerDayChange(timestamp: number) {
    const date = new Date(timestamp);

    this.requestNewMobilitySearch(
      this._serviceMobility.currentQueryParamOrigin,
      this._serviceMobility.currentQueryParamDestiny,
      date
    );
  }

  goPlaces(){
    localStorage.setItem('Origin', JSON.stringify(this.origin));
    localStorage.setItem('Destiny', JSON.stringify(this.destiny));
    this.router.navigateByUrl('/mobility/places');
  }

  /**
   * Handler trip change
   * @param {'ARRIVAL' | 'DEPARTURE'} _tripeType
   */
  handlerTripChange(_tripeType: 'ARRIVAL' | 'DEPARTURE') {
    const destiny =
      this._serviceMobility.currentQueryParamDestiny ===
      this._serviceMobility.currentQueryParamDestiny
        ? this._serviceMobility.currentQueryParamOrigin
        : this._serviceMobility.currentQueryParamDestiny;
    const origin =
      this._serviceMobility.currentQueryParamOrigin ===
      this._serviceMobility.currentQueryParamOrigin
        ? this._serviceMobility.currentQueryParamDestiny
        : this._serviceMobility.currentQueryParamOrigin;

    this.requestNewMobilitySearch(
      origin,
      destiny,
      this._serviceMobility.currentDateSelection
    );
  }

  /**
   * Request new mobility search
   * @param {string} origin
   * @param {string} destiny
   * @param {Date}   date
   */
  requestNewMobilitySearch(origin: string, destiny: string, date: Date) {
    this.loading = true;

    this._serviceMobility
      .searchRoute(origin, destiny, date)
      .pipe(
        first(),
        tap(() => (this.loading = false)),
        takeUntil(this._subject)
      )
      .subscribe(routes => {
        this.routing = routes;
      });
  }
}
