import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

import { FiFifteenDayModule } from '@fi-sas/kiosk/components/day/fifteen/fifteen.module';
import { FiRouteItemModule } from '@fi-sas/kiosk/components/route/item/route-item.module';
import { FiContextTitleModule } from '@fi-sas/kiosk/components/context/title/title.module';
import { FiOptionsTripModule } from '@fi-sas/kiosk/components/options/trip/options-trip.module';

import { FiMobilityService } from '../mobility.service';
import { FiRouteListResolver } from './route-list.resolver';
import { FiRouteListComponent } from './route-list.component';
import { FiRouteListRoutingModule } from './route-list-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    RouterModule,
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule,
    FiContextTitleModule,
    FiOptionsTripModule,
    FiFifteenDayModule,
    FiRouteItemModule,
    FiRouteListRoutingModule
  ],
  declarations: [FiRouteListComponent],
  providers: [
    {
      provide: FiRouteListResolver,
      useClass: FiRouteListResolver,
      deps: [FiMobilityService]
    }
  ]
})
export class FiMobilityRoutesListModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
