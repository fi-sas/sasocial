import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve
} from '@angular/router';
import { FiMobilityService } from '../mobility.service';

export class FiRouteListResolver implements Resolve<any> {
  constructor(private _serviceMobility: FiMobilityService) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot}    state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const origin = route.queryParamMap.get('origin');
    const destiny = route.queryParamMap.get('destiny');
    const date = route.queryParamMap.get('date')
      ? new Date(route.queryParamMap.get('date'))
      : new Date();

    return this._serviceMobility.searchRoute(origin, destiny, date);

  }
}
