import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiPlacesResolver } from './places.resolver';
import { FiMobilityPlacesComponent } from './places.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiMobilityPlacesComponent,
    resolve: {
      places: FiPlacesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiMobilityPlacesRoutingModule {}
