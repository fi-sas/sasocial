import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, first, takeUntil } from 'rxjs/operators';
import { clone, has, get } from 'lodash';
import { FiMobilityService, MobilityPlaceInterface } from '../mobility.service';

/**
 * Component
 */
@Component({
  selector: 'fi-mobility-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.less']
})
export class FiMobilityPlacesComponent implements OnInit, OnDestroy {

  /* Public properties */
  public swapped = false;

  public originPlaces: MobilityPlaceInterface[];
  public destinyPlaces: MobilityPlaceInterface[];

  public selectedOrigin: MobilityPlaceInterface;
  public selectedDestiny: MobilityPlaceInterface;
  public selectedRoute: any;

  public inputSearchOrigin = new FormControl();
  public inputSearchDestiny = new FormControl();

  public showSearchOrigin = false;
  public showSearchDestiny = true;

  /* Private properties */
  private _subject: Subject<boolean> = new Subject<boolean>();

  /**
   * Constructor
   * @param {FiMobilityService} private _serviceMobility
   * @param {ActivatedRoute} private _route
   */
  constructor(
    private _serviceMobility: FiMobilityService,
    private _route: ActivatedRoute,
  ) { }

  /**
   * On init
   */
  ngOnInit() {

    /* resolver origins / destines */
    this._route.data.pipe(takeUntil(this._subject)).subscribe(({ places }) => {
      this.originPlaces = places.origins;
      this.destinyPlaces = places.destinies;
    });


    if (localStorage.getItem('Origin') && localStorage.getItem('Destiny')) {
      this.selectedOrigin = JSON.parse(localStorage.getItem('Origin'));
      this.selectedDestiny = JSON.parse(localStorage.getItem('Destiny'));
      localStorage.removeItem('Origin');
      localStorage.removeItem('Destiny');
    }
  }

  setValueInputSearchDestiny(){
    this.inputSearchDestiny.setValue((<HTMLInputElement>document.getElementById("searchDestiny")).value);
    this.getPlacesFilter(this.inputSearchDestiny.value, 'destiny');
  }

  setValueInputSearchOrigin(){
    this.inputSearchOrigin.setValue((<HTMLInputElement>document.getElementById("searchOrigin")).value);
    this.getPlacesFilter(this.inputSearchOrigin.value, 'origin');
  }

  getPlacesFilter(search, type) {
    this._serviceMobility.getPredefinedPlaces(search).pipe(first()).subscribe((data) => {
      if (type == 'destiny') {
        this.destinyPlaces = data;
      } else {
        this.originPlaces = data;
      }

    })
  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    this._subject.next(true);
    this._subject.unsubscribe();

  }

  /**
   * Handler swap places
   */
  handlerSwap() {

    this.swapped = !this.swapped;


    this.showSearchDestiny = !this.showSearchDestiny;
    this.showSearchOrigin = !this.showSearchOrigin;

    /* go to origin */
    if (this.swapped) {
      const auxsearchDesti = this.inputSearchDestiny.value;
      const auxsearchOrig = this.inputSearchOrigin.value;
      if(this.inputSearchDestiny.value && this.inputSearchOrigin.value) {
        this.inputSearchOrigin.setValue(auxsearchDesti);
        this.inputSearchDestiny.setValue(auxsearchOrig);
      }else if (this.inputSearchDestiny.value) {
        this.inputSearchOrigin.setValue(this.inputSearchDestiny.value);
        this.inputSearchDestiny.reset('', { emitEvent: false });
      } else if (this.inputSearchOrigin.value) {
        this.inputSearchDestiny.setValue(this.inputSearchOrigin.value);
        this.inputSearchOrigin.reset('', { emitEvent: false });
      } 
      const origins = clone(this.originPlaces);
      const selectedOrigin = clone(this.selectedOrigin);

      this.originPlaces = this.destinyPlaces;
      this.destinyPlaces = origins;
      this.selectedOrigin = this.selectedDestiny;
      this.selectedDestiny = selectedOrigin;



      /* go to destiny */
    } else {
      const auxsearchDesti = this.inputSearchDestiny.value;
      const auxsearchOrig = this.inputSearchOrigin.value;
      if(this.inputSearchDestiny.value && this.inputSearchOrigin.value) {
        this.inputSearchOrigin.setValue(auxsearchDesti);
        this.inputSearchDestiny.setValue(auxsearchOrig);
      }else if (this.inputSearchDestiny.value) {
        this.inputSearchOrigin.setValue(this.inputSearchDestiny.value);
        this.inputSearchDestiny.reset('', { emitEvent: false });
      } else if (this.inputSearchOrigin.value) {
        this.inputSearchDestiny.setValue(this.inputSearchOrigin.value);
        this.inputSearchOrigin.reset('', { emitEvent: false });
      } 
      const destinies = clone(this.destinyPlaces);
      const selectedDestiny = clone(this.selectedDestiny);

      this.destinyPlaces = this.originPlaces;
      this.originPlaces = destinies;
      this.selectedDestiny = this.selectedOrigin;
      this.selectedOrigin = selectedDestiny;

    }

  }

  /**
   * Handler selected origin
   * @param {any} origin
   */
  handlerOrigin(origin: any) {
    this.selectedOrigin = origin;
    this._serviceMobility.selectedOrigin = this.selectedOrigin;
    if (this.inputSearchOrigin.value) {
      this.inputSearchOrigin.reset('', { emitEvent: false });
      this.getPlacesFilter('', 'origin');
    }


  }

  /**
   * Handler selected destiny
   * @param {any} destiny
   */
  handlerDestiny(destiny: any) {
    this.selectedDestiny = destiny;
    this._serviceMobility.selectedDestiny = this.selectedDestiny;
    if (this.inputSearchDestiny.value) {
      this.inputSearchDestiny.reset('', { emitEvent: false });
      this.getPlacesFilter('', 'destiny');
    }
  }

}
