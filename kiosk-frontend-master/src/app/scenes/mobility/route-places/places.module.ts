import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FiShareModule } from '@fi-sas/share';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiContextTitleModule } from '@fi-sas/kiosk/components/context/title/title.module';

import { FiPlacesResolver } from './places.resolver';
import { FiMobilityService } from '../mobility.service';
import { FiMobilityPlacesComponent } from './places.component';
import { FiMobilityPlacesRoutingModule } from './places-routing.module';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiKeyboardModule } from '@fi-sas/kiosk/components/keyboard/keyboard.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    AngularSvgIconModule,
    FiShareModule,
    FiMobilityPlacesRoutingModule,
    FiContextTitleModule,
    FiDirectivesModule,
    FiKeyboardModule,
  ],
  declarations: [FiMobilityPlacesComponent],
  providers: [
    {
      provide: FiPlacesResolver,
      useClass: FiPlacesResolver,
      deps: [FiMobilityService]
    }
  ]
})
export class FiMobilityPlacesModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
