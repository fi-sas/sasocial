import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { FiMobilityService } from '../mobility.service';

export class FiPlacesResolver implements Resolve<any> {
  constructor(private _serviceMobility: FiMobilityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return forkJoin(
      this._serviceMobility.getOriginPlaces(),
      this._serviceMobility.getPredefinedPlaces('')
    ).pipe(
      flatMap(response => {
        return of({ origins: response[0], destinies: response[1] });
      })
    );
  }
}
