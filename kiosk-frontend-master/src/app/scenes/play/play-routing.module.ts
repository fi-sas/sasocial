import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiPlayComponent } from './play.component';

const routes: Routes = [
  {
    path: '',
    component: FiPlayComponent,
    data: {
      name: 'play'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiPlayRoutingModule {}
