import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiPlayComponent } from './play.component';

describe('PlayComponent', () => {
  let component: FiPlayComponent;
  let fixture: ComponentFixture<FiPlayComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FiPlayComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
