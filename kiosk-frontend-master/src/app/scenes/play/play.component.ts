import { Component, OnInit } from '@angular/core';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';

@Component({
  selector: 'fi-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.less']
})
export class FiPlayComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    MenuAction.next({ active: false });
  }
}
