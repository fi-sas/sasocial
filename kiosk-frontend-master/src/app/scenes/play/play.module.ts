import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiPlayRoutingModule } from './play-routing.module';
import { FiPlayComponent } from './play.component';

@NgModule({
  imports: [CommonModule, FiPlayRoutingModule],
  declarations: [FiPlayComponent]
})
export class FiPlayModule {}
