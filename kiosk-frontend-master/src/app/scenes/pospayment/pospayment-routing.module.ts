import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiPosPaymentComponent } from './pospayment.component';


const routes: Routes = [  {
  path: '',
  component: FiPosPaymentComponent,
  data: {
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiPospaymentRoutingModule { }
