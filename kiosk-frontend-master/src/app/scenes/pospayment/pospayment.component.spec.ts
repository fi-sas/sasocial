import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PospaymentComponent } from './pospayment.component';

describe('PospaymentComponent', () => {
  let component: PospaymentComponent;
  let fixture: ComponentFixture<PospaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PospaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PospaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
