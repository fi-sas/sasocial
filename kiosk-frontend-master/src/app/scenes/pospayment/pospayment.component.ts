import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FiPosPaymentService } from './pospayment.service';
import { first, finalize, catchError, delay } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FiConditionService } from '@fi-sas/kiosk/services/condition.service';
import { FiTpaService, FiTpaStatusEnum } from '@fi-sas/kiosk/services/tpa.service';
import { FiRoutingHistoryService } from "@fi-sas/kiosk/services/routing.service";

@Component({
  selector: 'fi-sas-pospayment',
  templateUrl: './pospayment.component.html',
  styleUrls: ['./pospayment.component.less'],
  animations: [
    trigger('fadeAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms', style({ opacity: 1 })),
      ]),
    ]),
  ]
})
export class FiPosPaymentComponent implements OnInit, OnDestroy {

  paymentID: string = null;
  transaction_value: number = null;
  posStatusSubscription: Subscription;
  posTransactionSubscription: Subscription;
  loading = false;
  FiTpaStatusEnum = FiTpaStatusEnum;
  status: FiTpaStatusEnum = FiTpaStatusEnum.WAITING;
  payment: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _routeHistoryService: FiRoutingHistoryService,
    private _pospaymentService: FiPosPaymentService,
    private _tpaService: FiTpaService,
    private _conditionService: FiConditionService
  ) {

  }

  ngOnInit(): void {
    this._conditionService.disable();
    this.paymentID = this._activatedRoute.snapshot.queryParams.paymentID;
    this.transaction_value = this._activatedRoute.snapshot.queryParams.transaction_value;

    if (!this.paymentID || !this.transaction_value) {
      // UPS SOMETHING WRONG WITH THE URL
      this.fail();
    }

    MenuAction.next({ active: false });
    this.getPayment();
    /***
     * 1 - GET THE ORDER (IF DONT EXIST ?)
     * 2 - CHECK IF IS STATUS 'PENDING' (IF IS NOT 'PENDING' ?)
     * 3 - CHECK TPA CONNECTION AND INIT PAYMENT
     * 4 - LISTEN TO EVENTS
     */
  }


  ngOnDestroy() {
    this._conditionService.enable();
    this._routeHistoryService.resetHistory();

    MenuAction.next({ active: true });
    if (this.posStatusSubscription) {
      this.posStatusSubscription.unsubscribe();
    }

    if(this.posTransactionSubscription) {
      this.posTransactionSubscription.unsubscribe();
    }
  }

  getPayment() {
    LoadingAction.next(true);
    this.loading = true;
    this._pospaymentService.getPayment(this.paymentID).pipe(
      first(),
      delay(2000),
      finalize(() => this.validateOder())
    ).subscribe(result => {
      this.payment = result.data[0];
    });
  }

  private validateOder() {
    if (!this.payment) {
      // ORDER NOT FOUND
      LoadingAction.next(false);
      //
      return;
    }

    if (this.payment.status !== 'pending') {
      // ORDER IS NOT PENDING
      LoadingAction.next(false);
      //return;
    }


    this.posTransactionSubscription = this._tpaService.getTpaTransactionObservable().subscribe(transaction => {
      if(transaction) {
        if(transaction.status === "ERROR")
            this.fail();
        if(transaction.status === "CANCELLED")
            this.cancel();
        if(transaction.status === "PAYED")
            this.success();
      } else {
        this.fail();
      }
    });

    this.posStatusSubscription = this._tpaService.getTpaStatusObservable().subscribe(status => {
      if(status) {
        this.status = status;
      }
    });

    this._tpaService.createPayment(
      this.paymentID,
      this.transaction_value,
    );

    LoadingAction.next(false);
  }

  success() {
    const timeout = setTimeout(() => {
      this._router
        .navigate(['/cart/success'], {
          queryParams: {
            // redirect: multiCart ? '/cart/list' : null
          }
        });
    }, 1000)
  }

  cancel() {
    const timeout = setTimeout(() => {
      this._router
        .navigate(['/cart/cancel'], {
          queryParams: {
            // redirect: multiCart ? '/cart/list' : null
          }
        });
    }, 1000)
  }

  fail() {
    const timeout = setTimeout(() => {
      this._router
        .navigate(['/cart/error'], {
          queryParams: {
            // redirect: multiCart ? '/cart/list' : null
          }
        });
    }, 1000)
  }
}
