import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiPospaymentRoutingModule } from './pospayment-routing.module';
import { FiPosPaymentComponent } from './pospayment.component';
import { FiShareModule } from '../../../../libs/share/src/lib/share.module';
import { FiPosPaymentService } from './pospayment.service';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { TranslateModule } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

@NgModule({
  declarations: [FiPosPaymentComponent],
  imports: [
    CommonModule,
    FiShareModule,
    TranslateModule,
    NgZorroAntdModule,
    NgxQRCodeModule,
    FiPospaymentRoutingModule
  ],
  providers: [
    FiPosPaymentService
  ]
})
export class FiPosPaymentSceneModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}