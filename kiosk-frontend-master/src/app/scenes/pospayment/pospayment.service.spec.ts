import { TestBed } from '@angular/core/testing';

import { PospaymentService } from './pospayment.service';

describe('PospaymentService', () => {
  let service: PospaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PospaymentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
