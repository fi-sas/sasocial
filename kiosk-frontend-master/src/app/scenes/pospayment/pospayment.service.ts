import { Injectable } from "@angular/core";
import {
  Resource,
  FiResourceService,
} from "../../../../libs/core/src/lib/resource.service";
import { FiUrlService } from "../../../../libs/core/src/lib/url.service";
import { FiAuthPresenter } from "../authentication/authentication.presenter";
import { FiStorageService } from "../../../../libs/core/src/lib/storage.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FiPosPaymentService {
  private _authData = null;

  constructor(
    private _urlService: FiUrlService,
    private _storageService: FiStorageService,
    private _resourceService: FiResourceService
  ) {}

  getPayment(paymentID: string): Observable<Resource<any>> {
    const url = this._urlService.get("PAYMENTS.PAYMENT_ID", { id: paymentID });
    this._authData =
      this._storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this._resourceService.read<any>(url, {
      headers: {
        Authorization: "Bearer ".concat(this._authData.token),
      },
    });
  }

  confirmPayment(
    paymentID: string,
    clientReceipt: string,
    merchantReceipt: string,
    CPUID: string
  ): Observable<Resource<any>> {
    const url = this._urlService.get("PAYMENTS.CONFIRM_TPA_PAYMENT");
    this._authData =
      this._storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this._resourceService.create<any>(
      url,
      {
        id: paymentID,
        client_receipt: clientReceipt,
        merchant_receipt: merchantReceipt,
        CPUID,
      },
      {
        headers: {
          Authorization: "Bearer ".concat(this._authData.token),
        },
      }
    );
  }

  cancelPayment(paymentID: string, CPUID: string): Observable<Resource<any>> {
    const url = this._urlService.get("PAYMENTS.CONFIRM_TPA_PAYMENT");
    this._authData =
      this._storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this._resourceService.create<any>(
      url,
      {
        id: paymentID,
        cancel: true,
        CPUID,
      },
      {
        headers: {
          Authorization: "Bearer ".concat(this._authData.token),
        },
      }
    );
  }

  periodEvent(receipt: string): Observable<Resource<any>> {
    const url = this._urlService.get("PAYMENTS.TPA_PERIOD_EVENT");
    this._authData =
      this._storageService.get<{ token: string }>("DEVICE_TOKEN");

    return this._resourceService.create<any>(
      url,
      {
        receipt
      },
      {
        headers: {
          Authorization: "Bearer ".concat(this._authData.token),
        },
      }
    );
  }
}
