
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TranslationModel } from './models/translation.model';

@Pipe({
  name: 'errorTranslation'
})
export class ErrorTranslationPipe implements PipeTransform {
  constructor(public translate: TranslateService) {

  }

  transform(value: TranslationModel[]): string {
    if(value.length > 0){
      return value.find((tran) => tran.language.acronym == this.translate.currentLang).message;
    }else{
      return '';
    }

  }

}