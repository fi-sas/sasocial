import { SubjectModel } from "./subject.model";

export class ServiceModelQueue {
    id?: number;
    active: boolean;
    updated_at?: Date;
    created_at?: Date;
    subjects: SubjectModel[];
    translations: TranslationModel[];
}

export class TranslationModel {
    id: number;
    name: string;
    service_id: number;
    language_id: number;
    updated_at?: Date;
    created_at?: Date;
}