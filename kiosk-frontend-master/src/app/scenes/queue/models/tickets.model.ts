import { SubjectModel } from "./subject.model";

export class TicketsModel {
    id: number;
    sequencial_number: number;
    order_number: number;
    estimated_time: Date;
    origin: string;
    call_number: number;
    call_times: number;
    initial_time: Date;
    final_time: Date;
    appointment_number: number;
    appointment_schedule: Date;
    appointment_reason: string;
    priority: boolean;
    notes: string;
    status: string;
    subject_id: number;
    email: string;
    student_number: string;
    type: string;
    user_id: number;
    attended_by_id: number;
    attended_on: number;
    created_at: Date;
    tickets_in_queue: number;
    subject: SubjectModel;
    ticket_code: string;
}