import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { Subscription } from 'rxjs';
import { finalize, first } from 'rxjs/operators';
import { issueTicketModel } from './models/issue-ticket.model';
import { ServiceModelQueue } from './models/service.model';
import { SubjectModel } from './models/subject.model';
import { TicketsModel } from './models/tickets.model';
import { TranslationModel } from './models/translation.model';
import { FiQueueService } from './queue.service';
import { FiPrintService } from './../../services/print.service';
const dayjs = require('dayjs');

@Component({
  selector: 'fi-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.less']
})
export class FiQueueComponent implements OnInit, OnDestroy {

  public subjectSelected: SubjectModel = new SubjectModel();
  public loading = true;
  public services: ServiceModelQueue[] = [];
  public modalError = false;
  public modalSuccess = false;
  public printerUnavailable = false;
  public responseDataTicket: TicketsModel = new TicketsModel();
  public translation: TranslationModel[] = [];
  public translationsSubscription: Subscription = null;

  constructor(
    public queueService: FiQueueService,
    public translateService: FiTranslateLazyService,
    public printerService: FiPrintService,
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this.translationsSubscription = this.translateService.getService().onLangChange.subscribe(() => {
      this.getServices();
    })
    this.getServices();
  }

  ngOnDestroy() {
    this.translationsSubscription.unsubscribe();
  }

  getServices() {
    this.queueService.listServices().pipe(first(), finalize(() => (this.loading = false))).subscribe((data) => {
      this.services = data;
    })
  }
  transformSecontToHour(value: number) {
    let h = Math.floor(value / 3600);
    let m = Math.floor(value % 3600 / 60);
    let hDisplay;
    let mDisplay;
    if (h == 0) {
      hDisplay = '00';
    } else if (h < 10) {
      hDisplay = '0' + h;
    } else {
      hDisplay = h;
    }
    if (m == 0) {
      mDisplay = '00';
    } else if (m < 10) {
      mDisplay = m + '0';
    } else {
      mDisplay = m;
    }
    return hDisplay + 'h' + mDisplay + 'm';
  }

  selected(subject: SubjectModel) {

    if (this.subjectSelected.id != undefined && this.subjectSelected.id == subject.id) {
      this.subjectSelected = new SubjectModel();
    } else {
      this.subjectSelected = subject;
    }
  }

  issueTicket() {

    if(!this.printerService.isAvailableToPrint()) {
      setTimeout(() => this.printerUnavailable = false, 5000);
      this.printerUnavailable = true;
      return;
    }

    let sendValues: issueTicketModel = new issueTicketModel();
    sendValues.priority = false;
    sendValues.subject_id = this.subjectSelected.id;
    this.queueService.issueTicket(sendValues).pipe(first()).subscribe((data) => {
      this.subjectSelected = new SubjectModel();
      this.responseDataTicket = data.data[0].ticket;

      this.printerService.printTicket({
        date: dayjs(this.responseDataTicket.created_at).format("YYYY-MM-DD"),
        hour: dayjs(this.responseDataTicket.created_at).format("HH:mm"),
        ticket: this.responseDataTicket.ticket_code,
        current_ticket: "---",
        people_after_you: data.data[0].ntickets.toString(),
        estimated_hour: dayjs(this.responseDataTicket.estimated_time).format("HH:mm"),
      });
      this.modalSuccess = true;

      setTimeout(() => this.modalSuccess = false, 5000);

    },
      (error: HttpErrorResponse) => {
        this.translation = [];
        this.translation = error.error.errors[0].translations;
        this.subjectSelected = new SubjectModel();
        this.modalError = true;
        setTimeout(() => this.modalError = false, 5000);
      })
  }
}
