import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { TranslateModule } from '@ngx-translate/core';

import { NgZorroAntdModule, NzGridModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiQueueRoutingModule } from './queue-routing.module';
import { FiQueueComponent } from './queue.component';
import { FiQueueService } from './queue.service';
import { ErrorTranslationPipe } from './error-translation.pipe';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

@NgModule({
  imports: [
    FiShareModule,
    TranslateModule,
    NgZorroAntdModule,
    AngularSvgIconModule,
    FiTickerModule,
    FiCartModule,
    FiQueueRoutingModule,
    NzGridModule,
    FiDirectivesModule
  ],
  declarations: [FiQueueComponent, ErrorTranslationPipe],
  providers: [
    {
      provide: FiQueueService,
      useClass: FiQueueService
    }
  ]
})
export class FiQueueModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
