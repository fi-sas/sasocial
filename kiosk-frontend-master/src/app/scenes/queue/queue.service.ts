
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import {
    FiResourceService,
    FiStorageService,
    FiUrlService
} from '@fi-sas/core';
import { ServiceModelQueue } from './models/service.model';
import { issueTicketModel } from './models/issue-ticket.model';
import { ResponseTicketModel } from './models/response-isseu-ticket.model';
import { catchError, map, timeout } from 'rxjs/operators';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

@Injectable()

export class FiQueueService {

    private authData = null;
  

    constructor(private resourceService: FiResourceService,
        private storageService: FiStorageService,
        private urlService: FiUrlService,
        private _translateService: FiTranslateLazyService,
    ) { }

    issueTicket(sendValue: issueTicketModel) {
        const url = this.urlService.get('QUEUE.ISSUETICKET');
        this.authData = this.storageService.get<{ token: string }>(
          'DEVICE_TOKEN'
        );
    
        return this.resourceService.create<ResponseTicketModel>(url, sendValue, {
          headers: {
            Authorization: 'Bearer '.concat(this.authData.token)
          }
        });
    }

    listServices() {
        const url = this.urlService.get('QUEUE.SERVICES');
        const params = new HttpParams()
            .set('query[active]', 'true');

        return this.resourceService
            .list<ServiceModelQueue>(url, {
                params: params,
                ...this.storageToken()
            })
            .pipe(
                map(response => response.data),
                timeout(30000),
                catchError(() => of([]))
            );
    }


    /**
     * Storage token
     */
    private storageToken() {
        let _lang = this._translateService.getLanguage();
        const storageToken = this.storageService.get<{ token: string }>(
            'DEVICE_TOKEN'
        );

        return {
            headers: {
                Authorization: 'Bearer ' + storageToken.token,
                'X-Language-Acronym': _lang
            }
        };

    }
}
