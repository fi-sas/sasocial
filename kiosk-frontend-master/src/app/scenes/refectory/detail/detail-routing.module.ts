import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiFoodDetailResolver } from './detail.resolver';
import { FiFoodDetailComponent } from './detail.component';

const routes: Routes = [
  {
    path: ':service_id/meal/:dish_id/:dish_type_id/:day/:dish_type/:code',
    pathMatch: 'full',
    component: FiFoodDetailComponent,
    resolve: {
      product: FiFoodDetailResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiDetailRoutingModule {}
