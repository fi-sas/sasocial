import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil, first, finalize } from 'rxjs/operators';
import { get } from 'lodash';

import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/kiosk/app.state';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiFoodDetailService } from './detail.service';
import { FiShoppingCartService } from '@fi-sas/kiosk/components/cart/shopping-cart.service';

/**
 * Component
 */
@Component({
  selector: 'fi-food-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class FiFoodDetailComponent implements OnInit, OnDestroy {
  loading = false;
  /* Public propreties */
  public product = null;
  public cartQuantity = 0;
  public stock = 999;
  // TODO: When personal payment is actived this should be removed
  public isUserLogged = false;
  public validProduct = '';
  /* Private propreties */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {TranslateService}       private _translate
   * @param {FiConditionService}     private _conditionService
   * @param {ActivatedRoute}         private _activatedRoute
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiFoodDetailService}    private _mealService
   * @param {Store<KioskState>}   @Inject(ApplicationState) private
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _translateService: FiTranslateLazyService,
    private _mealService: FiFoodDetailService,
    private shoppingCartService: FiShoppingCartService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) { }

  /**
   * On init
   */
  ngOnInit() {
   
    // TODO: When personal payment is actived this should be removed
    const storeUser = this._appState.createSlice('user');
    storeUser
      .watch()
      .pipe(takeUntil(this._subject))
      .subscribe(user => (this.isUserLogged = !!user));
    // ------

    /* resolver */
    this._activatedRoute.data.pipe(takeUntil(this._subject)).subscribe(data => {
      this.product = get(data, 'product[0]', null);
      this.existProduct();
    });


    /* language change*/
    this._translateService
      .getService()
      .onLangChange.pipe(takeUntil(this._subject))
      .subscribe(() => {
        this._mealService
          .getMeals(this._mealService.serviceID, this._mealService.dishID)
          .pipe(takeUntil(this._subject))
          .subscribe(res => {
            this.product = res[0];
          });
      });

    if (this.product && this.isUserLogged) {
      this.shoppingCartService.getAllProducts()
        .pipe(first())
        .subscribe(
          (result) => {
            if (result.length > 0) {
              result.forEach(prod => {
                prod.items.forEach(item => {
                  if (item.extra_info.menu_dish_id == this.product.id) {
                    this.cartQuantity = item.quantity;
                    this.stock = item.max_quantity;
                  }
                });
              })
            } else {
              this.cartQuantity = 0;
            }

          }
        );
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  existProduct() {
    this.validProduct = '';
    this.shoppingCartService.cartData$.subscribe(
      (cartObjects) => {
        if(cartObjects.length == 0) {
          this.stock = 999;
          this.cartQuantity = 0;
        } else {
          cartObjects.forEach(cart => {
            if (cart.items.length) {
              cart.items.forEach(item => {
                if (item.extra_info && item.extra_info.menu_dish_id === this.product.id) {
                  this.validProduct = item.id;
                  this.stock = item.max_quantity;
                }
              });
            }
          });
        }
       
      }
    );
  }

  /**
   * Handler quantity change
   * @param {object} item object
   */
  handlerQtChange(item: { action: 'add' | 'remove', quantity: 0}) {
    this.loading = true;
    this.existProduct();
    
    if (item.action == 'add') {
      if(this.validProduct == '') {
        this.shoppingCartService.addToCartAlimentation(this.product.id).pipe(
          first()
        ).subscribe(
          () => {
            this.shoppingCartService.getAllProducts()
              .pipe(first(),finalize(()=>this.loading=false))
              .subscribe(
                (result) => {
                  this.shoppingCartService.getDataNotification();
                  
                }
              );
          }
        );
      }else {
        this.shoppingCartService.changeQtdItem(this.validProduct, item.quantity).pipe(first(),finalize(()=>this.loading=false)).subscribe(()=> {
          this.shoppingCartService.getDataNotification();
        })
      }
      
    }else{
      if(item.quantity == 0) {
        this.shoppingCartService.removeItemCard(this.validProduct).pipe(first(),finalize(()=>this.loading=false)).subscribe(()=> {
          this.shoppingCartService.getDataNotification();
        })
      }else {
        this.shoppingCartService.changeQtdItem(this.validProduct, item.quantity).pipe(first(),finalize(()=>this.loading=false)).subscribe(()=> {
          this.shoppingCartService.getDataNotification();
        })
      }
      
    }
  }
}
