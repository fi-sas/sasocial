import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiDetailRoutingModule } from './detail-routing.module';
import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiShopBigModule } from '@fi-sas/kiosk/components/buttons/shop-big/shop-big.module';
import { FiProductMoreDetailModule } from '@fi-sas/kiosk/components/product/refectory/more-detail/more-detail.module';

import { FiFoodDetailService } from './detail.service';
import { FiFoodDetailResolver } from './detail.resolver';
import { FiFoodDetailComponent } from './detail.component';
import {
  FiUrlService,
  FiResourceService,
  FiStorageService
} from '@fi-sas/core';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { FiProductBasicDetailModule } from '@fi-sas/kiosk/components/product/refectory/basic-detail/basic-detail.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    FiDetailRoutingModule,
    FiTickerModule,
    TranslateModule,
    FiDirectivesModule,
    FiProductBasicDetailModule,
    FiProductMoreDetailModule,
    FiShopBigModule
  ],
  providers: [
    {
      provide: FiFoodDetailService,
      useClass: FiFoodDetailService,
      deps: [FiUrlService, FiStorageService, FiResourceService, FiTranslateLazyService]
    },
    {
      provide: FiFoodDetailResolver,
      useClass: FiFoodDetailResolver,
      deps: [FiFoodDetailService]
    }
  ],
  declarations: [FiFoodDetailComponent]
})
export class FiFoodDetailModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
