import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { map } from 'rxjs/operators';
import { FiFoodDetailService } from './detail.service';

@Injectable()
export class FiFoodDetailResolver implements Resolve<any> {
  constructor(private _service: FiFoodDetailService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this._service.serviceID = route.paramMap.get('service_id');
    this._service.dishID = route.paramMap.get('dish_id');
    this._service.dishTypeID = route.paramMap.get('dish_type_id');
    this._service.code = route.paramMap.get('code');

    return this._service
      .getMeals(this._service.serviceID, this._service.dishID)
      .pipe(map(products => products));
  }
}
