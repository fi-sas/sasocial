import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map, timeout, catchError, concatMap } from 'rxjs/operators';
import { get } from 'lodash';

import { FiUrlService, FiResourceService, FiStorageService } from '@fi-sas/core';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

@Injectable()
export class FiFoodDetailService {

  /* public properties */
  public serviceID = null;
  public dishID = null;
  public dishTypeID = null;
  public code = null;

  /* private properties */
  private _lang = null;

  /**
   * Constructor
   * @param {FiUrlService}           private _urlService
   * @param {FiStorageService}       private _storageService
   * @param {FiResourceService}      private _resourceService
   * @param {FiTranslateLazyService} private _translateService
   */
  constructor(
    private _urlService: FiUrlService,
    private _storageService: FiStorageService,
    private _resourceService: FiResourceService,
    private _translateService: FiTranslateLazyService
  ) {}

  /**
   * Get auth token
   */
  getAuthToken() {
    const storageDeviceToken = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    this._lang = this._translateService.getLanguage();

    return {
      headers: {
        Authorization:
          'Bearer ' + (storageDeviceToken ? storageDeviceToken.token : ''),
          'X-Language-Acronym': this._lang
      }
    };
  }

  /**
   * Get meals
   * @param {string} serviceID
   * @param {string} dishID
   * @param {string} dishTypeID
   */
  getMeals(serviceID: string, dishID: string) {
    const url = this._urlService.get('ALIMENTATION.MEALDETAIL', {
      service_id: serviceID,
      dish_id: dishID
    });

    return this._resourceService
      .read<any>(url, this.getAuthToken())
      .pipe(
        map(res => res.data),
        timeout(30000),
        catchError(err => of([] as any[]))
      );
  }

}
