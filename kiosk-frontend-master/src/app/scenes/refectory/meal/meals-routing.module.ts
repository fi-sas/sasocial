import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiFoodMealsComponent } from './meals.component';
import { FiMealsResolver } from './meals.resolver'

const routes: Routes = [
  {
    path: ':school_id/:service_id',
    pathMatch: 'full',
    component: FiFoodMealsComponent,
    resolve: {
      data: FiMealsResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiFoodMealsRoutingModule {}
