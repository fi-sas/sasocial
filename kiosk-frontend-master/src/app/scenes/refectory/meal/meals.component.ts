import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Subject, forkJoin, of } from 'rxjs';
import { flatMap, takeUntil, map, mergeMap } from 'rxjs/operators';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiServicesService } from '@fi-sas/kiosk/scenes/refectory/service/services.service';
import { FiMealsService } from '@fi-sas/kiosk/scenes/refectory/meal/meals.service';
import { KioskState } from '@fi-sas/kiosk/app.state';
import { ApplicationState, Store } from '@fi-sas/core';

/**
 * Interfaces
 */
interface TittleInterface {
  mainTitle: string;
  secondaryTitle: string;
  secondarySubtitle: string;
  secondaryLabel: string;
  type: string;
}

interface SchoolInterface {
  id: number;
  name: string;
  acronym: string;
  active: boolean;
  isDefault: boolean;
}

interface ServiceInterface {
  id: number;
  name: string;
  type: string;
}

/**
 * Component
 */
@Component({
  selector: 'fi-food-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.less']
})
export class FiFoodMealsComponent implements OnInit, OnDestroy {
  /* public properties */
  public school = null;
  public service = null;
  public meals = [];
  public isUserLogged = false;

  public titleData: TittleInterface = null;
  public menuCategories = null;

  public selectedCategory = null;
  public selectedDay = new Date().toISOString().split('T')[0];
  public selectedDishType = new Date().getHours() > 13 ? 'dinner' : 'lunch';
  public selectedDishTypeActive = new Date().getHours() > 13 ? 1 : 0;

  public loadingMeals = true;

  /* private properties */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {TranslateService}       private _translate
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiServicesService}      private _servicesService
   * @param {FiMealsService}         private _mealsService
   * @param {ActivatedRoute}         private _route
   */
  constructor(
    private _translate: TranslateService,
    private _translateService: FiTranslateLazyService,
    private _servicesService: FiServicesService,
    private _mealsService: FiMealsService,
    private _route: ActivatedRoute,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  /**
   * On init
   * @memberof FiMealsComponent
   */
  ngOnInit() {
    /* normal navigation*/
    const storeUser = this._appState.createSlice('user');
    storeUser
      .watch()
      .pipe(takeUntil(this._subject))
      .subscribe(user => (this.isUserLogged = !!user));

    this.school = this._servicesService.getSchool();
    this.service = this._servicesService.getService();

    this.setTitle();
    this.getCategories(true);
    this.getInitialMeals();

    this._translateService
      .getService()
      .onLangChange.pipe(takeUntil(this._subject))
      .subscribe(() => {
        this.setTitle();
        this.getCategories();
        this.getMeals();
      });

    /* reload */
    if (!this.school || !this.service) {
      this._route.params.pipe(takeUntil(this._subject)).subscribe(params => {
        const schoolID = Number(params['school_id']);
        const serviceID = Number(params['service_id']);

        forkJoin(
          this._servicesService.getSchools(),
          this._servicesService.getServices(schoolID)
        )
          .pipe(
            takeUntil(this._subject),
            mergeMap(res => {
              const schools: SchoolInterface[] = res[0];
              const services: ServiceInterface[] = res[1];
              const defaultSchool = schools.filter(item => item.isDefault);
              const canteens = services.filter(
                item => item.type === 'canteen' && item.id === serviceID
              );

              this.school =
                defaultSchool && defaultSchool.length > 0
                  ? defaultSchool[0]
                  : null;
              this.service =
                canteens && canteens.length > 0 ? canteens[0] : null;

              this.setTitle();

              return of(res);
            })
          )
          .subscribe();
      });
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Get meals from resolver
   */
  getInitialMeals() {
    this._route.data.pipe(takeUntil(this._subject)).subscribe(({ data }) => {
      this.meals = data.meals;
      this.loadingMeals = false;
    });
  }

  /**
   * Get meals from service
   */
  getMeals() {
    this.loadingMeals = true;

    this._mealsService
      .getMeals(this.service.id, this.selectedDay, this.selectedDishType)
      .pipe(takeUntil(this._subject))
      .subscribe(res => {
        this._mealsService
          .mealsSignatureForListDetails(
            {
              id: this.service.id,
              name: this.service ? this.service.name : '',
              school: this.school ? this.school.code : '',
              dish: this.selectedDishType,
              day: this.selectedDay
            },
            res
          )
          .pipe(takeUntil(this._subject))
          .subscribe(data => {
            this.meals = data;
          });

        this.loadingMeals = false;
      });
  }

  /**
   * Set categories
   */
  getCategories(init: boolean = false) {
    /* categories */
    if (init) {
      this._route.data.pipe(takeUntil(this._subject)).subscribe(({ data }) => {
        let categories = [];
        categories = data.categories.map((item, index) => ({
          id: item.id,
          name: item.translations[0].name,
          description: null,
          type: 'bar',
          active: false
        }));
        categories.unshift({
          id: 0,
          name: 'Menus',
          description: null,
          type: 'canteen',
          active: true
        });

        this.menuCategories = { categories: categories };
        this.selectedCategory = this.menuCategories.categories[0];
      });
    } else {
      if (this.service) {
        this._mealsService
          .getCategories(this.service.id)
          .pipe(
            takeUntil(this._subject),
            map(res => {
              let categories = [];
              categories = res.map((item, index) => ({
                id: item.id,
                name: item.translations[0].name,
                description: null,
                type: 'product',
                active: false
              }));
              categories.unshift({
                id: 0,
                name: 'Menus',
                description: null,
                type: 'canteen',
                active: true
              });

              this.menuCategories = { categories: categories };
              this.selectedCategory = this.menuCategories.categories[0];

            })
          )
          .subscribe();
      }
    }
  }

  /**
   * Set scene title
   */
  setTitle() {
    /* title */
    forkJoin(
      this._translate.get('FOOD.MEALS.TITLE'),
      this._translate.get('FOOD.MEALS.CHANGE')
    )
      .pipe(
        takeUntil(this._subject),
        flatMap(res => {
          this.titleData = {
            mainTitle: res[0],
            secondaryTitle: this.school ? this.school.code : '',
            secondarySubtitle: this.school
              ? this.service ? this.service.name : ''
              : '',
            secondaryLabel: res[1],
            type: 'REFACTORY'
          };
          return of(res);
        })
      )
      .subscribe();
  }

  /**
   * Handler category change
   * @param {number} $event
   */
  handlerCategoryChange($event: number) {
    const categoryID = $event;
    const selectedCategory = this.menuCategories.categories.filter(
      item => item.id === categoryID
    );

    if (
      selectedCategory &&
      selectedCategory.length > 0 &&
      selectedCategory[0].type === 'canteen'
    ) {
      this.selectedCategory = selectedCategory[0];
      this.getMeals();
    }
  }

  /**
   * Handler day change
   * @param {number} $event date
   */
  handlerDayChange($event: number) {
    const day = new Date($event).toISOString().split('T')[0];

    this.selectedDay = day;
    this.getMeals();
  }

  /**
   * Handler dish type change
   * @param {any} $event
   */
  handlerDishTypeChange($event: any) {
    const type = $event.index === 0 ? 'lunch' : 'dinner';

    this.selectedDishType = type;
    this.getMeals();
  }
}
