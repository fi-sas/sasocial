import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiContextTwoColModule } from '@fi-sas/kiosk/components/context/two-col/two-col.module';
import { FiMenuVerticalItemModule } from '@fi-sas/kiosk/components/menu/vertical-item/vertical-item.module';
import { FiFifteenDayModule } from '@fi-sas/kiosk/components/day/fifteen/fifteen.module';
import { FiProductListDetailModule } from '@fi-sas/kiosk/components/product/refectory/list-detail/list-detail.module';
import { FiProductListOptionsModule } from '@fi-sas/kiosk/components/product/refectory/list-options/list-options.module';

import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';

import { FiFoodMealsRoutingModule } from './meals-routing.module';
import { FiMealsResolver } from './meals.resolver';
import { FiMealsService } from './meals.service';
import { FiServicesService } from '@fi-sas/kiosk/scenes/refectory/service/services.service';
import { FiFoodMealsComponent } from './meals.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiTickerModule,
    FiContextTwoColModule,
    FiMenuVerticalItemModule,
    FiFifteenDayModule,
    FiProductListOptionsModule,
    FiProductListDetailModule,
    FiDirectivesModule,
    FiFoodMealsRoutingModule
  ],
  declarations: [
    FiFoodMealsComponent
  ],
  providers: [
    {
      provide: FiMealsResolver,
      useClass: FiMealsResolver,
      deps: [FiMealsService, FiServicesService]
    }
  ]
})
export class FiFoodMealsModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
