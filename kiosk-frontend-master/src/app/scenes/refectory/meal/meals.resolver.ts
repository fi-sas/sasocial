import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve
} from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { flatMap, mergeMap, concat, map } from 'rxjs/operators';

import { FiMealsService } from './meals.service';
import { FiServicesService } from '@fi-sas/kiosk/scenes/refectory/service/services.service';

export class FiMealsResolver implements Resolve<any> {
  constructor(
    private _mealsService: FiMealsService,
    private _servicesService: FiServicesService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const serviceID = Number(route.paramMap.get('service_id'));

    const school = this._servicesService.getSchool();
    const service = this._servicesService.getService();

    const meals = {
      categories: null,
      meals: null
    };

    return forkJoin(
      this._mealsService.getCategories(serviceID),
      this._mealsService.getMeals(
        serviceID,
        new Date().toISOString().split('T')[0],
        new Date().getHours() > 13 ? 'dinner' : 'lunch'
      )
    ).pipe(
      map(res => {
        meals.categories = res[0];
        return res;
      }),
      mergeMap(res =>
        this._mealsService.mealsSignatureForListDetails(
          {
            id: serviceID,
            name: service ? service.name : null,
            school: school ? school.acronym : null,
            dish: new Date().getHours() > 13 ? 'dinner' : 'lunch',
            day: new Date().toISOString().split('T')[0]
          },
          res[1]
        )
      ),
      mergeMap(res => {
        meals.meals = res;
        return of(meals);
      })
    );
  }
}
