import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";

import { TranslateService } from "@ngx-translate/core";
import { has } from "lodash";
import { forkJoin, of } from "rxjs";
import { flatMap, map, timeout, catchError } from "rxjs/operators";

import { FiConfigurator } from "@fi-sas/configurator";
import {
  FiResourceService,
  FiUrlService,
  FiStorageService,
} from "@fi-sas/core";
import { FiTranslateLazyService } from "@fi-sas/kiosk/services/translate.lazy.service";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class FiMealsService {
  /* private properties */
  private _lang = this._translateService.getLanguage();
  private _authData = null;

  /**
   * Creates an instance of FiMealsService.
   * @param {TranslateService}       private _translate
   * @param {FiConfigurator}         private _configurator
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiUrlService}           private _urlService
   * @param {FiResourceService}      private _resourceService
   * @param {FiStorageService}       private _storageService
   */
  constructor(
    private _translate: TranslateService,
    private _configurator: FiConfigurator,
    private _translateService: FiTranslateLazyService,
    private _urlService: FiUrlService,
    private _resourceService: FiResourceService,
    private _storageService: FiStorageService
  ) {}

  /**
   * Get categories
   * @param {number} serviceID service id
   */
  getCategories(serviceID: number) {
    const params = new HttpParams()
    .append('limit', '-1')
    .append('offset', '0');
    const url = this._urlService.get("ALIMENTATION.CATEGORIES", {
      service_id: serviceID
    });

    this._authData = this._storageService.get<{ token: string }>(
      "DEVICE_TOKEN"
    );
    this._lang = this._translateService.getLanguage();

    const languages = this._configurator.getOptionTree("LANGUAGES")[
      "LANGUAGES"
    ];
    const langFound = languages.filter((item) => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list<any>(url, {
        params,
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang,
        },
      })
      .pipe(
        map((resp) => {
          return resp.data;
        }),
        timeout(30000),
        catchError((err) => of([]))
      );
  }

  /**
   * Get meals
   * @param {number} serviceID service id
   * @param {string} day day date
   * @param {string} type dish type lunch ou dinner
   */
  getMeals(serviceID: number, day: string, type: string) {
    const url = this._urlService.get("ALIMENTATION.MEALS", {
      service_id: serviceID,
      day: day,
      type: type,
    });
    this._authData = this._storageService.get<{ token: string }>(
      "DEVICE_TOKEN"
    );
    this._lang = this._translateService.getLanguage();

    const languages = this._configurator.getOptionTree("LANGUAGES")[
      "LANGUAGES"
    ];
    const langFound = languages.filter((item) => this._lang === item.acronym);
    const langID = langFound && langFound.length > 0 ? langFound[0].id : null;

    return this._resourceService
      .list<any>(url, {
      headers: {
        Authorization: "Bearer " + this._authData.token,
        "X-Language-Acronym": this._lang,
      },
    })
      .pipe(
        map((resp) => {
          return resp.data;
        }),
        timeout(30000),
        catchError((err) => of([]))
      );
  }

  /**
   * Transform meals
   * @param {any} meals
   */
  mealsSignatureForListDetails(service: any, meals: any) {
    return forkJoin(
      this._translate.get("FOOD.MEALS.TITLE"),
      this._translate.get("FOOD.MEALS.LUNCH"),
      this._translate.get("FOOD.MEALS.DINNER")
    ).pipe(
      map((res) => {
        const serviceTypeTranslation = res[0];
        const lunchTranslation = res[1];
        const dinnerTranslation = res[2];

        const dishTypeTranslation =
          service.dish === "lunch" ? lunchTranslation : dinnerTranslation;

        const langAcronym = this._translateService.getLanguage();
        const datePipe = new DatePipe(
          langAcronym + "-" + langAcronym.toUpperCase()
        );
        const day = datePipe.transform(service.day, "dd MMM yyyy");
        return meals.map((item) => {
          const mealName = item.translations.length
            ? item.translations[0].name
            : null;
          const mealType = item.dish_type_translation.length
            ? item.dish_type_translation[0].name
            : null;

          return {
            id: item.id,
            available: item.available,
            isStockAvailable: item.isStockAvailable,
            code: item.code,
            tax_id: item.tax_id,
            account_id: item.account_id,
            service_id: item.service_id,
            type: item.dish_type_id,
            image: item.file ? item.file.url : "",
            title: item.dish_type_translation.length
              ? item.dish_type_translation[0].name
              : null,
            description: item.translations.length
              ? item.translations[0].name
              : null,
            location: item.location,
            allergens: item.allergens,
            user_allergic: item.user_allergic,
            price: item.price,
            vat:
              has(item, "tax") && has(item.tax, "tax_value")
                ? item.tax.tax_value
                : 0,
            service: service,
            day: service.day,
            mealType: service.dish,
            namePayment: mealName,
            cartTitle:
              mealName +
              " - " +
              mealType +
              " - " +
              dishTypeTranslation +
              " - " +
              day,
            cartDescription: item.location,
            stockQuantity: item.stockQuantity,
            cartQuantity: null,
            cartPrice: null,
            cartConsumeAt: item.date,
            cartMealCategory: item.meal.toUpperCase(),
            cartMealType: item.dish_type_translation.length
              ? item.dish_type_translation[0].name
              : null,
          };
        });
      })
    );
  }
}
