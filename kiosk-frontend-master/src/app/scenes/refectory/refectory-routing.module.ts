import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiRefectoryComponent } from './refectory.component';

const routes: Routes = [
  {
    path: 'meals',
    component: FiRefectoryComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/refectory/meal/meals.module#FiFoodMealsModule'
  },
  {
    path: 'services',
    component: FiRefectoryComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/refectory/service/services.module#FiServicesModule'
  },
  {
    path: 'detail',
    component: FiRefectoryComponent,
    loadChildren:
      '@fi-sas/kiosk/scenes/refectory/detail/detail.module#FiFoodDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiFoodRoutingModule {}
