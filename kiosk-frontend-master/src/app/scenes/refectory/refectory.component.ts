import { Component, OnInit, ContentChildren, ViewChildren } from '@angular/core';

@Component({
  selector: 'fi-sas-food',
  templateUrl: './refectory.component.html',
  styleUrls: ['./refectory.component.less']
})
export class FiRefectoryComponent implements OnInit {

  /**
   * Constructor
   * @memberof FiRefectoryComponent
   */
  constructor() {}

  /**
   * On init
   */
  ngOnInit() {}

}
