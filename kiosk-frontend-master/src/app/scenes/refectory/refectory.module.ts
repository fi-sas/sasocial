import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';

import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiMenuModule } from '@fi-sas/kiosk/components/menu/main/menu.module';
import { FiCartModule } from '@fi-sas/kiosk/components/cart/cart.module';

import { FiServicesService } from '@fi-sas/kiosk/scenes/refectory/service/services.service';
import { FiMealsService } from '@fi-sas/kiosk/scenes/refectory/meal/meals.service';

import { FiFoodRoutingModule } from './refectory-routing.module';
import { FiRefectoryComponent } from './refectory.component';


@NgModule({
  imports: [
    NgZorroAntdModule,
    TranslateModule,
    FiShareModule,
    FiTickerModule,
    FiMenuModule,
    FiCartModule,
    FiFoodRoutingModule
  ],
  declarations: [
    FiRefectoryComponent
  ],
  providers: [
    {
      provide: FiServicesService,
      useClass: FiServicesService
    },
    {
      provide: FiMealsService,
      useClass: FiMealsService
    }
  ]
})
export class FiFoodModule {
  constructor() {}
}
