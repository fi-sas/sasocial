import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiServicesComponent } from './services.component';
import { FiServicesResolver } from './services.resolver';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FiServicesComponent,
    resolve: {
      data: FiServicesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiServicesRoutingModule {}
