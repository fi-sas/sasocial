import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LoadingAction } from '@fi-sas/kiosk/services/loading.reducer';

import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiServicesService } from './services.service';

/**
 * Component
 */
@Component({
  selector: 'fi-sas-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.less']
})
export class FiServicesComponent implements OnInit, OnDestroy {
  /* public properties */
  public defaultSchool = null;
  public schools = null;
  public services = null;

  public contextTitle = null;
  public categories = null;

  public menuTitle = '';
  public menuCategories = null;

  /* private properties */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {TranslateService}       private _translate
   * @param {FiTranslateLazyService} private _translateService
   * @param {ActivatedRoute}         private _route
   * @param {FiServicesService}      private _service
   */
  constructor(
    private _translate: TranslateService,
    private _translateService: FiTranslateLazyService,
    private _route: ActivatedRoute,
    private _service: FiServicesService,
    private _router: Router
  ) { }

  /**
   * On init
   */
  ngOnInit() {
    this.setMenuTitle();

    this._translateService
      .getService()
      .onLangChange.pipe(takeUntil(this._subject))
      .subscribe(() => {
        this.setMenuTitle();
      });

    this._route.data.pipe(takeUntil(this._subject)).subscribe(({ data }) => {
      if (data) {
        this.defaultSchool = data.defaultSchool[0];

        this.schools = data.schools;

        this.categories = data.schools.map(item => ({
          id: item.id,
          name: item.code,
          description: item.name,
          active: false
        }));

        this.menuCategories = {
          title: this.menuTitle,
          categories: this.categories
        };
        setTimeout(()=> {
          this.getFirstCategory();
        },100)
       
      }
    });
  }



  /**
   * On destroy
   */
  ngOnDestroy() { }

  /**
   * Set menu title
   */
  setMenuTitle() {
    this._translate
      .get('FOOD.SERVICE.MENU.TITLE')
      .pipe(takeUntil(this._subject))
      .subscribe((res: string) => {
        this.menuTitle = res;
      });
  }

  getFirstCategory() {
    if (this.menuCategories.categories.length > 0) {
      this.menuCategories.categories.forEach(category => {
        if (category.active) {
          this.changeSchool(category.id);
        }
      });
    }
  }

  /**
   * Change school
   * @param {number} $e option selected
   */
  changeSchool($event) {
    const id = $event;

    LoadingAction.next(true);

    const selectedSchool = this.schools.filter(item => item.id === id);
    this.contextTitle = {
      title: selectedSchool[0].code,
      subtitle: selectedSchool[0].name
    };

    if (selectedSchool && selectedSchool.length > 0) {
      this._service.setSchool(selectedSchool[0]);

    }

    this._service
      .getServices(id)
      .pipe(takeUntil(this._subject))
      .subscribe(
        data => {
          this.services = data;
          LoadingAction.next(false);
        },
        err => {
          LoadingAction.next(false);
        }
      );
  }

  /**
   * Set service
   * @param {Object} $event selected service object
   */
  setService($event) {
    const service = $event;
    this._service.setService(service);
    if (service.type === 'canteen') {
      this._router.navigate([
        '/refectory/meals/' + this.defaultSchool.id + '/' + service.id
      ]);
    }
  }
}
