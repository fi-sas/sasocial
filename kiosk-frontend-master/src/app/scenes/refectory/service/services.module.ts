import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';

import { FiTickerModule } from '@fi-sas/kiosk/components/ticker/ticker.module';
import { FiDirectivesModule } from '@fi-sas/kiosk/directives/directives.module';
import { FiContextLargeBlockModule } from '@fi-sas/kiosk/components/context/large-block/large-block.module';
import { FiMenuVerticalItemModule } from '@fi-sas/kiosk/components/menu/vertical-item/vertical-item.module';
import { FiServiceListDetailModule } from '@fi-sas/kiosk/components/service/list-detail/list-detail.module';

import { FiServicesResolver } from './services.resolver';
import { FiServicesService } from './services.service';
import { FiServicesRoutingModule } from './services-routing.module';

import { FiServicesComponent } from './services.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    NgZorroAntdModule,
    FiShareModule,
    FiServicesRoutingModule,
    FiTickerModule,
    TranslateModule,
    FiDirectivesModule,
    FiContextLargeBlockModule,
    FiMenuVerticalItemModule,
    FiServiceListDetailModule
  ],
  declarations: [
    FiServicesComponent
  ],
  providers: [
    {
      provide: FiServicesResolver,
      useClass: FiServicesResolver,
      deps: [FiServicesService]
    }
  ],
})
export class FiServicesModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
