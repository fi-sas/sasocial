import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { flatMap, mergeMap, concat } from 'rxjs/operators';
import { FiServicesService } from './services.service';

export class FiServicesResolver implements Resolve<any> {

  constructor(
    private _service: FiServicesService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this._service.getSchools().pipe(mergeMap(data => {

      if (!data || data.length === 0) {
        return of(null);
      }

      const defaultSchool = data.filter(item => item.isDefault);

      if (defaultSchool && defaultSchool.length > 0) {
        this._service.setSchool(defaultSchool[0]);
      }

      const serviceID = ((defaultSchool && defaultSchool.length > 0) ? defaultSchool[0].id : data[0].id);
      const serviceObject = (defaultSchool && defaultSchool.length > 0) ? defaultSchool : [data[0]];

      return this._service.getServices(serviceID).pipe(flatMap(res => of({
        defaultSchool: serviceObject,
        schools: data,
        services: res
      })));

    }));

  }
}
