import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map, timeout, catchError } from 'rxjs/operators';

import { FiConfigurator } from '@fi-sas/configurator';
import { FiTranslateLazyService } from '@fi-sas/kiosk/services/translate.lazy.service';
import { FiResourceService, FiUrlService, FiStorageService } from '@fi-sas/core';

/* school interface */
interface SchoolInterface {
  id: number,
  name: string,
  acronym: string,
  isDefault: boolean,
  active: boolean
}

/* service interface */
interface ServiceInterface {
  id: number,
  active: true,
  name: string,
  type: string,
  maintenance: boolean,
  created_at: string,
  updated_at: string
}

@Injectable()
export class FiServicesService {

  private _lang = this._translateService.getLanguage();
  private _authData = null;

  private selectedSchool = null;
  private selectedService = null;

  /**
   * Constructor
   * @param {FiConfigurator}         private _configurator
   * @param {FiUrlService}           private _urlService
   * @param {FiResourceService}      private _resourceService
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiStorageService}       private _storageService
   */
  constructor(
    private _configurator: FiConfigurator,
    private _urlService: FiUrlService,
    private _resourceService: FiResourceService,
    private _translateService: FiTranslateLazyService,
    private _storageService: FiStorageService
  ) {}

  /**
   * Get schools
   */
  getSchools() {

    const url = this._urlService.get('ALIMENTATION.SCHOOLS');
    this._authData = this._storageService.get<{ token: string }>('DEVICE_TOKEN');

    const languages = this._configurator.getOptionTree('LANGUAGES')["LANGUAGES"];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = (langFound && langFound.length > 0) ? langFound[0].id : null;

    return this._resourceService
      .list<SchoolInterface>(url, {
        headers: {
          Authorization: 'Bearer ' + this._authData.token,
          'X-Language-Acronym': this._lang
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );

  }

  /**
   * Get services by school
   */
  getServices(school_id) {

    const url = this._urlService.get('ALIMENTATION.SERVICES', {school_id: school_id});
    this._authData = this._storageService.get<{ token: string }>('DEVICE_TOKEN');

    const languages = this._configurator.getOptionTree('LANGUAGES')["LANGUAGES"];
    const langFound = languages.filter(item => this._lang === item.acronym);
    const langID = (langFound && langFound.length > 0) ? langFound[0].id : null;

    return this._resourceService
      .list<ServiceInterface>(url, {
        headers: {
          Authorization: 'Bearer ' + this._authData.token,
          'X-Language-Acronym': this._lang
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );

  }

  /**
   * Set selected scholl
   * @param {any} obj selected school
   */
  setSchool(obj: SchoolInterface) {
    this.selectedSchool = obj;
  }

  /**
   * Get selected school
   */
  getSchool() {
    return this.selectedSchool;
  }

  /**
   * Set selected service
   * @param {any} obj selected service
   */
  setService(obj: ServiceInterface) {
    this.selectedService = obj;
  }

  /**
   * Get selected service
   */
  getService() {
    return this.selectedService;
  }

}
