import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiUnavailableComponent } from './unavailable.component';
import { TranslateModule } from '@ngx-translate/core';

describe('UnavailableComponent', () => {
  let component: FiUnavailableComponent;
  let fixture: ComponentFixture<FiUnavailableComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [TranslateModule.forRoot()],
        declarations: [FiUnavailableComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiUnavailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
