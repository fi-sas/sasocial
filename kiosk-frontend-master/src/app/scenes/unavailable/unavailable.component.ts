import { Component, OnInit, OnDestroy } from '@angular/core';

import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { HeaderActionDisable } from '@fi-sas/kiosk/components/header/header.reducer';

@Component({
  selector: 'fi-sas-unavailable',
  templateUrl: './unavailable.component.html',
  styleUrls: ['./unavailable.component.less']
})
export class FiUnavailableComponent implements OnInit, OnDestroy {

  /**
   * On init
   */
  ngOnInit() {
    MenuAction.next({active: false});
    HeaderActionDisable.next({active: true, ticker: false});
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    MenuAction.next({active: true});
    HeaderActionDisable.next({active: true, ticker: true});
  }
}
