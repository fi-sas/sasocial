import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { ScreenAction } from '@fi-sas/kiosk/services/screen.reducer';
import { UserAction } from '@fi-sas/kiosk/services/auth.reducer';
import { MenuAction } from '@fi-sas/kiosk/components/menu/main/menu.reducer';
import { FiTranslateLazyService } from './translate.lazy.service';
import { FiRoutingHistoryService } from './routing.service';
import { FiAuthService } from './auth.service';
import { FiKeyboardService } from '../components/keyboard/keyboard.service';
import { KeyboardType } from '../components/keyboard/keyboard.component';



@Injectable()
export class FiConditionService {
  private _timeout = 60000;
  private _interval: NodeJS.Timer;
  private _function = (event: MouseEvent) => {
    this.reset();
    this.setInactive();
  };

  constructor(
    private _router: Router,
    private _configurator: FiConfigurator,
    private _translateService: FiTranslateLazyService,
    private _fiRoutingHistoryService: FiRoutingHistoryService,
    private _fiAuthenticationService: FiAuthService,
    @Inject(DOCUMENT) private _doc: Document,
    private _fiKeyboardService: FiKeyboardService,
  ) {
    this.enable();
  }

  public disable() {
    this.removeListener();
    this.reset();
  }

  public enable() {
    this.setListener();
    this.reset();
    this.setInactive();
  }

  private removeListener() {
    this._doc.removeEventListener('click', this._function);
  }

  private setListener() {
    this._timeout = this._configurator.getOption('SCREEN_SAVER.TIME_OUT');

    if (this._timeout < 59000) {
      this._timeout = 60000;
    }
    this._doc.addEventListener('click', this._function);

    //this._doc.removeEventListener('click', _function);
  }

  private setInactive() {
    this._timeout = this._configurator.getOption('SCREEN_SAVER.TIME_OUT');
    const langAcronym: string = this._configurator.getOption(
      'DEFAULT_LANG',
      'pt'
    );
    this._interval = setInterval(() => {
      ScreenAction.next(true);
      UserAction.next(null);
      this._fiAuthenticationService.logout();
      this._translateService.setLanguage(langAcronym);
      this._fiRoutingHistoryService.resetHistory();
      this._fiKeyboardService.status.next({ visible: false, input: null, keyboardType: KeyboardType.All });
      MenuAction.next({ authentication: false, logged: false });

      this.gotoScreenSaver();
    }, this._timeout);
  }

  private reset() {
    clearInterval(this._interval);
  }

  gotoScreenSaver(): void {
    this._router.navigate(['/']);
  }

  goto(route: string): void {
    let url = route;
    let params = null;

    const segments = this._router
      .parseUrl(route)
      .toString()
      .split('?');
    if (segments && segments.length > 0) {
      url = segments[0];
      params = this._router.parseUrl(route);
    }
    this._router.navigate([url], params);
  }
}
