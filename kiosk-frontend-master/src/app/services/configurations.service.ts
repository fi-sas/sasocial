import { HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
  FiResourceService,
  FiStorageService,
  FiUrlService,
} from "@fi-sas/core";
import { Observable, of } from "rxjs";
import { catchError, map, timeout } from "rxjs/operators";
import { FiTranslateLazyService } from "./translate.lazy.service";

@Injectable({
  providedIn: "root",
})
export class ConfigurationsService {
  constructor(
    private resourceService: FiResourceService,
    private storageService: FiStorageService,
    private urlService: FiUrlService,
    private _translateService: FiTranslateLazyService
  ) {}

  geral() {
    const url = this.urlService.get("CONFIG.GET_CONFIG");
    let params = new HttpParams();
    params = params.set("offset", "0");
    params = params.set("limit", "-1");

    return this.resourceService
      .list<any>(url, {
      params: params,
      ...this.storageToken(),
    })
      .pipe(
        map((response) => response.data),
        timeout(30000),
        catchError(() => of([]))
      );
  }

  geralServicesKiosk() {
    const url = this.urlService.get("CONFIG.SERVICES_KIOSK");
    let params = new HttpParams();
    params = params.set("offset", "0");
    params = params.set("limit", "-1");

    return this.resourceService
      .list<any>(url, {
      params: params,
      ...this.storageToken(),
    })
      .pipe(
        map((response) => response.data),
        timeout(30000),
        catchError(() => of([]))
      );
  }

  setCPUID(CPUID: string): Observable<any> {
    const url = this.urlService.get("CONFIG.SET_CPUID", {});

    return this.resourceService
      .update<any>(url, {
      CPUID
    }, {
      ...this.storageToken(),
    })
      .pipe(
        map((response) => response.data),
        timeout(30000),
        catchError(() => of([]))
      );
  }

  getLocalStorageService(id: number) {
    const data = JSON.parse(
      localStorage.getItem("configuraction_active_menu_kiosk")
    );
    if (
      data.find((service) => service.id == id) == null ||
      data.find((service) => service.id == id) == undefined
    ) {
      return [];
    }
    return data.find((service) => service.id == id);
  }

  getLocalStorageTotalConfiguration() {
    const data = JSON.parse(
      localStorage.getItem("configuraction_active_menu_kiosk")
    );
    return data;
  }

  /**
   * Storage token
   */
  private storageToken() {
    const _lang = this._translateService.getLanguage();
    const storageToken =
      this.storageService.get<{ token: string }>("DEVICE_TOKEN");

    return {
      headers: {
        Authorization: "Bearer " + storageToken.token,
        "X-Language-Acronym": _lang,
      },
    };
  }
}
