import { Injectable, Inject } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  constructor(@Inject(NzModalService) private modalService: NzModalService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status > 499) {
          this.warningModal();
        }

        return throwError(error);
      })
    );
  }

  warningModal() {
    this.modalService.error({
      nzContent: 'Error on service connection was detected. Please inform the administrator.',
      nzWrapClassName: 'vertical-center-modal'
    });

    //ref.afterClose.pipe(first()).subscribe(() => ref.destroy());
  }
}
