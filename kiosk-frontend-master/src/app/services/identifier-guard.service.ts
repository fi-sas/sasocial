import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { of, Observable } from 'rxjs';
import { FiAuthService } from './auth.service';
import { flatMap, retry, concatMap, catchError, mergeMap } from 'rxjs/operators';

export class FiIdentifierGuardService implements CanActivate {
  constructor(private _router: Router, private _authService: FiAuthService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    if (!this.isOnline()) {
      this.navigateToUnavailable();

      return of(false);
    }

    if (this._authService.hasStorageToken()) {
      return this._authService
        .validateToken()
        .pipe(
          concatMap(rs => (rs ? of(true) : this.generateRefreshToken())),
          catchError(error => {
            return of(false)
          })
        );
    }

    return this.generateToken();
  }

  generateRefreshToken() {
    return this._authService.requestRefreshToken().pipe(
      mergeMap(authorized => {
        if (!authorized) {
          this._router.navigate(['unavailable']);
        }

        return of(authorized);
      })
    );
  }

  generateToken() {
    return this._authService.setDeviceToken().pipe(
      mergeMap(authorized => {
        if (!authorized) {
          this._router.navigate(['unavailable']);
        }

        return of(authorized);
      })
    );
  }

  navigateToUnavailable() {
    this._router.navigate(['unavailable']);
  }

  isOnline() {
    return window.navigator.onLine;
  }
}
