import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FiMenuPaymentService {
  public menu = true;
  public payCartButton = true;

  private observable = new BehaviorSubject({
    menu: this.menu,
    payCartButton: this.payCartButton
  });
  public changes = this.observable.asObservable();

  /**
   * Set Menu
   */
  public setMenu(value: boolean) {
    this.menu = value;
    this.observable.next({ menu: value, payCartButton: this.payCartButton });
  }

  /**
   * Set Pay Cart
   */
  public setPayCart(value: boolean) {
    this.payCartButton = value;
    this.observable.next({ menu: this.menu, payCartButton: value });
  }
}
