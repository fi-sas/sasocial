import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { ScreenAction } from './screen.reducer';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ConfigurationsService } from './configurations.service';
import { filter, first } from 'rxjs/operators';
import { FiConfigurator } from '@fi-sas/configurator';

@Injectable({
  providedIn: 'root'
})
export class FiMiddlewareListenerService {

  private isSocketOn = false;
  private _cpuid: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    private _router: Router,
    private _socket: Socket,) {

    const timeout = setTimeout(() => {
      if (!this.isSocketOn && this._router.url !== '/install')
        this.navigateToUnavailable();

      clearTimeout(timeout);
    }, 5000);

    _socket.on('CPUID_READ', (cpuid) => this._cpuid.next(cpuid));

    _socket.on('connect', () => {
      this.isSocketOn = true;

      if (this._router.url !== '/install') {
        this.gotoScreenSaver();
      }
    });
    _socket.on('disconnect', () => {
      this.isSocketOn = false;
      if (this._router.url !== '/install') {
        this.navigateToUnavailable();
      }
    });


  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    if (this.isSocketOn || state.url === '/install') {
      return of(true);
    } else {
      this.navigateToUnavailable();
      return of(false);
    }
  }

  gotoScreenSaver(): void {
    ScreenAction.next(true);
    this._router.navigate(['/']);
  }

  navigateToUnavailable() {
    this._router.navigate(['unavailable']);
  }

  public isMiddlewarePresent(): boolean {
    return this.isSocketOn;
  }

  requestCPUID(): void {
    this._socket.emit("GET_CPUID");
  }

  getCPUIDObservable(): Observable<string> {
    return this._cpuid.asObservable().pipe(
      filter(prd => prd !== null)
    );
  }

}
