import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap, filter, distinctUntilChanged, timeout } from 'rxjs/operators';

@Injectable()
export class FiRfidService {

  _rfid: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private socket: Socket) {
    socket.on("RFID_READ", (value) => this._rfid.next(value));

    /*setTimeout(() => {
      this._rfid.next('DA1D7B82');
    }, 1500);*/

  }

  getRfidObservable(): Observable<string> {
    return this._rfid.asObservable().pipe(
      filter(prd => prd !== null)
    );
  }

}
