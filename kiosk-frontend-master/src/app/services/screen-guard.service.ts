import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { FiAuthService } from './auth.service';
import { Inject, Injectable } from '@angular/core';
import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/kiosk/app.state';

@Injectable()
export class FiScreenGuardService implements CanActivate {
  constructor(
    private _router: Router,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const store = this._appState.createSlice('ui').createSlice('screensaver');

    return store.watch(screen => {
      if (!screen) {
        this.navigateToDashboard();
      }

      return screen;
    });
  }

  navigateToDashboard() {
    this._router.navigate(['/dashboard']);
  }
}
