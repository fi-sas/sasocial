import { Injectable } from "@angular/core";
import { Socket } from "ngx-socket-io";
import { Subject, Observable, of, zip, from, interval } from "rxjs";
import { first, filter, delay, map } from "rxjs/operators";
import { FiPosPaymentService } from "../scenes/pospayment/pospayment.service";

export enum FiTpaStatusEnum {
  WAITING = "WAITING",
  WAIT_CARD_IN = "WAIT_CARD_IN",
  CARD_INSERTED = "CARD_INSERTED",
  WAIT_CARD_OUT = "WAIT_CARD_OUT",
  CARD_OUT = "CARD_OUT",
  WAIT_CLIENT_CONFIRMATION = "WAIT_CLIENT_CONFIRMATION",
  WAIT_PIN = "WAIT_PIN",
  IN_COMMUNICATION = "IN_COMMUNICATION",
  DELIVERY_OK = "DELIVERY_OK",
  PRINTING = "PRINTING",
  TRANSACTION_FINISHED = "TRANSACTION_FINISHED",
}

@Injectable({
  providedIn: "root",
})
export class FiTpaService {
  _status: Subject<FiTpaStatusEnum> = new Subject<FiTpaStatusEnum>();
  _transaction: Subject<any> = new Subject<FiTpaStatusEnum>();
  _available: Subject<any> = new Subject<boolean>();

  constructor(
    private socket: Socket,
    private pospaymentService: FiPosPaymentService
  ) {
    socket.on("TPA_READ", (value) => {
      this._status.next(value);
    });

    socket.on("TPA_PERIOD_EVENT", (value) => {
      this.pospaymentService
        .periodEvent(value.receipt)
        .pipe(first())
        .subscribe((res) => {});
    });

    socket.on("TPA_TRANSACTION_FINISHED", (value) => {
      if (value.status === "CANCELLED") {
        this.pospaymentService
          .cancelPayment(value.paymentID, value.CPUID)
          .pipe(first())
          .subscribe(
            () => {
              this._transaction.next(value);
            },
            (err) => {
              this._transaction.next(false);
              this.notCancelPayment(value.paymentID);
            }
          );
      }

      if (value.status === "PAYED") {
        this.pospaymentService
          .confirmPayment(
            value.paymentID,
            value.clientReceipt,
            value.merchantReceipt,
            value.CPUID
          )
          .pipe(first())
          .subscribe(
            () => {
              this.confirmPayment(value.paymentID);
              this._transaction.next(value);
            },
            (err) => {
              this.notConfirmPayment(value.paymentID);
              this._transaction.next(false);
            }
          );
      }

      if (value.status === "ERROR") {
        this.pospaymentService
          .cancelPayment(value.paymentID, value.CPUID)
          .pipe(first())
          .subscribe(
            () => {
              this._transaction.next(value);
            },
            (err) => {
              this._transaction.next(false);
              this.notCancelPayment(value.paymentID);
            }
          );
      }
    });

    socket.on("TPA_AVAILABLE_READ", (available) =>
      this._available.next(available)
    );
  }

  public requestAvailable(): void {
    this.socket.emit("GET_TPA_AVAILABLE");
  }

  public confirmPayment(payment_id: string) {
    this.socket.emit("CONFIRM_TPA_PAYMENT", {
      paymentID: payment_id,
    });
  }

  public notConfirmPayment(payment_id: string) {
    this.socket.emit("NOT_CONFIRM_TPA_PAYMENT", {
      paymentID: payment_id,
    });
  }

  public notCancelPayment(payment_id: string) {
    this.socket.emit("NOT_CANCEL_TPA_PAYMENT", {
      paymentID: payment_id,
    });
  }

  public createPayment(payment_id: string, transaction_value: number): void {
    this.socket.emit("CREATE_TPA_PAYMENT", {
      payment_id,
      transaction_value,
    });

    /*const subs = zip(
      from([
        FiTpaStatusEnum.WAITING,
        FiTpaStatusEnum.WAIT_CARD_IN,
        FiTpaStatusEnum.WAIT_CARD_OUT,
        FiTpaStatusEnum.CARD_OUT,
        FiTpaStatusEnum.WAIT_CLIENT_CONFIRMATION,
        FiTpaStatusEnum.WAIT_PIN,
        FiTpaStatusEnum.IN_COMMUNICATION,
        FiTpaStatusEnum.DELIVERY_OK,
        FiTpaStatusEnum.PRINTING,
        FiTpaStatusEnum.TRANSACTION_FINISHED,
      ]),
      interval(2000)
    ).subscribe(s => {
      this._status.next(s[0])

      if(s[0] === FiTpaStatusEnum.TRANSACTION_FINISHED)
        subs.unsubscribe()
    });*/
  }

  getTpaStatusObservable(): Observable<FiTpaStatusEnum> {
    return this._status.asObservable().pipe(filter((prd) => prd !== null));
  }

  getTpaTransactionObservable(): Observable<any> {
    return this._transaction.asObservable().pipe(filter((prd) => prd !== null));
  }

  getTpaAvailableObservable(): Observable<any> {
    return this._available.asObservable().pipe(filter((prd) => prd !== null));
  }
}
