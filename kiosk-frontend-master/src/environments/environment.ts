export const apiAddress = 'https://sasocialdev.sas.ipvc.pt/';

export const environment = {
  production: false,
  environment: 'LOCAL',
  api_gateway: 'https://sasocialdev.sas.ipvc.pt'

};
