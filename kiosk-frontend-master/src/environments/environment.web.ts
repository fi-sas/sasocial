// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `index.ts`, but if you do
// `ng build --env=prod` then `index.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const apiAddress = 'http://62.28.241.42/';

export const environment = {
  production: false,
  environment: 'DEV',
  api_gateway: 'https://sasocialdev.sas.ipvc.pt'
};
