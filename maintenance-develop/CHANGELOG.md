# [1.0.0-rc.2](https://gitlab.com/fi-sas/maintenance/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-01-05)


### Bug Fixes

* remove seed ([7f6411d](https://gitlab.com/fi-sas/maintenance/commit/7f6411d75e84ec2985d4c5e910f5971297ff4ce9))

# 1.0.0-rc.1 (2021-01-05)


### Bug Fixes

* **docker-compose:** create docker-compose to prod and dev ([b4b6ad4](https://gitlab.com/fi-sas/maintenance/commit/b4b6ad445be67e9c9cf9001a192c106a6e65dcff))


### Features

* add all endpoints ([c34d906](https://gitlab.com/fi-sas/maintenance/commit/c34d906807c0aff221bf5f770d56c0c56256f2fe))
* new base boilerplate ([200fb59](https://gitlab.com/fi-sas/maintenance/commit/200fb593094946e5bb8cf9f977925bb3b081609b))
