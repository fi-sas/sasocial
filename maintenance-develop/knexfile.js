require("dotenv").config();

let knexBaseConfig = {
  client: "postgresql",
  connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE,
  },
  migrations: {
    tableName: "migrations",
  },
  debug: process.env.DATABASE_DEBUG === "true",
};
let knexConfig = {
  development: {
    ...knexBaseConfig,
    seeds: {
      directory: [__dirname + "/seeds", __dirname + "/seeds/sample_seeds"],
    },
  },
  production: {
    ...knexBaseConfig,
    seeds: {
      directory: [__dirname + "/seeds"],
    },
  },
};

console.log(`KnexConfig / using environment: ${process.env.NODE_ENV}`);

module.exports = knexConfig[process.env.NODE_ENV || "development"];
