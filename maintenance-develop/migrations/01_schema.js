module.exports.up = async (db) =>
    db.schema
        .createTable("maintenance_request", (table) => {
            table.increments();
            table.integer("target_id").unsigned().notNullable();
            table.enum("target", ["Assets", "Components"]).notNullable();
            table.integer("priority").unsigned().notNullable().defaultTo(0);
            table.integer("category_failure_id").unsigned().notNullable();
            table.text("description");
            table.integer("file_id").unsigned();
            table.integer("maintainer_id").unsigned();
            table.integer("user_id").unsigned().notNullable();
            table.string("user_email", 200);
            table.string("user_phone", 25);
            table.enum(
                "status",
                ["Open", "Reopen", "Reject", "Execute", "Analysis", "Cancel", "Finish"],
            ).notNullable().defaultTo("Open");
            table.text("cancel_justification");
            table.text("review");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("maintenance_technical_report", (table) => {
            table.increments();
            table.integer("previous_report_id").unsigned();
            table.integer("target_id").unsigned().notNullable();
            table.enum("target", ["Request", "Plan"]).notNullable();
            table.integer("user_id").unsigned().notNullable();
            table.datetime("intervention_date").notNullable();
            table.datetime("intervention_start").unsigned(); // Hora início
            table.datetime("intervention_end").unsigned(); // Hora fim
            table.datetime("intervention_spent").unsigned(); // Tempo gasto
            table.text("parameters");
            table.enum("status", ["Success", "Reject", "Unresolved", "Progress"]).notNullable();
            table.text("description");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("maintenance_plan", (table) => {
            table.increments();
            table.integer("target_id").unsigned().notNullable();
            table.enum("target", ["Assets", "Components"]).notNullable();
            table.integer("area_id").unsigned().notNullable();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("maintenance_plan_task", (table) => {
            table.increments();
            table.string("name", 100).notNullable();
            table.string("code", 255);
            table.integer("maintenance_plan_id").unsigned().notNullable();
            table.integer("area_task_id").unsigned().notNullable();
            table.integer("maintainer_id").unsigned().notNullable();
            table.enum("recurrence", ["None", "Daily", "Weekly", "Monthly", "Annually"]).defaultTo("None");
            table.string("repeat_month", 2);
            table.string("repeat_day", 2);
            table.string("repeat_weekday", 2);
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("maintenance_request_task", (table) => {
            table.increments();
            table.string("name", 100).notNullable();
            table.string("code", 255);
            table.integer("maintenance_request_id").unsigned().notNullable();
            table.integer("maintainer_id").unsigned().notNullable();
            table.boolean("notification").defaultTo(false);
            table.datetime("notification_date");
            table.datetime("intervention_date");
            table.datetime("intervention_start");
            table.datetime("intervention_end");
            table.integer("intervention_spent").unsigned();
            table.text("parameters");
            table.enum("status", ["Success", "Reject", "Unresolved", "Progress"]).notNullable();
            table.text("observations");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("task_attachments", (table) => {
            table.increments();
            table.integer("task_id")
                .unsigned()
                .references("id")
                .inTable("maintenance_plan_task")
                .notNullable();
            table.integer("file_id").unsigned();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
            table.unique(["task_id", "file_id"]);
        })
        .createTable("task_records", (table) => {
            table.increments();
            table.integer("task_id")
                .unsigned()
                .references("id")
                .inTable("maintenance_plan_task")
                .notNullable();
            table.integer("maintainer_id").unsigned().notNullable();
            table.boolean("notification").defaultTo(false);
            table.datetime("notification_date");
            table.boolean("notification_sent").defaultTo(false);
            table.datetime("intervention_date");
            table.datetime("intervention_start");
            table.datetime("intervention_end");
            table.integer("intervention_spent").unsigned();
            table.text("parameters");
            table.enum("status", ["Success", "Reject", "Unresolved", "Progress"]).notNullable();
            table.text("observations");
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
        })
        .createTable("request_task_attachment", (table) => {
            table.increments();
            table.integer("task_id")
                .unsigned()
                .references("id")
                .inTable("maintenance_request_task")
                .notNullable();
            table.integer("file_id").unsigned();
            table.datetime("updated_at").notNullable();
            table.datetime("created_at").notNullable();
            table.unique(["task_id", "file_id"]);
        })
        .createTable("maintenance_notification", (table) => {
            table.increments();
            table.integer("maintenance_id").notNullable().unsigned();
            table.enum("maintenance_type", ["Plan", "Request"]).notNullable();
            table.integer("user_id").unsigned();
            table.string("alert_type_key", 255).notNullable();
            table.datetime("date").notNullable();
            table.text("data");
            table.boolean("success").notNullable().defaultTo(false);
            table.text("response");
        });

module.exports.down = async (db) =>
    db.schema
        .dropTable("maintenance_request")
        .dropTable("maintenance_technical_report")
        .dropTable("maintenance_plan")
        .dropTable("maintenance_plan_task")
        .dropTable("maintenance_request_task")
        .dropTable("task_attachments")
        .dropTable("task_records")
        .dropTable("request_task_attachment")
        .dropTable("maintenance_notification");
