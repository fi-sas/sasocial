FROM node:12-buster-slim

ARG NODE_ENV

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm i

COPY . .

CMD npm run start:$NODE_ENV
