"use strict";

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const Cron = require("moleculer-cron");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
  name: "maintenance.maintenances_notifications",
  table: "maintenance_notification",

  adapter: new KnexAdpater(require("../knexfile")),
  mixins: [DbMixin("maintenance", "maintenances_notifications"), Cron],

  /**
   * Cron job
   */
  crons: [
    {
      name: "sendNotifications",
      cronTime: "*/15 * * * *",
      onTick: function () {
        this.getLocalService(
          "maintenance.maintenances_notifications"
        ).actions.sendNotification();
      },
    },
  ],

  /**
   * Settings
   */
  settings: {
    fields: [
      "id",
      "maintenance_id",
      "maintenance_type",
      "user_id",
      "alert_type_key",
      "date",
      "data",
      "success",
      "response",
    ],
    defaultWithRelateds: [],
    withRelateds: {},
    entityValidator: {
      maintenance_id: { type: "number" },
      maintenance_type: { type: "string" },
      user_id: { type: "number", nullable: true },
      alert_type_key: { type: "string" },
      date: { type: "date" },
      data: { type: "string", nullable: true },
      success: { type: "boolean", default: false },
      response: { type: "string", nullable: true },
    },
  },
  hooks: {
    before: {
      create: [
        function sanatizeParams(ctx) {
          ctx.params.created_at = new Date();
          ctx.params.updated_at = new Date();
        },
      ],
      update: [
        function sanatizeParams(ctx) {
          ctx.params.updated_at = new Date();
        },
      ],
    },
  },
  /**
   * Dependencies
   */
  dependencies: [],

  /**
   * Actions
   */
  actions: {
    sendNotification: {
      async handler(ctx) {
        const list_for_notification = await this.adapter
          .db("task_records")
          .where("notification", true)
          .andWhere("notification_sent", false)
          .andWhere("notification_date", "<=", new Date());
        if (list_for_notification.length) {
          for (const elem of list_for_notification) {
            let success;
            let response;
            try {
              const user = await ctx.call("authorization.users.get", {
                id: elem.maintainer_id,
              });
              const notification = await ctx.call(
                "notifications.alerts.create_alert",
                {
                  alert_type_key: "MAINTENANCE_PLAN_NOTIFICATION",
                  user_id: user[0].id,
                  user_data: {
                    email: user[0].email ? user[0].email : null,
                    phone: user[0].phone ? user[0].phone : null,
                  },
                  data: {
                    elem,
                  },
                  variables: {},
                  external_uuid: null,
                  medias: [],
                }
              );
              success = true;
              response = JSON.stringify(notification);
              elem.notification_sent = true;
              await ctx.call("maintenance.tasks_records.update", elem);
            } catch (error) {
              success = false;
              response = JSON.stringify(error);
            }
            const notification_save = {
              maintenance_id: elem.maintainer_id,
              maintenance_type: "Plan",
              user_id: elem.maintainer_id,
              alert_type_key: "MAINTENANCE_PLAN_NOTIFICATION",
              date: new Date(),
              data: JSON.stringify(elem),
              success,
              response,
            };
            return await this._create(ctx, notification_save);
          }
        }
      },
    },
  },

  /**
   * Events
   */
  events: {},

  /**
   * Methods
   */
  methods: {},

  /**
   * Service created lifecycle event handler
   */
  created() {},

  /**
   * Service started lifecycle event handler
   */
  async started() {},

  /**
   * Service stopped lifecycle event handler
   */
  async stopped() {},
};
