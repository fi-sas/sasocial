"use strict";

const { Errors } = require("@fisas/ms_core").Helpers;

const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.maintenances_plans",
	table: "maintenance_plan",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "maintenances_plans")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"target_id",
			"target",
			"area_id",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["tasks"],
		withRelateds: {
			tasks(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "maintenance.maintenances_plans_tasks", "tasks", "id", "maintenance_plan_id");
			}
		},
		entityValidator: {
			target_id: { type: "number" },
			target: { type: "string" },
			area_id: { type: "number" }
		},
	},
	hooks: {
		before: {
			create: [
				"validations",
				async function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
		after: {
			create : [ "getAndSaveAreaMaintenanceTasks" ]
		}
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},

		/**
		 * Action add MaintenancesPlansTasks on MaintenancePlan
		 */
		addTasks: {
			rest : "POST /:id/tasks",
			params: {
				name: { type: "string" },
				code: { type: "string" },
				maintainer_id: { type: "number" },
				recurrence: { type: "string" }
			},
			visibility: "published",
			async handler(ctx) {
				const maintenancePlan = await this._get(ctx, { id : ctx.params.id });
				const entity = {
					name: ctx.params.name,
					code: ctx.params.code,
					maintenance_plan_id: parseInt(ctx.params.id),
					area_task_id: maintenancePlan[0].area_id,
					maintainer_id: ctx.params.maintainer_id,
					recurrence: ctx.params.recurrence,
					repeat_month: null,
					repeat_day: null,
					repeat_weekday: null,
				};
				await ctx.call("maintenance.maintenances_plans_tasks.create", entity);
				return await this._get(ctx, { id : ctx.params.id });
			}
		},

		/**
		 * Action for pacth MaintenancePlansTasks
		 */
		patchTask:{
			rest: "PATCH /:id/tasks/:task_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				ctx.params.id = ctx.params.task_id;
				await ctx.call("maintenance.maintenances_plans_tasks.patch", ctx.params );
				return await ctx.call("maintenance.maintenances_plans_tasks.get", { id : ctx.params.task_id });
			}
		},

		/**
		 * Action for update MaintenancePlansTasks
		 */
		updateTask :{
			rest:"PUT /:id/tasks/:task_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				ctx.params.id = ctx.params.task_id;
				await ctx.call("maintenance.maintenances_plans_tasks.update", ctx.params );
				return await ctx.call("maintenance.maintenances_plans_tasks.get", { id : ctx.params.task_id });
			}
		},

		/**
		 * Action for remove MaintenancePlansTasks
		 */
		deleteTask: {
			rest:"DELETE /:id/tasks/:task_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				return await ctx.call("maintenance.maintenances_plans_tasks.remove", { id : ctx.params.task_id } );
			}
		},

		/**
		 * Action for add attachment to the task
		 */
		addAttachmentToTask:{
			rest: "POST /:id/tasks/:task_id/attachments",
			params:{
				file_id : { type: "number" }
			},
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				await ctx.call("media.files.get", { id : ctx.params.file_id });
				this.verifyPlansAndTask(ctx);
				const entity = {
					task_id: parseInt(ctx.params.task_id),
					file_id: parseInt(ctx.params.file_id)
				};
				return await ctx.call("maintenance.tasks_attachments.create", entity);
			}
		},

		/**
		 * Action for remove attachment for task
		 */
		removeAttachmentToTask: {
			rest: "DELETE /:id/tasks/:task_id/attachments/:attach_id",
			visibility: "published",
			async handler(ctx){
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				await ctx.call("maintenance.tasks_attachments.get", { id : ctx.params.attach_id });
				return await ctx.call("maintenance.tasks_attachments.remove", { id : ctx.params.attach_id });
			}
		},

		/**
		 * Action for add Record to the task
		 */
		addRecordToTheTask:{
			rest: "POST /:id/tasks/:task_id/records",
			params: {
				maintainer_id: { type: "number" },
				notification: { type: "boolean" },
				notification_date: { type : "date", convert: true },
				intervention_date: { type: "date", convert: true },
				intervention_start: { type: "date", convert: true },
				intervention_end: { type: "date", convert: true },
				intervention_spent: { type: "number" },
				parameters: { type: "string" },
				status: { type: "string" },
				observations: { type : "string" }
			},
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				const entity = {
					maintainer_id: ctx.params.maintainer_id,
					notification: ctx.params.notification,
					notification_date: ctx.params.notification_date,
					intervention_date: ctx.params.intervention_date,
					intervention_end: ctx.params.intervention_end,
					intervention_start: ctx.params.intervention_start,
					intervention_spent: ctx.params.intervention_spent,
					parameters: ctx.params.parameters,
					observations: ctx.params.observations,
					task_id: parseInt(ctx.params.task_id),
					status: ctx.params.status,
				};
				return await ctx.call("maintenance.tasks_records.create", entity );
			}
		},

		/**
		 * Action for update Record to the task
		 */
		pacthRecordToTask: {
			rest: "PATCH /:id/tasks/:task_id/records/:record_id",
			params: {
				maintainer_id: { type: "number" },
				notification: { type: "boolean" },
				notification_date: { type : "date", convert: true },
				intervention_date: { type: "date", convert: true },
				intervention_start: { type: "date", convert: true },
				intervention_end: { type: "date", convert: true },
				intervention_spent: { type: "number" },
				parameters: { type: "string" },
				status: { type: "string" },
				observations: { type : "string" }
			},
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				await ctx.call("maintenance.tasks_records.get", { id : ctx.params.record_id });
				const entity = {
					id: ctx.params.record_id,
					maintainer_id: ctx.params.maintainer_id,
					notification: ctx.params.notification,
					notification_date: ctx.params.notification_date,
					intervention_date: ctx.params.intervention_date,
					intervention_end: ctx.params.intervention_end,
					intervention_start: ctx.params.intervention_start,
					intervention_spent: ctx.params.intervention_spent,
					parameters: ctx.params.parameters,
					observations: ctx.params.observations,
					task_id: parseInt(ctx.params.task_id),
					status: ctx.params.status,
				};
				return await ctx.call("maintenance.tasks_records.update", entity);
			}
		},

		/**
		 * Action for delete record to the task
		 */
		deleteRecordToTask: {
			rest: "DELETE /:id/tasks/:task_id/records/:record_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				this.verifyPlansAndTask(ctx);
				await ctx.call("maintenance.tasks_records.get", { id : ctx.params.record_id });
				return await ctx.call("maintenance.tasks_records.remove", { id : ctx.params.record_id });
			}
		}
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {
		async validations(ctx) {
			let target;
			if (ctx.params.target.toLowerCase() === "assets") {
				target = await ctx.call("infrastructure.assets.get", { id: ctx.params.target_id });
			}
			if (ctx.params.target.toLowerCase() === "components") {
				target = await ctx.call("infrastructure.components.get", { id: ctx.params.target_id });
			}
			if (target[0].area_id !== undefined && target[0].area_id !== ctx.params.area_id) {
				throw new Errors.ValidationError("Target area_id is different from the the input area_id",
					"TARGET_AREAID_DIFFERENT_THE_INPUT_AREAID",
					{});
			}
		},
		async getAndSaveAreaMaintenanceTasks(ctx, resp){
			let listMaintenancesPlansTasksIDS = [];
			let listMaintenancesPlansTasks = [];
			let target;
			if (ctx.params.target.toLowerCase() === "assets") {
				target = await ctx.call("infrastructure.assets.get", { id: ctx.params.target_id });
			}
			if (ctx.params.target.toLowerCase() === "components") {
				target = await ctx.call("infrastructure.components.get", { id: ctx.params.target_id });
			}
			const tasksRequest =  await ctx.call("infrastructure.area-maintenance-tasks.find", {
				query: {
					area_id: ctx.params.area_id
				}
			});
			if (tasksRequest.length === 0) {
				throw new Errors.ValidationError("The area_id provided is not valid.",
					"THE_AREAID_IS_NOT_VALID",
					{});
			}
			for(const task of tasksRequest) {
				const entity = {
					name: task.name,
					code: task.code,
					maintenance_plan_id: resp[0].id,
					area_task_id: task.id,
					maintainer_id: target[0].maintainer_id,
					recurrence: "None",
					repeat_month: null,
					repeat_day: null,
					repeat_weekday: null,
				};
				const plans_tasks = await ctx.call("maintenance.maintenances_plans_tasks.create", entity);
				listMaintenancesPlansTasksIDS.push(plans_tasks[0].id);
			}
			for ( const plans of listMaintenancesPlansTasksIDS){
				const MaintenancePlanTask = await ctx.call("maintenance.maintenances_plans_tasks.get", { id : plans });
				listMaintenancesPlansTasks.push(MaintenancePlanTask[0]);

			}
			resp[0].tasks = listMaintenancesPlansTasks;
			return resp;
		},

		async verifyPlansAndTask(ctx) {
			const maintenancePlanTask = await ctx.call("maintenance.maintenances_plans_tasks.get", { id : ctx.params.task_id });
			if (maintenancePlanTask[0].maintenance_plan_id !== parseInt(ctx.params.id)) {
				throw new Errors.ValidationError("Plans task not correspond the maintenance plan",
					"PLANS_TASKS_NOT_CORRESPOND_THE_MAINTENANCE_PLAN",
					{});
			}
		}
	},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
