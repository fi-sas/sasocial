"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.maintenances_plans_tasks",
	table: "maintenance_plan_task",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "maintenances_plans_tasks")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"name",
			"code",
			"maintenance_plan_id",
			"area_task_id",
			"maintainer_id",
			"recurrence",
			"repeat_month",
			"repeat_day",
			"repeat_weekday",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: ["attachments", "records"],
		withRelateds: {
			attachments(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "maintenance.tasks_attachments", "attachments", "id", "task_id");
			},
			records(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "maintenance.tasks_records", "records", "id", "task_id");
			},
		},
		entityValidator: {
			name: { type: "string" },
			code: { type: "string", nullable: true },
			maintenance_plan_id : { type: "number" },
			area_task_id : { type: "number" },
			maintainer_id : { type : "number" },
			recurrence : { type: "string", default : "None" },
			repeat_month : { type: "string", nullable: true },
			repeat_day : {  type: "string", nullable: true },
			repeat_weekday : { type: "string", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
