"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { Errors } = require("@fisas/ms_core").Helpers;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.maintenances_requests",
	table: "maintenance_request",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "maintenances_requests")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"target_id",
			"target",
			"priority",
			"category_failure_id",
			"description",
			"file_id",
			"maintainer_id",
			"user_id",
			"user_email",
			"user_phone",
			"status",
			"cancel_justification",
			"review",
			"updated_at",
			"created_at"
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			target_id: { type: "number" },
			target : { type: "string" },
			priority : { type: "number", default: 0  },
			category_failure_id : { type: "number" },
			description : { type: "string", nullable: true },
			file_id : { type: "number", nullable: true },
			maintainer_id : { type: "number", nullable: true, optional: true },
			user_id : { type: "number", optional: true },
			user_email : { type: "string", max: 200, nullable: true, optional: true },
			user_phone : { type: "string", max: 25, nullable: true, optional: true },
			status : { type: "string", default: "Open", optional: true },
			cancel_justification : { type: "string", nullable: true, optional: true },
			review : { type: "string", nullable: true, optional: true }
		},
	},
	hooks: {
		before: {
			list: [
				"filters"
			],
			create: [
				"validations",
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
		list: {
			// REST: GET /
			visibility: "published",
		},
		create: {
			// REST: POST /
			visibility: "published",
		},
		get: {
			// REST: GET /:id
			visibility: "published",
		},
		update: {
			// REST: PUT /:id
			visibility: "published",
		},
		remove: {
			// REST: DELETE /:id
			visibility: "published",
		},
		patch: {
			// REST: PATCH /:id
			visibility: "published",
		},
		/**
		 * Action for cancel maintenance request
		 */
		cancel_request:{
			rest: "POST /:id/cancel",
			params:{
				cancel_justification: { type : "string" }
			},
			visibility: "published",
			async handler(ctx) {
				const maintenanceRequest = await this._get(ctx, { id : ctx.params.id });
				if (maintenanceRequest[0].user_id !== ctx.meta.user.id) {
					throw new Errors.ValidationError("Maintenance Request not found",
						"MAINTENANCE_REQUEST_NOT_FOUND",
						{});
				}
				if (maintenanceRequest[0].status !== "Open") {

					throw new Errors.ValidationError("Maintenance Request not found",
						"MAINTENANCE_REQUEST_NOT_FOUND",
						{});
				}
				maintenanceRequest[0].status = "Cancel";
				maintenanceRequest[0].cancel_justification =  ctx.params.cancel_justification;
				return await this._update(ctx, maintenanceRequest[0]);
			}
		},

		/**
		 * Action for review maintenance request
		 */
		review_request: {
			rest: "POST /:id/review",
			params: {
				review: { type: "string" }
			},
			visibility: "published",
			async handler(ctx) {
				const maintenanceRequest = await this._get(ctx, { id : ctx.params.id });

				if (maintenanceRequest[0].user_id !== ctx.meta.user.id) {
					throw new Errors.ValidationError("Maintenance Request not found",
						"MAINTENANCE_REQUEST_NOT_FOUND",
						{});
				}
				if (maintenanceRequest[0].status !== "Finish") {

					throw new Errors.ValidationError("Maintenance Request not found",
						"MAINTENANCE_REQUEST_NOT_FOUND",
						{});
				}
				maintenanceRequest[0].review = ctx.params.review;
				return await this._update(ctx, maintenanceRequest[0]);
			}
		},

		/**
		 * Action for patch status of maintenance request
		 */
		patch_status:{
			rest: "PATCH /:id/status",
			params: {
				status : { type: "string" }
			},
			visibility: "published",
			async handler(ctx) {
				const maintenanceRequest = await this._get(ctx, { id : ctx.params.id });
				maintenanceRequest[0].status = ctx.params.status;
				return await this._update(ctx, maintenanceRequest[0]);
			}
		},

		/**
		 * Action for patch maintainer of maintenance request
		 */
		patch_maintainer: {
			rest: "PATCH /:id/maintainer",
			params:{
				maintainer_id : { type: "number",  },
			},
			visibility: "published",
			async handler(ctx) {
				const maintenanceRequest = await this._get(ctx, { id : ctx.params.id });
				maintenanceRequest[0].maintainer_id =  ctx.params.maintainer_id;
				return await this._update(ctx, maintenanceRequest[0]);
			}
		},

		/**
		 * Action for get tasks of maintenance requests
		 */
		get_requests_tasks: {
			rest: "GET /:id/tasks",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				return await ctx.call("maintenance.maintenances_requests_tasks.find", {
					query: {
						maintenance_request_id : ctx.params.id
					}
				});
			}
		},

		/**
		 * Action for add task to maintenance request
		 */
		add_request_tasks: {
			rest:"POST /:id/tasks",
			visibility: "published",
			params:{
				name: { type: "string" },
				code: { type: "string" },
				maintainer_id: { type: "number" },
				notification: { type: "boolean" },
				notification_date: { type: "date", convert: true },
				intervention_date: { type: "date", convert: true },
				intervention_start: { type: "date", convert: true },
				intervention_end: { type: "date", convert: true },
				intervention_spent: { type: "number" },
				parameters: { type: "string" },
				status: { type: "string" },
				observations: { type: "string" }
			},
			async handler(ctx) {
				await this._get(ctx, { id: ctx.params.id });
				const entity = {
					name: ctx.params.name,
					code: ctx.params.code,
					maintainer_id: ctx.params.maintainer_id,
					notification: ctx.params.notification,
					notification_date: ctx.params.notification_date,
					intervention_date: ctx.params.intervention_date,
					intervention_start: ctx.params.intervention_start,
					intervention_end: ctx.params.intervention_end,
					intervention_spent: ctx.params.intervention_spent,
					parameters: ctx.params.parameters,
					status: ctx.params.status,
					observations: ctx.params.observations,
					maintenance_request_id: parseInt(ctx.params.id)
				};
				return await ctx.call("maintenance.maintenances_requests_tasks.create", entity);
			}
		},

		/**
		 * Action for update task of maintenance request
		 */
		update_request_task: {
			rest: "PUT /:id/tasks/:task_id",
			visibility: "published",
			params: {
				name: { type: "string" },
				code: { type: "string" },
				maintainer_id: { type: "number" },
				notification: { type: "boolean" },
				notification_date: { type: "date", convert: true },
				intervention_date: { type: "date", convert: true },
				intervention_start: { type: "date", convert: true },
				intervention_end: { type: "date", convert: true },
				intervention_spent: { type: "number" },
				parameters: { type: "string" },
				status: { type: "string" },
				observations: { type: "string" }
			},
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				const taskRequest = await ctx.call("maintenance.maintenances_requests_tasks.get", { id : ctx.params.task_id });
				if(taskRequest[0].maintenance_request_id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError("Maintenance Request Task not found",
						"MAINTENANCE_REQUEST_TASK_NOT_FOUND",
						{});
				}
				ctx.params.maintenance_request_id = parseInt(ctx.params.id);
				ctx.params.id = ctx.params.task_id;
				return await ctx.call("maintenance.maintenances_requests_tasks.update", ctx.params);
			}
		},

		/**
		 * Action for patch task of maintenance request
		 */
		patch_request_task:{
			rest: "PATCH /:id/tasks/:task_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				const taskRequest = await ctx.call("maintenance.maintenances_requests_tasks.get", { id : ctx.params.task_id });
				if(taskRequest[0].maintenance_request_id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError("Maintenance Request Task not found",
						"MAINTENANCE_REQUEST_TASK_NOT_FOUND",
						{});
				}
				ctx.params.id = ctx.params.task_id;
				return await ctx.call("maintenance.maintenances_requests_tasks.patch", ctx.params);
			}
		},

		/**
		 * Action for delete task of maintenance request
		 */
		delete_request_task: {
			rest: "DELETE /:id/tasks/:task_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				const taskRequest = await ctx.call("maintenance.maintenances_requests_tasks.get", { id : ctx.params.task_id });
				if(taskRequest[0].maintenance_request_id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError("Maintenance Request Task not found",
						"MAINTENANCE_REQUEST_TASK_NOT_FOUND",
						{});
				}
				return await ctx.call("maintenance.maintenances_requests_tasks.remove", { id : ctx.params.task_id });
			}
		},

		/**
		 * Action for add attachment to maintenance request task
		 */
		add_attachment_task:{
			rest: "POST /:id/tasks/:task_id/attachments",
			params:{
				file_id: { type: "number" }
			},
			visibility: "published",
			async handler(ctx){
				await ctx.call("media.files.get", { id : ctx.params.file_id });
				await this._get(ctx, { id : ctx.params.id });
				const taskRequest = await ctx.call("maintenance.maintenances_requests_tasks.get", { id : ctx.params.task_id });
				if(taskRequest[0].maintenance_request_id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError("Maintenance Request Task not found",
						"MAINTENANCE_REQUEST_TASK_NOT_FOUND",
						{});
				}
				const entity = {
					task_id : parseInt(ctx.params.task_id),
					file_id : parseInt(ctx.params.file_id)
				};
				return await ctx.call("maintenance.requests_tasks_attachments.create", entity );
			}
		},

		delete_attachment_task: {
			rest: "DELETE /:id/tasks/:task_id/attachments/:attach_id",
			visibility: "published",
			async handler(ctx) {
				await this._get(ctx, { id : ctx.params.id });
				const taskRequest = await ctx.call("maintenance.maintenances_requests_tasks.get", { id : ctx.params.task_id });
				if(taskRequest[0].maintenance_request_id !== parseInt(ctx.params.id)) {
					throw new Errors.ValidationError("Maintenance Request Task not found",
						"MAINTENANCE_REQUEST_TASK_NOT_FOUND",
						{});
				}
				const attachment = await ctx.call("maintenance.requests_tasks_attachments.get", { id : ctx.params.attach_id });
				if(attachment[0].task_id !== parseInt(ctx.params.task_id)) {
					throw new Errors.ValidationError("Task Attachment not found",
						"TASK_ATTACHMENT_NOT_FOUND",
						{});
				}
				return await ctx.call("maintenance.requests_tasks_attachments.remove", { id : ctx.params.attach_id });
			}
		}
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {
		async validations(ctx) {
			let target;
			if (ctx.params.target.toLowerCase() === "assets") {
				target = await ctx.call("infrastructure.assets.get", { id: ctx.params.target_id });
			}
			if (ctx.params.target.toLowerCase() === "components") {
				target = await ctx.call("infrastructure.components.get", { id: ctx.params.target_id });
			}
			if (target[0].area_id !== undefined && target[0].area_id !== ctx.params.area_id) {
				throw new Errors.ValidationError("Target area_id is different from the the input area_id",
					"TARGET_AREAID_DIFFERENT_THE_INPUT_AREAID",
					{});
			}
			if(ctx.params.file_id) {
				await ctx.call("media.files.get", { id : ctx.params.file_id });
			}
			ctx.params.user_id = ctx.meta.user.id;
			ctx.params.status = "Open";
			if(ctx.meta.user.email !== null &&  ctx.meta.user.email !== "" ) {
				ctx.params.user_email = ctx.meta.user.email;
			}
			if(ctx.meta.user.phone !== null && ctx.meta.user.phone !== "" ) {
				ctx.params.user_phone = ctx.meta.user.phone;
			}
		},

		filters(ctx){
			if (ctx.params.target) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["target"] = ctx.params.target;
			}
			if(ctx.params.target_id) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["target_id"] = ctx.params.target_id;
			}
			if (ctx.params.priority) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["priority"] = ctx.params.priority;
			}
			if(ctx.params.category_failure_id){
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["category_failure_id"] = ctx.params.category_failure_id;
			}
			if(ctx.params.file_id) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["file_id"] = ctx.params.file_id;
			}
			if(ctx.params.user_id) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["user_id"] = ctx.params.user_id;
			}
			if(ctx.params.user_email) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["user_email"] = ctx.params.user_email;
			}
			if(ctx.params.user_phone){
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["user_phone"] = ctx.params.user_phone;
			}
			if(ctx.params.cancel_justification) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["cancel_justification"] = ctx.params.cancel_justification;
			}
			if(ctx.params.review) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["review"] = ctx.params.review;
			}
			if(ctx.params.status) {
				ctx.params.query = ctx.params.query || {};
				ctx.params.query["status"] = ctx.params.status;
			}
		}
	},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
