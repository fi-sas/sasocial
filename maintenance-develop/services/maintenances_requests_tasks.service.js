"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { hasMany } = require("@fisas/ms_core").Helpers.WithRelateds;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.maintenances_requests_tasks",
	table: "maintenance_request_task",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "maintenances_requests_tasks")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"name",
			"code",
			"maintenance_request_id",
			"maintainer_id",
			"notification",
			"notification_date",
			"intervention_date",
			"intervention_start",
			"intervention_end",
			"intervention_spent",
			"parameters",
			"status",
			"observations",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: ["attachments"],
		withRelateds: {
			attachments(ids, docs, rule, ctx) {
				return hasMany(docs, ctx, "maintenance.requests_tasks_attachments", "attachments", "id", "task_id");
			}
		},
		entityValidator: {
			name: { type: "string", max: 100 },
			code : { type: "string", max: 255, nullable: true },
			maintenance_request_id : { type : "number" },
			maintainer_id : { type : "number" },
			notification : { type : "boolean", default: false },
			notification_date : { type : "date", nullable: true, convert: true },
			intervention_date : { type : "date", nullable: true, convert: true },
			intervention_start : { type: "date", nullable: true, convert: true },
			intervention_end : { type: "date", nullable: true, convert: true },
			intervention_spent : { type: "number", nullable: true },
			parameters : { type : "string", nullable: true },
			status : { type : "string" },
			observations : { type: "string", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
