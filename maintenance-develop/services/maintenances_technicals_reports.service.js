"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.maintenances_technicals_reports",
	table: "maintenance_technical_report",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "maintenances_technicals_reports")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"previous_report_id",
			"target_id",
			"target",
			"user_id",
			"intervention_date",
			"intervention_start",
			"intervention_spent",
			"parameters",
			"status",
			"description",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			previous_report_id: { type: "number", nullable: true },
			target_id : { type: "number" },
			target : { type: "string" },
			user_id : { type: "number" },
			intervention_date : { type: "date", nullable: true },
			intervention_start : { type: "date", nullable: true },
			intervention_spent : { type: "date", nullable: true },
			parameters : { type: "string", nullable: true },
			status : { type: "string" },
			description : { type: "string", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
