"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.requests_tasks_attachments",
	table: "request_task_attachment",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "requests_tasks_attachments")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"task_id",
			"file_id",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			task_id: { type: "number" },
			file_id: { type: "number", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
