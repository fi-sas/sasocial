"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.tasks_attachments",
	table: "task_attachments",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "tasks_attachments")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"task_id",
			"file_id",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			task_id: { type: "number" },
			file_id: { type: "number", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
