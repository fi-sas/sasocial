"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "maintenance.tasks_records",
	table: "task_records",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("maintenance", "tasks_records")],

	/**
   * Settings
   */
	settings: {
		fields: [
			"id",
			"task_id",
			"maintainer_id",
			"notification",
			"notification_date",
			"notification_sent",
			"intervention_date",
			"intervention_start",
			"intervention_end",
			"intervention_spent",
			"parameters",
			"status",
			"observations",
			"updated_at",
			"created_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			task_id: { type: "number" },
			maintainer_id : { type: "number" },
			notification : { type: "boolean", default: false },
			notification_date : { type: "date", nullable: true },
			notification_sent : { type: "boolean", default: false },
			intervention_date : { type: "date", nullable: true },
			intervention_start : { type: "date", nullable: true },
			intervention_end : { type: "date", nullable: true },
			intervention_spent : { type: "number", nullable: true },
			parameters : { type: "string", nullable: true },
			status: { type: "string" },
			observations : { type: "string", nullable: true }
		},
	},
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				},
			],
		},
	},
	/**
   * Dependencies
   */
	dependencies: [],

	/**
   * Actions
   */
	actions: {
	},

	/**
   * Events
   */
	events: {},

	/**
   * Methods
   */
	methods: {},

	/**
   * Service created lifecycle event handler
   */
	created() { },

	/**
   * Service started lifecycle event handler
   */
	async started() { },

	/**
   * Service stopped lifecycle event handler
   */
	async stopped() { },
};
