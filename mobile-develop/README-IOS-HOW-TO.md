
[Source](https://nativescript.nl/tips/how-to-test-your-nativescript-app-on-a-real-ios-device/ "Permalink to How to test your NativeScript App on a real iOS device | Tips")

# How to test your NativeScript App on a real iOS device | Tips

## How to test your NativeScript App on a real iOS device?

Although it's very easy and usefull to test your app in the iOS simulator, at one point you'll need to test your app on a real iOS device too without distributing it via [TestFlight][1]. Android will allow you to just simple transfer your app to a device and test it. For iOS however you'll need to do more.

### 1\. Enroll in the Apple Developer Program

Joining the [Apple Developer Program][2] is the first step. As a member, you have access to the resources you need to configure app services and to submit new apps and updates. 

### 2\. Create an iOS Certificate

Login to the [Apple Developer Member Center][3] and click on 'Certificates, IDs & Profiles'.

Click on the plus sign at the top and select 'iOS App Development' on the next page, scroll all the way down and hit 'Continue'.

![Certificates, IDs & Profiles][4]

The next step is to create a Certificate Signing Request (CSR).

In the Applications folder on your Mac, open the Utilities folder and launch Keychain Access.

Within the Keychain Access drop down menu, select 'Keychain Access' > 'Certificate Assistant' > 'Request a Certificate from a Certificate Authority'. The Certificate Information window below will appear.

![Certificate Signing Request][5]

In this window, enter the following information:

* In the User Email Address field (E-mailadres gebruiker), enter/select your email address.
* In the Common Name field (Algemene naam), create a name for your private key (e.g., My Dev Key).
* The CA Email Address field (E-mailadres CA) should be left empty.
* In the "Request is" group (Aanvraag is), select the "Saved to disk" option (Bewaard op schijf).

Click Continue (Ga door) within Keychain Access to complete the CSR generating process and save the file.

Your CSR file is now created, a public and private key pair is automatically generated. Your private key is stored on your computer. It is stored in the login Keychain by default and can be viewed in the Keychain Access app under the "Keys" category. Your requested certificate is the public half of your key pair.

Go back to the browser and click 'Continue' to go to the page to upload your .certSigningRequest file.

After clicking 'Continue' your certificate will be generated and is ready for download.

After downloading your certificate, double click the .cer file to install it in the Keychain Access. Make sure to save a backup copy of your private and public keys somewhere secure.

![Your certificate is ready][6]

### 3\. Register an App ID

Click on 'App IDs' below 'Identifiers' in the menu at the left and then on the plus sign at the top.

Enter an name for your app in 'App ID Description' and select 'Explicit App ID' at the 'App ID Suffix' section.

You now have to enter your bundle ID. This can be found in your package.json file under "nativescript" > "id".
    
    
    {
      "description": "NativeScript Application",
      "license": "SEE LICENSE IN ",
      "readme": "NativeScript Application",
      "repository": "",
      "nativescript": {
        "id": "org.nativescript.PROJECTNAME",
        "tns-android": {
          "version": "2.5.0"
        },
        "tns-ios": {
          "version": "2.5.0"
        }
      }
    
    

Scroll all the way down and hit 'Continue' and 'Register' on the next page.

### 4\. Register a device

Click on 'All' below 'Devices' in the menu at the left and then on the plus sign at the top.

Under 'Register Device' enter a name for your device.

Next we need the UDID (Unique Device Identifier) of your device.

To get this, connect your device to your Mac, open a terminal window and enter the following command.
    
    
    instruments -s devices
    

This will generate a list a displayed below.

**HAL** is the name of my MacBook Pro ( [remember HAL?][7]), **iPhone van Rowdy (9.3.5)** is my connected device (an iPhone 4S with iOS 9.3.5) and all others are Simulators. The UDID is displayed within the brackets.
    
    
    Known Devices:
    HAL [K3H78TBV-L3EY-C42R-3AQ4-2932NZRBA7A2]
    iPhone van Rowdy (9.3.5) [7f270c86841322c66a03d4ef836ae29f167d6364]
    Apple TV 1080p (10.1) [8110BD26-849B-44D3-BD6D-85873A937840] (Simulator)
    Apple Watch - 38mm (3.1) [0C64CC61-3FD2-40DF-80C4-B4D0FCBE33B3] (Simulator)
    Apple Watch - 42mm (3.1) [0FCE1486-73FD-4C2E-8921-0C312AE16AE1] (Simulator)
    iPad Air (10.2) [E7029332-8DBB-42D3-AF87-B55798B99A7E] (Simulator)
    iPad Air 2 (10.2) [C204558D-1D23-4093-8E6F-1F945E865B14] (Simulator)
    iPad Pro (12.9 inch) (10.2) [76BEB007-0622-4013-9EEF-91767CBC5197] (Simulator)
    iPad Pro (9.7 inch) (10.2) [ADB591FD-3AB1-438F-B463-F6C1B01CE1E7] (Simulator)
    iPad Retina (10.2) [E5E6E8CD-8701-4C4F-8CA9-F27FD20176FA] (Simulator)
    iPhone 5 (10.2) [31C6E00F-A8FD-4C1E-BE1F-25FD8A09397B] (Simulator)
    iPhone 5s (10.2) [804ADF59-CF68-4C34-BFC2-F61CEDB1EEDB] (Simulator)
    iPhone 6 (10.2) [1D0EF76C-80FC-40AB-9DD9-C31588C5A38E] (Simulator)
    iPhone 6 Plus (10.2) [95E850B0-28AF-4D19-9D79-DE266AA59835] (Simulator)
    iPhone 6s (10.2) [DD2A5684-DB86-41FE-8D0F-D13B283227DE] (Simulator)
    iPhone 6s Plus (10.2) [FC0CCD45-A949-4103-AB8F-2A6A101C7D6D] (Simulator)
    iPhone 7 (10.2) [970B5CE1-37D3-409B-9147-A1DFCD9CA76A] (Simulator)
    iPhone 7 (10.2) + Apple Watch Series 2 - 38mm (3.1) [B6B07C5F-F245-45E1-A808-BF02E25F2D5B] (Simulator)
    iPhone 7 Plus (10.2) [3877D8E3-CD4F-4B7A-978E-B0091BAEB61B] (Simulator)
    iPhone 7 Plus (10.2) + Apple Watch Series 2 - 42mm (3.1) [EC62F5EE-7AA2-4FC5-88AD-83E273FC0FDF] (Simulator)
    iPhone SE (10.2) [6DC88662-B5AC-4829-8EAF-4B89CAB17A97] (Simulator)
    

Now copy the UDID (without the brackets) of your device and paste it in the UDID field in the browser and hit 'Continue'. And click 'Register' on the next page.

If you want to use more iOS devices, just repeat this step.

### 4\. Add an iOS Provisioning Profile

Click on 'All' below 'Provisioning Profiles' in the menu at the left and then on the plus sign at the top.

You'll now be asked what type of provisioning profile you need. Select 'iOS App Development' to create a provisioning profile to install development apps on test devices. Scroll down and click 'Continue'.

The next step is to select an App ID you want a provisioning file for. The App ID registered in step 3 should be available in the drop down. Select it and click 'Continue' again.

On the next page you select the certificate created in step 2 and click on 'Continue'.

Now it's time to select the device(s) you wish to include in this provisioning profile.   
To install an app signed with this profile on a device, the device must be included.

Next you have to provide a name that will be used to identify the profile in the portal.

And finally download and double click the file (.mobileprovision) to install your Provisioning Profile. Xcode will be started, but no feedback is given.

### 5\. Run it on your device!

Before trying to run your app on your iOS device I would advice you to remove the existing platforms directory.

Go to XCode > Preferences > Accounts, add your apple acount and go to Team User and Admin and add your certificate. (folder build/ios)

If already exists certificates exporte and install (double click) the private and public key.
    
    
    rm -rf platforms
    

Now build the app again.
    
    
    tns build ios
    

And run it!
    
    
    tns run ios
    

[1]: https://developer.apple.com/testflight/
[2]: https://developer.apple.com/programs/
[3]: https://developer.apple.com/
[4]: https://nativescript.nl/img/ios-1.png
[5]: https://nativescript.nl/img/ios-2.png
[6]: https://nativescript.nl/img/ios-3.png
[7]: https://en.wikipedia.org/wiki/HAL_9000

  