# Fisas Mobile

## Quick Start

This app use [NativeScript (Angular)](https://www.nativescript.org/).<br><br>
Execute the following command to create an app from this template:

```
tns install
tns run ios/android
```

## Build

### Android

```
tns build android
```

### IOS

```
tns build ios --release --forDevice
```

## Help

> Note: This commands is to delete all this directories and "reinstall"

```
rm -rf platforms
rm -rf hooks
rm -rf node_modules
```

```
tns install
tns run ios/android
```

### Change hosts

```
File: src/shared/config/environment.ts
```
