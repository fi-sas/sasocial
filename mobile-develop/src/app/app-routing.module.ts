import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

/**
 * Routes
 */
const routes: Routes = [
  /* default route */
  {
    path: "",
    redirectTo: "/start-up/languages",
    pathMatch: "full"
  },

  /* routes */
  {
    path: "start-up",
    loadChildren: "~/app/scenes/start-up/start-up.module#StartUpModule"
  },
  {
    path: "login",
    loadChildren: "~/app/scenes/login/login.module#LoginModule"
  },
  {
    path: "dashboard",
    loadChildren: "~/app/scenes/dashboard/dashboard.module#DashboardModule"
  },
  {
    path: "contacts",
    loadChildren: "~/app/scenes/contacts/contacts.module#ContactsModule"
  },
  {
    path: "about",
    loadChildren: "~/app/scenes/about/about.module#AboutModule"
  },
  {
    path: "user",
    loadChildren: "~/app/scenes/user/user.module#UserModule"
  },
  {
    path: "news",
    loadChildren: "~/app/scenes/news/news.module#NewsModule"
  },
  {
    path: "ticket",
    loadChildren: "~/app/scenes/ticket/ticket.module#TicketModule"
  },
  {
    path: "meals",
    loadChildren: "~/app/scenes/meals/meals.module#MealsModule"
  },
  {
    path: "ubike",
    loadChildren: "~/app/scenes/ubike/ubike.module#UbikeModule"
  },
  {
    path: "mobility",
    loadChildren: "~/app/scenes/mobility/mobility.module#MobilityModule"
  },
  {
    path: "current-account",
    loadChildren: "~/app/scenes/current-account/current-account.module#CurrentAccountModule"
  },
  /* routes for play and tests */
  {
    path: "play/posts",
    loadChildren: "~/app/scenes/play/post/list/post-list.module#PostListModule"
  },
  {
    path: "play/posts/:id",
    loadChildren:
      "~/app/scenes/play/post/detail/post-detail.module#PostDetailModule"
  }
];

/**
 * Module
 */
@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
