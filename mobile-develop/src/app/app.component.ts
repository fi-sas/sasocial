import { Component, OnInit, NgZone } from "@angular/core";
import * as Connectivity from "tns-core-modules/connectivity";
import * as dialogs from "tns-core-modules/ui/dialogs";

/* firebase.google.com */
import { LocalNotifications } from "nativescript-local-notifications";
//import * as firebase from "nativescript-plugin-firebase";
const firebase = require("nativescript-plugin-firebase");
import { StorageService } from "./shared/storage/storage.service";
import { AuthService } from "./shared/auth/auth.service";
import { Config } from "./shared/storage/storage";
import { Translate } from "./shared/translate.service";

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {
    /**
     * Public properties
     */
    public connectionType: string;

    public configStorage: Config;
    

    public language: string;

    /**
   * Constructor
   * @param {Translate}       private _translate
   */
    constructor(
        private zone: NgZone,
        private _storage: StorageService,
        private _storageService: StorageService,
        private _translate: Translate
    ) {
        this.connectionType = "Unknown";
    }
    
    ngOnInit(): void {
        this.configStorage = this._storage.get("config");
        
        this.configStorage = this._storageService.get("config");
        this.language = this.configStorage.language.acronym;
        this._translate.setLanguage(this.language);
        
        firebase.init({
        showNotifications: true,
        showNotificationsWhenInForeground: true,

        onPushTokenReceivedCallback: (token) => {
            console.log('[Firebase] onPushTokenReceivedCallback:', { token });
        },

        onMessageReceivedCallback: (message) => {
            console.log('[Firebase] onMessageReceivedCallback:', { message });
            // dialogs.alert({
            //     title: (message.title !== undefined ? message.title : ""),
            //     message: JSON.stringify(message.body),
            //     okButtonText: "Ok"
            // });
        }
        })
        .then(() => {
            console.log('[Firebase] Initialized');
        })
        .catch(error => {
            console.log('[Firebase] Initialize', { error });
        });

        /* no internet */
        this.connectionType = this.connectionToString(
            Connectivity.getConnectionType()
        );
        Connectivity.startMonitoring(connectionType => {
            this.zone.run(() => {
                this.connectionType = this.connectionToString(connectionType);
                if (this.connectionType === "No Connection!") {
                    dialogs.alert({
                        title: this.language === "pt" ? "Sem Internet" : "No Internet!",
                        message:
                        this.language === "pt"
                            ? "Sem Ligação à Internet"
                            : "No Internet Connection!",
                        okButtonText: "Ok"
                    })
                    .then(function () { });
                }
            });
        });
    }

    public connectionToString(connectionType: number): string {
        switch (connectionType) {
            case Connectivity.connectionType.none:
                return "No Connection!";
            case Connectivity.connectionType.wifi:
                return "Connected to WiFi!";
            case Connectivity.connectionType.mobile:
                return "Connected to Cellular!";
            default:
                return "Unknown";
        }
    }
}