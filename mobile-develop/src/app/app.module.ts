import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

/* {N} */
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

/* app routes */
import { AppRoutingModule } from "./app-routing.module";

/* languages translations */
import { registerLocaleData } from "@angular/common";
import localePt from "@angular/common/locales/pt";
import localeEn from "@angular/common/locales/en";
registerLocaleData(localePt, "pt-PT");
registerLocaleData(localeEn, "en-US");

/* config */
import { CONFIGURATION } from "./shared/config/config";

/* shared services */
import {
  Configurator,
  OPTIONS_TOKEN
} from "./shared/config/configurator.service";
import { AuthService } from "./shared/auth/auth.service";
import { ResourceService } from "./shared/resource.service";
import { StorageService } from "./shared/storage/storage.service";
import { UrlService } from "./shared/url.service";
import { Translate } from "./shared/translate.service";

import { MenuModule } from "~/app/components/menu/menu.module";

import { MenuService } from "~/app/components/menu/menu.service";

/* components */
import { AppComponent } from "./app.component";
import { Config } from "./shared/storage/storage";

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    TranslateModule.forRoot(),

    /* in imports, routing must be at the end */
    AppRoutingModule,
    MenuModule
  ],
  providers: [
    { provide: OPTIONS_TOKEN, useValue: CONFIGURATION },
    Configurator,
    ResourceService,
    StorageService,
    UrlService,
    {
      provide: AuthService,
      useClass: AuthService,
      deps: [UrlService, Configurator, StorageService, ResourceService]
    },
    {
      provide: Translate,
      useClass: Translate,
      deps: [TranslateService]
    },
    {
      provide: MenuService,
      useClass: MenuService
    }
  ],
  declarations: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
  public configStorage: Config;
  /**
   * Create instance of the module.
   * @param {Translate}    private _translate
   * @param {Configurator} private _config
   */
  constructor(
    private _translate: Translate,
    private _configurator: Configurator,
    private _storageService: StorageService
  ) {
    this.init();
  }

  /**
   * Set language in translate service.
   */
  private init(): void {
    this.configStorage = this._storageService.get("config");
    // const langAcronym = this.configStorage.language.acronym;
    // console.log('LANGUAGE IS: ' + langAcronym);
    // const langAcronym: string = this._configurator.getOption(
    //   "DEFAULT_LANG",
    //   "pt"
    // );

    //this._translate.setLanguage(langAcronym);
  }
}
