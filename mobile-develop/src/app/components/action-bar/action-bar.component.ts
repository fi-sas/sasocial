import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Page as corePage } from "tns-core-modules/ui/page";
import { Subject, of, Observable } from "rxjs";
import { takeUntil, flatMap, retry } from "rxjs/operators";

import { AuthService } from "~/app/shared/auth/auth.service";
import { MenuService } from "~/app/components/menu/menu.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "fi-action-bar",
  moduleId: module.id,
  templateUrl: "./action-bar.component.html",
  styleUrls: ["./action-bar.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ActionBarComponent implements OnInit, OnDestroy {
  /**
   * Inputs
   */
  @Input() public actionBarType: number = 1;
  @Input() public actionBarTitle: string;
  @Input() public actionBarBack: string = null;

  @Output() public backPress = new EventEmitter();

  /**
   * Public proprities
   */
  public isLogged = false;
  public menuOpen = false;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Router}           private _router
   * @param {RouterExtensions} private _routerExtensions
   * @param {Page}             private _page
   * @param {MenuService}      private _menuService$
   * @param {AuthService}      private _authService
   * @param {StorageService}   private _storage
   * @param {corePage}         private _corePage
   */
  constructor(
    private _router: Router,
    private _routerExtensions: RouterExtensions,
    private _page: Page,
    private _menuService$: MenuService,
    private _authService: AuthService,
    private _storage: StorageService,
    private _corePage: corePage
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this._corePage.on('unloaded', (data) => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Toogle menu
   */
  toogleMenu() {
    this.menuOpen = !this.menuOpen;
    this._menuService$.toogleMenu(this.menuOpen);
  }

  /**
   * Back in history
   */
  back() {
    this.backPress.emit();
    if (this.actionBarBack) {
      this._router.navigate([this.actionBarBack]);
    } else {
      this._routerExtensions.back();
    }
  }

  /**
   * Go to dashboard
   */
  goToDashboard() {
    this.generateToken()
      .pipe(takeUntil(this._subject))
      .subscribe(() => {
        this._router.navigate(["dashboard"]);
      });
  }

  /**
   * Generate token
   */
  generateToken(): Observable<boolean> {
    return this._authService.setDeviceToken().pipe(
      retry(5),
      flatMap(authorized => {
        return of(true);
      })
    );
  }
}
