import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  Input
} from "@angular/core";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
//import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "fi-canteen-ticket",
  moduleId: module.id,
  templateUrl: "./canteen-ticket.component.html",
  styleUrls: ["./canteen-ticket.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class CanteenTicketComponent implements OnInit, OnDestroy {
  /**
   * Inputs
   */
  @Input() public image: string;
  @Input() public date: string;
  @Input() public meal: string;
  @Input() public name: string;
  @Input() public type: string;
  @Input() public refectory: string;
  @Input() public marginBottom: number = 0;
  @Input() public marginLeft: number = 0;
  @Input() public border: boolean = false;
  @Input() public maxLines: boolean = false;
  @Input() public width: string;

  /**
   * Public proprities
   */
  public configStorage: Config;
  public institution: string;

  /**
   * Private proprities
   */

  /**
   * Constructor
   */
  constructor(
    private _storage: StorageService,
    ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
  }
  /**
   * On destroy
   */
  ngOnDestroy(): void {}
}
