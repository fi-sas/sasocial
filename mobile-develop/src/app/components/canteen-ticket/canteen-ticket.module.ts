import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { TranslateModule } from "@ngx-translate/core";

import { CanteenTicketComponent } from "./canteen-ticket.component";

import { Translate } from "../../shared/translate.service";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule],
  exports: [CanteenTicketComponent],
  declarations: [CanteenTicketComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CanteenTicketModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
