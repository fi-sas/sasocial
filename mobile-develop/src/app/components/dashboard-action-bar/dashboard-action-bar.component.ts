import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Page as corePage } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { parseJwt, Utf8 } from "~/app/shared/utilities/string-utilities";

import { StorageService } from "~/app/shared/storage/storage.service";
import { MenuService } from "~/app/components/menu/menu.service";
import { Config, User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "fi-dashboard-action-bar",
  moduleId: module.id,
  templateUrl: "./dashboard-action-bar.component.html",
  styleUrls: ["./dashboard-action-bar.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class DashboardActionBarComponent implements OnInit, OnDestroy {
  /**
   * Public proprities
   */
  public isLogged = false;
  public menuOpen = false;
  public configStorage: Config;
  public userStorage: User;
  public tokenStorage: any;

  public institution: string;
  public loggedUser: string;
  public user: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Router}           private _router
   * @param {StorageService}   private _storage
   * @param {RouterExtensions} private _routerExtensions
   * @param {Page}             private _page
   * @param {MenuService}      private _menuService$
   * @param {corePage}         private _corePage
   */
  constructor(
    private _router: Router,
    private _storage: StorageService,
    private _routerExtensions: RouterExtensions,
    private _page: Page,
    private _menuService$: MenuService,
    private _corePage: corePage
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.userStorage = this._storage.get("user");

    this.isLogged = this.userStorage.logged;

    this.institution = this.configStorage.institution.acronym;
    this.loggedUser = this.userStorage.logged ? this.setUserName() : null;

    this._storage.change.pipe(takeUntil(this._subject)).subscribe(res => {
      this.userStorage = this._storage.get("user");
      this.isLogged = this.userStorage.logged;
      this.loggedUser = this.userStorage.logged ? this.setUserName() : null;
    });

    this._corePage.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Toogle Menu
   */
  toogleMenu() {
    this.menuOpen = !this.menuOpen;
    this._menuService$.toogleMenu(this.menuOpen);
  }

  /**
   * Go back in history
   */
  back() {
    this._routerExtensions.back();
  }

  /**
   * Show profile or login
   */
  showProfileOrLogin() {
    if (this.isLogged) {
      this.goToUserProfile();
    } else {
      this.goToLogin();
    }
  }

  /**
   * Go to user profile
   */
  goToUserProfile() {
    this._router.navigate(["user/profile"]);
  }

  /**
   * Go to login
   */
  goToLogin() {
    this._router.navigate(["login"]);
  }

  /**
   * Set User Name
   */
  setUserName() {
    this.tokenStorage = this._storage.get("device_token");
    const tokenData: any = parseJwt(this.tokenStorage.token);
    this.user = tokenData.user;

    return Utf8(this.user.name);
  }
}
