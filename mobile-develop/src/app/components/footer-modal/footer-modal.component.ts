import { Component, OnInit, OnDestroy, ViewEncapsulation, EventEmitter, Output } from "@angular/core";
import { Router } from "@angular/router";
import { takeUntil } from "rxjs/operators";
import { Subject, forkJoin } from "rxjs";

import { FooterModalService } from "~/app/shared/services/footer-modal.service";
import { PaymentsService } from "~/app/shared/services/payments.service";
import { BalancesService } from "~/app/shared/services/balances.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { find } from "lodash";
import { alert } from "tns-core-modules/ui/dialogs";
import { UbikeService } from "~/app/shared/services/ubike.service";

/**
 * Component
 */
@Component({
  selector: "fi-footer-modal",
  moduleId: module.id,
  templateUrl: "./footer-modal.component.html",
  styleUrls: ["./footer-modal.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class FooterModalComponent implements OnInit, OnDestroy {
  /**
   * Public proprities
   */
  public ticket: any; // object with  meal properties
  public ticketId: any; // object with reservation properties
  public active: boolean;
  public userStorage: any;
  public isLogged: boolean;
  public success: boolean = false; // buying operation success state
  public success2: boolean = false; // cancel operation success state
  public successUbike: boolean = false; // cancel ubike application success state
  public error: boolean = false; // general error
  public noBalance: boolean = false; // insufficient balance error
  public isLoading: boolean = false;
  public footerType: number; // case 0 is a buying operation, case 1 is a cancel operation
  public candidature: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  @Output() reloadPage = new EventEmitter();
  
  /**
   * Constructor
   * @param {FooterModalService}  private _modalService
   * @param {PaymentsService}     private _paymentsService
   * @param {BalancesService}     private _balancesService
   * @param {MealsService}        private _mealsService
   * @param {StorageService}      private _storageService
   * @param {Router}              private _router
   */
  constructor(
    private _modalService: FooterModalService,
    private _paymentsService: PaymentsService,
    private _balancesService: BalancesService,
    private _mealsService: MealsService,
    private _storageService: StorageService,
    private _ubikeService: UbikeService,
    private _router: Router
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    this._modalService.pipe(takeUntil(this._subject)).subscribe(data => {
      this.active = this._modalService.modalOpen;
      this.footerType = this._modalService.footerType;
      this.ticket = this._modalService.ticket;
      this.ticketId = this._modalService.ticketId;
      this.candidature = this._modalService.candidatureId;
      this.userStorage = this._storageService.get("user");
      this.isLogged = this.userStorage.logged;

      if (this.active && !this.isLogged) {
        this._modalService.toogleModal(false);
        alert({
          title: "Não estás Logado!",
          message: "Serviço de compra não disponível.",
          okButtonText: "Ok"
        }).then(function() {});

        /* this._router.navigate(["login"]); */
      }
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Close Modal
   */
  closeModal() {
    this._modalService.toogleModal(false);
    this.resetModal();
  }

  /**
   * Reset Modal
   */
  resetModal() {
    this.success = false;
    this.success2 = false;
    this.successUbike = false;
    this.error = false;
    this.noBalance = false;
  }

  /**
   * Buy Meal
   */
  buy() {
    this.isLoading = true;

    this._balancesService
      .getBalances()
      .pipe(takeUntil(this._subject))
      .subscribe(
        res => {
          let account = find(res, function(o) {
            return o.account_id == 1;
          });

          if (account.current_balance < this.ticket.price) {
            this.pay();
            // this.noBalance = true;
            // this.isLoading = false;
          } else {
            this.pay();
          }
        },
        err => {
          this.error = true;
          this.isLoading = false;
        }
      );
  }

  /**
   * Cancel Ticket
   */
  cancel() {
    this.isLoading = true;

    this._mealsService
      .cancelReservation(this.ticketId.id)
      .pipe(takeUntil(this._subject))
      .subscribe(
        res => {
          forkJoin(
            this._balancesService.getBalances(),
            this._mealsService.getReservations()
          )
            .pipe(takeUntil(this._subject))
            .subscribe(res => {
              this.success2 = true;
              this.isLoading = false;
          });
        },
        err => {
          console.log('Erro: ' + JSON.stringify(err));
          this.error = true;
          this.isLoading = false;
        }
      );
  }

  /**
   * Pay Meal
   */
  pay() {
    this._paymentsService
      .payCart(1, this.ticket)
      .pipe(takeUntil(this._subject))
      .subscribe(
        res => {
          forkJoin(
            this._balancesService.getBalances(),
            this._mealsService.getReservations()
          )
            .pipe(takeUntil(this._subject))
            .subscribe(res => {
              this.success = true;
              this.isLoading = false;
            });
        },
        err => {
          if(err.error.errors[0].code == 104 || err.error.errors[0].message == "Insufficient funds"){
            this.noBalance = true;
          }
          else{
            this.error = true;
          }
          this.isLoading = false;
        }
      );
  }

  // cancel ubike application
  cancelUbike() {
    this.isLoading = true;
    const data = {
      action: "cancel"
    };
    this._ubikeService
      .withdrawApplication(this.candidature, data)
      .pipe(takeUntil(this._subject))
      .subscribe(
        res => {
          // reinicar página;
          this.isLoading = false;
          this.successUbike = true;
          this.reloadPage.emit();
        },
        err => {
        console.log('Erro: ' + JSON.stringify(err));
        this.error = true;
        this.isLoading = false;
        }
      );
  }
}
