import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { TranslateModule } from "@ngx-translate/core";

import { CanteenTicketModule } from "~/app/components/canteen-ticket/canteen-ticket.module";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";
import { PaymentsService } from "~/app/shared/services/payments.service";
import { BalancesService } from "~/app/shared/services/balances.service";
import { MealsService } from "~/app/shared/services/meals.service";

import { FooterModalComponent } from "./footer-modal.component";

import { Translate } from "../../shared/translate.service";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, CanteenTicketModule],
  exports: [FooterModalComponent],
  declarations: [FooterModalComponent],
  providers: [
    {
      provide: FooterModalService,
      useClass: FooterModalService,
      deps: []
    },
    {
      provide: PaymentsService,
      useClass: PaymentsService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: BalancesService,
      useClass: BalancesService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: MealsService,
      useClass: MealsService,
      deps: [UrlService, ResourceService, StorageService]
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FooterModalModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
