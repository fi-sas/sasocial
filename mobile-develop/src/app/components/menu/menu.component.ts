import { Component, OnInit, ViewEncapsulation, OnDestroy } from "@angular/core";
import {
  GestureTypes,
  SwipeGestureEventData
} from "tns-core-modules/ui/gestures";
const stackModule = require("tns-core-modules/ui/layouts/stack-layout");
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { StorageService } from "~/app/shared/storage/storage.service";
import { MenuService } from "~/app/components/menu/menu.service";
import { User, Config } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { parseJwt } from "~/app/shared/utilities/string-utilities";

import * as firebase from "nativescript-plugin-firebase";

/**
 * Component
 */
@Component({
  selector: "fi-menu",
  moduleId: module.id,
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class MenuComponent implements OnInit, OnDestroy {
  /**
   * Public proprities
   */
  public menuOpen = false;
  public institution: string;
  public institution2: string;
  public initialMenu = false;
  public configStorage: Config;
  public ubike = false;

  public userStorage: User;
  public tokenStorage: any;
  public topic: any;

  public isLogged = false;

  /* Private */
  private _subject = new Subject<boolean>();
  private _authData = null;
  /**
   * Constructor
   * @param {StorageService} private _storage
   * @param {Router}         private _router
   * @param {MenuService}    private _menuService$
   */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _menuService$: MenuService,
    private _ubikeService: UbikeService
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this._menuService$.pipe(takeUntil(this._subject)).subscribe(data => {
      this.menuOpen = this._menuService$.menuOpen;
      this.institution = this._menuService$.institution;
      this.getStorageData();
      this.setInstitution();
    });
  }

  setInstitution(): void {
    const configStorage: Config = this._storage.get("config");
    this.institution2 = configStorage.institution.acronym;
  }

  /**
   * Swipe close
   * @param {SwipeGestureEventData} args
   */
  swipeClose(args: SwipeGestureEventData) {
    if (args.direction === 2) {
      this.toogleMenuClose();
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Toogle menu close
   */
  toogleMenuClose() {
    this.initialMenu = true;
    this._menuService$.toogleMenu(false);
  }

  /**
   * Close Menu
   */
  toogleMenuNav() {
    this._menuService$.toogleMenu(false);
  }

  /**
   * Logout
   */
  logout(): void {
    this.toogleMenuNav();
    const userStorage: User = this._storage.get("user");
    // unsubscribe based on email topic
    this.topic = (userStorage.email).replace("@",".");
    firebase.unsubscribeFromTopic(this.topic).then(() => console.log("Unsubscribed from topic: " + this.topic));
    
    // unsubscribe based on institution
    switch(this.institution2){
      case 'IPVC': 
        firebase.unsubscribeFromTopic('geral.ipvc.pt').then(() => console.log("Unsubscribed to topic: geral.ipvc.pt"));
        break;
      case 'IPCA':
        firebase.unsubscribeFromTopic('geral.ipca.pt').then(() => console.log("Unsubscribed to topic: geral.ipca.pt"));
        break;
      case 'IPB': 
        firebase.unsubscribeFromTopic('geral.ipb.pt').then(() => console.log("Unsubscribed to topic: geral.ipb.pt"));
        break;
    }

    userStorage.logged = false;
    this._storage.set("user", userStorage);

    const tokenStorage: any = this._storage.get("device_token");
    //tokenStorage.token = null;
    this._storage.set("device_token", tokenStorage);
    console.log(tokenStorage);
    this._router.navigate(["login"]);
  }

  /**
   *UBIKE
   */
  getCandidatures() {
    this._ubikeService
      .getCandidature(-1, 0)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.tokenStorage = this._storage.get("device_token");


        const token = parseJwt(this.tokenStorage.token);
        const userID: any = token.user.id;

        if (res.data && res.data.length > 0 && res.data[0].user_id == userID) {
          this.ubike = true;
        } else {
          this.ubike = false;
        }
      });
  }

  /**
   * Get storage data
   */
  getStorageData(): void {
    this.configStorage = this._storage.get("config");
    this.userStorage = this._storage.get("user");
    this.tokenStorage = this._storage.get("device_token");
    this.isLogged = this.userStorage.logged;

    if (this.isLogged) {
      this.getCandidatures();
    }
  }
}
