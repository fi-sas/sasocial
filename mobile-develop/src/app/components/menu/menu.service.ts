import { Injectable, ɵConsole } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

@Injectable()
export class MenuService extends BehaviorSubject<any> {
  /**
   * Public propreties
   */
  public menuOpen = false;
  public institution: string;
  public configStorage: Config;

  /**
   * Constructor
   */
  constructor(private _storage: StorageService) {
    super(null);
  }

  /**
   * Toogle menu
   * @param {boolean} menuOpen
   */
  toogleMenu(menuOpen: boolean) {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;

    this.menuOpen = !this.menuOpen;
    this.next(null);
  }
}
