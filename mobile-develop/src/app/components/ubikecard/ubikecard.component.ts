import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Page as corePage } from "tns-core-modules/ui/page";
import { Subject, of, Observable } from "rxjs";
import { takeUntil, flatMap, retry } from "rxjs/operators";

import { AuthService } from "~/app/shared/auth/auth.service";
import { MenuService } from "~/app/components/menu/menu.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";

/**
 * Component
 */
@Component({
    selector: "fi-ubikecard",
    moduleId: module.id,
    templateUrl: "./ubikecard.component.html",
    styleUrls: ["./ubikecard.component.scss"],
    encapsulation: ViewEncapsulation.Native
})

export class UbikecardComponent implements OnInit, OnDestroy {
    /* Public proprities */

    public isLoading = true;
    public bikeId = 0;
    public id: any = null;

    public limit = -1;
    public offset = 0;
    public data: any;
  
    /* Private proprities */
    private _subject = new Subject<boolean>();

    // emit modal in parent component
    @Output() ModalPageOpen = new EventEmitter();

    /**
    * Constructor
    * @param {Router}             private _router
    * @param {RouterExtensions}   private _routerExtensions
    * @param {Page}               private _page
    * @param {MenuService}        private _menuService$
    * @param {AuthService}        private _authService
    * @param {StorageService}     private _storage
    * @param {corePage}           private _corePage
    */

    constructor(
        private _page: Page,
        private _corePage: corePage,
        private _ubikeService: UbikeService,
        private _router: Router,
        private _storage: StorageService
    ) {
        this._page.actionBarHidden = true;
    }

  /* On init */
    ngOnInit(): void {
        this.getTrips();
        this.getCandidatures();
        this._corePage.on("unloaded", data => {
            //this.ngOnDestroy();
        });

        this._storage.change.subscribe(data => {
            if(data.action == 'add' && data.item.key === 'reloaddash'){
                this.getTrips();
                this.getCandidatures();
                this._corePage.on("unloaded", data => {
                    //this.ngOnDestroy();
                });
            }
        });
    }

    getTrips() {
        this._ubikeService.getTrips(this.limit, this.offset)
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
            if (res && res.data && res.data.length > 0) {
                this.bikeId = res.data[0].bike_id;
            }
            this.isLoading = false;
        });
    }
    getCandidatures() {
    this._ubikeService
        .getCandidature(this.limit, this.offset)
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
        if (res && res.data && res.data.length > 0) {
            this.data = res.data[0];
            this.id = this.data.id;
            }
        
        this.isLoading = false;
        });
    }

    // if the the current screen is 'dashboard', clicking in the card (outside the button) navigates to details. Else does nothing
    gotoDetails(){
        if(this._router.url == "/ubike/dashboard"){   
            this._router.navigate(['/ubike/candidatura-details/']);
        }
    }
    
    approve() {
        const data = {
            action: "confirm"
        };

        this._ubikeService
            .approveApplication(this.id, data)
            .pipe(takeUntil(this._subject))
            .subscribe(() => {
                this._storage.set('reloaddash', true);
                this.getCandidatures();
            });
    }
    reject() {
        const data = {
            action: "reject"
        };
        this._ubikeService
        .rejectApplication(this.id, data)
        .pipe(takeUntil(this._subject))
        .subscribe(() => {
            this._storage.set('reloaddash', true);
            this.getCandidatures();
        });
    }

    withdraw() {
        this.ModalPageOpen.emit(this.id);
        // const data = {
        //     action: "cancel"
        // };
        // this._ubikeService
        // .withdrawApplication(this.id, data)
        // .pipe(takeUntil(this._subject))
        // .subscribe(() => {
        //     this._storage.set('reloaddash', true);
        //     this.getCandidatures();
        //     this.reloadPage.emit();
        // });
    }

    /* On destroy */
    ngOnDestroy(): void {
        this._subject.next(true);
        this._subject.unsubscribe();
    }
}
