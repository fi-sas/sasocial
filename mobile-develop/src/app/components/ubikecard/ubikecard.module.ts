import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { TranslateModule } from "@ngx-translate/core";

import { UbikecardComponent } from "./ubikecard.component";

import { Translate } from "../../shared/translate.service";
import { FooterModalModule } from "../footer-modal/footer-modal.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, FooterModalModule],
  exports: [UbikecardComponent],
  declarations: [UbikecardComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UbikecardModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
