//import { LocalNotifications } from "nativescript-local-notifications";

export class CustomAppDelegate /*extends UIResponder implements UIApplicationDelegate*/ {

    /*public static ObjCProtocols = [UIApplicationDelegate];
    public static ObjCExposedMethods = {
        "runOnBackground": { returns: interop.types.void }
    };


    private bgTask;
    private data;
    private timer;
    public timeLuch = new Date(new Date().getTime() + (10 * 1000));
    public timeDinner = new Date(new Date().getTime() + (40 * 1000));
    private aux;


    public applicationDidEnterBackground(application: UIApplication) {
        console.log("Enter background");
        this.bgTask = application.beginBackgroundTaskWithNameExpirationHandler("MyTask", () => {
            this.endBackgroundTask();
        });

        this.aux = 1;
        console.log("Start on background.");
        this.timer = NSTimer.scheduledTimerWithTimeIntervalTargetSelectorUserInfoRepeats(2, this, "runOnBackground", null, true);
    }

    public applicationDidFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<string, any>): boolean {
        return true;
    }

    private endBackgroundTask(): void {
        if (this.timer) {
            this.timer.invalidate();
            this.timer = null;
        }
        this.data = {};
        this.aux = 1;
        UIApplication.sharedApplication.endBackgroundTask(this.bgTask);
        this.bgTask = UIBackgroundTaskInvalid;
        console.log("End of background task.");
    }

    public runOnBackground(): void {
        if (this.aux == 3) {
            this.endBackgroundTask();
            return;
        }

        console.log(this.aux);

        fetch('https://jsonplaceholder.typicode.com/posts/'+this.aux).then((response) => response.json()).then((res:any) => {
            console.log('----------');
            console.log(JSON.stringify(res));
            console.log('----------');
            this.data = {
                id: res.id,
                title: res.title.slice(0, 10),
                body: 'Yeahhh Funca!',
                at: (this.aux == 1) ? this.timeLuch : this.timeDinner
            };

            LocalNotifications.schedule([this.data]).then(
                function() {
                  //this.endBackgroundTask();
              return;
                },
                function(error) {
                  console.log("scheduling error: " + error);
                }
          );

            this.aux++;
        }).catch((err) => {
            console.log('ERRO: '+JSON.stringify(err));
        });

    }*/
}