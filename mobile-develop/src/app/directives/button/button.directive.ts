import { Directive, HostBinding, HostListener } from "@angular/core";

@Directive({
  selector: "[fisasButton]"
})
export class ButtonDirective {
  constructor() {}

  @HostBinding("class.fisas__btn--hover") isClicked = false;

  @HostListener("onTouch") onTouch() {
    this.isClicked = !this.isClicked;
  }
}
