import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ButtonDirective } from "./button.directive";

@NgModule({
  schemas: [NO_ERRORS_SCHEMA],
  exports: [ButtonDirective],
  declarations: [ButtonDirective]
})
export class ButtonDirectiveModule {}
