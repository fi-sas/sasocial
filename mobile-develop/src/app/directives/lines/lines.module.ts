import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { LinesDirective } from "./lines.directive";

@NgModule({
  schemas: [NO_ERRORS_SCHEMA],
  exports: [LinesDirective],
  declarations: [LinesDirective]
})
export class LinesDirectiveModule {}
