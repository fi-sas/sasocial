import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import * as utils from "tns-core-modules/utils/utils";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config, User } from "~/app/shared/storage/storage";
/**
 * Component
 */
@Component({
  selector: "Contacts",
  moduleId: module.id,
  templateUrl: "./contacts.component.html",
  styleUrls: ["./contacts.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ContactsComponent implements OnInit {
  /**
   * Public proprities
   */

  public isLoading = true;
  public configStorage: Config;
  public institution: any;

  /**
   * Constructor
   * @param {StorageService}  private _storage
   */
  constructor(private _storage: StorageService) {}

  /**
   * On init
   */

  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
  }

  clickUrl(url) {
    utils.openUrl(url);
  }
}
