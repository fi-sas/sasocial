import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { Translate } from "~/app/shared/translate.service";

import { ContactsRoutingModule } from "./contacts-routing.module";
import { ContactsComponent } from "./contacts.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    ContactsRoutingModule
  ],
  declarations: [ContactsComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ContactsModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
