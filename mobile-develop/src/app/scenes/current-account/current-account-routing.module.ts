import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CurrentAccountComponent } from "./current-account.component";

const routes: Routes = [
    /* main */
    {
        path: "list",
        component: CurrentAccountComponent,
        loadChildren: "~/app/scenes/current-account/list/list.module#CurrentAccountListModule"
    },
    /* current-account /detail + id */
    {
        path: "detail/:id",
        component: CurrentAccountComponent,
        loadChildren: "~/app/scenes/current-account/detail/detail.module#CurrentAccountDetailModule"
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CurrentAccountRoutingModule {}