import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
import { BalancesService } from '~/app/shared/services/balances.service';

/**
 * Component
 */
@Component({
  selector: 'ns-current-account',
  moduleId: module.id,
  templateUrl: './current-account.component.html',
  styleUrls: ['./current-account.component.css'],
  encapsulation: ViewEncapsulation.Native
})
export class CurrentAccountComponent implements OnInit {
    /**
     * Public proprities
     */
    public isLoading = true;
    public configStorage: Config;
    public institution: any;
    /**
     * Constructor
     * @param {StorageService}    private _storage
     * @param {Router}            private _router
     * @param {Page}              private _page
     * @param {BalancesService}   private _balancesService
     */
    constructor(
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _balancesService: BalancesService
    ) {
        this._page.actionBarHidden = true;
    }

    ngOnInit() {
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
    }

}