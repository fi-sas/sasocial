import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { CurrentAccountComponent } from './current-account.component';

import { CurrentAccountRoutingModule } from "./current-account-routing.module";
import { UrlService } from '~/app/shared/url.service';
import { ResourceService } from '~/app/shared/resource.service';
import { StorageService } from '~/app/shared/storage/storage.service';
import { BalancesService } from '~/app/shared/services/balances.service';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CurrentAccountComponent],
  imports: [NativeScriptCommonModule,TranslateModule,CurrentAccountRoutingModule],
  providers: [
    {
        provide: BalancesService,
        useClass: BalancesService,
        deps: [UrlService, ResourceService, StorageService]
    }
],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CurrentAccountModule { }