import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from "rxjs";
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '~/app/shared/storage/storage.service';
import { Page } from 'tns-core-modules/ui/page/page';
import { BalancesService } from '~/app/shared/services/balances.service';
import { Config } from '~/app/shared/storage/storage';

/**
 * Component
 */
@Component({
    selector: 'ns-detail',
    moduleId: module.id,
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.Native
})
export class CurrentAccountDetailComponent implements OnInit {
    /**
     * Public properties
     */
    public isLoading = true;
    public locale: string;
    public movementId: number;
    public movementDetail: any;
    public movementStatus: string;

    /**
     * Private proprities
     */
    private _subject = new Subject<boolean>();
    /**
    * Constructor
     * @param {ActivatedRoute} private _activatedRoute
     * @param {StorageService} private _storageService
     * @param {BalancesService} private _balancesService
     * @param {Page}           private _page
     */

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _storageService: StorageService,
        private _balancesService: BalancesService,
        private _page: Page
    ) {}

    /* On init */
    ngOnInit() {
        /* language */
        const configStorage: Config = this._storageService.get("config");
        const langAcronym = configStorage.language.acronym;
        this.locale = langAcronym + "-" + langAcronym.toUpperCase();

        /* route params */
        this._activatedRoute.params.forEach(params => {
            this.movementId = params["id"];
        });
        console.log('Mounting Details comp com id: ' + this.movementId);

        /* movement detail */
        this._balancesService
            .findItem(this.movementId.toString())
            .subscribe((res: any) => {
                this.movementDetail = res.data[0];
                switch(this.movementDetail.status){
                    case 'Confirmed':
                        this.movementStatus = "Confirmado";
                        break;
                    case 'Pending':
                        this.movementStatus = "Pendente";
                        break;
                    case 'Paid':
                        this.movementStatus = "Pago";
                        break;
                    case 'Cancelled':
                        this.movementStatus = "Cancelado";
                        break;
                    case 'Cancelled due to Lack of Payment':
                        this.movementStatus = "Cancelado por falta de pagamento"
                        break;
                }
                this.isLoading = false;
            });
    }

    cancelMove(){
        console.log('Cancel movement');
    }
    /* On destroy */
    ngOnDestroy() {
        this._subject.next(true);
        this._subject.unsubscribe();
    }
}
