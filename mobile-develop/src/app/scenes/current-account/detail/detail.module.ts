import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { CurrentAccountDetailComponent } from './detail.component';
import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';
import { ButtonDirectiveModule } from '~/app/directives/button/button.module';
import { NgShadowModule } from 'nativescript-ng-shadow';
import { TranslateModule } from '@ngx-translate/core';
import { Translate } from '~/app/shared/translate.service';
import { NativeScriptFormsModule } from 'nativescript-angular';
import { CurrentAccountDetailRoutingModule } from './detail.routing.module';

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
    declarations: [CurrentAccountDetailComponent],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgShadowModule,
        TranslateModule,
        ActionBarModule,
        ButtonDirectiveModule,
        CurrentAccountDetailRoutingModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
    })
export class CurrentAccountDetailModule { 
    /**
	 * Constructor
	 * @param {Translate} private _translate
	 */
    constructor(
		private _translate: Translate
		) {
			this._translate.load(PT_TRANSLATION, "pt");
			this._translate.load(EN_TRANSLATION, "en");
		}
}