import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { CurrentAccountDetailComponent } from "./detail.component";

const routes: Routes = [
  /* default: mobility / detail */
  {
    path: "",
    component: CurrentAccountDetailComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class CurrentAccountDetailRoutingModule {}