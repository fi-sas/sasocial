import { Component, OnInit, OnDestroy } from '@angular/core';
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
import { BalancesService } from '~/app/shared/services/balances.service';

/**
 * Component
 */
@Component({
    selector: 'ns-list',
    moduleId: module.id,
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class CurrentAccountListComponent implements OnInit, OnDestroy {
    /**
     * Public properties
     */
    public isLoading = true;
    public locale: string;
    public accounts: any;
    public movements: any;
    public groupByDate: any;
    public balance: any;
    public inDebt: any;
    public account_name: any;
    public institution: any;
    public configStorage: Config;

    public next: any;
    public selected = 1;
    public total = 0;
    public maxTotalLimit = 200;
    public limit = 10;
    public offset = 0;
    public loadMore = true;

    /**
     * Private properties
     */
    private _subject = new Subject<boolean>();

    /**
     * Constructor
     * @param {BalancesService}    private _balancesService
     * @param {StorageService} private _storageService
     * @param {Page}           private _page
     */
    constructor(
        private _balancesService: BalancesService,
        private _page: Page,
        private _storage: StorageService
    ) {}

    ngOnInit(): void {
        this.movements = [];
        
        this._balancesService.getBalances().subscribe((data: any) => {
            this.accounts = data;
            this.selected = data[0].account_id;
            this.balance = data[0].current_balance;
            this.inDebt = data[0].in_debt;
            this.account_name = data[0].account_name;
        })
        // get moves with loader
        this.getMovements(true);

        /* language */
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        const langAcronym = this.configStorage.language.acronym;
        this.locale = langAcronym + "-" + langAcronym.toUpperCase();
    }

    // switches selected account and shows loader animation
    selectAccount(x,y,w,z){
        if (this.isLoading){
            return;
        }
        else{
            this.movements = [];
            this.selected = x;
            this.account_name = y;
            this.balance = w;
            this.inDebt = z;
            this.offset = 0;
            this.total = 0;
            this.loadMore = true;
            this.getMovements(true);
        }
    }

    // gets movements without loader
    getMovs(){
        this.getMovements(false);
    }
    /* On destroy */
    ngOnDestroy() {
        this._subject.next(true);
        // this._subject.unsubscribe();
    }

    /* Get more movements */
    getMovements(x) {
    if(x == true){
        this.isLoading = true;
    }

    if (this.offset + this.limit < this.total) {
        this.offset = this.offset + this.limit;
    }
    
        /* movements */
        this._balancesService
            .getMovements(this.limit, this.offset, this.selected)
            .pipe(takeUntil(this._subject))
            .subscribe((res: any) => {
                this.total = res.link.total;
                this.isLoading = false;

            if(res.data && res.data.length > 0){
                let moreMovements = res.data;
                this.movements = this.movements.concat(moreMovements);
                
                if(this.movements.length >= this.total){
                    this.loadMore = false;
                }
            }
        });
    }

    isEqualPrev(prev,current){
        const d1 = new Date(prev);
        const d2 = new Date(current);
        if(d1.toDateString() === d2.toDateString()){
            return true;
        }
        else{
            return null;
        }
    }
}