import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import {
  ModalDatetimepicker,
  PickerOptions
} from "nativescript-modal-datetimepicker";
import { Subject, of, Observable, forkJoin } from "rxjs";
import {
  takeUntil,
  flatMap,
  retry,
  concatMap,
  catchError
} from "rxjs/operators";

import { orderBy } from "lodash";
import { parseJwt } from "~/app/shared/utilities/string-utilities";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config, User } from "~/app/shared/storage/storage";
import { AuthService } from "~/app/shared/auth/auth.service";

import { NewsService } from "~/app/shared/services/news.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { BalancesService } from "~/app/shared/services/balances.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "Dashboard",
  moduleId: module.id,
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class DashboardComponent implements OnInit, OnDestroy {
  /**
   * Public properties
   */
  public birthday;

  public isLoading = true;

  public configStorage: Config;
  public userStorage: User;
  public tokenStorage: any;

  public isLogged = false;

  public institution = null;
  public school = null;
  public refectory = null;

  public user: any;
  public news: any;
  public balances: any;

  public meals: any;
  public reservations: any;
  public day: any;
  public hour: number;
  public refectoryId: number;

  /**
   * Private properties
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {AuthService}     private _authService
   * @param {NewsService}     private _newsService
   * @param {BalancesService} private _balancesService
   * @param {MealsService}    private _mealsService
   * @param {Page}            private _page
   * @param {FooterModalService}  private _modalService
   */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _authService: AuthService,
    private _newsService: NewsService,
    private _balancesService: BalancesService,
    private _mealsService: MealsService,
    private _page: Page,
    private _modalService: FooterModalService
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.birthday = "";

    this.getStorageData();
    this.setMealsData();

    this.dataInit();

    this._balancesService.emitter
      .asObservable()
      .pipe(takeUntil(this._subject))
      .subscribe(data => {
        this.balances = this._balancesService.balances;
      });

    this._mealsService.emitter
      .asObservable()
      .pipe(takeUntil(this._subject))
      .subscribe(data => {
        this.reservations = this._mealsService.reservations;
      });

    this._page.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * Data Init
   */
  dataInit() {
    this.getData()
      .pipe(takeUntil(this._subject))
      .subscribe(res => {
          const tokenData: any = parseJwt(this.tokenStorage.token);
          this.user = tokenData.user;

          this.news = res[0].data;
          this.meals = orderBy(res[1].data, ["available"], ["desc"]);
          this.balances = res[2];
          this.reservations = res[3];
        this.isLoading = false;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Get storage data
   */
  getStorageData(): void {
    this.configStorage = this._storage.get("config");
    this.userStorage = this._storage.get("user");
    this.tokenStorage = this._storage.get("device_token");
    this.isLogged = this.userStorage.logged;

    this.institution = this.configStorage.institution;
    this.school = this.configStorage.school.acronym;
    this.refectory = this.configStorage.refectory.name;
  }

  /**
   * Set meals data
   */
  setMealsData(): void {
    this.day = new Date().toISOString().split("T")[0];
    this.hour = new Date().getHours();
    this.refectoryId = this.configStorage.refectory.id;
  }

  /**
   * Get dashboard data
   */

  getData(): Observable<any> {
    return this.validateToken().pipe(
      concatMap(() =>
        forkJoin(
          this._newsService.getNews(10, 0),
          this._mealsService.getMeals(
            this.refectoryId,
            this.day,
            this.hour > 13 ? "dinner" : "lunch"
          ),
          this._balancesService.getBalances(),
          this._mealsService.getReservations()
        )
      )
    );
  }

  /**
   * Validate token
   */
  validateToken(): Observable<boolean> {
    const userStorage: User = this._storage.get("user");
    const tokenData: any = parseJwt(this.tokenStorage.token);

    /* if user guest */
    if (
      this._authService.hasStorageToken() &&
      tokenData.user.user_name === "guest"
    ) {
      userStorage.logged = false;
      this._storage.set("user", userStorage);
      return this.generateToken();
    } else if (this._authService.hasStorageToken()) {
      /* if token in storage */
      return this._authService.validateToken().pipe(
        concatMap(rs => {
          if (rs) {
            return of(true);
          } else {
            userStorage.logged = false;
            this._storage.set("user", userStorage);
            return this.generateToken();
          }
        }),
        catchError(error => {
          return of(false);
        })
      );
    } else {
      return this.generateToken();
    }
  }

  /**
   * Generate token
   */
  generateToken(): Observable<boolean> {
    return this._authService.setDeviceToken().pipe(
      retry(5),
      flatMap(authorized => {
        return of(true);
      })
    );
  }

  /**
   * Pick date
   */
  pickDate(): void {
    const picker = new ModalDatetimepicker();

    picker
      .pickDate({
        title: "Please enter your birthday",
        theme: "dark",
        maxDate: new Date(),
        is24HourView: false
      })
      .then(result => {
        this.birthday =
          result["year"] + "-" + result["month"] + "-" + result["day"];
      })
      .catch(error => {
        console.log("Error: " + error);
      });
  }

  /**
   * Open Modal
   */
  openModal(product) {
    this._modalService.setTicketModal(product);
    this._modalService.toogleModal(true);
  }

  // checks if a reservation annulment can be done
  check(item){
    if(item.menu_dish.nullable_until){
      let nullable = new Date(item.menu_dish.nullable_until).getTime();
      var now = new Date().getTime();
      if(nullable - now > 0){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return true;
    }
  }
  
  // cancel a reservation
  cancelTicket(item){
    let resp = this.check(item);
    if (resp == true){
      this._modalService.setTicketModal2(item);
      this._modalService.toogleModal(true);
    }
    else{
      console.log('Hora excedida');
    }    
  }
}
