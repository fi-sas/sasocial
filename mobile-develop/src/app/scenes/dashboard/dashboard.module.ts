import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgShadowModule } from "nativescript-ng-shadow";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from "@ngx-translate/core";

import { ButtonDirectiveModule } from "~/app/directives/button/button.module";
import { LinesDirectiveModule } from "~/app/directives/lines/lines.module";
import { DashboardActionBarModule } from "~/app/components/dashboard-action-bar/dashboard-action-bar.module";
import { MenuModule } from "~/app/components/menu/menu.module";
import { Translate } from "~/app/shared/translate.service";
import { CanteenTicketModule } from "~/app/components/canteen-ticket/canteen-ticket.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { NewsService } from "~/app/shared/services/news.service";
import { BalancesService } from "~/app/shared/services/balances.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NgShadowModule,
    TranslateModule,
    DashboardActionBarModule,
    CanteenTicketModule,
    FooterModalModule,
    ButtonDirectiveModule,
    LinesDirectiveModule,
    DashboardRoutingModule,
    MenuModule
  ],
  declarations: [DashboardComponent],
  providers: [
    {
      provide: NewsService,
      useClass: NewsService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: BalancesService,
      useClass: BalancesService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: MealsService,
      useClass: MealsService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: FooterModalService,
      useClass: FooterModalService,
      deps: []
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
