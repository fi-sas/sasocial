import { Component, OnInit, ViewEncapsulation } from "@angular/core";

/**
 * Component
 */
@Component({
  selector: "Error",
  moduleId: module.id,
  templateUrl: "./error.component.html",
  styleUrls: ["./error.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ErrorComponent implements OnInit {
  /**
   * On init
   */
  ngOnInit(): void {}
}
