import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CreateAccountComponent } from "./create-account.component";

const routes: Routes = [

  /* default: login / create-account */
  {
    path: "",
    component: CreateAccountComponent,
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class CreateAccountRoutingModule { }
