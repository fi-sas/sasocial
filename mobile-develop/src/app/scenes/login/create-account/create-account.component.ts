import { Component, OnInit, ViewEncapsulation } from "@angular/core";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "CreateAccount",
  moduleId: module.id,
  templateUrl: "./create-account.component.html",
  styleUrls: ['./create-account.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class CreateAccountComponent implements OnInit {
  /* Public proprities */
  public institution: any;

  /**
  * Constructor
  * @param {StorageService}  private _storage
  */
  constructor(
    private _storage: StorageService
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    const configStorage: Config = this._storage.get("config");
    this.institution = configStorage.institution.acronym;
  }

}
