import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { LoginComponent } from "./login.component";
import { CreateAccountComponent } from "./create-account/create-account.component";
import { RecoverComponent } from "./recover/recover.component";

const routes: Routes = [

    /* login */
    {
      path: '',
      component: LoginComponent
    },

    /* login / create-account */
    {
      path: 'create-account',
      loadChildren: '~/app/scenes/login/create-account/create-account.module#CreateAccountModule'
    },

    /* login / recover */
    {
      path: 'recover',
      loadChildren: '~/app/scenes/login/recover/recover.module#RecoverModule'
    }

  ];


@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LoginRoutingModule { }
