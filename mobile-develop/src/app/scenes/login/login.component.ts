import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Subject, of } from "rxjs";
import { takeUntil, catchError } from "rxjs/operators";
import { keys } from "lodash";

import { StorageService } from "~/app/shared/storage/storage.service";
import { AuthService } from "~/app/shared/auth/auth.service";
import { STORAGE, Config, User } from "~/app/shared/storage/storage";
import * as firebase from "nativescript-plugin-firebase";

/**
 * Interfaces
 */
interface UserLoginInterface {
  email: string;
  pin: string;
  password: string;
}

interface UserErrorInterface {
  email: boolean;
  pin: boolean;
  password: boolean;
}

/**
 * Component
 */
@Component({
  selector: "Login",
  moduleId: module.id,
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class LoginComponent implements OnInit, OnDestroy {
  /**
   * Public proprities
   */
  public isLoading = false;
  public showScreen = false;
  public configStorage: Config;
  public userStorage: User;

  public topic: string;
  public language: string;
  public institution: string;
  public refectory: string;

  public user: UserLoginInterface = {
    email: null,
    pin: null,
    password: null
  };

  public error: UserErrorInterface = {
    email: false,
    pin: false,
    password: false
  };

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Router}         private _router
   * @param {StorageService} private _storageService
   * @param {AuthService}    private _authService
   * @param {Page}           private _page
   */
  constructor(
    private _router: Router,
    private _storageService: StorageService,
    private _authService: AuthService,
    private _page: Page
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storageService.get("config");
    this.userStorage = this._storageService.get("user");

    if (this.userStorage.email) {
      this.user.email = this.userStorage.email;
    }

    this.language = this.configStorage.language.acronym;
    this.institution = this.configStorage.institution.acronym;
    this.refectory = this.configStorage.refectory.name;

    if (this.userStorage.logged) {
      this._router.navigate(["start-up/allergens"]);
    } else {
      this.showScreen = true;
    }

    this._page.on('unloaded', (data) => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /** Submit Login - with PIN */
  submitLoginPin() {
    this.isLoading = true;

    this._authService
      .loginByEmailPin({
        email: this.user.email,
        pin: this.user.pin
      })
      .pipe(
        takeUntil(this._subject),
        catchError(err => {
          this.error.email = true;
          this.error.pin = true;
          this.isLoading = false;

          return of(err);
        })
      )
      .subscribe((res: boolean) => {
        this.error.email = res ? false : true;
        this.error.pin = res ? false : true;
        this.isLoading = false;

        /* all good */
        if (res) {

          /* reset storage for diferent user, but not device token */
          if (
            this.userStorage.email !== null &&
            this.userStorage.email !== this.user.email
          ) {

            const storageKeys = keys(STORAGE);
            storageKeys.map(key => {
              if (key !== 'device_token') {
                this._storageService.set(key, STORAGE[key]);
              }
            });

            this.subscribeTopic();
            this.configStorage.allergens.done = false;
            this._storageService.set("config", this.configStorage);

            this._router.navigate(["start-up/allergens"]);

            /* maintain storage for same user */
          } else {
            this.subscribeTopic();
            
            if (this.configStorage.allergens.done) {
              this._router.navigate(["dashboard"]);
            } else {
              this._router.navigate(["start-up/allergens"]);
            }

          }
        }
      });
  }

  /** Submit Login - with Password */
  submitLoginPass() {
    this.isLoading = true;

    this._authService
      .loginByEmailPassword({
        email: this.user.email,
        password: this.user.password
      })
      .pipe(
        takeUntil(this._subject),
        catchError(err => {
          this.error.email = true;
          this.error.password = true;
          this.isLoading = false;
          
          return of(err);
        })
      )
      .subscribe((res: boolean) => {
        this.error.email = res ? false : true;
        this.error.password = res ? false : true;
        this.isLoading = false;

        /* all good */
        if (res) {

          /* reset storage for diferent user, but not device token */
          if (
            this.userStorage.email !== null &&
            this.userStorage.email !== this.user.email
          ) {

            const storageKeys = keys(STORAGE);
            storageKeys.map(key => {
              if (key !== 'device_token') {
                this._storageService.set(key, STORAGE[key]);
              }
            });

            this.subscribeTopic();
            this.configStorage.allergens.done = false;
            this._storageService.set("config", this.configStorage);

            this._router.navigate(["start-up/allergens"]);

            /* maintain storage for same user */
          } else {
            this.subscribeTopic();
            
            if (this.configStorage.allergens.done) {
              this._router.navigate(["dashboard"]);
            } else {
              this._router.navigate(["start-up/allergens"]);
            }

          }
        }
      });
  }

  subscribeTopic(){
    this.userStorage.email = this.user.email;
    this.userStorage.logged = true;

    this._storageService.set("user", this.userStorage);
    //replace of '@' by '.', in order to subscribe the firebase individual topic
    this.topic = (this.user.email).replace("@",".");

    firebase.subscribeToTopic(this.topic).then(() => console.log("Subscribed to topic: " + this.topic));

    switch(this.institution){
      case 'IPVC': 
        firebase.subscribeToTopic('geral.ipvc.pt').then(() => console.log("Subscribed to topic: geral.ipvc.pt"));
        break;
      case 'IPCA':
        firebase.subscribeToTopic('geral.ipca.pt').then(() => console.log("Subscribed to topic: geral.ipca.pt"));
        break;
      case 'IPB': 
        firebase.subscribeToTopic('geral.ipb.pt').then(() => console.log("Subscribed to topic: geral.ipb.pt"));
        break;
    }
  }
  /**
   * Clear inputs on focus
   */
  onFocusClear() {
    if (this.error.email == true) {
      this.user.pin = null;
      this.error.email = false;
      this.error.pin = false;
      this.user.password = null;
      this.error.password = false;
    }
  }
}