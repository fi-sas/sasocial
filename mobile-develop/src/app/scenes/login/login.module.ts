import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from '@ngx-translate/core';

import { Translate } from "~/app/shared/translate.service";
import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';

import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";

const PT_TRANSLATION = require('./pt.translation.json');
const EN_TRANSLATION = require('./en.translation.json');

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    TranslateModule,
    ActionBarModule,
    LoginRoutingModule
  ],
  declarations: [
    LoginComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class LoginModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, 'pt');
    this._translate.load(EN_TRANSLATION, 'en');
  }
}
