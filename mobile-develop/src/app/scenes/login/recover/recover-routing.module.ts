import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { RecoverComponent } from "./recover.component";

const routes: Routes = [

  /* default: login / create-account */
  {
    path: "",
    component: RecoverComponent,
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class RecoverRoutingModule { }
