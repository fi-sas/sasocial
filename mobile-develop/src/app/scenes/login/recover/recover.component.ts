import { Component, OnInit, ViewEncapsulation } from "@angular/core";

import { StorageService } from "../../../shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "Recover",
  moduleId: module.id,
  templateUrl: "./recover.component.html",
  styleUrls: ['./recover.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class RecoverComponent implements OnInit {
  /* Public proprities */
  public institution: any;
  /**
  * Constructor
  * @param {StorageService}  private _storage      [description]
  * @param {[type]}          [description]
  */
  constructor(
    private _storage: StorageService,
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    const configStorage: Config = this._storage.get("config");
    this.institution = configStorage.institution.acronym;
  }

}
