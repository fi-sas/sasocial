import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from '@ngx-translate/core';

import { Translate } from "~/app/shared/translate.service";
import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';

import { RecoverRoutingModule } from "./recover-routing.module";
import { RecoverComponent } from "./recover.component";

const PT_TRANSLATION = require('./pt.translation.json');
const EN_TRANSLATION = require('./en.translation.json');

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    RecoverRoutingModule
  ],
  declarations: [
    RecoverComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class RecoverModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, 'pt');
    this._translate.load(EN_TRANSLATION, 'en');
  }
}
