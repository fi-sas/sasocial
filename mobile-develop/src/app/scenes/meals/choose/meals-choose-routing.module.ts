import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MealsChooseComponent } from "./meals-choose.component";

const routes: Routes = [
  /* default: news / choose */
  {
    path: "",
    component: MealsChooseComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MealsChooseRoutingModule {}
