import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { MealsChooseService } from "./meals-choose.service";

/**
 * Component
 */
@Component({
  selector: "MealsChoose",
  moduleId: module.id,
  templateUrl: "./meals-choose.component.html",
  styleUrls: ["./meals-choose.component.scss"]
})
export class MealsChooseComponent implements OnInit {
  /**
   * Public properties
   */
  public isLoading = true;

  public schools: string;
  public refectories: number;

  public schoolsScreen = true;
  public refectoryScreen = false;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {MealsChooseService} private _mealsChoose
   * @param {Router}             private _router
   */
  constructor(
    private _mealsChoose: MealsChooseService,
    private _router: Router
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    this._mealsChoose
      .getSchools()
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.schools = res;
        this.isLoading = false;
      });
  }

  /**
   * Select a school
   * @param {object} obj item school
   */
  choiceSchool(obj) {
    this._mealsChoose.setSchool(obj);
    this.schoolsScreen = false;
    this.isLoading = true;

    this.getRefectories();
  }

  /**
   * Select a school
   */
  getRefectories() {
    const school_id = this._mealsChoose.getSchool().id;

    this._mealsChoose
      .getServices(school_id)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.refectoryScreen = true;
        this.refectories = res;
        this.isLoading = false;
      });
  }

  /**
   * Select a refectory
   * @param {object} obj item refectory
   */
  choiceRefectory(obj) {
    this._mealsChoose.setService(obj);
    this._router.navigate(["/meals/list"]);
  }
}
