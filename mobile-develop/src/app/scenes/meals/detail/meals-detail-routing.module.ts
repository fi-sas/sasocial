import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MealsDetailComponent } from "./meals-detail.component";

const routes: Routes = [
  /* default: news / detail */
  {
    path: "",
    component: MealsDetailComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MealsDetailRoutingModule {}
