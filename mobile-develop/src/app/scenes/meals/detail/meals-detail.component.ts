import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Config } from "~/app/shared/storage/storage";
import { StorageService } from "~/app/shared/storage/storage.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "MealsDetail",
  moduleId: module.id,
  templateUrl: "./meals-detail.component.html",
  styleUrls: ["./meals-detail.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class MealsDetailComponent implements OnInit {
  /**
   * Public properties
   */
  public isLoading = true;
  public isModal = false;

  public institution: string;
  public refectory: number;

  public locale: string;
  public mealsId: number;
  public mealsDetail: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {ActivatedRoute} private _activatedRoute
   * @param {StorageService} private _storageService
   * @param {mealsService}  private _mealsService
   * @param {FooterModalService}  private _modalService
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _storageService: StorageService,
    private _mealsService: MealsService,
    private _modalService: FooterModalService
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    /* language */
    const configStorage: Config = this._storageService.get("config");
    const langAcronym = configStorage.language.acronym;
    this.locale = langAcronym + "-" + langAcronym.toUpperCase();

    /* institution and refectory */
    this.institution = configStorage.institution.acronym;
    this.refectory = configStorage.refectory.id;

    /* route params */
    this._activatedRoute.params.forEach(params => {
      this.mealsId = params["id"];
    });

    /* meal detail */
    this._mealsService
      .getMeal(this.refectory.toString(), this.mealsId.toString()) //View completed example 7, 440
      .pipe(takeUntil(this._subject))
      .subscribe(data => {
        this.mealsDetail = data[0];
        this.isLoading = false;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Open Modal
   */
  openModal(product) {
    this._modalService.setTicketModal(product);
    this._modalService.toogleModal(true);
  }
}
