import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgShadowModule } from "nativescript-ng-shadow";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";
import { ButtonDirectiveModule } from "~/app/directives/button/button.module";

import { MealsDetailRoutingModule } from "./meals-detail-routing.module";
import { MealsDetailComponent } from "./meals-detail.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NgShadowModule,
    TranslateModule,
    ActionBarModule,
    FooterModalModule,
    ButtonDirectiveModule,
    MealsDetailRoutingModule
  ],
  declarations: [MealsDetailComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MealsDetailModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
