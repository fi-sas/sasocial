import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MealsListComponent } from "./meals-list.component";

const routes: Routes = [
  /* default: news / list */
  {
    path: "",
    component: MealsListComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MealsListRoutingModule {}
