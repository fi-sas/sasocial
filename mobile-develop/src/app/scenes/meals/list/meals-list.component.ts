import { Component, OnInit, OnDestroy } from "@angular/core";

import { Config } from "~/app/shared/storage/storage";
import { Subject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Translate } from "~/app/shared/translate.service";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";

import { StorageService } from "~/app/shared/storage/storage.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { MealsChooseService } from "~/app/scenes/meals/choose/meals-choose.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "MealsList",
  moduleId: module.id,
  templateUrl: "./meals-list.component.html",
  styleUrls: ["./meals-list.component.scss"]
})
export class MealsListComponent implements OnInit {
  /**
   * Public properties
   */
  public configStorage: Config;
  public isLoading = true;
  public institution: string;
  public school: string;
  public refectory: string;

  public refectoryId: number;
  public mealType = new Date().getHours() > 13 ? "dinner" : "lunch";
  public mealsList: any;

  public next15Days = [];
  public selectedDay: any;
  public contentElement = null;
  public locale: string;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();
  private subscription: Subscription;

  /**
   * Constructor
   * @param {StorageService}          private _storageService
   * @param {MealsService}            private _mealsService
   * @param {MealsChooseService}      private _mealsChoose
   * @param {TranslateService}        private _translateService
   * @param {FooterModalService}      private _modalService
   */
  constructor(
    private _storageService: StorageService,
    private _mealsService: MealsService,
    private _mealsChoose: MealsChooseService,
    private _translateService: TranslateService,
    private _modalService: FooterModalService
  ) {
    let formated;
    let fridays = 0;
    for (let i = 0; i < 25 && fridays < 3; i++) {
      const today = new Date();
      const day = new Date().setDate(new Date().getDate() + i);
      const weekDay = new Date(day).getDay();

      today.setDate(new Date().getDate() + i);
      const dd = today.getDate();
      const mm = today.getMonth() + 1;
      const yyyy = today.getFullYear();
      formated =
        yyyy +
        "-" +
        (("" + mm).length < 2 ? "0" : "") +
        mm +
        "-" +
        (("" + dd).length < 2 ? "0" : "") +
        dd;

      if (i === 0) {
        this.selectedDay = formated;
      }

      /* friday */
      if (weekDay !== 0 && weekDay % 5 === 0) {
        fridays++;
      }

      this.next15Days.push({ date: day, weekDay: weekDay, formated: formated });
    }
  }

  /**
   * On init
   */
  ngOnInit(): void {
    /* data institution */
    this.configStorage = this._storageService.get("config");
    const school = this._mealsChoose.getSchool();
    const refectory = this._mealsChoose.getService();
    this.institution = this.configStorage.institution.acronym;
    this.school = school ? school.acronym : this.configStorage.school.acronym;
    this.refectory = refectory
      ? refectory.name
      : this.configStorage.refectory.name;
    this.refectoryId = refectory
      ? refectory.id
      : this.configStorage.refectory.id;

    /* days */
    const currentLang = this._translateService.currentLang || "pt";
    //let currentLang = this.configStorage.language.acronym;
    //console.log('ESQUEMA: ' + this._translateService.currentLang);
    this.subscription = this._translateService.onLangChange.subscribe(
      (event: LangChangeEvent) => {
        this.locale = event.lang + "-" + event.lang.toUpperCase();
      }
    );

    this.locale = currentLang + "-" + currentLang.toUpperCase();

    /* meals */
    this.getMeals(this.refectoryId, this.selectedDay, this.mealType);
  }

  /**
   * Select a day
   * @param {number} index days array index
   * @param {number} day   date of a day
   */
  selectDay(index, day) {
    if (day.weekDay % 6 !== 0) {
      this.isLoading = true;
      //const days = this.contentElement.children[0].children;
      //this.contentElement.scrollLeft = days[index].offsetLeft - 120;
      this.selectedDay = day.formated;

      this.getMeals(this.refectoryId, day.formated, this.mealType);
    }
  }

  /**
   * Select type
   * @param {string} type lunch or dinner
   */
  selectType(type) {
    this.isLoading = true;
    this.mealType = type;

    this.getMeals(this.refectoryId, this.selectedDay, type);
  }

  /**
   * Get Meals
   * @param {string} refectory
   * @param {string} day
   * @param {string} type
   */
  getMeals(refectory, day, type) {
    this._mealsService
      .getMeals(refectory, day, type)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.mealsList = res.data;
        this.isLoading = false;
      });
  }

  /**
   * Open Modal
   */
  openModal(product) {
    this._modalService.setTicketModal(product);
    this._modalService.toogleModal(true);
  }
}
