import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgShadowModule } from "nativescript-ng-shadow";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { ButtonDirectiveModule } from "~/app/directives/button/button.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

import { MealsListRoutingModule } from "./meals-list-routing.module";
import { MealsListComponent } from "./meals-list.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NgShadowModule,
    TranslateModule,
    ActionBarModule,
    ButtonDirectiveModule,
    FooterModalModule,
    MealsListRoutingModule
  ],
  declarations: [MealsListComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MealsListModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
