import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MealsComponent } from "./meals.component";

const routes: Routes = [
  /* meals / list */
  {
    path: "",
    component: MealsComponent,
    children: [
      {
        path: "list",
        loadChildren:
          "~/app/scenes/meals/list/meals-list.module#MealsListModule"
      },

      {
        path: "choose",
        loadChildren:
          "~/app/scenes/meals/choose/meals-choose.module#MealsChooseModule"
      },

      {
        path: "detail/:id",
        loadChildren:
          "~/app/scenes/meals/detail/meals-detail.module#MealsDetailModule"
      }
    ]
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MealsRoutingModule {}
