import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { has } from "lodash";

import { MealsService } from "~/app/shared/services/meals.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "Meals",
  moduleId: module.id,
  templateUrl: "./meals.component.html",
  styleUrls: ["./meals.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class MealsComponent implements OnInit {
  /**
   * Public proprities
   */

  /**
   *Private
   */

  /**
   * Constructor
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {Page}            private _page
   * @param {MealsService}    private _mealsService
   */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _page: Page,
    private _mealsService: MealsService
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {}
}
