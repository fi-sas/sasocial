import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { MealsService } from "~/app/shared/services/meals.service";
import { MealsChooseService } from "./choose/meals-choose.service";
import { PaymentsService } from "~/app/shared/services/payments.service";

import { MealsRoutingModule } from "./meals-routing.module";
import { MealsComponent } from "./meals.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, MealsRoutingModule],
  declarations: [MealsComponent],
  providers: [
    {
      provide: MealsService,
      useClass: MealsService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: MealsChooseService,
      useClass: MealsChooseService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: PaymentsService,
      useClass: PaymentsService,
      deps: [UrlService, ResourceService, StorageService]
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MealsModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
