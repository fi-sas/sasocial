import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MobilityDashboardComponent } from "./mobility-dashboard.component";

const routes: Routes = [
  {
    path: "",
    component: MobilityDashboardComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MobilityDashboardRoutingModule {}