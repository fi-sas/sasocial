import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, ElementRef } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { MobilityService } from "~/app/shared/services/mobility.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";

/**
 * Component
 */
@Component({
    selector: "MobilityDashboard",
    moduleId: module.id,
    templateUrl: "./mobility-dashboard.component.html",
    styleUrls: ["./mobility-dashboard.component.scss"],
})
export class MobilityDashboardComponent implements OnInit, OnDestroy {
    @ViewChild('localSelect', {static: false}) localSelect: ElementRef;
    /**
     * Public properties
     */
    public isLoading = false;
    public locale: string;
    public configStorage: Config;
    public institution: any;
    public language: any;
    public locals: any;
    public routes: any;
    public travelType: any;
    public origin = {label: "", id: ""};
    public destination = {label: "", id: ""};
    public travelDate: any = null;
    public travelDateString: string;
    
    public total = 0;
    public maxTotalLimit = 200;
    public limit = 10;
    public offset = 0;
    public screen = 0;
    public lowerScreen = 0;
    /**
    * Private properties
    */
    private _subject = new Subject<boolean>();

    /**
    * Constructor
    * @param {MobilityService}     private _mobilityService
    * @param {StorageService}      private _storageService
    * @param {Page}                private _page
    */

    constructor(
        private _mobilityService: MobilityService,
        private _storage: StorageService
    ) {}

    /**
     * On init
     */
    ngOnInit(): void {
        this._mobilityService.getLocals(this.maxTotalLimit,this.offset,'-id').subscribe((data: any) => {
            this.locals = data.data;
        })
        
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        this.language = this.configStorage.language.acronym;
        this.travelType = 'oneway';
        this.getRoutesList();
    }

    /** Select type 
    @param {string} type oneway or pass */
    selectType(type) {
        this.isLoading = true;
        this.travelType = type;
    }

    /* place input */
    setPlace(arg){
        if (this.destination.label != arg.value) {
            this.destination.id = "";
        }
        this.destination.label = arg.value;
    }

    /** date picker */
    pickTravelDate(): void {
        let mSen = ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        let mSpt = ['','Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        const picker = new ModalDatetimepicker();
        picker
        .pickDate({
            title: "",
            theme: "dark",
            minDate: new Date(),
            is24HourView: false
        })
        .then(result => {
            if(!result["year"] || !result["month"] || !result["day"]){
                console.log('Undefined date');
            }
            else{
                this.travelDate = result["year"] + "/" + result["month"] + "/" + result["day"];
                console.log(this.travelDate);
                // set date string
                let monthN = result["month"];
                
                if(this.language === "en"){
                    this.travelDateString = result["day"] + " " + mSen[monthN] + " " + result["year"];
                }
                else{
                    this.travelDateString = result["day"] + " " + mSpt[monthN] + " " + result["year"];
                }
            }
        })
        .catch(error => {});
    }

    /* navigation to next screen and passing of params */
    getLink(){
        return ['/mobility/route', {origin: JSON.stringify(this.origin), destination: JSON.stringify(this.destination), travelDate: this.travelDate, travelDateString: this.travelDateString}];
    }

    /* switch positions between the search fields (origin and destination) */
    switchLocalInputs(){
        let a = this.origin;
        this.origin = this.destination;
        this.destination = a;
    }

    /* functions related with the originPicker */
    showScreen(x){
        this.screen = x;
        console.log(this.screen);
    }

    setOrigin(oId, oLocal){
        this.origin = {id: oId, label: oLocal};
        this.screen = 0;
    }

    setDestiny(oId, oLocal){
        this.destination = {id: oId, label: oLocal};
        this.screen = 0;
    }

    /* On destroy */
    ngOnDestroy() {
        this._subject.next(true);
        this._subject.unsubscribe();
    }

    /* Get more locals */
    getLocals() {
        this.isLoading = true;
        if (
            this.offset + this.limit < this.total &&
            this.offset + this.limit < this.maxTotalLimit
        ) {
            this.offset = this.offset + this.limit;
        }
    }

    setLowerScreen(x){
        this.lowerScreen = x;
    }

    getRoutesList(){
        this._mobilityService.getRoutes().subscribe((res: any) => {
            if(res){
                this.routes = res.data;
            }
        })
    }
}