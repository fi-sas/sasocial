import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgShadowModule } from "nativescript-ng-shadow";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from "@ngx-translate/core";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { ButtonDirectiveModule } from "~/app/directives/button/button.module";
import { MobilityDashboardComponent } from "./mobility-dashboard.component";
import { MobilityDashboardRoutingModule } from "./mobility-dashboard-routing.module";
import { Translate } from "~/app/shared/translate.service";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
	declarations: [MobilityDashboardComponent],
	imports: [
		NativeScriptCommonModule,
		NativeScriptFormsModule,
		NgShadowModule,
		TranslateModule,
		ActionBarModule,
		ButtonDirectiveModule,
		MobilityDashboardRoutingModule
	],
	schemas: [NO_ERRORS_SCHEMA]
})

export class MobilityDashboardModule {
	/**
	 * Constructor
	 * @param {Translate} private _translate
	 */
	constructor(
		private _translate: Translate
		) {
			this._translate.load(PT_TRANSLATION, "pt");
			this._translate.load(EN_TRANSLATION, "en");
		}
}