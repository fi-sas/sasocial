import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MobilityDetailComponent } from "./mobility-detail.component";

const routes: Routes = [
  /* default: mobility / detail */
  {
    path: "",
    component: MobilityDetailComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MobilityDetailRoutingModule {}