import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'ns-detail',
    moduleId: module.id,
    templateUrl: './mobility-detail.component.html',
    styleUrls: ['./mobility-detail.component.scss'],
    encapsulation: ViewEncapsulation.Native
})
export class MobilityDetailComponent implements OnInit {
    /**
     * Public properties
     */
    public objProp: string;
    public detail: any;
    public destinationHour: any;
    public destinationLocal: any;
    public originLocal: any;
    public originHour: any;

    public newRouteArray = [];
    public isExternal = false;

    constructor() { }
    ngOnInit() {
        // "paragem" is the response item of route search
        this.objProp = localStorage.getItem("paragem");
        let OBJ = JSON.parse(this.objProp);
        // OBJ.route contains the route steps (ex: [0] - caminhada, [1] - autocarro, etc.)
        this.detail = OBJ.route;
        // set "main trip" hour and place
        this.originHour = OBJ.departure_hour;
        this.originLocal = OBJ.departure_place;
        this.destinationHour = OBJ.arrival_hour;
        this.destinationLocal = OBJ.arrival_place;
        this.checkExternal();
        this.setStandardDetails();
    }

    getFromNewRouteArray(pos, property) {
        return (pos < this.newRouteArray.length) ? this.newRouteArray[property] : '';
    }

    // set a standard way to access the origin date and origin local for each step of the trip
    setStandardDetails(){
        this.detail.forEach((el, index) => {
            if(el.steps[0].vehicle === 'CAMINHADA'){
                if(!this.detail[index-1]){
                    let hour = this.originHour;
                    let local = this.originLocal;
                    let new_obj = {hour: hour, local: local};
                    this.newRouteArray = [...this.newRouteArray, new_obj];
                    console.log('Caminhada: ' + hour + ' & ' + local);
                }
                else{
                    let hour = this.detail[index-1].steps[1].departure_time;
                    let local = this.detail[index-1].steps[1].local;
                    let new_obj = {hour: hour, local: local};
                    this.newRouteArray = [...this.newRouteArray, new_obj];
                    console.log('Caminhada: ' + hour + ' & ' + local);
                }
            }
            else{
                let hour = el.steps[0].departure_time;
                let local = el.steps[0].local;
                let new_obj = {hour: hour, local: local};
                this.newRouteArray = [...this.newRouteArray, new_obj];
                console.log('Transporte: ' + hour + ' & ' + local);
            }
        });
    }

    checkExternal(){
        this.detail.forEach((el) => {
            if(el.external == true){
                this.isExternal = true;
            }
        })
    }
}