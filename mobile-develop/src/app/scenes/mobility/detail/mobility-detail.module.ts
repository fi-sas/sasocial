import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { MobilityDetailComponent } from './mobility-detail.component';
import { MobilityDetailRoutingModule } from "./mobility-detail-routing.module";
import { NativeScriptFormsModule } from 'nativescript-angular';
import { NgShadowModule } from 'nativescript-ng-shadow';
import { TranslateModule } from '@ngx-translate/core';
import { Translate } from '~/app/shared/translate.service';
import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';
import { ButtonDirectiveModule } from '~/app/directives/button/button.module';

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  declarations: [MobilityDetailComponent],
  imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgShadowModule,
        TranslateModule,
        ActionBarModule,
        ButtonDirectiveModule,
        MobilityDetailRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MobilityDetailModule { 
  /**
	 * Constructor
	 * @param {Translate} private _translate
	 */
	constructor(
		private _translate: Translate
		) {
			this._translate.load(PT_TRANSLATION, "pt");
			this._translate.load(EN_TRANSLATION, "en");
		}
}