import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MobilityComponent } from "./mobility.component";

const routes: Routes = [
    {
        path: "dashboard",
        component: MobilityComponent,
        loadChildren: "~/app/scenes/mobility/dashboard/mobility-dashboard.module#MobilityDashboardModule"
    },
    
    /* mobility / route */
    {
        path: "route",
        component: MobilityComponent,
        loadChildren: "~/app/scenes/mobility/route/mobility-route.module#MobilityRouteModule"
    },
    
    /* mobility / detail */
    {
        path: "detail",
        component: MobilityComponent,
        loadChildren: "~/app/scenes/mobility/detail/mobility-detail.module#MobilityDetailModule"
    },

    /* mobility / prices + route id + ticket type + route name */
    {
        path: "prices/:routeId/:ticket/:routeName",
        component: MobilityComponent,
        loadChildren: "~/app/scenes/mobility/prices/mobility-prices.module#MobilityPricesModule"
    }
    // {
    //     path: "",
    //     component: MobilityComponent,
    //     children: [
    //         /* mobility / dashboard */
            
    //     ]
    // }
]

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MobilityRoutingModule {}