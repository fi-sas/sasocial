import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";

import { MobilityService } from "~/app/shared/services/mobility.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
    selector: "ns-mobility",
    moduleId: module.id,
    templateUrl: "./mobility.component.html",
    styleUrls: ["./mobility.component.scss"],
    encapsulation: ViewEncapsulation.Native
})
export class MobilityComponent implements OnInit {
    /**
     * Constructor
     * @param {StorageService}    private _storage
     * @param {Router}            private _router
     * @param {Page}              private _page
     * @param {MobilityService}   private _mobilityService
     */
    constructor(
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _mobilityService: MobilityService
    ) {
        this._page.actionBarHidden = true;
    }

    /**
     * On init
     */
    ngOnInit(): void {
    }
}