import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";


import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { MobilityService } from "~/app/shared/services/mobility.service";
import { MobilityRoutingModule } from "./mobility-routing.module";
import { MobilityComponent } from "./mobility.component";

@NgModule({
    imports: [NativeScriptCommonModule, TranslateModule, MobilityRoutingModule],
    declarations: [MobilityComponent],
    providers: [
        {
            provide: MobilityService,
            useClass: MobilityService,
            deps: [UrlService, ResourceService, StorageService]
        }
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class MobilityModule {
}