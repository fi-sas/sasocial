import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MobilityPricesComponent } from "./mobility-prices.component";

const routes: Routes = [
  {
    path: "",
    component: MobilityPricesComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MobilityPricesRoutingModule {}
