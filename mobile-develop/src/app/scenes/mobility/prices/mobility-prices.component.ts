import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { device } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { parseJwt } from "~/app/shared/utilities/string-utilities";
import { MobilityService } from "~/app/shared/services/mobility.service";

/**
 * Component
 */
@Component({
    selector: "MobilityPrices",
    moduleId: module.id,
    templateUrl: "./mobility-prices.component.html",
    styleUrls: ["./mobility-prices.component.scss"],
    encapsulation: ViewEncapsulation.Native
})
export class MobilityPricesComponent implements OnInit {
    /**
     * Public proprities
     */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public institution: string;
    public tokenStorage: any;
    public prices: any;
    public travelType: any;
    public routeId: any;
    public profileID: any;
    public showError: boolean;
    public routeName: any;

    /**
     * Constructor
     * @param {Configurator}    private _config
     * @param {StorageService}  private _storage
     * @param {Router}          private _router
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     */
    constructor(
        private _config: Configurator,
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _translate: Translate,
        private _mobilityService: MobilityService,
        private _activatedRoute: ActivatedRoute
    ) {
        this._page.actionBarHidden = true;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        /* route params */
        this._activatedRoute.params.forEach(params => {
            this.routeId = params["routeId"];
            this.travelType = params["ticket"];
            this.routeName = params["routeName"];
        });

        this.isLoading = true;
        this.showError = false;
        this.tokenStorage = this._storage.get("device_token");
        const token = parseJwt(this.tokenStorage.token);
        this.profileID = token.user.profile_id;

        this.getRoutesList();
    }

    getRoutesList(){
        let travelTypeID: any;
        switch(this.travelType){
            case "oneway":
                travelTypeID = 1;
                break;
            case "pass":
                travelTypeID = 2;
                break;
            default:
                travelTypeID = 1;
                break;
        }

        this._mobilityService.getPrices(this.routeId, travelTypeID, this.profileID).subscribe((res: any) => {
            if(res.data && res.data.length > 0){
                this.prices = res.data[0].prices;
                this.isLoading = false;
            }
            else{
                this.showError = true;
                this.isLoading = false;
            }
        })
        
    }
}
