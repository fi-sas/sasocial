import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { MobilityPricesRoutingModule } from "./mobility-prices-routing.module";
import { MobilityPricesComponent } from "./mobility-prices.component";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, MobilityPricesRoutingModule, ActionBarModule,],
  declarations: [MobilityPricesComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MobilityPricesModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
