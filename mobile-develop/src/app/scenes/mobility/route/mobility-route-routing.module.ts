import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MobilityRouteComponent } from "./mobility-route.component";

const routes: Routes = [
  /* default: mobility / route */
  {
    path: "",
    component: MobilityRouteComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MobilityRouteRoutingModule {}