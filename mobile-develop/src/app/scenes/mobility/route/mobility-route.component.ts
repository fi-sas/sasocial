import { Component, OnInit, OnDestroy, ViewEncapsulation, Input } from '@angular/core';
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { MobilityService } from "~/app/shared/services/mobility.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
import { RouterExtensions } from "nativescript-angular/router";
/**
 * Component
 */
@Component({
    selector: 'MobilityRoute',
    moduleId: module.id,
    templateUrl: './mobility-route.component.html',
    styleUrls: ['./mobility-route.component.scss'],
    encapsulation: ViewEncapsulation.Native
})
export class MobilityRouteComponent implements OnInit {
    @Input() public actionBarBack: string = null;
    /**
     * Public properties
     */
    public isLoading = true;
    public locale: string;
    public route = [];
    public configStorage: Config;
    public institution: any;
    public originProp: any;
    public originPropString: any;
    public destinationProp: any;
    public dateProp: any;
    public datePropString: any;
    public respStatus: string;

    /**
     * Private properties
     */
    private _subject = new Subject<boolean>();

    /**
     * Constructor
     * @param {Router}              private _router
     * @param {RouterExtensions}    private _routerExtensions
     * @param {ActivatedRoute}      private _activatedRoute
     * @param {MobilityService}     private _mobilityService
     * @param {StorageService}      private _storageService
     * @param {Page}                private _page
     */
    constructor(
        private _router: Router,
        private _routerExtensions: RouterExtensions,
        private _mobilityService: MobilityService,
        private _storageService: StorageService,
        private _page: Page,
        private _storage: StorageService,
        private _activatedRoute: ActivatedRoute,
    ) {}
    
    /**
     * On init
     */
    ngOnInit() {
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        
        /* Get/set of route params */
        this._activatedRoute.params.forEach(params => {
            // // empty search date params returns a 'Today' string
            if(params["travelDate"] != 'null'){
                this.dateProp = params["travelDate"];
                this.datePropString = params["travelDateString"];
            }
            else{
                this.dateProp = null;
            }
            this.originProp = JSON.parse(params["origin"]);
            this.destinationProp = JSON.parse(params["destination"]);
        });

        /* Search route with requested params */
        this._mobilityService.getRoute(this.isItID(this.originProp),this.isItID(this.destinationProp),this.dateProp).subscribe((res: any) => {
            this.respStatus = res.status;
            this.isLoading = false;
            this.route = this.route.concat(res.data);
            this.route.forEach((element: { timedifcalc: string; arrival_hour: any; departure_hour: any; }) => {
                element.timedifcalc = this.timeDifCalc(element.arrival_hour, element.departure_hour);
            });
            this.route.sort(function(el1: any,el2: any){
                let s = el1.departure_hour;
                let s2 = s.split(":", 2);
                let d = el2.departure_hour;
                let d2 =d.split(":", 2);
                
                // Order items based on departure with upward hour:time
                if(s2[0] == d2[0]){
                    // if hours are equal, it orders items based on minutes value
                    return s2[1] - d2[1];
                }
                else{
                    // orders items based on hour value
                    return s2[0]-d2[0];
                }
            })
        })
    }
    
    /* Check if search params shall be a input string or the id */
    isItID(obj){
        return (obj.id> 0) ? obj.id : obj.label;
    }

    /* Returns time difference between arrival and departure hour in the "? h ? min" format */
    timeDifCalc(arrivHour, depHour) {

        // 12:30:00 -> 12, 30
        let arrivalTime = arrivHour.split(":",2);
        let departureTime = depHour.split(":",2);

        // set date format A
        let arrivaltoDate = new Date();
        arrivaltoDate.setHours(arrivalTime[0],arrivalTime[1],0);

        // set date format B
        let departuretoDate = new Date();
        departuretoDate.setHours(departureTime[0],departureTime[1],0);

        // dates difference -> Miliseconds result
        let resH = arrivaltoDate.getTime() - departuretoDate.getTime();

        // if the arrival hour is in the next day 
        if(resH < 0){
            resH = resH + 86400000;
        }

        // convert to hours and minutes & return res with string format
        let finalH = Math.floor((resH / (1000 * 60 * 60)) % 24);
        let finalMin = Math.floor((resH / (1000 * 60)) % 60) + 1;

        if(finalH > 0){
            return finalH + " h " + finalMin + " min";
        }
        else{
            if(finalH < 0){
                return finalMin + " min";
            }
            else{
                return finalMin + " min";
            }
        }
    }

    /* Go back to previous screen */
    back() {
        if (this.actionBarBack) {
          this._router.navigate([this.actionBarBack]);
        } else {
          this._routerExtensions.back();
        }
      }

    /* Moves to route detail page: the data is sent with item.route param */
    passDetailProps(item){
        localStorage.setItem("paragem", JSON.stringify(item));
        console.log('Item stops: ' + item.stops);
    }
}