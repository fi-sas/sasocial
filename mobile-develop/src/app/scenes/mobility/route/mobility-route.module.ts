import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NgShadowModule } from 'nativescript-ng-shadow';
import { TranslateModule } from '@ngx-translate/core';
import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';
import { ButtonDirectiveModule } from '~/app/directives/button/button.module';
import { MobilityRouteComponent } from './mobility-route.component';
import { Translate } from '~/app/shared/translate.service';
import { MobilityRouteRoutingModule } from './mobility-route-routing.module';
import { isAndroid } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
    imports: [ 
        NativeScriptCommonModule,
		NativeScriptFormsModule,
		NgShadowModule,
		TranslateModule,
		ButtonDirectiveModule,
        MobilityRouteRoutingModule 
    ],
    declarations: [
        MobilityRouteComponent
     ],
	schemas: [NO_ERRORS_SCHEMA]
})
export class MobilityRouteModule { 
  /**
	 * Constructor
	 * @param {Translate} private _translate
	 */
	constructor(
		private page: Page,
		private _translate: Translate
		) {
			if (isAndroid) {
				this.page.actionBarHidden = true;
			}
			this._translate.load(PT_TRANSLATION, "pt");
			this._translate.load(EN_TRANSLATION, "en");
		}
}