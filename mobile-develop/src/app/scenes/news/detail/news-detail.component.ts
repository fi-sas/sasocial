import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Config } from "~/app/shared/storage/storage";
import { StorageService } from "~/app/shared/storage/storage.service";
import { NewsService } from "~/app/shared/services/news.service";
/**
 * Component
 */
@Component({
  selector: "NewsDetail",
  moduleId: module.id,
  templateUrl: "./news-detail.component.html",
  styleUrls: ["./news-detail.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class NewsDetailComponent implements OnInit, OnDestroy {
  /**
   * Public properties
   */
  public locale: string;
  public newsId: number;
  public newsDetail: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {ActivatedRoute} private _activatedRoute
   * @param {StorageService} private _storageService
   * @param {NewsService}    private _newsService
   * @param {Page}           private _page
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _storageService: StorageService,
    private _newsService: NewsService,
    private _page: Page
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    /* language */
    const configStorage: Config = this._storageService.get("config");
    const langAcronym = configStorage.language.acronym;
    this.locale = langAcronym + "-" + langAcronym.toUpperCase();

    /* route params */
    this._activatedRoute.params.forEach(params => {
      this.newsId = params["id"];
    });

    /* news detail */
    if (this._newsService.news.length) {
      this.newsDetail = this._newsService.findItem(this.newsId);
    } else {
      this._newsService
        .getNews(10, 0)
        .pipe(takeUntil(this._subject))
        .subscribe(data => {
          this.newsDetail = this._newsService.findItem(this.newsId);
        });
    }

    this._page.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }
}
