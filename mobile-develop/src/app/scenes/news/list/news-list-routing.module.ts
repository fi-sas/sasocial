import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { NewsListComponent } from "./news-list.component";

const routes: Routes = [
  /* default: news / list */
  {
    path: "",
    component: NewsListComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class NewsListRoutingModule {}
