import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";
import { NewsService } from "~/app/shared/services/news.service";

/**
 * Component
 */
@Component({
  selector: "NewsList",
  moduleId: module.id,
  templateUrl: "./news-list.component.html",
  styleUrls: ["./news-list.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class NewsListComponent implements OnInit, OnDestroy {
  /**
   * Public properties
   */
  public isLoading = false;
  public locale: string;
  public news: any;

  public total = 0;
  public maxTotalLimit = 200;
  public limit = 10;
  public offset = 0;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {NewsService}    private _newsService
   * @param {StorageService} private _storageService
   * @param {Page}           private _page
   */
  constructor(
    private _newsService: NewsService,
    private _storageService: StorageService,
    private _page: Page
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    /* language */
    const configStorage: Config = this._storageService.get("config");
    const langAcronym = configStorage.language.acronym;
    this.locale = langAcronym + "-" + langAcronym.toUpperCase();

    /* news */
    this.news = [];
    this.getNews();

    this._page.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Get more movements
   */
  getNews() {
    this.isLoading = true;

    if (
      this.offset + this.limit < this.total &&
      this.offset + this.limit < this.maxTotalLimit
    ) {
      this.offset = this.offset + this.limit;
    }

    /* movements */
    this._newsService
      .getNews(this.limit, this.offset)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        const moreNews = res.data;

        this.news = this.news.concat(moreNews);
        this.total = res.link.total;

        this.isLoading = false;
      });
  }
}
