import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { NewsComponent } from "./news.component";

const routes: Routes = [
  /* news / list */
  {
    path: "list",
    component: NewsComponent,
    loadChildren: "~/app/scenes/news/list/news-list.module#NewsListModule"
  },

  /* news / detail */
  {
    path: "detail/:id",
    component: NewsComponent,
    loadChildren: "~/app/scenes/news/detail/news-detail.module#NewsDetailModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class NewsRoutingModule {}
