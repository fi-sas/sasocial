import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpClient } from "@angular/common/http";

/* Component */
@Component({
  selector: "PostDetail",
  moduleId: module.id,
  templateUrl: "./post-detail.component.html",
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  /* public props */
  public postID = null
  public postDetailUrl = 'https://jsonplaceholder.typicode.com/posts/:id';
  public postDetail:any = null;

  /* constructor */
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions
  ) {}

  /* on init */
  ngOnInit() {

    this.route.params.subscribe(params => {
      this.postID = params['id'];
      this.http.get(this.postDetailUrl.replace(':id', this.postID)).subscribe((res:any) => {
        this.postDetail = res;
      });
    });

  }

  /* go back */
  goBack() {
    this.routerExtensions.backToPreviousPage();
  }
}
