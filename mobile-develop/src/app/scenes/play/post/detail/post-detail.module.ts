import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { PostDetailRoutingModule } from "./post-detail-routing.module";
import { PostDetailComponent } from "./post-detail.component";

@NgModule({
    imports: [
        HttpClientModule,
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        PostDetailRoutingModule
    ],
    declarations: [
        PostDetailComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PostDetailModule { }
