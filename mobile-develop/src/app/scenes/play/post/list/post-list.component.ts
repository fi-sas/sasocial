import { Component, OnInit, ViewEncapsulation } from "@angular/core";

import { ResourceService } from "../../../../shared/resource.service";
import { Configurator } from "../../../../shared/config/configurator.service";
import { UrlService } from "../../../../shared/url.service";
import { StorageService } from "../../../../shared/storage/storage.service";
import { Translate } from "../../../../shared/translate.service";

/**
 * Component
 */
@Component({
    selector: "PostList",
    moduleId: module.id,
    templateUrl: "./post-list.component.html",
    styleUrls: ['./post-list.component.scss'],
    encapsulation: ViewEncapsulation.Native
})
export class PostListComponent implements OnInit {

    /**
     * Public proprities
     */
    public lang = null;
	public postsUrl = 'https://jsonplaceholder.typicode.com/posts';
	public posts:any = null;

   /**
    * Constructor
    * @param {ResourceService} private _resource     [description]
    * @param {Configurator}    private _config       [description]
    * @param {UrlService}      private _urlService   [description]
    * @param {StorageService}  private _storage      [description]
    * @param {Translate}       private _translate    [description]
    * @param {[type]}                  [description]
    */
    constructor(
        private _resource: ResourceService,
        private _config: Configurator,
        private _urlService: UrlService,
        private _storage: StorageService,
        private _translate: Translate,
    ) {}

    /**
     * On init
     */
    ngOnInit(): void {

        this.lang = this._config.getOption('DEFAULT_LANG');
        const url = this._urlService.get('ALIMENTATION.CATEGORIES', {service_id: 2});

        if (this._storage.has('key1')) {
            this._storage.set('key1', 'Segunda vez');
        } else {
            this._storage.set('key1', 'Primeira vez');
        }

        this._resource.request('GET', this.postsUrl).subscribe((res:any) => {
            this.posts = res;
        });
    }

    /**
     * Change language
     */
    changeLanguage() {
        this.lang = this.lang === 'pt' ? 'en' : 'pt';
        this._translate.setLanguage(this.lang);
    }

}
