import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { TranslateModule } from '@ngx-translate/core';

import { PostListRoutingModule } from "./post-list-routing.module";
import { PostListComponent } from "./post-list.component";

import { Translate } from "../../../../shared/translate.service";

const PT_TRANSLATION = require('./pt.translation.json');
const EN_TRANSLATION = require('./en.translation.json');

@NgModule({
  imports: [
    HttpClientModule,
    NativeScriptCommonModule,
    NativeScriptUIListViewModule,
    TranslateModule,
    PostListRoutingModule
  ],
  declarations: [
    PostListComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class PostListModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, 'pt');
    this._translate.load(EN_TRANSLATION, 'en');
  }
}
