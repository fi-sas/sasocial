import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AllergensComponent } from "./allergens.component";

const routes: Routes = [

  /* default: start-up / languages */
  {
    path: "",
    component: AllergensComponent,
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [
    NativeScriptRouterModule
  ]
})
export class AllergensRoutingModule {}