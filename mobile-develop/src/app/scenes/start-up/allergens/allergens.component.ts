import { Component, OnInit, ViewEncapsulation, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { TranslateService } from '@ngx-translate/core';
import { Subject, of, forkJoin } from 'rxjs';
import { takeUntil, catchError, flatMap } from 'rxjs/operators';
import * as dialogs  from 'tns-core-modules/ui/dialogs';
import { findKey, cloneDeep } from 'lodash';

import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Config, User } from "~/app/shared/storage/storage";

import { AllergensService, AllergensInterface } from "./allergens.service";

/**
 * Component
 */
@Component({
  selector: "Allergens",
  moduleId: module.id,
  templateUrl: "./allergens.component.html",
  styleUrls: ['./allergens.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class AllergensComponent implements OnInit, OnDestroy {
  institution = '';
  /**
   * Public proprities
   */
  public isLoading = true;
  public configStorage: Config;
  public userStorage: User;
  public allergens: AllergensInterface[];

  public isEnabled = false;

  /* Private */
  private _subject = new Subject<boolean>();
  private translations = {
    error : {
      title: null,
      message: null,
      button: null
    }
  }

  /**
   * Constructor
   * @param {Router}           private _router
   * @param {StorageService}   private _storageService
   * @param {AllergensService} private _allergensService
   * @param {TranslateService} private _translateService
   * @param {Page}             private _page
   */
  constructor(
    private _router: Router,
    private _storageService: StorageService,
    private _allergensService: AllergensService,
    private _translateService: TranslateService,
    private _page: Page
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {

    this.loadTranslations();
  
    this.configStorage = this._storageService.get('config');
    this.userStorage = this._storageService.get('user');

    this.institution = this.configStorage.institution.acronym;

    this._allergensService.getAllergens().pipe(
      takeUntil(this._subject),
      catchError(err => {
        dialogs.alert({
          title: this.translations.error.title,
          message: this.translations.error.message,
          okButtonText: this.translations.error.button
        }).then(function() {
          // code here
        });
        return of([])
      }),
    ).subscribe((data) => {
      this.allergens = data;
    });

    this._page.on('unloaded', (data) => {
      //this.ngOnDestroy();
    });

  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    this._subject.next(true);
    this._subject.unsubscribe();

  }

  /**
   * load translations
   */
  loadTranslations() {

    forkJoin(
      this._translateService.get('ALLERGENS.ERROR.TITLE'),
      this._translateService.get('ALLERGENS.ERROR.MESSAGE'),
      this._translateService.get('ALLERGENS.ERROR.OK_BUTTON_TEXT'),
    ).pipe(
      takeUntil(this._subject),
      flatMap(trans => {

        this.translations.error.title = trans[0];
        this.translations.error.message = trans[1];
        this.translations.error.button = trans[2];

        return of(trans);
      })
    ).subscribe();

  }

  /**
   * Set user allergens
   */
  /**
   * Allergens checked
   * @param {number} id
   */
  allergensChecked(id:number) {

    if (!this.isLoading) {

      const index = findKey(this.allergens, item => item["id"] === id);
      this.allergens[index]['allergic'] = (!this.allergens[index]['allergic']);

      const allergens = [];
      this.allergens.map(item => {
        if (item.allergic) {
          allergens.push(item.id)
        }
      });

      this._allergensService.setAllergens(allergens).pipe(
        takeUntil(this._subject),
        catchError(err => {
          dialogs.alert({
            title: this.translations.error.title,
            message: this.translations.error.message,
            okButtonText: this.translations.error.button
          }).then(function() {
            // code here
          });
          return of([])
        }),
      ).subscribe();

    }

  }
  /**
   * Last allergen in *ngFor="..."
   * @param {AllergensInterface} item
   */
  lastAllergen(item:AllergensInterface) {

    if (this.isLoading) {
      this.isLoading = false;
    }

  }

  /**
   * Go to dashboard
   */
  goToDashboard() {

    this.userStorage.allergens = this.allergens;
    this._storageService.set('user', this.userStorage);

    this.configStorage.allergens.done = true;
    this._storageService.set('config', this.configStorage);

    this._router.navigate(["dashboard"]);

  }

}
