import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from '@ngx-translate/core';

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { ActionBarModule } from '~/app/components/action-bar/action-bar.module';
import { Translate } from "~/app/shared/translate.service";

import { AllergensRoutingModule } from "./allergens-routing.module";
import { AllergensService } from "./allergens.service";
import { AllergensComponent } from "./allergens.component";

const PT_TRANSLATION = require('./pt.translation.json');
const EN_TRANSLATION = require('./en.translation.json');

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    AllergensRoutingModule
  ],
  declarations: [
    AllergensComponent
  ],
  providers: [
    {
      provide: AllergensService,
      useClass: AllergensService,
      deps: [UrlService, ResourceService, StorageService]
    }
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AllergensModule {

  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {

    this._translate.load(PT_TRANSLATION, 'pt');
    this._translate.load(EN_TRANSLATION, 'en');

  }

}
