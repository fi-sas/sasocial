import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Interface
 */
export interface AllergensInterface {
  id: number;
  name?: string;
  description?: string;
  allergic: boolean;
  translations?: AllergenTranslation[];
}

interface AllergenTranslation {
  language_id: number;
  name: string;
  description: string;
}

@Injectable()
export class AllergensService {
  /**
   * Private properties
   */
  private _config: Config;
  private _lang: string;
  private _authData = null;
  public _configStorage: Config;

  /**
   * Constructor
   * @param {UrlService}      private _urlService
   * @param {ResourceService} private _resourceService
   * @param {StorageService}  private _storageService
   */
  constructor(
    private _urlService: UrlService,
    private _resourceService: ResourceService,
    private _storageService: StorageService
  ) {
    this.init();
  }

  /**
   * Init
   */
  init() {
    this._configStorage = this._storageService.get("config");
    this._lang = this._configStorage.language.acronym;
  }

  /**
   * Get user allergens by token
   */
  getAllergens() {
    this.init();
    const url = this._urlService.get("ALIMENTATION.ALERGENS.READ");
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    return this._resourceService
      .list<AllergensInterface>(url, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
      })
      .pipe(
        timeout(30000),
        catchError(err => of([])),
        map((res: any) => {
          //if(res.data && res.data.length > 0){
          res.data.map(item => {
            item["name"] = item.translations[0].name;
            item["description"] = item.translations[0].description;
            delete item.translations;
          });
          return res.data;
        //}
        })
      );
  }

  /**
   * Set user allergens by token
   */
  setAllergens(allergens: number[]) {
    this.init();
    const data = { allergens: allergens };
    const url = this._urlService.get("ALIMENTATION.ALERGENS.UPDATE");
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    return this._resourceService
      .update<any>(url, data, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
      })
      .pipe(
        timeout(30000),
        catchError(err => of([])),
        map((res: any) => {
          // if(res.data && res.data.length > 0){
          res.data.map(item => {
            item["name"] = item.translations[0].name;
            item["description"] = item.translations[0].description;
            delete item.translations;
          });

          return res.data;
        //}
        })
      );
  }
}
