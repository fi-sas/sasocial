import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { InstitutionsComponent } from "./institutions.component";

const routes: Routes = [

  /* default: start-up / institutions */
  {
    path: "",
    component: InstitutionsComponent,
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [
    NativeScriptRouterModule
  ]
})
export class InstitutionsRoutingModule {}