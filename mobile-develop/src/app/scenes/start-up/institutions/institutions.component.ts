import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config, Institution, User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "Institutions",
  moduleId: module.id,
  templateUrl: "./institutions.component.html",
  styleUrls: ["./institutions.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class InstitutionsComponent implements OnInit {
  /* Public properties */
  public institutions: Institution[];
  public userStorage: User;
  public config: Config;

  /**
   * Constructor
   * @param {Configurator}    private _config
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   */
  constructor(
    private _config: Configurator,
    private _storage: StorageService,
    private _router: Router
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.config = this._storage.get("config");
    this.institutions = this._config.getOptionTree("INSTITUTIONS")[
      "INSTITUTIONS"
    ];
  }

  /**
   * Select and Set instituition to storage
   */
  selectInstituition(acronym: string) {
    // Remove old
    const userStorage: User = this._storage.get("user");
    userStorage.logged = false;
    this._storage.set("user", userStorage);

    const tokenStorage: any = this._storage.get("device_token");
    tokenStorage.token = null;
    this._storage.set("device_token", tokenStorage);

    // Add new
    const index = findIndex(this.institutions, o => o.acronym === acronym);
    const institution = this.institutions[index];

    this.config.institution.id = institution.id;
    this.config.institution.acronym = acronym;
    this.config.institution.name = institution.name;

    this._storage.set("config", this.config);

    this._router.navigate(["start-up/refectories"]);
  }
}
