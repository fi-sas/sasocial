import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { device } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
    selector: "Languages",
    moduleId: module.id,
    templateUrl: "./languages.component.html",
    styleUrls: ["./languages.component.scss"],
    encapsulation: ViewEncapsulation.Native
})
export class LanguagesComponent implements OnInit {
    /* Public proprities */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public didSelect = false;

    /**
     * Constructor
     * @param {Configurator}    private _config
     * @param {StorageService}  private _storage
     * @param {Router}          private _router
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     */
    constructor(
        private _config: Configurator,
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _translate: Translate
    ) {
        this._page.actionBarHidden = true;
    }

    /* On init */
    ngOnInit(): void {
        this.configStorage = this._storage.get("config");
        this.userStorage = this._storage.get("user");

    if (this.configStorage.language.acronym !== null && this.configStorage.institution.id !== null && this.configStorage.school.id !== null && this.configStorage.refectory.id !== null && 
        this.configStorage.allergens.done && !this.userStorage.logged) {
            this._router.navigate(["login"]);
        } else if (this.configStorage.language.acronym !== null && this.configStorage.institution.id !== null && this.configStorage.school.id !== null &&
        this.configStorage.refectory.id !== null && !this.configStorage.allergens.done && this.userStorage.logged) {
        this._router.navigate(["start-up/allergens"]);
    } else if (this.configStorage.language.acronym !== null &&
        this.configStorage.institution.id !== null && this.configStorage.school.id !== null && this.configStorage.refectory.id !== null && this.configStorage.allergens.done &&
        this.userStorage.logged) {
            let a = this.configStorage.language.acronym;
            let b = this.configStorage.language.name;
            this.selectLanguage(a,b);
            this._router.navigate(["dashboard"]);
    } else {
        this.isLoading = false;
    }

    this.languages = this._config.getOptionTree("LANGUAGES")["LANGUAGES"];
    //this.languageAcronym = this._config.getOption("DEFAULT_LANG");
    this.languageAcronym= this.configStorage.language.acronym;

    const index = findIndex(
        this.languages,
        o => o.acronym === this.languageAcronym
    );
        this.languageName = index !== -1 ? this.languages[index].name : "";

        this._translate.setLanguage(this.languageAcronym);
    }

    /**
     * Select language
     * @param {string} lang language
     * @param {string} name language name
     */
    selectLanguage(acronym: string, name: string): void {
        this.languageAcronym = acronym;
        this.languageName = name;

        this._translate.setLanguage(this.languageAcronym);
        this.didSelect = true;
    }

    /**
     * Set language to storage
     */
    setLanguage(): void {
        this.configStorage.language.acronym = this.languageAcronym;
        this.configStorage.language.name = this.languageName;

        this._storage.set("config", this.configStorage);
        if(this.didSelect == true){
            this._router.navigate(["start-up/institutions"]);
        }
    }
}
