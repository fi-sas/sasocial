import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { RefectoriesComponent } from "./refectories.component";
import { RefectoriesResolver } from "./refectories.resolver";

const routes: Routes = [

  /* default: start-up / refectories */
  {
    path: "",
    component: RefectoriesComponent,
    pathMatch: 'full'

    /* resolver does not work properly (on back): https://github.com/NativeScript/nativescript-angular/issues/665 */
    /*
    ,resolve: {
      data: RefectoriesResolver
    }
    */

  }

];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(routes)
  ],
  exports: [
    NativeScriptRouterModule
  ]
})
export class RefectoriessRoutingModule {}