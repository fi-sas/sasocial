import { Component, OnInit, ViewEncapsulation, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Subject, of, from } from "rxjs";
import {
  takeUntil,
  flatMap,
  map,
  retry,
  concatMap,
  last
} from "rxjs/operators";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Refectory, School, Config } from "~/app/shared/storage/storage";
import { AuthService } from "~/app/shared/auth/auth.service";

import { RefectoriesService } from "./refectories.service";

/**
 * Component
 */
@Component({
  selector: "Refectories",
  moduleId: module.id,
  templateUrl: "./refectories.component.html",
  styleUrls: ["./refectories.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class RefectoriesComponent implements OnInit, OnDestroy {
  /* Public properties */
  public isLoading = true;
  public refectories: Refectory[];
  public config: Config;

  /* Private */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {StorageService}     private _storage
   * @param {Router}             private _router
   * @param {ActivatedRoute}     private _activatedRoute
   * @param {AuthService}        private _auth
   * @param {RefectoriesService} private _refectories
   * @param {Page}               private _page
   */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _auth: AuthService,
    private _refectories: RefectoriesService,
    private _page: Page
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.config = this._storage.get("config");
    const deviceToken: { token: string } = this._storage.get("device_token");

    if (deviceToken.token === null) {
      this.generateToken()
        .pipe(
          takeUntil(this._subject),
          concatMap(() => this.getRefectories()),
          last()
        )
        .subscribe(data => {
          this.refectories = data;
          this.isLoading = false;
        });
    } else {
      this.getRefectories()
        .pipe(takeUntil(this._subject))
        .subscribe(data => {
          this.refectories = data;
          this.isLoading = false;
        });
    }

    this._page.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Generate token
   */
  generateToken() {
    return this._auth.setDeviceToken().pipe(
      retry(5),
      flatMap(authorized => {
        return of([]);
      })
    );
  }

  /**
   * Get refectories
   */
  getRefectories() {
    const refectories = [];

    return this._refectories.getSchools().pipe(
      flatMap((item: any) => from(item)),
      flatMap((item: any) =>
        this._refectories
          .getServices(item.id)
          .pipe(map(val => ({ schools: item, services: val })))
      ),
      flatMap(item => {
        const schools = item.schools;
        const services: any = item.services;

        services.map(o => {
          if (o.type === "canteen") {
            refectories.push({
              id: o.id,
              schoolId: schools.id,
              name: `${o.name}`,
              description: `${schools.name}`,
              schoolName: `${schools.name}`,
              schoolAcronym: `${schools.acronym}`
            });
          }
        });

        return of(refectories);
      })
    );
  }

  /**
   * Select and set refectory (canteen) to storage
   * @param {Refectory} refectory selected canteen
   * @param {School} school selected canteen school
   */
  selectRefectory(refectory: Refectory, school: School) {
    this.config.refectory.id = refectory.id;
    this.config.refectory.schoolId = refectory.schoolId;
    this.config.refectory.name = refectory.name;
    this.config.refectory.description = refectory.description;

    this.config.school.id = school.id;
    this.config.school.name = school.name;
    this.config.school.acronym = school.acronym;

    this._storage.set("config", this.config);

    this._router.navigate(["login"]);
  }
}
