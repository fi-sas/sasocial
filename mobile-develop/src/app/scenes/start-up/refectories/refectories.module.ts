import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { ButtonDirectiveModule } from "~/app/directives/button/button.module";
import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { Translate } from "~/app/shared/translate.service";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { RefectoriessRoutingModule } from "./refectories-routing.module";
import { RefectoriesService } from "./refectories.service";
import { RefectoriesComponent } from "./refectories.component";
import { RefectoriesResolver } from "./refectories.resolver";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    NativeScriptUIListViewModule,
    ButtonDirectiveModule,
    RefectoriessRoutingModule
  ],
  declarations: [RefectoriesComponent],
  providers: [
    {
      provide: RefectoriesService,
      useClass: RefectoriesService,
      deps: [UrlService, ResourceService, StorageService]
    }

    /* resolver does not work properly (on back): https://github.com/NativeScript/nativescript-angular/issues/665 */
    /*
    ,{
      provide: RefectoriesResolver,
      useClass: RefectoriesResolver,
      deps: [AuthService, RefectoriesService]
    }
    */
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RefectoriesModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
