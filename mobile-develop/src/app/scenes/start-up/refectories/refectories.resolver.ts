import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve
} from "@angular/router";
import { of, from } from "rxjs";
import { flatMap, map, retry, concat, last } from "rxjs/operators";

import { AuthService } from "~/app/shared/auth/auth.service";

import { RefectoriesService } from "./refectories.service";

export class RefectoriesResolver implements Resolve<any> {
  /**
   * Constructor
   * @param {AuthService}        private _auth
   * @param {RefectoriesService} private _refectories
   */
  constructor(
    private _auth: AuthService,
    private _refectories: RefectoriesService
  ) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot}    state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.generateToken().pipe(
      concat(this.getRefectories()),
      last()
    );
  }

  /**
   * Generate token
   */
  generateToken() {
    return this._auth.setDeviceToken().pipe(
      retry(5),
      flatMap(authorized => {
        return of([]);
      })
    );
  }

  /**
   * Get refectories
   */
  getRefectories() {
    const refectories = [];

    return this._refectories.getSchools().pipe(
      flatMap((item: any) => from(item)),
      flatMap((item: any) =>
        this._refectories
          .getServices(item.id)
          .pipe(map(val => ({ schools: item, services: val })))
      ),
      flatMap(item => {
        const schools = item.schools;
        const services: any = item.services;

        services.map(o => {
          if (o.type === "canteen") {
            refectories.push({
              id: o.id,
              schoolId: schools.id,
              name: `${o.name}`,
              description: `${schools.name}`
            });
          }
        });

        return of(refectories);
      })
    );
  }
}
