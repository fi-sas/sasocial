import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

/* school interface */
interface SchoolInterface {
  id: number;
  name: string;
  acronym: string;
  isDefault: boolean;
  active: boolean;
}

/* service interface */
interface ServiceInterface {
  id: number;
  active: true;
  name: string;
  type: string;
  maintenance: boolean;
  created_at: string;
  updated_at: string;
}

@Injectable()
export class RefectoriesService {
  /**
   * Private properties
   */
  private _config: Config;
  private _lang: string;
  private _authData = null;
  public _configStorage: Config;

  private selectedSchool = null;
  private selectedService = null;

  /**
   * Constructor
   * @param {UrlService}      private _urlService
   * @param {ResourceService} private _resourceService
   * @param {StorageService}  private _storageService
   */
  constructor(
    private _urlService: UrlService,
    private _resourceService: ResourceService,
    private _storageService: StorageService
  ) {
    this.init();
  }

  /**
   * Init
   */
  init() {
    this._configStorage = this._storageService.get("config");
    this._lang = this._configStorage.language.acronym;
  }

  /**
   * Get schools
   */
  getSchools() {
    this.init();
    const url = this._urlService.get("ALIMENTATION.SCHOOLS");
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    return this._resourceService
      .list<SchoolInterface>(url, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Get selected school
   */
  getSchool() {
    return this.selectedSchool;
  }

  /**
   * Set selected school
   * @param {SchoolInterface} obj selected school
   */
  setSchool(obj: SchoolInterface) {
    this.selectedSchool = obj;
  }

  /**
   * Get services by school
   */
  getServices(school_id) {
    this.init();
    const url = this._urlService.get("ALIMENTATION.SERVICES", {
      school_id: school_id
    });

    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    return this._resourceService
      .list<ServiceInterface>(url, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
      })
      .pipe(
        map(resp => {
          return resp.data;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Get selected service
   */
  getService() {
    return this.selectedService;
  }

  /**
   * Set selected service
   * @param {ServiceInterface} obj selected service
   */
  setService(obj: ServiceInterface) {
    this.selectedService = obj;
  }
}
