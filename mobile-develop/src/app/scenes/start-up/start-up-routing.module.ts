import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { StartUpComponent } from "./start-up.component";

const routes: Routes = [

    /* start-up / languages */
    {
      path: 'languages',
      component: StartUpComponent,
      loadChildren: '~/app/scenes/start-up/languages/languages.module#LanguagesModule'
    },

    /* start-up / institutions */
    {
      path: 'institutions',
      component: StartUpComponent,
      loadChildren: '~/app/scenes/start-up/institutions/institutions.module#InstitutionsModule'
    },

    /* start-up / refectories */
    {
      path: 'refectories',
      component: StartUpComponent,
      loadChildren: '~/app/scenes/start-up/refectories/refectories.module#RefectoriesModule'
    },

    /* start-up / allergens */
    {
      path: 'allergens',
      component: StartUpComponent,
      loadChildren: '~/app/scenes/start-up/allergens/allergens.module#AllergensModule'
    }

  ];


@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class StartUpRoutingModule { }
