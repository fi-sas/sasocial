import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { TicketListComponent } from "./ticket-list.component";

const routes: Routes = [
  /* default: Ticket / list */
  {
    path: "",
    component: TicketListComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class TicketListRoutingModule {}
