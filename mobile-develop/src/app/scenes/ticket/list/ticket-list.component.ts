import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

import { MealsService } from "~/app/shared/services/meals.service";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "TicketList",
  moduleId: module.id,
  templateUrl: "./ticket-list.component.html",
  styleUrls: ["./ticket-list.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class TicketListComponent implements OnInit, OnDestroy {
  /**
   * Public properties
   */
  public configStorage: Config;
  public institution: string;
  public isLoading = true;

  public reservations: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {MealsService}    private _mealsService
   * @param {StorageService}  private _storage
   */
  constructor(
    private _mealsService: MealsService,
    private _storage: StorageService,
    private _modalService: FooterModalService
  ) {}

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;

    this._mealsService
      .getReservations()
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.reservations = res;
        this.isLoading = false;
      });

      this._mealsService.emitter
      .asObservable()
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.reservations = this._mealsService.reservations;
        this.isLoading = false;
      });
  }

  check(item){
    if(item.menu_dish.nullable_until){
      let nullable = new Date(item.menu_dish.nullable_until).getTime();
      var now = new Date().getTime();
      if(nullable - now > 0){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return true;
    }
  }

  cancelTicket(item){
    let resp = this.check(item);
    if (resp == true){
      this._modalService.setTicketModal2(item);
      this._modalService.toogleModal(true);
    }
    else{
      console.log('Hora excedida');
    }    
  }

  /**
   * On destroy
   */
  ngOnDestroy() {}
}
