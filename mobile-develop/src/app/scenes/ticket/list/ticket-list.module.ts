import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgShadowModule } from "nativescript-ng-shadow";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { ButtonDirectiveModule } from "~/app/directives/button/button.module";
import { CanteenTicketModule } from "~/app/components/canteen-ticket/canteen-ticket.module";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { MealsService } from "~/app/shared/services/meals.service";

import { TicketListRoutingModule } from "./ticket-list-routing.module";
import { TicketListComponent } from "./ticket-list.component";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NgShadowModule,
    TranslateModule,
    ActionBarModule,
    FooterModalModule,
    CanteenTicketModule,
    ButtonDirectiveModule,
    TicketListRoutingModule
  ],
  declarations: [TicketListComponent],
  providers: [
    {
      provide: MealsService,
      useClass: MealsService,
      deps: [UrlService, ResourceService, StorageService]
    },
    {
      provide: FooterModalService,
      useClass: FooterModalService,
      deps: []
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TicketListModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
