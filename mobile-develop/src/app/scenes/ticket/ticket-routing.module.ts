import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { TicketComponent } from "./ticket.component";

const routes: Routes = [
  /* Ticket / list */
  {
    path: "list",
    component: TicketComponent,
    loadChildren: "~/app/scenes/ticket/list/ticket-list.module#TicketListModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class TicketRoutingModule {}
