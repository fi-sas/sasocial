import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";
import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { TicketRoutingModule } from "./ticket-routing.module";
import { TicketComponent } from "./ticket.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, TicketRoutingModule],
  declarations: [TicketComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TicketModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
