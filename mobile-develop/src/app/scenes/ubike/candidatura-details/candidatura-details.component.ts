import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { device } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/**
 * Component
 */
@Component({
  selector: "CandidaturaDetails",
  moduleId: module.id,
  templateUrl: "./candidatura-details.component.html",
  styleUrls: ["./candidatura-details.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class CandidaturaDetailsComponent implements OnInit {
  /**
   * Public proprities
   */
  public isLoading = true;
  public languages: Language[];
  public languageAcronym: string;
  public languageName: string;
  public configStorage: Config;
  public userStorage: User;
  public institution: string;

  public limit = -1;
  public offset = 0;

  public useCar;

  //DATA

  public data: any;

  /**
   * Private proprities
   */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {Configurator}    private _config
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {Page}            private _page
   * @param {Translate}       private _translate
   * @param {FooterModalService}      private _modalService
   */
  constructor(
    private _config: Configurator,
    private _storage: StorageService,
    private _router: Router,
    private _page: Page,
    private _translate: Translate,
    private _ubikeService: UbikeService,
    private _modalService: FooterModalService
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
    this.getCandidatures();
  }

  getCandidatures() {
    this._ubikeService
      .getCandidature(this.limit, this.offset)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.data = res.data[0];
        this.isLoading = false;
      });
  }
  openModal() {
    this._modalService.setUbikeModal(this.data.id); 
    this._modalService.toogleModal(true);
  }
}
