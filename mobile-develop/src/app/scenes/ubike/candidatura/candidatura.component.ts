import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { parseJwt } from "~/app/shared/utilities/string-utilities";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";

/**
 * Component
 */
@Component({
    selector: "Candidatura",
    moduleId: module.id,
    templateUrl: "./candidatura.component.html",
    styleUrls: ["./candidatura.component.scss"],
    encapsulation: ViewEncapsulation.Native
})
export class CandidaturaComponent implements OnInit {
    
    /* Public proprities */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public institution: string;

    //DATA
    public carBrand: any = null;
    public carModel: any = null;
    public carPower: any = null;
    public carYear: any = null;
    public dailyCommute: any = null;
    public dailyConsumption: any;
    public dailyKms: any = null;
    public height: any = null;
    public weight: any = null;
    public startDate: any = null;
    public endDate: any = null;
    public transportType: any = null;
    public fuelType: any = null;
    public bikeType: any = null;
    public today = null;
    public yesterday = null;
    public firstOption = null;
    public secondOption = false;
    public resHeight: any = null;
    public resWeight: any = null;
    public resKm: any = null;

    public userAddress = null;
    public userBirthDate = null;
    public userPostalCode = null;

    public terms: any = null;
    public data;

    public useCar = false;

    private _subject = new Subject<boolean>();
    private _authData = null;

    public error = false;

    /**
     * Constructor
     * @param {Configurator}    private _config
     * @param {StorageService}  private _storage
     * @param {Router}          private _router
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     * @param {UbikeService}    private _ubikeService
     */
    constructor(
        private _config: Configurator,
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _translate: Translate,
        private _ubikeService: UbikeService,
    ) {
        this._page.actionBarHidden = true;
    }

    /* On init */
    ngOnInit(): void {
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;

        this.today = new Date();
        this.yesterday = new Date(this.today);
        this.yesterday.setDate(this.today.getDate() - 1);
    }

    onFocusClear() {
        //
    }
    sendCandidature() {
        const validate = this.validateForm();

        if (!validate) {
            this.error = true;
            return;
        }


        this._authData = this._storage.get<{ token: string }>("device_token");

        const token = parseJwt(this._authData.token);

        if (token.user.address === null) {
            this.userAddress = "Morada ND";
        } else {
            this.userAddress = token.user.address;
        }

        if (token.user.birth_date === null) {
            this.userBirthDate = "1900-01-01T00:00:00.000Z";
        } else {
            this.userBirthDate = token.user.birth_date;
        }

        if (token.user.postal_code === null) {
            this.userPostalCode = "0000-000";
        } else {
            this.userPostalCode = token.user.address;
        }

        if(this.height.includes(',')){
            this.resHeight = this.height.replace(",",".");
        }
        else{
            this.resHeight = this.height;
        }

        if(this.weight.includes(',')){
            this.resWeight = this.weight.replace(",",".");
        }
        else{
            this.resWeight = this.weight;
        }

        if(this.dailyKms.includes(',')){
            this.resKm = this.dailyKms.replace(",",".");
        }
        else{
            this.resKm = this.dailyKms;
        }

        this.data = {
            address: this.userAddress,
            applicant_consent: this.terms,
            birth_date: this.userBirthDate,
            // consent_date: this.yesterday,
            daily_commute: this.transportType,
            email: token.user.email,
            end_date: this.endDate,
            full_name: token.user.name,
            gender: token.user.gender,
            height: this.resHeight,
            phone_number: token.user.phone,
            postal_code: this.userPostalCode,
            start_date: this.startDate,
            student_number: token.user.student_number,
            typology_first_preference: this.firstOption,
            other_typology_preference: this.secondOption,
            user_id: token.user.id,
            weight: this.resWeight,
            daily_kms: this.resKm,
            //CAR
            engine_power: this.carPower,
            daily_consumption: this.fuelType,
            manufacturer: this.carBrand,
            model: this.carModel,
            year_manufactured: this.carYear
        };
        
        this._ubikeService
            .sendCandidature(this.data)
            .pipe(takeUntil(this._subject))
            .subscribe(() => {
                this._router.navigate(["ubike/dashboard/"]);
            });
    }   

    onCheckedChangeTransport(value) {
        if (this.transportType != null) {
            this.transportType = null;
            this.usingCar();
        } else {
            this.transportType = value;
            this.usingCar();
        }
    }

    onCheckedChangeFuel(value) {
        if (this.fuelType != null) {
            this.fuelType = null;
        } else {
            this.fuelType = value;
        }
    }

    onCheckedChangeBike(value) {
        if (this.bikeType != null) {
            this.bikeType = null;
        } else {
            this.bikeType = value;
            this.firstOption = value;
        }
    }

    onCheckedCheckSecond() {
        this.secondOption = true;
    }
    onCheckedChangeTerms(value) {
        if (this.terms != null) {
            this.terms = null;
        } else {
            this.terms = value;
        }
    }

    usingCar() {
        if (this.transportType === "Car" || this.transportType === "Motorcycle") {
            this.useCar = true;
        } else {
            this.useCar = false;
        }
    }

    pickStartDate(): void {
        const picker = new ModalDatetimepicker();

        picker.pickDate({
            title: "",
            theme: "dark",
            minDate: new Date(),
            is24HourView: false
        })
        .then(result => {
            this.startDate = result["year"] + "-" + result["month"] + "-" + result["day"];
        })
        .catch(error => { });
    }
    pickEndDate(): void {
        const picker = new ModalDatetimepicker();

    picker.pickDate({
            title: "",
            theme: "dark",
            minDate: new Date(),
            is24HourView: false
        })
    .then(result => {
        this.endDate = result["year"] + "-" + result["month"] + "-" + result["day"];
    })
        .catch(error => { });
    }

    validateForm() {
        if (!this.useCar) {
            if (
                this.transportType !== null &&
                this.dailyKms !== null &&
                this.bikeType !== null &&
                this.startDate !== null &&
                this.endDate !== null &&
                this.weight !== null &&
                this.height !== null
            ) {
                return true;
            } else {
                return false;
            }
        } else if (this.useCar) {
            if (
                this.transportType !== null &&
                this.carBrand !== null &&
                this.carModel !== null &&
                this.carYear !== null &&
                this.carPower !== null &&
                this.fuelType !== null &&
                this.dailyKms !== null &&
                this.bikeType !== null &&
                this.startDate !== null &&
                this.endDate !== null &&
                this.weight !== null &&
                this.height !== null
            ) {
                return true;
            } else {
                return false;
            }
        }
    }
}
