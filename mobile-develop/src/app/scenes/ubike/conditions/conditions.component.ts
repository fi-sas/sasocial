import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { device } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "Conditions",
  moduleId: module.id,
  templateUrl: "./conditions.component.html",
  styleUrls: ["./conditions.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ConditionsComponent implements OnInit {
  /**
   * Public proprities
   */
  public isLoading = true;
  public languages: Language[];
  public languageAcronym: string;
  public languageName: string;
  public configStorage: Config;
  public userStorage: User;
  public institution: string;

  /**
   * Constructor
   * @param {Configurator}    private _config
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {Page}            private _page
   * @param {Translate}       private _translate
   */
  constructor(
    private _config: Configurator,
    private _storage: StorageService,
    private _router: Router,
    private _page: Page,
    private _translate: Translate
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
  }

  /**
   * Select language
   * @param {string} lang language
   * @param {string} name language name
   */
}
