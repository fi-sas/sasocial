import { Component, OnInit, ViewEncapsulation, OnDestroy, OnChanges, DoCheck } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { FooterModalService } from "~/app/shared/services/footer-modal.service";

/* Component */
@Component({
    selector: "Dashboard",
    moduleId: module.id,
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"],
    encapsulation: ViewEncapsulation.Native
})
export class DashboardComponent implements OnInit, OnDestroy {
    /* Public proprities */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public institution: string;

    public trips: any;
    public total = 0;
    public limit = -1;
    public offset = 0;

    public bikeId;
    public totalKms = "0";
    public totalCo = "0";
    public totalTrips = 0;
    public totalPoints = 0;

    public tab = 2;
    public dashboard: boolean = false;
    public dataHomepage;
    public dataCandidature;
    public allowCandidature = 1;

    public distance: any;

    private _subject = new Subject<boolean>();

    /**
     * Constructor
     * @param {Configurator}    private _config
     * @param {StorageService}  private _storage
     * @param {Router}          private _router
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     * @param {UbikeService}    private _ubikeService
     * @param {FooterModalService}      private _modalService
     */

    constructor(
        private _config: Configurator,
        private _storage: StorageService,
        private _router: Router,
        private _page: Page,
        private _translate: Translate,
        private _ubikeService: UbikeService,
        private _modalService: FooterModalService
    ) { }

    /* On init */
    ngOnInit(): void {
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        this.loadPage();
    }

    loadPage() {
        this.getCandidatures();
        this.trips = [];
        this.getTrips();
    }

    ngOnDestroy() {
        this._subject.next(true);
        this._subject.unsubscribe();
    }

    getCandidatures() {
        this._ubikeService
        .getCandidature(this.limit, this.offset)
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
            if(res && res.data && res.data.length > 0) {
                this.dataCandidature = res.data[0];

                if (this.dataCandidature.status === "contracted" && this.dataCandidature.bike_id !== null) {
                    this.dashboard = true;
                    this.tab = 1;
                    this.allowCandidature = 0;
                } else {
                    if(
                        this.dataCandidature.status === "cancelled" || this.dataCandidature.status === "withdrawal" || this.dataCandidature.status === "closed" || 
                        this.dataCandidature.status === 'unassigned' || this.dataCandidature.status === 'rejected'
                    ){
                    this.allowCandidature = 1;
                    }
                    else{
                        this.allowCandidature = 0;
                        this.tab = 2;
                        this.dashboard = false;
                    }
                }
            }
            this.isLoading = false;
        });
    }

    getTrips() {
        this.isLoading = true;

        this._ubikeService
        .getTrips(this.limit, this.offset)
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
            let co = 0;
            let kms = 0;
            if(res && res.data && res.data.length > 0) {
                this.bikeId = res.data[0].bike_id;

                /* TOTAL NUMBER OF TRIPS */
                this.totalTrips = res.data.length;

                /* TOTAL KMS */
                kms = res.data.reduce((accumulator, currentValue) => accumulator + currentValue.distance, 0); 
                this.totalKms = kms.toFixed(2);
                /* TOTAL CO2 */
                co = res.data.reduce((accumulator, currentValue) => accumulator + currentValue.co2_emissions_saved, 0);
                this.totalCo = co.toFixed(2);
            }

            this.isLoading = false;
        });
    }

    getHomepage() {
        this._ubikeService
        .getHomepage()
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
            if (res && res.data && res.data.length > 0){
                this.dataHomepage = res.data[0].value;
            }
            this.isLoading = false;
        });
    }

    enableTab(tab) {
        this.tab = tab;
    }
    /* Open Modal */
    openModal() {
        this._modalService.setUbikeModal(this.dataCandidature.id);
        this._modalService.toogleModal(true);
    }
}
