import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { UbikecardModule } from "~/app/components/ubikecard/ubikecard.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    DashboardRoutingModule,
    ActionBarModule,
    UbikecardModule,
    FooterModalModule
  ],
  declarations: [DashboardComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
