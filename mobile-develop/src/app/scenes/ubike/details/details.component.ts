import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Configurator } from "~/app/shared/config/configurator.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { Page } from "tns-core-modules/ui/page";
import { Router } from "@angular/router";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Translate } from "~/app/shared/translate.service";
import { UbikeService } from "~/app/shared/services/ubike.service";
import * as moment from "moment";

/**
 * Component
 */
@Component({
  selector: "Details",
  moduleId: module.id,
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class DetailsComponent implements OnInit {
  /**
   * Public proprities
   */
  public isLoading = true;
  public languages: Language[];
  public languageAcronym: string;
  public languageName: string;
  public configStorage: Config;
  public userStorage: User;
  public institution: string;

  public month = moment();

  public trips: any;
  public total = 0;
  public limit = -1;
  public offset = 0;

  public tripsList: any;

  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {StorageService}  private _storage
   * @param {Page}            private _page
   * @param {Translate}       private _translate
   * @param {UbikeService}    private _ubikeService
   */
  constructor(
    private _storage: StorageService,
    private _page: Page,
    private _translate: Translate,
    private _ubikeService: UbikeService
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
    this.languageAcronym = this.configStorage.language.acronym;

    /* GET TODAY MONTH */
    this.month.locale(this.languageAcronym);
    this.getData(
      this.month.startOf("month").format("YYYY-MM-DD"),
      this.month.endOf("month").format("YYYY-MM-DD")
    );
  }

  showDetails(item){
    console.log('DISTANCIA: ' + item.distance + 'km' + ' e TEMPO DE: ' + item.time + 'min.');
  }

  next() {
    this.isLoading = true;
    this.month = this.month.add("1", "month");
    this.getData(
      this.month.startOf("month").format("YYYY-MM-DD"),
      this.month.endOf("month").format("YYYY-MM-DD")
    );
  }
  prev() {
    this.isLoading = true;
    this.month = this.month.subtract("1", "month");
    this.getData(
      this.month.startOf("month").format("YYYY-MM-DD"),
      this.month.endOf("month").format("YYYY-MM-DD")
    );
  }

  getData(start, end) {
    this._ubikeService
      .getTripsMonth(this.limit, this.offset, start, end)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.tripsList = res.data;
        this.isLoading = false;
      });
  }
}
