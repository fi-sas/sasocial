import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MaintenanceComponent } from "./maintenance.component";
import { UbikeService } from "~/app/shared/services/ubike.service";

const routes: Routes = [
  {
    path: "",
    component: MaintenanceComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule],
  providers: [UbikeService]
})
export class MaintenanceRoutingModule {}
