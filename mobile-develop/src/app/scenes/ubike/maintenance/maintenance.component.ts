import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { parseJwt } from "~/app/shared/utilities/string-utilities";

/**
 * Component
 */
@Component({
  selector: "Maintenance",
  moduleId: module.id,
  templateUrl: "./maintenance.component.html",
  styleUrls: ["./maintenance.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class MaintenanceComponent implements OnInit {
  /**
   * Public proprities
   */
  public isLoading = true;
  public languages: Language[];
  public languageAcronym: string;
  public languageName: string;
  public configStorage: Config;
  public userStorage: User;
  public institution: string;

  public limit = -1;
  public offset = 0;
  public data: any;
  public description: any;
  public bikeID: any;
  public today = null;
  public yesterday = null;

  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {StorageService}  private _storage
   * @param {Page}            private _page
   * @param {Translate}       private _translate
   * @param {UbikeService}    private _ubikeService
   * @param {ActivatedRoute}  private _activatedRoute
   * @param {Router}          private _router

   */
  constructor(
    private _storage: StorageService,
    private _page: Page,
    private _translate: Translate,
    private _ubikeService: UbikeService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.institution = this.configStorage.institution.acronym;
    this.languageAcronym = this.configStorage.language.acronym;
    this.getCandidatures();
  }
  getCandidatures() {
    this._ubikeService
      .getCandidature(this.limit, this.offset)
      .pipe(takeUntil(this._subject))
      .subscribe((res: any) => {
        this.data = res.data[0];
        this.bikeID = this.data.bike_id;
        this.isLoading = false;
      });
  }

  sendMaintenance() {
    let isValid = this.validateForm();

    if(isValid){
      this.today = new Date();
      this.yesterday = new Date(this.today);
      this.yesterday.setDate(this.today.getDate() - 1);

      const data = {
        subject: "MAINTENANCE_REQUEST",
        request_description: this.description,
        begin_date: this.yesterday
      };

      this._ubikeService
        .sendMaintenance(data)
        .pipe(takeUntil(this._subject))
        .subscribe(() => {
          this._router.navigate(["ubike/dashboard/"]).then(() => {
            // alert box with success message
            switch(this.languageAcronym){
              case 'pt':
                alert({title: "UBike", message: 'O pedido de manutenção foi enviado!', okButtonText: "OK"});
                break;
              case 'en':
                alert({title: "UBike", message: 'Your maintenance request was sent!', okButtonText: "OK"});
                break;
              default:
                alert({title: "UBike", message: 'O pedido de manutenção foi enviado!', okButtonText: "OK"});
                break;
            }
          })
        });
    }
  }

  validateForm(){
    if(this.description !== null){
      return true;
    }
    else{
      return false;
    }
  }
}