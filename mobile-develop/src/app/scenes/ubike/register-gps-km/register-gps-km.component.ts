import { Component, OnInit, ViewEncapsulation, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import * as Geolocation from "nativescript-geolocation";
const timerModule = require("tns-core-modules/timer");

/* Component */
@Component({
    selector: "RegisterGPSkm",
    moduleId: module.id,
    templateUrl: "./register-gps-km.component.html",
    styleUrls: ["./register-gps-km.component.scss"],
    encapsulation: ViewEncapsulation.Native
})

export class RegisterGPSkmComponent implements OnInit, OnDestroy {
/* Public proprities */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public institution: string;

    public total = 0;
    public timeOF = 0;
    public dashboard: boolean = false
    public started = false;
    public kmVar: any;

    public initialLocation: Geolocation.Location;
    public finalLocation: Geolocation.Location;

    public distance: any;

    private _subject = new Subject<boolean>();
    public timeTT: any;
    public distanceTT: any;
    public distanceTotal = 0;

    /**
     * Constructor
     * @param {StorageService}  private _storage
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     * @param {UbikeService}    private _ubikeService
     * @param {ActivatedRoute}  private _activatedRoute
     * @param {Router}          private _router

    */
    constructor(
        private _storage: StorageService,
        private _page: Page,
        private _translate: Translate,
        private _ubikeService: UbikeService,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        this._page.actionBarHidden = true;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        this.languageAcronym = this.configStorage.language.acronym;
        this.getDeviceLocation()
        .then(
            location => (this.initialLocation = location),
            error => {
                console.error(error);
            }
        );
    }

    getDeviceLocation(): Promise<Geolocation.Location> {
        return new Promise((resolve, reject) => {
        Geolocation.enableLocationRequest().then(() => {
            Geolocation.getCurrentLocation({ desiredAccuracy: 10, timeout: 5000 })
            .then(location => {
                resolve(location);
                console.log('localizado em: ' + JSON.stringify(location));
            })
            .catch(error => {
                reject(error);
            });
        });
        });
    }

    // renders time in hh:mm:ss format
    newTimeFormat(time){
        var hrs = Math.floor(time / 3600);
        var mins = Math.floor((time % 3600) / 60);
        var secs = Math.floor(time % 60);

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }
        else{
            ret += "0:" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

    // returns time value as minutes
    calcTimes(timeOF){
        // Minutes and seconds calc - it must return a minutes value only (rounded if sec > 30)
        var mins = Math.floor(timeOF / 60);
        var secs = Math.floor(timeOF % 60);
        if(mins == 0){
            this.total = 1;
        }
        else{
            if(secs >= 30){
                this.total = mins + 1;
            }
            else{
                this.total = mins;
            }
        }
        return this.total;
    }

    // returns distance value as Km (ex:23.45 km)
    showKm(){
        let a = (this.distanceTotal/1000).toFixed(2);
        return a;
    }
    // returns trip distance in meters (sets initial location as )
    countDistance(){
        this.getDeviceLocation()
        .then(
            location => (this.finalLocation = location),
            error => {
                console.error(error);
            }
        )
        .then(() => 
            Geolocation.distance(this.initialLocation, this.finalLocation)
        )
        .then(result => {
            console.log('Suming meters: ' + result);
            this.distanceTotal = this.distanceTotal + result;
            console.log('Moment distance is: ' + this.distanceTotal);
        })
        // after the distance calculation, the new initialLocation will be the recent finalLocation in order to use its value to the next distance calculation cycle
        this.initialLocation = this.finalLocation;
    }

    // adds 1 sec to the duration of the trip
    timeCount() {
        this.timeTT = timerModule.setInterval(() => {
            this.timeOF = this.timeOF + 1;
        }, 1000);
    }

    // starts time & distance counters (gets info with a 15 sec interval)
    startTrip() {
        this.timeCount();
        this.started = true;

        this.getDeviceLocation().then(
        location => (this.initialLocation = location),
        error => {
            console.error(error);
        });
        
        this.distanceTT = timerModule.setInterval(() => {
            this.countDistance();
        }, 10000);
    }

    // gets final time (in minutes), final distance and submit it to Ubike Service
    stopTrip() {
        // removing time and distance counters
        timerModule.clearInterval(this.timeTT);
        timerModule.clearInterval(this.distanceTT);

        this.started = false;

        // get final time in minutes
        let timeVar = this.calcTimes(this.timeOF);
        
        // get final location and add it to the final distance
        this.getDeviceLocation()
        .then(
            location => (this.finalLocation = location),
            error => {
                console.error(error);
            }
        )
        .then(() =>
            Geolocation.distance(this.initialLocation, this.finalLocation)
        )
        .then(result => {
            this.distanceTotal += result;
            console.log('Final distance is: ' + this.distanceTotal);

            if (this.distanceTotal > 0) {
                this.kmVar = (this.distanceTotal/1000).toFixed(2);
            }

            console.log('KMSSS:_ ' + this.kmVar);

            const data = {
                distance: "1",
                height_diff: 10,
                time: timeVar,
                trip_start: new Date(),
                description: "UBike Trip"
            };
            console.log('DATA IS: ' + JSON.stringify(data));
            
            if(this.distanceTotal > 0){
                this._ubikeService
                .sendTrip(data)
                .pipe(takeUntil(this._subject))
                .subscribe(() => {
                    this._router.navigate(["ubike/dashboard/"])
                    .then(() => {
                    //alert box with success message
                    switch(this.languageAcronym){
                        case 'pt':
                        alert('A viagem foi registada com sucesso!');
                        break;
                        case 'en':
                        alert('Your trip was recorded with success!');
                        break;
                        default:
                        alert('A viagem foi registada com sucesso!');
                        break;
                    }
                })
            });
        }
        else{
            //console.log('No Km to register');
            this._router.navigate(["ubike/dashboard/"]).then(() => {
                //alert box with "fail" message -- if the trip has 0.00 km it won't be recorded
                switch(this.languageAcronym){
                    case 'pt':
                        alert({title: "UBike", message: 'A viagem não foi registada por não incluir qualquer distância percorrida!', okButtonText: "OK"});
                        break;
                    case 'en':
                        alert({title: "UBike", message: 'Your trip was not recorded because it has a null distance value!', okButtonText: "OK"});
                        break;
                    default:
                        alert({title: "UBike", message: 'A viagem não foi registada por não incluir qualquer distância percorrida!', okButtonText: "OK"});
                        break;
                }
            })
        }
        // last line   
        })
    }

    ngOnDestroy() {
        // removing time and distance counters
        timerModule.clearInterval(this.timeTT);
        timerModule.clearInterval(this.distanceTT);

        this._subject.next(true);
        this._subject.unsubscribe();
    }
}
