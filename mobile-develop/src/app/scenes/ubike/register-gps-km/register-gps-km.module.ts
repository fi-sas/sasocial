import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { Translate } from "~/app/shared/translate.service";

import { RegisterGPSkmRoutingModule } from "./register-gps-km-routing.module";
import { RegisterGPSkmComponent } from "./register-gps-km.component";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    TranslateModule,
    RegisterGPSkmRoutingModule,
    ActionBarModule,
    FooterModalModule
  ],
  declarations: [RegisterGPSkmComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RegisterGPSkmModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
