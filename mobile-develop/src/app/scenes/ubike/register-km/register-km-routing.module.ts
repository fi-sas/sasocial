import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { RegisterkmComponent } from "./register-km.component";
import { UbikeService } from "~/app/shared/services/ubike.service";

const routes: Routes = [
  {
    path: "",
    component: RegisterkmComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule],
  providers: [UbikeService]
})
export class RegisterkmRoutingModule {}
