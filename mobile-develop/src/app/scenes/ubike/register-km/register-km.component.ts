import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";
import { UbikeService } from "~/app/shared/services/ubike.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { ActivatedRoute } from "@angular/router";

/* Component */
@Component({
    selector: "Registerkm",
    moduleId: module.id,
    templateUrl: "./register-km.component.html",
    styleUrls: ["./register-km.component.scss"],
    encapsulation: ViewEncapsulation.Native
})

export class RegisterkmComponent implements OnInit {
/* Public proprities */
    public isLoading = true;
    public languages: Language[];
    public languageAcronym: string;
    public languageName: string;
    public configStorage: Config;
    public userStorage: User;
    public institution: string;

    public limit = -1;
    public offset = 0;
    public total = 0;
    public trips: any;
    public tripsList: any;
    public error: boolean;

    public tripId;
    public tripData;

    public option = 1;

    //DATA TO SEND
    public tripDistance = null;
    public tripTime = null;
    public tripDate = null;
    public tripDateString = null;
    public tripDescription = null;
    public bikeId: any;

    private _subject = new Subject<boolean>();

    /**
     * Constructor
     * @param {StorageService}  private _storage
     * @param {Page}            private _page
     * @param {Translate}       private _translate
     * @param {UbikeService}    private _ubikeService
     * @param {ActivatedRoute}  private _activatedRoute
     * @param {Router}          private _router

    */
    constructor(
        private _storage: StorageService,
        private _page: Page,
        private _translate: Translate,
        private _ubikeService: UbikeService,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        this._page.actionBarHidden = true;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.error = false;
        this.configStorage = this._storage.get("config");
        this.institution = this.configStorage.institution.acronym;
        this.getTrips();

        // this._activatedRoute.params.forEach(params => {
        //     this.tripId = params["id"];
        //     this.tripDistance = params["distance"];
        //     this.tripDate = params["date"].split("T")[0];
        //     this.tripDescription = params["description"];

        //     if (params["id"]) {
        //     this.option = 2;
        //     }
        // });
    }

    getTrips() {
        this._ubikeService
        .getTrips(this.limit, this.offset)
        .pipe(takeUntil(this._subject))
        .subscribe((res: any) => {
            if(res && res.data && res.data.length > 0) {
                this.bikeId = res.data[0].bike_id;
            }
            this.isLoading = false;
        });
    }

    sendTrip() {
        let isValid = this.validateForm();

        if(isValid){
            if(this.tripDistance.includes(',')){
                this.tripDistance = this.tripDistance.replace(",",".");
            }
            else{
                this.tripDistance = this.tripDistance;
            }

            const data = {
                distance: this.tripDistance,
                height_diff: 10,
                time: this.tripTime,
                trip_start: this.tripDate,
                description: this.tripDescription,
            };

            this._ubikeService
            .sendTrip(data)
            .pipe(takeUntil(this._subject))
            .subscribe(() => {
                this._router.navigate(["ubike/dashboard/"]);
            });
        }
        else{
            this.error = true;
        }
    }

    validateForm(){
        if(this.tripDistance !== null && this.tripDate !== null && this.tripDescription !== null){
            return true;
        }
        else{
            return false;
        }
    }

    updateTrip() {
        const data = {
            distance: this.tripDistance,
            height_diff: 10,
            time: 1800,
            trip_start: new Date(this.tripDate),
            description: this.tripDescription,
        };

        this._ubikeService
        .updateTrip(this.tripId, data)
        .pipe(takeUntil(this._subject))
        .subscribe(() => {
            this._router.navigate(["ubike/dashboard/"]);
        });
    }

    deleteTrip() {
        this._ubikeService
        .deleteTrip(this.tripId)
        .pipe(takeUntil(this._subject))
        .subscribe(() => {
            this._router.navigate(["ubike/dashboard/"]);
        });
    }
    /**
     * Pick date
     */
    pickDate(): void {
        const picker = new ModalDatetimepicker();

        picker.pickDate({
            title: "",
            theme: "dark",
            maxDate: new Date(),
            is24HourView: false
        })
        .then(result => {
            if(result){
                this.tripDateString = result["year"] + "-" + result["month"] + "-" + result["day"];
                this.tripDate = new Date(result.year, result.month - 1, result.day);
            }
        })
        .catch(error => {});
    }
}
