import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { Translate } from "~/app/shared/translate.service";

import { RegisterkmRoutingModule } from "./register-km-routing.module";
import { RegisterkmComponent } from "./register-km.component";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { FooterModalModule } from "~/app/components/footer-modal/footer-modal.module";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    TranslateModule,
    RegisterkmRoutingModule,
    ActionBarModule,
    FooterModalModule
  ],
  declarations: [RegisterkmComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RegisterkmModule {
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
