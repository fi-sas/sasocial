import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { UbikeComponent } from "./ubike.component";

const routes: Routes = [
  /* start*/
  {
    path: "start",
    component: UbikeComponent,
    loadChildren: "~/app/scenes/ubike/start/start.module#StartModule"
  },
  {
    path: "dashboard",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/dashboard/dashboard.module#DashboardModule"
  },
  {
    path: "conditions",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/conditions/conditions.module#ConditionsModule"
  },
  {
    path: "candidatura",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/candidatura/candidatura.module#CandidaturaModule"
  },
  {
    path: "register-km/:id/:distance/:date",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/register-km/register-km.module#RegisterkmModule"
  },
  {
    path: "register-km",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/register-km/register-km.module#RegisterkmModule"
  },
  {
    path: "register-gps-km",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/register-gps-km/register-gps-km.module#RegisterGPSkmModule"
  },
  {
    path: "maintenance",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/maintenance/maintenance.module#MaintenanceModule"
  },
  {
    path: "details",
    component: UbikeComponent,
    loadChildren: "~/app/scenes/ubike/details/details.module#DetailsModule"
  },
  {
    path: "candidatura-details",
    component: UbikeComponent,
    loadChildren:
      "~/app/scenes/ubike/candidatura-details/candidatura-details.module#CandidaturaDetailsModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class UbikeRoutingModule {}
