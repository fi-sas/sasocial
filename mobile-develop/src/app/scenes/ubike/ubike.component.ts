import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { has } from "lodash";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Config } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
    selector: "Ubike",
    moduleId: module.id,
    templateUrl: "./ubike.component.html",
    styleUrls: ['./ubike.component.scss'],
    encapsulation: ViewEncapsulation.Native
})
export class UbikeComponent implements OnInit {

  /**
  * Constructor
  * @param {StorageService}  private _storage
  * @param {Router}          private _router
  * @param {Page}            private _page
  */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _page: Page
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit():void {}

}
