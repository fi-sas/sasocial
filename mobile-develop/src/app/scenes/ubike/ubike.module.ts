import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from '@ngx-translate/core';

import { Translate } from "~/app/shared/translate.service";

import { UbikeRoutingModule } from "./ubike-routing.module";
import { UbikeComponent } from "./ubike.component";

const PT_TRANSLATION = require('./pt.translation.json');
const EN_TRANSLATION = require('./en.translation.json');

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    UbikeRoutingModule
  ],
  declarations: [
    UbikeComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class UbikeModule {

  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(
    private _translate: Translate
  ) {

    this._translate.load(PT_TRANSLATION, 'pt');
    this._translate.load(EN_TRANSLATION, 'en');

  }

}
