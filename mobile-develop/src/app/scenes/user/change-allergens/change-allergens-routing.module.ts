import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ChangeAllergensComponent } from "./change-allergens.component";

const routes: Routes = [
  {
    path: "",
    component: ChangeAllergensComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ChangeAllergensRoutingModule {}
