import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { Translate } from "~/app/shared/translate.service";
import { AllergensService } from "~/app/scenes/start-up/allergens/allergens.service";

import { ChangeAllergensRoutingModule } from "./change-allergens-routing.module";
import { ChangeAllergensComponent } from "./change-allergens.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    ChangeAllergensRoutingModule
  ],
  declarations: [ChangeAllergensComponent],
  providers: [
    {
      provide: AllergensService,
      useClass: AllergensService,
      deps: [UrlService, ResourceService, StorageService]
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ChangeAllergensModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
