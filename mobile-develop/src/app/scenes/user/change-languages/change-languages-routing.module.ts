import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ChangeLanguagesComponent } from "./change-languages.component";

const routes: Routes = [
  {
    path: "",
    component: ChangeLanguagesComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ChangeLanguagesRoutingModule {}
