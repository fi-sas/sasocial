import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { device } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { findIndex } from "lodash";

import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { Translate } from "~/app/shared/translate.service";
import { Language, Config, User } from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "ChangeLanguages",
  moduleId: module.id,
  templateUrl: "./change-languages.component.html",
  styleUrls: ["./change-languages.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ChangeLanguagesComponent implements OnInit {
  /**
   * Public proprities
   */
  public isLoading = true;
  public languages: Language[];
  public languageAcronym: string;
  public languageName: string;
  public configStorage: Config;
  public userStorage: User;
  public isLogged = false;
  public tokenStorage: any;

  /**
   * Constructor
   * @param {Configurator}    private _config
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {Page}            private _page
   * @param {Translate}       private _translate
   */
  constructor(
    private _config: Configurator,
    private _storage: StorageService,
    private _router: Router,
    private _page: Page,
    private _translate: Translate
  ) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.configStorage = this._storage.get("config");
    this.userStorage = this._storage.get("user");
    this.tokenStorage = this._storage.get("device_token");
    this.isLogged = this.userStorage.logged;

    this.languages = this._config.getOptionTree("LANGUAGES")["LANGUAGES"];
    this.languageAcronym = this.configStorage.language.acronym;

    const index = findIndex(
      this.languages,
      o => o.acronym === this.languageAcronym
    );
    this.languageName = index !== -1 ? this.languages[index].name : "";

    this._translate.setLanguage(this.languageAcronym);
  }

  /**
   * Select language
   * @param {string} lang language
   * @param {string} name language name
   */
  selectLanguage(acronym: string, name: string): void {
    this.languageAcronym = acronym;
    this.languageName = name;

    this._translate.setLanguage(this.languageAcronym);
    this.setLanguage();
  } 

  /**
   * Set language to storage
   */
  setLanguage(): void {
    this.configStorage.language.acronym = this.languageAcronym;
    this.configStorage.language.name = this.languageName;

    this._storage.set("config", this.configStorage);

    if (this.isLogged) {
      this._router.navigate(["user/profile"]);
    } else {
      this._router.navigate(["dashboard"]);
    }
  }
}
