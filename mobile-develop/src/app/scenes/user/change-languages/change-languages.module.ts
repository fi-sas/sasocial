import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { Translate } from "~/app/shared/translate.service";

import { ChangeLanguagesRoutingModule } from "./change-languages-routing.module";
import { ChangeLanguagesComponent } from "./change-languages.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    ChangeLanguagesRoutingModule
  ],
  declarations: [ChangeLanguagesComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ChangeLanguagesModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
