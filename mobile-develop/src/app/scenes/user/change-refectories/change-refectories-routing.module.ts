import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ChangeRefectoriesComponent } from "./change-refectories.component";

const routes: Routes = [
  {
    path: "",
    component: ChangeRefectoriesComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ChangeRefectoriesRoutingModule {}
