import { Component, OnInit, ViewEncapsulation, OnDestroy } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject, of, from } from "rxjs";
import {
  takeUntil,
  flatMap,
  map,
  retry,
  concatMap,
  last
} from "rxjs/operators";

import { StorageService } from "~/app/shared/storage/storage.service";
import { Refectory, Config } from "~/app/shared/storage/storage";
import { AuthService } from "~/app/shared/auth/auth.service";

import { RefectoriesService } from "~/app/scenes/start-up/refectories/refectories.service";

/**
 * Component
 */
@Component({
  selector: "ChangeRefectories",
  moduleId: module.id,
  templateUrl: "./change-refectories.component.html",
  styleUrls: ["./change-refectories.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ChangeRefectoriesComponent implements OnInit, OnDestroy {
  /* Public properties */
  public isLoading = true;
  public refectories: Refectory[];
  public config: Config;
  public refectory: Refectory;

  /* Private */
  private _subject = new Subject<boolean>();

  /**
   * Constructor
   * @param {StorageService}     private _storage
   * @param {Router}             private _router
   * @param {ActivatedRoute}     private _activatedRoute
   * @param {AuthService}        private _auth
   * @param {RefectoriesService} private _refectories
   * @param {Page}               private _page
   */
  constructor(
    private _storage: StorageService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _auth: AuthService,
    private _refectories: RefectoriesService,
    private _page: Page
  ) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.config = this._storage.get("config");
    this.setRefectory();

    this.getRefectories()
      .pipe(takeUntil(this._subject))
      .subscribe(data => {
        this.refectories = data;
        this.isLoading = false;
      });

    this._page.on("unloaded", data => {
      //this.ngOnDestroy();
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * Get refectories
   */
  getRefectories() {
    const refectories = [];

    return this._refectories.getSchools().pipe(
      flatMap((item: any) => from(item)),
      flatMap((item: any) =>
        this._refectories
          .getServices(item.id)
          .pipe(map(val => ({ schools: item, services: val })))
      ),
      flatMap(item => {
        const schools = item.schools;
        const services: any = item.services;

        services.map(o => {
          if (o.type === "canteen") {
            refectories.push({
              id: o.id,
              schoolId: schools.id,
              name: `${o.name}`,
              description: `${schools.name}`
            });
          }
        });

        return of(refectories);
      })
    );
  }

  /**
   * Set refectory information
   */
  setRefectory(): void {
    const configStorage: Config = this._storage.get("config");
    this.refectory = configStorage.refectory;
  }

  /**
   * Select and set refectory (canteen) to storage
   * @param {Refectory} refectory selected canteen
   */
  selectRefectory(refectory: Refectory) {
    this.config.refectory.id = refectory.id;
    this.config.refectory.schoolId = refectory.schoolId;
    this.config.refectory.name = refectory.name;
    this.config.refectory.description = refectory.description;

    this._storage.set("config", this.config);

    this._router.navigate(["user/profile"]);
  }
}
