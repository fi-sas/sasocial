import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { ActionBarModule } from "~/app/components/action-bar/action-bar.module";
import { Translate } from "~/app/shared/translate.service";

import { UrlService } from "~/app/shared/url.service";
import { ResourceService } from "~/app/shared/resource.service";
import { StorageService } from "~/app/shared/storage/storage.service";

import { RefectoriesService } from "~/app/scenes/start-up/refectories/refectories.service";

import { ChangeRefectoriesRoutingModule } from "./change-refectories-routing.module";
import { ChangeRefectoriesComponent } from "./change-refectories.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [
    NativeScriptCommonModule,
    TranslateModule,
    ActionBarModule,
    ChangeRefectoriesRoutingModule
  ],
  declarations: [ChangeRefectoriesComponent],
  providers: [
    {
      provide: RefectoriesService,
      useClass: RefectoriesService,
      deps: [UrlService, ResourceService, StorageService]
    }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ChangeRefectoriesModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
