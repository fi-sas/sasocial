import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import * as firebase from "nativescript-plugin-firebase";

import { parseJwt, Utf8 } from "~/app/shared/utilities/string-utilities";
import { StorageService } from "~/app/shared/storage/storage.service";
import {
  Config,
  User,
  Institution,
  Allergens,
  Refectory,
  Language
} from "~/app/shared/storage/storage";

/**
 * Component
 */
@Component({
  selector: "Profile",
  moduleId: module.id,
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class ProfileComponent implements OnInit {
  /**
   * Public proprities
   */
  public user: any;
  public institution: any;

  public allergens: Allergens[];
  public allergensSumary: string;
  public refectory: Refectory;
  public language: Language;
  public topic: string;

  /**
   * Constructor
   * @param {StorageService}  private _storage
   * @param {Router}          private _router
   * @param {AuthService}     private _authService
   * @param {NewsService}     private _newsService
   * @param {BalancesService} private _balancesService
   */
  constructor(private _storage: StorageService, private _router: Router) { }

  /**
   * On init
   */
  ngOnInit(): void {
    this.setInstitution();
    this.setUser();
    this.setAlergens();
    this.setRefectory();
    this.setLanguage();
  }

  /**
   * Set institution information
   */
  setInstitution(): void {
    const configStorage: Config = this._storage.get("config");
    this.institution = configStorage.institution.acronym;
  }

  /**
   * Set user information
   */
  setUser(): void {
    const deviceToken: any = this._storage.get("device_token");
    const tokenData: any = parseJwt(deviceToken.token);
    this.user = Utf8(tokenData.user.name);
  }

  /**
   * Set allergens informtion
   */
  setAlergens(): void {
    const userStorage: User = this._storage.get("user");

    this.allergens = userStorage.allergens;
    this.allergensSumary = "";

    let count = 1;
    if(this.allergens){
      this.allergens.map((item, index) => {
        if (count < 3 && item.allergic === true && item.name) {
          this.allergensSumary = this.allergensSumary + item.name.trim() + ", ";
          count++;
        }
      });
    }

    if (count > 1) {
      this.allergensSumary = this.allergensSumary + " ...";
    } else {
      this.allergensSumary = "-";
    }
  }

  /**
   * Set refectory information
   */
  setRefectory(): void {
    const configStorage: Config = this._storage.get("config");
    this.refectory = configStorage.refectory;
  }

  /**
   * Set institution information
   */
  setLanguage(): void {
    const configStorage: Config = this._storage.get("config");
    this.language = configStorage.language;
  }

  /**
   * Logout
   */
  logout(): void {
    const userStorage: User = this._storage.get("user");
    // replacer
    this.topic = (userStorage.email).replace("@",".");
    firebase.unsubscribeFromTopic(this.topic).then(() => console.log("Unsubscribed from topic: " + this.topic));

    // unsubscribe based on institution
    switch(this.institution){
      case 'IPVC': 
        firebase.unsubscribeFromTopic('geral.ipvc.pt').then(() => console.log("Unsubscribed to topic: geral.ipvc.pt"));
        break;
      case 'IPCA':
        firebase.unsubscribeFromTopic('geral.ipca.pt').then(() => console.log("Unsubscribed to topic: geral.ipca.pt"));
        break;
      case 'IPB': 
        firebase.unsubscribeFromTopic('geral.ipb.pt').then(() => console.log("Unsubscribed to topic: geral.ipb.pt"));
        break;
    }

    userStorage.logged = false;
    this._storage.set("user", userStorage);

    const tokenStorage: any = this._storage.get("device_token");
    tokenStorage.token = null;
    this._storage.set("device_token", tokenStorage);

    this._router.navigate(["login"]);
  }

  /**
   * Go allergens
   */
  goAllergens() {
    this._router.navigate(["user/allergens"]);
  }

  /**
   * Go Refectories
   */
  goRefectories() {
    this._router.navigate(["user/refectories"]);
  }

  /**
   * Go Languages
   */
  goLanguages() {
    this._router.navigate(["user/languages"]);
  }
}
