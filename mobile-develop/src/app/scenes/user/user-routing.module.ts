import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { UserComponent } from "./user.component";

const routes: Routes = [
  /* user / profile */
  {
    path: "profile",
    component: UserComponent,
    loadChildren: "~/app/scenes/user/profile/profile.module#ProfileModule"
  },

  /* user / allergens */
  {
    path: "allergens",
    component: UserComponent,
    loadChildren:
      "~/app/scenes/user/change-allergens/change-allergens.module#ChangeAllergensModule"
  },

  /* user / languages */
  {
    path: "languages",
    component: UserComponent,
    loadChildren:
      "~/app/scenes/user/change-languages/change-languages.module#ChangeLanguagesModule"
  },

  /* user / refectories */
  {
    path: "refectories",
    component: UserComponent,
    loadChildren:
      "~/app/scenes/user/change-refectories/change-refectories.module#ChangeRefectoriesModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class UserRoutingModule {}
