import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Page } from "tns-core-modules/ui/page";

/**
 * Component
 */
@Component({
  selector: "User",
  moduleId: module.id,
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
  encapsulation: ViewEncapsulation.Native
})
export class UserComponent implements OnInit {
  /**
   * Constructor
   * @param {Page}            private _page
   */
  constructor(private _page: Page) {
    this._page.actionBarHidden = true;
  }

  /**
   * On init
   */
  ngOnInit(): void {}
}
