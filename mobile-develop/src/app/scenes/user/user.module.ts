import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { TranslateModule } from "@ngx-translate/core";

import { Translate } from "~/app/shared/translate.service";

import { UserRoutingModule } from "./user-routing.module";
import { UserComponent } from "./user.component";

const PT_TRANSLATION = require("./pt.translation.json");
const EN_TRANSLATION = require("./en.translation.json");

@NgModule({
  imports: [NativeScriptCommonModule, TranslateModule, UserRoutingModule],
  declarations: [UserComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UserModule {
  /**
   * Constructor
   * @param {Translate} private _translate
   */
  constructor(private _translate: Translate) {
    this._translate.load(PT_TRANSLATION, "pt");
    this._translate.load(EN_TRANSLATION, "en");
  }
}
