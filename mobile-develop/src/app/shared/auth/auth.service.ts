import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { of } from "rxjs";
import { timeout, catchError, flatMap, map } from "rxjs/operators";

import { UrlService } from "~/app/shared/url.service";
import { Configurator } from "~/app/shared/config/configurator.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService, Resource } from "~/app/shared/resource.service";

/* Interfaces */
export interface TokenIdentifier {
  token: string;
}

export interface UserInterface {
  active: boolean;
  created_at: string;
  email: string;
  external: string | null;
  id: number;
  institute: string | null;
  name: string;
  phone: string;
  rfid: string;
  student_number: number | null;
  updated_at: string;
  user_name: string;
}

@Injectable()
export class AuthService {
  constructor(
    private _urlService: UrlService,
    private _configurator: Configurator,
    private _storageService: StorageService,
    private _resourceService: ResourceService
  ) {}

  /**
   * Set device token
   * @param {[type]} force = false
   */
  public setDeviceToken(force = false) {
    const deviceID = this.whoAmI();

    if (!deviceID) {
      return of(false);
    }

    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN");

    return this._requestDeviceToken(url);
  }

  /**
   * Who am I
   */
  whoAmI() {
    return this._configurator.getOption<number>("DEVICE_ID", false);
  }

  /**
   * Has storage token
   */
  hasStorageToken() {
    return this._storageService.has("device_token");
  }

  /**
   * Validate token
   */
  validateToken() {
    const hasDeviceToken = this.hasStorageToken();

    if (hasDeviceToken) {
      const storageDeviceToken = this._storageService.get<{ token: string }>(
        "device_token"
      );

      const url = this._urlService.get("AUTH.VALIDATE_TOKEN", {
        token: storageDeviceToken.token
      });

      return this._requestValidateToken(url).pipe(
        catchError(error => of(false))
      );
    } else {
      return of(true);
    }
  }

  /**
   * Login by pin
   * @param {object} params user and pin
   */
  /**
   * Login by email and password
   * @param {string }} params [description]
   */
  public loginByEmailPassword(params: { email: string; password: string }) {
    const email = params.email;
    const password = params.password;

    const device = this._configurator.getOption("DEVICE_ID");
    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN", { id: device });

    return this._resourceService
      .create(url, { email: email, password: password })
      .pipe(
        catchError(error => of(error)),
        map((response: Resource<TokenIdentifier>) => {
          let authorized = false;

          if (response && Array.isArray(response.data)) {
            this._storageService.set("device_token", response.data[0]);
            authorized = true;
          }

          return authorized;
        })
      );
  }

  public loginByEmailPin(params: { email: string; pin: string }) {
    const email = params.email;
    const pin = params.pin;

    const device = this._configurator.getOption("DEVICE_ID");
    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN", { id: device });

    return this._resourceService
      .create(url, { email: email, pin: pin })
      .pipe(
        catchError(error => of(error)),
        map((response: Resource<TokenIdentifier>) => {
          let authorized = false;

          if (response && Array.isArray(response.data)) {
            this._storageService.set("device_token", response.data[0]);
            authorized = true;
          }

          return authorized;
        })
      );
  }

  /**
   * Get current user
   */
  public getCurrentUser() {
    const deviceToken = this._storageService.get<{ token: string }>(
      "device_token",
      false
    );

    const url = this._urlService.get("AUTH.GET_USER");

    return this._resourceService.read<UserInterface>(url, {
      headers: {
        Authorization: "Bearer " + deviceToken.token
      }
    });
  }

  /**
   * Logout
   */
  public logout() {
    this._storageService.remove("device_token");
  }

  /**
   * Request device token
   * @param {string} url
   */
  private _requestDeviceToken(url: string) {
    return this._resourceService.create<TokenIdentifier>(url, {}).pipe(
      timeout(2500),
      // catchError(error => of(error)),
      flatMap((rs: Resource<TokenIdentifier> | HttpErrorResponse | Error) => {
        let state = true;

        if (rs instanceof HttpErrorResponse || rs instanceof Error) {
          this._storageService.remove("device_token");

          state = false;
        } else {
          this._storageService.set("device_token", rs.data[0]);
        }

        return of(state);
      })
    );
  }

  /**
   * Request validate token
   * @param {string} url
   */
  private _requestValidateToken(url: string) {
    return this._resourceService.request("GET", url).pipe(
      catchError(error => of(false)),
      map((response: Resource<any[]>) => {
        return response;
      })
    );
  }
}
