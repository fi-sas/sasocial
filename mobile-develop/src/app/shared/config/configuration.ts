import { environment } from "./environment";

export const configuration = {
    DEVICE_ID: 9,
    DEFAULT_LANG: "pt",
    DEFAULT_INSTITUTION: "IPCA",
    LANGUAGES: [
    { acronym: "pt", name: "Português" },
    { acronym: "en", name: "English" }
    ],
    INSTITUTIONS: [
    { acronym: "IPB", name: "Instituto Politécnico de Bragança" },
    { acronym: "IPCA", name: "Instituto Politécnico do Cávado e do Ave" },
    { acronym: "IPVC", name: "Instituto Politécnico de Viana do Castelo" }
    ],
    DOMAINS_API: [
    { HOST: environment.api_auth, KEY: "@api_auth" },
    { HOST: environment.api_config, KEY: "@api_config" },
    { HOST: environment.api_media, KEY: "@api_media" },
    { HOST: environment.api_comunication, KEY: "@api_comunication" },
    { HOST: environment.api_alimentation, KEY: "@api_alimentation" },
    { HOST: environment.api_mobility, KEY: "@api_mobility" },
    { HOST: environment.api_current_account, KEY: "@api_current_account" },
    { HOST: environment.api_payments, KEY: "@api_payments" },
    { HOST: environment.api_ubike, KEY: "@api_ubike" }
    ],
    ENDPOINTS: {
    AUTH: {
        AUTHORIZE_TOKEN: "@api_auth:/v1/authorize/device-type/MOBILE",
        VALIDATE_TOKEN: "@api_auth:/v1/authorize/validate-token/:token",
        GET_USER: "@api_auth:/v1/authorize/user"
    },
    CONFIG: {
        GET_CONFIG: "@api_config:/v1/configuration"
    },
    MEDIA: {
        UPLOADS: "@api_media:/",
        RESIZE: "@api_media:/v1/files/:id/image/:width"
    },
    COMMUNICATION: {
        NEWS: "@api_comunication:/v1/posts/mobile"
    },
    ALIMENTATION: {
        SCHOOLS: "@api_alimentation:/v1/menu/schools",
        SERVICES: "@api_alimentation:/v1/menu/:school_id/services",
        CATEGORIES: "@api_alimentation:/v1/menu/service/:service_id/families",
        RESERVATIONS: "@api_alimentation:/v1/menu/user/reservations?sort=meal,date",
        CANCEL_RESERVATION: "@api_alimentation:/v1/menu/user/reservations/:ticket_id/cancel",
        MEALS: "@api_alimentation:/v1/menu/service/:service_id/menus/:day/:type?withRelated=taxes",
        MEALDETAIL: "@api_alimentation:/v1/menu/service/:service_id/dish/:dish_id?withRelated=taxes",
        ALERGENS: {
            READ: "@api_alimentation:/v1/menu/user-config/allergens",
            UPDATE: "@api_alimentation:/v1/menu/user-config/allergens"
        }
    },
    MOBILITY: {
        LOCALS: "@api_mobility:/v1/locals",
        ROUTE: "@api_mobility:/v1/route_search",
        PRICES: "@api_mobility:/v1/price_table",
        ROUTES: "@api_mobility:/v1/routes"
    },

    CURRENT_ACCOUNT: {
        BALANCES: "@api_current_account:/v1/movements/balances",
        MOVEMENTS: "@api_current_account:/v1/movements",
        MOVEMENTSDETAIL: "@api_current_account:/v1/movements/:movement_id?withRelated=items"
    },
    PAYMENTS: {
        CART: {
        CREATE: "@api_payments:/v1/cart",
        CHECKOUT: "@api_payments:/v1/cart/:cart_id/checkout"
        }
    },

    UBIKE: {
        BIKES: "@api_ubike:/v1/bikes",
        CANDIDATURE: "@api_ubike:/v1/applications",
        CANDIDATURE_STATUS: "@api_ubike:/v1/applications/client/:id/status",
        APP_FORMS: "@api_ubike:/v1/application-forms",
        HOMEPAGE: "@api_ubike:/v1/configs/application-homepage",
        MAINTENANCE: "@api_ubike:/v1/maintenances",
        TRIPDETAIL: "@api_ubike:/v1/trips/:id",
        TRIPS: "@api_ubike:/v1/trips"
    }
  }
};
