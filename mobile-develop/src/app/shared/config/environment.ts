export const hostAddresses = {
  IPB: {
    api_auth: "http://ipb.fisas-project.com",
    api_config: "http://ipb.fisas-project.com",
    api_media: "http://ipb.fisas-project.com",
    api_comunication: "http://ipb.fisas-project.com",
    api_alimentation: "http://ipb.fisas-project.com",
    api_mobility: "http://ipb.fisas-project.com",
    api_current_account: "http://ipb.fisas-project.com",
    api_payments: "http://ipb.fisas-project.com",
    api_ubike: "http://ipb.fisas-project.com"
  },
  IPCA: {
    api_auth: "https://portal.sas.ipca.pt/api",
    api_config: "https://portal.sas.ipca.pt/api",
    api_media: "https://portal.sas.ipca.pt/",
    api_comunication: "https://portal.sas.ipca.pt/api",
    api_alimentation: "https://portal.sas.ipca.pt/api",
    api_mobility: "https://portal.sas.ipca.pt/api",
    api_current_account: "https://portal.sas.ipca.pt/api",
    api_payments: "https://portal.sas.ipca.pt/api",
    api_ubike: "https://portal.sas.ipca.pt/api"
  },
  // IPVC production endpoints
  
  IPVC: {
    api_auth: "https://sasocial.sas.ipvc.pt/api",
    api_config: "https://sasocial.sas.ipvc.pt/api",
    api_media: "https://sasocial.sas.ipvc.pt/",
    api_comunication: "https://sasocial.sas.ipvc.pt/api",
    api_alimentation: "https://sasocial.sas.ipvc.pt/api",
    api_mobility: "https://sasocial.sas.ipvc.pt/api",
    api_current_account: "https://sasocial.sas.ipvc.pt/api",
    api_payments: "https://sasocial.sas.ipvc.pt/api",
    api_ubike: "https://sasocial.sas.ipvc.pt/api"
  }
  // IPVC development endpoints
  // IPVC: {
  //   api_auth: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_config: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_media: "https://sasocialdev.sas.ipvc.pt/",
  //   api_comunication: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_alimentation: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_mobility: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_current_account: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_payments: "https://sasocialdev.sas.ipvc.pt/api",
  //   api_ubike: "https://sasocialdev.sas.ipvc.pt/api"
  // }
};

export const environment = {
  api_auth: "@api_auth:/authorization",
  api_config: "@api_config:/configuration",
  api_media: "@api_media:/media",
  api_comunication: "@api_comunication:/communication",
  api_alimentation: "@api_alimentation:/alimentation",
  api_mobility: "@api_mobility:/bus",
  api_current_account: "@api_current_account:/current_account",
  api_payments: "@api_payments:/payments",
  api_ubike: "@api_ubike:/u_bike"
};
