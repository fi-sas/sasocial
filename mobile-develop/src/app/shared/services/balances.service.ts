import { Injectable, EventEmitter } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError, tap } from "rxjs/operators";
import { sortBy } from "lodash";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

/* interfaces */
export interface Balance {
    account_id: number;
    account_name?: string;
    current_balance: number;
    in_debt: number;
}

@Injectable()
export class BalancesService {
    /* Public properties */
    public movements: any;

    public balances: Balance[] = [
    {
        account_id: 0,
        account_name: "Guest",
        current_balance: 0,
        in_debt: 0
    }
    ];
    public emitter = new EventEmitter();

    /* Private properties */
    private _lang: string;
    private _authData = null;
    public _configStorage: Config;

    /**
     * Constructor
     * @param {UrlService}      private _urlService
     * @param {ResourceService} private _resourceService
     * @param {StorageService}  private _storageService
     */
    constructor(
        private _urlService: UrlService,
        private _resourceService: ResourceService,
        private _storageService: StorageService
    ) {
        this.init();
    }

    /* Init */
    init() {
    this.setLanguage();
    }

    /* Set Language */
    setLanguage() {
        this._configStorage = this._storageService.get("config");
        this._lang = this._configStorage.language.acronym;
    }

    /* Get balances */
    getBalances() {
        this.setLanguage();
        const url = this._urlService.get("CURRENT_ACCOUNT.BALANCES");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );

        return this._resourceService
        .list<any>(url, {
        headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
        }
        })
        .pipe(
        timeout(30000),
        catchError(err => of([])),
        map((res: any) => {
            /* truncate name, format balance */
            res.data.map(item => {
            const index = item.account_name.substr(0, 20).lastIndexOf(" ");
            if (index !== -1) {
                item.account_name = item.account_name.substr(0, index);
            }
            item["current_balance_formated"] = new Intl.NumberFormat("pt-PT", {
                style: "currency",
                currency: "EUR"
            }).format(item.current_balance);
            });

            this.balances = res.data;
            this.balances = sortBy(this.balances, item => item.account_id);
            this.emitter.next(this.balances);

            return this.balances;
        })
        );
    }

    /* Get movements */
    getMovements(limit: number, offset: number, accountid: number) {
        this.setLanguage();
        const url = this._urlService.get("CURRENT_ACCOUNT.MOVEMENTS");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );

        if (offset === 0) {
            this.movements = [];
        }

        const serviceOptions =
        "?limit=" +
        limit +
        "&offset=" +
        offset +
        "&sort=-id&withRelated=items" + "&account_id=" + accountid;

        return this._resourceService
        .list<any>(url + serviceOptions, {
            headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
            this.movements = this.movements.concat(res.data);
            return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     * Get one move
     * @param {number} id move id
     */

    findItem(movement_id: string){
        this.setLanguage();
        const url = this._urlService.get("CURRENT_ACCOUNT.MOVEMENTSDETAIL");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );
        return this._resourceService
        .list<any>(url.replace(":movement_id",movement_id), {
            headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
            this.movements = this.movements.concat(res.data);
            return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }
}
