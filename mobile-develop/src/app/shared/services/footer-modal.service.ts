import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class FooterModalService extends BehaviorSubject<any> {
  /**
   * Public propreties
   */
  public modalOpen = false;
  public ticket = {};
  public ticketId: any;
  public footerType: any;
  public candidatureId: any;

  /**
   * Set Ticket
   * @param {object} ticket
   */

  // modal for buy ticket operation
  setTicketModal(ticket: object) {
    this.footerType = 0;
    this.ticket = ticket;
  }

  // modal for cancel ticket operation
  setTicketModal2(ticketId: object) {
    this.footerType = 1;
    this.ticketId = ticketId;
  }

  // modal for general operation
  setUbikeModal(candidatureIdP: any) {
    this.candidatureId = candidatureIdP;
    this.footerType = 2;
  }

  /**
   * Toogle menu
   * @param {boolean} menuOpen
   */
  toogleModal(modal: boolean) {
    this.modalOpen = modal;
    this.next(null);
  }
}
