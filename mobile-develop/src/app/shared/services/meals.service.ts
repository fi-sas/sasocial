import { Injectable, EventEmitter } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";
import { find } from "lodash";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

@Injectable()
export class MealsService {
    /* Public properties */
    public meals: any;
    public reservations: any;
    public emitter = new EventEmitter();

    /* Private properties */
    private _lang: string;
    private _authData = null;
    public _configStorage: Config;

    /**
     * Constructor
     * @param {UrlService}      private _urlService
     * @param {ResourceService} private _resourceService
     * @param {StorageService}  private _storageService
     */
    constructor(
        private _urlService: UrlService,
        private _resourceService: ResourceService,
        private _storageService: StorageService
    ) {
        this.init();
    }

    /* Init */
    init() {
        this.meals = [];
        this.setLanguage();
    }

    /* Set Language */
    setLanguage() {
        this._configStorage = this._storageService.get("config");
        this._lang = this._configStorage.language.acronym;
    }

    /**
     * Get meals
     * @param {number} serviceId service id
     * @param {string} day day date
     * @param {string} type dish type: lunch or dinner
    */
    getMeals(serviceId: number, day: string, type: string) {
        this.setLanguage();
        const url = this._urlService.get("ALIMENTATION.MEALS", {
            service_id: serviceId,
            day: day,
            type: type
        });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService
        .list<any>(url, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
        })
        .pipe(
            map(res => {
            res.data.map(item => {
                item["price_formated"] = new Intl.NumberFormat("pt-PT", {
                style: "currency",
                currency: "EUR"
                }).format(item.price);
            });
                this.meals = this.meals.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     * Get one meal
     * @param {string} serviceID
     * @param {string} dishID
     * @param {string} dishTypeID
     */
    getMeal(serviceID: string, dishID: string) {
        this.setLanguage();
        const url = this._urlService.get("ALIMENTATION.MEALDETAIL", {
            service_id: serviceID,
            dish_id: dishID
        });
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );

        return this._resourceService
        .read<any>(url, {
            headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
            }
        })
        .pipe(
        map(res => {
            res.data.map(item => {
                item["price_formated"] = new Intl.NumberFormat("pt-PT", {
                    style: "currency",
                    currency: "EUR"
                }).format(item.price);
            });
            return res.data;
        }),
            timeout(30000),
            catchError(err => of([] as any[]))
        );
    }

    /**
     * Get one meal by memory
     * @param {number} id meal id
     */
    findItem(id: number) {
        return find(this.meals, item => Number(item.id) === Number(id));
    }

    /* Get bought/reservation meals */
    getReservations() {
        this.setLanguage();
        const url = this._urlService.get("ALIMENTATION.RESERVATIONS");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );

        return this._resourceService
        .list<any>(url, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
        map(res => {
            this.reservations = res.data;
            this.emitter.next(this.reservations);

            return this.reservations;
        }),
            timeout(30000),
            catchError(err => of([] as any[]))
        );
    }

    /* Cancel reservation */
    cancelReservation(ticketId: string) {
        this.setLanguage();

        // default property
        let data = {
            "payment_method_id": 1
        }

        const url = this._urlService.get("ALIMENTATION.CANCEL_RESERVATION");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );

        return this._resourceService.create<any>(url.replace(":ticket_id",ticketId), data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }
}
