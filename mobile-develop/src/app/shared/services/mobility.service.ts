import { Injectable, EventEmitter } from '@angular/core';
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";
import { find } from "lodash";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

@Injectable()
export class MobilityService {
    /**
     * Public properties
     */
    public locals: any;
    public route: any;
    public routes: any;
    public prices: any;
    public emitter = new EventEmitter();

    /**
     * Private properties
     */
    private _lang: string;
    private _authData = null;
    public _configStorage: Config;

    /**
     * Constructor
     * @param {UrlService}      private _urlService
     * @param {ResourceService} private _resourceService
     * @param {StorageService}  private _storageService
     */
    constructor(
        private _urlService: UrlService,
        private _resourceService: ResourceService,
        private _storageService: StorageService
    ) {
        this.init();
    }

    /**
     * Init
     */
    init() {
        this.locals = [];
        this.route = [];
        this.prices = [];
        this.routes = [];
        this.setLanguage();
    }
    /**
     * Set Language
     */
    setLanguage() {
        this._configStorage = this._storageService.get("config");
        this._lang = this._configStorage.language.acronym;
    }
    
    /**
     * Get services by school
     */
    getLocals(limit: number, offset: number, sort: string) {
        this.setLanguage();
        const url = this._urlService.get("MOBILITY.LOCALS");
        this._authData = this._storageService.get<{ token: string }>(
        "device_token"
        );
        
        if (offset === 0) {
            this.locals = [];
        }

        const serviceOptions = "?limit=" + limit + "&offset=" + offset + "&sort=id";

        return this._resourceService
        .list<any>(url + serviceOptions, {
          headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
          }
        })
        .pipe(
            map(res => {
                this.locals = this.locals.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }
    /**
     * Get one local
     * @param {number} id local id
     */
    getRoute(origin: any, destination: any, date: string) {
        this.setLanguage();
        const url = this._urlService.get("MOBILITY.ROUTE");
        this._authData = this._storageService.get<{ token: string }>("device_token");
        if(date === null || date === 'undefined'){
            const serviceOptions = "?origin=" + origin + "&destination=" + destination;
            return this._resourceService
            .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
            })
            .pipe(
                map(res => {
                    this.route = this.route.concat(res.data);
                    return res;
                }),
                timeout(30000),
                catchError(err => of([]))
            );
        }
        else{
            const serviceOptions = "?origin=" + origin + "&destination=" + destination + "&date=" + date;
            return this._resourceService
            .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
            })
            .pipe(
                map(res => {
                    this.route = this.route.concat(res.data);
                    return res;
                }),
                timeout(30000),
                catchError(err => of([]))
            );
        }
    }
    
    /**
     * Get routes by institute
     */
    getRoutes() {
        this.setLanguage();
        const url = this._urlService.get("MOBILITY.ROUTES");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );
    
        const serviceOptions = "?limit=-1&offset=0&sort=id";

        return this._resourceService
        .list<any>(url + serviceOptions, {
          headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
          }
        })
        .pipe(
            map(res => {
                this.routes = this.routes.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     * Get prices
     */
    getPrices(routeId,ticketId, UserTypeID) {
        this.setLanguage();
        const url = this._urlService.get("MOBILITY.PRICES");
        this._authData = this._storageService.get<{ token: string }>(
            "device_token"
        );
    
        const serviceOptions = "?route_id=" + routeId + "&ticket_type_id=" + ticketId + "&type_user_id=" + UserTypeID + "&currency=euro";
        
        return this._resourceService
        .list<any>(url + serviceOptions, {
          headers: {
            Authorization: "Bearer " + this._authData.token,
            "X-Language-Acronym": this._lang
          }
        })
        .pipe(
            map(res => {
                //this.prices = this.prices.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }
}
