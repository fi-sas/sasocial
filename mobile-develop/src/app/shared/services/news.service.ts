import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";
import { find } from "lodash";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

@Injectable()
export class NewsService {
  /**
   * Public properties
   */
  public news: any;

  /**
   * Private properties
   */
  private _lang: string;
  private _authData = null;
  public _configStorage: Config;

  /**
   * Constructor
   * @param {UrlService}      private _urlService
   * @param {ResourceService} private _resourceService
   * @param {StorageService}  private _storageService
   */
  constructor(
    private _urlService: UrlService,
    private _resourceService: ResourceService,
    private _storageService: StorageService
  ) {
    this.init();
  }

  /**
   * Init
   */
  init() {
    this.news = [];
    this.setLanguage();
  }

  /**
   * Set Language
   */
  setLanguage() {
    this._configStorage = this._storageService.get("config");
    this._lang = this._configStorage.language.acronym;
  }

  /**
   * Get services by school
   */
  getNews(limit: number, offset: number) {
    this.setLanguage();
    const url = this._urlService.get("COMMUNICATION.NEWS");
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    if (offset === 0) {
      this.news = [];
    }

    const serviceOptions =
      "?limit=" +
      limit +
      "&offset=" +
      offset +
      "&sort=-id&status=Approved&alive=true";

    return this._resourceService
      .list<any>(url + serviceOptions, {
        headers: {
          Authorization: "Bearer " + this._authData.token,
          "X-Language-Acronym": this._lang
        }
      })
      .pipe(
        map(res => {
          this.news = this.news.concat(res.data);
          return res;
        }),
        timeout(30000),
        catchError(err => of([]))
      );
  }

  /**
   * Get one news
   * @param {number} id news id
   */
  findItem(id: number) {
    return find(this.news, item => Number(item.id) === Number(id));
  }
}
