import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { has } from "lodash";
import { forkJoin, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";

import { parseJwt } from "~/app/shared/utilities/string-utilities";

/* interfaces */
interface PaymentsCart {
  account_id: number;
  // user_id: number;
  items: CartProductItem[];
}

interface CartProductItem {
  service_id: number;
  product_code: string;
  name: string;
  article_type: string;
  description: string;
  location: string;
  extra_info: string;
  quantity: number;
  liquid_value: number;
  vat_id: number;
}

@Injectable()
export class PaymentsService {
  /**
   * Public properties
   */
  public isUserLogged = false;
  public user = null;
  public tokenStorage: any;

  public product: any;

  /**
   * Private properties
   */
  private _lang: string;
  private _authData = null;
  public _configStorage: Config;

  /**
   * Constructor
   * @param {UrlService}      private _urlService
   * @param {ResourceService} private _resourceService
   * @param {StorageService}  private _storageService
   * @param {TranslateService}  private _translateService
   */
  constructor(
    private _urlService: UrlService,
    private _resourceService: ResourceService,
    private _storageService: StorageService,
    private _translateService: TranslateService
  ) { }

  /**
   * Init
   */
  init() {
    this.product = null;
    this.setLanguage();
    this.setUser();
  }

  /**
   * Set Language
   */
  setLanguage() {
    this._configStorage = this._storageService.get("config");
    this._lang = this._configStorage.language.acronym;
  }

  /**
   * Set user
   */
  setUser() {
    this.tokenStorage = this._storageService.get("device_token");
    const tokenData: any = parseJwt(this.tokenStorage.token);
    this.user = tokenData.user;
  }

  /**
   * Pay account
   * @param {number}    accountId
   */
  payCart(accountId: number, product: any) {
    this.setLanguage();
    this.setUser();
    return forkJoin(this.saveCart(this.user.id, accountId, product)).pipe(
      mergeMap(data => {
        const cart = data[0].data[0];
        return this.checkoutCart(cart.id, 1).pipe(
          map(res => {
            /* TODO: Uncomment when modules exist */
            //this.clearModule(moduleName, accountId);
            return of(res);
          })
        );
      })
    );
  }

  /**
   * Save cart
   * @param {number}  userId curent logged on user id
   * @param {number}  accountId current account id
   */
  saveCart(userId: number, accountId: number, product: any) {
    const langAcronym = this._lang;
    const datePipe = new DatePipe(
      langAcronym + "-" + langAcronym.toUpperCase()
    );
    const day = datePipe.transform(product.date, "dd MMM yyyy");

    const module_required_values = {
      refectory_id: product.id,
      refectory_consume_at: product.date,
      refectory_meal_category: product.meal.toUpperCase(),
      refectory_meal_type: product.dish_type_translation[0].name
    };

    // microservice payments cart data
    const data: PaymentsCart = {
      account_id: accountId,
      // user_id: userId,
      items: <CartProductItem[]>[]
    };

    // microservice payments cart product item
    const cartProduct: CartProductItem = {
      service_id: product.service_id,
      product_code: String(product.code),
      name: product.translations[0].name,
      description: product.location,
      article_type: "REFECTORY",
      location: product.location,
      extra_info: JSON.stringify(module_required_values),
      quantity: 1,
      liquid_value:
        product.price /
        (1 +
          (has(product, "tax") && has(product.tax, "tax_value")
            ? product.tax.tax_value
            : 0)),
      vat_id: product.tax_id
    };

    data.items.push(cartProduct);

    const url = this._urlService.get("PAYMENTS.CART.CREATE");
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    return this._resourceService.create<any>(url, data, {
      headers: {
        Authorization: "Bearer " + this._authData.token,
        "X-Language-Acronym": this._lang
      }
    });
  }

  /**
   * Checkout cart
   * @param {number} cartId    micro service cart id
   * @param {number} paymentId micro service payment id
   */
  checkoutCart(cartId: number, paymentId: number) {
    const url = this._urlService.get("PAYMENTS.CART.CHECKOUT", {
      cart_id: cartId
    });
    this._authData = this._storageService.get<{ token: string }>(
      "device_token"
    );

    const data = {
      payment_method_id: paymentId,
      payment_method_data: {}
    };

    return this._resourceService.create<any>(url, data, {
      headers: {
        Authorization: "Bearer " + this._authData.token,
        "X-Language-Acronym": this._lang
      }
    });
  }
}
