import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { map, timeout, catchError } from "rxjs/operators";
import { find } from "lodash";

import { UrlService } from "~/app/shared/url.service";
import { StorageService } from "~/app/shared/storage/storage.service";
import { ResourceService } from "~/app/shared/resource.service";
import { Config } from "~/app/shared/storage/storage";
import { parseJwt } from "../utilities/string-utilities";

@Injectable()
export class UbikeService {
    /* Public properties */
    public trips: any;
    public bikes: any;
    public candidatures: any;
    public homepages: any;

    /* Private properties */
    private _lang: string;
    private _authData = null;
    public _configStorage: Config;
  emitter: any;

    /**
     * Constructor
     * @param {UrlService}      private _urlService
     * @param {ResourceService} private _resourceService
     * @param {StorageService}  private _storageService
     */
    constructor(
        private _urlService: UrlService,
        private _resourceService: ResourceService,
        private _storageService: StorageService
    ) {
        this.init();
    }

    /**
     * Init
     */
    init() {
        this.trips = [];
        this.setLanguage();
    }

    /**
     * Set Language
     */
    setLanguage() {
        this._configStorage = this._storageService.get("config");
        this._lang = this._configStorage.language.acronym;
    }

    /**
     *Get all trips
    */
    getTrips(limit: number, offset: number) {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.TRIPS");
        this._authData = this._storageService.get<{ token: string }>("device_token");

        const token = parseJwt(this._authData.token);
        const userID: any = token.user.id;

        if (offset === 0) {
            this.trips = [];
        }

        const serviceOptions = "?limit=" + limit + "&offset=" + offset + "&user_id=" + userID + "&sort=-trip_start";

        return this._resourceService
        .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
                this.trips = this.trips.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     * Get one Trip
    */
    getTrip(id: number) {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.TRIPDETAIL", { id: id });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService
        .list<any>(url, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
            this.trips = this.trips.concat(res.data);
            return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     *Get all trips month
    */
    getTripsMonth(limit: number, offset: number, start: any, end: any) {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.TRIPS");
        this._authData = this._storageService.get<{ token: string }>("device_token");

        const token = parseJwt(this._authData.token);
        const userID: any = token.user.id;

        if (offset === 0) {
            this.trips = [];
        }

        const serviceOptions = "?limit=" + limit + "&offset=" + offset + "&user_id=" + userID + "&sort=-trip_start" + "&trip_start[lte]=" + end + "&trip_start[gte]=" + start;

        return this._resourceService
        .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
                this.trips = this.trips.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
     *Send trip data
    */
    sendTrip(data) {
        console.log('DATA: ' + JSON.stringify(data));
        this.setLanguage();

        const url = this._urlService.get("UBIKE.TRIPS", {});
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    /**
     *Update trip data
    */
    updateTrip(tripID: string, data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.TRIPDETAIL", { id: tripID });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.update<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    /**
     *Delete Trip
    */
    deleteTrip(tripID: string) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.TRIPDETAIL", { id: tripID });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.delete(url, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    sendCandidature(data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.CANDIDATURE", {});
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    /**
     *Get candidature
    */
    getCandidature(limit: number, offset: number) {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.CANDIDATURE");
        this._authData = this._storageService.get<{ token: string }>("device_token");

        const token = parseJwt(this._authData.token);
        const userID: any = token.user.id;

        if (offset === 0) {
            this.candidatures = [];
        }

        const serviceOptions = "?limit=" + limit + "&offset=" + offset + "&user_id=" + userID + "&sort=-id";
        return this._resourceService
        .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
                this.candidatures = this.candidatures.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    getBikes(limit: number, offset: number) {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.BIKES");
        this._authData = this._storageService.get<{ token: string }>("device_token");

        const token = parseJwt(this._authData.token);
        const userID: any = token.user.id;

        if (offset === 0) {
            this.bikes = [];
        }

        const serviceOptions = "?limit=" + limit + "&offset=" + offset;

        return this._resourceService
        .list<any>(url + serviceOptions, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
                this.bikes = this.bikes.concat(res.data);
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    sendMaintenance(data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.APP_FORMS", {});
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    /**
     * Get homepage
     */
    getHomepage() {
        this.setLanguage();
        const url = this._urlService.get("UBIKE.HOMEPAGE");
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService
        .list<any>(url, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        })
        .pipe(
            map(res => {
                this.homepages = res.data;
                return res;
            }),
            timeout(30000),
            catchError(err => of([]))
        );
    }

    /**
    * Approve Application
    */
    approveApplication(id: number, data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.CANDIDATURE_STATUS", { id: id });
        this._authData = this._storageService.get<{ token: string }>("device_token");
        
        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }

    /**
    * Reject Application
    */
    rejectApplication(id: number, data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.CANDIDATURE_STATUS", { id: id });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }
    /**
    * Withdraw Application
    */
    withdrawApplication(id: number, data) {
        this.setLanguage();

        const url = this._urlService.get("UBIKE.CANDIDATURE_STATUS", { id: id });
        this._authData = this._storageService.get<{ token: string }>("device_token");

        // console.log(this._authData.token)
        return this._resourceService.create<any>(url, data, {
            headers: {
                Authorization: "Bearer " + this._authData.token,
                "X-Language-Acronym": this._lang
            }
        });
    }
}
