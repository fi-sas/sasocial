import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { keys, has } from 'lodash'
import { STORAGE }  from './storage'

/* local storage */
const localStorage = require( "nativescript-localstorage" );

export interface StorageValueType {
  [key: string]: any;
}

export type StorageAction = 'add' | 'remove' | 'reset';

@Injectable()
export class StorageService {

  /**
   * Observable
   */
  public change = new Subject<{
    action: StorageAction;
    item: StorageValueType;
  }>();

  /**
   * Private properties
   */
  private _storage:any = null;

  /**
   * Constructor
   */
  constructor() {
    this._storage = localStorage;
    this.init();
  }

  /**
   * Initialize storage
   */
  init() {
    const storageKeys = keys(STORAGE);

    const deviceVersion = this.get('version');
    const storageVersion = STORAGE['version'];

    storageKeys.map(key => {
      if (!this.has(key) || deviceVersion !== storageVersion) {
        this.set(key, STORAGE[key]);
      }
    });
  }

  /**
   * Get key
   * @param {string} key
   * @param {any}    defaults
   */
  get<T>(key: string, defaults = null): T | null {
    return this.has(key) ? JSON.parse(this._storage.getItem(key)) : defaults;
  }

  /**
   * Set key
   * @param {string} key
   * @param {any}    value
   */
  set(key: string, value: any): void {
    this._storage.setItem(key, JSON.stringify(value));

    this.change.next({ action: 'add', item: { key: key, value: value } });
  }

  /**
   * Has key
   * @param  {string}  key [description]
   * @return {boolean}     [description]
   */
  has(key: string): boolean {
    return !!this._storage.getItem(key);
  }

  /**
   * Remove key
   * @param {string} key [description]
   */
  remove(key: string): void {
    this._storage.removeItem(key);

    this.change.next({ action: 'remove', item: { key: key, value: null } });
  }

  /**
   * Reset
   */
  reset() {
    this._storage.clear();

    this.change.next({ action: 'reset', item: null });
  }

  /**
   * Size
   */
  size() {
    return this._storage.length;
  }
}
