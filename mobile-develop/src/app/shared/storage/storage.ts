/* interfaces */
export interface Language {
  acronym: string;
  name: string;
}

export interface Institution {
  id: number;
  acronym: string;
  name: string;
}

export interface School {
  id: number;
  acronym: string;
  name: string;
}

export interface Refectory {
  id: number;
  schoolId: number;
  name: string;
  description?: string;
}

export interface Config {
  language: Language;
  institution: Institution;
  school: School;
  refectory: Refectory;
  allergens: {
    done: boolean;
  };
}

export interface User {
  logged: boolean;
  email: string;
  allergens: Allergens[];
}

export interface Allergens {
  id: number;
  name?: string;
  description?: string;
  allergic: boolean;
}

export interface Storage {
  version: string;
  config: Config;
  device_token: { token: string };
  user: User;
}

/* initial storage */
export const STORAGE: Storage = {
  /* storage version, if changed, storage starts with this values */
  version: "0.1.2",

  /* startup configuration */
  config: {
    language: {
      acronym: null,
      name: null
    },
    institution: {
      id: null,
      acronym: null,
      name: null
    },
    school: {
      id: null,
      acronym: null,
      name: null
    },
    refectory: {
      id: null,
      schoolId: null,
      name: null,
      description: null
    },
    allergens: {
      done: false
    }
  },

  /* device token, with or without logged user */
  device_token: { token: null },

  /* user information */
  user: {
    logged: false,
    email: null,
    /* { id: 1, name: "Moluscos", description: "", allergic: false } */
    allergens: []
  }
};
