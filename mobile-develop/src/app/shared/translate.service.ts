import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class Translate {
  _lang: any;
  /**
   * Create instance of the service.
   *
   * @param {TranslateService} _translateService
   * @param {NzI18nService} _antLangService
   */
  constructor(
    @Inject(TranslateService) private _translateService: TranslateService
  ) {}

  /**
   * Load object of translations to the service.
   *
   * @param {Object} translation
   * @param {string} lang
   */
  load(translation: Object, lang?: string): void {
    const language = lang ? lang : this._translateService.currentLang;
    //console.log('Current language is: ' + this._translateService.currentLang);
    this._translateService.setTranslation(language, translation, true);
  }

  /**
   * Get current defined language.
   *
   * @returns string
   */
  getLanguage(): string {
    return this._translateService.currentLang;
  }

  /**
   * Set the language on Translation service and
   * on AntdService.
   *
   * @param {string} lang
   */
  setLanguage(lang: string): void {
    this._translateService.setDefaultLang(lang);
    this._translateService.use(lang);
  }

  getService() {
    return this._translateService;
  }
}
