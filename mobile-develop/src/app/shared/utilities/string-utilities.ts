import { isIOS } from "tns-core-modules/platform";

declare var NSString: any;
declare var NSUTF8StringEncoding: any;
declare var java: any;
declare var android: any;
declare var NSData: any;
declare var NSDataBase64DecodingIgnoreUnknownCharacters: any;

/**
 * Base64 encode
 * @param {string} value
 */
export function base64Encode(value: string) {
  let base64 = null;

  if (isIOS) {
    const text = NSString.stringWithString(value);
    const data = text.dataUsingEncoding(NSUTF8StringEncoding);
    base64 = data.base64EncodedStringWithOptions(0);
  } else {
    const text = new java.lang.String(value);
    const data = text.getBytes("UTF-8");
    base64 = android.util.Base64.encodeToString(
      data,
      android.util.Base64.DEFAULT
    );
  }

  return base64;
}

/**
 * Base64 decode
 * @param {string} value
 */
export function base64Decode(str: string) {
  const chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  let output: string = "";

  str = String(str).replace(/=+$/, "");

  if (str.length % 4 === 1) {
    throw new Error("INVALID");
  }

  for (
    // initialize result and counters
    let bc: number = 0, bs: any, buffer: any, idx: number = 0;
    // get next character
    (buffer = str.charAt(idx++));
    // character found in table? initialize bit storage and add its ascii value;
    /* tslint:disable */
    ~buffer &&
      ((bs = bc % 4 ? bs * 64 + buffer : buffer),
        // and if not first of each 4 characters,
        // convert the first 8 bits to one ascii character
        bc++ % 4)
      ? (output += String.fromCharCode(255 & (bs >> ((-2 * bc) & 6))))
      : 0 /* tslint:enable */
  ) {
    // try to find character in table (0-63, not found => -1)
    buffer = chars.indexOf(buffer);
  }
  return output;
}

/**
 * Utf8 decode
 * @param {string} value
 */
export function Utf8(e: string) {
  var t = "";
  var n = 0;
  var r = 0;
  var c1 = 0;
  var c2 = 0;
  var c3;
  while (n < e.length) {
    r = e.charCodeAt(n);
    if (r < 128) {
      t += String.fromCharCode(r);
      n++;
    } else if (r > 191 && r < 224) {
      c2 = e.charCodeAt(n + 1);
      t += String.fromCharCode(((r & 31) << 6) | (c2 & 63));
      n += 2;
    } else {
      c2 = e.charCodeAt(n + 1);
      c3 = e.charCodeAt(n + 2);
      t += String.fromCharCode(((r & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      n += 3;
    }
  }
  return t;
}

export function parseJwt(token) {
  const base64Url = token.split(".")[1];
  const base64 = base64Url
    .replace(/-/g, "+")
    .replace(/_/g, "/")
    .replace(/,/g, "=");
  return JSON.parse(base64Decode(base64));
}
