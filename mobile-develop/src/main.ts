// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppModule } from "./app/app.module";
import { device } from "tns-core-modules/platform/platform";
// import { ios as iOSUtils } from "tns-core-modules/utils/utils";
import * as application from 'application';
// import application = require("tns-core-modules/application");

require("nativescript-plugin-firebase");

//lock screen orientation on portrait
var orientation = require('nativescript-orientation');
orientation.setOrientation("portrait");

//import { ios, run as applicationRun } from "tns-core-modules/application";
/* import { CustomAppDelegate } from "./app/custom-app-delegate"; */

/* ios.delegate = CustomAppDelegate; */
platformNativeScriptDynamic().bootstrapModule(AppModule);
