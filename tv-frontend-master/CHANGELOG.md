## [0.24.1](https://gitlab.com/fi-sas/tv-frontend/compare/v0.24.0...v0.24.1) (2022-04-20)


### Bug Fixes

* minor text correction ([b5683ea](https://gitlab.com/fi-sas/tv-frontend/commit/b5683ea088f309446c6817b170b392ecf7b0af7e))

# [0.24.0](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.14...v0.24.0) (2022-03-18)


### Features

* **media:** add url page to gallery view ([b0aa979](https://gitlab.com/fi-sas/tv-frontend/commit/b0aa979f1bc18ae933cc1699b3e8c6aabdee9b59))

## [0.23.14](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.13...v0.23.14) (2022-02-03)


### Bug Fixes

* **priority:** change algorithm of priorize medias ([e567599](https://gitlab.com/fi-sas/tv-frontend/commit/e5675997e2b5c0c4b8bb4c63afe033813bfa6daa))

## [0.23.13](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.12...v0.23.13) (2021-12-22)


### Bug Fixes

* **queue:** change style ([4d79b20](https://gitlab.com/fi-sas/tv-frontend/commit/4d79b205539f6eb2d8ff6b7220a2a22c5bb68fe3))

## [0.23.12](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.11...v0.23.12) (2021-12-17)


### Bug Fixes

* **queue:** ticket space ([e6dec86](https://gitlab.com/fi-sas/tv-frontend/commit/e6dec86f5a3d99f8fb9b12cdd187ad535527f847))

## [0.23.11](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.10...v0.23.11) (2021-12-16)


### Bug Fixes

* **socketio:** change config path ([641df32](https://gitlab.com/fi-sas/tv-frontend/commit/641df3292c0f502c285041d5715f02797b95192c))

## [0.23.10](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.9...v0.23.10) (2021-12-10)


### Bug Fixes

* **queue:** nzspan change ([ad0880b](https://gitlab.com/fi-sas/tv-frontend/commit/ad0880b721ae414ee28b9c70481c67ecc3ba0295))

## [0.23.9](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.8...v0.23.9) (2021-11-05)


### Bug Fixes

* **queue:** style change ([2039e54](https://gitlab.com/fi-sas/tv-frontend/commit/2039e54210e5cfb335c4e99afdab7bb8a9633774))

## [0.23.8](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.7...v0.23.8) (2021-11-03)


### Bug Fixes

* **queue:** style change ([dd7c552](https://gitlab.com/fi-sas/tv-frontend/commit/dd7c552ed0bd10c87423ac8a48b50904a4a58c4c))

## [0.23.7](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.6...v0.23.7) (2021-11-02)


### Bug Fixes

* **geral:** text-change ([14f4286](https://gitlab.com/fi-sas/tv-frontend/commit/14f428699608406a76b866c7cb68d9a2f9f89c86))

## [0.23.6](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.5...v0.23.6) (2021-09-17)


### Bug Fixes

* **config:** time configurations ([c527b59](https://gitlab.com/fi-sas/tv-frontend/commit/c527b5940e5f93fb9f53e8014e55bda71774810f))

## [0.23.5](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.4...v0.23.5) (2021-09-15)


### Bug Fixes

* **install:** change title ([033e54f](https://gitlab.com/fi-sas/tv-frontend/commit/033e54f7706335525cdd363a79544d939d4b9bb1))

## [0.23.4](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.3...v0.23.4) (2021-09-13)


### Bug Fixes

* **install:** change input ([c4ac62c](https://gitlab.com/fi-sas/tv-frontend/commit/c4ac62c6e19f2426fbd13c4e3558787eeb124a7e))

## [0.23.3](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.2...v0.23.3) (2021-09-13)


### Bug Fixes

* **install:** news settings ([eddfbe6](https://gitlab.com/fi-sas/tv-frontend/commit/eddfbe6a433fddb10ed928a7b362e72d3bdcdb45))

## [0.23.2](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.1...v0.23.2) (2021-09-08)


### Bug Fixes

* **geral:** themes from other institutes ([55ae945](https://gitlab.com/fi-sas/tv-frontend/commit/55ae945e9840eddc05c9d61f1f3b184e093ce606))

## [0.23.1](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0...v0.23.1) (2021-08-31)


### Bug Fixes

* **queue:** update tickets ([d0c361c](https://gitlab.com/fi-sas/tv-frontend/commit/d0c361c7121a4799a286603365cb694cdc5db63d))

# [0.23.0](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.20...v0.23.0) (2021-06-25)


### Bug Fixes

* **app:** remove guard from install ([38fceea](https://gitlab.com/fi-sas/tv-frontend/commit/38fceea62fac47af43563bb3e4b1a5e654fe088a))
* **config:** change install ([7b9875a](https://gitlab.com/fi-sas/tv-frontend/commit/7b9875a277ecc950cb3755a882c53cad53d1e174))
* **home:** fix home scene ([2fe253c](https://gitlab.com/fi-sas/tv-frontend/commit/2fe253cb85615b0735210b04b953cd4ddd1d6e19))
* **media/ticker:** set status enum uppercase ([4c80aa3](https://gitlab.com/fi-sas/tv-frontend/commit/4c80aa3ba2e19c8c646bbe13b4d2262eb733d127))
* **medias:** change url files ([a6696e8](https://gitlab.com/fi-sas/tv-frontend/commit/a6696e8b2328f6b26e09c9d142645f940e87135e))
* **merge:** fix conflits ([63e8e81](https://gitlab.com/fi-sas/tv-frontend/commit/63e8e812b8b578ac97f1f936f0dcadce7e11cdec))
* **merge:** remove duplicate import ([a26a95d](https://gitlab.com/fi-sas/tv-frontend/commit/a26a95da59fff9943ae6c10e4b79e4edc8ab3557))
* **queue:** allow queuing configuration by device ([779030b](https://gitlab.com/fi-sas/tv-frontend/commit/779030be7bcea2bbdad5a1a19e6ac9747146f5fd))
* **queue:** api_queue config ([706af54](https://gitlab.com/fi-sas/tv-frontend/commit/706af54fb927a1a900a5aff2c628aec2fd8497c1))
* **queue:** fields ms ([7c1264b](https://gitlab.com/fi-sas/tv-frontend/commit/7c1264be6cef423ae3baeed8545a9d72b765c05d))
* **queue:** fix media bar position ([53283c8](https://gitlab.com/fi-sas/tv-frontend/commit/53283c829a814bb34a5cd3f46864d735dee803c5))
* **queue:** improvement aspect ([f59b1a5](https://gitlab.com/fi-sas/tv-frontend/commit/f59b1a54df7f5c911d1d0922e235cb562d18c854))
* **queue:** Some visual fixs ([948ce05](https://gitlab.com/fi-sas/tv-frontend/commit/948ce05b081ba4ecf266d11130305e799c8df7ae))
* **socket.io:** add eventsto socketio serice ([27e9958](https://gitlab.com/fi-sas/tv-frontend/commit/27e9958f3b6f050b40c0452eb46a8e6b4790dbd3))
* **tickers:** on internet shutdown the tickers dont start ([1bdc883](https://gitlab.com/fi-sas/tv-frontend/commit/1bdc88326b3f16099185cba767646449a3178ba6))
* update new auth method ([0e42525](https://gitlab.com/fi-sas/tv-frontend/commit/0e425251cd8abdc88b3af867744fa59f0d76c1ec))
* **tickers:** ticker is now a isolated component ([5dfa1db](https://gitlab.com/fi-sas/tv-frontend/commit/5dfa1db0b30e8d1d62daf6d9973a7b9f5bca607a))


### Features

* **configs:** add option to start as QUEUE or TV only ([672d86d](https://gitlab.com/fi-sas/tv-frontend/commit/672d86d9b98c973a9e3f6191fd277afc2d2901e4))
* **queue:** add next ticket call ([143c073](https://gitlab.com/fi-sas/tv-frontend/commit/143c07325ec48dd25f730a25c475f846e210fed2))
* **queue:** add service for queue ([f23a255](https://gitlab.com/fi-sas/tv-frontend/commit/f23a255aeb79e5ddf095d68e7d43affe09bb3c05))
* **queue:** create queue module ([8f0668b](https://gitlab.com/fi-sas/tv-frontend/commit/8f0668bf295a6b5fa3a4584f17f0b797328e6b13))
* **queue:** include modules ([4730d14](https://gitlab.com/fi-sas/tv-frontend/commit/4730d148d6f606fca058d44cf3f62b0f1684eb2c))
* **queue:** layout ([e23feb2](https://gitlab.com/fi-sas/tv-frontend/commit/e23feb2f88596a5cb3a2a5dc40baaf38e1201738))
* **queue:** layout for queue ([0f1504f](https://gitlab.com/fi-sas/tv-frontend/commit/0f1504fb797180bbaa842e5ceff47fa9908353f5))
* **routing:** rounting ([8f44f8b](https://gitlab.com/fi-sas/tv-frontend/commit/8f44f8b1566dc767fa577c24e8d74509df9e5282))
* **socket.io:** add socket io service ([0e4a5ec](https://gitlab.com/fi-sas/tv-frontend/commit/0e4a5ec60dcf9f5244189d1d5f6ea5df2824cfd4))
* **socket.io:** add socketio connection ([c7b519d](https://gitlab.com/fi-sas/tv-frontend/commit/c7b519d8b410effe634516196e54cba0520e67c7))

# [0.23.0-rc.7](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.6...v0.23.0-rc.7) (2021-06-24)


### Bug Fixes

* **tickers:** on internet shutdown the tickers dont start ([1bdc883](https://gitlab.com/fi-sas/tv-frontend/commit/1bdc88326b3f16099185cba767646449a3178ba6))

# [0.23.0-rc.6](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.5...v0.23.0-rc.6) (2021-05-28)


### Features

* **configs:** add option to start as QUEUE or TV only ([672d86d](https://gitlab.com/fi-sas/tv-frontend/commit/672d86d9b98c973a9e3f6191fd277afc2d2901e4))

# [0.23.0-rc.5](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.4...v0.23.0-rc.5) (2021-05-05)


### Bug Fixes

* **queue:** improvement aspect ([f59b1a5](https://gitlab.com/fi-sas/tv-frontend/commit/f59b1a54df7f5c911d1d0922e235cb562d18c854))

# [0.23.0-rc.4](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.3...v0.23.0-rc.4) (2021-04-06)


### Bug Fixes

* **config:** change install ([7b9875a](https://gitlab.com/fi-sas/tv-frontend/commit/7b9875a277ecc950cb3755a882c53cad53d1e174))
* **queue:** fields ms ([7c1264b](https://gitlab.com/fi-sas/tv-frontend/commit/7c1264be6cef423ae3baeed8545a9d72b765c05d))
* **socket.io:** add eventsto socketio serice ([27e9958](https://gitlab.com/fi-sas/tv-frontend/commit/27e9958f3b6f050b40c0452eb46a8e6b4790dbd3))


### Features

* **queue:** add next ticket call ([143c073](https://gitlab.com/fi-sas/tv-frontend/commit/143c07325ec48dd25f730a25c475f846e210fed2))

# [0.23.0-rc.3](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.2...v0.23.0-rc.3) (2021-04-06)


### Bug Fixes

* **queue:** api_queue config ([706af54](https://gitlab.com/fi-sas/tv-frontend/commit/706af54fb927a1a900a5aff2c628aec2fd8497c1))

# [0.23.0-rc.2](https://gitlab.com/fi-sas/tv-frontend/compare/v0.23.0-rc.1...v0.23.0-rc.2) (2021-04-05)


### Bug Fixes

* **queue:** allow queuing configuration by device ([779030b](https://gitlab.com/fi-sas/tv-frontend/commit/779030be7bcea2bbdad5a1a19e6ac9747146f5fd))

# [0.23.0-rc.1](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.20-rc.2...v0.23.0-rc.1) (2021-03-15)


### Bug Fixes

* **home:** fix home scene ([2fe253c](https://gitlab.com/fi-sas/tv-frontend/commit/2fe253cb85615b0735210b04b953cd4ddd1d6e19))
* **merge:** fix conflits ([63e8e81](https://gitlab.com/fi-sas/tv-frontend/commit/63e8e812b8b578ac97f1f936f0dcadce7e11cdec))
* **merge:** remove duplicate import ([a26a95d](https://gitlab.com/fi-sas/tv-frontend/commit/a26a95da59fff9943ae6c10e4b79e4edc8ab3557))
* **queue:** fix media bar position ([53283c8](https://gitlab.com/fi-sas/tv-frontend/commit/53283c829a814bb34a5cd3f46864d735dee803c5))
* **queue:** Some visual fixs ([948ce05](https://gitlab.com/fi-sas/tv-frontend/commit/948ce05b081ba4ecf266d11130305e799c8df7ae))
* **tickers:** ticker is now a isolated component ([5dfa1db](https://gitlab.com/fi-sas/tv-frontend/commit/5dfa1db0b30e8d1d62daf6d9973a7b9f5bca607a))


### Features

* **queue:** add service for queue ([f23a255](https://gitlab.com/fi-sas/tv-frontend/commit/f23a255aeb79e5ddf095d68e7d43affe09bb3c05))
* **queue:** create queue module ([8f0668b](https://gitlab.com/fi-sas/tv-frontend/commit/8f0668bf295a6b5fa3a4584f17f0b797328e6b13))
* **queue:** include modules ([4730d14](https://gitlab.com/fi-sas/tv-frontend/commit/4730d148d6f606fca058d44cf3f62b0f1684eb2c))
* **queue:** layout ([e23feb2](https://gitlab.com/fi-sas/tv-frontend/commit/e23feb2f88596a5cb3a2a5dc40baaf38e1201738))
* **queue:** layout for queue ([0f1504f](https://gitlab.com/fi-sas/tv-frontend/commit/0f1504fb797180bbaa842e5ceff47fa9908353f5))
* **routing:** rounting ([8f44f8b](https://gitlab.com/fi-sas/tv-frontend/commit/8f44f8b1566dc767fa577c24e8d74509df9e5282))
* **socket.io:** add socket io service ([0e4a5ec](https://gitlab.com/fi-sas/tv-frontend/commit/0e4a5ec60dcf9f5244189d1d5f6ea5df2824cfd4))
* **socket.io:** add socketio connection ([c7b519d](https://gitlab.com/fi-sas/tv-frontend/commit/c7b519d8b410effe634516196e54cba0520e67c7))

## [0.22.20-rc.2](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.20-rc.1...v0.22.20-rc.2) (2021-03-12)


### Bug Fixes

* update new auth method ([0e42525](https://gitlab.com/fi-sas/tv-frontend/commit/0e425251cd8abdc88b3af867744fa59f0d76c1ec))

## [0.22.20-rc.1](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.19...v0.22.20-rc.1) (2020-12-18)


### Bug Fixes

* **app:** remove guard from install ([38fceea](https://gitlab.com/fi-sas/tv-frontend/commit/38fceea62fac47af43563bb3e4b1a5e654fe088a))
* **media/ticker:** set status enum uppercase ([4c80aa3](https://gitlab.com/fi-sas/tv-frontend/commit/4c80aa3ba2e19c8c646bbe13b4d2262eb733d127))
* **medias:** change url files ([a6696e8](https://gitlab.com/fi-sas/tv-frontend/commit/a6696e8b2328f6b26e09c9d142645f940e87135e))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.22.20](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.19...v0.22.20) (2020-12-09)

### [0.22.19](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.18...v0.22.19) (2020-10-02)

### [0.22.18](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.17...v0.22.18) (2020-10-02)

### [0.22.17](https://gitlab.com/fi-sas/tv-frontend/compare/v0.22.16...v0.22.17) (2020-10-02)


### Bug Fixes

* **core:** remove message box on erro 500 ([5f7f2a7](https://gitlab.com/fi-sas/tv-frontend/commit/5f7f2a7a9fca99721ffd68be6c931d68b4c08972))

### 0.22.16 (2020-09-15)
