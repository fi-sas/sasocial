import { FiAppComponent } from './app.component';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { COMPONENTS_MODULES } from './app.module';
import { ApplicationProvisionModule } from './app.provision';

describe('> Application Component', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          ApplicationProvisionModule,
          ...COMPONENTS_MODULES
        ],
        declarations: [FiAppComponent]
      }).compileComponents();
    })
  );

  xit(
    '# Should create component',
    async(() => {
      const fixture = TestBed.createComponent(FiAppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    })
  );
});
