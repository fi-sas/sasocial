import { Component, OnInit, HostBinding, Inject } from '@angular/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

import { ApplicationState, Store } from '@fi-sas/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { KioskState } from '@fi-sas/tv/app.state';

import { LoadingAction, loadingReduce } from '@fi-sas/tv/services/loading.reducer';
import { FiTranslateLazyService } from './services/translate.lazy.service';

@Component({
  selector: 'fi-root',
  templateUrl: './app.component.html',
  styles: [
    `
  :host {
    width: 100%;
    height: 100%;
    background-color: transparent;
  }
  `
  ]
})
export class FiAppComponent implements OnInit {
  @HostBinding('class') theme = 'page theme-ipvc';

  /* public props */
  public language: string;
  public pending = false;

  /**
   * Constructor
   * @param {Router}                 private                   _router
   * @param {FiConfigurator}         private                   _configurator
   * @param {FiTranslateLazyService} private                   _translateLazyService
   * @param {Store<KioskState>}      @Inject(ApplicationState) private               _appState
   */
  constructor(
    private _router: Router,
    private _configurator: FiConfigurator,
    private _translateLazyService: FiTranslateLazyService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  /**
   * On init
   */
  ngOnInit() {
    this._appState.select().subscribe(state => {
      console.group('APP-STATE');
      console.dir(state);
      console.groupEnd();
    });

    this._router.events.subscribe((event: RouterEvent) => {
      this.routerInterceptor(event);
    });

    this.theme =
      'page theme-' +
      this._configurator
        .getOption<string>('ORGANIZATION.THEME', 'IPVC')
        .toLowerCase();

    this.language = this._translateLazyService.getLanguage();

    const store = this._appState.createSlice('ui').createSlice('loading');
    store.addReducer(LoadingAction, loadingReduce);
    store.watch().subscribe(state => {
      this.pending = state;
    });

  }

  /**
   * Router interceptor
   * @param {RouterEvent} event
   */
  private routerInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.pending = true;
    }

    if (
      event instanceof NavigationEnd ||
      event instanceof NavigationCancel ||
      event instanceof NavigationError
    ) {
      this.pending = false;
    }

  }

}
