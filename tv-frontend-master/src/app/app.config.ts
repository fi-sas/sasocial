import { environment } from '../environments/environment';

export const CONFIGURATION = {
  DEFAULT_LANG: 'pt',
  FEED: true,
  LONGITUDE: -8.61233,
  LATITUDE: 41.53183,
  LANGUAGES: [
    { acronym: 'pt', id: 3, name: 'Português' },
    { acronym: 'en', id: 4, name: 'English' }
  ],
  DOMAINS_API: [
    { HOST: environment.api_gateway, KEY: '@api_gateway' },
    { HOST: '', KEY: '@local' }
  ],
  ENDPOINTS: {
    AUTH: {
      AUTHORIZE_TOKEN: '@api_gateway:/api/authorization/authorize/device/:id',
      VALIDATE_TOKEN: '@api_gateway:/api/authorization/authorize/validate-token',
      REFRESH_TOKEN: '@api_gateway:/api/authorization/authorize/refresh-token/:type',
      GET_USER: '@api_gateway:/api/authorization/authorize/user'
    },
    CONFIG: {
      GET_CONFIG: '@api_gateway:/api/authorization/configuration'
    },
    MEDIA: {
      UPLOADS: '@api_gateway:/api/media/',
      RESIZE: '@api_gateway:/api/media/files/:id/image/:width'
    },
    COMMUNICATION: {
      MEDIA: '@api_gateway:/api/communication/posts/medias-tv',
      TICKERS: '@api_gateway:/api/communication/posts/tickers'
    },
    QUEUE: {
      TV_SERVICES: '@api_gateway:/api/queue/services/device?withRelated=subjects,translations,groups'
    },
  },
  SCREEN_SAVER: {
    TIME_OUT: 60000
  },
  ORGANIZATION: {
    /* themes: IPVC, IPCA or IPB */
    THEME: 'IPCA',
    COLORS: {
      IPVC: [
        '#3faec0',
        '#1791a5',
        '#107e90',
        '#0f6776',
        '#0b5a67',
        '#084d59',
        '#06353c',
        '#041e22',
        '#031416',
        '#02090b'
      ],
      IPCA: [
        '#0f8156',
        '#076743',
        '#005133',
        '#02482e',
        '#013c26',
        '#013320',
        '#012819',
        '#011d12',
        '#04130d',
        '#010b07'
      ],
      IPB: [
        '#a5006a',
        '#870157',
        '#79024e',
        '#6e0347',
        '#680143',
        '#61003e',
        '#520035',
        '#330021',
        '#210417',
        '#15020e'
      ],
      IPCOIMBRA: [
        '#737474',
        '#666868',
        '#5A5C5D',
        '#4D5051',
        '#414445',
        '#34393A',
        '#282D2E',
        '#1B2122',
        '#0F1517',
        '#02090B'
      ],
      IPLEIRIA: [
        '#737474',
        '#666868',
        '#5A5C5D',
        '#4D5051',
        '#414445',
        '#34393A',
        '#282D2E',
        '#1B2122',
        '#0F1517',
        '#02090B'
      ],
      IPPORTALEGRE: [
        '#BF9F33',
        '#AA8E2F',
        '#957E2A',
        '#806D26',
        '#6B5C21',
        '#564C1D',
        '#413B18',
        '#2C2A14',
        '#171A0F',
        '#02090B'
      ],
      IPSANTAREM: [
        '#0F8156',
        '#076743',
        '#005133',
        '#02482E',
        '#013C26',
        '#013320',
        '#012819',
        '#011D12',
        '#04130D',
        '#010B07'
      ],
      IPVISEU: [
        '#B84E86',
        '#A44678',
        '#903F6B',
        '#7B375D',
        '#672F4F',
        '#532842',
        '#3F2034',
        '#2A1826',
        '#161119',
        '#02090B'
      ]
    }
  },
  DEVICE_ID: 6
};
