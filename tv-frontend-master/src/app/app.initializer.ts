import { Injectable, Inject, Injector } from '@angular/core';
import { FiElectronService } from './services/electron.service';

import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FiConfigurator } from '@fi-sas/configurator';

@Injectable()
export class ApplicationInitializer {
  constructor(
    private _electronService: FiElectronService,
    private _configurationService: FiConfigurator,
    @Inject(Injector) private _injector: Injector
  ) {}

  public initialize() {
    return this._shouldInstall()
      .pipe(
        map(install => {
          const router = this._injector.get(Router);

          if (install) {
            router.navigate(['/install']);
          } else {
            if(this._configurationService.hasOption("DEVICE_TYPE") && this._configurationService.getOption("DEVICE_TYPE") == "TV") {
              router.navigate(['/']);
            } else {
              router.navigate(['/queue']);
            }
          }
        })
      )
      .toPromise();
  }

  private _shouldInstall() {
    return of(!!(this._electronService.isElectron() && this._isFirstRun()));
  }

  private _isFirstRun(): boolean {
    return this._electronService.preferences.size < 1;
  }
}
