import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FiAppComponent } from './app.component';
import { ApplicationRoutingModule } from './app.routing';
import { ApplicationProvisionModule } from './app.provision';

import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [FiAppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApplicationProvisionModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {
      enabled: environment.production
    }),
    ApplicationRoutingModule,
  ],
  bootstrap: [FiAppComponent],
  exports: [ApplicationProvisionModule]
})
export class AppModule {
  constructor(
    private _appRef: ApplicationRef,
    private _translateLazyService: FiTranslateLazyService
  ) {
    console.group('APPLICATION REFERENCE');
    console.dir(this._appRef);
    console.groupEnd();

    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');

  }
}
