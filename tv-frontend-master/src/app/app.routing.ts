/**
 * Application Routing Module
 *
 * All routes are defined in this module. Lazy load of
 * routing is configured to use.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FiHomeModule } from './scenes/home/home.module';
import { FiUnavailableModule } from './scenes/unavailable/unavailable.module';
import { FiIdentifierGuardService } from './services/identifier-guard.service';
import { FiScreenGuardService } from './services/screen-guard.service';

const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => FiHomeModule,
    canActivate: [FiIdentifierGuardService, FiScreenGuardService]
  },
  {
    path: 'queue',
    pathMatch: 'full',
    loadChildren: '@fi-sas/tv/scenes/queue/queue.module#FiQueueModule',
    canActivate: [FiIdentifierGuardService, FiScreenGuardService]
  },
  {
    path: 'install',
    pathMatch: 'full',
    loadChildren: '@fi-sas/tv/scenes/install/install.module#FiInstallModule',
  },
  {
    path: 'unavailable',
    pathMatch: 'full',
    loadChildren: () => FiUnavailableModule
  },
  /* for testing purposes */
  {
    path: 'play',
    loadChildren: '@fi-sas/tv/scenes/play/play.module#FiPlayModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, {
      enableTracing: false,
      initialNavigation: 'enabled',
      useHash: false
    })
  ],
  exports: [RouterModule]
})
export class ApplicationRoutingModule {}
