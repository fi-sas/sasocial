import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiClockComponent } from './clock.component';

describe('> ClockComponent', () => {
  let component: FiClockComponent;
  let fixture: ComponentFixture<FiClockComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FiClockComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(FiClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('# Should create component instance.', () => {
    expect(component).toBeTruthy();
  });
});
