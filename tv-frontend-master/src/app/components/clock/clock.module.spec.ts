import { FiClockModule } from './clock.module';

describe('ClockModuleModule', () => {
  let clockModuleModule: FiClockModule;

  beforeEach(() => {
    clockModuleModule = new FiClockModule();
  });

  it('should create an instance', () => {
    expect(clockModuleModule).toBeTruthy();
  });
});
