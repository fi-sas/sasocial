import { NgModule } from '@angular/core';

import { FiShareModule } from '@fi-sas/share';
import { FiClockComponent } from './clock.component';

@NgModule({
  imports: [FiShareModule],
  declarations: [FiClockComponent],
  exports: [FiClockComponent]
})
export class FiClockModule {}
