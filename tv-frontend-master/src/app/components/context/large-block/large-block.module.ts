import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiDirectivesModule } from '@fi-sas/tv/directives/directives.module';
import { FiContextLargeBlockComponent } from './large-block.component';

@NgModule({
  imports: [NgZorroAntdModule, FiShareModule, FiDirectivesModule],
  declarations: [FiContextLargeBlockComponent],
  exports: [FiContextLargeBlockComponent]
})
export class FiContextLargeBlockModule {}
