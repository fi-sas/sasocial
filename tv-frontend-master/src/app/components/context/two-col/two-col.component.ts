import { Component, Input, AfterViewInit } from '@angular/core';

interface TitleInterface {
  mainTitle: string,
  secondaryTitle: string,
  secondarySubtitle: string,
  secondaryLabel: string
}

@Component({
  selector: 'fi-context-two-col',
  templateUrl: './two-col.component.html',
  styleUrls: ['./two-col.component.less']
})
export class FiContextTwoColComponent implements AfterViewInit {

  @Input()
  data: TitleInterface = null;

  ngAfterViewInit() {

  }
}
