import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FiShareModule } from '@fi-sas/share';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FiContextTwoColComponent } from './two-col.component';
import { FiDirectivesModule } from '@fi-sas/tv/directives/directives.module';

@NgModule({
  imports: [
    NgZorroAntdModule,
    RouterModule,
    AngularSvgIconModule,
    FiShareModule,
    FiDirectivesModule
  ],
  declarations: [FiContextTwoColComponent],
  exports: [FiContextTwoColComponent]
})
export class FiContextTwoColModule {}
