import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, Input, Output, Inject, OnDestroy, EventEmitter } from '@angular/core';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'fi-fifteen-day',
  templateUrl: './fifteen.component.html',
  styleUrls: ['./fifteen.component.less']
})
export class FiFifteenDayComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('content', { read: ElementRef }) public content: ElementRef<any>;

  /* Inputs */
  @Input('topBorder')
  topBorder = true;
  @Input('bottomBorder')
  bottomBorder = true;

  @Input() public language: string;

  /* Outputs */
  @Output() OnOptionChange = new EventEmitter<number>();

  /* Public properties */
  public next15Days = [];
  public selectedDay = ((new Date()).setDate((new Date()).getDate()));
  public contentElement = null;
  public locale: string;

  public showTopBorder = true;
  public showBottomBorder = true;

  /* private properties */
  private subscription: Subscription;

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   * @param {DatePipe}               private _datePipe
   * @param {TranslateService}       private _translateService
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService,
    private _datePipe: DatePipe,
    private _translateService: TranslateService
  ) {

    let fridays = 0;
    for (let i = 0; i < 25 && fridays < 3; i++) {

      const day = ((new Date()).setDate((new Date()).getDate() + i));
      const weekDay = (new Date(day)).getDay();

      /* friday */
      if (weekDay !== 0 && weekDay % 5 === 0) {
        fridays++;
      }

      this.next15Days.push({date: day, weekDay: weekDay});

    }

  }

  /**
   * On init
   */
  ngOnInit() {
    const currentLang = this._translateService.currentLang || 'pt';

    this.showTopBorder = this.topBorder;
    this.showBottomBorder = this.bottomBorder;

    this.subscription = this._translateService.onLangChange.subscribe(
      (event: LangChangeEvent) => {
        this.locale = event.lang + '-' + event.lang.toUpperCase();
      }
    );

    this.locale = currentLang + '-' + currentLang.toUpperCase();

    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy() {

    if (this.subscription) {
      this.subscription.unsubscribe();
    }

  }

  /**
   * After view init
   */
  ngAfterViewInit() {
    this.contentElement = this.content.nativeElement.querySelector('.simplebar-content') as HTMLDivElement;
  }

  /**
   * Select a day
   * @param {number} index days array index
   * @param {number} day   date of a day
   */
  selectDay(index, day) {

    if (day.weekDay % 6 !== 0) {
      const days = this.contentElement.children[0].children;
      this.contentElement.scrollLeft = days[index].offsetLeft - 120;
      this.selectedDay = day.date;

      this.OnOptionChange.emit(this.selectedDay);
    }

  }

}
