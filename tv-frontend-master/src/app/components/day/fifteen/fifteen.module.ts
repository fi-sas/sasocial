import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiDirectivesModule } from '@fi-sas/tv/directives/directives.module';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import { FiFifteenDayComponent } from './fifteen.component';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
  	NgZorroAntdModule,
  	TranslateModule,
  	FiShareModule,
  	FiDirectivesModule
  ],
  declarations: [FiFifteenDayComponent],
  exports: [FiFifteenDayComponent]
})
export class FiFifteenDayModule {

  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }

}
