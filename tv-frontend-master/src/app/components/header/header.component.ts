import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenAction } from '@fi-sas/tv/services/screen.reducer';
import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/tv/app.state';
import {
  HeaderActionEnable,
  HeaderEnableReduce,
  HeaderActionDisable,
  HeaderDisableReduce
} from './header.reducer';
import { FiConfigurator } from '@fi-sas/configurator';


@Component({
  selector: 'fi-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[class.hidden]': '!enable'
  }
})
export class FiHeaderComponent implements OnInit {
  public enable: boolean;

  public title;

  /**
   * Constructor
   * @param {Router} private _router
   * @param {FiTickerService} private _tickerService
   * @param {FiTranslateLazyService} private _translateService
   * @param {FiConfigurator}    private _configurator
   * @param {Store<KioskState>} @Inject(ApplicationState) private _appState
   */
  constructor(
    private _router: Router,
    private _configurator: FiConfigurator,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {
    const store = this._appState.createSlice('ui').createSlice('header');

    store.addReducer(HeaderActionEnable, HeaderEnableReduce);
    store.addReducer(HeaderActionDisable, HeaderDisableReduce);
  }

  /**
   * On init
   */
  ngOnInit() {
    const store = this._appState.createSlice('ui').createSlice('header');
    store.watch().subscribe(state => {
      this.enable = state.active;
    });

    this.title = this._configurator
      .getOption<string>('ORGANIZATION.THEME', 'IPVC')
      .toUpperCase();
  }

  /**
   * Go to screen saver
   */
  goToScreenSaver() {
    ScreenAction.next(true);

    this._router.navigate(['/']);
  }
}
