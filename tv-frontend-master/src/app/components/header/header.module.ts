import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { FiShareModule } from '@fi-sas/share';
import { FiHeaderComponent } from './header.component';
import { FiTickerModule } from '@fi-sas/tv/components/ticker/ticker.module';
import { FiDateModule } from '@fi-sas/tv/components/date/date.module';
import { FiClockModule } from '@fi-sas/tv/components/clock/clock.module';
import { FiWeatherModule } from '@fi-sas/tv/components/weather/weather.module';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [
    FiShareModule,
    FiDateModule,
    NgZorroAntdModule,
    FiWeatherModule,
    FiClockModule,
    TranslateModule,
    FiTickerModule
  ],
  providers: [
  ],
  declarations: [FiHeaderComponent],
  exports: [FiHeaderComponent]
})
export class FiHeaderModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
