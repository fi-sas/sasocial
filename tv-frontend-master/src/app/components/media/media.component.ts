import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy,
  HostBinding,
} from "@angular/core";
import { Subscription } from "rxjs";
import { NzCarouselComponent } from "ng-zorro-antd";

import { FiUrlService } from "@fi-sas/core";
import { FiPriorityService } from "@fi-sas/share";
import { FiConfigurator } from "@fi-sas/configurator";
import { FiMediaService } from "@fi-sas/tv/components/media/media.service";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";

interface SlideEvent {
  from: number;
  to: number;
}

@Component({
  selector: "fi-media",
  templateUrl: "./media.component.html",
  styleUrls: ["./media.component.less"],
  providers: [FiPriorityService],
})
export class FiMediaComponent implements OnInit, OnDestroy {
  @HostBinding("class.page__body") isPageBody = true;
  /* Public properties */
  public mediaInterval: NodeJS.Timer;

  public defaultAutoPlaySpeed = 8000;
  public slides = [];

  public totalVideos = 0;
  public totalVideosMeta = 0;
  public videos = [];

  public progressBarInterval: NodeJS.Timer;
  public progressBarWidth = 0;

  public _subscriptions: Subscription[] = [];

  public medias;
  public priorityMedias = [];

  public urlMedia = this._urlService.get("MEDIA.UPLOADS");

  public title;

  public error = false;

  set subscriptions(subscription) {
    this._subscriptions.push(subscription);
  }

  @ViewChild("progressBarBack") progressBarBackground: ElementRef;
  @ViewChild("progressBarTimer") progressBarTimer: ElementRef;
  @ViewChild("carousel") carousel: NzCarouselComponent;

  /**
   * Creates an instance of FiMediaComponent.
   * @param {Renderer2}         private _renderer2
   * @param {FiMediaService}    private _mediaService
   * @param {FiPriorityService} private _priorityService
   * @param {FiUrlService}      private _urlService
   * @param {FiConfigurator}    private _configurator
   */
  constructor(
    private _renderer2: Renderer2,
    private _mediaService: FiMediaService,
    private _priorityService: FiPriorityService,
    private _urlService: FiUrlService,
    private _configurator: FiConfigurator,
    private _sanitizer: DomSanitizer
  ) {}

  /**
   * On destroy.
   *
   * @memberof FiMediaComponent
   */
  ngOnDestroy() {
    this._subscriptions.forEach((subscriptions) => subscriptions.unsubscribe());

    clearInterval(this.mediaInterval);
    clearInterval(this.progressBarInterval);
  }

  /**
   * On init.
   *
   * @memberof FiMediaComponent
   */
  ngOnInit() {
    this.title = this._configurator
      .getOption<string>("ORGANIZATION.THEME", "IPVC")
      .toUpperCase();

    this.getMedia();

    this.mediaInterval = setInterval(() => {
      this._priorityService.reset();
      this.getMedia();
    }, 1800000);
  }

  /**
   * Error image/video
   */
  errorSrc($event) {
    $event.srcElement.setAttribute("data-error", "true");
    $event.srcElement.removeAttribute("src");
    $event.srcElement.style.display = "none";
  }

  /**
   * Get media.
   *
   * @memberof FiMediaComponent
   */
  getMedia() {
    this.medias = [];
    this.priorityMedias = [];
    this.slides = [];
    this.totalVideos = 0;
    this.totalVideosMeta = 0;
    this.videos = [];
    this.progressBarWidth = 0;

    this.subscriptions = this._mediaService.getMedia().subscribe((data) => {
      this.medias = data;
      this.priorityMedias = data;

      /* remove empty path files */
      this.medias = this.medias.filter((media) => {
        if (!media.channel_name || media.channel_name === "MediaTV") {
          return (
            Object.prototype.hasOwnProperty.call(
              media.translations[0],
              "file_16_9"
            ) &&
            Object.prototype.hasOwnProperty.call(
              media.translations[0].file_16_9,
              "path"
            ) &&
            media.translations[0].file_16_9.path &&
            media.translations[0].file_16_9.public
          );
        }

        if (media.channel_name === "UrlMediaTV") {
          return !!media.translations[0].url;
        }
      });

      /* add media to priority service */
      this.medias.forEach((media) => {
        this._priorityService.add(media, media.weight === 0 ? 1 : media.weight);
      });

      /* get new media object from priority service multiplied by 3 */
      for (let i = 0; i < this.medias.length * 3; i++) {
        const media = this._priorityService.get();
        if (media) {
          this.priorityMedias.push(media);
        }
      }

      /* add original media at least one time to new media */
      this.medias.forEach((media) => {
        const position = Math.floor(
          Math.random() * (this.medias.length * 3 + 1)
        );
        this.priorityMedias.splice(position, 0, media);
      });

      /* set media with new media priority */
      this.medias = this.priorityMedias;

      if (this.medias.length) {
        const waitForCarousel = setTimeout(() => {
          this.slides = this.carousel
            ? this.carousel.carouselContents.toArray()
            : [];
          this.carousel.nzAutoPlay = false;
          this.setVideosData();
          clearTimeout(waitForCarousel);
        });
      }
    });
  }

  /**
   * Set videos meta data.
   *
   * @memberof FiMediaComponent
   */
  setVideosData() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;

    this.slides.forEach((slide, index) => {
      const elemVideo: HTMLVideoElement = (
        slide.el as HTMLElement
      ).querySelector("video");

      if (elemVideo) {
        this.totalVideos++;

        elemVideo.onloadedmetadata = function () {
          (this as HTMLVideoElement).autoplay = false;
          (this as HTMLVideoElement).controls = false;
          self.totalVideosMeta++;

          const duration = (this as HTMLVideoElement).duration * 1000;
          self.videos[index] = duration;
          if (self.totalVideos === self.totalVideosMeta) {
            self.carousel.nzAutoPlay = true;
            // self.setVideoForPlay(0);
            // self.setImageForPlay(0);
            self.carousel.goTo(0);
          }
        };

        elemVideo.onerror = function () {
          self.videos[index] = 8000;
          self.carousel.nzAutoPlay = true;
          // self.setVideoForPlay(0);
          // self.setImageForPlay(0);
          self.carousel.goTo(0);
        };
      }

      if (index === this.slides.length - 1) {
        if (this.totalVideos === 0) {
          self.carousel.nzAutoPlay = true;
          // self.setImageForPlay(0);
          self.carousel.goTo(0);
          self.progressBar(self.carousel.nzAutoPlaySpeed);
        }
      }
    });
  }

  /**
   * Carousel before change event
   *
   * @param {SlideEvent} current
   * @memberof FiMediaComponent
   */
  beforeChange(current: SlideEvent) {
    this.setVideoForPlay(current.to);
    this.setImageForPlay(current.to);
    this.progressBar(this.carousel.nzAutoPlaySpeed);
  }

  /**
   * Carousel after change event.
   *
   * @param {number} current
   * @memberof FiMediaComponent
   */
  afterChange(current: number) {
    //this.setImageForPlay(current);
    //this.progressBar(this.carousel.nzAutoPlaySpeed);
  }

  /**
   * Set video for play.
   *
   * @param {number} index
   * @memberof FiMediaComponent
   */
  setVideoForPlay(index: number) {
    if ((this.slides[index].el as HTMLElement).querySelector("video")) {
      const duration = this.videos[index];
      this.carousel.nzAutoPlaySpeed = duration;
      (this.slides[index].el as HTMLElement).querySelector("video").muted =
        false;
      (this.slides[index].el as HTMLElement).querySelector("video").volume = 1;
      (this.slides[index].el as HTMLElement).querySelector("video").load();
      (this.slides[index].el as HTMLElement).querySelector("video").play();
      this.progressBar(duration);
    }
  }

  /**
   * Set image for play.
   *
   * @param {*} index
   * @memberof FiMediaComponent
   */
  setImageForPlay(index: number) {
    if ((this.slides[index].el as HTMLElement).querySelector("img")) {
      this.carousel.nzAutoPlaySpeed = this.defaultAutoPlaySpeed;
      this.progressBar(this.defaultAutoPlaySpeed);
    }
  }

  /**
   * Progress bar animation.
   *
   * @param {*} duration
   * @memberof FiMediaComponent
   */
  progressBar(duration) {
    this.progressBarWidth = 0;
    clearInterval(this.progressBarInterval);

    this.progressBarInterval = setInterval(() => {
      if (this.progressBarTimer && this.progressBarTimer.nativeElement) {
        this._renderer2.setStyle(
          this.progressBarTimer.nativeElement,
          "width",
          (this.progressBarWidth++).toString() + "px"
        );
      } else {
        clearInterval(this.progressBarInterval);
      }
    }, duration / this.progressBarBackground.nativeElement.offsetWidth);
  }

  safeIFrameUrl(url: string): SafeUrl {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
