import { Injectable } from '@angular/core';
import {
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';
import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError, flatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

@Injectable()
export class FiMediaService {
  /**
   * Constructor
   * @param {FiResourceService}      private _resourceService
   * @param {FiUrlService}           private _urlService
   * @param {FiTranslateLazyService} private _translateLazyService
   * @param {FiStorageService}       private _storageService
   */
  constructor(
    private _resourceService: FiResourceService,
    private _urlService: FiUrlService,
    private _translateLazyService: FiTranslateLazyService,
    private _storageService: FiStorageService
  ) {}

  /**
   * Get media
   */
  getMedia() {
    const langAcronym = this._translateLazyService.getLanguage();
    const params = new HttpParams()
      .set('limit', '100')
      .set('offset', '0')
      .set('sort', '-id')
      .set('status', 'APPROVED')
      .set('alive', 'true');

    const url = this._urlService.get('COMMUNICATION.MEDIA');

    const authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    return this._resourceService
      .list(url, {
        params: params,
        headers: {
          Authorization: 'Bearer ' + authData.token,
          // 'X-Language-ID': '3',
          'X-Language-Acronym': langAcronym.toUpperCase()
        }
      })
      .pipe(
        catchError(error => of(error)),
        flatMap(
          res => (res instanceof HttpErrorResponse ? of([]) : of(res.data))
        )
      );
  }
}
