import { Component, OnInit, OnChanges, Input, ContentChild, ElementRef, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

/* service interface */
interface ServiceInterface {
  id: number,
  active: true,
  name: string,
  type: string,
  maintenance: boolean,
  created_at: string,
  updated_at: string
}

/**
 * Component
 */
@Component({
  selector: 'fi-service-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.less']
})
export class FiServiceListDetailComponent implements OnInit, OnChanges {

  @Input() public language: string;

  @Input('data')
  data: ServiceInterface = null;

  @Input('type')
  type = 'all';

  @Output() OnSelectedService = new EventEmitter<ServiceInterface>();

  @ContentChild('largeBlock', { read: ElementRef })
  public largeBlockText;

  public services = null;

  /**
   * Constructor
   * @param {FiTranslateLazyService} private _translateLazyService
   */
  constructor(
    private _translateLazyService: FiTranslateLazyService
  ) {}

  /**
   * On init
   */
  ngOnInit() {

    this.services = this.data;

    if (!this.language) {
      this.language = this._translateLazyService.getLanguage();
    }

  }

  /**
   * On changes
   */
  ngOnChanges(changes: SimpleChanges) {

    if (!changes.data.firstChange) {
      this.services = changes.data.currentValue;
    }

  }

  /**
   * Handler select service
   */
  handlerSelectService(item: ServiceInterface) {

    this.OnSelectedService.emit(item);

  }

}
