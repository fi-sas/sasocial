import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiTickerComponent } from './ticker.component';

describe('TickerComponent', () => {
  let component: FiTickerComponent;
  let fixture: ComponentFixture<FiTickerComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [FiTickerComponent]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(FiTickerComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
        });
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
