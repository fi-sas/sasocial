import {
  OnDestroy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ElementRef,
  Renderer2,
  Directive,
  AfterViewInit,
  ContentChildren,
  QueryList,
  Input,
  OnChanges,
  EventEmitter,
  Inject,
  OnInit,
  ViewChildren,
} from "@angular/core";
import { Observable } from "rxjs";
import { map, take } from "rxjs/operators";
import {
  TickerInterface,
  FiTickerService,
} from "@fi-sas/tv/components/ticker/ticker.service";
import { FiTranslateLazyService } from "@fi-sas/tv/services/translate.lazy.service";
import { KioskState } from "@fi-sas/tv/app.state";
import { ApplicationState, Store } from "@fi-sas/core";
import { tick } from "@angular/core/testing";
@Directive({
  selector: "fi-ticker-wrap, [fi-ticker-wrap]",
})
export class FiTickerWrapperDirective {
  constructor(public el: ElementRef) {}
}

@Directive({
  selector: "fi-ticker-item, [fi-ticker-item]",
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    class: "ticker__item",
  },
})
export class FiTickerItemDirective implements OnChanges {
  @Input() scope: string;

  output = new EventEmitter();

  constructor(public el: ElementRef) {}

  ngOnChanges(changes: any) {
    this.output.emit(changes);
  }
}

@Component({
  selector: "fi-ticker",
  templateUrl: "./ticker.component.html",
  styleUrls: ["./ticker.component.less"],
})
export class FiTickerComponent implements AfterViewInit, OnDestroy, OnInit {
  public tickers$: Observable<TickerInterface[]>;
  public enableTicker = true;

  @ViewChild(FiTickerWrapperDirective)
  tickerContainer: FiTickerWrapperDirective;

  @ViewChildren(FiTickerItemDirective)
  tickerItemsNodes: QueryList<FiTickerItemDirective>;

  public tickerNodes: {
    totalWidth: number;
    nodeWidth: number;
    tickerNode: FiTickerItemDirective;
  }[] = [];
  public tickerFirstNode: FiTickerItemDirective;
  public tickerScopeNode: string;

  public tickerIndex = 0;
  public tickerMargin = 0;
  public tickerTotalWidth = 0;
  public tickerPaddingRight = 200;

  public interval: NodeJS.Timer;

  public scopeSubscribe: any;

  constructor(
    private r: Renderer2,
    private changeDetector: ChangeDetectorRef,
    private _tickerService: FiTickerService,
    private _translateService: FiTranslateLazyService,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  ngOnInit() {
    const store = this._appState.createSlice("ui").createSlice("header");
    store.watch().subscribe((state) => {
      this.enableTicker = state.ticker;
    });

    const tout = setTimeout(() => {
      this.tickers$ = this.requestTickers();
      clearTimeout(tout);
    }, 200);

    this._translateService.getService().onLangChange.subscribe((data) => {
      this.enableTicker = true;
      this._tickerService.forceReload();

      this.tickers$ = this.requestTickers();
    });
  }

  ngAfterViewInit() {
    this.tickerItemsNodes.changes.subscribe(() => {
      this.tickerItemsNodes.forEach((item) =>
        item.output.subscribe((i) => {
          this.tickerScopeNode =
            this.tickerNodes[this.tickerIndex].tickerNode.scope;
          this.changeDetector.markForCheck();
          this.changeDetector.detectChanges();
        })
      );

      const totalTicker = this.tickerItemsNodes.length;

      if (totalTicker) {
        /* set ticker first node */
        this.tickerFirstNode = this.tickerItemsNodes.first;

        /* set ticker scope */
        this.tickerScopeNode = this.tickerFirstNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();

        /* calculate and set widths */
        let totalWidth = 0;
        this.tickerItemsNodes.map((tickerNode) => {
          const nodeWidth = tickerNode.el.nativeElement.offsetWidth as number;
          totalWidth = totalWidth + nodeWidth;
          this.tickerNodes.push({
            totalWidth: totalWidth,
            nodeWidth: nodeWidth,
            tickerNode: tickerNode,
          });
        });

        /* set ticker total width */
        this.tickerTotalWidth =
          this.tickerContainer.el.nativeElement.offsetWidth;

        this.moveLeft();
      }
    });
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  /**
   * Request tickers
   */
  requestTickers() {
    return this._tickerService.getTickers().pipe(
      map((tickers) => {
        tickers && tickers.length > 0
          ? (this.enableTicker = true)
          : (this.enableTicker = false);
        this.enableTicker;
        return tickers;
      }),
      take(1)
    );
  }

  moveLeft(): void {
    this.tickerMargin = this.tickerTotalWidth;

    this.interval = setInterval(() => {
      /* last node reached */
      if (
        this.tickerIndex === this.tickerNodes.length - 1 &&
        this.tickerMargin <
          -(
            this.tickerNodes[this.tickerNodes.length - 1].totalWidth -
            this.tickerPaddingRight
          )
      ) {
        this.tickerMargin = this.tickerTotalWidth;
        this.tickerIndex = 0;

        this.tickerScopeNode =
          this.tickerNodes[this.tickerIndex].tickerNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();

        clearInterval(this.interval);
        this.moveLeft();
      }

      /* next node reached */
      if (
        this.tickerMargin + this.tickerNodes[this.tickerIndex].totalWidth <
          this.tickerTotalWidth &&
        this.tickerIndex !== this.tickerNodes.length - 1
      ) {
        this.tickerIndex = this.tickerIndex + 1;

        this.tickerScopeNode =
          this.tickerNodes[this.tickerIndex].tickerNode.scope;
        this.changeDetector.markForCheck();
        this.changeDetector.detectChanges();
      }

      /* move first node */
      this.r.setStyle(
        this.tickerFirstNode.el.nativeElement,
        "margin-left",
        this.tickerMargin-- + "px"
      );
    }, 10);
  }
}
