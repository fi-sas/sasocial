import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';
import {
  FiUrlService,
  FiResourceService,
  FiStorageService
} from '@fi-sas/core';
import { FiTickerService } from '@fi-sas/tv/components/ticker/ticker.service';

import {
  FiTickerComponent,
  FiTickerItemDirective,
  FiTickerWrapperDirective
} from './ticker.component';

@NgModule({
  imports: [FiShareModule],
  declarations: [
    FiTickerComponent,
    FiTickerItemDirective,
    FiTickerWrapperDirective
  ],
  providers: [
    {
      provide: FiTickerService,
      useClass: FiTickerService,
      deps: [
        FiUrlService,
        FiResourceService,
        FiTranslateLazyService,
        FiStorageService
      ]
    }
  ],
  exports: [FiTickerComponent, FiTickerItemDirective, FiTickerWrapperDirective]
})
export class FiTickerModule {}
