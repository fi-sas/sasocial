import { Injectable } from '@angular/core';
import { FiTranslateLazyService } from '../../services/translate.lazy.service';
import {
  FiResourceService,
  FiUrlService,
  FiStorageService
} from '@fi-sas/core';

import { Observable, of, timer, Subject } from 'rxjs';
import { map, shareReplay, flatMap, takeUntil } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';

export interface TickerInterface {
  category: {
    id: number;
    translations: [
      {
        language_id: number;
        name: string;
      }
    ];
  };
  id: number;
  status: string;
  translations: [
    {
      title?: string;
    }
  ];
  weight: number;
}

const REFRESH_INTERVAL = 1800000;

@Injectable()
export class FiTickerService {
  ticker$: Observable<TickerInterface[]>;
  reload$ = new Subject<void>();

  constructor(
    private _urlService: FiUrlService,
    private _resourceService: FiResourceService,
    private _translateService: FiTranslateLazyService,
    private _storageService: FiStorageService
  ) {
  }

  public getTickers() {
    if (!this.ticker$) {
      const timer$ = timer(0, REFRESH_INTERVAL);

      this.ticker$ = timer$.pipe(
        flatMap(_ => {
          return this.requestTickers()
        }),
        takeUntil(this.reload$),
        shareReplay(1)
      );
    }

    return this.ticker$;
  }

  public forceReload() {
    this.reload$.next();
    this.ticker$ = null;
  }

  private requestTickers() {
    const params = new HttpParams()
      .set('limit', '100')
      .set('offset', '0')
      .set('sort', '-id')
      .set('withRelated', 'category')
      .set('status', 'APPROVED')
      .set('alive', 'true');

    const langAcronym = this._translateService.getLanguage();
    const urlTickers = this._urlService.get('COMMUNICATION.TICKERS');

    const authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    return this._resourceService
      .list<TickerInterface>(urlTickers, {
        headers: {
          Authorization: 'Bearer ' + authData.token,
          // 'X-Language-ID': '3'
          'X-Language-Acronym': langAcronym.toUpperCase()
        },
        params: params
      })
      .pipe(
        map(resp => {
          return resp.data;
        })
      );
  }
}
