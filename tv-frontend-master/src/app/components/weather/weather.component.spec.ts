import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiWeatherComponent } from './weather.component';
import { FiWeatherService } from '@fi-sas/tv/components/weather/weather.service';
import { FiResourceService, FiCoreModule } from '@fi-sas/core';

describe('FiWeatherComponent', () => {
  let component: FiWeatherComponent;
  let fixture: ComponentFixture<FiWeatherComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiCoreModule],
        declarations: [FiWeatherComponent],
        providers: [
          {
            provide: FiWeatherService,
            useClass: FiWeatherService,
            deps: [FiResourceService]
          }
        ]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(FiWeatherComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
        });
    })
  );

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
