import { FiWeatherModule } from './weather.module';

describe('WeatherModule', () => {
  let weatherModule: FiWeatherModule;

  beforeEach(() => {
    weatherModule = new FiWeatherModule();
  });

  it('should create an instance', () => {
    expect(weatherModule).toBeTruthy();
  });
});
