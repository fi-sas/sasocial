import { NgModule } from '@angular/core';
import { FiShareModule } from '@fi-sas/share';

import { FiScrollDirective } from './scroll.directive';

@NgModule({
  imports: [FiShareModule],
  declarations: [FiScrollDirective],
  exports: [FiScrollDirective]
})
export class FiDirectivesModule {}
