import { Directive, ElementRef, AfterViewInit, Input } from '@angular/core';
const SimpleBar = require('simplebar').default;

@Directive({
  selector: 'fi-scroll, [fi-scroll]'
})
export class FiScrollDirective implements AfterViewInit {
  private el: any;

  public autoHide = true;
  public scrollbarMinSize = 10;

  public classContent = 'simplebar-content';
  public classScrollContent = 'simplebar-scroll-content';
  public classScrollbar = 'simplebar-scrollbar';
  public classTrack = 'simplebar-track';

  @Input('fi-scroll')
  set scrollOptions(params: any) {
    if (params.autoHide === false || params.autoHide === true) {
      this.autoHide = params.autoHide === true ? true : false;
    }

    if (params.scrollbarMinSize) {
      this.scrollbarMinSize = params.scrollbarMinSize
        ? params.scrollbarMinSize
        : this.scrollbarMinSize;
    }

    if (params.classContent) {
      this.classContent = params.classContent
        ? params.classContent
        : this.classContent;
    }

    if (params.classScrollContent) {
      this.classScrollContent = params.classScrollContent
        ? params.classScrollContent
        : this.classScrollContent;
    }

    if (params.classScrollbar) {
      this.classScrollbar = params.classScrollbar
        ? params.classScrollbar
        : this.classScrollbar;
    }

    if (params.classTrack) {
      this.classTrack = params.classTrack ? params.classTrack : this.classTrack;
    }
  }

  constructor(private _element: ElementRef) {}

  ngAfterViewInit() {
    this.el = new SimpleBar(this._element.nativeElement, {
      autoHide: this.autoHide,
      scrollbarMinSize: this.scrollbarMinSize,
      classNames: {
        content: this.classContent,
        scrollContent: this.classScrollContent,
        scrollbar: this.classScrollbar,
        track: this.classTrack
      }
    });

    this.el.recalculate();

    this.el.getScrollElement().addEventListener('scroll', () => {
      /* code here */
    });
  }
}
