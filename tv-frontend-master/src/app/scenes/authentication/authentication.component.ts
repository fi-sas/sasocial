import { Component, OnInit, Renderer2, Inject, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { forkJoin, of, Subject } from 'rxjs';
import { takeUntil, flatMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { NzModalService } from 'ng-zorro-antd';

import { FiAuthPresenter } from './authentication.presenter';

export type AUTHENTICATION_MODE = 'PIN' | 'RFID';

/* enums */
enum AuthenticationMode {
  PIN = 'PIN',
  RFID = 'RFID'
}

@Component({
  selector: 'fi-sas-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.less']
})
export class FiAuthenticationComponent implements OnInit, OnDestroy {
  /* public */
  public mode: AUTHENTICATION_MODE = AuthenticationMode.PIN;
  public authenticateForm: FormGroup = this._authPresenter.authenticatePinForm;

  /* private */
  private _subject = new Subject<boolean>();

  /**
   * Creates an instance of FiAuthenticationComponent
   * @param {Renderer2}             private _render
   * @param {FiAuthPresenter}       private _authPresenter
   * @param {TranslateService}      private _translate
   * @param {NzModalService}        private _modal
   */
  constructor(
    private _render: Renderer2,
    private _authPresenter: FiAuthPresenter,
    private _translate: TranslateService,
    private _modal: NzModalService
  ) {}

  /**
   * On init
   */
  ngOnInit() {}

  /**
   * On destroy
   */
  ngOnDestroy() {
    this._subject.next(true);
    this._subject.unsubscribe();
  }

  /**
   * On input focus.
   *
   * @param {FocusEvent} ev
   * @memberof FiAuthenticationComponent
   */
  inputOnFocus(ev: FocusEvent) {
    const el = (ev.target as HTMLInputElement).parentElement;

    this.setElementShadow(true, el);
  }

  /**
   * On input blur.
   *
   * @param {FocusEvent} ev
   * @memberof FiAuthenticationComponent
   */
  inputOnBlur(ev: FocusEvent) {
    const el = (ev.target as HTMLInputElement).parentElement;

    this.setElementShadow(false, el);
  }

  /**
   * Set element shadow.
   *
   * @param {boolean} active
   * @param {HTMLElement} el
   * @memberof FiAuthenticationComponent
   */
  setElementShadow(active: boolean, el: HTMLElement) {
    active
      ? this._render.addClass(el, 'focus')
      : this._render.removeClass(el, 'focus');
  }

  /**
   * Send notification
   * @param {string} type
   */
  sendNotification(type: string): void {
    forkJoin(
      this._translate.get('AUTHENTICATION.NOTIFICATION.TITLE'),
      this._translate.get('AUTHENTICATION.NOTIFICATION.DESCRIPTION')
    )
      .pipe(
        takeUntil(this._subject),
        flatMap(res => {
          this._modal.info({
            nzTitle: res[0],
            nzContent: res[1],
            nzWrapClassName: 'vertical-center-modal'
          });
          return of([]);
        })
      )
      .subscribe();
  }
}
