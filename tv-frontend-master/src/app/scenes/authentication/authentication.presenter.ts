import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FiAuthService } from '@fi-sas/tv/services/auth.service';
import { tap, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class FiAuthPresenter {
  isLoading = false;

  public authenticatePinForm: FormGroup = this._fb.group({
    username: ['', [Validators.required]],
    pin: ['', [Validators.required]]
  });

  constructor(private _fb: FormBuilder, private _authService: FiAuthService) {}

  public authenticateByPin() {
    this.validatePinForm();

    if (this.authenticatePinForm.valid) {
      return this.makeAuthenticationByPin().pipe(
        map(loggedin => {
          if (!loggedin) {
            this.authenticatePinForm.controls['username'].setErrors({
              required: true
            });
            this.authenticatePinForm.controls['pin'].setErrors({
              required: true
            });

            this.authenticatePinForm.controls['username'].markAsTouched();
            this.authenticatePinForm.controls['pin'].markAsTouched();
          }

          return loggedin;
        })
      );
    } else {
      return of(false);
    }
  }

  public authenticateByRfid(rfid: string) {
    return this.makeAuthenticationByRfid(rfid);
  }

  public validatePinForm() {
    // tslint:disable-next-line:forin
    for (const key in this.authenticatePinForm.controls) {
      this.authenticatePinForm.controls[key].markAsDirty();
      // this.authenticatePinForm.controls[key].markAsPristine();
      this.authenticatePinForm.controls[key].updateValueAndValidity();
    }
  }

  public makeAuthenticationByPin() {
    this.isLoading = true;

    const user = this.authenticatePinForm.value.username;
    const pin = this.authenticatePinForm.value.pin;

    return this._authService
      .loginByPin({ user: user, pin: pin })
      .pipe(tap(() => (this.isLoading = false)));
  }

  public makeAuthenticationByRfid(rfid: string) {
    this.isLoading = true;

    return this._authService
      .loginByRfid(rfid)
      .pipe(tap(() => (this.isLoading = false)));
  }

  public currentUser() {
    return this._authService.getCurrentUser();
  }

  public destroySession() {
    this._authService.logout();

    return of(true);
  }
}
