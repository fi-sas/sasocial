import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiHomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: FiHomeComponent,
    data: {
      name: 'home'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiHomeRoutingModule {}
