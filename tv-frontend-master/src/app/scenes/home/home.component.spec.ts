import { FiHomeModule } from './home.module';
import { FiCoreModule } from '@fi-sas/core';
import { FiHomeComponent } from './home.component';
import { OPTIONS_TOKEN } from '@fi-sas/configurator';
import { CONFIGURATION } from '@fi-sas/tv/app.config';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

describe('> Home Component', () => {
  let component: FiHomeComponent;
  let fixture: ComponentFixture<FiHomeComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [FiCoreModule, TranslateModule.forRoot(), FiHomeModule],
        providers: [
          { provide: OPTIONS_TOKEN, useValue: CONFIGURATION },
          {
            provide: FiTranslateLazyService,
            useClass: FiTranslateLazyService,
            deps: [TranslateService]
          }
        ]
      })
        .compileComponents()
        .then(() => {
          fixture = TestBed.createComponent(FiHomeComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
        });
    })
  );

  xit(
    '# Should have component instance',
    async(() => {
      expect(component).toBeTruthy();
    })
  );
});
