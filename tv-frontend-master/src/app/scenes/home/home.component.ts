import {
  Component,
  HostListener,
  HostBinding,
  OnInit,
  OnDestroy
} from '@angular/core';

import { ScreenAction } from '@fi-sas/tv/services/screen.reducer';

import { FiConditionService } from '@fi-sas/tv/services/condition.service';
import { FiConfigurator } from '@fi-sas/configurator';

@Component({
  selector: 'fi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class FiHomeComponent implements OnInit, OnDestroy {
  feed: boolean;
  @HostListener('document:click', ['$event'])
  kioskActivation(event: MouseEvent) {
    event.preventDefault();

    //this._conditionService.goto('dashboard');
  }

  /**
   * Constructor
   * @param {FiConditionService} private _conditionService
   */
  constructor(private _conditionService: FiConditionService, private _configurator: FiConfigurator,) {}

  /**
   * On init
   */
  ngOnInit() {
    this.feed = this._configurator
    .getOption<boolean>("FEED");
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    ScreenAction.next(false);
  }
}
