import { FiHomeModule } from './home.module';

import { TestBed, async } from '@angular/core/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

xdescribe('> Home Module', () => {
  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [TranslateModule.forRoot(), FiHomeModule],
        providers: [
          {
            provide: FiTranslateLazyService,
            useClass: FiTranslateLazyService,
            deps: [TranslateService]
          }
        ]
      }).compileComponents();
    })
  );

  it(
    '# Should resolve TranslateService',
    async(() => {
      expect(TestBed.get(TranslateService)).toBeTruthy();
    })
  );

  it(
    '# Should resolve TranslateLazyService',
    async(() => {
      expect(TestBed.get(FiTranslateLazyService)).toBeTruthy();
    })
  );
});
