import { NgModule, ApplicationRef } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { FiShareModule } from '@fi-sas/share';
import { FiHomeComponent } from './home.component';
import { FiHomeRoutingModule } from './home-routing.module';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

import { FiMediaModule } from '@fi-sas/tv/components/media/media.module';
import { FiTickerModule } from '@fi-sas/tv/components/ticker/ticker.module';
import { FiHeaderModule } from '@fi-sas/tv/components/header/header.module';

export const COMPONENTS_MODULES = [
  FiMediaModule,
  FiTickerModule,
  FiHeaderModule
];
@NgModule({
  imports: [
    ...COMPONENTS_MODULES,
    NgZorroAntdModule,
    TranslateModule,
    FiHomeRoutingModule,
    FiShareModule,
    FiTickerModule,
    FiMediaModule
  ],
  declarations: [FiHomeComponent]
})
export class FiHomeModule {
  constructor(
    private _appRef: ApplicationRef,
    private _translateLazyService: FiTranslateLazyService
  ) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');

    console.group('HOME MODULE REFERENCE');
    console.dir(this._appRef);
    console.groupEnd();
  }
}
