import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { merge } from "lodash";

import {
  HeaderActionDisable,
  HeaderActionEnable,
} from "@fi-sas/tv/components/header/header.reducer";
import { FiElectronService } from "@fi-sas/tv/services/electron.service";
import { FiStorageService } from "@fi-sas/core";
import { FiConfigurator } from "@fi-sas/configurator";

interface FormInstallValue {
  general: {
    lang: string;
    organization: string;
    type: string;
    feed: boolean;
    latitude: number;
    longitude: number;
  };
  network: {
    device: number;
    domain: string;
  };
}


@Component({
  selector: "fi-install",
  templateUrl: "./install.component.html",
  styleUrls: ["./install.component.less"],
})
export class FiInstallComponent implements OnInit, OnDestroy {
  configTabIndex = 0;

  formInstall: FormGroup;

  constructor(
    private _router: Router,
    private _electronService: FiElectronService,
    private _configurator: FiConfigurator,
    private _storage: FiStorageService
  ) { }

  ngOnInit() {
    this._storage.reset();
    HeaderActionDisable.next({ active: false, ticker: false });

    const urlPattern = /^(?:(?:https?):\/\/).*/i;

    this.formInstall = new FormGroup({
      general: new FormGroup({
        lang: new FormControl("pt", [Validators.required]),
        organization: new FormControl("IPVC", [Validators.required]),
        type: new FormControl("TV", [Validators.required]),
        feed: new FormControl(true, [Validators.required]),
        longitude: new FormControl(null, [Validators.required]),
        latitude: new FormControl(null, [Validators.required]),
      }),
      network: new FormGroup({
        device: new FormControl(6, [Validators.required]),
        domain: new FormControl("https://SASOCOCIAL.pt/", [
          Validators.required,
          Validators.pattern(urlPattern),
        ]),
      }),
    });

    this.formInstall.patchValue({
      general: {
        lang: this._electronService.preferences.has("DEFAULT_LANG")
          ? this._electronService.preferences.get("DEFAULT_LANG")
          : "pt",
        organization: this._electronService.preferences.has("ORGANIZATION")
          ? this._electronService.preferences.get("ORGANIZATION").THEME
          : "IPVC",
        type: this._electronService.preferences.has("DEVICE_TYPE")
          ? this._electronService.preferences.get("DEVICE_TYPE")
          : "TV",
        feed: this._electronService.preferences.has("FEED")
          ? this._electronService.preferences.get("FEED")
          : true,
        latitude: this._electronService.preferences.has("LATITUDE")
          ? this._electronService.preferences.get("LATITUDE")
          : 41.69323,
        longitude: this._electronService.preferences.has("LONGITUDE")
          ? this._electronService.preferences.get("LONGITUDE")
          : -8.83287,
      },
      network: {
        device: this._electronService.preferences.has("DEVICE_ID")
          ? this._electronService.preferences.get("DEVICE_ID")
          : "",
        domain: this._electronService.preferences.has("DOMAINS_API")
          ? this._electronService.preferences.get("DOMAINS_API")[0].HOST
          : "https://SASOCOCIAL.pt/",
      },
    });
  }

  ngOnDestroy() { }

  onSubmit($event: Event) {
    $event.preventDefault();

    const model: FormInstallValue = this.formInstall.value;

    const config = {
      DEFAULT_LANG: model.general.lang,
      FEED: model.general.feed,
      LATITUDE: model.general.latitude,
      LONGITUDE: model.general.longitude,
      DEVICE_ID: model.network.device,
      DEVICE_TYPE: model.general.type,
      DOMAINS_API: [
        { HOST: model.network.domain, KEY: "@api_gateway" },
        { HOST: "", KEY: "@local" },
      ],
      ORGANIZATION: {
        THEME: model.general.organization,
      },
    };

    this._electronService.changes.subscribe(() => {
      this._configurator.options = merge(this._configurator.options, config);
    });

    this._electronService.storeConfig(config);
    this._electronService.send("ELECTRON_RELAUCH");
  }
}
