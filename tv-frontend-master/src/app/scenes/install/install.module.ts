import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { FiShareModule } from '@fi-sas/share';
import { FiInstallComponent } from './install.component';
import { FiInstallRoutingModule } from './install-routing.module';

@NgModule({
  imports: [FiShareModule, NgZorroAntdModule, FiInstallRoutingModule],
  declarations: [FiInstallComponent]
})
export class FiInstallModule {}
