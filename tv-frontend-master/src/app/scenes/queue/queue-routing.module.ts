import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiQueueComponent } from './queue.component';

const routes: Routes = [
  {
    path: '',
    component: FiQueueComponent,
    data: {
      name: 'queue'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiQueueRoutingModule {}
