import {
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import { LangChangeEvent, TranslateService } from "@ngx-translate/core";
import { Observable, Subject } from "rxjs";
import { first, takeUntil } from "rxjs/operators";
import { FiConfigurator } from "@fi-sas/configurator";
import { FiQueueService } from "@fi-sas/tv/services/queue.service";
import { FiApiSocketioService } from "@fi-sas/tv/services/api-socketio.service";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { ServiceModelQueue } from "./model/service.model";

interface NextTicket {
  desk: string;
  operator: string;
  photo: string;
  service_name: string;
  subject_name: string;
  ticket: string;
}

@Component({
  selector: "fi-sas-queue",
  templateUrl: "./queue.component.html",
  styleUrls: ["./queue.component.less"],
  animations: [
    // the fade-in/fade-out animation.
    trigger("simpleFadeAnimation", [
      // the "in" style determines the "resting" state of the element when it is visible.
      state("in", style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(":enter", [style({ opacity: 0 }), animate(600)]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(":leave", animate(600, style({ opacity: 0 }))),
    ]),
  ],
})
export class FiQueueComponent implements OnInit, OnDestroy {
  unsubscribe$: Subject<void> = new Subject<void>();

  private _nextTicketAudio = new Audio();
  nextTicketObservable: Observable<NextTicket>;
  nextTicket: NextTicket = {
    desk: "-",
    operator: "-",
    photo: "",
    service_name: "-",
    subject_name: "-",
    ticket: "-",
  };

  feed: boolean;

  public locale: string;
  public title;
  services: ServiceModelQueue[] = [];

  constructor(
    private _translateService: TranslateService,
    private _configurator: FiConfigurator,
    private fiQueueService: FiQueueService,
    private _socketIO: FiApiSocketioService
  ) { }

  ngOnInit(): void {
    this.title = this._configurator
      .getOption<string>("ORGANIZATION.THEME", "IPVC")
      .toUpperCase();
    this.feed = this._configurator
      .getOption<boolean>("FEED");


    const currentLang = this._translateService.currentLang || "pt";

    this._translateService.onLangChange
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: LangChangeEvent) => {
        this.locale = event.lang + "-" + event.lang.toUpperCase();
      });

    this.locale = currentLang + "-" + currentLang.toUpperCase();
    this.getTvServices();
    this.createNextTicketSubscription();

    this._nextTicketAudio.src = "../../../assets/sounds/nextTicket.mp3";
    this._nextTicketAudio.load();
  }

  createNextTicketSubscription() {
    this.nextTicketObservable = new Observable((observer) => {
      this._socketIO
        .getQueueNextTicket()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((result) => {
          this._nextTicketAudio.pause();
          this._nextTicketAudio.currentTime = 0;
          this._nextTicketAudio.play();

          observer.next(result[0]);
        });
      setTimeout(() => observer.next(this.nextTicket), 100);
    });

    this.nextTicketObservable
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.nextTicket = result;
        this.getTvServices();
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getTvServices() {
    this.fiQueueService.getTvServices().pipe(first()).subscribe((data) => {
      this.services = data;
    })
  }
}
