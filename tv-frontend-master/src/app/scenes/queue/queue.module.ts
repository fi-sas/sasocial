import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiQueueComponent } from './queue.component';
import { FiQueueRoutingModule } from './queue-routing.module';
import { FiTickerModule } from '@fi-sas/tv/components/ticker/ticker.module';
import { FiHeaderModule } from '@fi-sas/tv/components/header/header.module';
import { FiMediaModule } from '@fi-sas/tv/components/media/media.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';
import { TranslateModule } from '@ngx-translate/core';

export const COMPONENTS_MODULES = [
  FiMediaModule,
  FiTickerModule,
  FiHeaderModule
];

@NgModule({
  declarations: [FiQueueComponent],
  imports: [
    FiQueueRoutingModule,
    ...COMPONENTS_MODULES,
    NgZorroAntdModule,
    TranslateModule,
    CommonModule
  ]
})
export class FiQueueModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
 }
