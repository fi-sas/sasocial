import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FiUnavailableComponent } from './unavailable.component';

const routes: Routes = [
  {
    path: '',
    component: FiUnavailableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiUnavailableRoutingModule {}
