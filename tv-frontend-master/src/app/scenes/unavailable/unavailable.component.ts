import { Component, OnInit, OnDestroy } from '@angular/core';

import { HeaderActionDisable } from '@fi-sas/tv/components/header/header.reducer';

@Component({
  selector: 'fi-sas-unavailable',
  templateUrl: './unavailable.component.html',
  styleUrls: ['./unavailable.component.less']
})
export class FiUnavailableComponent implements OnInit, OnDestroy {
  /**
   * On init
   */
  ngOnInit() {
    HeaderActionDisable.next({ active: true, ticker: false });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    HeaderActionDisable.next({ active: true, ticker: true });
  }
}
