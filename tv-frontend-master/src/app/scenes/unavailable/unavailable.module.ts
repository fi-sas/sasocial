import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { FiShareModule } from '@fi-sas/share';
import { FiUnavailableComponent } from './unavailable.component';
import { FiUnavailableRoutingModule } from './unavailable-routing.module';
import { FiTranslateLazyService } from '@fi-sas/tv/services/translate.lazy.service';

import PT_TRANSLATION from './pt.translation.json';
import EN_TRANSLATION from './en.translation.json';

@NgModule({
  imports: [FiShareModule, FiUnavailableRoutingModule, TranslateModule],
  declarations: [FiUnavailableComponent]
})
export class FiUnavailableModule {
  constructor(private _translateLazyService: FiTranslateLazyService) {
    this._translateLazyService.load(PT_TRANSLATION, 'pt');
    this._translateLazyService.load(EN_TRANSLATION, 'en');
  }
}
