import { Injectable } from "@angular/core";
import { FiConfigurator } from "@fi-sas/configurator";
import { FiStorageService, FiUrlService } from "@fi-sas/core";

import { environment } from "environments/environment";
import { BehaviorSubject, interval, Observable } from "rxjs";
import { filter, map } from "rxjs/operators";
import { io } from "socket.io-client";

interface SocketIOEvent {
  event: string,
  data: any
}

@Injectable({
  providedIn: "root",
})
export class FiApiSocketioService {

  private _eventSubject: BehaviorSubject<SocketIOEvent> = new BehaviorSubject(null);

  private _socketIO = null;

  constructor(
    private _configuratorService: FiConfigurator,
    private _storageService: FiStorageService
  ) {
    const authData = this._storageService.get<{ token: string }>(
      "DEVICE_TOKEN"
    );

    if (authData) {
      //super({ url: 'ws://0.0.0.0/api' ||environment.socketio_url, options: {}});
      this._socketIO = io(this._configuratorService.getOptionTree<{ DOMAINS_API: { HOST: string}[] }>("DOMAINS_API", true).DOMAINS_API[0].HOST, {
        transports: ["polling"],
        path: "/socket.io",
        reconnection: true,
        autoConnect: true,
        auth: {
          token: authData.token,
        },
      });

      this._socketIO.on("connect", () => {
        console.log("CONNECTED");
      });

      this._socketIO.on("ping", () => {
        console.log("PING");
      });

      this._socketIO.on("disconnect", (reason) => {
        console.log("DISCONNECTED", reason);
      });

      this._socketIO.on("reconnect_attempt", () => {
        console.log("RECONNECT_ATTEMPT");
      });

      this._socketIO.on("error", (...args) => {
        console.log("On Error");
        console.log(args);
      });


      this._socketIO.on("QUEUE_NEXT_TICKET", (...args) => {
        this._eventSubject.next({ event: 'QUEUE_NEXT_TICKET', data: args });
        console.log("QUEUE_NEXT_TICKET");
        console.log(args);
      });

      this.connect();
    }
  }

  connect() {
    console.log("Trying to connect");
    return this._socketIO.connect();
  }


  getQueueNextTicket(): Observable<any> {
    return this.getEventObservable('QUEUE_NEXT_TICKET');
  }

  getEventObservable(event?: string): Observable<any|SocketIOEvent> {
    if(event) {
      return this._eventSubject.asObservable().pipe(
        filter(evt => evt && evt.event === event),
        map(({data}) => data)
      )
    }

    return this._eventSubject.asObservable().pipe(
      filter(evt => evt !== null));
  }
}
