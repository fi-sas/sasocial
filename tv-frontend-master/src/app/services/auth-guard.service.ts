import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { FiAuthService } from './auth.service';
import { Inject } from '@angular/core';
import { ApplicationState, Store } from '@fi-sas/core';
import { KioskState } from '@fi-sas/tv/app.state';

export class FiAuthenticationGuardService implements CanActivate {
  constructor(
    private _router: Router,
    @Inject(ApplicationState) private _appState: Store<KioskState>
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const store = this._appState.createSlice('user');

    return store.watch(user => {
      if (user) {
        this.navigateToDashboard();
      }

      return user ? false : true;
    });
  }

  navigateToDashboard() {
    //this._router.navigate(['/dashboard']);
  }
}
