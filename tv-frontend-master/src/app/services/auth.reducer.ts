import { Action, Reducer } from '@fi-sas/core';
import { UserInterface } from '@fi-sas/tv/services/auth.service';

export const UserAction = new Action<{ user: {} | null }>('USER_ACTION');

export const userAuthReduce: Reducer<
  { user: UserInterface | null },
  { user: UserInterface | null }
> = (state, payload) => {
  return payload ? { ...state, ...payload.user } : null;
};
