import { Injectable } from "@angular/core";
import {
  FiUrlService,
  FiResourceService,
  FiStorageService,
  Resource,
} from "@fi-sas/core";

import { Observable, of } from "rxjs";
import { HttpErrorResponse } from "@angular/common/http";
import { timeout, catchError,map, mergeMap, first } from "rxjs/operators";
import { FiConfigurator } from "@fi-sas/configurator";

export interface TokenIdentifier {
  token: string;
  expires_in: Date;
}

export interface UserInterface {
  active: boolean;
  created_at: string;
  email: string;
  external: string | null;
  id: number;
  institute: string | null;
  name: string;
  phone: string;
  rfid: string;
  student_number: number | null;
  updated_at: string;
  user_name: string;
}

@Injectable()
export class FiAuthService {
  private _refreshTimeout;

  constructor(
    private _urlService: FiUrlService,
    private _configurator: FiConfigurator,
    private _storageService: FiStorageService,
    private _resourceService: FiResourceService
  ) {}

  public setDeviceToken(force = false) {
    const deviceID = this.whoAmI();

    if (!deviceID) {
      return of(false);
    }

    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN", {
      id: deviceID,
    });

    return this._requestDeviceToken(url);
  }

  whoAmI() {
    return this._configurator.getOption<number>("DEVICE_ID", false);
  }

  hasStorageToken() {
    return this._storageService.has("DEVICE_TOKEN");
  }

  validateToken() {
    const hasDeviceToken = this.hasStorageToken();

    if (hasDeviceToken) {
      const storageDeviceToken = this._storageService.get<{
        token: string;
        expires_in: Date;
      }>("DEVICE_TOKEN");

      const url = this._urlService.get("AUTH.VALIDATE_TOKEN", {});

      return this._requestValidateToken(url, storageDeviceToken.token).pipe(
        catchError((error) => of(false))
      );
    } else {
      return of(true);
    }
  }

  public loginByPin(params: { user: string; pin: string }) {
    const user = params.user;
    const pin = params.pin;

    const device = this._configurator.getOption("DEVICE_ID");

    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN", { id: device });

    return this._resourceService
      .create(url, { user_name: user, pin: pin })
      .pipe(
        timeout(2500),
        catchError((error) => of(error)),
        map((response: Resource<TokenIdentifier>) => {
          let authorized = false;

          if (response && Array.isArray(response.data)) {
            this._createDeviceTokenObj(response.data[0]);
            authorized = true;
          }

          return authorized;
        })
      );
  }

  public loginByRfid(rfid: string) {
    const device = this._configurator.getOption("DEVICE_ID");

    const url = this._urlService.get("AUTH.AUTHORIZE_TOKEN", { id: device });

    return this._resourceService.create(url, { rfid: rfid }).pipe(
      timeout(2500),
      catchError((error) => of(error)),
      map((response: Resource<TokenIdentifier>) => {
        let authorized = false;

        if (response && Array.isArray(response.data)) {
          this._createDeviceTokenObj(response.data[0]);
          authorized = true;
        }

        return authorized;
      })
    );
  }

  public getCurrentUser() {
    const deviceToken = this._storageService.get<{ token: string }>(
      "DEVICE_TOKEN",
      false
    );

    const url = this._urlService.get("AUTH.GET_USER");

    return this._resourceService.read<UserInterface>(url, {
      headers: {
        Authorization: "Bearer " + deviceToken.token,
      },
    });
  }

  public logout() {
    this._storageService.remove("DEVICE_TOKEN");
  }

  private _requestDeviceToken(url: string) {
    return this._resourceService.create<TokenIdentifier>(url, {}).pipe(
      timeout(2500),
      //catchError(error => of(error)),
      mergeMap((rs: Resource<TokenIdentifier> | HttpErrorResponse | Error) => {
        let state = true;

        if (rs instanceof HttpErrorResponse || rs instanceof Error) {
          this._storageService.remove("DEVICE_TOKEN");

          state = false;
        } else {
          this._createDeviceTokenObj(rs.data[0]);
        }

        return of(state);
      })
    );
  }

  private _createDeviceTokenObj(obj: TokenIdentifier) {
    this._storageService.set("DEVICE_TOKEN", obj);
    // CREATES THE TIMEOUT TO UPDATE THE TOKEN
    this._createTimeoutToUpdateToken(obj);
  }

  private _createTimeoutToUpdateToken(obj: TokenIdentifier) {
    const now = new Date().getTime();
    const diff = new Date(obj.expires_in).getTime() -
      now -
      1000 * 60;

    this._refreshTimeout = setTimeout(() => {
      this.requestRefreshToken().pipe(first()).subscribe();
      clearTimeout(this._refreshTimeout);
    }, diff);
  }

  private _requestValidateToken(url: string, token: string) {

    if(!this._refreshTimeout) {
      this._createTimeoutToUpdateToken(this._storageService.get("DEVICE_TOKEN"));
    }

    return this._resourceService
      .create<any>(url, { token })
      .pipe(
        catchError((error) => of(false)),
        map((response: Resource<any[]>) => {
          return response;
        })
      );
  }

  requestRefreshToken(): Observable<boolean> {
    return new Observable((observer) => {
      this._resourceService
        .create<TokenIdentifier>(this._urlService.get("AUTH.REFRESH_TOKEN", { type: "TV" }),{},{ withCredentials: true,})
        .pipe(first())
        .subscribe(
          (value) => {
            this._createDeviceTokenObj(value.data[0]);
            observer.next(true);
            observer.complete();
          },
          (err) => {
            observer.next(false);
            observer.complete();
          }
        );
    });
  }
}
