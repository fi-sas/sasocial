import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { ScreenAction } from '@fi-sas/tv/services/screen.reducer';
import { UserAction } from '@fi-sas/tv/services/auth.reducer';
/* import { MenuAction } from '@fi-sas/tv/components/menu/main/menu.reducer'; */

@Injectable()
export class FiConditionService {
  private _timeout = 60000;
  private _interval: NodeJS.Timer;

  constructor(
    private _router: Router,
    private _configurator: FiConfigurator,
    @Inject(DOCUMENT) private _doc: Document
  ) {
    this.setListener();
    this.reset();
    this.setInactive();
  }

  private setListener() {
    this._timeout = this._configurator.getOption('SCREEN_SAVER.TIME_OUT');

    if (this._timeout < 59000) {
      this._timeout = 60000;
    }

    this._doc.addEventListener('click', (event: MouseEvent) => {
      this.reset();
      this.setInactive();
    });
  }

  private setInactive() {
    this._interval = setInterval(() => {
      ScreenAction.next(true);
      UserAction.next(null);
      /*  MenuAction.next({ authentication: false, logged: false }); */

      this.gotoScreenSaver();
    }, this._timeout);
  }

  private reset() {
    clearInterval(this._interval);
  }

  goToQueue(): void {
    this._router.navigate(['/queue']);
  }

  gotoScreenSaver(): void {
    this._router.navigate(['/']);
  }

  goto(route: string): void {
    let url = route;
    let params = null;

    const segments = this._router
      .parseUrl(route)
      .toString()
      .split('?');

    if (segments && segments.length > 0) {
      url = segments[0];
      params = this._router.parseUrl(route);
    }

    this._router.navigate([url], params);
  }
}
