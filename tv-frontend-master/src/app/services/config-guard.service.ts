import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Inject } from '@angular/core';
import {
  FiUrlService,
  FiStorageService,
  FiResourceService
} from '@fi-sas/core';
import { FiConfigurator } from '@fi-sas/configurator';
import { map, flatMap, catchError } from 'rxjs/operators';

export interface RemoteConfigInterface {
  device: {
    active: boolean;
    components: {
      id: number;
      active: boolean;
      created_at: string;
      device_id: number;
      name: string;
      updated_at: string;
    }[];
    created_at: string;
    groups: {
      id: number;
      active: boolean;
      name: string;
      created_at: string;
      updated_at: string;
    }[];
    id: number;
    name: string;
    orientation: string;
    secondsToInactivity: string | null;
    type: string;
    updated_at: string;
    uuid: string;
  };
  languages: {
    acronym: string;
    active: boolean;
    id: number;
    name: string;
    created_at: string;
    updated_at: string;
  }[];
  services: {
    id: number;
    name: string;
    description: string;
    active: boolean;
    created_at: string;
    updated_at;
  }[];
}

export class FiConfigGuardService implements CanActivate {
  constructor(
    private _urlService: FiUrlService,
    private _configurator: FiConfigurator,
    private _storageService: FiStorageService,
    private _resourceService: FiResourceService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.getRemoteConfig().pipe(
      catchError(err => of({ data: [] })),
      map(response => {
        const config = response && response.data ? response.data[0] : null;

        if (config && config.device && config.device.secondsToInactivity) {
          this._configurator.setOption(
            'SCREEN_SAVER.TIME_OUT',
            config.device.secondsToInactivity * 1000
          );
        }

        if (
          config &&
          Array.isArray(config.languages) &&
          config.languages.length > 0
        ) {
          config.languages.forEach((lang, index) => {
            this._configurator.setOption(`LANGUAGES.${index}.id`, lang.id);
            this._configurator.setOption(`LANGUAGES.${index}.name`, lang.name);
            this._configurator.setOption(
              `LANGUAGES.${index}.acronym`,
              lang.acronym.toLowerCase()
            );
          });
        }

        if (config) {
          this._configurator.setOption('REMOTE_CONFIG', response.data[0]);
        }
      }),
      flatMap(() => of(true))
    );
  }

  getRemoteConfig() {
    const url = this._urlService.get('CONFIG.GET_CONFIG');

    const storageDeviceToken = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    return this._resourceService.read<RemoteConfigInterface>(url, {
      headers: {
        Authorization:
          'Bearer ' + (storageDeviceToken ? storageDeviceToken.token : '')
      }
    });
  }
}
