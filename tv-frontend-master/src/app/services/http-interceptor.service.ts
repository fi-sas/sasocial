import { FiAuthService } from '@fi-sas/tv/services/auth.service';
import { FiStorageService } from '@fi-sas/core';
import { Injectable, Inject } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of, throwError, Subject } from 'rxjs';
import { catchError, first, finalize, switchMap } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  refreshTokenInProgress = false;
  tokenRefreshedSource = new Subject();
  tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

  constructor(
    @Inject(NzModalService) private modalService: NzModalService,
    private _storageService: FiStorageService,
    private _authService: FiAuthService
  ) { }

  addAuthHeader(request: HttpRequest<any>): HttpRequest<any> {
    const authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    if (authData) {
      return request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + authData.token,
        }
      });
    }
    return request;
  }

  refreshToken(): Observable<any> {
    if (this.refreshTokenInProgress) {
      return new Observable(observer => {
        this.tokenRefreshed$.subscribe(() => {
          observer.next();
          observer.complete();
        });
      });
    } else {
      this.refreshTokenInProgress = true;
      return new Observable(observer => {
        this._authService.setDeviceToken().pipe(first(), finalize(() => {
          this.refreshTokenInProgress = false;
          this.tokenRefreshedSource.next();
          observer.next();
          observer.complete();
        })).subscribe();
      });
    }
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return this.refreshToken().pipe(switchMap(() => {
            req = this.addAuthHeader(req);
            return next.handle(req);
          }), catchError(err => {
            console.log(err);
            return throwError(error);
          }));
        }
        if (error instanceof HttpErrorResponse && error.status > 499) {
          console.error('An Error occured during the request');
          console.error(req);
          this.warningModal();
        }

        return throwError(error);
      })
    );
  }

  warningModal() {
    /*this.modalService.error({
      nzContent: 'Error on service connection was detected. Please inform the administrator.',
      nzWrapClassName: 'vertical-center-modal'
    });*/

    //ref.afterClose.pipe(first()).subscribe(() => ref.destroy());
  }
}
