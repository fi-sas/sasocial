import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FiResourceService } from '../../../libs/core/src/lib/resource.service';
import { FiStorageService } from '../../../libs/core/src/lib/storage.service';
import { FiUrlService } from '../../../libs/core/src/lib/url.service';
import { catchError, flatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { FiTranslateLazyService } from './translate.lazy.service';

@Injectable({
  providedIn: 'root'
})
export class FiQueueService {

  constructor(
    private _resourceService: FiResourceService,
    private _urlService: FiUrlService,
    private _translateLazyService: FiTranslateLazyService,
    private _storageService: FiStorageService
  ) { }

   /**
   * Get media
   */
  getTvServices() {
    const langAcronym = this._translateLazyService.getLanguage();
    const params = new HttpParams()
    .set('withRelated', 'subjects,translations')
    .set('query[active]', 'true');

    const url = this._urlService.get('QUEUE.TV_SERVICES');

    const authData = this._storageService.get<{ token: string }>(
      'DEVICE_TOKEN'
    );

    return this._resourceService
      .list(url, {
        params: params,
        headers: {
          Authorization: 'Bearer ' + authData.token,
          'X-Language-Acronym': langAcronym.toUpperCase()
        }
      })
      .pipe(
        catchError(error => of(error)),
        flatMap(
          res => (res instanceof HttpErrorResponse ? of([]) : of(res.data))
        )
      );
  }
}
