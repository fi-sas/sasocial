import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzI18nService, en_US, pt_PT } from 'ng-zorro-antd';

@Injectable()
export class FiTranslateLazyService {
  /**
   * Create instance of the service.
   *
   * @param {TranslateService} _translateService
   * @param {NzI18nService} _antLangService
   */
  constructor(
    @Inject(TranslateService) private _translateService: TranslateService,
    @Inject(NzI18nService) private _antLangService: NzI18nService
  ) {}

  /**
   * Load object of translations to the service.
   *
   * @param {Object} translation
   * @param {string} lang
   */
  load(translation: Object, lang?: string): void {
    const language = lang ? lang : this._translateService.currentLang;
    this._translateService.setTranslation(language, translation, true);
  }

  /**
   * Get current defined language.
   *
   * @returns string
   */
  getLanguage(): string {
    return this._translateService.currentLang;
  }

  /**
   * Set the language on Translation service and
   * on AntdService.
   *
   * @param {string} lang
   */
  setLanguage(lang: string): void {
    this._translateService.setDefaultLang(lang);
    this._translateService.use(lang);

    switch (lang) {
      case 'en':
        this._antLangService.setLocale(en_US);
        break;
      default:
        this._antLangService.setLocale(pt_PT);
        break;
    }
  }

  getService() {
    return this._translateService;
  }
}
